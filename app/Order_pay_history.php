<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_pay_history extends Model
{
    //
	protected $table = 'order_pay_history';
	
   protected $primaryKey = 'pay_history_id';

}
