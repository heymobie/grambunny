<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class MerchantRating extends Model

{

    protected $appends= ["user"];




   public function getUserAttribute()
   {

   	$user = User::find($this->user_id);

    if(!empty($user)){

    $userdata = array(
    "id" => $user['id'],
    "token" => isset($user['token'])?$user['token']:"",
    "name" => isset($user['name'])?$user['name']:"",
    "lname" => isset($user['lname'])?$user['lname']:"",
    "email" => isset($user['email'])?$user['email']:"",
    "profile_image" => isset($user['profile_image'])?$user['profile_image']:"",
    "license_front" => isset($user['license_front'])?$user['license_front']:"",
    "license_back" => isset($user['license_back'])?$user['license_back']:"",
    "customer_type" => isset($user['address'])?$user['customer_type']:"",
    "marijuana_card" => isset($user['marijuana_card'])?$user['marijuana_card']:"",
    "oauth_provider" => isset($user['oauth_provider'])?$user['oauth_provider']:"",
    "oauth_id" => isset($user['oauth_id'])?$user['oauth_id']:"",
    "user_mob" => isset($user['user_mob'])?$user['user_mob']:"",
    "user_address" => isset($user['user_address'])?$user['user_address']:"",
    "user_city" => isset($user['user_city'])?$user['user_city']:"",
    "user_states" => isset($user['user_states'])?$user['user_states']:"",
    "user_zipcode" => isset($user['user_zipcode'])?$user['user_zipcode']:"",
    "wallet_amount" => isset($user['wallet_amount'])?$user['wallet_amount']:"",
    "user_lat" => isset($user['user_lat'])?$user['user_lat']:"",
    "user_long" => isset($user['user_long'])?$user['user_long']:"",
    "avg_rating" => isset($user['avg_rating'])?(string)$user['avg_rating']:"",
    "rating_count" => isset($user['rating_count'])?(string)$user['rating_count']:"",
    "is_admin" => isset($user['is_admin'])?(string)$user['is_admin']:"",
    "user_status" => isset($user['user_status'])?(string)$user['user_status']:"",
    "user_pass" => isset($user['user_pass'])?$user['user_pass']:"",
    "deviceid" => isset($user['deviceid'])?$user['deviceid']:"",
    "devicetype" => isset($user['devicetype'])?$user['devicetype']:"",
    "carrier_id" => isset($user['carrier_id'])?(string)$user['carrier_id']:"",
    "carrier_name" => isset($user['carrier_name'])?$user['carrier_name']:"",
    "carrier_email" => isset($user['carrier_email'])?$user['carrier_email']:"",
    "api_token" => isset($user['api_token'])?$user['api_token']:"",
    "user_role" => isset($user['user_role'])?(string)$user['user_role']:"",
    "user_fbid" => isset($user['user_fbid'])?$user['user_fbid']:"",
    "user_gpid" => isset($user['user_gpid'])?$user['user_gpid']:"",
    "restrictions" => isset($user['restrictions'])?$user['restrictions']:"",
    "user_otp" => isset($user['user_otp'])?$user['user_otp']:"",
    "checkout_verificaiton" => isset($user['checkout_verificaiton'])?(string)$user['checkout_verificaiton']:"",
    "log_id" => isset($user['log_id'])?(string)$user['log_id']:"",
    "dob" => isset($user['dob'])?$user['dob']:"",
    "email_verified_at" => isset($user['email_verified_at'])?$user['email_verified_at']:"",
    "created_at" => isset($user['created_at'])?$user['created_at']:"",
    "updated_at" => isset($user['updated_at'])?$user['updated_at']:"",
    "stripe_id" => isset($user['stripe_id'])?$user['stripe_id']:"",
    "card_brand" => isset($user['card_brand'])?$user['card_brand']:"",
    "card_last_four" => isset($user['card_last_four'])?$user['card_last_four']:"",
    "trial_ends_at" => isset($user['trial_ends_at'])?$user['trial_ends_at']:"",
    "devicetoken" => isset($user['devicetoken'])?$user['devicetoken']:"",
    "os_name" => isset($user['os_name'])?$user['os_name']:"",
    "email_notification" => isset($user['email_notification'])?(string)$user['email_notification']:"",
    "notification" => isset($user['notification'])?(string)$user['notification']:"",
    "sms_notification" => isset($user['sms_notification'])?(string)$user['sms_notification']:"",
    "ProfileURL" => isset($user['ProfileURL'])?$user['ProfileURL']:"",
    "LicenseFrontURL" => isset($user['LicenseFrontURL'])?$user['LicenseFrontURL']:"",
    "LicenseBackURL" => isset($user['LicenseBackURL'])?$user['LicenseBackURL']:"",
    "MarijuanaCardURL" => isset($user['MarijuanaCardURL'])?$user['MarijuanaCardURL']:""                 
    );

        return $userdata;  

         }else{
            return (Object)[];
         }

   }



}

