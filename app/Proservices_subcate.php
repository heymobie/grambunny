<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proservices_subcate extends Model
{
    //
    public $timestamps = false;

    protected $table = 'product_service_sub_category';
    
    protected $primaryKey = 'id';

}
