<?php

namespace App;

use App\Rating;

use App\Vendor;

use App\Proservicescate;

use App\Proservices_subcate;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{

    //

    protected $table = 'product_service';

    protected $primaryKey = 'id';

   // protected $appends=["ratings","categoryname","ImageURL","subcategoryname","vendor","stockStatus","images"];

    protected $appends=["categoryname","ImageURL","subcategoryname","images","totalprice"];


    public function getCategoryNameAttribute(){

    	$category=Proservicescate::find($this->category_id);

		if ($category) {

			return $category->category;

		}

			return '';

		}


        public function getmerchantnameAttribute(){

    	$vids = Vendor::find($this->vendor_id);

		if ($vids) {

			return $vids->name.' '.$vids->last_name;

		}

			return 'Admin';

		}


    public function getsubcategorynameAttribute() {

		$subcategory=Proservices_subcate::find($this->sub_category_id);

		if ($subcategory) {

			return $subcategory->sub_category;

		}

		return "";

    }


        public function gettotalpriceAttribute() {

		$price=$this->price;
		$quantity=$this->quantity;

		$subtotal = $price*$quantity; 

		$subtotal = number_format($subtotal,2);

		return $subtotal;

    }



    public function getImageURLAttribute(){

      	return asset("/public/uploads/product")."/".$this->image;

	}

	

	public function getVendorAttribute(){

		return Vendor::find($this->vendor_id);

	}



	public function getStockStatusAttribute()

	{

		return $this->stock==1? ["class" => "","status"=>"In Stock"]: ["class" => "text-danger","status"=>"Out of Stock"];

	}



	public function getImagesAttribute()
	{

		$images=DB::table("ps_images")->where("ps_id",$this->id)->get();

		$imagesWithPath=$images->map(function($image){

			 $image->path=asset("/public/uploads/product")."/".$image->name;

			 return $image;

		});

	    foreach ($imagesWithPath as $key => $value) {
      	    $search = null;
	        $replace = '""';
	        array_walk($value,
	        function (&$v) use ($search, $replace){
	            $v = str_replace($search, $replace, $v);    
	            }                                                                     
	        );
	    } 

		return $imagesWithPath;

	}


	public function getRatingsAttribute()
    {

		$rating= Rating::where("type",1)->where("ps_id",$this->id)->where("status",1)->get();

        $sum= $rating->sum("rating");

        if ($rating->count() > 0) {

            $total=$sum/$rating->count();

            $totalRating= number_format( $total,1);

        }else{

            $totalRating= 0;

		}

        $reviews= Rating::where("type",1)->where("ps_id",$this->id)->where("status",1)->latest()->paginate(15);

		return ["reviews" => $reviews,"total" => $totalRating];

	}



}

