<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Cashier\Billable;

use App\Proservicescate;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Authenticatable 
{

    use Billable;

    /**

     * The attributes that are mass assignable.

     * 

     * @var array

     */

    /*protected $fillable = [

        'name','lname', 'email', 'password', 'user_status','user_pass','user_mob','carrier_id','carrier_name','carrier_email'

    ];*/


    protected $table = 'vendor';

	protected $primaryKey = 'vendor_id';

	protected $fillable = [

        'name','last_name', 'business_name', 'password', 'email','address','address1','suburb','state','zipcode','vendor_create','vendor_status'

    ];

     protected $appends=["vendor","licensefrontURL","licensebackURL","profileURL","profile2URL","profile3URL","profile4URL","membership","fullname","Category","Subcategory"];

      function getVendorAttribute(){

      $vendordtls = Vendor::find($this->vendor_id);

      $vendordata = array(
                 "vendor_id" => $vendordtls['vendor_id'],
                 "unique_id" => isset($vendordtls['unique_id'])?$vendordtls['unique_id']:"",
                    "name" => isset($vendordtls['name'])?$vendordtls['name']:"",
                    "username" => isset($vendordtls['username'])?$vendordtls['username']:"",
                    "last_name" => isset($vendordtls['last_name'])?$vendordtls['last_name']:"",
                    "business_name" => isset($vendordtls['business_name'])?$vendordtls['business_name']:"",
                    "email" => isset($vendordtls['email'])?$vendordtls['email']:"",
                    "mailing_address" => isset($vendordtls['mailing_address'])?$vendordtls['mailing_address']:"",
                    "address" => isset($vendordtls['address'])?$vendordtls['address']:"",
                    "avg_rating" => isset($vendordtls['avg_rating'])? (string)$vendordtls['avg_rating']:"",
                    "rating_count" => isset($vendordtls['rating_count'])? (string)$vendordtls['rating_count']:"",
                    "lat" => isset($vendordtls['lat'])?$vendordtls['lat']:"",
                    "lng" => isset($vendordtls['lng'])?$vendordtls['lng']:"",
                    "address1" =>isset($vendordtls['address1'])?$vendordtls['address1']:"",
                 "suburb" =>isset($vendordtls['suburb'])?$vendordtls['suburb']:"",
                    "state" => isset($vendordtls['state'])?$vendordtls['state']:"",
                    "city" => isset($vendordtls['city'])?$vendordtls['city']:"",
                    "zipcode" => isset($vendordtls['zipcode'])?$vendordtls['zipcode']:"",
                    "mob_no" => isset($vendordtls['mob_no'])?$vendordtls['mob_no']:"",
                    "contact_no" => isset($vendordtls['contact_no'])?$vendordtls['contact_no']:"",
                    "vendor_status" => isset($vendordtls['vendor_status'])? (string)$vendordtls['vendor_status']:"",
                    "login_status" => isset($vendordtls['login_status'])? (string)$vendordtls['login_status']:"",
                    "vendor_type" => isset($vendordtls['vendor_type'])?$vendordtls['vendor_type']:"",
                    "wallet_amount" => isset($vendordtls['wallet_amount'])?$vendordtls['wallet_amount']:"",
                    "deviceid" => isset($vendordtls['deviceid'])?$vendordtls['deviceid']:"",
                    "devicetype" => isset($vendordtls['devicetype'])?$vendordtls['devicetype']:"",
                    "market_area" => isset($vendordtls['market_area'])?$vendordtls['market_area']:"",
                    "service_radius" => isset($vendordtls['service_radius'])?$vendordtls['service_radius']:"",
                    "driver_license" => isset($vendordtls['driver_license'])?$vendordtls['driver_license']:"",
                    "license_expiry" => isset($vendordtls['license_expiry'])?$vendordtls['license_expiry']:"",
                    "license_front" => isset($vendordtls['license_front'])?$vendordtls['license_front']:"",
                    "license_back" => isset($vendordtls['license_back'])?$vendordtls['license_back']:"",
                    "ssn" => isset($vendordtls['ssn'])?$vendordtls['ssn']:"",
                    "dob" => isset($vendordtls['dob'])?$vendordtls['dob']:"",
                    "profile_img1" => isset($vendordtls['profile_img1'])?$vendordtls['profile_img1']:"",
                    "profile_img2" => isset($vendordtls['profile_img2'])?$vendordtls['profile_img2']:"",
                    "profile_img3" => isset($vendordtls['profile_img3'])?$vendordtls['profile_img3']:"",
                    "profile_img4" => isset($vendordtls['profile_img4'])?$vendordtls['profile_img4']:"",
                    "type" => isset($vendordtls['type'])? (string)$vendordtls['type']:"",
                    "category_id" => isset($vendordtls['category_id'])? (string)$vendordtls['category_id']:"",
                    "sub_category_id" => isset($vendordtls['sub_category_id'])?$vendordtls['sub_category_id']:"",
                    "service" => isset($vendordtls['service'])?$vendordtls['service']:"",
                    "permit_type" => isset($vendordtls['permit_type'])?$vendordtls['permit_type']:"",
                    "permit_number" => isset($vendordtls['permit_number'])?$vendordtls['permit_number']:"",
                    "permit_expiry" => isset($vendordtls['permit_expiry'])?$vendordtls['permit_expiry']:"",
                    "description" => isset($vendordtls['description'])?$vendordtls['description']:"",
                    "otp" => isset($vendordtls['otp'])?$vendordtls['otp']:"",
                    "forgetpass_request" => isset($vendordtls['forgetpass_request'])?$vendordtls['forgetpass_request']:"",
                    "forgetpass_request_status" => isset($vendordtls['forgetpass_request_status'])?$vendordtls['forgetpass_request_status']:"",
                    "plan_id" => isset($vendordtls['plan_id'])?$vendordtls['plan_id']:"",
                    "plan_purchased" => isset($vendordtls['plan_purchased'])?$vendordtls['plan_purchased']:"",
                    "plan_expiry" => isset($vendordtls['plan_expiry'])?$vendordtls['plan_expiry']:"",
                    "txn_id" => isset($vendordtls['txn_id'])?$vendordtls['txn_id']:"",
                    "created_at" => isset($vendordtls['created_at'])?$vendordtls['created_at']:"",
                    "updated_at" => isset($vendordtls['updated_at'])?$vendordtls['updated_at']:"",
                    "stripe_id" => isset($vendordtls['stripe_id'])?$vendordtls['stripe_id']:"",
                    "make" => isset($vendordtls['make'])?$vendordtls['make']:"",
                    "model" => isset($vendordtls['model'])?$vendordtls['model']:"",
                    "color" => isset($vendordtls['color'])?$vendordtls['color']:"",
                    "year" => isset($vendordtls['year'])?$vendordtls['year']:"",
                    "license_plate" => isset($vendordtls['license_plate'])?$vendordtls['license_plate']:"",
                    "views" => isset($vendordtls['views'])? (string)$vendordtls['views']:"",
                    "sales_tax" => isset($vendordtls['sales_tax'])? (string)$vendordtls['sales_tax']:"",
                    "excise_tax" => isset($vendordtls['excise_tax'])? (string)$vendordtls['excise_tax']:"",
                    "city_tax" => isset($vendordtls['city_tax'])? (string)$vendordtls['city_tax']:"",
                    "commission_rate" => isset($vendordtls['commission_rate'])? (string)$vendordtls['commission_rate']:"",
                    "delivery_fee" => isset($vendordtls['delivery_fee'])? (string)$vendordtls['delivery_fee']:"",
                    "minimum_order_amount" => isset($vendordtls['minimum_order_amount'])? (string)$vendordtls['minimum_order_amount']:"",
                    "map_icon" => isset($vendordtls['map_icon'])?$vendordtls['map_icon']:"",
                    "type_of_merchant" => isset($vendordtls['type_of_merchant'])?$vendordtls['type_of_merchant']:"",
                    "cash_card" => isset($vendordtls['cash_card'])? (string)$vendordtls['cash_card']:"",
                    "licensefrontURL" => isset($vendordtls['licensefrontURL'])?$vendordtls['licensefrontURL']:"",
                    "licensebackURL" => isset($vendordtls['licensebackURL'])?$vendordtls['licensebackURL']:"",
                    "profileURL" => isset($vendordtls['profileURL'])?$vendordtls['profileURL']:"",
                    "profile2URL" => isset($vendordtls['profile2URL'])?$vendordtls['profile2URL']:"",
                    "profile3URL" => isset($vendordtls['profile3URL'])?$vendordtls['profile3URL']:"",
                    "profile4URL" => isset($vendordtls['profile4URL'])?$vendordtls['profile4URL']:"",
                    "membership" => isset($vendordtls['membership'])?$vendordtls['membership']:"",
                    "fullname" => isset($vendordtls['fullname'])?$vendordtls['fullname']:"",
                    "Category" => isset($vendordtls['Category'])?$vendordtls['Category']:"",
                    "Subcategory" => isset($vendordtls['Subcategory'])?$vendordtls['Subcategory']:""
                          
                         );

      return $vendordata;     
         
   }  

    public function getLicensefrontURLAttribute()
    {

        if(!is_null($this->license_front)){

            return asset("public/uploads/vendor/license/".$this->license_front);

        }else{

            return ;//asset("public/uploads/vendor/profile/default.png");

        }

        

    }



    public function getLicensebackURLAttribute()

    {

        if(!is_null($this->license_back)){

            return asset("public/uploads/vendor/license/".$this->license_back);

        }else{

            return ;// asset("public/uploads/vendor/profile/default.png");

        }

        

    }



        public function getProfileURLAttribute()

    {

        if(!is_null($this->profile_img1)){

            return asset("public/uploads/vendor/profile/".$this->profile_img1);

        }else{

            return asset("public/uploads/vendor/profile/default.png");

        }

        

    }



    public function getProfile2URLAttribute()

    {

        if(!is_null($this->profile_img2)){

            return asset("public/uploads/vendor/profile/".$this->profile_img2);

        }else{

            return asset("public/uploads/vendor/profile/default.png");

        }

        

    }

	

    public function getProfile3URLAttribute()

    {

        if(!is_null($this->profile_img3)){

            return asset("public/uploads/vendor/profile/".$this->profile_img3);

        }else{

            return asset("public/uploads/vendor/profile/default.png");

        }

        

    }



    public function getProfile4URLAttribute()

    {

        if(!is_null($this->profile_img4)){

            return asset("public/uploads/vendor/profile/".$this->profile_img4);

        }else{

            return asset("public/uploads/vendor/profile/default.png");

        }

        

    }



    public function getMembershipAttribute()

    {

        if (!is_null($this->plan_expiry)) {

            $today=Carbon::now();

            //$today=Carbon::createFromFormat("Y-m-d","2020-01-31")->subDay();

            $purchase=Carbon::createFromFormat("Y-m-d",$this->plan_purchased);

            $expire=Carbon::createFromFormat("Y-m-d",$this->plan_expiry);

            $remainingDays=$today->diffInDays($expire,false);

            return ["remainingDays" => $remainingDays,"status" => $remainingDays > 0 ? '1' : '0' ];

        }else{   

            return ["remainingDays" => '0',"status" => '0'];

        }

    }



    public function getFullnameAttribute()

    {

        return $this->name." ".$this->last_name;

    }



    public function getCategoryAttribute()

    {



            $cat_name = DB::table('product_service_category')       

                        ->where('id', '=' ,$this->category_id)

                        ->value('category');
            if(empty($cat_name)){
                $cat_name ="";
            }            

            return $cat_name;

    }



    public function getSubcategoryAttribute()

    {



            $subcats = $this->sub_category_id;

            $subcatsde = json_decode($subcats);

            $subcatagorys = array();

            

            if(!empty($subcatsde)){ 



               /* foreach ($subcatsde as $subcat) {



                $subcatagory = DB::table('product_service_sub_category')

                ->where('id', '=' ,$subcat)

                ->where('status', '=' ,1)

                ->orderBy('id', 'asc')

                ->first(); 



                $subcatagorys[] = array(

                'id'=> $subcatagory->id,

                'sub_category'=>$subcatagory->sub_category

                );    



                } */



                $subcatagorys = DB::table('product_service_sub_category')

                    ->whereIn('id',$subcatsde)

                    ->orderBy('id', 'asc')

                    ->get();

            }
                       




            return $subcatagorys;       

    }



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];

}

