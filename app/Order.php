<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

use App\Vendor;

use App\Productservice;

use App\Coupon_code;

use DB;

class Order extends Model
{

   protected $appends = ['vendor','user','coupon'];

   function getVendorAttribute(){

   		/* $vendor = Vendor::find($this->vendor_id);
         if(!empty($vendor)){
            return $vendor;
         }else{
            return (Object)[];
         } */

      $vendordtls = Vendor::find($this->vendor_id);

      $vendordata = array(
                 "vendor_id" => $vendordtls['vendor_id'],
                 "unique_id" => isset($vendordtls['unique_id'])?$vendordtls['unique_id']:"",
                    "name" => isset($vendordtls['name'])?$vendordtls['name']:"",
                    "username" => isset($vendordtls['username'])?$vendordtls['username']:"",
                    "last_name" => isset($vendordtls['last_name'])?$vendordtls['last_name']:"",
                    "business_name" => isset($vendordtls['business_name'])?$vendordtls['business_name']:"",
                    "email" => isset($vendordtls['email'])?$vendordtls['email']:"",
                    "mailing_address" => isset($vendordtls['mailing_address'])?$vendordtls['mailing_address']:"",
                    "address" => isset($vendordtls['address'])?$vendordtls['address']:"",
                    "avg_rating" => isset($vendordtls['avg_rating'])? (string)$vendordtls['avg_rating']:"",
                    "rating_count" => isset($vendordtls['rating_count'])? (string)$vendordtls['rating_count']:"",
                    "lat" => isset($vendordtls['lat'])?$vendordtls['lat']:"",
                    "lng" => isset($vendordtls['lng'])?$vendordtls['lng']:"",
                    "address1" =>isset($vendordtls['address1'])?$vendordtls['address1']:"",
                 "suburb" =>isset($vendordtls['suburb'])?$vendordtls['suburb']:"",
                    "state" => isset($vendordtls['state'])?$vendordtls['state']:"",
                    "city" => isset($vendordtls['city'])?$vendordtls['city']:"",
                    "zipcode" => isset($vendordtls['zipcode'])?$vendordtls['zipcode']:"",
                    "mob_no" => isset($vendordtls['mob_no'])?$vendordtls['mob_no']:"",
                    "contact_no" => isset($vendordtls['contact_no'])?$vendordtls['contact_no']:"",
                    "vendor_status" => isset($vendordtls['vendor_status'])? (string)$vendordtls['vendor_status']:"",
                    "login_status" => isset($vendordtls['login_status'])? (string)$vendordtls['login_status']:"",
                    "vendor_type" => isset($vendordtls['vendor_type'])?$vendordtls['vendor_type']:"",
                    "wallet_amount" => isset($vendordtls['wallet_amount'])?$vendordtls['wallet_amount']:"",
                    "deviceid" => isset($vendordtls['deviceid'])?$vendordtls['deviceid']:"",
                    "devicetype" => isset($vendordtls['devicetype'])?$vendordtls['devicetype']:"",
                    "market_area" => isset($vendordtls['market_area'])?$vendordtls['market_area']:"",
                    "service_radius" => isset($vendordtls['service_radius'])?$vendordtls['service_radius']:"",
                    "driver_license" => isset($vendordtls['driver_license'])?$vendordtls['driver_license']:"",
                    "license_expiry" => isset($vendordtls['license_expiry'])?$vendordtls['license_expiry']:"",
                    "license_front" => isset($vendordtls['license_front'])?$vendordtls['license_front']:"",
                    "license_back" => isset($vendordtls['license_back'])?$vendordtls['license_back']:"",
                    "ssn" => isset($vendordtls['ssn'])?$vendordtls['ssn']:"",
                    "dob" => isset($vendordtls['dob'])?$vendordtls['dob']:"",
                    "profile_img1" => isset($vendordtls['profile_img1'])?$vendordtls['profile_img1']:"",
                    "profile_img2" => isset($vendordtls['profile_img2'])?$vendordtls['profile_img2']:"",
                    "profile_img3" => isset($vendordtls['profile_img3'])?$vendordtls['profile_img3']:"",
                    "profile_img4" => isset($vendordtls['profile_img4'])?$vendordtls['profile_img4']:"",
                    "type" => isset($vendordtls['type'])? (string)$vendordtls['type']:"",
                    "category_id" => isset($vendordtls['category_id'])? (string)$vendordtls['category_id']:"",
                    "sub_category_id" => isset($vendordtls['sub_category_id'])?$vendordtls['sub_category_id']:"",
                    "service" => isset($vendordtls['service'])?$vendordtls['service']:"",
                    "permit_type" => isset($vendordtls['permit_type'])?$vendordtls['permit_type']:"",
                    "permit_number" => isset($vendordtls['permit_number'])?$vendordtls['permit_number']:"",
                    "permit_expiry" => isset($vendordtls['permit_expiry'])?$vendordtls['permit_expiry']:"",
                    "description" => isset($vendordtls['description'])?$vendordtls['description']:"",
                    "otp" => isset($vendordtls['otp'])?$vendordtls['otp']:"",
                    "forgetpass_request" => isset($vendordtls['forgetpass_request'])?$vendordtls['forgetpass_request']:"",
                    "forgetpass_request_status" => isset($vendordtls['forgetpass_request_status'])?$vendordtls['forgetpass_request_status']:"",
                    "plan_id" => isset($vendordtls['plan_id'])?$vendordtls['plan_id']:"",
                    "plan_purchased" => isset($vendordtls['plan_purchased'])?$vendordtls['plan_purchased']:"",
                    "plan_expiry" => isset($vendordtls['plan_expiry'])?$vendordtls['plan_expiry']:"",
                    "txn_id" => isset($vendordtls['txn_id'])?$vendordtls['txn_id']:"",
                    "created_at" => isset($vendordtls['created_at'])?$vendordtls['created_at']:"",
                    "updated_at" => isset($vendordtls['updated_at'])?$vendordtls['updated_at']:"",
                    "stripe_id" => isset($vendordtls['stripe_id'])?$vendordtls['stripe_id']:"",
                    "make" => isset($vendordtls['make'])?$vendordtls['make']:"",
                    "model" => isset($vendordtls['model'])?$vendordtls['model']:"",
                    "color" => isset($vendordtls['color'])?$vendordtls['color']:"",
                    "year" => isset($vendordtls['year'])?$vendordtls['year']:"",
                    "license_plate" => isset($vendordtls['license_plate'])?$vendordtls['license_plate']:"",
                    "views" => isset($vendordtls['views'])? (string)$vendordtls['views']:"",
                    "sales_tax" => isset($vendordtls['sales_tax'])? (string)$vendordtls['sales_tax']:"",
                    "excise_tax" => isset($vendordtls['excise_tax'])? (string)$vendordtls['excise_tax']:"",
                    "city_tax" => isset($vendordtls['city_tax'])? (string)$vendordtls['city_tax']:"",
                    "commission_rate" => isset($vendordtls['commission_rate'])? (string)$vendordtls['commission_rate']:"",
                    "delivery_fee" => isset($vendordtls['delivery_fee'])? (string)$vendordtls['delivery_fee']:"",
                    "minimum_order_amount" => isset($vendordtls['minimum_order_amount'])? (string)$vendordtls['minimum_order_amount']:"",
                    "map_icon" => isset($vendordtls['map_icon'])?$vendordtls['map_icon']:"",
                    "type_of_merchant" => isset($vendordtls['type_of_merchant'])?$vendordtls['type_of_merchant']:"",
                    "cash_card" => isset($vendordtls['cash_card'])? (string)$vendordtls['cash_card']:"",
                    "licensefrontURL" => isset($vendordtls['licensefrontURL'])?$vendordtls['licensefrontURL']:"",
                    "licensebackURL" => isset($vendordtls['licensebackURL'])?$vendordtls['licensebackURL']:"",
                    "profileURL" => isset($vendordtls['profileURL'])?$vendordtls['profileURL']:"",
                    "profile2URL" => isset($vendordtls['profile2URL'])?$vendordtls['profile2URL']:"",
                    "profile3URL" => isset($vendordtls['profile3URL'])?$vendordtls['profile3URL']:"",
                    "profile4URL" => isset($vendordtls['profile4URL'])?$vendordtls['profile4URL']:"",
                    "membership" => isset($vendordtls['membership'])?$vendordtls['membership']:"",
                    "fullname" => isset($vendordtls['fullname'])?$vendordtls['fullname']:"",
                    "Category" => isset($vendordtls['Category'])?$vendordtls['Category']:"",
                    "Subcategory" => isset($vendordtls['Subcategory'])?$vendordtls['Subcategory']:""
                          
                         );

      return $vendordata;     
         
   }



   public function getUserAttribute()

   {

   	$user = User::find($this->user_id);

    if(!empty($user)){

    $userdata = array(
    "id" => $user['id'],
    "token" => isset($user['token'])?$user['token']:"",
    "name" => isset($user['name'])?$user['name']:"",
    "lname" => isset($user['lname'])?$user['lname']:"",
    "email" => isset($user['email'])?$user['email']:"",
    "profile_image" => isset($user['profile_image'])?$user['profile_image']:"",
    "license_front" => isset($user['license_front'])?$user['license_front']:"",
    "license_back" => isset($user['license_back'])?$user['license_back']:"",
    "customer_type" => isset($user['address'])?$user['customer_type']:"",
    "marijuana_card" => isset($user['marijuana_card'])?$user['marijuana_card']:"",
    "oauth_provider" => isset($user['oauth_provider'])?$user['oauth_provider']:"",
    "oauth_id" => isset($user['oauth_id'])?$user['oauth_id']:"",
    "user_mob" => isset($user['user_mob'])?$user['user_mob']:"",
    "user_address" => isset($user['user_address'])?$user['user_address']:"",
    "user_city" => isset($user['user_city'])?$user['user_city']:"",
    "user_states" => isset($user['user_states'])?$user['user_states']:"",
    "user_zipcode" => isset($user['user_zipcode'])?$user['user_zipcode']:"",
    "wallet_amount" => isset($user['wallet_amount'])?$user['wallet_amount']:"",
    "user_lat" => isset($user['user_lat'])?$user['user_lat']:"",
    "user_long" => isset($user['user_long'])?$user['user_long']:"",
    "avg_rating" => isset($user['avg_rating'])?(string)$user['avg_rating']:"",
    "rating_count" => isset($user['rating_count'])?(string)$user['rating_count']:"",
    "is_admin" => isset($user['is_admin'])?(string)$user['is_admin']:"",
    "user_status" => isset($user['user_status'])?(string)$user['user_status']:"",
    "user_pass" => isset($user['user_pass'])?$user['user_pass']:"",
    "deviceid" => isset($user['deviceid'])?$user['deviceid']:"",
    "devicetype" => isset($user['devicetype'])?$user['devicetype']:"",
    "carrier_id" => isset($user['carrier_id'])?(string)$user['carrier_id']:"",
    "carrier_name" => isset($user['carrier_name'])?$user['carrier_name']:"",
    "carrier_email" => isset($user['carrier_email'])?$user['carrier_email']:"",
    "api_token" => isset($user['api_token'])?$user['api_token']:"",
    "user_role" => isset($user['user_role'])?(string)$user['user_role']:"",
    "user_fbid" => isset($user['user_fbid'])?$user['user_fbid']:"",
    "user_gpid" => isset($user['user_gpid'])?$user['user_gpid']:"",
    "restrictions" => isset($user['restrictions'])?$user['restrictions']:"",
    "user_otp" => isset($user['user_otp'])?$user['user_otp']:"",
    "checkout_verificaiton" => isset($user['checkout_verificaiton'])?(string)$user['checkout_verificaiton']:"",
    "log_id" => isset($user['log_id'])?(string)$user['log_id']:"",
    "dob" => isset($user['dob'])?$user['dob']:"",
    "email_verified_at" => isset($user['email_verified_at'])?$user['email_verified_at']:"",
    "created_at" => isset($user['created_at'])?$user['created_at']:"",
    "updated_at" => isset($user['updated_at'])?$user['updated_at']:"",
    "stripe_id" => isset($user['stripe_id'])?$user['stripe_id']:"",
    "card_brand" => isset($user['card_brand'])?$user['card_brand']:"",
    "card_last_four" => isset($user['card_last_four'])?$user['card_last_four']:"",
    "trial_ends_at" => isset($user['trial_ends_at'])?$user['trial_ends_at']:"",
    "devicetoken" => isset($user['devicetoken'])?$user['devicetoken']:"",
    "os_name" => isset($user['os_name'])?$user['os_name']:"",
    "email_notification" => isset($user['email_notification'])?(string)$user['email_notification']:"",
    "notification" => isset($user['notification'])?(string)$user['notification']:"",
    "sms_notification" => isset($user['sms_notification'])?(string)$user['sms_notification']:"",
    "ProfileURL" => isset($user['ProfileURL'])?$user['ProfileURL']:"",
    "LicenseFrontURL" => isset($user['LicenseFrontURL'])?$user['LicenseFrontURL']:"",
    "LicenseBackURL" => isset($user['LicenseBackURL'])?$user['LicenseBackURL']:"",
    "MarijuanaCardURL" => isset($user['MarijuanaCardURL'])?$user['MarijuanaCardURL']:""                 
    );
        return $userdata;  

         /* $users = DB::table('users')->where("id",$this->user_id)->get();

         foreach ($users as $key => $value) {
            $search = null;
            $replace = '';
            array_walk($value,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            );
        }  
         
         return $users; */  

         }else{
            return (Object)[];
         }

   }



  /* public function getProductAttribute()

   {



   	//return Productservice::find($this->ps_id);

   	

   } */



   public function getCouponAttribute()

   {



   	$Coupon = Coupon_code::find($this->coupon_id);
      if(!empty($Coupon)){
            return $Coupon;
         }else{
            return (Object)[];
         }
   	

   }



}

