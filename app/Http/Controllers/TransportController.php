<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

Use DB;
use App\Restaurant;
use Route;
use Response;
use View;
use Cart;
use Session;
use Auth;

use App\Transport_order_history;
use App\Transport_order;

use App\Transport_payment;
use App\Transport_payment_history;

class TransportController extends Controller
{

	public function __construct(){ 
	    //	$this->middleware('auth');
    }
	
	public function get_from_locationlist()
	{
		
		 $return = array();

		$keyword = trim(Input::get('term'));
		if(!empty($keyword))
		{	
				$i=0;
				
					
			if(is_numeric($keyword))
			{
				/*STATE SEARCH WITH ABBRIVATION*/
				$Suburb_detail  = DB::table('location')
								->where('pincode', 'like' ,$keyword.'%')
								->where('status', '=' ,'1')
								->get();	
					
				if(!empty($Suburb_detail)){
					foreach($Suburb_detail as $slist)		
					{	
						//array_push($return,array('label'=>$slist->Suburb_name,'value'=>$slist->Suburb_name));	
							 array_push($return,array('label'=>$slist->pincode.' - '.$slist->Suburb_name.', '.$slist->City_name.', '.$slist->state,'value'=>$slist->pincode.' - '.$slist->Suburb_name.', '.$slist->City_name.', '.$slist->state,'suburb'=>$slist->Suburb_name,'id'=>$slist->location_id,'pincode'=>$slist->pincode,'city'=>$slist->City_name,'state'=>$slist->state,'search_type'=>'pcode'));
									
						$i++;			
					}
					
				}
				else
				{
					array_push($return,array('label'=>'No Results'));	
				}
				/*STATE SEARCH */
			}
			else
			{
				/*STATE SEARCH WITH ABBRIVATION*/
				$Suburb_detail  = DB::table('location')
								->where('Suburb_name', 'like' , $keyword.'%')
								->where('status', '=' ,'1')
								->get();	
					
				if(!empty($Suburb_detail)){
					foreach($Suburb_detail as $slist)		
					{	
						//array_push($return,array('label'=>$slist->Suburb_name,'value'=>$slist->Suburb_name));	
							 array_push($return,array('label'=>$slist->Suburb_name.' - '.$slist->pincode.', '.$slist->City_name.', '.$slist->state,'value'=>$slist->Suburb_name.' - '.$slist->pincode.', '.$slist->City_name.', '.$slist->state,'suburb'=>$slist->Suburb_name,'id'=>$slist->location_id,'pincode'=>$slist->pincode,'city'=>$slist->City_name,'state'=>$slist->state,'search_type'=>'surbur'));
									
						$i++;			
					}
					
				}
				else
				{
					array_push($return,array('label'=>'No Results'));	
				}
				/*STATE SEARCH */
			}		
		
				
				
			}
		
		return json_encode($return);
	
	
	}
	public function get_locationlist()
	{
		
		 $return = array();

		$keyword = trim(Input::get('term'));
		if(!empty($keyword))
		{	
				$i=0;
				
					
			if(is_numeric($keyword))
			{
				/*STATE SEARCH WITH ABBRIVATION*/
				$Suburb_detail  = DB::table('location')
								->where('pincode', 'like' ,$keyword.'%')
								->get();	
					
				if(!empty($Suburb_detail)){
					foreach($Suburb_detail as $slist)		
					{	
						//array_push($return,array('label'=>$slist->Suburb_name,'value'=>$slist->Suburb_name));	
							 array_push($return,array('label'=>$slist->pincode.' - '.$slist->Suburb_name.', '.$slist->City_name.', '.$slist->state,'value'=>$slist->pincode.' - '.$slist->Suburb_name.', '.$slist->City_name.', '.$slist->state,'suburb'=>$slist->Suburb_name,'id'=>$slist->location_id,'pincode'=>$slist->pincode,'city'=>$slist->City_name,'state'=>$slist->state,'search_type'=>'pcode'));
									
						$i++;			
					}
					
				}
				else
				{
					array_push($return,array('label'=>'No Results'));	
				}
				/*STATE SEARCH */
			}
			else
			{
				/*STATE SEARCH WITH ABBRIVATION*/
				$Suburb_detail  = DB::table('location')
								->where('Suburb_name', 'like' , $keyword.'%')
								->get();	
					
				if(!empty($Suburb_detail)){
					foreach($Suburb_detail as $slist)		
					{	
						//array_push($return,array('label'=>$slist->Suburb_name,'value'=>$slist->Suburb_name));	
							 array_push($return,array('label'=>$slist->Suburb_name.' - '.$slist->pincode.', '.$slist->City_name.', '.$slist->state,'value'=>$slist->Suburb_name.' - '.$slist->pincode.', '.$slist->City_name.', '.$slist->state,'suburb'=>$slist->Suburb_name,'id'=>$slist->location_id,'pincode'=>$slist->pincode,'city'=>$slist->City_name,'state'=>$slist->state,'search_type'=>'surbur'));
									
						$i++;			
					}
					
				}
				else
				{
					array_push($return,array('label'=>'No Results'));	
				}
				/*STATE SEARCH */
			}		
		
				
				
			}
		
		return json_encode($return);
	
	
	}
	
	function show_returntime_drop()
	{
	 	 $get_time = trim(Input::get('ontime'));
		
		 $tt =   strtotime($get_time);//strtotime(substr($get_time,0,2).':'.substr($get_time,2,3));
		
		if(Input::get('type')=='same'){		
			$starttime = date ("H:i",($tt+(180*60)));
		}
		if(Input::get('type')=='diff'){		
			$starttime = date ("H:i",($tt));
		}
		
		
	
	$dopdown = '';
		$dopdown = '<select class="form-control search_returntime" name="return_time" id="return_time" required="required">'; 
		$dopdown .= '<option value="">Select time</option>';
					
						// your start time
			$endtime = '22:00';  // End time
			$duration = '15';  // split by 15min
			 
			
			$start_time    = strtotime ($starttime); //change to strtotime
			$end_time      = strtotime ($endtime); //change to strtotime
			 
			$add_mins  = $duration * 60;
			 
			while ($start_time <= $end_time) // loop between time
			{
				$dtime = date ("H:i", $start_time);
				$sdtval = str_replace(':','',$dtime);
		
			$dopdown .= '<option value="'.$sdtval.'" >'.$dtime.'</option>';
			   $start_time += $add_mins; // to check endtie=me
			}
				$dopdown .= '</select>';
				
		echo	$dopdown;
	}
	
	
	function get_transportlist()
	{
	
	//	echo '<pre>';
	//	print_r($_REQUEST);

			DB::enableQueryLog();  
	
	  $surbur_lable='';
	  $surbur_name='';
	  $post_code='';
	  $loc_id='';
	  $search_type='';
	  
	  $to_surbur_lable='';
	  $to_surbur_name='';
	  $to_post_code='';
	  $to_loc_id='';
	  $to_search_type='';
	  
	  $on_time='';
	  $return_time = '';
	  $ondate='';
	  $return_date='';
	  
	  
	  
	  if(Session::has('on_date'))
	   {
	   		$ondate=Session::get('on_date');
	   }
	   if(Session::has('on_time'))
	   {
		  $on_time =  Session::get('on_time');
	   }
	   if(Session::has('return_date'))
	   {
			$return_date = Session::get('return_date');
	   }
	   if(Session::has('return_time'))
	   {
			$return_time = Session::get('return_time');
	   }
	   
	  
	   if(isset($_GET['on_date']) && (!empty($_GET['on_date'])))
	   {
	   		$ondate=$_GET['on_date'];
	   		Session::set('on_date', $_GET['on_date']);
	   }
	   if(isset($_GET['on_time']) && (!empty($_GET['on_time'])))
	   {
	   	  $tt =$_GET['on_time'];
		  $on_time =  (substr($tt,0,2).':'.substr($tt,2,3));
	  	  Session::set('on_time', $on_time);
	   }
	   if(isset($_GET['return_date']) && (!empty($_GET['return_date'])))
	   {
	      
    	 $return_date=$_GET['return_date'];
	  	 Session::set('return_date', $_GET['return_date']);
	   }
	   if(isset($_GET['return_time']) && (!empty($_GET['return_time'])))
	   {
	   		$tt =$_GET['return_time'];
			$return_time =  (substr($tt,0,2).':'.substr($tt,2,3));
	   		Session::set('return_time', $return_time);
	   }
	   		 
	   
	   $day_name = '';
	   
				 
	   if(
	   		(isset($ondate) && (!empty($ondate))) && 
	 	  (isset($return_date) && (!empty($return_date))) && 
		  (strtotime($ondate) == strtotime($return_date) )
	   )
	   
	   {
	   			$day_name =  strtolower(date("D",strtotime($return_date)));
				
	   }
	  
	  
	  if(isset($_GET['surbur']) && (!empty($_GET['surbur'])))
	  {
	  	  $surbur_lable=$_GET['from_lable'];
		  $surbur_name=$_GET['surbur'];
		  $post_code=$_GET['pcode'];
		  $loc_id=$_GET['loc_id'];
		  $search_type=$_GET['search_type'];
		  
		  $from_location = array('surbur_lable'=>$surbur_lable,
		  						 'surbur_name'=>$surbur_name,
		  						 'post_code'=>$post_code,
		  						 'loc_id'=>$loc_id,
		  						 'search_type'=>$search_type
		  						);
		  Session::set('from_location', $from_location);
	  }
	  
	  
	  if(isset($_GET['to_suburb']) && (!empty($_GET['to_suburb'])))
	  {
	  	  $to_surbur_lable=$_GET['to_lable'];
		  $to_surbur_name=$_GET['to_suburb'];
		  $to_post_code=$_GET['to_pcode'];
		  $to_loc_id=$_GET['to_loc_id'];
		  $to_search_type=$_GET['to_search_type'];
		  
		  $to_location = array('to_surbur_lable'=>$to_surbur_lable,
		  						 'to_surbur_name'=>$to_surbur_name,
		  						 'to_post_code'=>$to_post_code,
		  						 'to_loc_id'=>$to_loc_id,
		  						 'to_search_type'=>$to_search_type
		  						);
		  Session::set('to_location', $to_location);
	  }
	  
	  
	  
	  if(empty($surbur_name) && (Session::has('from_location')))
	  {
	  	
		  $surbur_lable=Session::get('from_location')['surbur_lable'];
		  $surbur_name=Session::get('from_location')['surbur_name'];
		  $post_code=Session::get('from_location')['post_code'];
		  $loc_id=Session::get('from_location')['loc_id'];
		  $search_type=Session::get('from_location')['search_type'];
	  }
	  
	  if(empty($to_surbur_name) && (Session::has('to_location')))
	  {	  	
		  $to_surbur_lable=Session::get('to_location')['to_surbur_lable'];
		  $to_surbur_name=Session::get('to_location')['to_surbur_name'];
		  $to_post_code=Session::get('to_location')['to_post_code'];
		  $to_loc_id=Session::get('to_location')['to_loc_id'];
		  $to_search_type=Session::get('to_location')['to_search_type'];
		
	  }
	  
	  
	  
	  
	  
	  
		
	  $category_list  = DB::table('vehicle_cate')->where('vcate_status', '=' ,'1')->orderBy('vcate_name', 'asc')->get();	
	  
	  $class_list  = DB::table('vehicle_class')->where('vclass_status', '=' ,'1')->orderBy('vclass_name', 'asc')->get();	
	  
	  
	  
	  
	  
	  //
	  
//	  categoryType
	  
	  
	 $vehicle_listing = DB::table('vehicle');
	   $vehicle_listing = $vehicle_listing->leftJoin('transport', 'vehicle.vehicle_transid', '=', 'transport.transport_id');
	  $vehicle_listing = $vehicle_listing->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id');
	   $vehicle_listing = $vehicle_listing->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id');
	  $vehicle_listing = $vehicle_listing->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id');
	  $vehicle_listing = $vehicle_listing->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id');
	$vehicle_listing = $vehicle_listing->select('transport.*','vehicle.*','vehicle_cate.vcate_name','vehicle_class.vclass_name','vehicle_make.make_name','vehicle_model.model_name');	
	  $vehicle_listing = $vehicle_listing->where('transport.transport_status', '!=' , 'INACTIVE');	
	  $vehicle_listing = $vehicle_listing->where('vehicle.vehicle_status', '=' , '1');
	  
	  
	  
	   if(isset($_GET['vehicle_id']) && ($_GET['vehicle_id']>0) && (!empty($surbur_name)))
	   {	
	 	 $vehicle_listing = $vehicle_listing->where('vehicle.vehicle_id', '=' ,$_GET['vehicle_id']);
	  
	   }
	  
	  
	  /* CHECK VEHICLE IS BOOKED OR NOT */
	
				
	  
	  
	/* if(
	 		(isset($_GET['on_date']) && (!empty($_GET['on_date']))) &&
			(isset($_GET['return_date']) && (!empty($_GET['return_date'])))
			
	 )	*/	
	 
	 
	if(
		 (isset($ondate) && (!empty($ondate))) && 
	 	  (isset($return_date) && (!empty($return_date))) 	
	 )	
	 {				
	 
			$vehicle_listing = $vehicle_listing->whereNotIn('vehicle.vehicle_id', function($query) {
			
					//->whereNotIn('order_status', ['2','8','6'])
			
					$query->select('vehicle_id')
					  ->from('transport_order')
					  ->where(function ($subquery) {
					  
					  	$on_date1 = date('Y-m-d',strtotime(Session::get('on_date')));
	  	  				$return_date1 =date('Y-m-d',strtotime(Session::get('return_date')));
			
						 $subquery->whereBetween('order_ondate', [$on_date1, $return_date1])
					 	 ->orwhereBetween('order_returndate', [$on_date1, $return_date1]);
					  })
					  ->whereNotIn('order_status', ['2','8','6']);	
										
				});
	 }	
	
	  
	  /*             END                */
	  
	  /* CHECH RESTAURANT IS CLOSED OR NOT THAT DAY */
	  if(!empty($day_name) )
	   {
	   
			$vehicle_listing = $vehicle_listing->whereNotIn('vehicle.vehicle_transid', function($query) {
			
			
	  	  				$return_date1 =   Session::get('return_date');
						
			$day_name =  strtolower(date("D",strtotime($return_date1)));
					$query->select('transport_id')
                  ->from('transport')
                   ->whereRaw('(FIND_IN_SET("'.$day_name.'",transport_close))');
					
			
				});
		}		 
				 
	  
	  
	  /* CHECH RESTAURANT IS CLOSED OR NOT THAT DAY  END */
	  
	  
	  
		if(isset($_GET['classType']) && (!empty($_GET['classType'])))				
		{	
		
						
			
			$vehicle_listing = $vehicle_listing->where(function($query) {
			
					$l=0;
					foreach($_GET['classType'] as $ftype )
					{	
					
						
						
						if($ftype>0)
						{
							if($l>0){
							 $query = $query->orWhere('vehicle.vehicle_classid',$ftype );
							 }
							 else
							 {
								 $query = $query->where('vehicle.vehicle_classid',$ftype );
							 }
						}
						
						
						$l++;
					}
			
				});
		
		
		  // $vehicle_listing = $vehicle_listing->where('vehicle.vehicle_classid',$_GET['classType']);
		}
		
		
		if(isset($_GET['categoryType']))				
		{				
			
			$vehicle_listing = $vehicle_listing->where(function($query) {
			
					$l=0;
					foreach($_GET['categoryType'] as $ftype )
					{	
					
						
						
						if($ftype>0)
						{
							if($l>0){
							 $query = $query->orWhere('vehicle.vehicle_catid',$ftype );
							 }
							 else
							 {
								 $query = $query->where('vehicle.vehicle_catid',$ftype );
							 }
						}
						
						
						$l++;
					}
			
				});
		
		}
	
	
	
	
	 	if(!empty($surbur_name) && (!empty($search_type) && ($search_type=='surbur')) )
		{	
			
			 $vehicle_listing = $vehicle_listing->leftJoin('transport_service', 'transport.transport_id', '=', 'transport_service.tport_service_transid');	
			 $vehicle_listing = $vehicle_listing->where('transport_service.tport_service_suburb',trim($surbur_name) );			
			 
		}
		
		
	 	if(!empty($post_code) && (!empty($search_type) && ($search_type=='pcode')))
		{
			
			 $vehicle_listing = $vehicle_listing->leftJoin('transport_service', 'transport.transport_id', '=', 'transport_service.tport_service_transid');	
		     $vehicle_listing = $vehicle_listing->where('transport_service.tport_service_postcode',$post_code );
		
		}
		
		
	
	
	
	 
	 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='2'))				
	 {		
		  
		  $vehicle_listing = $vehicle_listing->orderBy('vehicle.vehicle_minrate', 'asc');
		 
	 }
	 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='3'))				
	 {
		  $vehicle_listing = $vehicle_listing->orderBy('vehicle_make.make_name', 'asc');	
		
	 }
	 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='4'))				
	 {
		$vehicle_listing = $vehicle_listing->orderBy('vehicle_make.make_name', 'desc');		
	 }
	 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='5'))				
	 {				
		$vehicle_listing = $vehicle_listing->orderBy(DB::raw("(SELECT count(`re_restid`) as total_review FROM review WHERE `re_status` = 'PUBLISHED' and `re_restid` = `restaurant`.`rest_id`)"), 'desc'); 
	 }		
	
		
	  
	  
	  
	 $vehicle_listing = $vehicle_listing->orderBy('transport.transport_classi', 'asc');	
	 
     $vehicle_listing = $vehicle_listing->groupBy('vehicle.vehicle_id');
	
	
	
	 
	 
	 
	 $vehicle_listing = $vehicle_listing->paginate(10);
	 
$url_pag_no = '';

		if(!empty($vehicle_listing->nextPageUrl()))
		{
			$url = $vehicle_listing->nextPageUrl();
			 $a = explode('=',$url);
			$url_pag_no = $a[1];
			
		}
		
		
		$total_vehicle = $vehicle_listing->total();
		
		$search_from = '1';
		$detailpage_link='';
	   if(isset($_GET['vehicle_id']) && ($_GET['vehicle_id']>0) && ($total_vehicle==1) && 
	   (!empty($surbur_name)) &&   (!empty($to_loc_id)) && (!empty($on_time))  &&  (!empty($ondate)) && (!empty($return_time))  && (!empty($return_date))
	   )
	   {	
	 	 $search_from = '2';
		 
		 
		 			$cart_bookinginfo = array(
										'vehicle_id' =>$vehicle_listing[0]->vehicle_id,
										'vehicle_from' =>$surbur_name,
										'vehicle_to' =>$to_surbur_lable,
										'vehicle_ontime' =>$on_time,
										'vehicle_ondate' =>$ondate,
										'vehicle_returntime' =>$return_time,
										'vehicle_returndate' =>$return_date
			
									 );
		
			Session::set('cart_bookinginfo', $cart_bookinginfo);
		 
		 
		$detailpage_link =  url('/'.strtolower(str_replace(' ','_',trim($vehicle_listing[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_listing[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_listing[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_listing[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_listing[0]->vehicle_id))));
		 
		//$detailpage_link='';
	  
	   }	   
	   elseif(isset($_GET['vehicle_id']) && ($_GET['vehicle_id']>0) && ($total_vehicle==0) )
	   {	
	 	 $search_from = '3';
		 
						  $vehicle_again1 =  DB::table('vehicle');
						  $vehicle_again1 =  $vehicle_again1->leftJoin('transport', 'vehicle.vehicle_transid', '=', 'transport.transport_id');
						  $vehicle_again1 = $vehicle_again1->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id');
						  $vehicle_again1 =  $vehicle_again1->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id');
						  $vehicle_again1 =  $vehicle_again1->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id');
						  $vehicle_again1 = $vehicle_again1->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id');
						 $vehicle_again1 = $vehicle_again1->select('transport.*','vehicle.*','vehicle_cate.vcate_name','vehicle_class.vclass_name','vehicle_make.make_name','vehicle_model.model_name');	
						  $vehicle_again1 = $vehicle_again1->where('transport.transport_status', '!=' , 'INACTIVE');	
						  $vehicle_again1 = $vehicle_again1->where('vehicle.vehicle_status', '=' , '1');
	  
											  
						  /* CHECK VEHICLE IS BOOKED OR NOT */
	
									
						  
						  
						 if(
								
								 (isset($ondate) && (!empty($ondate))) && 
								  (isset($return_date) && (!empty($return_date))) 	
						 )				
						 {				
						 
								$vehicle_again1 = $vehicle_again1->whereNotIn('vehicle.vehicle_id', function($query) {
								
								
										$query->select('vehicle_id')
											  ->from('transport_order')
											  ->where(function ($subquery) {
											  
											  
											$on_date1 = date('Y-m-d',strtotime(Session::get('on_date')));
	  	  									$return_date1 =date('Y-m-d',strtotime(Session::get('return_date')));
						
						
											 $subquery->whereBetween('order_ondate', [$on_date1, $return_date1])
												 ->orwhereBetween('order_returndate', [$on_date1, $return_date1]);
											  })
											  ->whereNotIn('order_status', ['2','8','6']);	
										
								
									});
						 }	
						
						  
						  /*             END                */
						  
						  /* CHECH RESTAURANT IS CLOSED OR NOT THAT DAY */
						  if(!empty($day_name) )
						   {
						   
								$vehicle_again1 = $vehicle_again1->whereNotIn('vehicle.vehicle_transid', function($query) {
								
								$return_date1 =   Session::get('return_date');
								$day_name =  strtolower(date("D",strtotime($return_date1)));
										$query->select('transport_id')
									  ->from('transport')
									   ->whereRaw('(FIND_IN_SET("'.$day_name.'",transport_close))');
										
								
									});
							}		 
									 
						  
						  
						  /* CHECH RESTAURANT IS CLOSED OR NOT THAT DAY  END */
						  
						  
							if(isset($_GET['classType']) && (!empty($_GET['classType'])))				
							{	
							
								$vehicle_listing = $vehicle_listing->where(function($query) {
						
								$l=0;
								foreach($_GET['classType'] as $ftype )
								{	
								
									
									
									if($ftype>0)
									{
										if($l>0){
										 $query = $query->orWhere('vehicle.vehicle_classid',$ftype );
										 }
										 else
										 {
											 $query = $query->where('vehicle.vehicle_classid',$ftype );
										 }
									}
									
									
									$l++;
								}
						
							});
							 //  $vehicle_again1 = $vehicle_again1->where('vehicle.vehicle_classid',$_GET['classType']);
							   
							   
							}
							
							
							if(isset($_GET['categoryType']))				
							{	
								
								
								
								$vehicle_again1 = $vehicle_again1->where(function($query) {
								
										$l=0;
										foreach($_GET['categoryType'] as $ftype )
										{	
										
											
											
											if($ftype>0)
											{
												if($l>0){
												 $query = $query->orWhere('vehicle.vehicle_catid',$ftype );
												 }
												 else
												 {
													 $query = $query->where('vehicle.vehicle_catid',$ftype );
												 }
											}
											
											
											$l++;
										}
								
									});
							
							}
						
						
						
						
							if(!empty($surbur_name) && (!empty($search_type) && ($search_type=='surbur')) )
							{
									
								
								 $vehicle_again1 = $vehicle_again1->leftJoin('transport_service', 'transport.transport_id', '=', 'transport_service.tport_service_transid');	
								 $vehicle_again1 = $vehicle_again1->where('transport_service.tport_service_suburb',trim($surbur_name) );	
								
						
								
								 
							}
							if(!empty($post_code) && (!empty($search_type) && ($search_type=='pcode')))
							{
								
								 $vehicle_again1 = $vehicle_again1->leftJoin('transport_service', 'transport.transport_id', '=', 'transport_service.tport_service_transid');	
								 $vehicle_again1 = $vehicle_again1->where('transport_service.tport_service_postcode',$post_code );
							
							}
							
							
						
						
						
						 
						 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='2'))				
						 {		
							  
							  $vehicle_again1 = $vehicle_again1->orderBy('vehicle.vehicle_minrate', 'asc');
							 
						 }
						 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='3'))				
						 {
							  $vehicle_again1 = $vehicle_again1->orderBy('vehicle_make.make_name', 'asc');	
							
						 }
						 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='4'))				
						 {
							$vehicle_again1 = $vehicle_again1->orderBy('vehicle_make.make_name', 'desc');		
						 }
						 if(isset($_GET['sort_by']) && ($_GET['sort_by']=='5'))				
						 {	
								
							$vehicle_again1 = $vehicle_again1->orderBy(DB::raw("(SELECT count(`re_restid`) as total_review FROM review WHERE `re_status` = 'PUBLISHED' and `re_restid` = `restaurant`.`rest_id`)"), 'desc'); 
						}		
						
							
						  
						  
						  
						 $vehicle_again1 = $vehicle_again1->orderBy('transport.transport_classi', 'asc');	
						 
						 $vehicle_again1 = $vehicle_again1->groupBy('vehicle.vehicle_id');
						
						
						
						 
						 
						 
						 $vehicle_again1 = $vehicle_again1->paginate(10);
						 
					$url_pag_no = '';
					
							if(!empty($vehicle_again1->nextPageUrl()))
							{
								$url = $vehicle_again1->nextPageUrl();
								 $a = explode('=',$url);
								$url_pag_no = $a[1];
								
							}
							
							
							 $total_vehicle = $vehicle_again1->total();
							 $vehicle_listing = $vehicle_again1;
	  
	   }
	   
			
	/* echo '<pre>';
	 echo $search_from ;
	// dd(DB::getQueryLog()); 
	 print_r($vehicle_listing)	;
	
			print_r(DB::getQueryLog());
			
	exit;*/	
	 $data_onview = array('vehicle_listing' =>$vehicle_listing,
	  					   'total_vehicle' =>$total_vehicle,
	  					   'category_list' =>$category_list,
	  					   'class_list' =>$class_list,
						   'page_no'=>$url_pag_no,
						   'surbur_lable'=>$surbur_lable,
						   'surbur_name'=>$surbur_name,
						   'post_code'=>$post_code,
						   'loc_id'=>$loc_id,
						   'search_type'=>$search_type,						   
	  
						   'to_surbur_lable'=>$to_surbur_lable,
						   'to_surbur_name'=>$to_surbur_name,
						   'to_post_code'=>$to_post_code,
						   'to_loc_id'=>$to_loc_id,
						   'to_search_type'=>$to_search_type,
						   
						   'on_time'=>$on_time,
						   'return_time'=>$return_time,
						   'ondate'=>$ondate,
						   'return_date'=>$return_date
  					   );
					   
		 if(request()->ajax()) {
		 
	//	 $top_counter_msg = $total_vehicle.' results In '.$surbur_name;
		  $top_counter_msg = 'Results In '.$surbur_name;
		 
	$cart_view =  view('ajax.transport_listing', array('vehicle_listing' =>$vehicle_listing,
	  					   'total_vehicle' =>$total_vehicle,
	  					   'category_list' =>$category_list,
	  					   'class_list' =>$class_list,
						   'page_no'=>$url_pag_no,
						   'surbur_lable'=>$surbur_lable,
						   'surbur_name'=>$surbur_name,
						   'post_code'=>$post_code,
						   'loc_id'=>$loc_id,
						   'search_type'=>$search_type,						   
	  
						   'to_surbur_lable'=>$to_surbur_lable,
						   'to_surbur_name'=>$to_surbur_name,
						   'to_post_code'=>$to_post_code,
						   'to_loc_id'=>$to_loc_id,
						   'to_search_type'=>$to_search_type,
						   
						   'on_time'=>$on_time,
						   'return_time'=>$return_time,
						   'ondate'=>$ondate,
						   'return_date'=>$return_date
  					   ))->render();
		 
    	 return response()->json( array('redirect_on' =>$search_from,'redirect_page' =>$detailpage_link, 'listing_view'=>$cart_view,'top_counter_msg'=>$top_counter_msg) );
	 
          //  return view('ajax.transport_listing')->with($data_onview);
        }
		else
		{
		    return view('transport_listing')->with($data_onview);
		}		

		
		
	}
	
	
	
	
	
	function show_search()
	{
		$vehicle_id = $_GET['vehicle_id'];
		$vehicle_from=$_GET['vehicle_from'];
		$vehicle_to=$_GET['vehicle_to'];
		$vehicle_ontime=$_GET['vehicle_ontime'];
		$vehicle_ondate=$_GET['vehicle_ondate'];
		$vehicle_returntime=$_GET['vehicle_returntime'];
		$vehicle_returndate=$_GET['vehicle_returndate'];
		
	if( (!empty($vehicle_from)) && (!empty($vehicle_to)) && (!empty($vehicle_ontime)) && 
		(!empty($vehicle_ondate)) && (!empty($vehicle_returntime)) && (!empty($vehicle_returndate))
	 )	
	{	
		
		 $vehicle_detail = DB::table('vehicle');
		  $vehicle_detail = $vehicle_detail->leftJoin('transport', 'vehicle.vehicle_transid', '=', 'transport.transport_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id');
		  $vehicle_detail = $vehicle_detail->select('transport.*','vehicle.*','vehicle_cate.vcate_name','vehicle_class.vclass_name','vehicle_make.make_name','vehicle_model.model_name');	
		  $vehicle_detail = $vehicle_detail->where('transport.transport_status', '!=' , 'INACTIVE');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_status', '=' , '1');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_id', '=' , $vehicle_id);
		  
		  if(!empty($vehicle_from) )
		{	
			
			 $vehicle_detail = $vehicle_detail->leftJoin('transport_service', 'transport.transport_id', '=', 'transport_service.tport_service_transid');	
			 $vehicle_detail = $vehicle_detail->where('transport_service.tport_service_suburb',trim($vehicle_from) );			
			 
		}
		  
		  
		  $vehicle_detail = $vehicle_detail->get();
	
		  if(count($vehicle_detail)>0)
		  {
			
				$cart_bookinginfo = array(
											'vehicle_id' =>$vehicle_id,
											'vehicle_from' =>$vehicle_from,
											'vehicle_to' =>$vehicle_to,
											'vehicle_ontime' =>$vehicle_ontime,
											'vehicle_ondate' =>date('Y-m-d',strtotime($vehicle_ondate)),
											'vehicle_returntime' =>$vehicle_returntime,
											'vehicle_returndate' =>date('Y-m-d',strtotime($vehicle_returndate))
				
										 );
			
				Session::set('cart_bookinginfo', $cart_bookinginfo);
			
			
			$detailpage_link =  url('/'.strtolower(str_replace(' ','_',trim($vehicle_detail[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_id))));
			
			  return response()->json( array('redirect_on' =>'1','redirect_page' => $detailpage_link) );
		  }
		  else
		  {
			
			$detailpage_link =  url('/transport_listing');
			
				 return response()->json( array('redirect_on' =>'2','redirect_page' => $detailpage_link) );
		  
		  }
	 }
	 else
	 {
		 $detailpage_link =  url('/transport_listing');
	  	
		 	 return response()->json( array('redirect_on' =>'2','redirect_page' => $detailpage_link) );
	  
	 }
		
		
	}
	
	
	function vehicle_detail()
	{
	
	
	
	
		  $tran_subrub =  Route::current()->getParameter('subrub');
		
		  $vehicle_reho =  Route::current()->getParameter('vehicle');
		
		  $vehicle_class =  Route::current()->getParameter('class');
		
		  $vehicle_make =  Route::current()->getParameter('make');
		
		  $vehicle_id =  Route::current()->getParameter('vehicle_id');
	
	//Session::set('cart_bookinginfo', $cart_bookinginfo);
	
		if(Session::has('cart_bookinginfo') && (Session::get('cart_bookinginfo')['vehicle_id']==$vehicle_id))
		{
		
		
			
			
			$date1=date_create(Session::get('cart_bookinginfo')['vehicle_ondate']);
			$date2=date_create(Session::get('cart_bookinginfo')['vehicle_returndate']);
			
			$diff=date_diff($date1,$date2);
			$count_no_days =  $diff->format("%a");


		
		  $vehicle_detail = DB::table('vehicle');
		  $vehicle_detail = $vehicle_detail->leftJoin('transport', 'vehicle.vehicle_transid', '=', 'transport.transport_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id');							
		  $vehicle_detail = $vehicle_detail->select('transport.*','vehicle.*','vehicle_cate.vcate_name','vehicle_class.vclass_name','vehicle_make.make_name','vehicle_model.model_name');	
		  $vehicle_detail = $vehicle_detail->where('transport.transport_status', '!=' , 'INACTIVE');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_status', '=' , '1');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_id', '=' , $vehicle_id);
		  
		
		
		
		 $vehicle_detail = $vehicle_detail->groupBy('vehicle.vehicle_id');
		 
		 
		 
		 $vehicle_detail = $vehicle_detail->get();
	
							
			if(!empty($vehicle_detail))
			{	
			
			
			 $trans_id = $vehicle_detail[0]->vehicle_transid;
				 $vehicle_id = $vehicle_detail[0]->vehicle_id;
			
				 
			/*	 COUNT TOTAL SUCESSFULL SUBMITED ORDER NUMBER FOR REGISTERED USER	*/
			$user_tot_order =0;
			if(!Auth::guest()) 
			{
				$user_id = Auth::user()->id;
				$user_tot_order = DB::table('transport_order')			
							->select('*')
							->where('transport_order.user_id', '=' ,$user_id)
							->where('transport_order.trans_id', '=' ,$trans_id)
							->where('transport_order.order_status', '!=' , '8' )
							->count();	
			
					
			}
			
			/*	 COUNT TOTAL SUCESSFULL SUBMITED ORDER NUMBER FOR REGISTERED USER END	*/			
					 		
			/* REVIEW START */
			
			$count_review = DB::table('transport_review')
						->where('re_transid', '=' ,$trans_id) 
						->where('re_vehicleid', '=' ,$vehicle_id)
						->where('re_status', '=' ,'PUBLISHED')
						->orderBy('re_vehicleid', 'desc')						
						->count();	
			$avg_rating = '';	
							
			if($count_review>0)			
			{
			$count_avg_rating =0;
			$total_rating = DB::table('transport_review')	
                		->select( DB::raw('SUM(re_rating) as rating'))
						->where('re_transid', '=' ,$trans_id) 
						->where('re_vehicleid', '=' ,$vehicle_id) 
						->where('re_status', '=' ,'PUBLISHED')
						->orderBy('re_vehicleid', 'desc')						
						->get();	
				$count_avg_rating =	$total_rating[0]->rating;
			 
			 $avg_rating = round(($count_avg_rating/$count_review)/2);		
				
			}
				
				/* REVIEW END */
				
				
				/* RATE LIST START */
				
				
				$rate_list = DB::table('vehicle_rate')		
							->where('rate_transid', '=' ,$trans_id)
							->where('rate_vehicleid', '=' ,$vehicle_id)
							->where('rate_status', '=' ,'1')
							->orderBy('rate_id', 'asc')
							->get();	
				$rate_array = '';	
				
				if(!empty($rate_list))
				{
					 $booking_rate_count = count($rate_list); 	
					$booking_rate_detail = '';
					foreach($rate_list as $rlist)
					{
					
						$booking_rate_detail[] = $rlist;
						
						
						
						
						
						$group_name= '';
						$group_list = '';
						
						$addons = DB::table('vehicle_addons')		
						->where('addon_transid', '=' ,$trans_id)
						->where('addon_vehicleid', '=' ,$vehicle_id)
						->where('addon_status', '=' ,'1')						
						->where('addon_rateid', '=' ,$rlist->rate_id)
						->select('*')
						->orderBy('addon_id', 'asc')						
						->groupBy('addon_groupname')
						->get();
						if(!empty($addons))
						{
								
							foreach($addons as $ad_list)
							{
								$group_name[]=$ad_list->addon_groupname;
								
								$addon_group = DB::table('vehicle_addons')		
									->where('addon_transid', '=' ,$trans_id)
									->where('addon_vehicleid', '=' ,$vehicle_id)
									->where('addon_status', '=' ,'1')	
									->where('addon_groupname', '=' ,$ad_list->addon_groupname)						
									->where('addon_rateid', '=' ,$rlist->rate_id)
									->select('*')
									->orderBy('addon_id', 'asc')	
									->get();
								$group_list[]=$addon_group;	
									
							}
							
						}
						
						
						$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);
											
						
						
							
					}
					
					
						$rate_array = array('booking_rate_count'=>$booking_rate_count,'booking_rate_detail'=>$booking_rate_detail,'item_addons'=>$item_addons);		
					
				}			
							
				/* END */
		
		  
		 
		  $service_area_list = DB::table('transport_service')		
							->where('tport_service_transid', '=' ,$trans_id)
							->where('tport_service_status', '=' ,'1')
							->orderBy('tport_service_id', 'asc')
							->get();	
				$promo_list = DB::table('transport_promo')		
							->where('tport_promo_transid', '=' ,$trans_id)
							->where('tport_promo_start', '<=' ,date('Y-m-d'))
							->where('tport_promo_end', '>=' ,date('Y-m-d'))
							->where('tport_promo_status', '=' ,'1')
							->orderBy('tport_promo_on', 'asc')
							->orderBy('tport_promo_id', 'desc')
							->get(); 	
							
		
				$vehicle_images = DB::table('vehicle_img')		
							->where('vehicle_id', '=' ,$vehicle_id)
							->get();					
			
						
				$cart = Cart::content();
				$cart_session_val = '1';
				if(count($cart)){
					foreach($cart as $item){	
							/*Cart::destroy();
							$cart_session_val = '0';*/
						
						if(isset($item->options['rest_id']))	
						{	
							Cart::destroy();
							
							$cart_session_val = '0';
						}	
						if(isset($item->options['vehicle_id']) && ($item->options['vehicle_id']!=$vehicle_id))	
						{	
							Cart::destroy();
							
							$cart_session_val = '0';
						}
							
						if(isset($item->options['vehicle_id']) && ($item->options['vehicle_id']==$vehicle_id))	
						{	
							if(($item->qty-1) != $count_no_days)	
							{	
								Cart::destroy();
								
								$cart_session_val = '0';
							}
						}	
						
					}	
				}	
				
			
			
			  if($cart_session_val){
				
				  $data_onview = array('vehicle_detail' =>$vehicle_detail,
									   'service_area'=>$service_area_list,
									   'promo_list'=>$promo_list,	
									   'rate_array'=>$rate_array,
									   'count_no_days'=>$count_no_days,
									   'cart'=>$cart,
								 	   'user_tot_order'=>$user_tot_order,
									   'vehicle_images'=>$vehicle_images,
							 	 	   'avg_rating'=>$avg_rating,
								 	   'total_review'=>$count_review
							   );
			}else
			{
				
				
				  $data_onview = array( 'vehicle_detail' =>$vehicle_detail,
									    'service_area'=>$service_area_list,
										'promo_list'=>$promo_list,	
										'rate_array'=>$rate_array,
										'count_no_days'=>$count_no_days,
										'user_tot_order'=>$user_tot_order,
										'vehicle_images'=>$vehicle_images,
							 	 	    'avg_rating'=>$avg_rating,
								 		'total_review'=>$count_review
							   );
			}
			/*echo '<pre>';
			print_r($data_onview);
			exit;*/
			 return view('vehicle_detail')->with($data_onview);;
			}
			else
			{
				return redirect()->to('/transport_listing');
			}
		}
		else
		{
			return redirect()->to('/transport_listing');
		}
	}
	
	
	function get_addons()
	{
	
		$trans_id = trim(Input::get('trans_id'));
		$vehicle_id = trim(Input::get('vehicle_id'));
		$rate_id = trim(Input::get('rate_id'));
		$menu_option =  trim(Input::get('menu_option'));;
	
	
	$menu_cat_detail = DB::table('vehicle_rate')		
							->where('rate_transid', '=' ,$trans_id)
							->where('rate_vehicleid', '=' ,$vehicle_id)
							->where('rate_id', '=' ,$rate_id)
							->where('rate_status', '=' ,'1')
							->select('*')
							->orderBy('rate_id', 'asc')
							->get();
							
			$menu_category_name	 = $menu_cat_detail[0]->rate_name;
			$menu_category_desc	 = $menu_cat_detail[0]->rate_desc;
			
			
							
							
			$group_name= '';
			$group_list = '';
			
			$addons = DB::table('vehicle_addons')		
			->where('addon_transid', '=' ,$trans_id)
			->where('addon_vehicleid', '=' ,$vehicle_id)			
			->where('addon_rateid', '=' ,$rate_id)
			->where('addon_status', '=' ,'1')			
			->select('*')
			->orderBy('addon_id', 'asc')						
			->groupBy('addon_groupname')
			->get();
			if(!empty($addons))
			{
					
				foreach($addons as $ad_list)
				{
					$group_name[]=$ad_list->addon_groupname;
					
					$addon_group = DB::table('vehicle_addons')	
						->where('addon_transid', '=' ,$trans_id)
						->where('addon_vehicleid', '=' ,$vehicle_id)			
						->where('addon_rateid', '=' ,$rate_id)
						->where('addon_status', '=' ,'1')			
						->where('addon_groupname', '=' ,$ad_list->addon_groupname)
						->select('*')
						->orderBy('addon_id', 'asc')	
						->get();
					$group_list[]=$addon_group;	
						
				}
				
			}
			
			
		//$item_addons = array('group_name'=>$group_name,'group_list'=>$group_list);
		$item_addons = array('group_name'=>$group_name,
							 'group_list'=>$group_list,
							 'trans_id'=>$trans_id,
							 'vehicle_id'=>$vehicle_id,
							 'rate_id'=>$rate_id,
							 'menu_option'=>$menu_option,
							 'menu_category_desc'=>$menu_category_desc,
							 'menu_category_name'=>$menu_category_name
							 
							 );
		 $addon_view =  view('ajax.transport_addon', $item_addons)->render();
	 
         return response()->json( array('addon_view'=>$addon_view) );
	 
			
	}
	
	
	
	
	function order_submit()
	{
		//echo '<pre>';
		//print_r($_POST);
		
		$cart = Cart::content();
		$detail_from_location = Session::get('cart_bookinginfo')['vehicle_from'];
		$detail_to_location = Session::get('cart_bookinginfo')['vehicle_to'];
		
		
		
		//print_r($cart);
		
		
		
		/*  UPDATE tO LOCATION DATA  */
		if(trim(Input::get('to_suburb'))!=trim(Session::get('cart_bookinginfo')['vehicle_to']))	
		{
		
		
			$detail_to_location = trim(Input::get('to_suburb'));
			
			$to_location = array('to_surbur_lable'=>trim(Input::get('to_lable')),
		  						 'to_surbur_name'=>trim(Input::get('to_suburb')),
		  						 'to_post_code'=>trim(Input::get('to_pcode')),
		  						 'to_loc_id'=>trim(Input::get('to_loc_id')),
		  						 'to_search_type'=>trim(Input::get('to_search_type'))
		  						);
		 	 Session::set('to_location', $to_location);
			
		}
		
		
		/*      END                  */
		
		
		
		$pickup_area =  trim(Input::get('pickup_area'));
		
		
		/* UPDATE FROMLOCATION IF WE CHANGE BY SERIVE AREA SECTION  */
		
		
		$transport_Suburb  = DB::table('transport_service')
								->where('tport_service_id', '=' ,$pickup_area)
								->get();	
		$keyword = 	trim($transport_Suburb[0]->tport_service_suburb);	
		
		
		
					
		if($keyword!=trim(Session::get('cart_bookinginfo')['vehicle_from']))	
		{
			
			$Suburb_detail  = DB::table('location')
								->where('Suburb_name', 'like' , $keyword.'%')
								->get();
								
			$surbur_lable =	$Suburb_detail[0]->Suburb_name.' - '.$Suburb_detail[0]->pincode.', '.$Suburb_detail[0]->City_name.', '.$Suburb_detail[0]->state;
			
			$detail_from_location = $Suburb_detail[0]->Suburb_name;
				
			$from_location = array('surbur_lable'=>$surbur_lable,
		  						 'surbur_name'=>$Suburb_detail[0]->Suburb_name,
		  						 'post_code'=>$Suburb_detail[0]->pincode,
		  						 'loc_id'=>$Suburb_detail[0]->location_id,
		  						 'search_type'=>'surbur'
		  						);
		  Session::set('from_location', $from_location);
			//print_r($Suburb_detail)	;				
		}					
		
		/* END   */
		
		
		
		
		
		
		  
		  
		  $cart_bookinginfo = array(
									'vehicle_id' =>Session::get('cart_bookinginfo')['vehicle_id'],
									'vehicle_from' =>$detail_from_location,
									'vehicle_to' =>$detail_to_location,
									'vehicle_ontime' =>Session::get('cart_bookinginfo')['vehicle_ontime'],
									'vehicle_ondate' =>Session::get('cart_bookinginfo')['vehicle_ondate'],
									'vehicle_returntime' =>Session::get('cart_bookinginfo')['vehicle_returntime'],
									'vehicle_returndate' =>Session::get('cart_bookinginfo')['vehicle_returndate']
		
									 );
		
			Session::set('cart_bookinginfo', $cart_bookinginfo);
						
		
		
		
		
		
		
		$rate_id = '';
		$total_days = '';
		if(count($cart)){
			foreach($cart as $item){	
					/*Cart::destroy();
					$cart_session_val = '0';*/
					
					$rate_id = $item->id;
					
					$total_days = $item->qty;
			}	
		}	
		
		
		
		$trans_id = trim(Input::get('trans_id'));
		$vehicle_id = trim(Input::get('vehicle_id'));		
		$on_date = trim(Session::get('cart_bookinginfo')['vehicle_ondate']);
		$on_time = trim(Session::get('cart_bookinginfo')['vehicle_ontime']);
		$return_date = trim(Session::get('cart_bookinginfo')['vehicle_returndate']);
		$return_time = trim(Session::get('cart_bookinginfo')['vehicle_returntime']);
		
		
		$from_location = trim($detail_from_location);//trim(Session::get('cart_bookinginfo')['vehicle_from']);  
		$to_location = trim($detail_to_location);;//trim(Session::get('cart_bookinginfo')['vehicle_to']);
		
		$total_days = $total_days;
		$total_nignt = trim(Input::get('total_night'));
		$subtotal = trim(Input::get('sub_total'));
		$delivery_fee = trim(Input::get('delivery_fee'));
		$each_day_allownce = trim(Input::get('each_day_allownce'));
		$driver_allownce = trim(Input::get('driver_allownce'));
		$total_charge = trim(Input::get('total_charge'));
		$promo_mode = '';
		$promo_value = '';
		$promo_amt_cal='';
		
		if(Input::get('promo_mode'))
		{
			$promo_mode = trim(Input::get('promo_mode'));
		}
		if(Input::get('promo_value'))
		{
			$promo_value =  trim(Input::get('promo_value'));
		}
		if(Input::get('promo_amt_cal'))
		{
			
			$promo_amt_cal= trim(Input::get('promo_amt_cal'));
		}
		
		$back_url =  trim(Input::get('rest_metatag'));
		
		
		
		
		
		
		
		
		$cart_order_detail=array(
							'back_url'=>$back_url,
							'vehicle_id'=>$vehicle_id,
							'trans_id'=>$trans_id,
							'rate_id'=>$rate_id,
							'on_date'=>$on_date,
							'on_time'=>$on_time,
							'return_date'=>$return_date,
							'return_time'=>$return_time,
							'from_location'=>$from_location,
							'to_location'=>$to_location,
							'total_days'=>$total_days,
							'total_nignt'=>$total_nignt,
							'subtotal'=>$subtotal,
							'delivery_fee'=>$delivery_fee,
							'each_day_allownce'=>$each_day_allownce,
							'driver_allownce'=>$driver_allownce,
							'total_charge'=>$total_charge,
							'promo_mode'=>$promo_mode,
							'promo_value'=>$promo_value,
							'promo_amt_cal'=>$promo_amt_cal,
							'pickup_area'=>$pickup_area 
		
						);
		Session::set('cart_order_detail',$cart_order_detail);				
	/*	print_r($cart_order_detail)		;						
		print_r(Session::get('from_location'))		;	*/	
		
		//exit;
		return redirect()->to('/transport_checkout');
		
	}
	
	/* CHECKOUT PAGE SHOW DATA */
	public function show_checkoutpage()
	{
	
		$cart = Cart::content();
		if(count($cart)){
		
		
		
		if((Session::has('from_location')) && (Session::has('to_location')) )
		{
			
			$picup_info = '';
			
			$from_data = DB::table('location')		
						->where('location_id', '=' ,Session::get('from_location')['loc_id'])
						->select('*')
						->orderBy('location_id', 'asc')						
						->groupBy('location_id')
						->get();
			
			$to_data = DB::table('location')		
						->where('location_id', '=' ,Session::get('to_location')['to_loc_id'])
						->select('*')
						->orderBy('location_id', 'asc')						
						->groupBy('location_id')
						->get();
			
		$pick_contact_name ='';
		$pick_contact_no ='';
		$pick_address1='';
		$pick_address2 ='';
		$pick_bookingtype ='Return_Trip';
		if(Session::has('picup_info'))
		{
			$pick_contact_name =Session::get('picup_info')['pick_contact_name'];
			$pick_contact_no =Session::get('picup_info')['pick_contact_no'];
			$pick_address1=Session::get('picup_info')['pick_address1'];
			$pick_address2 =Session::get('picup_info')['pick_address2'];
			$pick_bookingtype = Session::get('picup_info')['pick_bookingtype'];
		}
			
			$picup_info =  array(
								  'pick_contact_name' =>$pick_contact_name,
								  'pick_contact_no' =>$pick_contact_no,
								  'pick_address1'=>$pick_address1,
								  'pick_address2' =>$pick_address2,
								  'pick_suburb'=>$from_data[0]->Suburb_name,
								  'pick_pcode'=>$from_data[0]->pincode,
								  'pick_district'=>$from_data[0]->District_name,
								  'pick_state'=>$from_data[0]->state,
								  'pick_bookingtype'=>$pick_bookingtype,
								  
								  
								  'pick_to_suburb'=>$to_data[0]->Suburb_name,
								  'pick_to_pcode'=>$to_data[0]->pincode,
								  'pick_to_district'=>$to_data[0]->District_name,
								  'pick_to_state'=>$to_data[0]->state								  
								  
								 );
			Session::set('picup_info',$picup_info);										 
			
		}
	
		
		/*  USER DETAIL  SHOW  START*/
			$user_detail =  array(
								  'firstname_order' =>'',
								  'lastname_order' =>'',
								  'tel_order'=>'',
								  'email_order' =>'',
								  'address_order'=>'',
								  'city_order'=>'',
								  'pcode_order'=>''
								 );
					
			if((Session::has('cart_userinfo')) && (!empty(Session::get('cart_userinfo')['firstname_order'])))
			{
			$user_detail = array(
								  'firstname_order' =>Session::get('cart_userinfo')['firstname_order'],
								  'lastname_order' =>Session::get('cart_userinfo')['lastname_order'],
								  'tel_order'=>Session::get('cart_userinfo')['tel_order'],
								  'email_order' =>Session::get('cart_userinfo')['email_order'],
								  'address_order'=>Session::get('cart_userinfo')['address_order'],
								  'city_order'=>Session::get('cart_userinfo')['city_order'],
								  'pcode_order'=>Session::get('cart_userinfo')['pcode_order']
								 );
			}			
			elseif(!Auth::guest()) 
			{
				$user_id = Auth::user()->id;
				$user_info = DB::table('users')->select('*')->where('id', '=' ,$user_id)->get();	
			
					$user_detail =  array(
										  'firstname_order' =>$user_info[0]->name,
										  'lastname_order' =>$user_info[0]->lname,
										  'tel_order'=>$user_info[0]->user_mob,
										  'email_order' =>$user_info[0]->email,
										  'address_order'=>$user_info[0]->user_address,
										  'city_order'=>$user_info[0]->user_city,
										  'pcode_order'=>$user_info[0]->user_zipcode
										 );
			}
			/*    END   */
		
			
		$data_onview = array( 'cart' =>$cart,
							  'user_detail'=>$user_detail
							 );
							 
			return View('transport_checkout')->with($data_onview);
		}
		else
		{
			return redirect()->to('/transport_listing');
		}
		
	}
	
	
	/* UPDATE USER DETAIL FROM CHECKOUT PAHE */
	public function userdetail_submit()
	{
		//echo '<pre>';
		//print_r($_POST);
		
		
		$picup_info =  array(
								  'pick_contact_name' => trim(Input::get('pick_contact_name')),
								  'pick_contact_no' => trim(Input::get('pick_contact_no')),
								  'pick_address1'=> trim(Input::get('pick_address1')),
								  'pick_address2' => trim(Input::get('pick_address2')),
								  'pick_suburb'=> trim(Input::get('pick_suburb')),
								  'pick_pcode'=> trim(Input::get('pick_pcode')),
								  'pick_district'=> trim(Input::get('pick_district')),
								  'pick_state'=> trim(Input::get('pick_state')),
								  'pick_bookingtype'=> trim(Input::get('pick_bookingtype')),
								  
								  
								  'pick_to_suburb'=> trim(Input::get('pick_to_suburb')),
								  'pick_to_pcode'=> trim(Input::get('pick_to_pcode')),
								  'pick_to_district'=> trim(Input::get('pick_to_district')),
								  'pick_to_state'=> trim(Input::get('pick_to_state'))								  
								  
								 );
			Session::set('picup_info',$picup_info);		
			
			
		
		$user_type = 0;
		if(Input::get('user_id')>0)
		{
			$user_type = 1;
		}
		
		//$cart_user['order_instruction'] = trim(Input::get('order_instruction'));
		
		
		
		$cart_user['firstname_order'] = trim(Input::get('firstname_order'));
		$cart_user['lastname_order'] = trim(Input::get('lastname_order'));
		$cart_user['tel_order'] = trim(Input::get('tel_order'));
		$cart_user['email_order'] = trim(Input::get('email_order'));
		$cart_user['address_order'] = trim(Input::get('address_order'));
		$cart_user['city_order'] = trim(Input::get('city_order'));
		$cart_user['pcode_order'] = trim(Input::get('pcode_order'));
		$cart_user['user_type'] = trim($user_type);
		$cart_user['user_id'] = trim(Input::get('user_id'));
		
		
			
	//	$cart_user['cart_data'] = $cart;
		//$cart_user['service_area'] = trim(Input::get('service_area'));
		//$cart_user['service_type'] = trim(Input::get('service_type'));
		
		
		/*echo '<pre>';
		print_r($cart_user);
		print_r(Session::get('cart_dinein'));
		exit;*/
		
		
		
		Session::set('cart_userinfo', $cart_user);
		
		//echo '1';
		return redirect()->to('/transport_payment');
		
	}
	
	public function show_paymentpage()
	{
		$cart = Cart::content();
			
		$data_onview = array( 'cart' =>$cart );
		
		return View('transport_payment')->with($data_onview);
	}
	
	
	
	/* ORDER CANCLE AND SUBMIT ON PAYPAL WEBSITE */
	
	function show_order_cancel()
	{
	//	echo '<pre>';
	//	print_r($_REQUEST);
		 $cart =  Cart::content();		 
	
		
		$orderdata = new Transport_order;
		
			
$orderdata->user_type 		   = Session::get('cart_userinfo')['user_type'];		
$orderdata->user_id 		   = Session::get('cart_userinfo')['user_id'];		
$orderdata->vehicle_id 		   = Session::get('cart_order_detail')['vehicle_id'];	
$orderdata->trans_id 		   = Session::get('cart_order_detail')['trans_id'];			
$orderdata->order_fname	       = Session::get('cart_userinfo')['firstname_order'];		
$orderdata->order_lname		   = Session::get('cart_userinfo')['lastname_order'];		
$orderdata->order_email		   = Session::get('cart_userinfo')['email_order'];		
$orderdata->order_tel		   = Session::get('cart_userinfo')['tel_order'];		
$orderdata->order_address	   = Session::get('cart_userinfo')['address_order'];		
$orderdata->order_city	       = Session::get('cart_userinfo')['city_order'];		
$orderdata->order_pcode		   = Session::get('cart_userinfo')['pcode_order'];	

$orderdata->order_from_location	 =  Session::get('cart_order_detail')['from_location'];			
$orderdata->order_to_location	 =  Session::get('cart_order_detail')['to_location'];			
$orderdata->order_ondate = 	 Session::get('cart_order_detail')['on_date'];		
$orderdata->order_ontime = 	 Session::get('cart_order_detail')['on_time'];		
$orderdata->order_returndate = 	 Session::get('cart_order_detail')['return_date'];		
$orderdata->order_returntime	 = 	 Session::get('cart_order_detail')['return_time'];
		
$orderdata->total_days	 = 	 Session::get('cart_order_detail')['total_days'];		
$orderdata->total_night	 = 	 Session::get('cart_order_detail')['total_nignt'];		
$orderdata->each_day_allownce = 	 Session::get('cart_order_detail')['each_day_allownce'];		
$orderdata->total_allownce	 =  Session::get('cart_order_detail')['driver_allownce'];	
	
$orderdata->order_pickarea	 = 	 Session::get('cart_order_detail')['pickup_area'];		
$orderdata->order_instruction = 	'';		
$orderdata->order_cancel_reason	 = 	 '';		
$orderdata->order_create	 = 	date('Y-m-d')	;	
$orderdata->order_status	 = 	'8';		
$orderdata->order_deliveryfee	 = 	 Session::get('cart_order_detail')['delivery_fee'];		
$orderdata->order_deliveryadd1 = 	'';		
$orderdata->order_deliveryadd2	 = 	'';		
$orderdata->order_deliverysurbur = 	'';		
$orderdata->order_deliverypcode	 = 	'';		
$orderdata->order_deliverynumber = 	'';		
$orderdata->order_min_delivery = 	'';		
$orderdata->order_remaning_delivery	 = 	'';		
$orderdata->order_subtotal	 = 	 Session::get('cart_order_detail')['subtotal'];		
$orderdata->order_total_amt = 	 Session::get('cart_order_detail')['total_charge'];		
$orderdata->order_carditem = 	  $cart;		
$orderdata->order_promo_mode	 = 	 Session::get('cart_order_detail')['promo_mode'];		
$orderdata->order_promo_val = 	 Session::get('cart_order_detail')['promo_value'];		
$orderdata->order_promo_cal = 	 Session::get('cart_order_detail')['promo_amt_cal'];



	/* PICup info*/
$orderdata->pick_contact_name = 	 Session::get('picup_info')['pick_contact_name'];	
$orderdata->pick_contact_no = 	    Session::get('picup_info')['pick_contact_no'];	
$orderdata->pick_address1 = 	  Session::get('picup_info')['pick_address1'];	
$orderdata->pick_address2 = 	 Session::get('picup_info')['pick_address2'];	
$orderdata->pick_suburb = 	 Session::get('picup_info')['pick_suburb'];	
$orderdata->pick_pcode = 	 Session::get('picup_info')['pick_pcode'];	
$orderdata->pick_district = 	 Session::get('picup_info')['pick_district'];	
$orderdata->pick_state = 	 Session::get('picup_info')['pick_state'];	
$orderdata->pick_to_suburb = 	 Session::get('picup_info')['pick_to_suburb'];	
$orderdata->pick_to_pcode = 	 Session::get('picup_info')['pick_to_pcode'];	
$orderdata->pick_to_district = 	 Session::get('picup_info')['pick_to_district'];	
$orderdata->pick_to_state = 	 Session::get('picup_info')['pick_to_state'];	 	
$orderdata->pick_bookingtype = 	 Session::get('picup_info')['pick_bookingtype'];	 






		//print_r($orderdata);

$orderdata->save();
$order_id = $orderdata->order_id;	
	
		/*  INSERT IN HISTORY TABLE */
$history_orderdata = new Transport_order_history;
$history_orderdata->order_id 		   = $order_id;	
$history_orderdata->user_type 		   = Session::get('cart_userinfo')['user_type'];		
$history_orderdata->user_id 		   = Session::get('cart_userinfo')['user_id'];		
$history_orderdata->trans_id 		   = Session::get('cart_order_detail')['trans_id'];		
$history_orderdata->vehicle_id 		   = Session::get('cart_order_detail')['vehicle_id'];			
$history_orderdata->order_fname	       = Session::get('cart_userinfo')['firstname_order'];		
$history_orderdata->order_lname		   = Session::get('cart_userinfo')['lastname_order'];		
$history_orderdata->order_email		   = Session::get('cart_userinfo')['email_order'];		
$history_orderdata->order_tel		   = Session::get('cart_userinfo')['tel_order'];		
$history_orderdata->order_address	   = Session::get('cart_userinfo')['address_order'];		
$history_orderdata->order_city	       = Session::get('cart_userinfo')['city_order'];		
$history_orderdata->order_pcode		   = Session::get('cart_userinfo')['pcode_order'];	

$history_orderdata->order_from_location	 =  Session::get('cart_order_detail')['from_location'];			
$history_orderdata->order_to_location	 =  Session::get('cart_order_detail')['to_location'];			
$history_orderdata->order_ondate = 	 Session::get('cart_order_detail')['on_date'];		
$history_orderdata->order_ontime = 	 Session::get('cart_order_detail')['on_time'];		
$history_orderdata->order_returndate = 	 Session::get('cart_order_detail')['return_date'];		
$history_orderdata->order_returntime	 = 	 Session::get('cart_order_detail')['return_time'];
		
$history_orderdata->total_days	 = 	 Session::get('cart_order_detail')['total_days'];		
$history_orderdata->total_night	 = 	 Session::get('cart_order_detail')['total_nignt'];		
$history_orderdata->each_day_allownce = 	 Session::get('cart_order_detail')['each_day_allownce'];		
$history_orderdata->total_allownce	 =  Session::get('cart_order_detail')['driver_allownce'];	
	
$history_orderdata->order_pickarea	 = 	 '';		
$history_orderdata->order_instruction = 	'';		
$history_orderdata->order_cancel_reason	 = 	 '';		
$history_orderdata->order_create	 = 	date('Y-m-d')	;	
$history_orderdata->order_status	 = 	'8';		
$history_orderdata->order_deliveryfee	 = 	 Session::get('cart_order_detail')['delivery_fee'];		
$history_orderdata->order_deliveryadd1 = 	'';		
$history_orderdata->order_deliveryadd2	 = 	'';		
$history_orderdata->order_deliverysurbur = 	'';		
$history_orderdata->order_deliverypcode	 = 	'';		
$history_orderdata->order_deliverynumber = 	'';		
$history_orderdata->order_min_delivery = 	'';		
$history_orderdata->order_remaning_delivery	 = 	'';		
$history_orderdata->order_subtotal	 = 	 Session::get('cart_order_detail')['subtotal'];		
$history_orderdata->order_total_amt = 	 Session::get('cart_order_detail')['total_charge'];		
$history_orderdata->order_carditem = 	  $cart;		
$history_orderdata->order_promo_mode	 = 	 Session::get('cart_order_detail')['promo_mode'];		
$history_orderdata->order_promo_val = 	 Session::get('cart_order_detail')['promo_value'];		
$history_orderdata->order_promo_cal = 	 Session::get('cart_order_detail')['promo_amt_cal'];

$history_orderdata->save();
$order_history_id = $history_orderdata->order_history_id;	
			/*  END */


/*  DISTORY SESSION VALUES */
 	Cart::destroy();
	Session::forget('on_date');
	Session::forget('on_time');
	Session::forget('return_date');
	Session::forget('return_time');
	Session::forget('from_location');
	Session::forget('to_location');
	Session::forget('cart_bookinginfo');
	Session::forget('cart_order_detail');	
	Session::forget('cart_userinfo');
	Session::forget('picup_info');		

/*              END           */



	$order_detail  = DB::table('transport_order')->where('order_id', '=' ,$order_id)->get();	
	
		$data_onview = array( 'cart' =>$cart,'order_detail'=>$order_detail);
		return View('transport_order_cancel')->with($data_onview);


	}
	
	
	function show_order_success()
	{
	
	
		 $cart =  Cart::content();		 
	
		
		$orderdata = new Transport_order;
		
			
$orderdata->user_type 		   = Session::get('cart_userinfo')['user_type'];		
$orderdata->user_id 		   = Session::get('cart_userinfo')['user_id'];		
$orderdata->vehicle_id 		   = Session::get('cart_order_detail')['vehicle_id'];	
$orderdata->trans_id 		   = Session::get('cart_order_detail')['trans_id'];			
$orderdata->order_fname	       = Session::get('cart_userinfo')['firstname_order'];		
$orderdata->order_lname		   = Session::get('cart_userinfo')['lastname_order'];		
$orderdata->order_email		   = Session::get('cart_userinfo')['email_order'];		
$orderdata->order_tel		   = Session::get('cart_userinfo')['tel_order'];		
$orderdata->order_address	   = Session::get('cart_userinfo')['address_order'];		
$orderdata->order_city	       = Session::get('cart_userinfo')['city_order'];		
$orderdata->order_pcode		   = Session::get('cart_userinfo')['pcode_order'];	

$orderdata->order_from_location	 =  Session::get('cart_order_detail')['from_location'];			
$orderdata->order_to_location	 =  Session::get('cart_order_detail')['to_location'];			
$orderdata->order_ondate = 	 Session::get('cart_order_detail')['on_date'];		
$orderdata->order_ontime = 	 Session::get('cart_order_detail')['on_time'];		
$orderdata->order_returndate = 	 Session::get('cart_order_detail')['return_date'];		
$orderdata->order_returntime	 = 	 Session::get('cart_order_detail')['return_time'];
		
$orderdata->total_days	 = 	 Session::get('cart_order_detail')['total_days'];		
$orderdata->total_night	 = 	 Session::get('cart_order_detail')['total_nignt'];		
$orderdata->each_day_allownce = 	 Session::get('cart_order_detail')['each_day_allownce'];		
$orderdata->total_allownce	 =  Session::get('cart_order_detail')['driver_allownce'];	
	
$orderdata->order_pickarea	 = 	 '';//Session::get('cart_order_detail')['pickup_area'];		
$orderdata->order_instruction = 	'';		
$orderdata->order_cancel_reason	 = 	 '';		
$orderdata->order_create	 = 	date('Y-m-d')	;	
$orderdata->order_status	 = 	'1';		
$orderdata->order_deliveryfee	 = 	 Session::get('cart_order_detail')['delivery_fee'];		
$orderdata->order_deliveryadd1 = 	'';		
$orderdata->order_deliveryadd2	 = 	'';		
$orderdata->order_deliverysurbur = 	'';		
$orderdata->order_deliverypcode	 = 	'';		
$orderdata->order_deliverynumber = 	'';		
$orderdata->order_min_delivery = 	'';		
$orderdata->order_remaning_delivery	 = 	'';		
$orderdata->order_subtotal	 = 	 Session::get('cart_order_detail')['subtotal'];		
$orderdata->order_total_amt = 	 Session::get('cart_order_detail')['total_charge'];		
$orderdata->order_carditem = 	  $cart;		
$orderdata->order_promo_mode	 = 	 Session::get('cart_order_detail')['promo_mode'];		
$orderdata->order_promo_val = 	 Session::get('cart_order_detail')['promo_value'];		
$orderdata->order_promo_cal = 	 Session::get('cart_order_detail')['promo_amt_cal'];
	/* PICup info*/
$orderdata->pick_contact_name = 	 Session::get('picup_info')['pick_contact_name'];	
$orderdata->pick_contact_no = 	 Session::get('picup_info')['pick_contact_no'];	
$orderdata->pick_address1 = 	 Session::get('picup_info')['pick_address1'];	
$orderdata->pick_address2 = 	 Session::get('picup_info')['pick_address2'];	
$orderdata->pick_suburb = 	 Session::get('picup_info')['pick_suburb'];	
$orderdata->pick_pcode = 	 Session::get('picup_info')['pick_pcode'];	
$orderdata->pick_district = 	 Session::get('picup_info')['pick_district'];	
$orderdata->pick_state = 	 Session::get('picup_info')['pick_state'];	
$orderdata->pick_to_suburb = 	 Session::get('picup_info')['pick_to_suburb'];	
$orderdata->pick_to_pcode = 	 Session::get('picup_info')['pick_to_pcode'];	
$orderdata->pick_to_district = 	 Session::get('picup_info')['pick_to_district'];	
$orderdata->pick_to_state = 	 Session::get('picup_info')['pick_to_state'];	 
$orderdata->pick_bookingtype = 	 Session::get('picup_info')['pick_bookingtype'];	




		//print_r($orderdata);

$orderdata->save();
$order_id = $orderdata->order_id;	
	
		/*  INSERT IN HISTORY TABLE */
$history_orderdata = new Transport_order_history;
$history_orderdata->order_id 		   = $order_id;	
$history_orderdata->user_type 		   = Session::get('cart_userinfo')['user_type'];		
$history_orderdata->user_id 		   = Session::get('cart_userinfo')['user_id'];		
$history_orderdata->trans_id 		   = Session::get('cart_order_detail')['trans_id'];		
$history_orderdata->vehicle_id 		   = Session::get('cart_order_detail')['vehicle_id'];			
$history_orderdata->order_fname	       = Session::get('cart_userinfo')['firstname_order'];		
$history_orderdata->order_lname		   = Session::get('cart_userinfo')['lastname_order'];		
$history_orderdata->order_email		   = Session::get('cart_userinfo')['email_order'];		
$history_orderdata->order_tel		   = Session::get('cart_userinfo')['tel_order'];		
$history_orderdata->order_address	   = Session::get('cart_userinfo')['address_order'];		
$history_orderdata->order_city	       = Session::get('cart_userinfo')['city_order'];		
$history_orderdata->order_pcode		   = Session::get('cart_userinfo')['pcode_order'];	

$history_orderdata->order_from_location	 =  Session::get('cart_order_detail')['from_location'];			
$history_orderdata->order_to_location	 =  Session::get('cart_order_detail')['to_location'];			
$history_orderdata->order_ondate = 	 Session::get('cart_order_detail')['on_date'];		
$history_orderdata->order_ontime = 	 Session::get('cart_order_detail')['on_time'];		
$history_orderdata->order_returndate = 	 Session::get('cart_order_detail')['return_date'];		
$history_orderdata->order_returntime	 = 	 Session::get('cart_order_detail')['return_time'];
		
$history_orderdata->total_days	 = 	 Session::get('cart_order_detail')['total_days'];		
$history_orderdata->total_night	 = 	 Session::get('cart_order_detail')['total_nignt'];		
$history_orderdata->each_day_allownce = 	 Session::get('cart_order_detail')['each_day_allownce'];		
$history_orderdata->total_allownce	 =  Session::get('cart_order_detail')['driver_allownce'];	
	
$history_orderdata->order_pickarea	 = 	 '';		
$history_orderdata->order_instruction = 	'';		
$history_orderdata->order_cancel_reason	 = 	 '';		
$history_orderdata->order_create	 = 	date('Y-m-d')	;	
$history_orderdata->order_status	 = 	'1';		
$history_orderdata->order_deliveryfee	 = 	 Session::get('cart_order_detail')['delivery_fee'];		
$history_orderdata->order_deliveryadd1 = 	'';		
$history_orderdata->order_deliveryadd2	 = 	'';		
$history_orderdata->order_deliverysurbur = 	'';		
$history_orderdata->order_deliverypcode	 = 	'';		
$history_orderdata->order_deliverynumber = 	'';		
$history_orderdata->order_min_delivery = 	'';		
$history_orderdata->order_remaning_delivery	 = 	'';		
$history_orderdata->order_subtotal	 = 	 Session::get('cart_order_detail')['subtotal'];		
$history_orderdata->order_total_amt = 	 Session::get('cart_order_detail')['total_charge'];		
$history_orderdata->order_carditem = 	  $cart;		
$history_orderdata->order_promo_mode	 = 	 Session::get('cart_order_detail')['promo_mode'];		
$history_orderdata->order_promo_val = 	 Session::get('cart_order_detail')['promo_value'];		
$history_orderdata->order_promo_cal = 	 Session::get('cart_order_detail')['promo_amt_cal'];

$history_orderdata->save();
$order_history_id = $history_orderdata->order_history_id;	
			/*  END */
			
			
			

/*  UPDATE USER INFROMATION IF HE CHANGE ANY START  */
		
	if(!Auth::guest()) 
	{
		$user_id = Auth::user()->id;	
								 
		DB::table('users')
		->where('id', '=' ,$user_id)
		->update(['user_address' => trim(Session::get('cart_userinfo')['address_order']),
				  'user_city'=> trim(Session::get('cart_userinfo')['city_order']),
				  'user_zipcode'=> trim(Session::get('cart_userinfo')['pcode_order']),
				  'user_mob'=>  trim(Session::get('cart_userinfo')['tel_order'])
				 ]);
	}
	
/*  UPDATE USER INFROMATION IF HE CHANGE ANY END  */


/*   PAYMENT TABLE INSERT DATA  */



$paydata = new Transport_payment;
$paydata->pay_orderid = $order_id;	
$paydata->pay_userid = Session::get('cart_userinfo')['user_id'];	
$paydata->pay_tx = Input::get('tx');	
$paydata->pay_st = Input::get('st');	
$paydata->pay_amt = Input::get('amt');	
$paydata->pay_cc = Input::get('cc');
$paydata->pay_status=1;
$paydata->save();
$pay_id = $paydata->pay_id;







$paydata_history = new Transport_payment_history;
$paydata_history->pay_orderid = $order_id;	
$paydata_history->pay_id= $pay_id;	
$paydata_history->pay_userid = Session::get('cart_userinfo')['user_id'];	
$paydata_history->pay_tx = Input::get('tx');	
$paydata_history->pay_st = Input::get('st');	
$paydata_history->pay_amt = Input::get('amt');	
$paydata_history->pay_cc = Input::get('cc');
$paydata_history->pay_status=1;
$paydata_history->save();
$history_pay_idPrimary = $paydata->history_pay_idPrimary;




/*       END                   */


/*  DISTORY SESSION VALUES */
 	Cart::destroy();
	Session::forget('on_date');
	Session::forget('on_time');
	Session::forget('return_date');
	Session::forget('return_time');
	Session::forget('from_location');
	Session::forget('to_location');
	Session::forget('cart_bookinginfo');
	Session::forget('cart_order_detail');	
	Session::forget('cart_userinfo');
	Session::forget('picup_info');		

/*              END           */



	$order_detail  = DB::table('transport_order')->where('order_id', '=' ,$order_id)->get();	
	
		$data_onview = array( 'cart' =>$cart,'order_detail'=>$order_detail);
		return View('transport_order_success')->with($data_onview);
		
	}
	
	
	
	
	
	public function transport_search_reset()
	{
		
		//print_r($_REQUEST);
		
		Cart::destroy();
		Session::forget('on_date');
		Session::forget('on_time');
		Session::forget('return_date');
		Session::forget('return_time');
		
		Session::forget('cart_bookinginfo');
		Session::forget('cart_order_detail');	
		
		echo '1';
		//Session::forget('cart_userinfo');
		
	}
	
	
	
	public function vehicle_review()
	{
			
		  $tran_subrub =  Route::current()->getParameter('subrub');
		
		  $vehicle_reho =  Route::current()->getParameter('vehicle');
		
		  $vehicle_class =  Route::current()->getParameter('class');
		
		  $vehicle_make =  Route::current()->getParameter('make');
		
		  $vehicle_id =  Route::current()->getParameter('vehicle_id');
		  
		  
		$tt =  Route::current()->getParameter('review');
		
		 $vehicle_detail = DB::table('vehicle');
		  $vehicle_detail = $vehicle_detail->leftJoin('transport', 'vehicle.vehicle_transid', '=', 'transport.transport_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id');
		  $vehicle_detail = $vehicle_detail->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id');							
		  $vehicle_detail = $vehicle_detail->select('transport.*','vehicle.*','vehicle_cate.vcate_name','vehicle_class.vclass_name','vehicle_make.make_name','vehicle_model.model_name');	
		  $vehicle_detail = $vehicle_detail->where('transport.transport_status', '!=' , 'INACTIVE');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_status', '=' , '1');	
		  $vehicle_detail = $vehicle_detail->where('vehicle.vehicle_id', '=' , $vehicle_id);
		 $vehicle_detail = $vehicle_detail->groupBy('vehicle.vehicle_id');
		 $vehicle_detail = $vehicle_detail->get();
		 
		if((!empty($vehicle_detail)) && ( $tt=='review'))
		{			
			$transid = $vehicle_detail[0]->vehicle_transid; 
			$vehicle_id = $vehicle_detail[0]->vehicle_id;  
			
			
			$avg_rating	= 0;
			
			$count_review = DB::table('transport_review')		
						->where('re_transid', '=' ,$transid) 
						->where('re_vehicleid', '=' ,$vehicle_id) 
						->where('re_status', '=' ,'PUBLISHED')
						->orderBy('re_id', 'desc')						
						->count();	
						
								
			$count_avg_rating =0;
			$total_rating = DB::table('transport_review')	
						->select( DB::raw('SUM(re_rating) as rating'))	
						->where('re_transid', '=' ,$transid) 
						->where('re_vehicleid', '=' ,$vehicle_id) 
						->where('re_status', '=' ,'PUBLISHED')
						->orderBy('re_id', 'desc')						
						->get();
		   $count_avg_rating =	$total_rating[0]->rating;
				
			 //$avg_rating = (($count_avg_rating)/5);
			 if($count_review>0)			
			 {		 	
				 $avg_rating = round(($count_avg_rating/$count_review)/2);
			 }
							
			$refno = 0;
			$user_id = 0;
			$order_detail='';
			$review_list='';
			if(!Auth::guest())
			{
				$user_id = Auth::user()->id ;
			}
			
			if(isset($_GET['refno']) && (!empty($_GET['refno'])))		
			{
				$refno = $_GET['refno'];
				
				$order_detail  =  DB::table('transport_order')		
						->where('trans_id', '=', $transid)	
						->where('vehicle_id', '=', $vehicle_id)
						->where('user_id', '=', $user_id)
						->where('order_id', '=', $refno)
						->select('*')
						->get();	
						
						
			}
			
			
			$review_list = DB::table('transport_review')			 
						->leftJoin('users', 'transport_review.re_userid', '=', 'users.id')
						->select('transport_review.*','users.name','users.lname')	
						->where('transport_review.re_transid', '=' ,$transid) 
						->where('transport_review.re_vehicleid', '=' ,$vehicle_id) 
						->where('transport_review.re_status', '=' , 'PUBLISHED' )
						->orderBy('re_id', 'desc')				
						->get();
				
						
			
			
			$data_onview = array('vehicle_detail' =>$vehicle_detail,	
								  'trans_id' =>$transid,	
								  'vehicle_id' =>$vehicle_id,	
								  'order_detail' =>$order_detail,	
								  'refno' =>$refno,		
								  'user_id' =>$user_id,	
								  'review_list' =>$review_list,
								  'total_review'=>$count_review	,
								  'avg_rating'=>$avg_rating			
								 );
								 
					 
			return view('transport_review')->with($data_onview);
			/* return view('restaurant_review');*/
			
			
		}
		else
		{
			return redirect()->to('/transport_listing');
		}	
		  
	}
	
}
