<?php

namespace App\Http\Controllers;

Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use App\Vendor;

use App\Vendorcate;

use App\Proservicescate;

use App\Proservices_subcate;

use Route;

use Illuminate\Http\Request;

use Mail;

use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Support\Str;

use App\Productservice;

use App\Video;
use File;

use Storage;



class AdminVendorController extends Controller
{

    public function __construct(){

    	$this->middleware('admin');

    }

    public function category_list(Request $request)

	{

		$psid = $request->psid;

	    $cate = DB::table('product_service_category')

					->where('type', '=' ,$psid)

					->orderBy('id', 'asc')

				    ->get();

		$data_onview = array('cate'=>$cate,'psid'=>$psid);

		return View('admin.ajax.category')->with($data_onview);

	}


	public function subcategory_list(Request $request)
	{


		$catid = $request->catid;

		$sub_category_id = $request->sub_category_id;

	    $subcate = DB::table('product_service_sub_category')

					->where('category_id', '=' ,$catid)

					->orderBy('id', 'asc')

				    ->get();

		$data_onview = array('subcate'=>$subcate,'catid'=>$catid,'sub_category_id'=>$sub_category_id);

		return View('admin.ajax.subcategory')->with($data_onview);


	}

	public function subcategory_list_add(Request $request)
	{


		$catid = $request->catid;

		$sub_category_id = $request->sub_category_id;

	    $subcate = DB::table('product_service_sub_category')

					->where('category_id', '=' ,$catid)

					->orderBy('id', 'asc')

				    ->get();

		$data_onview = array('subcate'=>$subcate,'catid'=>$catid,'sub_category_id'=>$sub_category_id);

		return View('admin.ajax.subcategory')->with($data_onview);


	}

	public function subcategorylist(Request $request)
	{

		$cateid = $request->cateid;

		$sub_cat_id = $request->sub_cat_id;

	    $sub_cate = DB::table('product_service_sub_category')
					->where('category_id', '=' ,$cateid)
					->orderBy('id', 'asc')
				    ->get();

		$data_onview = array('sub_cate'=>$sub_cate,'sub_cat_id'=>$sub_cat_id);

		return View('admin.ajax.sub_category')->with($data_onview);

	}


    public function product_service_merchant_list()
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'asc')

			->get();

        $data_onview = array('vendor_list' =>$vendor_list);

		return View('admin.ps_merchant_list')->with($data_onview);

	}


public function product_merchant_list()
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'asc')

			->get();

        $data_onview = array('vendor_list' =>$vendor_list);

		return View('admin.product_merchant_list')->with($data_onview);

	}


	public function all_product_list()
	{

		DB::connection()->enableQueryLog();

	    $rest_list = DB::table('product_service')->where('type', '=', 0)->orderBy('id', 'asc')->get();

		$cat_name = DB::table('product_service_category')->value('category');

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('admin.all_product_list')->with($data_onview);

	}

	public function my_product_list()
	{

		DB::connection()->enableQueryLog();

	    $rest_list = DB::table('product_service')->where('type', '=', 0)->where('vendor_id', '=', 0)->orderBy('id', 'asc')->get();

		$cat_name = DB::table('product_service_category')->value('category');
		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('admin.my_product_list')->with($data_onview);

	}

	public function merchant_product_list()
	{

		DB::connection()->enableQueryLog();

	    $rest_list = DB::table('product_service')->where('vendor_id', '!=' ,0)->orderBy('id', 'asc')->simplePaginate(25);

		$cat_name = DB::table('product_service_category')->value('category');

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('admin.merchant_product_list')->with($data_onview);

	}


/** New product code */

	public function product_form()
	{

	    $category_list  = DB::table('product_service_category')->where('type', '!=' ,'3')->orderBy('category', 'asc')->get();

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id;

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('sub_category', 'asc')

							->get();

		$product_unit = DB::table('product_unit')
                ->where('type', '!=' ,'3')
							->where('status', '=' ,'1')

							->orderBy('unit_name', 'asc')->get();

		$product_types = DB::table('product_types')

							->where('status', '=' ,'1')

							->orderBy('types_name', 'asc')->get();

		$product_brands = DB::table('product_brands')

							->where('status', '=' ,'1')

							->orderBy('brand_name', 'asc')->get();


        $id = 0;

  		$data_onview = array('category_list'=>$category_list,
  			                 'sub_category_list'=>$sub_category_list,
							 'product_unit'=>$product_unit,
							 'product_types'=>$product_types,
							 'product_brands'=>$product_brands,
							 'id'=>$id);

		return View('admin.product_form')->with($data_onview);

	}



   public function product_edit($id)
	{


       	$category_list  = DB::table('product_service_category')->where('type', '!=' ,'3')->orderBy('category', 'asc')->get();
		$sub_category_list = DB::table('product_service_sub_category')

							->orderBy('sub_category', 'asc')

							->get();

		$product_unit = DB::table('product_unit')
		         ->where('type', '!=' ,'3')

							->where('status', '=' ,'1')

							->orderBy('unit_name', 'asc')->get();

		$product_detail  = DB::table('product_service')->where('id', '=' ,$id)->get();

		$gallery_image = DB::table('ps_images')

	     					->where('ps_id', '=' ,$id)

							 ->whereNull('thumb')

							->get();

		$product_types = DB::table('product_types')

							->where('status', '=' ,'1')

							->orderBy('types_name', 'asc')->get();

		$product_brands = DB::table('product_brands')

							->where('status', '=' ,'1')

							->orderBy('brand_name', 'asc')->get();

			//echo "<pre>";print_r($gallery_image);die();

		$glimage = array();


			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }

  		  	$data_onview = array('product_detail' =>$product_detail,

								 'product_unit'=>$product_unit,

								 'category_list'=>$category_list,

  			                     'sub_category_list'=>$sub_category_list,

  			                     'product_types'=>$product_types,

							     'product_brands'=>$product_brands,

								 'glimage'=>$glimage,

								 'id'=>$id);

			return View('admin.product_form')->with($data_onview);

	}



	public function product_action(Request $request)
	{

		//print_r($request->product_image);die;
		$product_id = Input::get('product_id');

		$product_old_img = Input::get('product_old_img');

		$product_old_glimg = Input::get('product_old_glimg');

		$date = date('Y-m-d H:i:s');

		$proname = Input::get('product_name');

		//$product_code = Str::random(8);

		$product_code_str = $this->random_string(8);

		$product_code = Input::get('product_code');

		$slug = Str::slug($proname.' '.$product_code_str, '-');

		$vendor_id = '0';

        $imageTlarg = '';

		    // if ($request->product_id==0) {

		    //     $this->validate($request,[

		    //     	"product_image" => "required|image",

		    //     ],[

		    //     	"product_image.image" => "The product image must be an image.",

		    //     ]);

		    //     $this->validate($request,["product_image_gallery" => "required"]);

		    //   }

        //         if ($request->hasFile("product_image")) {

        //         	$productImage=$request->product_image;

        //         	$image =$largeImage= Image::make($productImage->getRealPath());

				// 	$image->resize(200,200)->save(public_path('uploads/product/'.$pname=str_random(6).time().'.'.$productImage->getClientOriginalExtension()));

				// 	$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(6).time().'.'.$productImage->getClientOriginalExtension());

        //         }else{

                   $pname = $product_old_img;

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg;

                   if(empty($glimg)){ $glimg = "dummy.png"; }

               // }

		if($product_id==0)

		{

				$product = new Productservice;

				$product->vendor_id = $vendor_id;

				$product->type = Input::get('ps_type');

				$product->category_id = Input::get('category_id');

				$product->sub_category_id = Input::get('sub_category_id');

				$product->name = Input::get('product_name');

				$product->slug = $slug;

				$product->description =  Input::get('product_desc');

				$product->price =  Input::get('product_price');

				$product->excise_tax =  Input::get('excise_tax');

				$product->quantity =  Input::get('product_quantity');

				$product->unit =  Input::get('unit');

				$product->brands =  Input::get('brands');

				$product->types =  Input::get('types');

				$product->potency_thc =  Input::get('potency_thc');

				$product->potency_cbd =  Input::get('potency_cbd');

				$product->image =  $pname;

				$product->keyword =  '';

				$product->product_code =  $product_code;

				$product->stock =  Input::get('stock');

				$product->status =  Input::get('status');

				$product->created_at = $date;

				$product->save();

				DB::table('ps_images')->insert([

				'name' => $glimg,

				'ps_id' => $product->id,

				'thumb' => NULL,

				'created_at' => $date,

				'updated_at' => $date

				]);

				// $files=$request->file('product_image_gallery');

				// foreach($files as $file){

				// 	$name=str_random(6).time().'.'.$file->getClientOriginalExtension();

				// 	$image=$file;

				// 	$image->move('public/uploads/product/',$name);

				//  	/* if ($i==1) {

				// 		$images =Image::make($file->getRealPath());

				// 		$images->resize(200,200)->save(public_path('uploads/product/thumb/'.$name));

				//  	} */

				// 	DB::table('ps_images')->insert([

				// 		'name' => $name,

				// 		'ps_id' => $product->id,

				// 		'thumb' => NULL,

				// 		'created_at' => $date,

				// 		'updated_at' => $date

				// 	]);

				//  }

				Session::flash('message', 'Item Added Sucessfully!');

				// return redirect()->to('/admin/my-product-list');
				return redirect()->back()->withMessage('Successfully!');



		}

		else

		{

			/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			DB::table('product_service')

            ->where('id', $product_id)

            ->update(['category_id' =>Input::get('category_id'),

            	      'sub_category_id' => Input::get('sub_category_id'),

            	      'type' =>Input::get('ps_type'),

            	      'name' =>Input::get('product_name'),

            	      'product_code' =>$product_code,

					  'description' =>Input::get('product_desc'),

					  'price'=>Input::get('product_price'),

					  'excise_tax'=>Input::get('excise_tax'),

					  'quantity'=>Input::get('product_quantity'),

					  'unit' =>  Input::get('unit'),

					  'brands' =>  Input::get('brands'),

					  'types' =>  Input::get('types'),

					  'potency_thc' =>  Input::get('potency_thc'),

					  'potency_cbd' =>  Input::get('potency_cbd'),

					  'image'=>$pname,

					  'keyword'=>'',

					  'stock' =>  Input::get('stock'),

					  'status'=>Input::get('status'),

					  'updated_at'=>$date

					 ]);


            //print_r($imageTlarg);die;

              if(!empty($imageTlarg)){

				DB::table('ps_images')

				->where('ps_id', $product_id)

				->where('thumb', 1)

				->update([

				'name' => $imageTlarg,

				]);

			   }
			  //  else{
			  //  		DB::table('ps_images')->insert([

					// 'name' => $imageTlarg,

					// 'ps_id' => $product_id,

					// 'thumb' => 1,

					// 'created_at' => $date,

					// 'updated_at' => $date

					// ]);
			  //  }

			   if($request->hasFile("product_image_gallery")) {

				$files=$request->file('product_image_gallery');

				foreach($files as $file){

					$name=str_random(6).time().'.'.$file->getClientOriginalExtension();

					$image=$file;

					$image->move('public/uploads/product/',$name);

					DB::table('ps_images')->insert([

						'name' => $name,

						'ps_id' => $product_id,

						'thumb' => NULL,

						'created_at' => $date,

						'updated_at' => $date

					]);

				 }

               }

				session()->flash('message', 'Item Update Sucessfully!');

				//return redirect()->to('/admin/product-edit/'.$product_id);

				return redirect()->back()->withMessage('Successfully!');

			    // return redirect()->to('/admin/my-product-list');

		}

	}


	public function product_delete($id)
	{

		DB::table('product_service')->where('id', '=', $id)->delete();

        DB::table('ps_images')->where('ps_id', '=', $id)->delete();

        DB::table('cart')->where('ps_id', '=', $id)->delete();

       // DB::table('orders_ps')->where('ps_id', '=', $id)->delete();

		Session::flash('message', 'Item Delete Sucessfully!');

		return redirect()->to('/admin/my-product-list');

	}

/* product code end */

	public function merchant_product_service_list(Request $request)
	{

		$vendor_id = $request->id;

	    $rest_list = DB::table('product_service')

					->where('vendor_id', '=' ,$vendor_id)

					->where('type', '=', 0)

					->orderBy('id', 'asc')

					->get();


		$rest_list_admin = DB::table('admin_merchant_product')

		->leftJoin('product_service', 'admin_merchant_product.product_id', '=', 'product_service.id')

		->where('admin_merchant_product.vendor_id', '=' ,$vendor_id)

		->select('admin_merchant_product.*','product_service.*')

		->orderBy('admin_merchant_product.id', 'asc')

		->get();


		$catid = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')
						->where('id', '=' ,$catid)
						->value('category');

		$data_onview = array('rest_list' =>$rest_list,'rest_list_admin' =>$rest_list_admin,'vendor_id' =>$vendor_id,'cat_name' =>$cat_name);

		return View('admin.product_service_list')->with($data_onview);

	}


   	public function merchant_inventory_copy(Request $request)
	{

		$vendor_id = $request->id;

	    DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')

		    //->where('vendor_status', '=' ,1)

		    ->where('vendor_id', '!=' ,$vendor_id)

			->orderBy('vendor_id', 'asc')

			->get();

        $data_onview = array('vendor_list' =>$vendor_list,'from_vendor_id' =>$vendor_id);

		return View('admin.merchant_inventory_copy')->with($data_onview);

	}


	public function vendor_product_action(Request $request)
	{

		$vendors_id = Input::get('add_vendor_id');

		$from_vendor_id = Input::get('from_vendor_id');


		if(empty($vendors_id)){

        Session::flash('message', 'Please Select Driver !');
        return redirect()->to('/admin/merchant-inventory-copy/'.$from_vendor_id);

		}else{

       	$get_products = DB::table('product_service')->where('vendor_id', '=' ,$from_vendor_id)->where('vendor_product_id', '=' ,0)->get();

        if(count($get_products)>0){

		foreach ($vendors_id as $key => $vendorid) {

        foreach ($get_products as $key => $get_product) {

        $get_product_imgs = DB::table('ps_images')->where('ps_id', '=' ,$get_product->id)->get();


        $checkexist = DB::table('product_service')->where('vendor_id', '=' ,$vendorid)->where('vendor_product_id', '=' ,$get_product->id)->get();


       if(count($checkexist)==0){

		$getid = DB::table('product_service')->insertGetId(array(
                                                'vendor_id'      => $vendorid,
                                                'vendor_product_id'     => $get_product->id,
                                                'type'    => $get_product->type,
                                                'category_id' => $get_product->category_id,
                                                'sub_category_id'    => $get_product->sub_category_id,
                                                'avg_rating'    => $get_product->avg_rating,
                                                'rating_count'    => $get_product->rating_count,
                                                'name'    => $get_product->name,
                                                'slug'    => $get_product->slug,
                                                'description'    => $get_product->description,
                                                'price'    => $get_product->price,
                                                'quantity'    => $get_product->quantity,
                                                'unit'    => $get_product->unit,
                                                'brands'    => $get_product->brands,
                                                'types'    => $get_product->types,
                                                'potency_thc'    => $get_product->potency_thc,
                                                'potency_cbd'    => $get_product->potency_cbd,
                                                'image'    => $get_product->image,
                                                'keyword'    => $get_product->keyword,
                                                'product_code'    => $get_product->product_code,
                                                'status'    => $get_product->status,
                                                'login_status'    => $get_product->login_status,
                                                'stock'    => $get_product->stock,
                                                'popular'    => $get_product->popular,
                                                'created_at'    => $get_product->created_at,
                                                'updated_at'    => $get_product->updated_at
                                                ));

		if($getid){

            foreach ($get_product_imgs as $key => $get_product_img) {

		        DB::table('ps_images')->insertGetId(array(
                                                'ps_id'      => $getid,
                                                'name'     => $get_product_img->name,
                                                'thumb'    => $get_product_img->thumb,
                                                'created_at' => $get_product_img->created_at,
                                                'updated_at'    => $get_product_img->updated_at
                                                ));

           }

		}

            Session::flash('message', 'Item Added Sucessfully!');

	       }else{

            //Session::flash('message', 'Item already exists!');

            Session::flash('message', 'Item Update Sucessfully!');

	       }

		 }

		}

		}else{

        Session::flash('message', 'No Product Found For This Driver !');
        return redirect()->to('/admin/merchant-inventory-copy/'.$from_vendor_id);

		}

	    }

		return redirect()->to('/admin/merchant-inventory-copy/'.$from_vendor_id);

	}


   	public function merchant_product_inventory_list(Request $request)
	{

		$vendor_id = $request->id;

	    $rest_list = DB::table('product_service')

					->where('vendor_id', '=' ,$vendor_id)

					->orderBy('id', 'asc')

					->get();


		$rest_list_admin = DB::table('admin_merchant_product')

		->leftJoin('product_service', 'admin_merchant_product.product_id', '=', 'product_service.id')

		->where('admin_merchant_product.vendor_id', '=' ,$vendor_id)

		->select('admin_merchant_product.*','product_service.*')

		->orderBy('admin_merchant_product.id', 'asc')

		->get();


		$catid = DB::table('vendor')

						->where('vendor_id', '=' ,$vendor_id)

						->value('category_id');


		$cat_name = DB::table('product_service_category')

						->where('id', '=' ,$catid)

						->value('category');


		$data_onview = array('rest_list' =>$rest_list,'rest_list_admin' =>$rest_list_admin,'vendor_id' =>$vendor_id,'cat_name' =>$cat_name);


		return View('admin.product_inventory_list')->with($data_onview);

	}


   public function inventory_product_action(Request $request)
	{

	  $searchpro = $request->all();

	  //$vendor_id = Auth::guard('vendor')->user()->vendor_id;

      $productid = $searchpro['data']['productid'];
      $productqty = $searchpro['data']['productqty'];
      $vendor_id = $searchpro['data']['vendorid'];

      $updatepro = DB::table('product_service')->where('id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['quantity' => $productqty]);

   if($updatepro==0){

    $updatepro = DB::table('admin_merchant_product')->where('product_id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['product_quantity' => $productqty]);

   }

   return $updatepro;

   }


	public function merchant_product_service_form(Request $request)
	{

		$vendor_id = $request->id;

	    $category_list  = DB::table('product_service_category')->where('type', '!=', 3)->orderBy('category', 'asc')->get();

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id;

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('sub_category', 'asc')

							->get();

		$product_unit = DB::table('product_unit')
              ->where('type', '!=', 3)
							->where('status', '=' ,'1')

							->orderBy('unit_name', 'asc')->get();

		$product_types = DB::table('product_types')

							->where('status', '=' ,'1')

							->orderBy('types_name', 'asc')->get();

		$product_brands = DB::table('product_brands')

							->where('status', '=' ,'1')

							->orderBy('brand_name', 'asc')->get();

        $id = 0;


  		  		$data_onview = array('vendor_id' =>$vendor_id,

								 'category_list'=>$category_list,

								 'sub_category_list'=>$sub_category_list,

								 'product_unit'=>$product_unit,

								 'product_types'=>$product_types,

							     'product_brands'=>$product_brands,

								 'id'=>$id);


		return View('admin.merchant_ps_form')->with($data_onview);

	}

	public function crop_product_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('product_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

        DB::table('product_service')
		//->where('vendor_id', $vendor_id)
		->where('id', $product_id)
		->update(['image' => $product_img]);

		DB::table('ps_images')
		->where('ps_id', $product_id)
		->where('thumb', 1)
		->update(['name' => $product_img]);

        return response()->json(['status'=>$product_img]);

	}


	public function gallery_crop_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('product_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

		DB::table('ps_images')
		->insert([
			'ps_id' => $product_id,
			'name' => $product_img,
			'created_at' => date("Y-m-d h:i:s"),
			'updated_at'=> date("Y-m-d h:i:s")
		]);

        return response()->json(['status'=>$product_img]);

	}



   public function merchant_product_service_edit($id)
	{

		$vendor_id = DB::table('product_service')

		->where('id', '=', $id)

		->value('vendor_id');

		$category_list  = DB::table('product_service_category')->where('type', '!=', 3)->orderBy('category', 'asc')->get();
		$sub_category_list = DB::table('product_service_sub_category')

							->orderBy('sub_category', 'asc')

							->get();

		$product_unit = DB::table('product_unit')
              ->where('type', '!=', 3)
							->where('status', '=' ,'1')

							->orderBy('unit_name', 'asc')->get();

		$product_detail  = DB::table('product_service')->where('id', '=' ,$id)->get();

		$gallery_image = DB::table('ps_images')

	     					->where('ps_id', '=' ,$id)

							 ->whereNull('thumb')

							->get();

		$product_types = DB::table('product_types')

							->where('status', '=' ,'1')

							->orderBy('types_name', 'asc')->get();

		$product_brands = DB::table('product_brands')

							->where('status', '=' ,'1')

							->orderBy('brand_name', 'asc')->get();

			//echo "<pre>";print_r($gallery_image);die();

		$glimage = array();


			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }

  		  	$data_onview = array('rest_detail' =>$product_detail,

								 'product_unit'=>$product_unit,

								 'vendor_id'=>$vendor_id,

								 'category_list'=>$category_list,

  			                     'sub_category_list'=>$sub_category_list,

  			                     'product_types'=>$product_types,

							     'product_brands'=>$product_brands,

								 'glimage'=>$glimage,

								 'id'=>$id);

			return View('admin.merchant_ps_form')->with($data_onview);


	}



	public function merchant_product_service_action(Request $request)
	{

		//print_r($request->product_image);die;
		$product_id = Input::get('product_id');

		$product_old_img = Input::get('product_old_img');

		$product_old_glimg = Input::get('product_old_glimg');

		$date = date('Y-m-d H:i:s');

		$proname = Input::get('product_name');

		//$product_code = Str::random(8);

		$product_code = $this->random_string(8);

		$product_code = Input::get('product_code');

		$slug = Str::slug($proname.' '.$product_code, '-');

		$vendor_id = Input::get('vendor_id');

        $imageTlarg = '';

		    // if ($request->product_id==0) {

		    //     $this->validate($request,[

		    //     	"product_image" => "required|image",

		    //     ],[

		    //     	"product_image.image" => "The product image must be an image.",

		    //     ]);

		    //     $this->validate($request,["product_image_gallery" => "required"]);

		    //   }

        //         if ($request->hasFile("product_image")) {

        //         	$productImage=$request->product_image;

        //         	$image =$largeImage= Image::make($productImage->getRealPath());

				// 	$image->resize(200,200)->save(public_path('uploads/product/'.$pname=str_random(6).time().'.'.$productImage->getClientOriginalExtension()));

				// 	$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(6).time().'.'.$productImage->getClientOriginalExtension());

        //         }else{

                  $pname = $product_old_img;

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg;

                   if(empty($glimg)){ $glimg = "dummy.png"; }

                //}

		if($product_id==0)

		{

				$product = new Productservice;

				$product->vendor_id = $vendor_id;

				$product->type = Input::get('ps_type');

				$product->category_id = Input::get('category_id');

				$product->sub_category_id = Input::get('sub_category_id');

				$product->name = Input::get('product_name');

				$product->slug = $slug;

				$product->description =  Input::get('product_desc');

				$product->price =  Input::get('product_price');

				$product->excise_tax =  Input::get('excise_tax');

				$product->quantity =  Input::get('product_quantity');

				$product->unit =  Input::get('unit');

				$product->brands =  Input::get('brands');

				$product->types =  Input::get('types');

				$product->potency_thc =  Input::get('potency_thc');

				$product->potency_cbd =  Input::get('potency_cbd');

				$product->image =  $pname;

				$product->keyword =  '';

				$product->product_code =  $product_code;

				$product->stock =  Input::get('stock');

				$product->status =  Input::get('status');

				$product->created_at = $date;

				$product->save();

				DB::table('ps_images')->insert([

				'name' => $glimg,

				'ps_id' => $product->id,

				'thumb' => NULL,

				'created_at' => $date,

				'updated_at' => $date

				]);

				// $files=$request->file('product_image_gallery');

				// foreach($files as $file){

				// 	$name=str_random(6).time().'.'.$file->getClientOriginalExtension();

				// 	$image=$file;

				// 	$image->move('public/uploads/product/',$name);

				//  	/* if ($i==1) {

				// 		$images =Image::make($file->getRealPath());

				// 		$images->resize(200,200)->save(public_path('uploads/product/thumb/'.$name));

				//  	} */

				// 	DB::table('ps_images')->insert([

				// 		'name' => $name,

				// 		'ps_id' => $product->id,

				// 		'thumb' => NULL,

				// 		'created_at' => $date,

				// 		'updated_at' => $date

				// 	]);

				//  }

				Session::flash('message', 'Item Added Sucessfully!');

				return redirect()->to('/admin/merchant-product-service-list/'.$vendor_id);


		}

		else

		{

			/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			DB::table('product_service')

            ->where('id', $product_id)

            ->update(['category_id' =>Input::get('category_id'),

            	      'sub_category_id' => Input::get('sub_category_id'),

            	      'type' =>Input::get('ps_type'),

            	      'name' =>Input::get('product_name'),

					  'product_code' =>$product_code,

					  'description' =>Input::get('product_desc'),

					  'price'=>Input::get('product_price'),

					  'excise_tax'=>Input::get('excise_tax'),

					  'quantity'=>Input::get('product_quantity'),

					  'unit' =>  Input::get('unit'),

					  'brands' =>  Input::get('brands'),

					  'types' =>  Input::get('types'),

					  'potency_thc' =>  Input::get('potency_thc'),

					  'potency_cbd' =>  Input::get('potency_cbd'),

					  'image'=>$pname,

					  'keyword'=>'',

					  'stock' =>  Input::get('stock'),

					  'status'=>Input::get('status'),

					  'updated_at'=>$date

					 ]);

            //print_r($imageTlarg);die;

              if(!empty($imageTlarg)){

				DB::table('ps_images')

				->where('ps_id', $product_id)

				->where('thumb', 1)

				->update([

				'name' => $imageTlarg,

				]);

			   }
			  //  else{
			  //  		DB::table('ps_images')->insert([

					// 'name' => $imageTlarg,

					// 'ps_id' => $product_id,

					// 'thumb' => 1,

					// 'created_at' => $date,

					// 'updated_at' => $date

					// ]);
			  //  }

			   if($request->hasFile("product_image_gallery")) {

				$files=$request->file('product_image_gallery');

				foreach($files as $file){

					$name=str_random(6).time().'.'.$file->getClientOriginalExtension();

					$image=$file;

					$image->move('public/uploads/product/',$name);

					DB::table('ps_images')->insert([

						'name' => $name,

						'ps_id' => $product_id,

						'thumb' => NULL,

						'created_at' => $date,

						'updated_at' => $date

					]);

				 }

               }

				session()->flash('message', 'Item Update Sucessfully!');

				return redirect()->to('/admin/merchant-product-service-edit/'.$product_id);

		}

	}


	public function merchant_product_service_delete($id)
	{

        $vendor_id = 	DB::table('product_service')
		->where('id', '=', $id)
		->value('vendor_id');

		DB::table('product_service')->where('id', '=', $id)->delete();

        DB::table('ps_images')->where('ps_id', '=', $id)->delete();

        DB::table('cart')->where('ps_id', '=', $id)->delete();

        //DB::table('orders_ps')->where('ps_id', '=', $id)->delete();

		Session::flash('message', 'Item Delete Sucessfully!');

		return redirect()->to('/admin/merchant-product-service-list/'.$vendor_id);

	}


 	public function product_gallery_delete($gl_id)
	{

	$getpsid =DB::table('ps_images')

		->where('id', '=', $gl_id)

		->value('ps_id');



		DB::table('ps_images')

		->where('id', '=', $gl_id)

		->delete();


		Session::flash('message', 'Image Delete Sucessfully!');


		return redirect()->to('/admin/product-edit/'.$getpsid);



	}


  	public function merchant_photo_delete($picno,$mid)
	{

        if($picno==2){ $profileno = 'profile_img2'; }
        if($picno==3){ $profileno = 'profile_img3'; }
        if($picno==4){ $profileno = 'profile_img4'; }

		DB::table('vendor')->where('vendor_id', $mid)->update([$profileno => '']);

		Session::flash('message', 'Profile Image Delete Successfully!');

		return redirect()->to('/admin/merchant-form/'.$mid);

	}


	public function merchant_list()
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'asc')

			->get();

       $data_onview = array('vendor_list' =>$vendor_list);

	   return View('admin.merchant_list')->with($data_onview);

	}


	public function export_in_excel(Request $request){

		$vendor_detail  = DB::table('vendor')->orderBy('vendor_id', 'ASC')->get();

        $vendor_status = '';
		// Excel file name for download
		$fileName = "Vendors_" . date('Y-m-d') . ".xls";

		// Column names
		$fields = array('Vendor Id', 'Vendor Name', 'Contact Number', 'Company Name', 'Email', 'Mailing Address', 'Apt/Suite/Unit Number', 'City', 'State', 'Zip Code', 'Merchant Google Map Address', 'Serviced Radius', 'Driver License', 'Expiration Date', 'Social Security Number or EIN', 'Date of Birth', 'Permit/License Type', 'Permit/License Number', 'Permit/License Expiration Date', 'Product Description');

		$excelData = implode("\t", array_values($fields)) . "\n";

		if($vendor_detail){

		foreach ($vendor_detail as $key => $value) {

	    if($value->vendor_status==0){  $vendor_status='InActive'; }else{ $vendor_status='Active'; }

        $lineData = array($value->vendor_id, $value->name.' '.$value->last_name, $value->mob_no, $value->business_name, $value->email, $value->mailing_address, $value->address, $value->city, $value->state, $value->zipcode, $value->market_area, $value->service_radius, $value->driver_license, $value->license_expiry, $value->ssn, $value->dob, $value->permit_type, $value->permit_number, $value->permit_expiry, $value->description);


       // array_walk($lineData, 'filterData');
        $excelData .= implode("\t", array_values($lineData)) . "\n";

	}

	}else{

		$excelData .= 'No records found...'. "\n";

	}
					// Headers for download
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$fileName\"");

		// Render excel data
		echo $excelData;

		exit;

	}



	public function vendor_pass_req_list()

	{

		DB::connection()->enableQueryLog();



		$vendor_req_list = DB::table('vendor')

		->select('*')

		->where('forgetpass_request_status', '=' ,'0')

		->orderBy('vendor_id', 'desc')

		->get();





		$data_onview = array('vendor_req_list' =>$vendor_req_list);



		return View('admin.vendor_fgt_pass_req_list')->with($data_onview);

	}





	public function vendor_search()

	{



		$vendor_list = DB::table('vendor');



		if( (Input::get('vendor_cont')) && (!empty(Input::get('vendor_cont'))))

		{



			$vendor_list = $vendor_list->where('vendor.mob_no', 'like' ,Input::get('vendor_cont'));

		}



		if( (Input::get('vendor_email')) && (!empty(Input::get('vendor_email'))))

		{

			$vendor_list = $vendor_list->where('vendor.email', 'like' ,Input::get('vendor_email'));

		}



		$vendor_list = $vendor_list->select('vendor.*');

		$vendor_list = $vendor_list->orderBy('vendor.vendor_id', 'desc');

		$vendor_list = $vendor_list->get();



		$data_onview = array('vendor_list' =>$vendor_list);

		return View('admin.ajax.vendor_list')->with($data_onview);

	}


	public function merchant_form(Request $request)
	{

		if($request->id)
		{

			$id = $request->id;

			$user_detail  = DB::table('vendor')->where('vendor_id', '=' ,$id)->get();

			$type = $user_detail[0]->type;

			$catagory = DB::table('product_service_category')

						->where('type', '=' ,$type)

						->where('status', '=' ,1)

						->get();

			$cateid = $user_detail[0]->category_id;

			$subcatagory = DB::table('product_service_sub_category')

						->where('category_id', '=' ,$cateid)

						->where('status', '=' ,1)

						->orderBy('id', 'asc')

						->get();

  		  	$data_onview = array('user_detail' =>$user_detail,'catagory'=>$catagory,'subcatagory'=>$subcatagory,'id'=>$id);

			return View('admin.merchant_form')->with($data_onview);

		}

		else

		{

			$id =0;

			$catagory = DB::table('product_service_category')->get();

  		  	$data_onview = array('id'=>$id,'catagory'=>$catagory);

			return View('admin.merchant_form')->with($data_onview);

		}

	}


		public function merchant_form_view(Request $request)
	{

		if($request->id)
		{

			$id = $request->id;

			$user_detail  = DB::table('vendor')->where('vendor_id', '=' ,$id)->get();

			$type = $user_detail[0]->type;

			$catagory = DB::table('product_service_category')

						->where('type', '=' ,$type)

						->where('status', '=' ,1)

						->get();

			$cateid = $user_detail[0]->category_id;

			$subcatagory = DB::table('product_service_sub_category')

						->where('category_id', '=' ,$cateid)

						->where('status', '=' ,1)

						->orderBy('id', 'asc')

						->get();

  		  	$data_onview = array('user_detail' =>$user_detail,'catagory'=>$catagory,'subcatagory'=>$subcatagory,'id'=>$id);

			return View('admin.merchant_form_view')->with($data_onview);

		}

	}
   public function crop_image_save(Request $request)
	{

		$image = $request->profile;
		$vendor_id = Input::get('vendor_id');
		$profile_field = Input::get('profile_field');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $profile_img1= '1'.time().'.png';
        $path = public_path('uploads/vendor/profile/'.$profile_img1);
        file_put_contents($path, $image);

        DB::table('vendor')
		->where('vendor_id', $vendor_id)
		->update([$profile_field => $profile_img1]);
        return response()->json(['status'=>$profile_img1]);

	}

	public function merchant_action(Request $request)
	{

		$vendor_id = Input::get('vendor_id');

		$profile_img1 = $request->profile_img1_old;

        $license_front = $request->license_front_old;

        $license_back = $request->license_back_old;

        $profile_img2 = $request->profile_img2_old;

        $profile_img3 = $request->profile_img3_old;

        $profile_img4 = $request->profile_img4_old;

        $map_icon = $request->map_icon_old;

        $pvideo1 = $request->pvideo1_old;

        //print_r($pvideo1);die;


        $open_close_mon = $request->open_close_mon;
        $mon = $request->mon1.'-'.$request->mon2;

        $open_close_tue = $request->open_close_tue;
        $tue = $request->tue1.'-'.$request->tue2;

        $open_close_wed = $request->open_close_wed;
        $wed = $request->wed1.'-'.$request->wed2;

        $open_close_thu = $request->open_close_thu;
        $thu = $request->thu1.'-'.$request->thu2;

        $open_close_fri = $request->open_close_fri;
        $fri = $request->fri1.'-'.$request->fri2;

        $open_close_sat = $request->open_close_sat;
        $sat = $request->sat1.'-'.$request->sat2;

        $open_close_sun = $request->open_close_sun;
        $sun = $request->sun1.'-'.$request->sun2;


		$userData = array(
			'vendor_id' => trim($vendor_id),
			'monday' => trim($mon),
			'tuesday' => trim($tue),
			'wednesday' => trim($wed),
			'thursday' =>  trim($thu),
			'friday' => trim($fri),
			'saturday' => trim($sat),
			'sunday' => trim($sun),
			'open_close_mon' => trim($open_close_mon),
			'open_close_tue' => trim($open_close_tue),
			'open_close_wed'=> trim($open_close_wed),
			'open_close_thu' => trim($open_close_thu),
			'open_close_fri'=> trim($open_close_fri),
			'open_close_sat' => trim($open_close_sat),
			'open_close_sun' => trim($open_close_sun)
		);


        $url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$request->market_area."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

		$response = file_get_contents($url);

		$data = json_decode($response);

		$geo=$data->results[0]->geometry->location;


		 if($request->hasFile('video'))
		    {

		   $imagename = DB::table('vendor')->where('vendor_id', $vendor_id)->value('video');
            if(File::exists(public_path($imagename))){
            File::delete(public_path($imagename));
            }

		     $path = $request->file('video')->store('videos', ['disk' => 'my_files']);
		     $pvideo1 =  $path;

		    }


		/*Uploading Images*/

		$request->hasFile("profile_img1") ? $request->file("profile_img1")->move("public/uploads/vendor/profile/",$profile_img1=str_random(16).'.jpg') : "";


		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/vendor/license/",$license_front=str_random(16).'.'.$request->license_front->extension()) : "";


		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/vendor/license/",$license_back=str_random(16).'.'.$request->license_back->extension()) : "";


		/*uploading optional images*/

	    $request->hasFile("profile_img2") ? $request->file("profile_img2")->move("public/uploads/vendor/profile/",$profile_img2=str_random(16).'.jpg') : "";


		$request->hasFile("profile_img3") ? $request->file("profile_img3")->move("public/uploads/vendor/profile/",$profile_img3=str_random(16).'.jpg') : "";


		$request->hasFile("profile_img4") ? $request->file("profile_img4")->move("public/uploads/vendor/profile/",$profile_img4=str_random(16).'.jpg') : "";

		//$request->hasFile("map_icon") ? $request->file("map_icon")->move("public/uploads/",$map_icon=str_random(16).'.jpg') : "";

		if($request->hasFile("map_icon")) {
		$mapicon = $request->map_icon;
		$image = Image::make($mapicon->getRealPath());
		$image->resize(50, 50)->save('public/uploads/' . $map_icon = str_random(6) . time() . '.' . $mapicon->getClientOriginalExtension());
		}


        $subcategory = $request->subcategory ;

		$sub_category = json_encode($subcategory);


		$browsername = $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);

        if($browsername=='Safari'){

        if(!empty($request->license_expiry)){

        $license_expiry = $this->dateFormatchange($request->license_expiry);

        }else{  $license_expiry = ''; }

		if(!empty($request->dob)){ $dob = $this->dateFormatchange($request->dob); }else{ $dob = ''; }

		if(!empty($request->permit_expiry)){ $permit_expiry = $this->dateFormatchange($request->permit_expiry);}else{ $permit_expiry = ''; }

        }else{

        $license_expiry = date("Y-m-d", strtotime($request->license_expiry));
		$dob = date("Y-m-d", strtotime($request->dob));
		$permit_expiry = date("Y-m-d", strtotime($request->permit_expiry));

        }

        if($license_expiry=='1969-12-31'){ $license_expiry=''; }
        if($dob=='1969-12-31'){ $dob=''; }
        if($permit_expiry=='1969-12-31'){ $permit_expiry=''; }

		if($vendor_id==0)
		{


			$input['email'] = Input::get('email');

			$rules = array('email' => 'unique:vendor,email');

			$validator = Validator::make($input, $rules);

			$msg = 'Duplicate Email';

			$useremail  = DB::table('users')->where('email', '=' , Input::get('email'))->first();

			$validator = Validator::make($request->all(), [
					'email' => 'required|email|max:100|unique:vendor',
					'password' => 'required_with:confirm_password|same:confirm_password'
				]);

			   	if ($validator->fails()) {
			   			$email = trim(Input::get('email'));
			   			$password = Hash::make(trim(Input::get('password')));
			   			return Redirect::back()->withErrors($validator);

                Session::put('email', $email);
				Session::put('password', $password);
           	}

			$request->session()->forget(['email','password']);


			/* if(!empty($useremail)){

			Session::flash('message', 'Duplicate Email!');

			return redirect()->to('/admin/merchant-form/')->with('msg');

		    } */


			/* if (($validator->fails())||(!empty($useremail)) ) {


				return redirect()->to('/admin/merchant-form/')->with('msg');

			}

			else { */


         $uniqueid = $this->random_string(12);

				$vendor = new Vendor;

				$vendor->username = $request->username;

				$vendor->name = $request->name;

				$vendor->last_name = $request->last_name;

				$vendor->email = $request->email;

				$vendor->mob_no = $request->mob_no;

				$vendor->contact_no = $request->contact_no;

				$vendor->business_name = $request->business;

				$vendor->address = $request->address;

				$vendor->mailing_address = $request->mailing_address;

				$vendor->city = $request->city;

				$vendor->state = $request->state;

				$vendor->zipcode = $request->zipcode;

                $vendor->pick_up_address =  $request->pick_up_address;

				  $vendor->market_area =  $request->market_area;

				  $vendor->service_radius =  $request->service_radius;

				  $vendor->driver_license =  $request->driver_license;

				  $vendor->license_expiry =  $license_expiry;

				  $vendor->license_front =  $license_front;

				  $vendor->license_back =  $license_back;

				  $vendor->ssn =  $request->ssn;

				  $vendor->dob =  $dob;

				  $vendor->profile_img1 =  $profile_img1;

				  $vendor->profile_img2 =  $profile_img2;

				  $vendor->profile_img3 =  $profile_img3;

				  $vendor->profile_img4 =  $profile_img4;

				  $vendor->video=  $pvideo1;

				  $vendor->map_icon =  $map_icon;

				  $vendor->type =  $request->type;

				  $vendor->category_id =  $request->category;

				  $vendor->sub_category_id =  $sub_category;

				  $vendor->sales_tax =  $request->sales_tax;

				  $vendor->excise_tax =  $request->excise_tax;

				  $vendor->city_tax =  $request->city_tax;

				  $vendor->permit_type =  $request->permit_type;

				  $vendor->permit_number =  $request->permit_number;

				  $vendor->permit_expiry =  $permit_expiry;

				  $vendor->description =  $request->description;

				  $vendor->make =  $request->make;

				  $vendor->model =  $request->model;

				  $vendor->color =  $request->color;

				  $vendor->year =  $request->year;

				  $vendor->license_plate =  $request->license_plate;


				  $vendor->lat=$geo->lat;

		          $vendor->lng=$geo->lng;


				  $vendor->password =  Hash::make(trim($request->password));

				  $vendor->vendor_status=  $request->vendor_status;

				  $vendor->type_of_merchant=  $request->type_of_merchant;

				  $vendor->login_status =  1;

				  $vendor->commission_rate = $request->commission_rate;

				  $vendor->delivery_fee = $request->delivery_fee;

				  $vendor->minimum_order_amount = $request->minimum_order_amount;

				  $vendor->unique_id = $uniqueid;

				  $vendor->cash_card = $request->cash_card;

				  $vendor->insta =  $request->insta;

			   $vendor->facebook =  $request->facebook;

			   $vendor->tiktok =  $request->tiktok;

			   $vendor->pinterest =  $request->pinterest;

				  $vendor->save();

				  $vendor_id = $vendor->vendor_id;

				$userData = array(
					'vendor_id' => trim($vendor_id),
					'monday' => trim($mon),
					'tuesday' => trim($tue),
					'wednesday' => trim($wed),
					'thursday' =>  trim($thu),
					'friday' => trim($fri),
					'saturday' => trim($sat),
					'sunday' => trim($sun),
					'open_close_mon' => trim($open_close_mon),
					'open_close_tue' => trim($open_close_tue),
					'open_close_wed'=> trim($open_close_wed),
					'open_close_thu' => trim($open_close_thu),
					'open_close_fri'=> trim($open_close_fri),
					'open_close_sat' => trim($open_close_sat),
					'open_close_sun' => trim($open_close_sun)
				);

				  $insdata = DB::table('vendor_business_time')->insert($userData);

				  Session::flash('message', 'Merchant Inserted Sucessfully!');

			      $email_content_detail = DB::table('email_content')

						->select('*')

						->where('email_id', '=' ,'1')

						->get();


				  $data = array(

								'vendor_name'=>Input::get('name').' '.Input::get('last_name'),

							    'vendor_email'=>Input::get('email'),

					            'email_content'=>$email_content_detail[0]->email_content

								);


				return redirect()->to('/admin/merchant-form/'.$vendor_id);

			//}

		}

		else

		{


		/*** EMAIL ***/

			$vendor_detail  = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();

			$old_status =	$vendor_detail[0]->vendor_status;

			$vmobile =	$vendor_detail[0]->mob_no;

			if( ($old_status==0) && (Input::get('vendor_status')==1))
			{

						$owner_link	 = 'https://www.grambunny.com/merchant';

						$email_content	= 'Welcome to Grambunny, your account is activated';

			$data = array(

						  'vendor_name'=>Input::get('name').' '.Input::get('last_name'),

					      'vendor_email'=>Input::get('email'),

			              'email_content'=>$email_content,

			              'owner_link'=>$owner_link


						  );

				   Mail::send('emails.vendoractive_email', $data, function ($message) use ($data) {

				   $message->from('info@grambunny.com', 'grambunny');

				   $message->to($data['vendor_email']);

   				   $message->subject('Grambunny merchant account is activated');

				   });

               if(!empty($vmobile)){ $this->sendSms($email_content, $vmobile); }

				/******************* SEND EMAIL AFTER PAYMENT  *******************/


			}



		/*** EMAIL end***/

			$commission_rate = '';

			$delivery_fee = '';

			$minimum_order_amount = '';

			if(!empty($request->commission_rate)){

				$commission_rate = $request->commission_rate;

			}

			if(!empty($request->delivery_fee)){

				$delivery_fee = $request->delivery_fee;

			}

			if(!empty($request->minimum_order_amount)){

			 $minimum_order_amount = $request->minimum_order_amount;

			}

			    $password = trim(Input::get('password'));

				  if(!empty($password)){


					DB::table('vendor')

					->where('vendor_id', $vendor_id)

					->update(['name' => Input::get('name'),

						'username' => Input::get('username'),

						'last_name'=>	 Input::get('last_name'),

						'email' => $request->email,

						'mob_no'=>  Input::get('mob_no'),

						'contact_no'=>  Input::get('contact_no'),

						'business_name'=>  $request->business,

						'address'=>  Input::get('address'),

						'mailing_address'=>  $request->mailing_address,

						'pick_up_address' => $request->pick_up_address,

						'city'=>  Input::get('city'),

						'state'=>  Input::get('state'),

					    'password' => Hash::make(trim(Input::get('password'))),

						'zipcode'=>  Input::get('zipcode'),

						'vendor_status'=>  Input::get('vendor_status'),

						'type_of_merchant'=>  Input::get('type_of_merchant'),

						 'login_status'=>  1,

						  'market_area'=>  $request->market_area,

						  'service_radius'=>  $request->service_radius,

						  'driver_license'=>  $request->driver_license,

						  'license_expiry'=>  $license_expiry,

						  'license_front'=>  $license_front,

						  'license_back'=>  $license_back,

						  'ssn'=>  $request->ssn,

						  'dob'=>  $dob,

						  'profile_img1'=>  $profile_img1,

						  'profile_img2'=>  $profile_img2,

						  'profile_img3'=>  $profile_img3,

						  'profile_img4'=>  $profile_img4,

						  'video'=>  $pvideo1,

						  'map_icon'=>  $map_icon,

						  'type'=>  $request->type,

						  'category_id'=>  $request->category,

						  'insta' => $request->insta,

						'facebook' => $request->facebook,

						'tiktok' => $request->tiktok,

						'pinterest' => $request->pinterest,

						  'sub_category_id' =>  $sub_category,

						  'sales_tax'=>  $request->sales_tax,

						  'excise_tax'=>  $request->excise_tax,

						  'city_tax'=>  $request->city_tax,

						  'permit_type'=>  $request->permit_type,

						  'permit_number'=>  $request->permit_number,

						  'permit_expiry'=>  $permit_expiry,

						  'description'=>  $request->description,

						  'make'=>  $request->make,

						  'model'=>  $request->model,

						  'color'=>  $request->color,

						  'year'=>  $request->year,

						  'license_plate'=>  $request->license_plate,

						  'lat'=> $geo->lat,

		                  'lng'=> $geo->lng,

		                  'commission_rate' => $commission_rate,

		                  'delivery_fee' => $delivery_fee,

		                  'minimum_order_amount' => $minimum_order_amount,

		                  'cash_card' => $request->cash_card

					]);


					}else{


						DB::table('vendor')

						->where('vendor_id', $vendor_id)

						->update(['name' => Input::get('name'),

							'username' => Input::get('username'),

							'last_name'=>	 Input::get('last_name'),

							'email' => $request->email,

							'mob_no'=>  Input::get('mob_no'),

							'contact_no'=>  Input::get('contact_no'),

							'business_name'=>  $request->business,

							'address'=>  Input::get('address'),

							'mailing_address'=>  $request->mailing_address,

							'pick_up_address' => $request->pick_up_address,

							'city'=>  Input::get('city'),

							'state'=>  Input::get('state'),

							'zipcode'=>  Input::get('zipcode'),

							'vendor_status'=>  Input::get('vendor_status'),

							'type_of_merchant'=>  Input::get('type_of_merchant'),

							'login_status' => 1,

							  'market_area'=>  $request->market_area,

							  'service_radius'=>  $request->service_radius,

							  'driver_license'=>  $request->driver_license,

							  'license_expiry'=>  $license_expiry,

							  'license_front'=>  $license_front,

							  'license_back'=>  $license_back,

							  'ssn'=>  $request->ssn,

							  'dob'=>  $dob,

							  'profile_img1'=>  $profile_img1,

							  'profile_img2'=>  $profile_img2,

							  'profile_img3'=>  $profile_img3,

							  'profile_img4'=>  $profile_img4,

							  'video'=>  $pvideo1,

							  'map_icon'=>  $map_icon,

							  'type'=>  $request->type,

						      'category_id'=>  $request->category,

						      'sub_category_id' =>  $sub_category,

						      'sales_tax'=>  $request->sales_tax,

						      'excise_tax'=>  $request->excise_tax,

						      'city_tax'=>  $request->city_tax,

							  'permit_type'=>  $request->permit_type,

							  'permit_number'=>  $request->permit_number,

							  'permit_expiry'=>  $permit_expiry,

							  'description'=>  $request->description,

							  'make'=>  $request->make,

							  'model'=>  $request->model,

							  'color'=>  $request->color,

							  'year'=>  $request->year,

							  'license_plate'=>  $request->license_plate,

							  'insta' => $request->insta,

						'facebook' => $request->facebook,

						'tiktok' => $request->tiktok,

						'pinterest' => $request->pinterest,

							  'lat'=> $geo->lat,

		                      'lng'=> $geo->lng,

		                      'commission_rate' => $commission_rate,

		                      'delivery_fee' => $delivery_fee,

		                      'minimum_order_amount' => $minimum_order_amount,

		                      'cash_card' => $request->cash_card

						]);

					}


			$getdata = DB::table('vendor_business_time')->where('vendor_id', $vendor_id)->get();

			if(count($getdata)>0){

			$updatedata = DB::table('vendor_business_time')->where('vendor_id', $vendor_id)->update($userData);

		    }else{

            $insdata = DB::table('vendor_business_time')->insert($userData);

		    }

				Session::flash('message', 'Merchant Information Updated Sucessfully!');

				return redirect()->to('/admin/merchant-form/'.$vendor_id);

		}



	}


    function sendSms($smstext, $to)
    {

        $phone = str_replace("(", "", $to);
        $phoneno = str_replace(") ", "", $phone);
        $phonenos = str_replace("-", "", $phoneno);

        $mobile = '1' . $phonenos;

        // 12013002832 , 19133505810

        $data = array(
            'from' => '12013002832',
            'text' => $smstext,
            'to' => $mobile,
            'api_key' => 'c2aa3522',
            'api_secret' => 'o5NWJ25e5FbszCxH'

        );

        $sendsms = json_encode($data, JSON_FORCE_OBJECT);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.nexmo.com/sms/json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $sendsms,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;

    }

/*Enter OTP */

    function get_browser_name($user_agent)
    {
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
        elseif (strpos($user_agent, 'Edge')) return 'Edge';
        elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
        elseif (strpos($user_agent, 'Safari')) return 'Safari';
        elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

        return 'Other';
    }


	public function dateFormatchange($date)
	{

		 $datearr = explode("/",$date);

		 return "$datearr[2]-$datearr[0]-$datearr[1]";

	}


public function dateFormat($date)
{
    $m = preg_replace('/[^0-9]/', '', $date);
    if (preg_match_all('/\d{2}+/', $m, $r)) {
        $r = reset($r);
        if (count($r) == 4) {
            if ($r[2] <= 12 && $r[3] <= 31) return "$r[0]$r[1]-$r[2]-$r[3]"; // Y-m-d
            if ($r[0] <= 31 && $r[1] != 0 && $r[1] <= 12) return "$r[2]$r[3]-$r[1]-$r[0]"; // d-m-Y
            if ($r[0] <= 12 && $r[1] <= 31) return "$r[2]$r[3]-$r[0]-$r[1]"; // m-d-Y
            if ($r[2] <= 31 && $r[3] <= 12) return "$r[0]$r[1]-$r[3]-$r[2]"; //Y-m-d
        }

        $y = $r[2] >= 0 && $r[2] <= date('y') ? date('y') . $r[2] : (date('y') - 1) . $r[2];
        if ($r[0] <= 31 && $r[1] != 0 && $r[1] <= 12) return "$y-$r[1]-$r[0]"; // d-m-y
    }
}

  public function random_string($length) {
	$password = '';
    $alphabets = range('A','Z');
    $final_array = array_merge($alphabets);
       while($length--) {

      $key = array_rand($final_array);

      $password .= $final_array[$key];

      }
   if (preg_match('/[A-Za-z0-9]/', $password))
    {
     return $password;
    }else{
    return  random_string();
    }

    }


	public function vendor_duplicate_email()

	{

		// Get the value from the form

		$input['email'] = Input::get('email');



		// Must not already exist in the `email` column of `users` table

		$rules = array('email' => 'unique:vendor,email');



		$validator = Validator::make($input, $rules);



		$useremail  = DB::table('users')->where('email', '=' , Input::get('email'))->first();



        if(!empty($useremail)){

        	echo '1';

        }else{



		if ($validator->fails()) {

			echo '1';

		}

		else {

			echo '2';

		}

	  }



	}



	public function merchant_delete($id)

	{
		$orders =DB::table('orders')->where('user_id', '=', $id)->pluck('id');

		DB::table('orders_ps')->whereIn('order_id', $orders)->delete();

		DB::table('temp_coupon_code_apply')->where('vendor_id', '=', $id)->delete();

		DB::table('coupon_code')->where('vendor_id', '=', $id)->delete();

		DB::table('vendor')->where('vendor_id', '=', $id)->delete();

		DB::table('orders')->where('vendor_id', '=', $id)->delete();

	    $product_id =DB::table('product_service')->where('vendor_id', '=', $id)->value('id');

		DB::table('product_service')->where('vendor_id', '=', $id)->delete();

		DB::table('ps_images')->where('ps_id', '=', $product_id)->delete();

		DB::table('cart')->where('ps_id', '=', $product_id)->delete();

		Session::flash('message', 'Merchant Information Deleted Sucessfully!');

		return Redirect('/admin/merchant-list');

	}

	public function merchant_view(Request $request)

	{

		$vendor_id =	$request->id;

		$vendor_detail  = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();

		$rest_list  = DB::table('restaurant')->where('vendor_id', '=' ,$vendor_id)->get();

		$transport_list  = DB::table('transport')->where('transport_vendor', '=' ,$vendor_id)->get();



  		$data_onview = array('vendor_detail' =>$vendor_detail,

							 'rest_list'=>$rest_list,

							 'transport_list'=>$transport_list,

							 'rest_list'=>$rest_list,

							 'id'=>$vendor_id);



		return View('admin.merchant_view')->with($data_onview);



	}



	public function product_service_unit()
	{

		DB::connection()->enableQueryLog();

		$ps_unit = DB::table('product_unit')
		   ->where('type', '!=', 3)

			 ->select('*')

			->orderBy('id', 'asc')

			->get();

    $data_onview = array('ps_unit' =>$ps_unit);

	return View('admin.product_service_unit')->with($data_onview);

	}


	public function event_service_unit()
	{

		DB::connection()->enableQueryLog();

		$ps_unit = DB::table('product_unit')
		  ->where('type', 3)

			 ->select('*')

			->orderBy('id', 'asc')

			->get();

    $data_onview = array('ps_unit' =>$ps_unit);

	return View('admin.event_service_unit')->with($data_onview);

	}

	public function ps_unit_form()
	{
			$id =0;

  		  	$data_onview = array('id'=>$id);

			return View('admin.ps_unit_form')->with($data_onview);
	}

	public function event_unit_form()
	{
			$id =0;

  		  	$data_onview = array('id'=>$id);

			return View('admin.event_unit_form')->with($data_onview);
	}


	public function unit_delete($id)
	{

		DB::table('product_unit')->where('id', '=', $id)->delete();

		Session::flash('message', 'Unit Deleted Sucessfully!');

		return Redirect('/admin/product_service_unit');

	}

	public function event_unit_delete($id)
	{

		DB::table('product_unit')->where('id', '=', $id)->delete();

		Session::flash('message', 'Unit Deleted Sucessfully!');

		return Redirect('/admin/event_service_unit');

	}

	public function ps_unit_action(Request $request)
	{

			DB::table('product_unit')

			->insert(['unit_name' => $request->unit_name,

					'unit_symbol'=>	 $request->unit_symbol,

					'type'=>	 0,

					'status'=>  1,

					'created_at'=> date("Y-m-d h:i:s")

			]);

		   Session::flash('message', 'Product Service Unit Inserted Sucessfully!');

		   return redirect()->to('/admin/product_service_unit');

	}

	public function event_unit_action(Request $request)
	{

			DB::table('product_unit')

			->insert(['unit_name' => $request->unit_name,

					'unit_symbol'=>	 $request->unit_symbol,

					'type'=>	 3,

					'status'=>  1,

					'created_at'=> date("Y-m-d h:i:s")

			]);

		   Session::flash('message', 'Event Service Unit Inserted Sucessfully!');

		   return redirect()->to('/admin/event_service_unit');

	}


	/* Brand add edit delete code */


	public function product_brands()
	{

		DB::connection()->enableQueryLog();

		$product_brand = DB::table('product_brands')

			 ->select('*')

			->orderBy('id', 'asc')

			->get();

    $data_onview = array('product_brand' =>$product_brand);

	return View('admin.product_brands')->with($data_onview);

	}

	public function product_brand_form()
	{
			$id =0;

  		  	$data_onview = array('id'=>$id);

			return View('admin.product_brand_form')->with($data_onview);
	}


	public function product_brand_delete($id)
	{

		DB::table('product_brands')->where('id', '=', $id)->delete();

		Session::flash('message', 'Brand Deleted Sucessfully!');

		return Redirect('/admin/product_brands');

	}

	public function product_brand_action(Request $request)
	{

			DB::table('product_brands')

			->insert(['brand_name' => $request->brand_name,

					'status'=>  1,

					'created_at'=> date("Y-m-d h:i:s")

			]);

		   Session::flash('message', 'Product Brand Inserted Sucessfully!');

		   return redirect()->to('/admin/product_brands');

	}


	/* Type add edit delete code */


	public function product_types()
	{

		DB::connection()->enableQueryLog();

		$product_type = DB::table('product_types')

			 ->select('*')

			->orderBy('id', 'asc')

			->get();

    $data_onview = array('product_type' =>$product_type);

	return View('admin.product_types')->with($data_onview);

	}

	public function product_type_form()
	{
			$id =0;

  		  	$data_onview = array('id'=>$id);

			return View('admin.product_type_form')->with($data_onview);
	}


	public function product_type_delete($id)
	{

		DB::table('product_types')->where('id', '=', $id)->delete();

		Session::flash('message', 'Type Deleted Sucessfully!');

		return Redirect('/admin/product_types');

	}

	public function product_type_action(Request $request)
	{

			DB::table('product_types')

			->insert(['types_name' => $request->types_name,

					'status'=>  1,

					'created_at'=> date("Y-m-d h:i:s")

			]);

		   Session::flash('message', 'Product Type Inserted Sucessfully!');

		   return redirect()->to('/admin/product_types');

	}

	/* End of brand type */


		 /* Product Services Category Start */



 	public function proservices_cate_list(Request $request)

	{



		DB::connection()->enableQueryLog();



		$vendor_list = DB::table('product_service_category')

			 ->select('*')

			->orderBy('id', 'asc')

			->get();





    $data_onview = array('vendor_cate_list' =>$vendor_list);



		return View('admin.product_service_category_list')->with($data_onview);

	}



   	public function proservices_cate_form(Request $request)

	{



       //dd($request->id);



		if(!is_null($request->id))

		{

			$id = $request->id;



			$catagory_detail  = DB::table('product_service_category')->where('id', '=' ,$id)->get();



  		  	$data_onview = array('catagory_detail' =>$catagory_detail,'id'=>$id);



			return View('admin.product_service_category_form')->with($data_onview);



		}

		else

		{

			$id =0;

  		  	$data_onview = array('id'=>$id);

			return View('admin.product_service_category_form')->with($data_onview);

		}





	}



   	public function proservices_cate_delete($id)

		{



        $type = DB::table('product_service_category')

				->where('id', $id)

				->value('type');

        if($type==0){$types = "Product";}else{$types = "Service";}

		DB::table('product_service_category')->where('id', '=', $id)->delete();

		Session::flash('message', $types.' Category Deleted Sucessfully!');

		return Redirect('/admin/product_service_category');



	}



	public function proservices_cate_action(Request $request)

	{

			//return dd($request->all());



		     $category_id = Input::get('category_id');



             $old_image = Input::get('old_image');



             if ($request->category_id==0) {



		        $this->validate($request,[

		        	"cat_image" => "required|image",

		        ],[

		        	"cat_image.image" => "The category image must be an image.",

		        ]);



		      }





                if ($request->hasFile("cat_image")) {

                	$categoryImage=$request->cat_image;



                	$image = Image::make($categoryImage->getRealPath());

					$image->resize(250,200)->save('public/uploads/category/'.$name=str_random(6).time().'.'.$categoryImage->getClientOriginalExtension());



                }else{



                 $name = $old_image;



                }



                $category_name = Input::get('category_name');

                $cat_type = Input::get('cat_type');

                $status = Input::get('status');

			    $type = ucwords(Input::get('cat_type'));

			    $date = date('Y-m-d H:i:s');



			    if($type==0){$types = "Product";}else{$types = "Service";}



		        if($category_id==0)

		        {



				$proservice = DB::table('product_service_category')

				->where('category', $category_name)

				->where('type', $cat_type)

				->get();



				$proservicecount = count($proservice);



               if($proservicecount > 0){



                Session::flash('message', $types.' Category Already Exists!');

				return redirect()->to('/admin/product_service_category');



               }else{



				$Vendorcate = new Proservicescate;

				$Vendorcate->category = $category_name;

				$Vendorcate->type = $cat_type;

				$Vendorcate->cat_image = $name;

				$Vendorcate->status = $status;

				$Vendorcate->created_at = $date;

				$Vendorcate->save();



				Session::flash('message', $types.' Category Inserted Sucessfully!');

				return redirect()->to('/admin/product_service_category');



				}



			}else{



               		DB::table('product_service_category')

					->where('id', $category_id)

					->update(['category' => $category_name,

						'type'=> $cat_type,

						'cat_image'=> $name,

						'status'=> $status

					]);



            	Session::flash('message', $types.' Category Updated Sucessfully!');

				return redirect()->to('/admin/product_service_category');



			}



	}



	/* Product Services Category End */







   /* Product Services Sub Category Start */



 	public function proservices_sub_cate_list(Request $request,Proservicescate $category)

	{

		return View('admin.product_service_sub_category_list',["category" => $category]);

	}



   	public function proservices_sub_cate_form(Request $request)

	{



		if(!is_null($request->id))

		{

			$id = $request->id;

  		  	$data_onview = array('id'=>$id);

			return View('admin.product_service_sub_category_form')->with($data_onview);

		}

		else

		{



		return redirect()->to('/admin/product_service_category');



		}





	}



	public function proservices_sub_cate_edit(Request $request)

	{





			$id = $request->id;



			$catagory_detail  = DB::table('product_service_sub_category')->where('id', '=' ,$id)->get();



  		  	$data_onview = array('catagory_detail' =>$catagory_detail,'id'=>$id);



			return View('admin.product_service_sub_category_edit')->with($data_onview);



	}



   	public function proservices_sub_cate_delete(Request $request)

		{



        $id = $request->id;



		$cate_id = DB::table('product_service_sub_category')

		->where('id', $id)

		->value('category_id');



		DB::table('product_service_sub_category')->where('id', '=', $id)->delete();



		Session::flash('message',' Sub Category Deleted Sucessfully!');

		return redirect()->route('subcategory.view',["category"=>$cate_id]);



	}



	public function proservices_sub_cate_action(Request $request)

	{

			//return dd($request->all());



		     $category_id = Input::get('category_id');



		        $this->validate($request,[

		        	"cat_image" => "required|image",

		        ],[

		        	"cat_image.image" => "The sub category image must be an image.",

		        ]);





                if ($request->hasFile("cat_image")) {

                	$categoryImage=$request->cat_image;



                	$image = Image::make($categoryImage->getRealPath());

					$image->resize(250,200)->save('public/uploads/category/'.$name=str_random(6).time().'.'.$categoryImage->getClientOriginalExtension());



                }



                $category_name = Input::get('category_name');

                $status = Input::get('status');

			    $date = date('Y-m-d H:i:s');





				$proservice = DB::table('product_service_sub_category')

				->where('sub_category', $category_name)

				->where('category_id', $category_id)

				->get();



				$proservicecount = count($proservice);



               if($proservicecount > 0){



                Session::flash('message',' Sub Category Already Exists!');

				return redirect()->route('subcategory.view',["category"=>$category_id]);



               }else{



				$Vendorcate = new Proservices_subcate;

				$Vendorcate->sub_category = $category_name;

				$Vendorcate->category_id = $category_id;

				$Vendorcate->cat_image = $name;

				$Vendorcate->status = $status;

				$Vendorcate->created_at = $date;

				$Vendorcate->save();



				Session::flash('message',' Sub Category Inserted Sucessfully!');

				return redirect()->route('subcategory.view',["category"=>$category_id]);



				}





	}



	public function proservices_sub_cate_edit_action(Request $request)

	{

			//return dd($request->all());



		     $category_id = Input::get('category_id');



             $old_image = Input::get('old_image');



             if ($request->category_id==0) {



		        $this->validate($request,[

		        	"cat_image" => "required|image",

		        ],[

		        	"cat_image.image" => "The category image must be an image.",

		        ]);



		      }





                if ($request->hasFile("cat_image")) {

                	$categoryImage=$request->cat_image;



                	$image = Image::make($categoryImage->getRealPath());

					$image->resize(250,200)->save('public/uploads/category/'.$name=str_random(6).time().'.'.$categoryImage->getClientOriginalExtension());



                }else{



                 $name = $old_image;



                }



                $category_name = Input::get('category_name');

                $status = Input::get('status');

			    $date = date('Y-m-d H:i:s');



               		DB::table('product_service_sub_category')

					->where('id', $category_id)

					->update(['sub_category' => $category_name,

						'cat_image'=> $name,

						'status'=> $status

					]);



				$cate_id = DB::table('product_service_sub_category')

				->where('id', $category_id)

				->value('category_id');



            	Session::flash('message', 'Sub Category Updated Sucessfully!');

				return redirect()->route('subcategory.view',["category"=>$cate_id]);





	}



	/* Product Services Sub Category End */



   public function merchant_logged_status()
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')->orderBy('login_status', 'desc')->get();
		//->where('login_status', 1)

		$data_onview = array('vendor_list' =>$vendor_list);

		return View('admin.merchant_logged_status')->with($data_onview);
	}


	public function merchant_logged_status_change(Request $request)
	{

		DB::connection()->enableQueryLog();

		$vid = $request->id;

		$status = DB::table('vendor')->where('vendor_id', $vid)->value('login_status');

		if($status==1){ $status=0; }else{ $status=1; }

		DB::table('vendor')

				->where('vendor_id', $vid)

				->update([

				'login_status' => $status,

				]);

       // return redirect()->back()->with('message', 'Merchant Logged In Status Change Sucessfully!');

        Session::flash('message', 'Merchant Logged Status Change Successfully!');

		return redirect()->to('/admin/merchant-logged-status');

	}



   public function banner_image()
	{

		DB::connection()->enableQueryLog();

		$bannerimage = DB::table('admin_setting')->where('id', 1)->first();

		$multiple_banner_image = DB::table('multiple_banner_image')->get();

		$data_onview = array('banner_image' =>$bannerimage->banner_image, 'multiple_banner_image' =>$multiple_banner_image);

		return View('admin.banner_image')->with($data_onview);

	}

	public function delete_multi_image(Request $request) {

          $user_id = $request->user_id;

          $img = DB::table('multiple_banner_image')->where('id',$user_id)->first();

          $image_path = public_path()."/uploads/advertisement/".$img->banner_image_multiple;

          unlink($image_path);
          $res = DB::table('multiple_banner_image')->where('id', '=', $user_id)->delete();

        if ($res) {
        	Session::flash('message',' Image has been deleted successfully!');
        } else {
        	Session::flash('message','Some internal issue occured.');

        }

    }


	public function banner_image_change(Request $request)
	{

		DB::connection()->enableQueryLog();

		$bannerimg = '';

		$product_old_img = Input::get('old_image');

		            if ($request->hasFile("banner_image")) {

                	$bannerimage = $request->banner_image;

                	$image =$largeImage= Image::make($bannerimage->getRealPath());

					//$image->resize(200,200)->save(public_path('uploads/advertisement/'.$pname=str_random(6).time().'.'.$bannerimage->getClientOriginalExtension()));

					$bannerimage->move(public_path("uploads/advertisement/"),$bannerimg=str_random(6).time().'.'.$bannerimage->getClientOriginalExtension());

                }else{

                 $bannerimg = $product_old_img;

               }



		DB::table('admin_setting')

				->where('id', 1)

				->update([

				'banner_image' => $bannerimg,

				]);

				 $insertedId = 1;

            if(!empty($insertedId)){

                $filename = [];
               if($request->hasfile('banner_image_multiple'))
            {
            foreach($request->file('banner_image_multiple') as $key => $file)
            {

               $filename = uniqid().'.'.$file->getClientOriginalName();

               $extension = $file->getClientOriginalExtension();

               $file->move("public/uploads/advertisement/",$filename);

               $filepath = public_path('uploads/advertisement/'.$filename);

                DB::table('multiple_banner_image')->insert(['image_id'=>$insertedId, 'banner_image_multiple'=>$filename, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);

               }
             }
           }

       // return redirect()->back()->with('message', 'Merchant Logged In Status Change Sucessfully!');

        Session::flash('message', 'Banner Image Update Successfully!');

		return redirect()->to('/admin/banner-image');

	}

	public function coupon_code()

	{

	//	$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		DB::connection()->enableQueryLog();
		$coupon_list = DB::table('coupon_code')
		->select('*')
		->where('vendor_id', '=' ,0)
		->orderBy('id', 'asc')
		->get();

		$data_onview = array('coupon_list' =>$coupon_list);

		return View('admin.coupon_code_list')->with($data_onview);

	}

	public function coupon_form()

	{

		return View('admin.coupon_code_form');

	}

	 public function add_coupon_code(Request $request)

	{

		$coupon = $request->coupon;
		$amount = $request->amount;
		$mamount = $request->mamount;
		$coupon_status = $request->coupon_status;
		$valid_till = $request->valid_till;
		$name = $request->coupon_name;
		$discount = $request->discount;

		if($request->per_user){ $per_user = $request->per_user; }else{ $per_user = 0; };
		if($request->usage_limit){ $usage_limit = $request->usage_limit;}else{  $usage_limit = 0; }

		$description = $request->description;

		$cdate = date("Y-m-d h:i:s");

		$vendor_id = 0;
		$checkdata = DB::table('coupon_code')
		            ->where('vendor_id', '=', $vendor_id)
		            ->where('coupon', '=', trim($coupon))
		            ->get();

        if(count($checkdata)>0)
        {

           Session::flash('error', 'Coupon code already exists!');
		   return redirect()->to('/merchant/coupon-code');

        }else{

		$CouponData = array(
			'coupon' => trim($coupon),
			'vendor_id' => $vendor_id,
			'amount' => trim($amount),
			'apply_min_amount' => trim($mamount),
			'status' => trim($coupon_status),
			'valid_till' => trim($valid_till),
			'per_user' => trim($per_user),
			'created_at' => $cdate,
			'name' => trim($name),
			'discount' => trim($discount),
			'usage_limit' => trim($usage_limit),
			'description' => trim($description),
			'updated_at' => $cdate

		);

		$affected = DB::table('coupon_code')->insert($CouponData);

		if($affected){

			Session::flash('message', 'Coupon added sucessfully!');

			return redirect()->to('/admin/coupan-list');

		}

	}

}

 public function coupon_delete($id)

	{

		DB::table('coupon_code')->where('id', '=', $id)->delete();

		Session::flash('message', 'Coupon deleted successfully!');

		return redirect()->to('/admin/coupan-list');

	}



	public function coupon_edit_form($id)

	{

			$coupon_detail  = DB::table('coupon_code')->where('id', '=' ,$id)->get();
			$data_onview = array(
				'coupon_detail' =>$coupon_detail,
				'coupon_id'=>$id,
			);

			return View('admin.coupon_editform')->with($data_onview);


	}



   public function update_coupon_detail(Request $request)

	{

		$coupon = $request->coupon;
		$amount = $request->amount;
		$mamount = $request->mamount;
		$coupon_status = $request->coupon_status;
		$valid_till = $request->valid_till;

		$coupon_id = $request->coupon_id;
		$name = $request->coupon_name;
		$discount = $request->discount;

		if($request->per_user){ $per_user = $request->per_user; }else{$per_user = 0; };
		if($request->usage_limit){ $usage_limit = $request->usage_limit;}else{ $usage_limit = 0; };

		$description = $request->description;
        $udate = date("Y-m-d h:i:s");

		$CouponData = array(

			'coupon' => trim($coupon),
			'amount' => trim($amount),
			'apply_min_amount' => trim($mamount),
			'status' => trim($coupon_status),
			'valid_till' => trim($valid_till),
			'per_user' => trim($per_user),
			'name' => trim($name),
			'discount' => trim($discount),
			'usage_limit' => trim($usage_limit),
			'description' => trim($description),
			'updated_at' => $udate

		);

		/* if(($amount >= $mamount) && ($discount!=1)){

           Session::flash('error', 'Coupon amount greater than minimum amount!');
		   return redirect()->to('/merchant/coupon-code');

        }else{ */

		$affected = DB::table('coupon_code')
		->where('id', $coupon_id)
		->update($CouponData);

		if($affected){

			Session::flash('message', 'Coupon details updated sucessfully!');

			return redirect()->to('/admin/coupan-list');

		}
	//}

	}

	public function coupan_merchant_list()
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'asc')

			->get();

        $data_onview = array('vendor_list' =>$vendor_list);

		return View('admin.coupan_merchant_list')->with($data_onview);

	}

	public function merchant_coupan_service_list(Request $request)
	{

		$vendor_id = $request->id;

	   			DB::connection()->enableQueryLog();
		$coupon_list = DB::table('coupon_code')
		->select('*')
		->where('vendor_id', '=' ,$vendor_id)
		->orderBy('id', 'asc')
		->get();

		$data_onview = array('coupon_list' =>$coupon_list);

		return View('admin.coupan_service_list')->with($data_onview);

	}


	/* Admin Event Management code start */
	public function event_management_list()
	{
		DB::connection()->enableQueryLog();
		$ticket_booth_list = DB::table('product_service')->where('vendor_id', '=', 0)->where('addon_type', '=', 0)->where('type', '=', 3)->orderBy('id', 'asc')->get();
		$cat_name = DB::table('product_service_category')->value('category');
		$data_onview = array('rest_list' => $ticket_booth_list, 'cat_name' => $cat_name);
		return View('admin.event_management_list')->with($data_onview);
	}


	public function event_management_add()
	{
		$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();
		$firstcate = DB::table('product_service_category')->first();
		$cateid = $firstcate->id;
		$sub_category_list = DB::table('product_service_sub_category')
			->where('category_id', '=', $cateid)
			->orderBy('id', 'asc')
			->get();

		$product_unit = DB::table('product_unit')
		   ->where('type', '=', '3')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$product_types = DB::table('product_types')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$product_brands = DB::table('product_brands')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$id = 0;

		$data_onview = array(
			'category_list' => $category_list,
			'sub_category_list' => $sub_category_list,
			'product_unit' => $product_unit,
			'product_types' => $product_types,
			'product_brands' => $product_brands,
			'id' => $id
		);
		return View('admin.event_management_add')->with($data_onview);
	}



	public function event_management_action(Request $request)
	{


			$this->validate($request, [
				'ticket_price'  => 'required|numeric|between:0,999999.99',
				'ticket_commission' => 'required|numeric|between:0,999999.99',
				'ticket_fee' => 'required|numeric|between:0,999999.99',
				'ticket_service_fee' => 'required|numeric|between:0,999999.99',
				'event_date'		=> 'required|date',
			   ]);

		$event_id = Input::get('event_id');
		$date = date('Y-m-d H:i:s');
		$eventname = Input::get('event_name');
		$ticket_code_str = $this->random_string(8);
		$slug = Str::slug($eventname . ' ' . $ticket_code_str, '-');
		$vendor_id = '0';


		$product_old_img = Input::get('product_old_img');
		$product_old_glimg = Input::get('product_old_glimg');

		// if ($request->hasFile("product_image")) {
		// 		$productImage = $request->product_image;
		// 		$image = $largeImage = Image::make($productImage->getRealPath());
		// 		$image->resize(200, 200)->save(public_path('uploads/product/' . $pname = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension()));
		// 		$productImage->move(public_path("uploads/product/"), $imageTlarg = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension());

		// 		}else{
					$pname = $product_old_img;

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg;

                   if(empty($glimg)){ $glimg = "dummy.png"; }
			//	}



		if($event_id == 0){
			$ticket_booth = new Productservice;
			$ticket_booth->vendor_id = $vendor_id;

			$ticket_booth->category_id = Input::get('category_id');
			$ticket_booth->sub_category_id = Input::get('sub_category_id');
      $ticket_booth->unit = Input::get('unit');
			$ticket_booth->type = Input::get('ps_type');
			$ticket_booth->addon_type =  Input::get('addon_type');
			$ticket_booth->name = Input::get('event_name');
			$ticket_booth->slug = $slug;
			$ticket_booth->notes_remarks =  Input::get('notes_remarks');
			$ticket_booth->description =  Input::get('event_desc');
			$ticket_booth->venue_name =  Input::get('venue_name');
			//$ticket_booth->image =  'product.jpg';
			$ticket_booth->image =  (!empty($pname)) ? $pname : 'product.jpg';
			$ticket_booth->stock = 1;
			$ticket_booth->venue_address =  Input::get('venue_address');
			$ticket_booth->event_date =  Input::get('event_date');
			$ticket_booth->event_start_time =  Input::get('event_start_time');
			$ticket_booth->event_end_time =  Input::get('event_end_time');
			$ticket_booth->quantity =  Input::get('ticket_inventory');
			$ticket_booth->seating_area =  Input::get('seating_area');
			$ticket_booth->price =  Input::get('ticket_price');
			$ticket_booth->ticket_commission =  Input::get('ticket_commission');
			$ticket_booth->ticket_fee =  Input::get('ticket_fee');
			$ticket_booth->ticket_service_fee =  Input::get('ticket_service_fee');
			$ticket_booth->product_code =  $ticket_code_str;
			$ticket_booth->status =  Input::get('status');
			$ticket_booth->created_at = $date;
			$ticket_booth->save();


			DB::table('ps_images')->insert(
				['ps_id' => $ticket_booth->id,'name' => $glimg, 'thumb' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
			);


			// if ($request->hasFile("product_image_gallery")) {
			// $files = $request->file('product_image_gallery');
			// 		foreach ($files as $file) {
			// 			$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
			// 			$image = $file;
			// 			$image->move('public/uploads/product/', $name);
			// 			DB::table('ps_images')->insert([
			// 				'name' => $name,
			// 				'ps_id' => $ticket_booth->id,
			// 				'thumb' => NULL,
			// 				'created_at' => $date,
			// 				'updated_at' => $date
			// 			]);
			// 		}
			// 	}
			Session::flash('message', 'Item Added Sucessfully!');
			return redirect()->to('/admin/event-management-list')->withMessage('Item Added Sucessfully!');;

		} else {
			/*********  END  START MANAGE TIME IN OTHER TABLE ******/
			// Please make changes this code for merchant
			// echo "<pre>";print('Please make changes for the below code for merchant');die;
			DB::table('product_service')
				->where('id', $event_id)
				->update([

					'category_id' => Input::get('category_id'),
					'sub_category_id' => Input::get('sub_category_id'),

					'name' => Input::get('event_name'),
					'product_code' => $ticket_code_str,
					'notes_remarks' => Input::get('notes_remarks'),
					'description' => Input::get('event_desc'),
					'venue_name' => Input::get('venue_name'),
					'venue_address' => Input::get('venue_address'),
					'unit' => Input::get('unit'),
					//'image' => 'product.jpg',
					'image' => $pname,
					'event_date' =>  Input::get('event_date'),
					'event_start_time' =>  Input::get('event_start_time'),
					'event_end_time' =>  Input::get('event_end_time'),
					'quantity' =>  Input::get('ticket_inventory'),
					'seating_area' =>  Input::get('seating_area'),
					'price' => Input::get('ticket_price'),
					'ticket_commission' => Input::get('ticket_commission'),
					'ticket_fee' =>  Input::get('ticket_fee'),
					'ticket_service_fee' =>  Input::get('ticket_service_fee'),
					'status' => Input::get('status'),
					'updated_at' => $date
				]);

/*
				DB::table('ps_images')->where('ps_id', $event_id)->update([
					'updated_at' => date('Y-m-d H:i:s'),
				]);
*/

/*
		if (!empty($imageTlarg)) {
				DB::table('ps_images')
					->where('ps_id', $event_id)
					->where('thumb', 1)
					->update([
						'name' => $imageTlarg,
					]);
			}

*/

				if ($request->hasFile("product_image_gallery")) {
							$files = $request->file('product_image_gallery');
							foreach ($files as $file) {
								$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
								$image = $file;
								$image->move('public/uploads/product/', $name);
								DB::table('ps_images')->insert([
									'name' => $name,
									'ps_id' => $event_id,
									'thumb' => NULL,
									'created_at' => $date,
									'updated_at' => $date
								]);
							}
				}

			session()->flash('message', 'Item Update Sucessfully!');
			return redirect()->back()->withMessage('Item Update Sucessfully!');
		}
	}


	public function event_management_delete($id)
	{
		DB::table('product_service')->where('id', '=', $id)->delete();

		DB::table('ps_images')->where('ps_id', '=', $id)->delete();

		Session::flash('message', 'Item Delete Sucessfully!');
		return redirect()->to('/admin/event-management-list');
	}


	public function event_management_gallery_delete($gl_id)
	{
		$getpsid = DB::table('ps_images')
			->where('id', '=', $gl_id)
			->value('ps_id');
		DB::table('ps_images')
			->where('id', '=', $gl_id)
			->delete();

		Session::flash('message', 'Image Delete Sucessfully!');
		return redirect()->to('/admin/event-management-edit/' . $getpsid);
	}



	public function event_management_edit($id)
	{
		$ticket_detail  = DB::table('product_service')->where('id', '=', $id)->get();
		$product_detail  = DB::table('product_service')->where('id', '=', $id)->get();

		$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();
		$sub_category_list = DB::table('product_service_sub_category')
			->orderBy('id', 'asc')
			->get();

			$product_unit = DB::table('product_unit')
		   ->where('type', '=', '3')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();


		$gallery_image = DB::table('ps_images')
		->where('ps_id', '=', $id)
		->whereNull('thumb')
		->get();
		$glimage = array();
				foreach ($gallery_image as $key => $value) {
					$glimage[] = $value;
		}

		$data_onview = array(
			'category_list' => $category_list,
			'sub_category_list' => $sub_category_list,
			'product_unit' => $product_unit,
			'ticket_detail' => $ticket_detail,
			'product_detail' => $product_detail,
			'glimage' => $glimage,
			'id' => $id
		);
		return View('admin.event_management_add')->with($data_onview);
	}

	/* Admin Event Management code end */

	/*  Admin view of merchant events start */

	public function event_merchant_list()
		{
			DB::connection()->enableQueryLog();
			$vendor_list = DB::table('vendor')
				->select('*')
				->orderBy('vendor_id', 'asc')
				->get();
			$data_onview = array('vendor_list' => $vendor_list);
			return View('admin.event_merchant_list')->with($data_onview);
		}

	public function event_product_service_list(Request $request)
			{

		$vendor_id = $request->id;
		$rest_list = DB::table('product_service')
			->where('vendor_id', '=', $vendor_id)
			->where('type', '=', 3)
			->orderBy('id', 'asc')
			->get();


		$rest_list_admin = DB::table('admin_merchant_product')

			->leftJoin('product_service', 'admin_merchant_product.product_id', '=', 'product_service.id')

			->where('admin_merchant_product.vendor_id', '=', $vendor_id)

			->select('admin_merchant_product.*', 'product_service.*')

			->orderBy('admin_merchant_product.id', 'asc')

		->get();


		$catid = DB::table('vendor')
			->where('vendor_id', '=', $vendor_id)
			->value('category_id');

		$cat_name = DB::table('product_service_category')
			->where('id', '=', $catid)
			->value('category');

		$data_onview = array('rest_list' => $rest_list, 'rest_list_admin' => $rest_list_admin, 'vendor_id' => $vendor_id, 'cat_name' => $cat_name);



		return View('admin.event_product_service_list')->with($data_onview);
	}



	public function event_product_service_action(Request $request)
	{


		$this->validate($request, [
			'ticket_price'  => 'required|numeric|between:0,999999.99',
			'ticket_commission' => 'required|numeric|between:0,999999.99',
			'ticket_fee' => 'required|numeric|between:0,999999.99',
			'ticket_service_fee' => 'required|numeric|between:0,999999.99',
			'event_date'		=> 'required|date',
		]);

		$event_id = Input::get('event_id');
		$date = date('Y-m-d H:i:s');
		$eventname = Input::get('event_name');
		$ticket_code_str = $this->random_string(8);
		$slug = Str::slug($eventname . ' ' . $ticket_code_str, '-');
		$vendor_id = $request->vendor_id;


		$product_old_img = Input::get('product_old_img');
		$product_old_glimg = Input::get('product_old_glimg');

		// if ($request->hasFile("product_image")) {
		// 		$productImage = $request->product_image;
		// 		$image = $largeImage = Image::make($productImage->getRealPath());
		// 		$image->resize(200, 200)->save(public_path('uploads/product/' . $pname = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension()));

		// 		$productImage->move(public_path("uploads/product/"), $imageTlarg = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension());
		// 	}else{
				$pname = $product_old_img;

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg;

                   if(empty($glimg)){ $glimg = "dummy.png"; }
			//}


		if($event_id == 0){
		$ticket_booth = new Productservice;
		$ticket_booth->vendor_id = $vendor_id;

	$ticket_booth->category_id = (!empty(Input::get('category_id'))) ? Input::get('category_id') : 0 ;
		$ticket_booth->sub_category_id = (!empty(Input::get('sub_category_id'))) ? Input::get('sub_category_id') : 0 ;

		$ticket_booth->type = Input::get('ps_type');
		$ticket_booth->unit = Input::get('unit');
		$ticket_booth->addon_type =  Input::get('addon_type');
		$ticket_booth->name = Input::get('event_name');
		$ticket_booth->slug = $slug;
		$ticket_booth->notes_remarks =  Input::get('notes_remarks');
		$ticket_booth->description =  Input::get('event_desc');
		$ticket_booth->venue_name =  Input::get('venue_name');
		//$ticket_booth->image =  'product.jpg';
		$ticket_booth->image =  (!empty($pname)) ? $pname : 'product.jpg';
		$ticket_booth->stock = 1;
		$ticket_booth->venue_address =  Input::get('venue_address');
		$ticket_booth->event_date =  Input::get('event_date');
		$ticket_booth->event_start_time =  Input::get('event_start_time');
		$ticket_booth->event_end_time =  Input::get('event_end_time');
		$ticket_booth->quantity =  Input::get('ticket_inventory');
		$ticket_booth->seating_area =  Input::get('seating_area');
		$ticket_booth->price =  Input::get('ticket_price');
		$ticket_booth->ticket_commission =  Input::get('ticket_commission');
		$ticket_booth->ticket_fee =  Input::get('ticket_fee');
		$ticket_booth->ticket_service_fee =  Input::get('ticket_service_fee');
		$ticket_booth->product_code =  $ticket_code_str;
		$ticket_booth->status =  Input::get('status');
		$ticket_booth->created_at = $date;
		$ticket_booth->save();


		DB::table('ps_images')->insert(
			['ps_id' => $ticket_booth->id,'name' => $glimg, 'thumb' => NULL , 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
		);

			// if ($request->hasFile("product_image_gallery")) {
			// $files = $request->file('product_image_gallery');
			// 		foreach ($files as $file) {
			// 			$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
			// 			$image = $file;
			// 			$image->move('public/uploads/product/', $name);
			// 			DB::table('ps_images')->insert([
			// 				'name' => $name,
			// 				'ps_id' => $ticket_booth->id,
			// 				'thumb' => NULL,
			// 				'created_at' => $date,
			// 				'updated_at' => $date
			// 			]);
			// 		}
			// 	}

		Session::flash('message', 'Item Added Sucessfully!');
		return redirect()->to('/admin/event-product-service-list/'.$vendor_id)->withMessage('Item Added Sucessfully!');;

		} else {
		/*********  END  START MANAGE TIME IN OTHER TABLE ******/
		// Please make changes this code for merchant
		// echo "<pre>";print('Please make changes for the below code for merchant');die;
		DB::table('product_service')
			->where('id', $event_id)
			->update([

				'category_id' => (!empty(Input::get('category_id'))) ? Input::get('category_id') : 0,
				'sub_category_id' => (!empty(Input::get('sub_category_id'))) ? Input::get('sub_category_id') : 0,

				'name' => Input::get('event_name'),
				'product_code' => $ticket_code_str,
				'notes_remarks' => Input::get('notes_remarks'),
				'description' => Input::get('event_desc'),
				'venue_name' => Input::get('venue_name'),
				'venue_address' => Input::get('venue_address'),
				'unit' => Input::get('unit'),
				//'image' => 'product.jpg',
				'image' => $pname,
				'event_date' =>  Input::get('event_date'),
				'event_start_time' =>  Input::get('event_start_time'),
				'event_end_time' =>  Input::get('event_end_time'),
				'quantity' =>  Input::get('ticket_inventory'),
				'seating_area' =>  Input::get('seating_area'),
				'price' => Input::get('ticket_price'),
				'ticket_commission' => Input::get('ticket_commission'),
				'ticket_fee' =>  Input::get('ticket_fee'),
				'ticket_service_fee' =>  Input::get('ticket_service_fee'),
				'status' => Input::get('status'),
				'updated_at' => $date
			]);


/*
		DB::table('ps_images')->where('ps_id', $event_id)->update([
				'updated_at' => date('Y-m-d H:i:s'),
			]);
*/
			if ($request->hasFile("product_image_gallery")) {
				$files = $request->file('product_image_gallery');
				foreach ($files as $file) {
					$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
					$image = $file;
					$image->move('public/uploads/product/', $name);
					DB::table('ps_images')->insert([
						'name' => $name,
						'ps_id' => $event_id,
						'thumb' => NULL,
						'created_at' => $date,
						'updated_at' => $date
					]);
				}
			}

		session()->flash('message', 'Item Update Sucessfully!');
		return redirect()->back()->withMessage('Item Update Sucessfully!');
		}

	}



	public function event_product_service_form(Request $request)
	{

	$vendor_id = $request->id;

	$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();

	$firstcate = DB::table('product_service_category')->first();

	$cateid = $firstcate->id;

	$sub_category_list = DB::table('product_service_sub_category')

		->where('category_id', '=', $cateid)

		->orderBy('id', 'asc')

		->get();

	$product_unit = DB::table('product_unit')

	   ->where('type', '=', '3')

		->where('status', '=', '1')

		->orderBy('id', 'asc')->get();

	$product_types = DB::table('product_types')

		->where('status', '=', '1')

		->orderBy('id', 'asc')->get();

	$product_brands = DB::table('product_brands')

		->where('status', '=', '1')

		->orderBy('id', 'asc')->get();

	$id = 0;


	$data_onview = array(
		'vendor_id' => $vendor_id,

		'category_list' => $category_list,

		'sub_category_list' => $sub_category_list,

		'product_unit' => $product_unit,

		'product_types' => $product_types,

		'product_brands' => $product_brands,

		'id' => $id
	);

	return View('admin.event_merchant_ps_form')->with($data_onview);
		}


	public function event_product_service_edit($id)
	{

		$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();
		$sub_category_list = DB::table('product_service_sub_category')

			->orderBy('id', 'asc')

			->get();

		$vendor_id = DB::table('product_service')
			->where('id', '=', $id)
			->value('vendor_id');

		$ticket_detail  = DB::table('product_service')->where('id', '=', $id)->get();

		$product_unit = DB::table('product_unit')

	   ->where('type', '=', '3')

		->where('status', '=', '1')

		->orderBy('id', 'asc')->get();

		$gallery_image = DB::table('ps_images')
		->where('ps_id', '=', $id)
		->whereNull('thumb')
		->get();
		$glimage = array();
		foreach ($gallery_image as $key => $value) {
			$glimage[] = $value;
		}

			$data_onview = array(
				'ticket_detail' => $ticket_detail,
				'category_list' => $category_list,
				'sub_category_list' => $sub_category_list,
                'product_unit' => $product_unit,
				'glimage' => $glimage,
				'id' => $id,
				'vendor_id' => $vendor_id,
			);
		return View('admin.event_merchant_ps_form')->with($data_onview);
	}

	public function event_product_service_delete($id)
	{
		$vendor_id = 	DB::table('product_service')
			->where('id', '=', $id)
			->value('vendor_id');
		DB::table('product_service')->where('id', '=', $id)->delete();
		DB::table('ps_images')->where('ps_id', '=', $id)->delete();
		//DB::table('orders_ps')->where('ps_id', '=', $id)->delete();
		Session::flash('message', 'Item Delete Sucessfully!');
		return redirect()->to('/admin/event-product-service-list/' . $vendor_id);
	}


	public function event_product_service_gallery_delete($gl_id)
	{  		$getpsid = DB::table('ps_images')
			->where('id', '=', $gl_id)
			->value('ps_id');
		DB::table('ps_images')
			->where('id', '=', $gl_id)
			->delete();

		Session::flash('message', 'Image Delete Sucessfully!');
		return redirect()->to('/admin/event-product-service-edit/' . $getpsid);
	}

	public function crop_event_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('event_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

        DB::table('product_service')
		//->where('vendor_id', $vendor_id)
		->where('id', $product_id)
		->update(['image' => $product_img]);

		DB::table('ps_images')
		->where('ps_id', $product_id)
		->where('thumb', 1)
		->update(['name' => $product_img]);

		return response()->json(['status'=>$product_img]);

	}


	public function gallery_event_crop_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('event_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

		DB::table('ps_images')
		->insert([
			'ps_id' => $product_id,
			'name' => $product_img,
			'created_at' => date("Y-m-d h:i:s"),
			'updated_at'=> date("Y-m-d h:i:s")
		]);

    return response()->json(['status'=>$product_img]);

	}

	public function event_cate_list(Request $request)
	{

		DB::connection()->enableQueryLog();

		$vendor_list = DB::table('product_service_category')

			->select('*')

			->orderBy('id', 'asc')

			->get();

		$data_onview = array('vendor_cate_list' => $vendor_list);

		return View('admin.event_category_list')->with($data_onview);
	}

	public function event_cate_form(Request $request)
	{

		//dd($request->id);

		if (!is_null($request->id)) {

			$id = $request->id;

			$catagory_detail  = DB::table('product_service_category')->where('id', '=', $id)->get();

			$data_onview = array('catagory_detail' => $catagory_detail, 'id' => $id);

			return View('admin.event_category_form')->with($data_onview);

		} else {

			$id = 0;

			$data_onview = array('id' => $id);

			return View('admin.event_category_form')->with($data_onview);
		}
	}

	/*  Admin view of merchant events end */



	public function event_cate_action(Request $request)
	{

		//return dd($request->all());

		$category_id = Input::get('category_id');

		$old_image = Input::get('old_image');

		/* if ($request->category_id == 0) {

			$this->validate($request, [

				"cat_image" => "required|image",

			], [

				"cat_image.image" => "The category image must be an image.",

			]);
		} */

        $name = '';

		if($request->hasFile("cat_image")) {

			$categoryImage = $request->cat_image;

			$image = Image::make($categoryImage->getRealPath());

			$image->resize(250, 200)->save('public/uploads/category/' . $name = str_random(6) . time() . '.' . $categoryImage->getClientOriginalExtension());
		} else {

			$name = $old_image;
		}



		$category_name = Input::get('category_name');

		$cat_type = Input::get('cat_type');

		$status = Input::get('status');

		$type = ucwords(Input::get('cat_type'));

		$date = date('Y-m-d H:i:s');

		$types = "Event";

		if ($category_id == 0) {

			$proservice = DB::table('product_service_category')

				->where('category', $category_name)

				->where('type', $cat_type)

				->get();

			$proservicecount = count($proservice);

			if ($proservicecount > 0) {

				Session::flash('message', $types . ' Category Already Exists!');

				return redirect()->to('/admin/product_event_category');
			} else {



				$Vendorcate = new Proservicescate;

				$Vendorcate->category = $category_name;

				$Vendorcate->type = $cat_type;

				$Vendorcate->cat_image = $name;

				$Vendorcate->status = $status;

				$Vendorcate->created_at = $date;

				$Vendorcate->save();

				Session::flash('message', $types . ' Category Inserted Sucessfully!');

				return redirect()->to('/admin/product_event_category');
			}

		} else {

			DB::table('product_service_category')

				->where('id', $category_id)

				->update([
					'category' => $category_name,

					'type' => $cat_type,

					'cat_image' => $name,

					'status' => $status

				]);

			Session::flash('message', $types . ' Category Updated Sucessfully!');

			return redirect()->to('/admin/product_event_category');
		}
	}


	public function event_cate_delete($id)
	{

		$type = DB::table('product_service_category')

			->where('id', $id)

			->value('type');

		if ($type == 0) {
			$types = "Product";
		} else {
			$types = "Service";
		}

		DB::table('product_service_category')->where('id', '=', $id)->delete();

		Session::flash('message', $types . ' Category Deleted Sucessfully!');

		return Redirect('/admin/product_event_category');
	}


	public function events_sub_cate_list(Request $request, Proservicescate $category)
	{

		return View('admin.events_sub_cate_list', ["category" => $category]);
	}


	public function events_sub_cate_form(Request $request)

	{

		if (!is_null($request->id)) {

			$id = $request->id;

			$data_onview = array('id' => $id);

			return View('admin.events_sub_cate_form')->with($data_onview);
		} else {



			return redirect()->to('/admin/product_event_category');
		}
	}

	public function events_sub_cate_edit(Request $request)

	{

		$id = $request->id;

		$catagory_detail  = DB::table('product_service_sub_category')->where('id', '=', $id)->get();

		$data_onview = array('catagory_detail' => $catagory_detail, 'id' => $id);

		return View('admin.events_sub_cate_edit')->with($data_onview);
	}


	public function events_sub_cate_action(Request $request)
	{

		//return dd($request->all());

		$category_id = Input::get('category_id');

		/* $this->validate($request, [

			"cat_image" => "required|image",

		], [

			"cat_image.image" => "The sub category image must be an image.",

		]); */

        $name = '';

		if ($request->hasFile("cat_image")) {

			$categoryImage = $request->cat_image;

			$image = Image::make($categoryImage->getRealPath());

			$image->resize(250, 200)->save('public/uploads/category/' . $name = str_random(6) . time() . '.' . $categoryImage->getClientOriginalExtension());
		}

		$category_name = Input::get('category_name');

		$status = Input::get('status');

		$date = date('Y-m-d H:i:s');

		$proservice = DB::table('product_service_sub_category')

			->where('sub_category', $category_name)

			->where('category_id', $category_id)

			->get();


		$proservicecount = count($proservice);

		if ($proservicecount > 0) {

			Session::flash('message', ' Sub Category Already Exists!');

			return redirect()->route('event_subcategory.view', ["category" => $category_id]);

		} else {

			$Vendorcate = new Proservices_subcate;

			$Vendorcate->sub_category = $category_name;

			$Vendorcate->category_id = $category_id;

			$Vendorcate->cat_image = $name;

			$Vendorcate->status = $status;

			$Vendorcate->created_at = $date;

			$Vendorcate->save();

			Session::flash('message', ' Sub Category Inserted Sucessfully!');

			return redirect()->route('event_subcategory.view', ["category" => $category_id]);
		}
	}


	public function events_sub_cate_edit_action(Request $request)
	{

		//return dd($request->all());

		$category_id = Input::get('category_id');

		$old_image = Input::get('old_image');

		/* if ($request->category_id == 0) {

			$this->validate($request, [

				"cat_image" => "required|image",

			], [

				"cat_image.image" => "The category image must be an image.",

			]);
		} */

        $name = '';

		if ($request->hasFile("cat_image")) {

			$categoryImage = $request->cat_image;

			$image = Image::make($categoryImage->getRealPath());

			$image->resize(250, 200)->save('public/uploads/category/' . $name = str_random(6) . time() . '.' . $categoryImage->getClientOriginalExtension());

		} else {

			$name = $old_image;
		}

		$category_name = Input::get('category_name');

		$status = Input::get('status');

		$date = date('Y-m-d H:i:s');

		DB::table('product_service_sub_category')

			->where('id', $category_id)

			->update([
				'sub_category' => $category_name,

				'cat_image' => $name,

				'status' => $status

			]);



		$cate_id = DB::table('product_service_sub_category')

			->where('id', $category_id)

			->value('category_id');

		Session::flash('message', 'Sub Category Updated Sucessfully!');

		return redirect()->route('event_subcategory.view', ["category" => $cate_id]);
	}

   public function events_sub_cate_delete(Request $request)
	{

		$id = $request->id;

		$cate_id = DB::table('product_service_sub_category')

			->where('id', $id)

			->value('category_id');

		DB::table('product_service_sub_category')->where('id', '=', $id)->delete();

		Session::flash('message', ' Sub Category Deleted Sucessfully!');

		return redirect()->route('event_subcategory.view', ["category" => $cate_id]);
	}


}

