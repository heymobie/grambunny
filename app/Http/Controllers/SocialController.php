<?php
 
namespace App\Http\Controllers;
 
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Socialite;
use Validator,Redirect,Response,File;
use Webpatser\Uuid\Uuid;
class SocialController extends Controller
{
public function redirect($provider)
{
    return Socialite::driver($provider)->redirect();
}
 
public function callback($provider)
{
           
    $getInfo = Socialite::driver($provider)->stateless()->user();
    if ($provider=="google") {
        $user = $this->google($getInfo,$provider); 
    }
    if ($provider=="facebook") {
        $user = $this->facebook($getInfo,$provider); 
    }
    Auth::guard('user')->login($user);
    return redirect()->route('homepage');
 
}
    function google($getInfo,$provider){
        $user=User::where('oauth_id', $getInfo->id)->orWhere("email",$getInfo->email)->first(); 
        if (!$user) {
            $user = User::create([
                'name'   => $getInfo->user["given_name"],
                'lname' => $getInfo->user["family_name"],
                "token"       => Uuid::generate()->string,
                'email'    => $getInfo->email,
                "password"  => Hash::make("asdfmnbv;"),
                'oauth_provider' => $provider,
                'oauth_id' => $getInfo->id,
                "email_verified_at" => Carbon::now()
            ]);
            return $user;
        }
        else{
            if (is_null($user->oauth_id)) {
                $user->oauth_id=$getInfo->id;
                $user->oauth_provider=$provider;
                $user->update();
            }
            return $user;
        }
    }

    public function facebook($getInfo,$provider)
    {
        $user=User::where("oauth_id",$getInfo->id)->orWhere("email",$getInfo->email)->first();
        if (!$user) {
            $fullname=explode(" ",$getInfo->name);
            $user = User::create([
                'name'   => $fullname[0],
                'lname' => $fullname[1],
                "token"       => Uuid::generate()->string,
                'email'    => $getInfo->email,
                "password"  => Hash::make("asdfmnbv;"),
                'oauth_provider' => $provider,
                'oauth_id' => $getInfo->id,
                "email_verified_at" => Carbon::now()
            ]);
            return $user;
        }
        else{
            if (is_null($user->oauth_id)) {
                $user->oauth_provider=$provider;
                $user->oauth_id=$getInfo->id;
                $user->update();
            }
            return $user;
        }
    }
}