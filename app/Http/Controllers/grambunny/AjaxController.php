<?php

namespace App\Http\Controllers\grambunny;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;



class AjaxController extends Controller{

    

    public function states(Request $request)

    {

        $states=DB::table("states")->get();

        return response()->json(["states" => $states]);

    }



    public function getCategories(Request $request)

    {

        $categories=DB::table("product_service_category")->where("status",1)->where("type",$request->type)->get();


        return response()->json(["categories" => $categories]);


    }



    public function getSubCategories(Request $request){

        $subCategories=DB::table("product_service_sub_category")->where("status",1)->where("category_id",$request->category)->get();

        return response()->json(["subCategories" => $subCategories]);



    }

    public function getMerchants(Request $request)

    {
        

        // $merchants=DB::table("vendor")->where("vendor_status",1)->orWhere("username",'like', '%'.$_GET['address'].'%')->orWhere("name",'like', '%'.$_GET['address'].'%')->orWhere("business_name",'like', '%'.$_GET['address'].'%')->first();


        $merchants=DB::table("vendor")->where("username",'like', '%'.$_GET['address'].'%')->orWhere("business_name",'like', '%'.$_GET['address'].'%')->where("vendor_status",1)->first();

        //print_r($merchants);die;

        return Response()->json($merchants);

    }


    public function chatsystem(Request $request)

    {



        $chatdata = $request->all();



        $date = date('Y-m-d H:i:s');



        $cmessage = $chatdata['chat_data']['cmessage'];

        $cuserid = $chatdata['chat_data']['cuserid']; 

        $cmerchantid = $chatdata['chat_data']['cmerchantid'];  



        DB::table('chat_system')->insert([

        'merchant_id' => $cmerchantid,

        'customer_id' => $cuserid,

        'message' => $cmessage,

        'chat_type' => 'sender',

        'read_status' => 0,

        'created_at' => $date    



        ]);



        $getchat = DB::table('chat_system')       

                    ->where('merchant_id', '=' ,$cmerchantid)

                    ->where('customer_id', '=' ,$cuserid)

                    ->orderBy('chat_id', 'asc')

                    ->get();



        $data_onview = array('chatmsg'=>$getchat); 



        return View('ajax.chat')->with($data_onview);



    }



    public function chathistory(Request $request)

    {



        $chatdata = $request->all();



        $cuserid = $chatdata['chat_data']['cuserid']; 

        $cmerchantid = $chatdata['chat_data']['cmerchantid'];  



        $getchat = DB::table('chat_system')       

                    ->where('merchant_id', '=' ,$cmerchantid)

                    ->where('customer_id', '=' ,$cuserid)

                    ->orderBy('chat_id', 'asc')

                    ->get();



        $data_onview = array('chatmsg'=>$getchat); 



        return View('ajax.chat')->with($data_onview);



    }



    public function chatmlist(Request $request)

    {



        $chatdata = $request->all();



        $cuserid = $chatdata['chat_data']['cuserid']; 



        $chat_merchant = DB::table('orders')

                ->where("user_id",$cuserid)

                ->whereNotIn("status",array('2', '4'))

                ->orderBy('orders.id', 'DESC')

                ->groupBy('orders.vendor_id')

                ->get();     



        $data_onview = array('chatmerchant'=>$chat_merchant); 



        return View('ajax.chat_mlist')->with($data_onview);



    }





}