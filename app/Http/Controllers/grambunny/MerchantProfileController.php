<?php

namespace App\Http\Controllers\grambunny;

use App\Vendor;
use App\Productservice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\MerchantRating;
use Illuminate\Support\Facades\Validator;
use App\Proservicescate;

class MerchantProfileController extends Controller
{

    public function profile(Request $request){


        $categories=Proservicescate::where("status",1)->get();

        $merchant=Vendor::where("username",$request->username)->where("vendor_status",1)->first();

        if(!is_null($merchant)){

                if(!session()->has($merchant->username."_view")){
                $merchant->views= $merchant->views+1;
                $merchant->update();
                session([$merchant->username."_view"=> 'true']);
            }

            $items=Productservice::where("vendor_id",$merchant->vendor_id)->where("status",1)->where("stock",1)->where("quantity",">",0)->get();

            //dd($items);

            //SELECT COUNT(ps_id) as top_ps, ps_id FROM orders where vendor_id=103 GROUP BY ps_id ORDER BY top_ps DESC
            $bestSellingItems = DB::table('orders')
                 ->select('ps_id', DB::raw('count(ps_id) as top_ps'))
                 ->where("vendor_id",$merchant->vendor_id)
                 ->groupBy('ps_id')
                 ->orderBy("top_ps","DESC")->limit(3)
                 ->get()->pluck("ps_id");

            $itemsIDs = $bestSellingItems->implode(",");

            $itemsbs=array();

            if(!empty($itemsIDs)){

             $itemsbs=Productservice::whereIn("id",$bestSellingItems)->orderByRaw("FIELD(id, $itemsIDs)")->get();

            }
            $complate = DB::table('orders')
                 ->where("vendor_id",$merchant->vendor_id)
                 ->where("status",4)
                 ->get();

               $corder = count($complate) ;

            $ratingReviewData = MerchantRating::select('profile_image', 'name', 'lname', 'rating', 'review', 'merchant_ratings.created_at')->join('users as u', 'merchant_ratings.user_id', '=', 'u.id')->Where('merchant_id', $merchant->vendor_id)->Where('merchant_ratings.status', 1)->get();


            $social_links = DB::table('merchant_social_links')->Where('merchant_id', $merchant->vendor_id)->first();

            $hoperation = DB::table('vendor_business_time')->Where('vendor_id', $merchant->vendor_id)->first();

            $page = (object)array('seo_title' => $merchant->fullname,'seo_keyword' => '','seo_description' => '','seo_author' => '', );

            // print_r($merchant);

            return view("seller-details",["merchant" => $merchant,"items" => $items,"itemsbs" => $itemsbs,"corder" => $corder, "ratingReviewData" => $ratingReviewData, 'social_links' => $social_links, 'hoperation' => $hoperation,"categories"  => $categories, 'page_data' =>$page]);
        }else{

           return view("mercahnt-inactive");

        }

    }

    public function services(Request $request)
    {

        $merchant=Vendor::where("username",$request->username)->where("login_status",1)->first();

        if(!is_null($merchant)){

            if(!session()->has($merchant->username."_view")){
                $merchant->views= $merchant->views+1;
                $merchant->update();
                session([$merchant->username."_view"=> 'true']);
            }
            $items=Productservice::where("vendor_id",$merchant->vendor_id)->where("status",1)->paginate(24);

            //echo "<pre>";print_r($items);die;
            return view("mercahnt-services",["merchant" => $merchant,"items" => $items]);
        }else{

           return view("mercahnt-inactive");

        }

    }

     public function saveMerchantReview(Request $request)
    {
        $validator=Validator::make($request->all(),[
            "rating" => ['required','numeric'],
            "comment" => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(["ok" => 0,"message" => "Please write your review"]);
        }

        $user_id = auth()->guard('user')->user()->id;
        $merchant_review_exist = MerchantRating::where('user_id', $user_id)->where('merchant_id', $request->merchant_id)->count();

        if($merchant_review_exist == 0){
            $merchantRating = new MerchantRating;
            $merchantRating->user_id = $user_id;
            $merchantRating->rating = $request->rating;
            $merchantRating->status = 0;
            $merchantRating->review = $request->comment;
            $merchantRating->merchant_id = $request->merchant_id;
            $merchantRating->save();

            // $no_of_times_rated = MerchantRating::where('merchant_id', $request->merchant_id)->count();
            // $rating_sum = MerchantRating::where('merchant_id', $request->merchant_id)->sum('rating');
            // $avg_rating = round($rating_sum/$no_of_times_rated);
            // Vendor::where('vendor_id', $request->merchant_id)->update( [ 'avg_rating' => $avg_rating,  'rating_count' => DB::raw('rating_count + 1') ] );

            return response()->json(["ok" => 1,"message" => "Thank you for your review and rating."]);
        }else{
            return response()->json(["ok" => 0,"message" => "Rating and review already given."]);
        }
    }

    public function saveCustomerReview(Request $request)
    {
        $validator=Validator::make($request->all(),[
            "rating" => ['required','numeric'],
            "comment" => ['required'],
        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0,"message" => "Please write your review & rating"]);
        }

        $vendor_id = $request->merchant_id;

        $customer_review_exist = DB::table('user_ratings')->where('user_id', $request->user_id)->where('merchant_id', $vendor_id)->where('order_id', $request->order_id)->count();

        if($customer_review_exist == 0){

                   $userData = array(

                    'merchant_id' => $vendor_id,

                    'user_id' => $request->user_id,

                    'status' => 0,

                    'rating' => $request->rating,

                    'review' => $request->comment,

                    'order_id'=> $request->order_id

                );


            DB::table('user_ratings')->insert($userData);

            return response()->json(["ok" => 1,"message" => "Thank you for your review and rating."]);

        }else{

            return response()->json(["ok" => 0,"message" => "Rating and review already given."]);

        }
    }
}
