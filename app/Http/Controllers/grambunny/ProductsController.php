<?php

namespace App\Http\Controllers\grambunny;
use App\Vendor;
use App\Productservice;
use App\Proservicescate;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Rating;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    public $distance=50;
    public $paginationValue=10;

    public function nearbyMerchants(Request $request){

       $request->all();

       Session::put('nearlat', $request->lat);
       Session::put('nearlng', $request->lng);

       //Session::flash('deep', '00000');

        $distance=50;
        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $request->lng . ') ) + sin( radians(' . $request->lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));
        $vendors= Arr::pluck($results,"vendor_id");
        //$vendorsDetials=Vendor::whereIn("vendor_id",$vendors)->where("login_status",1)->get();
        $vendorsDetials=Vendor::whereIn("vendor_id",$vendors)->where("vendor_status",1)->get();
        $data=$vendorsDetials->map(function ($vendor) use ($results){
            foreach($results as $result){
                if ($result->vendor_id==$vendor->vendor_id) {
                    $vendor["distance"]=round($result->distance,1)." Miles";
                }
            }
            return $vendor;
        });
        return $data;
    }

    public function nearbyMerchantstest(){
        //$request->all();
        $distance=50;
        $lat = "22.7533";
        $lng = "75.8937";

        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));
        $vendors= Arr::pluck($results,"vendor_id");
        $vendorsDetials=Vendor::whereIn("vendor_id",$vendors)->get();
        $data=$vendorsDetials->map(function ($vendor) use ($results){
            foreach($results as $result){
                if ($result->vendor_id==$vendor->vendor_id) {
                    $vendor["distance"]=round($result->distance,1)." Miles";
                }
            }
            print_r($vendor);
        });
        print_r($data);
    }

    public function getProducts(Request $request)
    {

        if (!empty(Session::get('search_vid'))) {

            if($request->has("category") and $request->category!=""){


                $nearbyItems= Productservice::where("vendor_id",Session::get('search_vid'))->where("status",1)->where("sub_category_id",$request->category)->latest()->paginate($this->paginationValue);

                $nearbyMerchantOnMap= Productservice::where("vendor_id",Session::get('search_vid'))->where("status",1)->where("sub_category_id",$request->category)->latest()->get()->pluck("vendor_id")->toArray();

            }else{

                $verdor= Vendor::where("vendor_id",Session::get('search_vid'))->where("login_status",1)->first();

                $username = $verdor['username'];
                $business_name = $verdor['business_name'];
                $uname =explode("-",$username);
                //print_r($uname[0]);die;
                 $resultsdata= DB::table("vendor")->where("username",'like', '%'.$uname[0].'%')->orWhere("business_name",'like', '%'.$business_name.'%')->where("vendor_status",1)->get();

                 //print_r($resultsdata);die;
                 $vendors= Arr::pluck($resultsdata,"vendor_id");

                // print_r($vendors);
                //$nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("login_status",1)->latest()->paginate($this->paginationValue);

                $nearbyItems = Productservice::where("vendor_id",Session::get('search_vid'))->where("status",1)->latest()->paginate($this->paginationValue);

                //$nearbyMerchantOnMap = Productservice::whereIn("vendor_id",$vendors)->where("status",1)->latest()->get()->pluck("vendor_id")->toArray();


                $nearbyMerchantOnMap = $vendors;
                 //$nearbyMerchantOnMap = Session::get('search_vid');
            }

            //print_r($nearbyMerchantOnMap);die;
            $verdorsOnMap= Vendor::whereIn("vendor_id",$nearbyMerchantOnMap)->where("login_status",1)->get();

            //print_r($verdorsOnMap);

        }else{


            $distance=50;
            $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $request->lng . ') ) + sin( radians(' . $request->lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));
            //echo "<pre>"; print_r($results);die;
            $vendors= Arr::pluck($results,"vendor_id");

            if($request->has("category") and $request->category!=""){

                //$nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("login_status",1)->where("sub_category_id",$request->category)->latest()->paginate($this->paginationValue);

                $nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->latest()->paginate($this->paginationValue);

                $nearbyMerchantOnMap= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->latest()->get()->pluck("vendor_id")->toArray();

            }else{


                //$nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("login_status",1)->latest()->paginate($this->paginationValue);

                $nearbyItems = Productservice::whereIn("vendor_id",$vendors)->where("status",1)->latest()->paginate($this->paginationValue);

                //$nearbyMerchantOnMap = Productservice::whereIn("vendor_id",$vendors)->where("status",1)->latest()->get()->pluck("vendor_id")->toArray();

                 $nearbyMerchantOnMap = $vendors;
            }


            $verdorsOnMap = Vendor::whereIn("vendor_id",$nearbyMerchantOnMap)->where("login_status",1)->get();



            $data = $nearbyItems->getCollection()->transform(function ($item) use ($results){
                foreach($results as $result){
                    if ($result->vendor_id==$item->vendor_id) {
                        $item["distance"]=round($result->distance,1)." Miles";
                    }
                }
                return $item;
            });

        }
        return response()->json(["items" => $nearbyItems,"merchants" => $verdorsOnMap],200);
    }

    public function products(Request $request){
        Session::put('search_vid', $request->vendor);
        //print_r($request->vendor);die;
        $validator=Validator::make($request->all(),[
            "lat" => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            "lng" => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],

        ]);

        if ($validator->fails()) {
            return abort(404);
        }


        if($request->vendor){

            //$verdorslist= Vendor::where("vendor_id",$request->vendor)->where("login_status",1)->get();
             $verdor= Vendor::where("vendor_id",$request->vendor)->where("login_status",1)->first();

                $username = $verdor['username'];
                $business_name = $verdor['business_name'];

                $uname =explode("-",$username);
                $verdorslist= DB::table("vendor")->where("username",'like', '%'.$uname[0].'%')->orWhere("business_name",'like', '%'.$business_name.'%')->where("vendor_status",1)->get();


        }else{
            $distance=50;
            $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $request->lng . ') ) + sin( radians(' . $request->lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . '  ORDER BY distance'));
            $vendors= Arr::pluck($results,"vendor_id");

            $verdorslist= Vendor::whereIn("vendor_id",$vendors)->where("login_status",1)->get();
        }
        //$verdorslist= Vendor::find($vendors);

        //echo "<pre>";print_r($verdorslist);die();

        return view("product-services-listing",["lat" => $request->lat,"lng" => $request->lng,"category" => $request->category,"verdorslist" => $verdorslist]);

    }

    public function getCategoryType(Request $request)
    {
        $cat = Proservicescate::select('type')->join('product_service_sub_category as pssc', 'pssc.category_id', '=', 'product_service_category.id')->Where('pssc.id', $request->sub_category_id)->first();
        return response()->json(["type"=>$cat['type'] ],200);
    }

    public function filteredPs(Request $request)
    {
        $categories=Proservicescate::where("status",1)->where("type",$request->pstype)->get();
        return response()->json(["categories"=>$categories],200);
    }

    public function filterInit()
    {
        $categories=Proservicescate::where("status",1)->get();
        return response()->json(["categories"=>$categories],200);
    }

    public function filterResults(Request $request)
    {


        //echo "<pre>"; print_r($_GET);die;
        $distance=$request->has("distance") && $request->distance!=""  ? $request->distance : $this->distance;
        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $request->lng . ') ) + sin( radians(' . $request->lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));
        $vendors= Arr::pluck($results,"vendor_id");

        if($request->has("price") AND $request->price>0){
            if($request->has("category") and $request->category!=""){

                $nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("price","<=",$request->price)->where("avg_rating","=",$request->review)->latest()->paginate($this->paginationValue);
                // ->where("login_status",1)
                $nearbyItemsMap= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("price","<=",$request->price)->where("avg_rating","=",$request->review)->groupBy("vendor_id")->latest()->get();
                // ->where("login_status",1)
            }else{

                $nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("price","<=",$request->price)->where("avg_rating","=",$request->review)->latest()->paginate($this->paginationValue);
                // ->where("login_status",1)
                $nearbyItemsMap= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("price","<=",$request->price)->where("avg_rating","=",$request->review)->groupBy("vendor_id")->latest()->get();
                // ->where("login_status",1)
            }
        }else{

            if($request->has("category") and $request->category!=""){

                $nearbyItemsMap= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("avg_rating","=",$request->review)->groupBy("vendor_id")->latest()->get();
                // ->where("login_status",1)
                $nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("sub_category_id",$request->category)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("avg_rating","=",$request->review)->latest()->paginate($this->paginationValue);
                // ->where("login_status",1)

            }else{

                $nearbyItemsMap= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("avg_rating","=",$request->review)->groupBy("vendor_id")->latest()->get();
                // ->where("login_status",1)
                $nearbyItems= Productservice::whereIn("vendor_id",$vendors)->where("status",1)->where("name","LIKE","%$request->search%")->where("type",$request->pstype)->where("avg_rating","=",$request->review)->latest()->paginate($this->paginationValue);
                // ->where("login_status",1)
            }
        }


        // $vendorsDetails= $nearbyItemsMap->pluck("vendor_id")->toArray();
        // $verdorsOnMap= Vendor::find($vendorsDetails);

        $verdorsOnMap= Vendor::whereIn("vendor_id",$vendors)->where("login_status",1)->get();


        $nearbyItems->getCollection()->transform(function ($item) use ($results){
            foreach($results as $result){
                if ($result->vendor_id==$item->vendor_id) {
                    $item["distance"]=round($result->distance,1)." Miles";
                }
            }
            return $item;
        });
        return response()->json(["items" => $nearbyItems,"merchants" => $verdorsOnMap],200);
    }


    public function productDetails(Request $request)
    {

    $vendor_id = 1;

    $cartqtyold = '';

    $user_id = csrf_token();

    if (!empty( auth()->guard('user')->user() ) ) {

        $user_id = auth()->guard('user')->user()->id;

    }

   $storename = Vendor::where("vendor_id",$vendor_id)->value('username');


    $item=Productservice::where("slug",$request->slug)->where("vendor_id",$vendor_id)->where("status",1)->first();


    $category = DB::table('product_service_category')->where('id', '=' ,$item->category_id)->value('category');

        if($item){

            $similarItem=Productservice::where("id","!=",$item->id)->where("vendor_id",$vendor_id)->where("sub_category_id",$item->sub_category_id)->inRandomOrder()->take(5)->get();

            if($similarItem->count() < 3){

                $similarItems = Productservice::where("id","!=",$item->id)->where("vendor_id",$vendor_id)->where("category_id",$item->category_id)->inRandomOrder()->take(5)->get();

            }else{

              $similarItems = $similarItem;

            }

            //if ($request->session()->has('ps_qty')){ $request->session()->put('ps_qty', 1); }
            //$request->session()->forget('ps_qty');
            //$request->session()->forget('ps_slug');
            $ps_qty = 1;

            $finalSimilarItems = [];
            if(count($similarItems) > 0){
                $i = 0;
                foreach($similarItems as $items){

                  $image = DB::table("ps_images")->where("ps_id",$items->id)->value('name');

                  $imageURL = asset("/public/uploads/product")."/".$image;

                    $finalSimilarItems[$i]['imageURL'] = $imageURL;
                    $finalSimilarItems[$i]['slug'] = $items->slug;
                    $finalSimilarItems[$i]['types'] = $items->types;
                    $finalSimilarItems[$i]['name'] = $items->name;
                    $finalSimilarItems[$i]['price'] = $items->price;
                    $finalSimilarItems[$i]['vendor_id'] = $items->vendor_id;
                    $finalSimilarItems[$i]['category_id'] = $items->category_id;
                    $avgRating = $items->avg_rating;
                    $finalSimilarItems[$i]['avgRating'] = round($avgRating);
                    $i++;
                }
            }

            $ratingReviewData = Rating::select('profile_image', 'name', 'lname', 'rating', 'review', 'ratings.created_at')->join('users as u', 'ratings.user_id', '=', 'u.id')->Where('ps_id', $item->id)->Where('status', 1)->get();

            $totalUserRating = Rating::Where('ps_id', $item->id)->Where('status', 1)->sum('rating');

            $cartqtyold = DB::table('cart')->where('ps_id',$item->id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

            return view("product-details",["item" => $item,"finalSimilarItems" => $finalSimilarItems,"ps_qty" => $ps_qty, "ratingReviewData" => $ratingReviewData, "totalUserRating" => $totalUserRating, "vendor_id" => $vendor_id, "cartqtyold" => $cartqtyold,"storename" => $storename,"category" => $category]);
        }
        abort(404);
    }


    public function productDetail(Request $request)
    {

    $vendor_id = $request->username;

    $cartqtyold = '';

    $user_id = csrf_token();

    if(!empty(auth()->guard('user')->user())){

        $user_id = auth()->guard('user')->user()->id;
    }

   $storename = Vendor::where("vendor_id",$vendor_id)->value('username');

    $item=Productservice::where("slug",$request->slug)->where("vendor_id",$vendor_id)->where("status",1)->first();

    $category = DB::table('product_service_category')->where('id', '=' ,$item->category_id)->value('category');



    $first_image = $item->image;

        if($item){

            $similarItem=Productservice::where("id","!=",$item->id)->where("vendor_id",$vendor_id)->where("sub_category_id",$item->sub_category_id)->inRandomOrder()->take(5)->get();

            if($similarItem->count() < 3){

                $similarItems = Productservice::where("id","!=",$item->id)->where("vendor_id",$vendor_id)->where("category_id",$item->category_id)->inRandomOrder()->take(5)->get();

            }else{

              $similarItems = $similarItem;

            }

            //if ($request->session()->has('ps_qty')){ $request->session()->put('ps_qty', 1); }
            //$request->session()->forget('ps_qty');
            //$request->session()->forget('ps_slug');
            $ps_qty = 1;

            $finalSimilarItems = [];
            if(count($similarItems) > 0){
                $i = 0;
                foreach($similarItems as $items){

                  $image = DB::table("ps_images")->where("ps_id",$items->id)->value('name');

                  $imageURL = asset("/public/uploads/product")."/".$image;

                    $finalSimilarItems[$i]['imageURL'] = $imageURL;
                    $finalSimilarItems[$i]['slug'] = $items->slug;
                    $finalSimilarItems[$i]['types'] = $items->types;
                    $finalSimilarItems[$i]['name'] = $items->name;
                    $finalSimilarItems[$i]['price'] = $items->price;
                    $finalSimilarItems[$i]['vendor_id'] = $items->vendor_id;
                    $finalSimilarItems[$i]['category_id'] = $items->category_id;
                    $avgRating = $items->avg_rating;
                    $finalSimilarItems[$i]['avgRating'] = round($avgRating);
                    $i++;
                }
            }

            $ratingReviewData = Rating::select('profile_image', 'name', 'lname', 'rating', 'review', 'ratings.created_at')->join('users as u', 'ratings.user_id', '=', 'u.id')->Where('ps_id', $item->id)->Where('status', 1)->get();

            $totalUserRating = Rating::Where('ps_id', $item->id)->Where('status', 1)->sum('rating');

            $cartqtyold = DB::table('cart')->where('ps_id',$item->id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

            $page = (object)array('seo_title' => $item->name,'seo_keyword' => '','seo_description' => '','seo_author' => '', );

            if($item->type==3){

            return view("ticket-booking", ["first_image" => $first_image , "item" => $item,"finalSimilarItems" => $finalSimilarItems,"ps_qty" => $ps_qty, "ratingReviewData" => $ratingReviewData, "totalUserRating" => $totalUserRating, "vendor_id" => $vendor_id, "cartqtyold" => $cartqtyold,"storename" => $storename,"category" => $category, 'page_data' =>$page]);

           }else{

            return view("product-details",["item" => $item,"finalSimilarItems" => $finalSimilarItems,"ps_qty" => $ps_qty, "ratingReviewData" => $ratingReviewData, "totalUserRating" => $totalUserRating, "vendor_id" => $vendor_id, "cartqtyold" => $cartqtyold,"storename" => $storename,"category" => $category, 'page_data' =>$page]);
          }
        }
        abort(404);
    }


    // public function merchantProducts(Request $request){
    //     $merchant=Vendor::where("username",$request->username)->where("vendor_status",1)->first();
    //     if(!is_null($merchant)){
    //         //SELECT COUNT(ps_id) as top_ps, ps_id FROM orders where vendor_id=103 GROUP BY ps_id ORDER BY top_ps DESC
    //         $bestSellingItems = DB::table('orders')
    //              ->select('ps_id', DB::raw('count(ps_id) as top_ps'))
    //              ->where("vendor_id",103)
    //              ->groupBy('ps_id')
    //              ->orderBy("top_ps","DESC")->limit(3)
    //              ->get()->pluck("ps_id");

    //             $itemsIDs = $bestSellingItems->implode(",");

    //         $items=Productservice::whereIn("id",$bestSellingItems) ->orderByRaw("FIELD(id, $itemsIDs)")->get();
    //         return view("seller-details",["merchant" => $merchant,"items" => $items]);
    //     }
    //     abort(404);
    // }


    public function saveUserPsReview(Request $request)
    {
        $validator=Validator::make($request->all(),[
            "rating" => ['required','numeric'],
            "comment" => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(["ok" => 0,"message" => "Please write your review"]);
        }

        $user_id = auth()->guard('user')->user()->id;
        $user_review_exist = Rating::where('user_id', $user_id)->where('ps_id', $request->ps_id)->count();

        if($user_review_exist == 0){
            $rating = new Rating;
            $rating->user_id = $user_id;
            $rating->rating = $request->rating;
            $rating->review = $request->comment;
            $rating->type = $request->type;
            $rating->ps_id = $request->ps_id;
            $rating->save();

           /* $no_of_times_rated = Rating::where('ps_id', $request->ps_id)->count();
            $rating_sum = Rating::where('ps_id', $request->ps_id)->sum('rating');
            $avg_rating = round($rating_sum/$no_of_times_rated);
            Productservice::where('id', $request->ps_id)->update( [ 'avg_rating' => $avg_rating,  'rating_count' => DB::raw('rating_count + 1') ] ); */

            return response()->json(["ok" => 1,"message" => "Thank you for your review and rating."]);
        }else{
            return response()->json(["ok" => 0,"message" => "Rating and review already given."]);
        }
    }
}
