<?php
namespace App\Http\Controllers\grambunny;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

class ChatController extends Controller{
    

    public function chatsystem(Request $request)
    {

        $chatdata = $request->all();

        $date = date('Y-m-d H:i:s');

        $cmessage = $chatdata['chat_data']['cmessage'];
        $cuserid = $chatdata['chat_data']['cuserid']; 
        $cmerchantid = $chatdata['chat_data']['cmerchantid'];  

        DB::table('chat_system')->insert([
        'merchant_id' => $cmerchantid,
        'customer_id' => $cuserid,
        'message' => $cmessage,
        'chat_type' => 'receiver',
        'read_status' => 0,
        'created_at' => $date    

        ]);

        $getchat = DB::table('chat_system')       
                    ->where('merchant_id', '=' ,$cmerchantid)
                    ->where('customer_id', '=' ,$cuserid)
                    ->orderBy('chat_id', 'asc')
                    ->get();

        $data_onview = array('chatmsg'=>$getchat); 

        return View('ajax.chat')->with($data_onview);

    }

    public function chathistory(Request $request)
    {

        $chatdata = $request->all();

        $cuserid = $chatdata['chat_data']['cuserid']; 
        $cmerchantid = $chatdata['chat_data']['cmerchantid'];  

        $getchat = DB::table('chat_system')       
                    ->where('merchant_id', '=' ,$cmerchantid)
                    ->where('customer_id', '=' ,$cuserid)
                    ->orderBy('chat_id', 'asc')
                    ->get();

        $data_onview = array('chatmsg'=>$getchat); 

        return View('ajax.chat')->with($data_onview);

    }

    public function chatmlist(Request $request)
    {

        $chatdata = $request->all();

        $cmerchantid = $chatdata['chat_data']['cmerchantid']; 

        $chat_merchant = DB::table('orders')
                ->where("vendor_id",$cmerchantid)
                ->whereNotIn("status",array('2', '4'))
                ->orderBy('orders.id', 'DESC')
                ->groupBy('orders.user_id')
                ->get();     

        $data_onview = array('chatmerchant'=>$chat_merchant); 

        return View('ajax.chat_ulist')->with($data_onview);

    }




}