<?php

namespace App\Http\Controllers\grambunny;

use App\Vendor;

use Carbon\Carbon;

use App\Productservice;

use App\Cart;

use Illuminate\Http\Request;

use App\Coupon_code as Coupon;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\User;

use App\Notification_list; 

use App\Notification_vendor_list;

use Hash;

use Session;

use Redirect;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Mail;


class CheckoutController extends Controller
{

    public function apply_coupon(Request $request)
    {

        $promo_code = $request->promo_code;
        $vendor_id = $request->vendor_id;
        $user_id = $request->user_id;
        $coupon_info = DB::table('coupon_code')->where('coupon','=',$promo_code)->where('vendor_id','=',$vendor_id)->first();

        $coupon_info1 = DB::table('coupon_code')->where('coupon','=',$promo_code)->where('vendor_id','=',0)->first();

        if(!empty($coupon_info->id)){

            $order_info = DB::table('orders')->where('coupon_id','=',$coupon_info->id)->where('vendor_id','=',$vendor_id)->where('user_id','=',$user_id)->first();

           // if(empty($order_info->id)){

                if($coupon_info->status == 1){

                    $start_time = date('d-m-Y',strtotime($coupon_info->created_at));
                    $end_time = date('d-m-Y',strtotime($coupon_info->valid_till));

                    if(strtotime($start_time) <= strtotime(date('Y-m-d')) && strtotime($end_time) >= strtotime(date('Y-m-d'))){

                        $qty = session('ps_qty');

                        $total_price = $request->item_price;

                        $subtotal_val =  intval( $total_price );
                        if($coupon_info->discount==0){

                        if($coupon_info->apply_min_amount > $subtotal_val){

                            return json_encode(array('status'=>0,'msg'=>'Your product amount should be more than minimum amount'));

                        }else{

                        if($coupon_info->amount > $subtotal_val){    

                        return json_encode(array('status'=>0,'msg'=>'Your order amount should be more than discount amount'));

                          }

                         }
                            
                        }

                        if($coupon_info->discount == 1){

                                $promo_amount = $total_price*$coupon_info->amount/100;

                                $request->session()->put('promo_amount', $promo_amount); 

                                $request->session()->put('promo_qty', $qty);

                                $request->session()->put('vendor_id', $vendor_id); 

                                $request->session()->put('coupon_id', $coupon_info->id); 

                                $request->session()->put('coupon_code', $coupon_info->coupon); 

                                return json_encode(array('status'=>1,'msg'=>'Promo applied!','promo_amount'=>$promo_amount));

                        }else{

                            //Session::set('promo_amount', $coupon_info->amount);

                            $request->session()->put('promo_amount', $coupon_info->amount);

                            $request->session()->put('promo_qty', $qty);

                            $request->session()->put('vendor_id', $vendor_id); 

                            $request->session()->put('coupon_id', $coupon_info->id); 

                              $request->session()->put('coupon_code', $coupon_info->coupon);

                            return json_encode(array('status'=>1,'msg'=>'Promo applied!','promo_amount'=>$coupon_info->amount)); 

                        }

                        

                    } else {

                        return json_encode(array('status'=>0,'msg'=>'Validity Expired!'));

                    }



                } else {

                    return json_encode(array('status'=>0,'msg'=>'Coupon code is inactive!'));

                }

           /* } else {

                return json_encode(array('status'=>0,'msg'=>'Coupon already used!'));

            } */

        }else if($coupon_info1){

                if($coupon_info1->status == 1){

                    $start_time = date('d-m-Y',strtotime($coupon_info1->created_at));

                    $end_time = date('d-m-Y',strtotime($coupon_info1->valid_till));

                    if(strtotime($start_time) <= strtotime(date('Y-m-d')) && strtotime($end_time) >= strtotime(date('Y-m-d'))){

                        $qty = session('ps_qty');

                        $total_price = $request->item_price;

                        $subtotal_val =  intval( $total_price );
                        if($coupon_info1->discount==0){

                        if($coupon_info1->apply_min_amount > $subtotal_val){

                            return json_encode(array('status'=>0,'msg'=>'Your product amount should be more than minimum amount'));

                        }else{

                        if($coupon_info1->amount > $subtotal_val){    

                        return json_encode(array('status'=>0,'msg'=>'Your order amount should be more than discount amount'));

                          }

                         }
                            
                        }

                        if($coupon_info1->discount == 1){

                                $promo_amount = $total_price*$coupon_info1->amount/100;

                                $request->session()->put('promo_amount', $promo_amount); 

                                $request->session()->put('promo_qty', $qty);

                                $request->session()->put('vendor_id', 0); 

                                $request->session()->put('coupon_id', $coupon_info1->id); 

                                $request->session()->put('coupon_code', $coupon_info1->coupon); 

                                return json_encode(array('status'=>1,'msg'=>'Promo applied!','promo_amount'=>$promo_amount));

                        }else{

                            //Session::set('promo_amount', $coupon_info->amount);

                            $request->session()->put('promo_amount', $coupon_info1->amount);

                            $request->session()->put('promo_qty', $qty);

                            $request->session()->put('vendor_id', $vendor_id); 

                            $request->session()->put('coupon_id', $coupon_info1->id);

                            $request->session()->put('coupon_code', $coupon_info1->coupon); 

                            return json_encode(array('status'=>1,'msg'=>'Promo applied!','promo_amount'=>$coupon_info1->amount)); 
                        }

                    } else {

                        return json_encode(array('status'=>0,'msg'=>'Validity Expired!'));

                    }



                } else {

                    return json_encode(array('status'=>0,'msg'=>'Coupon code is inactive!'));

                }

            // } else {

            //     return json_encode(array('status'=>0,'msg'=>'Coupon already used!'));

            // }

        } else {

            return json_encode(array('status'=>0,'msg'=>'Invalid coupon code!'));

        }

    }

    public function submit_order(Request $request)
    {

        $locationId = $request->locationId;
        $sourceId = $request->sourceId;// payment token

        $verificationToken = $request->verificationToken;
        $idempotencyKey = $request->idempotencyKey;

        $paymentMethod = $request->pmethodc;
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $user_mob = $request->user_mob;
        $data['user_mob'] = $request->user_mob;
        $user_id = '';
        $redirect_url = '';
        $ajxstatus = 1;

        $firstNameT = array();
        $lastNameT = array();

        if(isset($request->firstNameT)){ $firstNameT = $request->firstNameT; }

        if(isset($request->lastNameT)){ $lastNameT = $request->lastNameT; }

        $ticket_service_fee = $request->ticket_service_fee;

        $ticket_fee = $request->ticket_fee;

        /* check cart product code */

       if($request->user_id){

        $cartuserid = $request->user_id;

        }else{

        $cartuserid = Session::get("_token");

        }

        $vendor_id = $request->vendor_id;
        $cartcount = $request->cart_count;

        $checkcartinfo = DB::table('cart')->where('user_id','=',$cartuserid)->where('vendor_id','=',$vendor_id)->get();     

        /* End check cart product code */

        if(count($checkcartinfo)>0 && count($checkcartinfo)==$cartcount){    

        if($request->user_status==2){

        $useremail  = DB::table('users')->where('email', '=' , $request->email)->get();

        if(count($useremail)>0){  

        DB::table('users')
        ->where('email',$request->email)
        ->update(['token'=>$request->_token, 'name'=>$request->firstName, 'lname'=>$request->lastName, 'user_mob'=>$request->user_mob]);

        $user_id = DB::table('users')
        ->where('email',$request->email)
        ->value('id');

        DB::table('cart')->where('user_id',Session::get("_token"))->update(['user_id' => $user_id]); 

        }else{

        $password = 'GB123456';     

        $user= new User();

        $user->token = $request->_token;//$token=str_random(16);

        $user->name = $request->firstName;

        $user->lname = $request->lastName;

        $user->email = $request->email;

        $user->password = Hash::make($password);

        $user->user_mob = $request->user_mob;

        $user->user_address = '';

        $user->user_city = '';

        $user->user_states = '';

        $user->user_zipcode = '';

        $user->dob = '';

        $user->license_front = ''; 

        $user->license_back = '';

        $user->marijuana_card = '';

        $user->customer_type = '';

        $user->profile_image = "user.jpg";

        $user->user_lat =  '';

        $user->user_long = '';

        $user->user_status = 1;

        $user->user_role = 2;

        $user->email_verified_at = Carbon::now();

        $user->save();

        $user_id =  $user->id;

        DB::table('cart')->where('user_id',Session::get("_token"))->update(['user_id' => $user_id]); 

       }

   }  

        // $latitude = $request->latitude;

        // $longitude = $request->longitude;

        if(isset($request->address)){

        $address = $request->address.', '.$request->address2;

         $address1 = $request->address;
         $address2 = $request->address2;

        }else{ $address = ''; }

        if(isset($request->city)){ $city = $request->city; }else{ $city = ''; }

        if(isset($request->state)){ $state = $request->state; }else{ $state = ''; }

        if(isset($request->zip)){ $zip = $request->zip; }else{ $zip = ''; }

        if(isset($request->country)){ $country = $request->country;} else{ $country = ''; }

        if(isset($request->addressp)){ $addressp = $request->addressp;} else{ $addressp = ''; }
        if(isset($request->cityp)){ $cityp = $request->cityp;} else{ $cityp = ''; }
        if(isset($request->statep)){ $statep = $request->statep;} else{ $statep = ''; }
        if(isset($request->zipp)){ $zipp = $request->zipp;} else{ $zipp = ''; }

        $pickupdel = $request->pickup_del;

        if(isset($request->pick_up_address)){ $pickup_add = $request->pick_up_address; }else{ $pickup_add = ''; }

        if($pickupdel=='delivery'){

        $addressd = $request->addressd.', '.$request->addressd2;
        $cityd = $request->cityd;
        $stated = $request->stated;
        $zipd = $request->zipd;
        $countryd = $request->countryd;

        if(empty($addressd)){
            return json_encode(array('status'=>0,'msg'=>'Please enter delivery address'));
        }
        elseif(empty($cityd)){
            return json_encode(array('status'=>0,'msg'=>'Please enter delivery city'));
        }
        elseif(empty($stated)){
            return json_encode(array('status'=>0,'msg'=>'Please enter delivery state'));
        }
        elseif(empty($zipd)){
            return json_encode(array('status'=>0,'msg'=>'Please enter delivery Zip'));
        }else{
 
        //$addressnew = $addressd.','.$cityd.','.$stated.','.$zipd;  
        $addressnew = $addressd.','.$cityd.','.$zipd;
        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
       // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addressnew).'&sensor=false&key='.$apiKey);

        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

        $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude

        }   
        if(empty($latitude) || empty($longitude)){
         
           $latitude = $request->latitude;
           $longitude = $request->longitude;
        }
        else{
           
        $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }

      }

    }else{

        $addressd = '';
        $cityd = '';
        $stated = '';
        $zipd = '';
        $countryd = '';
   
      if(!empty($pickup_add)){
        $addressnew = $pickup_add;
       }else{

       $addressnew = DB::table('vendor')->where('vendor_id','=',$request->vendor_id)->value('pick_up_address');

       }
        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
       // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($addressnew).'&sensor=false&key='.$apiKey);

        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

        $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude

        }   
        if(empty($latitude) || empty($longitude)){
         
           $latitude = $request->latitude;
           $longitude = $request->longitude;

        }else{
           
        $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
        $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }    

    }

        $comment = $request->comment;

        $cc_name = $request->firstName.' '.$request->lastName;

        $cardtype = '';

        $cc_number = $request->cc_number;

        $cc_expiration = $request->cc_expiration;

        $cc_cvv = $request->cc_cvv;
        
        $cc_expiry = $request->cc_expire_month.'/'.$request->cc_expire_year;

        $ccmonth = $request->cc_expire_month;
        $ccyear = $request->cc_expire_year;

        $item_price = $request->item_price;

        $item_qty = '';//$request->item_qty;

        $sub_total = $request->sub_total;

        $promo_amount = $request->promo_amount;

        $final_amount = $request->final_amount; //$request->sub_total+$request->saletax+$request->deliveryfee-$request->promo_amount;//

        $vendor_id = $request->vendor_id;

        $saletax = $request->saletax;

        $excisetax = $request->excisetax; 

        $citytax = $request->citytax;

        $deliveryfee = $request->deliveryfee;

        $sales_tax_percent = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('sales_tax');

        $excise_tax_percent = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('excise_tax');
         //print_r($excise_tax_percent);die;
        $city_tax_percent = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('city_tax');

        $commission_rates = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('commission_rate');

        $merchant_email = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('email');

        $merchant_name = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('username');
        //print_r($merchant_name);die;

        $merchant_last_name = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('last_name');

        $merchant_mob_no = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('mob_no');

        $device_type = 'website';

        $device_os_name = 'website';

        $ps_id = '';//$request->ps_id;

        if(empty($user_id)){

        $user_id = $request->user_id;

        }

        $vendor_id = $request->vendor_id;

        $delivery_time = date('h:i', strtotime('1 hour'));

        $userdob = DB::table('users')->where('id','=',$user_id)->value('dob');

        $user_dob = date("m/d/Y", strtotime($userdob));  

    $nonce = strval(hexdec(bin2hex(openssl_random_pseudo_bytes(4, $cstrong))));

    $timestamp = strval(time()*1000); //time stamp in milli seconds

    $getdata =  DB::table('orders')->get();

    if(count($getdata) > 0){

    $latestOrder = DB::table('orders')->get()->last()->order_id; 

    }else{

    $latestOrder = 0;

    }


    $order_id = str_pad($latestOrder + 1, 8, "0", STR_PAD_LEFT);

    //$order_id = strtoupper($this->generateRandomString(6));

    $reference_no = rand(100000,999999);
   
    $accountNumber = '';

  if($paymentMethod=='card'){ 

  // Api by randy
        
  /* $curl = curl_init();

  curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://heymobiepayment.com/v1/users/get-access-token',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "email": "sandbox@heymobie.com"
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$aresponse = curl_exec($curl);

curl_close($curl);

$aresponsedata = json_decode($aresponse); 

$access_token = $aresponsedata->data->access_token; */

/*  

$access_token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImxpdmVlbnZAcGFja3NkYXNoLmNvbSIsImF1dGhlbnRpY2F0ZV9pZCI6IjIzZTE0YTRlOWMyOTRmOTlhNDQ2YTViZGFkNDFlOTdmIiwiYXV0aGVudGljYXRlX3B3IjoiZTMxYzA4YTM2YTYwY2Y1MzZmM2EwODk4ZDM3MjMxMjMiLCJzZWNyZXRfa2V5IjoiNjVkNDE4NmUzM2RkZTAuMDk2MjU0OTYiLCJwbGF0Zm9ybV91cmwiOiJodHRwczovL2FwaS5wYXltZW50ZWNobm9sb2dpZXMuY28udWsvIiwiaXNfbGl2ZSI6dHJ1ZSwiY2xpZW50X25hbWUiOiJIRVlNT0JJRSIsImNsaWVudF9wcmVmaXgiOiJITSIsImlhdCI6MTcxMTQ0NDI5OH0.cv9141plzWlUSUX-QsUcAJOQ_6pEHg98cHJU7mz8h8Q";


    $data = array(
    "orderid" => $order_id,
    "amount" => $final_amount,
    "currency" => "USD",
    "ccn" => $cc_number,
    "expire" => $cc_expiry,
    "cvc" => $cc_cvv,
    "firstname" => $firstName,
    "lastname" => $lastName,
    "email" => $email ,
    "street" => $address,
    "city" => $city,
    "zip" => $zip,
    "state" => $state,
    "country" => "USA",//$country
    "phone" => $user_mob,
    "transaction_hash" => "", 
    "customerip" => $_SERVER['REMOTE_ADDR'],
    "dob" => date("Y-m-d", strtotime($userdob)),
    "success_url" => "https://www.grambunny.com/payment_successful",
    "fail_url" => "https://www.grambunny.com/payment_failed",
    "notify_url" => ""
     );

    $paydata = json_encode($data, JSON_FORCE_OBJECT);

    $curl = curl_init();

  curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://heymobiepayment.com/v1/pt/payment/capture',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => $paydata,
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'API: '.$access_token
  ),
));

$response = curl_exec($curl);
curl_close($curl);

//print_r($response); die;

$responsedata = json_decode($response); 

$code = $responsedata->code;
$status = $responsedata->status;
$redirect_url = $responsedata->response->redirect_url;

$restransactionid = $responsedata->response->transaction_id;
$resorderid = $responsedata->response->order_id; 
$resmessage = $responsedata->response->message;
$last_four = substr($cc_number, -4);
$responseCode = $responsedata->response->errorCode;

if($code=='200' && $status=='success'){

 $resstatus = 'success'; 
 $ajxstatus = 1;

}elseif($code=='200' && $status=='redirect-required'){ 

 $resstatus = 'pending';
 $ajxstatus = 2; 

}elseif($code=='430' && $status=='failed'){ 

 $resstatus = 'failed'; 
 $ajxstatus = 0; 

}elseif($code=='430' && $status=='error'){ 

 $resstatus = 'Payment failed'; 
 $ajxstatus = 0; 

}else{ $resstatus = '';  } */

// close Api by randy


           /* $payload = '{
            "createTransactionRequest": {
                "merchantAuthentication": {
                    "name": "5a63zbzAgXKK",
                    "transactionKey": "8h9QAh6UtC395z43"
                },
                "refId": "'.$reference_no.'",
                "transactionRequest": {
                    "transactionType": "authCaptureTransaction",
                    "amount": "'.$final_amount.'",
                    "payment": {
                        "creditCard": {
                            "cardNumber": "'.$cc_number.'",
                            "expirationDate": "'.$cc_expiry.'",
                            "cardCode": "'.$cc_cvv.'"
                        }
                    },
                    "tax": {
                    "amount": "'.$saletax.'",
                    "name": "Sales Tax",
                    "description": ""
                     },
                    "shipping": {
                        "amount": "'.$deliveryfee.'",
                        "name": "Shipping Price",
                        "description": ""
                    },
                    "poNumber": "'.$user_mob.'",
                    "customer": {
                        "id": "'.$user_id.'"
                    },
                    "billTo": {
                        "firstName": "'.$firstName.'",
                        "lastName": "'.$lastName.'",
                        "company": "",
                        "address": "'.$address.'",
                        "city": "'.$city.'",
                        "state": "'.$state.'",
                        "zip": "'.$zip.'",
                        "country": "US"
                    },

                    "shipTo": {
                        "firstName": "'.$firstName.'",
                        "lastName": "'.$lastName.'",
                        "company": "",
                        "address": "'.$address.'",
                        "city": "'.$city.'",
                        "state": "'.$state.'",
                        "zip": "'.$zip.'",
                        "country": "US"
                    },
                    "customerIP": "'.$_SERVER['REMOTE_ADDR'].'",
                    "transactionSettings": {
                        "setting": {
                            "settingName": "testRequest",
                            "settingValue": "false"
                        }
                    },
                    "processingOptions": {
                        "isSubsequentAuth": "true"
                    },
                    "subsequentAuthInformation": {
                        "originalNetworkTransId": "123456789NNNH",
                        "originalAuthAmount": "'.$final_amount.'",
                        "reason": "resubmission"
                    },
                    "authorizationIndicatorType": {
                        "authorizationIndicator": "final"
                    }
                  }
                }
              }';


                  $curl = curl_init();
                  curl_setopt_array($curl, array(
                  CURLOPT_URL => 'https://api.authorize.net/xml/v1/request.api',
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => 'POST',
                  CURLOPT_POSTFIELDS => $payload,
                  CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                  ),
                ));

                
                $result = curl_exec($curl);

                $responseJson = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $result);
            
                curl_close($curl); 

                $results = json_decode($responseJson,true); */

                // if ($results['transactionResponse']['responseCode'] == '1') {

                // Api by randy

                 /*  if($responseCode==200){

                   $results['transactionResponse']['responseCode'] = '1';

                   $cardtype = 'GB Card Service'; 

                   $ptxn_status = $resstatus;
                   $ptxn_tag = $restransactionid;
                   $preceipt_url = '';
                   $transactiontag = $restransactionid;
                   $transaction_type = 'Grambunny Card Service';
                   $authorization_num = '';
                   $transaction_approved = $resmessage;
                   $bank_message = '';


                    $data = array(

                    'user_id' => $user_id,

                    'order_id' => $order_id,

                    'cc_expiry' => $cc_expiry,

                    'credit_card_type' => 'CREDIT',

                    'amount' => $final_amount,

                    'transaction_type' => 'Grambunny Card Service',

                    'transaction_approved' => $resstatus,

                    'created_at' => date('Y-m-d H:i:s'),

                    'updated_at' => date('Y-m-d H:i:s')

                );


               DB::table('users_card')->insertGetId($data);
                      
                }
                else
                {
                      return json_encode(array('status'=>0,'msg'=>$resmessage));   

                } */


$ip_address = $_SERVER['REMOTE_ADDR'];
$currency_code = "USD";
$cardtype = '';

 /* Valid card number validation */

/* 

$cvariexp = $ccmonth.$ccyear;

            $card_varification = array (
            'METHOD' => 'DoDirectPayment',
            'USER' => 'deepaksanidhya_api1.gmail.com',
            'PWD' => 'NVA4VZETTGTWFNTM',
            'SIGNATURE' => 'AXsc3YsPwPHamsW.7NrxtR-8foL9A3XXp2YQYJMz9-3ae8CC1S0ixBPx',
            'VERSION' => '58.0',
            'PAYMENTACTION' => 'Authorization',
            'IPADDRESS' => $ip_address,
            'CREDITCARDTYPE' => $cardtype,
            'ACCT' => $cc_number,
            'EXPDATE' => $cvariexp,
            'AMT' => '0.00',
            'CURRENCYCODE' => 'USD',
            'STREET' => '1920 S',
            'STREET2' => 'Archibald Ave',
            'CITY' => 'San Francisco',
            'STATE' => 'CA',
            'ZIP' => '94121',
            'COUNTRYCODE' => 'US',
            'EMAIL' => '',

       );

       $nvp_string = '';
       foreach($card_varification as $var=>$val)
       {
          $nvp_string .= '&'.$var.'='.urlencode($val);
       }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, 'https://www.sandbox.paypal.com/nvp');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

        $vresult = curl_exec($curl);

        curl_close($curl);

       $cveryfication = $this->NVPToArray($vresult);

       if($cveryfication['L_ERRORCODE0'] == '10527'){

       $responsedata['message'] = "Please enter a valid card number";
       $responsedata['status'] = 'Failure';
       $responsedata['data'] = array();

       print_r($responsedata);

     } */

/* Valid card number validation end */

//$PAYPAL_CLIENT_ID = 'AQapJetngAQB6Iqd9Fp74Pgtc3tGQXb1eYPYXXfeu8nvJNfZGmy1MggZWtWcMfJpEqPk0FQvPVD50cQq';
//$PAYPAL_SECRET = 'EATxrqgmumDOgaoy04YYOWUTl-f-Ut1QMyGLUoB0cjW8awAL0lrzNcJYvxIiCAY-9oJ0Kz-1YViZorL4';

//Soil Farm USA Inc

$PAYPAL_CLIENT_ID = 'ARqtzOPutihRfG8_qWB0FIGVwZysEFRmNlCM4viwKiBd_r0MvFZyR5jwhUqsh6to8CVNZ_A7gRBlDXbC';
$PAYPAL_SECRET = 'EGSoFIAFpqRcsVEl9u_Nf7XjmfFofp5Xmy0Ycq8tNjMoqAkbXzs5YexBW1Uv2ZaNkm6ha4ChW-It0XaN';


    $data = array(
     'PAYPAL_CLIENT_ID'=> $PAYPAL_CLIENT_ID, 
     'PAYPAL_SECRET'=> $PAYPAL_SECRET,
     'final_amount'=> $final_amount,
     'cc_number'=> $cc_number,
     'cc_cvv'=> $cc_cvv,
     'ccyear'=> $ccyear,
     'ccmonth'=> $ccmonth,
     'cc_name'=> $cc_name,
     'address1'=> $address1,
     'address2'=> $address2,
     'city'=> $city,
     'state'=> $state,
     'zip'=> $zip,
     'country'=> $country,
     );


    $checkpost = json_encode($data, JSON_FORCE_OBJECT);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://heymobie.com/ws/pdpayment',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $checkpost,
      CURLOPT_HTTPHEADER => array(
        'heymobie: #G!em@H2e%y$M',
        'Content-Type: application/json'
      ),
    ));


    $result = curl_exec($curl);

    curl_close($curl);

    $datad = json_decode(json_decode($result,true)); 

/* Heymobie API End */

if(isset($datad->id)){

$response_id = $datad->id;
$intent = $datad->intent;
$status = $datad->status;

$card_name = $datad->payment_source->card->name;
$last_digits = $datad->payment_source->card->last_digits;
$expiry = $datad->payment_source->card->expiry;
$brand = $datad->payment_source->card->brand;
$type = $datad->payment_source->card->type;
$country_code = $datad->payment_source->card->bin_details->bin_country_code;

$currency_code = $datad->purchase_units[0]->amount->currency_code;
$txn_amount = $datad->purchase_units[0]->amount->value;
$merchant_id = $datad->purchase_units[0]->payee->merchant_id;

$transaction_id = $datad->purchase_units[0]->payments->captures[0]->id;
$txn_status = $datad->purchase_units[0]->payments->captures[0]->status;

$txn_currency_code = $datad->purchase_units[0]->payments->captures[0]->amount->currency_code;
$txn_amount = $datad->purchase_units[0]->payments->captures[0]->amount->value;

$create_time = $datad->create_time;

$results['transactionResponse']['responseCode'] = '1';

if($txn_status=='DECLINED'){

    return json_encode(array('status'=>0,'msg'=>'Card Declined By Bank')); 

}

    $authCode = $response_id;
    $transId = $transaction_id;
    $accountNumber = $merchant_id;
    $accountType = $brand;
    $refId = $response_id;
    $code = $currency_code;
    $text = $status;
    $ptxn_status = $txn_status;
    $ptxn_tag = $transaction_id;
    $preceipt_url = $response_id;
    $transactiontag = $merchant_id;
    $cardtype = $type;
    $transaction_type = '';
    $authorization_num = $transId;
    $transaction_approved = $txn_status;
    $bank_message = $text;

                
    $datads = array(
    'user_id' => $user_id,
    'order_id' => $order_id,
    'cardholder_name' => $cc_name,
    'cc_number' => $last_digits,
    'cc_expiry' => $expiry,
    'cvd_code' => '',
    'credit_card_type' => $brand,
    'amount' => $final_amount,
    'reference_no' => $response_id,
    'transaction_type' => $cardtype,
    'transaction_tag' => strval($transactiontag),
    'authorization_num' => $authorization_num,
    'transaction_approved' => $transaction_approved,
    'bank_message' => $bank_message,
    'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => date('Y-m-d H:i:s')

);

$lastid = DB::table('users_card')->insertGetId($datads);

}
else
{

    $name = $datad->name;

    if($datad->details[0]->description){

    $message = $datad->details[0]->description;

    }else{

    $message = $datad->message;

    }

     $results['transactionResponse']['responseCode'] = '0';

    return json_encode(array('status'=>0,'msg'=>$message));

}
         

        }else if($paymentMethod=='square'){ 

        $note = 'Heymobie Payment'; 
        
        $squreamount = round($final_amount*100);
        $curl = curl_init();

        $app_id = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->value('app_id');

        $access_token = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->value('access_token');

        $location_id = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->value('location_id');

        $payment_mode = DB::table('admin_setting')->where('id', '=' ,1)->value('payment_mode');

        if($payment_mode==1){

        $squareurl = 'https://connect.squareup.com/v2/payments';    

        }else{

        $squareurl = 'https://connect.squareupsandbox.com/v2/payments';

        }

        /* $curl = curl_init();
         curl_setopt_array($curl, array(
          CURLOPT_URL => $squareurl,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "idempotency_key": "'.$idempotencyKey.'",
            "source_id": "'.$sourceId.'",
            "accept_partial_authorization": false,
            "amount_money": {
              "amount": '.$squreamount.',
              "currency": "USD"
            },
            "autocomplete": true,
            "reference_id": "'.$reference_no.'",
            "location_id": "'.$location_id.'",
            "note": "'.$note.'"
          }',
          CURLOPT_HTTPHEADER => array(
            'Square-Version: 2023-07-20',
            'Authorization: Bearer '.$access_token,
            'Content-Type: application/json'
          ),
        )); 

        $result = curl_exec($curl);

        $results = json_decode($result,true);

        curl_close($curl); 
        
        print_r($result); die; */ 



    /* $url = 'https://heymobie.com/square.php';

    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);

    $sourceId = trim($data);  */

   /* Live code comment 4-2-24

     $data = array(
      'squareurl'=> $squareurl,
      'idempotencyKey'=> $idempotencyKey,
      'sourceId'=> $sourceId,
      'squreamount'=> $squreamount,
      'reference_no'=> $reference_no,
      'location_id'=> $location_id,
      'note'=> $note,
      'access_token'=> $access_token
     );
   
    $checkpost = json_encode($data, JSON_FORCE_OBJECT);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://heymobie.com/ws/webpayment',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $checkpost,
      CURLOPT_HTTPHEADER => array(
        'heymobie: #G!em@H2e%y$M',
        'Content-Type: application/json'
      ),
    ));


        $result = curl_exec($curl);

        $results = json_decode($result,true);

        curl_close($curl);

        if(isset($results['errors'][0]['detail'])){

        $paystatus = $results['errors'][0]['detail'];

        return json_encode(array('status'=>0,'msg'=>$paystatus));

        }else{

        $paymentID = $results['payment']['id']; 
        $paystatus = $results['payment']['status']; // COMPLETED

                   $cardtype = 'Square Payment';

                   if($paystatus=='COMPLETED'){

                   $results['transactionResponse']['responseCode'] = '1';

                    $sorderID = $results['payment']['order_id'];
                    $card_details = $results['payment']['card_details']['card'];  
                    $card_brand = $card_details['card_brand'];
                    $last_4 = $card_details['last_4'];
                    $exp_month = $card_details['exp_month'];
                    $exp_year = $card_details['exp_year'];
                    $card_type = $card_details['card_type'];          

                   $ptxn_status = 'success';
                   $ptxn_tag = $paymentID;
                   $preceipt_url = '';
                   $transactiontag = $sorderID;

                   $transaction_type = $card_type;
                   $authorization_num = '';
                   $transaction_approved = $paystatus;
                   $bank_message = '';


                    $data = array(

                    'user_id' => $user_id,

                    'order_id' => $order_id,

                    'cc_expiry' => $exp_month.'-'.$exp_year,

                    'credit_card_type' => $card_type,

                    'amount' => $final_amount,

                    'transaction_type' => $card_type,

                    'transaction_approved' => $paystatus,

                    'created_at' => date('Y-m-d H:i:s'),

                    'updated_at' => date('Y-m-d H:i:s')

                );

               DB::table('users_card')->insertGetId($data);

                 }
                else
                {

                      return json_encode(array('status'=>0,'msg'=>$paystatus));   

                }

              } */

 // Heymobie site payment

               $ajxstatus = 3; 
               $redirect_url = "https://heymobie.com/square.php?paymentmode=".$payment_mode."&&squreamount=".$squreamount."&&app_id=".$app_id."&&location_id=".$location_id."&&access_token=".$access_token."&&address=".$address1."&&address2=".$address2."&&city=".$city."&&zip=".$zip."&&firstName=".$firstName."&&email=".$email."&&user_mob=".$user_mob."&&country=".$country;

                   $cardtype = 'Square Payment';

                   $results['transactionResponse']['responseCode'] = '1';

                   $ptxn_status = 'pending';
                   $ptxn_tag = '';
                   $preceipt_url = '';
                   $transactiontag = '';

                   $transaction_type = '';
                   $authorization_num = '';
                   $transaction_approved = '';
                   $bank_message = '';                 

                   }else if($paymentMethod=='cash'){  

                   $cardtype = 'Cash';

                   $results['transactionResponse']['responseCode'] = '1';

                   $ptxn_status = 'success';
                   $ptxn_tag = '';
                   $preceipt_url = '';
                   $transactiontag = '';

                   $transaction_type = '';
                   $authorization_num = '';
                   $transaction_approved = '';
                   $bank_message = '';

                   }else{

                   $cardtype = 'Card Swipe';

                   $results['transactionResponse']['responseCode'] = '1';

                   $ptxn_status = 'success';
                   $ptxn_tag = '';
                   $preceipt_url = '';
                   $transactiontag = '';

                   $transaction_type = '';
                   $authorization_num = '';
                   $transaction_approved = '';
                   $bank_message = '';

                }    
            

        $coupon_id = session('coupon_id');

        if (empty($coupon_id)) {

            $coupon_id = null;

        }


        $verification_code = rand(1000,9999);

        $data = array(

                    'vendor_id' => $vendor_id,

                    'user_id' => $user_id,

                    'ps_id' => $ps_id,

                    'ps_qty' => (!empty($item_qty) ? $item_qty : 1),

                    'product_type' => $request->ps_type,

                    'order_id' => $order_id,

                    'address' => $address,

                    'addressp' => $addressp,

                    'addressd' => $addressd,

                    'city' => $city,

                    'cityp' => $cityp,

                    'cityd' => $cityd,

                    'mobile_no' => $user_mob,

                    'email' => $email,

                    'instruction' => $comment,

                    'cancel_reason' => NULL,

                    'status' => (!empty($request->ps_type && $request->ps_type==3) ? 4 : 0),

                    'delivery_fee' => $deliveryfee,

                    'ticket_fee' => $ticket_fee,

                    'ticket_service_fee' => $ticket_service_fee, 

                    'delivery_time' => $delivery_time,

                    'latitude' => (!empty($latitude) ? $latitude : ''),

                    'longitude' => (!empty($longitude) ? $longitude : ''),

                    'sub_total' => $sub_total,

                    'total' => $final_amount,

                    'service_tax' => $saletax,

                    'excise_tax' => $excisetax,

                    'city_tax' => $citytax,

                    'delivery_fee' => $deliveryfee,

                    'promo_amount' => $promo_amount,

                    'coupon_id' => $coupon_id,

                    'verification_code' => $verification_code,

                    'txn_id' => strval($ptxn_tag), 

                    'transaction_tag' => (string)$transactiontag,

                    'authorization_num' => $preceipt_url,

                    'receipt_url' => $preceipt_url,

                    'pay_status' => $ptxn_status,

                    'payment_method' => $cardtype,

                    'first_name' => $firstName,

                    'last_name' => $lastName,

                    'state' => $state,

                    'statep' => $statep,

                    'stated' => $stated,

                    'zip' => $zip,

                    'zipp' => $zipp,

                    'zipd' => $zipd,

                    'country' => $country,

                    'countryd' => $countryd,

                     'city_tax_percent' => $city_tax_percent,

                     'excise_tax_percent' => $excise_tax_percent,

                     'sales_tax_percent' => $sales_tax_percent,

                     'commission_rates'  =>  $commission_rates,

                     'merchnt_email' => $merchant_email,

                     'merchnt_pickup_address' => $pickup_add,

                     'merchant_mob'  => $merchant_mob_no,

                     'merchant_name'  =>$merchant_name,

                     'merchant_last_name'  =>$merchant_last_name,

                    'device_type' => $device_type,

                    'device_os_name' => $device_os_name,

                    'created_at' => date('Y-m-d H:i:s'),

                    'updated_at' => date('Y-m-d H:i:s')

                );


        $id = DB::table('orders')->insertGetId($data);


        $addr_info = DB::table('delivery_address')->where('del_address','=',$address)->first();

        if(empty($addr_info->del_id)){ 

            $addr_data = array(

                        'del_userid' => $user_id,

                        'del_contactno' => $user_mob,

                        'del_address' => $address,

                        'del_zipcode' => $zip,

                        'del_state' => $state,

                        'del_lat' => (!empty($latitude) ? $latitude : ''),

                        'del_long' => (!empty($longitude) ? $longitude : ''),

                        'created_at' => date('Y-m-d H:i:s'),

                        'updated_at' => date('Y-m-d H:i:s')

                    );

            $addr_id = DB::table('delivery_address')->insertGetId($addr_data);

        }

        // if ($sub_total > $final_amount) {

        //     $promo_amount = $sub_total-$final_amount;

        // } else {

        //     $promo_amount = 0; 

        // }



        if(!empty($id)){ 

            // logic to insert values into orders_ps table for multiple product/service order

            $ps_info = DB::table('cart')->where('user_id','=',$user_id)->where('vendor_id','=',$vendor_id)->get();

            $vendor_info = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

            $ps_data = array();

            $ps_ids = array();

            $ps_final_data = array();


            foreach($ps_info as $ps){

                $ps_data['ps_id'] = $ps->ps_id;

                $ps_data['ps_qty'] = $ps->quantity;

                $ps_data['order_id'] = $id;

                $ps_data['created_at'] = date('Y-m-d H:i:s');

                $ps_data['updated_at'] = date('Y-m-d H:i:s');

                $product_name = DB::table('product_service')->where('id','=',$ps->ps_id)->value('name');

                $product_price = DB::table('product_service')->where('id','=',$ps->ps_id)->value('price');

                $image = DB::table('product_service')->where('id','=',$ps->ps_id)->value('image');

                $ps_data['name'] = $product_name;

                $ps_data['price'] = $product_price;

                $ps_data['image'] = $image;

                $ps_final_data[] = $ps_data;

                $ps_ids[] = $ps->ps_id;

                $popular_count = DB::table('product_service')->where('id','=',$ps->ps_id)->value('popular'); 

                $cquantity = DB::table('product_service')->where('id','=',$ps->ps_id)->value('quantity'); 

                $popquenty = $ps->quantity+$popular_count;

                $qtyupdate = $cquantity-$ps->quantity;

                $popular_update = DB::table('product_service')->where('id','=',$ps->ps_id)->update(['popular' => $popquenty,'quantity' => $qtyupdate]); 

            }

            $order_ps_id = DB::table('orders_ps')->insert($ps_final_data);

        /* $vendor_order = DB::table('orders')

        ->where('id', '=' ,$order_id)

        ->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

        ->first(); */

       $data['excise_tax_percent'] = $excise_tax_percent;
       $data['sales_tax_percent'] = $sales_tax_percent;
       $data['city_tax_percent'] = $city_tax_percent;

            //end for orders_ps

            $ps_info = Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->whereIn('product_service.id',$ps_ids)->where('crt.user_id',$user_id)->get();

            $item_arr =array();    

            foreach( $ps_info as $psinfo){

            $iteminfo['name'] = (!empty($psinfo->name) ? $psinfo->name : '');

            $iteminfo['quantity'] = (!empty($psinfo->quantity) ? $psinfo->quantity : '');

            $iteminfo['price'] = (!empty($psinfo->price) ? $psinfo->price : '');

            $iteminfo['unit'] = (!empty($psinfo->unit) ? $psinfo->unit : '');

            $iteminfo['product_code'] = (!empty($psinfo->product_code) ? $psinfo->product_code : '');

            $item_arr[] = $iteminfo;

            }

            $coupon_name = DB::table('coupon_code')->where('id','=',$coupon_id)->value('name'); 

            $data['order_id'] = $order_id;

            $data['item_info'] = $item_arr;

            $data['vendor_email'] = $vendor_info->email;

            $data['vendor_id'] = $vendor_info->vendor_id;

            $data['mob_no'] = $vendor_info->contact_no; 

            $data['vendor_maddress'] = $vendor_info->mailing_address;
            $data['vendor_address'] = $vendor_info->address;
            $data['vendor_city'] = $vendor_info->city;
            $data['vendor_state'] = $vendor_info->state;
            $data['vendor_zipcode'] = $vendor_info->zipcode; 

            $data['pickupdel'] = $pickupdel;
            $data['pickup_add'] = $pickup_add;

            $data['sales_tax'] = $vendor_info->sales_tax; 

            $data['excise_tax'] = $vendor_info->excise_tax; 

            $data['city_tax'] = $vendor_info->city_tax; 

            $data['driver_license'] = $vendor_info->permit_number;  

            $data['vendor_name'] = (!empty($vendor_info->name) ? $vendor_info->name : '').' '.(!empty($vendor_info->last_name) ? $vendor_info->last_name : '');

            $data['business_name'] = $vendor_info->business_name; 

            $data['user_name'] = (!empty($firstName) ? $firstName : '').' '.(!empty($lastName) ? $lastName : '');

            $data['user_dob'] = $user_dob;

            $data['user_email'] = $email;

            $data['order_msg'] = ($results['transactionResponse']['responseCode']=='1' ? '' : 'OOPs! Payment transaction has been failed.');

            $data['order_msg'] = ('');

            $data['accountNumber'] = $accountNumber;

            $data['payment_method'] = $cardtype;

            $data['order_service_tax'] = $saletax;

            $data['order_excise_tax'] = $excisetax; 

            $data['order_city_tax'] = $citytax;

            $data['order_delivery_fee'] = $deliveryfee;

            $data['order_ticket_fee'] = $ticket_fee;
                
            $data['order_service_fee'] = $ticket_service_fee; 

            $data['order_subtotal_amt'] = $sub_total;

            $data['order_promo_cal'] = $promo_amount; 

            $data['coupon_name'] = $coupon_name; 

            $data['order_total'] = $final_amount;

            $data['date_time'] = date('m/d/Y H:i:s'); 
            $data['user_id'] = $user_id;
            $data['address'] = $address;
            $data['addressd'] = $addressd;
            $data['city'] = $city;
            $data['cityd'] = $cityd;
            $data['state'] = $state;
            $data['stated'] = $stated;
            $data['zip'] = $zip;
            $data['zipd'] = $zipd;
            $data['country'] = $country;
            $data['countryd'] = $countryd;

            $data['user_mob'] = $request->user_mob;

            $data['verification_code'] = $verification_code;

    $event_id = DB::table('orders_ps')->where('order_id', '=', $id)->value('ps_id');

    $event_type = DB::table('product_service')->where('id', '=', $event_id)->value('type');

    $event_detail = DB::table('product_service')->where('id', '=', $event_id)->first();

           if($event_type==3){

                $ticketqty = $item_arr[0]['quantity']; 

                for ($i=0; $i < $ticketqty; $i++) { 

                // QR Code Start

                $ticket_id = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 16); 
                $ticket_id = strtoupper($ticket_id);    

                $qrurl = url('qrcode/qrcode.php'); 
                $curl = curl_init($qrurl);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $qrdata = "vendor_id=$vendor_id&order_id=$ticket_id";

                curl_setopt($curl, CURLOPT_POSTFIELDS, $qrdata);

                $qrresult = curl_exec($curl);
                curl_close($curl);

                $qrcodeurl = url('qrcode/temp/'.trim($qrresult));    

                DB::table('orders')->where('id', '=', $id)->update(['ticket_id' => $ticket_id,'ticket_qrcode' => $qrcodeurl]);

                    $qrdata = array(
                        'order_id' => $id,
                        'ticket_id' => $ticket_id,
                        'ticket_qrcode' => $qrcodeurl,
                        'created_at' => date('Y-m-d H:i:s')
                    );


                DB::table('book_multiple_ticket')->insert($qrdata);

                if($i!=0){

                $data['user_name'] = $firstNameT[$i-1] . ' ' . $lastNameT[$i-1];

                 }

                $data['qrcodeimg'] = $qrcodeurl;
                $data['ticket_id'] = $ticket_id;

                $data['price'] = $event_detail->price;
                $data['event_name'] = $event_detail->name;
                $data['venue_name'] = $event_detail->venue_name;
                $data['venue_address'] = $event_detail->venue_address;
                $data['event_date'] = $event_detail->event_date;
                $data['event_start_time'] = $event_detail->event_start_time;
                $data['event_end_time'] = $event_detail->event_end_time;
                $data['description'] = $event_detail->description;


    $data['advertising_image'] = DB::table('admin_setting')->where('id', '=', 1)->value('advertising_image');

//QR Code End
// pdf generation code start
                
if($i!=0){ $data['user_name'] = $firstNameT[$i-1] . ' ' . $lastNameT[$i-1];}

              $generated_filename = strtoupper($this->generateRandomString(6));
              $data['generated_filename'] = $generated_filename; 

              $pdfurl = url('/public/fpdf1/fpdfcode.php'); 

                    $curl = curl_init($pdfurl);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                
                    $payload = json_encode($data); 

                    curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

                    $qrresult = curl_exec($curl);
                    curl_close($curl);   
   
                Mail::send('emails.event_detail', $data, function ($message) use ($data) {

        $message->attach(asset("/public/fpdf1/gen_file")."/".$data['generated_filename'].".pdf");

                $message->from(config("app.webmail"), config("app.mailname"));

                $message->to($data['user_email']);

                $message->cc('nick@heymobie.com');

                $message->subject('Grambunny - Ticket Confirmation');

                }); 

                if(!empty($vendor_info->vendor_id)){

                    $data['vendor_email'] = $vendor_info->email;

                    $vendormob = $vendor_info->mob_no;

                    $data['order_msg'] = ('You have received a new order, please accept or reject the order in order management.');

                    $data['order_msg'] = ($results['transactionResponse']['responseCode'] == '1' ? 'You have received a new order, please accept or reject the order in order management.' : 'OOPs! Client new order payment transaction has been failed.');

                    $data['vendor_name'] = (!empty($vendor_info->name) ? $vendor_info->name : '') . ' ' . (!empty($vendor_info->last_name) ? $vendor_info->last_name : '');

                    $data['user_name'] = (!empty($firstName) ? $firstName : '') . ' ' . (!empty($lastName) ? $lastName : '');

                    if($i!=0){ $data['user_name'] = $firstNameT[$i-1] . ' ' . $lastNameT[$i-1]; }

                    Mail::send('emails.event_detail', $data, function ($message) use ($data) {

                        $message->attach(asset("/public/fpdf1/gen_file")."/".$data['generated_filename'].".pdf");

                        $message->from(config("app.webmail"), config("app.mailname"));
                        $message->to($data['vendor_email']);      
                        $message->cc('nick@heymobie.com');
                        $message->subject('Grambunny - Ticket Confirmation');
                    }); 

                }


            DB::table('book_multiple_ticket')->where('ticket_id', '=', $ticket_id)->update(['ticket_pdf' => $generated_filename.'.pdf']);

              $pdf_file = DB::table('book_multiple_ticket')->where('ticket_id', '=', $ticket_id)->value('ticket_pdf'); 

               $smstext = "Ticket Booked : https://grambunny.com/public/fpdf1/gen_file/".$pdf_file; 

               //$smstext = "Ticket Booked : Your Ticket Id - ".$ticket_id;

               $to = $request->user_mob;

               $sendpdf = $this->sendEventSms($smstext, $to); 

               }

            }else{ 


            Mail::send('emails.order_detail', $data, function ($message) use ($data) {
            $message->from(config("app.webmail"), config("app.mailname"));
            $message->to($data['user_email']);
            $message->cc('nick@heymobie.com');
            $message->subject('Grambunny - Order Confirmation');
            });

            
            if (!empty($vendor_info->vendor_id)){

                $data['vendor_email'] = $vendor_info->email;

                $vendormob = $vendor_info->mob_no;

                $data['order_msg'] = ('You have received a new order, please accept or reject the order in order management.');

                $data['order_msg'] = ($results['transactionResponse']['responseCode']=='1' ? 'You have received a new order, please accept or reject the order in order management.' : 'OOPs! Client new order payment transaction has been failed.');

                $data['vendor_name'] = (!empty($vendor_info->name) ? $vendor_info->name : '').' '.(!empty($vendor_info->last_name) ? $vendor_info->last_name : '');

               $data['user_name'] = (!empty($firstName) ? $firstName : '').' '.(!empty($lastName) ? $lastName : '');

                 Mail::send('emails.order_detail', $data, function ($message) use ($data){

                    $message->from(config("app.webmail"), config("app.mailname"));
                    $message->to($data['vendor_email']);
                    $message->cc('nick@heymobie.com');
                    $message->subject('Grambunny - New Order Confirmation');

                }); 

            }

        }

            $request->session()->forget('promo_amount');

            $request->session()->forget('coupon_id');

            $request->session()->forget('ps_qty');

            $request->session()->forget('promo_qty');

            $request->session()->forget('vendor_id');

            $request->session()->forget('ps_slug');

            $divice_token = DB::table('vendor_device_token')

                                ->where('userid', '=', $vendor_id)

                                ->where('notification_status', '=',1)

                                ->first();

          $user_divice_token = DB::table('users')->where('id', '=', $user_id)->first();                                  
          if($divice_token)
            {
    
                 $title = "New order received : Id#".$order_id;

                 $tag = "Order detail";

                 $token = $divice_token->devicetoken;

                 $idos = $id;

                 $vid = $vendor_info->vendor_id;

                $response = $this->sendNotification($token,$tag,$title);

                 if($response){

                  $this->save_notification_driver($user_id,$title,$idos,$tag,$vid);

                 }

            } 


            if($divice_token)
            {

                 $title = "Hey ".$user_divice_token->name." ".$user_divice_token->lname." your order is Pending";

                 $tag = "Order detail";

                 $token = $user_divice_token->devicetoken;

                 $idos = $id;

                 $vid = $vendor_info->vendor_id;

                $notistatus =  $user_divice_token->notification;

                if($notistatus==1){ 

                 $response = $this->sendNotification($token,$tag,$title);

                }
                 
                $this->save_notification($user_id,$title,$idos,$tag,$vid);

            } 

            //$cartdelete = DB::table('cart')->where('user_id',$user_id)->delete(); 

            $ordermsg = $data['order_msg'];

            //$smstext = "Your order have been successfully created and verification code is :".$verification_code;

            $smstext = "Your order have been successfully created";

            $vmobile=$vendor_info->mob_no;

            $vsmstext = "New Order has been received and Order Id : #".$order_id;
           // $this->sendSmstwillo($user_mob, $smstext);

           // $this->sendSmstwillo($vmobile, $vsmstext);

            $this->sendSms($smstext,$user_mob,$vendormob,$ordermsg);

            return json_encode(array('status'=>$ajxstatus,'msg'=>'Order has been generated successfully!','order_id'=>base64_encode($id),'redirect_url'=>$redirect_url));

        }else{ 


            if($results['transactionResponse']['responseCode']=='1'){

                return json_encode(array('status'=>0,'msg'=>"Payment has been done. But order has not been generated. Please contact to merchant for this issue."));   

             }else{

                return json_encode(array('status'=>0,'msg'=>'Your order could not be processed. Please try again.'));    

             } 
            
          }

        }else{

        return json_encode(array('status'=>0,'msg'=>'Your order could not be processed. Cart is empty.')); 
       
        } 

    }


    function NVPToArray($NVPString)
    {
        $proArray = array();
        while(strlen($NVPString))
        {
            // name
            $keypos= strpos($NVPString,'=');
            $keyval = substr($NVPString,0,$keypos);
            // value
            $valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
            $valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
            // decoding the respose
            $proArray[$keyval] = urldecode($valval);
            $NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
        }
        return $proArray;
    }

    function sendSmstwillo($number,$body){
        $account_sid = 'AC6e4b89ea09330c627c016b271e15f2cb';
        $auth_token = 'ecc5ea0b633b8e55c290428a7bc4b413';

        $url = "https://api.twilio.com/2010-04-01/Accounts/$account_sid/SMS/Messages";
        $to = $number;//"+12014222748";//"(909) 504-9412";/"(626) 716-0558";/
        $from = "(310) 861-2683"; // twilio trial verified number 

        $data = array (
            'From' => $from,
            'To' => $to,
            'Body' => $body,
        );

        $post = http_build_query($data);
        $x = curl_init($url);
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_USERPWD, "$account_sid:$auth_token");
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
        curl_close($x);
    }



    function sendSms($smstext,$to,$vendormob,$ordermsg)
    {
            $phone = str_replace("(","",$to); 
            $phoneno = str_replace(") ","",$phone); 
            $phonenos = str_replace("-","",$phoneno);

            $mobile = '1'.$phonenos;

            $data = array(
              'from'=> '12013002832',
              'text'=> $smstext,
              'to'=> $mobile,
              'api_key'=> 'c2aa3522',
              'api_secret'=> 'o5NWJ25e5FbszCxH'

            );
   
         $sendsms = json_encode($data, JSON_FORCE_OBJECT);

          $curl = curl_init();

          curl_setopt_array($curl, array(
          CURLOPT_URL => "https://rest.nexmo.com/sms/json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>$sendsms,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;

        $phonev = str_replace("(","",$vendormob); 
        $phonenov = str_replace(") ","",$phonev); 
        $phonenosv = str_replace("-","",$phonenov);

        $mobilev = '1'.$phonenosv;    

        $data = array(
              'from'=> '12013002832',
              'text'=> $ordermsg,
              'to'=> $mobilev,
              'api_key'=> 'c2aa3522',
              'api_secret'=> 'o5NWJ25e5FbszCxH'

           );
   
         $sendsms = json_encode($data, JSON_FORCE_OBJECT);


          $curl = curl_init();

          curl_setopt_array($curl, array(
          CURLOPT_URL => "https://rest.nexmo.com/sms/json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS =>$sendsms,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return 1 ;

    }


function sendNotification($token,$title,$message)
{

$serverKey='AAAAScDfAKY:APA91bGRbTNXI3EEoabhjKa8slHwL9nPSJXiDCtKaTAr7kXWZjA6HojfZUAQwXIrDwr6lDfocP6ZstegPYlwK42XAUET5ULYGZtnvGjrhhTmAaTf8StngdcHjdDJnHpVZEmwSCIhJwUj';

$data = array("to" => $token, "notification" => array( "title" => $title, "body" => $message,'sound' => 'default'));

$data_string = json_encode($data);

$headers = array ( 'Authorization: key='.$serverKey, 'Content-Type: application/json' );

$ch = curl_init(); curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);
$result = curl_exec($ch);
curl_close ($ch);

return $result; 

}

function save_notification($user_id,$title,$orderid,$tag,$vid){

$notifiy = new Notification_list;

$notifiy->order_id = $orderid;

$notifiy->noti_userid = $user_id;

$notifiy->noti_title = $tag;

$notifiy->noti_desc = $title;

$notifiy->vendor_id = $vid;

$notifiy->save();

  }

function save_notification_driver($user_id,$title,$orderid,$tag,$vid){

$notifiy = new Notification_vendor_list;

$notifiy->order_id = $orderid;

$notifiy->noti_userid = $user_id;

$notifiy->noti_title = $tag;

$notifiy->noti_desc = $title;

$notifiy->vendor_id = $vid;

$notifiy->save();

}



    public function generateRandomString($length = 10) {

        //$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $characters = '0123456789';

        $charactersLength = strlen($characters);

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

        }

        return $randomString;

    }


    public function thank_you_page(Request $request)
    {

        Session::forget('guest_id');
        Session::forget('coupon_code');

        $order_id = base64_decode($request->id);

        if(isset($_GET['status'])){

        if(!empty($_GET['paymentID'])){ 

        $paymentID =  $_GET['paymentID']; 
        $msg = $_GET['msg'];

        DB::table('orders')->where('id','=',$order_id)->update(['pay_status' => $msg,'txn_id' => $paymentID]);

       }else{

       $msg = 'Failed';//$_GET['msg']; 

       DB::table('orders')->where('id','=',$order_id)->update(['transaction_tag' => $_GET['msg'],'pay_status' => $msg]);

       }

        $data['pstatus']  =  $_GET['status'];
        $data['pmsg']  =  $_GET['msg'];

      }else{

        $data['pstatus']  =  1;
        $data['pmsg']  =  '';

      }

        $order_info = DB::table('orders')->where('id','=',$order_id)->first();

         $vendor_order = DB::table('orders')

        ->where('id', '=' ,$order_id)

        ->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

        ->first();

        $data['pslist'] = DB::table('orders_ps')

        ->where('order_id', '=' ,$order_info->id)

        ->get();

       // print_r($data['pslist']);die;

        $data['excise_tax_percent']  =  $vendor_order->excise_tax_percent;
        $data['sales_tax_percent']  =  $vendor_order->sales_tax_percent;
        $data['city_tax_percent']  =  $vendor_order->city_tax_percent;

        //print_r($vendor_order);die;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $data['product_info'] = DB::table('product_service')->where('id','=',$order_info->ps_id)->first();

        $data['order_info'] = $order_info;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $ps_info = DB::table('cart')->where('user_id','=',$order_info->user_id)->where('vendor_id','=',$order_info->vendor_id)->get();

        $ps_ids = array();

        foreach( $ps_info as $ps){

            $ps_ids[] = $ps->ps_id;

        } 

        //print_r($ps_ids);die;
        $ps_info=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->whereIn('product_service.id',$ps_ids)->where('crt.user_id',$order_info->user_id)->get();

        //echo "<pre>"; print_r($ps_info);die;
        $item_arr =array();  

        foreach( $ps_info as $psinfo){

            $iteminfo['ps_type'] = (!empty($psinfo->ps_type) ? $psinfo->ps_type : '');

            $iteminfo['name'] = (!empty($psinfo->name) ? $psinfo->name : '');

            $iteminfo['quantity'] = (!empty($psinfo->quantity) ? $psinfo->quantity : '');

            $iteminfo['price'] = (!empty($psinfo->price) ? $psinfo->price : '');

            $iteminfo['ticket_service_fee'] = (!empty($psinfo->ticket_service_fee) ? $psinfo->ticket_service_fee : '');
            $iteminfo['ticket_fee'] = (!empty($psinfo->ticket_fee) ? $psinfo->ticket_fee : '');

            $item_arr[] = $iteminfo;

        }

        $data['item_info'] = $item_arr;

        $data['market_area'] = DB::table('vendor')->where('vendor_id','=',$order_info->vendor_id)->value('pick_up_address');

        //echo "<pre>";print_r($data['item_info']); die;

        DB::table('cart')->where('user_id','=',$order_info->user_id)->delete();

        $request->session()->put('trackid', $order_id);

        return view("thank-you-page")->with($data);   

    }



    public function successful(Request $request)
    {

        $data['pstatus']  = '';

        if(isset($_GET['oid'])){

        Session::forget('guest_id');
        Session::forget('coupon_code');

        $orid = $_GET['oid'];

        $orderid = explode("-",$orid);

        if(isset($orderid[1])){

        $order_id = $orderid[1];

        DB::table('orders')->where('order_id','=',$order_id)->update(['pay_status' => 'success']);

        $order_info = DB::table('orders')->where('order_id','=',$order_id)->first();

         $vendor_order = DB::table('orders')

        ->where('order_id', '=' ,$order_id)

        ->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

        ->first();

        $data['pslist'] = DB::table('orders_ps')

        ->where('order_id', '=' ,$order_info->id)

        ->get();

        $data['pstatus']  =  1;

       // print_r($data['pslist']);die;

        $data['excise_tax_percent']  =  $vendor_order->excise_tax_percent;
        $data['sales_tax_percent']  =  $vendor_order->sales_tax_percent;
        $data['city_tax_percent']  =  $vendor_order->city_tax_percent;

        //print_r($vendor_order);die;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $data['product_info'] = DB::table('product_service')->where('id','=',$order_info->ps_id)->first();

        $data['order_info'] = $order_info;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $ps_info = DB::table('cart')->where('user_id','=',$order_info->user_id)->where('vendor_id','=',$order_info->vendor_id)->get();

        $ps_ids = array();

        foreach( $ps_info as $ps){

            $ps_ids[] = $ps->ps_id;

        } 

        //print_r($ps_ids);die;
        $ps_info=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->whereIn('product_service.id',$ps_ids)->where('crt.user_id',$order_info->user_id)->get();

        //echo "<pre>"; print_r($ps_info);die;
        $item_arr =array();  

        foreach( $ps_info as $psinfo){

            $iteminfo['ps_type'] = (!empty($psinfo->ps_type) ? $psinfo->ps_type : '');

            $iteminfo['name'] = (!empty($psinfo->name) ? $psinfo->name : '');

            $iteminfo['quantity'] = (!empty($psinfo->quantity) ? $psinfo->quantity : '');

            $iteminfo['price'] = (!empty($psinfo->price) ? $psinfo->price : '');

            $iteminfo['ticket_service_fee'] = (!empty($psinfo->ticket_service_fee) ? $psinfo->ticket_service_fee : '');
            $iteminfo['ticket_fee'] = (!empty($psinfo->ticket_fee) ? $psinfo->ticket_fee : '');

            $item_arr[] = $iteminfo;

        }

        $data['item_info'] = $item_arr;

        $data['market_area'] = DB::table('vendor')->where('vendor_id','=',$order_info->vendor_id)->value('pick_up_address');

        //echo "<pre>";print_r($data['item_info']); die;

        DB::table('cart')->where('user_id','=',$order_info->user_id)->delete();

        $request->session()->put('trackid', $order_id);

        return view("thank-you-page")->with($data); 

        }else{

          return redirect('/') ;

        }

        }else{

         return redirect('/') ;

        }  

    }


    public function failed(Request $request)
    {

        $data['pstatus']  = '';

        if(isset($_GET['oid'])){

        Session::forget('guest_id');
        Session::forget('coupon_code');

        $orid = $_GET['oid'];

        $orderid = explode("-",$orid);

        if(isset($orderid[1])){

        $order_id = $orderid[1];

         DB::table('orders')->where('order_id','=',$order_id)->update(['pay_status' => 'failed']);

        $order_info = DB::table('orders')->where('order_id','=',$order_id)->first();

         $vendor_order = DB::table('orders')

        ->where('order_id', '=' ,$order_id)

        ->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

        ->first();


        $data['pslist'] = DB::table('orders_ps')

        ->where('order_id', '=' ,$order_info->id)

        ->get();


        $data['pstatus']  =  0;

       // print_r($data['pslist']);die;

        $data['excise_tax_percent']  =  $vendor_order->excise_tax_percent;
        $data['sales_tax_percent']  =  $vendor_order->sales_tax_percent;
        $data['city_tax_percent']  =  $vendor_order->city_tax_percent;

        //print_r($vendor_order);die;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $data['product_info'] = DB::table('product_service')->where('id','=',$order_info->ps_id)->first();

        $data['order_info'] = $order_info;

        $data['user_info'] = DB::table('users')->where('id','=',$order_info->user_id)->first();

        $ps_info = DB::table('cart')->where('user_id','=',$order_info->user_id)->where('vendor_id','=',$order_info->vendor_id)->get();

        $ps_ids = array();

        foreach( $ps_info as $ps){

            $ps_ids[] = $ps->ps_id;

        } 

        //print_r($ps_ids);die;
        $ps_info=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->whereIn('product_service.id',$ps_ids)->where('crt.user_id',$order_info->user_id)->get();

        //echo "<pre>"; print_r($ps_info);die;
        $item_arr =array();  

        foreach( $ps_info as $psinfo){

            $iteminfo['ps_type'] = (!empty($psinfo->ps_type) ? $psinfo->ps_type : '');

            $iteminfo['name'] = (!empty($psinfo->name) ? $psinfo->name : '');

            $iteminfo['quantity'] = (!empty($psinfo->quantity) ? $psinfo->quantity : '');

            $iteminfo['price'] = (!empty($psinfo->price) ? $psinfo->price : '');

            $iteminfo['ticket_service_fee'] = (!empty($psinfo->ticket_service_fee) ? $psinfo->ticket_service_fee : '');
            $iteminfo['ticket_fee'] = (!empty($psinfo->ticket_fee) ? $psinfo->ticket_fee : '');

            $item_arr[] = $iteminfo;

        }

        $data['item_info'] = $item_arr;

        $data['market_area'] = DB::table('vendor')->where('vendor_id','=',$order_info->vendor_id)->value('pick_up_address');

        //echo "<pre>";print_r($data['item_info']); die;

        DB::table('cart')->where('user_id','=',$order_info->user_id)->delete();

        $request->session()->put('trackid', $order_id);

        return view("payment_failed")->with($data); 

         }else{

         return redirect('/') ;

        }   

        }else{

         return redirect('/') ;

        }   

    }


    public function track_order(Request $request)
    {

        $order_id = $request->id;

        if($request->id){ 

        //$order_id = session('trackid'); 

        $order_info = DB::table('orders')->where('id','=',$order_id)->first();

        //print_r($order_info); die;

        if(!empty($order_info->addressd)){

        $data['user_address'] = $order_info->addressd.','.$order_info->cityd.','.$order_info->stated.','.$order_info->zipd; 
        }else{

        $data['user_address'] = $order_info->address.','.$order_info->city.','.$order_info->state.','.$order_info->zip; 

        }

        $vendor_id = $order_info->vendor_id;

        $vendor_info = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

        $market_area = $vendor_info->market_area;

        $data['market_area'] = $market_area;

        return view("track_order")->with($data);

      }else{ return redirect('/my-orders') ; }      

    }


  public function location()
    {

        $order_id = session('trackid'); 

        $order_info = DB::table('orders')->where('id','=',$order_id)->first();

        $vendor_id = $order_info->vendor_id;

        $latlong = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

        $lat = $latlong->lat;

        $lng = $latlong->lng;

        echo '{"lat":"'.$lat.'", "lng":"'.$lng.'"}';   

    }


    public function cart(Request $request)

    {

        $validator = Validator::make($request->all(), [

            'qty' => 'numeric',

        ]);

        if ($validator->fails()) {

            abort(404);

        }

        if (!empty(auth()->guard('user')->user())) {

            $user_id = auth()->guard('user')->user()->id;

            $user_info = DB::table('users')->where('id','=',$user_id)->first();

            $delivery_addresss = DB::table('delivery_address')->where('del_userid','=',$user_id)->get();

        } else {

            $user_info = array();

            $user_id = Session::get("_token");

            $delivery_addresss = array();

        }

        //$item=Productservice::where("slug",$request->slug)->where("status",1)->first();

        //$items=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

         $items1=Productservice::select('product_service.*', 'crt.quantity', 'product_service.quantity as pquantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();
        
        foreach ($items1 as $item) {
           $quantity = $item->quantity;
           $pquantity = $item->pquantity;
           if($pquantity < $quantity){
                 DB::table('cart')->where('ps_id',$item->id)->delete();
                  return redirect('/') ; 
           }
        }

        $items=Productservice::select('product_service.*', 'crt.quantity', 'product_service.quantity as pquantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        $request->session()->put('ps_slug', $request->slug);

        $qty = session('ps_qty');

        $request->session()->put('cart_uid', $user_id);


       /* if (empty($qty) || is_null($qty)){ 

            //return redirect($request->slug.'/details');

            $request->session()->put('ps_qty', 1);

            $qty = session('ps_qty');

        } */



        // if (!empty(session('vendor_id'))) 

        // {

        //     if ($item->vendor_id != session('vendor_id')) {

        //         $request->session()->forget('promo_amount');

        //         $request->session()->forget('promo_qty');

        //         $request->session()->forget('vendor_id');

        //         $request->session()->forget('coupon_id');

        //     } elseif(session('promo_qty') != $qty) {

        //         $request->session()->forget('promo_amount');

        //         $request->session()->forget('promo_qty');

        //         $request->session()->forget('vendor_id');

        //         $request->session()->forget('coupon_id');

        //     }

        // }



        if (empty(session('promo_amount')) || is_null(session('promo_amount'))) {

            $promo_amount = 0;

        } else {

            $promo_amount = session('promo_amount');

        }
 

        $state_list = DB::table('states')->get();

        if(sizeof($items) ){

            return view("cart",["items" => $items,"qty" => $qty,"promo_amount" => $promo_amount,"user_info" => $user_info,'state_list' => $state_list,'user_id' => $user_id,'delivery_addresss' => $delivery_addresss]);

        }else{ 

            return redirect('/') ; 

        } 

    }


    public function checkout(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'qty' => 'numeric',

        ]);

        if ($validator->fails()) {

            abort(404);

        }

        $user_id =  '';
        $last_data_get = '';  

        //dd(Session::get('guest_id'));


        if (!empty(auth()->guard('user')->user())) {

        $user_id = auth()->guard('user')->user()->id;

        DB::table('cart')->where('user_id',Session::get("_token"))->update(['user_id' => $user_id]); 

        $check_cart = DB::table('cart')->where('user_id',$user_id)->get();

        $cart_count = count($check_cart);

        if(count($check_cart)==0){ return redirect()->to('/'); }

         $user_status = 1; 
         $last_data_get = '';  

        }else{

        /* $user_id =  Session::get("_token");

        $user_status = 2;
 
        $check_cart = DB::table('cart')->where('user_id',$user_id)->get();

        $cart_count = count($check_cart);

        if(count($check_cart)==0){ return redirect()->to('/'); } */

        return redirect()->to('/'); 

     }

       if(empty($user_id)){
      
        return redirect()->to('/');

       }


        //$items = Productservice::select('product_service.*', 'crt.quantity', 'crt.ps_id')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

         $items1 = Productservice::select('product_service.*', 'crt.quantity', 'crt.ps_id', 'product_service.quantity as pquantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        foreach ($items1 as $item) {
           $quantity = $item->quantity;
           $pquantity = $item->pquantity;
           if($pquantity < $quantity){
                DB::table('cart')->where('ps_id',$item->id)->delete();
           }
        }

        $items = Productservice::select('product_service.*', 'crt.quantity', 'crt.ps_id', 'product_service.quantity as pquantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();



        $cart_info = DB::table('cart')->where('user_id','=',$user_id)->first();

        $vendor_id = !empty( $cart_info ) ? $cart_info->vendor_id : '';

        $product_type = !empty($cart_info) ? $cart_info->ps_type : '';
        $ticket_qty = !empty($cart_info) ? $cart_info->quantity : '';

        $total_price = 0;

        foreach( $items as $item){

            $total_price = $total_price +  ( $item->quantity*$item->price );  

        }

        //$total_price = number_format( $total_price, 2);


        //$all_price = Productservice::select('product_service.*', 'crt.quantity', 'crt.ps_id')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->sum('product_service.price');



        //print_r($all_price);die;

        // if (empty($qty) || is_null($qty)){ 

        //     //return redirect($request->slug.'/details');

        //     $request->session()->put('ps_qty', 1);

        //     $qty = session('ps_qty');

        // }




        if (empty(session('promo_amount')) || is_null(session('promo_amount'))) {

            $promo_amount = 0;

        } else {

            $promo_amount = session('promo_amount');

        }

        if (empty(session('coupon_code')) || is_null(session('coupon_code'))) {

            $coupon_code = 0;

        } else {

            $coupon_code = session('coupon_code');

        }


        if (!empty(auth()->guard('user')->user())) {

            $user_id = auth()->guard('user')->user()->id;

            $user_info = DB::table('users')->where('id','=',$user_id)->first();

            $delivery_addresss = DB::table('delivery_address')->where('del_userid','=',$user_id)->get();

        }else{

            $user_info = array();

            $delivery_addresss = array();

        }


        $state_list = DB::table('states')->get(); 
             // echo "<pre>";
        // print_r($items);die;

        if(sizeof($items)){

        if($product_type==3){ 

            return view("event-checkout", ["items" => $items, "promo_amount" => $promo_amount, "coupon_code" => $coupon_code, "user_info" => $user_info, 'state_list' => $state_list, 'user_id' => $user_id, 'delivery_addresss' => $delivery_addresss, 'vendor_id' => $vendor_id, 'total_price' => $total_price, 'product_type' => $product_type, 'ticket_qty' => $ticket_qty, 'last_data_get' => $last_data_get,'user_status' => $user_status,'cart_count' => $cart_count]); 

        }else{    

            return view("checkout-page",["items" => $items, "promo_amount" => $promo_amount, "coupon_code" => $coupon_code, "user_info" => $user_info,'state_list' => $state_list,'user_id' => $user_id,'delivery_addresss' => $delivery_addresss, 'vendor_id' => $vendor_id, 'total_price' => $total_price,'user_status' => $user_status,'cart_count' => $cart_count]); 
         }   

        }else{

            return abort(404);

        }

    }


    public function check_same_merchant(Request $request)
    {   

        $user_id = $request->_token;

        if(!empty(auth()->guard('user')->user())){

            $user_id = auth()->guard('user')->user()->id;
        }

        //$product_id = (!empty($request->product_id)) ? $request->product_id : '';
        $vendor_id = (!empty($request->vendor_id)) ? $request->vendor_id : '';

        $store1 = '';
        $store2 = '';
      
            $cart =  new Cart;

            $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->count();

            if( $count > 0 ){

            $mstatus = 1;    

             }else{ 

            $count1 = DB::table('cart')->where('vendor_id','!=' ,$vendor_id)->where('user_id',$user_id)->get();  

            if( count($count1) > 0 ){ 

            $mstatus = 0;  
            $mname = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('name');
            $last_name = DB::table('vendor')->where('vendor_id','=',$vendor_id)->value('last_name');

            $store2 = $mname.' '.$last_name;

            $vendor1 = $count1[0]->vendor_id;  

            $mname1 = DB::table('vendor')->where('vendor_id','=',$vendor1)->value('name');
            $last_name1 = DB::table('vendor')->where('vendor_id','=',$vendor1)->value('last_name');

            $store1 = $mname1.' '.$last_name1; 

             }else{  

            $mstatus = 1;           

              }

            }

    return response()->json(["mstatus" => $mstatus,"msg" => 'Check same merchant',"store1" => $store1,"store2" => $store2]);

    }



    public function quantity(Request $request)
    {   
        
        Session::forget('coupon_code');
          
        $user_id = $request->_token;

        if(!empty(auth()->guard('user')->user())){

            $user_id = auth()->guard('user')->user()->id;

        }else{

        if(!empty(Session::get('guest_id'))){

        $user_id =  Session::get('guest_id');

         }

       }

        $product_id = (!empty($request->product_id)) ? $request->product_id : '';

        $vendor_id = (!empty($request->vendor_id)) ? $request->vendor_id : '';

        $qty = $request->qty;

        $pro_type = DB::table('product_service')->where('id', $product_id)->value('type');

        //promo amount will be 0 when product added/removed/updated into cart        

        $request->session()->put('promo_amount', 0); 


        if( isset($request->update) ){

            DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->update(['quantity' => $qty]);

           //return response()->json(["msg" => 'Updated successfully']);

        }elseif( isset($request->delete) ){

            DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->delete();

            //return response()->json(["msg" => 'Deleted successfully']);

        }else{

            $count = DB::table('cart')->where('user_id', $user_id)->where('ps_id', $product_id)->where('ps_type', $pro_type)->where('vendor_id', $vendor_id)->count();

            if( $count > 0 ){

            $qtyold = DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

            $newqty =  $qtyold+$qty; 

                DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->update(['quantity' => $newqty]);

             }else{

                
              // $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->count();

                //if( $count == 0){

              DB::table('cart')->where('vendor_id','!=' ,$vendor_id)->where('user_id',$user_id)->delete();

              DB::table('cart')->where('vendor_id', $vendor_id)->where('user_id', $user_id)->where('ps_type', '!=', $pro_type)->delete();

                if($pro_type==3){

                DB::table('cart')->where('vendor_id', $vendor_id)->where('user_id', $user_id)->where('ps_type', $pro_type)->delete();

                }

               // }

                $cart =  new Cart;

                $cart->user_id = $user_id;
                $cart->ps_id = $product_id;
                $cart->ps_type = $pro_type;
                $cart->vendor_id = $vendor_id;
                $cart->quantity = $qty;
                $cart->save();

            }

            //return response()->json(["msg" => 'Added successfully']);

        }

    $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->sum('cart.quantity');

    $request->session()->put('ps_qty', $count);

    $request->session()->put('cart_uid', $user_id);

    return response()->json(["cartcount" => $count,"msg" => 'Product Added Successfully!']);

    }


    public function addtocart(Request $request)
    {   

        $user_id = $request->_token;

        if (!empty(auth()->guard('user')->user())) {

            $user_id = auth()->guard('user')->user()->id;
        }

        $product_id = (!empty($request->product_id)) ? $request->product_id : '';

        $vendor_id = (!empty($request->vendor_id)) ? $request->vendor_id : '';

        $qty = $request->qty;    

        $pro_type = DB::table('product_service')->where('id', $product_id)->value('type');  

        $request->session()->put('promo_amount', 0); 
    
            $count = DB::table('cart')->where('user_id', $user_id)->where('ps_id', $product_id)->where('ps_type', $pro_type)->where('vendor_id', $vendor_id)->count();

            if( $count > 0 ){

            $qtyold = DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

            $newqty =  $qtyold+$qty; 

                DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->update(['quantity' => $newqty]);

             }else{

            DB::table('cart')->where('vendor_id','!=' ,$vendor_id)->where('user_id',$user_id)->delete();

            DB::table('cart')->where('vendor_id', $vendor_id)->where('user_id', $user_id)->where('ps_type', '!=', $pro_type)->delete();

            if($pro_type==3){

            DB::table('cart')->where('vendor_id', $vendor_id)->where('user_id', $user_id)->where('ps_type', $pro_type)->delete();

            }  

                $cart =  new Cart;
                $cart->user_id = $user_id;
                $cart->ps_id = $product_id;
                $cart->ps_type = $pro_type;
                $cart->vendor_id = $vendor_id;
                $cart->quantity = $qty;
                $cart->save();

            }

 
    $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->sum('cart.quantity');

    $request->session()->put('ps_qty', $count);

    $request->session()->put('cart_uid', $user_id);

    return response()->json(["cartcount" => $count,"msg" => 'Product Added Successfully!']);

    }


    public function validateCouponCode(Request $request)

    {

        $pid=decrypt($request->pid);



        $item= Productservice::find($pid);

        $coupon=Coupon::where("coupon",$request->coupon)->where("vendor_id",$item->vendor->vendor_id);

        if($coupon->exists()){

            $isValid=$coupon->where("valid_till",">",Carbon::now()->format("Y-m-d"))->exists();

            if($isValid){

                $validCoupon=$coupon->first();

                $couponUsage=DB::table("coupon_logs")->where("coupon_id",$validCoupon->id);

                if($validCoupon->usage_limit > $couponUsage->count()){

                    $couperUsagePerUser=$couponUsage->where("user_id",auth()->guard('user')->user()->id);

                    if($validCoupon->per_user > $couperUsagePerUser->count()){

                        return response()->json(["coupon is working!!"]);

                    }else{

                        return response()->json(["user limit"]);

                    }

                }else{

                    return response()->json(["limit"]);

                }

                //return response()->json(["code" => 1,"message" => "Coupon Code Applied.","data" => []])

            }

            return response()->json(["coupon is working s".$coupon->first()]);

        }    

        return response()->json(["coupon not working ".$item->vendor->vendor_id]);

    }

     function sendEventSms($smstext, $to)
    {

        $phone = str_replace("(", "", $to);
        $phoneno = str_replace(") ", "", $phone);
        $phonenos = str_replace("-", "", $phoneno);

        $mobile = '1' . $phonenos;

        $data = array(
            'from' => '12013002832',
            'text' => $smstext,
            'to' => $mobile,
            'api_key' => 'c2aa3522',
            'api_secret' => 'o5NWJ25e5FbszCxH'

        );

        $sendsms = json_encode($data, JSON_FORCE_OBJECT);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.nexmo.com/sms/json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $sendsms,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        //return $response;

        curl_close($curl);

        return 1;

    }

    function createpdf(){

                $data['qrcodeimg'] = 'https://www.grambunny.com/qrcode/temp/G0EFAU8TCRKK8GKS1697290962.png';
                $data['order_id'] = '141';
                $data['ticket_id'] = 'G0EFAU8TCRKK8GKS';
                $data['price'] = '100';
                $data['event_name'] = 'Grambunny';
                $data['venue_name'] = 'Testvanue';
                $data['venue_address'] = 'vijaynagar indore';
                $data['event_date'] = '25-10-23';
                $data['event_start_time'] = '10am';
                $data['event_end_time'] = '10pm';
                $data['description'] = 'Test description';
                $data['user_name'] = 'Rahul';

            $generated_filename = strtoupper($this->generateRandomString(6));
            $data['generated_filename'] = $generated_filename; 
            $pdfurl = url('/public/fpdf1/fpdfcode.php');                 
            $curl = curl_init($pdfurl);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);                

            $payload = json_encode($data); 

            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

            header('Content-type: application/pdf');

            $qrresult = curl_exec($curl);

            curl_close($curl);  

            //echo $qrresult; 

    }
       

}

