<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

Use DB;

use App\Restaurant;

use Illuminate\Support\Facades\Route;

 

use Response;

use View;

use Cart;

use Session;

use Auth;

use App\Favourite_restaurant;

use App\Review;

class ListController extends Controller

{

	public function __construct(){

	    //	$this->middleware('auth');

		if(!Session::has('SITE_LOGIN'))

		{

			return redirect()->to('/');

		}

	}


	public function restaurant_listing()

	{

		

		DB::enableQueryLog();

		$show_data_setting = 0;

		$admin_setting = DB::table('admins')->select('*')->get();

		$show_data_setting = $admin_setting[0]->show_data;

		$home_cuisine_name =  '';

		$surbur_name='';

		$post_code='';

		$search_type='';

		$state_code='';

		$loc_id='';

		$search_cuisine='';

		$home_lat='';

		$home_lng='';

		$limit = 10;

		$search_rest_type= '';



		if(Session::has('home_lat'))

		{

		//	echo 'NEW';

			$home_lat = Session::get('home_lat');

			$home_lng = Session::get('home_lng');

			$city = Session::get('city');			

		}

		else

		{

			/* $ip = '';

			$ip = $_SERVER['REMOTE_ADDR'];

			if(empty($ip)){ $ip = '122.170.204.29'; }



			$curl_handle=curl_init();

			curl_setopt($curl_handle, CURLOPT_URL,'http://www.geoplugin.net/php.gp?ip='.$ip);

			curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);

			curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

			curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');

			$query = curl_exec($curl_handle);

			curl_close($curl_handle);

			$location = (unserialize($query)); */



			$latitude =  '22.7533';//$location['geoplugin_latitude'];

			$longitude = '75.8937';//$location['geoplugin_longitude'];



		   if($latitude){ 

		   	$home_lat = $latitude; 

		   }else{ 

		   	$home_lat = "22.7533"; 

		   	$latitude = "22.7533"; 

		   }



		   if($longitude){

		    $home_lng = $longitude; 

		    }else{ 

			$home_lng = "‎75.8937"; 

			$longitude = "‎75.8937";

		   }



		   	Session::put('home_lat',$latitude );

			Session::put('home_lng',$longitude );



		}





		

		/********************** START SEARCH ACCORDING TO SURBUR ***********************/

		

		if((request()->surbru_name) && (!(request()->ajax())))

		{

			$search_surbur_name = request()->surbru_name;

				#Find latitude and longitude



			/*Google API commented*/

			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=$search_surbur_name";



			$json_data = file_get_contents($url);

			$result = json_decode($json_data, TRUE);

			$latitude = $result['results'][0]['geometry']['location']['lat'];

			$longitude = $result['results'][0]['geometry']['location']['lng'];

			Session::put('home_lat',$latitude );

			Session::put('home_lng',$longitude );

			$home_lat = $latitude;

			$home_lng = $longitude;

		}

		/******************** END SEARCH ACCORDING TO SURBUR ************************/



		if(Session::has('home_cuisine') )

		{

			$search_cuisine=Session::get('home_cuisine');

		}

		if(Session::has('home_rest_type') )

		{

			$search_rest_type=Session::get('home_rest_type');

		}

		if(Session::has('home_location') )

		{

			$surbur_name= Session::get('home_location');

			$post_code=Session::get('home_postal_code');

			$state_code=Session::get('home_level');

			//print_r($Suburb_detail);

			$from_location = array('surbur_lable'=>Session::get('home_location'),

				'surbur_name'=>Session::get('home_locality'),

				'post_code'=>Session::get('home_postal_code'),

				'home_lat'=>Session::get('home_lat'),

				'home_lng'=>Session::get('home_lng')

			);

			Session::put('from_location', $from_location);

		}

		if(request()->cuisine_name)

		{

			$home_cuisine_name =  request()->cuisine_name;

			$home_cuisine  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')

			->where('cuisine_name', '=' ,str_replace('_',' ',$home_cuisine_name))

			->orderBy('cuisine_name', 'asc')->get();

			$search_cuisine = array('0'=>$home_cuisine[0]->cuisine_id);

		}

		if(isset($_GET['foodType']))

		{

			Session::forget('home_cuisine');

			$search_cuisine=$_GET['foodType'];

		}





        $cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();

			

		

		$rest_listing = DB::table('search_restaurant_view');

		print_r($rest_listing)

		$Unine_listing = DB::table('search_restaurant_view as s1');



		if( (!empty($home_lat)) && (!empty($home_lng)))

		{

			$KM = 1;

			$MILE = "0.621371";

			$distang_range = 5;

	        //?page=1&sort_by=0&total_rec_counter=8&surbur=&pcode=&loc_id=&search_type=&range=10

			if(isset($_GET['range']) && (!empty($_GET['range'])))

			{

				$distang_range = $_GET['range'];



			}

		



			$DISTANCE = $distang_range;

			$rest_listing = $rest_listing->select(DB::raw("*,(((acos(sin((".$home_lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$home_lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$home_lng." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance"));

			

            $rest_listing = $rest_listing->having("distance","<",$DISTANCE);



			 //***************** SHOW ALWAYS GOOGLE DATA  ********************/

                       $Unine_listing = $Unine_listing->select(DB::raw("*,(((acos(sin((".$home_lat."*pi()/180)) * sin((`s1`.`rest_lat`*pi()/180))+cos((".$home_lat."*pi()/180)) * cos((`s1`.`rest_lat`*pi()/180)) * cos(((".$home_lng." - `s1`.`rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance"));

                       $Unine_listing = $Unine_listing->having("distance","<",$DISTANCE);

                       $Unine_listing = $Unine_listing->where('s1.google_type', '=' , '1');

			  // $rest_listing = $rest_listing->union($Unine_listing);

                       /****************** SHOW ALWAYS GOOGLE DATA  ********************/





                  }

                  else

                  {

                   	$rest_listing = $rest_listing->select('*');

			 //***************** SHOW ALWAYS GOOGLE DATA  ********************/

			// $Unine_listing = $Unine_listing->select("*");

                   	$Unine_listing = $Unine_listing->where('s1.google_type', '=' , '0');

                   	/****************** SHOW ALWAYS GOOGLE DATA  ********************/

                   }

                   if(isset($_GET['rest_rating']) && (!empty($_GET['rest_rating'])))

                   {

                   	$rest_rating = $_GET['rest_rating'];

                   	$Unine_listing = $Unine_listing->where(function($query) use ($rest_rating) {

                   		$l=0;

                   		foreach($rest_rating as $ftype )

                   		{

                   			if($ftype>0)

                   			{

                   				if($l>0){

                   					$query = $query->orWhere('totalrating','=',$ftype);

                   				}

                   				else

                   				{

                   					$query = $query->Where('totalrating','=',$ftype);

                   				}

                   			}

                   			$l++;

                   		}

                   	});

                   }

                   if(isset($search_cuisine) && (!empty($search_cuisine)))

                   {

                   	$Unine_listing = $Unine_listing->where(function($query) use ($search_cuisine) {

                   		$l=0;

                   		foreach($search_cuisine as $ftype )

                   		{

                   			if($ftype>0)

                   			{

                   				if($l>0){

                   					$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",s1.rest_cuisine)');

                   				}

                   				else

                   				{

                   					$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",s1.rest_cuisine)');

                   				}

                   			}

                   			$l++;

                   		}

                   	});

                   }



                   //Deep comment

                   

                   if($show_data_setting==0)

                   {

                   	   $rest_listing = $rest_listing->union($Unine_listing); // deep

                   }

                   /************************          UNIONE DATA      *****************************/

		//$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

                   $rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'UNPUBLISHED');

                   $rest_listing = $rest_listing->where('search_restaurant_view.google_type', '=' , '0');

		/*if(!empty($surbur_name))

		{

			$rest_listing = $rest_listing->where('search_restaurant_view.rest_address', 'like' ,"%".$surbur_name);

		}

		if(isset($_GET['cartType']) && (!empty($_GET['cartType'])))

		{

		  $rest_listing = $rest_listing->whereRaw('FIND_IN_SET("'.$_GET['cartType'].'",search_restaurant_view.rest_service)');

		}

		*/

		/********************** START SEARCH ACCORDING TO SURBUR ***********************/

		if(request()->surbru_name)

		{

			$search_surbur_name =  request()->surbru_name;

			$rest_listing = $rest_listing->where('search_restaurant_view.rest_suburb', '!=' , str_replace('-',' ',$search_surbur_name));

		}

		/******************** END SEARCH ACCORDING TO SURBUR ************************/

		if(isset($_GET['cartType']) && (!empty($_GET['cartType'])))

		{

			$service_type = $_GET['cartType'];

			$rest_listing = $rest_listing->where(function($query) use ($service_type) {

				$l=0;

				foreach($service_type as $ftype )

				{

					if($l>0){

						$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_service)');

					}

					else

					{

						$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_service)');

					}

					$l++;

				}

			});

		}

		if(isset($search_cuisine) && (!empty($search_cuisine)))

		{

			$rest_listing = $rest_listing->where(function($query) use ($search_cuisine) {

				$l=0;

				foreach($search_cuisine as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

						}

						else

						{

							$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

						}

					}

					$l++;

				}

			});

		}

		if(isset($search_rest_type) && (!empty($search_rest_type)))

		{

			$rest_listing = $rest_listing->where(function($query) use ($search_rest_type) {

				$l=0;

				foreach($search_rest_type as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_type)');

						}

						else

						{

							$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_type)');

						}

					}

					$l++;

				}

			});

		}

		if(isset($_GET['rest_rating']) && (!empty($_GET['rest_rating'])))

		{

			$rest_rating = $_GET['rest_rating'];

			$rest_listing = $rest_listing->where(function($query) use ($rest_rating) {

				$l=0;

				foreach($rest_rating as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							$query = $query->orWhere('totalrating','=',$ftype);

						}

						else

						{

							$query = $query->Where('totalrating','=',$ftype);

						}

					}

					$l++;

				}

			});

				//$Unine_listing = $Unine_listing->where('s1.google_type', '=' , '1');

		}

		if(isset($_GET['rest_price_level']) && (!empty($_GET['rest_price_level'])))

		{

			$rest_rating = $_GET['rest_price_level'];

			$rest_rating = $rest_listing->where(function($query) use ($rest_rating) {

				$l=0;

				foreach($rest_rating as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							$query = $query->orWhere('rest_price_level','=',$ftype);

						}

						else

						{

							$query = $query->Where('rest_price_level','=',$ftype);

						}

					}

					$l++;

				}

			});

		}



		$rest_listing = $rest_listing->orderBy('google_type', 'asc');

		$rest_listing = $rest_listing->orderBy('rest_id', 'desc');

		if(isset($_GET['sort_by']) && ($_GET['sort_by']=='2'))

		{

			$rest_listing = $rest_listing->whereRaw('FIND_IN_SET("Delivery",search_restaurant_view.rest_service)');

			// $rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'asc');

			$rest_listing = $rest_listing->orderBy('rest_mindelivery', 'asc');

		}

		if(isset($_GET['sort_by']) && ($_GET['sort_by']=='3'))

		{

			// $rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_name', 'asc');

			$rest_listing = $rest_listing->orderBy('distance', 'asc');

		}

		if(isset($_GET['sort_by']) && ($_GET['sort_by']=='4'))

		{

			 //$rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_name', 'desc');

			$rest_listing = $rest_listing->orderBy('rest_name', 'desc');

		}

		if(isset($_GET['sort_by']) && ($_GET['sort_by']=='5'))

		{

			// $rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'desc');

			$rest_listing = $rest_listing->orderBy('totalrating', 'desc');

		}

		$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

		$test_sql  = $rest_listing;

		$test_sql  = $test_sql->get();

		$rest_listing = $rest_listing->simplePaginate($limit);

		$tot_rec = count($test_sql);

		/***************************************  MAP DATA SHOW *********/

		$map_array ='';

		if($tot_rec>0)

		{

			$m=0;

			foreach($test_sql as $mdata)

			{

				$redirect_path ='';

				$image_link = '';

				if($mdata->google_type==1)

				{

					$image_link = $mdata->rest_logo;

				}

				else

				{

					if(!empty($mdata->rest_logo))

					{

						$image_link = url('/').'/uploads/reataurant/'.$mdata->rest_logo;

					}

					else

					{

						$image_link = url('/design/front/img/logo.png');

					}

				}

				$addr1= str_replace(',',' ',$mdata->rest_name);

				$latitude=$mdata->rest_lat;

				$longitude=$mdata->rest_long;

				// $map_array[$m] = array($addr1,$latitude,$longitude);

				// $map_array .= "'".$addr1.",".$latitude.",".$longitude."' |";

				// $map_array .= $addr1.",".$latitude.",".$longitude.",";

				$map_array .= $addr1.",".$latitude.",".$longitude.",".$image_link.",".$mdata->totalrating.",".$redirect_path."',";

				$m++;

			}

		}

		 //$j_map = json_encode($map_array);

		$j_map = json_encode(rtrim($map_array,","));

		/*******************************  MAP DATA SHOW   *****************/

		$selected_cuisine = '';

		foreach($cuisine_list as $Culist)

		{

			if((!empty($search_cuisine)) && (in_array($Culist->cuisine_id, $search_cuisine)))

			{

				$selected_cuisine .= '<a href="#" class="slct-fltr remove_top_cuisine" id="slct-fltr1" data-cui_id="'.$Culist->cuisine_id.'">'.$Culist->cuisine_name.'<span><i class="fa fa-times"></i></span></a>';

			}

		}

		$fav_list = '';

		if(!Auth::guest())

		{

			$userid = Auth::user()->id;

			$fav_list_data = DB::table('favourite_restaurant')

			->where('favrest_userid', '=' ,$userid)

			->where('favrest_status', '=' , '1')

			->orderBy('favrest_id','desc')->get();

			if($fav_list_data)

			{

				foreach($fav_list_data as $flist)

				{

					$fav_list[]=$flist->favrest_restid;

				}

			}

		}

		$url_pag_no = '';

		$lastPage  = ceil($tot_rec/$limit);

		$current_page = $rest_listing->currentPage();

		if($current_page<$lastPage)

		{

			/*$url = $rest_listing->nextPageUrl();

			$a = explode('=',$url);*/

			$url_pag_no = $current_page+1;

		}

		$data_onview = array('rest_listing' =>$rest_listing,

			'total_restaurant' =>$tot_rec,

			'cuisine_list' =>$cuisine_list,

			'page_no'=>$url_pag_no,

			'surbur_name'=>$surbur_name,

			'post_code'=>$post_code,

			'loc_id'=>$loc_id,

			'search_type'=>$search_type,

			'search_cuisine'=>$search_cuisine,

			'selected_cuisine'=>$selected_cuisine,

			'fav_list'=>$fav_list,

			'j_map'=>$j_map,

			'test_sql'=>$test_sql

		);

	//	dd(DB::getQueryLog());

	//	print_r(DB::getQueryLog());

		if(request()->ajax()) {

			$page_load='';

			if($url_pag_no>0) {

				$page_load='<a href="javascript:void(0)" class="load_more_bt wow fadeIn" data-wow-delay="0.2s" id="rest_list_load-'.$url_pag_no.'" data-nextpage="'.$url_pag_no.'" >Load more...</a>';

			}

			$ajax_lsiting_view =  view('ajax.restaurant_listing', $data_onview)->render();

			return response()->json( array('total_record' => $tot_rec,'selected_cuisine' => $selected_cuisine, 'ajax_lsiting_view'=>$ajax_lsiting_view,'page_load'=>$page_load,'j_map'=>$j_map,'test_sql'=>$test_sql) );

		}

		else

		{

			return view('restaurant_listing')->with($data_onview);

		}

	}





		public function restaurant_detail_API_QURY()

			{

				$rest_subrub =  Route::current()->getParameter('subrub');

				$rest_title =  Route::current()->getParameter('restname');

				$rest_listing = DB::table('search_restaurant_view');

				$rest_listing = $rest_listing->select('*');

				$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

				$rest_listing = $rest_listing->where('search_restaurant_view.rest_metatag', '=' ,$rest_title);

				$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

				$rest_listing = $rest_listing->get();

		//print_r($rest_listing);

				$rest_id = $rest_listing[0]->rest_id;

				$menu_list = DB::table('menu')

				->where('restaurant_id', '=' ,$rest_id)

				->where('menu_status', '=' ,'1')

				->orderBy('menu_order', 'asc')

				->get();

				$menu_array = '';

				$food_array='';

				$popular_item = '';

				$popular_item_count =0;

				$popular_item_count_11 =0;

				if(!empty($menu_list))

				{

					$popular_food_detail = '';

					$popular_food_data='';

					foreach($menu_list as $mlist)

					{

						$menu_id = $mlist->menu_id;

						$menu_name = $mlist->menu_name;

						$menu_desc = $mlist->menu_desc;

						$menu_detail =$mlist;

						$menu_item_detail = '';

						$menu_item_name = '';

						$menu_item_desc = '';

						$menu_item_price = '';

						$food_detail = '';

						$food_data='';

						$counter_popular =0 ;

						$menu_cat_detail = DB::table('menu_category')

						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

						->where('menu_category.rest_id', '=' ,$rest_id)

						->where('menu_category.menu_id', '=' ,$menu_id)

						->where('menu_category.menu_cat_status', '=' ,'1')

						->select('menu_category.*','menu_category_item.*')

						->orderBy('menu_category.menu_category_id', 'asc')

						->get();

						$menu_item_count =count($menu_cat_detail);

						$item_addons = '';

						if(!empty($menu_cat_detail))

						{

							foreach($menu_cat_detail as $menu_item)

							{

								$food_data['food_id'] = $menu_item->menu_category_id;

								$food_data['food_popular'] = $menu_item->menu_cat_popular;

								$food_data['food_diet'] = $menu_item->menu_cat_diet;

								$food_data['food_name'] = $menu_item->menu_category_name;

								$food_data['food_price'] = $menu_item->menu_category_price;

								$food_data['food_desc'] = $menu_item->menu_category_desc;

								$food_data['food_portion'] = $menu_item->menu_category_portion;

								$group_name= '';

								$group_list = '';

								$ff_popular = '';

								$popular_food_data='';

								$addons = DB::table('menu_category_addon')

								->where('addon_restid', '=' ,$rest_id)

								->where('addon_menuid', '=' ,$menu_id)

								->where('addon_status', '=' ,'1')

								->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

								->select('*')

								->orderBy('addon_id', 'asc')

								->groupBy('addon_groupname')

								->get();

								if(!empty($addons))

								{

									foreach($addons as $ad_list)

									{

										$group_name[]=$ad_list->addon_groupname;

										$ff['addon_gropname'] = $ad_list->addon_groupname;

										$addon_group = DB::table('menu_category_addon')

										->where('addon_restid', '=' ,$rest_id)

										->where('addon_menuid', '=' ,$menu_id)

										->where('addon_status', '=' ,'1')

										->where('addon_groupname', '=' ,$ad_list->addon_groupname)

										->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

										->select('*')

										->orderBy('addon_id', 'asc')

										->get();

										$group_list[]=$addon_group;

										$addon_group_list ='';

										foreach($addon_group as $group_list_loop)

										{

										//$addon_group_list[]=array('')

											$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

												'addon_menucatid'=>$group_list_loop->addon_menucatid,

												'addon_groupname'=>$group_list_loop->addon_groupname,

												'addon_option'=>$group_list_loop->addon_option,

												'addon_name'=>$group_list_loop->addon_name,

												'addon_price'=>$group_list_loop->addon_price,

												'addon_status'=>$group_list_loop->addon_status

											);

										}

										$ff['addon_detail'] = $addon_group_list;

										$food_data['food_addon'][]=$ff;

										if($menu_item->menu_cat_popular==1)

										{

											$ff_popular['addon_detail'] = $addon_group_list;

											$ff_popular['addon_gropname'] = $ad_list->addon_groupname;

											$popular_food_data['food_addon'][] = $ff_popular;

										}

									}

								}

								$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);

								$food_detail[] = $food_data;

								if($menu_item->menu_cat_popular==1){

									$popular_item_count_11 ++;

									$counter_popular++;

									$popular_food_data['food_id'] = $menu_item->menu_category_id;

									$popular_food_data['food_popular'] = $menu_item->menu_cat_popular;

									$popular_food_data['food_diet'] = $menu_item->menu_cat_diet;

									$popular_food_data['food_name'] = $menu_item->menu_category_name;

									$popular_food_data['food_price'] = $menu_item->menu_category_price;

									$popular_food_data['food_desc'] = $menu_item->menu_category_desc;

									$popular_food_data['food_portion'] = $menu_item->menu_category_portion;

									$popular_food_detail[] = $popular_food_data;

								}

							}

						}

						$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail);

						if($counter_popular>0){

							$popular_item = array('popular_item_count'=>$popular_item_count_11,'popular_food_detail'=>$popular_food_detail);

						}

					}

				}

				echo '<pre>';

				print_r($popular_item);

				print_r($food_array);

			}





		public function restaurant_detail()

		{

			DB::enableQueryLog();

			$avg_rating = '';

			$user_tot_order = 0;

			$rest_food_good = 0;

			$rest_delivery_ontime = 0;

			$rest_order_accurate = 0;

			$rest_subrub =  request()->subrub;

			$rest_title =  request()->restname;

			$area_id = '';



			$city = 'Indore';//Session::get('city');



			//echo $post_code; die;

			$surl = config('app.url').'/home'; 

		    /*<!--->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')-->*/



		$rest_detail = DB::table('restaurant')

		->select('restaurant.*',

			DB::raw("(SELECT GROUP_CONCAT(cuisine_name SEPARATOR ', ') AS `combined_A` FROM `cuisine` WHERE FIND_IN_SET( cuisine_id,restaurant.rest_cuisine)) as cuisine_name")

		  )

		->where('restaurant.rest_status', '!=' , 'INACTIVE')

		->where('restaurant.rest_metatag', '=' , $rest_title)

		->get();



       	if($city){



			$area_id = $city;



		    }else{



		    $city = '';//$rest_detail[0]->rest_city; 



            $area_id = $city;   	

		   }  



		if(!empty($rest_detail)){

			$rest_id = $rest_detail[0]->rest_id;

			$user_id = '';

			$guest_id = '';

			/*	 COUNT TOTAL SUCESSFULL SUBMITED ORDER NUMBER FOR REGISTERED USER	*/

			if(!Auth::guest()){

				$user_id = Auth::user()->id;

				$user_tot_order = DB::table('order_payment')

				->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')

				->select('*')

				->where('order_payment.pay_userid', '=' ,$user_id)

				->where('order.rest_id', '=' ,$rest_id)

				->where('order_payment.pay_tx', '!=' , '' )

				->count();

			}else{

				$guest_id = Session::get('_token');

			}

			/*	 COUNT TOTAL SUCESSFULL SUBMITED ORDER NUMBER FOR REGISTERED USER END	*/

			/** MANAGE VIEW MENU  LOG OF RESTAURANT **/



			$promo_desc_list = DB::table('promotion')

			->where('promo_restid', '=' ,$rest_id)

			->where('promo_status', '=' ,'1')

			->first();

			//echo "<pre>"; print_r($promo_desc_list->promo_desc); die;



			$data_view_history=array('user_id'=>$user_id,

				'guest_id'=>$guest_id,

				'rest_id'=>$rest_id,

				'menu_id'=>'',

				'submenu_id'=>'',

				'device_type'=>'Website',

				'device_name'=>'',

				'device_os'=>'',

				'view_date'=>date('Y-m-d H:i:s'),

				'view_time'=>date('Y-m-d H:i:s')

			);

			DB::table('view_menu_history')->insert($data_view_history);

			/**     END **/

			$sql_food = DB::table('review')

			->select(DB::raw("round((count(`re_food_good`) / 2),0) as  food_good"))

			->where('re_restid', '=' , $rest_id)

			->where('re_food_good', '=' , '1')

			->get();

			$sql_delivery_ontime = DB::table('review')

			->select(DB::raw("round((count(`re_delivery_ontime`) / 2),0) as  delivery_ontime"))

			->where('re_restid', '=' , $rest_id)

			->where('re_delivery_ontime', '=' , '1')

			->get();

			$sql_rder_accurate = DB::table('review')

			->select(DB::raw("round((count(`re_order_accurate`) / 2),0) as  order_accurate"))

			->where('re_restid', '=' , $rest_id)

			->where('re_order_accurate', '=' , '1')

			->get();

			$rest_food_good = $sql_food[0]->food_good;

			$rest_delivery_ontime = $sql_delivery_ontime[0]->delivery_ontime;

			$rest_order_accurate = $sql_rder_accurate[0]->order_accurate;

			$menu_list = DB::table('menu')

			->where('restaurant_id', '=' ,$rest_id)

			->where('menu_status', '=' ,'1')

			->where('area_id', '=' ,$area_id)

			->orderBy('menu_order', 'asc')

			->get();

			$count_review = DB::table('review')

			->where('re_restid', '=' ,$rest_id)

			->where('re_status', '=' ,'PUBLISHED')

			->orderBy('re_id', 'desc')

			->count();

			$avg_rating = '';

			if($count_review>0){

				$count_avg_rating =0;

				$total_rating = DB::table('review')

				->select( DB::raw('SUM(re_rating) as rating'))

				->where('re_restid', '=' ,$rest_id)

				->where('re_status', '=' ,'PUBLISHED')

				->orderBy('re_id', 'desc')

				->get();

				$count_avg_rating =	$total_rating[0]->rating;

				$avg_rating = round(($count_avg_rating/$count_review));

			}

			$menu_array = '';

			$popular_item = '';

			$menu_list = DB::table('menu_category')

			->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')

			->where('menu_category.rest_id', '=' ,$rest_id)

			->where('menu_category.menu_cat_status', '=' ,'1')

			->where('menu_category.area_id', '=' ,$area_id)

			->where('menu.menu_status', '=' ,'1')

			->where('menu.restaurant_id', '=' ,$rest_id)

			->select('menu.*')

			->orderBy('menu.menu_id', 'asc')

			->groupBy('menu.menu_id')

			->get();

			$menu_array = '';

			$food_array=array();

			$popular_item = '';

			$popular_item_count =0;

			$popular_item_count_11 =0;

			if(!empty($menu_list)){

				$popular_food_detail = array();

				$popular_food_data='';

				foreach($menu_list as $mlist){

					$menu_id = $mlist->menu_id;

					$menu_name = $mlist->menu_name;

					$menu_name_ar = $mlist->menu_name_ar;

					$menu_desc = $mlist->menu_desc;

					$menu_desc_ar = $mlist->menu_desc_ar;

					$menu_detail =$mlist;

					$menu_item_detail = '';

					$menu_item_name = '';

					$menu_item_desc = '';

					$menu_item_price = '';

					$food_detail = array();

					$food_data=array();

					$counter_popular =0 ;

					$menu_cat_detail = DB::table('menu_category')

					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

					->where('menu_category.rest_id', '=' ,$rest_id)

					->where('menu_category.menu_id', '=' ,$menu_id)

					->where('menu_category.menu_cat_status', '=' ,'1')

					->where('menu_category.area_id', '=' ,$area_id)

					->select('menu_category.*','menu_category_item.*')

					->orderBy('menu_category.menu_category_id', 'asc')

					->get();

					$menu_item_count =count($menu_cat_detail);

					$item_addons = array();

					if(!empty($menu_cat_detail)){

						foreach($menu_cat_detail as $menu_item){

							$food_data['food_menu_id'] = $menu_id;

							$food_data['food_id'] = $menu_item->menu_category_id;

							$food_data['food_popular'] = $menu_item->menu_cat_popular;

							$food_data['food_diet'] = $menu_item->menu_cat_diet;

							$food_data['food_name'] = $menu_item->menu_category_name;

							$food_data['food_name_ar'] = $menu_item->menu_category_name_ar;

							$food_data['food_price'] = $menu_item->menu_category_price;

				

							if(!empty($menu_item->menu_category_image)){

							$food_data['food_image'] = trim(url('/').'/uploads/reataurant/menu/'.$menu_item->menu_category_image);

							}

							else

							{

							$food_data['food_image'] =trim(url('/').'/uploads/reataurant/menu/menu.jpg');

							}



							$food_data['food_desc'] = $menu_item->menu_category_desc;

							$food_data['food_desc_ar'] = $menu_item->menu_category_desc_ar;

							$food_data['food_portion'] = $menu_item->menu_category_portion;

							$food_data['food_size_detail']='';

							$pp_ff = '';

							if($menu_item->menu_category_portion=='yes'){

								$food_size = DB::table('category_item')

								->where('rest_id', '=' ,$rest_id)

								->where('menu_id', '=' ,$menu_id)

								->where('menu_category', '=' ,$menu_item->menu_category_id)

								->where('menu_item_status', '=' ,'1')

								->select('*')

								->orderBy('menu_cat_itm_id', 'asc')

								->groupBy('menu_item_title')

								->get();

								if(count($food_size)>0)

								{

									$food_data['food_size_detail']=$food_size;

									$pp_ff =$food_size;

								}

								else

								{

									$food_data['food_size_detail']=array();

									$pp_ff =array();

								}

							}else{

								$food_data['food_size_detail']=array();

								$pp_ff =array();

							}

							$group_name= '';

							$group_list = '';

							$ff_popular = '';

							$popular_food_data=array();



						array_push($item_addons, array('group_name'=>$group_name,'group_list'=>$group_list));



							array_push($food_detail, $food_data);



							if($menu_item->menu_cat_popular==1){

								$popular_item_count_11 ++;

								$counter_popular++;

								$popular_food_data['food_menu_id'] = $menu_id;

								$popular_food_data['food_id'] = $menu_item->menu_category_id;

								$popular_food_data['food_popular'] = $menu_item->menu_cat_popular;

								$popular_food_data['food_diet'] = $menu_item->menu_cat_diet;

								$popular_food_data['food_name'] = $menu_item->menu_category_name;

								$popular_food_data['food_name_ar'] = $menu_item->menu_category_name_ar;

								$popular_food_data['food_price'] = $menu_item->menu_category_price;



							if(!empty($menu_item->menu_category_image)){

							$popular_food_data['food_image'] = trim(url('/').'/uploads/reataurant/menu/'.$menu_item->menu_category_image);

							}

							else

							{

							$popular_food_data['food_image'] =trim(url('/').'/uploads/reataurant/menu/menu.jpg');

							}



								$popular_food_data['food_desc'] = $menu_item->menu_category_desc;

								$popular_food_data['food_desc_ar'] = $menu_item->menu_category_desc_ar;

								$popular_food_data['food_portion'] = $menu_item->menu_category_portion;

								$popular_food_data['food_size_detail']=	$pp_ff;

								array_push($popular_food_detail, $popular_food_data);

							}

						}

					}else{

						$food_detail=array();

					}



					array_push($food_array, array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'menu_name_ar'=>$menu_name_ar,'menu_desc'=>$menu_desc,'menu_desc_ar'=>$menu_desc_ar,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail));



					if($counter_popular>0){

						$popular_item = array('popular_item_count'=>$popular_item_count_11,'popular_food_detail'=>$popular_food_detail);

					}

				}

			}

			else

			{

				$food_array=array();

			}

			$menu_array = 	$food_array;

			//$popular_item	 = $new_populate_array;

			/** PROMOTIONS CODE START  **/

			$current_promo = '';

			$promo_list = DB::table('promotion')

			->where('promo_restid', '=' ,$rest_id)

			->where('promo_status', '=' ,'1')

			->orderBy('promo_on', 'asc')

			->orderBy('promo_id', 'desc')

			->groupBy('promo_id')

			->get();

			$current_promo = array();

			if($promo_list){

				foreach($promo_list as $plist ){

					$current_promo1 = array();

					$db_day = '';

					if(!empty($plist->promo_day)){

						$db_day = explode(',',$plist->promo_day);

					}

					if($plist->promo_on=='schedul'){

						$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

						if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00')){

							if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d')) && (empty($plist->promo_day)))

							{

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo[] = $current_promo1;

							}

							if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d')) && ((!empty($plist->promo_day)) && (in_array($day_name,$db_day))))

							{

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo1['promo_end'] = $plist->promo_end;

								$current_promo1['promo_start'] = $plist->promo_start;

								$current_promo[] = $current_promo1;

							}

						}

						elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))){

							$db_day = explode(',',$plist->promo_day);

							if(in_array($day_name,$db_day))

							{

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo1['promo_end'] = $plist->promo_end;

								$current_promo1['promo_start'] = $plist->promo_start;

								$current_promo[] = $current_promo1;

							}

						}

					}

					else

					{

						$current_promo1['promo_id'] = $plist->promo_id;

						$current_promo1['promo_restid'] = $plist->promo_restid;

						$current_promo1['promo_for'] = $plist->promo_for;

						$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

						$current_promo1['promo_on'] = $plist->promo_on;

						$current_promo1['promo_desc'] = $plist->promo_desc;

						$current_promo1['promo_mode'] = $plist->promo_mode;

						$current_promo1['promo_buy'] = $plist->promo_buy;

						$current_promo1['promo_value'] = $plist->promo_value;

						$current_promo1['promo_end'] = $plist->promo_end;

						$current_promo1['promo_start'] = $plist->promo_start;

						array_push($current_promo, $current_promo1);

					}

				}

			}

			$promo_list = $current_promo;

			$service_area_list = DB::table('service')

			->where('service_restid', '=' ,$rest_id)

			->where('service_status', '=' ,'1')

			->orderBy('service_id', 'asc')

			->get();



		

			$cart = Cart::content();



	

			$cart_session_val = '1';

			if(count($cart)){

				foreach($cart as $item){

					if(isset($item->options['vehicle_id']))

					{

						Cart::destroy();

						$cart_session_val = '0';

					}

					if(isset($item->options['rest_id']) && ($item->options['rest_id']!=$rest_id))

					{

						Cart::destroy();

						Session::forget('service_option');

						Session::forget('service_area');

						Session::forget('rest_id');

						Session::forget('rest_meta_url');

						Session::forget('delivery_fee');

						Session::forget('cart_dinein');

						Session::forget('delivery_fee_min');

						Session::forget('delivery_fee_min_remaing');

						Session::forget('promo_mode');

						Session::forget('promo_value');

						Session::forget('sub_total');

						Session::forget('promo_amt_cal');

						$cart_session_val = '0';

					}

				}

			}

			/* CHECK SEARCH AREA IS RELATED TO SEARCH LOCATION OR NOT START */

			if((Session::has('service_option')) && (Session::get('service_area')!='Delivery'))

			{

				Session::forget('delivery_fee');

				Session::forget('delivery_fee_min');

			}

			if((Session::has('home_location_id')) && (!(Session::has('service_area'))))

			{

				$loc_id= Session::get('home_location_id');

				if(!empty($loc_id) && ($loc_id>0)){

					$Suburb_detail  = DB::table('location')

					->where('location_id', '=' , $loc_id)

					->get();

					$surbur_name= $Suburb_detail[0]->Suburb_name;

					$post_code=$Suburb_detail[0]->pincode;

					$state_code=$Suburb_detail[0]->state;

					$search_area  = DB::table('service')

					->where('service_restid', '=' ,$rest_id)

					->where('service_suburb', '=' ,$surbur_name)

					->where('service_postcode', '=' ,$post_code)

					->where('service_status', '=' ,'1')

					->orderBy('service_id', 'asc')

					->get();

					if(count($search_area)>0)

					{

							//echo $search_area[0]->service_charge;

							// $search_area[0]->service_id;

						Session::put('delivery_fee', $rest_detail[0]->rest_mindelivery);

						Session::put('service_area', $search_area[0]->service_id);

					}

				}

			}

			else

			{

				$search_area  = DB::table('service')

				->where('service_restid', '=' ,$rest_id)

				->where('service_status', '=' ,'1')

				->orderBy('service_id', 'asc')

				->get();

				if(count($search_area)>0)

				{

					Session::put('delivery_fee', $rest_detail[0]->rest_mindelivery);

					Session::put('service_area', $search_area[0]->service_id);

				}

			}

			/*                END                    */

			/*   DINEIN TODAY TIME AND CLAENDAR SART  */

			$rest_start = '';

			$rest_end = '';

			$open_time = '';

			$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

			switch ($day_name) {

				case 'sun':

				$open_time = explode('_',$rest_detail[0]->rest_sun);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'mon':

				$open_time = explode('_',$rest_detail[0]->rest_mon);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'tue':

				$open_time = explode('_',$rest_detail[0]->rest_tues);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'wed':

				$open_time = explode('_',$rest_detail[0]->rest_wed);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'thu':

				$open_time = explode('_',$rest_detail[0]->rest_thus);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'fri':

				$open_time = explode('_',$rest_detail[0]->rest_fri);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

				case 'sat':

				$open_time = explode('_',$rest_detail[0]->rest_sat);

				$rest_start = $open_time[0];

				$rest_end = $open_time[1];

				break;

			}

			$subtract_start_mins = (int) $rest_detail[0]->rest_delivery_from*60;

			$subtract_end_mins = (int) $rest_detail[0]->rest_delivery_to*60;

			$start = date("H:i", (strtotime($rest_start)+$subtract_start_mins));

			$end = date("H:i", (strtotime($rest_end)-$subtract_end_mins));

				$starttime = $start; //'9:00';  // your start time

				$endtime = $end; //'21:00';  // End time

				$duration = '15';  // split by 30 mins

				$array_of_time = array ();

				$start_time    = strtotime ($starttime); //change to strtotime

				$end_time      = strtotime ($endtime); //change to strtotime

				$add_mins  = $duration * 60;

				while ($start_time <= $end_time) // loop between time

				{

					$array_of_time[] = date ("H:i", $start_time);

				   $start_time += $add_mins; // to check endtie=me

				}



			if($cart_session_val=='0')

			{

				$data_onview = array('rest_detail' =>$rest_detail,

					'menu_array'=>$menu_array,

					'service_area'=>$service_area_list,

					'total_review'=>$count_review,

					'rest_id' =>$rest_id,

					'delivery_time'=>$array_of_time,

					'avg_rating'=>$avg_rating,

					'popular_item'=>$popular_item,

					'promo_list'=>$promo_list,

					'user_tot_order'=>$user_tot_order,

					'food_good'=>$rest_food_good,

					'delivery_ontime'=>$rest_delivery_ontime,

					'order_accurate'=>$rest_order_accurate,

					'promo_desc_list'=>$promo_desc_list

				);

			}

			elseif($cart_session_val=='1')

			{

				$data_onview = array('rest_detail' =>$rest_detail,

					'menu_array'=>$menu_array,

					'service_area'=>$service_area_list,

					'rest_id' =>$rest_id,

					'total_review'=>$count_review,

					'cart' =>$cart,

					'delivery_time'=>$array_of_time,

					'avg_rating'=>$avg_rating,

					'popular_item'=>$popular_item,

					'promo_list'=>$promo_list,

					'user_tot_order'=>$user_tot_order,

					'food_good'=>$rest_food_good,

					'delivery_ontime'=>$rest_delivery_ontime,

					'order_accurate'=>$rest_order_accurate,

					'promo_desc_list'=>$promo_desc_list

				);

			  }

				

				return view('restaurant_detail')->with($data_onview);

			}

			else

			{

				return redirect()->to($surl);

			}

	}



		public function home_search()

		{

			if(isset($_POST['cuisine']))

			{

				Session::put('home_cuisine',Input::get('cuisine'));

			}

			if(isset($_POST['location']) && (!empty($_POST['location'])))

			{

				Session::put('home_location',Input::get('location'));

				Session::put('home_street_number',Input::get('street_number'));

				Session::put('home_route',Input::get('route'));

				Session::put('home_locality',Input::get('locality'));

				Session::put('home_level',Input::get('level'));

                if(Input::get('postal_code')){

				Session::put('home_postal_code',Input::get('postal_code'));

			   }else{ Session::put('home_postal_code','452001'); }



				Session::put('home_country',Input::get('country'));

			   }



			return redirect()->to('/restaurant_listing');

		}

		public function restaurant_review()

		{

			$rest_subrub =  Route::current()->getParameter('subrub');

			$rest_title =  Route::current()->getParameter('restname');

			$tt =  Route::current()->getParameter('review');

			/*	->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')	*/

			$rest_detail = DB::table('restaurant')

			->select('restaurant.*',

				DB::raw("(SELECT GROUP_CONCAT(cuisine_name SEPARATOR ', ') AS `combined_A` FROM `cuisine` WHERE FIND_IN_SET( cuisine_id,restaurant.rest_cuisine)) as cuisine_name"))

			->where('restaurant.rest_status', '!=' , 'INACTIVE')

			->where('restaurant.rest_metatag', '=' , $rest_title)

			->get();

			if((!empty($rest_detail)) && ( $tt=='review'))

			{

				$rest_id = $rest_detail[0]->rest_id;

				$avg_rating	= 0;

				$count_review = DB::table('review')

				->where('re_restid', '=' ,$rest_id)

				->where('re_status', '=' ,'PUBLISHED')

				->orderBy('re_id', 'desc')

				->count();

				$count_avg_rating =0;

				$total_rating = DB::table('review')

				->select( DB::raw('SUM(re_rating) as rating'))

				->where('re_restid', '=' ,$rest_id)

				->where('re_status', '=' ,'PUBLISHED')

				->orderBy('re_id', 'desc')

				->get();

				$count_avg_rating =	$total_rating[0]->rating;

			 //$avg_rating = (($count_avg_rating)/5);

				if($count_review>0)

				{

					$avg_rating = round(($count_avg_rating/$count_review)/2);

				}

				$refno = 0;

				$user_id = 0;

				$order_detail='';

				$review_list='';

				if(!Auth::guest())

				{

					$user_id = Auth::user()->id ;

				}

				if(isset($_GET['refno']) && (!empty($_GET['refno'])))

				{

					$refno = $_GET['refno'];

					$order_detail  =  DB::table('order')

					->where('rest_id', '=', $rest_id)

					->where('user_id', '=', $user_id)

					->where('order_id', '=', $refno)

					->select('*')

					->get();

				}

				$review_list = DB::table('review')

				->leftJoin('users', 'review.re_userid', '=', 'users.id')

				->select('review.*','users.name','users.lname')

				->where('review.re_restid', '=' , $rest_id)

				->where('review.re_status', '=' , 'PUBLISHED' )

				->orderBy('re_id', 'desc')

				->get();

				$data_onview = array('rest_detail' =>$rest_detail,

					'rest_id' =>$rest_id,

					'order_detail' =>$order_detail,

					'refno' =>$refno,

					'user_id' =>$user_id,

					'review_list' =>$review_list,

					'total_review'=>$count_review	,

					'avg_rating'=>$avg_rating

				);

				return view('restaurant_review')->with($data_onview);

				/* return view('restaurant_review');*/

			}

			else

			{

				return redirect()->to('/restaurant_listing');

			}

		}

		function get_dineinfrm()

		{

		//.echo '<pre>';

		//print_r($_POST);

			$time = trim(Input::get('pickup_time'));

			$time = substr($time,0,2).':'.substr($time,2,2);

			$cart_dinein['dinein_date'] = trim(Input::get('dinein_date'));

			$cart_dinein['dinein_pic'] = trim(Input::get('dinein_pic'));

			$cart_dinein['holiday'] = trim(Input::get('holiday'));

			$cart_dinein['pickup_time'] = trim(Input::get('pickup_time'));

			$cart_dinein['dinein_time'] = trim($time);

			$cart_user['todayclosed'] = trim(Input::get('todayclosed'));

		//print_r($cart_user);

		//exit;

			Session::put('cart_dinein', $cart_dinein);

			$info = array('dinein_pic' =>trim(Input::get('dinein_pic')),

				'dinein_date' =>trim(Input::get('dinein_date')),

				'holiday' =>trim(Input::get('holiday')),

				'pickup_time' =>trim(Input::get('pickup_time')),

				'dinein_time' =>trim($time),

				'todayclosed' =>trim(Input::get('todayclosed'))

			);

			$cart_view =  view('ajax.dinein_frm', array('info' => $info))->render();

			return response()->json( array('dinein_view'=>$cart_view,

				'dinein_pic'=>trim(Input::get('dinein_pic')),

				'dinein_date'=>trim(Input::get('dinein_date')),

				'dinein_time'=>trim(Input::get('pickup_time'))) );

		}

		function show_reorder()

		{

		//echo '<pre>';

	//	echo $_REQUEST['refno'];

			Cart::destroy();

			Session::forget('service_option');

			Session::forget('service_area');

			Session::forget('rest_id');

			Session::forget('rest_meta_url');

			Session::forget('delivery_fee');

			Session::forget('cart_dinein');

			$order_id = $_REQUEST['refno'];

			if(!Auth::guest())

			{

		 	//echo '<br>'.$user_id = Auth::user()->id;

			}

			$order_list = DB::table('order')

			->leftJoin('users', 'order.user_id', '=', 'users.id')

			->select('order.*','users.name','users.lname')

			->where('order.order_id', '=' , $order_id)

			->get();

				//print_r($order_list);

		//		print_r(json_decode($order_list[0]->order_carditem));

			$get_rest_id = '';

			if(isset($order_list[0]->order_carditem) && (count($order_list[0]->order_carditem)))

			{

				$order_carditem = json_decode($order_list[0]->order_carditem, true);

				foreach($order_carditem as $item)

				{

			 	//echo $item['id'];

					$price = '0';

					$qty =  $item['qty'];

					$name = '';

					$name_ar = '';

					$option_name = '';

					$addon_list='';

					$old_option_name = $item['options']['option_name'];

					$get_rest_id =  $rest_id =$item['options']['rest_id'];

					$menu_id =$item['options']['menu_id'];

					$menu_catid = $item['options']['menu_catid'];

					$item_detail  = DB::table('menu_category')

					->where('menu_category_id', '=' ,$menu_catid)

					->where('menu_category.menu_cat_status', '=' ,'1')

					->get();

					if(count($item_detail)>0)

					{

						$name = $item_detail[0]->menu_category_name;

						$name_ar = $item_detail[0]->menu_category_name_ar;



						if($item_detail[0]->menu_category_portion=='no')

						{

							$price =$item_detail[0]->menu_category_price;

						}

						if($item_detail[0]->menu_category_portion=='yes')

						{

							$option_name = $old_option_name;

							$option_detail  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_catid)->get();

							if(!empty($old_option_name) &&($old_option_name=='large'))

							{

								$name =$name.nl2br($option_detail[0]->menu_large_title);

								$price =$option_detail[0]->menu_large_price;

							}

							if(!empty($old_option_name) &&($old_option_name=='medium'))

							{

								$name =$name.nl2br($option_detail[0]->menu_medium_title);

								$price =$option_detail[0]->menu_medium_price;

							}

							if(!empty($old_option_name) &&($old_option_name=='small'))

							{

								$name =$name.nl2br($option_detail[0]->menu_small_title);

								$price =$option_detail[0]->menu_small_price;

							}

						}

					}

			//if(empty($price)){ $price =$item_detail[0]->menu_category_price;}

					if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

					{

						foreach($item['options']['addon_data'] as $addon)

						{

				   //$cart_price = $cart_price+$addon['price'];

				// {{$addon['name']}}

							$ad_detail = DB::table('menu_category_addon')

							->where('addon_id', '=' ,$addon['id'])

							->where('addon_status', '=' ,'1')

							->get();

				//	print_r($ad_detail);

							if(count($ad_detail)>0){

								$ad_name = trim($ad_detail[0]->addon_groupname).': '.trim($ad_detail[0]->addon_name);

								$ad_price = $ad_detail[0]->addon_price;

								$addon_list[]=array('name'=>$ad_name,'price'=>$ad_price,'id'=>$ad_detail[0]->addon_id);

							}

				} // @endforeach

			 }//@endif

			 Cart::add(array('id'=>$menu_catid,

			 	'name' => $name,

			 	'name_ar' => $name_ar,

			 	'qty' => $qty,

			 	'price' => $price,

			 	'options'=>['menu_catid' => $menu_catid,

			 	'rest_id' => $rest_id,

			 	'menu_id' => $menu_id,

			 	'option_name'=>$option_name,

			 	'addon_data'=>$addon_list

			 ]

			)

			);

			} //endforeach

		}//@endif

		$cart = Cart::content();

		//print_r(Cart::content());

		/* MANAGE USER INFO ACCORDING TO OLD ORDER START*/

		$cart_user['delivery_add1'] ='';

		$cart_user['delivery_add2'] ='';

		$cart_user['delivery_surbur'] ='';

		$cart_user['delivery_postcode'] ='';

		$cart_user['delivery_number'] ='';

		$cart_user['pickup_date'] ='';

		$cart_user['pickup_time'] ='';

		$cart_user['rest_meta_url']='';

		$cart_user['rest_id']=trim($order_list[0]->rest_id);

		$cart_user['deliveri_time']=trim('asp');

		$cart_user['order_instruction'] = '';

		$cart_user['service_area'] = '';

		$cart_user['service_type'] = '';

		$cart_user['order_tablepic'] ='';

		$cart_user['pickup_time'] ='';

		$cart_user['pickup_date'] = '';

		$cart_user['firstname_order'] = trim($order_list[0]->order_fname);

		$cart_user['lastname_order'] = trim($order_list[0]->order_lname);

		$cart_user['tel_order'] = trim($order_list[0]->order_tel);

		$cart_user['email_order'] = trim($order_list[0]->order_email);

		$cart_user['address_order'] = trim($order_list[0]->order_address);

		$cart_user['city_order'] = trim($order_list[0]->order_city);

		$cart_user['pcode_order'] = trim($order_list[0]->order_pcode);

		$cart_user['user_type'] = '1';

		$cart_user['user_id'] = trim(Auth::user()->id);

		$cart_user['cart_data'] = $cart;

		Session::put('cart_userinfo', $cart_user);

		/* END */

		$rest_detail = DB::table('restaurant')

		->where('restaurant.rest_id', '=' , $get_rest_id)

		->get();

		$path =strtolower(str_replace(' ','_',(trim($rest_detail[0]->rest_suburb))).'/'.$rest_detail[0]->rest_metatag);

		return redirect()->to('/'.$path.'?erro=1');

	}

	function get_addons()

	{

		$rest_id = trim(Input::get('rest_id'));

		$menu_id = trim(Input::get('menu_id'));

		$menu_category_id = trim(Input::get('menu_cat_id'));

		$menu_option =  trim(Input::get('menu_option'));;

		$menu_cat_detail = DB::table('menu_category')

		->where('menu_category.rest_id', '=' ,$rest_id)

		->where('menu_category.menu_id', '=' ,$menu_id)

		->where('menu_category.menu_category_id', '=' ,$menu_category_id)

		->where('menu_category.menu_cat_status', '=' ,'1')

		->select('menu_category.*')

		->orderBy('menu_category.menu_category_id', 'asc')

		->get();

		$menu_category_name	 = $menu_cat_detail[0]->menu_category_name;

		$menu_category_desc	 = $menu_cat_detail[0]->menu_category_desc;

		$menu_category_name_ar	 = $menu_cat_detail[0]->menu_category_name_ar;

		$menu_category_desc_ar	 = $menu_cat_detail[0]->menu_category_desc_ar;

		$group_name= '';

		$group_list = '';



		$addons = DB::table('menu_category_addon')

		->where('addon_restid', '=' ,$rest_id)

		->where('addon_menuid', '=' ,$menu_id)

		->where('addon_status', '=' ,'1')

		->where('addon_menucatid', '=' ,$menu_category_id)

		->select('*')

		->orderBy('addon_id', 'asc')

		->groupBy('addon_groupname')

		->get();



		$lang = Session::get('locale');



		if(!empty($addons))

		{

			foreach($addons as $ad_list)

			{

                if(Session::get('locale')=='ar'){

				$group_name[]=$ad_list->addon_groupname_ar;

			    }else{

			    $group_name[]=$ad_list->addon_groupname;	

			    }



				$addon_group = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_groupname', '=' ,$ad_list->addon_groupname)

				->where('addon_menucatid', '=' ,$menu_category_id)

				->select('*')

				->orderBy('addon_id', 'asc')

				->get();

				$group_list[]=$addon_group;

			}

		}

		//$item_addons = array('group_name'=>$group_name,'group_list'=>$group_list);

		$item_addons = array('group_name'=>$group_name,

			'group_list'=>$group_list,

			'rest_id'=>$rest_id,

			'menu_id'=>$menu_id,

			'menu_category_id'=>$menu_category_id,

			'menu_option'=>$menu_option,

			'menu_category_desc'=>$menu_category_desc,

			'menu_category_name'=>$menu_category_name,

			'menu_category_desc_ar'=>$menu_category_desc_ar,

			'menu_category_name_ar'=>$menu_category_name_ar

		);

		$addon_view =  view('ajax.addon', $item_addons)->render();

         //return response()->json( array('addon_view'=>$addon_view) );

		return response()->json( array('addon_view'=>$addon_view,'addon_count'=>count($addons),'addon_lang'=>$lang) );

	}

	function make_favourite()

	{

		//print_r($_REQUEST)	;

		$user_id = $_GET['userid'];

		$rest_id = $_GET['restid'];

		$favourite_list  = DB::table('favourite_restaurant')

		->where('favrest_restid', '=' ,$rest_id)

		->where('favrest_userid', '=' ,$user_id)

		->orderBy('favrest_id', 'desc')->get();

		if($favourite_list)

		{

			$old_favrest_status=$favourite_list[0]->favrest_status;

			$new_status = 0;

			if($old_favrest_status==0){$new_status =1;}

			if($old_favrest_status==1){$new_status =0;}

			DB::table('favourite_restaurant')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->update(['favrest_status' => $new_status

		]);

			$favoruite = $new_status;

		}

		else

		{

			$fav_data = new Favourite_restaurant;

			$fav_data->favrest_restid = trim($rest_id);

			$fav_data->favrest_userid = trim($user_id);

			$fav_data->favrest_status = '1';

			$fav_data->save();

			$favoruite = 1;

		}

		return response()->json( array('favoruite' => $favoruite) );

	}

	public function home_search_google()

	{	//echo Input::get('_token');die;



	$type = '';

	$total_record = '';

	if(isset($_POST['cuisine']))

	{

		Session::put('home_cuisine',Input::get('cuisine'));

		$type = 'cuisine';

		$total_record = '0';

	}

	if(isset($_POST['rest_type']))

	{

		Session::put('home_rest_type',Input::get('rest_type'));

		$type = 'rest_type';

		$total_record = '0';

	}

	if(isset($_POST['location']) && (!empty($_POST['location'])))

	{

		Session::put('_token',Input::get('_token'));

		Session::put('home_location',Input::get('location'));

		Session::put('home_street_number',Input::get('street_number'));

		Session::put('home_route',Input::get('route'));

		Session::put('home_locality',Input::get('locality'));

		Session::put('home_level',Input::get('level'));



		if(Input::get('postal_code')){

		Session::put('home_postal_code',Input::get('postal_code'));

	    }else{ Session::put('home_postal_code','452001'); }



		Session::put('home_country',Input::get('country'));

		Session::put('home_lat',Input::get('lat'));

		Session::put('home_lng',Input::get('lng'));



		$city = $this->getcity(Input::get('location'));



        if(!empty($city)){ Session::put('city',$city); }



			//echo Input::get('location'); die;

		$rest_listing = DB::table('search_restaurant_view');

		$rest_listing = $rest_listing->select('search_restaurant_view.*');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_address', 'like' ,"%".Input::get('location'));

		$rest_listing = $rest_listing->count();

		$type = 'location';

		$total_record = $rest_listing;

	}

	return response()->json( array('total_record' => $total_record,'search_type' => $type) );

}

function add_google_rest()

{

	$lat_long = explode(',',Input::get('rest_latlng'));

	$latitude= trim(str_replace('(','',$lat_long [0]));

	$longitude= trim(str_replace(')','',$lat_long [1]));

	$total_rec  = DB::table('restaurant')->where('rest_name', '=' ,Input::get('rest_name'))->count();

	if($total_rec==0)

	{

		$rest = new Restaurant;

		$rest->rest_name = Input::get('rest_name');

		$rest->rest_address = Input::get('rest_address');

		$rest->rest_logo = Input::get('rest_logo');

		$rest->rest_status ='PUBLISHED';

		$rest->google_type ='1';

		$rest->rest_lat = $latitude;

		$rest->rest_long = $longitude;

		$rest->save();

		$rest_id = $rest->rest_id;

		if(!empty($rest_id)){

			DB::enableQueryLog();

			$range = Input::get('rest_range');

			if($range>0)

			{

				$range=Input::get('rest_range');

				if($range<=5){

					$range=(Input::get('rest_range')*2);

				}

			}

			else

			{

				$range = 0;

			}

			$order_re = new Review;

			$order_re->re_orderid = 0;

			$order_re->re_userid = 0;

			$order_re->re_content = '';

			$order_re->re_restid = $rest_id;

			$order_re->re_rating = $range;

			$order_re->re_status = 'PUBLISHED';

			$order_re->save();

			$re_id = $order_re->re_id;

				//return response()->json( array('query' => DB::getQueryLog()) );

		}

	}

}



function get_current_location()

{

    

	$lat= Input::get('ip_lat');

	$long = Input::get('ip_lng');

	Session::put('home_location','');

	Session::put('home_street_number','');

	Session::put('home_route','');

	Session::put('home_locality',Input::get('ip_city')) ;

	Session::put('home_level','');

	if(Input::get('ip_postal_code')){

	Session::put('home_postal_code',Input::get('ip_postal_code'));

    }else{ Session::put('home_postal_code','Indore'); }

	Session::put('home_country',Input::get('ip_country'));

	if(Input::get('ip_lat')){ Session::put('home_lat',Input::get('ip_lat')); }else{ Session::put('home_lat','‎22.7533');}

	if(Input::get('ip_lng')){ Session::put('home_lng',Input::get('ip_lng')); }else{ Session::put('home_lng','75.8937');}

	$distancein_miles = 10;

	$count_data =count(DB::table('search_restaurant_view')

		->select(DB::raw("*,

			(((acos(sin((".$lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$long." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance"))

		->having("distance","<",$distancein_miles )

		->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE')

		->groupBy('search_restaurant_view.rest_id')

		->get());

	$google_data = '';

	$status = '';

	return response()->json( array('status' => 1) );

  }



    public function getcity($address){

    if(!empty($address)){

        //Formatted address

        $formattedAddr = str_replace(' ','+',$address);

        //Send request and receive json data by address

        $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=true_or_false&key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE'); 

        $output1 = json_decode($geocodeFromAddr);



       if($output1->status=='OK'){



        $latitude  = $output1->results[0]->geometry->location->lat; 

        $longitude = $output1->results[0]->geometry->location->lng;



        $city = '';

       

      foreach ($output1->results[0]->address_components as $key => $value) {



      if($value->types[0]=='locality'){



         $city = $value->long_name;



        }



      if($value->types[0]=='administrative_area_level_2'){



          $city = $value->long_name;   



        }



     }



     return $city;



     }else{  return "";  }



    }else{

        return "";   

    }

  }





}