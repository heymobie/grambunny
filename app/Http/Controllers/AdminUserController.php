<?php

namespace App\Http\Controllers;

use DB;

use Hash;

use Session;

use App\Http\Requests;

use App\Productservice;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Carbon\Carbon;

use App\User;

use App\Admin;

use Route;

use Illuminate\Http\Request;

use Mail;

class AdminUserController extends Controller
{

	public function __construct(){

		$this->middleware('admin');

	}

	public function admin_list()
	{

		DB::connection()->enableQueryLog();

		$admin_ids  = DB::table('admins')->value('id');

	 if($admin_ids!='1'){

	  return redirect()->to('/admin/dashboard');

	  }else{

      $admin_list = DB::table('admins')

		->select('*')

		->orderBy('id', 'asc')

		->get();

		$data_onview = array('admin_list' =>$admin_list);

	  return View('admin.admin_list')->with($data_onview);

	}

	}

	public function admin_form(Request $request)
	{
		$state_list = DB::table('states')->get();

		if($request->id){

	    $id = $request->id;
        $user_detail  = DB::table('admins')->where('id', '=' ,$id)->get();
		$user_list  = DB::table('admins')->where('status', '=' ,'1')->where('id', '!=' ,$id)->get();

        $data_onview = array('user_detail' =>$user_detail,'id'=>$id,'user_list'=>$user_list,'state_list'=>$state_list);

        return View('admin.admin_form')->with($data_onview);

		}else{

         $id =0;
         $user_list  = DB::table('admins')->where('status', '=' ,'1')->where('id', '!=' ,$id)->get();
         $data_onview = array('id'=>$id,'user_list'=>$user_list,'state_list'=>$state_list);

         return View('admin.admin_form')->with($data_onview);

		}
	}


		public function admin_action(Request $request)
	{

		$request->all();

		$admin_id = Input::get('admin_id');

		if($admin_id==0)
		{

		$this->validate($request,["email" => "required|email|unique:admins","password" => "required_with:confirmpassword|same:confirmpassword"]);

				$user = new Admin;
				$user->name = trim(Input::get('name'));
				$user->lname = trim(Input::get('lname'));
				$user->email = trim(Input::get('email'));
				$user->mobile_no = trim(Input::get('mobile_no'));
				$user->password =  Hash::make(trim(Input::get('password')));
				$user->status = trim(Input::get('status'));
				$user->save();

				$user_id = $user->id;

				Session::flash('message', 'Admin Inserted Sucessfully!');

				return redirect()->to('/admin/adminlist');

		    }else{

			$pass = Input::get('password');

			if(!empty($pass)){

				DB::table('admins')
				->where('id', $admin_id)
				->update(['name' => trim(Input::get('name')),

					'lname'=>  trim(Input::get('lname')),

					'status'=>trim(Input::get('status')),

					'password' => Hash::make(trim(Input::get('password'))),

					'mobile_no'=>trim(Input::get('mobile_no')),

					'email' => trim(Input::get('email'))


				]);

			}else{

				DB::table('admins')

				->where('id', $admin_id)

				->update(['name' => trim(Input::get('name')),

					'lname'=>  trim(Input::get('lname')),

					'status'=>trim(Input::get('status')),

					'mobile_no'=>trim(Input::get('mobile_no')),

					'email' => trim(Input::get('email'))

				]);

			}

			Session::flash('message', 'Admin Information Updated Sucessfully!');

			return redirect()->to('/admin/adminlist');

		}

	}


	public function user_list()
	{

		 /* DB::table('users')
        ->where('user_status',2)
		->update(['user_status'=>1, 'user_role'=>2]); */

		DB::connection()->enableQueryLog();

		$user_list = DB::table('users')

		->select('*')

		->orderBy('id', 'asc')

		->get();

		$data_onview = array('user_list' =>$user_list);

	 return View('admin.user_list')->with($data_onview);

	}


	public function abandoned_cart()
	{

		DB::connection()->enableQueryLog();

		$user_list = DB::table('users')

		->where('user_status', '=' ,1)

		->orderBy('id', 'asc')

		->get();


		$data_onview = array('user_list' =>$user_list);

	 return View('admin.abandoned_cart')->with($data_onview);

	}


	public function abandoned_cart_view(Request $request)
	{

	    $user_id = $request->id;

         $items=Productservice::select('product_service.*', 'crt.quantity', 'product_service.quantity as pquantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        $data_onview = array('items' =>$items);

        return View('admin.abandoned_cart_view')->with($data_onview);

	}

		public function export_in_excel(Request $request){

		$users_detail  = DB::table('users')->orderBy('id', 'ASC')->get();

        $user_status = '';
		// Excel file name for download
		$fileName = "Customer_" . date('Y-m-d') . ".xls";

		// Column names
		$fields = array('Customer Id', 'Customer Name', 'Contact Number', 'Email', 'Date of Birth', 'Address', 'City', 'State', 'Zip Code', 'User status');

		$excelData = implode("\t", array_values($fields)) . "\n";

		if($users_detail){

		foreach ($users_detail as $key => $value) {

	    if($value->user_status==0){  $user_status='InActive'; }else{ $user_status='Active'; }

        $lineData = array($value->id, $value->name.' '.$value->lname, $value->user_mob, $value->email, $value->dob, $value->user_address, $value->user_city, $value->user_states, $value->user_zipcode, $user_status);


       // array_walk($lineData, 'filterData');
        $excelData .= implode("\t", array_values($lineData)) . "\n";

	}

	}else{

		$excelData .= 'No records found...'. "\n";

	}
					// Headers for download
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"$fileName\"");

		// Render excel data
		echo $excelData;

		exit;

	}


	public function user_form(Request $request)

	{

		$state_list = DB::table('states')->get();

		if($request->id){

	    $id = $request->id;
        $user_detail  = DB::table('users')->where('id', '=' ,$id)->get();
		$user_list  = DB::table('users')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();

        $data_onview = array('user_detail' =>$user_detail,'id'=>$id,'user_list'=>$user_list,'state_list'=>$state_list);

        return View('admin.user_form')->with($data_onview);

		}else{

         $id =0;
         $user_list  = DB::table('users')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();
         $data_onview = array('id'=>$id,'user_list'=>$user_list,'state_list'=>$state_list);

         return View('admin.user_form')->with($data_onview);

		}
	}

	public function user_action(Request $request)
	{

		$request->all();

		$this->validate($request,[

		 	//"firstname"	=> "required|min:3",
//
		 	//"lastname"	=> "required|min:3",

		 	//"email"		=> "required|email|unique:users",

		 	//"city"		=> "required",

		 	"dob"		=> "required|date",

		 	//"phone"		=> "required|min:10",

		 	//"password"	=> "required|min:6"

		]);

		$user_id = Input::get('user_id');
		$address = Input::get('user_address');
		//$user_city = Input::get('user_city');
		//$user_states = Input::get('user_states');
		$zipcode = Input::get('user_zipcode');
		$addressnew = $zipcode;

		//$user_dobs = date("Y/m/d", strtotime($request->user_dob));


		  if(preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $request->dob)) {

       }else{

      return redirect()->back()->withErrors(["dob" => "Please use valid date format (mm/dd/yyyy)"])->withInput();

       }

		$date=Carbon::now();

        $minDate= $date->subYear(21);

		$dob=Carbon::createFromFormat("m/d/Y",$request->dob);

		$dob = date("Y-m-d", strtotime($request->dob));

		 if (!$minDate->greaterThan($dob)) {

		return redirect()->back()->withErrors(["dob" => "Date must be greater then 21 Year."])->withInput();

		 }

		//print_r($addressnew);die;
		$profile_old = Input::get('profile_old');

	//	print_r($profile_old);die;

		           $profile_image = $profile_old;

                   if(empty($profile_image)){ $profile_image = "dummy.png"; }

		$license_front_image = $request->license_front_old;
		$license_back_image = $request->license_back_old;
		$marijuana_card_image = $request->marijuana_card_old;

 /*
        $url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

		$response = file_get_contents($url);

		$data = json_decode($response);
		if(!empty($data->results)){
			$geo=$data->results[0]->geometry->location;
		}else{
			$url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$addressnew."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

			$response = file_get_contents($url);

			$data = json_decode($response);
			$geo=$data->results[0]->geometry->location;

		}
		 */


		$request->hasFile("profile") ? $request->file("profile")->move("public/uploads/user/",$profile_image=str_random(16).'.jpg') : "";


		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/user/",$license_front_image=str_random(16).'.'.$request->license_front->extension()) : "";

		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/user/",$license_back_image=str_random(16).'.'.$request->license_back->extension()) : "";

		$request->hasFile("marijuana_card") ? $request->file("marijuana_card")->move("public/uploads/user/",$marijuana_card_image=str_random(16).'.'.$request->marijuana_card->extension()) : "";

		if(empty($profile_image)){ $profile_image = "user.jpg"; }

		if($user_id==0)
		{


	//	$this->validate($request,["email" => "required|email|unique:users"]);

		$this->validate($request,["email" => "required|email|unique:users","password" => "required_with:confirmpassword|same:confirmpassword"]);

				$user = new User;
				$user->name = trim(Input::get('name'));
				$user->lname = trim(Input::get('lname'));
				$user->email = trim(Input::get('email'));
				$user->password =  Hash::make(trim(Input::get('password')));
				$user->user_address = trim(Input::get('user_address'));
				$user->user_city = trim(Input::get('user_city'));
				$user->user_zipcode = trim(Input::get('user_zipcode'));
				$user->user_status = trim(Input::get('user_status'));
				$user->user_pass= md5(Input::get('password'));
				$user->user_mob = Input::get('user_mob');
				$user->dob = $dob;
				$user->user_states = Input::get('user_states');
				$user->profile_image = $profile_image;
				$user->user_lat = '';
                $user->user_long = '';
                $user->license_front = $license_front_image;
                $user->license_back = $license_back_image;
                $user->customer_type = trim(Input::get('customer_type'));
                $user->marijuana_card = $marijuana_card_image;
				$user->save();

				$user_id = $user->id;

				Session::flash('message', 'User Inserted Sucessfully!');

				return redirect()->to('/admin/userlist');

		    }else{

			$pass = Input::get('password');

			if(!empty($pass)){

				DB::table('users')
				->where('id', $user_id)
				->update(['name' => trim(Input::get('name')),

					'lname'=>  trim(Input::get('lname')),

					'user_address'=>trim(Input::get('user_address')),

					'user_city'=>trim(Input::get('user_city')),

					'user_status'=>trim(Input::get('user_status')),

					'user_zipcode'=>trim(Input::get('user_zipcode')),

					'password' => Hash::make(trim(Input::get('password'))),

					'user_pass' => md5(Input::get('password')),

					'user_mob'=>trim(Input::get('user_mob')),

					'email' => trim(Input::get('email')),

					'dob'=>  $dob,

					'user_states'=>  Input::get('user_states'),

					'profile_image'=>  $profile_image,

					'license_front'=>  $license_front_image,

					'license_back'=>  $license_back_image,

					'customer_type'=>trim(Input::get('customer_type')),

					'marijuana_card'=>  $marijuana_card_image,

					'user_lat'=> '',

					'user_long'=> ''

				]);

			}else{

				DB::table('users')

				->where('id', $user_id)

				->update(['name' => trim(Input::get('name')),

					'lname'=>  trim(Input::get('lname')),

					'user_address'=>trim(Input::get('user_address')),

					'user_city'=>trim(Input::get('user_city')),

					'user_status'=>trim(Input::get('user_status')),

					'user_zipcode'=>trim(Input::get('user_zipcode')),

					'user_mob'=>trim(Input::get('user_mob')),

					'email' => trim(Input::get('email')),

					'dob'=>  $dob,

					'user_states'=>  Input::get('user_states'),

					'profile_image'=>  $profile_image,

					'license_front'=>  $license_front_image,

					'license_back'=>  $license_back_image,

					'customer_type'=>trim(Input::get('customer_type')),

					'marijuana_card'=>  $marijuana_card_image,

					'user_lat'=> '',

					'user_long'=> ''

				]);

			}

			Session::flash('message', 'User Information Updated Sucessfully!');

			return redirect()->to('/admin/userlist');

		}

	}

	public function crop_profile_image_save(Request $request)
	{

		//echo "text";die;

		$image = Input::get('productimg');

		//$vendor_id = Input::get('vendor_id');
		$user_id = Input::get('user_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $profile_img= '1'.time().'.png';
        $path = public_path('uploads/user/'.$profile_img);
        file_put_contents($path, $image);

        DB::table('users')
		//->where('vendor_id', $vendor_id)
		->where('id', $user_id)
		->update(['profile_image' => $profile_img]);

		// DB::table('ps_images')
		// ->where('ps_id', $product_id)
		// ->where('thumb', 1)
		// ->update(['name' => $profile_img]);

        return response()->json(['status'=>$profile_img]);

	}

	public function user_duplicate_email()

	{

		$input['email'] = Input::get('email');

		$input1['user_mob'] = Input::get('user_mob');

		$rules = array('email' => 'unique:users,email');

		$validator = Validator::make($input, $rules);

		$rules1 = array('user_mob' => 'unique:users,user_mob');

		$validator1 = Validator::make($input1, $rules1);

		if(($validator->fails()) && ($validator1->fails())) {

		echo '1';

		}

		elseif($validator->fails()) {

			echo '2';

		}elseif($validator1->fails()) {

			echo '3';

		}else {

			echo '4';

		}

	}

	public function user_delete($id)

	{

		$orders =DB::table('orders')->where('user_id', '=', $id)->pluck('id');

		DB::table('orders_ps')->whereIn('order_id', $orders)->delete();

		DB::table('orders')->where('user_id', '=', $id)->delete();

		DB::table('device_token')->where('userid', '=', $id)->delete();

		DB::table('users')->where('id', '=', $id)->delete();

		Session::flash('message', 'User Information Deleted Sucessfully!');

		return Redirect('/admin/userlist');


	}

	public function admin_delete($id)

	{

		DB::table('admins')->where('id', '=', $id)->delete();

		Session::flash('message', 'Admin Information Deleted Sucessfully!');

		return Redirect('/admin/adminlist');


	}



}



