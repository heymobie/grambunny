<?php
namespace App\Http\Controllers\subadmin_controllers;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Route;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Restaurant;
use App\Menu;
use App\Menu_category;
use App\Menu_category_item;
use App\Menu_category_addon;
use App\Service;
use App\Bankdetail;
use App\Promotion;
class SubadminResturantDetailController extends Controller
{
	public function __construct(){
		//$this->middleware('admin');
		$check = session()->get('user_id');
		if(count($check)!=1){
			Redirect::to('/sub-admin')->send();
		}
	}
	public function restaurant_search()
	{
		$rest_list = DB::table('restaurant');
		$rest_list = $rest_list->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id');
		if( (Input::get('rest_cont')) && (!empty(Input::get('rest_cont'))))
		{
			$rest_list = $rest_list->where('restaurant.rest_contact', 'like' ,Input::get('rest_cont'));
			$rest_list = $rest_list->orwhere('restaurant.rest_landline', 'like' ,Input::get('rest_cont'));
		}
		if( (Input::get('rest_name')) && (!empty(Input::get('rest_name'))))
		{
			$rest_list = $rest_list->where('restaurant.rest_name', 'like' ,'%'.Input::get('rest_name').'%');
		}
		$rest_list = $rest_list->select('restaurant.*','vendor.name');
		$rest_list = $rest_list->orderBy('restaurant.rest_id', 'desc');
		$rest_list = $rest_list->get();
		$data_onview = array('rest_list' =>$rest_list);
		return View('subadmin.ajax.restaurant_list')->with($data_onview);
	}
	public function restaurant_form()
	{
		/*$vendor_detail = DB::table('vendor')
			 ->select('*')
			->orderBy('vendor_id', 'desc')
			->get();*/
			$mon_from='';
			$mon_to='';
			$tues_from='';
			$tues_to='';
			$wed_from='';
			$wed_to='';
			$thu_from='';
			$thu_to='';
			$fri_from='';
			$fri_to='';
			$sat_from='';
			$sat_to='';
			$sun_from='';
			$sun_to='';
			if(Route::current()->getParameter('id'))
			{
				$id = Route::current()->getParameter('id');
				$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$id)->get();
				if(($id>0) && (!empty($rest_detail[0]->rest_mon))){
					$v = explode('_',$rest_detail[0]->rest_mon);
					$mon_from=$v[0];
					$mon_to=$v[1];;
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_tues))){
					$v = explode('_',$rest_detail[0]->rest_tues);
					$tues_from=$v[0];
					$tues_to=$v[1];;
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_wed))){
					$v = explode('_',$rest_detail[0]->rest_wed);
					$wed_from=$v[0];
					$wed_to=$v[1];;
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_thus))){
					$v = explode('_',$rest_detail[0]->rest_thus);
					$thu_from=$v[0];
					$thu_to=$v[1];;
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_fri))){
					$v = explode('_',$rest_detail[0]->rest_fri);
					$fri_from=$v[0];
					$fri_to=$v[1];
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_sat))){
					$v = explode('_',$rest_detail[0]->rest_sat);
					$sat_from=$v[0];
					$sat_to=$v[1];
				}
				if(($id>0) && (!empty($rest_detail[0]->rest_sun))){
					$v = explode('_',$rest_detail[0]->rest_sun);
					$sun_from=$v[0];
					$sun_to=$v[1];
				}
				$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();
				$data_onview = array('rest_detail' =>$rest_detail,
					'mon_from' =>$mon_from,
					'mon_to' =>$mon_to,
					'tues_from' =>$tues_from,
					'tues_to' =>$tues_to,
					'wed_from' =>$wed_from,
					'wed_to' =>$wed_to,
					'thu_from' =>$thu_from,
					'thu_to' =>$thu_to,
					'fri_from' =>$fri_from,
					'fri_to' =>$fri_to,
					'sat_from' =>$sat_from,
					'sat_to' =>$sat_to,
					'sun_from' =>$sun_from,
					'sun_to' =>$sun_to,
					'cuisine_list'=>$cuisine_list,
					'id'=>$id);
			}
			else
			{
				$vendor_id =  Route::current()->getParameter('vendor_id');
				$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();
				$id =0;
				$data_onview = array('vendor_id' =>$vendor_id,
					'mon_from' =>$mon_from,
					'mon_to' =>$mon_to,
					'tues_from' =>$tues_from,
					'tues_to' =>$tues_to,
					'wed_from' =>$wed_from,
					'wed_to' =>$wed_to,
					'thu_from' =>$thu_from,
					'thu_to' =>$thu_to,
					'fri_from' =>$fri_from,
					'fri_to' =>$fri_to,
					'sat_from' =>$sat_from,
					'sat_to' =>$sat_to,
					'sun_from' =>$sun_from,
					'sun_to' =>$sun_to,
					'cuisine_list'=>$cuisine_list,
					'id'=>$id);
			}
			return View('subadmin.restaurant_form')->with($data_onview);
		}
		function clean_string($string){
			$string1 = strtolower($string);
		//Make alphanumeric (removes all other characters)
			$string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);
		//Clean up multiple dashes or whitespaces
			$string1 = preg_replace("/[\s-]+/", " ", $string1);
		//Convert whitespaces and underscore to dash
			$string1 = preg_replace("/[\s_]/", "-", $string1);
			if( !empty($string1) || $string1 != '' ){
				return $string1;
			}else{
				return $string;
			}
		}
		public function restaurant_action(Request $request)
		{
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		$meta = $this->clean_string(trim(Input::get(['rest_name'])));
		$rest_id = Input::get('rest_id');
		$rest_old_logo = Input::get('rest_old_logo');
		$new_fileName = '';
		if(isset($_FILES['rest_logo']) && (!empty($_FILES['rest_logo']['name'][0]))){
			if(!empty($rest_old_logo)){
				$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 // $file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;
				if(file_exists($file_remove)){
					unlink($file_remove);
				}
			}
			$destinationPath = 'uploads/reataurant/';
			$image = $request->file('rest_logo');
			$extension 		= 	$image->getClientOriginalExtension();
			$imageRealPath 	= 	$image->getRealPath();
    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();
			if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))
			{
				$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();
				$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/'.$thumbName);;
				asset($destinationPath . $thumbName);
				$new_fileName = $thumbName;
			}
			else
			{
				Session::flash('message_error', 'Image type is invalide!');
				if($rest_id>0){
					return redirect()->to('/sub-admin/restaurant-form/'.$rest_id);
				}
				else
				{
					return redirect()->to('/sub-admin/vendor-rest-form/'.Input::get('vendor_id'));
				}
			}
		}
		elseif(!empty($rest_old_logo))
		{
			$new_fileName = $rest_old_logo;
		}
		if(Input::get('rest_service')){
			$rest_service = implode(',',Input::get('rest_service'));
		}
		else
		{
			$rest_service ='';
		}
		if(Input::get('close_day')){
			$rest_close = implode(',',Input::get('close_day'));
		}
		else
		{
			$rest_close ='';
		}
		$today_special='0';
		if(Input::get('rest_special'))
		{
			$today_special='1';
		}
		if($rest_id==0)
		{
			$rest = new Restaurant;
			$rest->rest_name = Input::get('rest_name');
			$rest->vendor_id =Input::get('vendor_id');
			$rest->rest_address = Input::get('rest_address');
			$rest->rest_city = Input::get('rest_city');
			$rest->rest_zip_code =  Input::get('rest_zip_code');
			$rest->rest_desc =  Input::get('rest_desc');
			$rest->rest_logo = $new_fileName;
			$rest->rest_service =$rest_service;
			$rest->rest_state = Input::get('rest_state');
			$rest->rest_suburb =Input::get('rest_suburb');
			$rest->rest_address2 =Input::get('rest_address2');
			$rest->rest_contact =Input::get('rest_contact');
			$rest->rest_special = $today_special;
			$rest->rest_contact =Input::get('rest_contact');
			$rest->rest_cuisine =Input::get('rest_cuisine');
			$rest->rest_mindelivery =Input::get('rest_mindelivery');
			$rest->rest_landline =Input::get('rest_landline');
			$rest->rest_commission =Input::get('rest_commission');
			$rest->rest_email =Input::get('rest_email');
			$rest->rest_status =Input::get('rest_status');
			$rest->rest_classi =Input::get('rest_classi');
			$rest->rest_metatag =$meta;
			$rest->rest_mon =Input::get('mon_from').'_'.Input::get('mon_to');
			$rest->rest_tues =Input::get('tues_from').'_'.Input::get('tues_to');
			$rest->rest_wed =Input::get('wed_from').'_'.Input::get('wed_to');
			$rest->rest_thus =Input::get('thu_from').'_'.Input::get('thu_to');
			$rest->rest_fri =Input::get('fri_from').'_'.Input::get('fri_to');
			$rest->rest_sat =Input::get('sat_from').'_'.Input::get('sat_to');
			$rest->rest_sun =Input::get('sun_from').'_'.Input::get('sun_to');
			$rest->rest_close =$rest_close;
			$rest->save();
			$rest_id = $rest->id;
			Session::flash('message', 'Restaurant Inserted Sucessfully!');
				//return redirect()->to('/admin/restaurant_list');
			return redirect()->to('/sub-admin/vendor_profile/'.Input::get('vendor_id'));
		}
		else
		{
			DB::table('restaurant')
			->where('rest_id', $rest_id)
			->update(['rest_name' =>Input::get('rest_name'),
				'vendor_id' =>Input::get('vendor_id'),
				'rest_address'=>Input::get('rest_address'),
				'rest_city'=>Input::get('rest_city'),
				'rest_zip_code'=>Input::get('rest_zip_code'),
				'rest_desc'=>Input::get('rest_desc'),
				'rest_service'=>$rest_service,
				'rest_state'=>Input::get('rest_state'),
				'rest_suburb'=>Input::get('rest_suburb'),
				'rest_address2'=>Input::get('rest_address2'),
				'rest_contact'=>Input::get('rest_contact'),
				'rest_special' => $today_special,
				'rest_mon'=>Input::get('mon_from').'_'.Input::get('mon_to'),
				'rest_tues'=>Input::get('tues_from').'_'.Input::get('tues_to'),
				'rest_wed'=>Input::get('wed_from').'_'.Input::get('wed_to'),
				'rest_thus'=>Input::get('thu_from').'_'.Input::get('thu_to'),
				'rest_fri'=>Input::get('fri_from').'_'.Input::get('fri_to'),
				'rest_sat'=>Input::get('sat_from').'_'.Input::get('sat_to'),
				'rest_sun'=>Input::get('sun_from').'_'.Input::get('sun_to'),
				'rest_cuisine'=>Input::get('rest_cuisine'),
				'rest_mindelivery'=>Input::get('rest_mindelivery'),
				'rest_landline'=>Input::get('rest_landline'),
				'rest_commission'=>Input::get('rest_commission'),
				'rest_email'=>Input::get('rest_email'),
				'rest_status'=>Input::get('rest_status'),
				'rest_classi'=>Input::get('rest_classi'),
				'rest_close'=>$rest_close,
				'rest_logo'=>$new_fileName,
				'rest_metatag' => $meta
			]);
			session()->flash('message', 'Restaurant Update Sucessfully!');
				//return redirect()->to('/admin/restaurant_list');
			return redirect()->to('/sub-admin/restaurant_view/'.$rest_id);
		}
	}
	public function restaurant_delete($rest_id)
	{
		$rest_id;
		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		if(!empty($rest_detail))
		{
			$rest_old_logo = $rest_detail[0]->rest_logo;
			$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;
			if(file_exists($file_remove)){
				unlink($file_remove);
			}
			$menu =  DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();
			if(!empty($menu))
			{
				DB::table('menu')->where('restaurant_id', '=', $rest_id)->delete();
			}
			$menu_cat =  DB::table('menu_category')->where('rest_id', '=' ,$rest_id)->get();
			if(!empty($menu_cat))
			{
				DB::table('menu_category')->where('rest_id', '=', $rest_id)->delete();
			}
			DB::table('restaurant')->where('rest_id', '=', $rest_id)->delete();
			return Redirect('/sub-admin/restaurant_list');
		}
	}
	public function view_restaurant_old()
	{
		$id = Route::current()->getParameter('id');
		$rest_list = DB::table('restaurant')
		->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')
		->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')
		->where('rest_id', '=' ,$id)
		->select('restaurant.*','vendor.name','cuisine.cuisine_name')
		->orderBy('restaurant.rest_id', 'desc')
		->get();
		$menu_list = DB::table('menu')
		->where('restaurant_id', '=' ,$id)
		->orderBy('menu_order', 'asc')
		->get();
		$service_list = DB::table('service')
		->where('service_restid', '=' ,$id)
		->orderBy('service_id', 'desc')
		->get();
		$promo_list = DB::table('promotion')
		->where('promo_restid', '=' ,$id)
		->orderBy('promo_id', 'desc')
		->get();
		$bank_list = DB::table('bankdetail')
		->where('bank_restid', '=' ,$id)
		->orderBy('bank_id', 'desc')
		->get();
		$menu_cat_detail = '';
		if(!empty($menu_list)){
			$menu_cat_detail = DB::table('menu_category')
			->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
			->where('menu_category.rest_id', '=' ,$id)
			->where('menu_category.menu_cat_popular', '=' ,'1')
			->select('menu_category.*','menu_category_item.*')
			->orderBy('menu_category.menu_category_id', 'desc')
			->get();
					//					->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)
		}
		$template_list = DB::table('template')
		->where('template_status', '=' ,'1')
		->join('template_menu', 'template.template_id', '=', 'template_menu.template_id')
		->orderBy('template.template_id', 'desc')
		->groupBy('template.template_id')
		->get();
		$data_onview = array('rest_detail' =>$rest_list,
			'menu_list'=>$menu_list,
			'id'=>$id,
			'menu_cate_detail'=>$menu_cat_detail,
			'service_list'=>$service_list,
			'promo_list'=>$promo_list,
			'bank_list'=>$bank_list,
			'template_list'=>$template_list
		);
		return View('subadmin.restaurant_view')->with($data_onview);
	}
	public function view_restaurant()
	{

		$zipcode = Auth::guard('subadmin')->user()->zipcoad;

		$area_id = DB::table('area_management')
				  ->where('zipcode', '=' , $zipcode)
		          ->value('area_id'); 

		DB::enableQueryLog();
		$id = Route::current()->getParameter('id');
		$rest_list = DB::table('restaurant')
		->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')
//					->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')
		->where('rest_id', '=' ,$id)
		->select("restaurant.*","vendor.name",
			DB::raw("(SELECT GROUP_CONCAT(cuisine_name SEPARATOR ', ') AS `combined_A` FROM `cuisine` WHERE FIND_IN_SET( cuisine_id,restaurant.rest_cuisine)) as cuisine_name")
		)
		->orderBy('restaurant.rest_id', 'desc')
		->get();
		//dd(DB::getQueryLog());
		$menu_list = DB::table('menu')
		->where('restaurant_id', '=' ,$id)
		->where('area_id', '=' ,$area_id)
		->orderBy('menu_order', 'asc')
		->get();
		$service_list = DB::table('service')
		->where('service_restid', '=' ,$id)
		->orderBy('service_id', 'desc')
		->get();
		$area_list = DB::table('area_management')->get();
		$promo_list = DB::table('promotion')
		->where('promo_restid', '=' ,$id)
		->orderBy('promo_id', 'desc')
		->get();
		$bank_list = DB::table('bankdetail')
		->where('bank_restid', '=' ,$id)
		->orderBy('bank_id', 'desc')
		->get();
		$menu_cat_detail = '';
		if(!empty($menu_list)){
			$menu_cat_detail = DB::table('menu_category')
			->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
			->where('menu_category.rest_id', '=' ,$id)
			->where('menu_category.area_id', '=' ,$area_id)
			->where('menu_category.menu_cat_popular', '=' ,'1')
			->select('menu_category.*','menu_category_item.*')
			->orderBy('menu_category.menu_category_id', 'desc')
			->get();
					//					->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)
		}
		$template_list = DB::table('template')
		->where('template_status', '=' ,'1')
		->join('template_menu', 'template.template_id', '=', 'template_menu.template_id')
		->orderBy('template.template_id', 'desc')
		->groupBy('template.template_id')
		->get();
		$data_onview = array('rest_detail' =>$rest_list,
			'menu_list'=>$menu_list,
			'id'=>$id,
			'menu_cate_detail'=>$menu_cat_detail,
			'service_list'=>$service_list,
			'promo_list'=>$promo_list,
			'bank_list'=>$bank_list,
			'template_list'=>$template_list,
			'area_list'=>$area_list
		);
		return View('subadmin.restaurant_view')->with($data_onview);
	}
}
