<?php
namespace App\Http\Controllers\subadmin_controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Route;
class SubadminareaController extends Controller
{
	public function __construct(){
		$check = session()->get('user_id');
		if(count($check)!=1){
			Redirect::to('/sub-admin')->send();
		}
	}
	/*areas list*/
	public function area_List()
	{
		DB::connection()->enableQueryLog();
		$area_list = DB::table('area_management')
		->select('*')
		->orderBy('area_id', 'desc')
		->get();
		$data_onview = array('area_list' =>$area_list);
		return View('subadmin.area_list')->with($data_onview);
	}
	/*Add area form*/
	public function area_form()
	{
		return View('subadmin.area_form');
	}
	/*Update area details*/
	public function UpdateAreaDetail(Request $request)
	{
		$postData = Request::all();
		$area_id = $postData['area_id'];
		$AreaData = array(
			'area_name' => trim($postData['address']),
			'place_id' => trim($postData['place_ID']),
			'area_city' => trim($postData['city']),
			'area_state' => trim($postData['state']),
			'area_country' =>trim($postData['country']),
			'route' => trim($postData['route']),
			'zipcode' => trim($postData['zipcode']),
			'update_at'=>date('y-m-d h:i:s')
		);
		$affected = DB::table('area_management')
		->where('area_id', $area_id)
		->update($AreaData);
		if($affected){
			Session::flash('message', 'Area details updated sucessfully!');
			return redirect()->to('/sub-admin/area_list');
		}
	}
	/*Delete area*/
	public function area_delete($id)
	{
		DB::table('area_management')->where('area_id', '=', $id)->delete();
		Session::flash('message', 'Area deleted sucessfully!');
		return redirect()->to('/sub-admin/area_list');
	}
	/*Edit area*/
	public function EditareaForm()
	{
		if(Route::current()->getParameter('id'))
		{
			$area_id = Route::current()->getParameter('id');
			$area_detail  = DB::table('area_management')->where('area_id', '=' ,$area_id)->get();
			$data_onview = array(
				'area_detail' =>$area_detail,
				'area_id'=>$area_id,
			);
			return View('subadmin.area_edit_form')->with($data_onview);
		}
	}
	/*add new area details*/
	public function addarea(Request $request)
	{
		$postData = Request::all();
		$AreaData = array(
			'area_name' => trim($postData['address']),
			'place_id' => trim($postData['place_ID']),
			'area_city' => trim($postData['city']),
			'area_state' => trim($postData['state']),
			'area_country' =>trim($postData['country']),
			'route' => trim($postData['route']),
			'zipcode' => trim($postData['zipcode']),
		);
		$affected = DB::table('area_management')->insert($AreaData);
		if($affected){
			Session::flash('message', 'Area added sucessfully!');
			return redirect()->to('/sub-admin/area_list');
		}
	}
	/*View area details*/
	public function ViewArea()
	{
		$area_id =	Route::current()->getParameter('id');
		$Areadetail  = DB::table('area_management')->where('area_id', '=' ,$area_id)->get();
		$data_onview = array('Areadetail' =>$Areadetail);
		return View('subadmin.area_view')->with($data_onview);
	}
}