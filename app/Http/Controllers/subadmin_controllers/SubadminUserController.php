<?php

namespace App\Http\Controllers\subadmin_controllers;

Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use App\User;

use Route;

use Illuminate\Http\Request;

use Mail;

class SubadminUserController extends Controller

{

	public function __construct(){

		$check = session()->get('user_id');

		if(count($check)!=1){

			Redirect::to('/sub-admin')->send();

		}

	}

	public function user_list()

	{



		$role = session()->get('user_role');

		DB::connection()->enableQueryLog();

		$user_list = DB::table('users')

		->select('*')

		->orderBy('id', 'desc')

		->get();

		$data_onview = array('user_list' =>$user_list);

		return View('subadmin.user_list')->with($data_onview);

	}

	public function user_form()

	{

		if(Route::current()->getParameter('id'))

		{

			$id = Route::current()->getParameter('id');

			$user_detail  = DB::table('users')->where('id', '=' ,$id)->get();

			$user_list  = DB::table('users')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();

			$data_onview = array('user_detail' =>$user_detail,

				'id'=>$id,

				'user_list'=>$user_list);

			return View('subadmin.user_form')->with($data_onview);

		}

		else

		{

			$id =0;

			$user_list  = DB::table('users')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();

			$data_onview = array('id'=>$id,

				'user_list'=>$user_list);

			return View('subadmin.user_form')->with($data_onview);

		}

	}

	public function user_action(Request $request)

	{

		/*echo '<pre>';

		print_r($_POST);

		exit;*/

		$user_id = Input::get('user_id');

		$carrier_id = '';

		$carrier_name = '';

		$carrier_email = '';

		$carrier_id =  Input::get('user_mob_type');

		switch($carrier_id)

		{

			case 1:

			$carrier_id = '1';

			$carrier_name = 'Alltel';

			$carrier_email = '@message.alltel.com';

			break;

			case 2:

			$carrier_id = '2';

			$carrier_name = 'AT&T';

			$carrier_email = '@txt.att.net';

			break;

			case 3:

			$carrier_id = '3';

			$carrier_name = 'Boost Mobile';

			$carrier_email = '@myboostmobile.com';

			break;

			case 4:

			$carrier_id = '4';

			$carrier_name = 'Sprint';

			$carrier_email = '@messaging.sprintpcs.com';

			break;

			case 5:

			$carrier_id = '5';

			$carrier_name = 'T-Mobile';

			$carrier_email = '@tmomail.net';

			break;

			case 6:

			$carrier_id = '6';

			$carrier_name = 'U.S. Cellular';

			$carrier_email = '@email.uscc.net';

			break;

			case 7:

			$carrier_id = '7';

			$carrier_name = 'Verizon';

			$carrier_email = '@vtext.com';

			break;

			case 8:

			$carrier_id = '8';

			$carrier_name = 'Virgin Mobile';

			$carrier_email = '@vmobl.com';

			break;

			case 9:

			$carrier_id = '9';

			$carrier_name = 'Republic Wireless';

			$carrier_email = '@text.republicwireless.com';

			break;

		}

		if($user_id==0)

		{

			$input['email'] = Input::get('email');

			// Must not already exist in the `email` column of `users` table

			$rules = array('email' => 'unique:users,email');

			$validator = Validator::make($input, $rules);

			$msg = 'Duplicate Email';

			if ($validator->fails()) {

				return Redirect::away('member/property_listing')->with('msg');

			}

			else {

				$carrier_id = '';

				$carrier_name = '';

				$carrier_email = '';

				$carrier_id =  Input::get('user_mob_type');

				switch($carrier_id)

				{

					case 1:

					$carrier_id = '1';

					$carrier_name = 'Alltel';

					$carrier_email = '@message.alltel.com';

					break;

					case 2:

					$carrier_id = '2';

					$carrier_name = 'AT&T';

					$carrier_email = '@txt.att.net';

					break;

					case 3:

					$carrier_id = '3';

					$carrier_name = 'Boost Mobile';

					$carrier_email = '@myboostmobile.com';

					break;

					case 4:

					$carrier_id = '4';

					$carrier_name = 'Sprint';

					$carrier_email = '@messaging.sprintpcs.com';

					break;

					case 5:

					$carrier_id = '5';

					$carrier_name = 'T-Mobile';

					$carrier_email = '@tmomail.net';

					break;

					case 6:

					$carrier_id = '6';

					$carrier_name = 'U.S. Cellular';

					$carrier_email = '@email.uscc.net';

					break;

					case 7:

					$carrier_id = '7';

					$carrier_name = 'Verizon';

					$carrier_email = '@vtext.com';

					break;

					case 8:

					$carrier_id = '8';

					$carrier_name = 'Virgin Mobile';

					$carrier_email = '@vmobl.com';

					break;

					case 9:

					$carrier_id = '8';

					$carrier_name = 'Republic Wireless';

					$carrier_email = '@text.republicwireless.com';

					break;

				}

				$user = new User;

				$user->name = trim(Input::get('name'));

				$user->lname = trim(Input::get('lname'));

				$user->email = trim(Input::get('email'));

				$user->password =  Hash::make(trim(Input::get('password')));

				$user->user_address = trim(Input::get('user_address'));

				$user->user_city = trim(Input::get('user_city'));

				$user->user_zipcode = trim(Input::get('user_zipcode'));

				$user->user_status = trim(Input::get('user_status'));

				//$user->user_status = '1';

				$user->user_pass= md5(Input::get('password'));

				$user->carrier_id= $carrier_id;

				$user->carrier_name= $carrier_name;

				$user->carrier_email= $carrier_email;

				$user->user_mob = Input::get('user_mob');

				// $user->dummy1 = Input::get('dummy1');

				// $user->dummy2 = Input::get('dummy2');

				// $user->dummy3 = Input::get('dummy3');

				$user->save();

				$user_id = $user->id;

				Session::flash('message', 'User Inserted Sucessfully!');

				/******************* SEND EMAIL AFTER PAYMENT  *******************/

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'1')

				->get();

				$data = array(

					'vendor_name'=>Input::get('name').' '.Input::get('last_name'),

					'vendor_email'=>Input::get('email'),

					'email_content'=>$email_content_detail[0]->email_content

				);

				Mail::send('emails.registration_email', $data, function ($message) use ($data) {

            //$m->from('hello@app.com', 'Your Application');

           // $m->to($user->email, $user->name)->subject('Your Reminder!');

					$message->from('info@grambunny.com', 'grambunny');

					$message->to($data['vendor_email']);

					$message->bcc('votiveshweta@gmail.com');

					$message->bcc('votivemobile.pankaj@gmail.com');

					$message->subject('Registration on www.grambunny.com');

				});

				/******************* SEND EMAIL AFTER PAYMENT  *******************/

				return redirect()->to('/sub-admin/userlist');

			}

		}

		else

		{

			DB::table('users')

			->where('id', $user_id)

			->update(['name' => trim(Input::get('name')),

				'lname'=>  trim(Input::get('lname')),

				'user_address'=>trim(Input::get('user_address')),

				'user_city'=>trim(Input::get('user_city')),

				'user_status'=>trim(Input::get('user_status')),

				'user_zipcode'=>trim(Input::get('user_zipcode')),

				'carrier_id'=>trim($carrier_id),

				'carrier_name'=>trim($carrier_name),

				'carrier_email'=>trim($carrier_email),

				'user_mob'=>trim(Input::get('user_mob')),

				// 'dummy1'=> trim(Input::get('dummy1')),

				// 'dummy2'=>trim(Input::get('dummy2')),

				// 'dummy3'=> trim(Input::get('dummy3'))

			]);

			Session::flash('message', 'User Information Updated Sucessfully!');

				//Session::flash('new', 'User Inserted Sucessfully!');

			return redirect()->to('/sub-admin/userlist');

		}

	}

	public function user_duplicate_email()

	{

		// Get the value from the form

		/*$input['email'] = Input::get('email');

		// Must not already exist in the `email` column of `users` table

		$rules = array('email' => 'unique:users,email');

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {

			echo '1';

		}

		else {

			echo '2';

		}*/

		// Get the value from the form

		$input['email'] = Input::get('email');

		$input1['user_mob'] = Input::get('user_mob');

		// Must not already exist in the `email` column of `users` table

		$rules = array('email' => 'unique:users,email');

		$validator = Validator::make($input, $rules);

		$rules1 = array('user_mob' => 'unique:users,user_mob');

		$validator1 = Validator::make($input1, $rules1);

		if(($validator->fails()) && ($validator1->fails())) {

			echo '1';

		}

		elseif($validator->fails()) {

			echo '2';

		}elseif($validator1->fails()) {

			echo '3';

		}

		else {

			echo '4';

		}

	}

	public function user_delete($id)

	{

		DB::table('order')->where('user_id', '=', $id)->delete();

		DB::table('order_payment')->where('pay_userid', '=', $id)->delete();

		DB::table('device_token')->where('userid', '=', $id)->delete();

		DB::table('users')->where('id', '=', $id)->delete();

		Session::flash('message', 'User Information Deleted Sucessfully!');

		return Redirect('/sub-admin/userlist');

	}

}

