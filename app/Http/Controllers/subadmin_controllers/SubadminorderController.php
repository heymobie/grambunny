<?php

namespace App\Http\Controllers\subadmin_controllers;

Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use Illuminate\Http\Request;

use App\Order_history;

use App\Order_pay_history;

use App\Notification_list;

use Mail;

class SubadminorderController extends Controller

{

	public function __construct(){

		$check = session()->get('user_id');

		if(count($check)!=1){

			Redirect::to('/sub-admin')->send();

		}

		//$this->middleware('admin');

	}

	public function show_orderlist()

	{

		$sub_admin_id = Auth::guard('subadmin')->user()->id;

		$city = Auth::guard('subadmin')->user()->city;



		DB::connection()->enableQueryLog();

	 	/*

		DB::connection()->enableQueryLog();

			$cuisine_list = DB::table('cuisine')

			 ->select('*')

			->orderBy('cuisine_id', 'desc')

			->get();

        $data_onview = array('cuisine_list' =>$cuisine_list);

		*/

        $user_id = 0;

        if(Route::current()->getParameter('userid'))

        {

        	$user_id = Route::current()->getParameter('userid');

        }

      $order_detail  =  DB::table('order') ;

      $order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

      $order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

      $order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');

		//$order_detail  = $order_detail->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id');

        if($user_id>0)

        {

        	$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

        }

        $order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

        $order_detail  =  $order_detail->where('restaurant.rest_city', '=', $city);



        $order_detail  = $order_detail->select('order.*', 'restaurant.rest_name', 'users.name', 'users.lname');

        $order_detail  = $order_detail->select('*');

        $order_detail  = $order_detail->orderBy('order.order_create', 'desc');

        $order_detail  = $order_detail->orderBy('order.order_id', 'desc');

        $order_detail  = $order_detail->get();

		/*dd(DB::getQueryLog());

		exit;*/

		$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

  		//echo '<pre>'.print_r($order_detail).'</pre>';	die;

		return View('subadmin.order_list')->with($data_onview);

	}

	public function show_orderdetail()

	{

		//echo 'test';

		$order_id = $_REQUEST['order'];

		$order_detail  =  DB::table('order')

		->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

		->leftJoin('users', 'order.user_id', '=', 'users.id')

		->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid')

		->where('order.order_id', '=', $order_id)

		->select('order.*','restaurant.rest_name', 'restaurant.rest_address', 'restaurant.rest_address2', 'restaurant.rest_suburb', 'restaurant.rest_state', 'restaurant.rest_zip_code', 'restaurant.rest_contact','order_payment.pay_tx','order_payment.pay_status')

		->orderBy('order.order_create', 'desc')

		->get();

		$order_history =  DB::table('order_history')

		->where('order_history.order_id', '=', $order_id)

		->where('order_history.old_status', '=', '1')

		->select('*')

		->get();

		if(!empty($order_history))

		{

			$order_create_date	= $order_history[0]->old_status_date;

		}

		else

		{

			$order_create_date	= $order_detail[0]->created_at;

		}

		//$data_onview = array('order_detail'=>$order_detail,'order_create_date'=>$order_create_date);

		DB::connection()->enableQueryLog();

		$user_list = DB::table('deliveryman')

		->select('*')

		->orderBy('id', 'desc')

		->get();

		$dm_status = DB::table('delivery_man_orders')

		->where('delivery_man_orders.order_id', '=', $order_id)

		->select('*')

		->get();

		if(empty($dm_status)){

			$data_onview = array('order_detail'=>$order_detail,'order_create_date'=>$order_create_date,'user_list'=>$user_list);

		}else{

			$data_onview = array('order_detail'=>$order_detail,'order_create_date'=>$order_create_date,'user_list'=>$user_list,'user_status'=>$dm_status);

		}

		return View('subadmin.order_view')->with($data_onview);

	}

	function send_notification_ios($token,$title,$body,$new_order_status,$order_id,$rest_name)

	{

		$url = "https://fcm.googleapis.com/fcm/send";

		$serverKey = 'AAAAe-XOOH4:APA91bE5zMgevG_2iuYP-eWtBicDn5xPh849N4c-94DYyWHX7R8bkrd8EJ4sWdL4e1HayclrttF-RoII9eL3M0Y30dvKtAh726t2U6PwKEAHFR79WbcJvRLKvROXfciLU1p5cW2HxtGZ';

		//$serverKey = 'AIzaSyBVAHr3vvKUb8-gtvDSvfLv_-1NWITPRpY';

		//$title = "Title";

		//$body = "Body of the message";

		$notification = array('title' =>$title , 'text' => $body, "order_status"=>$new_order_status, "order_id"=>$order_id, "rest_name"=>$rest_name, 'sound' => 'default', 'badge' => '1');

		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

		$json = json_encode($arrayToSend);

		$headers = array();

		$headers[] = 'Content-Type: application/json';

		$headers[] = 'Authorization: key='. $serverKey;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");

		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

		$response = curl_exec($ch);

		if ($response === FALSE) {

			return false;

			//die('FCM Send Error: ' . curl_error($ch));

		}

		else

		{

			return true;

		}

		curl_close($ch);

	}

	function sendNotification($target,$user_id, $title, $description, $new_order_status,$order_id,$rest_name){

		//FCM API end-point

		$url = 'https://fcm.googleapis.com/fcm/send';

		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key

		//$server_key = "AIzaSyC-7C512j6ohsAnFyQYptXZqOsHHYwRPgg";

		//$server_key = "AIzaSyAbdVVVxM3sc_-bypK0HYxC267dkwHvfLY";

		//$server_key = "AIzaSyCtLj31Tvspyi91oI-5OsIjnfhYXdLgkUw";

		$server_key = "AAAAe-XOOH4:APA91bE5zMgevG_2iuYP-eWtBicDn5xPh849N4c-94DYyWHX7R8bkrd8EJ4sWdL4e1HayclrttF-RoII9eL3M0Y30dvKtAh726t2U6PwKEAHFR79WbcJvRLKvROXfciLU1p5cW2HxtGZ";

		//$data = $param;"notification_id": "1",

		$data =  array("self_id"=>$user_id, "title"=>$title, "message"=>$description, "order_status"=>$new_order_status, "order_id"=>$order_id, "rest_name"=>$rest_name);

		$fields = array();

		$fields['data'] = $data;

		if(is_array($target)){

			$fields['registration_ids'] = $target;

		}else{

			$fields['to'] = $target;

		}

		//header with content_type api key

		$headers = array(

			'Content-Type:application/json',

			'Authorization:key='.$server_key

		);

		//CURL request to route notification to FCM connection server (provided by Google)

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);

		//echo $result;die;

		if ($result === FALSE) {

			return false;

		}

		curl_close($ch);

		$jsn =json_decode($result);

		if($jsn->success){

			return true;

		}

		else{

			return true;

		}

	}

	public function assign_order_action()

	{

		$status = 0;

		$arstatus = 0;

		$dm_id = trim(Input::get('dm_id'));

		$orderid = trim(Input::get('aorder_id'));

		$verification  = DB::table('order')->where('order_id', '=' ,$orderid)->get();

		$vercode = $verification[0]->verification_code;

		$userData = array(

			'dm_id' => $dm_id,

			'order_id' => $orderid,

			'order_status' => $status,

			'verification_code' => $vercode,

			'accept_reject_status' => $arstatus,

			'received_order_time'=>date('y-m-d h:i:s'),

			'pickup_order_time'=>'0',

			'deliver_order_time'=>'0'

		);

		$dm_status = DB::table('delivery_man_orders')

		->where('delivery_man_orders.order_id', '=', $orderid)

		->select('*')

		->get();

		if(empty($dm_status)){

			$affected = DB::table('delivery_man_orders')->insert($userData);

			$uorder = DB::table('order')->where('order_id',$orderid)->update(['order_status' => 7]);

		}else{

			$affected = DB::table('delivery_man_orders')->where('order_id',$orderid)->update(['dm_id' =>$dm_id,'order_status' => $status,'accept_reject_status' => $arstatus]);

		}

		return $status;

	}

	public function update_order_action()

	{

		$order_id = Input::get('order_id');

		$old_status = Input::get('old_status');

		$new_order_status = Input::get('order_status');

		$user_name ='';

		$email_content = '';

		$rest_name = '';

		DB::enableQueryLog();

		$order_cancel_reason = '';

		if(Input::get('order_cancel_reason'))

		{

			$order_cancel_reason = Input::get('order_cancel_reason');

		}

		$order_detail  =  DB::table('order')

		->where('order_id', '=', $order_id)

		->select('*')

		->orderBy('order.order_create', 'desc')

		->get();

		$rest_detail = DB::table('restaurant')

		->select('*')

		->where('rest_id', '=' ,$order_detail[0]->rest_id)

		->get();

		$user_name = $order_detail[0]->order_fname.' '.$order_detail[0]->order_lname;

		$user_email = $order_detail[0]->order_email;

		$user_contact = $order_detail[0]->order_tel;

		$rest_name = $rest_detail[0]->rest_name;

		/*************************** SEND NOTIFCATON TO USER ACCORDING TO ORDER STAUS ********************/

			if($new_order_status==2)  //cancel

			{

				$email_subject = 'Your order is cancel.';

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'5')

				->get();

				$emailcontent =$email_content_detail[0]->email_content;

				$searchArray = array("order_id", "order_uniqueid", "f_name");

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			if($new_order_status==4) //Confirmed

			{

				$email_subject = 'Your order is confirmed.';

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'6')

				->get();

				$emailcontent =$email_content_detail[0]->email_content;

				$searchArray = array("order_id", "order_uniqueid", "f_name");

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			if($new_order_status==5) //Completed

			{

				$email_subject = 'Your order is completed.';

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'4')

				->get();

				$emailcontent =$email_content_detail[0]->email_content;

				$searchArray = array("order_id", "order_uniqueid", "f_name");

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			if($new_order_status==6) //Reject

			{

				$email_subject = 'Your order is reject.';

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'7')

				->get();

				$emailcontent =$email_content_detail[0]->email_content;

				$searchArray = array("order_id", "order_uniqueid", "f_name");

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			$data = array(

				'user_name'=>$user_name,

				'user_email'=>$user_email,

				'email_content'=>$email_content,

				'user_contact'=>$user_contact	,

				'email_subject'=>$email_subject,

				'order_detail'=>$order_detail,

				'rest_detail'=>$rest_detail

			);

			if(!empty($user_email))

			{

				//echo 'test';

				echo Mail::send('emails.orderstatus_email', $data, function ($message) use ($data) {

					$message->from('info@grambunny.com','grambunny');

					$message->to($data['user_email']);

					$message->bcc('votiveshweta@gmail.com');

					$message->bcc('votivemobile.pankaj@gmail.com');

					$message->subject($data['email_subject']);

				});

			}

			if(($order_detail[0]->user_id>0) || (!empty($order_detail[0]->order_guestid)))

			{

				//$user_detail  =  DB::table('users')->where('id', '=', $order_detail[0]->user_id)->get();

				$notification_title =$email_subject;

				$notification_description = $email_content;

						//$token_data = DB::table('device_token')->where('userid', '=', trim($order_detail[0]->user_id))->get();

				if($order_detail[0]->user_id>0)	{

					$token_data = DB::table('device_token')->where('userid', '=', trim($order_detail[0]->user_id))->where('devicetype', '=', trim($order_detail[0]->order_device))->get();

				}

				elseif(!empty($order_detail[0]->order_guestid))

				{

					$token_data = DB::table('device_token')->where('guest_id', '=', trim($order_detail[0]->order_guestid))->where('devicetype', '=', trim($order_detail[0]->order_device))->get();

				}

				if($token_data)

				{

					$user_id = $order_detail[0]->user_id;

					$target = $token_data[0]->devicetoken;

					$device_type = $token_data[0]->devicetype;

					$device_id = $token_data[0]->deviceid;

					$guest_id = $order_detail[0]->order_guestid;

								//$this->sendNotification($target,$user_id, $notification_title, $notification_description,$device_type);

					if($device_type=='android')

					{

						$this->sendNotification($target,$user_id, $notification_title, $notification_description,$new_order_status,$order_id,$rest_name);

					}

					elseif($device_type=='iphone')

					{

						$this->send_notification_ios($target,$user_id, $notification_title, $notification_description,$new_order_status,$order_id,$rest_name);

					}

					$notifiy = new Notification_list;

					$notifiy->noti_userid = $user_id;

					$notifiy->noti_guestid = $guest_id;

					$notifiy->noti_title = $notification_title;

					$notifiy->noti_desc = $notification_description;

					$notifiy->noti_deviceid = $device_id;

					$notifiy->noti_devicetype = $device_type;

					$notifiy->noti_read = '0';

					$notifiy->save();

					$noti_id = $notifiy->noti_id;

				}

			}

			/**********************************   END  *****************************/

			/* SAVE HISTORY OF OLD ORDER */

			$ohis = new Order_history;

			$ohis->order_id = $order_detail[0]->order_id;

			$ohis->old_status =$order_detail[0]->order_status;

			$ohis->old_status_date = $order_detail[0]->updated_at;

			$ohis->old_status_date = $order_detail[0]->updated_at;

			$ohis->save();

			/* UPDATE PAYMENT TABLE BASED ON ORDER CANCEL OR REJECT OR COMPLETED BY PARTNER START  */

			if($new_order_status == '2' || $new_order_status == '6')

			{

				$payment_detail  =  DB::table('order_payment')

				->where('pay_orderid', '=', $order_id)

				->select('*')

				->get();

				$pay_his = new Order_pay_history;

				$pay_his->pay_id = $payment_detail[0]->pay_id;

				$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;

				$pay_his->pay_user_id = $payment_detail[0]->pay_userid;

				$pay_his->pay_old_status = $payment_detail[0]->pay_status;

				$pay_his->old_status_date = $payment_detail[0]->created_at;

				$pay_his->order_history_created	= date('Y-m-d');

				$pay_his->save();

				DB::table('order_payment')->where('pay_orderid',$order_id)

				->update(['pay_status' =>'2' ]

			);

				//print_r(DB::getQueryLog());

			}

			if($new_order_status == '5')

			{

				$payment_detail  =  DB::table('order_payment')

				->where('pay_orderid', '=', $order_id)

				->select('*')

				->get();

				$pay_his = new Order_pay_history;

				$pay_his->pay_id = $payment_detail[0]->pay_id;

				$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;

				$pay_his->pay_user_id = $payment_detail[0]->pay_userid;

				$pay_his->pay_old_status = $payment_detail[0]->pay_status;

				$pay_his->old_status_date = $payment_detail[0]->created_at;

				$pay_his->order_history_created	= date('Y-m-d');

				$pay_his->save();

				DB::table('order_payment')->where('pay_orderid',$order_id)

				->update(['pay_status' =>'3' ]

			);

			//	print_r(DB::getQueryLog());

			}

			/*                            END                              */

			/* UPDATE STATUS IN TABLE START */

			DB::table('order')->where('order_id',$order_id)

			->update(['order_status' =>$new_order_status,

				'order_cancel_reason' =>$order_cancel_reason ]

			);

			//	print_r(DB::getQueryLog());

			echo 'Status Updted!';

		}

		public function ajax_search_list()

		{



			$sub_admin_id = Auth::guard('subadmin')->user()->id;

		    $city = Auth::guard('subadmin')->user()->city;



			$from_date=Input::get('fromdate');;

			$to_date=Input::get('todate');;

			$status = Input::get('order_status');

			$user_id = Input::get('user_id');

			if($status!=11 && $status!=12 && $status!=13 && $status!=14 && $status!=15 && $status!=16 ){

				$order_detail  =  DB::table('order');

				$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

				$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

				/*USER BASE SEARC FORM USER PAGE  */

				if($user_id>0)

				{

					$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

				}

				/* Status Search */

				if(!empty($status) && ($status>0))

				{

					$order_detail = $order_detail->where('order.order_status', '=', $status);

				}

				/* Get Data between Date */

				if(!empty($from_date) && ($from_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

				}

				if(!empty($to_date) && ($to_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

				}

				$order_detail  =  $order_detail->where('restaurant.rest_city', '=', $city);

				$order_detail = $order_detail->select('*');

				$order_detail = $order_detail->orderBy('order.order_create', 'desc');

				$order_detail = $order_detail->get();

				$data_onview = array('order_detail'=>$order_detail,'orderstatus'=>$status);

				return View('subadmin.ajax.order_list')->with($data_onview);

			}else{

				$order_detail  =  DB::table('order');

				$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

				$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

				/*USER BASE SEARC FORM USER PAGE  */

				if($user_id>0)

				{

					$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

				}

				$order_detail  =  $order_detail->where('restaurant.rest_city', '=', $city);

				$order_detail = $order_detail->select('*');

				$order_detail = $order_detail->orderBy('order.order_create', 'desc');

				$order_detail = $order_detail->get();

				$data_onview = array('order_detail'=>$order_detail,'orderstatus'=>$status);

				return View('subadmin.ajax.order_list')->with($data_onview);

			}

		}

		public function show_orderreport()

		{

		//echo '<pre>';

		//print_r($_POST);

			$from_date=Input::get('fromdate');;

			$to_date=Input::get('todate');;

			$status = Input::get('order_status');

			$user_id = Input::get('user_id');

			$order_detail  =  DB::table('order');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

			/*USER BASE SEARC FORM USER PAGE  */

			if($user_id>0)

			{

				$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

			}

			/* Status Search */

			if(!empty($status) && ($status>0))

			{

				$order_detail = $order_detail->where('order.order_status', '=', $status);

			}

			/* Get Data between Date */

			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

				$order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

			}

			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

				$order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

			}

			/* End */

			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();

  		//$data_onview = array('order_detail'=>$order_detail);

			$title = 'Order Report';

			$all_array = array();

			if(!empty($order_detail))

			{

				$sr=0;

				$total_amt = '';

				foreach($order_detail as $data_list)

					{	$sr++;

						$status = '';

						if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

						if($data_list->order_status=='2'){ $status = 'Cancelled'; }

						if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

						if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

						if($data_list->order_status=='5'){ $status = 'Partner Completed'; }

						if($data_list->order_status=='6'){ $status = 'Reject'; }

						if($data_list->order_status=='7'){ $status = 'Review Completed'; }

						$cart_price = 0;

						$sumary = '';

						if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

						{

							$order_carditem = json_decode($data_list->order_carditem, true);

							foreach($order_carditem as $item){

								$cart_price = $cart_price+$item['price'];

								$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";

								if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

								{

									foreach($item['options']['addon_data'] as $addon)

									{

										$cart_price = $cart_price+$addon['price'];

										$sumary .=$addon['name'].' ';

										if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

										$sumary .="\n";

									}

								}

							}

						}

						$all_array[] = array('0'=>$sr,

							'1'=>$data_list->order_id,

							'2'=>$data_list->order_create,

							'3'=>$data_list->rest_name,

							'4'=>$status,

							'5'=>$data_list->order_fname.' '.$data_list->order_lname,

							'6'=>$data_list->order_tel,

							'7'=>$sumary,

							'8'=>'$'.$cart_price,

						);

					}

					$data = $all_array ;

				}

				else

				{

					$data = array(

						array('No REcod Found')

					);

				}

				$filename = "order_report.csv";

				$fp = fopen('php://output', 'w');

		//$header[] ='';

				$header =array('',	$title, '', '');

				header('Content-type: application/csv');

				header('Content-Disposition: attachment; filename='.$filename);

				fputcsv($fp, $header);

				$header2 =array('SNo.', 'OrderNo', 'OrderReq_date', 'Restaurant','Status','UserName','Contact No', 'Order Sumary','TotalAmount');

				fputcsv($fp, $header2);

				foreach ($data as $row) {

					fputcsv($fp, $row);

				}

			}

			public function show_paymentlist()

			{

				$payment_detail = '';

				$payment_detail  =  DB::table('order_payment')

				->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')

				->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

				->leftJoin('users', 'order.user_id', '=', 'users.id')

				->where('order_payment.pay_status', '>', 0)

				->select('*')

				->orderBy('order_payment.pay_id', 'desc')

				->get();

				//echo '<pre>'		;

	//print_r($payment_detail );

	//exit;

				$data_onview = array('payment_detail'=>$payment_detail);

				return View('subadmin.payment_list')->with($data_onview);

			}

			public function show_paymentdetail()

			{

				$pay_id = $_REQUEST['order'];

				$payment_detail = '';

				$payment_detail  =  DB::table('order_payment')

				->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')

				->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

				->leftJoin('users', 'order.user_id', '=', 'users.id')

				->where('order_payment.pay_status', '>', 0)

				->where('order_payment.pay_id', '=', $pay_id)

				->select('*','order.created_at as order_submit_on')

				->orderBy('order_payment.pay_id', 'desc')

				->get();

				//echo '<pre>'		;

	//print_r($payment_detail );

	//exit;

				if(count($payment_detail>0)){

					$order_create_date = $payment_detail[0]->order_submit_on;

					$order_history_submit =  DB::table('order_history')

					->where('order_history.order_id', '=', $payment_detail[0]->order_id)

					->where('order_history.old_status', '=', '1')

					->select('*')

					->get();

					if(!empty($order_history_submit))

					{

						$order_create_date	= $order_history_submit[0]->old_status_date;

					}

					/* ORDER COMPLETED DATE */

					$order_can_comp_reg = $payment_detail[0]->order_submit_on;

					$order_history_complete =  DB::table('order_history')

					->where('order_history.order_id', '=', $payment_detail[0]->order_id)

					->where('order_history.old_status', '=', '5')

					->select('*')

					->get();

					if(!empty($order_history_complete))

					{

						$order_can_comp_reg	= $order_history_complete[0]->old_status_date;

					}

					/* END */

					$order_history_detail  =  DB::table('order_history')

					->where('order_history.order_id', '=', $payment_detail[0]->order_id)

					->where('order_history.old_status', '=', '5')

					->select('*')

					->orderBy('order_history.order_history_id', 'desc')

					->get();

					$data_onview = array('order_detail'=>$payment_detail,'order_history_detail'=>$order_history_detail,'order_create_date'=>$order_create_date,'order_can_comp_reg'=>$order_can_comp_reg);

		//return View('admin.payment_list')->with($data_onview);

					return View('subadmin.payment_view')->with($data_onview);

				}

				else

				{

				}

			}

			public function ajax_show_paymentlist()

			{

		//echo '<pre>';

		//print_r($_POST);

				$from_date=Input::get('fromdate');;

				$to_date=Input::get('todate');;

				$status = Input::get('order_status');

				$user_id = Input::get('user_id');

				$order_detail  =  DB::table('order_payment')	;

				$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');

				$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

				$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

				$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);

				/* Status Search */

				if(!empty($status) && ($status>0))

				{

					if($status==7)

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');

					}

					elseif($status==6)

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');

						$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '5');

					}

					else

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);

					}

				}

				/* Get Data between Date */

				if(!empty($from_date) && ($from_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

				}

				if(!empty($to_date) && ($to_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

				}

				/* End */

				$order_detail = $order_detail->select('*');

				$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');

				$order_detail = $order_detail->get();

				$data_onview = array('order_detail'=>$order_detail);

				return View('subadmin.ajax.payment_list')->with($data_onview);

		//print_r($data_onview);

			}

			public function show_paytmentreport()

			{

				$from_date=Input::get('fromdate');;

				$to_date=Input::get('todate');;

				$status = Input::get('order_status');

				$user_id = Input::get('user_id');

				$order_detail  =  DB::table('order_payment')	;

				$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');

				$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

				$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

				$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);

				/* Status Search */

				if(!empty($status) && ($status>0))

				{

					if($status==4)

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');

						$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '3');

					}

					elseif($status==5)

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');

					}

					else

					{

						$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);

					}

				}

				/* Get Data between Date */

				if(!empty($from_date) && ($from_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

				}

				if(!empty($to_date) && ($to_date!='0000-00-00'))

				{

					$order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

				}

				/* End */

				$order_detail = $order_detail->select('*');

				$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');

				$order_detail = $order_detail->get();

  		//$data_onview = array('order_detail'=>$order_detail);

				$title = 'Payment Report';

				$all_array = array();

				if(!empty($order_detail))

				{

					$sr=0;

					$total_amt = '';

					foreach($order_detail as $data_list)

						{	$sr++;

							$payment_status = '';

							$order_can_comp_reg_date	= '';

							if($data_list->pay_status=='1') { $payment_status = 'Complete'; }

							if($data_list->pay_status=='2') { $payment_status = 'ToRefund'; }

							if($data_list->pay_status=='3') { $payment_status = 'ToPayout'; }

							if($data_list->pay_status=='4') { $payment_status = 'Completed Refund'; }

							if($data_list->pay_status=='5') { $payment_status = 'Completed Payout'; }

							$status = '';

							if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

							if($data_list->order_status=='2'){ $status = 'Cancelled'; $order_can_comp_reg_date	= $data_list->created_at; }

							if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

							if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

							if($data_list->order_status=='5'){ $status = 'Partner Completed'; $order_can_comp_reg_date	= $data_list->created_at;}

							if($data_list->order_status=='6'){ $status = 'Reject'; $order_can_comp_reg_date	= $data_list->created_at; }

							if($data_list->order_status=='7'){ $status = 'Review Completed'; }

							$cart_price = 0;

							$sumary = '';

							/* ORDER SUMMARY AND CALCULATE TOTAL AMOUNT WITH COMMISSION AMOUNT */

							if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

							{

								$order_carditem = json_decode($data_list->order_carditem, true);

								foreach($order_carditem as $item){

									$cart_price = $cart_price+$item['price'];

									$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";

									if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

									{

										foreach($item['options']['addon_data'] as $addon)

										{

											$cart_price = $cart_price+$addon['price'];

											$sumary .=$addon['name'].' ';

											if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

											$sumary .="\n";

										}

									}

								}

							}

							$sumary .="\n  Sub Total : $".number_format($cart_price,2);

							$sumary .="\n";

							$sumary .="\n  Delivery Fee : $".number_format($data_list->order_deliveryfee,2);

							if($data_list->order_remaning_delivery>0)

							{

								$sumary .="\n";

								$sumary .="\n $".number_format($data_list->order_min_delivery,2).' min Delivery amount';

								$sumary .=" Remaining Amount :$".number_format($data_list->order_remaning_delivery,2).'';

								$sumary .="\n";

							}

							if($order_detail[0]->order_promo_cal>0)

							{

								$sumary .=" Dels : -$".number_format($data_list->order_promo_cal,2).'';

							}

							$promo_amt_cal= '0.00';

							$total_amt= '';

							if($data_list->order_promo_cal>0)

							{

								$promo_amt_cal= $data_list->order_promo_cal;

							}

							$total_amt= $cart_price+$data_list->order_deliveryfee-$promo_amt_cal;

							if(($data_list->order_remaning_delivery>0) &&

								($data_list->order_min_delivery>0) &&

								(($cart_price-$data_list->order_promo_cal)<$data_list->order_min_delivery)

							)

							{

								$total_amt= ($data_list->order_min_delivery+$data_list->order_deliveryfee);

							}

							$Commission_amt = '0.00';

							if($data_list->pay_status=='3' || $data_list->pay_status=='5' )

							{

								$Commission_amt =	number_format((($total_amt*$data_list->rest_commission)/100),2);

							}

							/*  END   */

							/*  ORDER PICKUP DATE AND TIME */

							$order_picup_date = '';

							if(($data_list->order_typetime=='later') || ($data_list->order_pickdate!='0000-00-00'))

							{

								$order_picup_date = $data_list->order_pickdate.' '.substr($data_list->order_picktime, 0, 2).':'.substr($data_list->order_picktime, 2, 3);

							}

							else

							{

								$order_history =  DB::table('order_history')

								->where('order_history.order_id', '=', $data_list->order_id)

								->where('order_history.old_status', '=', '1')

								->select('*')

								->get();

								if(!empty($order_history))

								{

									$order_picup_date	= $order_history[0]->old_status_date;

								}

							}

							/*    END   */

							/* ORDER COMPLETE /REJECT /CANCEL DATE AND TIME */

							$order_history_comp =  DB::table('order_history')

							->where('order_history.order_id', '=', $data_list->order_id)

							->where('order_history.old_status', '=', '5')

							->select('*')

							->get();

							if(!empty($order_history_comp))

							{

								$order_can_comp_reg_date	= $order_history_comp[0]->old_status_date;

							}

							/*        END        */

							$all_array[] = array('0'=>$sr,

								'1'=>$data_list->order_id,

								'2'=>$data_list->order_create,

								'3'=>$data_list->rest_name,

								'4'=>$status,

								'5'=>$data_list->order_fname.' '.$data_list->order_lname,

								'6'=>$data_list->order_tel,

								'7'=>$sumary,

								'8'=>'$'.$total_amt,

								'9'=>$payment_status,

								'10'=>$order_picup_date,

								'11'=>$order_can_comp_reg_date,

								'12'=>'$'.$total_amt,

								'13'=>$data_list->rest_commission,

								'14'=>$Commission_amt,

								'15'=>$data_list->pay_tx

							);

						}

						$data = $all_array ;

					}

					else

					{

						$data = array(

							array('No REcod Found')

						);

					}

					$filename = "payment_report.csv";

					$fp = fopen('php://output', 'w');

		//$header[] ='';

					$header =array('',	$title, '', '');

					header('Content-type: application/csv');

					header('Content-Disposition: attachment; filename='.$filename);

					fputcsv($fp, $header);

					$header2 =array('SNo.',

						'OrderNo',

						'OrderReq_date',

						'Restaurant',

						'Status',

						'UserName',

						'Contact No',

						'Order Sumary',

						'TotalAmount',

						'Payment Status',

						'Order DineIn/Pickup/Delivery Date',

						'OrderComplete /Cancle /Rejcte Date',

						'Order Payout/Refund Amount',

						'Commission',

						'Commission Amount',

						'Paypal Ref'

					);

					fputcsv($fp, $header2);

					foreach ($data as $row) {

						fputcsv($fp, $row);

					}

				}

				public function update_payment_action()

				{

		//print_r($_POST);

		//pay_outgoing_Ref

					$pay_id = Input::get('pay_id');

					$order_id = Input::get('order_id');

					$old_status = Input::get('old_status');

					$new_pay_status = Input::get('pay_status');

					$outgoing_reference = Input::get('outgoing_transaction_reference');

		//DB::enableQueryLog();

					$payment_detail  =  DB::table('order_payment')

					->where('pay_orderid', '=', $order_id)

					->where('pay_id', '=', $pay_id)

					->select('*')

					->get();

					$pay_his = new Order_pay_history;

					$pay_his->pay_id = $payment_detail[0]->pay_id;

					$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;

					$pay_his->pay_user_id = $payment_detail[0]->pay_userid;

					$pay_his->pay_old_status = $payment_detail[0]->pay_status;

					$pay_his->old_status_date = $payment_detail[0]->created_at;

					$pay_his->order_history_created	= date('Y-m-d');

					$pay_his->save();

					/* UPDATE STATUS IN TABLE START */

					DB::table('order_payment')->where('pay_id',$pay_id)

					->update(['pay_status' =>$new_pay_status,

						'pay_outgoing_Ref' =>$outgoing_reference ]

					);

			//	print_r(DB::getQueryLog());

					echo 'Status Updted!';

				}

			}

