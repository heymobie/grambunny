<?php
namespace App\Http\Controllers\subadmin_controllers;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Route;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Restaurant;
use App\Menu;
use App\Menu_category;
use App\Menu_category_item;
use App\Menu_category_addon;
use App\Service;
use App\Bankdetail;
use App\Promotion;
use App\Category_item;
use App\Rest_time;
class SubadminresturantController extends Controller
{
	public function __construct(){
		//$this->middleware('admin');
		$check = session()->get('user_id');
		if(count($check)!=1){
			Redirect::to('/sub-admin')->send();
		}
	}
	public function restaurant_list()
	{

		$sub_admin_id = Auth::guard('subadmin')->user()->id;
		$city = Auth::guard('subadmin')->user()->city;

		DB::connection()->enableQueryLog();
		$rest_list = DB::table('restaurant')
		->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')
		->select('restaurant.*','vendor.name')
		->where('restaurant.rest_city', $city)
		->orderBy('restaurant.google_type', 'asc')
		->orderBy('restaurant.rest_id', 'desc')
		->get();
		$data_onview = array('rest_list' =>$rest_list);
		return View('subadmin.restaurant_list')->with($data_onview);
	}
	public function restaurant_search()
	{
		$rest_list = DB::table('restaurant');
		$rest_list = $rest_list->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id');
		if( (Input::get('rest_cont')) && (!empty(Input::get('rest_cont'))))
		{
			$rest_list = $rest_list->where('restaurant.rest_contact', 'like' ,Input::get('rest_cont'));
			$rest_list = $rest_list->orwhere('restaurant.rest_landline', 'like' ,Input::get('rest_cont'));
		}
		if( (Input::get('rest_name')) && (!empty(Input::get('rest_name'))))
		{
			$rest_list = $rest_list->where('restaurant.rest_name', 'like' ,'%'.Input::get('rest_name').'%');
		}
		$rest_list = $rest_list->select('restaurant.*','vendor.name');
		$rest_list = $rest_list->orderBy('restaurant.rest_id', 'desc');
		$rest_list = $rest_list->get();
		$data_onview = array('rest_list' =>$rest_list);
		return View('subadmin.ajax.restaurant_list')->with($data_onview);
	}
	public function restaurant_form()
	{

		$mon_from='';
		$mon_to='';
		$tues_from='';
		$tues_to='';
		$wed_from='';
		$wed_to='';
		$thu_from='';
		$thu_to='';
		$fri_from='';
		$fri_to='';
		$sat_from='';
		$sat_to='';
		$sun_from='';
		$sun_to='';
		$mon_del_from='';
		$mon_del_to='';
		$tue_del_from='';
		$tue_del_to='';
		$wed_del_from='';
		$wed_del_to='';
		$thu_del_from='';
		$thu_del_to='';
		$fri_del_from='';
		$fri_del_to='';
		$sat_del_from='';
		$sat_del_to='';
		$sun_del_from='';
		$sun_del_to='';
		if(Route::current()->getParameter('id'))
		{
			$id = Route::current()->getParameter('id');
			$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$id)->get();
			if(($id>0) && (!empty($rest_detail[0]->rest_mon)))
			{
				$v = explode('_',$rest_detail[0]->rest_mon);
				$mon_from=$v[0];
				$mon_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_tues))){
				$v = explode('_',$rest_detail[0]->rest_tues);
				$tues_from=$v[0];
				$tues_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_wed))){
				$v = explode('_',$rest_detail[0]->rest_wed);
				$wed_from=$v[0];
				$wed_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_thus))){
				$v = explode('_',$rest_detail[0]->rest_thus);
				$thu_from=$v[0];
				$thu_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_fri))){
				$v = explode('_',$rest_detail[0]->rest_fri);
				$fri_from=$v[0];
				$fri_to=$v[1];
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_sat))){
				$v = explode('_',$rest_detail[0]->rest_sat);
				$sat_from=$v[0];
				$sat_to=$v[1];
			}
			if(($id>0) && (!empty($rest_detail[0]->rest_sun))){
				$v = explode('_',$rest_detail[0]->rest_sun);
				$sun_from=$v[0];
				$sun_to=$v[1];
			}
			if(($id>0) && (!empty($rest_detail[0]->mon_del))){
				$v = explode('_',$rest_detail[0]->mon_del);
				$mon_del_from=$v[0];
				$mon_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->tue_del))){
				$v = explode('_',$rest_detail[0]->tue_del);
				$tue_del_from=$v[0];
				$tue_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->wed_del))){
				$v = explode('_',$rest_detail[0]->wed_del);
				$wed_del_from=$v[0];
				$wed_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->thu_del))){
				$v = explode('_',$rest_detail[0]->thu_del);
				$thu_del_from=$v[0];
				$thu_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->fri_del))){
				$v = explode('_',$rest_detail[0]->fri_del);
				$fri_del_from=$v[0];
				$fri_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->sat_del))){
				$v = explode('_',$rest_detail[0]->sat_del);
				$sat_del_from=$v[0];
				$sat_del_to=$v[1];;
			}
			if(($id>0) && (!empty($rest_detail[0]->sun_del))){
				$v = explode('_',$rest_detail[0]->sun_del);
				$sun_del_from=$v[0];
				$sun_del_to=$v[1];;
			}
			$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();
			$data_onview = array('rest_detail' =>$rest_detail,
				'mon_from' =>$mon_from,
				'mon_to' =>$mon_to,
				'tues_from' =>$tues_from,
				'tues_to' =>$tues_to,
				'wed_from' =>$wed_from,
				'wed_to' =>$wed_to,
				'thu_from' =>$thu_from,
				'thu_to' =>$thu_to,
				'fri_from' =>$fri_from,
				'fri_to' =>$fri_to,
				'sat_from' =>$sat_from,
				'sat_to' =>$sat_to,
				'sun_from' =>$sun_from,
				'sun_to' =>$sun_to,
				'cuisine_list'=>$cuisine_list,
				'id'=>$id,
				'sun_to_del'=>$sun_del_to,
				'mon_to_del'=>$mon_del_to,
				'tue_to_del'=>$tue_del_to,
				'wed_to_del'=>$wed_del_to,
				'thu_to_del'=>$thu_del_to,
				'fri_to_del'=>$fri_del_to,
				'sat_to_del'=>$sat_del_to,
				'mon_from_del'=>$mon_del_from,
				'tue_from_del'=>$tue_del_from,
				'wed_from_del'=>$wed_del_from,
				'thu_from_del'=>$thu_del_from,
				'fri_from_del'=>$fri_del_from,
				'sat_from_del'=>$sat_del_from,
				'sun_from_del'=>$sun_del_from
			);
		}
		else
		{
			$vendor_id =  Route::current()->getParameter('vendor_id');
			$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();
			$id =0;
			$data_onview = array('vendor_id' =>$vendor_id,
				'mon_from' =>$mon_from,
				'mon_to' =>$mon_to,
				'tues_from' =>$tues_from,
				'tues_to' =>$tues_to,
				'wed_from' =>$wed_from,
				'wed_to' =>$wed_to,
				'thu_from' =>$thu_from,
				'thu_to' =>$thu_to,
				'fri_from' =>$fri_from,
				'fri_to' =>$fri_to,
				'sat_from' =>$sat_from,
				'sat_to' =>$sat_to,
				'sun_from' =>$sun_from,
				'sun_to' =>$sun_to,
				'cuisine_list'=>$cuisine_list,
				'id'=>$id,
				'sun_to_del'=>$sun_del_to,
				'mon_to_del'=>$mon_del_to,
				'tue_to_del'=>$tue_del_to,
				'wed_to_del'=>$wed_del_to,
				'thu_to_del'=>$thu_del_to,
				'fri_to_del'=>$fri_del_to,
				'sat_to_del'=>$sat_del_to,
				'mon_from_del'=>$mon_del_from,
				'tue_from_del'=>$tue_del_from,
				'wed_from_del'=>$wed_del_from,
				'thu_from_del'=>$thu_del_from,
				'fri_from_del'=>$fri_del_from,
				'sat_from_del'=>$sat_del_from,
				'sun_from_del'=>$sun_del_from
			);
		}
		return View('subadmin.restaurant_form')->with($data_onview);
	}
	function clean_string($string){
		$string1 = strtolower($string);
		//Make alphanumeric (removes all other characters)
		$string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);
		//Clean up multiple dashes or whitespaces
		$string1 = preg_replace("/[\s-]+/", " ", $string1);
		//Convert whitespaces and underscore to dash
		$string1 = preg_replace("/[\s_]/", "-", $string1);
		if( !empty($string1) || $string1 != '' ){
			return $string1;
		}else{
			return $string;
		}
	}
	public function check_day_time($start,$closed)
	{
				/*$s_explode = explode('_',$start_closed);
				$t1 =strtotime($s_explode[0]);
				$t2= strtotime($s_explode[1]);*/
				$t1 =$start;
				$t2=$closed;
				$tc = '';
				if($t1>$t2)
				{
				$tc = '1';//'next_day';
			}
			if($t1<$t2)
			{
				$tc = '0';//'same_day';
			}
			return $tc;
		}
		public function restaurant_action(Request $request)
		{


			$meta = $this->clean_string(trim(Input::get(['rest_name'])));
			$rest_id = Input::get('rest_id');
			$rest_old_logo = Input::get('rest_old_logo');
			$new_fileName = '';
			$rest_old_banner = Input::get('rest_old_banner');
			$new_banner = '';
			/********************     CALCULATE LAT AND LONG ACCORDING TO ADDRESS ************************/
			$latitude='';
			$longitude='';
		/*$address =  Input::get('rest_address').", ".Input::get('rest_suburb').", ".Input::get('rest_state'); // Google HQ
       $prepAddr = str_replace(' ','+',$address);
       $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
       $output= json_decode($geocode);	*/
       /*if(empty($output->results[0])){
	       	$latitude= '40.486155';
			$longitude= '-74.451850';
       }
       else{
	       $latitude = $output->results[0]->geometry->location->lat;
	       $longitude = $output->results[0]->geometry->location->lng;
	   }*/
	   $latitude = Input::get('r_lat');
	   $longitude = Input::get('r_lang');
	       //if(empty($latitude))
	   /********************     CALCULATE LAT AND LONG ACCORDING TO ADDRESS END   ************************/
	   /*****RESTAURANT IMAGE ***/
	   if(isset($_FILES['rest_logo']) && (!empty($_FILES['rest_logo']['name'][0]))){
	   	if(!empty($rest_old_logo)){
	   		$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;
	   		if(file_exists(realpath($file_remove))){
	   			unlink($file_remove);
	   		}
	   	}
	   	$destinationPath = 'uploads/reataurant/';
	   	$image = $request->file('rest_logo');
	   	$extension 		= 	$image->getClientOriginalExtension();
	   	$imageRealPath 	= 	$image->getRealPath();
    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();
	   	if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))
	   	{
	   		$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();
	   		$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/'.$thumbName);;
	   		asset($destinationPath . $thumbName);
	   		$new_fileName = $thumbName;
	   	}
	   	else
	   	{
	   		Session::flash('message_error', 'Image type is invalide!');
	   		if($rest_id>0){
	   			return redirect()->to('/sub-admin/restaurant-form/'.$rest_id);
	   		}
	   		else
	   		{
	   			return redirect()->to('/sub-admin/vendor-rest-form/'.Input::get('vendor_id'));
	   		}
	   	}
	   }
	   elseif(!empty($rest_old_logo))
	   {
	   	$new_fileName = $rest_old_logo;
	   }
 		///****** END **//
 		/////****************************** BANNER IMAGE ***************/
	   if(isset($_FILES['rest_banner']) && (!empty($_FILES['rest_banner']['name'][0]))){
	   	$destinationPath = 'uploads/reataurant/banner/';
	   	$image = $request->file('rest_banner');
	   	$extension 		= 	$image->getClientOriginalExtension();
	   	$imageRealPath 	= 	$image->getRealPath();
	   	if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))
	   	{
	   		$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();
					//$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/'.$thumbName);;
	   		$img = Image::make($imageRealPath)->save('uploads/reataurant/banner/'.$thumbName);;
	   		asset($destinationPath . $thumbName);
	   		$new_banner = $thumbName;
	   	}
	   	else
	   	{
	   		Session::flash('message_error', 'Image type is invalide!');
	   		if($rest_id>0){
	   			return redirect()->to('/sub-admin/restaurant-form/'.$rest_id);
	   		}
	   		else
	   		{
	   			return redirect()->to('/sub-admin/vendor-rest-form/'.Input::get('vendor_id'));
	   		}
	   	}
	   }
	   elseif(!empty($rest_old_logo))
	   {
	   	$new_banner = $rest_old_banner;
	   }
 		/////////////// END ***********************************************/
	   $rest_cuisine_data='';
	   if(Input::get('rest_cuisine')){
	   	$rest_cuisine_data = implode(',',Input::get('rest_cuisine'));
	   }
	   $rest_type_data='';
	   if(Input::get('rest_type')){
	   	$rest_type_data = implode(',',Input::get('rest_type'));
	   }
	   if(Input::get('rest_service')){
	   	$rest_service = implode(',',Input::get('rest_service'));
	   }
	   else
	   {
	   	$rest_service ='';
	   }
	   if(Input::get('close_day')){
	   	$rest_close_day = implode(',',Input::get('close_day'));
	   }
	   else
	   {
	   	$rest_close_day ='';
	   }
	   $today_special='0';
	   if(Input::get('rest_special'))
	   {
	   	$today_special='1';
	   }
	   if($rest_id==0){
	   	$rest = new Restaurant;
	   	$rest->rest_name = Input::get('rest_name');
	   	$rest->vendor_id =Input::get('vendor_id');
	   	$rest->rest_address = Input::get('rest_address');
	   	$rest->rest_city = Input::get('rest_suburb');
	   	$rest->rest_zip_code =  Input::get('rest_zip_code');
	   	$rest->rest_desc =  Input::get('rest_desc');
	   	$rest->rest_logo = $new_fileName;
	   	$rest->rest_banner = $new_banner;
	   	$rest->rest_service =$rest_service;
	   	$rest->rest_state = Input::get('rest_state');
	   	$rest->rest_suburb =Input::get('rest_suburb');
	   	$rest->rest_address2 ='';
	   	$rest->rest_contact =Input::get('rest_contact');
	   	$rest->rest_special = $today_special;
	   	$rest->rest_contact =Input::get('rest_contact');
	   	$rest->rest_cuisine = $rest_cuisine_data;
	   	$rest->rest_mindelivery =Input::get('rest_mindelivery');
	   	$rest->rest_landline =Input::get('rest_landline');
	   	$rest->rest_commission =Input::get('rest_commission');
	   	$rest->rest_email =Input::get('rest_email');
	   	$rest->rest_status =Input::get('rest_status');
	   	$rest->rest_classi =Input::get('rest_classi');
	   	$rest->rest_metatag =$meta;
	   	$rest->rest_mon =Input::get('mon_from').'_'.Input::get('mon_to');
	   	$rest->rest_tues =Input::get('tues_from').'_'.Input::get('tues_to');
	   	$rest->rest_wed =Input::get('wed_from').'_'.Input::get('wed_to');
	   	$rest->rest_thus =Input::get('thu_from').'_'.Input::get('thu_to');
	   	$rest->rest_fri =Input::get('fri_from').'_'.Input::get('fri_to');
	   	$rest->rest_sat =Input::get('sat_from').'_'.Input::get('sat_to');
	   	$rest->rest_sun =Input::get('sun_from').'_'.Input::get('sun_to');
	   	$rest->rest_close =$rest_close_day;
	   	$rest->mon_del =Input::get('mon_from_del').'_'.Input::get('mon_to_del');
	   	$rest->tue_del =Input::get('tues_from_del').'_'.Input::get('tues_to_del');
	   	$rest->wed_del =Input::get('wed_from_del').'_'.Input::get('wed_to_del');
	   	$rest->thu_del =Input::get('thu_from_del').'_'.Input::get('thu_to_del');
	   	$rest->fri_del =Input::get('fri_from_del').'_'.Input::get('fri_to_del');
	   	$rest->sat_del =Input::get('sat_from_del').'_'.Input::get('sat_to_del');
	   	$rest->sun_del =Input::get('sun_from_del').'_'.Input::get('sun_to_del');
				/*$rest->rest_delivery_from = Input::get('delivery_from');
				$rest->rest_delivery_to = Input::get('delivery_to');
*/
				/*$rest->rest_holiday_from = Input::get('holiday_from');
				$rest->rest_holiday_to = Input::get('holiday_to');*/
				$rest->rest_lat = $latitude;
				$rest->rest_long = $longitude;
				$rest->rest_type = $rest_type_data;
				$rest->rest_price_level = Input::get('rest_price_level');
				$rest->rest_servicetax = Input::get('rest_servicetax');
				$rest->rest_partial_pay = Input::get('rest_partial_pay');
				$rest->rest_partial_percent = Input::get('rest_partial_percent');
				$rest->rest_cash_deliver = Input::get('rest_cash_deliver');
				$rest->rest_delivery_upto =Input::get('rest_cash_deliver');
				$rest->rest_min_orderamt = Input::get('rest_min_orderamt');
				$rest->save();
				//$rest_id = $rest->id;
				$rest_id = $rest->rest_id;
				/********* START MANAGE TIME IN OTHER TABLE *******/
		//	$rest_time_detail  = DB::table('rest_time')->where('rest_id', '=' ,$rest_id)->get();
				// Sunday
				$s1 =strtotime(Input::get('sun_from'));
				$s2 = strtotime(Input::get('sun_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sun';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sun';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Mon';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// MONDAY
				$s1 =strtotime(Input::get('mon_from'));
				$s2 = strtotime(Input::get('mon_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Mon';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Mon';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Tue';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// TUESDAY
				$s1 =strtotime(Input::get('tues_from'));
				$s2 = strtotime(Input::get('tues_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Tue';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Tue';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Wed';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// WEDNESDAY
				$s1 =strtotime(Input::get('wed_from'));
				$s2 = strtotime(Input::get('wed_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Wed';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Wed';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Thu';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// THUSDAY
				$s1 =strtotime(Input::get('thu_from'));
				$s2 = strtotime(Input::get('thu_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Thu';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Thu';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Fri';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// FRIDAY
				$s1 =strtotime(Input::get('fri_from'));
				$s2 = strtotime(Input::get('fri_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Fri';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Fri';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Sat';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// SATURADY
				$s1 =strtotime(Input::get('sat_from'));
				$s2 = strtotime(Input::get('sat_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sat';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sat';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Sun';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
				/*********  END  START MANAGE TIME IN OTHER TABLE ******/
				Session::flash('message', 'Restaurant Inserted Sucessfully!');
				//return redirect()->to('/admin/restaurant_list');
			//	return redirect()->to('/admin/vendor_profile/'.Input::get('vendor_id'));
				if(Input::get('vendor_id')>0)
				{
					return redirect()->to('/sub-admin/vendor_profile/'.Input::get('vendor_id'));
				}
				else
				{
					return redirect()->to('/sub-admin/restaurant_view/'.$rest_id);
				}
			}
			else
			{
				/********* START MANAGE TIME IN OTHER TABLE *******/
				$rest_time_detail  = DB::table('rest_time')->where('rest_id', '=' ,$rest_id)->delete();
				// Sunday
				$s1 =strtotime(Input::get('sun_from'));
				$s2 = strtotime(Input::get('sun_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sun';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sun';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Mon';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// MONDAY
				$s1 =strtotime(Input::get('mon_from'));
				$s2 = strtotime(Input::get('mon_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Mon';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Mon';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Tue';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// TUESDAY
				$s1 =strtotime(Input::get('tues_from'));
				$s2 = strtotime(Input::get('tues_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Tue';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Tue';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Wed';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// WEDNESDAY
				$s1 =strtotime(Input::get('wed_from'));
				$s2 = strtotime(Input::get('wed_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Wed';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Wed';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Thu';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// THUSDAY
				$s1 =strtotime(Input::get('thu_from'));
				$s2 = strtotime(Input::get('thu_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Thu';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Thu';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Fri';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// FRIDAY
				$s1 =strtotime(Input::get('fri_from'));
				$s2 = strtotime(Input::get('fri_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Fri';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Fri';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Sat';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
			// SATURADY
				$s1 =strtotime(Input::get('sat_from'));
				$s2 = strtotime(Input::get('sat_to'));
				$s = $this->check_day_time($s1, $s2);
				if($s==0){
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sat';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = date('H:i:s',$s2);
					$rest->save();
				}
				elseif($s==1)
				{
					$rest = new Rest_time;
					$rest->rest_id = $rest_id;
					$rest->day ='Sat';
					$rest->open_time = date('H:i:s',$s1);
					$rest->close_time = '23:59:00';
					$rest->save();
					$rest_close = new Rest_time;
					$rest_close->rest_id = $rest_id;
					$rest_close->day ='Sun';
					$rest_close->open_time = '00:00:00';
					$rest_close->close_time = date('H:i:s',$s2);
					$rest_close->save();
				}
				/*********  END  START MANAGE TIME IN OTHER TABLE ******/
				DB::table('restaurant')
				->where('rest_id', $rest_id)
				->update(['rest_name' =>Input::get('rest_name'),
					'vendor_id' =>Input::get('vendor_id'),
					'rest_address'=>Input::get('rest_address'),
					'rest_city'=>Input::get('rest_suburb'),
					'rest_zip_code'=>Input::get('rest_zip_code'),
					'rest_desc'=>Input::get('rest_desc'),
					'rest_service'=>$rest_service,
					'rest_state'=>Input::get('rest_state'),
					'rest_suburb'=>Input::get('rest_suburb'),
					'rest_address2'=>'',
					'rest_contact'=>Input::get('rest_contact'),
					'rest_special' => $today_special,
					'rest_lat' => $latitude,
					'rest_long' => $longitude,
					'rest_mon'=>Input::get('mon_from').'_'.Input::get('mon_to'),
					'rest_tues'=>Input::get('tues_from').'_'.Input::get('tues_to'),
					'rest_wed'=>Input::get('wed_from').'_'.Input::get('wed_to'),
					'rest_thus'=>Input::get('thu_from').'_'.Input::get('thu_to'),
					'rest_fri'=>Input::get('fri_from').'_'.Input::get('fri_to'),
					'rest_sat'=>Input::get('sat_from').'_'.Input::get('sat_to'),
					'rest_sun'=>Input::get('sun_from').'_'.Input::get('sun_to'),
					'rest_cuisine'=>$rest_cuisine_data,
					'rest_mindelivery'=>Input::get('rest_mindelivery'),
					'rest_landline'=>Input::get('rest_landline'),
					'rest_commission'=>Input::get('rest_commission'),
					'rest_email'=>Input::get('rest_email'),
					'rest_status'=>Input::get('rest_status'),
					'rest_classi'=>Input::get('rest_classi'),
					'rest_close'=>$rest_close_day,
					'rest_logo'=>$new_fileName,
					'rest_banner'=> $new_banner,
					'rest_metatag' => $meta,
				  /*'rest_delivery_from' => Input::get('delivery_from'),
				  'rest_delivery_to' =>  Input::get('delivery_to'),*/
				  'mon_del'=>Input::get('mon_from_del').'_'.Input::get('mon_to_del'),
				  'tue_del'=>Input::get('tue_from_del').'_'.Input::get('tue_to_del'),
				  'wed_del'=>Input::get('wed_from_del').'_'.Input::get('wed_to_del'),
				  'thu_del'=>Input::get('thu_from_del').'_'.Input::get('thu_to_del'),
				  'fri_del'=>Input::get('fri_from_del').'_'.Input::get('fri_to_del'),
				  'sat_del'=>Input::get('sat_from_del').'_'.Input::get('sat_to_del'),
				  'sun_del'=>Input::get('sun_from_del').'_'.Input::get('sun_to_del'),
				  'rest_holiday_from' =>  Input::get('holiday_from'),
				  'rest_holiday_to' =>  Input::get('holiday_to'),
				  'rest_type' =>   $rest_type_data,
				  'rest_price_level' =>Input::get('rest_price_level'),
				  'rest_servicetax' =>Input::get('rest_servicetax'),
				  'rest_partial_pay' => Input::get('rest_partial_pay'),
				  'rest_partial_percent' => Input::get('rest_partial_percent'),
				  'rest_cash_deliver' => Input::get('rest_cash_deliver'),
				  'rest_delivery_upto' => Input::get('rest_delivery_upto'),
				  'rest_min_orderamt' => Input::get('rest_min_orderamt')
				]);
				session()->flash('message', 'Restaurant Update Sucessfully!');
				//return redirect()->to('/admin/restaurant_list');
				return redirect()->to('/sub-admin/restaurant_view/'.$rest_id);
			}
		}
		public function restaurant_delete($rest_id)
		{
			$rest_id;
			$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
			if(!empty($rest_detail))
			{
				$rest_old_logo = $rest_detail[0]->rest_logo;
				$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 // $file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;
				if(file_exists(realpath($file_remove))){
					unlink($file_remove);
				}
				$menu =  DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();
				if(!empty($menu))
				{
					DB::table('menu')->where('restaurant_id', '=', $rest_id)->delete();
				}
				$menu_cat =  DB::table('menu_category')->where('rest_id', '=' ,$rest_id)->get();
				if(!empty($menu_cat))
				{
					DB::table('menu_category')->where('rest_id', '=', $rest_id)->delete();
				}
				DB::table('restaurant')->where('rest_id', '=', $rest_id)->delete();
				return Redirect('/admin/restaurant_list');
			}
		}
		public function view_restaurant()
		{
			$id = Route::current()->getParameter('id');
			$rest_list = DB::table('restaurant')
			->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')
				//	->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')
			->leftJoin('cuisine', function($join){
				$join->on(DB::raw("find_in_set(cuisine.cuisine_id, restaurant.rest_cuisine)",DB::raw(''),DB::raw('')));
			})
			->where('rest_id', '=' ,$id)
			->select('restaurant.*','Group_concat(DISTINCT(cuisine.cuisine_name)) AS cuisine_name','vendor.name')
//'cuisine.cuisine_name'
			->orderBy('restaurant.rest_id', 'desc')
			->get();
			$menu_list = DB::table('menu')
			->where('restaurant_id', '=' ,$id)
			->orderBy('menu_order', 'asc')
			->get();
			$service_list = DB::table('service')
			->where('service_restid', '=' ,$id)
			->orderBy('service_id', 'desc')
			->get();
			$promo_list = DB::table('promotion')
			->where('promo_restid', '=' ,$id)
			->orderBy('promo_id', 'desc')
			->get();
			$bank_list = DB::table('bankdetail')
			->where('bank_restid', '=' ,$id)
			->orderBy('bank_id', 'desc')
			->get();
			$menu_cat_detail = '';
			if(!empty($menu_list)){
				$menu_cat_detail = DB::table('menu_category')
				->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
				->where('menu_category.rest_id', '=' ,$id)
				->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)
				->select('menu_category.*','menu_category_item.*')
				->orderBy('menu_category.menu_category_id', 'asc')
				->get();
			}
			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'id'=>$id,'menu_cate_detail'=>$menu_cat_detail,'service_list'=>$service_list,'promo_list'=>$promo_list,'bank_list'=>$bank_list);
			return View('subadmin.restaurant_view')->with($data_onview);
		}
		public function ajax_update_sortorder()
		{
		//echo '<pre>';
		//print_r($_POST);
			foreach ($_POST['listItem'] as $position => $item)
			{
			//echo  "position = ".$position." ITEM ".$item.'<br>';
				DB::table('menu')
				->where('restaurant_id', Input::get('rest_id'))
				->where('menu_id', $item)
				->update(['menu_order' =>($position+1)
			]);
			}
		}
		/* MANU FUNCTION */
		public function menu_list()
		{
			DB::connection()->enableQueryLog();
			$menu_list = DB::table('menu')
			->leftJoin('restaurant', 'menu.restaurant_id', '=', 'restaurant.rest_id')
			->select('menu.*','restaurant.rest_name' )
			->orderBy('menu_order', 'asc')
			->get();
			$data_onview = array('menu_list' =>$menu_list);
			return View('subadmin.menu_list')->with($data_onview);
		}
		public function ajax_menu_form()
		{
			$id =0;
			if(Input::get('menu'))
			{
				$rest_id =  Input::get('rest');
				$menu_id =  Input::get('menu');
				$id = Input::get('menu');
				$aria_id =  Input::get('aria_id');
				$menu_detail  = DB::table('menu')->where('menu_id', '=' ,$menu_id)->get();
				$data_onview = array('rest_id' =>$rest_id,'aria_id' =>$aria_id,
					'menu_detail' =>$menu_detail,
					'id'=>$id);
			}
			else
			{
				$rest_id =  Input::get('rest_id');
				$aria_id =  Input::get('aria_id');
				$id =0;
				$data_onview = array('rest_id' =>$rest_id,'aria_id' =>$aria_id,'menu_detail' =>'','id'=>$id);
			}
			return View('subadmin.ajax.menu_form')->with($data_onview);
		}
		public function menu_form()
		{
			$rest_detail  = DB::table('restaurant')->get();
			if(Route::current()->getParameter('id'))
			{
				$id = Route::current()->getParameter('id');
				$menu_detail  = DB::table('menu')->where('menu_id', '=' ,$id)->get();
				$data_onview = array('rest_detail' =>$rest_detail,
					'menu_detail'=>$menu_detail,
					'id'=>$id);
			}
			else
			{
				$id =0;
				$data_onview = array('rest_detail' =>$rest_detail,
					'id'=>$id);
			}
			return View('subadmin.menu_form')->with($data_onview);
		}
		public function menu_action(Request $request)
		{

		$area_id = Input::get('caria_id');//Auth::guard('subadmin')->user()->zipcoad;

	    /*$area_id = DB::table('area_management')
				  ->where('zipcode', '=' , $zipcode)
		          ->value('area_id'); */


		if(Input::get('from')=='back'){
			$rest_id = 	Input::get('restaurant_id');
			$rest_list = DB::table('restaurant')
			->where('rest_id', '=' ,$rest_id)
			->orderBy('restaurant.rest_id', 'desc')
			->get();
			$menu_list = DB::table('menu')
			->where('restaurant_id', '=' ,Input::get('restaurant_id'))
			->orderBy('menu_order', 'asc')
			->get();
			$menu_id = $menu_list[0]->menu_id;
			$menu_detail = DB::table('menu')
			->where('restaurant_id', '=' ,$rest_id)
			->where('menu_id', '=' ,$menu_id)
			->orderBy('menu_order', 'asc')
			->get();
			$menu_cat_detail = '';
			if(!empty($menu_list)){
				$menu_cat_detail = DB::table('menu_category')
				->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
				->where('menu_category.rest_id', '=' ,$rest_id)
				->where('menu_category.menu_id', '=' ,$menu_id)
				->select('menu_category.*','menu_category_item.*')
				->orderBy('menu_category.menu_category_id', 'asc')
				->get();
			}
			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
			return View('subadmin.ajax.menu_list')->with($data_onview);
		}
		else
		{
			$menu_id = Input::get('menu_id');
			if($menu_id==0)
			{
				$rest_id =  Input::get('restaurant_id');
				$menu_max_value  = DB::table('menu')
				->where('restaurant_id', '=' ,$rest_id)
				->select(\DB::raw('MAX(menu.menu_order) as max_order'))
				->get();
				$max_order_no = $menu_max_value[0]->max_order;
				$menu = new Menu;
				$menu->restaurant_id = Input::get('restaurant_id'); 
                $menu->area_id =  $area_id;
				$menu->menu_name =  Input::get('menu_name');
				$menu->menu_desc =  Input::get('menu_desc');
				$menu->menu_status =  Input::get('menu_status');
				$menu->menu_order =  ($max_order_no+1);
				$menu->save();
				$menu_id = $menu->menu_id;
				Session::flash('menu_message', 'Restaurant Menu Inserted Sucessfully!');
					//return redirect()->to('/admin/menu_list');
				$rest_list = DB::table('restaurant')
				->where('rest_id', '=' ,$rest_id)
				->orderBy('restaurant.rest_id', 'desc')
				->get();
				$menu_detail = DB::table('menu')
				->where('restaurant_id', '=' ,$rest_id)
				->where('menu_id', '=' ,$menu_id)
				->where('area_id', '=' ,$area_id)
				->orderBy('menu_order', 'asc')
				->get();
				$menu_list = DB::table('menu')
				->where('restaurant_id', '=' ,Input::get('restaurant_id'))
				->where('area_id', '=' ,$area_id)
				->orderBy('menu_order', 'asc')
				->get();
				$menu_cat_detail = '';
				if(!empty($menu_list)){
					$menu_cat_detail = DB::table('menu_category')
					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
					->where('menu_category.rest_id', '=' ,$rest_id)
					->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)
					->where('menu_category.area_id', '=' ,$area_id)
					->select('menu_category.*','menu_category_item.*')
					->orderBy('menu_category.menu_category_id', 'asc')
					->get();
				}
				$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'aria_id'=>$area_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
				   //  $data_onview = array('menu_list'=>$menu_list,'res_id'=>$res_id);
				   /*if(Input::get('from')=='submit'){ */
					return View('subadmin.ajax.menu_list')->with($data_onview);
				/* }
				elseif(Input::get('from')=='addnext'){
					return View('subadmin.ajax.menu_list_form')->with($data_onview);
				} */
			}
			else
			{
				$rest_id = 	Input::get('restaurant_id');
				if(Input::get('from')=='update'){
					DB::table('menu')
					->where('menu_id', $menu_id)
					->update(['restaurant_id' =>Input::get('restaurant_id'),
						'menu_name'=>Input::get('menu_name'),
						'menu_desc'=>Input::get('menu_desc'),
						'menu_status'=>Input::get('menu_status')
					]);
					session()->flash('menu_message', 'Restaurant Menu Updated Sucessfully!');
				}
				$rest_list = DB::table('restaurant')
				->where('rest_id', '=' ,$rest_id)
				->orderBy('restaurant.rest_id', 'desc')
				->get();
				$menu_detail = DB::table('menu')
				->where('restaurant_id', '=' ,$rest_id)
				->where('menu_id', '=' ,$menu_id)
				->where('area_id', '=' ,$area_id)
				->orderBy('menu_order', 'asc')
				->get();
				$menu_list = DB::table('menu')
				->where('restaurant_id', '=' ,Input::get('restaurant_id'))
				->where('area_id', '=' ,$area_id)
				->orderBy('menu_order', 'asc')
				->get();
				$menu_cat_detail = '';
				if(!empty($menu_list)){
					$menu_cat_detail = DB::table('menu_category')
					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
					->where('menu_category.rest_id', '=' ,$rest_id)
					->where('menu_category.menu_id', '=' ,$menu_id)
					->where('menu_category.area_id', '=' ,$area_id)
					->select('menu_category.*','menu_category_item.*')
					->orderBy('menu_category.menu_category_id', 'asc')
					->get();
				}
				$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'aria_id'=>$area_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
				return View('subadmin.ajax.menu_list')->with($data_onview);
			}
		}
	}
	public function menu_delete($menu_id)
	{
		$menu_cat =  DB::table('menu_category')->where('menu_id', '=' ,$menu_id)->get();
		if(!empty($menu_cat))
		{
			DB::table('menu_category')->where('menu_id', '=', $menu_id)->delete();
		}
		DB::table('menu')->where('menu_id', '=', $menu_id)->delete();
		return Redirect('/admin/menu_list');
	}
	/* MENU CATEGORY FUNCTIONALITY */
	public function menu_category_list()
	{
		DB::connection()->enableQueryLog();
		$menu_cat_list = DB::table('menu_category')
		->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')
		->leftJoin('restaurant', 'menu_category.rest_id', '=', 'restaurant.rest_id')
		->select('menu_category.*','restaurant.rest_name','menu.menu_name' )
		->orderBy('menu.menu_id', 'desc')
		->get();
		$data_onview = array('category_list' =>$menu_cat_list);
		return View('subadmin.menu_category_list')->with($data_onview);
	}
	public function ajax_menu_item_form()
	{
		$rest_id =  Input::get('rest_id');
		$menu_id =  Input::get('menu_id');
		$menu_aria_id =  Input::get('menu_aria_id');
		$id =0;
		$menu_detail = DB::table('menu')
		->where('menu_id', '=' ,$menu_id)
		->get();
		$data_onview = array('rest_id' =>$rest_id,
			'menu_id' =>$menu_id,
			'menu_aria_id' =>$menu_aria_id,
			'menu_name' =>$menu_detail[0]->menu_name,
			'id'=>$id);
		return View('subadmin.ajax.menu_form_item')->with($data_onview);
	}
	public function ajax_update_menu_category()
	{
		//echo '<pre>';
		//print_r($_POST);
		$rest_id =Input::get('rest');
		$menu_id=Input::get('menu');
		$menu_cate_id = Input::get('menu_itme');
		$menu_detail = DB::table('menu')->where('menu_id', '=' ,$menu_id)->get();
		$cate_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$menu_cate_id)->get();
		$menu_cat_addon = '';
		if($cate_detail[0]->menu_category_portion=='yes'){
			$menu_cat_addon =  DB::table('menu_category_item')->where('menu_category', '=' ,$menu_cate_id)->get();
		}
		$data_onview = array('rest_id' =>$rest_id,
			'menu_id' =>$menu_id,
			'menu_name' =>$menu_detail[0]->menu_name,
			'cate_detail'=>$cate_detail,
			'area_id' => Input::get('aria_id'),
			"menu_cat_addon"=>$menu_cat_addon,
			'id'=>$menu_cate_id);
		return View('subadmin.ajax.update_menu_form_item')->with($data_onview);
	}
	public function menu_category_form()
	{
		$rest_detail  = DB::table('restaurant')->get();
		if(Route::current()->getParameter('id'))
		{
			$id = Route::current()->getParameter('id');
			$cate_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$id)->get();
			$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$cate_detail[0]->rest_id)->get();
			$data_onview = array('rest_detail'=>$rest_detail,
				'menu_detail'=>$menu_detail,
				'cate_detail'=>$cate_detail,
				'id'=>$id);
		}
		else
		{
			$id =0;
			$data_onview = array('rest_detail'=>$rest_detail,
				'menu_detail'=>'',
				'id'=>$id);
		}
		return View('subadmin.menu_category_form')->with($data_onview);
	}
	public function menu_category_action(Request $request)
	{
			
		//Auth::guard('subadmin')->user()->zipcoad;

	    /* $area_id = DB::table('area_management')
				  ->where('zipcode', '=' , $zipcode)
		          ->value('area_id'); */

		$menu_category_id = Input::get('menu_category_id');

	   if(Input::get('mitem_aria_id')){
        $mitem_aria_id = Input::get('mitem_aria_id');
        }else{ $mitem_aria_id = '';	}

		if(Input::get('from')=='back'){
			$menu_id = Input::get('menu_id');
			$rest_id = Input::get('restaurant_id');
			$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
			$menu_list = DB::table('menu')
			->where('restaurant_id', '=' ,$rest_id)
			->orderBy('menu_order', 'asc')
			->get();
			$menu_detail = DB::table('menu')
			->where('restaurant_id', '=' ,$rest_id)
			->where('menu_id', '=' ,$menu_id)
			->orderBy('menu_id', 'desc')
			->get();
			$menu_cat_detail = '';
			if(!empty($menu_list)){
				$menu_cat_detail = DB::table('menu_category')
				->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
				->where('menu_category.rest_id', '=' ,$rest_id)
				->where('menu_category.menu_id', '=' ,$menu_id)
				->select('menu_category.*','menu_category_item.*')
				->orderBy('menu_category.menu_category_id', 'asc')
				->get();
			}
			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
			return View('subadmin.ajax.menu_list')->with($data_onview);
		}
		else  
		{
			if($menu_category_id==0)
			{
				$menu = new Menu_category;
				$menu->rest_id = Input::get('restaurant_id');
				$menu->menu_id =  Input::get('menu_id');
				$menu->area_id =  $mitem_aria_id;
				$menu->menu_category_name =  Input::get('menu_name');
				$menu->menu_category_desc =  Input::get('menu_desc');
				$menu->menu_cat_popular =  Input::get('menu_cat_popular');
				$menu->menu_cat_diet =  Input::get('menu_cat_diet');
				$menu->menu_category_price =  Input::get('menu_price');
				$menu->menu_category_portion = Input::get('diff_size');
				$menu->menu_cat_status = Input::get('menu_cat_status');
				$menu->save();
				$menu_category_id = $menu->menu_category_id;
				if(Input::get('diff_size')=='yes')
				{
					for($c=0;$c<count($_POST['item_id']);$c++)
					{
						if((!empty(trim($_POST['item_title'][$c]))))
						{
							$menu_sub_itme = new  Category_item;
							$menu_sub_itme->rest_id	 =Input::get('restaurant_id');
							$menu_sub_itme->menu_id	 = Input::get('menu_id');
							$menu_sub_itme->area_id	 = $mitem_aria_id;
							$menu_sub_itme->menu_category	 = $menu_category_id;
							$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];
							$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];
							$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];
							$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');
							$menu_sub_itme->save();
						}
					}
						/*$menu_addon = new Menu_category_item;
						$menu_addon->rest_id = Input::get('restaurant_id');
						$menu_addon->menu_id =  Input::get('menu_id');
						$menu_addon->menu_category =  $menu_category_id;
						$menu_addon->menu_large_title =  Input::get('l_title');
						$menu_addon->menu_medium_title =  Input::get('m_title');
						$menu_addon->menu_small_title =  Input::get('s_title');
						$menu_addon->menu_large_price =  Input::get('l_price');
						$menu_addon->menu_medium_price =  Input::get('m_price');
						$menu_addon->menu_small_price =  Input::get('s_price');
							$menu_addon->menu_lstatus =  Input::get('l_status');
							$menu_addon->menu_mstatus =  Input::get('m_status');
							$menu_addon->menu_sstatus =  Input::get('s_status');
						$menu_addon->save();
						$menu_cat_itm_id = $menu_addon->id;*/
					}
					Session::flash('menu_message', 'Menu Item Inserted Sucessfully!');
					//return redirect()->to('/admin/menu_categorylist');
					$rest_id = Input::get('restaurant_id');
					/* START */
					$menu_id = Input::get('menu_id');
					$rest_id = Input::get('restaurant_id');
					$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
					$menu_list = DB::table('menu')
					->where('restaurant_id', '=' ,$rest_id)
					->where('area_id', '=' ,$mitem_aria_id)
					->orderBy('menu_order', 'asc')
					->get();
					$menu_detail = DB::table('menu')
					->where('restaurant_id', '=' ,$rest_id)
					->where('menu_id', '=' ,$menu_id)
					->where('area_id', '=' ,$mitem_aria_id)
					->orderBy('menu_id', 'desc')
					->get();
					$menu_cat_detail = '';
					if(!empty($menu_list)){
						$menu_cat_detail = DB::table('menu_category')
						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
						->where('menu_category.rest_id', '=' ,$rest_id)
						->where('menu_category.menu_id', '=' ,$menu_id)
						->where('menu_category.area_id', '=' ,$mitem_aria_id)
						->select('menu_category.*','menu_category_item.*')
						->orderBy('menu_category.menu_category_id', 'asc')
						->get();
					}
					/* END */
					//if(Input::get('from')=='submit'){
						$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'aria_id'=>$mitem_aria_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
						return View('subadmin.ajax.menu_list')->with($data_onview);
					/* }
					elseif(Input::get('from')=='addnext'){
						$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail,'id'=>'0');
						return View('subadmin.ajax.menu_item_list_form')->with($data_onview);
					} */
				}
				else
				{
					if(Input::get('from')=='update'){
						DB::table('menu_category')
						->where('menu_category_id', $menu_category_id)
						->update(['menu_category_name'=>Input::get('menu_name'),
							'menu_category_price' =>  Input::get('menu_price'),
							'menu_category_desc'=>Input::get('menu_desc'),
							'menu_category_portion'=>Input::get('diff_size'),
							'menu_cat_status'=>Input::get('menu_cat_status'),
							'menu_cat_popular'=> Input::get('menu_cat_popular'),
							'menu_cat_diet'=> Input::get('menu_cat_diet')
						]);
						if(Input::get('diff_size')=='yes')
						{
							for($c=0;$c<count($_POST['item_id']);$c++)
							{
								if($_POST['item_id'][$c]==0)
								{
									if((!empty(trim($_POST['item_title'][$c]))))
									{
										$menu_sub_itme = new  Category_item;
										$menu_sub_itme->rest_id	 =Input::get('restaurant_id');
										$menu_sub_itme->menu_id	 = Input::get('menu_id');
										$menu_sub_itme->menu_category	 = $menu_category_id;
										$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];
										$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];
										$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];
										$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');
										$menu_sub_itme->save();
									}
								}
								else
								{
									DB::table('category_item')
									->where('menu_cat_itm_id',$_POST['item_id'][$c])
									->update(['menu_item_title'=>$_POST['item_title'][$c],
										'menu_item_price' =>$_POST['item_price'][$c],
										'menu_item_status'=>$_POST['item_status'][$c]
									]);
								}
							}
					/*
						$addon_list  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_category_id)->get();
						if(empty($addon_list)){
							$menu_addon = new Menu_category_item;
							$menu_addon->rest_id = Input::get('restaurant_id');
							$menu_addon->menu_id =  Input::get('menu_id');
							$menu_addon->menu_category =  $menu_category_id;
							$menu_addon->menu_large_price =  Input::get('l_price');
							$menu_addon->menu_medium_price =  Input::get('m_price');
							$menu_addon->menu_small_price =  Input::get('s_price');
							$menu_addon->menu_large_title =  Input::get('l_title');
							$menu_addon->menu_medium_title =  Input::get('m_title');
							$menu_addon->menu_small_title =  Input::get('s_title');
							$menu_addon->menu_lstatus =  Input::get('l_status');
							$menu_addon->menu_mstatus =  Input::get('m_status');
							$menu_addon->menu_sstatus =  Input::get('s_status');
							$menu_addon->save();
							$menu_cat_itm_id = $menu_addon->id;
						}
						else
						{
							DB::table('menu_category_item')
							->where('menu_category', $menu_category_id)
							->update(['menu_large_price'=>Input::get('l_price'),
									  'menu_medium_price' =>  Input::get('m_price'),
									  'menu_small_price'=>Input::get('s_price'),
									  'menu_large_title'=>Input::get('l_title'),
									  'menu_medium_title'=>Input::get('m_title'),
									  'menu_small_title'=>Input::get('s_title'),
									  'menu_lstatus'=> Input::get('l_status'),
									  'menu_mstatus'=> Input::get('m_status'),
									  'menu_sstatus'=> Input::get('s_status')
									 ]);
						}
					*/
					}
					elseif(Input::get('diff_size')=='no')
					{
						$addon_list  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_category_id)->get();
						if(!empty($addon_list))
						{
							DB::table('menu_category_item')->where('menu_category', '=', $menu_category_id)->delete();
						}
					}
					session()->flash('menu_message', 'Restaurant Menu Item Updated Sucessfully!');
				}
				$menu_id = Input::get('menu_id');
				$rest_id = Input::get('restaurant_id');
				$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
				$menu_list = DB::table('menu')
				->where('restaurant_id', '=' ,$rest_id)
				->where('area_id', '=' ,$mitem_aria_id)
				->orderBy('menu_order', 'asc')
				->get();
				$menu_detail = DB::table('menu')
				->where('restaurant_id', '=' ,$rest_id)
				->where('menu_id', '=' ,$menu_id)
				->where('area_id', '=' ,$mitem_aria_id)
				->orderBy('menu_id', 'desc')
				->get();
				$menu_cat_detail = '';
				if(!empty($menu_list)){
					$menu_cat_detail = DB::table('menu_category')
					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
					->where('menu_category.rest_id', '=' ,$rest_id)
					->where('menu_category.menu_id', '=' ,$menu_id)
					->where('menu_category.area_id', '=' ,$mitem_aria_id)
					->select('menu_category.*','menu_category_item.*')
					->orderBy('menu_category.menu_category_id', 'asc')
					->get();
				}
				$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'aria_id'=>$mitem_aria_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
				return View('subadmin.ajax.menu_list')->with($data_onview);
			}
		}
	}
	public function get_menulist()
	{
		$rest_id =Input::get('restaurant_id');
		$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();
		$menu_select = '';
		if(!empty($menu_detail)){
			$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >
			<option value="">Select Restaurant </option>';
			foreach($menu_detail as $menu)
			{
				$menu_select .='<option value="'.$menu->menu_id.'">'.$menu->menu_name.'</option>';
			}
			$menu_select .='</select> ';
		}
		else
		{
			$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >
			<option value="">Select Restaurant </option>
			</select> ';
		}
		echo $menu_select ;
	}
	/* MANAGE MENU CATEGORY ITEM FUNCTIONALITY  */
	public function get_categorylist()
	{
		$rest_id =Input::get('restaurant_id');
		$menu_id =Input::get('menu_id');
		$cate_detail  = DB::table('menu_category')
		->where('rest_id', '=' ,$rest_id)
		->where('menu_id', '=' ,$menu_id)
		->get();
		$cate_select = '';
		if(!empty($cate_detail)){
			$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >
			<option value="">Select Menu Category </option>';
			foreach($cate_detail as $menu)
			{
				$cate_select .='<option value="'.$menu->menu_category_id.'">'.$menu->menu_category_name.'</option>';
			}
			$cate_select .='</select> ';
		}
		else
		{
			$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >
			<option value="">Select Menu Category  </option>
			</select> ';
		}
		echo $cate_select ;
	}
	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS START */
	public function ajax_addons_list()
	{
		//echo '<pre>';
		//print_r($_POST);
		$addon_list = '';
		$rest_id = Input::get('rest');
		$menu_id =Input::get('menu');
		$menu_item = Input::get('menu_itme');
		$addon_list = DB::table('menu_category_addon')
		->where('addon_restid', '=' ,$rest_id)
		->where('addon_menuid', '=' ,$menu_id)
		->where('addon_menucatid', '=' ,$menu_item)
		->orderBy('addon_id', 'asc')
		->get();
		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$menu_list = DB::table('menu')
		->where('restaurant_id', '=' ,$rest_id)
		->where('menu_id', '=' ,$menu_id)
		->get();
		$item_detil = DB::table('menu_category')
		->where('rest_id', '=' ,$rest_id)
		->where('menu_category_id', '=' ,$menu_item)
		->where('menu_id', '=' ,$menu_id)
		->get();
		$rest_name = $rest_list[0]->rest_name;
		$menu_name = $menu_list[0]->menu_name;
		$item_name = $item_detil[0]->menu_category_name;
		$data_onview = array('addon_list'=>$addon_list,
			'rest_name'=>$rest_name,
			'menu_name'=>$menu_name,
			'item_name'=>$item_name,
			'rest_id'=>$rest_id,
			'menu_id'=>$menu_id,
			'menu_item'=>$menu_item,
		);
		return View('subadmin.ajax.addon_list')->with($data_onview);;
	}
	public function ajax_addons_form()
	{
		//print_r($_POST);
		$id =0;
		$addon_detail = '';
		if(Input::get('addon_id'))
		{
			$addon_id = Input::get('addon_id');
			$id = Input::get('addon_id');
			$addon_detail  = DB::table('menu_category_addon')->where('addon_id', '=' ,$addon_id)->get();
		}
		$addon_list = '';
		$rest_id = Input::get('rest');
		$menu_id =Input::get('menu');
		$menu_item = Input::get('menu_itme');
		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$menu_list = DB::table('menu')
		->where('restaurant_id', '=' ,$rest_id)
		->where('menu_id', '=' ,$menu_id)
		->get();
		$item_detil = DB::table('menu_category')
		->where('rest_id', '=' ,$rest_id)
		->where('menu_category_id', '=' ,$menu_item)
		->where('menu_id', '=' ,$menu_id)
		->get();
		$rest_name = $rest_list[0]->rest_name;
		$menu_name = $menu_list[0]->menu_name;
		$item_name = $item_detil[0]->menu_category_name;
		$data_onview = array('addon_detail'=>$addon_detail,
			'rest_name'=>$rest_name,
			'menu_name'=>$menu_name,
			'item_name'=>$item_name,
			'rest_id'=>$rest_id,
			'menu_id'=>$menu_id,
			'menu_item'=>$menu_item,
			'id'=>$id
		);
		return View('subadmin.ajax.addon_form')->with($data_onview);
	}
	public function ajax_addons_action(Request $request)
	{
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		$addon_id = Input::get('addon_id');
		if($addon_id==0)
		{
			$addon = new Menu_category_addon;
			$addon->addon_restid = Input::get('addon_restid');
			$addon->addon_menuid =Input::get('addon_menuid');
			$addon->addon_menucatid = Input::get('addon_menucatid');
			$addon->addon_name = trim(Input::get('addon_name'));
			$addon->addon_price =  Input::get('addon_price');
			$addon->addon_status =  Input::get('addon_status');
			$addon->addon_groupname = strtoupper(trim(Input::get('addon_groupname')));
			$addon->addon_option =  Input::get('addon_option');
			$addon->save();
			$addon_id = $addon->addon_id;
			Session::flash('addon_message', 'Restaurant Addon Inserted Sucessfully!');
			$rest_id =  Input::get('service_restid');
			/* FEATCH DATA FROM TABLE FOR SHOW */
			$addon_list = '';
			$rest_id = Input::get('addon_restid');
			$menu_id = Input::get('addon_menuid');
			$menu_item = Input::get('addon_menucatid');
			$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
			$menu_list = DB::table('menu')
			->where('restaurant_id', '=' ,$rest_id)
			->where('menu_id', '=' ,$menu_id)
			->get();
			$item_detil = DB::table('menu_category')
			->where('rest_id', '=' ,$rest_id)
			->where('menu_category_id', '=' ,$menu_item)
			->where('menu_id', '=' ,$menu_id)
			->get();
			$rest_name = $rest_list[0]->rest_name;
			$menu_name = $menu_list[0]->menu_name;
			$item_name = $item_detil[0]->menu_category_name;
			/* END */
			if(Input::get('from')=='submit'){
				$addon_list = DB::table('menu_category_addon')
				->where('addon_restid', '=' ,$rest_id)
				->where('addon_menuid', '=' ,$menu_id)
				->where('addon_menucatid', '=' ,$menu_item)
				->orderBy('addon_id', 'asc')
				->get();
				$data_onview = array('addon_list'=>$addon_list,
					'rest_name'=>$rest_name,
					'menu_name'=>$menu_name,
					'item_name'=>$item_name,
					'rest_id'=>$rest_id,
					'menu_id'=>$menu_id,
					'menu_item'=>$menu_item
				);
				return View('subadmin.ajax.addon_list')->with($data_onview);
			}
			elseif(Input::get('from')=='addnext'){
				$id = 0;
				$addon_detail = '';
				$data_onview = array('addon_detail'=>$addon_detail,
					'rest_name'=>$rest_name,
					'menu_name'=>$menu_name,
					'item_name'=>$item_name,
					'rest_id'=>$rest_id,
					'menu_id'=>$menu_id,
					'menu_item'=>$menu_item,
					'id'=>$id
				);
				return View('subadmin.ajax.addon_form')->with($data_onview);
			}
		}
		else
		{
			$rest_id = Input::get('addon_restid');
			$menu_id = Input::get('addon_menuid');
			$menu_item = Input::get('addon_menucatid');
			$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
			$menu_list = DB::table('menu')
			->where('restaurant_id', '=' ,$rest_id)
			->where('menu_id', '=' ,$menu_id)
			->get();
			$item_detil = DB::table('menu_category')
			->where('rest_id', '=' ,$rest_id)
			->where('menu_category_id', '=' ,$menu_item)
			->where('menu_id', '=' ,$menu_id)
			->get();
			$rest_name = $rest_list[0]->rest_name;
			$menu_name = $menu_list[0]->menu_name;
			$item_name = $item_detil[0]->menu_category_name;
			if(Input::get('from')=='update'){
				DB::table('menu_category_addon')
				->where('addon_id', $addon_id)
				->update(['addon_name' =>trim(Input::get('addon_name')),
					'addon_price'=>Input::get('addon_price'),
					'addon_status'=>Input::get('addon_status'),
					'addon_groupname'=>strtoupper(trim(Input::get('addon_groupname'))),
					'addon_option'=>  Input::get('addon_option')
				]);
				Session::flash('addon_message', 'Restaurant Addon Update Sucessfully!');
			}
			/* FEATCH DATA FROM TABLE FOR SHOW */
			$addon_list = DB::table('menu_category_addon')
			->where('addon_restid', '=' ,$rest_id)
			->where('addon_menuid', '=' ,$menu_id)
			->where('addon_menucatid', '=' ,$menu_item)
			->orderBy('addon_id', 'asc')
			->get();
			$data_onview = array('addon_list'=>$addon_list,
				'rest_name'=>$rest_name,
				'menu_name'=>$menu_name,
				'item_name'=>$item_name,
				'rest_id'=>$rest_id,
				'menu_id'=>$menu_id,
				'menu_item'=>$menu_item
			);
			return View('subadmin.ajax.addon_list')->with($data_onview);
		}
	}
	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS END */
	/* AJAX OTHER FUNCTION */
	public function ajax_show_menudetail()
	{

		$area_id = Input::get('aria');//Auth::guard('subadmin')->user()->zipcoad;

		/* $area_id = DB::table('area_management')
				  ->where('zipcode', '=' , $zipcode)
		          ->value('area_id'); */

		$menu_id = Input::get('menu');
		$rest_id = Input::get('rest');
		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$menu_list = DB::table('menu')
		->where('restaurant_id', '=' ,$rest_id)
		->where('area_id', '=' ,$area_id)
		->orderBy('menu_order', 'asc')
		->get();
		$menu_detail = DB::table('menu')
		->where('restaurant_id', '=' ,$rest_id)
		->where('menu_id', '=' ,$menu_id)
		->where('area_id', '=' ,$area_id)
		->orderBy('menu_id', 'desc')
		->get();
		$menu_cat_detail = '';
		if(!empty($menu_list)){
			$menu_cat_detail = DB::table('menu_category')
			->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
			->where('menu_category.rest_id', '=' ,$rest_id)
			->where('menu_category.menu_id', '=' ,$menu_id)
			->where('menu_category.area_id', '=' ,$area_id)
			->select('menu_category.*','menu_category_item.*')
			->orderBy('menu_category.menu_category_id', 'asc')
			->get();
		}
		$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'aria_id'=>$area_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
		       //  $data_onview = array('menu_list'=>$menu_list,'res_id'=>$res_id);
		return View('subadmin.ajax.menu_list')->with($data_onview);
	}
	/* SERVICE AREA TABE START */
	public function ajax_service_form()
	{
		$rest_id =  Input::get('rest_id');
		$id =0;
		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$service_detail = '';
		if(Input::get('service_id'))
		{
			$service_id = Input::get('service_id');
			$id = Input::get('service_id');
			$service_detail  = DB::table('service')->where('service_id', '=' ,$service_id)->get();
		}
		$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'service_detail'=>$service_detail,'id'=>$id);
		return View('subadmin.ajax.service_form')->with($data_onview);
	}
	public function service_action(Request $request)
	{
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		$service_id = Input::get('service_id');
		if(Input::get('from')=='back'){
			$service_id = Input::get('service_id');
			$rest_id =  Input::get('service_restid');
			$service_list = DB::table('service')
			->where('service_restid', '=' ,$rest_id)
			->orderBy('service_id', 'desc')
			->get();
			$data_onview = array('rest_id' =>$rest_id,'service_list'=>$service_list);
			return View('subadmin.ajax.service_list')->with($data_onview);
		}
		else
		{
			if($service_id==0)
			{
				$service = new Service;
				$service->service_restid = Input::get('service_restid');
				$service->service_suburb =Input::get('service_suburb');
				$service->service_postcode = Input::get('service_postcode');
				$service->service_charge = Input::get('service_charge');
				$service->service_status =  Input::get('service_status');
				$service->save();
				$service_id = $service->service_id;
				Session::flash('serivce_message', 'Restaurant Service Inserted Sucessfully!');
				$rest_id =  Input::get('service_restid');
				if(Input::get('from')=='submit'){
					$service_list = DB::table('service')
					->where('service_restid', '=' ,$rest_id)
					->orderBy('service_id', 'desc')
					->get();
					$data_onview = array('rest_id' =>$rest_id,'service_list'=>$service_list);
					return View('subadmin.ajax.service_list')->with($data_onview);
				}
				elseif(Input::get('from')=='addnext'){
					$id =0;
					$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
					$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id);
					return View('subadmin.ajax.service_form')->with($data_onview);
				}
			}
			else
			{
				$service_id = Input::get('service_id');
				$rest_id =  Input::get('service_restid');
				if(Input::get('from')=='update'){
					DB::table('service')
					->where('service_id', $service_id)
					->update(['service_suburb' =>Input::get('service_suburb'),
						'service_postcode'=>Input::get('service_postcode'),
						'service_charge'=>Input::get('service_charge'),
						'service_status'=>Input::get('service_status')
					]);
					Session::flash('serivce_message', 'Restaurant Service Update Sucessfully!');
				}
				$service_list = DB::table('service')
				->where('service_restid', '=' ,$rest_id)
				->orderBy('service_id', 'desc')
				->get();
				$data_onview = array('rest_id' =>$rest_id,'service_list'=>$service_list);
				return View('subadmin.ajax.service_list')->with($data_onview);
			}
		}
	}
	/* SERVICE AREA TABE END */
	/* PROMOSTIONS TABE START */
	public function ajax_promo_form()
	{
		$rest_id =  Input::get('rest_id');
		$id =0;
		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$rest_menu	 = DB::table('menu_category')
		->select('menu_category.*')
		->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')
		->where('menu.menu_status', '=' , '1')
		->where('menu_category.rest_id', '=' ,$rest_id)
		->where('menu_category.menu_cat_status', '=' ,'1')
		->orderBy('menu_category.menu_category_id', 'desc')
		->get();
		$promo_detail = '';
		if(Input::get('promo_id'))
		{
			$promo_id = Input::get('promo_id');
			$id = Input::get('promo_id');
			$promo_detail  = DB::table('promotion')->where('promo_id', '=' ,$promo_id)->get();
		}
		$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'promo_detail'=>$promo_detail,'id'=>$id,'rest_menu'=>$rest_menu);
		return View('subadmin.ajax.promo_form')->with($data_onview);
	}
	public function promo_action_07_Nov_2017(Request $request)
	{
		$promo_id = Input::get('promo_id');
		if(Input::get('from')=='back'){
			$promo_id = Input::get('promo_id');
			$rest_id =  Input::get('promo_restid');
			$promo_list = DB::table('promotion')
			->where('promo_restid', '=' ,$rest_id)
			->orderBy('promo_id', 'desc')
			->get();
			$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
			return View('subadmin.ajax.promo_list')->with($data_onview);
		}
		else
		{
			if($promo_id==0)
			{
				$promo = new Promotion;
				$promo->promo_restid = Input::get('promo_restid');
				$promo->promo_on =Input::get('promo_on');
				$promo->promo_desc = Input::get('promo_desc');
				$promo->promo_mode = Input::get('promo_mode');
				$promo->promo_value	 =  Input::get('promo_value');
				$promo->promo_start	 =  Input::get('promo_start');
				$promo->promo_end	 =  Input::get('promo_end');
				$promo->promo_status =  Input::get('promo_status');
				$promo->save();
				$promo_id = $promo->promo_id;
				Session::flash('promo_message', 'Restaurant Promostion Inserted Sucessfully!');
				$rest_id =  Input::get('promo_restid');
				if(Input::get('from')=='submit'){
					$promo_list = DB::table('promotion')
					->where('promo_restid', '=' ,$rest_id)
					->orderBy('promo_id', 'desc')
					->get();
					$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
					return View('subadmin.ajax.promo_list')->with($data_onview);
				}
				elseif(Input::get('from')=='addnext'){
					$id =0;
					$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
					$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id);
					return View('subadmin.ajax.promo_form')->with($data_onview);
				}
			}
			else
			{
				$promo_id = Input::get('promo_id');
				$rest_id =  Input::get('promo_restid');
				if(Input::get('from')=='update'){
					DB::table('promotion')
					->where('promo_id', $promo_id)
					->update(['promo_restid' =>Input::get('promo_restid'),
						'promo_on' =>Input::get('promo_on'),
						'promo_desc' =>Input::get('promo_desc'),
						'promo_mode' =>Input::get('promo_mode'),
						'promo_value' =>Input::get('promo_value'),
						'promo_start' =>Input::get('promo_start'),
						'promo_end' =>Input::get('promo_end'),
						'promo_status' =>Input::get('promo_status')
					]);
					Session::flash('promo_message', 'Restaurant Promostion Update Sucessfully!');
				}
				$promo_list = DB::table('promotion')
				->where('promo_restid', '=' ,$rest_id)
				->orderBy('promo_id', 'desc')
				->get();
				$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
				return View('subadmin.ajax.promo_list')->with($data_onview);
			}
		}
	}
	public function promo_action_13_Nov_2017(Request $request)
	{
		$promo_id = Input::get('promo_id');
		if(Input::get('from')=='back'){
			$promo_id = Input::get('promo_id');
			$rest_id =  Input::get('promo_restid');
			$promo_list = DB::table('promotion')
			->where('promo_restid', '=' ,$rest_id)
			->orderBy('promo_id', 'desc')
			->get();
			$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
			return View('subadmin.ajax.promo_list')->with($data_onview);
		}
		else
		{
/*echo '<pre>';
print_r($_POST);
exit;*/
$promo_desc = '';
$promo_mode = '';
$promo_buy = '';
$promo_value = '';
$promo_start = '';
$promo_end = '';
$promo_day = '';
$promo_restid = Input::get('promo_restid');
$promo_for =Input::get('promo_for');
$promo_on = Input::get('promo_on');
if(Input::get('promo_on')=='qty')
{
	$promo_desc = Input::get('promo_desc_qty');
	$promo_mode = Input::get('promo_mode_qty');
	$promo_buy = Input::get('promo_buy_qty');
	$promo_value = Input::get('promo_offer_qty');
	$promo_start = '';
	$promo_end = '';
}
if(Input::get('promo_on')=='total_amt')
{
	$promo_desc = Input::get('promo_desc_cost');
	$promo_mode = Input::get('promo_mode_cost');
	$promo_buy = Input::get('promo_value_cost');
	$promo_value = Input::get('promo_offer_cost');
	$promo_start = '';
	$promo_end = '';
}
if(Input::get('promo_on')=='schedul')
{
	$promo_desc = Input::get('promo_desc_range');
	$promo_mode = Input::get('promo_mode_range');
	$promo_buy = Input::get('promo_buy_range');
	$promo_value = Input::get('promo_offer_range');
	$promo_start = Input::get('promo_start');
	$promo_end = Input::get('promo_end');
	if(isset($_POST['day_name']))
	{
		$promo_day = implode(',',$_POST['day_name']);
	}
}
$promo_status = '';
if($promo_id==0)
{
	$promo = new Promotion;
	$promo->promo_restid = $promo_restid;
	$promo->promo_for = $promo_for;
	$promo->promo_on =$promo_on;
	$promo->promo_desc = $promo_desc;
	$promo->promo_mode = $promo_mode;
	$promo->promo_buy  = $promo_buy ;
	$promo->promo_value	 =  $promo_value;
	$promo->promo_start	 =  $promo_start;
	$promo->promo_end	 =  $promo_end;
	$promo->promo_status =  Input::get('promo_status');
	$promo->promo_day = $promo_day;
	$promo->save();
	$promo_id = $promo->promo_id;
	Session::flash('promo_message', 'Restaurant Promostion Inserted Sucessfully!');
	$rest_id =  Input::get('promo_restid');
	if(Input::get('from')=='submit'){
		$promo_list = DB::table('promotion')
		->where('promo_restid', '=' ,$rest_id)
		->orderBy('promo_id', 'desc')
		->get();
		$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
		return View('subadmin.ajax.promo_list')->with($data_onview);
	}
	elseif(Input::get('from')=='addnext'){
		$id =0;
		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id);
		return View('subadmin.ajax.promo_form')->with($data_onview);
	}
}
else
{
	$promo_id = Input::get('promo_id');
	$rest_id =  Input::get('promo_restid');
	if(Input::get('from')=='update'){
		DB::table('promotion')
		->where('promo_id', $promo_id)
		->update([
			'promo_restid' =>$promo_restid,
			'promo_for' => $promo_for,
			'promo_on' =>$promo_on,
			'promo_desc' => $promo_desc,
			'promo_mode' => $promo_mode,
			'promo_buy' =>$promo_buy,
			'promo_value' => $promo_value,
			'promo_start' =>  $promo_start,
			'promo_end' => $promo_end,
			'promo_day'=> $promo_day,
			'promo_status' =>  Input::get('promo_status')
		]);
		Session::flash('promo_message', 'Restaurant Promostion Update Sucessfully!');
	}
	$promo_list = DB::table('promotion')
	->where('promo_restid', '=' ,$rest_id)
	->orderBy('promo_id', 'desc')
	->get();
	$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
	return View('subadmin.ajax.promo_list')->with($data_onview);
}
}
}
public function promo_action(Request $request)
{
	$promo_id = Input::get('promo_id');
	if(Input::get('from')=='back'){
		$promo_id = Input::get('promo_id');
		$rest_id =  Input::get('promo_restid');
		$promo_list = DB::table('promotion')
		->where('promo_restid', '=' ,$rest_id)
		->orderBy('promo_id', 'desc')
		->get();
		$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
		return View('subadmin.ajax.promo_list')->with($data_onview);
	}
	else
	{
/*echo '<pre>';
print_r($_POST);
exit;*/
$promo_menu_item = '';
$promo_desc = '';
$promo_mode = '';
$promo_buy = '';
$promo_value = '';
$promo_start = '';
$promo_end = '';
$promo_day = '';
$promo_restid = Input::get('promo_restid');
$promo_for =Input::get('promo_for');
$promo_on = Input::get('promo_on');
if(Input::get('promo_on')=='qty')
{
	$promo_desc = Input::get('promo_desc_qty');
	$promo_mode = Input::get('promo_mode_qty');
	$promo_buy = Input::get('promo_buy_qty');
	$promo_value = Input::get('promo_offer_qty');
	$promo_menu_item = Input::get('promo_menu_item');
	$promo_start = '';
	$promo_end = '';
}
if(Input::get('promo_on')=='total_amt')
{
	$promo_desc = Input::get('promo_desc_cost');
	$promo_mode = Input::get('promo_mode_cost');
	$promo_buy = Input::get('promo_value_cost');
	$promo_value = Input::get('promo_offer_cost');
	$promo_start = '';
	$promo_end = '';
}
if(Input::get('promo_on')=='schedul')
{
	$promo_desc = Input::get('promo_desc_range');
	$promo_mode = Input::get('promo_mode_range');
	$promo_buy = Input::get('promo_buy_range');
	$promo_value = Input::get('promo_offer_range');
	$promo_start = Input::get('promo_start');
	$promo_end = Input::get('promo_end');
	if(isset($_POST['day_name']))
	{
		$promo_day = implode(',',$_POST['day_name']);
	}
}
$promo_status = '';
if($promo_id==0)
{
	$promo = new Promotion;
	$promo->promo_restid = $promo_restid;
	$promo->promo_for = $promo_for;
	$promo->promo_on =$promo_on;
	$promo->promo_desc = $promo_desc;
	$promo->promo_mode = $promo_mode;
	$promo->promo_buy  = $promo_buy ;
	$promo->promo_value	 =  $promo_value;
	$promo->promo_start	 =  $promo_start;
	$promo->promo_end	 =  $promo_end;
	$promo->promo_status =  Input::get('promo_status');
	$promo->promo_day = $promo_day;
	$promo->promo_menu_item = $promo_menu_item;
	$promo->save();
	$promo_id = $promo->promo_id;
	Session::flash('promo_message', 'Restaurant Promostion Inserted Sucessfully!');
	$rest_id =  Input::get('promo_restid');
	if(Input::get('from')=='submit'){
		$promo_list = DB::table('promotion')
		->where('promo_restid', '=' ,$rest_id)
		->orderBy('promo_id', 'desc')
		->get();
		$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
		return View('subadmin.ajax.promo_list')->with($data_onview);
	}
	elseif(Input::get('from')=='addnext'){
		$id =0;
		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
		$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id);
		return View('subadmin.ajax.promo_form')->with($data_onview);
	}
}
else
{
	$promo_id = Input::get('promo_id');
	$rest_id =  Input::get('promo_restid');
	if(Input::get('from')=='update'){
		DB::table('promotion')
		->where('promo_id', $promo_id)
		->update([
			'promo_restid' =>$promo_restid,
			'promo_for' => $promo_for,
			'promo_on' =>$promo_on,
			'promo_desc' => $promo_desc,
			'promo_mode' => $promo_mode,
			'promo_buy' =>$promo_buy,
			'promo_value' => $promo_value,
			'promo_start' =>  $promo_start,
			'promo_end' => $promo_end,
			'promo_day'=> $promo_day,
			'promo_menu_item'=>$promo_menu_item,
			'promo_status' =>  Input::get('promo_status')
		]);
		Session::flash('promo_message', 'Restaurant Promostion Update Sucessfully!');
	}
	$promo_list = DB::table('promotion')
	->where('promo_restid', '=' ,$rest_id)
	->orderBy('promo_id', 'desc')
	->get();
	$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list);
	return View('subadmin.ajax.promo_list')->with($data_onview);
}
}
}
/* PROMOSTION TABE END */
/* BANKDETAIL TABE START */
public function ajax_bank_form()
{
	$rest_id =  Input::get('rest_id');
	$id =0;
	$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
	$bank_detail = '';
	if(Input::get('bank_id'))
	{
		$bank_id = Input::get('bank_id');
		$id = Input::get('bank_id');
		$bank_detail  = DB::table('bankdetail')->where('bank_id', '=' ,$bank_id)->get();
	}
	$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'bank_detail'=>$bank_detail,'id'=>$id);
	return View('subadmin.ajax.bank_form')->with($data_onview);
}
public function bank_action(Request $request)
{
	$bank_id = Input::get('bank_id');
	$rest_id = Input::get('bank_restid');
	if(Input::get('from')=='back'){
		$bank_id = Input::get('bank_id');
		$rest_id =  Input::get('bank_restid');
		$bank_list = DB::table('bankdetail')
		->where('bank_restid', '=' ,$rest_id)
		->orderBy('bank_id', 'desc')
		->get();
		$data_onview = array('rest_id' =>$rest_id,'bank_list'=>$bank_list);
		return View('subadmin.ajax.bank_list')->with($data_onview);
	}
	else
	{
		if($bank_id==0)
		{
			$bank_active = DB::table('bankdetail')
			->where('bank_restid', '=' ,$rest_id)
			->where('bank_status', '=' ,'1')
			->orderBy('bank_id', 'desc')
			->get();
			if(!empty($bank_active) && (Input::get('bank_status')=='1'))
			{
				DB::table('bankdetail')->where('bank_status', '1')->update(['bank_status' =>'0']);
			}
			$bank= new Bankdetail;
			$bank->bank_restid = Input::get('bank_restid');
			$bank->bank_uname =Input::get('bank_uname');
			$bank->bank_name = Input::get('bank_name');
			$bank->bank_branch = Input::get('bank_branch');
			$bank->bank_bsb	 =  Input::get('bank_bsb');
			$bank->bank_acc	 =  Input::get('bank_acc');
			$bank->bank_status	 =  Input::get('bank_status');
			$bank->save();
			$bank_id = $bank->bank_id;
			Session::flash('bank_message', 'Restaurant Bank Inserted Sucessfully!');
			$rest_id =  Input::get('bank_restid');
			if(Input::get('from')=='submit'){
				$bank_list = DB::table('bankdetail')
				->where('bank_restid', '=' ,$rest_id)
				->orderBy('bank_id', 'desc')
				->get();
				$data_onview = array('rest_id' =>$rest_id,'bank_list'=>$bank_list);
				return View('subadmin.ajax.bank_list')->with($data_onview);
			}
			elseif(Input::get('from')=='addnext'){
				$id =0;
				$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
				$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id);
				return View('subadmin.ajax.bank_form')->with($data_onview);
			}
		}
		else
		{
			$bank_id = Input::get('bank_id');
			$rest_id =  Input::get('bank_restid');
			if(Input::get('from')=='update'){
				$bank_active = DB::table('bankdetail')
				->where('bank_restid', '=' ,$rest_id)
				->where('bank_status', '=' ,'1')
				->orderBy('bank_id', 'desc')
				->get();
				if(!empty($bank_active) && (Input::get('bank_status')=='1'))
				{
					DB::table('bankdetail')->where('bank_status', '1')->update(['bank_status' =>'0']);
				}
				DB::table('bankdetail')
				->where('bank_id', $bank_id)
				->update([
					'bank_restid' =>Input::get('bank_restid'),
					'bank_uname' =>Input::get('bank_uname'),
					'bank_name' => Input::get('bank_name'),
					'bank_branch' => Input::get('bank_branch'),
					'bank_bsb' =>  Input::get('bank_bsb'),
					'bank_acc' =>  Input::get('bank_acc'),
					'bank_status' => Input::get('bank_status')
				]);
				Session::flash('bank_message', 'Restaurant BankDetails Update Sucessfully!');
			}
			$bank_list = DB::table('bankdetail')
			->where('bank_restid', '=' ,$rest_id)
			->orderBy('bank_id', 'desc')
			->get();
			$data_onview = array('rest_id' =>$rest_id,'bank_list'=>$bank_list);
			return View('subadmin.ajax.bank_list')->with($data_onview);
		}
	}
}
/* BANKDETAIL TABE END */
public function show_suburblist()
{
		//print_r($_REQUEST);
	$return = array();
	$keyword = Input::get('term');
	if(!empty($keyword))
	{
		$i=0;
		/*STATE SEARCH WITH ABBRIVATION*/
		$Suburb_detail  = DB::table('location')
		->where('Suburb_name', 'like' ,'%'.$keyword.'%')
		->get();
		if(!empty($Suburb_detail)){
			foreach($Suburb_detail as $slist)
			{
					//array_push($return,array('label'=>$slist->Suburb_name,'value'=>$slist->Suburb_name));
				array_push($return,array('label'=>$slist->Suburb_name.', '.$slist->pincode.', '.$slist->City_name.', '.$slist->state,'value'=>$slist->Suburb_name,'id'=>$slist->location_id,'pincode'=>$slist->pincode,'city'=>$slist->City_name,'state'=>$slist->state));
				$i++;
			}
		}
		/*STATE SEARCH */
	}
	return json_encode($return);
}
/* SHOW POPULAR LIST START */
function menu_category_popularlist()
{

	$rest_id = Input::get('rest_id');
	$aria_id = Input::get('aria');
	$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();
	$menu_list = DB::table('menu')
	->where('restaurant_id', '=' ,$rest_id)
	->where('area_id', '=' ,$aria_id)
	->orderBy('menu_order', 'asc')
	->get();
	$menu_detail = '';
	$menu_cat_detail = '';
	if(!empty($menu_list)){
		$menu_cat_detail = DB::table('menu_category')
		->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')
		->where('menu_category.rest_id', '=' ,$rest_id)
		->where('menu_category.area_id', '=' ,$aria_id)
		->where('menu_category.menu_cat_popular', '=' ,'1')
		->select('menu_category.*','menu_category_item.*')
		->orderBy('menu_category.menu_category_id', 'asc')
		->get();
	}
	$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>'','menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);
	
	return View('subadmin.ajax.popular_menu_item_list')->with($data_onview);
}
/* POPUPLAR LIST END */
function get_template_action()
{
		/*echo '<pre>';
		print_r($_POST);
		exit;*/
		$template_ids = '';
		$rest_id = Input::get('restaurant_id');
		$tempalte_data = Input::get('tempalte_data');
		$rest_empty = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->where('template_id', '=' ,'')->count();
		if(isset($_POST['tempalte_data']))
		{
			if($rest_empty)
			{
				foreach($tempalte_data as $temp_id)
				{
					$template_ids = $temp_id;
					$menu_list = DB::table('template_menu')
					->where('template_id', '=' ,$temp_id)
					->orderBy('menu_order', 'asc')
					->get();
					if($menu_list)
					{
						foreach($menu_list as $mt_list)
						{
							$menu = new Menu;
							$menu->restaurant_id = $rest_id;
							$menu->template_id = $mt_list->template_id;
							$menu->menu_name =  $mt_list->menu_name;
							$menu->menu_desc =  $mt_list->menu_desc;
							$menu->menu_status = $mt_list->menu_status;
							$menu->menu_order =  $mt_list->menu_order;
							$menu->menu_image = '';
							$menu->save();
							$menu_id = $menu->menu_id;
							$menu_cat_detail = DB::table('template_menu_category')
							->where('template_id', '=' ,$temp_id)
							->where('menu_id', '=' ,$mt_list->menu_id)
							->select('*')
							->orderBy('menu_category_id', 'asc')
							->get();
							if($menu_cat_detail)
							{
								foreach($menu_cat_detail as $mtc_list)
								{
									$menu_category = new  Menu_category ;
									$menu_category->rest_id = $rest_id;
									$menu_category->menu_id =  $menu_id;
									$menu_category->menu_category_name =  $mtc_list->menu_category_name;
									$menu_category->menu_category_desc =  $mtc_list->menu_category_desc;
									$menu_category->menu_cat_popular =  $mtc_list->menu_cat_popular;
									$menu_category->menu_cat_diet =  $mtc_list->menu_cat_diet;
									$menu_category->menu_category_price =  $mtc_list->menu_category_price;
									$menu_category->menu_category_portion = $mtc_list->menu_category_portion;
									$menu_category->menu_cat_status = $mtc_list->menu_cat_status;
									$menu_category->save();
									$menu_category_id = $menu_category->menu_category_id;
									/***************** sub item list *****/
									$menu_cat_item_detail = DB::table('template_category_item')
									->where('menu_category', '=' ,$mtc_list->menu_category_id)
									->where('menu_id', '=' ,$mt_list->menu_id)
									->select('*')
									->orderBy('menu_cat_itm_id', 'asc')
									->get();
									if($menu_cat_item_detail)
									{
										foreach($menu_cat_item_detail as $mtci_list)
										{
											$menu_sub_itme = new  Category_item;
											$menu_sub_itme->rest_id	 = $rest_id;
											$menu_sub_itme->menu_id	 = $menu_id;
											$menu_sub_itme->menu_category	 = $menu_category_id;
											$menu_sub_itme->menu_item_title	 = $mtci_list->menu_item_title;
											$menu_sub_itme->menu_item_price	 = $mtci_list->menu_item_price;
											$menu_sub_itme->menu_item_status = $mtci_list->menu_item_status;
											$menu_sub_itme->menu_cat_itm_price = $mtci_list->menu_cat_itm_price;
											$menu_sub_itme->save();
										}
									}
									/************* addon list   *****/
									$addon_list = DB::table('template_menu_category_addon')
									->where('addon_templateid', '=' ,$temp_id)
									->where('addon_menuid', '=' ,$mt_list->menu_id)
									->where('addon_menucatid', '=' ,$mtc_list->menu_category_id)
									->orderBy('addon_id', 'asc')
									->get();
									if($addon_list)
									{
										foreach($addon_list as $mtca_list)
										{
											$addon = new Menu_category_addon;
											$addon->addon_restid = $rest_id;
											$addon->addon_menuid = $menu_id;
											$addon->addon_menucatid = $menu_category_id;
											$addon->addon_name = trim($mtca_list->addon_name);
											$addon->addon_price =  $mtca_list->addon_price;
											$addon->addon_status =  $mtca_list->addon_status;
											$addon->addon_groupname = strtoupper(trim($mtca_list->addon_groupname));
											$addon->addon_option =  $mtca_list->addon_option;
											$addon->addon_default =  $mtca_list->addon_default;
											$addon->save();
										}
									}
								}
							}
						}
					}
				}
				DB::table('restaurant')->where('rest_id', $rest_id)->update(['template_id' =>$template_ids]);
			}
		}
		return redirect()->to('/sub-admin/restaurant_view/'.$rest_id);
	}
}
