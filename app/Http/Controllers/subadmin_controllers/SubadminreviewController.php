<?php
namespace App\Http\Controllers\subadmin_controllers;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Route;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Restaurant;
use App\Menu;
use App\Menu_category;
use App\Menu_category_item;
use App\Menu_category_addon;
use App\Service;
use App\Bankdetail;
use App\Promotion;
class SubadminreviewController extends Controller
{
	public function __construct(){
		$check = session()->get('user_id');
		if(count($check)!=1){
			Redirect::to('/sub-admin')->send();
		}
		//$this->middleware('admin');
	}
	public function show_reviewlist()
	{
		//echo '<pre>';
	/*	$review_detail  =  DB::table('review')
						->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id')
						->leftJoin('users', 'review.re_userid', '=', 'users.id')
						->leftJoin('order', 'review.re_orderid', '=', 'order.order_id')
					//	->where('review.re_status', '=' ,'SUBMIT')
						->select('*')
						->orderBy('review.re_id', 'asc')
						->get();*/
						$review_detail  =  DB::table('review')
						->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id')
						->leftJoin('users', 'review.re_userid', '=', 'users.id')
						->leftJoin('order', 'review.re_orderid', '=', 'order.order_id')
						//->where('review.re_status', '=' ,'SUBMIT')
						->where('review.re_orderid', '>' ,'0')
						->select('*')
						->orderBy('review.re_id', 'asc')
						->get();
		//print_r($order_detail);
						$data_onview = array('review_detail'=>$review_detail);
						return View('subadmin.review_list')->with($data_onview);
					}
					public function show_review_action()
					{
		//echo '<pre>';
		//print_r($_POST);
						$reason = '';
						if(Input::get('reason') && (!empty(Input::get('reason'))) && (Input::get('revew_status')=='REJECT'))
						{
							$reason =Input::get('reason');
						}
						DB::table('review')
						->where('re_id', Input::get('re_id'))
						->update(['re_status' => Input::get('revew_status'),
							're_rejectreson'=>  $reason,
							're_rejectdate'=>  date('Y-m-d')
						]);
						Session::flash('message', 'Review Updated Successfully!');
						return redirect()->to('/sub-admin/review_list');
					}
					public function ajax_search_list()
					{
		//echo '<pre>';
		//print_r($_POST);
						$from_date=Input::get('fromdate');;
						$to_date=Input::get('todate');;
						$status = trim(Input::get('order_status'));
						$user_id = Input::get('user_id');
						$review_detail  = DB::table('review');
						$review_detail  = $review_detail->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id');
						$review_detail  = $review_detail->leftJoin('users', 'review.re_userid', '=', 'users.id');
						$review_detail  = $review_detail->leftJoin('order', 'review.re_orderid', '=', 'order.order_id');
						$review_detail  = $review_detail->where('review.re_orderid', '>' ,'0');
						if(!empty($status) && ($status!='All'))
						{
							$review_detail = $review_detail->where('review.re_status', '=', $status);
						}
						/* Get Data between Date */
						if(!empty($from_date) && ($from_date!='0000-00-00'))
						{
							$review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from_date)));
						}
						if(!empty($to_date) && ($to_date!='0000-00-00'))
						{
							$review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to_date)));
						}
						/* End */
						$review_detail = $review_detail->select('*');
						$review_detail = $review_detail->orderBy('review.re_id', 'asc');
						$review_detail = $review_detail->get();
						$data_onview = array('review_detail'=>$review_detail);
						return View('subadmin.ajax.review_list')->with($data_onview);
		//print_r($data_onview);
					}
					function review_delete($id)
					{
						DB::table('review')->where('re_id', '=', $id)->delete();
						Session::flash('message', 'Information Deleted Sucessfully!');
						return Redirect('/sub-admin/review_list');
					}
				}
