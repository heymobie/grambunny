<?php



namespace App\Http\Controllers\subadmin_controllers;





Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use App\Vendor;

use Route;

use Illuminate\Http\Request;

use Mail;



class SubadminVendorController extends Controller

{

	public function __construct(){

		//$this->middleware('admin');

		$check = session()->get('user_id');

		if(count($check)!=1){

			Redirect::to('/sub-admin')->send();

		}

	}





	public function vendor_list()

	{

		DB::connection()->enableQueryLog();



		$vendor_list = DB::table('vendor')

		->select('*')

		->orderBy('vendor_id', 'desc')

		->get();





		$data_onview = array('vendor_list' =>$vendor_list);



		return View('subadmin.vendor_list')->with($data_onview);

	}







	public function vendor_search()

	{



		$vendor_list = DB::table('vendor');



		if( (Input::get('vendor_cont')) && (!empty(Input::get('vendor_cont'))))

		{



			$vendor_list = $vendor_list->where('vendor.mob_no', 'like' ,Input::get('vendor_cont'));

		}



		if( (Input::get('vendor_email')) && (!empty(Input::get('vendor_email'))))

		{

			$vendor_list = $vendor_list->where('vendor.email', 'like' ,Input::get('vendor_email'));

		}



		$vendor_list = $vendor_list->select('vendor.*');

		$vendor_list = $vendor_list->orderBy('vendor.vendor_id', 'desc');

		$vendor_list = $vendor_list->get();



		$data_onview = array('vendor_list' =>$vendor_list);

		return View('subadmin.ajax.vendor_list')->with($data_onview);

	}





	public function vendor_form()

	{



		if(Route::current()->getParameter('id'))

		{

			$id = Route::current()->getParameter('id');



			$user_detail  = DB::table('vendor')->where('vendor_id', '=' ,$id)->get();



			$data_onview = array('user_detail' =>$user_detail,

				'id'=>$id);



			return View('subadmin.vendor_form')->with($data_onview);



		}

		else

		{

			$id =0;

			$data_onview = array('id'=>$id);

			return View('subadmin.vendor_form')->with($data_onview);

		}

	}

	public function vendor_action(Request $request)

	{

		/*echo '<pre>';

		print_r($_POST);

		exit;*/







		$vendor_id = Input::get('vendor_id');



		if($vendor_id==0)

		{



			$input['email'] = Input::get('email');



			// Must not already exist in the `email` column of `users` table

			$rules = array('email' => 'unique:vendor,email');



			$validator = Validator::make($input, $rules);

			$msg = 'Duplicate Email';



			if ($validator->fails()) {

				return Redirect::away('/sub-admin/vendorlist')->with('msg');

			}

			else {



				$vendor = new Vendor;

				$vendor->name = Input::get('name');

				$vendor->last_name = Input::get('last_name');

				$vendor->email = Input::get('email');

				$vendor->mob_no = Input::get('mob_no');

				$vendor->address = Input::get('address');

				$vendor->address1 = Input::get('address1');

				$vendor->suburb = Input::get('suburb');

				$vendor->state = Input::get('state');

				$vendor->zipcode = Input::get('zipcode');

				$vendor->vendor_status=  Input::get('vendor_status');

				$vendor->password =  Hash::make(trim(Input::get('password')));

				$vendor->save();

				$vendor_id = $vendor->vendor_id;



				Session::flash('message', 'Vendor Inserted Sucessfully!');

				//return redirect()->to('/admin/vendorlist');









				/******************* SEND EMAIL AFTER PAYMENT  *******************/





				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'1')

				->get();







				$data = array(



					'vendor_name'=>Input::get('name').' '.Input::get('last_name'),

					'vendor_email'=>Input::get('email'),

					'email_content'=>$email_content_detail[0]->email_content



				);

				Mail::send('emails.registration_email', $data, function ($message) use ($data) {







            //$m->from('hello@app.com', 'Your Application');

           // $m->to($user->email, $user->name)->subject('Your Reminder!');



					$message->from('info@grambunny.com', 'grambunny');

					$message->to($data['vendor_email']);

					$message->bcc('votiveshweta@gmail.com');

					$message->bcc('votivemobile.pankaj@gmail.com');

					$message->subject('Registration on grubpoint.com');

				});



				/******************* SEND EMAIL AFTER PAYMENT  *******************/







				return redirect()->to('/sub-admin/vendor_profile/'.$vendor_id);

			}

		}

		else

		{





			/*** EMAIL ***/

			$vendor_detail  = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();



			$old_status =	$vendor_detail[0]->vendor_status;

			if( ($old_status==0) && (Input::get('vendor_status')==1))

			{



				/******************* SEND EMAIL AFTER PAYMENT  *******************/





			/*$email_content_detail = DB::table('email_content')

						->select('*')

						->where('email_id', '=' ,'1')

						->get();*/



						$owner_link	 = 'https://www.grambunny.com/vendor';

						$email_content	= 'Your accoutn is activted by admin. You can login in ser section manage restaurant.';



						$data = array(



							'vendor_name'=>Input::get('name').' '.Input::get('last_name'),

							'vendor_email'=>Input::get('email'),

							'email_content'=>$email_content,

							'owner_link'=>$owner_link



						);

						Mail::send('emails.vendoractive_email', $data, function ($message) use ($data) {







            //$m->from('hello@app.com', 'Your Application');

           // $m->to($user->email, $user->name)->subject('Your Reminder!');



							$message->from('info@grambunny.com', 'grambunny');

							$message->to($data['vendor_email']);

							$message->subject('Owner Account acctive by grubpoint.com');

						});



						/******************* SEND EMAIL AFTER PAYMENT  *******************/





					}







					/*** EMAIL end***/







					DB::table('vendor')

					->where('vendor_id', $vendor_id)

					->update(['name' => Input::get('name'),

						'last_name'=>	 Input::get('last_name'),

						'mob_no'=>  Input::get('mob_no'),

						'address'=>  Input::get('address'),

						'address1'=>  Input::get('address1'),

						'suburb'=>  Input::get('suburb'),

						'state'=>  Input::get('state'),

						'zipcode'=>  Input::get('zipcode'),

						'vendor_status'=>  Input::get('vendor_status')

					]);



					Session::flash('message', 'Vendor Information Updated Sucessfully!');

				//Session::flash('new', 'User Inserted Sucessfully!');

				//return redirect()->to('/admin/vendorlist');

					return redirect()->to('/sub-admin/vendor_profile/'.$vendor_id);

				}



			}

			public function vendor_duplicate_email()

			{

		// Get the value from the form

				$input['email'] = Input::get('email');



		// Must not already exist in the `email` column of `users` table

				$rules = array('email' => 'unique:vendor,email');



				$validator = Validator::make($input, $rules);



				if ($validator->fails()) {

					echo '1';

				}

				else {

					echo '2';

				}



			}



			public function vendor_delete($id)

			{





		/*echo $id;

		echo '<pre>';

		print_r($_POST);

		exit;*/



		DB::table('vendor')->where('vendor_id', '=', $id)->delete();

		Session::flash('message', 'Vendor Information Deleted Sucessfully!');

		return Redirect('/sub-admin/vendorlist');





	}



	public function view_vendor()

	{



		$vendor_id =	Route::current()->getParameter('id');

		$vendor_detail  = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();

		$rest_list  = DB::table('restaurant')->where('vendor_id', '=' ,$vendor_id)->get();

		$transport_list  = DB::table('transport')->where('transport_vendor', '=' ,$vendor_id)->get();



		$data_onview = array('vendor_detail' =>$vendor_detail,

			'rest_list'=>$rest_list,

			'transport_list'=>$transport_list,

			'rest_list'=>$rest_list,

			'id'=>$vendor_id);



		return View('subadmin.vendor_view')->with($data_onview);



	}









}

