<?php
namespace App\Http\Controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Route;
use Illuminate\Support\Facades\Input;

class AdmindeliverymanController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}

	/*Delivery man list*/
	public function user_list()
	{
		DB::connection()->enableQueryLog();
		$user_list = DB::table('deliveryman')
		->select('*')
		->orderBy('id', 'desc')
		->get();
		$data_onview = array('user_list' =>$user_list);
		return View('admin.deliverman_list')->with($data_onview);
	}

	/*Add delivery man form*/
	public function user_form(Request $request)
	{
		if(Route::current()->getParameter('id'))
		{
			$id = Route::current()->getParameter('id');
			$user_detail  = DB::table('deliveryman')->where('id', '=' ,$id)->get();
			$user_list  = DB::table('deliveryman')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();
			$data_onview = array('user_detail' =>$user_detail,
				'id'=>$id,
				'user_list'=>$user_list);
			return View('admin.deliveryman_form')->with($data_onview);
		}
		else
		{
			$id =0;
			$user_list  = DB::table('deliveryman')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();
			$data_onview = array('id'=>$id,
				'user_list'=>$user_list);
			return View('admin.deliveryman_form')->with($data_onview);
		}

	}

	/*Delivery man add action*/
	public function user_action(Request $request)
	{

		$user_id = Input::get('user_id');
		$carrier_id = '';
		$carrier_name = '';
		$carrier_email = '';
		$carrier_id =  Input::get('user_mob_type');
		switch($carrier_id)
		{
			case 1:
			$carrier_id = '1';
			$carrier_name = 'Alltel';
			$carrier_email = '@message.alltel.com';
			break;
			case 2:
			$carrier_id = '2';
			$carrier_name = 'AT&T';
			$carrier_email = '@txt.att.net';
			break;
			case 3:
			$carrier_id = '3';
			$carrier_name = 'Boost Mobile';
			$carrier_email = '@myboostmobile.com';
			break;
			case 4:
			$carrier_id = '4';
			$carrier_name = 'Sprint';
			$carrier_email = '@messaging.sprintpcs.com';
			break;
			case 5:
			$carrier_id = '5';
			$carrier_name = 'T-Mobile';
			$carrier_email = '@tmomail.net';
			break;
			case 6:
			$carrier_id = '6';
			$carrier_name = 'U.S. Cellular';
			$carrier_email = '@email.uscc.net';
			break;
			case 7:
			$carrier_id = '7';
			$carrier_name = 'Verizon';
			$carrier_email = '@vtext.com';
			break;
			case 8:
			$carrier_id = '8';
			$carrier_name = 'Virgin Mobile';
			$carrier_email = '@vmobl.com';
			break;
			case 9:
			$carrier_id = '9';
			$carrier_name = 'Republic Wireless';
			$carrier_email = '@text.republicwireless.com';
			break;
		}
		if($user_id==0)
		{
			$input['email'] = Input::get('email');
			// Must not already exist in the `email` column of `users` table
			$rules = array('email' => 'unique:deliveryman,email');
			$validator = Validator::make($input, $rules);
			$msg = 'Duplicate Email';
			if ($validator->fails()) {
				return Redirect::away('member/property_listing')->with('msg');
			}
			else {
				$carrier_id = '';
				$carrier_name = '';
				$carrier_email = '';
				$carrier_id =  Input::get('user_mob_type');
				switch($carrier_id)
				{
					case 1:
					$carrier_id = '1';
					$carrier_name = 'Alltel';
					$carrier_email = '@message.alltel.com';
					break;
					case 2:
					$carrier_id = '2';
					$carrier_name = 'AT&T';
					$carrier_email = '@txt.att.net';
					break;
					case 3:
					$carrier_id = '3';
					$carrier_name = 'Boost Mobile';
					$carrier_email = '@myboostmobile.com';
					break;
					case 4:
					$carrier_id = '4';
					$carrier_name = 'Sprint';
					$carrier_email = '@messaging.sprintpcs.com';
					break;
					case 5:
					$carrier_id = '5';
					$carrier_name = 'T-Mobile';
					$carrier_email = '@tmomail.net';
					break;
					case 6:
					$carrier_id = '6';
					$carrier_name = 'U.S. Cellular';
					$carrier_email = '@email.uscc.net';
					break;
					case 7:
					$carrier_id = '7';
					$carrier_name = 'Verizon';
					$carrier_email = '@vtext.com';
					break;
					case 8:
					$carrier_id = '8';
					$carrier_name = 'Virgin Mobile';
					$carrier_email = '@vmobl.com';
					break;
					case 9:
					$carrier_id = '8';
					$carrier_name = 'Republic Wireless';
					$carrier_email = '@text.republicwireless.com';
					break;
				}



				$userData = array(
					'name' => trim(Input::get('name')),
					'lname' => trim(Input::get('lname')),
					'email' => trim(Input::get('email')),
					'password' =>  Hash::make(trim(Input::get('password'))),
					'user_address' => trim(Input::get('user_address')),
					'user_city' => trim(Input::get('user_city')),
					'user_zipcode' => trim(Input::get('user_zipcode')),
					'user_status' => trim(Input::get('user_status')),
					'order_income' => trim(Input::get('order_income')),
					'user_pass'=> md5(Input::get('password')),
					'user_mob' => Input::get('user_mob'),
					'created_at'=>date('y-m-d h:i:s')
				);
				$affected = DB::table('deliveryman')->insert($userData);

				//$user_id = $user->id;
				Session::flash('message', 'Delivery Man Created Sucessfully!');
				/******************* SEND EMAIL AFTER PAYMENT  *******************/
				// $email_content_detail = DB::table('email_content')
				// ->select('*')
				// ->where('email_id', '=' ,'1')
				// ->get();
				// $data = array(
				// 	'vendor_name'=>Input::get('name').' '.Input::get('last_name'),
				// 	'vendor_email'=>Input::get('email'),
				// 	'email_content'=>$email_content_detail[0]->email_content
				// );

				// Mail::send('emails.registration_email', $data, function ($message) use ($data) {
				// 	$message->from('info@otlubli.com', 'otlubli');
				// 	$message->to($data['vendor_email']);
				// 	$message->bcc('votiveshweta@gmail.com');
				// 	$message->bcc('votivemobile.pankaj@gmail.com');
				// 	$message->subject('Registration on otlubli.com');
				// });
				/******************* SEND EMAIL AFTER PAYMENT  *******************/
				return redirect()->to('/admin/delivery_manlist');
			}
		}else{

			$UpdateData = Request::all();

			if(!empty($UpdateData['updtpassword'])){

				DB::table('deliveryman')
				->where('id', $user_id)
				->update(['name' => trim(Input::get('name')),
					'lname'=>  trim(Input::get('lname')),
					'user_address'=>trim(Input::get('user_address')),
					'user_city'=>trim(Input::get('user_city')),
					'user_status'=>trim(Input::get('user_status')),
					'order_income' => trim(Input::get('order_income')),
					'user_zipcode'=>trim(Input::get('user_zipcode')),
					'password' =>  Hash::make(trim(Input::get('updtpassword'))),
					'user_pass'=> md5(Input::get('updtpassword')),
					'user_mob'=>trim(Input::get('user_mob')),
				]);
			}else{
				DB::table('deliveryman')
				->where('id', $user_id)
				->update(['name' => trim(Input::get('name')),
					'lname'=>  trim(Input::get('lname')),
					'user_address'=>trim(Input::get('user_address')),
					'user_city'=>trim(Input::get('user_city')),
					'user_status'=>trim(Input::get('user_status')),
					'order_income' => trim(Input::get('order_income')),
					'user_zipcode'=>trim(Input::get('user_zipcode')),
					'user_mob'=>trim(Input::get('user_mob')),
				]);
			}

			Session::flash('message', 'Detail Updated Sucessfully!');
				//Session::flash('new', 'User Inserted Sucessfully!');
			return redirect()->to('/admin/delivery_manlist');
		}
	}





	/*Delete sub-admin*/
	public function Deliveryman_delete($id)
	{
		DB::table('deliveryman')->where('id', '=', $id)->delete();
		Session::flash('message', 'Delivery man deleted sucessfully!');
		return redirect()->to('/admin/delivery_manlist');

	}




	/*Check duplicate delivery man*/
	public function deliveryman_duplicate_email()
	{


				// Get the value from the form
		$input['email'] = Input::get('email');
		$input1['user_mob'] = Input::get('user_mob');
				// Must not already exist in the `email` column of `users` table
		$rules = array('email' => 'unique:deliveryman,email');
		$validator = Validator::make($input, $rules);
		$rules1 = array('user_mob' => 'unique:deliveryman,user_mob');
		$validator1 = Validator::make($input1, $rules1);
		if(($validator->fails()) && ($validator1->fails())) {
			echo '1';
		}
		elseif($validator->fails()) {
			echo '2';
		}elseif($validator1->fails()) {
			echo '3';
		}
		else {
			echo '4';
		}

	}

	/*View Delivery man details*/
	public function ViewDeliveryman()
	{
		$Deliveryman_id =	Route::current()->getParameter('id');
		$Deliverymandetail  = DB::table('deliveryman')->where('id', '=' ,$Deliveryman_id)->get();
		$data_onview = array('Deliverymandetail' =>$Deliverymandetail);
		return View('admin.deliveryman_view')->with($data_onview);

	}


}