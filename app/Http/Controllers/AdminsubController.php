<?php



namespace App\Http\Controllers;



use Request;



Use DB;



use Hash;



use Session;



use App\Http\Requests;



use Illuminate\Support\Facades\Auth;



use Redirect;



use Illuminate\Support\Facades\Validator;



use Route;



use Illuminate\Support\Facades\Input;







class AdminsubController extends Controller



{



	public function __construct(){



		$this->middleware('admin');



	}







	/*Sub-admin list*/



	public function user_list()



	{



		DB::connection()->enableQueryLog();



		$user_list = DB::table('admins')



		->select('*')



		->where('role','=',2)



		->orderBy('id', 'desc')



		->get();



		$data_onview = array('user_list' =>$user_list);



		return View('admin.sub-adminuser_list')->with($data_onview);



	}







	/*Add sub-admin form*/



	public function user_form(Request $request)



	{



		return View('admin.sub-admin_form');



	}







	/*Add -subadmin action*/



	public function subAdmin_form(Request $request)



	{



		/*



		$rules = array(



			'name' => 'required|Alpha',



			'lname' => 'required|Alpha',



			'email' => 'required|unique:sub-admins',



			'password' => 'required|Max:6|Min:6',



			'address' => 'required',



			'city' => 'required',



			'zipcode' => 'required|Numeric',



			'status' => 'required',



			'mob' => 'required|Numeric|Max:10|unique:sub-admins',



		);



		$params = Request::all();



		$validator = Validator::make($params, $rules);







		if($validator->passes())



		{



			*/



			$postData = Request::all();



			$SubAdminData = array(



				'name' => trim($postData['name']),



				'lname' => trim($postData['lname']),



				'email' =>trim($postData['email']),



				'password' =>  Hash::make(trim($postData['password'])),



				'postal_address' => trim($postData['address']),



				'city' => trim($postData['city']),



				'zipcoad' => trim($postData['zipcode']),



				'status' => trim($postData['status']),



				'mobile_no' => $postData['mob'],



				'role'=>2,



				'created_at'=>date('y-m-d h:i:s')



			);



			$affected = DB::table('admins')->insert($SubAdminData);



			if($affected){



				/******************* SEND EMAIL AFTER PAYMENT  *******************/



					/*



					$email_content_detail = DB::table('email_content')



					->select('*')



					->where('email_id', '=' ,'1')



					->get();



					$data = array(



					'vendor_name'=>Input::get('name').' '.Input::get('last_name'),



					'vendor_email'=>Input::get('email'),



					'email_content'=>$email_content_detail[0]->email_content



					);



					Mail::send('emails.registration_email', $data, function ($message) use ($data) {



						$message->from('info@grambunny.com', 'www.grambunny.com');



						$message->to($data['vendor_email']);



						$message->subject('Registration on www.grambunny.com as sub-admin.');



					}); */



					/******************* SEND EMAIL AFTER PAYMENT  *******************/



					Session::flash('message', 'Sub-Admin Inserted Sucessfully!');



					return redirect()->to('/admin/subadmin_list');



				}



				/*



			}else{



				return redirect()->back()->withErrors($validator)->withInput();



			}



			*/



		}







		/*Delete sub-admin*/



		public function subadmin_delete($id)



		{



			DB::table('admins')->where('id', '=', $id)->delete();



			Session::flash('message', 'Sub-admin deleted sucessfully!');



			return redirect()->to('/admin/subadmin_list');







		}







		/*Edit sub-admin*/



		public function EditsubadminForm()



		{



			if(Route::current()->getParameter('id'))



			{



				$id = Route::current()->getParameter('id');



				$user_detail  = DB::table('admins')->where('id', '=' ,$id)->get();



				$data_onview = array(



					'user_detail' =>$user_detail,



					'id'=>$id,



				);



				return View('admin.sub-adminEdit_form')->with($data_onview);



			}











		}







		/*Update sub-admin details*/



		public function UpdateSubAdminDetail(Request $request)



		{



			$UpdateData = Request::all();



			$subadmin_id = $UpdateData['subadmin_id'];



			if(!empty($UpdateData['password'])){



				$UpdateSubAdminData = array(



					'name' => trim($UpdateData['name']),



					'lname' => trim($UpdateData['lname']),



					'email' =>trim($UpdateData['email']),



					'password' =>  Hash::make(trim($UpdateData['password'])),



					'postal_address' => trim($UpdateData['address']),



					'city' => trim($UpdateData['city']),



					'zipcoad' => trim($UpdateData['zipcode']),



					'status' => trim($UpdateData['status']),



					'mobile_no' => $UpdateData['mob'],



					'updated_at'=>date('y-m-d h:i:s')



				);



			}else{



				$UpdateSubAdminData = array(



					'name' => trim($UpdateData['name']),



					'lname' => trim($UpdateData['lname']),



					'email' =>trim($UpdateData['email']),



				//'password' =>  Hash::make(trim($UpdateData['password'])),



					'postal_address' => trim($UpdateData['address']),



					'city' => trim($UpdateData['city']),



					'zipcoad' => trim($UpdateData['zipcode']),



					'status' => trim($UpdateData['status']),



					'mobile_no' => $UpdateData['mob'],



					'updated_at'=>date('y-m-d h:i:s')



				);



			}







			$affected = DB::table('admins')



			->where('id', $subadmin_id)



			->update($UpdateSubAdminData);



			if($affected){



				Session::flash('message', 'Sub-Admin details updated sucessfully!');



				return redirect()->to('/admin/subadmin_list');



			}



		}







		public function sub_adminduplicate_email()



		{



			//echo "test";







		// Get the value from the form



			$input['email'] = Input::get('email');



		// Must not already exist in the `email` column of `users` table



			$rules = array('email' => 'unique:admins,email');



			$validator = Validator::make($input, $rules);



			if ($validator->fails()) {



				echo '1';



			}



			else {



				echo '2';



			}



		}







		/*View sub-admin details*/



		public function ViewSubadmin()



		{



			$Subadmin_id =	Route::current()->getParameter('id');



			$Subadmindetail  = DB::table('admins')->where('id', '=' ,$Subadmin_id)->get();



			$data_onview = array('Subadmindetail' =>$Subadmindetail);



			return View('admin.sub-admin_view')->with($data_onview);







		}











	}