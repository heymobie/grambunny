<?php

namespace App\Http\Controllers;



use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Auth;



use Route;

use Session;

use Redirect;

use Hash;

Use DB;

//use Auth;

//use Session; 

use Mail;

use Braintree_Transaction;



use App\Transport_review;

use App\Restaurant; 

use App\User;

use App\Device_token;

use App\User_otpdetail; 

use App\Temp_cart;

use App\Favourite_restaurant;

use App\Favourite_food;

use App\Delivery_address;

use App\Review;

use App\Order;

use App\Order_payment;

use App\Notification_list;



class TestReportController extends Controller

{

	public function index()

	{

		$order_rest  =  DB::table('order')		

						    ->select('order.rest_id')

						    ->where('order.order_create', '=', date('Y-m-d'))

						     ->where('order.is_report', '=', '0')

  						    ->groupBy('order.rest_id')

						    ->get();

					    

		if(!empty($order_rest))

		{

			foreach ($order_rest as $key1 => $arr) 

			{

				$restaurantInfo  =  DB::table('restaurant')->where('rest_id', '=', $arr->rest_id)->first();



				$t=date('d-m-Y');

				$day = date("D",strtotime($t));



				if(!empty($restaurantInfo))

				{

					if ($day == 'Mon') {

						$restTime = $restaurantInfo->rest_mon;

					} elseif($day == 'Tue') {

						$restTime = $restaurantInfo->rest_tues;

					} elseif($day == 'Wed') {

						$restTime = $restaurantInfo->rest_wed;

					} elseif($day == 'Thu') {

						$restTime = $restaurantInfo->rest_thus;

					} elseif($day == 'Fri') {

						$restTime = $restaurantInfo->rest_fri;

					} elseif($day == 'Sat') {

						$restTime = $restaurantInfo->rest_sat;

					} elseif($day == 'Sun') {

						$restTime = $restaurantInfo->rest_sun;

					}



					$rest_time = explode('_', $restTime);

					$edate = $rest_time[1];

					

					$time = strtotime($rest_time[1]);

					$time = $time - (2 * 60);

					$sdate = date("H:i A", $time);



					if($sdate <= date('H:i A') && $edate >= date('H:i A'))

					{



					}



					$order_detail  =  DB::table('order')		

									    ->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

									    ->leftJoin('users', 'order.user_id', '=', 'users.id')

									    ->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid')

			  						    ->where('order.rest_id', '=', $arr->rest_id)

			  						    ->where('order.order_create', '=', date('Y-m-d'))

			  						    ->select('order.*','restaurant.rest_name', 'restaurant.rest_address', 'restaurant.rest_address2', 'restaurant.rest_suburb', 'restaurant.rest_state', 'restaurant.rest_zip_code', 'restaurant.rest_contact','order_payment.pay_tx','order_payment.pay_status','restaurant.rest_city','restaurant.rest_mon','restaurant.rest_tues','restaurant.rest_wed','restaurant.rest_thus','restaurant.rest_fri','restaurant.rest_sat','restaurant.rest_sun')

									    ->orderBy('order.order_create', 'desc')

									    ->get();



					//echo view('daily_report')->with('order_detail',$order_detail);	

					$orderdetail['order_detail'] = $order_detail;				    

					Mail::send('daily_report', $orderdetail, function ($message) {

					    $message->from('info@grambunny.com', 'grambunny');

					    //$message->to($restaurantInfo->rest_email);	

					    $message->to('votivephp.akash@gmail.com');	

					    //$message->to($data['vendor_email']);	

			   			$message->bcc('votivephp.raveena@gmail.com');

			   			//$message->bcc('votivemobile.pankaj@gmail.com');

						$message->subject('Daily Report');	

					});	 



					if (Mail::failures()) {

				        // return response showing failed emails

				        echo "not send";

				    } else {

				    	echo "send";

				    	foreach ($order_detail as $key2 => $val2) 

				    	{

				    		DB::table('order')->where('order_id', '=' ,$val2->order_id)->update(['is_report'=>'1']);

				    	}

				    	

				    }

				}

				



			}

		}						    



	}

}



?>