<?php
/**
 *Managing Vendor auth and registration
 * 
 */
namespace App\Http\Controllers\Vendor;
use App\Http\Controllers\Controller;
use App\Vendor;
use App\Proservicescate;
use App\Productservice;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

use App\Video;
use File;

use Storage;
	
class VendorAuthController extends Controller
{
	/**
	 * @return view
	 */
	public function registrationForm(Request $request,$step)
	{
		if($step=="get-started"){
			return view("auth.Vendor.RegistrationProcess.first");
		}
		elseif($step=="basic-info" AND session()->has("credentials")){
				//return session()->get("credentials");
				return view("auth.Vendor.RegistrationProcess.second");
		}
		elseif($step=="bussiness-details" AND session()->has("basic-info")){
			//	return session()->get("basic-info");
			
			return view("auth.Vendor.RegistrationProcess.third");

		}elseif($step=="email-verify"){
			//	return session()->get("basic-info");
			
			return view("auth.Vendor.RegistrationProcess.emailverify");
		}
		else{

			return view("auth.Vendor.RegistrationProcess.first");
		}
	}

 
	public function firstStep(Request $request)
	{
		$this->validate($request,[
			"email" => "required|email|unique:vendor",
			"password"	=> "required_with:confimr_password|same:confimr_password"
		]);	

		$useremail  = DB::table('users')->where('email', '=' , $request->email)->first();

    if(!empty($useremail)){
    
    return redirect()->back()->withErrors(["email" => "The email has already been taken."])->withInput();

    }

		$data=encrypt($request->email."`^|^|`".$request->password);
		Mail::send('auth.emails.vendor-email-verification',["data" => $data], function ($message) use ($request) {
			$message->from(config("app.webmail"), config("app.mailname"));
		    $message->to($request->email);
		    $message->subject('Email Verification');
		});

		return redirect()->route("vendorRegistration",["step" => "email-verify"]);
		
		//return redirect()->back()->with("success","A verification link has been sent to your email account. Please verify your email to continue.");
	}

	public function verifyVendorEmail(Request $request)
	{
		try {
			$data=decrypt($request->data);
			$credentials=explode("`^|^|`",$data);
			$checkIfRegister=Vendor::where("email",$credentials[0])->exists();
			if ($checkIfRegister) {
				return "You're already verified and register";
			}
		} 
		catch (DecryptException $e) {
			return $e;	
		}

		session(['credentials' => ["email" => $credentials[0],"password" => $credentials[1]]]);
		return redirect()->route("vendorRegistration",["step" => "basic-info"]);
	}



	public function secondStep(Request $request)
	{
		$this->validate($request,[
			"firstname"	=> "required",
			"lastname"	=> "required",
			"username"	=> 'required|string|max:255|regex:/^[A-Za-z0-9]+(?:[_][A-Za-z0-9]+)*$/u|unique:vendor',
			//"zipcode"	=> "required",
			//"mail_address" => "required",
			//"city"	=> "required",
			//"state" => "required",
		],[
			"username.regex" => "username should not contain spaces,hyphens and special char." 
		]); 	

		$data=array_except($request->all(),["_token"]);
		$step1Data=session()->get("credentials");
		$vendor= array_merge($step1Data,$data);
		session(['basic-info' => $vendor]);
		return redirect()->route("vendorRegistration",["step" => "bussiness-details"]);
	}

	public function crop_image_save(Request $request)
	{

		$image = $request->profile;

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $profile_img1= '1'.time().'.png';
        $path = public_path('uploads/vendor/profile/'.$profile_img1);
        file_put_contents($path, $image);		

        return response()->json(['status'=>$profile_img1]);

	}	

	public function thirdStep(Request $request)
	{
		$uniqueid = $this->random_string(12); 
		//return $request->all();
		//return session()->get("vendor.data");
		//return dd($request->all());

		$this->validate($request,[
			"market_area"	=> "required",
			//"service_radius"	=> "required",
			//"driver_license"	=> "required",
			//"license_expiry"	=> "required",
			"license_front"		=> "required|mimes:jpeg,bmp,png",
			"license_back"		=> "required|mimes:jpeg,bmp,png",
			//"ssn"				=> "required",
			//"dob"				=> "required",
			//"description"		=> "required",
			//"make"				=> "required",
			//"model"				=> "required",
			//"year"				=> "required",
			//"color"				=> "required",
			//"licenseplate"		=> "required",
			//"type"				=> "required|in:0,1",
			//"category"    		=> "required"
		]);
        
		$url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$request->market_area."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");
		$response = file_get_contents($url);
		$data = json_decode($response);
		//$geo=$data->results[0]->geometry->viewport->northeast;
		$geo=$data->results[0]->geometry->location;

		$sub_category = $request->subcategory;

		$subcategory = json_encode($sub_category);

		$profile_img1 = $request->profile_img1_old;

      // if(!empty($profile_img1)){$profile_img1 = "dummy.png";}

        $profile_img2 = $request->profile_img2_old;
      // if(!empty($profile_img2)){$profile_img2 = "user.jpg";}

        $profile_img3 = $request->profile_img3_old;
     //  if(!empty($profile_img3)){$profile_img3 = "user.jpg";}

        $profile_img4 = $request->profile_img4_old;
     //  if(!empty($profile_img4)){$profile_img4 = "user.jpg";}

        $pvideo1 = $request->pvideo1_old;

       $vendor_id = 0;

        if($request->hasFile('video'))
            {

           $imagename = DB::table('vendor')->where('vendor_id', $vendor_id)->value('video');
            if(File::exists(public_path($imagename))){
            File::delete(public_path($imagename));
            }

             $path = $request->file('video')->store('videos', ['disk' => 'my_files']);
             $pvideo1 =  $path;

            }

		$vendor=new Vendor();
		$vendor->name=session()->get("basic-info.firstname");
		$vendor->last_name=session()->get("basic-info.lastname");
		$vendor->username=session()->get("basic-info.username");
		$vendor->business_name=session()->get("basic-info.business");
		$vendor->email=session()->get("basic-info.email");
		//$vendor->address=ucfirst(session()->get("basic-info.city").','.session()->get("basic-info.state").' '.session()->get("basic-info.zipcode"));
		$vendor->address=session()->get("basic-info.address");
		$vendor->address1=session()->get("basic-info.address");
		$vendor->city=session()->get("basic-info.city");
		$vendor->suburb=session()->get("basic-info.city");
		$vendor->state=session()->get("basic-info.state");
		$vendor->zipcode=session()->get("basic-info.zipcode");
		$vendor->mob_no=session()->get("basic-info.phone");
		$vendor->password=Hash::make(session()->get("basic-info.password"));
		$vendor->mailing_address=session()->get("basic-info.mail_address");
		$vendor->market_area=$request->market_area;
		$vendor->service_radius=$request->service_radius;
		$vendor->driver_license=$request->driver_license;
		$vendor->license_expiry=date("Y-m-d",strtotime($request->license_expiry));
		$vendor->ssn=$request->ssn;
		$vendor->dob=date("Y-m-d",strtotime($request->dob));
		$vendor->type=$request->type;
		$vendor->category_id=$request->category;
		$vendor->sub_category_id=$subcategory;
		$vendor->vendor_status=0;
		$vendor->permit_type=$request->permit_type;
		$vendor->permit_number=$request->permit_number;
		$vendor->permit_expiry=date("Y-m-d",strtotime($request->permit_expiry));
		$vendor->description=$request->description;
		$vendor->make=$request->make;
		$vendor->model=$request->model;
		$vendor->year=$request->year;
		$vendor->color=$request->color;
		$vendor->license_plate=$request->licenseplate;
		$vendor->lat=$geo->lat;
		$vendor->lng=$geo->lng;
		$vendor->sales_tax=$request->sales_tax;
		$vendor->excise_tax=$request->excise_tax;
		$vendor->city_tax=$request->city_tax;
		$vendor->commission_rate=$request->commission_rate;
		$vendor->insta=$request->insta;
		$vendor->facebook=$request->facebook;
		$vendor->tiktok=$request->tiktok;
		$vendor->pinterest=$request->pinterest;
		$vendor->unique_id = $uniqueid;

		/*Uploading Images*/
		
		//$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/vendor/license/",$license_front=str_random(16).'.jpg') : $license_front=NULL;

		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/vendor/license/",$license_front=str_random(16).'.'.$request->license_front->extension()) : "";

		//$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/vendor/license/",$license_back=str_random(16).'.jpg') : $license_back=NULL;

		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/vendor/license/",$license_back=str_random(16).'.'.$request->license_back->extension()) : "";

		//$request->hasFile("profile_img1") ? $request->file("profile_img1")->move("public/uploads/vendor/profile/",$profile_img1=str_random(16).'.jpg') : $profile_img1=NULL;

		/*uploading optional images*/
		//$request->hasFile("profile_img2") ? $request->file("profile_img2")->move("public/uploads/vendor/profile/",$profile_img2=str_random(16).'.jpg') : $profile_img2=NULL;
		//$request->hasFile("profile_img3") ? $request->file("profile_img3")->move("public/uploads/vendor/profile/",$profile_img3=str_random(16).'.jpg') : $profile_img3=NULL;
		//$request->hasFile("profile_img4") ? $request->file("profile_img4")->move("public/uploads/vendor/profile/",$profile_img4=str_random(16).'.jpg') : $profile_img4=NULL;

        /* if($request->profile_img1){

		$image = $request->profile_img1;
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $profile_img1= '1'.time().'.png';
        $path = public_path('uploads/vendor/profile/'.$profile_img1);
        file_put_contents($path, $image);

        }else{ $profile_img1=NULL; } */


		$vendor->license_front=$license_front;
		$vendor->license_back=$license_back;
		$vendor->profile_img1=$profile_img1;
		$vendor->profile_img2=$profile_img2;
		$vendor->profile_img3=$profile_img3;
		$vendor->profile_img4=$profile_img4;
		$vendor->video=$pvideo1;


		Mail::send('auth.emails.vendor-account-registered',["firstname" =>  $vendor->name], function ($message) use ($vendor) {
			$message->from(config("app.webmail"), config("app.mailname"));
		    $message->to($vendor->email, $vendor->name." ".$vendor->last_name);
		    $message->subject('Registration Complete');
		});
		$vendor->save();
		session(['vendor' => ["data" => NULL,"step" => "success"]]);
		return redirect()->route("vendorRegistration.success",["id" => $vendor->vendor_id]);
	}

	public function random_string($length) {
	$password = '';
    $alphabets = range('A','Z');
    $final_array = array_merge($alphabets);
       while($length--) {

      $key = array_rand($final_array);

      $password .= $final_array[$key];
                        
      }
   if (preg_match('/[A-Za-z0-9]/', $password))
    {
     return $password;
    }else{
    return  random_string();
    }

 }

	
	public function success(){
		return view("auth.Vendor.RegistrationProcess.success");
	}

	public function nearByVendors(Request $request)
	{
		$lat=$request->lat;
		$lng=$request->lng;
		$distance=2;
		$results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));
		$results;
		$vendors= Arr::pluck($results,"vendor_id");
		
	 	return Productservice::whereIn("vendor_id",$vendors)->where("sub_category_id",$request->subcate)->get();



	}

	public function nearByVendorsPage()
	{
		return view("restaurant_listing");
	}

	public function subcategory_list(Request $request)
	{

		$catid = $request->catid;

	    $subcate = DB::table('product_service_sub_category')		
					->where('category_id', '=' ,$catid)
					->orderBy('id', 'asc')
				    ->get();

		$data_onview = array('subcate'=>$subcate,'catid'=>$catid); 

		return View('ajax.subcategory')->with($data_onview);

	}

}
?>