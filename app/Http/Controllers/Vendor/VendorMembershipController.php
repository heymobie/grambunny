<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use App\MembershipTransaction;
use App\Vendor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Stripe;

class VendorMembershipController extends Controller
{
    public function plans(Request $request)
    {
    	return view("vendor.membership.plans");
    }

    public function paymentPage(Request $request)
    {
    	return view("vendor.membership.payment");
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
  		  $merchant= Vendor::find(auth()->guard("vendor")->id());
        $stripe= Stripe\Charge::create ([
                "amount" => 35 * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from votivetech.com." ,
        ]);
       
        $transaction=new MembershipTransaction();
        $transaction->txn_id = $stripe->balance_transaction;
        $transaction->merchant_id = $merchant->vendor_id;
        $transaction->plan_id = 1;
        $transaction->amount = 35;
        $transaction->billing_details = json_encode($stripe->billing_details);
        $transaction->payment_method_details =json_encode($stripe->payment_method_details);
        $transaction->receipt_url = $stripe->receipt_url;
        $transaction->status = $stripe->status;
        $transaction->stripe_id = $stripe->id;
        $transaction->save();

  		$merchant->stripe_id = $stripe->id;
    	$merchant->plan_purchased = Carbon::now()->format("Y-m-d");
      if (is_null($merchant->plan_id)) {
    		$merchant->plan_expiry = Carbon::now()->addDay(30)->format("Y-m-d");
      }else{
        $merchant->plan_expiry = Carbon::createFromFormat("Y-m-d",$merchant->plan_expiry)->addDay(30);
      }
    	$merchant->plan_id = 1;
  		$merchant->update();
  		 

        Session::flash('success', 'Payment successful!');
          
        return back();
    }
}
