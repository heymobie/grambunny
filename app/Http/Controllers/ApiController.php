<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;

use Hash;

Use DB;

use Auth;

use App\Restaurant;

use App\User;

use App\Device_token;

use App\User_otpdetail;

use App\Temp_cart;

use App\Favourite_restaurant;

use App\Favourite_food;

use App\Delivery_address;

use Session;

use Mail;

use App\Review;

use App\Order;

use App\Order_payment;

use App\Notification_list;

use Braintree_Transaction;

class ApiController extends Controller

{

	private $param;

	public function __construct()

	{

        //blockio init

		$this->param = (array)json_decode(file_get_contents('php://input'), true);

	}



	// 29-8-2019 start

	function generateRandomString($length = 6) {

		$characters = '0123456789';

		$charactersLength = strlen($characters);

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= $characters[rand(0, $charactersLength - 1)];

		}

		return $randomString;

	}

	function order_uniqueid($length = 6) {

		$characters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';

		$charactersLength = strlen($characters);

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= $characters[rand(0, $charactersLength - 1)];

		}

		return $randomString;

	}

	public function register(Request $request) {

		//echo "This is test reg Hello";die;

		$data['firstname'] = '';

		$data['email'] = '';

		$data['password'] = '';

		$data['facebookid'] = '';

		$data['phone'] = '';

		$data['deviceid'] = '';

		$data['devicetype'] = '';

		$data['carrier_name'] = '';

		$carrier_id = '';

		$carrier_name = '';

		$carrier_email = '';

		if(isset($this->param["fname"]))

		{

			$data['firstname'] = $this->param["fname"];

		}

		if(isset($this->param["email"]))

		{

			$data['email'] =  $this->param["email"];

		}

		if(isset($this->param["password"]))

		{

			$data['password'] = $password  = $this->param["password"];

		}

		if(isset($this->param["facebookid"]))

		{

			$data['facebookid'] = $this->param["facebookid"];

		}

		if(isset($this->param["mobileno"]))

		{

			$data['phone'] = $this->param["mobileno"];

		}

		if(isset($this->param["carrier_name"]))

		{

			$data['carrier_name'] = $carrier_id = $this->param["carrier_name"];

			switch($carrier_id)

			{

				case 1:

				$carrier_id = '1';

				$carrier_name = 'Alltel';

				$carrier_email = '@message.alltel.com';

				break;

				case 2:

				$carrier_id = '2';

				$carrier_name = 'AT&T';

				$carrier_email = '@txt.att.net';

				break;

				case 3:

				$carrier_id = '3';

				$carrier_name = 'Boost Mobile';

				$carrier_email = '@myboostmobile.com';

				break;

				case 4:

				$carrier_id = '4';

				$carrier_name = 'Sprint';

				$carrier_email = '@messaging.sprintpcs.com';

				break;

				case 5:

				$carrier_id = '5';

				$carrier_name = 'T-Mobile';

				$carrier_email = '@tmomail.net';

				break;

				case 6:

				$carrier_id = '6';

				$carrier_name = 'U.S. Cellular';

				$carrier_email = '@email.uscc.net';

				break;

				case 7:

				$carrier_id = '7';

				$carrier_name = 'Verizon';

				$carrier_email = '@vtext.com';

				break;

				case 8:

				$carrier_id = '8';

				$carrier_name = 'Virgin Mobile';

				$carrier_email = '@vmobl.com';

				break;

				case 9:

				$carrier_id = '9';

				$carrier_name = 'Republic Wireless';

				$carrier_email = '@text.republicwireless.com';

				break;

			}

		}

	/*else if(empty($data['facebookid'])){

			$response['errorcode'] = "10004";

			$response['ok'] = 0;

			$response['message']="Facebook Address:Required parameter missing";

		}*/

		$response = array();

		/*if(empty($data['firstname'])){

			$response['errorcode'] = "10001";

			$response['ok'] = 0;

			$response['message']="Full name:Required parameter missing";

		}else */

		if(empty($data['phone'])){

			$response['errorcode'] = "10002";

			$response['ok'] = 0;

			$response['message']="Moblie No:Required parameter missing";

		}else if(empty($data['password'])){

			$response['errorcode'] = "10003";

			$response['ok'] = 0;

			$response['message']="password:Required parameter missing";

		}else if(empty($data['carrier_name'])){

			$response['errorcode'] = "10008";

			$response['ok'] = 0;

			$response['message']="Carrier Name:Required parameter missing";

		}elseif (!empty($data)) {

			$user_detail  = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

			$user_detail_email  = DB::table('users')->where('email', '=' ,$this->param['email'])->get();

			$response='';

			if($user_detail){

				$response['errorcode'] = "10005";

				$response['ok'] = 0;

				$response['message']="Mobile No already exists";

				$response['data'] = array('user_detail'=>$user_detail);

			}elseif(($user_detail_email) && (!empty($this->param['email']))){

				$response['errorcode'] = "10006";

				$response['ok'] = 0;

				$response['message']="Email already exists";

				$response['data'] = array('user_detail'=>$user_detail_email);

			}else{

				$result = new User;

				$result->name = trim($this->param['fname']);

				$result->lname = trim($this->param['lname']);

				$result->email = trim($this->param['email']);

				$result->password = bcrypt($this->param['password']);

				$result->user_pass = md5($this->param['password']);

				$result->user_mob = trim($this->param['mobileno']);

				$result->user_fbid = trim($this->param['facebookid']);

				$result->user_gpid = trim($this->param['googleplusid']);

				$result->user_role = '1';

				$result->deviceid = trim($this->param['deviceid']);

				$result->devicetype = trim($this->param['devicetype']);

				$result->user_status = '0';

				$result->carrier_id = $carrier_id;

				$result->carrier_name =$carrier_name;

				$result->carrier_email = $carrier_email;

				$result->dummy1 = trim($this->param['dummy1']);

				$result->dummy2 = trim($this->param['dummy2']);

				$result->dummy3 = trim($this->param['dummy3']);

				$result->checkout_verificaiton = trim($this->param['checkout_verificaiton']);

				$result->save();

				if($result){

			//	$user_id = $result->id;

					//$otp_code = trim($this->generateRandomString(6));

					$otp_code='123456';

					$user_id = $result->id;

					$otp_data = new User_otpdetail;

					$otp_data->user_id = trim($user_id);

					$otp_data->user_mob = trim($this->param['mobileno']);

					$otp_data->otp_number = $otp_code;

					$otp_data->save();

					$otp_inserted_id = $otp_data->otp_id;

					/*********** EMAIL FOR SEN OTP START *************/

					$email_content_detail = DB::table('email_content')

					->select('*')

					->where('email_id', '=' ,'1')

					->get();

					$email_content = $email_content_detail[0]->email_content;

					$searchArray = array("{{user_name}}");

					$replaceArray = array(trim($this->param['fname']));

					$msg = str_replace($searchArray, $replaceArray, $email_content);

					$msg_reg_otp =$msg.''.$otp_code;

				//$msg_reg_otp = 'Congrats, '.trim($this->param['fname']).'! you are registered/verified succesfully, The verification code for registration on grambunny is :'.$otp_code ;

					Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

						$message->from('info@grambunny.com', 'grambunny');

						$otp_email = trim($this->param['mobileno']).$carrier_email;

						$message->to($otp_email);

						$message->bcc('votiveshweta@gmail.com');

						$message->bcc('votivemobile.pankaj@gmail.com');

						$message->bcc('votivemobile.dilip@gmail.com');

						$message->bcc('votiveiphone.hariom@gmail.com');

						$message->bcc('zubaer.votive@gmail.com');

						$message->subject('Registration OTP');

					});

					/********** EMAIL FOR SEN OTP END  *************/

					$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

					$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

				//$response['message'] = "User successfully created.";

					$response['message'] = "You have registered successfully. OTP has been sent on your mobile number. Please verify it";

					$response['ok'] = 1;

					$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

				}else{

					$response['message']="User not created.";

					$response['ok'] = 0;

					$response['errorcode'] = "10007";

				}

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}





	public function login (Request $request){

		$response= array();

		$data_post['username'] = '';

		$data_post['password'] = '';

		if(isset($this->param['mobileno'])){

			$data_post['username'] = $this->param['mobileno'];

		}

		if(isset($this->param['password'])){

			$data_post['password'] = $this->param['password'];

		}

		/*$data_post['deviceid'] = Input::get('deviceid');

		$data_post['devicetype'] = Input::get('devicetype');*/

		if(empty($data_post['username'])){

			$response['errorcode'] = "40001";

			$response['ok'] = 0;

			$response['message']="Mobileno:Required parameter missing";

		}elseif(empty($data_post['password'])){

			$response['errorcode'] = "40002";

			$response['ok'] = 0;

			$response['message'] = "Password: Required parameter missing";

		}

		elseif(!empty($data_post)){

			$FILD_NAME = '';

			if(is_numeric ($this->param['mobileno']))

			{

				$FILD_NAME = 'user_mob';

			}

			else

			{

				$FILD_NAME = 'email';

			}

			$check_mobile_no  = DB::table('users')->where($FILD_NAME, '=' ,$this->param['mobileno'])->get();

			if(count($check_mobile_no)>0)

			{

				$otp_code = trim($this->generateRandomString(6));

				if(Auth::attempt([$FILD_NAME => $this->param['mobileno'], 'password' => $this->param['password']]))

				{

					$user_id =  Auth::user()->id;

					$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

					if($user_detail[0]->user_status==1){

						/** MANAGE LOG START **/

						$log_data['log_type'] = 'normal' ;

						$log_data['log_userid'] =$user_id ;

						$log_data['log_date'] = date('Y-m-d H:i');

						$log_data['log_time']= date('Y-m-d H:i');

						$log_data['log_by'] = Input::get('devicetype') ;

						$log_data['device_id'] = Input::get('deviceid') ;

						$log_data['device_type'] = Input::get('device_name') ;

						$log_data['device_os_name'] = Input::get('os_name') ;

						//$log_detail = DB::table('login_history')->insert($log_data);

						$log_id =$id = DB::table('login_history')->insertGetId($log_data);

						$update_log =  DB::table('users')->where('id', '=' ,$user_id)->update(['log_id'=>$log_id]);

						/** MANAGE LOG END **/

						$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

					//	$response['message'] = "Sign In: Success";

						$response['message'] = "You have logged in successfully";

						$response['ok'] = 1;

						$response['data'] = array('user_detail'=>$user_data);

					}

					else{

						/*********** EMAIL FOR SEN OTP START *************/

						$fullname = $user_detail[0]->name;

						$carrier_email = $user_detail[0]->carrier_email;

						$user_email_send_mail= $user_detail[0]->email;

						$msg_reg_otp ='Dear  '.trim($fullname).',The verification code for login on grambunny is : '.$otp_code ;

						if($FILD_NAME=='user_mob'){

							$otp_data = new User_otpdetail;

							$otp_data->user_id = trim($user_id);

							$otp_data->user_mob = trim($this->param['mobileno']);

							$otp_data->otp_number =$otp_code;

							$otp_data->save();

							$otp_inserted_id = $otp_data->otp_id;

							Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

								$message->from('info@grambunny.com', 'grambunny');

								$otp_email = trim($this->param['mobileno']).$carrier_email;

								$message->to($otp_email);

								$message->bcc('votiveshweta@gmail.com');

								$message->bcc('votivemobile.pankaj@gmail.com');

								$message->bcc('votivemobile.dilip@gmail.com');

								$message->bcc('votiveiphone.hariom@gmail.com');

								$message->bcc('zubaer.votive@gmail.com');

								$message->subject('Two Factor Auth OTP');

							});

						}

						if($FILD_NAME=='email'){

							$otp_data = new User_otpdetail;

							$otp_data->user_id = trim($user_id);

							$otp_data->user_email = trim($this->param['mobileno']);

							$otp_data->otp_number =$otp_code;

							$otp_data->save();

							$otp_inserted_id = $otp_data->otp_id;

							Mail::raw($msg_reg_otp, function ($message) use ($user_email_send_mail){

								$message->from('info@grambunny.com', 'grambunny');

								$otp_email = $user_email_send_mail;

								$message->to($otp_email);

								$message->bcc('votiveshweta@gmail.com');

								$message->bcc('votivemobile.pankaj@gmail.com');

								$message->bcc('votivemobile.dilip@gmail.com');

								$message->bcc('votiveiphone.hariom@gmail.com');

								$message->bcc('zubaer.votive@gmail.com');

								$message->subject('Two Factor Auth OTP');

							});

						}

						/********** EMAIL FOR SEN OTP END  *************/

						$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

						$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

						$response['errorcode'] = "40004";

						$response['ok'] = 2;

						$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

						$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

						/*$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

						$response['errorcode'] = "40004";

						$response['ok'] = 2;

						$response['data'] = array('user_detail'=>$user_data);

						$response['message'] ='Sorry.Your acoount is not active';		*/

					}

				}

				else

				{

					if(is_numeric ($this->param['mobileno']))

					{

						if($check_mobile_no[0]->user_mob!=  $this->param['mobileno'])

						{

							$response['errorcode'] = "40006";

							$response['ok'] = 0;

							$response['message'] = "Invalid Mobile Number/Email!";

						}

						else

						{

							$response['errorcode'] = "40003";

							$response['ok'] = 0;

							$response['message'] = "Invalid Password!";

						}

					}

					else

					{

						if($check_mobile_no[0]->email!=  $this->param['mobileno'])

						{

							$response['errorcode'] = "40006";

							$response['ok'] = 0;

							$response['message'] = "Invalid Mobile Number/Email!";

						}

						else

						{

							$response['errorcode'] = "40003";

							$response['ok'] = 0;

							$response['message'] = "Invalid Password!";

						}

					}

				}

			}

			else

			{

				$response['errorcode'] = "40005";

				$response['ok'] = 0;

				$response['message'] = " Number/Email Does Not Exists in Our System, so please Registration then login!";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}





	/************************************************************/

	//	FUNCTION NAME : Verify OTP

	//	FUNCTION USE :  OTP Verification data

	//	FUNCTION ERRORCODE : 20000

	/****************************************************/

	public function verify_OTP (Request $request){



		$response= array();





		$data_post['username'] = '';

		$data_post['otp'] = '';

		$data_post['from'] = '';



		if(isset($this->param['mobileno'])){

			$data_post['username'] = $this->param['mobileno'];

		}

		if(isset($this->param['otp'])){

			$data_post['otp'] = $this->param['otp'];

		}

		if(isset($this->param['from'])){

			$data_post['from'] = $this->param['from'];

		}

		/*$data_post['deviceid'] = Input::get('deviceid');

		$data_post['devicetype'] = Input::get('devicetype');*/



		if(empty($data_post['otp'])){

			$response['errorcode'] = "20001";

			$response['ok'] = 0;

			$response['message'] = "OTP: Required parameter missing";

		}elseif(empty($data_post['username'])){

			$response['errorcode'] = "20002";

			$response['ok'] = 0;

			$response['message']="mobileno:Required parameter missing";

		}elseif(empty($data_post['from'])){

			$response['errorcode'] = "20002";

			$response['ok'] = 0;

			$response['message']="From:Required parameter missing";

		}

		elseif(!empty($data_post)){



			$otp_detail = DB::table('user_otpdetail')

			->where('user_mob', '=' ,trim($this->param['mobileno']))

			->where('otp_number', '=' ,trim($this->param['otp']))

			->get();

			if($otp_detail)

			{



				$user_id = $otp_detail[0]->user_id;

				$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();



				if($this->param['from']=='signup')

				{

					DB::table('users')

					->where('id', $user_id)

					->update(['user_status' => trim('1')

				]);



					$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();



					DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

					$response['message']='Sign In: Success';

					$response['data'] = array('user_detail'=>$user_detail,'from'=>'signup');

					$response['ok'] = 1;





				}

				elseif(($this->param['from']=='signin') || ($this->param['from']=='forgetpwd'))

				{



					DB::table('users')

					->where('id', $user_id)

					->update(['user_status' => trim('1')

				]);



					$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();





					if($user_detail[0]->user_status==1)

					{

						$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();



						DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

						//$response['message']='Sign In: Success';



						$response['message']='Sign In: Success';

						$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

						$response['ok'] = 1;

					}

					else

					{

						$response['errorcode'] = "20004";

						$response['ok'] = 2;

						$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

						$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

					}

				}



			}

			else

			{

				$response['errorcode'] = "20003";

				$response['data'] = array('from'=>$this->param['from']);

				$response['ok'] = 0;

				$response['message'] = "Invalid OTP. Please enter a valid otp";

			}



		}



		header('Content-type: application/json');

		echo json_encode($response);

	}



	/************************************************************/

	//	FUNCTION NAME : Resent OTP

	//	FUNCTION USE :  Resent OTP  data

	//	FUNCTION ERRORCODE : 30000

	/****************************************************/

	public function resent_OTP (Request $request){

		$response= array();

		$data_post['username'] = '';

		$data_post['from'] = '';

		if(isset($this->param['mobileno']))

		{

			$data_post['username'] = $this->param['mobileno'];

		}

		if(isset($this->param['from']))

		{

			$data_post['from'] = $this->param['from'];

		}

		/*$data_post['deviceid'] = Input::get('deviceid');

		$data_post['devicetype'] = Input::get('devicetype');*/

		if(empty($data_post['username'])){

			$response['errorcode'] = "30001";

			$response['ok'] = 0;

			$response['message'] = "Mobile No: Required parameter missing";

		}elseif(empty($data_post['from'])){

			$response['errorcode'] = "30002";

			$response['ok'] = 0;

			$response['message']="From:Required parameter missing";

		}

		elseif(!empty($data_post)){

			$user_detail = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

			if(!empty($user_detail))

			{

				$user_id = $user_detail[0]->id;

				$carrier_email = $user_detail[0]->carrier_email;

				$fullname = $user_detail[0]->name;

				$otp_code = trim($this->generateRandomString(6));

				$type_form = $this->param['from'];

				if($this->param['from']=='signup')

				{

					$otp_data = new User_otpdetail;

					$otp_data->user_id = trim($user_id);

					$otp_data->user_mob = trim($this->param['mobileno']);

					$otp_data->otp_number = trim($otp_code);

					$otp_data->save();

					$otp_inserted_id = $otp_data->otp_id;

					$user_data = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

					$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

					//$response['message'] = "Resend OTP.";

					$response['message'] = "OTP has been sent on your mobile number. Please verify it.";

					$response['ok'] = 1;

					$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail,'from'=>$this->param['from']);

				}elseif(($this->param['from']=='signin') || ($this->param['from']=='forgetpwd'))

				{

					$otp_data = new User_otpdetail;

					$otp_data->user_id = trim($user_id);

					$otp_data->user_mob = trim($this->param['mobileno']);

					$otp_data->otp_number = trim($otp_code);

					$otp_data->save();

					$otp_inserted_id = $otp_data->otp_id;

					$user_data = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

					$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

					if($user_detail[0]->user_status==1)

					{

						//$response['message'] = "Resend OTP.";

						$response['message'] = "OTP has been sent on your mobile number. Please verify it.";

						$response['ok'] = 1;

						$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail,'from'=>$this->param['from']);

					}

					else

					{

						$response['errorcode'] = "30004";

						$response['ok'] = 2;

						$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

						$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

					}

				}

				/*********** EMAIL FOR SEN OTP START *************/

				$subject_otp = 'Resend OTP for '.$type_form;

				$msg_reg_otp ='Dear  '.trim($fullname).', The verification code resend for '.$type_form .'  on grambunny is : '.$otp_code ;

				Mail::raw($msg_reg_otp, function ($message) use ($carrier_email,$subject_otp){

					$message->from('info@grambunny.com', 'grambunny');

					$otp_email = trim($this->param['mobileno']).$carrier_email;

					$message->to($otp_email);

					$message->bcc('votiveshweta@gmail.com');

					$message->bcc('votivemobile.pankaj@gmail.com');

					$message->bcc('votivemobile.dilip@gmail.com');

					$message->bcc('votiveiphone.hariom@gmail.com');

					$message->bcc('zubaer.votive@gmail.com');

					$message->subject($subject_otp);

				});

				/********** EMAIL FOR SEN OTP END  *************/

			}

			else

			{

				$response['errorcode'] = "30003";

				$response['data'] = array('from'=>$this->param['from']);

				$response['ok'] = 0;

				$response['message'] = "Invalid Mobile Number!";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}



	/************************************************************/

	//	FUNCTION NAME : FORGOT PASSWORD

	//	FUNCTION USE :  app user FORGOT PASSWORD functionality

	//	FUNCTION ERRORCODE : 50000

	/****************************************************/

	public function forget_pwd(Request $request)

	{

		$response= array();

		$data_post['username'] = '';

		if(isset($this->param['mobileno'])){

			$data_post['username'] = $this->param['mobileno'];

		}

		if(empty($data_post['username'])){

			$response['errorcode'] = "50001";

			$response['ok'] = 0;

			$response['message'] = "Mobile No: Required parameter missing";

		}

		elseif(!empty($data_post)){

			$user_detail = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

			if(!empty($user_detail)){

				//$otp_code = trim($this->generateRandomString(6));

				$otp_code='123456';

				$carrier_email = trim($user_detail[0]->carrier_email);

				if($user_detail[0]->user_status==1){

					$user_id =  $user_detail[0]->id;

					$otp_data = new User_otpdetail;

					$otp_data->user_id = trim($user_id);

					$otp_data->user_mob = trim($this->param['mobileno']);

					$otp_data->otp_number = $otp_code;

					$otp_data->save();

					$otp_inserted_id = $otp_data->otp_id;

					/*********** EMAIL FOR SEN OTP START *************/

					$msg_reg_otp = 'Dear '.trim($user_detail[0]->name).', The verification code for Forgot Password on grambunny is :'.$otp_code ;

					Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

						$message->from('info@grambunny.com', 'grambunny');

						$otp_email = trim($this->param['mobileno']).$carrier_email;

						$message->to($otp_email);

						$message->bcc('votiveshweta@gmail.com');

						$message->bcc('votivemobile.pankaj@gmail.com');

						$message->bcc('votivemobile.dilip@gmail.com');

						$message->bcc('zubaer.votive@gmail.com');

						$message->subject('Forgot Passowrd OTP');

					});

					/********** EMAIL FOR SEN OTP END  *************/

					$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

					$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

						//$response['message'] = "Forget Pwd: Mobile Number Varify!";

					$response['message'] = "OTP has been sent on your mobile number. Please verify it.";

					$response['ok'] = 1;

					$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

				}

				else

					{	$response['errorcode'] = "50002";

				$response['ok'] = 2;

				$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

			}

		}

		else

		{

			$response['errorcode'] = "50003";

			$response['ok'] = 0;

				//$response['message'] ='Sorry.Your mobile is not registered ';

			$response['message'] ='The mobile number is not registered with us. Please try again. ';

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Reset_password

	//	FUNCTION USE :  app user onfromation update

	//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function reset_password (Request $request){

	$user_id = Input::get('userid');

	$data['user_id'] = Input::get("userid");

	$data['newpassword'] = Input::get("newpassword");

	$response = array();

	if(empty($data['newpassword'])){

		$response['errorcode'] = "200001";

		$response['ok'] = 0;

		$response['message']="New password:Required parameter missing";

	}elseif(empty($data['user_id'])){

		$response['message']="user id:Required parameter missing";

		$response['ok'] = 0;

		$response['errorcode'] = "200002";

	}elseif(!empty($data)) {

		DB::table('users')

		->where('id', $user_id)

		->update(['password' =>  Hash::make(trim(Input::get('newpassword'))),

			'user_pass'=> md5(trim(Input::get('newpassword')))

		]);

		$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

		$response['message'] = "Password updated sucessfully.";

		$response['ok'] = 1;

		$response['data'] = $user_data;

	}

	header('Content-type: application/json');

	echo json_encode($response);

}





/************************************************************/

	//	FUNCTION NAME : Search_restaurant

	//	FUNCTION USE :  SHOW ALL RESTAURANT LIST AND IF SEARCH WITH ANY CONDITION

	//	FUNCTION ERRORCODE : 100000

/****************************************************/



public function search_restaurant()

{

	DB::enableQueryLog();

	$data['add_lat'] = '';

	$data['add_lng'] = '';

	if(isset($this->param["add_lat"]))

	{

		$data['add_lat'] = $this->param["add_lat"];

	}

	if(isset($this->param["add_lng"]))

	{

		$data['add_lng'] =  $this->param["add_lng"];

	}

	$response = array();

	if(empty($data['add_lat'])){

		$response['errorcode'] = "100001";

		$response['ok'] = 0;

		$response['message'] = "Address Lat: Required parameter missing";

	}

	elseif(empty($data['add_lng'])){

		$response['errorcode'] = "100002";

		$response['ok'] = 0;

		$response['message'] = "Address Lng: Required parameter missing";

	}elseif(!empty($data))

	{

		$test_sql = '';

		$lat = Input::get('add_lat');

		$long = Input::get('add_lng');

		$distancein_miles = 5;

		//$keyword = Input::get('keyword_search');

		//$keyword = Input::get('keywords');

		$distancein_miles =$this->param['distanceinmiles'];

		$keyword =$this->param['keywords'];

		$open_status_search  = $this->param['open_status'];

		/*************************/

		$count_data =count(DB::table('search_restaurant_view')

			->select(DB::raw("*,

				(((acos(sin((".$lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$long." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance"))

			->having("distance","<",$distancein_miles )

			->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE')

			->groupBy('search_restaurant_view.rest_id')

			->get());

		$google_data = '';

		 /*if($count_data<10)

		 {

     $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=500&type=restaurant&key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU";

      $json=file_get_contents($url);

      $data1=json_decode($json,true);

		  		// print_r($data1 );

		  if($data1['status']=='OK'){

		  		if(!empty($data1['results']))

				{

				   foreach($data1['results'] as $result)

				   {

				   	$rest_logo='';

				  // print_r($result );

				  $rest_rating='';

					   $rest_lat=$result['geometry']['location']['lat'];

					   $rest_lang=$result['geometry']['location']['lng'];

					   $rest_name=$result['name'];

					   if(isset($result['rating'])){

					   	$rest_rating=$result['rating'];

					  	}

					   if(isset($result['photos'])){

					   		//$rest_logo=$result['photos'][0]['photo_reference'];

							$photo_reference = $result['photos'][0]['photo_reference'];

						 $url_image =

"https://maps.googleapis.com/maps/api/place/photo?maxwidth=135&photoreference=$photo_reference&key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU";

						$rest_logo = $url_image ;

					   }

					   $rest_vicinity = $result['vicinity'];

					    $total_rec  = DB::table('restaurant')->where('rest_name', '=' ,$rest_name)->count();

		// echo $rest_name.'---'.$total_rec.'<br>' ;

						if($total_rec==0)

						{

								$rest = new Restaurant;

								$rest->rest_name = $rest_name;

								$rest->rest_address = $rest_vicinity;

								$rest->rest_logo = $rest_logo;

								$rest->rest_status ='PUBLISHED';

								$rest->google_type ='1';

								$rest->rest_lat =  $rest_lat;

								$rest->rest_long = $rest_lang;

								$rest->save();

						 $rest_id = $rest->rest_id;

							if(!empty($rest_id)){

							DB::enableQueryLog();

							  $range =$rest_rating;

							  if($range>0)

							  {

									$range=$rest_rating;

								if($range<=5){

									$range=($rest_rating*2);

								}

							  }

							  else

							  {

								$range = 0;

							  }

								$order_re = new Review;

								$order_re->re_orderid = 0;

								$order_re->re_userid = 0;

								$order_re->re_content = '';

								$order_re->re_restid = $rest_id;

								$order_re->re_rating = $range;

								$order_re->re_status = 'PUBLISHED';

								$order_re->save();

								$re_id = $order_re->re_id;

							//	print_r( DB::getQueryLog());

							}

						}

					}

				}

			}

		 }

		 */

		 /************************/

		 $limit = 10;

		/*if((Input::get('per_page')))

		{

			$limit = Input::get('per_page');

		}*/

		$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();

		$rest_listing = DB::table('search_restaurant_view');

		if(!empty($lat) && ($long))

		{

		/*	$rest_listing = $rest_listing->select(DB::raw("*,

                     (3959 * ACOS(COS(RADIANS(".$lat."))

                           * COS(RADIANS(rest_lat))

                           * COS(RADIANS(".$long.") - RADIANS(rest_long))

                           + SIN(RADIANS(".$lat."))

                           * SIN(RADIANS(rest_lat)))) AS distance")

						  );

						  $rest_listing = $rest_listing->having("distance","<",$distancein_miles);*/

						  $rest_listing = $rest_listing->select(DB::raw("*,(((acos(sin((".$lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$long." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance, (SELECT `rest_time`.`rest_id` FROM `rest_time` WHERE `rest_time`.`rest_id` = `search_restaurant_view`.`rest_id` and `rest_time`.`day` = '".date('D')."' and `rest_time`.`open_time` <= '".date('H:i:s')."' and `rest_time`.`close_time` >= '".date('H:i:s')."'  limit 1 ) as 'open_status' "));

						  $rest_listing = $rest_listing->having("distance","<",$distancein_miles);

						}

						else

						{

							$rest_listing = $rest_listing->select('*');

						}

						$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

						if( $this->param['reset']==0){

							if(($open_status_search!='') && ($open_status_search==1))

							{

								$cc_time['cc_time'] = date('H:i:s');

								$cc_time['cc_day'] = date('D');

								/*`search_restaurant_view`.`rest_id` = (SELECT `rest_time`.`rest_id` FROM `rest_time` where `rest_time`.`rest_id` = `search_restaurant_view`.`rest_id` and `rest_time`.`day`='mon' and (`rest_time`.`open_time` < '06:01:16') AND (`rest_time`.`close_time` > '06:01:16')) */

								$rest_listing = $rest_listing->whereIn('search_restaurant_view.rest_id',function($query) use ($cc_time) {

									$query->select('rest_time.rest_id')

									->from('rest_time')

									->where('rest_time.day', '=' , $cc_time['cc_day'])

									->where('rest_time.open_time', '<' , $cc_time['cc_time'] )

									->where('rest_time.close_time', '>' , $cc_time['cc_time'] );

								});

							}

							elseif(($open_status_search!='') && ($open_status_search==0))

							{

								$cc_time['cc_time'] = date('H:i:s');

								$cc_time['cc_day'] = date('D');

								/*`search_restaurant_view`.`rest_id` = (SELECT `rest_time`.`rest_id` FROM `rest_time` where `rest_time`.`rest_id` = `search_restaurant_view`.`rest_id` and `rest_time`.`day`='mon' and (`rest_time`.`open_time` < '06:01:16') AND (`rest_time`.`close_time` > '06:01:16')) */

								$rest_listing = $rest_listing->whereNOTIn('search_restaurant_view.rest_id',function($query) use ($cc_time) {

									$query->select('rest_time.rest_id')

									->from('rest_time')

									->where('rest_time.day', '=' , $cc_time['cc_day'])

									->where('rest_time.open_time', '<' ,$cc_time['cc_time'] )

									->where('rest_time.close_time', '>' , $cc_time['cc_time'] );

								});

							}

			/*( 3959 * acos( cos( radians($t->emp_address_lat) ) * cos( radians( gtw_student_details.st_address_lat ) )

 	* cos( radians(gtw_student_details.st_address_long) - radians($t->emp_address_long)) + sin(radians($t->emp_address_lat))

 	* sin( radians(gtw_student_details.st_address_lat)))) AS distance*/

			/*if(!empty($keyword))

			{

				$rest_listing = $rest_listing->where('search_restaurant_view.rest_name', 'like' ,'%'.$keyword.'%');

				$rest_listing = $rest_listing->orWhere('search_restaurant_view.rest_zip_code', 'like' , $keyword);

			}*/

			if(!empty($keyword))

			{

				if(!empty($keyword))

				{

					$rest_listing = $rest_listing->whereIn('search_restaurant_view.rest_id',function($query) use ($keyword) {

						$query->select('food_rest_name_view.rest_id')

						->from('food_rest_name_view')

						->where('food_rest_name_view.rest_food', 'like' , '%'.$keyword.'%');

					});

				}

			}

			$foodType = $this->param['food_type'];

			$foodType_post = 0;

			//print_r($foodType);

			if(empty($foodType))

			{

				$foodType_post=0;

			}

			else

			{

				$foodType_post=1;

			}

			if(isset($foodType) && (count($foodType)>0) && ($foodType_post==1) )

			{

				$rest_listing = $rest_listing->where(function($query) {

					$l=0;

					foreach($this->param['food_type'] as $ftype )

					{

						if($ftype>0)

						{

							if($l>0){

								$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

							}

							else

							{

								$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

							}

						/*if($l>0){

						 $query = $query->orWhere('search_restaurant_view.rest_cuisine',$ftype );

						}

						else

						{

							$query = $query->where('search_restaurant_view.rest_cuisine',$ftype );

						}*/

					}

					$l++;

				}

			});

			}

			if(!empty($this->param['rating']))

			{

				$rest_listing = $rest_listing->where('search_restaurant_view.totalrating', '=' ,$this->param['rating']);

			}

			if(!empty($this->param['price_level']))

			{

				$rest_listing = $rest_listing->where('search_restaurant_view.rest_price_level', '=' ,$this->param['price_level']);

			}

			if($this->param['sortedby']>0)

			{

				if($this->param['sortedby']=='1')	 //1.Rating(Ascending) (value=1)

				{

					$rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'asc');

				}

				 if($this->param['sortedby']=='2')	 //2.Rating(Descending) (value=2)

				 {

				 	$rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'desc');

				 }

				 if($this->param['sortedby']=='3')	 //2.Rating(Descending) (value=2)

				 {

				 	$rest_listing = $rest_listing->orderBy('distance', 'asc');

				 }

				 if($this->param['sortedby']=='4')	 //2.Rating(Descending) (value=2)

				 {

				 	$rest_listing = $rest_listing->orderBy('distance', 'desc');

				 }

				 // $rest_listing->orderBy('distance', 'asc');

				 if($this->param['sortedby']=='5')		 //5.Delivery Min(Ascending) fees,(value=5)

				 {

				 	$rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'asc');

				 }

				  if($this->param['sortedby']=='6')		 //6.Delivery Min(Descending) fees,(value=6)

				  {

				  	$rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'desc');

				  }

				/* if($this->param['sortedby']=='1')	 //1.rating (value=1)

				 {

					 $rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'desc');

				 }

				 if($this->param['sortedby']==1)	 //2.price (Note clear)

				 {

					 $rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'asc');

				 }

				 if($this->param['sortedby']==1)	 //3.distance,(value=2)

				 {

					 $rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'asc');

				 }

				 if($this->param['sortedby']=='3')		 //delivery fees,(value=3)

				 {

					 $rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'desc');

				 }

				 if($this->param['sortedby']=='4')		//5.restaurentname(value=4)

				 {

					 $rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_name', 'desc');

					}*/

				}

			}

			$rest_listing = $rest_listing->orderBy('search_restaurant_view.google_type', 'asc');

			$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

			$test_sql  = $rest_listing;

			$test_sql  = $test_sql->get();

			$rest_listing = $rest_listing->simplePaginate($limit);

			$total_restaurant =count($rest_listing) ;

		//	 dd(DB::getQueryLog());

			$rest_data = '';

			$open_staus = 0 ;

			$tot_rec = count($test_sql);

		// print_r($test_sql);

			$lastPage  = ceil($tot_rec/$limit);

			foreach($rest_listing as $listing)

			{

				/** FAVOURIT OPTION **/

				$rest_favourite= 0;

				if(isset($this->param['userid']) && ($this->param['userid']>0))

				{

					$favourate_listing = DB::table('favourite_restaurant')

					->where('favrest_userid', '=' ,$this->param['userid'])

					->where('favrest_status', '=' ,'1')

					->where('favrest_restid', '=' ,$listing->rest_id)

					->get();

					if($favourate_listing)

					{

						$rest_favourite= 1;

					}

				}

				/**  END   ***/

				$rating = 0;

				if($listing->totalrating=='NULL')

				{

					$rating = 0;

				}

				else

				{

					$rating = round($listing->totalrating);

				}

			$open_staus = 0 ;// 0-close / 1-open Calcualtion reamning

			/********************/

			/*******************/

			$dist_km = '';

			if(@$listing->distance)

			{

				$dist_km = $listing->distance;

			}

			$logo_rest = '';

			if($listing->google_type==1)

			{

				$logo_rest = $listing->rest_logo;

			}

			else

			{

				$logo_rest = url('/').'/uploads/reataurant/'.$listing->rest_logo;

				/**************************** TIME OPEN STATUS  *******/

				$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

				$rest_end = '';

				$old_day = 0;

				$current_time =$tt=  strtotime(date("H:i"));

				if(!empty($listing->open_status))

				{

					$open_staus = 1;

				}

				/*if($day_name=='sun') {

										if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_sat)){

													$rest_time2 = explode('_',$listing->rest_sat);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

											if(!empty($listing->rest_sun)){

											$rest_time1 = explode('_',$listing->rest_sun);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_sat)){

													$rest_time2 = explode('_',$listing->rest_sat);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

											}

											else

											{

												$open_time = '1';

											}

										}

										}

									}

				if($day_name=='mon') {

										if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_sun)){

													$rest_time2 = explode('_',$listing->rest_sun);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

											if(!empty($listing->rest_mon)){

											$rest_time1 = explode('_',$listing->rest_mon);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_sun)){

													$rest_time2 = explode('_',$listing->rest_sun);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

													}

											}

											else

											{

												$open_time ='1';

											}

										}

										}

									}

				if($day_name=='tue') {

										  if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_mon)){

													$rest_time2 = explode('_',$listing->rest_mon);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

										if(!empty($listing->rest_tues)){

											$rest_time1 = explode('_',$listing->rest_tues);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_mon)){

													$rest_time2 = explode('_',$listing->rest_mon);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

													}

											}

											else

											{

												$open_time ='1';

											}

										}

										}

									}

				if($day_name=='wed') {

										  if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_tues)){

													$rest_time2 = explode('_',$listing->rest_tues);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

										if(!empty($listing->rest_wed)){

											$rest_time1 = explode('_',$listing->rest_wed);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_tues)){

													$rest_time2 = explode('_',$listing->rest_tues);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

													}

											}

											else

											{

												$open_time ='1';

											}

										}

										}

									}

				if($day_name=='thu') {

									  if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_wed)){

													$rest_time2 = explode('_',$listing->rest_wed);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

										if(!empty($listing->rest_thus)){

											$rest_time1 = explode('_',$listing->rest_thus);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_wed)){

													$rest_time2 = explode('_',$listing->rest_wed);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

													}

											}

											else

											{

												$open_time ='1';

											}

										}

										}

									}

				if($day_name=='fri') {

									  if((!empty($listing->rest_close)) &&

											(in_array($day_name,(explode(',',$listing->rest_close)) )))

										{

											 if(!empty($listing->rest_thus)){

													$rest_time2 = explode('_',$listing->rest_thus);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

												}

										}

										else

										{

											if(!empty($listing->rest_fri)){

											$rest_time1 = explode('_',$listing->rest_fri);

											$start_time = date("H:i", (strtotime($rest_time1[0])));

											if((strtotime($start_time)>$tt))

											{

												if(!empty($listing->rest_thus)){

													$rest_time2 = explode('_',$listing->rest_thus);

													if((strtotime($rest_time2[1])>$current_time))

													{

														$open_time ='1';

													}

													}

											}

											else

											{

												$open_time ='1';

											}

										}

										}

									}

				if($day_name=='sat') {

													  if((!empty($listing->rest_close)) &&

															(in_array($day_name,(explode(',',$listing->rest_close)) )))

														{

															 if(!empty($listing->rest_fri)){

																	$rest_time2 = explode('_',$listing->rest_fri);

																	if((strtotime($rest_time2[1])>$current_time))

																	{

																		$open_time ='1';

																	}

																}

														}

														else

														{

															if(!empty($listing->rest_sat)){

															$rest_time1 = explode('_',$listing->rest_sat);

															$start_time = date("H:i", (strtotime($rest_time1[0])));

															if((strtotime($start_time)>$tt))

															{

																if(!empty($listing->rest_fri)){

																	$rest_time2 = explode('_',$listing->rest_fri);

																	if((strtotime($rest_time2[1])>$current_time))

																	{

																		$open_time ='1';

																	}

																	}

															}

															else

															{

																$open_time ='1';

															}

														}

														}

													}*/

													/*********************  TIME OPEN STATUS END ***************/

												}

												$cuisine_name = '';

												if(!empty($listing->cuisine_name))

												{

													$cuisine_name =	$listing->cuisine_name;

												}

												$rest_data[] = array(

													'rest_id' => $listing->rest_id,

													'rest_name' => $listing->rest_name,

													'rest_metatag' => $listing->rest_metatag,

													'rest_logo' => trim($logo_rest),

													'rest_address' => $listing->rest_address,

													'rest_address2' => $listing->rest_address2,

													'rest_city' => $listing->rest_city,

													'rest_zip_code' => $listing->rest_zip_code,

													'rest_suburb' => $listing->rest_suburb,

													'rest_state' => $listing->rest_state,

													'rest_service'=>$listing->rest_service,

													'rest_mindelivery'=>$listing->rest_mindelivery,

													'rest_status'=>$listing->rest_status,

													'rest_email'=>$listing->rest_email,

													'cuisine_name'=>$cuisine_name,

													'total_review'=>$listing->totalreview,

													'total_rating'=>$rating,

													'distance'=>$dist_km,

													'open_status'=>$open_staus,

													'rest_favourite'=>$rest_favourite,

													'rest_lat'=>$listing->rest_lat,

													'rest_long'=>$listing->rest_long,

													'rest_google'=>$listing->google_type,

													'rest_price_level'=>$listing->rest_price_level,

													'rest_min_orderamt' => trim($listing->rest_min_orderamt)

												);

											}

											$rest_detail_format['total'] = $tot_rec  ;

											$rest_detail_format['per_page'] =$rest_listing->perPage();

											$rest_detail_format['current_page'] =$rest_listing->currentPage();;

											$rest_detail_format['last_page'] =$lastPage;

											$rest_detail_format['data'] =$rest_data;

											if($total_restaurant>0)

											{

												$response['message'] = "All restaurant list";

												$response['ok'] = 1;

												$response['cuisine_list'] = $cuisine_list;

												$response['rest_listing'] = $rest_detail_format;

			//$response['rest_listing_main'] = $rest_listing;

											}

											else

											{

												$response['message']="No restaurant found!";

												$response['ok'] = 0;

												$response['errorcode'] = "100001";

											}

										}

										header('Content-type: application/json');

										echo json_encode($response);

									}





									/************************************************************/

	//	FUNCTION NAME : show_soical_login

	//	FUNCTION USE :  Remove all old restaurnt item and insert new

	//	FUNCTION ERRORCODE : 460000

									/****************************************************/

									public function get_soical_signup()

									{

										$data['firstname'] = '';

										$data['email'] = '';

										$data['password'] = '';

										$data['facebookid'] = '';

										$data['phone'] = '';

										$data['deviceid'] = '';

										$data['devicetype'] = '';

										$data['carrier_name'] = '';

										$carrier_id = '';

										$carrier_name = '';

										$carrier_email = '';

										if(isset($this->param["fname"]))

										{

											$data['firstname'] = $this->param["fname"];

										}

										if(isset($this->param["email"]))

										{

											$data['email'] =  $this->param["email"];

										}

										if(isset($this->param["password"]))

										{

											$data['password'] = $password  = $this->param["password"];

										}

										if(isset($this->param["facebookid"]))

										{

											$data['facebookid'] = $this->param["facebookid"];

										}

										if(isset($this->param["mobileno"]))

										{

											$data['phone'] = $this->param["mobileno"];

										}

										if(isset($this->param["carrier_name"]))

										{

											$data['carrier_name'] = $carrier_id = $this->param["carrier_name"];

											switch($carrier_id)

											{

												case 1:

												$carrier_id = '1';

												$carrier_name = 'Alltel';

												$carrier_email = '@message.alltel.com';

												break;

												case 2:

												$carrier_id = '2';

												$carrier_name = 'AT&T';

												$carrier_email = '@txt.att.net';

												break;

												case 3:

												$carrier_id = '3';

												$carrier_name = 'Boost Mobile';

												$carrier_email = '@myboostmobile.com';

												break;

												case 4:

												$carrier_id = '4';

												$carrier_name = 'Sprint';

												$carrier_email = '@messaging.sprintpcs.com';

												break;

												case 5:

												$carrier_id = '5';

												$carrier_name = 'T-Mobile';

												$carrier_email = '@tmomail.net';

												break;

												case 6:

												$carrier_id = '6';

												$carrier_name = 'U.S. Cellular';

												$carrier_email = '@email.uscc.net';

												break;

												case 7:

												$carrier_id = '7';

												$carrier_name = 'Verizon';

												$carrier_email = '@vtext.com';

												break;

												case 8:

												$carrier_id = '8';

												$carrier_name = 'Virgin Mobile';

												$carrier_email = '@vmobl.com';

												break;

												case 9:

												$carrier_id = '9';

												$carrier_name = 'Republic Wireless';

												$carrier_email = '@text.republicwireless.com';

												break;

											}

										}

										$response = array();

										if((empty($data['phone'])) && (empty($data['email']))){

											$response['errorcode'] = "460001";

											$response['ok'] = 0;

											$response['message']="Moblie No / Email Id :Required parameter missing";

										}elseif (!empty($data)) {

											$response='';

											$user_detail  = '';

											$user_detail_email  = '';

											if((isset($this->param["mobileno"]))&& (!empty($this->param['mobileno'])))

											{

												$user_detail  = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

											}

											if((isset($this->param["email"]))&& (!empty($this->param['email'])))

											{

												$user_detail_email  = DB::table('users')->where('email', '=' ,$this->param['email'])->get();

											}

											if((isset($this->param["mobileno"]))&& (!empty($this->param['mobileno'])) && (!empty( $user_detail)))

											{

												$response['errorcode'] = "460002";

												$response['ok'] = 1;

												$response['message']="Mobile No already exists";

												$response['data'] = array('user_detail'=>$user_detail);

											}elseif( (isset($this->param['email']))&& (!empty($this->param['email'])) && (!empty($user_detail_email)))

											{

	/*	 DB::enableQueryLog();

			print_r($user_detail_email);

			dd(DB::getQueryLog());

			exit;*/

			$response['errorcode'] = "460006";

			$response['ok'] = 1;

			$response['message']="Email already exists";

			$response['data'] = array('user_detail'=>$user_detail_email);

		}else{

			$result = new User;

			$result->name = trim($this->param['fname']);

			$result->lname = trim($this->param['lname']);

			$result->email = trim($this->param['email']);

			$result->password = bcrypt($this->param['password']);

			$result->user_pass = md5($this->param['password']);

			$result->user_mob = trim($this->param['mobileno']);

			$result->user_fbid = trim($this->param['facebookid']);

			$result->user_gpid = trim($this->param['googleplusid']);

			$result->user_role = '1';

			$result->deviceid = trim($this->param['deviceid']);

			$result->devicetype = trim($this->param['devicetype']);

			$result->user_status = '1';

			$result->carrier_id = $carrier_id;

			$result->carrier_name =$carrier_name;

			$result->carrier_email = $carrier_email;

			$result->dummy1 = trim($this->param['dummy1']);

			$result->dummy2 = trim($this->param['dummy2']);

			$result->dummy3 = trim($this->param['dummy3']);

			$result->checkout_verificaiton = trim($this->param['checkout_verificaiton']);

			$result->save();

			if($result){

				$user_id = $result->id;

				$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

				$response['message'] = "You have registered  successfully.";

				$response['ok'] = 1;

				$response['data'] = array('user_detail'=>$user_data);

			}else{

				$response['message']="User not created.";

				$response['ok'] = 0;

				$response['errorcode'] = "460007";

			}

		}

	}

	header('Content-type: application/json');

	//echo json_encode($result);

	echo json_encode($response);

}







//	FUNCTION NAME : View User Profile

	//	FUNCTION USE :  app user onfromation update

	//	FUNCTION ERRORCODE : 70000

/****************************************************/

public function viewprofile (Request $request){

	$data_post['user_id'] = '';

	if(isset($this->param['userid'])){

		$data_post['user_id'] = $this->param['userid'];

		$user_id = $this->param['userid'];

	}

	if(empty($data_post['user_id'])){

		$response['errorcode'] = "70001";

		$response['ok'] = 0;

		$response['message'] = "User Id: Required parameter missing";

	}elseif(!empty($data_post))

	{

		$user_id = $this->param['userid'];

		$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

		if(!empty($user_detail)){

			if($user_detail[0]->user_status==1){

				$dummy1_id='0';$dummy1_name='House';

				$dummy2_id='0';$dummy2_name='House';

				$dummy3_id='0';$dummy3_name='House';

				//$dummy1_name = $user_detail[0]->dummy1;

				if(($user_detail[0]->dummy1>0) && ($user_detail[0]->dummy1!='House')){

					$user_dummy1  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy1)->get();

					$dummy1_name = $user_dummy1[0]->name.' '.$user_dummy1[0]->lname;

					$dummy1_id = $user_detail[0]->dummy1;

				}

				if(($user_detail[0]->dummy2>0) && ($user_detail[0]->dummy2!='House')){

					$user_dummy2  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy2)->get();

					$dummy2_name = $user_dummy2[0]->name.' '.$user_dummy2[0]->lname;

					$dummy2_id = $user_detail[0]->dummy2;

				}

				if(($user_detail[0]->dummy3>0) && ($user_detail[0]->dummy3!='House')){

					$user_dummy3  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy3)->get();

					$dummy3_name = $user_dummy3[0]->name.' '.$user_dummy3[0]->lname;

					$dummy3_id = $user_detail[0]->dummy3;

				}

				$response['message'] = "View Profile : View user detail";

				$response['ok'] = 1;

				$response['data'] = array(

					'user_detail'=>$user_detail,

					'dummy1_id' =>$dummy1_id,

					'dummy1_name' =>trim($dummy1_name),

					'dummy2_id' =>$dummy2_id,

					'dummy2_name' =>trim($dummy2_name),

					'dummy3_id' =>$dummy3_id,

					'dummy3_name' =>trim($dummy3_name)

				);

			}

			else

			{

				$response['errorcode'] = "70002";

				$response['ok'] = 2;

				$response['message'] ='Sorry.Your account is not active. Please verify your mobile number via OTP.';

			}

		}

		else

		{

			$response['errorcode'] = "70003";

			$response['ok'] = 0;

			$response['message'] ='Sorry.Invalide user Id!';

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Update_profile

	//	FUNCTION USE :  app user onfromation update

	//	FUNCTION ERRORCODE : 60000

/****************************************************/

public function update_profile (Request $request){

	$user_id = Input::get('userid');

	$data['user_id'] = '';

	$data['firstname'] = '';

	$data['address'] = '';

	$data['city'] = '';

	$data['postcode'] = '';

	if(isset($this->param["userid"]))

	{

		$data['user_id'] = $this->param["userid"];

	}

	if(isset($this->param["fname"]))

	{

		$data['firstname'] = $this->param["fname"];

	}

	if(isset($this->param["address"]))

	{

		$data['address'] =  $this->param["address"];

	}

	if(isset($this->param["city"]))

	{

		$data['city'] = $password  = $this->param["city"];

	}

	if(isset($this->param["postcode"]))

	{

		$data['postcode'] = $this->param["postcode"];

	}

	$response = array();

	if(empty($data['firstname'])){

		$response['errorcode'] = "60001";

		$response['ok'] = 0;

		$response['message']="Full name:Required parameter missing";

	}else if(empty($data['user_id'])){

		$response['errorcode'] = "60002";

		$response['ok'] = 0;

		$response['message']="User Id:Required parameter missing";

	}elseif (!empty($data)) {

		DB::table('users')

		->where('id', $user_id)

		->update(['name' => trim($this->param['fname']),

			'lname' => trim($this->param['lname']),

			'user_address'=>  trim($data['address']),

			'user_city'=>  trim($data['city']),

			'user_zipcode'=>  trim($data['postcode'])

		]);

						 /*,

						  'dummy1'=>trim($this->param['dummy1']),

						  'dummy2'=> trim($this->param['dummy2']),

						  'dummy3'=> trim($this->param['dummy3'])*/

						  $user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

						  $response['message'] = "Profile is successfully updated";

						  $response['ok'] = 1;

						  $response['data'] = array('user_detail'=>$user_data);

						}

						header('Content-type: application/json');

						echo json_encode($response);

					}





					// 30-8-2019



					/************************************************************/

	//	FUNCTION NAME : Device_token

	//	FUNCTION USE :  Store device tokan and other info

	//	FUNCTION ERRORCODE : 80000

					/****************************************************/

					public function device_token (Request $request){

						$data['userid'] = $user_id = Input::get("userid");

						$data['guest_id'] = $guest_id =Input::get("guest_id");

						$data['devicetoken'] = Input::get("devicetoken");

						$data['deviceid'] = Input::get("deviceid");

						$data['devicetype'] = Input::get("devicetype");

						$response = array();

//		if(empty($data['userid']))

						if(	(($user_id==0) || empty($user_id)) && (empty($guest_id)) )

						{

							$response['errorcode'] = "80001";

							$response['ok'] = 0;

							$response['message']="user id /Guest id :Required parameter missing";

						}elseif(empty($data['devicetoken'])){

							$response['errorcode'] = "80002";

							$response['ok'] = 0;

							$response['message']="device token:Required parameter missing";

						}else if(empty($data['deviceid'])){

							$response['errorcode'] = "80003";

							$response['ok'] = 0;

							$response['message']="device id:Required parameter missing";

						}else if(empty($data['devicetype'])){

							$response['errorcode'] = "80004";

							$response['ok'] = 0;

							$response['message']="device type:Required parameter missing";

						}elseif (!empty($data)){

				/*$result = new Device_token;

				$result->userid = trim(Input::get('userid'));

				$result->devicetoken = trim(Input::get('devicetype'));

				$result->deviceid = trim(Input::get('deviceid'));

				$result->devicetype = trim(Input::get('devicetype'));

				$result->save();*/

				/*$user_detail = DB::table('device_token')

							->where('userid', '=' ,trim(Input::get('userid')))

							->where('deviceid', '=' ,trim(Input::get('deviceid')))

							->where('devicetype', '=' ,trim(Input::get('devicetype')))

							->get();*/

							if(Input::get('devicetype')=='android')

							{

								if(($user_id>0) &&(!empty($user_id)))

								{

									$user_detail = DB::table('device_token')

									->where('userid', '=' ,trim($user_id))

									->where('deviceid', '=' ,trim(Input::get('deviceid')))

									->where('devicetype', '=' ,trim(Input::get('devicetype')))

									->get();

								}

								elseif(!empty($guest_id))

								{

									$user_detail = DB::table('device_token')

									->where('guest_id', '=' ,trim($guest_id))

									->where('deviceid', '=' ,trim(Input::get('deviceid')))

									->where('devicetype', '=' ,trim(Input::get('devicetype')))

									->get();

								}

								if(count($user_detail)>0)

								{

									if(($user_id>0) &&(!empty($user_id)))

									{

										DB::table('device_token')

										->where('userid', '=' ,trim(Input::get('userid')))

										->where('deviceid', '=' ,trim(Input::get('deviceid')))

										->where('devicetype', '=' ,trim(Input::get('devicetype')))

										->update(['devicetoken' => trim($data['devicetoken'])

									]);

									}

									elseif(!empty($guest_id))

									{

										DB::table('device_token')

										->where('guest_id', '=' ,trim($guest_id))

										->where('deviceid', '=' ,trim(Input::get('deviceid')))

										->where('devicetype', '=' ,trim(Input::get('devicetype')))

										->update(['devicetoken' => trim($data['devicetoken'])

									]);

									}

									$response['message']="device Token Updated sucessfully!";

								}

								else

								{

									$result = new Device_token;

									$result->userid = trim($user_id);

									$result->guest_id = trim($guest_id);

									$result->devicetoken = trim(Input::get('devicetoken'));

									$result->notification_status = trim(1);

									$result->deviceid = trim(Input::get('deviceid'));

									$result->devicetype = trim(Input::get('devicetype'));

									$result->save();

									$response['message']="device Token sucessfully added!";

								}

								$response['ok'] = 1;

							}

							elseif(Input::get('devicetype')=='iphone')

							{

								if(($user_id>0) &&(!empty($user_id)))

								{

									$user_detail = DB::table('device_token')

									->where('userid', '=' ,trim($user_id))

									->where('devicetype', '=' ,trim(Input::get('devicetype')))

									->get();

								}

								elseif(!empty($guest_id))

								{

									$user_detail = DB::table('device_token')

									->where('guest_id', '=' ,trim($guest_id))

									->where('devicetype', '=' ,trim(Input::get('devicetype')))

									->get();

								}

								if(count($user_detail)>0)

								{

									if(($user_id>0) &&(!empty($user_id)))

									{

										DB::table('device_token')

										->where('userid', '=' ,trim(Input::get('userid')))

										->where('devicetype', '=' ,trim(Input::get('devicetype')))

										->update(['devicetoken' => trim($data['devicetoken']),

											'deviceid' => trim(Input::get('deviceid'))

										]);

									}

									elseif(!empty($guest_id))

									{

										DB::table('device_token')

										->where('guest_id', '=' ,trim($guest_id))

										->where('devicetype', '=' ,trim(Input::get('devicetype')))

										->update(['devicetoken' => trim($data['devicetoken']),

											'deviceid' => trim(Input::get('deviceid'))

										]);

									}

									$response['message']="device Token Updated sucessfully!";

								}

								else

								{

									$result = new Device_token;

									$result->userid = trim($user_id);

									$result->guest_id = trim($guest_id);

									$result->devicetoken = trim(Input::get('devicetoken'));

									$result->notification_status = trim(1);

									$result->deviceid = trim(Input::get('deviceid'));

									$result->devicetype = trim(Input::get('devicetype'));

									$result->save();

									$response['message']="device Token sucessfully added!";

								}

								$response['ok'] = 1;

							}

						}

						header('Content-type: application/json');

						echo json_encode($response);

					}









					public function viewpagecontent(){

						$home_content_api = DB::table('home_content')

						->select('*')

						->where('home_id', '=' , '5')

						->get();

						$response['message'] = "View home page content";

						$response['ok'] = 1;

						$response['data'] = array(

							'home_content'=>$home_content_api[0]->home_content

						);

						header('Content-type: application/json');

						echo json_encode($response);

					}









					// 31-8-2019

					/************************************************************/

					//	FUNCTION NAME : Make_favourite

					//	FUNCTION USE :  app user favourite any restaurant

					//	FUNCTION ERRORCODE : 210000

					/****************************************************/

					public function make_favourite()

					{

						$data['user_id'] = $user_id =Input::get("userid");

						$data['rest_id'] = $rest_id = Input::get("rest_id");

						$response = array();

						if(empty($data['user_id'])){

							$response['errorcode'] = "210001";

							$response['ok'] = 0;

							$response['message']="User Id:Required parameter missing";

						}elseif(empty($data['rest_id'])){

							$response['message']="Restaurant id:Required parameter missing";

							$response['ok'] = 0;

							$response['errorcode'] = "210002";

						}elseif(!empty($data)) {

							$user_favourite = DB::table('favourite_restaurant')

							->where('favrest_restid', '=' ,$rest_id)

							->where('favrest_userid', '=' ,$user_id)

							->select('*')

							->orderBy('favrest_userid', 'asc')

							->get();

							if($user_favourite)

							{

								DB::table('favourite_restaurant')

								->where('favrest_restid', '=' ,$rest_id)

								->where('favrest_userid', '=' ,$user_id)

								->update(['favrest_status' => '1'

							]);

							}

							else

							{

								$fav_data = new Favourite_restaurant;

								$fav_data->favrest_restid = trim($rest_id);

								$fav_data->favrest_userid = trim($user_id);

								$fav_data->favrest_status = '1';

								$fav_data->save();

							}

			//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

							$response['message'] = "Restaurant make as Favourite sucessfully.";

							$response['ok'] = 1;

			//$response['data'] = $user_data;

						}

						header('Content-type: application/json');

						echo json_encode($response);

					}







					/************************************************************/

					//	FUNCTION NAME : Remove_favourite

					//	FUNCTION USE :  app user favourite any restaurant

					//	FUNCTION ERRORCODE : 220000

					/****************************************************/

					public function remove_favourite()

					{

						$data['user_id'] = $user_id =Input::get("userid");

						$data['rest_id'] = $rest_id = Input::get("rest_id");

						$response = array();

						if(empty($data['user_id'])){

							$response['errorcode'] = "220001";

							$response['ok'] = 0;

							$response['message']="User Id:Required parameter missing";

						}elseif(empty($data['rest_id'])){

							$response['message']="Restaurant id:Required parameter missing";

							$response['ok'] = 0;

							$response['errorcode'] = "220002";

						}elseif(!empty($data)) {

							$user_favourite = DB::table('favourite_restaurant')

							->where('favrest_restid', '=' ,$rest_id)

							->where('favrest_userid', '=' ,$user_id)

							->select('*')

							->orderBy('favrest_userid', 'asc')

							->get();

							if($user_favourite)

							{

								DB::table('favourite_restaurant')

								->where('favrest_restid', '=' ,$rest_id)

								->where('favrest_userid', '=' ,$user_id)

								->update(['favrest_status' => '0'

							]);

							}

			//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

							$response['message'] = "Restaurant remove as favourite sucessfully.";

							$response['ok'] = 1;

			//$response['data'] = $user_data;

						}

						header('Content-type: application/json');

						echo json_encode($response);

					}



					/************************************************************/

	//	FUNCTION NAME : Restaurant_review

	//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

	//	FUNCTION ERRORCODE : 120000

					/****************************************************/

					public function restaurant_review_listing()

					{

						$data['restaurentid'] =$rest_id = Input::get("restaurentid");

						if(empty($data['restaurentid'])){

							$response['errorcode'] = "120001";

							$response['ok'] = 0;

							$response['message']="Restaurant Id:Required parameter missing";

						}elseif (!empty($data)){

							$rest_listing = DB::table('search_restaurant_view');

							$rest_listing = $rest_listing->select('*');

							$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

							$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

							$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

							$rest_listing = $rest_listing->get();

							if($rest_listing){

								/* COUNT CALCULATION OF RATING  FOR REVIEW /FOOD/ ETC */

								$rest_food_good ='0';

								$rest_delivery_ontime ='0';

								$rest_order_accurate ='0';

								$count_review = DB::table('review')

								->where('re_restid', '=' ,$rest_id)

								->where('re_status', '=' ,'PUBLISHED')

								->orderBy('re_id', 'desc')

								->count();

								if($count_review>0)

								{

									$sql_food = DB::table('review')

									->select(DB::raw("round((count(`re_food_good`) / ".$count_review."),0) as  food_good"))

									->where('re_restid', '=' , $rest_id)

									->where('re_food_good', '=' , '1')

									->get();

									$sql_delivery_ontime = DB::table('review')

									->select(DB::raw("round((count(`re_delivery_ontime`) / ".$count_review."),0) as  delivery_ontime"))

									->where('re_restid', '=' , $rest_id)

									->where('re_delivery_ontime', '=' , '1')

									->get();

									$sql_rder_accurate = DB::table('review')

									->select(DB::raw("round((count(`re_order_accurate`) / ".$count_review."),0) as  order_accurate"))

									->where('re_restid', '=' , $rest_id)

									->where('re_order_accurate', '=' , '1')

									->get();

									$rest_food_good = $sql_food[0]->food_good;

									$rest_delivery_ontime = $sql_delivery_ontime[0]->delivery_ontime;

									$rest_order_accurate = $sql_rder_accurate[0]->order_accurate;

								}

								/* END */

								/*RESTAURNT DETAILDATA ARRAY */

								$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

								$rest_data["vendor_id"] = trim($rest_listing[0]->vendor_id);

								$rest_data["rest_name"] = trim($rest_listing[0]->rest_name);

								$rest_data["rest_metatag"] = trim($rest_listing[0]->rest_metatag);

								$rest_data["rest_logo"] = trim(url('/').'/uploads/reataurant/'.$rest_listing[0]->rest_logo);

								$rest_data["rest_address"] = trim($rest_listing[0]->rest_address);

								$rest_data["rest_address2"] = trim($rest_listing[0]->rest_address2);

								$rest_data["rest_city"] = trim($rest_listing[0]->rest_city);

								$rest_data["rest_zip_code"] = trim($rest_listing[0]->rest_zip_code);

								$rest_data["rest_suburb"] = trim($rest_listing[0]->rest_suburb);

								$rest_data["rest_state"] = trim($rest_listing[0]->rest_state);

								$rest_data["rest_desc"] = trim($rest_listing[0]->rest_desc);

								$rest_data["rest_contact"] = trim($rest_listing[0]->rest_contact);

								$rest_data["rest_service"] = trim($rest_listing[0]->rest_service);

								$rest_data["rest_create"] = trim($rest_listing[0]->rest_create);

								$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

								$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

								$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

								$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

								$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

								$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

								$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

								$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

								$rest_data["rest_cuisine"] = trim($rest_listing[0]->rest_cuisine);

								$rest_data["rest_mindelivery"] = trim($rest_listing[0]->rest_mindelivery);

								$rest_data["rest_landline"] = trim($rest_listing[0]->rest_landline);

								$rest_data["rest_commission"] = trim($rest_listing[0]->rest_commission);

								$rest_data["rest_email"] = trim($rest_listing[0]->rest_email);

								$rest_data["rest_status"] = trim($rest_listing[0]->rest_status);

								$rest_data["rest_classi"] = trim($rest_listing[0]->rest_classi);

								$rest_data["rest_special"] = trim($rest_listing[0]->rest_special);

								$rest_data["rest_delivery_from"] = trim($rest_listing[0]->rest_delivery_from);

								$rest_data["rest_delivery_to"] = trim($rest_listing[0]->rest_delivery_to);

								$rest_data["rest_holiday_from"] = trim($rest_listing[0]->rest_holiday_from);

								$rest_data["rest_holiday_to"] = trim($rest_listing[0]->rest_holiday_to);

								$rest_data["rest_openstatus"] = trim($rest_listing[0]->rest_openstatus);

								$rest_data["rest_banner"] = trim(url('/').'/uploads/reataurant/banner/'.$rest_listing[0]->rest_banner);

								$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

								$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

								$rest_data["created_at"] = trim($rest_listing[0]->created_at);

								$rest_data["updated_at"] = trim($rest_listing[0]->updated_at);

								$totalrating = 0;

								if(!empty($rest_listing[0]->totalrating)){$totalrating = $rest_listing[0]->totalrating; }

								$rest_data["totalrating"] = trim($totalrating);

								$rest_data["totalreview"] = trim($rest_listing[0]->totalreview);

								$rest_data["cuisine_name"] = trim($rest_listing[0]->cuisine_name);

								$rest_data["rest_price_level"] = trim($rest_listing[0]->rest_price_level);

								$rest_data["rest_servicetax"] = trim($rest_listing[0]->rest_servicetax);

								$rest_data["rest_cash_deliver"] = trim($rest_listing[0]->rest_cash_deliver);

								$rest_data["rest_partial_pay"] = trim($rest_listing[0]->rest_partial_pay);

								$rest_partial_percent = 0;

								if($rest_listing[0]->rest_partial_pay==1)

								{

									$rest_partial_percent = $rest_listing[0]->rest_partial_percent;

								}

								$rest_data["rest_partial_percent"] = trim($rest_partial_percent);

								$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

/*$rest_data["rest_food_good"] = trim('92% Food was good');

$rest_data["rest_on_time"] = trim('89% Delivery was on time');

$rest_data["rest_order_accurate"] = trim('98% Order was accurate');*/

$rest_data["rest_food_good"] = trim('');

$rest_data["rest_on_time"] = trim('');

$rest_data["rest_order_accurate"] = trim('');

if($rest_food_good>0){

	$rest_data["rest_food_good"] = trim($rest_food_good.'% Food was good');

}

if($rest_delivery_ontime>0){

	$rest_data["rest_on_time"] = trim($rest_delivery_ontime.'% Delivery was on time');

}

if($rest_order_accurate>0){

	$rest_data["rest_order_accurate"] = trim($rest_order_accurate .'% Order was accurate');

}

$rest_data["rest_min_orderamt"] = trim($rest_listing[0]->rest_min_orderamt);

/* END **/

$limit = 10;

if((Input::get('per_page')))

{

	$limit = Input::get('per_page');

}

$review_data = DB::table('review')

->Join('users', 'review.re_userid', '=', 'users.id')

->select('review.*','users.name','users.lname')

->where('review.re_restid', '=' , $rest_id)

->where('review.re_status', '=' , 'PUBLISHED' )

->orderBy('re_id', 'desc')

->paginate($limit);

if($review_data)

{

	$review_data_list ='';

	foreach($review_data as $listing)

	{

		$review_data_list[]=array(

			're_id' => $listing->re_id,

			're_orderid' => $listing->re_orderid,

			're_userid' => $listing->re_userid,

			're_restid' => $listing->re_restid,

			're_content' => $listing->re_content,

			're_status' => $listing->re_status,

			're_rejectreson' => $listing->re_rejectreson,

			're_rating' => $listing->re_rating,

			'created_at' =>date('d-m-Y',strtotime($listing->created_at)),

			'updated_at' =>date('d-m-Y',strtotime($listing->updated_at)),

			'name' => $listing->name,

			'lname' => $listing->lname

		);

	}

	if($review_data->total()==0)

	{

		$review_data_list =array();

	}

	$review_list['total'] =$review_data->total() ;

	$review_list['per_page'] =$review_data->perPage();

	$review_list['current_page'] =$review_data->currentPage();;

	$review_list['last_page'] =$review_data->lastPage();

	$review_list['data'] =$review_data_list;

	$review_list_counter = $review_data->total();

}

else

{

	$review_list = array();

	$review_list_counter =0;

}

$response['message'] = "Restaurant Detail";

$response['ok'] = 1;

$response['restaurant_detail'] = $rest_data;

$response['review_list'] = $review_list;

$response['review_list_count'] = $review_list_counter;

}

else

{

	$response['errorcode'] = "120002";

	$response['ok'] = 0;

	$response['message']="Restaurant Id: Invalid Restaurant ID";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Food_autosuggest

	//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

	//	FUNCTION ERRORCODE : 180000

/****************************************************/

public function food_rest_search()

{

	DB::enableQueryLog();

	$test_sql = '';

	$keyword = Input::get('search_by');

	$lat ='';

	$long = '';

	$rest_listing = DB::table('food_rest_name_view');

	if(!empty($keyword))

	{

		$rest_listing = $rest_listing->where('food_rest_name_view.rest_food', 'like' , '%'.$keyword.'%');

	}

	$rest_listing = $rest_listing->orderBy('food_rest_name_view.rest_food', 'asc');

	$rest_listing = $rest_listing->groupBy('food_rest_name_view.rest_food');

	$test_sql  = $rest_listing;

	$rest_listing = $rest_listing->get();

		// dd(DB::getQueryLog());

	$total_restaurant =count($rest_listing) ;

	$rest_data = '';

	$open_staus = 0 ;

	if($total_restaurant>0)

	{

		$response['message'] = "All restaurant list";

		$response['ok'] = 1;

		$response['search_by'] = $keyword;

		$response['food_listing'] = $rest_listing;

	}

	else

	{

		$response['message']="No result found!";

		$response['ok'] = 0;

		$response['errorcode'] = "180001";

	}

	header('Content-type: application/json');

	echo json_encode($response);

}





/************************************************************/

	//	FUNCTION NAME : food_search

	//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

	//	FUNCTION ERRORCODE : 190000

/****************************************************/

public function food_search()

{

	DB::enableQueryLog();

	$test_sql = '';

	$rest_id = Input::get('restaurentid');

	$keyword = Input::get('search_by');

	$lat ='';

	$long = '';

	$total_rec_counter=0;

	$food_detail ='';

	$menu_list = DB::table('menu_detail');

	$menu_list = $menu_list->where('menu_detail.restaurant_id', '=' , $rest_id);

	$menu_list = $menu_list->where('menu_detail.menu_status', '=' , '1');

	if(!empty($keyword))

	{

		$menu_list = $menu_list->where('menu_detail.menu_name', 'like' , '%'.$keyword.'%');

	}

	$menu_list = $menu_list->groupBy('menu_detail.menu_id');

	$test_sql  = $menu_list;

	$menu_list = $menu_list->get();

	$total_restaurant =count($menu_list) ;

	if(count($menu_list)>0)

	{

		$total_rec_counter++;

		foreach($menu_list as $mlist)

		{

			$menu_id = $mlist->menu_id;

			$item_listing = DB::table('menu_category');

			$item_listing = $item_listing->where('menu_category.rest_id', '=' , $rest_id);

			$item_listing = $item_listing->where('menu_category.menu_id', '=' , $menu_id);

			$item_listing = $item_listing->where('menu_category.menu_cat_status', '=' , '1');

			$item_listing = $item_listing->groupBy('menu_category.menu_category_id');

			$test_sql  = $item_listing;

			$item_listing = $item_listing->get();

			if(count($item_listing)>0)

			{

				foreach($item_listing as $ilist)

				{

					$price = '0.00';

					if($ilist->menu_category_portion=='no')

					{

						$price = $ilist->menu_category_price;

					}

					$food_detail[]=array('foodRestId'=>$ilist->rest_id,

						'foodMenuId'=>$ilist->menu_id,

						'foodId'=>$ilist->menu_category_id,

						'foodName'=>$ilist->menu_category_name,

						'foodPrice'=>$price

					);

				}

			}

		}

	}

	elseif(count($menu_list)==0)

	{

		$item_listing = DB::table('menu_category');

		$item_listing = $item_listing->where('menu_category.rest_id', '=' , $rest_id);

		if(!empty($keyword))

		{

			$item_listing = $item_listing->where('menu_category.menu_category_name', 'like' , '%'.$keyword.'%');

		}

		$item_listing = $item_listing->where('menu_category.menu_cat_status', '=' , '1');

		$item_listing = $item_listing->groupBy('menu_category.menu_category_id');

		$test_sql  = $item_listing;

		$item_listing = $item_listing->get();

		if(count($item_listing)>0)

		{

			$total_rec_counter++;

			foreach($item_listing as $ilist)

			{

				$price = '0.00';

				if($ilist->menu_category_portion=='no')

				{

					$price = $ilist->menu_category_price;

				}

				$food_detail[]=array('foodRestId'=>$ilist->rest_id,

					'foodMenuId'=>$ilist->menu_id,

					'foodId'=>$ilist->menu_category_id,

					'foodName'=>$ilist->menu_category_name,

					'foodPrice'=>$price

				);

			}

		}

	}

		// dd(DB::getQueryLog());

	$rest_data = '';

	$open_staus = 0 ;

	if($total_rec_counter>0)

	{

		$response['message'] = "All restaurant list";

		$response['ok'] = 1;

		$response['search_by'] = $keyword;

		$response['rest_id'] = $rest_id;

		$response['food_listing'] = $food_detail;

	}

	else

	{

		$response['message']="No result found!";

		$response['ok'] = 0;

		$response['errorcode'] = "190001";

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Change_password

	//	FUNCTION USE :  Update app user password

	//	FUNCTION ERRORCODE : 90000

/****************************************************/

public function change_password()

{

	$user_id = Input::get('userid');

	$data['user_id'] = Input::get("userid");

	$data['newpassword'] = Input::get("newpassword");

	$data['oldpassword'] = Input::get("oldpassword");

	$response = array();

	if(empty($data['oldpassword'])){

		$response['errorcode'] = "50001";

		$response['ok'] = 0;

		$response['message']="Old password:Required parameter missing";

	}elseif(empty($data['newpassword'])){

		$response['message']="New password:Required parameter missing";

		$response['ok'] = 0;

		$response['errorcode'] = "50002";

	}elseif(empty($data['user_id'])){

		$response['message']="user id:Required parameter missing";

		$response['ok'] = 0;

		$response['errorcode'] = "50003";

	}elseif(!empty($data)) {

		$credentials = ['password' => trim(Input::get('oldpassword')),'id' =>  $user_id];

		if( Auth::attempt($credentials))

		{

			DB::table('users')

			->where('id', $user_id)

			->update(['password' =>  Hash::make(trim(Input::get('newpassword'))),

				'user_pass'=> md5(trim(Input::get('newpassword')))

			]);

			$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

			$response['message'] = "Password updated sucessfully.";

			$response['ok'] = 1;

			$response['data'] = $user_data;

		}

		else

		{

			$response['message']="Old Password Not Match";

			$response['ok'] = 0;

			$response['errorcode'] = "50004";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Favourite_list

	//	FUNCTION USE :  app user favourite any restaurant

	//	FUNCTION ERRORCODE : 230000

/****************************************************/

public function favourite_list()

{

	$data['user_id'] = $user_id =Input::get("userid");

	$response = array();

	if(empty($data['user_id'])){

		$response['errorcode'] = "230001";

		$response['ok'] = 0;

		$response['message']="User Id:Required parameter missing";

	}elseif(!empty($data)) {

		$limit = 10;

			/*if((Input::get('per_page')))

			{

				$limit = Input::get('per_page');

			}*/

			$user_favourite = DB::table('favourite_restaurant')

			->leftJoin('search_restaurant_view', 'favourite_restaurant.favrest_restid', '=', 'search_restaurant_view.rest_id')

			->where('favrest_userid', '=' ,$user_id)

			->where('favrest_status', '=' ,'1')

			->select('*')

			->orderBy('favrest_userid', 'asc')

			->paginate($limit)	;

			$user_favourite_counter = $user_favourite->total();

								//->get();

			if($user_favourite_counter>0)

			{

				$rest_data ='';

				foreach($user_favourite as $listing)

				{

					$rating = 0;

					if($listing->totalrating=='NULL')

					{

						$rating = 0;

					}

					else

					{

						$rating = round($listing->totalrating);

					}

					$open_staus = 1 ;// Calcualtion reamning

					$dist_km = '';

					if(@$listing->distance)

					{

						$dist_km = $listing->distance;

					}

					/** FAVOURIT OPTION **/

					$rest_favourite= 1;

					/**  END   ***/

					$cuisine_name = '';

					if(!empty($listing->cuisine_name))

					{

						$cuisine_name =	$listing->cuisine_name;

					}

					$rest_data[] = array(

						'rest_id' => $listing->rest_id,

						'rest_name' => $listing->rest_name,

						'rest_metatag' => $listing->rest_metatag,

						'rest_logo' => trim(url('/').'/uploads/reataurant/'.$listing->rest_logo),

						'rest_address' => $listing->rest_address,

						'rest_address2' => $listing->rest_address2,

						'rest_city' => $listing->rest_city,

						'rest_zip_code' => $listing->rest_zip_code,

						'rest_suburb' => $listing->rest_suburb,

						'rest_state' => $listing->rest_state,

						'rest_service'=>$listing->rest_service,

						'rest_mindelivery'=>$listing->rest_mindelivery,

						'rest_status'=>$listing->rest_status,

						'rest_email'=>$listing->rest_email,

						'cuisine_name'=>$cuisine_name,

						'total_review'=>$listing->totalreview,

						'total_rating'=>$rating,

						'distance'=>$dist_km,

						'open_status'=>$open_staus,

						'rest_favourite'=>$rest_favourite,

						'rest_min_orderamt' => trim($listing->rest_min_orderamt)

					);

				}

				$rest_detail_format['total'] =$user_favourite->total() ;

				$rest_detail_format['per_page'] =$user_favourite->perPage();

				$rest_detail_format['current_page'] =$user_favourite->currentPage();;

				$rest_detail_format['last_page'] =$user_favourite->lastPage();

				$rest_detail_format['data'] =$rest_data;

				$response['message'] = "Restaurant List.";

				$response['ok'] = 1;

				$response['restaurant_list'] =  $rest_detail_format;

			}

			else

			{

				$response['message']="No favourite restaurant list found ";

				$response['ok'] = 0;

				$response['errorcode'] = "230003";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : Add_order_review

	//	FUNCTION USE :  app user favourite any restaurant

	//	FUNCTION ERRORCODE : 240000

	/****************************************************/

	public function post_order_review()

	{

		$data['user_id'] = $user_id =Input::get("userid");

		$data['order_id'] = $order_id =Input::get("order_id");

		$data['review_text'] = $review_text =Input::get("review_text");

		$data['rest_id'] = $rest_id =Input::get("rest_id");

		$data['rating'] = $rating =Input::get("rating");

		$rate_food = Input::get("rate_food");

		$rate_delviery =Input::get("rate_delviery");

		$rate_order = Input::get("rate_order");

		/*}elseif(empty($data['review_text'])){

			$response['errorcode'] = "240003";

			$response['ok'] = 0;

			$response['message']="Review:Required parameter missing";*/

			$response = array();

			if(empty($data['user_id'])){

				$response['errorcode'] = "240001";

				$response['ok'] = 0;

				$response['message']="User Id:Required parameter missing";

			}elseif(empty($data['order_id'])){

				$response['errorcode'] = "240002";

				$response['ok'] = 0;

				$response['message']="Order Id:Required parameter missing";

			}elseif(!empty($data)) {

				$order_detail  =  DB::table('order')

				->where('user_id', '=' ,$user_id)

				->where('order_id', '=' ,$order_id)

				->where('order_status', '=' ,'5')

				->select('*')

				->orderBy('order.order_id', 'desc')

				->get();

				if(count($order_detail )>0)

				{

				//print_r($order_detail);

					$rest_id = $order_detail[0]->rest_id;

					$review_counter  =  DB::table('review')

					->where('re_userid', '=' ,$user_id)

					->where('re_orderid', '=' ,$order_id)

					->select('*')

					->orderBy('review.re_orderid', 'desc')

					->get();

					if(count($review_counter)>0){

						$response['errorcode'] = "240006";

						$response['ok'] = 0;

						$response['message']="You post review on this order!";

					}

					else

					{

						$order_re = new Review;

						$order_re->re_orderid = $order_id;

						$order_re->re_userid = $user_id;

						$order_re->re_content = $review_text ;

						$order_re->re_restid = $rest_id;

						$order_re->re_rating = $rating;

						$order_re->re_food_good = $rate_food;

						$order_re->re_delivery_ontime = $rate_delviery;

						$order_re->re_order_accurate = $rate_order;

						$order_re->re_status = 'SUBMIT';

						$order_re->save();

						$re_id = $order_re->re_id;

						$response['message'] = "Your Review Submited!";

						$response['ok'] = 1;

					}

				}

				else

				{

					$response['errorcode'] = "240005";

					$response['ok'] = 0;

					$response['message']="Your order is not completed!";

				}

			}

			header('Content-type: application/json');

			echo json_encode($response);

		}

		/************************************************************/

	//	FUNCTION NAME : Show_order_listing

	//	FUNCTION USE :  app user favourite any restaurant

	//	FUNCTION ERRORCODE : 250000

		/****************************************************/

		public function show_order_listing()

		{

			$data['user_id'] = $user_id =Input::get("userid");

			$response = array();

			if(empty($data['user_id'])){

				$response['errorcode'] = "250001";

				$response['ok'] = 0;

				$response['message']="User Id:Required parameter missing";

			}elseif(!empty($data)) {

				$limit = 10;

			/*if((Input::get('per_page')))

			{

				$limit = Input::get('per_page');

			}*/

			$order_detail  =  DB::table('order')

			->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

			->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

			->where('user_id', '=' ,$user_id)

			->where('order.order_status', '!=' ,'2')

			->select('*')

			->orderBy('order.order_id', 'desc')

			->groupBy('order.order_id')

			->paginate($limit)	;

			$order_counter = $order_detail->total();

			if(count($order_detail )>0)

			{

				$order_detail_list = '';

				foreach($order_detail as $order_list)

				{

					$logo = '';

					if(!empty($order_list->rest_logo)){

						$logo =  url('/').'/uploads/reataurant/'.$order_list->rest_logo;

					}

					else

					{

						$logo =  url('/').'design/front/img/logo.png';

					}

					$order_menu = '';

					$cart_price = 0;

					$order_deliveryfee	= '0.00';

					if($order_list->order_type=='Delivery')

					{

						$order_deliveryfee	= number_format($order_list->order_deliveryfee,2);

					}

					$Food_name='';

					if(isset($order_list->order_carditem) && (count($order_list->order_carditem)))

					{

						$order_carditem = json_decode($order_list->order_carditem, true);

						foreach($order_carditem as $item)

						{

							$Food_name .= $item['name'].'<br>';

							$cart_price = $cart_price+$item['price'];

							$order_menu .= $item['qty'].' x '.$item['name'].', ';

							if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

							{

								foreach($item['options']['addon_data'] as $addon)

								{

									$Food_name .=$addon['name'].'<br>';

									$cart_price = $cart_price+$addon['price'];

									$order_menu .= $addon['name'];

								}

							}

						}

					}

					//$total_amt = number_format(($cart_price+$order_deliveryfee),2);

					$total_amt = number_format($order_list->order_total,2);

					$order_status_text='';

					if($order_list->order_status=='1') { $order_status_text='Order Submit';}

					if($order_list->order_status=='2') { $order_status_text='Cancelled'; }

					if($order_list->order_status=='3') { $order_status_text='Sent to Partner'; }

					if($order_list->order_status=='4') { $order_status_text='Partner Confirmed'; }

					if($order_list->order_status=='5') { $order_status_text='Partner Completed'; }

					if($order_list->order_status=='6') { $order_status_text='Reject'; }

					if($order_list->order_status=='7') { $order_status_text='Review Completed'; }

					$review_post=0;

					if(!empty($order_list->re_id))

					{

						$review_post=1;

					}

					$order_detail_list[]= array(

						'order_id'=>$order_list->order_id,

						'order_uniqueid'=>$order_list->order_uniqueid,

						'user_id'=>$user_id,

						'rest_id'=>$order_list->rest_id,

						'rest_name'=>$order_list->rest_name,

						'food_name'=>$Food_name,

						'rest_logo'=>$logo ,

						'order_date'=>date('d-m-Y',strtotime($order_list->order_create)),

						'order_type'=>$order_list->order_type,

						'order_status'=>$order_list->order_status,

						'order_status_text'=>$order_status_text,

						'order_deliveryfee'=>$order_deliveryfee,

						'order_subtotal'=> number_format($cart_price,2),

						'order_total'=> $total_amt,

						'review_flag'=>$review_post

					);

				}

				$order_detail_format['total'] =$order_detail->total() ;

				$order_detail_format['per_page'] =$order_detail->perPage();

				$order_detail_format['current_page'] =$order_detail->currentPage();;

				$order_detail_format['last_page'] =$order_detail->lastPage();

				$order_detail_format['data'] =$order_detail_list;

				$response['message'] = "Order List.";

				$response['ok'] = 1;

				$response['order_list'] =  $order_detail_format;

			}

			else

			{

				$response['errorcode'] = "250002";

				$response['ok'] = 0;

				$response['message']="Order not found";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : Food_removecart

	//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

	//	FUNCTION ERRORCODE : 260000

	/****************************************************/

	public function food_removecart()

	{

		$data['user_id'] = Input::get("user_id");

		$data['guest_id'] = Input::get("guest_id");

		$data['rest_id'] = Input::get("rest_id");

		$data['menu_id'] = Input::get("menu_id");

		$data['menu_catid'] = Input::get("menu_catid");

		$data['deviceid'] = Input::get("deviceid");

		$data['devicetype'] = Input::get("devicetype");

		if(empty($data['rest_id'])){

			$response['errorcode'] = "260001";

			$response['ok'] = 0;

			$response['message']="Restaurant Id:Required parameter missing";

		}elseif(empty($data['deviceid'])){

			$response['errorcode'] = "260002";

			$response['ok'] = 0;

			$response['message']="Device Id:Required parameter missing";

		}elseif(empty($data['devicetype'])){

			$response['errorcode'] = "260003";

			$response['ok'] = 0;

			$response['message']="Device type:Required parameter missing";

		}elseif(!empty($data)){

			$cart_listing = DB::table('temp_cart');

			$cart_listing = $cart_listing->select('*');

			$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

			$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

			$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

			$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

			$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

			$cart_listing = $cart_listing->get();

			if($cart_listing)

			{

				DB::table('temp_cart')

				->where('user_id', '=' ,trim($this->param['user_id']))

				->where('guest_id', '=' ,trim($this->param['guest_id']))

				->where('deviceid', '=' ,trim($this->param['deviceid']))

				->where('rest_id', '=' ,trim($this->param['rest_id']))

				->where('deviceid', '=' ,trim($this->param['deviceid']))

				->where('devicetype', '=' ,trim($this->param['devicetype']))

				->delete();

				$response['message'] = "Cart  Deleted.";

				$response['ok'] = 1;

			}

			else

			{

				$response['errorcode'] = "26000";

				$response['ok'] = 0;

				$response['message']="Invalide User Id OR Device Id";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : update_notification_status

	//	FUNCTION USE :  USER NOTIFICATION STAUTS

	//	FUNCTION ERRORCODE : 270000

	/****************************************************/

	public function update_notification_status()

	{

		/*print_r($this->param);

		exit;*/

		DB::enableQueryLog();

		$data['userid'] = $user_id =Input::get("userid");

		$data['guest_id'] = $guest_id =Input::get("guest_id");

		$data['status'] = Input::get("status");

		$data['deviceid'] = Input::get("deviceid");

		$data['devicetype'] = Input::get("devicetype");

	//	if(empty($data['user_id']))

		if( (($user_id==0) || empty($user_id)) && (empty($guest_id))	)

		{

			$response['errorcode'] = "270001";

			$response['ok'] = 0;

			$response['message']="User Id/Guest Id:Required parameter missing";

		}elseif(($data['status']=='') || ($data['status']<0)){

			$response['errorcode'] = "270002";

			$response['ok'] = 0;

			$response['message']="Notofication Status:Required parameter missing";

		}elseif(empty($data['deviceid'])){

			$response['errorcode'] = "270003";

			$response['ok'] = 0;

			$response['message']="Device Id:Required parameter missing";

		}elseif(empty($data['devicetype'])){

			$response['errorcode'] = "270004";

			$response['ok'] = 0;

			$response['message']="Device type:Required parameter missing";

		}elseif(!empty($data)){

			$device_listing = DB::table('device_token');

			$device_listing = $device_listing->select('*');

			if(($user_id>0) &&(!empty($user_id)))

			{

				$device_listing = $device_listing->where('userid', '=',Input::get("userid"));

			}elseif(!empty($guest_id))

			{

				$device_listing = $device_listing->where('guest_id','=', trim($guest_id));

			}

			if(Input::get("devicetype")=='android')

			{

				$device_listing = $device_listing->where('deviceid', '=',Input::get("deviceid"));

			}

			$device_listing = $device_listing->where('devicetype','=', Input::get("devicetype"));

			$device_listing = $device_listing->get();

		//dd(DB::getQueryLog());

			if(count($device_listing)>0)

			{

		/*		DB::table('device_token')

					->where('userid', Input::get("userid"))

					->where('deviceid', Input::get("deviceid"))

					->where('devicetype', Input::get("devicetype"))

					->update(['notification_status' => trim(Input::get("status"))

				]);*/

				if(($user_id>0) &&(!empty($user_id)))

				{

						//->where('deviceid', Input::get("deviceid"))

					DB::table('device_token')

					->where('userid', Input::get("userid"))

					->where('devicetype', Input::get("devicetype"))

					->update(['notification_status' => trim(Input::get("status"))

				]);

				}elseif(!empty($guest_id))

				{

						//->where('deviceid', Input::get("deviceid"))

					DB::table('device_token')

					->where('guest_id', trim($guest_id))

					->where('devicetype', Input::get("devicetype"))

					->update(['notification_status' => trim(Input::get("status"))

				]);

				}

				$device_data = DB::table('device_token');

				if(($user_id>0) &&(!empty($user_id)))

				{

					$device_data = $device_data->where('userid', Input::get("userid"));

				}elseif(!empty($guest_id))

				{

					$device_data = $device_data->where('guest_id', '=' ,trim($guest_id));

				}

						//$device_data = $device_data->where('deviceid', Input::get("deviceid"));

				$device_data = $device_data->where('devicetype', Input::get("devicetype"));

				$device_data = $device_data->get();

				$response['message'] = "Notification status successfully updated.";

				$response['ok'] = 1;

				$response['data'] = array('user_detail'=>$device_data);

			}

			else

			{

				$response['errorcode'] = "270005";

				$response['ok'] = 0;

				$response['message']="This user not registered";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : Make_food_favourite

	//	FUNCTION USE :  app user favourite any restaurant food

	//	FUNCTION ERRORCODE : 280000

	/****************************************************/

	public function make_food_favourite()

	{

		$data['user_id'] = $user_id =Input::get("userid");

		$data['rest_id'] = $rest_id = Input::get("rest_id");

		$data['food_id'] = $food_id = Input::get("food_id");

		$response = array();

		if(empty($data['user_id'])){

			$response['errorcode'] = "280001";

			$response['ok'] = 0;

			$response['message']="User Id:Required parameter missing";

		}elseif(empty($data['rest_id'])){

			$response['message']="Restaurant id:Required parameter missing";

			$response['ok'] = 0;

			$response['errorcode'] = "280002";

		}elseif(empty($data['rest_id'])){

			$response['message']="Food id:Required parameter missing";

			$response['ok'] = 0;

			$response['errorcode'] = "280003";

		}elseif(!empty($data)) {

			$user_favourite = DB::table('favourite_food')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->where('favrest_menucatid', '=' ,$food_id)

			->select('*')

			->orderBy('favrest_userid', 'asc')

			->get();

			if($user_favourite)

			{

				DB::table('favourite_food')

				->where('favrest_restid', '=' ,$rest_id)

				->where('favrest_userid', '=' ,$user_id)

				->where('favrest_menucatid', '=' ,$food_id)

				->update(['favrest_status' => '1'

			]);

			}

			else

			{

				$fav_data = new Favourite_food;

				$fav_data->favrest_restid = trim($rest_id);

				$fav_data->favrest_userid = trim($user_id);

				$fav_data->favrest_menucatid = trim($food_id);

				$fav_data->favrest_status = '1';

				$fav_data->save();

			}

			//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

			$response['message'] = "Restaurant food make as Favourite sucessfully.";

			$response['ok'] = 1;

			//$response['data'] = $user_data;

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}



	/************************************************************/

	//	FUNCTION NAME : show_contentpage_link

	//	FUNCTION USE :  Show all content pages

	//	FUNCTION ERRORCODE : 300000

	/****************************************************/

	public function show_contentpage_link()

	{

		$response['message'] = "check all pages link";

		$response['page_data'] =array(

			'aboutus'=>url('/').'/show_about',

			'contactus'=>url('/').'/show_contact',

			'termsconditions'=>url('/').'/show_terms_condition',

			'privacypolicy'=>url('/').'/show_privacy_policy',

		);

		$response['ok'] = 1;

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : food_add_removecart

	//	FUNCTION USE :  Remove all old restaurnt item and insert new

	//	FUNCTION ERRORCODE : 310000

	/****************************************************/

	public function food_add_removecart()

	{

		$data['user_id'] =$rest_id = Input::get("user_id");

		$data['guest_id'] =$rest_id = Input::get("guest_id");

		$data['rest_id'] =$rest_id = Input::get("rest_id");

		$data['menu_id'] =$rest_id = Input::get("menu_id");

		$data['menu_catid'] =$rest_id = Input::get("food_id");

		$data['menu_subcatid'] =$rest_id = Input::get("food_size_id");

		$data['menu_addonid'] =$rest_id = Input::get("food_addonid");

		$data['qty'] =$rest_id = Input::get("qty");

		$data['deviceid'] =$rest_id = Input::get("deviceid");

		$data['devicetype'] =$rest_id = Input::get("devicetype");

		if(empty($data['rest_id'])){

			$response['errorcode'] = "310001";

			$response['ok'] = 0;

			$response['message']="Restaurant Id:Required parameter missing";

		}elseif(empty($data['menu_id'])){

			$response['errorcode'] = "310002";

			$response['ok'] = 0;

			$response['message']="Menu Id:Required parameter missing";

		}elseif(empty($data['menu_catid'])){

			$response['errorcode'] = "310003";

			$response['ok'] = 0;

			$response['message']="Food Id:Required parameter missing";

		}elseif(empty($data['qty'])){

			$response['errorcode'] = "310004";

			$response['ok'] = 0;

			$response['message']="Food Quantity:Required parameter missing";

		}elseif(empty($data['deviceid'])){

			$response['errorcode'] = "310005";

			$response['ok'] = 0;

			$response['message']="Device Id:Required parameter missing";

		}elseif(empty($data['devicetype'])){

			$response['errorcode'] = "310006";

			$response['ok'] = 0;

			$response['message']="Device type:Required parameter missing";

		}elseif(!empty($data)){

			DB::table('temp_cart')

			->where('user_id', '=' ,trim($this->param['user_id']))

			->where('guest_id', '=' ,trim($this->param['guest_id']))

			->where('rest_id', '!=' ,trim($this->param['rest_id']))

			->delete();

			$rest_cartid = '';

			$rest_cartid = (date('ymdHis'));

			$cart_data = new Temp_cart;

			$cart_data->user_id	 = trim($this->param['user_id']);

			$cart_data->rest_cartid	 = trim($rest_cartid);

			$cart_data->guest_id = trim($this->param['guest_id']);

			$cart_data->rest_id = trim($this->param['rest_id']);

			$cart_data->menu_id = trim($this->param['menu_id']);

			$cart_data->menu_catid = trim($this->param['food_id']);

			$cart_data->menu_subcatid = trim($this->param['food_size_id']);

			$cart_data->menu_addonid = trim($this->param['food_addonid']);

			$cart_data->qty = trim($this->param['qty']);

			$cart_data->deviceid = trim($this->param['deviceid']);

			$cart_data->devicetype= trim($this->param['devicetype']);

			$cart_data->save();

			$response['message'] = "Item add in cart sucessfully";

			$response['ok'] = 1;

			$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}

	/************************************************************/

	//	FUNCTION NAME : show_soical_login

	//	FUNCTION USE :  Remove all old restaurnt item and insert new

	//	FUNCTION ERRORCODE : 320000

	/****************************************************/

	public function show_soical_login()

	{

		$data['emailid'] = '';

		$data['mobileno'] = '';

		$data['socialtype'] = '';

		$data['deviceid'] = '';

		$data['devicetype'] = '';

		if(isset($this->param["emailid"]))

		{

			$data['emailid'] = $this->param["emailid"];

		}

		if(isset($this->param["mobileno"]))

		{

			$data['mobileno'] =  $this->param["mobileno"];

		}

		$response = array();

		if( (empty($data['emailid'])) && (empty($data['mobileno'])))

		{

			$response['errorcode'] = "320001";

			$response['ok'] = 0;

			$response['message']="Mobile Number or Email Id  Required parameter ";

		}elseif (!empty($data)) {

			$user_detail_counter = 0;

			$user_detail_email_counter = 0;

			if(!empty($this->param['mobileno']))

			{

				$user_detail_counter = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->count();

			}

			if(!empty($this->param['emailid']))

			{

				$user_detail_email_counter = DB::table('users')->where('email', '=' ,$this->param['emailid'])->count();

			}

			$response='';

			if(($user_detail_counter>0) && (!empty($this->param['mobileno']))){

				$user_detail  = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

				$user_id = $user_detail[0]->id;

				if($user_detail[0]->user_status==1){

					$response['message']='Sign In: Success';

					$response['data'] = $user_detail;

					$response['ok'] = 1;

					$response['Status'] = 1;

				}

				else{

					$otp_code = trim($this->generateRandomString(6));

					$user_id = $user_detail[0]->id;

					$otp_data = new User_otpdetail;

					$otp_data->user_id = trim($user_id);

					$otp_data->user_mob = trim($user_detail[0]->user_mob);

								$otp_data->otp_number =  $otp_code; //trim('123456');

								$otp_data->save();

								/*********** EMAIL FOR SEN OTP START *************/

								$full_name =  $user_detail[0]->user_fname.' '.$user_detail[0]->user_lname;

								$msg_reg_otp = 'Congrats, '.trim($full_name).'! you are registered/verified succesfully, The verification code for registration on grambunny is :'.$otp_code ;

								Mail::raw($msg_reg_otp, function ($message){

					//$message->to('contact@contact.com');

									$message->from('info@grambunny.com', 'grambunny');

									$otp_email = trim($user_detail[0]->user_mob).$carrier_email;

									$message->to($otp_email);

									$message->bcc('votiveshweta@gmail.com');

									$message->bcc('votivemobile.pankaj@gmail.com');

									$message->bcc('votivemobile.dilip@gmail.com');

									$message->bcc('votiveiphone.hariom@gmail.com');

									$message->bcc('zubaer.votive@gmail.com');

									$message->subject('Registration OTP');

								});

								/********** EMAIL FOR SEN OTP END  *************/

								$otp_inserted_id = $otp_data->otp_id;

								$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

								$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

								$response['errorcode'] = "320002";

								$response['ok'] = 2;

								$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

								$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

							}

						}elseif(($user_detail_email_counter>0) && (!empty($this->param['emailid']))){

							$user_detail_email  = DB::table('users')->where('email', '=' ,$this->param['emailid'])->get();

							$user_id = $user_detail_email[0]->id;

							if($user_detail_email[0]->user_status==1){

								$response['message']='Sign In: Success';

								$response['data'] = $user_detail_email;

								$response['ok'] = 1;

								$response['Status'] = 1;

							}

							else{

								$response['errorcode'] = "320003";

								$response['ok'] = 2;

								if(!empty($user_detail_email[0]->user_mob))

								{

									$otp_code = trim($this->generateRandomString(6));

									$user_id = $user_detail_email[0]->id;

									$otp_data = new User_otpdetail;

									$otp_data->user_id = trim($user_id);

									$otp_data->user_mob = trim($user_detail_email[0]->user_mob);

								$otp_data->otp_number =  $otp_code; //trim('123456');

								$otp_data->save();

								/*********** EMAIL FOR SEN OTP START *************/

								$full_name =  $user_detail_email[0]->user_fname.' '.$user_detail_email[0]->user_lname;

								$msg_reg_otp = 'Congrats, '.trim($full_name).'! you are registered/verified succesfully, The verification code for registration on grambunny is :'.$otp_code ;

								Mail::raw($msg_reg_otp, function ($message){

								//$message->to('contact@contact.com');

									$message->from('info@grambunny.com', 'grambunny');

									$otp_email = trim($user_detail_email[0]->user_mob).$carrier_email;

									$message->to($otp_email);

									$message->bcc('votiveshweta@gmail.com');

									$message->bcc('votivemobile.pankaj@gmail.com');

									$message->bcc('votivemobile.dilip@gmail.com');

									$message->bcc('votiveiphone.hariom@gmail.com');

									$message->bcc('zubaer.votive@gmail.com');

									$message->subject('Registration OTP');

								});

								/********** EMAIL FOR SEN OTP END  *************/

								$otp_inserted_id = $otp_data->otp_id;

								$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

								$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

								$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

							}

							$response['message'] ='Sorry.Your account is not active. Please verify your mobile number via OTP.';

						}

					}elseif(($user_detail_counter==0) && ($user_detail_email_counter==0)){

						$response['message']='User not exist!';

						$response['ok'] = 1;

						$response['Status'] = 0;

					}

				}

				header('Content-type: application/json');

				echo json_encode($response);

			}

			/************************************************************/

	//	FUNCTION NAME : show_soical_login

	//	FUNCTION USE :  Remove all old restaurnt item and insert new

	//	FUNCTION ERRORCODE : 330000

			/****************************************************/

			public function show_restaurant_open_05_jan_2018()

			{

		//print_r( $this->param);

		//exit;

				$data['rest_id'] =$rest_id = Input::get("rest_id");

				$data['userid'] = $user_id = Input::get("userid");

				$data['guest_id'] = $guest_id = Input::get("guest_id");

				$data['deviceid'] = $deviceid = Input::get("deviceid");

				$data['devicetype'] = $devicetype = Input::get("devicetype");

				$data['deli_date'] = $deli_date = Input::get("date");

				$data['deli_time'] = $deli_time = Input::get("time");

				if(empty($data['rest_id'])){

					$response['errorcode'] = "330001";

					$response['ok'] = 0;

					$response['message']="Restaura

					d:Required parameter missing";

				}elseif(empty($data['deli_date'])){

					$response['errorcode'] = "330002";

					$response['ok'] = 0;

					$response['message']="Date : Required parameter missing";

				}elseif(empty($data['deli_time'])){

					$response['errorcode'] = "330003";

					$response['ok'] = 0;

					$response['message']="Time : Required parameter missing";

				}elseif (!empty($data)){

					$rest_listing = DB::table('restaurant');

					$rest_listing = $rest_listing->select('*');

					$rest_listing = $rest_listing->where('rest_status', '!=' , 'INACTIVE');

					$rest_listing = $rest_listing->where('rest_id', '=' ,$rest_id);

					$rest_listing = $rest_listing->groupBy('rest_id');

					$rest_listing = $rest_listing->get();

					$cart_item_count='0';

					$rest_cart_id='';

					/*RESTAURNT DETAILDATA ARRAY */

					$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

					$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

					$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

					$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

					$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

					$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

					$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

					$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

					$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

					$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

					$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

					$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

$open_status = '0'; //0-open 1-closed

$open_time = '';

$day_name =  strtolower(date("D",strtotime($deli_date)));

$date_time = $deli_date.' '.$deli_time;

$current_time = $tt=  strtotime($date_time);

if($day_name=='sun')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_sat)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_sat);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_sun);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_sat)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_sat);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='mon')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_sun)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_sun);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_mon);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_sun)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_sun);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='tue')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_mon)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_mon);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_tues);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_mon)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_mon);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='wed')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_tues)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_wed);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_tues)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='thu')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_wed)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_wed);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_thus);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_wed)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_wed);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='fri')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_tues)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_fri);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_tues)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

if($day_name=='sat')

{

	if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

	{

		if(!empty($rest_listing[0]->rest_fri)){

			$rest_time2 = explode('_',$rest_listing[0]->rest_fri);

			$start_time = date("H:i", (strtotime($rest_time2[0])));

			$end_time = date("H:i", (strtotime($rest_time2[1])));

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

				$s = $new_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

	else

	{

		$rest_time1 = explode('_',$rest_listing[0]->rest_sat);

		$start_time = date("H:i", (strtotime($rest_time1[0])));

		$end_time = date("H:i", (strtotime($rest_time1[1])));

		if($tt<strtotime($deli_date.' '.$start_time))

		{

			if(!empty($rest_listing[0]->rest_fri)){

				$rest_time2 = explode('_',$rest_listing[0]->rest_fri);

				$start_time = date("H:i", (strtotime($rest_time2[0])));

				$end_time = date("H:i", (strtotime($rest_time2[1])));

				if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

				{

					$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

					$s = $new_date.' '.$start_time;

					$e = $deli_date.' '.$end_time;

					$daytime1 =strtotime($s);

					$daytime3 = strtotime($e);

				}

			}

		}

		else

		{

			if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

			{

				$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

				$s = $deli_date.' '.$start_time;

				$e = $new_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

			else

			{

				$s = $deli_date.' '.$start_time;

				$e = $deli_date.' '.$end_time;

				$daytime1 =strtotime($s);

				$daytime3 = strtotime($e);

			}

		}

	}

}

$currentTime = $tt;

if( ((!empty($daytime1) && $currentTime > $daytime1)) && ((!empty($daytime3) && $currentTime < $daytime3)) ){

	$open_status = 0;

	$status_msg= "Restaurant is open";

}

else

{

	$open_status = 1;

	$status_msg= "Restaurant is close";

}

/** PROMOTIONS CODE END  **/

$rest_data['open_status']=$open_status;

$rest_data['open_time']=$open_time;

$rest_data['status_msg']=$status_msg;

		///print_r($rest_data);

		//exit;

if($rest_listing){

	$response['message'] = "Restaurant OPEN Detail";

	$response['ok'] = 1;

			$response['restaurant_detail'] = $rest_data;//$rest_listing;

		}

		else

		{

			$response['errorcode'] = "110002";

			$response['ok'] = 0;

			$response['message']="Restaurant Id: Invalid Restaurant ID";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}





/************************************************************/

	//	FUNCTION NAME : Restaurant_review

	//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

	//	FUNCTION ERRORCODE : 130000

/****************************************************/

public function restaurant_food_detail()

{

	DB::enableQueryLog();

	$data['restaurentid'] = $rest_id = Input::get("restaurentid");

	$data['food_menuid'] = $menu_id = Input::get("food_menuid");

	$data['foodid'] = $menu_cate_id = Input::get("foodid");

	$data['user_id'] = $user_id = Input::get("userid");

	if(empty($data['foodid'])){

		$response['errorcode'] = "130001";

		$response['ok'] = 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif(empty($data['restaurentid'])){

		$response['errorcode'] = "130002";

		$response['ok'] = 0;

		$response['message']="Food MenuId:Required parameter missing";

	}elseif(empty($data['restaurentid'])){

		$response['errorcode'] = "130003";

		$response['ok'] = 0;

		$response['message']="Food Id:Required parameter missing";

	}elseif (!empty($data)){

		$fav_status = '0';

		if($user_id>0)

		{

			$fav_listing = DB::table('favourite_food')

			->select('*')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->where('favrest_menucatid', '=' ,$menu_cate_id)

			->get();

			if($fav_listing)

			{

				$fav_status = $fav_listing[0]->favrest_status;

			}

		}

		$rest_listing = DB::table('search_restaurant_view');

		$rest_listing = $rest_listing->select('*');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

		$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

		$rest_listing = $rest_listing->get();

		if($rest_listing){

			$menu_list = DB::table('menu')

			->where('restaurant_id', '=' ,$rest_id)

			->where('menu_id', '=' ,$menu_id)

			->where('menu_status', '=' ,'1')

			->orderBy('menu_order', 'asc')

			->get();





			// echo "<pre>";

			// print_r($menu_list);

			// echo "</pre>";

			// die;





				// dd(DB::getQueryLog());

			$menu_array = '';

			$food_array='';

			$popular_item = '';

			$popular_item_count =0;

			$popular_item_count_11 =0;

			if(!empty($menu_list))

			{

				$popular_food_detail = '';

				$popular_food_data='';

				foreach($menu_list as $mlist)

				{

					$menu_id = $mlist->menu_id;

					$menu_name = $mlist->menu_name;

					$menu_desc = $mlist->menu_desc;

					$menu_detail =$mlist;

					$menu_item_detail = '';

					$menu_item_name = '';

					$menu_item_desc = '';

					$menu_item_price = '';

					$food_detail = '';

					$food_data='';

					$counter_popular =0 ;

					$menu_cat_detail = DB::table('menu_category')

					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

					->where('menu_category.rest_id', '=' ,$rest_id)

					->where('menu_category.menu_id', '=' ,$menu_id)

					->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

					->where('menu_category.menu_cat_status', '=' ,'1')

					->select('menu_category.*','menu_category_item.*')

					->orderBy('menu_category.menu_category_id', 'asc')

					->get();





					// echo "<pre>";

					// print_r($menu_cat_detail);

					// echo "</pre>";

					// die;







					$menu_item_count =count($menu_cat_detail);









					$item_addons = '';

					if(!empty($menu_cat_detail))

					{

						foreach($menu_cat_detail as $menu_item)

						{

							$food_data['food_id'] = $menu_item->menu_category_id;

							$food_data['food_popular'] = $menu_item->menu_cat_popular;

							$food_data['food_diet'] = $menu_item->menu_cat_diet;

							$food_data['food_name'] = $menu_item->menu_category_name;

							$food_data['food_price'] = $menu_item->menu_category_price;

							$food_data['food_desc'] = $menu_item->menu_category_desc;

							$food_data['food_subitem'] = $menu_item->menu_category_portion;

							$food_data['food_subitem_title']='';

							$food_data['food_size_detail']='';

							if($menu_item->menu_category_portion=='yes')

							{

								$food_data['food_subitem_title']='Choose a size';

								$food_size = DB::table('category_item')

								->where('rest_id', '=' ,$rest_id)

								->where('menu_id', '=' ,$menu_id)

								->where('menu_category', '=' ,$menu_item->menu_category_id)

								->where('menu_item_status', '=' ,'1')

								->select('*')

								->orderBy('menu_cat_itm_id', 'asc')

								->groupBy('menu_item_title')

								->get();

								$food_data['food_size_detail']=$food_size;

							}

							else

							{

								$food_data['food_size_detail']=array();

							}

							$group_name= '';

							$group_list = '';

							$ff_popular = '';

							$popular_food_data='';

							$addons = DB::table('menu_category_addon')

							->where('addon_restid', '=' ,$rest_id)

							->where('addon_menuid', '=' ,$menu_id)

							->where('addon_status', '=' ,'1')

							->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

							->select('*')

							->orderBy('addon_id', 'asc')

							->groupBy('addon_groupname')

							->get();

							if(!empty($addons))

							{

								foreach($addons as $ad_list)

								{

									$option_type = 0;

									if(substr($ad_list->addon_groupname, -1)=='*')

									{

										$option_type = 1;

									}

									$group_name[]=$ad_list->addon_groupname;

									$ff['addon_gropname'] = $ad_list->addon_groupname;

									$ff['addon_type'] = $ad_list->addon_option;

									$ff['addon_mandatory_or_not'] = $option_type;

									$addon_group = DB::table('menu_category_addon')

									->where('addon_restid', '=' ,$rest_id)

									->where('addon_menuid', '=' ,$menu_id)

									->where('addon_status', '=' ,'1')

									->where('addon_groupname', '=' ,$ad_list->addon_groupname)

									->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

									->select('*')

									->orderBy('addon_id', 'asc')

									->get();

									$group_list[]=$addon_group;

									$addon_group_list ='';

									foreach($addon_group as $group_list_loop)

									{

											//$addon_group_list[]=array('')

										$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

											'addon_menucatid'=>$group_list_loop->addon_menucatid,

											'addon_groupname'=>$group_list_loop->addon_groupname,

											'addon_option'=>$group_list_loop->addon_option,

											'addon_name'=>$group_list_loop->addon_name,

											'addon_price'=>$group_list_loop->addon_price,

											'addon_status'=>$group_list_loop->addon_status

										);

									}

									$ff['addon_detail'] = $addon_group_list;

									$food_data['food_addon'][]=$ff;

									if($menu_item->menu_cat_popular==1)

									{

										$ff_popular['addon_detail'] = $addon_group_list;

										$ff_popular['addon_gropname'] = $ad_list->addon_groupname;

										$ff_popular['addon_type'] = $ad_list->addon_option;

										$popular_food_data['food_addon'][] = $ff_popular;

									}

								}

							}

							else

							{

								$food_data['food_addon']=array();

								$ff['addon_gropname'] = '';

							}

							$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);

							$food_detail[] = $food_data;

						}

						$promo_list = DB::table('promotion')

						->where('promo_restid', '=' ,$rest_id)

						->where('promo_for', '=' ,'each_food')

						->where('promo_menu_item', '=' ,$menu_cate_id)

						->where('promo_status', '=' ,'1')

						->orderBy('promo_buy', 'desc')

						->groupBy('promo_id')

						->get();

						/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/

						$current_promo='';

						if(($user_id>0) && ($user_id!=''))

						{

							foreach($promo_list as $plist )

							{

								if($plist->promo_for=='each_food')

								{

									$current_promo1 = '';

									if($plist->promo_on=='schedul')

									{

										$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

										$db_day = '';

										if(!empty($plist->promo_day))

										{

											$db_day = explode(',',$plist->promo_day);

										}

										if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

										{

											if(

												($plist->promo_start<=date('Y-m-d')) &&

												($plist->promo_end>=date('Y-m-d')) &&

												(empty($plist->promo_day))

											)

											{

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;

												$current_promo[] = $current_promo1;

											}

											if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

												&& (!empty($plist->promo_day))

												&& (in_array($day_name,$db_day) )

											)

											{

												$array_amt[]=$plist->promo_buy;

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;

												$current_promo[] = $current_promo1;

											}

										}

										elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))

										{

											$db_day = explode(',',$plist->promo_day);

											if(in_array($day_name,$db_day))

											{

												$array_amt[]=$plist->promo_buy;

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;

												$current_promo[] = $current_promo1;

											}

										}

									}

									elseif($plist->promo_on=='qty')

									{

										$current_promo1['promo_id'] = $plist->promo_id;

										$current_promo1['promo_restid'] = $plist->promo_restid;

										$current_promo1['promo_for'] = $plist->promo_for;

										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

										$current_promo1['promo_on'] = $plist->promo_on;

										$current_promo1['promo_desc'] = $plist->promo_desc;

										$current_promo1['promo_mode'] = $plist->promo_mode;

										$current_promo1['promo_buy'] = $plist->promo_buy;

										$current_promo1['promo_value'] = $plist->promo_value;

										$current_promo1['promo_end'] = $plist->promo_end;

										$current_promo1['promo_start'] = $plist->promo_start;

										$current_promo[] = $current_promo1;

									}

								}

							}

							if(empty($current_promo)){$current_promo=array();}

						}

						else

						{

							$current_promo=array();

						}

						/*************************** END ******************************/

						$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'fav_status'=>$fav_status,'food_detail'=>$food_detail,'promo_list'=>$current_promo);

						$response['message'] = "Restaurant Detail";

						$response['ok'] = 1;

						//	$response['restaurant_detail'] = $rest_listing;

							//$response['popular_menu_detal'] = $popular_item;

						$response['menu_detal'] = $food_array;

					}

					else

					{

						$response['errorcode'] = "130005";

						$response['ok'] = 0;

						$response['message']="Food Id: Invalide Food ID";

					}

				}

			}

			else

			{

				$response['errorcode'] = "130004";

				$response['ok'] = 0;

				$response['message']="Menu Id: Invalide Menu ID";

			}

		}

		else

		{

			$response['errorcode'] = "130004";

			$response['ok'] = 0;

			$response['message']="Restaurant Id: Invalid Restaurant ID";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : show_notification

	//	FUNCTION USE :  Show all notification listing

	//	FUNCTION ERRORCODE : 440000

/****************************************************/



function show_notification()

{

	$response = array();

	$data['user_id'] =	$user_id = Input::get("userid");

	$data['guest_id'] = $guest_id = Input::get("guest_id");

	$data['deviceid'] = $deviceid = Input::get("deviceid");

	$data['devicetype'] = $devicetype = Input::get("devicetype");

	if(empty($data['user_id'])){

		$response['errorcode'] = "430001";

		$response['ok'] = 0;

		$response['message']="User Id:Required parameter missing";

	}

	elseif (!empty($data)){

		$limit = 10;

		//$notification_detail = DB::table('notification_list')->where('noti_userid', $user_id)->get();

		$notification_detail = DB::table('notification_list')

		->where('noti_userid', '=' ,$user_id)

		->select('*')

		->orderBy('noti_id', 'desc')

		->paginate($limit)	;

		$notification_counter = $notification_detail->total();

		if($notification_counter>0)

		{

			$notification_data='';

			foreach($notification_detail as $notify)

			{

				$notification_data[] = array(

					'noti_id' => $notify->noti_id,

					'noti_userid' => $notify->noti_userid,

					'noti_guestid' => $notify->noti_guestid,

					'noti_title' => $notify->noti_title,

					'noti_desc' => $notify->noti_desc,

					'noti_deviceid' => $notify->noti_deviceid,

					'noti_read' => $notify->noti_read,

					'created_at' => date("H:i A d-m-Y", (strtotime($notify->created_at))),

					'updated_at' => date("H:i A d-m-Y", (strtotime($notify->updated_at)))

				);

			}

			$rest_detail_format['total'] =$notification_detail->total() ;

			$rest_detail_format['per_page'] =$notification_detail->perPage();

			$rest_detail_format['current_page'] =$notification_detail->currentPage();;

			$rest_detail_format['last_page'] =$notification_detail->lastPage();

			$rest_detail_format['data'] =$notification_data;

					/*for ($i=0; $i < $rest_detail_format['per_page']; $i++) {

						$rest_detail_format['data'][$i]->created_at = date("H:i A d-m-Y", (strtotime($rest_detail_format['data'][$i]->created_at)));

					}*/

					$response['user_id']=$user_id;

					$response['notification_detail']=$rest_detail_format;

					$response['ok'] = 1;

					$response['message']='View Your Order!';

				}

				else

				{

					$response['user_id']=$user_id;

					$response['errorcode'] = "430002";

					$response['ok'] = 0;

					$response['message']='notification list not found!';

				}

			}

			header('Content-type: application/json');

			echo json_encode($response);

		}



		/************************************************************/

	//	FUNCTION NAME : show_restaurant_open

	//	FUNCTION USE :  show restaurant open

	//	FUNCTION ERRORCODE : 350000

		/****************************************************/



		public function show_restaurant_open()

		{

			DB::enableQueryLog();

		//print_r( $this->param);

		//exit;

			$data['rest_id'] =$rest_id = Input::get("rest_id");

			$data['userid'] = $user_id = Input::get("userid");

			$data['guest_id'] = $guest_id = Input::get("guest_id");

			$data['deviceid'] = $deviceid = Input::get("deviceid");

			$data['devicetype'] = $devicetype = Input::get("devicetype");

			$data['deli_date'] = $deli_date = Input::get("date");

			$data['deli_time'] = $deli_time = Input::get("time");

			if(empty($data['rest_id'])){

				$response['errorcode'] = "330001";

				$response['ok'] = 0;

				$response['message']="Restaurant Id:Required parameter missing";

			}elseif(empty($data['deli_date'])){

				$response['errorcode'] = "330002";

				$response['ok'] = 0;

				$response['message']="Date : Required parameter missing";

			}elseif(empty($data['deli_time'])){

				$response['errorcode'] = "330003";

				$response['ok'] = 0;

				$response['message']="Time : Required parameter missing";

			}elseif (!empty($data)){

				$rest_listing = DB::table('restaurant');

				$rest_listing = $rest_listing->select('*');

				$rest_listing = $rest_listing->where('rest_status', '!=' , 'INACTIVE');

				$rest_listing = $rest_listing->where('rest_id', '=' ,$rest_id);

				$rest_listing = $rest_listing->groupBy('rest_id');

				$rest_listing = $rest_listing->get();

				$cart_item_count='0';

				$rest_cart_id='';

				/*RESTAURNT DETAILDATA ARRAY */

				$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

				$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

				$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

				$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

				$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

				$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

				$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

				$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

$rest_data["rest_close"] = '';//trim($rest_listing[0]->rest_close);

$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

$open_status = '0'; //1-open 0-closed 05-jan-2018 New

$open_time = '';

$day_name =  strtolower(date("D",strtotime($deli_date)));

$date_time = $deli_date.' '.$deli_time;

$date_time = $deli_date.' '.date('H:i:s',strtotime($deli_time));

$current_time = $tt=  strtotime($date_time);

$open_detail_listing = DB::table('rest_time');

$open_detail_listing = $open_detail_listing->select('*');

$open_detail_listing = $open_detail_listing->where('rest_id', '=' ,$rest_id);

$open_detail_listing = $open_detail_listing->where('day', '=' ,(date("D",strtotime($deli_date))));

$open_detail_listing = $open_detail_listing->where('open_time', '<=' ,date('H:i:s',strtotime($deli_time)));

$open_detail_listing = $open_detail_listing->where('close_time', '>=' ,date('H:i:s',strtotime($deli_time)));

$open_detail_listing = $open_detail_listing->get();

		// dd(DB::getQueryLog());

if($open_detail_listing)

{

	$open_status = 1;

	$status_msg= "Restaurant is open";

}

else

{

	$open_status = 0;

	$status_msg= "Restaurant is close";

}

/** PROMOTIONS CODE END  **/

$rest_data['open_status']=$open_status;

$rest_data['open_time']=$open_time;

$rest_data['status_msg']=$status_msg;

		///print_r($rest_data);

		//exit;

if($rest_listing){

	$response['message'] = "Restaurant OPEN Detail";

	$response['ok'] = 1;

			$response['restaurant_detail'] = $rest_data;//$rest_listing;

		}

		else

		{

			$response['errorcode'] = "110002";

			$response['ok'] = 0;

			$response['message']="Restaurant Id: Invalid Restaurant ID";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : get_update_order_type

	//	FUNCTION USE :  Update Order type it's pickup or delivery

	//	FUNCTION ERRORCODE : 380000

/****************************************************/

function get_update_order_type()

{

	$data['user_id'] = Input::get("user_id");

	$data['guest_id'] = Input::get("guest_id");

	$data['rest_id'] = Input::get("rest_id");

	$data['deviceid'] = Input::get("deviceid");

	$data['devicetype'] = Input::get("devicetype");

	$data['rest_cartid'] = Input::get("rest_cartid");

	$data['order_type'] = Input::get("order_type");

	if(empty($data['rest_id'])){

		$response['errorcode'] = "380001";

		$response['ok'] = 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif(empty($data['deviceid'])){

		$response['errorcode'] = "380002";

		$response['ok'] = 0;

		$response['message']="Device Id:Required parameter missing";

	}elseif(empty($data['devicetype'])){

		$response['errorcode'] = "38003";

		$response['ok'] = 0;

		$response['message']="Device type:Required parameter missing";

	}elseif(empty($data['rest_cartid'])){

		$response['errorcode'] = "380004";

		$response['ok'] = 0;

		$response['message']="Rest Cart Id:Required parameter missing";

	}elseif(empty($data['order_type'])){

		$response['errorcode'] = "380005";

		$response['ok'] = 0;

		$response['message']="Order Type:Required parameter missing";

	}elseif(!empty($data)){

		$rest_id =trim($this->param['rest_id']);

		$user_id =trim($this->param['user_id']);

		$cart_detail ='';

		$cart_listing = DB::table('temp_cart');

		$cart_listing = $cart_listing->select('*');

		$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

		$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

		$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

		$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

		$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

		$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

		$cart_listing = $cart_listing->get();

		if($cart_listing)

		{

			/********************** UPDATE SELECTED FILE IF ANY ADDRESS IS SELECTED*****************************/

			$address_id =trim($this->param['address_id']);

			if(!empty($address_id))

			{

				DB::table('delivery_address')

				->where('del_userid', '=' ,trim($this->param['user_id']))

				->update([

					'del_selected' => '0'

				]);

				DB::table('delivery_address')

				->where('del_id', '=' ,trim($this->param['address_id']))

				->where('del_userid', '=' ,trim($this->param['user_id']))

				->update([

					'del_selected' => '1'

				]);

			}

			/********************** END*****************************/

			DB::table('temp_cart')

			->where('user_id', '=' ,trim($this->param['user_id']))

			->where('guest_id', '=' ,trim($this->param['guest_id']))

			->where('rest_cartid', '=' ,trim($this->param['rest_cartid']))

			->where('deviceid', '=' ,trim($this->param['deviceid']))

			->where('devicetype', '=' ,trim($this->param['devicetype']))

			->update([

				'order_type' => trim($this->param['order_type'])

			]);

			$response['message'] = "Cart order type updated!";

			$response['ok'] = 1;

		}

		else

		{

			$response['errorcode'] = "380006";

			$response['ok'] = 0;

			$response['message']="This cart is empty";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}







/************************************************************/

	//	FUNCTION NAME : get_verify_delivery_address

	//	FUNCTION USE :  Verify delivery address is correct or not

	//	FUNCTION ERRORCODE : 390000

/****************************************************/

function get_verify_delivery_address()

{

	$data['rest_id'] =$rest_id = Input::get("rest_id");

	$data['userid'] = $user_id = Input::get("userid");

	$data['guest_id'] = $guest_id = Input::get("guest_id");

	$data['deviceid'] = $deviceid = Input::get("deviceid");

	$data['devicetype'] = $devicetype = Input::get("devicetype");

	$data['address_lat'] = $address_lat = Input::get("address_lat");

	$data['address_lang'] = $address_lang = Input::get("address_lang");

	if(empty($data['rest_id'])){

		$response['errorcode'] = "390001";

		$response['ok'] = 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif(empty($data['address_lat'])){

		$response['errorcode'] = "390002";

		$response['ok'] = 0;

		$response['message']="Address Latitute: Required parameter missing";

	}elseif(empty($data['address_lang'])){

		$response['errorcode'] = "390003";

		$response['ok'] = 0;

		$response['message']="Address Langitute: Required parameter missing";

	}

	elseif (!empty($data)){

		$rest_listing = DB::table('search_restaurant_view')

		->select("*")

		->where("rest_id","=",$rest_id)

		->get();

		if($rest_listing)

		{

			$diff_distance =  $this->distance($rest_listing[0]->rest_lat, $rest_listing[0]->rest_long, $address_lat, $address_lang, "m");

			if( $diff_distance<$rest_listing[0]->rest_delivery_upto)

			{

				$response['message'] = "Address verified successfully!";

				$response['rest_data'] = array('rest_delivery_upto'=>$rest_listing[0]->rest_delivery_upto,'distance'=>$diff_distance);

				$response['ok'] = 1;

			}

			else

			{

				$response['errorcode'] = "390004";

				$response['message'] = "Restaurant not give delivery on your address!";

				$response['ok'] = 0;

			}

		}

		else

		{

			$response['errorcode'] = "390005";

			$response['message'] = "Restaurant not available";

			$response['ok'] = 0;

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : add_delivery_address

	//	FUNCTION USE :  Addd new delivery address

	//	FUNCTION ERRORCODE : 350000

/****************************************************/



public function add_delivery_address()

{

	$data['rest_id'] =$rest_id = Input::get("rest_id");

	$data['userid'] = $user_id = Input::get("userid");

	$data['guest_id'] = $guest_id = Input::get("guest_id");

	$data['deviceid'] = $deviceid = Input::get("deviceid");

	$data['devicetype'] = $devicetype = Input::get("devicetype");

	$data['street_address'] = $street_address = Input::get("street_address");

	$data['city'] = $city = Input::get("city");

	$data['state'] = $state = Input::get("state");

	$data['zipcode'] = $zipcode = Input::get("zipcode");

	$data['country'] = $country = Input::get("country");

	$data['phone_no'] = $address_lat = Input::get("phone_no");

	$data['address_lat'] = $address_lat = Input::get("address_lat");

	$data['address_lang'] = $address_lang = Input::get("address_lang");

	if(empty($data['rest_id'])){

		$response['errorcode'] = "340001";

		$response['ok'] = 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif(empty($data['street_address'])){

		$response['errorcode'] = "340002";

		$response['ok'] = 0;

		$response['message']="Street Address : Required parameter missing";

	}elseif(empty($data['city'])){

		$response['errorcode'] = "340003";

		$response['ok'] = 0;

		$response['message']="City: Required parameter missing";

	}elseif(empty($data['state'])){

		$response['errorcode'] = "340004";

		$response['ok'] = 0;

		$response['message']="State: Required parameter missing";

	}elseif(empty($data['zipcode'])){

		$response['errorcode'] = "340005";

		$response['ok'] = 0;

		$response['message']="State: Required parameter missing";

	}elseif(empty($data['country'])){

		$response['errorcode'] = "340006";

		$response['ok'] = 0;

		$response['message']="country: Required parameter missing";

	}elseif(empty($data['phone_no'])){

		$response['errorcode'] = "340007";

		$response['ok'] = 0;

		$response['message']="Contact Number: Required parameter missing";

	}

	elseif (!empty($data)){

		$rest_listing = DB::table('search_restaurant_view')

		->select("*")

		->where("rest_id","=",$rest_id)

		->get();

		if($rest_listing)

		{

			$diff_distance =  $this->distance($rest_listing[0]->rest_lat, $rest_listing[0]->rest_long, $address_lat, $address_lang, "m");

			if( $diff_distance<$rest_listing[0]->rest_delivery_upto)

			{

				$result = new Delivery_address;

				$result->del_userid = trim($this->param['userid']);

				$result->del_guestid = trim($this->param['guest_id']);

				$result->del_contactno = trim($this->param['phone_no']);

				$result->del_address = trim($this->param['street_address']);

				$result->del_city = trim($this->param['city']);

				$result->del_zipcode = trim($this->param['zipcode']);

				$result->del_state = trim($this->param['state']);

				$result->del_country = trim($this->param['country']);

				$result->del_selected = '0';

				$result->deviceid = trim($this->param['deviceid']);

				$result->devicetype = trim($this->param['devicetype']);

				$result->save();

				$add_id =$result->del_id;

				$address_data = DB::table('delivery_address')->where('del_id', '=' ,$add_id)->get();

				$response['message'] = "Delivery address added successfully!";

				$response['address_data'] = $address_data;

				$response['ok'] = 1;

			}

			else

			{

				$response['errorcode'] = "340008";

				$response['message'] = "Restaurant not give delivery on your address!";

				$response['ok'] = 0;

			}

		}

		else

		{

			$response['errorcode'] = "340009";

			$response['message'] = "Restaurant not available";

			$response['ok'] = 0;

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

	//	FUNCTION NAME : Restaurant_detail

	//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

	//	FUNCTION ERRORCODE : 110000

/****************************************************/

public function restaurant_detail()

{

	$data['restaurentid'] =$rest_id = Input::get("restaurentid");

	$data['userid'] = $user_id = Input::get("userid");

	$data['guest_id'] = $guest_id = Input::get("guest_id");

	$data['deviceid'] = $deviceid = Input::get("deviceid");

	$data['devicetype'] = $devicetype = Input::get("devicetype");

	$device_name = Input::get("device_name");

	$device_osname = Input::get("device_osname");

		/*elseif(($user_id=='') || ($user_id<0)){

			$response['errorcode'] = "110003";

			$response['ok'] = 0;

			$response['message']="User Id:Required parameter missing";

		}*/

		if(empty($data['restaurentid'])) {

			$response['errorcode'] = "110001";

			$response['ok'] = 0;

			$response['message']="Restaurant Id:Required parameter missing";

		} elseif (!empty($data)) {

			$rest_listing = DB::table('search_restaurant_view');

			$rest_listing = $rest_listing->select('*');

			$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

			$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

			$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

			$rest_listing = $rest_listing->get();

			$cart_item_count='0';

			$rest_cart_id='';

			if( ($user_id>0) || ($guest_id!=''))

			{

				$cart_rest_listing = DB::table('temp_cart')

				->select('*')

				->where('user_id', '=' ,trim($user_id))

				->where('guest_id', '=' ,trim($guest_id))

				->where('rest_id', '=' ,trim($rest_id))

				->where('deviceid', '=' ,trim($deviceid))

				->where('devicetype', '=' ,trim($devicetype))

				->get();

				if(count($cart_rest_listing)>0)

				{

					$rest_cart_id = $cart_rest_listing[0]->rest_cartid;

					$cart_item_count = count($cart_rest_listing);

				}

			}

			/************ VIEW MENU LOG HISTORY ***************/

			$data_view_history=array('user_id'=>$user_id,

				'guest_id'=>$guest_id,

				'rest_id'=>$rest_id,

				'menu_id'=>'',

				'submenu_id'=>'',

				'device_type'=>$devicetype,

				'device_name'=>$device_name,

				'device_os'=>$device_osname,

				'view_date'=>date('Y-m-d H:i:s'),

				'view_time'=>date('Y-m-d H:i:s')

			);

			DB::table('view_menu_history')->insert($data_view_history);

			/*************** VIEW MENU LOG HISTORY END *********************/

			/* COUNT CALCULATION OF RATING  FOR REVIEW /FOOD/ ETC */

			$rest_food_good ='0';

			$rest_delivery_ontime ='0';

			$rest_order_accurate ='0';

			$count_review = DB::table('review')

			->where('re_restid', '=' ,$rest_id)

			->where('re_status', '=' ,'PUBLISHED')

			->orderBy('re_id', 'desc')

			->count();

			if($count_review>0)

			{

				$sql_food = DB::table('review')

				->select(DB::raw("round((count(`re_food_good`) / ".$count_review."),0) as  food_good"))

				->where('re_restid', '=' , $rest_id)

				->where('re_food_good', '=' , '1')

				->get();

				$sql_delivery_ontime = DB::table('review')

				->select(DB::raw("round((count(`re_delivery_ontime`) / ".$count_review."),0) as  delivery_ontime"))

				->where('re_restid', '=' , $rest_id)

				->where('re_delivery_ontime', '=' , '1')

				->get();

				$sql_rder_accurate = DB::table('review')

				->select(DB::raw("round((count(`re_order_accurate`) / ".$count_review."),0) as  order_accurate"))

				->where('re_restid', '=' , $rest_id)

				->where('re_order_accurate', '=' , '1')

				->get();

				$rest_food_good = $sql_food[0]->food_good;

				$rest_delivery_ontime = $sql_delivery_ontime[0]->delivery_ontime;

				$rest_order_accurate = $sql_rder_accurate[0]->order_accurate;

			}

			/* END */

			/*RESTAURNT DETAILDATA ARRAY */

			$rest_data["rest_cart_id"] = trim($rest_cart_id);

			$rest_data["cart_item_count"] = trim($cart_item_count);

			$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

			$rest_data["vendor_id"] = trim($rest_listing[0]->vendor_id);

			$rest_data["rest_name"] = trim($rest_listing[0]->rest_name);

			$rest_data["rest_metatag"] = trim($rest_listing[0]->rest_metatag);

			$rest_data["rest_logo"] = trim(url('/').'/uploads/reataurant/'.$rest_listing[0]->rest_logo);

			$rest_data["rest_address"] = trim($rest_listing[0]->rest_address);

			$rest_data["rest_address2"] = trim($rest_listing[0]->rest_address2);

			$rest_data["rest_city"] = trim($rest_listing[0]->rest_city);

			$rest_data["rest_zip_code"] = trim($rest_listing[0]->rest_zip_code);

			$rest_data["rest_suburb"] = trim($rest_listing[0]->rest_suburb);

			$rest_data["rest_state"] = trim($rest_listing[0]->rest_state);

			$rest_data["rest_desc"] = trim($rest_listing[0]->rest_desc);

			$rest_data["rest_contact"] = trim($rest_listing[0]->rest_contact);

			$rest_data["rest_service"] = trim($rest_listing[0]->rest_service);

			$rest_data["rest_create"] = trim($rest_listing[0]->rest_create);

			$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

			$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

			$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

			$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

			$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

			$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

			$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

			$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

			$rest_data["rest_cuisine"] = trim($rest_listing[0]->rest_cuisine);

			$rest_data["rest_mindelivery"] = trim($rest_listing[0]->rest_mindelivery);

			$rest_data["rest_landline"] = trim($rest_listing[0]->rest_landline);

			$rest_data["rest_commission"] = trim($rest_listing[0]->rest_commission);

			$rest_data["rest_email"] = trim($rest_listing[0]->rest_email);

			$rest_data["rest_status"] = trim($rest_listing[0]->rest_status);

			$rest_data["rest_classi"] = trim($rest_listing[0]->rest_classi);

			$rest_data["rest_special"] = trim($rest_listing[0]->rest_special);

			$rest_data["rest_delivery_from"] = trim($rest_listing[0]->rest_delivery_from);

			$rest_data["rest_delivery_to"] = trim($rest_listing[0]->rest_delivery_to);

			$rest_data["rest_holiday_from"] = trim($rest_listing[0]->rest_holiday_from);

			$rest_data["rest_holiday_to"] = trim($rest_listing[0]->rest_holiday_to);

			$rest_data["rest_openstatus"] = trim($rest_listing[0]->rest_openstatus);

			if(!empty($rest_listing[0]->rest_banner)){

				$rest_data["rest_banner"] = trim(url('/').'/uploads/reataurant/banner/'.$rest_listing[0]->rest_banner);

			}

			else

			{

				$rest_data["rest_banner"] =trim(url('/').'/uploads/reataurant/banner/sub_header_2.jpg');

			}

			$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

			$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

			$rest_data["created_at"] = trim($rest_listing[0]->created_at);

			$rest_data["updated_at"] = trim($rest_listing[0]->updated_at);

			$totalrating = 0;

			if(!empty($rest_listing[0]->totalrating)){$totalrating = $rest_listing[0]->totalrating; }

			$rest_data["totalrating"] = trim($totalrating);

			//$rest_data["totalrating"] = trim($rest_listing[0]->totalrating);

			$rest_data["totalreview"] = trim($rest_listing[0]->totalreview);

			$rest_data["cuisine_name"] = trim($rest_listing[0]->cuisine_name);

			$rest_data["rest_price_level"] = trim($rest_listing[0]->rest_price_level);

			$rest_data["rest_servicetax"] = trim($rest_listing[0]->rest_servicetax);

			$rest_data["rest_cash_deliver"] = trim($rest_listing[0]->rest_cash_deliver);

			$rest_data["rest_partial_pay"] = trim($rest_listing[0]->rest_partial_pay);

			$rest_partial_percent = 0;

			if($rest_listing[0]->rest_partial_pay==1)

			{

				$rest_partial_percent = $rest_listing[0]->rest_partial_percent;

			}

			$rest_data["rest_partial_percent"] = trim($rest_partial_percent);

			$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

			/*$rest_data["rest_food_good"] = trim('92% Food was good');

			$rest_data["rest_on_time"] = trim('89% Delivery was on time');

			$rest_data["rest_order_accurate"] = trim('98% Order was accurate');*/

			$rest_data["rest_food_good"] = trim('');

			$rest_data["rest_on_time"] = trim('');

			$rest_data["rest_order_accurate"] = trim('');

			if($rest_food_good>0){

				$rest_data["rest_food_good"] = trim($rest_food_good.'% Food was good');

			}

			if($rest_delivery_ontime>0){

				$rest_data["rest_on_time"] = trim($rest_delivery_ontime.'% Delivery was on time');

			}

			if($rest_order_accurate>0){

				$rest_data["rest_order_accurate"] = trim($rest_order_accurate .'% Order was accurate');

			}

			$rest_data["rest_min_orderamt"] = trim($rest_listing[0]->rest_min_orderamt);

			/* END **/

			if($rest_listing) {

				/*$menu_list = DB::table('menu')

								->where('restaurant_id', '=' ,$rest_id)

								->where('menu_status', '=' ,'1')

								->orderBy('menu_order', 'asc')

								->get();*/

								$menu_list = DB::table('menu_category')

								->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')

								->where('menu_category.rest_id', '=' ,$rest_id)

								->where('menu_category.menu_cat_status', '=' ,'1')

								->where('menu.restaurant_id', '=' ,$rest_id)

								->where('menu.menu_status', '=' ,'1')

								->select('menu.*')

								->orderBy('menu.menu_id', 'asc')

								->groupBy('menu.menu_id')

								->get();

								$menu_array = '';

								$food_array=array();

								$popular_item = '';

								$popular_item_count =0;

								$popular_item_count_11 =0;

								if(!empty($menu_list))

								{

									$popular_food_detail = array();

									$popular_food_data='';

									foreach($menu_list as $mlist)

									{

										$menu_id = $mlist->menu_id;

										$menu_name = $mlist->menu_name;

										$menu_desc = $mlist->menu_desc;

										$menu_detail =$mlist;

										$menu_item_detail = '';

										$menu_item_name = '';

										$menu_item_desc = '';

										$menu_item_price = '';

										$food_detail = array();

										$food_data=array();

										$counter_popular =0 ;

										$menu_cat_detail = DB::table('menu_category')

										->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

										->where('menu_category.rest_id', '=' ,$rest_id)

										->where('menu_category.menu_id', '=' ,$menu_id)

										->where('menu_category.menu_cat_status', '=' ,'1')

										->select('menu_category.*','menu_category_item.*')

										->orderBy('menu_category.menu_category_id', 'asc')

										->get();

										$menu_item_count =count($menu_cat_detail);

										$item_addons = array();

										if(!empty($menu_cat_detail))

										{

											foreach($menu_cat_detail as $menu_item)

											{

												$food_data['food_menuid'] = $menu_id;

												$food_data['food_id'] = $menu_item->menu_category_id;

												$food_data['food_popular'] = $menu_item->menu_cat_popular;

												$food_data['food_diet'] = $menu_item->menu_cat_diet;

												$food_data['food_name'] = $menu_item->menu_category_name;

												$food_data['food_price'] = $menu_item->menu_category_price;

												$food_data['food_desc'] = $menu_item->menu_category_desc;

												$food_data['food_subitem'] = $menu_item->menu_category_portion;

												$food_data['food_size_detail']='';

												$pp_ff = '';

												if($menu_item->menu_category_portion=='yes')

												{

													$food_size = DB::table('category_item')

													->where('rest_id', '=' ,$rest_id)

													->where('menu_id', '=' ,$menu_id)

													->where('menu_category', '=' ,$menu_item->menu_category_id)

													->where('menu_item_status', '=' ,'1')

													->select('*')

													->orderBy('menu_cat_itm_id', 'asc')

													->groupBy('menu_item_title')

													->get();

													if(count($food_size)>0)

													{

														$food_data['food_size_detail']=$food_size;

														$pp_ff =$food_size;

													}

													else

													{

														$food_data['food_size_detail']=array();

														$pp_ff =array();

													}

												}

												else

												{

													$food_data['food_size_detail']=array();

													$pp_ff =array();

												}

												$group_name= '';

												$group_list = '';

												$ff_popular = '';

												$popular_food_data=array();

									/*$addons = DB::table('menu_category_addon')

									->where('addon_restid', '=' ,$rest_id)

									->where('addon_menuid', '=' ,$menu_id)

									->where('addon_status', '=' ,'1')

									->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

									->select('*')

									->orderBy('addon_id', 'asc')

									->groupBy('addon_groupname')

									->get();

									if(!empty($addons))

									{

										foreach($addons as $ad_list)

										{

											$group_name[]=$ad_list->addon_groupname;

											$ff['addon_gropname'] = $ad_list->addon_groupname;

											$addon_group = DB::table('menu_category_addon')

												->where('addon_restid', '=' ,$rest_id)

												->where('addon_menuid', '=' ,$menu_id)

												->where('addon_status', '=' ,'1')

												->where('addon_groupname', '=' ,$ad_list->addon_groupname)

												->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

												->select('*')

												->orderBy('addon_id', 'asc')

												->get();

											$group_list[]=$addon_group;

											$addon_group_list ='';

											foreach($addon_group as $group_list_loop)

											{

												//$addon_group_list[]=array('')

												$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

												'addon_menucatid'=>$group_list_loop->addon_menucatid,

												'addon_groupname'=>$group_list_loop->addon_groupname,

												'addon_option'=>$group_list_loop->addon_option,

												'addon_name'=>$group_list_loop->addon_name,

												'addon_price'=>$group_list_loop->addon_price,

												'addon_status'=>$group_list_loop->addon_status

												);

											}

											$ff['addon_detail'] = $addon_group_list;

											$food_data['food_addon'][]=$ff;

											if($menu_item->menu_cat_popular==1)

											{

												$ff_popular['addon_detail'] = $addon_group_list;

												$ff_popular['addon_gropname'] = $ad_list->addon_groupname;

												$popular_food_data['food_addon'][] = $ff_popular;

											}

										}

									}

									else

									{

											$food_data['food_addon']=array();

											$ff['addon_gropname'] = '';

											if($menu_item->menu_cat_popular==1)

											{

												$ff_popular['addon_detail'] = '';

												$ff_popular['addon_gropname'] = '';

												$popular_food_data['food_addon']=array();

											}

										}*/

										$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);

										$food_detail[] = $food_data;

										if($menu_item->menu_cat_popular==1){

											$popular_item_count_11 ++;

											$counter_popular++;

											$popular_food_data['food_menuid'] = $menu_id;

											$popular_food_data['food_id'] = $menu_item->menu_category_id;

											$popular_food_data['food_popular'] = $menu_item->menu_cat_popular;

											$popular_food_data['food_diet'] = $menu_item->menu_cat_diet;

											$popular_food_data['food_name'] = $menu_item->menu_category_name;

											$popular_food_data['food_price'] = $menu_item->menu_category_price;

											$popular_food_data['food_desc'] = $menu_item->menu_category_desc;

											$popular_food_data['food_subitem'] = $menu_item->menu_category_portion;

											$popular_food_data['food_size_detail']=	$pp_ff;

											$popular_food_detail[] = $popular_food_data;

										}

									}

								}

								else

								{

									$food_detail=array();

								}

								$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail);

								if($counter_popular>0){

									$popular_item = array('popular_item_count'=>$popular_item_count_11,'popular_food_detail'=>$popular_food_detail);

								}

							}

						}

						else

						{

							$food_array=array();

						}

						if(isset($popular_item['popular_item_count']) && ($popular_item['popular_item_count']>0))

						{

							$new_populate_array = array('menu_id'=>'0','menu_name'=>'popular','food_counter'=>$popular_item['popular_item_count'],'food_detail'=>$popular_item['popular_food_detail']);

							array_unshift($food_array,$new_populate_array );

						}

						/** PROMOTIONS CODE START  **/

						$current_promo = array();

					//if(($user_id>0) && ($user_id!=''))

					//echo $rest_id;		{

						$promo_list = DB::table('promotion')

						->where('promo_restid', '=' ,$rest_id)

						->where('promo_status', '=' ,'1')

						->orderBy('promo_on', 'asc')

						->orderBy('promo_id', 'desc')

						->groupBy('promo_id')

						->get();

					//$current_promo = array();

						if($promo_list)

						{

							foreach($promo_list as $plist)

							{

								$current_promo1 = array();

								if($plist->promo_on=='schedul')

								{

									$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

									if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

									{

										if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d')) && (empty($plist->promo_day)))

										{

											$current_promo1['promo_id'] = $plist->promo_id;

											$current_promo1['promo_restid'] = $plist->promo_restid;

											$current_promo1['promo_for'] = $plist->promo_for;

											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

											$current_promo1['promo_on'] = $plist->promo_on;

											$current_promo1['promo_desc'] = $plist->promo_desc;

											$current_promo1['promo_mode'] = $plist->promo_mode;

											$current_promo1['promo_buy'] = $plist->promo_buy;

											$current_promo1['promo_value'] = $plist->promo_value;

											$current_promo[] = $current_promo1;

										}

										$db_day = explode(',',$plist->promo_day);

										if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

											&& (!empty($plist->promo_day))

											&& (in_array($day_name,$db_day))

										)

										{

											$current_promo1['promo_id'] = $plist->promo_id;

											$current_promo1['promo_restid'] = $plist->promo_restid;

											$current_promo1['promo_for'] = $plist->promo_for;

											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

											$current_promo1['promo_on'] = $plist->promo_on;

											$current_promo1['promo_desc'] = $plist->promo_desc;

											$current_promo1['promo_mode'] = $plist->promo_mode;

											$current_promo1['promo_buy'] = $plist->promo_buy;

											$current_promo1['promo_value'] = $plist->promo_value;

											$current_promo1['promo_end'] = $plist->promo_end;

											$current_promo1['promo_start'] = $plist->promo_start;

											$current_promo[] = $current_promo1;

										}

									}

									elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day)))

									{

										$db_day = explode(',',$plist->promo_day);

										if(in_array($day_name,$db_day))

										{

											$current_promo1['promo_id'] = $plist->promo_id;

											$current_promo1['promo_restid'] = $plist->promo_restid;

											$current_promo1['promo_for'] = $plist->promo_for;

											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

											$current_promo1['promo_on'] = $plist->promo_on;

											$current_promo1['promo_desc'] = $plist->promo_desc;

											$current_promo1['promo_mode'] = $plist->promo_mode;

											$current_promo1['promo_buy'] = $plist->promo_buy;

											$current_promo1['promo_value'] = $plist->promo_value;

											$current_promo1['promo_end'] = $plist->promo_end;

											$current_promo1['promo_start'] = $plist->promo_start;

											$current_promo[] = $current_promo1;

										}

									}

								}

								else

								{

									$current_promo1['promo_id'] = $plist->promo_id;

									$current_promo1['promo_restid'] = $plist->promo_restid;

									$current_promo1['promo_for'] = $plist->promo_for;

									$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

									$current_promo1['promo_on'] = $plist->promo_on;

									$current_promo1['promo_desc'] = $plist->promo_desc;

									$current_promo1['promo_mode'] = $plist->promo_mode;

									$current_promo1['promo_buy'] = $plist->promo_buy;

									$current_promo1['promo_value'] = $plist->promo_value;

									$current_promo1['promo_end'] = $plist->promo_end;

									$current_promo1['promo_start'] = $plist->promo_start;

									$current_promo[] = $current_promo1;

								}

							}

						}

					/*else

					{

						$current_promo = array();

					}*/

					/*}

					else

					{

						$current_promo = array();

					}*/

					/** PROMOTIONS CODE END  **/

					$response['message'] = "Restaurant Detail";

					$response['ok'] = 1;

					$response['restaurant_detail'] = $rest_data;//$rest_listing;

					$response['current_promo']  = $current_promo;//$Promostion listing;

					$response['menu_detal'] = $food_array;

					$response['menu_list'] = $menu_list;

				}

				else

				{

					$response['errorcode'] = "110002";

					$response['ok'] = 0;

					$response['message']="Restaurant Id: Invalid Restaurant ID";

				}

			}

			header('Content-type: application/json');

			echo json_encode($response);

		}





		public function food_addcart()

		{

			$data['user_id'] =$user_id = Input::get("user_id");

			$data['guest_id'] = Input::get("guest_id");

			$data['rest_id'] = $rest_id = Input::get("rest_id");

			$data['menu_id'] = Input::get("menu_id");

			$data['menu_catid'] = Input::get("food_id");

			$data['menu_subcatid'] = Input::get("food_size_id");

			$data['menu_addonid'] = Input::get("food_addonid");

			$data['qty']  = Input::get("qty");

			$data['deviceid'] = Input::get("deviceid");

			$data['devicetype'] = Input::get("devicetype");

			$data['special_instruction'] = Input::get("special_instruction");

			if(empty($data['rest_id'])){

				$response['errorcode'] = "140001";

				$response['ok'] = 0;

				$response['message']="Restaurant Id:Required parameter missing";

			}elseif(empty($data['menu_id'])){

				$response['errorcode'] = "140002";

				$response['ok'] = 0;

				$response['message']="Menu Id:Required parameter missing";

			}elseif(empty($data['menu_catid'])){

				$response['errorcode'] = "140003";

				$response['ok'] = 0;

				$response['message']="Food Id:Required parameter missing";

			}elseif(empty($data['qty'])){

				$response['errorcode'] = "140004";

				$response['ok'] = 0;

				$response['message']="Food Quantity:Required parameter missing";

			}elseif(empty($data['deviceid'])){

				$response['errorcode'] = "140005";

				$response['ok'] = 0;

				$response['message']="Device Id:Required parameter missing";

			}elseif(empty($data['devicetype'])){

				$response['errorcode'] = "140006";

				$response['ok'] = 0;

				$response['message']="Device type:Required parameter missing";

			}elseif(!empty($data)){

				$rest_cartid = '';

				$cart_listing = DB::table('temp_cart');

				$cart_listing = $cart_listing->select('*');

				$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

				$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

				$cart_listing = $cart_listing->where('rest_id', '!=' ,trim($this->param['rest_id']));

				$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

				$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

				$cart_listing = $cart_listing->get();

				if($cart_listing)

				{

					$response['errorcode'] = "140007";

					$response['ok'] = 0;

					$response['message']="Adding this item to your cart will remove all other items that aren't from this restaurant. Do you wish to continue?";

				}

				else

				{

					$cart_rest_listing = DB::table('temp_cart')

					->select('*')

					->where('user_id', '=' ,trim($this->param['user_id']))

					->where('guest_id', '=' ,trim($this->param['guest_id']))

					->where('rest_id', '=' ,trim($this->param['rest_id']))

					->where('deviceid', '=' ,trim($this->param['deviceid']))

					->where('devicetype', '=' ,trim($this->param['devicetype']))

					->get();

					if($cart_rest_listing)

					{

						$rest_cartid = $cart_rest_listing[0]->rest_cartid;

					}

					else

					{

						$rest_cartid = (date('ymdHis'));

					}

				/* $cart_record = DB::table('temp_cart')

							->select('*')

							->where('user_id', '=' ,trim($this->param['user_id']))

							->where('guest_id', '=' ,trim($this->param['guest_id']))

							->where('rest_id', '=' ,trim($this->param['rest_id']))

							->where('menu_id', '=' ,trim($this->param['menu_id']))

							->where('menu_catid', '=' ,trim($this->param['food_id']))

							->where('menu_subcatid', '=' ,trim($this->param['food_size_id']))

										->where('deviceid', '=' ,trim($this->param['deviceid']))

										->where('devicetype', '=' ,trim($this->param['devicetype']))

							->get();

				$total_record = count($cart_record);

				if($total_record>0)

				{

					$qty = (($cart_record[0]->qty)+$this->param['qty']);

					$temp_id = $cart_record[0]->cart_id;

					DB::table('temp_cart')

					->where('cart_id', trim($temp_id))

					->update(['user_id' =>  trim($this->param['user_id']),

							  'guest_id' => trim($this->param['guest_id']),

							  'rest_id' =>  trim($this->param['rest_id']),

							  'menu_id' => trim($this->param['menu_id']),

							  'menu_catid' =>  trim($this->param['food_id']),

							  'menu_subcatid' =>  trim($this->param['food_size_id']),

							  'menu_addonid' =>  trim($this->param['food_addonid']),

							  'qty' =>  trim($qty),

							  'special_instruction' =>  trim($this->param['special_instruction'])

						 ]);

				}

				else

				{

					$cart_data = new Temp_cart;

					$cart_data->user_id	 = trim($this->param['user_id']);

					$cart_data->rest_cartid	 = trim($rest_cartid);

					$cart_data->guest_id = trim($this->param['guest_id']);

					$cart_data->rest_id = trim($this->param['rest_id']);

					$cart_data->menu_id = trim($this->param['menu_id']);

					$cart_data->menu_catid = trim($this->param['food_id']);

					$cart_data->menu_subcatid = trim($this->param['food_size_id']);

					$cart_data->menu_addonid = trim($this->param['food_addonid']);

					$cart_data->qty = trim($this->param['qty']);

					$cart_data->special_instruction =  trim($this->param['special_instruction']);

					$cart_data->deviceid = trim($this->param['deviceid']);

					$cart_data->devicetype= trim($this->param['devicetype']);

					$cart_data->save();

				}*/

				$cart_data = new Temp_cart;

				$cart_data->user_id	 = trim($this->param['user_id']);

				$cart_data->rest_cartid	 = trim($rest_cartid);

				$cart_data->guest_id = trim($this->param['guest_id']);

				$cart_data->rest_id = trim($this->param['rest_id']);

				$cart_data->menu_id = trim($this->param['menu_id']);

				$cart_data->menu_catid = trim($this->param['food_id']);

				$cart_data->menu_subcatid = trim($this->param['food_size_id']);

				$cart_data->menu_addonid = trim($this->param['food_addonid']);

				$cart_data->qty = trim($this->param['qty']);

				$cart_data->special_instruction =  trim($this->param['special_instruction']);

				$cart_data->deviceid = trim($this->param['deviceid']);

				$cart_data->devicetype= trim($this->param['devicetype']);

				$cart_data->save();

				$response['message'] = "Item add in cart sucessfully";

				$response['ok'] = 1;

				$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

			}

			/*$response['restaurant_detail'] = $rest_listing;

			$response['popular_menu_detal'] = $popular_item;

			$response['menu_detal'] = $food_array;

			$response['menu_list'] = $menu_list;*/

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}



	/************************************************************/

	//	FUNCTION NAME : Food_Update_Tocard

	//	FUNCTION USE :  RESTAURANT FOOD ITEM ADD IN CART

	//	FUNCTION ERRORCODE : 150000

	/****************************************************/

	public function food_updatecart()

	{

		$data['cart_id'] = Input::get("cart_id");

		$data['rest_cartid']  =$rest_cartid = Input::get("rest_cartid");

		$data['user_id'] = Input::get("user_id");

		$data['guest_id'] = Input::get("guest_id");

		$data['rest_id'] = Input::get("rest_id");

		$data['menu_id'] = Input::get("menu_id");

		$data['menu_catid'] = Input::get("food_id");

		$data['menu_subcatid'] =Input::get("menu_subcatid");

		$data['menu_addonid'] =Input::get("menu_addonid");

		$data['qty'] = Input::get("qty");

		$data['deviceid'] = Input::get("deviceid");

		$data['devicetype'] =Input::get("devicetype");

		$data['special_instruction'] = Input::get("special_instruction");

		if(empty($data['cart_id'])){

			$response['errorcode'] = "150000";

			$response['ok'] = 0;

			$response['message']="Cart Id:Required parameter missing";

		}else if(empty($data['rest_id'])){

			$response['errorcode'] = "150001";

			$response['ok'] = 0;

			$response['message']="Restaurant Id:Required parameter missing";

		}elseif(empty($data['menu_id'])){

			$response['errorcode'] = "150002";

			$response['ok'] = 0;

			$response['message']="Menu Id:Required parameter missing";

		}elseif(empty($data['menu_catid'])){

			$response['errorcode'] = "150003";

			$response['ok'] = 0;

			$response['message']="Food Id:Required parameter missing";

		}elseif(empty($data['qty'])){

			$response['errorcode'] = "150004";

			$response['ok'] = 0;

			$response['message']="Food Quantity:Required parameter missing";

		}elseif(empty($data['deviceid'])){

			$response['errorcode'] = "150005";

			$response['ok'] = 0;

			$response['message']="Device Id:Required parameter missing";

		}elseif(empty($data['devicetype'])){

			$response['errorcode'] = "150006";

			$response['ok'] = 0;

			$response['message']="Device type:Required parameter missing";

		}elseif(empty($data['rest_cartid'])){

			$response['errorcode'] = "150007";

			$response['ok'] = 0;

			$response['message']="Rest Cart Id:Required parameter missing";

		}elseif(!empty($data)){

			$cart_listing = DB::table('temp_cart');

			$cart_listing = $cart_listing->select('*');

			$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

			$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

			$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

			$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

			$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

			$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

			$cart_listing = $cart_listing->get();

			if($cart_listing)

			{

				DB::table('temp_cart')

				->where('cart_id', trim($this->param['cart_id']))

				->where('rest_cartid', trim($this->param['rest_cartid']))

				->where('deviceid', '=' ,trim($this->param['deviceid']))

				->where('devicetype', '=' ,trim($this->param['devicetype']))

				->update(['user_id' =>  trim($this->param['user_id']),

					'guest_id' => trim($this->param['guest_id']),

					'rest_id' =>  trim($this->param['rest_id']),

					'menu_id' => trim($this->param['menu_id']),

					'menu_catid' =>  trim($this->param['food_id']),

					'menu_subcatid' =>  trim($this->param['food_size_id']),

					'menu_addonid' =>  trim($this->param['food_addonid']),

					'qty' =>  trim($this->param['qty']),

					'special_instruction' =>  trim($this->param['special_instruction'])

				]);

				$cart_detail = DB::table('temp_cart')

				->where('user_id', '=' ,trim($this->param['user_id']))

				->where('guest_id', '=' ,trim($this->param['guest_id']))

				->where('deviceid', '=' ,trim($this->param['deviceid']))

				->where('deviceid', '=' ,trim($this->param['deviceid']))

				->where('devicetype', '=' ,trim($this->param['devicetype']))

				->get();

				$response['message'] = "Cart Updated.";

				$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

				$response['ok'] = 1;

			}

			else

			{

				$response['errorcode'] = "150008";

				$response['ok'] = 0;

				$response['message']="This rest cart not available in our data base!";

			}

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}





	/************************************************************/

	//	FUNCTION NAME : Remove_food_favourite

	//	FUNCTION USE :  app user remove favourite  restaurant food

	//	FUNCTION ERRORCODE : 290000

	/****************************************************/

	public function remove_food_favourite()

	{

		$data['user_id'] = $user_id =Input::get("userid");

		$data['rest_id'] = $rest_id = Input::get("rest_id");

		$data['food_id'] = $food_id = Input::get("food_id");

		$response = array();

		if(empty($data['user_id'])){

			$response['errorcode'] = "290001";

			$response['ok'] = 0;

			$response['message']="User Id:Required parameter missing";

		}elseif(empty($data['rest_id'])){

			$response['message']="Restaurant id:Required parameter missing";

			$response['ok'] = 0;

			$response['errorcode'] = "290002";

		}elseif(empty($data['food_id'])){

			$response['message']="Food id:Required parameter missing";

			$response['ok'] = 0;

			$response['errorcode'] = "290003";

		}elseif(!empty($data)) {

			$user_favourite = DB::table('favourite_food')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->where('favrest_menucatid', '=' ,$food_id)

			->select('*')

			->orderBy('favrest_userid', 'asc')

			->get();

			if($user_favourite)

			{

				DB::table('favourite_food')

				->where('favrest_restid', '=' ,$rest_id)

				->where('favrest_userid', '=' ,$user_id)

				->where('favrest_menucatid', '=' ,$food_id)

				->update(['favrest_status' => '0'

			]);

			}

			//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

			$response['message'] = "Restaurant food remove as favourite sucessfully.";

			$response['ok'] = 1;

			//$response['data'] = $user_data;

		}

		header('Content-type: application/json');

		echo json_encode($response);

	}





	/************************************************************/

	//	FUNCTION NAME : Food_getdetail

	//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

	//	FUNCTION ERRORCODE : 170000

	/****************************************************/

	public function food_getcartdeatil()

	{

		DB::enableQueryLog();

		$data['user_id'] = Input::get("user_id");

		$data['guest_id'] = Input::get("guest_id");

		$data['rest_id'] = Input::get("rest_id");

		$data['deviceid'] = Input::get("deviceid");

		$data['devicetype'] = Input::get("devicetype");

		$data['rest_cartid'] =$rest_cartid = Input::get("rest_cartid");

		if(empty($data['rest_id'])){

			$response['errorcode'] = "170001";

			$response['ok'] = 0;

			$response['message']="Restaurant Id:Required parameter missing";

		}elseif(empty($data['deviceid'])){

			$response['errorcode'] = "170002";

			$response['ok'] = 0;

			$response['message']="Device Id:Required parameter missing";

		}elseif(empty($data['devicetype'])){

			$response['errorcode'] = "170003";

			$response['ok'] = 0;

			$response['message']="Device type:Required parameter missing";

		}elseif(empty($data['rest_cartid'])){

			$response['errorcode'] = "170004";

			$response['ok'] = 0;

			$response['message']="Rest Cart Id:Required parameter missing";

		}elseif(!empty($data)){

			$rest_id =trim($this->param['rest_id']);

			$user_id =trim($this->param['user_id']);

			$cart_food_id='';

			$cart_detail ='';

			$cart_listing = DB::table('temp_cart');

			$cart_listing = $cart_listing->select('*');

			$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

			$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

			$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

			$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

			$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

			$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

			$cart_listing = $cart_listing->get();

			if($cart_listing)

			{

				$delivery_fee = 0;

				$rest_detail = DB::table('restaurant')

				->where('rest_id', '=' ,$rest_id)

				->where('rest_status', '!=' , 'INACTIVE')

				->orderBy('rest_id', 'asc')

				->groupBy('rest_id')

				->get();

				$promo_list = DB::table('promotion')

				->where('promo_restid', '=' ,$rest_id)

				->where('promo_status', '=' ,'1')

				->orderBy('promo_buy', 'desc')

				->groupBy('promo_id')

				->get();

				$service_charge = $rest_detail[0]->rest_servicetax;

				if($cart_listing[0]->order_type=='delivery')

				{

					$delivery_fee = $rest_detail[0]->rest_mindelivery;

				}

				$sub_total_cart = 0;

				$json_data_session = '';

				$addon_list ='';

				$food_current_promo = '';

				foreach($cart_listing as $clist)

				{

					/********FOOD DETAILS /******/

					$rest_id  = $clist->rest_id;

					$menu_id = $clist->menu_id;

					$menu_cate_id =$clist->menu_catid;

					$menu_subcate_id = $clist->menu_subcatid;

					$addon_id = $clist->menu_addonid;

					$foodsize_price =0;

					$food_addonPrice = 0;

					$total_price =0 ;

					$item_name_cc = '';

					$item_price_cc = '';

					$offer_on_menu_item = '';

					/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/

						//if(($user_id>0) && ($user_id!='')){

					foreach($promo_list as $plist )

					{

						if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))

						{

							$current_promo1 = '';

							$food_cart_amount[] = $total_price;

							$food_cart_qty[] =$clist->qty;

							if($plist->promo_on=='schedul')

							{

								$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

								$db_day = '';

								if(!empty($plist->promo_day))

								{

									$db_day = explode(',',$plist->promo_day);

								}

								if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

								{

									if(

										($plist->promo_start<=date('Y-m-d')) &&

										($plist->promo_end>=date('Y-m-d')) &&

										(empty($plist->promo_day)) &&

										($plist->promo_buy<=$clist->qty)

									)

									{

										$current_promo1['promo_id'] = $plist->promo_id;

										$current_promo1['promo_restid'] = $plist->promo_restid;

										$current_promo1['promo_for'] = $plist->promo_for;

										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

										$current_promo1['promo_on'] = $plist->promo_on;

										$current_promo1['promo_desc'] = $plist->promo_desc;

										$current_promo1['promo_mode'] = $plist->promo_mode;

										$current_promo1['promo_buy'] = $plist->promo_buy;

										$current_promo1['promo_value'] = $plist->promo_value;

										$current_promo1['promo_end'] = $plist->promo_end;

										$current_promo1['promo_start'] = $plist->promo_start;

										$food_current_promo[] = $current_promo1;

										$offer_on_menu_item = $plist->promo_desc;

									}

									if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

										&& (!empty($plist->promo_day))

										&& (in_array($day_name,$db_day))

									)

									{

										$array_amt[]=$plist->promo_buy;

										$current_promo1['promo_id'] = $plist->promo_id;

										$current_promo1['promo_restid'] = $plist->promo_restid;

										$current_promo1['promo_for'] = $plist->promo_for;

										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

										$current_promo1['promo_on'] = $plist->promo_on;

										$current_promo1['promo_desc'] = $plist->promo_desc;

										$current_promo1['promo_mode'] = $plist->promo_mode;

										$current_promo1['promo_buy'] = $plist->promo_buy;

										$current_promo1['promo_value'] = $plist->promo_value;

										$current_promo1['promo_end'] = $plist->promo_end;

										$current_promo1['promo_start'] = $plist->promo_start;

										$food_current_promo[] = $current_promo1;

										$offer_on_menu_item = $plist->promo_desc;

									}

								}

								elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))

								{

									$db_day = explode(',',$plist->promo_day);

									if(in_array($day_name,$db_day))

									{

										$array_amt[]=$plist->promo_buy;

										$current_promo1['promo_id'] = $plist->promo_id;

										$current_promo1['promo_restid'] = $plist->promo_restid;

										$current_promo1['promo_for'] = $plist->promo_for;

										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

										$current_promo1['promo_on'] = $plist->promo_on;

										$current_promo1['promo_desc'] = $plist->promo_desc;

										$current_promo1['promo_mode'] = $plist->promo_mode;

										$current_promo1['promo_buy'] = $plist->promo_buy;

										$current_promo1['promo_value'] = $plist->promo_value;

										$current_promo1['promo_end'] = $plist->promo_end;

										$current_promo1['promo_start'] = $plist->promo_start;

										$food_current_promo[] = $current_promo1;

										$offer_on_menu_item = $plist->promo_desc;

									}

								}

							}

							elseif($plist->promo_on=='qty')

							{

								if(($plist->promo_buy<=$clist->qty))

								{

									$current_promo1['promo_id'] = $plist->promo_id;

									$current_promo1['promo_restid'] = $plist->promo_restid;

									$current_promo1['promo_for'] = $plist->promo_for;

									$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

									$current_promo1['promo_on'] = $plist->promo_on;

									$current_promo1['promo_desc'] = $plist->promo_desc;

									$current_promo1['promo_mode'] = $plist->promo_mode;

									$current_promo1['promo_buy'] = $plist->promo_buy;

									$current_promo1['promo_value'] = $plist->promo_value;

									$current_promo1['promo_end'] = $plist->promo_end;

									$current_promo1['promo_start'] = $plist->promo_start;

									$food_current_promo[] = $current_promo1;

									$offer_on_menu_item = $plist->promo_desc;

								}

							}

						}

					}

						//}

					/*************************** END ******************************/

					$menu_list = DB::table('menu')

					->where('restaurant_id', '=' ,$clist->rest_id)

					->where('menu_id', '=' ,$clist->menu_id)

					->where('menu_status', '=' ,'1')

					->orderBy('menu_order', 'asc')

					->get();

				// dd(DB::getQueryLog());

					$menu_array = '';

					$food_array='';

					if(!empty($menu_list))

					{

						$menu_id = $menu_list[0]->menu_id;

						$menu_name = $menu_list[0]->menu_name;

						$menu_item_count = 1;

						$food_detail='';

						$menu_cat_detail = DB::table('menu_category')

						->where('menu_category.rest_id', '=' ,$rest_id)

						->where('menu_category.menu_id', '=' ,$menu_id)

						->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

						->where('menu_category.menu_cat_status', '=' ,'1')

						->select('menu_category.*')

						->orderBy('menu_category.menu_category_id', 'asc')

						->get();

						$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

						$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

						$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

						$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

						$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

						$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

						$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

						$food_detail['food_subitem_title']='';

						if($menu_cat_detail[0]->menu_category_portion=='yes')

						{

							$food_detail['food_subitem_title'] = 'Choose a size';

							$food_size = DB::table('category_item')

							->where('rest_id', '=' ,$rest_id)

							->where('menu_id', '=' ,$menu_id)

							->where('menu_category', '=' ,$menu_cate_id)

							->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

							->where('menu_item_status', '=' ,'1')

							->select('*')

							->orderBy('menu_cat_itm_id', 'asc')

							->groupBy('menu_item_title')

							->get();

							$food_detail['food_size_detail']=$food_size;

							$item_price_cc =$food_size[0]->menu_item_price;

							$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

							$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

						}

						else

						{

							$food_detail['food_size_detail']=array();

							$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

						}

						$addon_list='';

						if(!empty($addon_id)){

							$addons = DB::table('menu_category_addon')

							->where('addon_restid', '=' ,$rest_id)

							->where('addon_menuid', '=' ,$menu_id)

							->where('addon_status', '=' ,'1')

							->where('addon_menucatid', '=' ,$menu_cate_id)

							->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

							->select('*')

							->orderBy('addon_id', 'asc')

							->groupBy('addon_groupname')

							->get();

							if(!empty($addons))

							{

								foreach($addons as $ad_list)

								{

									$option_type = 0;

									if(substr($ad_list->addon_groupname, -1)=='*')

									{

										$option_type = 1;

									}

									$group_name[]=$ad_list->addon_groupname;

									$ff['addon_gropname'] = $ad_list->addon_groupname;

									$ff['addon_type'] = $ad_list->addon_option;

									$ff['addon_mandatory_or_not'] = $option_type;

									$addon_group = DB::table('menu_category_addon')

									->where('addon_restid', '=' ,$rest_id)

									->where('addon_menuid', '=' ,$menu_id)

									->where('addon_status', '=' ,'1')

									->where('addon_groupname', '=' ,$ad_list->addon_groupname)

									->where('addon_menucatid', '=' ,$menu_cate_id)

									->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

									->select('*')

									->orderBy('addon_id', 'asc')

									->get();

									$group_list[]=$addon_group;

									$addon_group_list ='';

									foreach($addon_group as $group_list_loop)

									{

											//$addon_group_list[]=array('')

										$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

											'addon_menucatid'=>$group_list_loop->addon_menucatid,

											'addon_groupname'=>$group_list_loop->addon_groupname,

											'addon_option'=>$group_list_loop->addon_option,

											'addon_name'=>$group_list_loop->addon_name,

											'addon_price'=>$group_list_loop->addon_price,

											'addon_status'=>$group_list_loop->addon_status

										);

										$addon_list[]=array('name'=>$group_list_loop->addon_name,

											'price'=>$group_list_loop->addon_price,

											'id'=>$group_list_loop->addon_id);

										$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

										$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

									}

									$ff['addon_detail'] = $addon_group_list;

									$food_detail['food_addon'][]=$ff;

								}

							}

						}

						else

						{

							$food_detail['food_addon'] = array();

						}

						$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

					}

					/******** FOOD DETAIL END ****/

					$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));

					$cart_detail[] = array(

						"cart_id"=> $clist->cart_id,

						"rest_cartid"=> $clist->rest_cartid,

						"user_id" => $clist->user_id,

						"guest_id" => $clist->guest_id,

						"rest_id"=> $clist->rest_id,

						"menu_id"=> $clist->menu_id,

						"food_id"=> $clist->menu_catid,

						"food_size_id"=> $clist->menu_subcatid,

						"food_addonid"=> $clist->menu_addonid,

						"qty"=> $clist->qty,

						"special_instruction"=>$clist->special_instruction,

						"deviceid"=> $clist->deviceid,

						"devicetype"=> $clist->devicetype,

						"food_detail"=>$food_array,

						"foodsize_price"=>$foodsize_price,

						"food_addonPrice"=>$food_addonPrice,

						"total_price"=>$total_price

					);

					$json_data_session[$clist->cart_id] =	array("rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,	   "name"=>$item_name_cc,"qty"=>$clist->qty,"price"=>$item_price_cc,"instruction"=>$clist->special_instruction,			    "options"=> array ( "menu_catid"=>$clist->menu_catid,

						"rest_id"=>$clist->rest_id,

						"menu_id"=>$clist->menu_id,

						"option_name"=> $clist->menu_subcatid,

						"addon_data"=>$addon_list,

						"offer_name"=>$offer_on_menu_item),

					"tax"=>'',

					"subtotal"=>$item_price_cc

				);

				}

				/*print_r($food_current_promo);

				print_r($food_amount);*/

				$serv_tax  = (($sub_total_cart*$service_charge)/100);

				/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/

				$current_promo = '';

				$offer_applied_on_this_cart='';

			//if(($user_id>0) && ($user_id!='')){

				if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))

				{

					$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

					/*$promo_list = DB::table('promotion')

								->where('promo_restid', '=' ,$rest_id)

								->where('promo_status', '=' ,'1')

								->where('promo_for', '=' ,'order')

								->orderBy('promo_buy', 'desc')

								->groupBy('promo_id')

								->get();*/

						// dd(DB::getQueryLog());

						//print_r($promo_list);

								//

					//	exit;

								$current_promo = '';

								$array_amt = '';

								foreach($promo_list as $plist )

								{

									if($plist->promo_for=='order')

									{

										$current_promo1 = '';

										if($plist->promo_on=='schedul')

										{

											$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

											$db_day = '';

											if(!empty($plist->promo_day))

											{

												$db_day = explode(',',$plist->promo_day);

											}

											if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

											{

												if(

													($plist->promo_start<=date('Y-m-d')) &&

													($plist->promo_end>=date('Y-m-d')) &&

													(empty($plist->promo_day)) &&

													((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))

												)

												{

													$array_amt[]=$plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;

												}

												if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

													&& (!empty($plist->promo_day))

													&& (in_array($day_name,$db_day) &&

														((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

												)

												{

													$array_amt[]=$plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;

												}

											}

											elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) &&

												((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

											{

												$db_day = explode(',',$plist->promo_day);

												if(in_array($day_name,$db_day))

												{

													$array_amt[]=$plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;

												}

											}

										}

										else

										{

											if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))

											{

												$array_amt[]=$plist->promo_buy;

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;

												$current_promo[] = $current_promo1;

											}

										}

									}

								}

							}

							else

							{

								$current_promo = array();

								$offer_applied_on_this_cart='';

							}

			/*}

			else

			{

				$current_promo = array();

				$offer_applied_on_this_cart= '';

			}*/

			$offer_amt = 0;

			$cal_sub_totla = $sub_total_cart;

			if((count($current_promo)>0) && (!empty($array_amt))) {

				$maxs = max($array_amt);

				$key_name = array_search ($maxs, $array_amt);

				$k=0;$i=0;

				foreach($current_promo as $pval)

				{

					if($key_name==$k)

					{

						if($pval['promo_mode']=='$')

						{

							$i++;

							$offer_applied_on_this_cart=$pval['promo_desc'];

							$offer_amt = ($offer_amt+$pval['promo_value']);

							$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);

						}

						if(($pval['promo_mode']=='%') )

							{	$i++;

								$offer_applied_on_this_cart=$pval['promo_desc'];

								$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);

								$cal_sub_totla = ($cal_sub_totla - $promo_val_per);

								$offer_amt = ($offer_amt+$promo_val_per);

							}

							if(($pval['promo_mode']=='0') )

							{

								$i++;

								$offer_applied_on_this_cart=$pval['promo_desc'];

							}

						}

						$k++;

					}

				}

				else{

					$offer_applied_on_this_cart='';

					$current_promo = array();

				}

				/** PROMOTIONS CODE END  **/

				/******************  FOOD ITEM  */

			/*$food_disocunt

			$food_current_promo*/

			$food_offer_amt = '';

			$offer_applied_on_food= '';

			if(!empty($food_current_promo) && (count($food_current_promo)>0))

			{

				$k=0;$i=0;

				foreach($food_current_promo as $pval)

				{

					$item_price = $food_cart_amount[$k];

					$item_qty = $food_cart_qty[$k];

					$cal_sub_total=number_format(($item_price*$item_qty),2);

					if($pval['promo_mode']=='$')

					{

						$i++;

						$offer_applied_on_food=$pval['promo_desc'];

						$food_offer_amt = ($offer_amt+$pval['promo_value']);

					}

					if(($pval['promo_mode']=='%') )

						{	$i++;

							$offer_applied_on_food=$pval['promo_desc'];

							$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);

							$food_offer_amt = ($offer_amt+$promo_val_per);

						}

						if(($pval['promo_mode']=='0') )

						{

							$i++;

							$offer_applied_on_food=$pval['promo_desc'];

						}

						$k++;

					}

				}

				else

				{

					$food_current_promo= array();

				}

				/*******************  END  ****/

				if(($sub_total_cart-$offer_amt-$food_offer_amt)<$delivery_fee)

				{

					$total_amt =($delivery_fee+$serv_tax);

				}

				else

				{

					$aa_total = ($sub_total_cart-$offer_amt-$food_offer_amt)+$serv_tax;

					$total_amt = str_replace( ',', '',$aa_total);

				}

			/*print_r($current_promo);

			print_r(json_encode($json_data_session));

			exit;*/

			DB::table('temp_cart')

			->where('rest_cartid', '=' ,$rest_cartid)

			->update(['cart_detail' => (json_encode($json_data_session)),

				'offer_detail'=> (json_encode($current_promo)),

				'offer_amt'=> trim($offer_amt),

				'offer_applied'=>$offer_applied_on_this_cart,

				'food_promo_list'=>json_encode($food_current_promo),

				'food_offer_applied'=>json_encode($offer_applied_on_food),

				'service_tax_amt'=>trim($serv_tax),

				'delivery_amt'=> trim($delivery_fee),

				'subtotal'=> trim($sub_total_cart),

				'total_amt'=> str_replace( ',', '',$total_amt)

			]);

			$response['message'] = "All deatil of cart data";

			$response['cart_detail'] = $cart_detail;

			$response['other_detail'] = array(

				'subtotal'=> trim($sub_total_cart),

				'rest_service' =>$service_charge,

				'delivery_fee'=>number_format($delivery_fee,2),

				'service_tax'=>number_format($serv_tax,2),

				'promo_list'=>$current_promo,

				'discount_amt'=>number_format($offer_amt,2),

				'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,

				'food_promo_list'=>$food_current_promo,

				'offer_applied_on_item'=>$offer_applied_on_food,

				'total_amt'=>str_replace( ',', '',number_format($total_amt,2))

			);

			$response['ok'] = 1;

		}

		else

		{

			$response['errorcode'] = "170005";

			$response['ok'] = 0;

			$response['message']="Your cart is empty!";

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



function get_save_order()

{

		//echo "asdfdasf";die;

	echo "hello";die;

		 //print_r(expression)$data=[];

	$response = array();

		//$data['user_id']

		 //$payment_pay_status = '';

	$data['user_id'] =$user_id = Input::get("user_id");

	$data['guest_id'] = $guest_id = Input::get("guest_id");

	$data['guest_name'] = $guest_name = Input::get("guest_name");

	$data['guest_carrier'] = $guest_carrier = Input::get("guest_carrier");

	$data['guest_email'] = $guest_email = Input::get("guest_email");

	$data['guest_mobileno'] = $guest_mobileno = Input::get("guest_mobileno");

	$data['deviceid'] = $deviceid = Input::get("deviceid");

	$data['devicetype'] = $devicetype = Input::get("devicetype");

	$data['rest_cartid'] = $rest_cartid = Input::get("rest_cartid");

	$data['rest_id'] = $rest_id = Input::get("rest_id");

	$data['paymenttype'] = $paymenttype = Input::get("paymenttype");

	$data['order_tip'] = $order_tip = Input::get("order_tip");

	$data['ordertype'] = $ordertype = Input::get("ordertype");

	$data['payment_mode'] = $payment_mode = Input::get("payment_mode");

	$data['card_number'] = $card_number = Input::get("card_number");

	$data['card_name'] = $card_name = Input::get("card_name");

	$data['card_expdate'] = $card_expdate = Input::get("card_expdate");

	$data['card_cvv'] = $card_cvv = Input::get("card_cvv");

	$data['order_date'] = $order_date = Input::get("order_date");

	$data['order_time'] = $order_time = Input::get("order_time");

	$data['order_delivery_address'] = $order_delivery_address = Input::get("order_delivery_address");

	$data['order_delivery_city'] = $order_delivery_city = Input::get("order_delivery_city");

	$data['order_delivery_state'] = $order_delivery_state = Input::get("order_delivery_state");

	$data['order_delivery_country'] = $order_delivery_country = Input::get("order_delivery_country");

	$data['order_delivery_zipcode'] = $order_delivery_zipcode = Input::get("order_delivery_zipcode");

	$data['order_delivery_contact'] = $order_delivery_contact = Input::get("order_delivery_contact");

	$data['order_instruction'] = $order_instruction = Input::get("order_instruction");

	$data['partial_pay_amt'] = $partial_pay_amt = Input::get("partial_pay_amt");

	$data['partial_remain_amt'] = $partial_remain_amt = Input::get("partial_remain_amt");

	if(empty($data['rest_id'])){

		$response['errorcode'] = "400001";

		$response['ok'] = 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif(empty($data['rest_cartid'])){

		$response['errorcode'] = "400002";

		$response['ok'] = 0;

		$response['message']="Cart Id : Required parameter missing";

	}elseif(empty($data['paymenttype'])){

		$response['errorcode'] = "400003";

		$response['ok'] = 0;

		$response['message']="Payment Type: Required parameter missing";

	}

	elseif(empty($data['ordertype'])){

		$response['errorcode'] = "400004";

		$response['ok'] = 0;

		$response['message']="Order Type: Required parameter missing";

	}

	elseif(empty($data['payment_mode'])){

		$response['errorcode'] = "400005";

		$response['ok'] = 0;

		$response['message']="Payment Mode: Required parameter missing";

	}

	elseif(($this->param['asap_or_not']== '0') && (empty($data['order_date']))){

		$response['errorcode'] = "400006";

		$response['ok'] = 0;

		$response['message']="Order Date: Required parameter missing";

	}

	elseif(($this->param['asap_or_not']== '0') && (empty($data['order_time']))){

		$response['errorcode'] = "400007";

		$response['ok'] = 0;

		$response['message']="Order Time: Required parameter missing";

	}

	elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_address']))){

		$response['errorcode'] = "400008";

		$response['ok'] = 0;

		$response['message']="Delivery Address: Required parameter missing";

	}

	elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_city']))){

		$response['errorcode'] = "400009";

		$response['ok'] = 0;

		$response['message']="Delivery City: Required parameter missing";

	}

	elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_state']))){

		$response['errorcode'] = "4000010";

		$response['ok'] = 0;

		$response['message']="Delivery State: Required parameter missing";

	}

	elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_country']))){

		$response['errorcode'] = "4000011";

		$response['ok'] = 0;

		$response['message']="Delivery Country: Required parameter missing";

	}

	elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_zipcode']))){

		$response['errorcode'] = "4000012";

		$response['ok'] = 0;

		$response['message']="Delivery Postcode: Required parameter missing";

	}

	elseif(empty($data['order_delivery_contact'])){

		$response['errorcode'] = "4000013";

		$response['ok'] = 0;

		$response['message']="Contact Number: Required parameter missing";

	}

	elseif (!empty($data)){

		/*$API_USERNAME = 'sandbo_1215254764_biz_api1.angelleye.com' ;

		$API_PASSWORD = '1215254774' ;

		$API_SIGNATURE = 	'AiKZhEEPLJjSIccz.2M.tbyW5YFwAb6E3l6my.pY9br1z2qxKx96W18v' ;

		$API_VERSION = '98.0' ;

		$API_ENDPOINT = 	'https://api-3t.sandbox.paypal.com/nvp'; */

		$user_detail = DB::table('users')->select('*')->where('id', '=' ,$user_id)->get();

		//echo "<pre>";print_r($user_detail);die;

		DB::enableQueryLog();

		$cart_detail ='';

		$cart_listing = DB::table('temp_cart');

		$cart_listing = $cart_listing->select('*');

		$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

		$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

		$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

		$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

		$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

		$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

		$cart_listing = $cart_listing->get();

	//print_r(DB::getQueryLog());

		$user_detail = DB::table('users')->select('*')->where('id', '=' ,$user_id)->get();

		$rest_detail = DB::table('restaurant')->select('*')->where('rest_id', '=' ,$rest_id)->get();

		if(count($cart_listing)>0){

			$cart =$cart_listing[0]->cart_detail;

			$promo_detail =$cart_listing[0]->offer_detail;

			$delivery_fee = $cart_listing[0]->delivery_amt;

			$promo_amt_cal= $cart_listing[0]->offer_amt;

			$sub_total= $cart_listing[0]->subtotal;

			$service_tax = $cart_listing[0]->service_tax_amt;

			$total_amt = $cart_listing[0]->total_amt;

			$order_promo_applied = $cart_listing[0]->offer_applied; ;

			$order_food_promo = $cart_listing[0]->food_promo_list; ;

			$order_food_promo_applied =$cart_listing[0]->food_offer_applied; ;

			$asap_or_not = trim($this->param['asap_or_not']);

			$order_typetime = '';

			if($asap_or_not==0)

			{

				$order_typetime = 'later';

			}

			if($asap_or_not==1)

			{

				$order_typetime = 'asp';

			}

			$orderdata = new Order;

			$orderdata->rest_id	           =  $rest_id;

			$orderdata->order_restcartid   = $cart_listing[0]->rest_cartid;

			if($user_id>0)

			{

				$order_username   = $user_detail[0]->name;

				$orderdata->user_type	       = '1';

				$orderdata->user_id	           = $user_id ;

				$orderdata->order_fname	       = $user_detail[0]->name;

				$orderdata->order_lname		   = $user_detail[0]->lname;

				$orderdata->order_email		   = $user_detail[0]->email;

				$orderdata->order_tel		   = $user_detail[0]->user_mob;

				$orderdata->order_carrier_email = $user_detail[0]->carrier_email;

				$orderdata->order_address	   = $user_detail[0]->user_address;

				$orderdata->order_city	       = $user_detail[0]->user_city;

				$orderdata->order_pcode		   = $user_detail[0]->user_zipcode;

			}

			else

			{

				$orderdata->user_type	       = '0';

				$order_username   = $guest_name;

				$carrier_email = '';

				switch($guest_carrier)

				{

					case 1:

					$carrier_id = '1';

					$carrier_name = 'Alltel';

					$carrier_email = '@message.alltel.com';

					break;

					case 2:

					$carrier_id = '2';

					$carrier_name = 'AT&T';

					$carrier_email = '@txt.att.net';

					break;

					case 3:

					$carrier_id = '3';

					$carrier_name = 'Boost Mobile';

					$carrier_email = '@myboostmobile.com';

					break;

					case 4:

					$carrier_id = '4';

					$carrier_name = 'Sprint';

					$carrier_email = '@messaging.sprintpcs.com';

					break;

					case 5:

					$carrier_id = '5';

					$carrier_name = 'T-Mobile';

					$carrier_email = '@tmomail.net';

					break;

					case 6:

					$carrier_id = '6';

					$carrier_name = 'U.S. Cellular';

					$carrier_email = '@email.uscc.net';

					break;

					case 7:

					$carrier_id = '7';

					$carrier_name = 'Verizon';

					$carrier_email = '@vtext.com';

					break;

					case 8:

					$carrier_id = '8';

					$carrier_name = 'Virgin Mobile';

					$carrier_email = '@vmobl.com';

					break;

					case 9:

					$carrier_id = '9';

					$carrier_name = 'Republic Wireless';

					$carrier_email = '@text.republicwireless.com';

					break;

				}

				$orderdata->order_guestid	   = $guest_id;

				$orderdata->order_fname	       = $guest_name;

				$orderdata->order_lname		   = '';

				$orderdata->order_carrier_email = $carrier_email;

				$orderdata->order_email		   = $guest_email;

				$orderdata->order_tel		   = $guest_mobileno;

				$orderdata->order_address	   = '';

				$orderdata->order_city	       = '';

				$orderdata->order_pcode		   = '';

			}

			$orderdata->order_type		   = $ordertype;

			$orderdata->order_typetime	   = $order_typetime;

			$orderdata->order_pickdate	   = $order_date;

			$orderdata->order_picktime	   = $order_time;

			$orderdata->order_pickarea	   = '';

			$orderdata->order_instruction  = $order_instruction;

			$orderdata->order_status       = '1';

			$orderdata->order_carditem     = $cart;

			$orderdata->order_promo_detail  = $promo_detail;

			$orderdata->order_deliveryfee =$delivery_fee;

			$orderdata->order_create	   = date('Y-m-d');

			$orderdata->order_promo_mode = '';

			$orderdata->order_promo_val = '';

			$orderdata->order_promo_cal = $promo_amt_cal;

			$orderdata->order_subtotal_amt = $sub_total;

			$orderdata->order_service_tax = $service_tax;

			/***** PARTIAL PAYMENT *****/

	//$orderdata->order_pmt_type= $payment_mode;

	//$payment_mode = 1 Full Payment

			if($payment_mode==1){

				$payment_pay_status = '1';

				$orderdata->order_pmt_type= $payment_mode;

				$orderdata->order_partial_percent = '0.00';

				$orderdata->order_partial_payment = '0.00';

				$orderdata->order_partial_remain = '0.00';

				$orderdata->order_paid_amt   = $total_amt;

				$orderdata->order_remaing_amt	= '0.00';

			}

			else

			{

				$orderdata->order_pmt_type= '2';

				$payment_pay_status = '2';

				$orderdata->order_partial_percent = $rest_detail[0]->rest_partial_percent;

				$orderdata->order_partial_payment = $partial_pay_amt;

				$orderdata->order_partial_remain = $partial_remain_amt;

				$orderdata->order_paid_amt   =  $partial_pay_amt;

				$orderdata->order_remaing_amt	= $partial_remain_amt;

			}

			if($paymenttype=='Cash')

			{

				$orderdata->order_paid_amt   =  '0.00';

				$orderdata->order_remaing_amt	= $total_amt;

			}

			/***** PARTIAL PAYMENT END*****/

			$orderdata->order_total = $total_amt;

			$orderdata->order_tip =  $order_tip;

			$orderdata->order_pmt_method= $paymenttype;

			$orderdata->order_deliveryadd1= $order_delivery_address;

			$orderdata->order_deliveryadd2= '';

			$orderdata->order_deliverysurbur= $order_delivery_city;

			$orderdata->order_deliverypcode= $order_delivery_zipcode;

			$orderdata->order_deliverynumber=  $order_delivery_contact;

			$orderdata->order_promo_applied = $order_promo_applied ;

			$orderdata->order_food_promo = $order_food_promo ;

			$orderdata->order_food_promo_applied =$order_food_promo_applied;

			$orderdata->order_device =trim($this->param['devicetype']);

			$orderdata->order_deviceid =trim($this->param['deviceid'])	;

			$orderdata->order_devicename =trim($this->param['device_name']);

			$orderdata->order_devicename =trim($this->param['device_name']);

			$orderdata->order_device_os=trim($this->param['os_name']);

			$email_subject = 'Your order received.';

			$email_content_detail = DB::table('email_content')

			->select('*')

			->where('email_id', '=' ,'3')

			->get();

			$email_content =$email_content_detail[0]->email_content;

			if($paymenttype=='Paypal')

			{

				$orderdata->save();

				$order_id = $orderdata->order_id;

				$paydata = new Order_payment;

				$paydata->pay_orderid = $order_id;

				$paydata->pay_method =$paymenttype;

				$paydata->pay_userid = $user_id ;

				$paydata->pay_tx = '';

				$paydata->pay_st = '';

				$paydata->pay_amt ='';

				$paydata->pay_cc = '';

				$paydata->pay_status=$payment_pay_status;

				$paydata->save();

				$pay_id = $paydata->pay_id;

				$response['order_id']=$order_id;

				$response['ok'] = 1;

				$response['message']="Order save successfully!";

			}

			elseif($paymenttype=='Cash')

			{

				$unq_id = trim($this->order_uniqueid(6));

				$orderdata->order_uniqueid= trim($unq_id);

				$orderdata->save();

				$order_id = $orderdata->order_id;

				$paydata = new Order_payment;

				$paydata->pay_orderid = $order_id;

				$paydata->pay_method =$paymenttype;

				$paydata->pay_userid = $user_id ;

				$paydata->pay_tx = '';

				$paydata->pay_st = '';

				$paydata->pay_amt = $total_amt;

				$paydata->pay_cc = 'USD';

				$paydata->pay_status=$payment_pay_status;

				$paydata->save();

				$pay_id = $paydata->pay_id;

				DB::table('temp_cart')->where('rest_cartid', '=', trim($rest_cartid))->delete();

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'3')

				->get();

						//$email_content = "hello raveena jaodn";

				$email_content = $email_content_detail[0]->email_content;

				$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

				$replaceArray = array($order_id, $unq_id);

				$msg = str_replace($searchArray, $replaceArray, $email_content);

				/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/

				$notification_title = 'Order received';

						//$notification_description = 'We are receive your order successfully!';

				$notification_description = $msg'.'.$email_content;

						//$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->get();

				if($user_id>0){

					$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->where('devicetype', '=', trim($devicetype))->get();

				}

				else

				{

					$token_data = DB::table('device_token')->where('guest_id', '=', trim($guest_id))->where('devicetype', '=', trim($devicetype))->get();

				}

				if($token_data)		{

					$target = $token_data[0]->devicetoken;

						// $this->sendNotification($target,$user_id, $notification_title, $notification_description,$devicetype);

					if($devicetype=='android')

					{

						$this->sendNotification($target,$user_id, $notification_title, $notification_description,$devicetype);

					}

					elseif($devicetype=='iphone')

					{

						$this->send_notification_ios($target,$notification_title, $notification_description);

					}

					$notifiy = new Notification_list;

					$notifiy->noti_userid = $user_id;

					$notifiy->noti_guestid = $guest_id;

					$notifiy->noti_title = $notification_title;

					$notifiy->noti_desc = $notification_description;

					$notifiy->noti_deviceid = $deviceid;

					$notifiy->noti_devicetype = $devicetype;

					$notifiy->noti_read = '0';

					$notifiy->save();

					$noti_id = $notifiy->noti_id;

				}

				/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/

				$response['order_id']=$order_id;

				$response['order_uniqueid']=$unq_id;

				$response['ok'] = 1;

				$response['message']='Your order received';

			}

			elseif($paymenttype=='Card')

			{

				$order_detail = DB::table('order')->select('*')->where('order_restcartid', '=' ,$rest_cartid)->get();

				if($order_detail)

				{

							//$order_id = $this->param['order_id'];

					$order_id = $order_detail[0]->order_id;

				}

				else

				{

					$orderdata->save();

					$order_id = $orderdata->order_id;

				}

				/************************** CART DATA START ***********************/

				$result_array = '';

				$EXPDATE = $card_expdate;

				$request_params = array

				(

					'METHOD' => 'DoDirectPayment',

								/*'USER' => $API_USERNAME,

								'PWD' => $API_PASSWORD,

								'SIGNATURE' => $API_SIGNATURE,

								'VERSION' =>$API_VERSION,

								'PAYMENTACTION' => 'Sale',

								'IPADDRESS' => $_SERVER['REMOTE_ADDR'],*/

								'CREDITCARDTYPE' => $card_name,

								'ACCT' => $card_number,

								'EXPDATE' => $EXPDATE,

								'CVV2' => $card_cvv,

								'FIRSTNAME' => $order_username,

								'STREET' => $order_delivery_address,

								'STATE' => $order_delivery_state,

								'COUNTRYCODE' => '',

								'AMT' => $total_amt,

								'CURRENCYCODE' => 'USD',

								'DESC' => 'Testing Payments Pro'

							);

						/*$nvp_string = '';

						foreach($request_params as $var=>$val)

						{

							$nvp_string .= '&'.$var.'='.urlencode($val);

						}

						$curl = curl_init();

						curl_setopt($curl, CURLOPT_VERBOSE, 1);

						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

						curl_setopt($curl, CURLOPT_TIMEOUT, 30);

						curl_setopt($curl, CURLOPT_URL, $API_ENDPOINT);

						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

						curl_setopt($curl, CURLOPT_POST, 1);

						curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

							$result = curl_exec($curl);

						curl_close($curl);

							$result_array = $this->NVPToArray($result);

							*/

								//$data['result_array'] = $result_array;

								//echo "hello";

							if(isset($this->param['ACK']))

							{

								if($this->param['ACK'] =='submitted_for_settlement')

								{

										//echo  $this->param['ACK'];die;

									$unq_id = $this->order_uniqueid(6);

									DB::table('order')->where('order_id', $order_id)->update(['order_uniqueid' => trim($unq_id)]);

									$paydata = new Order_payment;

									$paydata->pay_orderid = $order_id;

									$paydata->pay_method =$paymenttype;

									$paydata->pay_userid = $user_id ;

									$paydata->pay_tx = $this->param['TRANSACTIONID'];

									$paydata->pay_st =$this->param['ACK'];

									$paydata->pay_amt = $total_amt;

									$paydata->pay_cc = $this->param['CURRENCYCODE'];

									$paydata->pay_status=$payment_pay_status;

									$paydata->save();

									$pay_id = $paydata->pay_id;

									DB::table('temp_cart')->where('rest_cartid', '=', trim($rest_cartid))->delete();

									/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/

									$notification_title = 'Order received';

						//$notification_description = 'We are receive your order sucessfully!';

									$email_content_detail = DB::table('email_content')

									->select('*')

									->where('email_id', '=' ,'3')

									->get();

						//$email_content = "hello raveena jaodn";

									$email_content = $email_content_detail[0]->email_content;

									$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

									$replaceArray = array($order_id, $unq_id);

									$notification_description = str_replace($searchArray, $replaceArray, $email_content);

						//$notification_description = 'We received your order successfully! Your order No is :'.$order_id .' AND Unique No: '.$unq_id.'. '.$email_content;

						//$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->get();

									if($user_id>0){

										$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->where('devicetype', '=', trim($devicetype))->get();

									}

									else

									{

										$token_data = DB::table('device_token')->where('guest_id', '=', trim($guest_id))->where('devicetype', '=', trim($devicetype))->get();

									}

									if($token_data)

									{

										$target = $token_data[0]->devicetoken;

							// $this->sendNotification($target,$user_id, $notification_title, $notification_description,$devicetype);

										if($devicetype=='android')

										{

											$this->sendNotification($target,$user_id, $notification_title, $notification_description,$devicetype);

										}

										elseif($devicetype=='iphone')

										{

											$this->send_notification_ios($target,$notification_title, $notification_description);

										}

										$notifiy = new Notification_list;

										$notifiy->noti_userid = $user_id;

										$notifiy->noti_guestid = $guest_id;

										$notifiy->noti_title = $notification_title;

										$notifiy->noti_desc = $notification_description;

										$notifiy->noti_deviceid = $deviceid;

										$notifiy->noti_devicetype = $devicetype;

										$notifiy->noti_read = '0';

										$notifiy->save();

										$noti_id = $notifiy->noti_id;

									}

									/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/

									$response['order_id']=$order_id;

									$response['order_uniqueid']=$unq_id;

									$response['ok'] = 1;

									$response['message']='Your order successfully save';

								}

								else

								{

									$response['order_id']=$order_id;

									$response['errorcode'] = "4000014";

									$response['ok'] = 0;

									$response['message']=$this->param['ACK'].' '.$$this->param['error'];

								}

							}

							/************************** CART DATA END ***********************/

						}

					//echo $cart_listing[0]->cart_detail;

					//exit;

					}

					else

					{

						$response['errorcode'] = "4000015";

						$response['ok'] = 0;

						$response['message']="Invalid cart id";

					}

				}

				header('Content-type: application/json');

				echo json_encode($response);

			}





			/************************************************************/

	//	FUNCTION NAME : Food_Delete

	//	FUNCTION USE :  RESTAURANT FOOD ITEM ADD IN CART

	//	FUNCTION ERRORCODE : 160000

			/****************************************************/

			public function food_itemdelete()

			{

				$data['rest_cartid']=$rest_cartid = Input::get("rest_cartid");

				$data['cart_id'] = Input::get("cart_id");

				$data['user_id'] = Input::get("user_id");

				$data['guest_id'] = Input::get("guest_id");

				$data['rest_id'] = Input::get("rest_id");

				$data['menu_id'] = Input::get("menu_id");

				$data['menu_catid'] = Input::get("menu_catid");

				$data['deviceid'] = Input::get("deviceid");

				$data['devicetype'] = Input::get("devicetype");

				if(empty($data['cart_id'])){

					$response['errorcode'] = "160000";

					$response['ok'] = 0;

					$response['message']="Cart Id:Required parameter missing";

				}else if(empty($data['rest_id'])){

					$response['errorcode'] = "160001";

					$response['ok'] = 0;

					$response['message']="Restaurant Id:Required parameter missing";

				}elseif(empty($data['deviceid'])){

					$response['errorcode'] = "160002";

					$response['ok'] = 0;

					$response['message']="Device Id:Required parameter missing";

				}elseif(empty($data['devicetype'])){

					$response['errorcode'] = "160003";

					$response['ok'] = 0;

					$response['message']="Device type:Required parameter missing";

				}elseif(empty($data['rest_cartid'])){

					$response['errorcode'] = "160004";

					$response['ok'] = 0;

					$response['message']="Rest Cart Id:Required parameter missing";

				}elseif(!empty($data)){

					DB::table('temp_cart')

					->where('user_id', '=' ,trim($this->param['user_id']))

					->where('guest_id', '=' ,trim($this->param['guest_id']))

					->where('cart_id', '=' ,trim($this->param['cart_id']))

					->where('rest_cartid', '=' ,trim($this->param['rest_cartid']))

					->where('deviceid', '=' ,trim($this->param['deviceid']))

					->where('devicetype', '=' ,trim($this->param['devicetype']))

					->delete();

					$response['message'] = "Cart Item Deleted.";

					$response['ok'] = 1;

					$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

				}

				header('Content-type: application/json');

				echo json_encode($response);

			}

          






		}

		