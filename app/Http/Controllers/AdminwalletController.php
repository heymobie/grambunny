<?php
namespace App\Http\Controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Route;
class AdminwalletController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}

 function my_wallet()
	{

		$payment_detail = '';

		$payment_detail  =  DB::table('order_payment')		
						->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')
						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')
						->leftJoin('users', 'order.user_id', '=', 'users.id')
						->where('order_payment.pay_status', '>', 0)
						->select('*')
						->orderBy('order_payment.pay_id', 'desc')
						->get();

			$vendor_list  =  DB::table('vendor')->get();
       		$wallet_amount  =  DB::table('order_payment')		
						->where('pay_status', '=', 1)
						->sum('pay_amt');
			$payed_amount  =  DB::table('vendor_payment_request')		
							->where('transfer_status', '=', 2)
							->sum('request_amount');	

			$vendor_payment_detail	= DB::table('admin_vendor_transaction')
										->where('txn_method', '=', 'wallet')
										->orderBy('admin_vendor_transaction.id', 'desc')
										->get();
			$vendor_request_detail	= DB::table('vendor')
						->Join('vendor_bank_details', 'vendor.vendor_id', '=', 'vendor_bank_details.vendor_id')
						->Join('admin_vendor_transaction', 'vendor.vendor_id', '=', 'admin_vendor_transaction.vendor_id')
						->where('txn_method', '=', 'bank')
						->select('vendor.name','vendor.wallet_amount','vendor_bank_details.*','admin_vendor_transaction.*')
						->orderBy('admin_vendor_transaction.id', 'desc')
						->get();
	//echo "<pre>"; print_r($vendor_request_detail); die;
  		$data_onview = array('payment_detail'=>$payment_detail,'vendor_payment_detail'=>$vendor_payment_detail,'vendor_request_detail'=>$vendor_request_detail,'vendor_list'=>$vendor_list,'wallet_amount'=>$wallet_amount, 'payed_amount'=>$payed_amount); 					   	
    	return view('admin.my_wallet')->with($data_onview);

	}


 function dm_wallet()
	{

	DB::enableQueryLog(); 

	$dm_id = '';
	$dm_bank_detail = ''; 		

	if(Route::current()->getParameter('id')){
    
    $dm_id = Route::current()->getParameter('id');
    $dm_bank_detail = DB::table('dm_bank_details')->where('dm_id', '=', $dm_id)->first();

	}

	$payment_detail = '';

	$payment_detail  =  DB::table('delivery_man_orders')		
						->leftJoin('order', 'delivery_man_orders.order_id', '=', 'order.order_id')
						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')
						->leftJoin('deliveryman', 'delivery_man_orders.dm_id', '=', 'deliveryman.id')
						->where('delivery_man_orders.order_status', '=', 3)
						->select('*')
						->orderBy('delivery_man_orders.order_id', 'desc')
						->get();

	$transfer_bank =  DB::table('delivery_man_orders')		
						->leftJoin('order', 'delivery_man_orders.order_id', '=', 'order.order_id')
						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')
						->leftJoin('deliveryman', 'delivery_man_orders.dm_id', '=', 'deliveryman.id')
						->where('delivery_man_orders.order_status', '=', 3)
						->select('*')
						->orderBy('delivery_man_orders.order_id', 'desc')
						->get();					

    $distribution =  DB::table('delivery_man_orders')		
						->leftJoin('order', 'delivery_man_orders.order_id', '=', 'order.order_id')
						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')
						->leftJoin('deliveryman', 'delivery_man_orders.dm_id', '=', 'deliveryman.id')
						->where('delivery_man_orders.order_status', '=', 3)
						->select('*')
						->orderBy('delivery_man_orders.order_id', 'desc')
						->get();

	//$dmcount = count($payment_detail);					

			$deliveryman_list  =  DB::table('deliveryman')->get();	

	
  		$data_onview = array('payment_detail'=>$payment_detail,'transfer_bank'=>$transfer_bank,'distribution'=>$distribution,'deliveryman_list'=>$deliveryman_list,'dm_id'=>$dm_id,'dm_bank_detail'=>$dm_bank_detail); 					   	
    	return view('admin.dm_wallet')->with($data_onview);

	}

    public function bank_setup()
	{


		$dm_id = Input::get('dm_id');

		$holder_name = Input::get('holder_name');
		$bank_name = Input::get('bank_name');	
		$branch = Input::get('branch');	
		$account_number = Input::get('account_number');	
		$ifsc_code = Input::get('ifsc_code');
		
		$existVendor = DB::table('dm_bank_details')->where('dm_id', '=', $dm_id)->first();

		if($existVendor === null)
		{
			DB::table('dm_bank_details')->insert(
			    ['dm_id' => $dm_id,
			     'holder_name' => $holder_name,
			     'bank_name' => $bank_name,
			 	 'branch' => $branch,
			 	 'account_number' => $account_number, 
			 	 'ifsc' => $ifsc_code,
			 	 'status' => 1
			 	]
			);

			Session::flash('message', 'Bank setup successfully!');
			return redirect()->to("/admin/dm_wallet");  
		} else {

			DB::table('dm_bank_details')
            ->where('dm_id', $dm_id)
            ->update(['holder_name' => $holder_name,
				     'bank_name' => $bank_name,
				 	 'branch' => $branch,
				 	 'account_number' => $account_number,
				 	 'ifsc' => $ifsc_code,
				 	 'status' => 1,
				 	 'updated_at' => date("Y-m-d h:i:sa")
				    ]);

			Session::flash('message', 'Bank details updated successfully!'); 
			return redirect()->to('/admin/dm_wallet');
		}

	}


    public function dm_status()
	{


	DB::enableQueryLog(); 

	$order_id = '';
	//$dm_orders = ''; 		

	if(Route::current()->getParameter('id')){
    
    $order_id = Route::current()->getParameter('id');
    //$dm_orders = DB::table('delivery_man_orders')->where('order_id', '=', $order_id)->first();

	}

			DB::table('delivery_man_orders')
            ->where('order_id', $order_id)
            ->update(['pay_status' => 1]);

			Session::flash('message', 'Payment status change successfully!'); 
			return redirect()->to('/admin/dm_wallet');

	}	

	public function transfer_vendor(Request $request)
	{

		 $postData = Request::all();
         $vendorid = trim(Input::get('vendorid'));
         $vamount = trim(Input::get('vamount'));
         $acommition = trim(Input::get('acommition'));

         $commition = ($vamount*$acommition)/100;
         $totalamt = $vamount-$commition;


         $wallet_amount  =  DB::table('vendor')		
						->where('vendor_id', '=', $vendorid)
						->value('wallet_amount');

        if(!empty($wallet_amount)){

        $vendoramt = $wallet_amount+$totalamt;

        }else{

       $vendoramt = $totalamt;

        }

        $admin_amount =  DB::table('order_payment')		
						->where('pay_status', '=', 1)
						->sum('pay_amt');

	 if($admin_amount >= $totalamt){	

		//$admintamout = $admin_amount-$totalamt;	

		//DB::table('admins')->where('id', '=', 1)
		//		          ->update(['wallet_amount' =>$admintamout]);

         DB::table('vendor')->where('vendor_id','=',$vendorid)

			->update(['wallet_amount' =>$vendoramt]);

			$walletdata = array(
			'vendor_id' => $vendorid,
			'vendor_amount' => $vamount,
			'admin_commission_per' => $acommition,
			'commission_amount' => $commition,
			'total_pay_amount' => $totalamt,
			'txn_method' => 'wallet',
		);

		$affected = DB::table('admin_vendor_transaction')->insert($walletdata);

		if($affected){

			Session::flash('message', 'Payment send in vendor wallet sucessfully !');
			return redirect()->to('admin/my_wallet');

		}					  
            

     }else{

         	Session::flash('message', 'Payment Show In Vendor Wallet Failed !');
			return redirect()->to('admin/my_wallet');

     }


	}

	public function request_amount_form(Request $request)
	{
		echo "<pre>chnageStatus"; print_r(Input::get('chnageStatus'));
		echo "<pre>vendorrequestid"; print_r(Input::get('vendorrequestid'));
		echo "<pre>request_amount"; print_r(Input::get('request_amount'));
		echo "<pre>vendorvendorid"; print_r(Input::get('vendorvendorid'));
		echo "<pre>vendorwalletamount"; print_r(Input::get('vendorwalletamount'));

		

         $vendorid = Input::get('vendorvendorid');
         $vamount = Input::get('vendorwalletamount');
         $reqamount = Input::get('request_amount');

		if($vamount >= $reqamount){

			DB::table('admin_vendor_transaction')
            ->where('id', Input::get('vendorrequestid'))
            ->update(['transfer_status' =>Input::get('chnageStatus')
					 ]);
            if(Input::get('chnageStatus') == 2){
            	$vendoramt = $vamount-$reqamount;

				DB::table('vendor')->where('vendor_id','=',$vendorid)
							  ->update(['wallet_amount' =>$vendoramt]);
				}			

			Session::flash('message', 'Status Updated Sucessfully !');
			return redirect()->to('admin/my_wallet');

		}else{

			Session::flash('message', 'Amount not sufficient.');
			return redirect()->to('admin/my_wallet');

		}
        
	}		

}