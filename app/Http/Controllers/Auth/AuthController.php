<?php







namespace App\Http\Controllers\Auth;







use App\User;



use Validator;



use App\Http\Controllers\Controller;





use Illuminate\Support\Facades\Input;



use App\User_otpdetail;

use Mail;



use DB;

use Session;

use Auth;





use Cart;











class AuthController extends Controller



{



	/*



    |--------------------------------------------------------------------------



    | Registration & Login Controller



    |--------------------------------------------------------------------------



    |



    | This controller handles the registration of new users, as well as the



    | authentication of existing users. By default, this controller uses



    | a simple trait to add these behaviors. Why don't you explore it?



    |



    */













	/**



	 * Where to redirect users after login / registration.



	 *



	 * @var string



	 */



	// protected $redirectTo = '/';



	protected $redirectTo = '/myaccount';



	protected $redirectAfterLogout = '/';



	//protected $guard = 'admin';







	/**



	 * Create a new authentication controller instance.



	 *



	 * @return void



	 */



	public function __construct()



	{



		$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
	}







	/**



	 * Get a validator for an incoming registration request.



	 *



	 * @param  array  $data



	 * @return \Illuminate\Contracts\Validation\Validator



	 */



	protected function validator(array $data)



	{



		return Validator::make($data, [



			'name' => 'required|max:255',



			'lname' => 'required|max:255',



			'email' => 'required|email|max:255|unique:users',

			'user_mob' => 'required|user_mob|max:10',



			'password' => 'required|min:6|confirmed',



		]);
	}







	/**



	 * Create a new user instance after a valid registration.



	 *



	 * @param  array  $data



	 * @return User



	 */



	protected function create(array $data)



	{



		return User::create([



			'name' => $data['name'],



			'lname' => $data['lname'],



			'email' => $data['email'],



			'user_mob' => $data['user_mob'],



			'user_status' => '1',



			'password' => bcrypt($data['password']),



			'user_pass' => md5($data['password']),



		]);
	}





	function generateRandomString($length = 6)
	{

		$characters = '0123456789';

		$charactersLength = strlen($characters);

		$randomString = '';

		for ($i = 0; $i < $length; $i++) {

			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}



	public function user_duplicate_email()

	{



		// Get the value from the form



		$input['_token'] = Input::get('_token');

		$input['email'] = Input::get('email');

		$input1['user_mob'] = Input::get('user_mob');



		// Must not already exist in the `email` column of `users` table



		$rules = array('email' => 'unique:users,email');

		$validator = Validator::make($input, $rules);



		$rules1 = array('user_mob' => 'unique:users,user_mob');

		$validator1 = Validator::make($input1, $rules1);







		if (($validator->fails()) && ($validator1->fails())) {

			return response()->json(array('sucess_message' => '', 'status' => '1', 'user_id' => '0'));
		} elseif ($validator->fails()) {

			return response()->json(array('sucess_message' => '', 'status' => '2', 'user_id' => '0'));
		} elseif ($validator1->fails()) {

			return response()->json(array('sucess_message' => '', 'status' => '3', 'user_id' => '0'));
		} else {



			$carrier_id = '';

			$carrier_name = '';

			$carrier_email = '';



			$carrier_id =  Input::get('user_mob_type');



			switch ($carrier_id) {

				case 1:

					$carrier_id = '1';

					$carrier_name = 'Alltel';

					$carrier_email = '@message.alltel.com';

					break;

				case 2:

					$carrier_id = '2';

					$carrier_name = 'AT&T';

					$carrier_email = '@txt.att.net';

					break;

				case 3:

					$carrier_id = '3';

					$carrier_name = 'Boost Mobile';

					$carrier_email = '@myboostmobile.com';

					break;

				case 4:

					$carrier_id = '4';

					$carrier_name = 'Sprint';

					$carrier_email = '@messaging.sprintpcs.com';

					break;

				case 5:

					$carrier_id = '5';

					$carrier_name = 'T-Mobile';

					$carrier_email = '@tmomail.net';

					break;

				case 6:

					$carrier_id = '6';

					$carrier_name = 'U.S. Cellular';

					$carrier_email = '@email.uscc.net';

					break;

				case 7:

					$carrier_id = '7';

					$carrier_name = 'Verizon';

					$carrier_email = '@vtext.com';

					break;

				case 8:

					$carrier_id = '8';

					$carrier_name = 'Virgin Mobile';

					$carrier_email = '@vmobl.com';

					break;

				case 9:

					$carrier_id = '9';

					$carrier_name = 'Republic Wireless';

					$carrier_email = '@text.republicwireless.com';

					break;
			}



			$checkout_verificaiton = 0;



			if (isset($_POST['two_factor'])) {

				$checkout_verificaiton = $_POST['two_factor'];
			}



			$checkout_contact = Input::get('user_mob') . $carrier_email;

			$checkout_email = trim(Input::get('email'));



			$user_password = Input::get('password');



			$user = new User;

			$user->name = trim(Input::get('name'));

			$user->lname = trim(Input::get('lname'));

			$user->email = trim(Input::get('email'));

			$user->user_mob = Input::get('user_mob');

			$user->user_status = Input::get('user_status');

			//$user->user_status = '0';

			$user->password =  bcrypt(trim(Input::get('password')));

			$user->user_pass = md5(Input::get('password'));

			$user->carrier_id = $carrier_id;

			$user->carrier_name = $carrier_name;

			$user->carrier_email = $carrier_email;

			$user->dummy1 = Input::get('dummy1');

			$user->dummy2 = Input::get('dummy2');

			$user->dummy3 = Input::get('dummy3');

			$user->checkout_verificaiton = $checkout_verificaiton;

			$user->save();

			$user_id = $user->id;

			Session::put('userid', $user_id);



			$otp_for = 'Guset_user_verification';

			$subject_auth = 'Email / Mobile verification OTP';







			if ((!empty($checkout_email)) && (!empty($checkout_contact))) {



				$user_carrier_email = $checkout_contact;

				$user_email_send_mail = $checkout_email;

				$sucess_message = "OTP has been sent on your Email .please verify it";
			} elseif ((empty($checkout_email)) && (!empty($checkout_contact))) {



				$user_carrier_email = $checkout_contact;

				$user_email_send_mail = $checkout_email;



				$sucess_message = "OTP has been sent on your mobile number.please verify it";
			} elseif ((!empty($checkout_email)) && (empty($checkout_contact))) {



				$user_carrier_email = $checkout_contact;

				$user_email_send_mail = $checkout_email;



				$sucess_message = "OTP has been sent on your Email .please verify it";
			}





			$otp_code = trim($this->generateRandomString(6));







			$otp_data = new User_otpdetail;

			$otp_data->user_id = trim($user_id);

			$otp_data->otp_for = trim($otp_for);

			$otp_data->user_mob = trim($checkout_contact);

			$otp_data->user_email = trim($checkout_email);

			$otp_data->otp_number = $otp_code;

			$otp_data->save();

			$otp_inserted_id = $otp_data->otp_id;

			Session::put('otpid', $otp_inserted_id);







			/*********** EMAIL FOR SEN OTP START *************/





			//$msg_reg_otp ='Hello User,The verification code for two factor auth on grambunny is : '.$otp_code ;



			Session::put('user_mob', Input::get('user_mob'));

			$msg_reg_otp = 'grambunny verification code is : ' . $otp_code;

			$curl = curl_init();

			$from = "+19294151309";
			$to = Input::get('user_mob');
			$msg = $msg_reg_otp;

			$data = "{\"from\":{\"phoneNumber\":\"$from\"},\"text\":\"$msg\",\"to\":[{\"phoneNumber\":\"$to\"}]}";

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://platform.devtest.ringcentral.com/restapi/v1.0/account/281244004/extension/281244004/sms",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => $data,
				CURLOPT_HTTPHEADER => array(
					"Content-Type: application/json",
					"Accept: application/json",
					"Authorization: Bearer U0pDMTFQMDFQQVMwMHxBQUFWTFYxNWo4dkZJNTJQblB1aXJFQzl1bnJZVUFXdE9PUElHVXVVZ2dvUDJaNnhsLUJLS1VnWFlxdG9scEJoLS1mTjdjWDZjaDlFRnBUREtkd1FCUDdyVHNaZzR1QzZlakZMTlBUSEhES1ZTZmtxb094YWVGb1IwM0U1LXBGczNEX21YenRmOFpyajl4enRURERKRVhHbnVlNFRYdkdISjJ1cFc0ai1GZHRDV1o0SXpVbFRld1hiNkwyNmJReVdRZ3xKdmhKTFF8bVROQWczNms5YnJiMExwcFZfWHI1UXxBUQ"
				),
			));

			$response = curl_exec($curl);

			curl_close($curl);

			//echo $response; die;





			if (!empty($user_carrier_email)) {



				/* Mail::raw($msg_reg_otp, function ($message) use ($user_carrier_email,$subject_auth){

    				$message->from('app@grambunny.com', 'grambunny');



    				$otp_email = $user_carrier_email;



    				$message->to($otp_email);

    				//$message->bcc('votiveshweta@gmail.com');

    				//$message->bcc('votivemobile.pankaj@gmail.com');

    				//$message->bcc('votivemobile.dilip@gmail.com');

    				//$message->bcc('votiveiphone.hariom@gmail.com');

    				//$message->bcc('zubaer.votive@gmail.com');

    				$message->subject($subject_auth);

    				Session::put('resendemail', $otp_email);

    			}); */
			}



			if (!empty($user_email_send_mail)) {



				/* Mail::raw($msg_reg_otp, function ($message) use ($user_email_send_mail,$subject_auth){

    				$message->from('app@grambunny.com', 'grambunny');



    				$otp_email = $user_email_send_mail;



    				$message->to($otp_email);

    				//$message->bcc('votiveshweta@gmail.com');

    				//$message->bcc('votivemobile.pankaj@gmail.com');

    				//$message->bcc('votivemobile.dilip@gmail.com');

    				//$message->bcc('votiveiphone.hariom@gmail.com');

    				//$message->bcc('zubaer.votive@gmail.com');

    				//$message->subject($subject_auth);

    				Session::put('resendemail', $otp_email);

    			}); */
			}





			return response()->json(array('sucess_message' => $sucess_message, 'status' => '4', 'user_id' => $user_id));
		}
	}



	public function resend_otp()

	{



		// Get the value from the form

		$email = Session::get('resendemail');



		// Must not already exist in the `email` column of `users` table



		$subject_auth = 'Email / Mobile verification OTP';

		$otp_code = trim($this->generateRandomString(6));

		$sucess_message = "OTP has been sent on your Email .please verify it";



		$otp_data = DB::table('user_otpdetail')

			->where('user_email', $email)

			->update(['otp_number' => $otp_code]);



		$otp_inserted_id = Session::get('otpid');

		$user_id = Session::get('userid');





		/*********** EMAIL FOR SEND OTP START *************/


		$msg_reg_otp = 'grambunny verification code is : ' . $otp_code;

		$curl = curl_init();

		$from = "+19294151309";
		$to = Session::get('user_mob');
		$msg = $msg_reg_otp;

		$data = "{\"from\":{\"phoneNumber\":\"$from\"},\"text\":\"$msg\",\"to\":[{\"phoneNumber\":\"$to\"}]}";

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://platform.devtest.ringcentral.com/restapi/v1.0/account/281244004/extension/281244004/sms",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Accept: application/json",
				"Authorization: Bearer U0pDMTFQMDFQQVMwMHxBQUFWTFYxNWo4dkZJNTJQblB1aXJFQzl1bnJZVUFXdE9PUElHVXVVZ2dvUDJaNnhsLUJLS1VnWFlxdG9scEJoLS1mTjdjWDZjaDlFRnBUREtkd1FCUDdyVHNaZzR1QzZlakZMTlBUSEhES1ZTZmtxb094YWVGb1IwM0U1LXBGczNEX21YenRmOFpyajl4enRURERKRVhHbnVlNFRYdkdISjJ1cFc0ai1GZHRDV1o0SXpVbFRld1hiNkwyNmJReVdRZ3xKdmhKTFF8bVROQWczNms5YnJiMExwcFZfWHI1UXxBUQ"
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);



		if (!empty($email)) {



			/* Mail::raw($msg_reg_otp, function ($message) use ($email,$subject_auth){

    			$message->from('app@grambunny.com', 'grambunny');

    			$otp_email = $email;

    			$message->to($otp_email);

    			$message->subject($subject_auth);

    			Session::put('resendemail', $otp_email);

    		}); */
		} else {

			return response()->json(array('sucess_message' => $sucess_message, 'status' => '3', 'user_id' => $user_id));
		}



		return response()->json(array('sucess_message' => $sucess_message, 'status' => '4', 'user_id' => $user_id));
	}





	public function get_otp_login()

	{

		$return_url = '';



		$user_id  = trim(Input::get('otp_user_id'));

		$otp_no = trim(Input::get('user_otp_number'));



		$otp_detail = DB::table('user_otpdetail')

			->where('user_id', '=', trim($user_id))

			->where('otp_number', '=', trim($otp_no))

			->get();

		if ($otp_detail) {

			$sucess_message = "OTP verifyed!";



			DB::table('user_otpdetail')

				->where('user_id', '=', trim($user_id))

				->where('otp_number', '=', trim($otp_no))

				->delete();



			DB::table('users')->where('id', '=', trim($user_id))->update(['user_status' =>  '1']);





			/******************* SEND EMAIL AFTER PAYMENT  *******************/



			$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=', '1')

				->get();



			$username  = Input::get('name') . ' ' . Input::get('lname');





			if (empty($username)) {

				$aa_u = explode('@', Input::get('email'));

				$username  = $aa_u[0];
			}



			$data = array(



				'vendor_name' => $username,

				'vendor_email' => Input::get('email'),

				'email_content' => $email_content_detail[0]->email_content



			);

			Mail::send('emails.registration_email', $data, function ($message) use ($data) {



				$message->from('app@grambunny.com', 'grambunny');

				$message->to($data['vendor_email']);

				$message->bcc('votivedeepak.php@gmail.com');

				//$message->bcc('votivemobile.pankaj@gmail.com');

				$message->subject('Registration on www.grambunny.com');
			});



			/******************* SEND EMAIL AFTER PAYMENT  *******************/



			$user_detail = DB::table('users')->where('id', '=', trim($user_id))->get();



			$cart = Cart::content();

			$FILD_NAME = '';



			$FILD_NAME = 'email';

			if (Auth::attempt([$FILD_NAME => Input::get('email'), 'password' => Input::get('password'), 'user_status' => '1'])) {





				$user_id =  Auth::user()->id;



				$user_detail  = DB::table('users')->where('id', '=', $user_id)->get();





				/***** UPDATE CART WHEN GUSET USER IS SIGN-IN  ****/



				$guest_id = Session::get('_token');

				$cart_listing = DB::table('rest_cart');

				$cart_listing = $cart_listing->select('*');

				$cart_listing = $cart_listing->where('guest_id', '=', trim($guest_id));

				$cart_listing = $cart_listing->get();



				if ($cart_listing) {

					DB::table('rest_cart')

						->where('guest_id', trim($guest_id))

						->update([
							'user_id' =>  trim($user_id),

							'guest_id' =>  trim('')

						]);
				}







				/******************         END           ********/





				$cart_user = '';







				$cart_user['delivery_add1'] = trim(Session::get('cart_userinfo')['delivery_add1']);

				$cart_user['delivery_add2'] = trim(Session::get('cart_userinfo')['delivery_add2']);

				$cart_user['delivery_surbur'] = trim(Session::get('cart_userinfo')['delivery_surbur']);

				$cart_user['delivery_postcode'] = trim(Session::get('cart_userinfo')['delivery_postcode']);

				$cart_user['delivery_number'] = trim(Session::get('cart_userinfo')['delivery_number']);







				$cart_user['delivery_fee'] = trim(Session::get('cart_userinfo')['delivery_fee']);

				$cart_user['sub_total'] = trim(Session::get('cart_userinfo')['sub_total']);

				$cart_user['service_tax'] = trim(Session::get('cart_userinfo')['service_tax']);

				$cart_user['delivery_fee_min'] = trim(Session::get('cart_userinfo')['delivery_fee_min']);

				$cart_user['delivery_fee_min_remaing'] = trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

				$cart_user['total_charge'] = trim(Session::get('cart_userinfo')['total_charge']);



				/***** DISCOUNT (PROMOTION) START */



				$cart_user['promo_discount'] = trim(Session::get('cart_userinfo')['promo_discount']);

				$cart_user['promo_discount_id'] = trim(Session::get('cart_userinfo')['promo_discount_id']);

				$cart_user['promo_discount_text'] = trim(Session::get('cart_userinfo')['promo_discount_text']);

				$cart_user['order_promo_detail'] = trim(Session::get('cart_userinfo')['order_promo_detail']);

				$cart_user['order_food_promo'] = trim(Session::get('cart_userinfo')['order_food_promo']);

				$cart_user['order_food_promo_applied'] = trim(Session::get('cart_userinfo')['order_food_promo_applied']);

				$cart_user['order_pmt_type'] = '1';





				/********** END *******/





				$cart_user['pickup_date'] = trim(Session::get('cart_userinfo')['pickup_date']);;

				$cart_user['pickup_time'] = trim(Session::get('cart_userinfo')['pickup_time']);;

				$cart_user['pickup_time_formate'] = trim(Session::get('cart_userinfo')['pickup_time_formate']);;

				$cart_user['rest_meta_url'] = trim(Session::get('cart_userinfo')['rest_meta_url']);;

				$cart_user['rest_id'] = trim(Session::get('cart_userinfo')['rest_id']);;

				$cart_user['deliveri_time'] = '';

				$cart_user['pickup_date'] = date('Y-m-d');

				$cart_user['rest_open_status'] = trim(Session::get('cart_userinfo')['rest_open_status']);





				$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

				$cart_user['firstname_order'] = trim($user_detail[0]->name);

				$cart_user['lastname_order'] = trim($user_detail[0]->lname);

				$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

				$cart_user['email_order'] = trim($user_detail[0]->email);

				$cart_user['address_order'] = trim($user_detail[0]->user_address);

				$cart_user['city_order'] = trim($user_detail[0]->user_city);

				$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

				$cart_user['user_type'] = trim(1);

				$cart_user['user_id'] = trim($user_detail[0]->id);



				$cart_user['service_area'] = '';

				$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);







				/**  PARTICLA PAYMENT PROCESS ***/



				$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

				$cart_user['partical_percent'] = trim(Session::get('cart_userinfo')['partical_percent']);

				$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

				$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);



				/**  PARTICLA PAYMENT PROCESS END ***/





				if (Cart::count()) {







					$transport_data = 0;



					$restaurant_data = 1;





					$response = array();



					$rest_id = '';



					foreach ($cart as $item) {

						if (isset($item->options['rest_id']) && ($item->options['rest_id'] != '')) {

							$rest_id = $item->options['rest_id'];

							break;
						}
					}



					$guest_id = '';

					if (($transport_data == 0) && ($restaurant_data == 1)) {



						DB::enableQueryLog();







						$offer_amt = 0;

						$offer_applied_on_this_cart = '';

						$current_promo = '';

						$service_tax = 0;

						$sub_total_price = 0;

						$promo_id = 0;

						$offer_applied_on_food = '';

						$food_current_promo = '';

						$json_data_session = '';

						$sub_total_cart = 0;



						$cart_food_id = '';



						$cart_detail = '';

						$cart_listing = DB::table('rest_cart');

						$cart_listing = $cart_listing->select('*');

						$cart_listing = $cart_listing->where('user_id', '=', trim($user_id));

						$cart_listing = $cart_listing->where('guest_id', '=', trim($guest_id));

						$cart_listing = $cart_listing->where('rest_id', '=', trim($rest_id));

						$cart_listing = $cart_listing->orderBy('cart_id', 'asc');

						$cart_listing = $cart_listing->get();



						if ($cart_listing) {



							Cart::destroy();



							$delivery_fee = 0;



							$rest_detail = DB::table('restaurant')

								->where('rest_id', '=', $rest_id)

								->where('rest_status', '!=', 'INACTIVE')

								->orderBy('rest_id', 'asc')

								->groupBy('rest_id')

								->get();



							$promo_list = DB::table('promotion')

								->where('promo_restid', '=', $rest_id)

								->where('promo_status', '=', '1')

								->orderBy('promo_buy', 'desc')

								->groupBy('promo_id')

								->get();





							$service_charge = $rest_detail[0]->rest_servicetax;





							if ($cart_listing[0]->order_type == 'delivery') {

								$delivery_fee = $rest_detail[0]->rest_mindelivery;
							}











							$sub_total_cart = 0;

							$json_data_session = '';

							$addon_list = '';



							$food_current_promo = '';



							foreach ($cart_listing as $clist) {





								/********FOOD DETAILS /******/





								$rest_id  = $clist->rest_id;

								$menu_id = $clist->menu_id;

								$menu_cate_id = $clist->menu_catid;

								$menu_subcate_id = $clist->menu_subcatid;

								$addon_id = $clist->menu_addonid;

								$foodsize_price = 0;

								$food_addonPrice = 0;

								$total_price = 0;



								$item_name_cc = '';

								$item_name_cc_ar = '';

								$item_price_cc = '';





								$offer_on_menu_item = '';







								/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/







								foreach ($promo_list as $plist) {



									if (($plist->promo_for == 'each_food') && ($clist->menu_catid == $plist->promo_menu_item)) {

										$current_promo1 = '';



										$food_cart_amount[] = $total_price;

										$food_cart_qty[] = $clist->qty;



										if ($plist->promo_on == 'schedul') {

											$day_name =  strtolower(date("D", strtotime(date('Y-m-d'))));



											$db_day = '';



											if (!empty($plist->promo_day)) {

												$db_day = explode(',', $plist->promo_day);
											}



											if (($plist->promo_start != '0000-00-00') && ($plist->promo_end != '0000-00-00')) {



												if (

													($plist->promo_start <= date('Y-m-d')) &&

													($plist->promo_end >= date('Y-m-d')) &&

													(empty($plist->promo_day)) &&

													($plist->promo_buy <= $clist->qty)

												) {

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$food_current_promo[] = $current_promo1;

													$offer_on_menu_item = $plist->promo_desc;
												}





												if (($plist->promo_start <= date('Y-m-d')) && ($plist->promo_end >= date('Y-m-d'))

													&& (!empty($plist->promo_day))

													&& (in_array($day_name, $db_day))



												) {



													$array_amt[] = $plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$food_current_promo[] = $current_promo1;

													$offer_on_menu_item = $plist->promo_desc;
												}
											} elseif (($plist->promo_start == '0000-00-00') && ($plist->promo_end == '0000-00-00') && (!empty($plist->promo_day)) && (in_array($day_name, $db_day))) {







												$db_day = explode(',', $plist->promo_day);



												if (in_array($day_name, $db_day)) {

													$array_amt[] = $plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$food_current_promo[] = $current_promo1;

													$offer_on_menu_item = $plist->promo_desc;
												}
											}
										} elseif ($plist->promo_on == 'qty') {



											if (($plist->promo_buy <= $clist->qty)) {

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;

												$food_current_promo[] = $current_promo1;

												$offer_on_menu_item = $plist->promo_desc;
											}
										}
									}
								}



								/*************************** END ******************************/





								$menu_list = DB::table('menu')

									->where('restaurant_id', '=', $clist->rest_id)

									->where('menu_id', '=', $clist->menu_id)

									->where('menu_status', '=', '1')

									->orderBy('menu_order', 'asc')

									->get();



								$menu_array = '';

								$food_array = '';



								if (!empty($menu_list)) {



									$menu_id = $menu_list[0]->menu_id;

									$menu_name = $menu_list[0]->menu_name;

									$menu_item_count = 1;

									$food_detail = '';





									$menu_cat_detail = DB::table('menu_category')

										->where('menu_category.rest_id', '=', $rest_id)

										->where('menu_category.menu_id', '=', $menu_id)

										->where('menu_category.menu_category_id', '=', $menu_cate_id)

										->where('menu_category.menu_cat_status', '=', '1')

										->select('menu_category.*')

										->orderBy('menu_category.menu_category_id', 'asc')

										->get();



									$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

									$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

									$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

									$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

									$food_detail['food_name_ar'] = $item_name_cc_ar = $menu_cat_detail[0]->menu_category_name_ar;

									$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

									$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

									$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;



									$food_detail['food_subitem_title'] = '';



									if ($menu_cat_detail[0]->menu_category_portion == 'yes') {

										$food_detail['food_subitem_title'] = 'Choose a size';



										$food_size = DB::table('category_item')

											->where('rest_id', '=', $rest_id)

											->where('menu_id', '=', $menu_id)

											->where('menu_category', '=', $menu_cate_id)

											->where('menu_cat_itm_id', '=', $menu_subcate_id)

											->where('menu_item_status', '=', '1')

											->select('*')

											->orderBy('menu_cat_itm_id', 'asc')

											->groupBy('menu_item_title')

											->get();





										$food_detail['food_size_detail'] = $food_size;

										$item_price_cc = $food_size[0]->menu_item_price;



										$total_price = number_format(($total_price + $food_size[0]->menu_item_price), 2);

										$foodsize_price = number_format(($foodsize_price + $food_size[0]->menu_item_price), 2);
									} else {

										$food_detail['food_size_detail'] = array();

										$total_price = number_format(($total_price + $menu_cat_detail[0]->menu_category_price), 2);
									}

									$addon_list = '';

									if (!empty($addon_id)) {



										$addons = DB::table('menu_category_addon')

											->where('addon_restid', '=', $rest_id)

											->where('addon_menuid', '=', $menu_id)

											->where('addon_status', '=', '1')

											->where('addon_menucatid', '=', $menu_cate_id)

											->whereRaw('FIND_IN_SET(addon_id,"' . $addon_id . '")')

											->select('*')

											->orderBy('addon_id', 'asc')

											->groupBy('addon_groupname')

											->get();



										if (!empty($addons)) {

											foreach ($addons as $ad_list) {

												$option_type = 0;

												if (substr($ad_list->addon_groupname, -1) == '*') {

													$option_type = 1;
												}



												$group_name[] = $ad_list->addon_groupname;

												$ff['addon_gropname'] = $ad_list->addon_groupname;

												$ff['addon_type'] = $ad_list->addon_option;

												$ff['addon_mandatory_or_not'] = $option_type;



												$addon_group = DB::table('menu_category_addon')

													->where('addon_restid', '=', $rest_id)

													->where('addon_menuid', '=', $menu_id)

													->where('addon_status', '=', '1')

													->where('addon_groupname', '=', $ad_list->addon_groupname)

													->where('addon_menucatid', '=', $menu_cate_id)

													->whereRaw('FIND_IN_SET(addon_id,"' . $addon_id . '")')

													->select('*')

													->orderBy('addon_id', 'asc')

													->get();

												$group_list[] = $addon_group;

												$addon_group_list = '';

												foreach ($addon_group as $group_list_loop) {

													$addon_group_list[] = array(
														'addon_id' => $group_list_loop->addon_id,

														'addon_menucatid' => $group_list_loop->addon_menucatid,

														'addon_groupname' => $group_list_loop->addon_groupname,

														'addon_option' => $group_list_loop->addon_option,

														'addon_name' => $group_list_loop->addon_name,

														'addon_price' => $group_list_loop->addon_price,

														'addon_status' => $group_list_loop->addon_status

													);





													$addon_list[] = array(
														'name' => $group_list_loop->addon_groupname . ': ' . $group_list_loop->addon_name,

														'price' => $group_list_loop->addon_price,

														'id' => $group_list_loop->addon_id
													);





													$total_price = number_format(($total_price + $group_list_loop->addon_price), 2);

													$food_addonPrice = $food_addonPrice . $group_list_loop->addon_price . ',';
												}

												$ff['addon_detail'] = $addon_group_list;

												$food_detail['food_addon'][] = $ff;
											}
										}
									} else {

										$food_detail['food_addon'] = array();
									}



									$food_array[] = array('menu_id' => $menu_id, 'menu_name' => $menu_name, 'food_counter' => $menu_item_count, 'food_detail' => $food_detail, 'offer_on_item' => $offer_on_menu_item);
								}



								/******** FOOD DETAIL END ****/



								$sub_total_cart = ($sub_total_cart + ($total_price * $clist->qty));





								Cart::add(
									array(

										'id' => $clist->menu_catid,

										'name' => $item_name_cc,

										'name_ar' => $item_name_cc_ar,

										'qty' => $clist->qty,

										'price' => number_format(($item_price_cc * $clist->qty), 2),

										'options' => [
											'menu_catid' => $clist->menu_catid,

											'instraction' => $clist->special_instruction,

											'rest_id' => $clist->rest_id,

											'menu_id' => $clist->menu_id,

											'option_name' => $clist->menu_subcatid,

											'addon_data' => $addon_list,

											'offer_on_item' => $offer_on_menu_item

										]

									)

								);



								$json_data_session[$clist->cart_id] =	array(
									"rowId" => $clist->cart_id, "id" => $clist->menu_catid,	   "name" => $item_name_cc, "qty" => $clist->qty, "price" => $item_price_cc, "instruction" => $clist->special_instruction,			    "options" => array(
										"menu_catid" => $clist->menu_catid,

										"rest_id" => $clist->rest_id,

										"menu_id" => $clist->menu_id,

										"option_name" => $clist->menu_subcatid,

										"addon_data" => $addon_list,

										"offer_name" => $offer_on_menu_item
									),

									"tax" => '',

									"subtotal" => $item_price_cc

								);
							}



							$serv_tax  = (($sub_total_cart * $service_charge) / 100);





							/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/

							$current_promo = '';

							$offer_applied_on_this_cart = '';





							if (($sub_total_cart >= $rest_detail[0]->rest_min_orderamt)) {

								$day_name =  strtolower(date("D", strtotime(date('Y-m-d'))));





								$current_promo = '';

								$array_amt = '';



								foreach ($promo_list as $plist) {



									if ($plist->promo_for == 'order') {

										$current_promo1 = '';



										if ($plist->promo_on == 'schedul') {

											$day_name =  strtolower(date("D", strtotime(date('Y-m-d'))));



											$db_day = '';



											if (!empty($plist->promo_day)) {

												$db_day = explode(',', $plist->promo_day);
											}



											if (($plist->promo_start != '0000-00-00') && ($plist->promo_end != '0000-00-00')) {



												if (

													($plist->promo_start <= date('Y-m-d')) &&

													($plist->promo_end >= date('Y-m-d')) &&

													(empty($plist->promo_day)) &&

													((($plist->promo_buy == 0) || ($plist->promo_buy <= $sub_total_cart)))

												) {

													$array_amt[] = $plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;
												}





												if (($plist->promo_start <= date('Y-m-d')) && ($plist->promo_end >= date('Y-m-d'))

													&& (!empty($plist->promo_day))

													&& (in_array($day_name, $db_day) &&

														((($plist->promo_buy == 0) || ($plist->promo_buy <= $sub_total_cart))))



												) {



													$array_amt[] = $plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;
												}
											} elseif (($plist->promo_start == '0000-00-00') && ($plist->promo_end == '0000-00-00') && (!empty($plist->promo_day)) && (in_array($day_name, $db_day)) &&

												((($plist->promo_buy == 0) || ($plist->promo_buy <= $sub_total_cart)))
											) {







												$db_day = explode(',', $plist->promo_day);



												if (in_array($day_name, $db_day)) {

													$array_amt[] = $plist->promo_buy;

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$current_promo[] = $current_promo1;
												}
											}
										} else {

											if (($plist->promo_buy == 0) || ($plist->promo_buy <= $sub_total_cart)) {

												$array_amt[] = $plist->promo_buy;

												$current_promo1['promo_id'] = $plist->promo_id;

												$current_promo1['promo_restid'] = $plist->promo_restid;

												$current_promo1['promo_for'] = $plist->promo_for;

												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

												$current_promo1['promo_on'] = $plist->promo_on;

												$current_promo1['promo_desc'] = $plist->promo_desc;

												$current_promo1['promo_mode'] = $plist->promo_mode;

												$current_promo1['promo_buy'] = $plist->promo_buy;

												$current_promo1['promo_value'] = $plist->promo_value;

												$current_promo1['promo_end'] = $plist->promo_end;

												$current_promo1['promo_start'] = $plist->promo_start;





												$current_promo[] = $current_promo1;
											}
										}
									}
								}
							} else {

								$current_promo = array();

								$offer_applied_on_this_cart = '';
							}



							$offer_amt = 0;

							$cal_sub_totla = $sub_total_cart;





							if ((count($current_promo) > 0) && (!empty($array_amt))) {



								$maxs = max($array_amt);

								$key_name = array_search($maxs, $array_amt);

								$k = 0;
								$i = 0;

								foreach ($current_promo as $pval) {

									if ($key_name == $k) {



										if ($pval['promo_mode'] == '$') {

											$i++;

											$offer_applied_on_this_cart = $pval['promo_desc'];

											$offer_amt = ($offer_amt + $pval['promo_value']);

											$cal_sub_totla = ($cal_sub_totla - $pval['promo_value']);
										}



										if (($pval['promo_mode'] == '%')) {
											$i++;

											$offer_applied_on_this_cart = $pval['promo_desc'];

											$promo_val_per = (($cal_sub_totla * $pval['promo_value']) / 100);

											$cal_sub_totla = ($cal_sub_totla - $promo_val_per);

											$offer_amt = ($offer_amt + $promo_val_per);
										}



										if (($pval['promo_mode'] == '0')) {

											$i++;

											$offer_applied_on_this_cart = $pval['promo_desc'];
										}
									}

									$k++;
								}
							} else {

								$offer_applied_on_this_cart = '';

								$current_promo = array();
							}



							/** PROMOTIONS CODE END  **/





							/******************  FOOD ITEM  */





							$food_offer_amt = '';

							$offer_applied_on_food = '';





							if (!empty($food_current_promo) && (count($food_current_promo) > 0)) {



								$k = 0;
								$i = 0;

								foreach ($food_current_promo as $pval) {



									$item_price = $food_cart_amount[$k];

									$item_qty = $food_cart_qty[$k];

									$cal_sub_total = number_format(($item_price * $item_qty), 2);



									if ($pval['promo_mode'] == '$') {

										$i++;

										$offer_applied_on_food = $pval['promo_desc'];

										$food_offer_amt = ($offer_amt + $pval['promo_value']);
									}



									if (($pval['promo_mode'] == '%')) {
										$i++;

										$offer_applied_on_food = $pval['promo_desc'];

										$promo_val_per = (($cal_sub_total * $pval['promo_value']) / 100);

										$food_offer_amt = ($offer_amt + $promo_val_per);
									}



									if (($pval['promo_mode'] == '0')) {

										$i++;

										$offer_applied_on_food = $pval['promo_desc'];
									}

									$k++;
								}
							} else {

								$food_current_promo = array();
							}



							/*******************  END  ****/





							if (($sub_total_cart - $offer_amt - $food_offer_amt) < $delivery_fee) {

								$total_amt = number_format(($delivery_fee + $serv_tax), 2);
							} else {



								$aa_total = ($sub_total_cart - $offer_amt - $food_offer_amt) + $serv_tax;

								$total_amt = number_format(($aa_total), 2);
							}





							DB::table('rest_cart')

								->where('user_id', '=', trim($user_id))

								->where('guest_id', '=', trim($guest_id))

								->where('rest_id', '=', trim($rest_id))

								->update([
									'cart_detail' => (json_encode($json_data_session)),

									'offer_detail' => (json_encode($current_promo)),

									'offer_amt' => trim($offer_amt),



									'offer_applied' => $offer_applied_on_this_cart,

									'food_promo_list' => json_encode($food_current_promo),

									'food_offer_applied' => json_encode($offer_applied_on_food),



									'service_tax_amt' => trim($serv_tax),

									'delivery_amt' => trim($delivery_fee),

									'subtotal' => trim($sub_total_cart),

									'total_amt' => trim($total_amt)

								]);







							$response['message'] = "All deatil of cart data";

							$response['cart_detail'] = $cart_detail;

							$response['other_detail'] = array(

								'subtotal' => number_format($sub_total_cart, 2),

								'rest_service' => $service_charge,

								'delivery_fee' => number_format($delivery_fee, 2),

								'service_tax' => number_format($serv_tax, 2),

								'promo_list' => $current_promo,

								'discount_amt' => number_format($offer_amt, 2),

								'offer_applied_on_this_cart' => $offer_applied_on_this_cart,

								'food_promo_list' => $food_current_promo,

								'offer_applied_on_item' => $offer_applied_on_food,

								'total_amt' => number_format($total_amt, 2)
							);

							$response['ok'] = 1;







							/*********************************  SET ALL CART SESSION AFTER LOGIN ***************************************/





							$cart_user['delivery_fee'] = trim(number_format($delivery_fee, 2));

							$cart_user['sub_total'] = trim(number_format($sub_total_cart, 2));

							$cart_user['service_tax'] = trim(number_format($serv_tax, 2));

							$cart_user['delivery_fee_min'] = trim(number_format($delivery_fee, 2));

							$cart_user['delivery_fee_min_remaing'] = trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

							$cart_user['total_charge'] = trim(number_format($total_amt, 2));



							/***** DISCOUNT (PROMOTION) START */



							$cart_user['promo_discount'] = trim(number_format($offer_amt, 2));

							$cart_user['promo_discount_id'] = trim(Session::get('cart_userinfo')['promo_discount_id']);

							$cart_user['promo_discount_text'] = trim($offer_applied_on_this_cart);

							$cart_user['order_promo_detail'] = ($current_promo);

							$cart_user['order_food_promo'] = ($food_current_promo);

							$cart_user['order_food_promo_applied'] = trim($offer_applied_on_food);

							$cart_user['order_pmt_type'] = '1';





							/********** END *******/

							$cart = Cart::content();



							$cart_user['cart_data'] = $cart;





							/**  PARTICLA PAYMENT PROCESS ***/



							if (Session::get('cart_userinfo')['partical_amt'] > 0) {

								$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

								$cart_user['partical_percent'] = trim(Session::get('cart_userinfo')['partical_percent']);

								$partical_amt = number_format((($total_amt * Session::get('cart_userinfo')['partical_percent']) / 100), 2);

								$partical_remaning_amt = number_format(($total_amt - $partical_amt), 2);

								$cart_user['partical_amt'] = trim($partical_amt);

								$cart_user['partical_remaning_amt'] = trim($partical_remaning_amt);
							} else {



								$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

								$cart_user['partical_percent'] = trim(Session::get('cart_userinfo')['partical_percent']);

								$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

								$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);
							}

							/**  PARTICAL PAYMENT PROCESS END ***/





							/**/





							$cart_user['delivery_add1'] = trim(Session::get('cart_userinfo')['delivery_add1']);

							$cart_user['delivery_add2'] = trim(Session::get('cart_userinfo')['delivery_add2']);

							$cart_user['delivery_surbur'] = trim(Session::get('cart_userinfo')['delivery_surbur']);

							$cart_user['delivery_postcode'] = trim(Session::get('cart_userinfo')['delivery_postcode']);

							$cart_user['delivery_number'] = trim(Session::get('cart_userinfo')['delivery_number']);





							$cart_user['pickup_date'] = trim(Session::get('cart_userinfo')['pickup_date']);;

							$cart_user['pickup_time'] = trim(Session::get('cart_userinfo')['pickup_time']);;

							$cart_user['pickup_time_formate'] = trim(Session::get('cart_userinfo')['pickup_time_formate']);;

							$cart_user['rest_meta_url'] = trim(Session::get('cart_userinfo')['rest_meta_url']);;

							$cart_user['rest_id'] = trim(Session::get('cart_userinfo')['rest_id']);;

							$cart_user['deliveri_time'] = '';

							$cart_user['pickup_date'] = date('Y-m-d');

							$cart_user['rest_open_status'] = trim(Session::get('cart_userinfo')['rest_open_status']);





							$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

							$cart_user['firstname_order'] = trim($user_detail[0]->name);

							$cart_user['lastname_order'] = trim($user_detail[0]->lname);

							$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

							$cart_user['email_order'] = trim($user_detail[0]->email);

							$cart_user['address_order'] = trim($user_detail[0]->user_address);

							$cart_user['city_order'] = trim($user_detail[0]->user_city);

							$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

							$cart_user['user_type'] = trim(1);

							$cart_user['user_id'] = trim($user_detail[0]->id);

							$cart_user['service_area'] = '';

							$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

							/***********************************  END *****************************************/
						}





						Session::set('cart_userinfo', $cart_user);



						$path = explode('www.grambunny.com', $_SERVER['HTTP_REFERER']);



						$new_path = explode('?', $path[1]);



						if ($path[1] == '/checkout') {

							$return_url = 'checkout';
						} elseif ($path[1] == '/payment') {

							$return_url = 'checkout';
						} else {

							$return_url = $path[1];
						}
					}
				} else {



					Session::set('cart_userinfo', $cart_user);



					$path = explode('www.grambunny.com', $_SERVER['HTTP_REFERER']);



					$new_path = explode('?', $path[1]);



					if (($new_path[0] == '/order_cancel') || ($new_path[0] == '/order_success')) {

						$return_url = 'myaccount';
					} else {

						$return_url = $path[1];
					}
				}
			}



			return response()->json(array('sucess_message' => $sucess_message, 'status' => '2', 'user_id' => $user_id, 'return_url' => $return_url));
		} else {

			$error_message = "Invalid OTP. Please enter a valid otp!";

			return response()->json(array('error_message' => $error_message, 'status' => '1', 'user_id' => $user_id));
		}
	}
}
