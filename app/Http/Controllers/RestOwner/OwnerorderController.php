<?php

namespace App\Http\Controllers\RestOwner;

use App\Http\Controllers\Controller;

use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use Illuminate\Http\Request;

use App\Order_history;

use App\Order_pay_history;

use App\Order;

use App\Notification_list;

use Carbon\Carbon;


class OwnerorderController extends Controller
{

    public function __construct(){

    $this->middleware('vendor');

    }


	public function show_orderlist(Request $request)
	{

	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    if($request->id==''){

        $orders  = DB::table('orders')
		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->where('product_service.type', '!=' ,3)
		->where('orders.vendor_id', '=' ,$vendor_id)
		->select('orders.*')
		->distinct('orders.id')
		->orderBy('orders.id', 'desc')
		->whereNotIn('orders.status',  array(5,6,7,) )
		->get();
		
       }else{

     $orders  = DB::table('orders')
		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->where('product_service.type', '!=' ,3)
		->where('orders.vendor_id', '=' ,$vendor_id)
		->select('orders.*')
		->distinct('orders.id')
		->orderBy('orders.id', 'desc')
		->whereNotIn('orders.status',  array(5,6,7,) )
		->get();
	}

       //  if($request->id==''){

       //  $orders = Order::where('vendor_id', $vendor_id)

       //  ->orderBy('id', 'desc')

       //  ->whereNotIn('status',  array(5,6,7,) )

       //  ->get();

       // }else{



       //  $orders = Order::where('vendor_id', $vendor_id)

       //  ->where('status', '=' ,$request->id)

       //  ->orderBy('id', 'desc')

       //  ->get();

       // }



  		$data_onview = array('order_detail'=>$orders,'vendor_id' =>$vendor_id,'searchid'=>$request->id); 

		return View('rest_owner.order_list')->with($data_onview);



	}

	public function show_event_list(Request $request)
	{

	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;

        if($request->id==''){

        $orders  = DB::table('orders')
		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->where('product_service.type', '=' ,3)
		->where('orders.vendor_id', '=' ,$vendor_id)
		->select('orders.*')
		->distinct('orders.id')
		->orderBy('orders.id', 'desc')
		->whereNotIn('orders.status',  array(5,6,7,) )
		->get();

       }else{

         $orders =  DB::table('orders')
		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->where('product_service.type', '=' ,3)
		->where('orders.vendor_id', '=' ,$vendor_id)
		->select('orders.*')
		->distinct('orders.id')
        ->where('orders.status', '=' ,$request->id)
        ->orderBy('orders.id', 'desc')
        ->get();

       }

  		$data_onview = array('order_detail'=>$orders,'vendor_id' =>$vendor_id,'searchid'=>$request->id); 

		return View('rest_owner.event_order_list')->with($data_onview);

	}


	public function show_event_detail(Request $request)
	{     	
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$orderid = $request->id;

		$order_countss = DB::table('orders')
		->where('id', '=' ,$orderid)
		->where('vendor_id', '=' ,$vendor_id)
		->get();

		if(count($order_countss)>0){

		$order_detail = DB::table('orders')
		->where('id', '=' ,$orderid)
		->where('vendor_id', '=' ,$vendor_id)
		->first();

		$pslist = DB::table('orders_ps')
		->leftJoin('product_service', 'product_service.id' , '='  ,'orders_ps.ps_id')
		->select('orders_ps.*','product_service.type','product_service.ticket_service_fee','product_service.ticket_fee')

		->where('order_id', '=' ,$orderid)
		->get();	

  	$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist,'vendor_id' =>$vendor_id); 
		return View('rest_owner.event_view')->with($data_onview);

	}else{

		return redirect()->to('/merchant/event-list');
	}

	}

	public function show_returnlist(Request $request)

	{



	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;



	    if($request->id==''){



	        $return_orders = Order::where('vendor_id', $vendor_id)

	        ->whereIn('status', array(5, 6, 7))

	        ->orderBy('id', 'desc')

	        ->paginate(10);   



       }else{



	        $return_orders = Order::where('vendor_id', $vendor_id)

	        ->where('status', '=' ,$request->id)

	        ->orderBy('id', 'desc')

	        ->paginate(10); 



       }



  		$data_onview = array('order_detail'=>$return_orders,'searchid'=>$request->id ); 

		return View('rest_owner.return_order_list')->with($data_onview);



	}





	public function show_scheduled_order()

	{



	DB::enableQueryLog();





		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();



		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';



		if($restaurant_detail)

		{

			$rest_id_array=[];

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}



		$user_id = 0;





		$order_detail='';



		if((count($rest_id_array)>0) && (!empty($rest_id_array))){



			$order_detail  =  DB::table('order')	;

			$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

			$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');





			 $order_detail = $order_detail->where(function($query) use ($rest_id_array) {



				$l=0;

				foreach($rest_id_array as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							 $query = $query->orWhere('order.rest_id','=',$ftype);

						 }

						 else

						 {

						 	 $query = $query->Where('order.rest_id','=',$ftype);

						 }

					}

					$l++;

				}



			});



            $order_detail  =  $order_detail->where('order.order_typetime', '=', 'later');

			$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);



			$order_detail  = $order_detail->select('*');

			$order_detail  = $order_detail->orderBy('order.order_id', 'desc');

			$order_detail  = $order_detail->get();





		}





  		$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);



		return View('rest_owner.scheduled_order_list')->with($data_onview);



	}


	public function show_orderdetail(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$orderid = $request->id;

		$order_count = DB::table('orders')
		->where('vendor_id', '=' ,$vendor_id)
		->where('id', '=' ,$orderid)
		->get();

		if(count($order_count)>0){

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->where('vendor_id', '=' ,$vendor_id)

		->first();

		$vendor_order = DB::table('orders')

		->where('id', '=' ,$orderid)

		->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

		->first();

		//print_r($vendor_order);die;

		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();

		//DB::table('notification_list')->where('order_id', $orderid)->where('vendor_id', $vendor_id)->update(['noti_read' => 1]);		

  		$data_onview = array('order_detail' =>$order_detail,'vendor_order' =>$vendor_order,'ps_list' =>$pslist,'vendor_id' =>$vendor_id); 
			 		
		return View('rest_owner.order_view')->with($data_onview);
	}

	else{

		return redirect()->to('/merchant/order-list');

	}
}


	public function update_order_action()
	{

		$order_status = Input::get('order_status');

			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$getvalue = DB::table('orders')->where('order_id',$order_id)->first();

			$user_id = $getvalue->user_id;

		if($order_status==1){

			$carddata = DB::table('users_card')->where('order_id',$order_id)->where('user_id',$getvalue->user_id)->where('transaction_type','05')->first();

        if(!empty($carddata)){

            $data = array(

              'gateway_id'=> 'G15552-42',
              'password'=> "V2wX5h1ov8iar0FR7ScRaePz2xZemfnF",
              'transaction_type'=> '00',
              'amount'=> $carddata->amount,
              'cardholder_name'=> $carddata->cardholder_name,
              'cc_number'=> $carddata->cc_number,
              'cc_expiry'=> $carddata->cc_expiry,
              'cvd_code'=> $carddata->cvd_code,
              'cvd_presence_ind'=> '1',
              'reference_no'=> $carddata->reference_no,
              'customer_ref'=> $order_id,
              'currency_code'=> 'USD',
              'credit_card_type'=> $carddata->credit_card_type,

    );
   
	$payload = json_encode($data, JSON_FORCE_OBJECT);

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.globalgatewaye4.firstdata.com/transaction",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>$payload,
	  CURLOPT_HTTPHEADER => array(
	    "Content-Type: application/json"
	  ),
	));


	$json_response = curl_exec($curl);	
	
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	$response = json_decode($json_response, true);

	curl_close($curl);

	$cc_number = substr($carddata->cc_number,-4,4);

	if($response['transaction_approved']=='1'){

	DB::table('users_card')->where('order_id',$order_id)->where('user_id',$getvalue->user_id)->update([

        'cc_number' => $cc_number,
        'cvd_code' => '000',
		'transaction_type' => $response['transaction_type'],
		'transaction_tag' =>  strval($response['transaction_tag']),
		'authorization_num' => $response['authorization_num'],
		'transaction_approved' => $response['transaction_approved'],
		'bank_message' => $response['bank_message'],
		'updated_at' => date('Y-m-d H:i:s')

		]);

	 Session::flash('message', 'Payment has been successfully processed');

     }else{

    Session::flash('errormsg', $json_response); 	

    return redirect()->to('/merchant/order-details/'.$id); // return redirect()->to('/merchant/order-list/'); 

     }

      }


	}

		$date = date('Y-m-d H:i:s');

		if( Input::get('return_flag') == 1){


			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$order_status = Input::get('order_status');

			$merchant_reason = Input::get('merchant_reason');

			DB::table('orders')->where('order_id',$order_id)->update(['status' =>$order_status,'status' =>$order_status,'return_reason_merchant'=> $merchant_reason,'updated_at'=> $date ]);

			return redirect()->to('/merchant/return-list');

		}else{

			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$order_status = Input::get('order_status');


			$cancel_reason = '';

			if(Input::get('cancel_reason'))

			{

				$cancel_reason = Input::get('cancel_reason');

			}


			DB::table('orders')->where('order_id',$order_id)->update(['status' =>$order_status, 'cancel_reason' => $cancel_reason,'updated_at'=> $date ]);

			if($order_status==2){

			 $getvalue1 = DB::table('orders_ps')->leftJoin('orders', 'orders_ps.order_id', '=', 'orders.id')->where('orders.order_id',$order_id)->select('*','orders_ps.ps_id as ps_id','orders_ps.ps_qty as ps_qty')->get();
			 	
			 	 foreach ($getvalue1 as $key => $value) {
			 	 	
			 	 	$getproductvalue = DB::table('product_service')->where('id',$value->ps_id)->first();
			 	 	

			 	 	if(!empty($getproductvalue)){
			  	

			 	 	$quantity = $getproductvalue->quantity+$value->ps_qty;

			 	 	DB::table('product_service')->where('id',$value->ps_id)->update(['quantity' =>$quantity]);

			 	 //	$soldqty = $getproductvalue->popular-$value->ps_qty;

			 	 	
                    DB::table('orders_ps')->where('order_id',$value->id)->update(['cancel_status' =>1]);
			 	 	//DB::table('orders_ps')->where('order_id',$value->id)->update(['ps_qty' =>0]);

			 	  }
				
				}
				
				}

			$order_statuss = '';


			 if($order_status==0){ $order_statuss="Pending"; }
			 if($order_status==1){ $order_statuss="Accept"; }
			 if($order_status==2){ $order_statuss="Cancelled"; }
			 if($order_status==3){ $order_statuss="On the way"; }
			 if($order_status==4){ $order_statuss="Delivered"; }

			 $getvalue = DB::table('orders')->where('order_id',$order_id)->first();

			 $user_divice_token = DB::table('users')->where('id', '=', $getvalue->user_id)->first(); 

            if($user_divice_token->devicetoken)
            {

                 $title = "Hey ".$user_divice_token->name." ".$user_divice_token->lname." your order is ".$order_statuss;

                 $tag = "Order detail";

                 $token = $user_divice_token->devicetoken;

                 $orid = $id;

                 $vid = $getvalue->vendor_id;

                 $notistatus =  $user_divice_token->notification;

                 if($notistatus==1){

                 $response = $this->sendNotification($token,$tag,$title);

                 }
                 
                $this->save_notification($user_id,$title,$orid,$tag,$vid);
					
			
            }  


			return redirect()->to('/merchant/order-details/'.$id);

		}					

   }


	public function show_scheduled_order_detail()

	{


		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();



		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';



		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}





	   if((count($rest_id_array)>0) && (!empty($rest_id_array))){



		$order_id = $_REQUEST['order'];

		$order_detail  =  DB::table('order')

						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

						->leftJoin('users', 'order.user_id', '=', 'users.id')

						->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid')



						->where('order.order_id', '=', $order_id)

						->where(function($query) use ($rest_id_array) {



								$l=0;

								foreach($rest_id_array as $ftype )

								{

									if($ftype>0)

									{

										if($l>0){

											 $query = $query->orWhere('order.rest_id','=',$ftype);

										 }

										 else

										 {

											 $query = $query->Where('order.rest_id','=',$ftype);

										 }

									}

									$l++;

								}



							})





						->select('order.*','restaurant.rest_name', 'restaurant.rest_address', 'restaurant.rest_address2', 'restaurant.rest_suburb', 'restaurant.rest_state', 'restaurant.rest_zip_code', 'restaurant.rest_contact','order_payment.pay_tx')

						->orderBy('order.order_create', 'desc')

						->get();





			if($order_detail)

			{



					$order_history =  DB::table('order_history')

							->where('order_history.order_id', '=', $order_id)

							->where('order_history.old_status', '=', '1')

							->select('*')

							->get();

					if(!empty($order_history))

					{

						$order_create_date	= $order_history[0]->old_status_date;

					}

					else

					{

						$order_create_date	= $order_detail[0]->created_at;

					}





				$data_onview = array('order_detail'=>$order_detail,'order_create_date'=>$order_create_date);

				return View('rest_owner.order_view')->with($data_onview);

			}

			else

			{

				 return redirect()->to('/vendor/orderlisting');

			}



	   }

	   else

	   {

		 return redirect()->to('/vendor/orderlisting');

	   }







	}	





   	public function update_order_address()

	{



		$order_id = Input::get('add_order_id');

		$order_address = Input::get('order_address');



		DB::enableQueryLog();



		DB::table('order')->where('order_id',$order_id)

							  ->update(['order_deliveryadd1' =>$order_address]);



		echo 'Address Updated!';				



   }



   	public function update_order_action_notification()
	{

		$order_id = Input::get('order_id');

		$old_status = Input::get('old_status');

		$new_order_status = Input::get('order_status');



		DB::enableQueryLog();



		$order_detail  =  DB::table('order')

						->where('order_id', '=', $order_id)

						->select('*')

						->orderBy('order.order_create', 'desc')

						->get();



	   $order_guestid = $order_detail[0]->order_guestid;

	   $order_user_id = $order_detail[0]->user_id;



	   if(!empty($order_user_id)){ $user_id = $order_user_id; }else if(!empty($order_guestid)){  $user_id = $order_guestid; }else{ $user_id = ''; }	



  if($new_order_status == '4')

	{

		$odr_cnfm_notif_cotnt = DB::table('email_content')->where('email_id',16)->first();

		

		$notification_title = $odr_cnfm_notif_cotnt->email_title;

		$notification_description = $odr_cnfm_notif_cotnt->email_content;



	}else if($new_order_status == '6'){



		$odr_cnld_notif_cotnt = DB::table('email_content')->where('email_id',17)->first();

		

		$notification_title = $odr_cnld_notif_cotnt->email_title;

		$notification_description = $odr_cnld_notif_cotnt->email_content;

		

	}else{ 



    	$notification_title = "";

		$notification_description = "";



	}	

				

	$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->get();



	if(empty($token_data)){ $token_data = DB::table('device_token')->where('guest_id', '=', trim($user_id))->get(); }	

	

	//$target = "fZqQg0Zap5M:APA91bHKf0536rKtBvDcWsrk4A2UgnTnUrkvN7ZcdFcZURgaWrWOS-DzkP0biEoFVlwiRiLeJ2t6EWbwUjgz0xYw0hKPsvQmhYulJYBf6SUtQW80_5GUetzqqa1JiUKtEXzXF7LZl0KD";						

	$token = $token_data[0]->devicetoken;

	$deviceid = $token_data[0]->deviceid;

	$devicetype = $token_data[0]->devicetype;

    $iosadres = '';

	 

	//$iosadres = $this->sendNotification($token,$notification_title,$notification_description);



	return 1;//$iosadres;

				

		/* $notifiy = new Notification_list;

		$notifiy->noti_userid = $user_id;

		$notifiy->noti_guestid = '';	

		$notifiy->noti_title = $notification_title;	

		$notifiy->noti_desc = $notification_description;	

		$notifiy->noti_deviceid = $deviceid;	

		$notifiy->noti_devicetype = $devicetype;	

		$notifiy->noti_read = '0';

		$notifiy->save();	



     return true;	*/				



   }



	public function ajax_search_list()

	{

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();



		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';



		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}



		$order_detail  =  '';



		if((count($rest_id_array)>0) && (!empty($rest_id_array))){



			$order_detail  =  DB::table('order');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



			 $order_detail = $order_detail->where(function($query) use ($rest_id_array) {



				$l=0;

				foreach($rest_id_array as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							 $query = $query->orWhere('order.rest_id','=',$ftype);

						 }

						 else

						 {

						 	 $query = $query->Where('order.rest_id','=',$ftype);

						 }

					}

					$l++;

				}



			});





			/*USER BASE SEARC FORM USER PAGE  */



			//$order_detail  =  $order_detail->where('order.order_picktime', '=', '0000');



			$order_detail  =  $order_detail->where('order.order_typetime', '=', 'asp');



			if($user_id>0)

			{

				$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

			}







			/* Status Search */



			if(!empty($status) && ($status>0))

			{

			  $order_detail = $order_detail->where('order.order_status', '=', $status);

			}



			/* Get Data between Date */





			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.order_create', '>=', date('Y-m-d 23:59:00',strtotime($from_date)));

			}





			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.order_create', '<=',date('Y-m-d 23:59:00',strtotime($to_date)));

			}



			/* End */







			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();



		}





  		$data_onview = array('order_detail'=>$order_detail);



		return View('rest_owner.ajax.order_list')->with($data_onview);





	}



	public function ajax_scheduled_search_list()

	{

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();



		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';



		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}



		$order_detail  =  '';



		if((count($rest_id_array)>0) && (!empty($rest_id_array))){



			$order_detail  =  DB::table('order');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



			 $order_detail = $order_detail->where(function($query) use ($rest_id_array) {



				$l=0;

				foreach($rest_id_array as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							 $query = $query->orWhere('order.rest_id','=',$ftype);

						 }

						 else

						 {

						 	 $query = $query->Where('order.rest_id','=',$ftype);

						 }

					}

					$l++;

				}



			});





			/*USER BASE SEARC FORM USER PAGE  */





			//$order_detail  =  $order_detail->where('order.order_picktime', '!=', '0000');

			$order_detail  =  $order_detail->where('order.order_typetime', '=', 'later');



			if($user_id>0)

			{

				$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

			}





			/* Status Search */



			if(!empty($status) && ($status>0))

			{

			  $order_detail = $order_detail->where('order.order_status', '=', $status);

			}



			/* Get Data between Date */







			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

			}





			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

			}



			/* End */



			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();



		}





  		$data_onview = array('order_detail'=>$order_detail);



		return View('rest_owner.ajax.order_list')->with($data_onview);





	}



	public function show_orderreport()
	{ 

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();


		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';

		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}


		$order_detail  =  '';


		if((count($rest_id_array)>0) && (!empty($rest_id_array))){

			$order_detail  =  DB::table('order');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

		    $order_detail = $order_detail->where(function($query) use ($rest_id_array) {

				$l=0;

				foreach($rest_id_array as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							 $query = $query->orWhere('order.rest_id','=',$ftype);

						 }

						 else

						 {

						 	 $query = $query->Where('order.rest_id','=',$ftype);

						 }

					}

					$l++;

				}



			});


			/*USER BASE SEARC FORM USER PAGE  */


			if($user_id>0)

			{

				$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

			}


			/* Status Search */



			if(!empty($status) && ($status>0))

			{

			  $order_detail = $order_detail->where('order.order_status', '=', $status);

			}



			/* Get Data between Date */


			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

			}



			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

			}


			/* End */


			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();

		}


		$title = 'Order Report';


		$all_array = array();

		if(!empty($order_detail))

		{

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)

			{	$sr++;


				 $status = '';

				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				 if($data_list->order_status=='2'){ $status = 'Cancelled'; }

				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; }

				 if($data_list->order_status=='6'){ $status = 'Reject'; }

				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }

				  $cart_price = 0;

				  $sumary = '';

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

		{

			$order_carditem = json_decode($data_list->order_carditem, true);

				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";


				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

				 {

					foreach($item['options']['addon_data'] as $addon)

					{

						$cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 }



			}

		}





				$all_array[] = array('0'=>$sr,

									'1'=>$data_list->order_id,

									'2'=>$data_list->order_create,

									'3'=>$data_list->rest_name,

									'4'=>$status,

									'5'=>$data_list->order_fname.' '.$data_list->order_lname,

									'6'=>$data_list->order_tel,

									'7'=>$sumary,

									'8'=>'$'.$cart_price,

									);





			}

			$data = $all_array ;

		}

		else

		{

			$data = array(

		   			array('No REcord Found')

			);

		}


		$filename = "order_report.csv";

		$fp = fopen('php://output', 'w');

		$header =array('',	$title, '', '');

		header('Content-type: application/csv');

		header('Content-Disposition: attachment; filename='.$filename);

		fputcsv($fp, $header);


		$header2 =array('SNo.', 'OrderNo', 'OrderReq_date', 'Restaurant','Status','UserName','Contact No', 'Order Sumary','TotalAmount');

		fputcsv($fp, $header2);


		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }





	}



	public function payment_list(Request $request)

	{



		$payment_detail = '';



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



     if($request->id==''){



		$order_detail  = DB::table('orders')

		->where('vendor_id', '=' ,$vendor_id)

		->orderBy('id', 'desc')

		->get();



	   }else{



       	$order_detail  = DB::table('orders')

       	->where('vendor_id', '=' ,$vendor_id)

		->where('status', '=' ,$request->id)

		->orderBy('id', 'desc')

		->get();



	   }



  		$data_onview = array('order_detail' =>$order_detail,'searchid' =>$request->id);  

		return View('rest_owner.payment_list')->with($data_onview);



	}



	public function payment_details(Request $request)
	{
		$orderid = $request->id;

		$order_count = DB::table('orders')

		->where('id', '=' ,$orderid)

		->get();

		if(count($order_count)>0){

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->first();


		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();			


  		$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist);  					 		

		return View('rest_owner.payment_view')->with($data_onview);

	}else{

		return redirect()->to('/merchant/payment-list');
	}

	}


	public function ajax_show_paymentlist()

	{

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');





		$order_detail = '';



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();



		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';



		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}



		if((count($rest_id_array)>0) && (!empty($rest_id_array))){







			$order_detail  =  DB::table('order_payment')	;

			$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

			$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);







			$order_detail = $order_detail->where(function($query) use ($rest_id_array) {



								$l=0;

								foreach($rest_id_array as $ftype )

								{

									if($ftype>0)

									{

										if($l>0){

											 $query = $query->orWhere('order.rest_id','=',$ftype);

										 }

										 else

										 {

											 $query = $query->Where('order.rest_id','=',$ftype);

										 }

									}

									$l++;

								}



							});









			/* Status Search */



			if(!empty($status) && ($status>0))

			{



				if($status==7)

				{

					$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');

				}

				elseif($status==6)

				{

					$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');

					$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '5');

				}

				else

				{

					$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);

				}

			}



			/* Get Data between Date */







			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

			}





			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

			}



			/* End */







			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');

			$order_detail = $order_detail->get();



		}





  		$data_onview = array('order_detail'=>$order_detail);



		return View('rest_owner.ajax.payment_list')->with($data_onview);



	}



	public function show_paytmentreport()

	{



		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');





		$order_detail  =  DB::table('order_payment')	;

		$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');

		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



		$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);





		/* Status Search */



		if(!empty($status) && ($status>0))

		{



			if($status==4)

			{

		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');

		  		$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '3');

			}

			elseif($status==5)

			{

		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');

			}

			else

			{

		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);

			}

		}



		/* Get Data between Date */







		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

		}





		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

		}



		/* End */







		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');

		$order_detail = $order_detail->get();





		$title = 'Payment Report';



		$all_array = array();

		if(!empty($order_detail))

		{

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)

			{

				$sr++;



				$payment_status = '';



				$order_can_comp_reg_date	= '';



				if($data_list->pay_status=='1') { $payment_status = 'Complete'; }

				if($data_list->pay_status=='2') { $payment_status = 'ToRefund'; }

				if($data_list->pay_status=='3') { $payment_status = 'ToPayout'; }

				if($data_list->pay_status=='4') { $payment_status = 'Completed Refund'; }

				if($data_list->pay_status=='5') { $payment_status = 'Completed Payout'; }





				$status = '';

				if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				if($data_list->order_status=='2'){ $status = 'Cancelled'; $order_can_comp_reg_date	= $data_list->created_at; }

				if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				if($data_list->order_status=='5'){ $status = 'Partner Completed'; $order_can_comp_reg_date	= $data_list->created_at;}

				if($data_list->order_status=='6'){ $status = 'Reject'; $order_can_comp_reg_date	= $data_list->created_at; }

				if($data_list->order_status=='7'){ $status = 'Review Completed'; }





				  $cart_price = 0;

				  $sumary = '';





				  /* ORDER SUMMARY AND CALCULATE TOTAL AMOUNT WITH COMMISSION AMOUNT */

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

		{

			$order_carditem = json_decode($data_list->order_carditem, true);



				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";



				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

				 {

					foreach($item['options']['addon_data'] as $addon)

					{

						$cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 }



			}

		}









		$sumary .="\n  Sub Total : $".number_format($cart_price,2);

		$sumary .="\n";

		$sumary .="\n  Delivery Fee : $".number_format($data_list->order_deliveryfee,2);



		if($data_list->order_remaning_delivery>0)

		{

			$sumary .="\n";

			$sumary .="\n $".number_format($data_list->order_min_delivery,2).' min Delivery amount';

			$sumary .=" Remaining Amount :$".number_format($data_list->order_remaning_delivery,2).'';

			$sumary .="\n";

		}

		if($order_detail[0]->order_promo_cal>0)

		{

			$sumary .=" Dels : -$".number_format($data_list->order_promo_cal,2).'';

		}









		$promo_amt_cal= '0.00';



		$total_amt= '';



		if($data_list->order_promo_cal>0)

		{

			$promo_amt_cal= $data_list->order_promo_cal;

		}





		$total_amt= $cart_price+$data_list->order_deliveryfee-$promo_amt_cal;



		if(($data_list->order_remaning_delivery>0) &&

		($data_list->order_min_delivery>0) &&

		(($cart_price-$data_list->order_promo_cal)<$data_list->order_min_delivery)

		)

		{

			$total_amt= ($data_list->order_min_delivery+$data_list->order_deliveryfee);

		}



		$Commission_amt = '0.00';

		if($data_list->pay_status=='3' || $data_list->pay_status=='5' )

		{

			$Commission_amt =	number_format((($total_amt*$data_list->rest_commission)/100),2);

		}



					/*  END   */





	    /*  ORDER PICKUP DATE AND TIME */



		$order_picup_date = '';



		if(($data_list->order_typetime=='later') || ($data_list->order_pickdate!='0000-00-00'))

		{

			$order_picup_date = $data_list->order_pickdate.' '.substr($data_list->order_picktime, 0, 2).':'.substr($data_list->order_picktime, 2, 3);

		}

		else

		{



			$order_history =  DB::table('order_history')

								->where('order_history.order_id', '=', $data_list->order_id)

								->where('order_history.old_status', '=', '1')

								->select('*')

								->get();



			if(!empty($order_history))

			{

				$order_picup_date	= $order_history[0]->old_status_date;

			}



		}







		/*    END   */







		/* ORDER COMPLETE /REJECT /CANCEL DATE AND TIME */

		$order_history_comp =  DB::table('order_history')

								->where('order_history.order_id', '=', $data_list->order_id)

								->where('order_history.old_status', '=', '5')

								->select('*')

								->get();



			if(!empty($order_history_comp))

			{

				$order_can_comp_reg_date	= $order_history_comp[0]->old_status_date;

			}







		/*        END        */



				$all_array[] = array('0'=>$sr,

									'1'=>$data_list->order_id,

									'2'=>$data_list->order_create,

									'3'=>$data_list->rest_name,

									'4'=>$status,

									'5'=>$data_list->order_fname.' '.$data_list->order_lname,

									'6'=>$data_list->order_tel,

									'7'=>$sumary,

									'8'=>'$'.$total_amt,

									'9'=>$payment_status,

									'10'=>$order_picup_date,

									'11'=>$order_can_comp_reg_date,

									'12'=>'$'.$total_amt,

									'13'=>$data_list->rest_commission,

									'14'=>$Commission_amt,

									'15'=>$data_list->pay_tx

									);





			}

			$data = $all_array ;

		}

		else

		{

			$data = array(

		   			array('No REcod Found')

			);

		}









		$filename = "payment_report.csv";

		$fp = fopen('php://output', 'w');





		$header =array('',	$title, '', '');



		header('Content-type: application/csv');

		header('Content-Disposition: attachment; filename='.$filename);

		fputcsv($fp, $header);



		$header2 =array('SNo.',

		'OrderNo',

		'OrderReq_date',

		'Restaurant',

		'Status',

		'UserName',

		'Contact No',

		'Order Sumary',

		'TotalAmount',

		'Payment Status',

		'Order DineIn/Pickup/Delivery Date',

		'OrderComplete /Cancle /Rejcte Date',

		'Order Payout/Refund Amount',

		'Commission',

		'Commission Amount',

		'Paypal Ref'





		);

		fputcsv($fp, $header2);





		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }



	}





	public function update_payment_action()

	{





		$pay_id = Input::get('pay_id');

		$order_id = Input::get('order_id');

		$old_status = Input::get('old_status');

		$new_pay_status = Input::get('pay_status');

		$outgoing_reference = Input::get('outgoing_transaction_reference');





		$payment_detail  =  DB::table('order_payment')

							->where('pay_orderid', '=', $order_id)

							->where('pay_id', '=', $pay_id)

							->select('*')

							->get();



		$pay_his = new Order_pay_history;

		$pay_his->pay_id = $payment_detail[0]->pay_id;

		$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;

		$pay_his->pay_user_id = $payment_detail[0]->pay_userid;

		$pay_his->pay_old_status = $payment_detail[0]->pay_status;

		$pay_his->old_status_date = $payment_detail[0]->created_at;

		$pay_his->order_history_created	= date('Y-m-d');

		$pay_his->save();







		/* UPDATE STATUS IN TABLE START */

			DB::table('order_payment')->where('pay_id',$pay_id)

							  ->update(['pay_status' =>$new_pay_status,

							  			'pay_outgoing_Ref' =>$outgoing_reference ]

							  );

			echo 'Status Updted!';





	}



function sendNotification($token,$title,$message)
{

$serverKey='AAAAScDfAKY:APA91bGRbTNXI3EEoabhjKa8slHwL9nPSJXiDCtKaTAr7kXWZjA6HojfZUAQwXIrDwr6lDfocP6ZstegPYlwK42XAUET5ULYGZtnvGjrhhTmAaTf8StngdcHjdDJnHpVZEmwSCIhJwUj';

$data = array("to" => $token, "notification" => array( "title" => $title, "body" => $message));

$data_string = json_encode($data);

$headers = array ( 'Authorization: key='.$serverKey, 'Content-Type: application/json' );

$ch = curl_init(); curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);
$result = curl_exec($ch);
curl_close ($ch);

return $result; 

}

	

function save_notification($user_id,$title,$orderid,$tag,$vid){

$notifiy = new Notification_list;

$notifiy->order_id = $orderid;

$notifiy->noti_userid = $user_id;

$notifiy->noti_title = $tag;

$notifiy->noti_desc = $title;

$notifiy->vendor_id = $vid;

$notifiy->save();

  }
	

	function send_notification_ios($token,$title,$body)

	{



				

		$url = "https://fcm.googleapis.com/fcm/send";

		

		//$serverKey = 'AIzaSyBVAHr3vvKUb8-gtvDSvfLv_-1NWITPRpY';

		// $serverKey = 'AAAABbj1iac:APA91bHB98reqH6mRC6t51StUorXZopv9cTq2hJx2W_AQ2Ln6wPJ2HVJYMK0OZ6somHSS2eZH_TlFtyz5Skrx4YtoX5gbUM7SmKO-xnO-DbjCgOn-i7L27-c_Qtzm5tj_YCA235v1vxw';

		$server_key = "AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1";

		//$title = "Title";

		//$body = "Body of the message";

		

		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

		$json = json_encode($arrayToSend);

		$headers = array();

		$headers[] = 'Content-Type: application/json';

		$headers[] = 'Authorization: key='. $serverKey;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");

		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

		

		$response = curl_exec($ch);

		

		if ($response === FALSE) {

			return '0';

			//die('FCM Send Error: ' . curl_error($ch));

		}

		else

		{

			return '1';

		}



		curl_close($ch);

		

	}

	public function show_ordertoday(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

		     $countday = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->whereIn('status',  array(4) )
	        ->get();

	        $sumday = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->whereIn('status',  array(4) )
	        ->sum('total');

	         $data_onview = array('order_detail'=>$orders,'searchid'=>$request->id,'sumday'=>$sumday, 'countday'=>$countday);

	        return View('rest_owner.report_list')->with($data_onview);
		

        // $orders = Order::whereDate('created_at', NOW())->where('vendor_id', $vendor_id)

        // ->orderBy('id', 'desc')

        // ->whereIn('status',  array(4) )

        // ->paginate(10);

        
        // $week = Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('vendor_id', $vendor_id)
        // ->orderBy('id', 'desc')
        // ->whereIn('status',  array(4) )
        // ->paginate(10);


        //  $month = Order::whereMonth('created_at', NOW())->where('vendor_id', $vendor_id)
        // ->orderBy('id', 'desc')
        // ->whereIn('status',  array(4) )
        // ->paginate(10);


	}


	public function filter_order(Request $request)

	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_id = Input::post('order_id');
		
		if(!empty($order_id)){
			$orders = Order::where('vendor_id', $vendor_id)->where('order_id','LIKE','%'.$order_id.'%')
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
		}

	}

	public function filter_order_status(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}
		
		if(!empty($order_status_serach)){

			if($order_status_serach==5){

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

			}else{

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->where('status',  $order_status_serach )
	        ->paginate(10);
			}
			
				
	        $data_onview = array('order_detail'=>$orders, 'searchid'=>$order_status_serach, 'order_status_serach'=>$order_status_serach); 

	        if(!empty($_GET['order_status_serach'])){

				return View('rest_owner.report_list')->with($data_onview);

			}else{

				 return View('rest_owner.ajax.filter')->with($data_onview);	
			}    
	        
		}else{

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	         ->where('status',  $order_status_serach )
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders, 'searchid'=>$order_status_serach, 'order_status_serach'=>$order_status_serach); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
		}

	}

		public function filter_order_id(Request $request)
	    {

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$customer_id = Input::post('customer_id');
		
		if(!empty($customer_id)){

			$orders = Order::leftJoin('users', 'orders.user_id', '=', 'users.id')->where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->where('users.id', 'LIKE', '%'.$customer_id.'%' )->select('*',"users.id as userid")
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_id); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
	        
		}else{

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_id); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
		}

	}


	public function filter_order_customer(Request $request)

	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$customer_name = Input::post('customer_name');
		
		if(!empty($customer_name)){
			$orders = Order::leftJoin('users', 'orders.user_id', '=', 'users.id')->where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid")
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_name); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_name); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
		}

	}

   public function filter_order_date(Request $request)

	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_date = Input::post('order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];
		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($order_date)){
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('created_at', [$data_to, $data_from])
	        // ->orwhereDate('created_at', '<', $date[0])
	        // ->orwhereDate('created_at', '>', $date[1])
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date,'order_date'=>$order_date); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date); 

	        return View('rest_owner.ajax.filter')->with($data_onview);	
		}

	}

	public function monthly(Request $request)

	{


	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;


         $month = Order::whereMonth('created_at', NOW())->where('vendor_id', $vendor_id)
        ->orderBy('id', 'desc')
        ->whereIn('status',  array(4) )
        ->get();

      


  		$data_onview = array('month'=>$month);
		return View('rest_owner.monthly')->with($data_onview);



	}

	public function today(Request $request)

	{


	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;


         $month = $orders = Order::whereDate('created_at', NOW())->where('vendor_id', $vendor_id)

        ->orderBy('id', 'desc')

        ->whereIn('status',  array(4) )
        ->get();

      


  		$data_onview = array('month'=>$month);
		return View('rest_owner.today')->with($data_onview);



	}
	
	public function week(Request $request)

	{


	    $vendor_id = Auth::guard('vendor')->user()->vendor_id;


         $month = Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('vendor_id', $vendor_id)
        ->orderBy('id', 'desc')
        ->whereIn('status',  array(4) )
        ->get();

      


  		$data_onview = array('month'=>$month);
		return View('rest_owner.week')->with($data_onview);



	}

	public function export_in_excel(Request $request){

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'Merchant ID', 'Merchant Name', 'Customer ID', 'Customer Name', 'Order Date', 'Order Status', 'Order Sub-Total', 'Order Discount', 'Order Delivery Fee', 'Order Excise Tax', 'Order City Tax', 'Order Sales Tax', 'Total Order Amount', 'Payment Type', 'Transaction ID', 'Payment Status', 'Coupon Code', 'Commission Rate(%)', 'Merchant Amount'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 


        $lineData = array($value->order_id, $value->vendor_id, $merchant_name, $value->user_id, $value->first_name.' '.$value->last_name, $value->created_at, $orstatus, $value->sub_total, $value->promo_amount, $value->delivery_fee, $value->excise_tax, $value->city_tax, $value->service_tax, $value->total, $value->payment_method, $value->txn_id, $value->pay_status, $coupon_code, $commission_rate, $tmerchantamnt);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
	}



	public function export_in_excel_filter(Request $request){

		$searchid = $request->searchid;

		$filter = $request->filter;

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;


		if(isset($searchid) && $filter==='order_status_export'){

		if($searchid==5){	

		 $order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();       

	     }else if(!empty($searchid) && $filter==='order_date_export'){

				$date = explode('_', $searchid);

				$data_to = $date[0];// deeporder

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

			 $order_detail = Order::where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('created_at', [$data_to, $data_from])
	        // ->orwhereDate('created_at', '<', $date[0])
	        // ->orwhereDate('created_at', '>', $date[1])
	        ->paginate(10);
				

	    }else{

            $order_detail  = Order::where('vendor_id', $vendor_id)
		    ->where('status',  $searchid)
	        ->orderBy('id', 'desc')
	        ->get(); 

	     }

	    }else{

		$order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();

	     }   


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'Merchant ID', 'Merchant Name', 'Customer ID', 'Customer Name', 'Order Date', 'Order Status', 'Order Sub-Total', 'Order Discount', 'Order Delivery Fee', 'Order Excise Tax', 'Order City Tax', 'Order Sales Tax', 'Total Order Amount', 'Payment Type', 'Transaction ID', 'Payment Status', 'Coupon Code', 'Commission Rate(%)', 'Merchant Amount'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

			if($order_detail){ 
			
			foreach ($order_detail as $key => $value) { 

			$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 


        $lineData = array($value->order_id, $value->vendor_id, $merchant_name, $value->user_id, $value->first_name.' '.$value->last_name, $value->created_at, $orstatus, $value->sub_total, $value->promo_amount, $value->delivery_fee, $value->excise_tax, $value->city_tax, $value->service_tax, $value->total, $value->payment_method, $value->txn_id, $value->pay_status, $coupon_code, $commission_rate, $tmerchantamnt);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
	}
		

	 public function order_list_export(Request $request){

  		if($request->sid==''){

		$order_detail  = DB::table('orders')

		->where('vendor_id', '=' ,$request->id)

		->orderBy('id', 'desc')

		->get();


	   }else{


       	$order_detail  = DB::table('orders')

		->where('vendor_id', '=' ,$request->id)

		->where('status', '=' ,$request->sid)

		->orderBy('id', 'desc')

		->get();


	   }

		// Excel file name for download 
		$fileName = "Order-Report_" . date('Y-m-d') . ".xls";
			 
			// Column names 
		$fields = array('Order-ID','Customer-ID','Customer-Name', 'Order-Price', 'Order-Status', 'Order-Date'); 
			 
			// Display column names as first row 
		$excelData = implode(" ", array_values($fields)) . "\n"; 

		 
			if($order_detail){ 
			    // Output each row of the data 
			   foreach ($order_detail as $key => $value) { 
					$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();
					$timestamp = strtotime($value->created_at);
					$newDate = date("m-d-Y", $timestamp);
					$dataPoints1 = array(
						array("label"=>date("d", $timestamp) , "y"=> $value->total),
						
					);

					if($value->status==0){ $status = 'Pending'; }
					if($value->status==1){ $status = 'Accept'; }
					if($value->status==2){ $status = 'Cancelled'; }
					if($value->status==3){ $status = 'On the way'; }  				
					if($value->status==4){ $status = 'Delivered'; }
					if($value->status==5){ $status = 'Requested for return'; } 
					if($value->status==6){ $status = 'Return request accepted'; }
					if($value->status==7){ $status = 'Return request declined'; }

					$name = $userinfo->name.'-'.$userinfo->lname;

					$orderprice = '$'.$value->total; 

					$orderid = $value->order_id;
			        
			        $lineData = array($orderid,$userinfo->id,$name, $orderprice, $status, $newDate); 
			        //array_walk($lineData, 'filterData'); 
			        $excelData .= implode(" ", array_values($lineData)) . "\n"; 
			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			//print_r($excelData); die;
			 
			// Headers for download 
			header("Content-Type: application/vnd.ms-excel"); 
			header("Content-Disposition: attachment; filename=\"$fileName\""); 
			 
			// Render excel data 
			echo $excelData; 
			 
			exit;
	}



  	public function show_product_report(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

		     $countday = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->whereIn('status',  array(4) )
	        ->get();

	        $sumday = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->whereIn('status',  array(4) )
	        ->sum('total');

	         $data_onview = array('order_detail'=>$orders,'searchid'=>$request->id,'sumday'=>$sumday, 'countday'=>$countday);

	        return View('rest_owner.product_report_list')->with($data_onview);
		
	}	


	public function post_product_report()
	{ 

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$vender_detail = DB::table('vendor')

						 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();


		$restaurant_detail = DB::table('restaurant')

						 ->select('*')

						 ->where('rest_status', '!=' ,'INACTIVE')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

		$rest_id_array='';

		if($restaurant_detail)

		{

			foreach($restaurant_detail as $rest_data)

			{

				$rest_id_array[]=$rest_data->rest_id;

			}

		}


		$order_detail  =  '';


		if((count($rest_id_array)>0) && (!empty($rest_id_array))){

			$order_detail  =  DB::table('order');

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

		    $order_detail = $order_detail->where(function($query) use ($rest_id_array) {

				$l=0;

				foreach($rest_id_array as $ftype )

				{

					if($ftype>0)

					{

						if($l>0){

							 $query = $query->orWhere('order.rest_id','=',$ftype);

						 }

						 else

						 {

						 	 $query = $query->Where('order.rest_id','=',$ftype);

						 }

					}

					$l++;

				}



			});


			/*USER BASE SEARC FORM USER PAGE  */


			if($user_id>0)

			{

				$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

			}


			/* Status Search */



			if(!empty($status) && ($status>0))

			{

			  $order_detail = $order_detail->where('order.order_status', '=', $status);

			}



			/* Get Data between Date */


			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

			}



			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

			}


			/* End */


			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();

		}


		$title = 'Order Report';


		$all_array = array();

		if(!empty($order_detail))

		{

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)

			{	$sr++;


				 $status = '';

				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				 if($data_list->order_status=='2'){ $status = 'Cancelled'; }

				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; }

				 if($data_list->order_status=='6'){ $status = 'Reject'; }

				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }

				  $cart_price = 0;

				  $sumary = '';

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

		{

			$order_carditem = json_decode($data_list->order_carditem, true);

				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";


				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

				 {

					foreach($item['options']['addon_data'] as $addon)

					{

						$cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 }



			}

		}





				$all_array[] = array('0'=>$sr,

									'1'=>$data_list->order_id,

									'2'=>$data_list->order_create,

									'3'=>$data_list->rest_name,

									'4'=>$status,

									'5'=>$data_list->order_fname.' '.$data_list->order_lname,

									'6'=>$data_list->order_tel,

									'7'=>$sumary,

									'8'=>'$'.$cart_price,

									);





			}

			$data = $all_array ;

		}

		else

		{

			$data = array(

		   			array('No REcord Found')

			);

		}


		$filename = "order_report.csv";

		$fp = fopen('php://output', 'w');

		$header =array('',	$title, '', '');

		header('Content-type: application/csv');

		header('Content-Disposition: attachment; filename='.$filename);

		fputcsv($fp, $header);


		$header2 =array('SNo.', 'OrderNo', 'OrderReq_date', 'Restaurant','Status','UserName','Contact No', 'Order Sumary','TotalAmount');

		fputcsv($fp, $header2);


		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }


	}


	public function product_export_excel(Request $request){

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order Date', 'Order ID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'Unit Price', 'Total'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 


$productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

foreach ($productinfo as $key => $pdata) {

	   $totalprice = '$'.$pdata->ps_qty*$pdata->price;
	   $unitprice = '$'.$pdata->price;

        $lineData = array($value->created_at, $value->order_id, $merchant_name, $value->first_name.' '.$value->last_name, $orstatus, $pdata->name, $pdata->ps_qty, $unitprice, $totalprice);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

		}	    

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
	}



	public function product_export_filter_excel(Request $request){

		$searchid = $request->searchid;

		$filter = $request->filter;

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		if(isset($searchid) && $filter==='order_status_export'){

		if($searchid==5){	

		 $order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();       

	     }else{

            $order_detail  = Order::where('vendor_id', $vendor_id)
		    ->where('status',  $searchid)
	        ->orderBy('id', 'desc')
	        ->get(); 

	     }

	    }else if(!empty($searchid) && $filter==='order_date_export'){

				$date = explode('_', $searchid);

				$data_to = $date[0];// deeporder

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

			 $order_detail = Order::where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('created_at', [$data_to, $data_from])
	        // ->orwhereDate('created_at', '<', $date[0])
	        // ->orwhereDate('created_at', '>', $date[1])
	        ->paginate(10);
				

	    }else{

		$order_detail  = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->get();

	     }   


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order Date', 'Order ID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'Unit Price', 'Total'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 


$productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

foreach ($productinfo as $key => $pdata) {

	   $totalprice = '$'.$pdata->ps_qty*$pdata->price;
	   $unitprice = '$'.$pdata->price;

        $lineData = array($value->created_at, $value->order_id, $merchant_name, $value->first_name.' '.$value->last_name, $orstatus, $pdata->name, $pdata->ps_qty, $unitprice, $totalprice);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

		}	

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
	}


  
	public function product_filter_order(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_id = Input::post('order_id');
		
		if(!empty($order_id)){
			$orders = Order::where('vendor_id', $vendor_id)->where('order_id','LIKE','%'.$order_id.'%')
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id,'order_id_filter'=>$order_id); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
		}

	}

	public function product_filter_order_status(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}
		
		if(!empty($order_status_serach)){

			if($order_status_serach==5){

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

			}else{

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->where('status',  $order_status_serach )
	        ->paginate(10);
			}
			
				
	        $data_onview = array('order_detail'=>$orders, 'searchid'=>$order_status_serach,'order_status_serach'=>$order_status_serach,'order_date_filter'=>''); 

	        if(!empty($_GET['order_status_serach'])){

				return View('rest_owner.report_list')->with($data_onview);

			}else{

				 return View('rest_owner.ajax.product_filter')->with($data_onview);	
			}    
	        
		}else{

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	         ->where('status',  $order_status_serach )
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders, 'searchid'=>$order_status_serach, 'order_status_serach'=>$order_status_serach); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
		}

	}



	public function product_filter_order_customer(Request $request)

	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$customer_name = Input::post('customer_name');
		
		if(!empty($customer_name)){
			$orders = Order::leftJoin('users', 'orders.user_id', '=', 'users.id')->where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid")
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_name,'customer_name'=>$customer_name); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders, 'searchid'=>$customer_name); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
		}

	}

   public function product_filter_order_date(Request $request)
	{
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];
		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($order_date)){

			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('created_at', [$data_to, $data_from])
	        // ->orwhereDate('created_at', '<', $date[0])
	        // ->orwhereDate('created_at', '>', $date[1])
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date,'order_date'=>$order_date); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
	        
		}else{
			$orders = Order::where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date); 

	        return View('rest_owner.ajax.product_filter')->with($data_onview);	
		}

	}	


}

