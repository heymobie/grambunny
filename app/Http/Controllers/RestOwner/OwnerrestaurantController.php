<?php 
namespace App\Http\Controllers\RestOwner;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Str;
Use DB;
use Hash;
use Session;
use Redirect;
use Validator;
use Route;
use File;
use App\Restaurant;
use App\Menu;
use App\Menu_category;

use App\Menu_category_item;

use App\Menu_category_addon;

use App\Service;

use App\Bankdetail;

use App\Promotion;

use App\Category_item;

use App\Rest_time;

use App\Proservicescate; 

use App\Productservice;



class OwnerrestaurantController  extends Controller
{

    public function __construct(){

    	$this->middleware('vendor');

    }	

	public function product_service_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')		

					->where('vendor_id', '=' ,$vendor_id)

					->orderBy('id', 'asc')

					->get();

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('rest_owner.product_service_list')->with($data_onview);

	}

	public function all_product_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')
	                ->where('type', '!=', 3)	

	                ->where('vendor_id', '=' ,$vendor_id)	

					->orderBy('id', 'asc')

					->get();


		$rest_list_admin = DB::table('admin_merchant_product')		

		->leftJoin('product_service', 'admin_merchant_product.product_id', '=', 'product_service.id')

		->where('admin_merchant_product.vendor_id', '=' ,$vendor_id)

		->select('admin_merchant_product.*','product_service.*')

		->orderBy('admin_merchant_product.id', 'asc')

		->get();			

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'rest_list_admin' =>$rest_list_admin,'cat_name' =>$cat_name);

		return View('rest_owner.all_product_list')->with($data_onview);

	}


	public function all_event_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')
	                ->where('type', '=', 3)	

	                ->where('vendor_id', '=' ,$vendor_id)	

					->orderBy('id', 'asc')

					->get();


		$rest_list_admin = DB::table('admin_merchant_product')		

		->leftJoin('product_service', 'admin_merchant_product.product_id', '=', 'product_service.id')

		->where('admin_merchant_product.vendor_id', '=' ,$vendor_id)

		->select('admin_merchant_product.*','product_service.*')

		->orderBy('admin_merchant_product.id', 'asc')

		->get();			

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'rest_list_admin' =>$rest_list_admin,'cat_name' =>$cat_name);

		return View('rest_owner.all_event_list')->with($data_onview);

	}

	public function inventory_product(Request $request)
	{

		$id = $request->id;

		$rest_list = '';

		$rest_list1 = '';

		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')->where('id', '=' ,$id)->where('vendor_id','=',$vendor_id)->paginate(10);

		if(count($rest_list)==0){


        $rest_list1 = DB::table('admin_merchant_product')->where('product_id','=',$id)->where('vendor_id','=',$vendor_id)->get();

		}

			
		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'rest_list1' =>$rest_list1,'cat_name' =>$cat_name);

		return View('rest_owner.inventory_product')->with($data_onview);

		}

		public function inventory_event(Request $request)
	{

		$id = $request->id;

		$rest_list = '';

		$rest_list1 = '';

		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')->where('id', '=' ,$id)->where('vendor_id','=',$vendor_id)->get();

		if(count($rest_list)==0){


        $rest_list1 = DB::table('admin_merchant_product')->where('product_id','=',$id)->where('vendor_id','=',$vendor_id)->get();

		}

			
		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'rest_list1' =>$rest_list1,'cat_name' =>$cat_name);

		return View('rest_owner.inventory_event')->with($data_onview);

		}
	

	public function my_product_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')
	                   ->where('type', '!=', 3)		

					->where('vendor_id', '=' ,$vendor_id)

					->where('admin_product_id', '=' ,0)

					->orderBy('id', 'asc')

					->get();

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('rest_owner.my_product_list')->with($data_onview);

	}

		public function my_event_list()
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		DB::connection()->enableQueryLog();
		$ticket_booth_list = DB::table('product_service')->where('vendor_id', '=', $vendor_id)->where('type', '=', 3)->orderBy('id', 'asc')->get();
		$cat_name = DB::table('product_service_category')->value('category');
		$data_onview = array('rest_list' => $ticket_booth_list, 'cat_name' => $cat_name);
		return View('rest_owner.my_event_list')->with($data_onview);

	}

	public function crop_event_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('event_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

        DB::table('product_service')
		//->where('vendor_id', $vendor_id)
		->where('id', $product_id)
		->update(['image' => $product_img]);

		DB::table('ps_images')
		->where('ps_id', $product_id)
		->where('thumb', 1)
		->update(['name' => $product_img]);

		return response()->json(['status'=>$product_img]);

	}

	public function gallery_event_crop_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('event_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

		DB::table('ps_images')
		->insert([
			'ps_id' => $product_id,
			'name' => $product_img,
			'created_at' => date("Y-m-d h:i:s"),
			'updated_at'=> date("Y-m-d h:i:s")
		]);

    return response()->json(['status'=>$product_img]);

	}

	public function admin_product_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$rest_list = DB::table('product_service')

		->where('type', '!=', 3)		

		->where('vendor_id', '=' ,$vendor_id)

		->where('admin_product_id', '!=' ,0)

		->orderBy('id', 'asc')

		->get();	


		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('rest_owner.admin_product_list')->with($data_onview);

	}

	public function admin_event_list()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$rest_list = DB::table('product_service')

		->where('type', '=', 3)		

		->where('vendor_id', '=' ,$vendor_id)

		->where('admin_product_id', '!=' ,0)

		->orderBy('id', 'asc')

		->get();	


		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name);

		return View('rest_owner.admin_event_list')->with($data_onview);

	}

   public function add_admin_product()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')

	                 ->where('type', '!=', 3) 		

					->where('vendor_id', '=' ,0)

					->orderBy('id', 'asc')

					->get();

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name,'vendor_id' =>$vendor_id);

		return View('rest_owner.add_admin_product')->with($data_onview);

	}


	 public function add_admin_event()
	{
		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $rest_list = DB::table('product_service')	
	                 ->where('type', '=', 3)		

					->where('vendor_id', '=' ,0)

					->orderBy('id', 'asc')

					->get();

		$catid = DB::table('vendor')		
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

		$cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$catid)
						->value('category');			

		$data_onview = array('rest_list' =>$rest_list,'cat_name' =>$cat_name,'vendor_id' =>$vendor_id);

		return View('rest_owner.add_admin_event')->with($data_onview);

	}

	public function service_list()

	{

		DB::connection()->enableQueryLog();	

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$ps_type = "1";

	    $rest_list = DB::table('product_service')			

					->where('vendor_id', '=' ,$vendor_id)

					->where('type', '=' ,$ps_type)

					->orderBy('id', 'desc')

					->get();

		$data_onview = array('rest_list' =>$rest_list);

		return View('rest_owner.service_list')->with($data_onview);

	}		

	public function restaurant_search()

	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$rest_list = DB::table('restaurant');		

		$rest_list = $rest_list->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id');

		if( (Input::get('rest_cont')) && (!empty(Input::get('rest_cont'))))

		{


			$rest_list = $rest_list->where('restaurant.rest_contact', 'like' ,Input::get('rest_cont'));						
			$rest_list = $rest_list->orwhere('restaurant.rest_landline', 'like' ,Input::get('rest_cont'));


		}


		if( (Input::get('rest_name')) && (!empty(Input::get('rest_name'))))


		{

			$rest_list = $rest_list->where('restaurant.rest_name', 'like' ,'%'.Input::get('rest_name').'%');

		}

		$rest_list = $rest_list->where('restaurant.vendor_id', '=' ,$vendor_id);

		$rest_list = $rest_list->select('restaurant.*','vendor.name');

		$rest_list = $rest_list->orderBy('restaurant.rest_id', 'desc');

		$rest_list = $rest_list->orderBy('restaurant.rest_id', 'desc');

		$rest_list = $rest_list->get();	

		$data_onview = array('rest_list' =>$rest_list);

		return View('rest_owner.ajax.restaurant_list')->with($data_onview);		

	}


	public function product_service_form(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	  // dd($proservice_cat);	

	    $ps_type  = DB::table('vendor')
	                     ->where('type', '!=', 3)
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');				

		$cat_name  = DB::table('product_service_category')->where('type', '!=' ,'3')->orderBy('category', 'asc')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->where('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->where('type', '!=' ,'3')->orderBy('category', 'asc')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		$product_unit = DB::table('product_unit')
		                     ->where('type', '!=' ,'3')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();	


		$product_types = DB::table('product_types')

							->where('status', '=' ,'1')

							->orderBy('id', 'asc')->get();
							
		$product_brands = DB::table('product_brands')

							->where('status', '=' ,'1')

							->orderBy('id', 'asc')->get();										

		if($request->id)

		{
			$id = $request->id;
			$rest_detail  = DB::table('product_service')

							->where('id', '=' ,$id)								

							 ->where('vendor_id', '=' ,$vendor_id)

							->get();

			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'product_types'=>$product_types,	

							     'product_brands'=>$product_brands,	

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.product_service_form')->with($data_onview);

			}

			else

			{

			$id =0;
  		  	$data_onview = array('vendor_id' =>$vendor_id,		
								 'proservice_cat'=>$proservice_cat,	
								 'proservice_sub_cat'=>$proservice_sub_cat,	
								 'category_list'=>$category_list,	
								 'sub_category_list'=>$sub_category_list,	
								 'product_unit'=>$product_unit,	
								 'product_types'=>$product_types,	
							     'product_brands'=>$product_brands,	
								 'cat_name'=>$cat_name,
								 'ps_type'=>$ps_type,
								 'id'=>$id); 

			return View('rest_owner.product_service_form')->with($data_onview);						 

		}



	}

	public function event_service_form(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();
		$firstcate = DB::table('product_service_category')->first();
		$cateid = $firstcate->id;
		$sub_category_list = DB::table('product_service_sub_category')
			->where('category_id', '=', $cateid)
			->orderBy('id', 'asc')
			->get();

		$product_unit = DB::table('product_unit')
		    ->where('type', '=', '3')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$product_types = DB::table('product_types')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$product_brands = DB::table('product_brands')
			->where('status', '=', '1')
			->orderBy('id', 'asc')->get();
		$id = 0;

		$data_onview = array(
			'vendor_id' =>$vendor_id,
			'category_list' => $category_list,
			'sub_category_list' => $sub_category_list,
			'product_unit' => $product_unit,
			'product_types' => $product_types,
			'product_brands' => $product_brands,
			'id' => $id
		);

			return View('rest_owner.event_service_form')->with($data_onview);						 

		}

		public function event_service_edit($id)
	{
		$ticket_detail  = DB::table('product_service')->where('id', '=', $id)->get();
		$product_detail  = DB::table('product_service')->where('id', '=', $id)->get();
		$product_unit = DB::table('product_unit')

	   ->where('type', '=', '3')

		->where('status', '=', '1')

		->orderBy('id', 'asc')->get();

		$category_list  = DB::table('product_service_category')->where('type', '=', 3)->get();
		$sub_category_list = DB::table('product_service_sub_category')
			->orderBy('id', 'asc')
			->get();

			$vendor_id = DB::table('product_service')
			->where('id', '=', $id)
			->value('vendor_id');


		$gallery_image = DB::table('ps_images')
		->where('ps_id', '=', $id)
		->whereNull('thumb')
		->get();
		$glimage = array();
				foreach ($gallery_image as $key => $value) {
					$glimage[] = $value;
		}

		$data_onview = array(
			'vendor_id' => $vendor_id,
			'category_list' => $category_list,
			'sub_category_list' => $sub_category_list,
			'product_unit' => $product_unit,
			'ticket_detail' => $ticket_detail,
			'product_detail' => $product_detail,
			'glimage' => $glimage,
			'id' => $id
		);
		return View('rest_owner.event_service_form')->with($data_onview);
	}

		public function event_service_action(Request $request)
	{


			$this->validate($request, [
				'ticket_price'  => 'required|numeric|between:0,999999.99',
				'ticket_commission' => 'required|numeric|between:0,999999.99',
				'ticket_fee' => 'required|numeric|between:0,999999.99',
				'ticket_service_fee' => 'required|numeric|between:0,999999.99',
				'event_date'		=> 'required|date',
			   ]);
			

		$event_id = Input::get('event_id');
		$date = date('Y-m-d H:i:s');
		$eventname = Input::get('event_name');
		$ticket_code_str = $this->random_string(8);
		$slug = Str::slug($eventname . ' ' . $ticket_code_str, '-');
		// $vendor_id = '0';

		$vendor_id = Input::get('vendor_id');


		$product_old_img = Input::get('product_old_img');
		$product_old_glimg = Input::get('product_old_glimg');

		// if ($request->hasFile("product_image")) {
		// 		$productImage = $request->product_image;
		// 		$image = $largeImage = Image::make($productImage->getRealPath());
		// 		$image->resize(200, 200)->save(public_path('uploads/product/' . $pname = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension()));
		// 		$productImage->move(public_path("uploads/product/"), $imageTlarg = str_random(6) . time() . '.' . $productImage->getClientOriginalExtension());

		// 		}else{
					$pname = $product_old_img;

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg;

                   if(empty($glimg)){ $glimg = "dummy.png"; }
			//	}



		if($event_id == 0){
			$ticket_booth = new Productservice;
			$ticket_booth->vendor_id = $vendor_id;

			$ticket_booth->category_id = Input::get('category_id');
			$ticket_booth->sub_category_id = Input::get('sub_category_id');
            $ticket_booth->unit = Input::get('unit');
			$ticket_booth->type = Input::get('ps_type');
			$ticket_booth->addon_type =  Input::get('addon_type');
			$ticket_booth->name = Input::get('event_name');
			$ticket_booth->slug = $slug;
			$ticket_booth->notes_remarks =  Input::get('notes_remarks');
			$ticket_booth->description =  Input::get('event_desc');
			$ticket_booth->venue_name =  Input::get('venue_name');
			//$ticket_booth->image =  'product.jpg';
			$ticket_booth->image =  (!empty($pname)) ? $pname : 'product.jpg';
			$ticket_booth->stock = 1;
			$ticket_booth->venue_address =  Input::get('venue_address');
			$ticket_booth->event_date =  Input::get('event_date');
			$ticket_booth->event_start_time =  Input::get('event_start_time');
			$ticket_booth->event_end_time =  Input::get('event_end_time');
			$ticket_booth->quantity =  Input::get('ticket_inventory');
			$ticket_booth->seating_area =  Input::get('seating_area');
			$ticket_booth->price =  Input::get('ticket_price');
			$ticket_booth->ticket_commission =  Input::get('ticket_commission');
			$ticket_booth->ticket_fee =  Input::get('ticket_fee');
			$ticket_booth->ticket_service_fee =  Input::get('ticket_service_fee');
			$ticket_booth->product_code =  $ticket_code_str;
			$ticket_booth->status =  Input::get('status');
			$ticket_booth->created_at = $date;
			$ticket_booth->save();


			DB::table('ps_images')->insert(
				['ps_id' => $ticket_booth->id,'name' => $glimg, 'thumb' => NULL, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
			);


			// if ($request->hasFile("product_image_gallery")) {
			// $files = $request->file('product_image_gallery');
			// 		foreach ($files as $file) {
			// 			$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
			// 			$image = $file;
			// 			$image->move('public/uploads/product/', $name);
			// 			DB::table('ps_images')->insert([
			// 				'name' => $name,
			// 				'ps_id' => $ticket_booth->id,
			// 				'thumb' => NULL,
			// 				'created_at' => $date,
			// 				'updated_at' => $date
			// 			]);
			// 		}
			// 	}
			Session::flash('message', 'Item Added Sucessfully!');
			return redirect()->to('/merchant/my-event-list')->withMessage('Item Added Sucessfully!');;

		} else {
			/*********  END  START MANAGE TIME IN OTHER TABLE ******/
			// Please make changes this code for merchant
			// echo "<pre>";print('Please make changes for the below code for merchant');die;
			DB::table('product_service')
				->where('id', $event_id)
				->update([

					'category_id' => Input::get('category_id'),
					'sub_category_id' => Input::get('sub_category_id'),
                    'unit' => Input::get('unit'),
					'name' => Input::get('event_name'),
					'product_code' => $ticket_code_str,
					'notes_remarks' => Input::get('notes_remarks'),
					'description' => Input::get('event_desc'),
					'venue_name' => Input::get('venue_name'),
					'venue_address' => Input::get('venue_address'),
					//'image' => 'product.jpg',
					'image' => $pname,
					'event_date' =>  Input::get('event_date'),
					'event_start_time' =>  Input::get('event_start_time'),
					'event_end_time' =>  Input::get('event_end_time'),
					'quantity' =>  Input::get('ticket_inventory'),
					'seating_area' =>  Input::get('seating_area'),
					'price' => Input::get('ticket_price'),
					'ticket_commission' => Input::get('ticket_commission'),
					'ticket_fee' =>  Input::get('ticket_fee'),
					'ticket_service_fee' =>  Input::get('ticket_service_fee'),
					'status' => Input::get('status'),
					'updated_at' => $date
				]);

/*
				DB::table('ps_images')->where('ps_id', $event_id)->update([
					'updated_at' => date('Y-m-d H:i:s'),
				]);
*/

/*
		if (!empty($imageTlarg)) {
				DB::table('ps_images')
					->where('ps_id', $event_id)
					->where('thumb', 1)
					->update([
						'name' => $imageTlarg,
					]);
			}

*/

				if ($request->hasFile("product_image_gallery")) {
							$files = $request->file('product_image_gallery');
							foreach ($files as $file) {
								$name = str_random(6) . time() . '.' . $file->getClientOriginalExtension();
								$image = $file;
								$image->move('public/uploads/product/', $name);
								DB::table('ps_images')->insert([
									'name' => $name,
									'ps_id' => $event_id,
									'thumb' => NULL,
									'created_at' => $date,
									'updated_at' => $date
								]);
							}
				}

			session()->flash('message', 'Item Update Sucessfully!');
			return redirect()->back()->withMessage('Item Update Sucessfully!');
		}
	}

		public function event_service_delete($id)
	{
		DB::table('product_service')->where('id', '=', $id)->delete();

		DB::table('ps_images')->where('ps_id', '=', $id)->delete();

		Session::flash('message', 'Item Delete Sucessfully!');
		return redirect()->to('/merchant/my-event-list');
	}



	public function admin_merchant_product_form(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->whereIn('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;


		$rest_detail = DB::table('product_service')->where('id', '=' ,$id)->get();
						

			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.admin_merchant_product_form')->with($data_onview);

	}

	public function admin_merchant_event_form(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->whereIn('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;


		$rest_detail = DB::table('product_service')->where('id', '=' ,$id)->get();
						

			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.admin_merchant_event_form')->with($data_onview);

	}



	public function admin_merchant_product_view(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->whereIn('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;


		$rest_detail = DB::table('product_service')->where('id', '=' ,$id)->get();

						
			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.admin_merchant_product_view')->with($data_onview);

	}

	public function admin_merchant_event_view(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->whereIn('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;


		$rest_detail = DB::table('product_service')->where('id', '=' ,$id)->get();

						
			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.admin_merchant_event_view')->with($data_onview);

	}




	public function merchant_product_view(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->where('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;

 
       		$rest_detail = DB::table('product_service')

		                    ->where('id', '=' ,$id)

		                    ->where('vendor_id', '=' ,$vendor_id)

							->get();	


			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.merchant_product_view')->with($data_onview);

	}

	public function merchant_event_view(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$proservice_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('category_id');

	    $ps_type  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('type');					

		$cat_name  = DB::table('product_service_category')->get();		

		$pssub_cat  = DB::table('vendor')
						->where('vendor_id', '=' ,$vendor_id)
						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->where('id',$sub_cat)
							->orderBy('id', 'asc')
							->get();

	    $category_list  = DB::table('product_service_category')->get();	

        $firstcate = DB::table('product_service_category')->first();

        $cateid = $firstcate->id; 

		$sub_category_list = DB::table('product_service_sub_category')

		                    ->where('category_id', '=' ,$cateid)

							->orderBy('id', 'asc')

							->get();						

		    $product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();					

			$id = $request->id;

 
       		$rest_detail = DB::table('product_service')

		                    ->where('id', '=' ,$id)

		                    ->where('vendor_id', '=' ,$vendor_id)

							->get();	


			$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

			$glimage = array();				

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }											
  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,

								 'category_list'=>$category_list, 

								 'sub_category_list'=>$sub_category_list,		

								 'product_unit'=>$product_unit,

								 'cat_name'=>$cat_name,

								 'ps_type'=>$ps_type,

								 'glimage'=>$glimage,	

								 'id'=>$id); 

				return View('rest_owner.merchant_event_view')->with($data_onview);

	}


  	public function merchant_photo_delete($picno,$mid)
	{

        if($picno==2){ $profileno = 'profile_img2'; }
        if($picno==3){ $profileno = 'profile_img3'; }
        if($picno==4){ $profileno = 'profile_img4'; }

		DB::table('vendor')->where('vendor_id', $mid)->update([$profileno => '']);

		Session::flash('message', 'Profile Image Delete Successfully!'); 

		return redirect()->to('/merchant/update_profile');

	}	



	public function service_form(Request $request)

	{



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;


		$proservice_cat  = DB::table('product_service_category')
						->where('type', '=' ,'1')
						->where('status', '=' ,'1')
						->orderBy('id', 'asc')
						->get();

		$proservice_sub_cat = DB::table('product_service_sub_category')
							->where('category_id', '=' ,$proservice_cat[0]->id)
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();

		$product_unit = DB::table('product_unit')
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();	


		if($request->id)
		{

			$id =$request->id;
			$rest_detail  = DB::table('product_service')
							->where('id', '=' ,$id)								
							 ->where('vendor_id', '=' ,$vendor_id)
							->get();

		   	$gallery_image = DB::table('ps_images')
	     					->where('ps_id', '=' ,$id)								
							 ->whereNull('thumb')
							->get();

		    $glimage = array();					

			foreach ($gallery_image as $key => $value) { $glimage[] = $value; }						

			$catidd = $rest_detail[0]->category_id;	
			
			$proservice_sub_cat = DB::table('product_service_sub_category')
							->where('category_id', '=' ,$catidd)
							->where('status', '=' ,'1')
							->orderBy('id', 'asc')->get();			

  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'proservice_cat'=>$proservice_cat, 

								 'proservice_sub_cat'=>$proservice_sub_cat,	

								 'product_unit'=>$product_unit,	

								 'glimage'=>$glimage,	

								 'id'=>$id); 				

			return View('rest_owner.service_form')->with($data_onview);

			}

			else

			{

			$id =0;

            $data_onview = array('vendor_id' =>$vendor_id,		

								 'proservice_cat'=>$proservice_cat,	

								 'proservice_sub_cat'=>$proservice_sub_cat,	

								 'product_unit'=>$product_unit,	

								 'id'=>$id); 

			return View('rest_owner.service_form')->with($data_onview);							 

		}

	}

	public function subcategory_list(Request $request)
	{

		$cateid = $request->catid;

		$sub_cat_id = $request->sub_cat_id;

	    $sub_cate = DB::table('product_service_sub_category')		
					->where('category_id', '=' ,$cateid)
					->orderBy('id', 'asc')
				    ->get();

		$data_onview = array('sub_cate'=>$sub_cate,'sub_cat_id'=>$sub_cat_id); 

		return View('rest_owner.ajax.sub_category')->with($data_onview);

	}
  
  public function subcategory_list_show(Request $request)
	{

		$cateid = $request->catid;

		$sub_cat_id = $request->sub_cat_id;

	    $sub_cate = DB::table('product_service_sub_category')		
					->where('category_id', '=' ,$cateid)
					->orderBy('id', 'asc')
				    ->get();

		$data_onview = array('sub_cate'=>$sub_cate,'sub_cat_id'=>$sub_cat_id); 

		return View('rest_owner.ajax.sub_category')->with($data_onview);

	}

	public function restaurant_branch_form(Request $request)

	{



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$mon_from='';		

		$mon_to='';		

		$tues_from='';		

		$tues_to='';		

		$wed_from='';		

		$wed_to='';		

		$thu_from='';		

		$thu_to='';		

		$fri_from='';		

		$fri_to='';		

		$sat_from='';		

		$sat_to='';		

		$sun_from='';		

		$sun_to='';



		if(Route::current()->getParameter('id'))

		{	

			$id = Route::current()->getParameter('id');

			$rest_detail  = DB::table('restaurant')

							->where('rest_id', '=' ,$id)				

							->where('vendor_id', '=' ,$vendor_id)

							->get();

			if($rest_detail)

			{

		

			if(($id>0) && (!empty($rest_detail[0]->rest_mon))){



				$v = explode('_',$rest_detail[0]->rest_mon);			

				$mon_from=$v[0];

				$mon_to=$v[1];	

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_tues))){

				$v = explode('_',$rest_detail[0]->rest_tues);			

				$tues_from=$v[0];

				$tues_to=$v[1];

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_wed))){

				$v = explode('_',$rest_detail[0]->rest_wed);			

				$wed_from=$v[0];

				$wed_to=$v[1];

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_thus))){

				$v = explode('_',$rest_detail[0]->rest_thus);			

				$thu_from=$v[0];

				$thu_to=$v[1];

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_fri))){

				$v = explode('_',$rest_detail[0]->rest_fri);			

				$fri_from=$v[0];

				$fri_to=$v[1];

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_sat))){

				$v = explode('_',$rest_detail[0]->rest_sat);			

				$sat_from=$v[0];

				$sat_to=$v[1];

			}



			if(($id>0) && (!empty($rest_detail[0]->rest_sun))){

				$v = explode('_',$rest_detail[0]->rest_sun);			

				$sun_from=$v[0];

				$sun_to=$v[1];

			}



			$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();	

  		  	$data_onview = array('rest_detail' =>$rest_detail,		

								 'mon_from' =>$mon_from,		

								 'mon_to' =>$mon_to,		

								 'tues_from' =>$tues_from,		

								 'tues_to' =>$tues_to,		

								 'wed_from' =>$wed_from,

								 'wed_to' =>$wed_to,

								 'thu_from' =>$thu_from,

								 'thu_to' =>$thu_to,

								 'fri_from' =>$fri_from,

								 'fri_to' =>$fri_to,

								 'sat_from' =>$sat_from,

								 'sat_to' =>$sat_to,

								 'sun_from' =>$sun_from,	

								 'sun_to' =>$sun_to,

								 'cuisine_list'=>$cuisine_list,	

								 'id'=>$id); 

				return View('rest_owner.restaurant_branch_form')->with($data_onview);

			}

			else

			{

				return redirect()->to('/merchant/product-list');

			}						

		}

		else

		{

			$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();	

			$id =0;

  		  	$data_onview = array('vendor_id' =>$vendor_id,		

								 'mon_from' =>$mon_from,		

								 'mon_to' =>$mon_to,		

								 'tues_from' =>$tues_from,		

								 'tues_to' =>$tues_to,		

								 'wed_from' =>$wed_from,		

								 'wed_to' =>$wed_to,		

								 'thu_from' =>$thu_from,		

								 'thu_to' =>$thu_to,		

								 'fri_from' =>$fri_from,		

								 'fri_to' =>$fri_to,

								 'sat_from' =>$sat_from,			

								 'sat_to' =>$sat_to,				

								 'sun_from' =>$sun_from,	

								 'sun_to' =>$sun_to,

								 'cuisine_list'=>$cuisine_list,		

								 'id'=>$id); 

			return View('rest_owner.restaurant_branch_form')->with($data_onview);

		}

	}



	function clean_string($string){


		$string1 = strtolower($string);

		//Make alphanumeric (removes all other characters)

		$string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);

		//Clean up multiple dashes or whitespaces

		$string1 = preg_replace("/[\s-]+/", " ", $string1);

		//Convert whitespaces and underscore to dash

		$string1 = preg_replace("/[\s_]/", "-", $string1);

		if( !empty($string1) || $string1 != '' ){

		return $string1;

		}else{

		return $string;

		}

	}


   public function inventory_product_action(Request $request)
	{	

	  $searchpro = $request->all();

	  $vendor_id = Auth::guard('vendor')->user()->vendor_id;

      $productid = $searchpro['data']['productid']; 
      $productqty = $searchpro['data']['productqty']; 
      //$vendorid = $searchpro['data']['vendorid']; 

      $updatepro = DB::table('product_service')->where('id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['quantity' => $productqty]); 

   if(empty($updatepro)){

    $updatepro = DB::table('admin_merchant_product')->where('product_id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['product_quantity' => $productqty]); 

   }
 
   return $updatepro;

   }

   public function inventory_event_action(Request $request)
	{	

	  $searchpro = $request->all();

	  $vendor_id = Auth::guard('vendor')->user()->vendor_id;

      $productid = $searchpro['data']['productid']; 
      $productqty = $searchpro['data']['productqty']; 
      //$vendorid = $searchpro['data']['vendorid']; 

      $updatepro = DB::table('product_service')->where('id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['quantity' => $productqty]); 

   if(empty($updatepro)){

    $updatepro = DB::table('admin_merchant_product')->where('product_id','=',$productid)->where('vendor_id','=',$vendor_id)->update(['product_quantity' => $productqty]); 

   }
 
   return $updatepro;

   }

	public function product_service_action(Request $request)
	{	

		$product_id = Input::get('product_id');

		$product_old_img = Input::get('product_old_img');

		$product_old_glimg = Input::get('product_old_glimg');

		$date = date('Y-m-d H:i:s');

		$proname = Input::get('product_name');

		$product_code_str = $this->random_string(8); 

		$product_code = Input::get('product_code');

		$slug = Str::slug($proname.' '.$product_code_str, '-');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$imageTlarg = '';

		    // if ($request->product_id==0) {

		    //     $this->validate($request,[

		    //     	"product_image" => "required|image",

		    //     ],[

		    //     	"product_image.image" => "The product image must be an image.",

		    //     ]);

		    //     $this->validate($request,["product_image_gallery" => "required"]);  

		    //   }

            //     if ($request->hasFile("product_image")) {

            //     	$productImage=$request->product_image;

            //     	$image =$largeImage= Image::make($productImage->getRealPath());    

			// 		$image->resize(200,200)->save(public_path('uploads/product/'.$pname=str_random(6).time().'.'.$productImage->getClientOriginalExtension()));

			// 		$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(6).time().'.'.$productImage->getClientOriginalExtension());

            //     }else{

                  $pname = $product_old_img; 

                   if(empty($pname)){ $pname = "dummy.png"; }

                   $glimg = $product_old_glimg; 

                   if(empty($glimg)){ $glimg = "dummy.png"; }

              //  }
       

		if($product_id==0)

		{

				$product = new Productservice; 

				$product->vendor_id = $vendor_id;

				$product->type = Input::get('ps_type');	

				$product->category_id = Input::get('category_id');

				$product->sub_category_id = Input::get('sub_category_id');	

				$product->name = Input::get('product_name');

				$product->slug = $slug;

				$product->description =  Input::get('product_desc');

				$product->price =  Input::get('product_price');

				$product->excise_tax =  Input::get('excise_tax');

				$product->quantity =  Input::get('product_quantity');

				$product->unit =  Input::get('unit');


				$product->brands =  Input::get('brands');

				$product->types =  Input::get('types');

				$product->potency_thc =  Input::get('potency_thc');

				$product->potency_cbd =  Input::get('potency_cbd');


				$product->image =  $pname;

				$product->keyword =  '';

				$product->product_code =  $product_code;

				$product->stock =  Input::get('stock');

				$product->status =  Input::get('status');

				$product->save();

				DB::table('ps_images')->insert([
				'name' => $glimg,
				'ps_id' => $product->id,
				'thumb' => Null,
				'created_at' => $date,
				'updated_at' => $date	

				]);

				 
				// $files=$request->file('product_image_gallery');

				// foreach($files as $file){

				// 	$name=str_random(6).time().'.'.$file->getClientOriginalExtension();
				// 	$image=$file;
				// 	$image->move('public/uploads/product/',$name);

				//  	/* if ($i==1) {
				// 		$images =Image::make($file->getRealPath());    
				// 		$images->resize(200,200)->save(public_path('uploads/product/thumb/'.$name));
				//  	} */

				// 	DB::table('ps_images')->insert([
				// 		'name' => $name,
				// 		'ps_id' => $product->id,
				// 		'thumb' => NULL,
				// 		'created_at' => $date,
				// 		'updated_at' => $date	
				// 	]);

				//  }
				 
				Session::flash('message', 'Item Added Sucessfully!'); 

				return redirect()->to('/merchant/my-product-list');

		}

		else

		{

			/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			DB::table('product_service')

            ->where('id', $product_id)

            ->update(['category_id' =>Input::get('category_id'),

            	      'sub_category_id' => Input::get('sub_category_id'),	

            	      'type' =>Input::get('ps_type'),

            	      'name' =>Input::get('product_name'),

            	      'product_code' => $product_code,

					  'description' =>Input::get('product_desc'), 

					  'price'=>Input::get('product_price'),

					  'excise_tax'=>Input::get('excise_tax'),

					  'quantity'=>Input::get('product_quantity'),

					  'unit' =>  Input::get('unit'),

					  'brands' =>  Input::get('brands'),

					  'types' =>  Input::get('types'),

					  'potency_thc' =>  Input::get('potency_thc'),

					  'potency_cbd' =>  Input::get('potency_cbd'),  

					  'image'=>$pname,

					  'keyword'=>'',

					  'stock' =>  Input::get('stock'),

					  'status'=>Input::get('status'),

					  'updated_at'=>$date


					 ]);

               if(!empty($imageTlarg)){

				DB::table('ps_images')
				->where('ps_id', $product_id)
				->where('thumb', 1)
				->update([
				'name' => $imageTlarg,
				]);

			   }
		 
			   if($request->hasFile("product_image_gallery")) {
				   
				$files=$request->file('product_image_gallery');

				foreach($files as $file){

					$name=str_random(6).time().'.'.$file->getClientOriginalExtension();
					$image=$file;
					$image->move('public/uploads/product/',$name);

					DB::table('ps_images')->insert([
						'name' => $name,
						'ps_id' => $product_id,
						'thumb' => NULL,
						'created_at' => $date,
						'updated_at' => $date	
					]);

				 }		 		

               }

				session()->flash('message', 'Item Update Sucessfully!'); 

				return redirect()->to('/merchant/product-form/'.$product_id);

		}

	}


	public function admin_product_action(Request $request)
	{	

		$product_id = Input::get('add_admin_product');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$date = date('Y-m-d H:i:s');

		if(empty($product_id)){

        Session::flash('message', 'Please Select Item !'); 
        return redirect()->to('/merchant/add-admin-product');

		}else{

		foreach ($product_id as $key => $value) {

	    //$product_price = DB::table('product_service')->where('id', '=' ,$value)->value('price');	

       // $product_quantity = DB::table('product_service')->where('id', '=' ,$value)->value('quantity');

		$get_product = DB::table('product_service')->where('id', '=' ,$value)->first();	

		$get_product_imgs = DB::table('ps_images')->where('ps_id', '=' ,$value)->get();	

        $checkexist = DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->where('admin_product_id', '=' ,$value)->get();


        if(count($checkexist)==0){

		$getid = DB::table('product_service')->insertGetId(array(
                                                'vendor_id'      => $vendor_id,
                                                'admin_product_id'     => $get_product->id,
                                                'type'    => $get_product->type,
                                                'category_id' => $get_product->category_id,
                                                'sub_category_id'    => $get_product->sub_category_id,
                                                'avg_rating'    => $get_product->avg_rating,
                                                'rating_count'    => $get_product->rating_count,
                                                'name'    => $get_product->name,
                                                'slug'    => $get_product->slug,
                                                'description'    => $get_product->description,
                                                'price'    => $get_product->price,
                                                'quantity'    => $get_product->quantity,
                                                'unit'    => $get_product->unit,
                                                'brands'    => $get_product->brands,
                                                'types'    => $get_product->types,
                                                'potency_thc'    => $get_product->potency_thc,
                                                'potency_cbd'    => $get_product->potency_cbd,
                                                'image'    => $get_product->image,
                                                'keyword'    => $get_product->keyword,
                                                'product_code'    => $get_product->product_code,
                                                'status'    => $get_product->status,
                                                'login_status'    => $get_product->login_status,
                                                'stock'    => $get_product->stock,
                                                'popular'    => $get_product->popular,
                                                'created_at'    => $get_product->created_at,
                                                'updated_at'    => $get_product->updated_at
                                                ));

		if($getid){

            foreach ($get_product_imgs as $key => $get_product_img) {        	

		        DB::table('ps_images')->insertGetId(array(
                                                'ps_id'      => $getid,
                                                'name'     => $get_product_img->name,
                                                'thumb'    => $get_product_img->thumb,
                                                'created_at' => $get_product_img->created_at,
                                                'updated_at'    => $get_product_img->updated_at
                                                ));	

           }

		}

            Session::flash('message', 'Item Added Sucessfully!');    

	        }else{

            Session::flash('message', 'Item already exists!'); 

	       }

		 }

	    }

		

		return redirect()->to('/merchant/admin-product-list');


		/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			/* DB::table('admin_merchant_product')

            ->where('vendor_id', $vendor_id)

            ->where('product_id', $product_id)

            ->update(['product_price' =>Input::get('product_price'),

            	      'product_quantity' => Input::get('product_quantity'),	

					  'pupdated_at'=>$date

					 ]);

			session()->flash('message', 'Item Update Sucessfully!'); 

			return redirect()->to('/merchant/admin-product-list'); */

	}


	public function admin_event_action(Request $request)
	{	

		$product_id = Input::get('add_admin_event');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$date = date('Y-m-d H:i:s');

		if(empty($product_id)){

        Session::flash('message', 'Please Select Item !'); 
        return redirect()->to('/merchant/add-admin-event');

		}else{

		foreach ($product_id as $key => $value) {

	    //$product_price = DB::table('product_service')->where('id', '=' ,$value)->value('price');	

       // $product_quantity = DB::table('product_service')->where('id', '=' ,$value)->value('quantity');

		$get_product = DB::table('product_service')->where('id', '=' ,$value)->first();	

		$get_product_imgs = DB::table('ps_images')->where('ps_id', '=' ,$value)->get();	

        $checkexist = DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->where('admin_product_id', '=' ,$value)->get();


        if(count($checkexist)==0){

		$getid = DB::table('product_service')->insertGetId(array(
                                                'vendor_id'      => $vendor_id,
                                                'admin_product_id'     => $get_product->id,
                                                'type'    => $get_product->type,
                                                'category_id' => $get_product->category_id,
                                                'sub_category_id'    => $get_product->sub_category_id,
                                                'avg_rating'    => $get_product->avg_rating,
                                                'rating_count'    => $get_product->rating_count,
                                                'name'    => $get_product->name,
                                                'slug'    => $get_product->slug,
                                                'description'    => $get_product->description,
                                                'price'    => $get_product->price,
                                                'quantity'    => $get_product->quantity,
                                                'unit'    => $get_product->unit,
                                                'brands'    => $get_product->brands,
                                                'types'    => $get_product->types,
                                                'potency_thc'    => $get_product->potency_thc,
                                                'potency_cbd'    => $get_product->potency_cbd,
                                                'image'    => $get_product->image,
                                                'keyword'    => $get_product->keyword,
                                                'product_code'    => $get_product->product_code,
                                                'status'    => $get_product->status,
                                                'login_status'    => $get_product->login_status,
                                                'stock'    => $get_product->stock,
                                                'popular'    => $get_product->popular,
                                                'created_at'    => $get_product->created_at,
                                                'updated_at'    => $get_product->updated_at
                                                ));

		if($getid){

            foreach ($get_product_imgs as $key => $get_product_img) {        	

		        DB::table('ps_images')->insertGetId(array(
                                                'ps_id'      => $getid,
                                                'name'     => $get_product_img->name,
                                                'thumb'    => $get_product_img->thumb,
                                                'created_at' => $get_product_img->created_at,
                                                'updated_at'    => $get_product_img->updated_at
                                                ));	

           }

		}

            Session::flash('message', 'Item Added Sucessfully!');    

	        }else{

            Session::flash('message', 'Item already exists!'); 

	       }

		 }

	    }

		

		return redirect()->to('/merchant/admin-event-list');


		/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			/* DB::table('admin_merchant_product')

            ->where('vendor_id', $vendor_id)

            ->where('product_id', $product_id)

            ->update(['product_price' =>Input::get('product_price'),

            	      'product_quantity' => Input::get('product_quantity'),	

					  'pupdated_at'=>$date

					 ]);

			session()->flash('message', 'Item Update Sucessfully!'); 

			return redirect()->to('/merchant/admin-product-list'); */

	}


	public function admin_product_update(Request $request)
	{	

		$product_id = Input::get('product_id');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$date = date('Y-m-d H:i:s');

		if(empty($product_id)){

        Session::flash('message', 'Please Select Item !'); 
        return redirect()->to('/merchant/add-admin-product');

		}else{
  
		/*********  END  START MANAGE TIME IN OTHER TABLE ******/

         DB::table('product_service')

            ->where('vendor_id', $vendor_id)

            ->where('id', $product_id)

            ->update(['price' =>Input::get('product_price'),

            	      'quantity' => Input::get('product_quantity'),	

					  'updated_at'=>$date

					 ]);

			session()->flash('message', 'Item Update Sucessfully!'); 

			return redirect()->to('/merchant/admin-product-list'); 

	    }

		Session::flash('message', 'Item Added Sucessfully!'); 

		return redirect()->to('/merchant/admin-product-list');

	}

   public function admin_event_update(Request $request)
	{	

		$product_id = Input::get('product_id');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$date = date('Y-m-d H:i:s');

		if(empty($product_id)){

        Session::flash('message', 'Please Select Item !'); 
        return redirect()->to('/merchant/add-admin-event');

		}else{
  
		/*********  END  START MANAGE TIME IN OTHER TABLE ******/

         DB::table('product_service')

            ->where('vendor_id', $vendor_id)

            ->where('id', $product_id)

            ->update(['price' =>Input::get('product_price'),

            	      'quantity' => Input::get('product_quantity'),	

					  'updated_at'=>$date

					 ]);

			session()->flash('message', 'Item Update Sucessfully!'); 

			return redirect()->to('/merchant/admin-event-list'); 

	    }

		Session::flash('message', 'Item Added Sucessfully!'); 

		return redirect()->to('/merchant/admin-event-list');

	}

	public function service_action(Request $request)

	{	

		$product_id = Input::get('product_id');

		$product_old_img = Input::get('product_old_img');

		$date = date('Y-m-d H:i:s');

		$proname = Input::get('product_name');

		$product_code = Str::random(8);

		$slug = Str::slug($proname.' '.$product_code, '-');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

        $imageTlarg = '';

		    if ($request->product_id==0) {

		        $this->validate($request,[

		        	"product_image" => "required|image",

		        ],[

		        	"product_image.image" => "The product image must be an image.",

		        ]);

		        $this->validate($request,["product_image_gallery" => "required"]);  

		      }

                if ($request->hasFile("product_image")) {

                	$productImage=$request->product_image;

                	$image =$largeImage= Image::make($productImage->getRealPath());     

					$image->resize(200,200)->save(public_path('uploads/product/'.$pname=str_random(6).time().'.'.$productImage->getClientOriginalExtension()));

					$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(6).time().'.'.$productImage->getClientOriginalExtension());

                }else{

                 $pname = $product_old_img; 

                }

		if($product_id==0)

		{

				$product = new Productservice; 

				$product->vendor_id = $vendor_id;

				$product->type = Input::get('ps_type');	

				$product->category_id = Input::get('category_id');

				$product->sub_category_id = Input::get('sub_category_id');

				$product->name = Input::get('product_name');

				$product->slug = $slug;

				$product->description =  Input::get('product_desc');

				$product->price =  Input::get('product_price');

				$product->unit =  Input::get('unit');

				$product->image =  $pname;

				$product->keyword =  '';

				$product->product_code =  $product_code;

				$product->stock =  Input::get('stock');

				$product->status =  Input::get('status');

				$product->created_at = $date;

				$product->save();

				DB::table('ps_images')->insert([
				'name' => $imageTlarg,
				'ps_id' => $product->id,
				'thumb' => 1,
				'created_at' => $date,
				'updated_at' => $date	

				]);

				$files=$request->file('product_image_gallery');
				
				foreach($files as $file){

					$name=str_random(6).time().$file->getClientOriginalName();
					$image=$file;
					$image->move('public/uploads/product/',$name);

				 	/* if ($i==1) {
						$images =Image::make($file->getRealPath());    
						$images->resize(200,200)->save(public_path('uploads/product/thumb/'.$name));
				 	} */

					DB::table('ps_images')->insert([
						'name' => $name,
						'ps_id' => $product->id,
						'thumb' => NULL,
						'created_at' => $date,
						'updated_at' => $date	
					]);

				 	
				 }


				Session::flash('message', 'Service Added Sucessfully!'); 

				return redirect()->to('/merchant/service-list');

		}else{

			/*********  END  START MANAGE TIME IN OTHER TABLE ******/

			DB::table('product_service')

            ->where('id', $product_id)

            ->update(['category_id' =>Input::get('category_id'),

            	      'sub_category_id' => Input::get('sub_category_id'),

            	      'type' =>Input::get('ps_type'),

            	      'name' =>Input::get('product_name'),

					  'description' =>Input::get('product_desc'),

					  'price'=>Input::get('product_price'),

					  'unit' =>  Input::get('unit'),

					  'image'=>$pname,

					  'keyword'=>'',

					  'stock' =>  Input::get('stock'),

					  'status'=>Input::get('status'),

					  'updated_at'=>$date

					 ]);		


              if(!empty($imageTlarg)){

				DB::table('ps_images')
				->where('ps_id', $product_id)
				->where('thumb', 1)
				->update([
				'name' => $imageTlarg,
				]);

			   }
		 
			   if($request->hasFile("product_image_gallery")) {
				   
				$files=$request->file('product_image_gallery');

				foreach($files as $file){

					$name=str_random(6).time().$file->getClientOriginalName();
					$image=$file;
					$image->move('public/uploads/product/',$name);

					DB::table('ps_images')->insert([
						'name' => $name,
						'ps_id' => $product_id,
						'thumb' => NULL,
						'created_at' => $date,
						'updated_at' => $date	
					]);

				 }		 		

               }

				session()->flash('message', 'Service Update Sucessfully!'); 

				return redirect()->to('/merchant/service-list');

		}

	}



	public function product_service_delete($product_id)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		DB::table('product_service')

		->where('id', '=', $product_id)

		->where('vendor_id', '=', $vendor_id)

		->delete();

		DB::table('ps_images')->where('ps_id', '=', $product_id)->delete();

		DB::table('cart')->where('ps_id', '=', $product_id)->delete();

		//DB::table('orders_ps')->where('ps_id', '=', $product_id)->delete();

		Session::flash('message', 'Item deleted successfully!'); 

		return redirect()->to('/merchant/my-product-list');

	}


	public function admin_merchant_product_delete($product_id)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		DB::table('product_service')

		->where('id', '=', $product_id)

		->where('vendor_id', '=', $vendor_id)

		->delete();

		DB::table('cart')->where('ps_id', '=', $product_id)->delete();

		Session::flash('message', 'Item deleted successfully!'); 

		return redirect()->to('/merchant/admin-product-list');

	}

	public function admin_merchant_event_delete($product_id)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		DB::table('product_service')

		->where('id', '=', $product_id)

		->where('vendor_id', '=', $vendor_id)

		->delete();

		DB::table('cart')->where('ps_id', '=', $product_id)->delete();

		Session::flash('message', 'Item deleted successfully!'); 

		return redirect()->to('/merchant/admin-event-list');

	}

	public function service_delete($service_id)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		DB::table('product_service')

		->where('id', '=', $service_id)

		->where('vendor_id', '=', $vendor_id)

		->delete();

		DB::table('cart')->where('ps_id', '=', $service_id)->delete();

		Session::flash('message', 'Service Delete Sucessfully!'); 

		return redirect()->to('/merchant/service-list');

	}		

	public function product_gallery_delete($gl_id)
	{

	$getpsid =DB::table('ps_images')
		->where('id', '=', $gl_id)
		->value('ps_id');

		DB::table('ps_images')
		->where('id', '=', $gl_id)
		->delete();

		Session::flash('message', 'Image Delete Sucessfully!'); 

		return redirect()->to('/merchant/product-form/'.$getpsid);

	}

  public function service_gallery_delete($gl_id)
	{

	$getpsid =DB::table('ps_images')
		->where('id', '=', $gl_id)
		->value('ps_id');

		DB::table('ps_images')
		->where('id', '=', $gl_id)
		->delete();

		Session::flash('message', 'Image Delete Sucessfully!'); 

		return redirect()->to('/merchant/service-form/'.$getpsid);

	}


	public function random_string($length) {
	$password = '';
    $alphabets = range('A','Z');
    $final_array = array_merge($alphabets);
       while($length--) {

      $key = array_rand($final_array);

      $password .= $final_array[$key];
                        
      }
   if (preg_match('/[A-Za-z0-9]/', $password))
    {
     return $password;
    }else{
    return  random_string();
    }

    }



	public function view_restaurant($product_id)
	{

       DB::enableQueryLog();  

       $vendor_id = Auth::guard('vendor')->user()->vendor_id;

	   $product_id = Route::current()->getParameter('id');

	   $rest_list = DB::table('product_service')		

					->where('id', '=' ,$product_id)

					->where('vendor_id', '=' ,$vendor_id)

					->orderBy('id', 'desc')

					->get();	

			$data_onview = array('rest_detail' =>$rest_list,'id'=>$product_id);

			return View('rest_owner.restaurant_view')->with($data_onview);

	}



	public function view_service()



	{



       DB::enableQueryLog();  

       $vendor_id = Auth::guard('vendor')->user()->vendor_id;

	   $id = Route::current()->getParameter('id');



	   $rest_list = DB::table('product_service')		

					->where('id', '=' ,$id)

					->where('vendor_id', '=' ,$vendor_id)

					->orderBy('id', 'desc')

					->get();	



		if(!$rest_list)

		{

		

			$data_onview = array('rest_detail' =>$rest_list,'id'=>$id);

	

			return View('rest_owner.service_view')->with($data_onview);

		}

		else

		{

			return redirect()->to('/merchant/servie-list');

		}

			



	}



	public function ajax_update_sortorder()



	{



		//echo '<pre>';



		//print_r($_POST);



		foreach ($_POST['listItem'] as $position => $item)



		{



			//echo  "position = ".$position." ITEM ".$item.'<br>'; 



			



			



			DB::table('menu')



            ->where('restaurant_id', Input::get('rest_id'))



            ->where('menu_id', $item)



            ->update(['menu_order' =>($position+1)



					 ]);



					 



					 



		}







	}



	



	



	



	/* MANU FUNCTION */



	



	



	public function menu_list()



	{



		DB::connection()->enableQueryLog();	







		$menu_list = DB::table('menu')



					->leftJoin('restaurant', 'menu.restaurant_id', '=', 'restaurant.rest_id')



					->select('menu.*','restaurant.rest_name' )



					->orderBy('menu_order', 'asc')



					->get();			







		$data_onview = array('menu_list' =>$menu_list);



		return View('rest_owner.menu_list')->with($data_onview);



	}	



	



	



	public function ajax_menu_form()



	{



		



		$id =0;	



		



		if(Input::get('menu'))



		{



			$rest_id =  Input::get('rest');	



			$menu_id =  Input::get('menu');	



			$id = Input::get('menu');



			$menu_detail  = DB::table('menu')->where('menu_id', '=' ,$menu_id)->get();	



			



			 $data_onview = array('rest_id' =>$rest_id,



								  'menu_detail' =>$menu_detail,



								  'id'=>$id);



		}



		else



		{



			$rest_id =  Input::get('rest_id');



			



			



			$id =0;



  		  	 $data_onview = array('rest_id' =>$rest_id,



								  'menu_detail' =>'',



								  'id'=>$id);



		}



		



		return View('rest_owner.ajax.menu_form')->with($data_onview);



	}



	



	public function menu_form()



	{



		$rest_detail  = DB::table('restaurant')->get();		



	



		if(Route::current()->getParameter('id'))



		{



			$id = Route::current()->getParameter('id');				



			$menu_detail  = DB::table('menu')->where('menu_id', '=' ,$id)->get();					



  		  	$data_onview = array('rest_detail' =>$rest_detail,



								'menu_detail'=>$menu_detail,



								'id'=>$id); 		



		}



		else



		{



			$id =0;



  		  	$data_onview = array('rest_detail' =>$rest_detail,



								 'id'=>$id); 



		}	



		return View('rest_owner.menu_form')->with($data_onview);



	}	



	



	public function menu_action(Request $request)



	{	  



		



		/*echo'<pre>';



		print_r($_POST);

		//print_r($_FILES);



		exit;*/



		if(Input::get('from')=='back'){



			



								



				$rest_id = 	Input::get('restaurant_id');







					



									



					$rest_list = DB::table('restaurant')		



								->where('rest_id', '=' ,$rest_id)



								->orderBy('restaurant.rest_id', 'desc')



								->get();	



													



					



					



					$menu_list = DB::table('menu')		



					->where('restaurant_id', '=' ,Input::get('restaurant_id'))



					->orderBy('menu_order', 'asc')



					->get();	



					



					$menu_id = $menu_list[0]->menu_id;



					



					$menu_detail = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->where('menu_id', '=' ,$menu_id)



								->orderBy('menu_order', 'asc')



								->get();	



								



					



					



							



					$menu_cat_detail = '';



					



					if(!empty($menu_list)){  			



					$menu_cat_detail = DB::table('menu_category')		



							->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



							->where('menu_category.rest_id', '=' ,$rest_id)



							->where('menu_category.menu_id', '=' ,$menu_id)



							->select('menu_category.*','menu_category_item.*')



							->orderBy('menu_category.menu_category_id', 'asc')



							->get();	



					}					



						



					$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



									



					return View('rest_owner.ajax.menu_list')->with($data_onview);



					



					



			



		}



		else



		{



		



			



			



			$menu_id = Input::get('menu_id');

			

			$new_fileName = '';

			

			

			



	/*****RESTAURANT IMAGE ***/	



		if(isset($_FILES['menu_image']) && (!empty($_FILES['menu_image']['name'][0]))){



		



			



				if(!empty($menu_image_old)){



				



					 $file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/menu/'.$menu_image_old;

			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;



			



					if(file_exists($file_remove)){	



					 unlink($file_remove);



					}











				}



		



		



		        $destinationPath = 'uploads/menu/';



				$image = $request->file('menu_image');



				$extension 		= 	$image->getClientOriginalExtension();



    			$imageRealPath 	= 	$image->getRealPath();



    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();



			



			



				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))



				{



					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();



					



					$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/menu/'.$thumbName);; 



					asset($destinationPath . $thumbName);



					



					$new_fileName = $thumbName;



				}



				else



				{



					Session::flash('message_error', 'Image type is invalide!'); 



					



				/*	if($rest_id>0){



					return redirect()->to('/admin/restaurant-form/'.$rest_id);



					}



					else



					{



						return redirect()->to('/admin/vendor-rest-form/'.Input::get('vendor_id'));



					}*/



				}



				



		}



		elseif(!empty($menu_image_old))

		{



			 $new_fileName = $menu_image_old;



		}

 ///****** END **//



			



			



			



			if($menu_id==0)



			{



			



			



			



				$rest_id =  Input::get('restaurant_id');



				



						



					$menu_max_value  = DB::table('menu')	



						->where('restaurant_id', '=' ,$rest_id)



						->select(\DB::raw('MAX(menu.menu_order) as max_order'))



						->get();	



					$max_order_no = $menu_max_value[0]->max_order; 



				



				



					



					$menu = new Menu;



					$menu->restaurant_id = Input::get('restaurant_id');



					$menu->menu_name =  Input::get('menu_name');



					$menu->menu_desc =  Input::get('menu_desc');



					$menu->menu_status =  Input::get('menu_status');



					$menu->menu_order =  ($max_order_no+1);

					

					$menu->menu_image = $new_fileName;



					$menu->save();



					$menu_id = $menu->menu_id;



					



					Session::flash('menu_message', 'Restaurant Menu Inserted Sucessfully!'); 



					//return redirect()->to('/admin/menu_list');



					



					



					 $rest_list = DB::table('restaurant')		



						->where('rest_id', '=' ,$rest_id)



						->orderBy('restaurant.rest_id', 'desc')



						->get();	



											



			$menu_detail = DB::table('menu')		



						->where('restaurant_id', '=' ,$rest_id)



						->where('menu_id', '=' ,$menu_id)



						->orderBy('menu_order', 'asc')



						->get();	



						



						



					



					 $menu_list = DB::table('menu')		



						->where('restaurant_id', '=' ,Input::get('restaurant_id'))



						->orderBy('menu_order', 'asc')



						->get();	



						



						



								



			  $menu_cat_detail = '';



			  



			if(!empty($menu_list)){  			



			$menu_cat_detail = DB::table('menu_category')		



						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



						->where('menu_category.rest_id', '=' ,$rest_id)



						->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)



						->select('menu_category.*','menu_category_item.*')



						->orderBy('menu_category.menu_category_id', 'asc')



						->get();	



			}					



						



			



			



			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);		



						



			



			



				   //  $data_onview = array('menu_list'=>$menu_list,'res_id'=>$res_id);



			



					if(Input::get('from')=='submit'){



						return View('rest_owner.ajax.menu_list')->with($data_onview);



					}



					elseif(Input::get('from')=='addnext'){				



						return View('rest_owner.ajax.menu_list_form')->with($data_onview);



					}



				



				



			}



			else



			{



								



				$rest_id = 	Input::get('restaurant_id');



				if(Input::get('from')=='update'){



					



					DB::table('menu')



						->where('menu_id', $menu_id)



						->update(['restaurant_id' =>Input::get('restaurant_id'),



						  'menu_name'=>Input::get('menu_name'),



						  'menu_desc'=>Input::get('menu_desc'),



						  'menu_image'=>$new_fileName,



						  'menu_status'=>Input::get('menu_status')



						 ]);



						 



					



						session()->flash('menu_message', 'Restaurant Menu Updated Sucessfully!');



					}



					



									



					$rest_list = DB::table('restaurant')		



								->where('rest_id', '=' ,$rest_id)



								->orderBy('restaurant.rest_id', 'desc')



								->get();	



													



					$menu_detail = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->where('menu_id', '=' ,$menu_id)



								->orderBy('menu_order', 'asc')



								->get();	



								



					



					



					$menu_list = DB::table('menu')		



					->where('restaurant_id', '=' ,Input::get('restaurant_id'))



					->orderBy('menu_order', 'asc')



					->get();	



					



					



							



					$menu_cat_detail = '';



					



					if(!empty($menu_list)){  			



					$menu_cat_detail = DB::table('menu_category')		



							->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



							->where('menu_category.rest_id', '=' ,$rest_id)



							->where('menu_category.menu_id', '=' ,$menu_id)



							->select('menu_category.*','menu_category_item.*')



							->orderBy('menu_category.menu_category_id', 'asc')



							->get();	



					}					



						



					$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



									



					return View('rest_owner.ajax.menu_list')->with($data_onview);



					



					



			}



		}



	}



	



	public function menu_delete($menu_id)



	{



		$menu_cat =  DB::table('menu_category')->where('menu_id', '=' ,$menu_id)->get();



		if(!empty($menu_cat))



		{



			DB::table('menu_category')->where('menu_id', '=', $menu_id)->delete();



		}	



	



		DB::table('menu')->where('menu_id', '=', $menu_id)->delete();



		return Redirect('/admin/menu_list');



	}



	



	



	/* MENU CATEGORY FUNCTIONALITY */



	



	public function menu_category_list()



	{



		DB::connection()->enableQueryLog();	







		$menu_cat_list = DB::table('menu_category')		



					->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')



					->leftJoin('restaurant', 'menu_category.rest_id', '=', 'restaurant.rest_id')



					->select('menu_category.*','restaurant.rest_name','menu.menu_name' )



					->orderBy('menu.menu_id', 'desc')



					->get();			







		$data_onview = array('category_list' =>$menu_cat_list);



		return View('rest_owner.menu_category_list')->with($data_onview);



	}



	



	



	public function ajax_menu_item_form()

	{



	



		$rest_id =  Input::get('rest_id');	



		$menu_id =  Input::get('menu_id');	



		$id =0;



		



		$menu_detail = DB::table('menu')		



					->where('menu_id', '=' ,$menu_id)



					->get();	



					



					



  		 $data_onview = array('rest_id' =>$rest_id,



		 					  'menu_id' =>$menu_id,



		 					  'menu_name' =>$menu_detail[0]->menu_name,



							  'id'=>$id); 



		return View('rest_owner.ajax.menu_form_item')->with($data_onview);



		



	}



	public function ajax_update_menu_category()



	{



		//echo '<pre>';



		//print_r($_POST);



		



		$rest_id =Input::get('rest'); 



		$menu_id=Input::get('menu');



		$menu_cate_id = Input::get('menu_itme');



		



		



		$menu_detail = DB::table('menu')->where('menu_id', '=' ,$menu_id)->get();



		



		$cate_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$menu_cate_id)->get();



		



		$menu_cat_addon = '';



		



		if($cate_detail[0]->menu_category_portion=='yes'){



			$menu_cat_addon =  DB::table('menu_category_item')->where('menu_category', '=' ,$menu_cate_id)->get();	



		} 



		



		 $data_onview = array('rest_id' =>$rest_id,



		 					  'menu_id' =>$menu_id,



		 					  'menu_name' =>$menu_detail[0]->menu_name,



							  'cate_detail'=>$cate_detail,



							  "menu_cat_addon"=>$menu_cat_addon,



							  'id'=>$menu_cate_id); 



		



		return View('rest_owner.ajax.update_menu_form_item')->with($data_onview);



	}



	



	



	



	



	public function menu_category_form()



	{



		$rest_detail  = DB::table('restaurant')->get();		



	



		if(Route::current()->getParameter('id'))



		{



			$id = Route::current()->getParameter('id');				



			$cate_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$id)->get();		



			$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$cate_detail[0]->rest_id)->get();				



  		  	$data_onview = array('rest_detail'=>$rest_detail,



								'menu_detail'=>$menu_detail,



								'cate_detail'=>$cate_detail,



								'id'=>$id); 		



		}



		else



		{



			$id =0;



  		  	$data_onview = array('rest_detail'=>$rest_detail,



								'menu_detail'=>'',



								 'id'=>$id); 



		}	



		return View('rest_owner.menu_category_form')->with($data_onview);



	}



	



	



	public function menu_category_action(Request $request)



	{	  



		



		$menu_category_id = Input::get('menu_category_id');



		



		if(Input::get('from')=='back'){



		



			



					$menu_id = Input::get('menu_id');



					$rest_id = Input::get('restaurant_id');



					



					$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



					



					$menu_list = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->orderBy('menu_order', 'asc')



								->get();



									



					$menu_detail = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->where('menu_id', '=' ,$menu_id)



								->orderBy('menu_id', 'desc')



								->get();	

										



					  $menu_cat_detail = '';



					  



					if(!empty($menu_list)){  			



		  $menu_cat_detail = DB::table('menu_category')		



						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



						->where('menu_category.rest_id', '=' ,$rest_id)



						->where('menu_category.menu_id', '=' ,$menu_id)



						->select('menu_category.*','menu_category_item.*')



						->orderBy('menu_category.menu_category_id', 'asc')



						->get();	



			}		



				



			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



			return View('rest_owner.ajax.menu_list')->with($data_onview);



		}



		else



		{		



			if($menu_category_id==0)



			{



			    $restid = Input::get('restaurant_id');



			    if(isset($_FILES['menu_image']) && (!empty($_FILES['menu_image']['name'][0]))){



			   	$destinationPath = 'uploads/reataurant/menu/';

			   	$image = $request->file('menu_image');

			   	$extension 		= 	$image->getClientOriginalExtension();

			   	$imageRealPath 	= 	$image->getRealPath();

		    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();

			   	//if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

			   	//{

			   		$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();

			   		$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/menu/'.$thumbName);;

			   		asset($destinationPath . $thumbName);

			   		$new_fileName = $thumbName;

			   //	}

	

			    }else{ $new_fileName = "menu.jpg";} 	



					$menu = new Menu_category;



					$menu->rest_id = Input::get('restaurant_id');



					$menu->menu_id =  Input::get('menu_id');



					$menu->menu_category_name =  Input::get('menu_name');



					$menu->menu_category_desc =  Input::get('menu_desc');



					$menu->menu_cat_popular =  Input::get('menu_cat_popular');



					$menu->menu_cat_diet =  Input::get('menu_cat_diet');



					$menu->menu_category_price =  Input::get('menu_price');



					$menu->menu_category_image =  $new_fileName;



					$menu->menu_category_portion = Input::get('diff_size');



					$menu->menu_cat_status = Input::get('menu_cat_status');



					$menu->save();



					$menu_category_id = $menu->menu_category_id;



					



					if(Input::get('diff_size')=='yes')



					{

						

						for($c=0;$c<count($_POST['item_id']);$c++)

						{	

						

							if((!empty(trim($_POST['item_title'][$c]))))

							{

								$menu_sub_itme = new  Category_item;

															

								$menu_sub_itme->rest_id	 =Input::get('restaurant_id');

								$menu_sub_itme->menu_id	 = Input::get('menu_id');

								$menu_sub_itme->menu_category	 = $menu_category_id;

								$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];

								$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];

								$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];

								$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');

		

								$menu_sub_itme->save();

							}

						}



					

						

//category_item

						/*$menu_addon = new Menu_category_item;



						$menu_addon->rest_id = Input::get('restaurant_id');



						$menu_addon->menu_id =  Input::get('menu_id');



						$menu_addon->menu_category =  $menu_category_id;



						$menu_addon->menu_large_title =  Input::get('l_title');



						$menu_addon->menu_medium_title =  Input::get('m_title');



						$menu_addon->menu_small_title =  Input::get('s_title');



						$menu_addon->menu_large_price =  Input::get('l_price');



						$menu_addon->menu_medium_price =  Input::get('m_price');



						$menu_addon->menu_small_price =  Input::get('s_price');



						



						



							$menu_addon->menu_lstatus =  Input::get('l_status');



							$menu_addon->menu_mstatus =  Input::get('m_status');



							$menu_addon->menu_sstatus =  Input::get('s_status');



							



						$menu_addon->save();



						$menu_cat_itm_id = $menu_addon->id;*/



					



					}



					



					



					



					



					Session::flash('menu_message', 'Menu Item Inserted Sucessfully!'); 



					//return redirect()->to('/admin/menu_categorylist');



					



					$rest_id = Input::get('restaurant_id');



			



					



					/* START */



					$menu_id = Input::get('menu_id');



					$rest_id = Input::get('restaurant_id');



					



					$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



					



					$menu_list = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->orderBy('menu_order', 'asc')



								->get();



									



					$menu_detail = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->where('menu_id', '=' ,$menu_id)



								->orderBy('menu_id', 'desc')



								->get();	



								



								



										



					  $menu_cat_detail = '';



					  



					if(!empty($menu_list)){  			



		  $menu_cat_detail = DB::table('menu_category')		



						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



						->where('menu_category.rest_id', '=' ,$rest_id)



						->where('menu_category.menu_id', '=' ,$menu_id)



						->select('menu_category.*','menu_category_item.*')



						->orderBy('menu_category.menu_category_id', 'asc')



						->get();	



			}					







					/* END */





					//if(Input::get('from')=='submit'){

	



			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



						return View('rest_owner.ajax.menu_list')->with($data_onview);



					/* }



					elseif(Input::get('from')=='addnext'){	



					



			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail,'id'=>'0');				



						return View('rest_owner.ajax.menu_item_list_form')->with($data_onview);



					} */



				



			}



			else



			{





				//if(Input::get('from')=='update'){		



				if(isset($_FILES['menu_image']) && (!empty($_FILES['menu_image']['name'][0]))){



			   	$destinationPath = 'uploads/reataurant/menu/';

			   	$image = $request->file('menu_image');

			   	$extension 		= 	$image->getClientOriginalExtension();

			   	$imageRealPath 	= 	$image->getRealPath();

		    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();

			   	//if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

			   	//{

			   		$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();

			   		$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/menu/'.$thumbName);;

			   		asset($destinationPath . $thumbName);

			   		$new_fileName = $thumbName;

			   //	}

	

			    }else{ $new_fileName = "";} 



                if($new_fileName==''){



				DB::table('menu_category')



				->where('menu_category_id', $menu_category_id)



				->update(['menu_category_name'=>Input::get('menu_name'),



						  'menu_category_price' =>  Input::get('menu_price'),



						  'menu_category_desc'=>Input::get('menu_desc'),



						  'menu_category_portion'=>Input::get('diff_size'),



						  'menu_cat_status'=>Input::get('menu_cat_status'),



						  'menu_cat_popular'=> Input::get('menu_cat_popular'),



						  'menu_cat_diet'=> Input::get('menu_cat_diet')



						 ]);



                

					}else{



                    	DB::table('menu_category')

						->where('menu_category_id', $menu_category_id)

						->update(['menu_category_name'=>Input::get('menu_name'),

							'menu_category_price' =>  Input::get('menu_price'),

							'menu_category_image' =>  $new_fileName,

							'menu_category_desc'=>Input::get('menu_desc'),

							'menu_category_portion'=>Input::get('diff_size'),

							'menu_cat_status'=>Input::get('menu_cat_status'),

							'menu_cat_popular'=> Input::get('menu_cat_popular'),

							'menu_cat_diet'=> Input::get('menu_cat_diet')

						]);



					}

						 



					if(Input::get('diff_size')=='yes')



					{

					

					

						for($c=0;$c<count($_POST['item_id']);$c++)

						{	

						

						

							if($_POST['item_id'][$c]==0)

							{	

								if((!empty(trim($_POST['item_title'][$c]))))

								{					

									$menu_sub_itme = new  Category_item;															

									$menu_sub_itme->rest_id	 =Input::get('restaurant_id');

									$menu_sub_itme->menu_id	 = Input::get('menu_id');

									$menu_sub_itme->menu_category	 = $menu_category_id;

									$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];

									$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];

									$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];

									$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');		

									$menu_sub_itme->save();

								}

							}

							else

							{

							

								DB::table('category_item')

							->where('menu_cat_itm_id',$_POST['item_id'][$c])

							->update(['menu_item_title'=>$_POST['item_title'][$c],

									  'menu_item_price' =>$_POST['item_price'][$c],

									  'menu_item_status'=>$_POST['item_status'][$c]

									 ]);

								

							}

						}





					

/*

						$addon_list  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_category_id)->get();	



						if(empty($addon_list)){



							$menu_addon = new Menu_category_item;



							$menu_addon->rest_id = Input::get('restaurant_id');



							$menu_addon->menu_id =  Input::get('menu_id');



							$menu_addon->menu_category =  $menu_category_id;



							$menu_addon->menu_large_price =  Input::get('l_price');



							$menu_addon->menu_medium_price =  Input::get('m_price');



							$menu_addon->menu_small_price =  Input::get('s_price');



							



							$menu_addon->menu_large_title =  Input::get('l_title');



							$menu_addon->menu_medium_title =  Input::get('m_title');



							$menu_addon->menu_small_title =  Input::get('s_title');



							



							



							$menu_addon->menu_lstatus =  Input::get('l_status');



							$menu_addon->menu_mstatus =  Input::get('m_status');



							$menu_addon->menu_sstatus =  Input::get('s_status');



							



							



							$menu_addon->save();



							$menu_cat_itm_id = $menu_addon->id;



						}



						else



						{



						



							DB::table('menu_category_item')



							->where('menu_category', $menu_category_id)



							->update(['menu_large_price'=>Input::get('l_price'),



									  'menu_medium_price' =>  Input::get('m_price'),



									  'menu_small_price'=>Input::get('s_price'),



									  'menu_large_title'=>Input::get('l_title'),



									  'menu_medium_title'=>Input::get('m_title'),



									  'menu_small_title'=>Input::get('s_title'),



									  'menu_lstatus'=> Input::get('l_status'),



									  'menu_mstatus'=> Input::get('m_status'),



									  'menu_sstatus'=> Input::get('s_status')



									 ]);



						



						}*/



					



					}	 



					elseif(Input::get('diff_size')=='no')



					{



						$addon_list  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_category_id)->get();	



						if(!empty($addon_list))



						{



							DB::table('menu_category_item')->where('menu_category', '=', $menu_category_id)->delete();



						}



						



					}



											 



					session()->flash('menu_message', 'Restaurant Menu Item Updated Sucessfully!'); 	



						 



				//}



				



					$menu_id = Input::get('menu_id');



					$rest_id = Input::get('restaurant_id');



					



					$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



					



					$menu_list = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->orderBy('menu_order', 'asc')



								->get();



									



					$menu_detail = DB::table('menu')		



								->where('restaurant_id', '=' ,$rest_id)



								->where('menu_id', '=' ,$menu_id)



								->orderBy('menu_id', 'desc')



								->get();	



								



								



										



					  $menu_cat_detail = '';



					  



					if(!empty($menu_list)){  			



		  $menu_cat_detail = DB::table('menu_category')		



						->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



						->where('menu_category.rest_id', '=' ,$rest_id)



						->where('menu_category.menu_id', '=' ,$menu_id)



						->select('menu_category.*','menu_category_item.*')



						->orderBy('menu_category.menu_category_id', 'asc')



						->get();	



			}		



				



			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



						return View('rest_owner.ajax.menu_list')->with($data_onview);



				



				



				



				



					



			}



		}



	}



	



	public function get_menulist()	



	{



		  $rest_id =Input::get('restaurant_id');



		



		



			$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();	



		$menu_select = '';



		



	



		if(!empty($menu_detail)){



		



		$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >



												<option value="">Select Restaurant </option>';



		



		foreach($menu_detail as $menu)



		{



			$menu_select .='<option value="'.$menu->menu_id.'">'.$menu->menu_name.'</option>';



		}



		



		$menu_select .='</select> ';



											



		}



		else



		{



			$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >



												<option value="">Select Restaurant </option>	



											</select> ';



		}



	



		echo $menu_select ;	



		



	}



	



	



	



	



	/* MANAGE MENU CATEGORY ITEM FUNCTIONALITY  */



	



	



	public function get_categorylist()	



	{



		  $rest_id =Input::get('restaurant_id');



		  $menu_id =Input::get('menu_id');



		



		



			$cate_detail  = DB::table('menu_category')



							->where('rest_id', '=' ,$rest_id)



							->where('menu_id', '=' ,$menu_id)



							->get();	



		$cate_select = '';



		



	



		if(!empty($cate_detail)){



		



		$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >



												<option value="">Select Menu Category </option>';



		



		foreach($cate_detail as $menu)



		{



			$cate_select .='<option value="'.$menu->menu_category_id.'">'.$menu->menu_category_name.'</option>';



		} 



		



		$cate_select .='</select> ';



											



		}



		else



		{



			$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >



												<option value="">Select Menu Category  </option>	



											</select> ';



		}



	



		echo $cate_select ;	



		



	}



	



	



	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS START */



	public function ajax_addons_list()



	{



		//echo '<pre>';



		//print_r($_POST);



		



		$addon_list = '';



		



		$rest_id = Input::get('rest');



		$menu_id =Input::get('menu');



		$menu_item = Input::get('menu_itme');



		



		



			



	 $addon_list = DB::table('menu_category_addon')		



					->where('addon_restid', '=' ,$rest_id)



					->where('addon_menuid', '=' ,$menu_id)



					->where('addon_menucatid', '=' ,$menu_item)



					->orderBy('addon_id', 'asc')



					->get();



					



								



		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



		



		$menu_list = DB::table('menu')



					->where('restaurant_id', '=' ,$rest_id)



					->where('menu_id', '=' ,$menu_id)



					->get();



						



		$item_detil = DB::table('menu_category')		



					->where('rest_id', '=' ,$rest_id)



					->where('menu_category_id', '=' ,$menu_item)



					->where('menu_id', '=' ,$menu_id)



					->get();



							



							



		



		$rest_name = $rest_list[0]->rest_name;



		$menu_name = $menu_list[0]->menu_name;



		$item_name = $item_detil[0]->menu_category_name;



		



		



			$data_onview = array('addon_list'=>$addon_list,



								 'rest_name'=>$rest_name,



								 'menu_name'=>$menu_name,



								 'item_name'=>$item_name,



								 'rest_id'=>$rest_id,



								 'menu_id'=>$menu_id,



								 'menu_item'=>$menu_item,



								 ); 



		



		return View('rest_owner.ajax.addon_list')->with($data_onview);;



	}


	public function ajax_addons_form()



	{



		



		//print_r($_POST);	



		$id =0;



		



		$addon_detail = '';



		



		



		if(Input::get('addon_id'))



		{



			$addon_id = Input::get('addon_id');



			$id = Input::get('addon_id');



			$addon_detail  = DB::table('menu_category_addon')->where('addon_id', '=' ,$addon_id)->get();	 



		}



		



		



		



		$addon_list = '';



		



		$rest_id = Input::get('rest');



		$menu_id =Input::get('menu');



		$menu_item = Input::get('menu_itme');



		



		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



		



		$menu_list = DB::table('menu')



					->where('restaurant_id', '=' ,$rest_id)



					->where('menu_id', '=' ,$menu_id)



					->get();



						



		$item_detil = DB::table('menu_category')		



					->where('rest_id', '=' ,$rest_id)



					->where('menu_category_id', '=' ,$menu_item)



					->where('menu_id', '=' ,$menu_id)



					->get();



							



							



		



		$rest_name = $rest_list[0]->rest_name;



		$menu_name = $menu_list[0]->menu_name;



		$item_name = $item_detil[0]->menu_category_name;



			



		



		



  	 $data_onview = array('addon_detail'=>$addon_detail,



								 'rest_name'=>$rest_name,



								 'menu_name'=>$menu_name,



								 'item_name'=>$item_name,



								 'rest_id'=>$rest_id,



								 'menu_id'=>$menu_id,



								 'rest_menu'=>$menu_item,



								 'id'=>$id



								 ); 



		return View('rest_owner.ajax.addon_form')->with($data_onview);



	}



	



	public function ajax_addons_action(Request $request)



	{



			



		/*echo '<pre>';



		print_r($_POST);



		exit;*/



	



		$addon_id = Input::get('addon_id');



  $def_addon = '0';



  	if(isset($_POST['addon_default']))

	{

		$def_addon = '1';

	}



  



		



		if($addon_id==0)



		{



		



				



				$addon = new Menu_category_addon;



				$addon->addon_restid = Input::get('addon_restid');



				$addon->addon_menuid =Input::get('addon_menuid');



				$addon->addon_menucatid = Input::get('addon_menucatid');



				$addon->addon_name = trim(Input::get('addon_name'));



				$addon->addon_price =  Input::get('addon_price');



				$addon->addon_status =  Input::get('addon_status');



				$addon->addon_groupname = strtoupper(trim(Input::get('addon_groupname')));



				$addon->addon_option =  Input::get('addon_option');

				$addon->addon_default =  $def_addon;



				



				$addon->save();



				$addon_id = $addon->addon_id;



				



				Session::flash('addon_message', 'Restaurant Addon Inserted Sucessfully!'); 



				



				$rest_id =  Input::get('service_restid');



				



				



				



				



				



				/* FEATCH DATA FROM TABLE FOR SHOW */



				



				$addon_list = '';



		



		$rest_id = Input::get('addon_restid');



		$menu_id = Input::get('addon_menuid');



		$menu_item = Input::get('addon_menucatid');



		



		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



		



		$menu_list = DB::table('menu')



					->where('restaurant_id', '=' ,$rest_id)



					->where('menu_id', '=' ,$menu_id)



					->get();



						



		$item_detil = DB::table('menu_category')		



					->where('rest_id', '=' ,$rest_id)



					->where('menu_category_id', '=' ,$menu_item)



					->where('menu_id', '=' ,$menu_id)



					->get();



		$rest_name = $rest_list[0]->rest_name;



		$menu_name = $menu_list[0]->menu_name;



		$item_name = $item_detil[0]->menu_category_name;




				/* END */



				if(Input::get('from')=='submit'){


	 			 $addon_list = DB::table('menu_category_addon')		



								->where('addon_restid', '=' ,$rest_id)



								->where('addon_menuid', '=' ,$menu_id)



								->where('addon_menucatid', '=' ,$menu_item)



								->orderBy('addon_id', 'asc')



								->get();



					



					$data_onview = array('addon_list'=>$addon_list,



								 'rest_name'=>$rest_name,



								 'menu_name'=>$menu_name,



								 'item_name'=>$item_name,



								 'rest_id'=>$rest_id,



								 'menu_id'=>$menu_id,



								 'menu_item'=>$menu_item



								 );	



								 	



					



					return View('rest_owner.ajax.addon_list')->with($data_onview);



				}



				elseif(Input::get('from')=='addnext'){		



					



					$id = 0;



					$addon_detail = '';



					 $data_onview = array('addon_detail'=>$addon_detail,



								 'rest_name'=>$rest_name,



								 'menu_name'=>$menu_name,



								 'item_name'=>$item_name,



								 'rest_id'=>$rest_id,



								 'menu_id'=>$menu_id,



								 'menu_item'=>$menu_item,



								 'id'=>$id



								 );	



					return View('rest_owner.ajax.addon_form')->with($data_onview);



				}



				



				



			



		}



		else



		{



		



				$rest_id = Input::get('addon_restid');



				$menu_id = Input::get('addon_menuid');



				$menu_item = Input::get('addon_menucatid');



				



				$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



				



				$menu_list = DB::table('menu')



							->where('restaurant_id', '=' ,$rest_id)



							->where('menu_id', '=' ,$menu_id)



							->get();



								



				$item_detil = DB::table('menu_category')		



							->where('rest_id', '=' ,$rest_id)



							->where('menu_category_id', '=' ,$menu_item)



							->where('menu_id', '=' ,$menu_id)



							->get();



									



									



				



				$rest_name = $rest_list[0]->rest_name;



				$menu_name = $menu_list[0]->menu_name;



				$item_name = $item_detil[0]->menu_category_name;



				



			if(Input::get('from')=='update'){



			



				DB::table('menu_category_addon')



					->where('addon_id', $addon_id)



					->update(['addon_name' =>trim(Input::get('addon_name')),



							  'addon_price'=>Input::get('addon_price'),



							  'addon_status'=>Input::get('addon_status'),



							  'addon_groupname'=>strtoupper(trim(Input::get('addon_groupname'))),



							  'addon_option'=>  Input::get('addon_option'),

							  'addon_default'=>  $def_addon





							 ]);



					 



				



				Session::flash('addon_message', 'Restaurant Addon Update Sucessfully!'); 



				



			}



			



			/* FEATCH DATA FROM TABLE FOR SHOW */



				



				 $addon_list = DB::table('menu_category_addon')		



								->where('addon_restid', '=' ,$rest_id)



								->where('addon_menuid', '=' ,$menu_id)



								->where('addon_menucatid', '=' ,$menu_item)



								->orderBy('addon_id', 'asc')



								->get();



		



					



					$data_onview = array('addon_list'=>$addon_list,



								 'rest_name'=>$rest_name,



								 'menu_name'=>$menu_name,



								 'item_name'=>$item_name,



								 'rest_id'=>$rest_id,



								 'menu_id'=>$menu_id,



								 'menu_item'=>$menu_item



								 );	



				



					return View('rest_owner.ajax.addon_list')->with($data_onview);



			



		}



	



	}



	



	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS END */



	



	



	



	



	



	



	



	



	



	



 



	



	 



	



	/* AJAX OTHER FUNCTION */



	



	public function ajax_show_menudetail()



	{



		//echo '<pre>';



		//print_r($_POST);



		



		$menu_id = Input::get('menu');



		$rest_id = Input::get('rest');



		



		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



		



		$menu_list = DB::table('menu')		



					->where('restaurant_id', '=' ,$rest_id)



					->orderBy('menu_order', 'asc')



					->get();



						



		$menu_detail = DB::table('menu')		



					->where('restaurant_id', '=' ,$rest_id)



					->where('menu_id', '=' ,$menu_id)



					->orderBy('menu_id', 'desc')



					->get();	



					



					



							



		  $menu_cat_detail = '';



		  



		if(!empty($menu_list)){  			



	  $menu_cat_detail = DB::table('menu_category')		



					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



					->where('menu_category.rest_id', '=' ,$rest_id)



					->where('menu_category.menu_id', '=' ,$menu_id)



					->select('menu_category.*','menu_category_item.*')



					->orderBy('menu_category.menu_category_id', 'asc')



					->get();	



		}					



					











		$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);		



					











		       //  $data_onview = array('menu_list'=>$menu_list,'res_id'=>$res_id);



		



					return View('rest_owner.ajax.menu_list')->with($data_onview);



	}



	



	



	



	



	



	



	/* SERVICE AREA TABE START */





	



	/* SERVICE AREA TABE END */



	



	



	



	/* PROMOSTIONS TABE START */



	



	



	public function ajax_promo_form()



	{



		



		$rest_id =  Input::get('rest_id');	



		$id =0;



		



		$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	

		

		$rest_menu	 = DB::table('menu_category')	

							->select('menu_category.*')	

							->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')	

							->where('menu.menu_status', '=' , '1')	

							->where('menu_category.rest_id', '=' ,$rest_id)	

							->where('menu_category.menu_cat_status', '=' ,'1')

							->orderBy('menu_category.menu_category_id', 'desc')

							->get();

							

							



		$promo_detail = '';



		



		if(Input::get('promo_id'))

		{



			$promo_id = Input::get('promo_id');



			$id = Input::get('promo_id');



			$promo_detail  = DB::table('promotion')->where('promo_id', '=' ,$promo_id)->get();	 



		}



  		 $data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'promo_detail'=>$promo_detail,'id'=>$id,

		 'rest_menu'=>$rest_menu); 



		return View('rest_owner.ajax.promo_form')->with($data_onview);



	}



	



	public function promo_action(Request $request)



	{	



	

		$promo_id = Input::get('promo_id');



  



  		if(Input::get('from')=='back'){



				$promo_id = Input::get('promo_id');



				$rest_id =  Input::get('promo_restid');



				$promo_list = DB::table('promotion')		



							->where('promo_restid', '=' ,$rest_id)



							->orderBy('promo_id', 'desc')



							->get();



				$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list); 	



				



				return View('rest_owner.ajax.promo_list')->with($data_onview);



				



			



		}



		else

		{





/*echo '<pre>';

print_r($_POST);

exit;*/

$promo_menu_item = '';

$promo_desc = '';

$promo_mode = '';

$promo_buy = '';

$promo_value = '';



$promo_start = '';

$promo_end = '';

$promo_day = '';

			

$promo_restid = Input::get('promo_restid');

$promo_for =Input::get('promo_for');

$promo_on = Input::get('promo_on');



if(Input::get('promo_on')=='qty')

{

	$promo_desc = Input::get('promo_desc_qty');

	$promo_mode = Input::get('promo_mode_qty');

	$promo_buy = Input::get('promo_buy_qty');

	$promo_value = Input::get('promo_offer_qty');

	$promo_menu_item = Input::get('promo_menu_item');	

	$promo_start = '';

	$promo_end = '';			

}





if(Input::get('promo_on')=='total_amt')

{

	$promo_desc = Input::get('promo_desc_cost');

	$promo_mode = Input::get('promo_mode_cost');

	$promo_buy = Input::get('promo_value_cost');

	$promo_value = Input::get('promo_offer_cost');

	$promo_start = '';

	$promo_end = '';			

}





if(Input::get('promo_on')=='schedul')

{

	$promo_desc = Input::get('promo_desc_range');

	$promo_mode = Input::get('promo_mode_range');

	$promo_buy = Input::get('promo_buy_range');

	$promo_value = Input::get('promo_offer_range');

	

	$promo_start = Input::get('promo_start');

	$promo_end = Input::get('promo_end');

	if(isset($_POST['day_name']))

	{

		$promo_day = implode(',',$_POST['day_name']);	

	}

	

		

}

$promo_status = '';



		



			if($promo_id==0)

			{





				



				$promo = new Promotion;



				$promo->promo_restid = $promo_restid;				

				$promo->promo_for = $promo_for;

				$promo->promo_on =$promo_on;

				$promo->promo_desc = $promo_desc;

				$promo->promo_mode = $promo_mode;

				$promo->promo_buy  = $promo_buy ;

				$promo->promo_value	 =  $promo_value;

				$promo->promo_start	 =  $promo_start;

				$promo->promo_end	 =  $promo_end;

				$promo->promo_status =  Input::get('promo_status');	

				$promo->promo_day = $promo_day;

				$promo->promo_menu_item = $promo_menu_item;

				$promo->save();



				$promo_id = $promo->promo_id;



				



				Session::flash('promo_message', 'Restaurant Promotion Inserted Sucessfully!'); 



				



				$rest_id =  Input::get('promo_restid');	



				



				



				if(Input::get('from')=='submit'){



				



				 $promo_list = DB::table('promotion')		



								->where('promo_restid', '=' ,$rest_id)



								->orderBy('promo_id', 'desc')



								->get();



					$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list); 	



					



					return View('rest_owner.ajax.promo_list')->with($data_onview);



				}



				elseif(Input::get('from')=='addnext'){		



					



					$id =0;					



					$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();

					

					$rest_menu	 = DB::table('menu_category')	

							->select('menu_category.*')	

							->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')	

							->where('menu.menu_status', '=' , '1')	

							->where('menu_category.rest_id', '=' ,$rest_id)	

							->where('menu_category.menu_cat_status', '=' ,'1')

							->orderBy('menu_category.menu_category_id', 'desc')

							->get();

							



					



					$data_onview = array('rest_id' =>$rest_id,'rest_detail'=>$rest_detail,'id'=>$id,'rest_menu'=>$rest_menu); 		



					return View('rest_owner.ajax.promo_form')->with($data_onview);



				}



				



			}

			else

			{



			



				$promo_id = Input::get('promo_id');



				$rest_id =  Input::get('promo_restid');



				if(Input::get('from')=='update'){			



				



					DB::table('promotion')



						->where('promo_id', $promo_id)



						->update([								  

								  'promo_restid' =>$promo_restid,



								  'promo_for' => $promo_for,



								  'promo_on' =>$promo_on,



								  'promo_desc' => $promo_desc,



								  'promo_mode' => $promo_mode,



								  'promo_buy' =>$promo_buy,



								  'promo_value' => $promo_value,



								  'promo_start' =>  $promo_start,



								  'promo_end' => $promo_end,

								  'promo_day'=> $promo_day,

								  

									'promo_menu_item'=>$promo_menu_item,



								  'promo_status' =>  Input::get('promo_status')	

						 ]);



					Session::flash('promo_message', 'Restaurant Promotion Update Sucessfully!'); 



				}



				



				$promo_list = DB::table('promotion')		



							->where('promo_restid', '=' ,$rest_id)



							->orderBy('promo_id', 'desc')



							->get();



				$data_onview = array('rest_id' =>$rest_id,'promo_list'=>$promo_list); 	



				



				return View('rest_owner.ajax.promo_list')->with($data_onview);



				



			}



		}	



	}



		



		



	/* PROMOSTION TABE END */



	



	



	/* BANKDETAIL TABE START */



	

	



	/* BANKDETAIL TABE END */





	



	



	



	



	/* SHOW POPULAR LIST START */



	function menu_category_popularlist()



	{



	



		  



		$rest_id = Input::get('rest_id');



		



		$rest_list  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();	



		



		$menu_list = DB::table('menu')		



					->where('restaurant_id', '=' ,$rest_id)



					->orderBy('menu_order', 'asc')



					->get();



						



		$menu_detail = '';							



		  $menu_cat_detail = '';



		  



		if(!empty($menu_list)){  			



	  $menu_cat_detail = DB::table('menu_category')		



					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')



					->where('menu_category.rest_id', '=' ,$rest_id)



					->where('menu_category.menu_cat_popular', '=' ,'1')



					->select('menu_category.*','menu_category_item.*')



					->orderBy('menu_category.menu_category_id', 'asc')



					->get();	



		}



		$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'rest_id'=>$rest_id,'menu_id'=>'','menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	



										



			return View('rest_owner.ajax.popular_menu_item_list')->with($data_onview);



	}



	/* POPUPLAR LIST END */



	/* Vendor Coupon code start */ 



	public function coupon_code()

	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		DB::connection()->enableQueryLog();
		$coupon_list = DB::table('coupon_code')
		->select('*')
		->where('vendor_id', '=' ,$vendor_id)
		->orderBy('id', 'asc')
		->get();

		$data_onview = array('coupon_list' =>$coupon_list);

		return View('rest_owner.couponcode_list')->with($data_onview);

	}



	public function coupon_form()

	{

		return View('rest_owner.couponcode_form');

	}



    public function add_coupon_code(Request $request)

	{

		$coupon = $request->coupon;
		$amount = $request->amount;
		$mamount = $request->mamount;
		$coupon_status = $request->coupon_status;
		$valid_till = $request->valid_till;
		$name = $request->coupon_name;
		$discount = $request->discount;

		if($request->per_user){ $per_user = $request->per_user; }else{ $per_user = 0; };
		if($request->usage_limit){ $usage_limit = $request->usage_limit;}else{  $usage_limit = 0; }

		$description = $request->description;

		$cdate = date("Y-m-d h:i:s");

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$checkdata = DB::table('coupon_code')
		            ->where('vendor_id', '=', $vendor_id)
		            ->where('coupon', '=', trim($coupon))
		            ->get();

        if(count($checkdata)>0)
        {   	

           Session::flash('error', 'Coupon code already exists!');
		   return redirect()->to('/merchant/coupon-code');

        }else{	

		$CouponData = array(
			'coupon' => trim($coupon),
			'vendor_id' => $vendor_id,
			'amount' => trim($amount),
			'apply_min_amount' => trim($mamount),
			'status' => trim($coupon_status),
			'valid_till' => trim($valid_till),
			'per_user' => trim($per_user),
			'created_at' => $cdate,
			'name' => trim($name),
			'discount' => trim($discount),
			'usage_limit' => trim($usage_limit),
			'description' => trim($description),
			'updated_at' => $cdate

		);

		$affected = DB::table('coupon_code')->insert($CouponData);

		if($affected){

			Session::flash('message', 'Coupon added sucessfully!');

			return redirect()->to('/merchant/coupon-code');

		}

	}	

}

   public function coupon_delete($id)

	{

		DB::table('coupon_code')->where('id', '=', $id)->delete();

		Session::flash('message', 'Coupon deleted successfully!');

		return redirect()->to('/merchant/coupon-code');

	}



	public function coupon_edit_form($id)

	{

			$coupon_detail  = DB::table('coupon_code')->where('id', '=' ,$id)->get();
			$data_onview = array(
				'coupon_detail' =>$coupon_detail,
				'coupon_id'=>$id,
			);

			return View('rest_owner.coupon_edit_form')->with($data_onview);


	}



   public function update_coupon_detail(Request $request)

	{	

		$coupon = $request->coupon;
		$amount = $request->amount;
		$mamount = $request->mamount;
		$coupon_status = $request->coupon_status;
		$valid_till = $request->valid_till;
		
		$coupon_id = $request->coupon_id;
		$name = $request->coupon_name;
		$discount = $request->discount;

		if($request->per_user){ $per_user = $request->per_user; }else{$per_user = 0; };
		if($request->usage_limit){ $usage_limit = $request->usage_limit;}else{ $usage_limit = 0; };

		$description = $request->description;
        $udate = date("Y-m-d h:i:s");

		$CouponData = array(

			'coupon' => trim($coupon),
			'amount' => trim($amount),
			'apply_min_amount' => trim($mamount),
			'status' => trim($coupon_status),
			'valid_till' => trim($valid_till),
			'per_user' => trim($per_user),
			'name' => trim($name),
			'discount' => trim($discount),
			'usage_limit' => trim($usage_limit),
			'description' => trim($description),
			'updated_at' => $udate

		);

		/* if(($amount >= $mamount) && ($discount!=1)){

           Session::flash('error', 'Coupon amount greater than minimum amount!');
		   return redirect()->to('/merchant/coupon-code');

        }else{ */

		$affected = DB::table('coupon_code')
		->where('id', $coupon_id)
		->update($CouponData);

		if($affected){

			Session::flash('message', 'Coupon details updated sucessfully!');

			return redirect()->to('/merchant/coupon-code');

		}
	//}

	}

	/* Vendor Coupon code end */

	function get_template_action()

	{

		/*echo '<pre>';	

		print_r($_POST);*/

	

		

		$template_ids = '';

		

		$rest_id = Input::get('restaurant_id');

		$tempalte_data = Input::get('tempalte_data');

		

		//$rest_empty = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->where('template_id', '=' ,'')->count();

		$rest_empty = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();

		

		if(isset($_POST['tempalte_data']))

		{//	echo 'test'

				foreach($tempalte_data as $temp_id)

				{

					$template_ids[] = $temp_id;

					$menu_list = DB::table('template_menu')	

						->where('template_id', '=' ,$temp_id)

						->orderBy('menu_order', 'asc')

						->get();	

					if($menu_list)		

					{						

				    	foreach($menu_list as $mt_list)			

						{

								

							$menu = new Menu;

							$menu->restaurant_id = $rest_id;

							$menu->template_id = $mt_list->template_id;

							$menu->menu_name =  $mt_list->menu_name;

							$menu->menu_desc =  $mt_list->menu_desc;

							$menu->menu_status = $mt_list->menu_status;

							$menu->menu_order =  $mt_list->menu_order;

							$menu->menu_image = '';

							$menu->save();

							$menu_id = $menu->menu_id;



							 $menu_cat_detail = DB::table('template_menu_category')	

												->where('template_id', '=' ,$temp_id)

												->where('menu_id', '=' ,$mt_list->menu_id)

												->select('*')

												->orderBy('menu_category_id', 'asc')

												->get();

							if($menu_cat_detail)					

							{

								foreach($menu_cat_detail as $mtc_list)

								{

									$menu_category = new  Menu_category ;

									$menu_category->rest_id = $rest_id;

									$menu_category->menu_id =  $menu_id;

									$menu_category->menu_category_name =  $mtc_list->menu_category_name;

									$menu_category->menu_category_desc =  $mtc_list->menu_category_desc;

									$menu_category->menu_cat_popular =  $mtc_list->menu_cat_popular;

									$menu_category->menu_cat_diet =  $mtc_list->menu_cat_diet;

									$menu_category->menu_category_price =  $mtc_list->menu_category_price;

									$menu_category->menu_category_portion = $mtc_list->menu_category_portion;

									$menu_category->menu_cat_status = $mtc_list->menu_cat_status;

									$menu_category->save();

									$menu_category_id = $menu_category->menu_category_id;

									

									

									/***************** sub item list *****/

									

						$menu_cat_item_detail = DB::table('template_category_item')	

												->where('menu_category', '=' ,$mtc_list->menu_category_id)

												->where('menu_id', '=' ,$mt_list->menu_id)

												->select('*')

												->orderBy('menu_cat_itm_id', 'asc')

												->get();

												

										if($menu_cat_item_detail)		

										{

											foreach($menu_cat_item_detail as $mtci_list)

											{											

												$menu_sub_itme = new  Category_item;															

												$menu_sub_itme->rest_id	 = $rest_id;

												$menu_sub_itme->menu_id	 = $menu_id;

												$menu_sub_itme->menu_category	 = $menu_category_id;

												$menu_sub_itme->menu_item_title	 = $mtci_list->menu_item_title;

												$menu_sub_itme->menu_item_price	 = $mtci_list->menu_item_price;

												$menu_sub_itme->menu_item_status = $mtci_list->menu_item_status;

												$menu_sub_itme->menu_cat_itm_price = $mtci_list->menu_cat_itm_price;

												$menu_sub_itme->save();

											}

										}

												

										

									/************* addon list   *****/

									

					$addon_list = DB::table('template_menu_category_addon')

									->where('addon_templateid', '=' ,$temp_id)

									->where('addon_menuid', '=' ,$mt_list->menu_id)

									->where('addon_menucatid', '=' ,$mtc_list->menu_category_id)

									->orderBy('addon_id', 'asc')

									->get();

									

										if($addon_list)

										{

											foreach($addon_list as $mtca_list)

											{

												$addon = new Menu_category_addon;

												$addon->addon_restid = $rest_id;								

												$addon->addon_menuid = $menu_id;

												$addon->addon_menucatid = $menu_category_id;								

												$addon->addon_name = trim($mtca_list->addon_name);								

												$addon->addon_price =  $mtca_list->addon_price;								

												$addon->addon_status =  $mtca_list->addon_status;								

												$addon->addon_groupname = strtoupper(trim($mtca_list->addon_groupname));								

												$addon->addon_option =  $mtca_list->addon_option;

												$addon->addon_default =  $mtca_list->addon_default;		

												$addon->save();												

											}										

										}

									

									

									

								}

							}							

							

						}

					}							

				}

				

				

						$rest_temp_id = $rest_empty[0]->template_id;

				

			if(empty($rest_temp_id))

			{			

				$rest_temp_id = implode(',',$template_ids);

			}

			else

			{

				$rest_temp_id = $rest_temp_id.','.implode(',',$template_ids);

			}

			

			DB::table('restaurant')->where('rest_id', $rest_id)->update(['template_id' =>$rest_temp_id]);		

						

				//DB::table('restaurant')->where('rest_id', $rest_id)->update(['template_id' =>$template_ids]);

			

		}

		



		return redirect()->to('/merchant/restaurant_view/'.$rest_id);

				



	}	

	



}



