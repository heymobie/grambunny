<?php

namespace App\Http\Controllers\RestOwner;
use App\Http\Controllers\Controller;


Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Route;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;

use App\Restaurant;
use App\Menu;
use App\Menu_category;
use App\Menu_category_item;
use App\Menu_category_addon;

use App\Service;
use App\Bankdetail;
use App\Promotion;


class OwnerreviewController  extends Controller
{

    public function __construct(){
    	$this->middleware('vendor');
    }
	
	
	public function show_reviewlist()
	{
		DB::enableQueryLog(); 	
		
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
		$vender_detail = DB::table('vendor')
						 ->select('*')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		
		$restaurant_detail = DB::table('restaurant')
						 ->select('*')
						 ->where('rest_status', '!=' ,'INACTIVE')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		$rest_id_array='';						 	
								 
		if($restaurant_detail)
		{	
			$rest_id_array=[];
			foreach($restaurant_detail as $rest_data)
			{
				$rest_id_array[]=$rest_data->rest_id;
			}
		}	
		
		
		$review_detail  = '';
		if((count($rest_id_array)>0) && (!empty($rest_id_array))){
		
		$review_detail  =  DB::table('review')		
						->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id')
						->leftJoin('users', 'review.re_userid', '=', 'users.id')
						->leftJoin('order', 'review.re_orderid', '=', 'order.order_id')	
						->where('review.re_orderid', '>' ,'0')
						->where(function($query) use ($rest_id_array) {
			
								$l=0;
								foreach($rest_id_array as $ftype )
								{	
									if($ftype>0)
									{
										if($l>0){
											 $query = $query->orWhere('review.re_restid','=',$ftype);	
										 }
										 else
										 {
											 $query = $query->Where('review.re_restid','=',$ftype);	
										 }
									}						
									$l++;
								}
							
							})	
			
						->select('*')
						->orderBy('review.re_id', 'asc')
						->get();
		}	
		
		$data_onview = array('review_detail'=>$review_detail); 
		return View('rest_owner.review_list')->with($data_onview);	
	}

	public function show_reviewlist_report()
	{
		
		DB::enableQueryLog(); 	
		
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
		$vender_detail = DB::table('vendor')
						 ->select('*')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		
		$restaurant_detail = DB::table('restaurant')
						 ->select('*')
						 ->where('rest_status', '!=' ,'INACTIVE')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		$rest_id_array='';						 	
								 
		if($restaurant_detail)
		{
			$rest_id_array=[];
			foreach($restaurant_detail as $rest_data)
			{
				$rest_id_array[]=$rest_data->rest_id;
			}
		}	
		
		
		$review_detail  = '';
		if((count($rest_id_array)>0) && (!empty($rest_id_array))){
		
		$review_detail  =  DB::table('review')		
						->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id')
						->leftJoin('users', 'review.re_userid', '=', 'users.id')
						->leftJoin('order', 'review.re_orderid', '=', 'order.order_id')	
						->where('review.re_orderid', '>' ,'0')
						->where('review.re_status', '=' , 'PUBLISHED')
						->where(function($query) use ($rest_id_array) {
		
								$l=0;
								foreach($rest_id_array as $ftype )
								{	
									if($ftype>0)
									{
										if($l>0){
											 $query = $query->orWhere('review.re_restid','=',$ftype);	
										 }
										 else
										 {
											 $query = $query->Where('review.re_restid','=',$ftype);	
										 }
									}						
									$l++;
								}
							
							})	
			
						->select('*')
						->orderBy('review.re_restid', 'asc')
						->get();

		               $item_details = DB::table('menu_category')
						->where(function($query) use ($rest_id_array) {
		
								$ll=0;
								foreach($rest_id_array as $fftype )
								{	
									if($fftype>0)
									{
										if($ll>0){
											 $query = $query->orWhere('menu_category.rest_id','=',$fftype);	
										 }
										 else
										 {
											 $query = $query->Where('menu_category.rest_id','=',$fftype);	
										 }
									}						
									$ll++;
								}
							
							})
						->groupBy('menu_category_name')
						->get();

		}

		
		$data_onview = array('review_detail'=>$review_detail,'item_details'=>$item_details); 
		return View('rest_owner.review_list_report')->with($data_onview);	
	}

	public function show_review_action()
	{
		$reason = '';
		if(Input::get('reason') && (!empty(Input::get('reason'))) && (Input::get('revew_status')=='REJECT'))
		{
			$reason =Input::get('reason');
		}
		DB::table('review')
            ->where('re_id', Input::get('re_id'))
            ->update(['re_status' => Input::get('revew_status'),
					  're_rejectreson'=>  $reason,
					  're_rejectdate'=>  date('Y-m-d')
					 ]);
					 
		 Session::flash('message', 'Review Updated Successfully!');
		 return redirect()->to('/vendor/review_list');

		 
	}
	
	
	
	public function ajax_search_list()
	{
		$from_date=Input::get('fromdate');;
		$to_date=Input::get('todate');;
		$status = trim(Input::get('order_status'));
		$user_id = Input::get('user_id');
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
		$vender_detail = DB::table('vendor')
						 ->select('*')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		
		$restaurant_detail = DB::table('restaurant')
						 ->select('*')
						 ->where('rest_status', '!=' ,'INACTIVE')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		$rest_id_array='';						 	
								 
		if($restaurant_detail)
		{
			foreach($restaurant_detail as $rest_data)
			{
				$rest_id_array[]=$rest_data->rest_id;
			}
		}	
		
		
		$review_detail  = '';
		if((count($rest_id_array)>0) && (!empty($rest_id_array))){
		
		
			$review_detail  = DB::table('review');		
			
			$review_detail  = $review_detail->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id');
			$review_detail  = $review_detail->leftJoin('users', 'review.re_userid', '=', 'users.id');
			$review_detail  = $review_detail->leftJoin('order', 'review.re_orderid', '=', 'order.order_id');			
			
			$review_detail  = $review_detail->where(function($query) use ($rest_id_array) {
			
								$l=0;
								foreach($rest_id_array as $ftype )
								{	
									if($ftype>0)
									{
										if($l>0){
											 $query = $query->orWhere('review.re_restid','=',$ftype);	
										 }
										 else
										 {
											 $query = $query->Where('review.re_restid','=',$ftype);	
										 }
									}						
									$l++;
								}
							
							})	;
			
			
			
			
			
			if(!empty($status) && ($status!='All'))
			{
			  $review_detail = $review_detail->where('review.re_status', '=', $status);
			}	
				
			/* Get Data between Date */
			
			
			
			if(!empty($from_date) && ($from_date!='0000-00-00'))
			{
			  $review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from_date)));
			}	
			
			
			if(!empty($to_date) && ($to_date!='0000-00-00'))
			{
			  $review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to_date)));
			}		
			
			/* End */
			
					
							
			$review_detail = $review_detail->select('*');
			$review_detail = $review_detail->orderBy('review.re_id', 'asc');
			$review_detail = $review_detail->get();	
				
			
		}
		
		$data_onview = array('review_detail'=>$review_detail); 				
		
		return View('admin.ajax.review_list')->with($data_onview);
		
		//print_r($data_onview);
		
	}


	public function ajax_search_list_report()
	{
		$from_date=Input::get('fromdate');;
		$to_date=Input::get('todate');;
		$status = trim(Input::get('order_status'));
		$user_id = Input::get('user_id');
		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
		$vender_detail = DB::table('vendor')
						 ->select('*')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		
		$restaurant_detail = DB::table('restaurant')
						 ->select('*')
						 ->where('rest_status', '!=' ,'INACTIVE')
						 ->where('vendor_id', '=' ,$vendor_id)
						 ->get();
		$rest_id_array='';
		//echo "<pre>"; print_r($restaurant_detail); die;
								 
		if($restaurant_detail)
		{
			foreach($restaurant_detail as $rest_data)
			{
				$rest_id_array[]=$rest_data->rest_id;
			}
		}	
		
		
		$review_detail  = '';
		if((count($rest_id_array)>0) && (!empty($rest_id_array))){
		
		
			$review_detail  = DB::table('review');		
			
			$review_detail  = $review_detail->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id');
			$review_detail  = $review_detail->leftJoin('users', 'review.re_userid', '=', 'users.id');
			$review_detail  = $review_detail->leftJoin('order', 'review.re_orderid', '=', 'order.order_id');			
			
			$review_detail  = $review_detail->where(function($query) use ($rest_id_array) {
			
								$l=0;
								foreach($rest_id_array as $ftype )
								{	
									if($ftype>0)
									{
										if($l>0){
											 $query = $query->orWhere('review.re_restid','=',$ftype);	
										 }
										 else
										 {
											 $query = $query->Where('review.re_restid','=',$ftype);	
										 }
									}						
									$l++;
								}
							
							})	;
			
		
			
			
			if(!empty($status) && ($status!='All'))
			{

			if($status=='daily'){

			$current_date = date("Y-m-d");	

			$review_detail = $review_detail->where('review.created_at', '>=', $current_date);

			}

		    if($status=='weekly'){	

		    $from = date('Y-m-d', strtotime('-7 days'));	
		    $to = date("Y-m-d");	

			$review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from)));
			$review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to)));

			 }

			if($status=='monthly'){	

			$from = date('Y-m-d', strtotime('-30 days'));	
		    $to = date("Y-m-d");		
		
			$review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from)));
			$review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to)));

			 }


			}	
				
			/* Get Data between Date */		
			
			if(!empty($from_date) && ($from_date!='0000-00-00'))
			{
			  $review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from_date)));
			}	
				
			if(!empty($to_date) && ($to_date!='0000-00-00'))
			{
			  $review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to_date)));
			}		
			
			/* End */				
							
			$review_detail = $review_detail->select('*');
			$review_detail = $review_detail->orderBy('review.re_id', 'asc');
			$review_detail = $review_detail->get();	
				
			
		}
		
		$data_onview = array('review_detail'=>$review_detail); 				
		
		return View('rest_owner.ajax.review_list_report')->with($data_onview);
		
		//print_r($data_onview);
		
	}
	
	
	function review_delete($id)
	{
				
		DB::table('review')->where('re_id', '=', $id)->delete();
		Session::flash('message', 'Information Deleted Sucessfully!');
		 return redirect()->to('/vendor/review_list');
	}
}
