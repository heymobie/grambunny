<?php



namespace App\Http\Controllers\RestOwner;



use App\Http\Controllers\Controller;



use App\Http\Requests;



use App\Notification_list;



use App\Vendor;
use App\Order;

use Carbon\Carbon;



use Illuminate\Http\Request;



use Illuminate\Support\Facades\Auth;



use Illuminate\Support\Facades\Hash;



use Illuminate\Support\Facades\Input;



use Illuminate\Support\Facades\DB;



use Redirect;



use Route;



use Session; 

use App\Video;
use File;

use Storage;





class OwnerController extends Controller



{



public function __construct(){



        $this->middleware('vendor');



   }





    public function category_list(Request $request)

	{



		$psid = $request->psid;



	    $cate = DB::table('product_service_category')		

					->where('type', '=' ,$psid)

					->orderBy('id', 'asc')

				    ->get();



		$data_onview = array('cate'=>$cate,'psid'=>$psid); 



		return View('admin.ajax.category')->with($data_onview);



	}



	public function subcategory_list(Request $request)

	{



		$catid = $request->catid;



	    $subcate = DB::table('product_service_sub_category')		

					->where('category_id', '=' ,$catid)

					->orderBy('id', 'asc')

				    ->get();



		$data_onview = array('subcate'=>$subcate,'catid'=>$catid); 



		return View('admin.ajax.subcategory')->with($data_onview);



	}  



	function bank_account()

	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$vendor_bank_detail = DB::table('vendor_bank_details')

		 						->where('vendor_id', '=' ,$vendor_id)

								->get();

        $vid = count($vendor_bank_detail);														

  		$data_onview = array('vendor_bank_detail'=>$vendor_bank_detail,'vid'=>$vid); 
			   
    	return view('rest_owner.bank_account')->with($data_onview);

	} 

 	function social_media()

	{

		$merchant_id = Auth::guard('vendor')->user()->vendor_id;

		$vendor_social_detail = DB::table('merchant_social_links')

		 						->where('merchant_id', '=' ,$merchant_id)

								->get();

        $vid = count($vendor_social_detail);														

  		$data_onview = array('vendor_social_detail'=>$vendor_social_detail,'vid'=>$vid); 
			   
    	return view('rest_owner.social_media')->with($data_onview);

	} 


	public function social_setup()

	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$facebook = Input::get('facebook');

		$twitter = Input::get('twitter');	

		$pinterest = Input::get('pinterest');	

		$instagram = Input::get('instagram');	

		$google = Input::get('google');

		$existVendor = DB::table('merchant_social_links')->where('merchant_id', '=', $vendor_id)->first();

		if($existVendor === null)

		{

			DB::table('merchant_social_links')->insert(

			    ['merchant_id' => $vendor_id,

			     'facebook_link' => $facebook,

			     'twitter_link' => $twitter,

			 	 'pinterest_link' => $pinterest,

			 	 'instagram_link' => $instagram,

			 	 'google_plus_link' => $google]

			);

			Session::flash('message', 'Social link setup sucessfully!');

			return redirect()->to("/merchant/social-media");  

		} else {

			DB::table('merchant_social_links')

            ->where('merchant_id', $vendor_id)

            ->update(['facebook_link' => $facebook,

				     'twitter_link' => $twitter,

				 	 'pinterest_link' => $pinterest,

				 	 'instagram_link' => $instagram,

				 	 'google_plus_link' => $google]);

			Session::flash('message', 'Social link updated sucessfully!'); 
			return redirect()->to('/merchant/social-media');

		}

	}

	
	function payment_setting()
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$vendor_payment_detail = DB::table('payment_setting')

		 						->where('vendor_id', '=' ,$vendor_id)

								->get();

        $vid = count($vendor_payment_detail);


  		$data_onview = array('vendor_payment_detail'=>$vendor_payment_detail,'vid'=>$vid); 
			   
    	return view('rest_owner.payment_setting')->with($data_onview);

	} 


  public function payment_setup()
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$app_id = Input::get('app_id');
		$location_id = Input::get('location_id');	
		$access_token = Input::get('access_token');	
		$cash = Input::get('cash');
		$credit_card = Input::get('credit_card'); 
		$swipe = NULL;//Input::get('swipe');
		$square_payment = NULL;

		/* if($credit_card==3){

		$square_payment = $credit_card;//Input::get('square_payment');
		$credit_card = '';

	    }else{

        $square_payment = '';

	    } */

		$existVendor = DB::table('payment_setting')->where('vendor_id', '=', $vendor_id)->first();

		if($existVendor === null)
		{

			DB::table('payment_setting')->insert(

			    ['vendor_id' => $vendor_id,
			     'app_id' => $app_id,
			     'location_id' => $location_id,
			 	 'access_token' => $access_token,
			 	 'status' => 1,
			 	 'swipe' => $swipe,
			 	 'cash' => $cash,
			     'credit_card' => $credit_card,
			     'square_payment' => $square_payment]

			);

			Session::flash('message', 'Payment account Add Successfully!');

			return redirect()->to("/merchant/payment-setting");  

		} else {

			DB::table('payment_setting')

            ->where('vendor_id', $vendor_id)

            ->update(['app_id' => $app_id,
				     'location_id' => $location_id,
				 	 'access_token' => $access_token,
				 	 'status' => 1,
				     'cash' => $cash,
				     'swipe' => $swipe,
			         'credit_card' => $credit_card,
			         'square_payment' => $square_payment]);

			Session::flash('message', 'Payment account update sucessfully!'); 

			return redirect()->to('/merchant/payment-setting');

		}


	}


	function index()

	{

		if(Auth::guard('vendor')->user()->vendor_id){

        $vendor_id = Auth::guard('vendor')->user()->vendor_id;

	    $order_detail  = DB::table('orders')

	    ->where('vendor_id', '=' ,$vendor_id)

		->orderBy('id', 'desc')

		->get();

		// $order_total  = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->get();

		// $ordercount = $order_total->count();				

		// $data_onview = array('order_detail' =>$order_detail,'ordercount'=>$ordercount); 

		$total_user  = DB::table('users')->get();

		$order_total  = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->get();

		$total_products  = DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->get();

		$today_date = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->get();

		$current_week = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();

		$order_pending  = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->where('status','=',0)->get();

		$today_date_pending = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status','=',0)->get();

		$today_pending = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status',0)->get();

		$today_Accept = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status',1)->get();

		$today_cancel = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status',2)->get();

		$today_on_the_way = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status',3)->get();

		$today_deliverd = DB::table('orders')->where('vendor_id', '=' ,$vendor_id)->whereDate('created_at', Carbon::today())->where('status',4)->get();

		//$current_week_pending = DB::table('orders')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('status','=',0)->get();

		$orderss = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
        ->groupBy('x')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
         ->where('vendor_id', '=' ,$vendor_id)
         ->get();

         //dd($orderss);

        $one_week = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->where('vendor_id', '=' ,$vendor_id)->get();

       $userss = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')
		    ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
		    ->groupBy('date')->where('vendor_id', '=' ,$vendor_id)
		    ->get();


		$ordercount = $order_total->count();

		$usercount = $total_user->count();

		//$merchantcount = $total_merchant->count();

		$totalproducts = $total_products->count();

		$todaydate = $today_date->count();

		$currentweek = $current_week->count();

		$orderpending = $order_pending->count();

		$todaydatepending = $today_date_pending->count();

		//$currentweekpending = $current_week_pending->count();


		$todaypending = $today_pending->count();

		$todayAccept = $today_Accept->count();

		$todaycancel = $today_cancel->count();

		$today_ontheway = $today_on_the_way->count();

		$todaydeliverd = $today_deliverd->count();

	//	$ordercount = $order_total->count();	

	//	$usercount = $total_user->count();			

		$data_onview = array('order_detail' =>$order_detail,'ordercount'=>$ordercount,'usercount'=>$usercount,'totalproducts'=>$totalproducts,'todaydate'=>$todaydate,'currentweek'=>$currentweek,'orderpending'=>$orderpending,'todaydatepending'=>$todaydatepending,'todaypending'=>$todaypending,'todayAccept'=>$todayAccept,'todaycancel'=>$todaycancel,'today_ontheway'=>$today_ontheway, 'todaydeliverd'=>$todaydeliverd,'orderss'=>$orderss, 'one_week'=>$one_week,'userss'=>$userss);	

    	return view('rest_owner.dashboard')->with($data_onview);

    }else{

    	return redirect()->route('merchant/login');		

    }

    }	



 function my_wallet()



	{



//		echo 'we are doing testing';



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$wallet_detail  =  DB::table('vendor_wallet_transaction')		



						->where('vendor_id', '=' ,$vendor_id)



						->orderBy('id', 'desc')



						->get();







		$user_detail  =  DB::table('vendor')		



						->where('vendor_id', '=' ,$vendor_id)



						->value('wallet_amount');







		$vendor_bank_detail = DB::table('vendor_bank_details')



		 						->where('vendor_id', '=' ,$vendor_id)



								->get();







		$vendor_payment_detail	= DB::table('admin_vendor_transaction')



								->where('vendor_id', '=' ,$vendor_id)



								->orderBy('id', 'desc')



								->get();





		$request_details	= DB::table('vendor')



						->Join('vendor_bank_details', 'vendor.vendor_id', '=', 'vendor_bank_details.vendor_id')



						->Join('vendor_payment_request', 'vendor.vendor_id', '=', 'vendor_payment_request.vendor_id')



						->where('vendor_bank_details.vendor_id', '=', $vendor_id)



						->select('vendor.name','vendor.wallet_amount','vendor_bank_details.*','vendor_payment_request.*')



						->orderBy('vendor_payment_request.request_id', 'desc')



						->get();			



  		$data_onview = array('wallet_detail'=>$wallet_detail,'wallet_amount'=>$user_detail,'vendor_payment_detail'=>$vendor_payment_detail, 'vendor_bank_detail'=>$vendor_bank_detail,'request_details'=>$request_details); 

				   	

    	return view('rest_owner.my_wallet')->with($data_onview);



	}



	function view_profile()
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$vender_detail = DB::table('vendor')

			  		 ->select('*')

						 ->where('vendor_id', '=' ,$vendor_id)

						 ->get();

			$type = $vender_detail[0]->type;	

			$catagory = DB::table('product_service_category')

						->where('type', '=' ,$type)

						->where('status', '=' ,1)

						->get();

			$cateid = $vender_detail[0]->category_id;	

			$subcatagory = DB::table('product_service_sub_category')

						->where('category_id', '=' ,$cateid)

						->where('status', '=' ,1)

						->orderBy('id', 'asc')

						->get();					 					 


		    $data_onview = array('vender_detail' =>$vender_detail,'catagory'=>$catagory,'subcatagory'=>$subcatagory,'id'=>$vendor_id); 			    	

    	    return view('rest_owner.update_profile')->with($data_onview);

	}

    public function crop_image_save(Request $request)
	{

		$image = $request->profile;
		$vendor_id = Input::get('vendor_id');
		$profile_field = Input::get('profile_field');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $profile_img1= '1'.time().'.png';
        $path = public_path('uploads/vendor/profile/'.$profile_img1);
        file_put_contents($path, $image);

        DB::table('vendor')
		->where('vendor_id', $vendor_id)
		->update([$profile_field => $profile_img1]);			

        return response()->json(['status'=>$profile_img1]);

	}

	function update_profile_action(Request $request)
	{

		$vendor_id = $request->vendor_id;

        $profile_img1 = $request->profile_img1_old;

        $license_front = $request->license_front_old;

        $license_back = $request->license_back_old;

        $profile_img2 = $request->profile_img2_old;

        $profile_img3 = $request->profile_img3_old;

        $profile_img4 = $request->profile_img4_old; 

        $pvideo1 = $request->pvideo1_old;


        $browsername = $this->get_browser_name($_SERVER['HTTP_USER_AGENT']);

        if($browsername=='Safari'){ 

        $license_expiry = $this->dateFormatchange($request->license_expiry); 

		$dob = $this->dateFormatchange($request->dob); 
		
		$permit_expiry = $this->dateFormatchange($request->permit_expiry); 

        }else{

        $license_expiry = date("Y-m-d", strtotime($request->license_expiry));
		$dob = date("Y-m-d", strtotime($request->dob)); 
		$permit_expiry = date("Y-m-d", strtotime($request->permit_expiry));

        }   


        $url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$request->market_area."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

		$response = file_get_contents($url);

		$data = json_decode($response);

		$geo=$data->results[0]->geometry->location;


		// if(!empty($request->mob_no)){
		
		// $geo->lat = $request->lat;
		
		// $geo->lng = $request->lat;
		
		// }else{
		
		// 	$geo->lat;
		
		// 	$geo->lng;
		// }


		//print_r($geo->lat." ".$geo->lng); die;
		/*Uploading Images*/

		$request->hasFile("profile_img1") ? $request->file("profile_img1")->move("public/uploads/vendor/profile/",$profile_img1=str_random(16).'.jpg') : "default.png";



		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/vendor/license/",$license_front=str_random(16).'.'.$request->license_front->extension()) : "";



		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/vendor/license/",$license_back=str_random(16).'.'.$request->license_back->extension()) : "";



	    $request->hasFile("profile_img2") ? $request->file("profile_img2")->move("public/uploads/vendor/profile/",$profile_img2=str_random(16).'.jpg') : "default.png";



		$request->hasFile("profile_img3") ? $request->file("profile_img3")->move("public/uploads/vendor/profile/",$profile_img3=str_random(16).'.jpg') : "default.png";



		$request->hasFile("profile_img4") ? $request->file("profile_img4")->move("public/uploads/vendor/profile/",$profile_img4=str_random(16).'.jpg') : "default.png";

		  if($request->hasFile('video'))
            {

           $imagename = DB::table('vendor')->where('vendor_id', $vendor_id)->value('video');
            if(File::exists(public_path($imagename))){
            File::delete(public_path($imagename));
            }

             $path = $request->file('video')->store('videos', ['disk' => 'my_files']);
             $pvideo1 =  $path;

            }

        $subcategory = $request->subcategory ;

		$sub_category = json_encode($subcategory);

            $vendor_status = 0;

        	 DB::table('vendor')

            ->where('vendor_id', $vendor_id)

            ->update(['name' => $request->name,

					  'last_name'=>	 $request->last_name,

					  'mob_no'=>  $request->mob_no,

					  'business_name'=>  $request->business,

					  'address'=>  $request->address,

					  'mailing_address'=>  $request->mailing_address,

					  'pick_up_address' => $request->pick_up_address,

					  'city'=>  $request->city,

					  'state'=>  $request->state,

					  'zipcode'=>  $request->zipcode,

					  'vendor_status'=>  $vendor_status,

					  'market_area'=>  $request->market_area,

					  'service_radius'=>  $request->service_radius,

					  'driver_license'=>  $request->driver_license,

					  'license_expiry'=>  $license_expiry,

					  'license_front'=>  $license_front,

					  'license_back'=>  $license_back,

					  'ssn'=>  $request->ssn,

					  'dob'=>  $dob,

					  'profile_img1'=>  $profile_img1,

					  'profile_img2'=>  $profile_img2,

					  'profile_img3'=>  $profile_img3,

					  'profile_img4'=>  $profile_img4,

					  'video'=>  $pvideo1, 

					  'type'=>  $request->type,

					  'category_id'=>  $request->category,

					  'sub_category_id' =>  $sub_category,

					  'sales_tax'=>  $request->sales_tax,

					  'permit_type'=>  $request->permit_type,

					  'permit_number'=>  $request->permit_number,

					  'permit_expiry'=>  $permit_expiry,

					  'description'=>  $request->description,

					  'make'=>  $request->make,

					  'insta'=>  $request->insta,
					  'facebook'=>  $request->facebook,
					  'tiktok'=>  $request->tiktok,
					  'pinterest'=>  $request->pinterest,

					  'model'=>  $request->model,

					  'color'=>  $request->color,

					  'year'=>  $request->year,

					  'license_plate'=>  $request->license_plate,

					  'lat'=> $geo->lat,

		              'lng'=> $geo->lng,

		              'delivery_fee'=>  $request->delivery_fee,

		              'minimum_order_amount'=>  $request->minimum_order_amount,

		              'excise_tax'=>  $request->excise_tax,

		              'city_tax'=>  $request->city_tax,

		              'cash_card' => $request->cash_card 

					 ]);    



		Session::flash('message', 'Account Information Updated Successfully!');

		return redirect()->route('merchant.updateProfilePage');				 

	}


    function get_browser_name($user_agent)
    {
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
        elseif (strpos($user_agent, 'Edge')) return 'Edge';
        elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
        elseif (strpos($user_agent, 'Safari')) return 'Safari';
        elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
       
        return 'Other';
    }	


	public function dateFormatchange($date)
	{ 

		 $datearr = explode("/",$date);

		 return "$datearr[2]-$datearr[0]-$datearr[1]";

	}	


public function dateFormat($date)
{
    $m = preg_replace('/[^0-9]/', '', $date);
    if (preg_match_all('/\d{2}+/', $m, $r)) {
        $r = reset($r);
        if (count($r) == 4) {
            if ($r[2] <= 12 && $r[3] <= 31) return "$r[0]$r[1]-$r[2]-$r[3]"; // Y-m-d
            if ($r[0] <= 31 && $r[1] != 0 && $r[1] <= 12) return "$r[2]$r[3]-$r[1]-$r[0]"; // d-m-Y
            if ($r[0] <= 12 && $r[1] <= 31) return "$r[2]$r[3]-$r[0]-$r[1]"; // m-d-Y
            if ($r[2] <= 31 && $r[3] <= 12) return "$r[0]$r[1]-$r[3]-$r[2]"; //Y-m-d
        }

        $y = $r[2] >= 0 && $r[2] <= date('y') ? date('y') . $r[2] : (date('y') - 1) . $r[2];
        if ($r[0] <= 31 && $r[1] != 0 && $r[1] <= 12) return "$y-$r[1]-$r[0]"; // d-m-y
    }
}



	public function showPasswordForm(){



    	return view('rest_owner.changepassword');



	}


  	public function show_account(){

    	return view('rest_owner.showaccount');

	}

	public function account_delete()
	{ 
		
	    $id = Auth::guard('vendor')->user()->vendor_id;

        Auth::guard('vendor')->logout();
 
        Session::flush();
    
	    $product_id =DB::table('product_service')->where('vendor_id', '=', $id)->get('id');

	    foreach ($product_id as $key => $value) {

	    	DB::table('ps_images')->where('ps_id', '=', $value->id)->delete(); 
	    }

		DB::table('temp_coupon_code_apply')->where('vendor_id', '=', $id)->delete();

		DB::table('coupon_code')->where('vendor_id', '=', $id)->delete();

		DB::table('orders')->where('vendor_id', '=', $id)->delete();

		DB::table('product_service')->where('vendor_id', '=', $id)->delete();

		DB::table('vendor')->where('vendor_id', '=', $id)->delete();

		Session::flash('message', 'Merchant Information Deleted Sucessfully!');

		return redirect()->route('merchant.login');

	}



	public function updatepasswordNOTINUSE()



	{



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$credentials = [



                'password' => Request::get('old_password'),



                'vendor_id' =>  $vendor_id



        ];



		if(Auth::guard('vendor')->attempt($credentials))



		{



			DB::table('vendor')



            ->where('vendor_id', $vendor_id)



            ->update(['password' =>  Hash::make(Request::get('password'))



						]);



			Session::flash('message', 'Password Update Sucessfully!'); 



			//$request->session()->keep('message','Old Password Not Match Sucessfully!');



		}



		else



		{			



			Session::flash('message', 'Old Password Not Match!'); 



		}



            return redirect()->route("merchant.updateProfilePage");  



		



	}



	public function updatePassword(Request $request)

	{

		$validation=$this->validate($request,[



            'old_password' => "required",



            'password' => 'required|confirmed|min:6',



            'password_confirmation' => "required"



        ]);



        if (!Hash::check($request->old_password, Auth::guard("vendor")->user()->password)) {



        	return redirect()->back()->withErrors(["old_password" => "Old password is incorrect"])->withInput();



        }

        $vendor=Vendor::find(Auth::guard("vendor")->user()->vendor_id);

        $vendor->password=Hash::make($request->password);

        $vendor->update();



        return redirect()->back()->with("message","Password updated successfully!");

	}





	public function request_amount()



	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$request_amount = Input::get('request_amount');	

		$vamount  =  DB::table('vendor')   

                   ->where('vendor_id', '=', $vendor_id)

                   ->value('wallet_amount');



		if($vamount >= $request_amount)



		{

            $requestdata = array(



			'vendor_id' => $vendor_id,



			'vendor_amount' => $request_amount,



			'transfer_status' => 0,



			'txn_method' => 'bank',



		);



		$affected = DB::table('admin_vendor_transaction')->insert($requestdata);

		Session::flash('message', 'Payment Request Sucessfully Submitted !'); 

		return redirect()->to('/vendor/my_wallet');



		}



		else



		{			



			Session::flash('message', 'Payment Request Failed !'); 



                return redirect()->to("/vendor/my_wallet");  



				



		}



		



	}

	public function crop_product_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('product_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);

        DB::table('product_service')
		//->where('vendor_id', $vendor_id)
		->where('id', $product_id)
		->update(['image' => $product_img]); 

		DB::table('ps_images')
		->where('ps_id', $product_id)
		->where('thumb', 1)
		->update(['name' => $product_img]);				

        return response()->json(['status'=>$product_img]);

	}


	public function gallery_crop_image_save(Request $request)
	{

		$image = Input::get('productimg');
		$vendor_id = Input::get('vendor_id');
		$product_id = Input::get('product_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $product_img= '1'.time().'.png';
        $path = public_path('uploads/product/'.$product_img);
        file_put_contents($path, $image);	

		DB::table('ps_images')
		->insert([
			'ps_id' => $product_id,
			'name' => $product_img,
			'created_at' => date("Y-m-d h:i:s"),
			'updated_at'=> date("Y-m-d h:i:s")
		]);		

        return response()->json(['status'=>$product_img]);

	}

	

  public function bank_setup()

	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$holder_name = Input::get('holder_name');

		$bank_name = Input::get('bank_name');	

		$branch = Input::get('branch');	

		$account_number = Input::get('account_number');	

		$ifsc_code = Input::get('ifsc_code');

		$existVendor = DB::table('vendor_bank_details')->where('vendor_id', '=', $vendor_id)->first();

		if($existVendor === null)

		{

			DB::table('vendor_bank_details')->insert(

			    ['vendor_id' => $vendor_id,

			     'holder_name' => $holder_name,

			     'bank_name' => $bank_name,

			 	 'branch' => $branch,

			 	 'account_number' => $account_number,

			 	 'ifsc' => $ifsc_code]

			);



			Session::flash('message', 'Bank Account Add Sucessfully!');

			return redirect()->to("/merchant/bank-account");  



		} else {



			DB::table('vendor_bank_details')

            ->where('vendor_id', $vendor_id)

            ->update(['holder_name' => $holder_name,

				     'bank_name' => $bank_name,

				 	 'branch' => $branch,

				 	 'account_number' => $account_number,

				 	 'ifsc' => $ifsc_code]);



			Session::flash('message', 'Bank details updated sucessfully!'); 



			return redirect()->to('/merchant/bank-account');



		}







	}







	



	/*********************************   NOTIFICATION ********************************/



	



	



	



	function sendNotification($target,$user_id, $title, $description, $devicetype){



		//FCM API end-point



		$url = 'https://fcm.googleapis.com/fcm/send';



		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key



		



		//$server_key = "AIzaSyC-7C512j6ohsAnFyQYptXZqOsHHYwRPgg";



		



		



		$server_key = "AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1"; 



		//$data = $param;"notification_id": "1",



		



		$data =  array("self_id"=>$user_id, "title"=>$title, "message"=>$description);



		



		$fields = array();



		$fields['data'] = $data;	



		if(is_array($target)){



		$fields['registration_ids'] = $target;



		}else{



		$fields['to'] = $target;



		}



		//header with content_type api key



		$headers = array(



		'Content-Type:application/json',



		 'Authorization:key='.$server_key



		);



		//CURL request to route notification to FCM connection server (provided by Google)	



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_URL, $url);



		curl_setopt($ch, CURLOPT_POST, true);



		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);



		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));



		$result = curl_exec($ch);



		//echo $result;die;



		if ($result === FALSE) {



		return false;



		}



		curl_close($ch);



		$jsn =json_decode($result);



		// echo $jsn->success;die;		



		if($jsn->success){



		return '1';



		}



		else{



		return '0';



		}







	}







	function send_notification_ios($token,$title,$body)



	{







				



		$url = "https://fcm.googleapis.com/fcm/send";



		



		//$serverKey = 'AIzaSyBVAHr3vvKUb8-gtvDSvfLv_-1NWITPRpY';



		$serverKey = 'AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1';



		//$title = "Title";



		//$body = "Body of the message";



		



		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');



		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



		$json = json_encode($arrayToSend);



		$headers = array();



		$headers[] = 'Content-Type: application/json';



		$headers[] = 'Authorization: key='. $serverKey;



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_URL, $url);



		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");



		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);



		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);



		



		$response = curl_exec($ch);



		



		if ($response === FALSE) {



			return '0';



			//die('FCM Send Error: ' . curl_error($ch));



		}



		else



		{



			return '1';



		}







		curl_close($ch);



		



	}



	



	function show_send_notification()



	{



		$data_onview ='';



		



		DB::enableQueryLog(); 	



		



		$user_info = '';



		



		



		$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$restaurant_detail = DB::table('restaurant')



						 ->select('*')



						 ->where('rest_status', '!=' ,'INACTIVE')



						 ->where('vendor_id', '=' ,$vendor_id)



						 ->get();



		$rest_id_array='';						 	



								 



		if($restaurant_detail)



		{	



			$rest_id_array=[];



			foreach($restaurant_detail as $rest_data)



			{



				$rest_id_array[]=$rest_data->rest_id;



			}



		}					 



						 



		if((count($rest_id_array)>0) && (!empty($rest_id_array))){				 



			$order_detail  =  DB::table('order')	;	



			$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');



			$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



			$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');	



			



			



			



			 $order_detail = $order_detail->where(function($query) use ($rest_id_array) {



			



				$l=0;



				foreach($rest_id_array as $ftype )



				{	



					if($ftype>0)



					{



						if($l>0){



							 $query = $query->orWhere('order.rest_id','=',$ftype);	



						 }



						 else



						 {



						 	 $query = $query->Where('order.rest_id','=',$ftype);	



						 }



					}						



					$l++;



				}



			



			});



			



			



			$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);



							



			$order_detail  = $order_detail->select('*');



			$order_detail  = $order_detail->orderBy('order.order_create', 'desc');



			$order_detail  = $order_detail->get();	



			



			



				



		$user_ids_array = '';



		if($order_detail)



		{



			$user_ids_array=[];



			foreach($order_detail as $order_data)



			{



				$user_ids_array[]=$order_data->user_id;



			}



				$user_ids_array = array_unique($user_ids_array);



				



			if((count($user_ids_array)>0) && (!empty($user_ids_array))){				



			$user_info = DB::table('device_token')		 



	 				->leftJoin('users', 'device_token.userid', '=', 'users.id')			



					->select('users.*','device_token.devicetoken','device_token.devicetype','device_token.deviceid'	)	



					->where('device_token.notification_status', '=' , '1')



					->where(function($query) use ($user_ids_array) {



			



						$l=0;



						foreach($user_ids_array as $ftype )



						{	



							if($ftype>0)



							{



								if($l>0){



									 $query = $query->orWhere('users.id','=',$ftype);	



								 }



								 else



								 {



									 $query = $query->Where('users.id','=',$ftype);	



								 }



							}						



							$l++;



						}



					



					})



			



			



					->orderBy('device_token.id', 'desc')



					->get();	



			}



		}	



		



		



		}



		



		$data_onview=[];			



		$data_onview['user_info']=	$user_info	;



		



		return View('rest_owner.notification_from')->with($data_onview);



	}



	



	



	function notification_action()



	{		



		



		/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/



		// print_r($_POST); die;



		



		if(isset($_POST['user_id']) && (count($_POST['user_id'])>0))



		{



		



			$c=0;



			foreach($_POST['user_id'] as $ulist)



			{



		



						$user_id = $ulist;



						$notification_title = $_POST['title'];



						$notification_description =$_POST['noti_content'];		



									



						$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->get();	



						



						



												



						$target = $token_data[0]->devicetoken;



						$deviceid = $token_data[0]->deviceid;



						$devicetype = $token_data[0]->devicetype;



						$iosadres = '';



						







						 // $this->sendNotification($target,$user_id, $notification_title, $notification_description, $devicetype);



						if($devicetype=='android')



						{



							$iosadres = $this->sendNotification($target,$user_id, $notification_title, $notification_description, $devicetype);



						}



						elseif($devicetype=='iphone')



						{







							$iosadres = $this->send_notification_ios($target,$notification_title, $notification_description);







						}



						//return $iosadres;



						 						



										



							$notifiy = new Notification_list;



							$notifiy->noti_userid = $user_id;



							$notifiy->noti_guestid = '';	



							$notifiy->noti_title = $notification_title;	



							$notifiy->noti_desc = $notification_description;	



							$notifiy->noti_deviceid = $deviceid;	



							$notifiy->noti_devicetype = $devicetype;	



							$notifiy->noti_read = '0';



							$notifiy->save();						



							



							$noti_id = $notifiy->noti_id;	



							



							$c++;



				}



				if($c==count($_POST['user_id']))



				{



								



					Session::flash('message', 'Notification Send successfully.');



				



					return redirect()->to('/vendor/send_notification');


				}


						/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/


		}
		else
		{


				Session::flash('message', 'Please select user name.');

				return redirect()->to('/vendor/send_notification');

		}


	}


    public function shopping_cart(){ 

    $vendor_id = Auth::guard('vendor')->user()->vendor_id;

	$shstatus  = DB::table('shopping_cart_status')->where('vendor_id', '=' ,$vendor_id)->first();		

  	$data_onview = array('cartstatus' => $shstatus); 

	return view('rest_owner.shopping_cart')->with($data_onview);

    }


	public function shop_action(Request $request)

	{

		$cart_status = Input::get('cart_status');

		$date = date('Y-m-d H:i:s');

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;

        $ustatus = DB::table('shopping_cart_status')->where('vendor_id', $vendor_id)->update(['status' => $cart_status,'updated_at'=> $date]);

        if($ustatus==0){

       			DB::table('shopping_cart_status')->insert(

			    ['vendor_id' => $vendor_id,

			     'status' => $cart_status,

			     'created_at' => $date,

			 	 'updated_at' => $date]

			);

        }

		Session::flash('message', 'Shopping Cart Status Updated Successfully!');

	   return redirect()->to('/merchant/shopping-cart');

	}

	
	/*****************************************************************/

public function orders_show(Request $request){

	$vendor_id = Auth::guard('vendor')->user()->vendor_id;

		$today = $request->today;
		$yesterday = $request->yesterday;
		$one_month = $request->one_month;

        $data['today'] = $request->today;
		$data['yesterday'] = $request->yesterday;
		$data['one_month'] = $request->one_month;

		//dd($today);

		if($today=="1"){

			//echo "testing1";die;

       // $today_users = DB::table('users')->whereDate('created_at', Carbon::today())->get();

      //  $today_merchant = DB::table('vendor')->whereDate('created_at', Carbon::today())->get();

       // $today_date = DB::table('orders')->whereDate('created_at', Carbon::today())->get();

     $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', Carbon::today())
    ->where('vendor_id', '=' ,$vendor_id)
    ->get();

       // $data['today_pending'] = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',0)->get();

        // $data['today_sell'] = DB::table('orders')->whereDate('created_at', Carbon::today())->sum('total');

        $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereDate('created_at', Carbon::today())->where('vendor_id', '=' ,$vendor_id)->get();

        $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')
		    ->whereDate('created_at', Carbon::today())
		    ->groupBy('date')->where('vendor_id', '=' ,$vendor_id)
		    ->get();

      //  $today_products  = DB::table('product_service')->whereDate('created_at', Carbon::today())->get();

      //  $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('rest_owner.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}


		if($yesterday=="2"){

			//echo "test";die;

			$startDate = Carbon::today();
       $endDate = Carbon::today()->subDay(7);
       $yesterdays = Carbon::yesterday();

     // $today_users = User::whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

       // $today_merchant = DB::table('vendor')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

        $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', $yesterdays)
    ->where('vendor_id', '=' ,$vendor_id)
    ->get();

       // dd()

      //  $data['today_pending'] = DB::table('orders')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->where('status',0)->get();

       $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->where('created_at', $yesterdays)->where('vendor_id', '=' ,$vendor_id)->get();

       $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupBy('date')->where('created_at', $yesterdays)->where('vendor_id', '=' ,$vendor_id)->get();

        //dd($data['today_sell']);

       // $today_products  = DB::table('product_service')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

       // $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('rest_owner.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}


		if($one_month=="3"){
			
       // $today_users = DB::table('users')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $today_merchant = DB::table('vendor')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $today_date = DB::table('orders')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

     $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
    ->where('vendor_id', '=' ,$vendor_id)
    ->get();

  //dd($data['today_date1']);
       // $data['today_pending'] = DB::table('orders')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->where('status',0)->get();

        $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->where('vendor_id', '=' ,$vendor_id)->get();

        $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->groupby('date') ->where('vendor_id', '=' ,$vendor_id)->get();

        //dd($data['today_sell']);

       // $today_products  = DB::table('product_service')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('rest_owner.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}

}

public function chart_filter_order_date(Request $request)
	{

		$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days')); 

		if((!empty($data_from)) ||(!empty($data_to))){
		
		if(!empty($order_date)){

			$data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
                     ->groupBy('x')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->where('vendor_id', '=' ,$vendor_id)
	        ->get();

	         $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor_id', '=' ,$vendor_id)->get();

	         $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupby('date')->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor_id', '=' ,$vendor_id)->get();
	        // dd($data['today_sell']);

	        $returnHTML = view('rest_owner.orders_get')->with($data)->render();
                   return response()->json($returnHTML);
	        
		}
	}

	}

}







