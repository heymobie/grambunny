<?php



namespace App\Http\Controllers\RestOwner;
use App\Http\Controllers\Controller;





Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use File;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image as Image;



use App\Template;

use App\Template_menu;

use App\Template_menu_category;

use App\Template_category_item;

use App\Template_menu_category_addon;




class OwnermenutemplateController extends Controller

{

    public function __construct(){

    	$this->middleware('vendor');

    }

	
	public function show_menutemplate_list()
	{
	
		$template_detail  = DB::table('template')->get();	
		
		$data_onview =array('template_detail'=>$template_detail);
		
		return View('rest_owner.menu_template_list')->with($data_onview);
	}
	
	
	
	public function show_menu_list()
	{
		if(Route::current()->getParameter('id'))
		{	
		   $id = Route::current()->getParameter('id');
		
		
		
	  $template_list = DB::table('template')
					->where('template_id', '=' ,$id)
					->select('*')
					->get();	
					
					
	  $menu_list = DB::table('template_menu')	
					->where('template_id', '=' ,$id)
					->orderBy('menu_order', 'asc')
					->get();

		  $menu_cat_detail = '';	  

		if(!empty($menu_list)){  			

	  $menu_cat_detail = DB::table('template_menu_category')	
	  	
					->where('template_id', '=' ,$id)

					->where('menu_id', '=' ,$menu_list[0]->menu_id)

					->select('*')

					->orderBy('menu_category_id', 'asc')

					->get();	

		}	



		$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'id'=>$id,'menu_cate_detail'=>$menu_cat_detail);
		
		
			return View('rest_owner.template.menulist')->with($data_onview);
		}
		else
		{			
			return redirect()->to('/vendore/view_menu_template');
		}
	}
	
	

	public function ajax_show_menudetail()
	{


		$menu_id = Input::get('menu');
		$template_id = Input::get('template_id');

		$template_list  = DB::table('template')->where('template_id', '=' ,$template_id)->get();	
		
		$menu_list = DB::table('template_menu')	
					->where('template_id', '=' ,$template_id)
					->orderBy('menu_order', 'asc')
					->get();

						

		$menu_detail = DB::table('template_menu')
					->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_id)
					->orderBy('menu_id', 'desc')
					->get();	

		  $menu_cat_detail = '';

		  

		if(!empty($menu_list)){  			

	  $menu_cat_detail = DB::table('template_menu_category')	
					->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_id)
					->select('*')
					->orderBy('menu_category_id', 'asc')
					->get();	

		}					

					





		$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);		

					return View('rest_owner.template.menu_list')->with($data_onview);

	}
	/* SHOW POPULAR LIST START */

	function menu_category_popularlist()
	{
		$template_id = Input::get('template_id');

		$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

		$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->orderBy('menu_order', 'asc')
					->get();
					
		$menu_detail = '';	

		  $menu_cat_detail = '';
		  
		if(!empty($menu_list)){  			

	  $menu_cat_detail = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
	  					->where('menu_cat_popular', '=' ,'1')->orderBy('menu_category_id', 'asc')
						->get();	

		}	


		$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>'','menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);	

										

			return View('rest_owner.template.popular_menu_item_list')->with($data_onview);

	}

	/* POPUPLAR LIST END */




	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS START */

	public function ajax_addons_list()
	{

		$addon_list = '';

		$template_id = Input::get('rest');
		$menu_id =Input::get('menu');
		$menu_item = Input::get('menu_itme');

	 $addon_list = DB::table('template_menu_category_addon')
					->where('addon_templateid', '=' ,$template_id)
					->where('addon_menuid', '=' ,$menu_id)
					->where('addon_menucatid', '=' ,$menu_item)
					->orderBy('addon_id', 'asc')
					->get();

	 $template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

		$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->where('menu_id', '=' ,$menu_id)
					->get();

						

		$item_detil = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
					->where('menu_category_id', '=' ,$menu_item)->where('menu_id', '=' ,$menu_id)
					->get();
		

		$template_name = $template_list[0]->template_name;

		$menu_name = $menu_list[0]->menu_name;

		$item_name = $item_detil[0]->menu_category_name;
		

			$data_onview = array('addon_list'=>$addon_list,
								 'template_name'=>$template_name,
								 'menu_name'=>$menu_name,
								 'item_name'=>$item_name,
								 'template_id'=>$template_id,
								 'menu_id'=>$menu_id,
								 'menu_item'=>$menu_item,
								 ); 		

		return View('rest_owner.template.addon_list')->with($data_onview);;
	}

	


	

	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS END */
	
	
	

}

