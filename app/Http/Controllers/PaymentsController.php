<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Braintree_Transaction;
use Session;
use App\User;

class PaymentsController extends Controller

{

	public function process(Request $request)
	{
		$amount = $request->input('amt', false);
	    $payload = $request->input('payload', false);
	    $nonce = $payload['nonce'];
	    $uwallet = $request->input('uwallet', false);
	   // $user_id = Auth::id();
	    $status = Braintree_Transaction::sale([
						'amount' => $amount,
						'paymentMethodNonce' => $nonce,
						'options' => [
						    'submitForSettlement' => True
						]
	    ]);

	    if($status->success){
	    	if($status->transaction->status=="submitted_for_settlement"){
			    
			    $card_number= $status->transaction->creditCard['last4'];
			    $name_card_order = $status->transaction->creditCard['cardType'];
			    $expire_month = $status->transaction->creditCard['expirationMonth'];
			    $expire_year = $status->transaction->creditCard['expirationYear'];
			    $transactionid = $status->transaction->id;
                $amount = $amount+$uwallet;
                Session::set('paid_wallet', $uwallet);
			    Session::set('card_number', $card_number);
			    Session::set('name_card_order', $name_card_order);
			    Session::set('expire_month', $expire_month);
			    Session::set('expire_year', $expire_year);
			    Session::set('transactionid', $transactionid);
			    Session::set('ACK', $status->success);
			    Session::set('card_status', $status->transaction->status);
			    Session::set('total_amt', $amount);

			}elseif($status->transaction->status=="settling"){
				
				$transactionid= $status->transaction->paypal['paymentId'];
			    $amount = $status->transaction->amount;
				$paypal_status = $status->transaction->status;
			    Session::set('paypal_status', $paypal_status);
			    Session::set('transactionid', $transactionid);
			    Session::set('total_amt', $amount);			    
			}
		}	
	   return response()->json($status);
	}

	public function cancel(Request $request)
	{
		$amount = $request->input('amt', false);
	    $transactionid = $request->input('transactionid', false);
		//print_r($request);		
		$result = Braintree_Transaction::void($transactionid);		
		if($result->success){
			return response()->json($result);
		}
		# true		
		# 'credit'
		//$result->transaction->amount;		
	}
}

