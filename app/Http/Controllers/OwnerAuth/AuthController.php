<?php

namespace App\Http\Controllers\OwnerAuth;

use App\Exceptions\Handler;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Vendor;

use Auth;

use Carbon\Carbon;

use DB;

use Illuminate\Contracts\Encryption\DecryptException;

 

use Illuminate\Foundation\Auth\ThrottlesLogins;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Mail;

use PHPMailer\PHPMailer;

use Session;

use Validator;



class AuthController extends Controller 

{  

    /*

    |--------------------------------------------------------------------------

    | Registration & Login Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users, as well as the

    | authentication of existing users. By default, this controller uses

    | a simple trait to add these behaviors. Why don't you explore it?

    |

    */



 



    /**

     * Where to redirect users after login / registration.

     *

     * @var string

     */

	

	

    protected $redirectTo = '/rest_owner/dashboard';

	   protected $redirectAfterLogout = '/rest_owner/login';

	     protected $guard = 'vendor';





    /**

     * Create a new authentication controller instance.

     *

     * @return void

     */

    public function __construct()

    {

		

       $this->middleware("vendorGuest", ['except' => 'logout']);

    }



    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */

	 

	 

	 public function registration()

	{

	

		return view('rest_owner.auth.register');

        

	}



    public function showLoginForm()
    {

  
        if (view()->exists('auth.authenticate')) {

            return view('auth.authenticate');

        }

        return view('rest_owner.auth.login');

    }

	

	

    protected function validator(array $data)

    {

        return Validator::make($data, [

            'name' => 'required|max:255',

            'email' => 'required|email|max:255|unique:users',

            'password' => 'required|min:6|confirmed',

        ]);

    }



    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return User

     */

    protected function create(array $data)

    {

        return User::create([

            'name' => $data['name'],

            'email' => $data['email'],

            'password' => bcrypt($data['password']),

        ]);

    }

	

	

	

	 public function forgotpwd()

	{

		if (view()->exists('auth.authenticate')) {

			return view('auth.authenticate');

		}

	

		return view('rest_owner.auth.reset');

	}

	

	function login(Request $request)
	{

        $this->validate($request,[

            "email" => "required|email",

            "password" => "required"

        ]);



        if (Auth::guard('vendor')->attempt(['email' => Input::get('email'), 

          'password' => Input::get('password')])){

            if (Auth::guard("vendor")->user()->vendor_status==0) {

                Auth::guard("vendor")->logout();

                return redirect()->back()->with("warning","Your account is not activated by administrator.");   

            } 

            if($request->remember=='on'){

                setcookie('vemail', $request->email, time() + (86400 * 30), "/"); // 86400 = 1 day

                setcookie('vpassword', $request->password, time() + (86400 * 30), "/"); // 86400 = 1 day

                setcookie('vremember',$request->remember, time() + (86400 * 30), "/"); // 86400 = 1 day    

                }else{

                setcookie('vemail', '', time() + (86400 * 30), "/"); // 86400 = 1 day

                setcookie('vpassword', '', time() + (86400 * 30), "/"); // 86400 = 1 day

                setcookie('vremember', '', time() + (86400 * 30), "/"); // 86400 = 1 day            

            } 

                $login_status = 1;

                $vendor_id = Auth::guard("vendor")->user()->vendor_id;

                DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

                DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

                return redirect()->intended(route("merchant.dashboard"));  



        }else{

            return redirect()->back()->withErrors(["email" => "Credentials do not match our records!","password" => " "])->withInput(); 

        }

 

	}



    public function sendPasswordResetLink(Request $request)

    {

        $this->validate($request,[

            "email" => "required|email"

        ]);

        $vendor = Vendor::where('email',$request->email)->first(); 

        if (!$vendor) return redirect()->back()->withErrors(['email' => 'Email address not found!'])->withInput();

        $genrateToken=str_random(60);

        DB::table('password_resets')->insert([

          'email' => $request->email,

          'token' => $genrateToken, 

          'created_at' => Carbon::now()

        ]);

        Mail::send('auth.emails.vendor-password-reset', ["firstname" => $vendor->name,"token" => encrypt($genrateToken)],function ($message) use ($vendor){

            $message->to($vendor->email);

            $message->subject('Forgot password');

            $message->from(config("app.webmail"), config("app.mailname"));

        });

        return redirect()->back()->with("success" , "Reset password link has been sent to your email address.");

    }



    public function resetPassword(Request $request)

    {

        try {

            $token=decrypt($request->token);

        } catch (DecryptException $e) {

            return view("rest_owner.auth.set_new_pass",["invalid"=>"Reset password link has been expired or Invalid!"]);

        }

        $validateToken=DB::table("password_resets")->where("token",$token)->first();

        if (!$validateToken) {

          return view("rest_owner.auth.set_new_pass")->with("invalid","Reset password link has been expired or Invalid!");

        }

        $currentTime = Carbon::now();

        if ($currentTime->diffInMinutes($validateToken->created_at) < 15) {

          return view("rest_owner.auth.set_new_pass")->with("token",$token);

        } 

        else {

          return view("rest_owner.auth.set_new_pass")->with("invalid","Reset password link has been expired or Invalid!");

        }

    

    }

    public function updatePassword(Request $request){

        try {

            $token=decrypt($request->token);

        } catch (DecryptException $e) {

            return redirect()->back()->with("invalid","Reset password link has been expired or Invalid!");

        }

 

        $this->validate($request, [

            'password' => 'required|min:6||confirmed',  

        ]);

        $password=$request->password;

        $tokenData=DB::table("password_resets")->where("token",$token)->first();

        if (is_null($tokenData)) {

            return redirect()->back()->with("invalid","Reset password link has been expired or Invalid!");

        }

        $user=Vendor::where("email",$tokenData->email)->first();

        $user->password=Hash::make($password);

        if ($user->update()) {

            $tokenData=DB::table("password_resets")->where("email",$user->email)->delete();

        } 

        return redirect()->route("merchant.login")->with("success","Password updated successfully!");

    }

	

	

	

	

	

	public function logout() {

  
    $login_status = 0;

    if(isset(Auth::guard("vendor")->user()->vendor_id)){

    $vendor_id = Auth::guard("vendor")->user()->vendor_id;

    }else{

    return redirect()->route('merchant.login');

    }

    DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

    DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

		Auth::guard('vendor')->logout();

		Session::flush();

		return redirect()->route('merchant.login');

	}


	public function show_forgotpwd()
	{

		if (view()->exists('auth.authenticate')) {

		return view('auth.authenticate');

		}

		return view('rest_owner.auth.forgot_pwd');

	}

	/*Send otp after enter email*/

public function sentOTP(Request $request)

{

 $AdminEmail = $request->input('email');

 $Admin = DB::table('vendor')->where('email',$AdminEmail)->first();

 if(!empty($Admin)){

 //    $email = $Admin->email;

 //    $name = $Admin->name;

    $Admin_id = $Admin->vendor_id;

 //    /*********** EMAIL FOR SEN OTP START *************/

    $otp_code = trim($this->generateRandomString(6));

 //    $msg_reg_otp = 'Dear '.trim($name).', The verification code for Forgot Password on Online order food is :'.$otp_code ;

 //    Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

 //       $message->from('info@gmail.com', 'Online order food');

 //       $otp_email = trim($email);

 //       $message->to($otp_email);

 //     // $message->bcc('votiveshweta@gmail.com');

 //     // $message->bcc('votivemobile.pankaj@gmail.com');

 //     // $message->bcc('votivemobile.dilip@gmail.com');

 //     // $message->bcc('zubaer.votive@gmail.com');

 //       $message->subject('Forgot Passowrd OTP');

 //   });

    // $otp_code='';

    $vndr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',2)->first();

    

    $address = $AdminEmail; //"deepaksanidhya@gmail.com";

    $subject = $vndr_fgtpss_email_cotnt->email_title;

    $message = $vndr_fgtpss_email_cotnt->email_content.$otp_code;



    $semail = $this->sendMail($address,$subject,$message);



    if(!empty($otp_code)){

     //$data=array('otp'=>$otp_code);

     DB::table('vendor')

     ->where('vendor_id',$Admin_id)

     ->update(['otp'=>$otp_code]);

     Session::flash('Succes', 'OTP has been sent successfully.');

     session()->flash('Admin_id', $Admin_id);

     //$data=array('Admin_id'=>$Admin_id);

     //return Redirect::route('/admin/password/enter_otp')->with(['data' => $data]);

     return redirect()->to('/vendor/password/enter_otp');

 }else{

    // echo "skdf";

    // die;

    Session::flash('Error', 'Email sent failed.');

    return redirect()->back();

}

    // // if (Mail::failures()){

    //          DB::table('admins')

    //  ->where('id',$Admin_id)

    //  ->update(['otp'=>$otp_code]);

    //  Session::flash('Succes', 'OTP has been sent successfully.');

    //  session()->flash('Admin_id', $Admin_id);

    // //     return redirect()->back();

    // // }

    // Session::flash('Succes', 'Email sent successfully');

    // return redirect()->to('/admin/password/enter_otp');

    //return redirect()->to();

}else{

    Session::flash('Error', 'Please enter your registered email.');

    return redirect()->back();

}

//return view('admin.auth.reset_pass');

}



/*Enter OTP */

public function VerifyOTPForm()

{

    session()->keep(['Admin_id']);

    return view('rest_owner.auth.enter_your_otp');

}

	

/*OTP submit*/

public function VerifyOTP(Request $request)

{

    $enterOTP = $request->input('user_otp');

    $AdminID = $request->input('AdminID');

    $AdminData = DB::table('vendor')->where('vendor_id',$AdminID)->first();

    $saveOTP = $AdminData->otp;

    $adminID = $AdminData->vendor_id;

    if($saveOTP==$enterOTP){

        //echo "Match";

        Session::flash('Succes', 'Your OTP has been verified successfully.');

        session()->flash('adminID', $adminID);

        return redirect()->to('/vendor/password/reset_your_password');

    }else{

        Session::flash('Otp_Error', 'OTP did not match.');

        session()->keep(['Admin_id']);

        return redirect()->back();

    }

}



public function setNewPasswordForm()

{

    session()->keep(['adminID']);

    return view('rest_owner.auth.set_new_pass');

}

public function setNewPassword(Request $request)

{

    $new_pass = $request->input('new_pass');

    $adminID = $request->input('AdminID');

    $affected = DB::table('vendor')

    ->where('vendor_id',$adminID)

    ->update(['password'=>bcrypt($new_pass)]);

    if($affected){

     //    $AdminData = DB::table('admins')->where('id',$adminID)->first();

     //    $email = $Admin->email;

     //    $name = $Admin->name;

     //    $msg_reg_otp = 'Dear '.trim($name).', Congratulations your password has been changed successfully.';

     //    Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

     //     $message->from('info@gmail.com', 'Online order food');

     //     $otp_email = trim($email);

     //     $message->to($otp_email);

     //     $message->subject('Passowrd successfully changed.');

     // });

        Session::flash('PassSucChange', 'Your password has been changed successfully.');

        return redirect('/vendor');

    }else{

        Session::flash('Change_pass', 'Somthing is wrong! please try again.');

        session()->keep(['adminID']);

        return redirect()->back();

    }

}



public function generateRandomString($length = 6) {

   $characters = '0123456789';

   $charactersLength = strlen($characters);

   $randomString = '';

   for ($i = 0; $i < $length; $i++) {

       $randomString .= $characters[rand(0, $charactersLength - 1)];

   }

   return $randomString;

}

	

	public function vendor_duplicate_email()

	{

		// Get the value from the form		

		$input['email'] = Input::get('email');

		

		// Must not already exist in the `email` column of `users` table

		

		$rules = array('email' => 'unique:vendor,email');		

		$validator = Validator::make($input, $rules);

		

		

		

		if($validator->fails()) {

			echo '1';

		}

		else {

			echo '2';

		}



	}



   public function sendMail($address,$subject,$message)

    {

        



            $mail = new PHPMailer\PHPMailer();

            $mail->IsSMTP(); // telling the class to use SMTP

            //$mail->Host       = "smtp.gmail.com"; // SMTP server

            //$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)



            $mail->SMTPAuth   = true;                 // enable SMTP authentication

            $mail->Port       = 25;                    // set the SMTP server port

            $mail->Host       = "mail.conceptline.org"; // SMTP server

            $mail->Username   = "app@grambunny.com";     // SMTP server username

            $mail->Password   = "^Lrhz018";            // SMTP server password



            $mail->SetFrom('app@grambunny.com', 'grambunny');



            $mail->Subject = $subject;



            $mail->MsgHTML($message);



            $mail->IsHTML(true); // send as HTML



            $address = $address;



            $mail->AddAddress($address);

           // $mail->AddCC('votivedeepak.php@gmail.com');



            if(!$mail->Send()) {



            //return $mail->ErrorInfo ;



            return false;   



            } else {



            return true;



            }



    }



}

