<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;





Use DB;

use Hash;

use Session;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use File;

use Intervention\Image\Facades\Image as Image;



use App\Transport;

use App\Vehicle;

use App\Vehicle_rate;

use App\Vehicle_addons;

use App\Vehicle_img;



use App\Transport_service; 

use App\Transport_bank;

use App\Transport_promo;







class AdmintransportController extends Controller

{



	public function __construct(){ 

    	$this->middleware('admin');

    }

	

	function clean_string($string){

	

		$string1 = strtolower($string);

		//Make alphanumeric (removes all other characters)

		$string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);

		//Clean up multiple dashes or whitespaces

		$string1 = preg_replace("/[\s-]+/", " ", $string1);

		//Convert whitespaces and underscore to dash

		$string1 = preg_replace("/[\s_]/", "-", $string1);

		if( !empty($string1) || $string1 != '' ){

		return $string1;

		}else{

		return $string;

		}

	

	}

	

	

	public function show_transportlist()

	{

	

		



	$rest_list = DB::table('restaurant')		

					->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')

					->select('restaurant.*','vendor.name')

					->orderBy('restaurant.rest_id', 'desc')

					->get();	

					





		$data_onview = array('rest_list' =>$rest_list);

		return View('admin.transport_list')->with($data_onview);

		

	}

	

	public function show_result()

	{

	

		/*<!--echo '<pre>';

		print_r($_POST);

		exit;-->*/

		

			

		$trans_list = DB::table('transport');		

		

		$trans_list = $trans_list->leftJoin('vendor', 'transport.transport_vendor', '=', 'vendor.vendor_id');

								

		$trans_list = $trans_list->leftJoin('vehicle', 'transport.transport_id', '=', 'vehicle.vehicle_transid');

								

		if( (Input::get('trans_cont')) && (!empty(Input::get('trans_cont'))))

		{

							

			$trans_list = $trans_list->where('transport.transport_contact', 'like' ,Input::get('trans_cont'));						

			$trans_list = $trans_list->orwhere('transport.transport_landline', 'like' ,Input::get('trans_cont'));

		}

		

		if( (Input::get('trans_name')) && (!empty(Input::get('trans_name'))))

		{

			$trans_list = $trans_list->where('transport.transport_name', 'like' ,'%'.Input::get('trans_name').'%');

		}

		

		if( (Input::get('vehicle_rego')) && (!empty(Input::get('vehicle_rego'))))

		{

			$trans_list = $trans_list->where('vehicle.vehicle_rego', 'like' ,'%'.Input::get('vehicle_rego').'%');

		}

	

		$trans_list = $trans_list->select('transport.*','vehicle.vehicle_rego','vendor.name');

		$trans_list = $trans_list->orderBy('transport.transport_id', 'desc');

		$trans_list = $trans_list->get();	

					

		$data_onview = array('trans_list' =>$trans_list);

		return View('admin.transport.transport_list')->with($data_onview);

	

	

		

	}

	

	public function show_transportfrm()

	{

	

		/*$vendor_detail = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'desc')

			->get();*/

			

		$mon_from='';		

		$mon_to='';		

		$tues_from='';		

		$tues_to='';		

		$wed_from='';		

		$wed_to='';		

		$thu_from='';		

		$thu_to='';		

		$fri_from='';		

		$fri_to='';		

		$sat_from='';		

		$sat_to='';		

		$sun_from='';		

		$sun_to='';

	

		if(Route::current()->getParameter('transport_id'))

		{

			$id = Route::current()->getParameter('transport_id');

				

			$trans_detail  = DB::table('transport')->where('transport_id', '=' ,$id)->get();	

			if(($id>0) && (!empty($trans_detail[0]->transport_mon))){

				$v = explode('_',$trans_detail[0]->transport_mon);											

				$mon_from=$v[0];

				$mon_to=$v[1];;	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_tues))){

				$v = explode('_',$trans_detail[0]->transport_tues);											

				$tues_from=$v[0];

				$tues_to=$v[1];;	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_wed))){

				$v = explode('_',$trans_detail[0]->transport_wed);											

				$wed_from=$v[0];

				$wed_to=$v[1];;	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_thus))){

				$v = explode('_',$trans_detail[0]->transport_thus);											

				$thu_from=$v[0];

				$thu_to=$v[1];;	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_fri))){

				$v = explode('_',$trans_detail[0]->transport_fri);											

				$fri_from=$v[0];

				$fri_to=$v[1];	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_sat))){

				$v = explode('_',$trans_detail[0]->transport_sat);											

				$sat_from=$v[0];

				$sat_to=$v[1];	

			}

			

			if(($id>0) && (!empty($trans_detail[0]->transport_sun))){

				$v = explode('_',$trans_detail[0]->transport_sun);											

				$sun_from=$v[0];

				$sun_to=$v[1];

			}

											

			

			

			

  		  	$data_onview = array('trans_detail' =>$trans_detail,		

								 'mon_from' =>$mon_from,		

								 'mon_to' =>$mon_to,		

								 'tues_from' =>$tues_from,		

								 'tues_to' =>$tues_to,		

								 'wed_from' =>$wed_from,		

								 'wed_to' =>$wed_to,		

								 'thu_from' =>$thu_from,		

								 'thu_to' =>$thu_to,		

								 'fri_from' =>$fri_from,		

								 'fri_to' =>$fri_to,

								 'sat_from' =>$sat_from,			

								 'sat_to' =>$sat_to,				

								 'sun_from' =>$sun_from,	

								 'sun_to' =>$sun_to,

								'id'=>$id); 		

		}

		else

		{

			$vendor_id =  Route::current()->getParameter('vendor_id');

		

			

			

			$id =0;

  		  	$data_onview = array('vendor_id' =>$vendor_id,		

								 'mon_from' =>$mon_from,		

								 'mon_to' =>$mon_to,		

								 'tues_from' =>$tues_from,		

								 'tues_to' =>$tues_to,		

								 'wed_from' =>$wed_from,		

								 'wed_to' =>$wed_to,		

								 'thu_from' =>$thu_from,		

								 'thu_to' =>$thu_to,		

								 'fri_from' =>$fri_from,		

								 'fri_to' =>$fri_to,

								 'sat_from' =>$sat_from,			

								 'sat_to' =>$sat_to,				

								 'sun_from' =>$sun_from,	

								 'sun_to' =>$sun_to,	

								 'id'=>$id); 

								 

		}

	

			return View('admin.transport_form')->with($data_onview);

	}

	

	public function transport_action(Request $request)

	{

		

		/*echo '<pre>';

		print_r($_POST);

		exit;*/

		$meta = $this->clean_string(trim(Input::get(['transport_name'])));

		

		$transport_id = Input::get('transport_id');

		$transport_old_logo = Input::get('transport_old_logo');

		$new_fileName = '';

		

		

				

		



		

		if(isset($_FILES['transport_logo']) && (!empty($_FILES['transport_logo']['name'][0]))){

		

			

				if(!empty($transport_old_logo)){

				

			 $file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/transport/'.$transport_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/transport/'.$transport_old_logo;

			

					if(file_exists($file_remove)){	

					 unlink($file_remove);

					}





				}

		

		

		        $destinationPath = 'uploads/transport/';

				$image = $request->file('transport_logo');

				$extension 		= 	$image->getClientOriginalExtension();

    			$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();

			

			

				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

				{

					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();

					

					$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/transport/'.$thumbName);; 

					asset($destinationPath . $thumbName);

					

					$new_fileName = $thumbName;

				}

				else

				{

					Session::flash('message_error', 'Image type is invalide!'); 

					

					if($rest_id>0){

					return redirect()->to('/admin/transport-update/'.$rest_id);

					}

					else

					{

						return redirect()->to('/admin/transport-form/'.Input::get('vendor_id'));

					}

				}

				

		}

		elseif(!empty($transport_old_logo))

		{

			 $new_fileName = $transport_old_logo;

		}

  

  

	

	  

	  if(Input::get('close_day')){

	   $transport_close = implode(',',Input::get('close_day'));

	  }

	  else

	  {

		  $transport_close ='';

	  }

	  

  

  

		

		if($transport_id==0)

		{

		

			

		

				

				$rest = new Transport;

				$rest->transport_name = Input::get('transport_name');

				$rest->transport_vendor =Input::get('transport_vendor');

				$rest->transport_address = Input::get('transport_address');

				$rest->transport_city = Input::get('transport_city');

				$rest->transport_pcode =  Input::get('transport_pcode');

				$rest->transport_desc =  Input::get('transport_desc');

				$rest->transport_logo = $new_fileName;

				$rest->transport_state = Input::get('transport_state');

				$rest->transport_suburb =Input::get('transport_suburb');

				$rest->transport_address2 =Input::get('transport_address2');

				$rest->transport_contact =Input::get('transport_contact');

			

				

										

				$rest->transport_landline =Input::get('transport_landline');				

				$rest->transport_commission =Input::get('transport_commission');

				$rest->transport_email =Input::get('transport_email');				

				$rest->transport_status =Input::get('transport_status');				

				$rest->transport_classi =Input::get('transport_classi');

				

				$rest->transport_metatag =$meta;

				

				

				

				

				

				

				

				$rest->transport_mon =Input::get('mon_from').'_'.Input::get('mon_to');

				$rest->transport_tues =Input::get('tues_from').'_'.Input::get('tues_to');

				$rest->transport_wed =Input::get('wed_from').'_'.Input::get('wed_to');

				$rest->transport_thus =Input::get('thu_from').'_'.Input::get('thu_to');

				$rest->transport_fri =Input::get('fri_from').'_'.Input::get('fri_to');

				$rest->transport_sat =Input::get('sat_from').'_'.Input::get('sat_to');

				$rest->transport_sun =Input::get('sun_from').'_'.Input::get('sun_to');

				$rest->transport_close =$transport_close;

				

				

				

				

				

				$rest->save();

				$transport_id = $rest->transport_id;

				

				Session::flash('message', 'Transport Inserted Sucessfully!'); 

				//return redirect()->to('/admin/restaurant_list');

				return redirect()->to('/admin/vendor_profile/'.Input::get('transport_vendor'));

			

		}

		else

		{

			DB::table('transport')

            ->where('transport_id', $transport_id)

            ->update(['transport_name' =>Input::get('transport_name'),

					  'transport_vendor' =>Input::get('transport_vendor'),

					  'transport_address'=>Input::get('transport_address'),

					  'transport_city'=>Input::get('transport_city'),

					  'transport_pcode'=>Input::get('transport_pcode'),

					  'transport_desc'=>Input::get('transport_desc'),

					  'transport_state'=>Input::get('transport_state'),

					  'transport_suburb'=>Input::get('transport_suburb'),

					  'transport_address2'=>Input::get('transport_address2'),

					  'transport_contact'=>Input::get('transport_contact'),

					  

					  

					  'transport_mon'=>Input::get('mon_from').'_'.Input::get('mon_to'),

					  'transport_tues'=>Input::get('tues_from').'_'.Input::get('tues_to'),

					  'transport_wed'=>Input::get('wed_from').'_'.Input::get('wed_to'),

					  'transport_thus'=>Input::get('thu_from').'_'.Input::get('thu_to'),

					  'transport_fri'=>Input::get('fri_from').'_'.Input::get('fri_to'),

					  'transport_sat'=>Input::get('sat_from').'_'.Input::get('sat_to'),

					  'transport_sun'=>Input::get('sun_from').'_'.Input::get('sun_to'),

					  

					  

					  

					  'transport_landline'=>Input::get('transport_landline'),

					  'transport_commission'=>Input::get('transport_commission'),

					  'transport_email'=>Input::get('transport_email'),

					  'transport_classi'=>Input::get('transport_classi'),

				

					  'transport_close'=>$transport_close,

				

					  'transport_logo'=>$new_fileName,

					  'transport_metatag' => $meta,

					  

					  'transport_status'=>Input::get('transport_status')

					 ]);

					 

				

				session()->flash('message', 'Transpot Update Sucessfully!'); 

				//return redirect()->to('/admin/restaurant_list');

				

				return redirect()->to('/admin/transport_view/'.$transport_id);

		}

	}

	

	

	public function show_transportdetail()

	{

		

	   $id = Route::current()->getParameter('transport_id');

	  

	  $rest_list = DB::table('transport')		

					->leftJoin('vendor', 'transport.transport_vendor', '=', 'vendor.vendor_id')

					->where('transport.transport_id', '=' ,$id)

					->select('transport.*','vendor.name')

					->orderBy('transport.transport_id', 'desc')

					->get();	

					

		

					

					

	  $service_list = DB::table('transport_service')		

					->where('tport_service_transid', '=' ,$id)

					->orderBy('tport_service_id', 'desc')

					->get();	

					

	$promo_list = DB::table('transport_promo')		

					->where('tport_promo_transid', '=' ,$id)

					->orderBy('tport_promo_id', 'desc')

					->get();

						

	$bank_list = DB::table('transport_bank')		

				->where('tprot_bank_transid', '=' ,$id)

				->orderBy('tprot_bank_id', 'desc')

				->get();		

				

				

					

	

	  $vehicle_list = DB::table('vehicle')		

					->where('vehicle_transid', '=' ,$id)

					->orderBy('vehicle_order', 'asc')

					->get();					

		  $vehicle_rate_detail = '';

		  $vehicle_detail = '';

		if(!empty($vehicle_list)){  

			

$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$id)

						->where('vehicle_id', '=' ,$vehicle_list[0]->vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();

						

											

 $vehicle_rate_detail = DB::table('vehicle_rate')		

					->where('vehicle_rate.rate_transid', '=' ,$id)

					->where('vehicle_rate.rate_vehicleid', '=' ,$vehicle_list[0]->vehicle_id)

					->select('*')

					->orderBy('vehicle_rate.rate_id', 'asc')

					->get();	

		}					

					





		$data_onview = array('transport_detail' =>$rest_list,'vehicle_list'=>$vehicle_list,'vehicle_detail'=>$vehicle_detail,'id'=>$id,'rate_detail'=>$vehicle_rate_detail,'service_list'=>$service_list,'promo_list'=>$promo_list,'bank_list'=>$bank_list);

		return View('admin.transport_view')->with($data_onview);

		

	

	}

	

	

	

	

	/* SERVICE AREA TABE START */

	public function ajax_service_form()

	{

	

		

		$transport_id =  Input::get('transport_id');	

		$id =0;

		

		$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();		

		

		$service_detail = '';

		

		if(Input::get('service_id'))

		{

			$service_id = Input::get('service_id');

			$id = Input::get('service_id');

			$service_detail  = DB::table('transport_service')->where('tport_service_id', '=' ,$service_id)->get();	 

		}

  		 $data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'service_detail'=>$service_detail,'id'=>$id); 

		return View('admin.transport.service_form')->with($data_onview);

	}

	

	public function service_action(Request $request)

	{	

		/*echo '<pre>';

		print_r($_POST);

		exit;*/

	

		$service_id = Input::get('service_id');

  		

		if(Input::get('from')=='back'){

			$service_id = Input::get('service_id');

			$transport_id =  Input::get('service_restid');

			

			$service_list = DB::table('transport_service')		

						->where('tport_service_transid', '=' ,$transport_id)

						->orderBy('tport_service_id', 'desc')

						->get();

			$data_onview = array('transport_id' =>$transport_id,'service_list'=>$service_list); 	

			

			return View('admin.transport.service_list')->with($data_onview);

			

		}

		else

		{

			if($service_id==0)

			{

			

				

				$service = new Transport_service;

				$service->tport_service_transid = Input::get('service_restid');

				$service->tport_service_suburb =Input::get('service_suburb');

				$service->tport_service_postcode = Input::get('service_postcode');

				$service->tport_service_charge = Input::get('service_charge');

				$service->tport_service_status =  Input::get('service_status');

				

				$service->save();

				$service_id = $service->tport_service_id;

				

				Session::flash('serivce_message', 'Transport Service Inserted Sucessfully!'); 

				

				$transport_id =  Input::get('service_restid');	

				

				

				if(Input::get('from')=='submit'){

				

				 $service_list = DB::table('transport_service')		

								->where('tport_service_transid', '=' ,$transport_id)

								->orderBy('tport_service_transid', 'desc')

								->get();

					$data_onview = array('transport_id' =>$transport_id,'service_list'=>$service_list); 	

					

					return View('admin.transport.service_list')->with($data_onview);

				}

				elseif(Input::get('from')=='addnext'){		

					

					$id =0;					

					$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();

					

					$data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'id'=>$id); 		

					return View('admin.transport.service_form')->with($data_onview);

				}

				

				

			

			}

			else

			{

			

				$service_id = Input::get('service_id');

				$transport_id =  Input::get('service_restid');

				if(Input::get('from')=='update'){

				

				DB::table('transport_service')

				->where('tport_service_id', $service_id)

				->update(['tport_service_suburb' =>Input::get('service_suburb'),

						  'tport_service_postcode'=>Input::get('service_postcode'),

						  'tport_service_charge'=>Input::get('service_charge'),

						  'tport_service_status'=>Input::get('service_status')

						 ]);

						 

					

					Session::flash('serivce_message', 'Tranpsort Service Update Sucessfully!'); 

					

				}

				

				  $service_list = DB::table('transport_service')		

								->where('tport_service_transid', '=' ,$transport_id)

								->orderBy('tport_service_transid', 'desc')

								->get();

						$data_onview = array('transport_id' =>$transport_id,'service_list'=>$service_list); 	

						

						return View('admin.transport.service_list')->with($data_onview);

				

			}

		}	

	}

	

	/* SERVICE AREA TABE END */

	

	

	

	/* PROMOSTIONS TABE START */

	

	

	public function ajax_promo_form()

	{

		

		$transport_id =  Input::get('transport_id');	

		$id =0;

		

		$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();		

		

		$promo_detail = '';

		

		if(Input::get('promo_id'))

		{

			$promo_id = Input::get('promo_id');

			$id = Input::get('promo_id');

			$promo_detail  = DB::table('transport_promo')->where('tport_promo_id', '=' ,$promo_id)->get();	 

		}

  		 $data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'promo_detail'=>$promo_detail,'id'=>$id); 

		return View('admin.transport.promo_form')->with($data_onview);

	}

	

	public function promo_action(Request $request)

	{	

	

	

		$promo_id = Input::get('promo_id');

		

		$transport_id =  Input::get('promo_restid');	

  

  		if(Input::get('from')=='back'){

				$promo_id = Input::get('promo_id');

				$transport_id =  Input::get('promo_restid');

				

				$promo_list = DB::table('transport_promo')		

							->where('tport_promo_transid', '=' ,$transport_id)

							->orderBy('tport_promo_id', 'desc')

							->get();

				$data_onview = array('transport_id' =>$transport_id,'promo_list'=>$promo_list); 	

				

				return View('admin.transport.promo_list')->with($data_onview);

				

			

		}

		else

		{

		

			if($promo_id==0)

			{

			

				

				$promo = new Transport_promo;

				$promo->tport_promo_transid = Input::get('promo_restid');

				$promo->tport_promo_on =Input::get('promo_on');

				$promo->tport_promo_desc = Input::get('promo_desc');

				$promo->tport_promo_mode = Input::get('promo_mode');

				$promo->tport_promo_value	 =  Input::get('promo_value');

				$promo->tport_promo_start	 =  Input::get('promo_start');

				$promo->tport_promo_end	 =  Input::get('promo_end');

				$promo->tport_promo_status =  Input::get('promo_status');

				

				$promo->save();

				$promo_id = $promo->tport_promo_id;

				

				Session::flash('promo_message', 'Transport Promostion Inserted Sucessfully!'); 

				

				$transport_id =  Input::get('promo_restid');	

				

				

				if(Input::get('from')=='submit'){

				

				 $promo_list = DB::table('transport_promo')		

								->where('tport_promo_transid', '=' ,$transport_id)

								->orderBy('tport_promo_id', 'desc')

								->get();

					$data_onview = array('transport_id' =>$transport_id,'promo_list'=>$promo_list); 	

					

					return View('admin.transport.promo_list')->with($data_onview);

				}

				elseif(Input::get('from')=='addnext'){		

					

					$id =0;					

					$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();		

					

					$data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'id'=>$id); 		

					return View('admin.transport.promo_form')->with($data_onview);

				}

				

			}

			else

			{

			

				$promo_id = Input::get('promo_id');

				$transport_id =  Input::get('promo_restid');

				if(Input::get('from')=='update'){			

				

					DB::table('transport_promo')

						->where('tport_promo_id', $promo_id)

						->update(['tport_promo_transid' =>Input::get('promo_restid'),

								  'tport_promo_on' =>Input::get('promo_on'),

								  'tport_promo_desc' =>Input::get('promo_desc'),

								  'tport_promo_mode' =>Input::get('promo_mode'),

								  'tport_promo_value' =>Input::get('promo_value'),

								  'tport_promo_start' =>Input::get('promo_start'),

								  'tport_promo_end' =>Input::get('promo_end'),

								  'tport_promo_status' =>Input::get('promo_status')

						 ]);

						 

						 

					Session::flash('promo_message', 'Transport Promostion Update Sucessfully!'); 

				}

				

				$promo_list = DB::table('transport_promo')		

							->where('tport_promo_transid', '=' ,$transport_id)

							->orderBy('tport_promo_id', 'desc')

							->get();

				$data_onview = array('transport_id' =>$transport_id,'promo_list'=>$promo_list); 	

				

				return View('admin.transport.promo_list')->with($data_onview);

				

			}

		}	

	}

		

		

	/* PROMOSTION TABE END */

	

	

	/* BANKDETAIL TABE START */

	

	

	public function ajax_bank_form()

	{

		

		$transport_id =  Input::get('transport_id');	

		$id =0;

		

		$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();		

		

		$bank_detail = '';

		

		if(Input::get('bank_id'))

		{

			$bank_id = Input::get('bank_id');

			$id = Input::get('bank_id');

			$bank_detail  = DB::table('transport_bank')->where('tprot_bank_id', '=' ,$bank_id)->get();	 

		}

  		 $data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'bank_detail'=>$bank_detail,'id'=>$id); 

		return View('admin.transport.bank_form')->with($data_onview);

	}

	

	public function bank_action(Request $request)

	{	

	

		$bank_id = Input::get('bank_id');	

		$transport_id = Input::get('bank_restid');

  

		if(Input::get('from')=='back'){

				$bank_id = Input::get('bank_id');

				$transport_id =  Input::get('bank_restid');				

				$bank_list = DB::table('transport_bank')		

							->where('tprot_bank_transid', '=' ,$transport_id)

							->orderBy('tprot_bank_id', 'desc')

							->get();

				$data_onview = array('transport_id' =>$transport_id,'bank_list'=>$bank_list); 	

				

				return View('admin.transport.bank_list')->with($data_onview);

		}

		else

		{

			if($bank_id==0)

			{

			

			

				$bank_active = DB::table('transport_bank')		

								->where('tprot_bank_transid', '=' ,$transport_id)

								->where('tprot_bank_status', '=' ,'1')

								->orderBy('tprot_bank_id', 'desc')

								->get();

								

				if(!empty($bank_active) && (Input::get('bank_status')=='1'))				

				{

					DB::table('transport_bank')->where('tprot_bank_status', '1')->update(['tprot_bank_status' =>'0']);				

				}

				

					

					$bank= new Transport_bank;

					$bank->tprot_bank_transid = Input::get('bank_restid');

					$bank->tprot_bank_uname =Input::get('bank_uname');

					$bank->tprot_bank_name = Input::get('bank_name');

					$bank->tprot_bank_branch = Input::get('bank_branch');

					$bank->tprot_bank_bsb	 =  Input::get('bank_bsb');

					$bank->tprot_bank_acc	 =  Input::get('bank_acc');

					$bank->tprot_bank_status	 =  Input::get('bank_status');

					

					$bank->save();

					$bank_id = $bank->tprot_bank_id;

					

					Session::flash('bank_message', 'Transport Bank Inserted Sucessfully!'); 

					

					$transport_id =  Input::get('bank_restid');	

					

					

					if(Input::get('from')=='submit'){

					

					 $bank_list = DB::table('transport_bank')		

									->where('tprot_bank_transid', '=' ,$transport_id)

									->orderBy('tprot_bank_id', 'desc')

									->get();

						$data_onview = array('transport_id' =>$transport_id,'bank_list'=>$bank_list); 	

						

						return View('admin.transport.bank_list')->with($data_onview);

					}

					elseif(Input::get('from')=='addnext'){		

						

						$id =0;					

						

					$transport_detail  = DB::table('transport')->where('transport_id', '=' ,$transport_id)->get();	

						

						$data_onview = array('transport_id' =>$transport_id,'transport_detail'=>$transport_detail,'id'=>$id); 		

						return View('admin.transport.bank_form')->with($data_onview);

					}

					

					

				

			}

			else

			{

			

				$bank_id = Input::get('bank_id');

				$transport_id =  Input::get('bank_restid');

				if(Input::get('from')=='update'){	

				

				

				$bank_active = DB::table('transport_bank')		

								->where('tprot_bank_transid', '=' ,$transport_id)

								->where('tprot_bank_status', '=' ,'1')

								->orderBy('tprot_bank_id', 'desc')

								->get();

								

				if(!empty($bank_active) && (Input::get('bank_status')=='1'))				

				{

					DB::table('transport_bank')->where('tprot_bank_status', '1')->update(['tprot_bank_status' =>'0']);				

				}

				

					DB::table('transport_bank')

						->where('tprot_bank_id', $bank_id)

						->update([

								  'tprot_bank_transid' =>Input::get('bank_restid'),

								  'tprot_bank_uname' =>Input::get('bank_uname'),

								  'tprot_bank_name' => Input::get('bank_name'),

								  'tprot_bank_branch' => Input::get('bank_branch'),

								  'tprot_bank_bsb' =>  Input::get('bank_bsb'),

								  'tprot_bank_acc' =>  Input::get('bank_acc'),

								  'tprot_bank_status' => Input::get('bank_status')

						 ]);

						 

						 

					Session::flash('bank_message', 'Transport BankDetails Update Sucessfully!'); 

				}

				

				$bank_list = DB::table('transport_bank')		

							->where('tprot_bank_transid', '=' ,$transport_id)

							->orderBy('tprot_bank_id', 'desc')

							->get();

				$data_onview = array('transport_id' =>$transport_id,'bank_list'=>$bank_list); 	

				

				return View('admin.transport.bank_list')->with($data_onview);

				

			}

		}	

	}

	

	/* BANKDETAIL TABE END */

	

	/* VEHICLE MODULE START */

	

	function vehicle_form()

	{

		

		$transport_id =  Input::get('transport_id');

				

		$id =0;	

		$vehicle_detail='';		

		$vehicle_images='';

		$make_list  = DB::table('vehicle_make')->where('make_status', '=' ,'1')->get();	

		$cate_list  = DB::table('vehicle_cate')->where('vcate_status', '=' ,'1')->get();	

		$class_list  = DB::table('vehicle_class')->where('vclass_status', '=' ,'1')->get();	

		$model_list  = array();	

		

		if(Input::get('vehicle_id'))

		{

			$id = $vehicle_id =  Input::get('vehicle_id');	

			$vehicle_detail  = DB::table('vehicle')->where('vehicle_id', '=' ,$vehicle_id)->get();	

			$vehicle_images  = DB::table('vehicle_img')->where('vehicle_id', '=' ,$vehicle_id)->get();	

			

			

			$model_list  = DB::table('vehicle_model')->where('model_status', '=' ,'1')

							->where('model_makeid', '=' ,$vehicle_detail[0]->vehicle_make)->get();	

			

		}

		

		$data_onview = array('transport_id' =>$transport_id,

							  'vehicle_detail' =>$vehicle_detail,							  

							  'vehicle_images' =>$vehicle_images,

							  'make_list' =>$make_list,

							  'model_list' =>$model_list,

							  'cate_list' => $cate_list,

							  'class_list' =>$class_list,

							  'id'=>$id

							 );

								  

		return View('admin.transport.vehicle_form')->with($data_onview);

	}

	

	

	

	public function vehicle_action(Request $request)

	{	  

		

	/*	echo'<pre>';

		//print_r($_POST);

		print_r($_FILES);

		exit;*/

				

		

		

		if(Input::get('from')=='back'){

			

								

		   $trans_id = 	Input::get('vehicle_transid');

				

				

			$trans_detail = DB::table('transport')		

							->where('transport_id', '=' ,$trans_id)

							->orderBy('transport.transport_id', 'desc')

							->get();	

						

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();

										

			$vehicle_id=$vehicle_list[0]->vehicle_id;

											

			$vehicle_detail = DB::table('vehicle')	

							->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

							->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

							->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

							->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

							->where('vehicle_transid', '=' ,$trans_id)

							->where('vehicle_id', '=' ,$vehicle_id)

							->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

							->orderBy('vehicle_order', 'asc')

							->get();	

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}		



					

		$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);		

						

									

					return View('admin.transport.vehicle_list')->with($data_onview);

					

					

			

		}

		else

		{

		

			

			

			$vehicle_id = Input::get('vehicle_id');

			

			

			

			if($vehicle_id==0)

			{

			

			

			

				$trans_id =  Input::get('vehicle_transid');

				

						

	$vehicle_max_value  = DB::table('vehicle')	

						->where('vehicle_transid', '=' ,$trans_id)

						->select(\DB::raw('MAX(vehicle.vehicle_order) as max_order'))

						->get();	

					$max_order_no = $vehicle_max_value[0]->max_order; 

				

				

					

					$vehicle = new Vehicle;

					$vehicle->vehicle_transid = Input::get('vehicle_transid');

					$vehicle->vehicle_rego =  Input::get('vehicle_rego');

					$vehicle->vehicle_make =  Input::get('vehicle_make');

					$vehicle->vehicle_model =  Input::get('vehicle_model');

					$vehicle->vehicle_year =  Input::get('vehicle_year');

					$vehicle->vehicle_catid =  Input::get('vehicle_catid');

					$vehicle->vehicle_classid =  Input::get('vehicle_classid');

					$vehicle->vehicle_pax =  Input::get('vehicle_pax');

					$vehicle->vehicle_largebag =  Input::get('vehicle_largebag');

					$vehicle->vehicle_smallbag =  Input::get('vehicle_smallbag');

					$vehicle->vehicle_minrate =  Input::get('vehicle_minrate');

					$vehicle->vehicle_ac =  Input::get('vehicle_ac');

					$vehicle->vehicle_music =  Input::get('vehicle_music');

					$vehicle->vehicle_video =  Input::get('vehicle_video');

					$vehicle->vehcile_desc =  Input::get('vehcile_desc');

					$vehicle->vehicle_drvierfname =  Input::get('vehicle_drvierfname');

					$vehicle->vehicle_driverlanme =  Input::get('vehicle_driverlanme');

					$vehicle->vehicle_mobno =  Input::get('vehicle_mobno');

					$vehicle->vehicle_allownce =  Input::get('vehicle_allownce');

					$vehicle->vehicle_status =  Input::get('vehicle_status');

					$vehicle->vehicle_order =  ($max_order_no+1);

					$vehicle->save();

					$vehicle_id = $vehicle->vehicle_id;

					

					if(isset($_FILES['other_images']) && (!empty($_FILES['other_images']['name'][0]))){

				

					if(empty($_POST['del_file'])){

						$all_del_file[] = '';

					}

					else{

						$all_del_file = explode(',',$_POST['del_file']);

					}

			

					$files = $request->file('other_images');

					$names = [];

				

					$cover =0;

						foreach ($files as $file) {

						$ff = $file->getClientOriginalName() ;

						

						if(!in_array($ff, $all_del_file))		

						{

							$img_name = '';

							$is_cover =0;

							$names[] = $file->getClientOriginalName();

							

					

							$destinationPath = 'uploads/vehicle/';

							$filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();

							

							

							 $upload_success = $file->move($destinationPath, $filename);

							

							

							Image::make('uploads/vehicle/'.$filename)->resize('250','250')->save('uploads/vehicle/thumb/'.$filename);

							

							  $listing_name =  asset($destinationPath . $filename);

							  

							  if($cover==0){$is_cover =1;}

							

							 DB::table('vehicle_img')->insert(

								['vehicle_id' => $vehicle_id , 'imgPath' => $filename,'isCover'=>$is_cover]

							);

						 

							$cover ++;

						} // END OF IF 	

					  }

					//return $names;

				

				}

					

					

					

					

		

	

	

	

	

					

					Session::flash('menu_message', 'Transport vehicle Inserted Sucessfully!'); 

					

					

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}					

							

						

						

					if(Input::get('from')=='submit'){			

			

						$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);

					}

					elseif(Input::get('from')=='addnext'){	

					

					$make_list  = DB::table('vehicle_make')->where('make_status', '=' ,'1')->get();	

					$cate_list  = DB::table('vehicle_cate')->where('vcate_status', '=' ,'1')->get();	

					$class_list  = DB::table('vehicle_class')->where('vclass_status', '=' ,'1')->get();	

					$model_list  = array();	

					

						

			

			

			$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail,

							  'make_list' =>$make_list,

							  'model_list' =>$model_list,

							  'cate_list' => $cate_list,

							  'class_list' =>$class_list,);	

						

						return View('admin.transport.vehicle_list_form')->with($data_onview);

					}

				

				

			}

			else

			{

								

				$trans_id = Input::get('vehicle_transid');

				$vehicle_id = Input::get('vehicle_id');

				if(Input::get('from')=='update'){

					

					DB::table('vehicle')

						->where('vehicle_id', $vehicle_id)

						->update([	

								  'vehicle_transid' => Input::get('vehicle_transid'),

								  'vehicle_rego' =>  Input::get('vehicle_rego'),

								  'vehicle_make' =>  Input::get('vehicle_make'),

								  'vehicle_model' =>  Input::get('vehicle_model'),

								  'vehicle_year' =>  Input::get('vehicle_year'),

								  'vehicle_catid' =>  Input::get('vehicle_catid'),

								  'vehicle_classid' => Input::get('vehicle_classid'),

								  'vehicle_pax' => Input::get('vehicle_pax'),

								  'vehicle_largebag' =>  Input::get('vehicle_largebag'),

								  'vehicle_smallbag' => Input::get('vehicle_smallbag'),

								  'vehicle_minrate' =>  Input::get('vehicle_minrate'),

								  'vehicle_ac' => Input::get('vehicle_ac'),

								  'vehicle_music' =>  Input::get('vehicle_music'),

								  'vehicle_video' => Input::get('vehicle_video'),

								  'vehcile_desc' =>  Input::get('vehcile_desc'),

								  'vehicle_drvierfname' =>  Input::get('vehicle_drvierfname'),

								  'vehicle_driverlanme' =>  Input::get('vehicle_driverlanme'),

								  'vehicle_mobno' => Input::get('vehicle_mobno'),

								  'vehicle_allownce' =>  Input::get('vehicle_allownce'),

								  'vehicle_status' =>  Input::get('vehicle_status')

						 ]);

						 

						 

						 

						 			

			//**Upload multipal image upload and resize**/

			

				if(isset($_FILES['other_images']) && (!empty($_FILES['other_images']['name'][0]))){

				

				$all_del_file = explode(',',$_POST['del_file']);

		

				$files = $request->file('other_images');

				$names = [];

			

					foreach ($files as $file) {

					$ff = $file->getClientOriginalName() ;

					if(!in_array($ff, $all_del_file))		

					{

						$img_name = '';

						$names[] = $file->getClientOriginalName();

						

				

						$destinationPath = 'uploads/vehicle/';

						$filename = md5(microtime() . $file->getClientOriginalName()) . "." . $file->getClientOriginalExtension();

						

						

						 $upload_success = $file->move($destinationPath, $filename);

						

						

						Image::make('uploads/vehicle/'.$filename)->resize('250','250')->save('uploads/vehicle/thumb/'.$filename);

						  $listing_name =  asset($destinationPath . $filename);

						  

						 

						

						 DB::table('vehicle_img')->insert(

							['vehicle_id' => $vehicle_id , 'imgPath' => $filename]

						);

					 

					} // END OF IF 	

				  }

				//return $names;

				

				}

	

				

			if(isset($_POST['isCover']) && !empty($_POST['isCover']))

			{

				DB::table('vehicle_img')

				->where('vehicle_id',$vehicle_id)

				->update(['isCover' => '0'

							]);

						

				DB::table('vehicle_img')

				->where('id',Input::get('isCover'))

				->update(['isCover' => '1'

							]);

			}



	



					

						session()->flash('menu_message', 'Vehicle Updated Sucessfully!');

					}

					

									

									

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

					$rate_detail = DB::table('vehicle_rate')	

								->where('rate_transid', '=' ,$trans_id)

								->where('rate_vehicleid', '=' ,$vehicle_id)

								->select('*')

								->orderBy('rate_vehicleid', 'asc')

								->get();	

					}	

					

					

		$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);			

					

					

			}

		}

	}

	

	

	function ajax_vehicle_detail()

	{

		

		//echo '<pre>';

		//print_r($_POST);

		

		$vehicle_id = Input::get('vehicle_id');

		$trans_id = Input::get('transport_id');

		

					

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}					

											

					



		$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);

	

	}

	

	function ajax_update_sortorder()

	{

		

		//echo '<pre>';

		//print_r($_POST);

		foreach ($_POST['listItem'] as $position => $item)

		{

			//echo  "position = ".$position." ITEM ".$item.'<br>'; 

			

			

			DB::table('vehicle')

            ->where('vehicle_transid', Input::get('trans_id'))

            ->where('vehicle_id', $item)

            ->update(['vehicle_order' =>($position+1)

					 ]);

					 

					 

		}



	

	}

	

	

	

	

	function ajax_vehicle_charge_form()

	{

		

	

		$trans_id =  Input::get('trans_id');	

		$vehicle_id =  Input::get('vehicle_id');	

		$rate_id =0;

		$charg_detail = '';

		

		$vehicle_detail = DB::table('vehicle')		

					->where('vehicle_id', '=' ,$vehicle_id)

					->get();	

					

		if(Input::get('rate_id'))

		{

				$rate_id =Input::get('rate_id');

		$charg_detail = DB::table('vehicle_rate')		

					->where('rate_transid', '=' ,$trans_id)

					->where('rate_vehicleid', '=' ,$vehicle_id)

					->where('rate_id', '=' ,$rate_id)

					->get();

			

		}			

					

					

  		 $data_onview = array('trans_id' =>$trans_id,

		 					  'vehicle_id' =>$vehicle_id,

		 					  'vehicle_rego' =>$vehicle_detail[0]->vehicle_rego,

		 					  'charg_detail' =>$charg_detail,

							  'rate_id'=>$rate_id); 

		return View('admin.transport.vehicle_charge_form')->with($data_onview);

		

	

	}

	

	

	

	 

	public function vehicle_charge_action(Request $request)

	{	  

	

		/*echo '<pre>';

		print_r($_POST);

		exit;*/

		

		

		$rate_id = Input::get('rate_id');

		

		if(Input::get('from')=='back'){

		

			$vehicle_id = Input::get('rate_vehicleid');

					$trans_id = Input::get('rate_transid');

		

					

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}					

											

					

				

			$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);

		}

		else

		{		

			if($rate_id==0)

			{

			

					

					$rate = new Vehicle_rate;

					$rate->rate_transid = Input::get('rate_transid');

					$rate->rate_vehicleid =  Input::get('rate_vehicleid');

					$rate->rate_name =  Input::get('rate_name');

					$rate->rate_desc =  Input::get('rate_desc');

					$rate->rate_diff =  Input::get('rate_diff');

					$rate->rate_lname =  Input::get('rate_lname');

					$rate->rate_lprice =  Input::get('rate_lprice');

					$rate->rate_lstatus = Input::get('rate_lstatus');

					$rate->rate_mname = Input::get('rate_mname');

					$rate->rate_mprice = Input::get('rate_mprice');

					$rate->rate_mstatus = Input::get('rate_mstatus');

					$rate->rate_sname = Input::get('rate_sname');

					$rate->rate_sprice = Input::get('rate_sprice');

					$rate->rate_sstatus = Input::get('rate_sstatus');

					$rate->rate_price = trim(Input::get('rate_price'));

					$rate->rate_status = Input::get('rate_status');

					$rate->save();

					$rate_id = $rate->rate_id;

					

					

					

					

					Session::flash('menu_message', 'Vehicle Rate Inserted Sucessfully!'); 

					//return redirect()->to('/admin/menu_categorylist');

					

					$rest_id = Input::get('rate_transid');

			

					

					/* START */

					$vehicle_id = Input::get('rate_vehicleid');

					$trans_id = Input::get('rate_transid');

		

					

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}					

											

					



		

					/* END */

					 

					 

					 

					

					if(Input::get('from')=='submit'){

					

			$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);

					}

					elseif(Input::get('from')=='addnext'){	

					

			$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail,'rate_id'=>'0');				

					return View('admin.transport.vehicle_charge_list_form')->with($data_onview);

						

					}

				

			}

			else

			{

			

			

				if(Input::get('from')=='update'){			

				DB::table('vehicle_rate')

				->where('rate_id', $rate_id)

				->update(['rate_transid'=>Input::get('rate_transid'),

						  'rate_vehicleid'=>  Input::get('rate_vehicleid'),

						  'rate_name'=>  Input::get('rate_name'),

						  'rate_desc'=> Input::get('rate_desc'),

						  'rate_diff'=> Input::get('rate_diff'),

						  'rate_lname'=>  Input::get('rate_lname'),

						  'rate_lprice'=>  Input::get('rate_lprice'),

						  'rate_lstatus'=> Input::get('rate_lstatus'),

						  'rate_mname'=> Input::get('rate_mname'),

						  'rate_mprice'=>Input::get('rate_mprice'),

						  'rate_mstatus'=> Input::get('rate_mstatus'),

						  'rate_sname'=> Input::get('rate_sname'),

						  'rate_sprice'=> Input::get('rate_sprice'),

						  'rate_sstatus'=> Input::get('rate_sstatus'),

						  'rate_price'=> Input::get('rate_price'),

						  'rate_status'=> Input::get('rate_status')

						 ]);

				

											 

					session()->flash('menu_message', 'Vehicle rates Updated Sucessfully!'); 	

						 

				}

				

					/* START */

					$vehicle_id = Input::get('rate_vehicleid');

					$trans_id = Input::get('rate_transid');

		

					

		$trans_detail = DB::table('transport')		

						->where('transport_id', '=' ,$trans_id)

						->orderBy('transport.transport_id', 'desc')

						->get();	

											

		$vehicle_detail = DB::table('vehicle')	

						->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')

						->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')

						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')

						->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')						

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->select('vehicle.*', 'vehicle_make.make_name', 'vehicle_model.model_name', 'vehicle_cate.vcate_name', 'vehicle_class.vclass_name')

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

					

			$vehicle_list = DB::table('vehicle')		

						->where('vehicle_transid', '=' ,$trans_id)

						->orderBy('vehicle_order', 'asc')

						->get();	

						

						

								

			  $rate_detail = '';

			  

			if(!empty($vehicle_list)){  			

			$rate_detail = DB::table('vehicle_rate')	

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->select('*')

						->orderBy('rate_vehicleid', 'asc')

						->get();	

			}					

											

			$data_onview = array('trans_detail' =>$trans_detail,'vehicle_list'=>$vehicle_list,'trans_id'=>$trans_id,'vehicle_id'=>$vehicle_id,'rate_detail'=>$rate_detail,'vehicle_detail'=>$vehicle_detail);	

			

						return View('admin.transport.vehicle_list')->with($data_onview);

				

				

				

				

					

			}

		}

	}

	

	

	

	

	public function ajax_addons_list()

	{

		

		

		$addon_list = '';

		

		$trans_id = Input::get('trans_id');

		$vehicle_id =Input::get('vehicle_id');

		$rate_id = Input::get('rate_id');

		

		

			

	 $addon_list = DB::table('vehicle_addons')		

					->where('addon_transid', '=' ,$trans_id)

					->where('addon_vehicleid', '=' ,$vehicle_id)

					->where('addon_rateid', '=' ,$rate_id)

					->orderBy('addon_id', 'asc')

					->get();

					

								

		$transport_list  = DB::table('transport')->where('transport_id', '=' ,$trans_id)->get();	

		

		$vehicle_list = DB::table('vehicle')

					->where('vehicle_transid', '=' ,$trans_id)

					->where('vehicle_id', '=' ,$vehicle_id)

					->get();

						

		$rate_detil = DB::table('vehicle_rate')		

					->where('rate_transid', '=' ,$trans_id)

					->where('rate_vehicleid', '=' ,$vehicle_id)

					->where('rate_id', '=' ,$rate_id)

					->get();

							

							

		

		$transport_name = $transport_list[0]->transport_name;

		$vehicle_rego = $vehicle_list[0]->vehicle_rego;

		$rate_name = $rate_detil[0]->rate_name;

		

		

			$data_onview = array('addon_list'=>$addon_list,

								 'transport_name'=>$transport_name,

								 'vehicle_rego'=>$vehicle_rego,

								 'rate_name'=>$rate_name,

								 'trans_id'=>$trans_id,

								 'vehicle_id'=>$vehicle_id,

								 'rate_id'=>$rate_id,

								 ); 

		

		return View('admin.transport.addon_list')->with($data_onview);;

	}

	

	

	

	

	public function ajax_addons_form()

	{

		

		//print_r($_POST);	

		$id =0;

		

		$addon_detail = '';

		

		

		if(Input::get('addon_id'))

		{

			$addon_id = Input::get('addon_id');

			$id = Input::get('addon_id');

			$addon_detail  = DB::table('vehicle_addons')->where('addon_id', '=' ,$addon_id)->get();	 

		}

		

		

		

		$addon_list = '';

		

		$trans_id = Input::get('trans_id');

		$vehicle_id =Input::get('vehicle_id');

		$rate_id = Input::get('rate_id');

		

		

								

		$transport_list  = DB::table('transport')->where('transport_id', '=' ,$trans_id)->get();	

		

		$vehicle_list = DB::table('vehicle')

					->where('vehicle_transid', '=' ,$trans_id)

					->where('vehicle_id', '=' ,$vehicle_id)

					->get();

						

		$rate_detil = DB::table('vehicle_rate')		

					->where('rate_transid', '=' ,$trans_id)

					->where('rate_vehicleid', '=' ,$vehicle_id)

					->where('rate_id', '=' ,$rate_id)

					->get();						

							

		

		

		$transport_name = $transport_list[0]->transport_name;

		$vehicle_rego = $vehicle_list[0]->vehicle_rego;

		$rate_name = $rate_detil[0]->rate_name;			

		

		

		

			$data_onview = array('addon_detail'=>$addon_detail,

								 'transport_name'=>$transport_name,

								 'vehicle_rego'=>$vehicle_rego,

								 'rate_name'=>$rate_name,

								 'trans_id'=>$trans_id,

								 'vehicle_id'=>$vehicle_id,

								 'rate_id'=>$rate_id,

								 'id'=>$id

								 ); 						 

		return View('admin.transport.addon_form')->with($data_onview);

	}

	

	public function ajax_addons_action(Request $request)

	{

			

		/*echo '<pre>';

		print_r($_POST);

		exit;*/

	

		$addon_id = Input::get('addon_id');

		if(Input::get('from')=='back'){

	

				$trans_id = Input::get('addon_transid');

				$vehicle_id =Input::get('addon_vehicleid');

				$rate_id = Input::get('addon_rateid');

									

				$transport_list  = DB::table('transport')->where('transport_id', '=' ,$trans_id)->get();	

				

				$vehicle_list = DB::table('vehicle')

							->where('vehicle_transid', '=' ,$trans_id)

							->where('vehicle_id', '=' ,$vehicle_id)

							->get();

								

				$rate_detil = DB::table('vehicle_rate')		

							->where('rate_transid', '=' ,$trans_id)

							->where('rate_vehicleid', '=' ,$vehicle_id)

							->where('rate_id', '=' ,$rate_id)

							->get();						

									

				

				

				$transport_name = $transport_list[0]->transport_name;

				$vehicle_rego = $vehicle_list[0]->vehicle_rego;

				$rate_name = $rate_detil[0]->rate_name;		

										

															

				 $addon_list = DB::table('vehicle_addons')		

								->where('addon_transid', '=' ,$trans_id)

								->where('addon_vehicleid', '=' ,$vehicle_id)

								->where('addon_rateid', '=' ,$rate_id)

								->orderBy('addon_id', 'asc')

								->get();

									 

									 

					$data_onview = array('addon_list'=>$addon_list,

									 'transport_name'=>$transport_name,

									 'vehicle_rego'=>$vehicle_rego,

									 'rate_name'=>$rate_name,

									 'trans_id'=>$trans_id,

									 'vehicle_id'=>$vehicle_id,

									 'rate_id'=>$rate_id

									 ); 

										

					

						return View('admin.transport.addon_list')->with($data_onview);

	

		}

		else

		{

			if($addon_id==0)

			{

					

					$addon = new Vehicle_addons;

					$addon->addon_transid = Input::get('addon_transid');

					$addon->addon_vehicleid =Input::get('addon_vehicleid');

					$addon->addon_rateid = Input::get('addon_rateid');

					$addon->addon_name = trim(Input::get('addon_name'));

					$addon->addon_price =  Input::get('addon_price');

					$addon->addon_status =  Input::get('addon_status');

					$addon->addon_groupname = strtoupper(trim(Input::get('addon_groupname')));

					$addon->addon_option =  Input::get('addon_option');

					

					$addon->save();

					$addon_id = $addon->addon_id;

					

					Session::flash('addon_message', 'Vehicle Addon Inserted Sucessfully!'); 

					

				

					

					

					

					

					/* FEATCH DATA FROM TABLE FOR SHOW */

					

					$addon_list = '';

					

			

			$trans_id = Input::get('addon_transid');

			$vehicle_id =Input::get('addon_vehicleid');

			$rate_id = Input::get('addon_rateid');

			

			

									

			$transport_list  = DB::table('transport')->where('transport_id', '=' ,$trans_id)->get();	

			

			$vehicle_list = DB::table('vehicle')

						->where('vehicle_transid', '=' ,$trans_id)

						->where('vehicle_id', '=' ,$vehicle_id)

						->get();

							

			$rate_detil = DB::table('vehicle_rate')		

						->where('rate_transid', '=' ,$trans_id)

						->where('rate_vehicleid', '=' ,$vehicle_id)

						->where('rate_id', '=' ,$rate_id)

						->get();						

								

			

			

			$transport_name = $transport_list[0]->transport_name;

			$vehicle_rego = $vehicle_list[0]->vehicle_rego;

			$rate_name = $rate_detil[0]->rate_name;			

			

		

			

			

			

									 

					/* END */

					

						

					

					

					if(Input::get('from')=='submit'){								

														

				 $addon_list = DB::table('vehicle_addons')		

								->where('addon_transid', '=' ,$trans_id)

								->where('addon_vehicleid', '=' ,$vehicle_id)

								->where('addon_rateid', '=' ,$rate_id)

								->orderBy('addon_id', 'asc')

								->get();

									 

									 

					$data_onview = array('addon_list'=>$addon_list,

									 'transport_name'=>$transport_name,

									 'vehicle_rego'=>$vehicle_rego,

									 'rate_name'=>$rate_name,

									 'trans_id'=>$trans_id,

									 'vehicle_id'=>$vehicle_id,

									 'rate_id'=>$rate_id

									 ); 

										

						

						return View('admin.transport.addon_list')->with($data_onview);

					}

					elseif(Input::get('from')=='addnext'){		

						

						$id = 0;

						$addon_detail = '';

									 

					$data_onview = array('addon_detail'=>$addon_detail,

									 'transport_name'=>$transport_name,

									 'vehicle_rego'=>$vehicle_rego,

									 'rate_name'=>$rate_name,

									 'trans_id'=>$trans_id,

									 'vehicle_id'=>$vehicle_id,

									 'rate_id'=>$rate_id,

									 'id'=>$id

									 ); 	

									 

						return View('admin.transport.addon_form')->with($data_onview);

					}

					

					

				

			}

			else

			{

				$trans_id = Input::get('addon_transid');

				$vehicle_id =Input::get('addon_vehicleid');

				$rate_id = Input::get('addon_rateid');

									

				$transport_list  = DB::table('transport')->where('transport_id', '=' ,$trans_id)->get();	

				

				$vehicle_list = DB::table('vehicle')

							->where('vehicle_transid', '=' ,$trans_id)

							->where('vehicle_id', '=' ,$vehicle_id)

							->get();

								

				$rate_detil = DB::table('vehicle_rate')		

							->where('rate_transid', '=' ,$trans_id)

							->where('rate_vehicleid', '=' ,$vehicle_id)

							->where('rate_id', '=' ,$rate_id)

							->get();						

									

				

				

				$transport_name = $transport_list[0]->transport_name;

				$vehicle_rego = $vehicle_list[0]->vehicle_rego;

				$rate_name = $rate_detil[0]->rate_name;		

				

						

				if(Input::get('from')=='update'){

				

					DB::table('vehicle_addons')

						->where('addon_id', $addon_id)

						->update(['addon_name' => trim(Input::get('addon_name')),

								  'addon_price' => Input::get('addon_price'),

								  'addon_status' =>  Input::get('addon_status'),

								  'addon_groupname' => strtoupper(trim(Input::get('addon_groupname'))),

								  'addon_option' => Input::get('addon_option')

								 ]);

						 

					

					Session::flash('addon_message', 'Vehcile Addon Update Sucessfully!'); 

					

				}

				

				/* FEATCH DATA FROM TABLE FOR SHOW */

					

															

				 $addon_list = DB::table('vehicle_addons')		

								->where('addon_transid', '=' ,$trans_id)

								->where('addon_vehicleid', '=' ,$vehicle_id)

								->where('addon_rateid', '=' ,$rate_id)

								->orderBy('addon_id', 'asc')

								->get();

									 

									 

					$data_onview = array('addon_list'=>$addon_list,

									 'transport_name'=>$transport_name,

									 'vehicle_rego'=>$vehicle_rego,

									 'rate_name'=>$rate_name,

									 'trans_id'=>$trans_id,

									 'vehicle_id'=>$vehicle_id,

									 'rate_id'=>$rate_id

									 ); 

										

					

						return View('admin.transport.addon_list')->with($data_onview);

				

			}

		}	

	

	}

	

	

	

	

	/* VEHICLE MODULE END */

	

	

	

	

	

	/*FIND MODLE BASED ON MAKE START*/

	function find_model()

	{

		$select_box = '';

		$make_id = Input::get('make_id');

		$model_list  = DB::table('vehicle_model')

						->where('model_makeid', '=' ,$make_id)

						->where('model_status', '=' ,'1')->get();

	$select_box = '<select class="form-control" name="vehicle_model" id="vehicle_model" required="required">';

	$select_box .= '<option value="">Select Model</option>';	

		if(count($model_list)>0)					

		{

			foreach($model_list as $mlist)

			{

				$select_box .='<option value="'.$mlist->model_id.'">'.$mlist->model_name.'</option>';	

			}

		}

	$select_box .= '</select>';	

	

	return $select_box;

	}

	/* END */

	

	

	

	/*REMOVE IMAGES FROM VEHICLE SECTION */

	

	function vehicle_image_update()

	{

		//print_r($_POST);

		

		$vehicle_id = Input::get('vid');

		$image_id = Input::get('vimageid');

		$cover = 0;

		

		$vehicle_image_delete  = DB::table('vehicle_img')

						->where('vehicle_id', '=' ,$vehicle_id)

						->where('id', '=' ,$image_id)

						->get();

		if(count($vehicle_image_delete)>0)

		{

		

			$rest_old_logo = $vehicle_image_delete[0]->imgPath; 

			

			$cover =  $vehicle_image_delete[0]->isCover; 

		

			 $file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/vehicle/'.$rest_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/vehicle/'.$rest_old_logo;

			if(file_exists($file_remove)){	

				unlink($file_remove);

			}

			

			DB::table('vehicle_img')->where('id', '=', $image_id)->delete();

			

			

			if($cover==1)

			{

				$vehicle_images_c  = DB::table('vehicle_img')->where('vehicle_id', '=' ,$vehicle_id)->get();	

				

				if(count($vehicle_images)>0)

				{

					DB::table('vehicle_img')

						->where('id',$vehicle_images_c[0]->id)

						->update(['isCover' => '1'

									]);

				}

			}

			

			

		}

		

		

		$vehicle_images  = DB::table('vehicle_img')->where('vehicle_id', '=' ,$vehicle_id)->get();	

		

		

		$hmtl_detail = '';

		if(count($vehicle_images)>0)

		{

			foreach ($vehicle_images as $img_list)

			{

								$checked = '';

									if($img_list->isCover){$checked ='checked="checked"';}

								

			 $hmtl_detail .= '<div class="col-md-3">  

							<div class="tab_five_img">

							<img src="'.url('/').'/uploads/vehicle/'.$img_list->imgPath.'" />

							 

							<img title="Delete"  alt="'.$img_list->imgPath.'" vimageid="'.$img_list->id.'"  src="'. url('/').'/design/front/img/delete.png" style="margin:3px 3px 3px 3px" onclick="delete_image(\''.$vehicle_id.'\',\''.$img_list->id.'\');">

							

							<input  type="radio" name="isCover" id="isCover" value="'.$img_list->id.'" '.$checked.'/> Is Cover

							

							</div></div>';

			

			}	

		}	

		

		print_r( $hmtl_detail );			

	}

	

}

