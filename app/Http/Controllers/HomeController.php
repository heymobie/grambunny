<?php



namespace App\Http\Controllers;



Use DB;

use App\Http\Requests;

use Illuminate\Http\Request;

use App\Restaurant;

use App\User;

use Mail;

/*use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Routing\Controller as BaseController;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Foundation\Auth\Access\AuthorizesResources;*/



class HomeController extends Controller

{



	public function __construct(){

	    //	$this->middleware('auth');

	}



	public function index()

	{



		$total_restaurant = Restaurant::where('rest_status','!=','INACTIVE')->count();

		$total_users = User::where('is_admin','=','0')->count();



		$today_special = DB::table('restaurant')

							->select('restaurant.*')

							->where('rest_special', '=' , '1')

							->where('rest_status', '!=' , 'INACTIVE')

							->orderBy('restaurant.rest_id', 'desc')

							->get();



		$cuisine = DB::table('cuisine')

						->select('*')

						->where('cuisine_status', '=' , '1')

						->orderBy('cuisine_id', 'desc')

						->get();



		$data_onview = array('today_special' =>$today_special,

							'total_restaurant' =>$total_restaurant,

							'total_users' =>$total_users,

							'cuisine_list' =>$cuisine,

		);

		return view('home')->with($data_onview);

	}



	public function vendor_order_confirm(Request $request){



		$id = $request->input('id');



		$status = DB::table('order')

					->select('*')

					->where('order_id', $id)

					->get();

		/*echo "<pre>"; print_r($status);

		echo $status[0]->order_status;die;*/

		$data = array();

		$rest_detail = array();

		if($status[0]->order_status==1 || $status[0]->order_status=='1'){



			$msg = 'This Order is now confirmed and the customer is sent a message with the confirmation.';



			DB::table('order')

				->where('order_id', $id)

				->update(['order_status' => '4' ]);



			$rest_detail = DB::table('restaurant')

						->select('*')

						->where('rest_id', '=' ,$status[0]->rest_id)

						->get();



			$email_subject = 'Your order is Confirmed.';

			$email_content_detail = DB::table('email_content')

										->select('*')

										->where('email_id', '=' ,'6')

										->get();

			$email_content =$email_content_detail[0]->email_content;

			$searchArray = array("order_id", "order_uniqueid", "f_name");



			$replaceArray = array($id, $status[0]->order_uniqueid, $status[0]->order_fname);

			$ordermessage = str_replace($searchArray, $replaceArray, $email_content);



			//$ordermessage= $email_content;



			if(!empty($status[0]->order_email))

			{

				$data = array('order_uniqueid'=>$status[0]->order_uniqueid,

								'order_name'=>$status[0]->order_fname,

								'order_email'=>$status[0]->order_email,

								'order_detail'=>$status,

								'rest_detail'=>$rest_detail,

								'order_id' => $status[0]->order_id,

								'ordermessage' => $ordermessage

				);

				//echo $data['order_email'];

				Mail::send('emails.order_generat', $data, function ($message) use ($data) {



					$message->from('app@grambunny.com', 'grambunny Order Confirmation');

					$message->to($data['order_email']);

					$message->bcc('votiveshweta@gmail.com');

					$message->bcc('votivemobile.pankaj@gmail.com');

					$message->subject('Order request confirmed');



				});



			}



		}else{



			$msg = 'The order is already confirmed.';

		}



		$data = array(

			'order_uniqueid'=>$status[0]->order_uniqueid,

			'order_name'=>$status[0]->order_fname,

			'order_email'=>$status[0]->order_email,

			'order_detail'=>$status,

			'rest_detail'=>$rest_detail,

			'order_id' => $status[0]->order_id,

			'msg'=>$msg,

		);

		return View('popup')->with($data);



	}



}

