<?php 
namespace App\Http\Controllers\Api;
use App\Coupon_code;
use App\Coupon_code_apply;
use App\Delivery_address;
use App\Device_token;
use App\Favourite_food;
use App\Favourite_restaurant;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Notification_list;
use App\Order;
use App\Order_payment;
use App\Productservice;
use App\Proservicescate; 
use App\Proservices_subcate; 
use App\Restaurant;
use App\Review;
use App\Temp_cart;
use App\User;
use App\User_otpdetail;
use App\Vendor;
use Auth;
use Braintree_Transaction;
use Carbon\Carbon;
//use Intervention\Image\Facades\Image as Image; 
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Mail;
use PHPMailer\PHPMailer;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ProductController extends Controller
{

private $param;

	public function category_list(){
		$response= array();
		$url = url('/').'/public/uploads/category';
		
		$data['category_list'] = array(); 
		$category  = DB::table('product_service_category')->where('status', '=' ,1)->get();
		if(count($category)>0){
			foreach ($category as $value) {
				if(!empty($value->cat_image)){
					$imageurl = $url.'/'.$value->cat_image;
				}else{
					$imageurl = "";
				}
				
				$data['category_list'][]=array(
						'category_id' => $value->id,
						'category' => $value->category,
						'cat_image' => $imageurl,
						'status' => $value->status,
				);
			}
		
			$response['message'] = "Category List";
			$response['status'] = 1;
			$response['data'] = array('category_list'=>$data['category_list']);
		}else{
			$response['message'] = "No data found";
			$response['status'] = 0;
			$response['data'] = array('vendor_detail'=>$data['category_list']);
		}
		echo json_encode($response);

	}

	public function products_list(Request $request){

		$response= array();

		DB::enableQueryLog();

		$categoryid = $request->category_id;

		$catproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)->get();

		if(count($catproduct)>0){

		$data['product_list']=$catproduct;	

		foreach ($catproduct as $value) {
			
		// $data['product_list'][]=array(
		// 				'product_id' => $value->id,
		// 				'name' => $value->name,
		// 				'image' => $value->image,
		// 				'status' => $value->status,
		// 		);

		$subcategory  = DB::table('product_service_sub_category')->where('category_id', '=' ,$categoryid)->get();

		$data['subcategory_detail']=$subcategory;

		}	


		}

		return $data;

	}


		public function post_products_list(Request $request){

		$response= array();

		DB::enableQueryLog();

		$categoryid = $request->category_id;

		$catproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)->get();

		if(count($catproduct)>0){

		$data['product_list']=$catproduct;	

		foreach ($catproduct as $value) {

		$subcategory  = DB::table('product_service_sub_category')->where('category_id', '=' ,$categoryid)->get();

		$data['subcategory_detail']=$subcategory;

		}	


		}

		return $data;

	}



}