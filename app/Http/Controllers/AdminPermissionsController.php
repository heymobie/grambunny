<?php
namespace App\Http\Controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Route;
use Illuminate\Support\Facades\Input;

class AdminPermissionsController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}

	/*Delivery man list*/
	public function permission_list()
	{

        //$user_id = Route::current()->getParameter('id');
		$menu_list  = DB::table('sub_admin_dashboard_menus')->get();

		$data_onview = array('menu_list' =>$menu_list);
		return View('admin.permission_form')->with($data_onview);

	}

	/*Add delivery man form*/
	// public function permission_forms(Request $request)
	// {
	// 	if(Route::current()->getParameter('id'))
	// 	{
	// 		//$id = Route::current()->getParameter('id');

	// 		die('ortoeriuto');


	// 		$user_detail  = DB::table('sub_admin_dashboard_menus')->where('id', '=' ,$id)->get();
	// 		$user_list  = DB::table('deliveryman')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();
	// 		$data_onview = array('user_detail' =>$user_detail,
	// 			'id'=>$id,
	// 			'user_list'=>$user_list);
	// 		return View('admin.permission_form')->with($data_onview);
	// 		return View('admin.permission_form');
	// 	}
	// 	else
	// 	{
	// 		$id =0;
	// 		// $user_list  = DB::table('deliveryman')->where('user_status', '=' ,'1')->where('id', '!=' ,$id)->get();
	// 		// $data_onview = array('id'=>$id,
	// 		// 	'user_list'=>$user_list);
	// 		//return View('admin.permission_form')->with($data_onview);

	// 		return View('admin.permission_form');
	// 	}

	// }

	public function permission_form(Request $request)
	{

		$user_id = Route::current()->getParameter('id');
		$menu_list  = DB::table('sub_admin_dashboard_menus')->get();
		$userDetail  = DB::table('admins')
		->where('id',$user_id)
		->get(['name','lname']);


		$permissionDetail = DB::table('sub_admin_access_permissions')
		->select('*')
		->where('sub_admin_id','=',$user_id)
		->get();



		$data_onview = array('menu_list' =>$menu_list,'user_id'=>$user_id,'user_detail'=> $userDetail,'permissionDetail'=>$permissionDetail);
		return View('admin.permission_form')->with($data_onview);

	}


 public function permission_action(Request $request)
	{

       $permission = Request::get('permission');

     /* echo "<pre>";
        print_r($permission);
        echo "</pre>";
        die; */

       if(!empty($permission)) {

		DB::table('sub_admin_dashboard_menus')
        ->update(['access_permissions' => 0]);

       }

     foreach ($permission as $key => $value) {
     	 
        DB::table('sub_admin_dashboard_menus')
		->where('menu_id','=',$value)
        ->update(['access_permissions'=>1]); 

       } 


		Session::flash('message', 'Sub Admin Access Permissions Set Sucessfully!');
		return redirect()->to('/admin/permissions_list');

	}

	/*Delivery man add action*/
	public function user_action(Request $request)
	{

		$user_id = Input::get('user_id');

		DB::connection()->enableQueryLog();
		$perExist = DB::table('sub_admin_access_permissions')
		->select('*')
		->where('sub_admin_id','=',$user_id)
		->get();

		if(!empty($perExist)){

			/*sub-admin exist with any kind of permission or at least any one*/

			$permissions = Request::all();

			// echo "<pre>";
			// print_r($permissions);
			// echo "</pre>";
			// die;

			if(isset($permissions['Dashboard'])){
				foreach ($permissions['Dashboard'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}
			}

			if(isset($permissions['User_Management'])){
				foreach ($permissions['User_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Delivery_man_Management'])) {

				foreach ($permissions['Delivery_man_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Area_Management'])) {
				foreach ($permissions['Area_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Vendor_Management'])) {

				foreach ($permissions['Vendor_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Vendor_Category'])) {

				foreach ($permissions['Vendor_Category'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Restaurant_Management'])) {

				foreach ($permissions['Restaurant_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Menu_Template'])) {

				foreach ($permissions['Menu_Template'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Order_Management'])) {

				foreach ($permissions['Order_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Review_&_rating'])) {

				foreach ($permissions['Review_&_rating'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Payment_Management'])) {

				foreach ($permissions['Payment_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Content_Management'])) {

				foreach ($permissions['Content_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Social_Management'])) {

				foreach ($permissions['Social_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Testimonial'])) {

				foreach ($permissions['Testimonial'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Home_page_content'])) {

				foreach ($permissions['Home_page_content'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Send_Notification_to_user'])){

				foreach ($permissions['Send_Notification_to_user'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			if(isset($permissions['Email_Template_Content'])) {

				foreach ($permissions['Email_Template_Content'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$per = DB::table('sub_admin_access_permissions')
					->select('*')
					->where('sub_admin_id','=',$user_id)
					->where('menu_id','=',$menu_id)
					->get();
					if(!empty($per)){
						$affected	= DB::table('sub_admin_access_permissions')
						->where('sub_admin_id', $permissions['user_id'])
						->where('menu_id', $menu_id)
						->update($PermisionData);

					}else{
						$PermisionInsert = array(
							'sub_admin_id'=>$permissions['user_id'],
							'menu_id'=>$menu_id,
							'all_permissions'=>0,
							'add_permission'=>$PermisionData['add_permission'],
							'edit_permission'=>$PermisionData['edit_permission'],
							'view_permission'=>$PermisionData['view_permission'],
							'delete_permission'=>$PermisionData['delete_permission'],
							'created'=>date('y-m-d h:i:s')
						);
						$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);
					}
				}

			}

			return redirect()->to('/admin/permissions_list');


		}else{


			/*sub-admin not exist with any kind of permission*/

			$permissions = Request::all();
			if(isset($permissions['Dashboard'])){
				foreach ($permissions['Dashboard'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['User_Management'])){
				foreach ($permissions['User_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['Delivery_man_Management'])){
				foreach ($permissions['Delivery_man_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Area_Management'])){
				foreach ($permissions['Area_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['Vendor_Management'])){
				foreach ($permissions['Vendor_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['Vendor_Category'])){
				foreach ($permissions['Vendor_Category'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Restaurant_Management'])){
				foreach ($permissions['Restaurant_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['Menu_Template'])){
				foreach ($permissions['Menu_Template'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Order_Management'])){
				foreach ($permissions['Order_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Review_&_rating'])){
				foreach ($permissions['Review_&_rating'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Payment_Management'])){
				foreach ($permissions['Payment_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}


			if(isset($permissions['Content_Management'])){
				foreach ($permissions['Content_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Social_Management'])){
				foreach ($permissions['Social_Management'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Testimonial'])){
				foreach ($permissions['Testimonial'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Home_page_content'])){
				foreach ($permissions['Home_page_content'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			if(isset($permissions['Send_Notification_to_user'])){
				foreach ($permissions['Send_Notification_to_user'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}



			if(isset($permissions['Email_Template_Content'])){
				foreach ($permissions['Email_Template_Content'] as $key => $permission){
					$menu_id = $key;
					$PermisionData = array();
					for($i=0; $i<count($permission);$i++){
						$perm_id = $permission[$i];
						if ($perm_id==1) {
							$PermisionData['add_permission'] = 1;
						}
						if ($perm_id==2) {
							$PermisionData['edit_permission'] = 1;
						}
						if($perm_id==3) {
							$PermisionData['view_permission'] = 1;
						}
						if($perm_id==4) {
							$PermisionData['delete_permission'] =1;
						}
					}

					if (empty($PermisionData['add_permission'])) {
						$PermisionData['add_permission'] = 0;
					}
					if (empty($PermisionData['edit_permission'])) {
						$PermisionData['edit_permission'] = 0;
					}
					if (empty($PermisionData['view_permission'])) {
						$PermisionData['view_permission'] = 0;
					}
					if (empty($PermisionData['delete_permission'])) {
						$PermisionData['delete_permission'] = 0;
					}

					$PermisionInsert = array(
						'sub_admin_id'=>$permissions['user_id'],
						'menu_id'=>$menu_id,
						'all_permissions'=>0,
						'add_permission'=>$PermisionData['add_permission'],
						'edit_permission'=>$PermisionData['edit_permission'],
						'view_permission'=>$PermisionData['view_permission'],
						'delete_permission'=>$PermisionData['delete_permission'],
						'created'=>date('y-m-d h:i:s')
					);
					$affected = DB::table('sub_admin_access_permissions')->insert($PermisionInsert);

				}
			}

			return redirect()->to('/admin/permissions_list');
		}

	}






	/*Delete sub-admin*/
	public function Deliveryman_delete($id)
	{
		DB::table('deliveryman')->where('id', '=', $id)->delete();
		Session::flash('message', 'Delivery man deleted sucessfully!');
		return redirect()->to('/admin/delivery_manlist');

	}




	/*Check duplicate delivery man*/
	// public function deliveryman_duplicate_email()
	// {		// Get the value from the form
	// 	$input['email'] = Input::get('email');
	// 	$input1['user_mob'] = Input::get('user_mob');
	// 			// Must not already exist in the `email` column of `users` table
	// 	$rules = array('email' => 'unique:deliveryman,email');
	// 	$validator = Validator::make($input, $rules);
	// 	$rules1 = array('user_mob' => 'unique:deliveryman,user_mob');
	// 	$validator1 = Validator::make($input1, $rules1);
	// 	if(($validator->fails()) && ($validator1->fails())) {
	// 		echo '1';
	// 	}
	// 	elseif($validator->fails()) {
	// 		echo '2';
	// 	}elseif($validator1->fails()) {
	// 		echo '3';
	// 	}
	// 	else {
	// 		echo '4';
	// 	}

	// }

	/*View Delivery man details*/
	// public function ViewDeliveryman()
	// {
	// 	$Deliveryman_id =	Route::current()->getParameter('id');
	// 	$Deliverymandetail  = DB::table('deliveryman')->where('id', '=' ,$Deliveryman_id)->get();
	// 	$data_onview = array('Deliverymandetail' =>$Deliverymandetail);
	// 	return View('admin.deliveryman_view')->with($data_onview);

	// }


}