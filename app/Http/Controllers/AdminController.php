<?php

namespace App\Http\Controllers;

// use Request;

Use DB;

use Hash;

use Session;

use Redirect;

use App\Http\Requests;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use App\User;
use Illuminate\Http\Request;
use App\Order;

use App\Vendor;



class AdminController extends Controller

{

    public function __construct(){

    	$this->middleware('admin');

    }



    public function index(){ 


    $order_detail  = DB::table('orders')

		->orderBy('id', 'desc')

		->get();

		$order_total  = DB::table('orders')->get();

		$total_user  = DB::table('users')->get();

		$total_merchant  = DB::table('vendor')->get();

		$total_products  = DB::table('product_service')->get();

		$today_date = DB::table('orders')->whereDate('created_at', Carbon::today())->get();

		// $current_week = DB::table('orders')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get();

		$currentweek = DB::table('orders')->sum('total');

		$order_pending  = DB::table('orders')->where('status','=',0)->get();

		$today_date_pending = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status','=',0)->get();

		$today_pending = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',0)->get();

		$today_Accept = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',1)->get();

		$today_cancel = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',2)->get();

		// $today_on_the_way = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',3)->get();

    $today_ontheway = DB::table('orders')->whereDate('created_at', Carbon::today())->sum('total');

		$today_deliverd = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',4)->get();

		$today_merchant = DB::table('vendor')->whereDate('created_at', Carbon::today())->get();

		$today_users = DB::table('users')->whereDate('created_at', Carbon::today())->get();

		//$current_week_pending = DB::table('orders')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('status','=',0)->get();

		$orderss = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
        ->groupBy('x')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
       ->get();

        $one_week = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();


        // $userss = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')
		//     ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
		//     ->groupBy('date')
		//     ->get();

		// $merchant = Order::selectRaw('COUNT(DISTINCT vendor_id) as vendor_id, DATE(created_at) as date')
		//     ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
		//     ->groupBy('date')
		//     ->get();


       $user_date1 = User::selectRaw('DATE(created_at) as x, COUNT(*) as y')
        ->groupBy('x')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
       ->get();

       $merchant_date1 = Vendor::selectRaw('DATE(created_at) as x, COUNT(*) as y')
        ->groupBy('x')
        ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
       ->get();

       $userDates = $user_date1->pluck('x')->toArray();

       $merchantDates = $merchant_date1->pluck('x')->toArray();

      $userlabel = array_unique(array_merge($userDates, $merchantDates));

     // dd($userlabel);

       $userss = User::selectRaw('COUNT(id) as user_id, DATE(created_at) as date')
		    ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
		    ->groupBy('date')
		    ->get();

   $users_get_weekly = array();     

	 foreach ($userss as $val) {
    $items['user_id'] = $val->user_id;
    $items['date'] = $val->date;
    $users_get_weekly[] = $items;
    }

     $item = array();

     $user_values_get =array();

     $values_get =array();

    foreach ($userlabel as $value) {
        $found = false;

    foreach ($users_get_weekly as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['user_id'] = $vget['user_id'];
            break;
        }
    }

    if (!$found) {
        $item['user_id'] = 0;
    }

    $item['date'] = $value;

     $user_values_get[] = $item;

     }

     $userss = $user_values_get;

		$merchant = Vendor::selectRaw('COUNT(vendor_id) as vendor_id, DATE(created_at) as date')
		    ->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])
		    ->groupBy('date')
		    ->get();

		 $items = array();

		 $valuesgetusers =array();

    foreach ($merchant as $val) {
    $items['vendor_id'] = $val->vendor_id;
    $items['date'] = $val->date;
    $valuesgetusers[] = $items;
    }

     $item = array();

    foreach ($userlabel as $value) {
        $found = false;

    foreach ($valuesgetusers as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['vendor_id'] = $vget['vendor_id'];
            break;
        }
    }

    if (!$found) {
        $item['vendor_id'] = 0;
    }

      $item['date'] = $value;

     $values_get[] = $item;

     }

     $merchant= $values_get;

		$ordercount = $order_total->count();

		$usercount = $total_user->count();

		$merchantcount = $total_merchant->count();

		$totalproducts = $total_products->count();

		$todaydate = $today_date->count();

		$todaymerchant = $today_merchant->count();

		$todayusers = $today_users->count();

		$orderpending = $order_pending->count();

		$todaydatepending = $today_date_pending->count();

		//$currentweekpending = $current_week_pending->count();

		$todaypending = $today_pending->count();

		$todayAccept = $today_Accept->count();

		$todaycancel = $today_cancel->count();

		//$today_ontheway = $today_on_the_way->count();

		$todaydeliverd = $today_deliverd->count();

		$data_onview = array('order_detail' =>$order_detail,'ordercount'=>$ordercount,'usercount'=>$usercount,'merchantcount'=>$merchantcount,'totalproducts'=>$totalproducts,'todaydate'=>$todaydate,'currentweek'=>$currentweek,'orderpending'=>$orderpending,'todaydatepending'=>$todaydatepending,'todaypending'=>$todaypending,'todayAccept'=>$todayAccept,'todaycancel'=>$todaycancel,'today_ontheway'=>$today_ontheway,'todaydeliverd'=>$todaydeliverd,'todaymerchant'=>$todaymerchant,'todayusers'=>$todayusers,'orderss'=>$orderss,'one_week'=>$one_week,'userss'=>$userss,'merchant'=>$merchant,'userlabel'=>$userlabel); 	 

		return view('admin.dashboard')->with($data_onview);

    }



	public function showPasswordForm(){

    	return view('admin.changepassword');

	}



	public function updatepassword(Request $request)

	{
	


		$admin_id = Auth::guard('admin')->user()->id;

		$credentials = [

                'password' => $request->get('old_password'),

                'id' =>  $admin_id

        ];



		if(Auth::guard('admin')->attempt($credentials))

		{

			DB::table('admins')

            ->where('id', $admin_id)

            ->update(['password' =>  Hash::make($request->get('password'))

						]);



			Session::flash('success_message', 'Password Update Sucessfully.'); 



			//$request->session()->keep('message','Old Password Not Match Sucessfully!');

			return redirect()->to('/admin/change_password');

		}

		else

		{	

			Session::flash('error_message', 'Old Password Not Match.'); 

			//return Redirect::away('/admin/change_password');

			//return redirect("/admin/change_password");

			//session()->keep('message','Old Password Not Match!');

            return redirect()->to("/admin/change_password");  

		}

	}

	

	function showactivelog()

	{

		$log_data = DB::table('login_history')

					->Join('users', 'login_history.log_userid', '=', 'users.id')					

					->orderBy('login_history.log_id','desc')

					->get();

					

		$data_onview = array('log_data' =>$log_data

					  		 ); 	

    	return view('admin.active_loglist')->with($data_onview);					

	}

	

	public function showactivelog_delete($id)

	{

		DB::table('login_history')->where('log_id', '=', $id)->delete();

		Session::flash('message', 'User Log Information Deleted Sucessfully!');

		return Redirect('/admin/active_log');

	}



	function showmenulog()

	{

		$log_data = DB::table('view_menu_history')

					->select('view_menu_history.*', 'users.name','users.lname','restaurant.rest_name')

					->leftJoin('users', 'view_menu_history.user_id', '=', 'users.id')

					->Join('restaurant', 'view_menu_history.rest_id', '=', 'restaurant.rest_id')

					->get();

					

		$data_onview = array('log_data' =>$log_data	); 	

    	return view('admin.menu_loglist')->with($data_onview);					

	}

	

	public function showmenulog_delete($id)

	{

		DB::table('view_menu_history')->where('id', '=', $id)->delete();

		Session::flash('message', 'User Viewmenu Log Information Deleted Sucessfully!');

		return Redirect('/admin/viewmenu_log');

	}



	public function showsetting()

	{

		//show_data

		$admin_detail = DB::table('admins')

					->select('*')

					->get();

					

		$data_onview = array('admin_detail' =>$admin_detail	); 	

    	return view('admin.general_setting_form')->with($data_onview);

	}



	public function update_admin_setting()

	{

		$admin_id = Auth::guard('admin')->user()->id;

			DB::table('admins')

            ->where('id', $admin_id)

            ->update(['show_data' => (Request::get('show_data'))]);

			Session::flash('message', 'Setting Update Sucessfully!'); 

			return redirect()->to('/admin/general_setting');	

	}


	public function orders_show(Request $request){

		$today = $request->today;
		$yesterday = $request->yesterday;
		$one_month = $request->one_month;

        $data['today'] = $request->today;
		$data['yesterday'] = $request->yesterday;
		$data['one_month'] = $request->one_month;

		//dd($today);

		if($today=="1"){

			//echo "testing1";die;

       // $today_users = DB::table('users')->whereDate('created_at', Carbon::today())->get();

      //  $today_merchant = DB::table('vendor')->whereDate('created_at', Carbon::today())->get();

       // $today_date = DB::table('orders')->whereDate('created_at', Carbon::today())->get();

        $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', Carbon::today())
    ->get();

    $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereDate('created_at', Carbon::today())->get();

    $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', Carbon::today())
    ->get();

    $data['today_merchant'] = Order::selectRaw('COUNT(DISTINCT vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date')->whereDate('created_at', Carbon::today())
    ->get();

   // dd($data['today_date']);

       // $data['today_pending'] = DB::table('orders')->whereDate('created_at', Carbon::today())->where('status',0)->get();

      //  $data['today_sell'] = DB::table('orders')->whereDate('created_at', Carbon::today())->sum('total');

      //  $today_products  = DB::table('product_service')->whereDate('created_at', Carbon::today())->get();

      //  $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('admin.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}


		if($yesterday=="2"){

			//echo "test";die;

			$startDate = Carbon::today();
       $endDate = Carbon::today()->subDay(7);
       $yesterdays = Carbon::yesterday();

     // $today_users = User::whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

       // $today_merchant = DB::table('vendor')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

        $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', $yesterdays)
    ->get();

    $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->where('created_at', $yesterdays)->get();

    $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', $yesterdays)
    ->get();

    $data['today_merchant'] = Order::selectRaw('COUNT(DISTINCT vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', $yesterdays)
    ->get();

       // dd()

      //  $data['today_pending'] = DB::table('orders')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->where('status',0)->get();

       // $data['today_sell'] = DB::table('orders')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->sum('total');

        //dd($data['today_sell']);

       // $today_products  = DB::table('product_service')->whereBetween('created_at', [Carbon::now()->subWeek()->format("Y-m-d H:i:s"), Carbon::now()])->get();

       // $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('admin.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}


		if($one_month=="3"){
			
       // $today_users = DB::table('users')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $today_merchant = DB::table('vendor')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $today_date = DB::table('orders')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

     $data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])
    ->get();

    $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

    $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupby('date') 
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

    $data['today_merchant'] = Order::selectRaw('COUNT(DISTINCT vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date') 
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

  //dd($data['today_date1']);
       // $data['today_pending'] = DB::table('orders')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->where('status',0)->get();

       // $data['today_sell'] = DB::table('orders')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->sum('total');

       // $today_products  = DB::table('product_service')->where(\DB::raw('month(created_at)'), Carbon::today()->month)->get();

       // $data['today_order_show'] = $today_date->count();

       // $data['todaymerchant_show'] = $today_merchant->count();

		//$data['todayusers_show'] = $today_users->count();

		//$data['todayproducts'] = $today_products->count();

		$returnHTML = view('admin.orders_get')->with($data)->render();
      return response()->json($returnHTML);

	}

}

public static function users_show(Request $request){

        $today = $request->today;
        $yesterday = $request->yesterday;
        $one_month = $request->one_month;

        $data['today'] = $today;
        $data['yesterday'] = $yesterday;
        $data['one_month'] = $one_month;
    
        if($data['today']=="1_user"){
        
     $data['today_date1'] = User::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', Carbon::today())
    ->get();

     $data['today_user'] = User::selectRaw('COUNT(id) as user_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', Carbon::today())
    ->get();

     // $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereDate('created_at', Carbon::today())->get();

    $data['today_merchant'] = Vendor::selectRaw('COUNT(vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date')->whereDate('created_at', Carbon::today())
    ->get();

        $returnHTML = view('admin.users_get')->with($data)->render();
      return response()->json($returnHTML);

    }
        if($data['yesterday']=="2_user"){

      $startDate = Carbon::today();
       $endDate = Carbon::today()->subDay(7);
       $yesterdays = Carbon::yesterday();

     $data['today_date1'] = User::selectRaw('DATE(created_at) as x, COUNT(*) as y')
    ->groupBy('x')
    ->whereDate('created_at', $yesterdays)
    ->get();

     $data['today_user'] = User::selectRaw('COUNT(id) as user_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', $yesterdays)
    ->get();

    $data['today_merchant'] = Vendor::selectRaw('COUNT(vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date')
    ->whereDate('created_at', $yesterdays)
    ->get();

   // dd($data['today_merchant']);

     // $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->where('created_at', $yesterdays)->get();

    $returnHTML = view('admin.users_get')->with($data)->render();
      return response()->json($returnHTML);

    }

    if($data['one_month']=="3_user"){

    $data['user_date1'] = User::selectRaw('DATE(users.created_at) as x, COUNT(*) as y')->groupBy('x')->whereBetween('users.created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

    $data['merchant_date1'] = Vendor::selectRaw('DATE(vendor.created_at) as x, COUNT(*) as y')->groupBy('x')
    ->whereBetween('vendor.created_at', [Carbon::now()->subDays(30), Carbon::now()])
    ->get();

   $userDates = $data['user_date1']->pluck('x')->toArray();

   $merchantDates = $data['merchant_date1']->pluck('x')->toArray();

      $data['today_date1'] = array_unique(array_merge($userDates, $merchantDates));

      $data['today_user'] = User::selectRaw('COUNT(users.id) as user_id, DATE(users.created_at) as date')
        ->whereBetween('users.created_at', [Carbon::now()->subDays(30), Carbon::now()])->groupby('date')->get();

      //  $data['today_user'] = array();

    foreach ($data['today_user'] as $val) {
    $items['user_id'] = $val->user_id;
    $items['date'] = $val->date;
    $users_get[] = $items;
    }

     $item = array();

    foreach ($data['today_date1'] as $value) {
        $found = false;

    foreach ($users_get as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['user_id'] = $vget['user_id'];
            break;
        }
    }

    if (!$found) {
        $item['user_id'] = 0;
    }

    $item['date'] = $value;

     $user_values_get[] = $item;

     }

     $data['today_user'] = $user_values_get;

      $data['today_merchant'] = Vendor::selectRaw('COUNT(vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date') 
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

         $items = array();

    foreach ($data['today_merchant'] as $val) {
    $items['vendor_id'] = $val->vendor_id;
    $items['date'] = $val->date;
    $valuesget[] = $items;
    }

     $item = array();

    foreach ($data['today_date1'] as $value) {
        $found = false;

    foreach ($valuesget as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['vendor_id'] = $vget['vendor_id'];
            break;
        }
    }

    if (!$found) {
        $item['vendor_id'] = 0;
    }

      $item['date'] = $value;

     $values_get[] = $item;

     }

     $data['today_merchant'] = $values_get;

        $returnHTML = view('admin.users_get')->with($data)->render();
        return response()->json($returnHTML);

    }

}

public function chart_filter_order_date(Request $request)
	{
		
		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days')); 
		
		if(!empty($order_date)){

			$data['today_date'] = Order::selectRaw('DATE(created_at) as x, COUNT(*) as y')
                     ->groupBy('x')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->get();

	        $data['today_sell'] = Order::selectRaw('sum(total) as total, DATE(created_at) as date')->groupBy('date')->whereBetween('orders.created_at', [$data_to, $data_from])->get();


	        $data['today_user'] = Order::selectRaw('COUNT(DISTINCT user_id) as user_id, DATE(created_at) as date')->groupby('date') 
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

        $data['today_merchant'] = Order::selectRaw('COUNT(DISTINCT vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date') 
        ->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])->get();

	        $returnHTML = view('admin.orders_get')->with($data)->render();
                   return response()->json($returnHTML);
	        
		}

	}

	public function user_chart_filter_order_date(Request $request)
	{
		//echo "fbhgfh";die;
		
		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days')); 

			//dd($data_from);

		if((!empty($data_from)) ||(!empty($data_to))){
		
		if(!empty($order_date)){

			$data['user_date1'] = User::selectRaw('DATE(created_at) as x, COUNT(*) as y')
                     ->groupBy('x')
	        ->whereBetween('users.created_at', [$data_to, $data_from])
	        ->get();

	        $data['merchant_date1'] = Vendor::selectRaw('DATE(created_at) as x, COUNT(*) as y')
                     ->groupBy('x')
	        ->whereBetween('vendor.created_at', [$data_to, $data_from])
	        ->get();

	        $userDates = $data['user_date1']->pluck('x')->toArray();

             $merchantDates = $data['merchant_date1']->pluck('x')->toArray();

             $data['today_date1'] = array_unique(array_merge($userDates, $merchantDates));

             $data['today_user'] = User::selectRaw('COUNT(id) as user_id, DATE(created_at) as date')->groupby('date')->whereBetween('created_at', [$data_to, $data_from])->get();

              foreach ($data['today_user'] as $val) {
    $items['user_id'] = $val->user_id;
    $items['date'] = $val->date;
    $users_get[] = $items;
    }

     $item = array();

    foreach ($data['today_date1'] as $value) {
        $found = false;

    foreach ($users_get as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['user_id'] = $vget['user_id'];
            break;
        }
    }

    if (!$found) {
        $item['user_id'] = 0;
    }

    $item['date'] = $value;

     $user_values_get[] = $item;

     }

     $data['today_user'] = $user_values_get;

     $data['today_merchant'] = Vendor::selectRaw('COUNT(vendor_id) as vendor_id, DATE(created_at) as date')->groupby('date')->whereBetween('created_at', [$data_to, $data_from])->get();

      $items = array();

    foreach ($data['today_merchant'] as $val) {
    $items['vendor_id'] = $val->vendor_id;
    $items['date'] = $val->date;
    $values_getid[] = $items;
    }

     $item = array();

    foreach ($data['today_date1'] as $value) {
        $found = false;

    foreach ($values_getid as $vget) {
        if ($vget['date'] == $value) {
            $found = true;
            $item['vendor_id'] = $vget['vendor_id'];
            break;
        }
    }

    if (!$found) {
        $item['vendor_id'] = 0;
    }

      $item['date'] = $value;

     $values_get[] = $item;

     }

     $data['today_merchant'] = $values_get;

	 $returnHTML = view('admin.users_get')->with($data)->render();
           return response()->json($returnHTML);
	        
		}
	}

	}

}