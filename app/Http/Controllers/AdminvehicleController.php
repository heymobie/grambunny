<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\Http\Requests;





Use DB;

use Hash;

use Session;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use File;

use Intervention\Image\Facades\Image as Image;





use App\Vehicle_class;

use App\Vehicle_cate;







class AdminvehicleController extends Controller

{



	public function __construct(){

    	$this->middleware('admin');

    }

	

	

	/**  VEHICLE CATEGORY  **/

	

	function category_list()

	{

		$category_list = DB::table('vehicle_cate')

			 ->select('*')

			->orderBy('vcate_id', 'desc')

			->get();



		

    	$data_onview = array('category_list' =>$category_list);

	

		return View('admin.vehicle_category_list')->with($data_onview);

	}

	

	function category_form()

	{

		

		if(Route::current()->getParameter('id'))

		{

			 $id = Route::current()->getParameter('id');

				

			$category_detail  = DB::table('vehicle_cate')->where('vcate_id', '=' ,$id)->get();		

			

  		  	$data_onview = array('category_detail' =>$category_detail,

								'id'=>$id); 

						

			return View('admin.vehicle_category_form')->with($data_onview);

		

		}

		else

		{

			$id =0;

  		  	$data_onview = array('id'=>$id); 

			return View('admin.vehicle_category_form')->with($data_onview);

		}

	}

	

	function category_action(Request $request)

	{		

		$vcate_id = Input::get('vcate_id');

		

		if($vcate_id==0)

		{

		

			$input['vcate_name'] = Input::get('vcate_name');

			

			// Must not already exist in the `email` column of `users` table

			$rules = array('vcate_name' => 'unique:vehicle_cate,vcate_name');

			

			$validator = Validator::make($input, $rules);

			Session::flash('error', 'Duplicate Vehicle Category!'); 

			

			if ($validator->fails()) {		

				return redirect()->to('/admin/vehicle_category-form');

			}

			else {

					$cui = new Vehicle_cate;

					$cui->vcate_name = Input::get('vcate_name');

					$cui->vcate_status = Input::get('vcate_status');

					$cui->save();

					$cui_id = $cui->vcate_id;

					

					Session::flash('message', 'Vehicle category Inserted Sucessfully!'); 

					return redirect()->to('/admin/vehicle_category_list');

			}

		}

		else

		{

			DB::table('vehicle_cate')

            ->where('vcate_id', $vcate_id)

            ->update(['vcate_name' => Input::get('vcate_name'),

					  'vcate_status'=> Input::get('vcate_status')

					 ]);

					 

				Session::flash('message', 'Vehicle category Information Updated Sucessfully!');

				return redirect()->to('/admin/vehicle_category_list');

		}

		

	}

	

	/**  END   **/

	

	

	/**  VEHICLE CLASS  **/

	

	function class_list()

	{

		$category_list = DB::table('vehicle_class')

			 ->select('*')

			->orderBy('vclass_id', 'desc')

			->get();



		

    	$data_onview = array('category_list' =>$category_list);

	

		return View('admin.vehicle_class_list')->with($data_onview);

	}

	

	function class_form()

	{

		

		if(Route::current()->getParameter('id'))

		{

			 $id = Route::current()->getParameter('id');

				

			$category_detail  = DB::table('vehicle_class')->where('vclass_id', '=' ,$id)->get();		

			

  		  	$data_onview = array('category_detail' =>$category_detail,

								'id'=>$id); 

						

			return View('admin.vehicle_class_form')->with($data_onview);

		

		}

		else

		{

			$id =0;

  		  	$data_onview = array('id'=>$id); 

			return View('admin.vehicle_class_form')->with($data_onview);

		}

	}

	

	function class_action(Request $request)

	{		

	/*echo '<pre>';

	print_r($_POST);

	exit;*/

		$vclass_id = Input::get('vclass_id');

		

		if($vclass_id==0)

		{

		

			$input['vclass_name'] = Input::get('vclass_name');

			

			// Must not already exist in the `email` column of `users` table

			$rules = array('vclass_name' => 'unique:vehicle_class,vclass_name');

			

			$validator = Validator::make($input, $rules);

			Session::flash('error', 'Duplicate Vehicle Class!'); 

			

			if ($validator->fails()) {		

				return redirect()->to('/admin/vehicle_class-form');

			}

			else {

					$cui = new Vehicle_class;

					$cui->vclass_name = Input::get('vclass_name');

					$cui->vclass_status = Input::get('vclass_status');

					$cui->save();

					$cui_id = $cui->vclass_id;

					

					Session::flash('message', 'Vehicle Class Inserted Sucessfully!'); 

					return redirect()->to('/admin/vehicle_class_list');

			}

		}

		else

		{

			DB::table('vehicle_class')

            ->where('vclass_id', $vclass_id)

            ->update(['vclass_name' => Input::get('vclass_name'),

					  'vclass_status'=> Input::get('vclass_status')

					 ]);

					 

				Session::flash('message', 'Vehicle Class Information Updated Sucessfully!');

				return redirect()->to('/admin/vehicle_class_list');

		}

		

	}

	

	/**  END   **/

	

}

