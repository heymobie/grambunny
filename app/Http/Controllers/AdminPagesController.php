<?php

namespace App\Http\Controllers;



Use DB;



use Hash;



use Session;



use App\Http\Requests;



use Illuminate\Support\Facades\Auth;



use Redirect;



use Illuminate\Support\Facades\Input;



use Validator;



use App\Page;



use App\Testimonial;



use App\Advertisement;



use Route; 



use Illuminate\Http\Request;



use Intervention\Image\Facades\Image as Image;



use App\Notification_list;



use Illuminate\Support\Str;





class AdminPagesController extends Controller

{ 



    public function __construct(){



    	$this->middleware('admin');



    }


    public function page_listing(){

		//echo  'TEST';

		DB::connection()->enableQueryLog();	

		$page_list = DB::table('pages')

			 ->select('*')

			->orderBy('page_id', 'asc')

			->get();

		$data_onview = array('page_list' =>$page_list);

		return View('admin.page_list')->with($data_onview);

    }

	public function page_form(Request $request)
	{

		if($request->id)
		{

			$id = $request->id;

			$page_detail  = DB::table('pages')->where('page_id', '=' ,$id)->get();		
  		  	$data_onview = array('page_detail' =>$page_detail,

								'id'=>$id); 

			return View('admin.page_form')->with($data_onview);

		}else{

			$id =0;

  		  	$data_onview = array('id'=>$id); 

			return View('admin.page_form')->with($data_onview);

		}

	}


	public function page_action(Request $request)
	{

		$page_id = Input::get('page_id');
		$date = date('Y-m-d H:i:s');
		$page_slug = Input::get('page_slug');
		$page_url = Str::slug($page_slug, '-');

		$page_title = Input::get('page_title');
		$page_content = Input::get('page_content');		
		$page_status = Input::get('page_status');
		$sub_title = Input::get('sub_title');
		$seo_title = Input::get('seo_title');
		$seo_keyword = Input::get('seo_keyword');
		$seo_description = Input::get('seo_description');
		$seo_author = Input::get('seo_author');

		if($page_id==0)
		{

			$input['page_title'] = Input::get('page_title');
			$input['page_url'] = $page_url;

			// Must not already exist in the `email` column of `users` table

			$rules = array('page_title' => 'unique:pages,page_title', 'page_url' => 'unique:pages,page_url');

			$validator = Validator::make($input, $rules);

			Session::flash('error', 'Duplicate Page Entry!'); 

			if ($validator->fails()) {		

				return redirect()->to('/admin/page-form');

			}else {

				if($page_url != '') {

					$page = DB::table('pages')

				            ->insert(['page_title' => $page_title,
									  'page_content'=> $page_content,
									  'page_url'=> $page_url,
									  'page_status'=> $page_status,
									  'page_content'=> $page_content,
									  'seo_title' => $seo_title,
									  'seo_keyword' => $seo_keyword,
									  'seo_description' => $seo_description,
									  'seo_author' => $seo_author,
									  'created_at'=> $date,
									  'updated_at'=> $date
									 ]);

					Session::flash('message', 'Page Inserted Successfully!'); 

					return redirect()->to('/admin/content_pagelist');

				} else {

					Session::flash('error', 'Duplicate Page Entry!'); 

					return redirect()->to('/admin/page-form');

				}

			}


		}else{

		$url = Input::get('page_url');	

		if($page_url != $url){

        	$input['page_url'] = $page_url;

			$rules = array('page_url' => 'unique:pages,page_url');

			$validator = Validator::make($input, $rules);

			if ($validator->fails()) {	

			    Session::flash('error', 'Duplicate Page Slug!'); 	
				return redirect()->to('/admin/page-form/'.$page_id);

			}

		}

			DB::table('pages')
            ->where('page_id', $page_id)
            ->update(['page_title' => trim(Input::get('page_title')),
					  'page_content'=> trim(Input::get('page_content')),
					  'page_url'=> $page_url,
					  'page_status'=> Input::get('page_status'),
					  'sub_title'=> Input::get('sub_title'),
	  				  'seo_title' => $seo_title,
					  'seo_keyword' => $seo_keyword,
					  'seo_description' => $seo_description,
					  'seo_author' => $seo_author,
					  'updated_at'=> $date

					 ]);
				
				Session::flash('message', 'Page Information Updated Successfully!');

				return redirect()->to('/admin/content_pagelist');
		}		

	}

	public function page_delete($id)
	{



		DB::table('pages')->where('page_id', '=', $id)->delete();



		Session::flash('message', 'Page deleted successfully!');



		return Redirect('/admin/content_pagelist');



	}
	

/* start faq page */

    public function faq_listing(){ 

		DB::connection()->enableQueryLog();	

		$page_list = DB::table('faq')

			 ->select('*')

			->orderBy('id', 'asc')

			->get();

		$data_onview = array('page_list' =>$page_list);

		return View('admin.faq_list')->with($data_onview);

    }

	public function faq_form(Request $request)
	{
		if($request->id)
		{
			$id = $request->id;

			$page_detail  = DB::table('faq')->where('id', '=' ,$id)->get();		

  		  	$data_onview = array('page_detail' =>$page_detail,'id'=>$id); 

			return View('admin.faq_form')->with($data_onview);

		}
		else
		{
			$id =0;

  		  	$data_onview = array('id'=>$id); 

			return View('admin.faq_form')->with($data_onview);

		}

	}

	public function faq_action(Request $request)
	{
		$page_id = Input::get('page_id');

		$date = date('Y-m-d H:i:s');

		if($page_id==0)
		{

			$input['page_title'] = Input::get('page_title');

			$rules = array('page_title' => 'unique:pages,page_title');

			$validator = Validator::make($input, $rules);

			Session::flash('error', 'Duplicate Faq Entry!'); 

			if ($validator->fails()) {		

				return redirect()->to('/admin/faq-form');

			}
			else {

				$page_title = Input::get('page_title');

				$page_content = Input::get('page_content');		

				$page_status = Input::get('page_status');

					$page = DB::table('faq')

				            ->insert(['title' => $page_title,

									  'content'=> $page_content,

									  'status'=> $page_status,

									  'created_at'=> $date,

									  'updated_at'=> $date

									 ]);

					Session::flash('message', 'Faq Inserted Successfully!'); 

					return redirect()->to('/admin/faq');

			}

		}
		else
		{

			DB::table('faq')

            ->where('id', $page_id)

            ->update(['title' => trim(Input::get('page_title')),

					  'content'=> trim(Input::get('page_content')),

					  'status'=> Input::get('page_status'),

					  'updated_at'=> $date

					 ]);		 

				Session::flash('message', 'Faq Updated Successfully!');

				return redirect()->to('/admin/faq');

		}		

	}

	public function faq_delete($id)
	{

		DB::table('faq')->where('id', '=', $id)->delete();

		Session::flash('message', 'Faq deleted successfully!');

		return Redirect('/admin/faq');

	}


/*  end of faq */

	
	public function seoUrl($string) {



	$new_string='';		



        //Lower case everything



        $string1 = strtolower(trim($string));



        //Make alphanumeric (removes all other characters)



        $string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);



        //Clean up multiple dashes or whitespaces



        $string1 = preg_replace("/[\s-]+/", " ", $string1);



        //Convert whitespaces and underscore to dash



        $string1 = preg_replace("/[\s_]/", "-", $string1);		



		



        if( !empty($string1) || $string1 != '' ){		



            $new_string =  $string1;



        }else{



            $new_string =  $string;



        }



		



		$query =	DB::table('pages')



			 ->select('*')



			->where('page_url','=',$new_string)



			->get();



		//echo "<pre>"; print_r($query); die;







		if($query)



		{	



			// echo "<pre>"; print_r($query[0]->page_url); die;



			// // foreach($query as $key => value)



			$checkar='-ar';



			if (strpos( $query[0]->page_url, $checkar ) !== false) {



				$new_string='NA';



				return  $new_string;



			} else {



				$count = count($query)+1;



				return $new_string.'-ar';	



			}			



		}



		else



		{



			return  $new_string;



		}



 



    }





	/*******************************************  TESTIMONIAL MODULE  FUNCTIONS  *******************************************/



	function get_testiminial()



	{



		DB::connection()->enableQueryLog();	



		$testimonial_list = DB::table('testimonial')



			 ->select('*')



			->orderBy('id', 'asc')



			->get();	



		$data_onview = array('testimonial_list' =>$testimonial_list);



		return View('admin.testimonial_list')->with($data_onview);



	}



	public function get_testiminial_form(Request $request)



	{		



		if($request->id)



		{



			$id = $request->id;

	

			$testimonial_detail  = DB::table('testimonial')->where('id', '=' ,$id)->get();		



  		  	$data_onview = array('testimonial_detail' =>$testimonial_detail,'id'=>$id); 

			

			return View('admin.testimonial_form')->with($data_onview);

		

		}



		else



		{



			$id =0;



  		  	$data_onview = array('id'=>$id); 



			return View('admin.testimonial_form')->with($data_onview);



		}



		



	}





	public function get_testiminial_action(Request $request)



	{



		$id = Input::get('id');



		$cuisine_old_logo = $_POST['old_image'];



		$new_cuisine = '';



 /////****************************** CUISINE IMAGE ***************/



		

		if($request->hasFile('testi_image')){



		        $destinationPath = 'public/uploads/testimonial/';



				$image = $request->file('testi_image');



				$extension 		= 	$image->getClientOriginalExtension();



    			$imageRealPath 	= 	$image->getRealPath();



    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();



				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))



				{



					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();				



					$img = Image::make($imageRealPath)->resize('140','140')->save('public/uploads/testimonial/'.$thumbName);;



					asset($destinationPath . $thumbName);					



					$new_cuisine = $thumbName;



				} else {



					Session::flash('message_error', 'Image type is invalide!'); 



					if($cuisine_id>0){



						return redirect()->to('/admin/testimonial-form/'.$id);



					} else {



						return redirect()->to('/admin/testimonial_list');



					}



				}



		} elseif(!empty($cuisine_old_logo)) {



			 $new_cuisine = $cuisine_old_logo;



		}



 /////////////// END ***********************************************/



		if($id==0)



		{



			$cui = new Testimonial;



			$cui->name = Input::get('name');



			$cui->content = Input::get('content');



			$cui->image = $new_cuisine;



			$cui->status = Input::get('status');



	



			$cui->save();



			



			Session::flash('message', 'Testimonial Inserted Successfully!'); 



			return redirect()->to('/admin/testimonial_list');



		}



		else



		{



			DB::table('testimonial')



            ->where('id', $id)



            ->update(['name' => trim(Input::get('name')),



					  'content'=> trim(Input::get('content')),



					  'status'=> Input::get('status'),



					  'image'=> $new_cuisine



					 ]);



					 



				Session::flash('message', 'Testimonial Information Updated Successfully!');



				return redirect()->to('/admin/testimonial_list');



		}		



	}



		



	function testimonial_delete($id)



	{



				



		DB::table('testimonial')->where('id', '=', $id)->delete();



		Session::flash('message', 'Testimonial Deleted Successfully!');



		return Redirect('/admin/testimonial_list');



	}





	/*******************************************  TESTIMONIAL MODULE  FUNCTIONS  END  *******************************************/



	function get_advertisement()
	{

		DB::connection()->enableQueryLog();	

		$advertisement_list = DB::table('advertisement')

			 ->select('*')

			->orderBy('id', 'asc')

			->get();	

		$data_onview = array('advertisement_list' =>$advertisement_list);

		return View('admin.advertisement_list')->with($data_onview);
	}



	public function get_advertisement_form(Request $request)



	{		



		if($request->id)



		{



			$id = $request->id;

	

			$advertisement_detail  = DB::table('advertisement')->where('id', '=' ,$id)->get();		



  		  	$data_onview = array('advertisement_detail' =>$advertisement_detail,'id'=>$id); 

			

			return View('admin.advertisement_form')->with($data_onview);

		

		}



		else



		{



			$id =0;



  		  	$data_onview = array('id'=>$id); 



			return View('admin.advertisement_form')->with($data_onview);



		}



		



	}





	public function get_advertisement_action(Request $request)
	{

		$id = Input::get('id');

		$cuisine_old_logo = $_POST['old_image'];

		$new_cuisine = '';


 /////****************************** CUISINE IMAGE ***************/


		if($request->hasFile('testi_image')){

		        $destinationPath = 'public/uploads/advertisement/';

				$image = $request->file('testi_image');

				$extension 		= 	$image->getClientOriginalExtension();

    			$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();

				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))



				{



					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();				



					$img = Image::make($imageRealPath)->save('public/uploads/advertisement/'.$thumbName);;



					asset($destinationPath . $thumbName);					



					$new_cuisine = $thumbName;



				} else {



					Session::flash('message_error', 'Image type is invalide!'); 



					if($cuisine_id>0){



						return redirect()->to('/admin/advertisement-form/'.$id);



					} else {



						return redirect()->to('/admin/advertisement-list');



					}



				}



		} elseif(!empty($cuisine_old_logo)) {



			 $new_cuisine = $cuisine_old_logo;



		}



 /////////////// END ***********************************************/



		if($id==0)



		{



			$cui = new Advertisement;



			$cui->name = Input::get('name');



			$cui->content = Input::get('content');



			$cui->image = $new_cuisine;



			$cui->status = Input::get('status');



			$cui->save();		



			Session::flash('message', 'Record Inserted Successfully!'); 



			return redirect()->to('/admin/advertisement-list');



		}



		else



		{



			DB::table('advertisement')



            ->where('id', $id)



            ->update(['name' => trim(Input::get('name')),



					  'content'=> trim(Input::get('content')),



					  'status'=> Input::get('status'),



					  'image'=> $new_cuisine



					 ]);



					 



				Session::flash('message', 'Record Updated Successfully!');



				return redirect()->to('/admin/advertisement-list');



		}		



	}



		



	function advertisement_delete($id)



	{



				



		DB::table('advertisement')->where('id', '=', $id)->delete();



		Session::flash('message', 'Advertisement Deleted Successfully!');



		return Redirect('/admin/advertisement-list');



	}

	



	



	



	/************************************* SHOW COUPON REQUEST **************************************/



	



	



	function show_coupon_request()



	{



		DB::connection()->enableQueryLog();	







		$testimonial_list = DB::table('coupon_request')



			 ->select('*')



			->orderBy('req_id', 'desc')



			->get();







		



		$data_onview = array('coupon_list' =>$testimonial_list);



		



		return View('admin.coupon_list')->with($data_onview);







	}



	



	function coupon_request_delete($id)



	{



				



		DB::table('coupon_request')->where('req_id', '=', $id)->delete();



		Session::flash('message', 'Information Deleted Successfully!');



		return Redirect('/admin/coupon_request');



	}



	



		



	function delete_all_coupon()



	{



		



		foreach($_POST['user_id'] as $creq_id) 



		{



			DB::table('coupon_request')->where('req_id', '=', $creq_id)->delete();



		}



		



		Session::flash('message', 'Information Deleted Successfully!');



		return Redirect('/admin/coupon_request');



		



	}



	



	



	//*/**********************END ***********************************////////



	



	



	



	



	



	



	/************************** HOME PAGE CONTENT ******************/



	function home_content()



	{



		$home_list = DB::table('home_content')



			 ->select('*')



			->orderBy('home_id', 'desc')



			->get();







		



		$data_onview = array('content_list' =>$home_list);



		



		return View('admin.home_content_list')->with($data_onview);



	}



	



	function show_content_form()



	{



		



		if(Route::current()->getParameter('id'))



		{



			 $id = Route::current()->getParameter('id');



				



			$page_detail  = DB::table('home_content')->where('home_id', '=' ,$id)->get();		



			



  		  	$data_onview = array('page_detail' =>$page_detail,



								'id'=>$id); 



						



			return View('admin.content_form')->with($data_onview);



		



		}



		else



		{



				return redirect()->to('/admin/homepage_content');



		}



		



	}



	



	



	function homepage_action()



	{



		



	/*	echo '<pre>';



		print_r($_POST);



		exit;*/



		



		$home_id = Input::get('home_id');



		



		



			DB::table('home_content')



            ->where('home_id', $home_id)



            ->update(['home_title' => trim(Input::get('home_title')),



					  'home_content'=> trim(Input::get('home_content')),



					  'home_content_ar'=> trim(Input::get('home_content_ar'))



					 ]);



					 



				Session::flash('message', 'Information Updated Successfully!');



				return redirect()->to('/admin/homepage_content');



	



	



	}



	



	/************      END          ******************/



	



	



	



	



	/*****************************************************************/



	



	



	



	function sendNotification($target,$user_id, $title, $description, $devicetype){



		//FCM API end-point



		$url = 'https://fcm.googleapis.com/fcm/send';



		//api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key



		



		// $server_key = "AAAABbj1iac:APA91bHB98reqH6mRC6t51StUorXZopv9cTq2hJx2W_AQ2Ln6wPJ2HVJYMK0OZ6somHSS2eZH_TlFtyz5Skrx4YtoX5gbUM7SmKO-xnO-DbjCgOn-i7L27-c_Qtzm5tj_YCA235v1vxw";



		$server_key = 'AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1';



		



		$data =  array("self_id"=>$user_id, "title"=>$title, "message"=>$description);



		



		$fields = array();



		$fields['data'] = $data;







		if(is_array($target)){



		$fields['registration_ids'] = $target;



		}else{



		$fields['to'] = $target;



		}



		//header with content_type api key



		$headers = array(



		'Content-Type:application/json',



		 'Authorization:key='.$server_key



		);



		//CURL request to route notification to FCM connection server (provided by Google)	



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_URL, $url);



		curl_setopt($ch, CURLOPT_POST, true);



		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);



		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);



		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));



		$result = curl_exec($ch);



		//echo $result;die;



		if ($result === FALSE) {



		return false;



		}



		curl_close($ch);



		$jsn =json_decode($result);



		



		if($jsn->success){



		return '1';



		}



		else{



		return '0';



		}







	}



	



	



	function send_notification_ios($token,$title,$body)



	{



				



		$url = "https://fcm.googleapis.com/fcm/send";



		



		//$serverKey = 'AIzaSyBVAHr3vvKUb8-gtvDSvfLv_-1NWITPRpY';



		// $serverKey = 'AAAABbj1iac:APA91bHB98reqH6mRC6t51StUorXZopv9cTq2hJx2W_AQ2Ln6wPJ2HVJYMK0OZ6somHSS2eZH_TlFtyz5Skrx4YtoX5gbUM7SmKO-xnO-DbjCgOn-i7L27-c_Qtzm5tj_YCA235v1vxw';



		$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



		//$title = "Title";



		//$body = "Body of the message";



		



		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');



		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



		$json = json_encode($arrayToSend);



		$headers = array();



		$headers[] = 'Content-Type: application/json';



		$headers[] = 'Authorization: key='. $serverKey;



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_URL, $url);



		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);



        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");



		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);



		curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);



		



		$response = curl_exec($ch);



		



		if ($response === FALSE) {



			return '0';



			//die('FCM Send Error: ' . curl_error($ch));



		}



		else



		{



			return '1';



		}



		curl_close($ch);



		



	}



	



	function show_send_notification()



	{



		$data_onview ='';



		



		DB::enableQueryLog(); 	



		



		$user_info = DB::table('device_token')		 



	 				->Join('users', 'device_token.userid', '=', 'users.id')			



					->select('users.*','device_token.devicetoken','device_token.devicetype','device_token.deviceid'	)	



					->where('device_token.notification_status', '=' , '1')



					->orderBy('device_token.id', 'desc')



					->get();	



					



		$data_onview['user_info']=	$user_info	;



		



		



		return View('admin.notification_from')->with($data_onview);



	}







	function notification_list()



	{



		$user_notif_detail = '';







		$user_notif_detail  =  DB::table('notification_list')		



								->select('*')



								->orderBy('notification_list.noti_id', 'desc')



								->get();







		$vendor_list  =  DB::table('vendor')->get();







		$delivery_notif_detail	= DB::table('deliveryman_notification_list')



									->orderBy('deliveryman_notification_list.noti_id', 'desc')



									->select('*')



									->get();







		$vendor_notif_detail	= DB::table('vendor_notification_list')



									->select('*')



									->orderBy('vendor_notification_list.noti_id', 'desc')



									->get();



		DB::table('notification_list')->update([ 'noti_read'=> 1 ]);



  		$data_onview = array('user_notif_detail'=>$user_notif_detail,'delivery_notif_detail'=>$delivery_notif_detail,'vendor_notif_detail'=>$vendor_notif_detail);	



		



		return View('admin.notification_list')->with($data_onview);



	}







	function delete_vendor_notification($id)



	{	



		if(!empty($id)){



			DB::table('vendor_notification_list')->where('noti_id', '=', $id)->delete();



			return 1;



		} else {



			return 0;	



		}		



	}







	function delete_deliveryman_notification($id)



	{	



		if(!empty($id)){



			DB::table('deliveryman_notification_list')->where('noti_id', '=', $id)->delete();



			return 1;



		} else {



			return 0;	



		}		



	}







	function delete_user_notification($id)



	{	



		if(!empty($id)){



			DB::table('notification_list')->where('noti_id', '=', $id)->delete();



			return 1;



		} else {



			return 0;	



		}		



	}



	



	function read_user_notification()



	{	



		DB::table('notification_list')->update([ 'noti_read'=> 1 ]);



			return 1;



	}







	function read_dman_notification()



	{	



		DB::table('deliveryman_notification_list')->update([ 'noti_read'=> 1 ]);



			return 1;



	}







	function read_vendor_notification()



	{	



		DB::table('vendor_notification_list')->update([ 'noti_read'=> 1 ]);



			return 1;



	}







	function notification_action()



	{



		/********* SEND NOTIFICATION FOR RECEIVE ORDER  ***********/



		if(isset($_POST['user_id']) && (count($_POST['user_id'])>0))



		{



			$c=0;



			foreach($_POST['user_id'] as $ulist)



			{



				$user_id = $ulist;



				$notification_title = $_POST['title'];



				$notification_description =$_POST['noti_content'];		



							



				$token_data = DB::table('device_token')->where('userid', '=', trim($user_id))->get();	



				//$target = "fZqQg0Zap5M:APA91bHKf0536rKtBvDcWsrk4A2UgnTnUrkvN7ZcdFcZURgaWrWOS-DzkP0biEoFVlwiRiLeJ2t6EWbwUjgz0xYw0hKPsvQmhYulJYBf6SUtQW80_5GUetzqqa1JiUKtEXzXF7LZl0KD";						



				$target = $token_data[0]->devicetoken;



				$deviceid = $token_data[0]->deviceid;



				$devicetype = $token_data[0]->devicetype;



				 



				if($devicetype=='android')



				{



					$iosadres = $this->sendNotification($target,$user_id, $notification_title, $notification_description, $devicetype);



				}



				elseif($devicetype=='iphone')



				{



					$iosadres = $this->send_notification_ios($target,$notification_title, $notification_description);



				}



								



					$notifiy = new Notification_list;



					$notifiy->noti_userid = $user_id;



					$notifiy->noti_guestid = '';	



					$notifiy->noti_title = $notification_title;	



					$notifiy->noti_desc = $notification_description;	



					$notifiy->noti_deviceid = $deviceid;	



					$notifiy->noti_devicetype = $devicetype;	



					$notifiy->noti_read = '0';



					$notifiy->save();						



					



					$noti_id = $notifiy->noti_id;	



					$c++;



				}







				if($c==count($_POST['user_id']))



				{



					//if($iosadres){			



					Session::flash('message', 'Notification Send Successfully.');



				    /* }else{



				    Session::flash('message', 'Notification Not Send');	



				    } */



					return redirect()->to('/admin/send_notification');



				}



				/************** SEND NOTIFICATION FOR RECEIVE ORDER  ***************/



		} else



		{			



			Session::flash('message', 'Please select user name.');



			return redirect()->to('/admin/send_notification');



		}



	}



	/************   EMAIL NOTIFICATION  CONTENT  ****************************/



	



	function email_content()



	{



		$email_content_list = DB::table('email_content')



			 ->select('*')



			->orderBy('email_id', 'asc')



			->get();



		$data_onview = array('email_content_list' =>$email_content_list);



		return View('admin.email_content_list')->with($data_onview);



	}



	



	function show_email_form(Request $request)



	{



		if($request->id)



		{



			$id = $request->id;

		

			$page_detail  = DB::table('email_content')->where('email_id', '=' ,$id)->get();		



  		  	$data_onview = array('page_detail' =>$page_detail,



								'id'=>$id); 



			return View('admin.email_content_form')->with($data_onview);



		}



		else



		{



				return redirect()->to('/admin/email_content');



		}



		



	}



	



	



	function email_content_action()



	{



		



		/*echo '<pre>';



		print_r($_POST);



		exit;



*/		



		$email_id = Input::get('email_id');



		



		



			DB::table('email_content')



            ->where('email_id', $email_id)



            ->update(['email_title' => trim(Input::get('email_title')),



					  'email_content'=> trim(Input::get('email_content')),



					  'content_type'=> trim(Input::get('content_type'))



					 ]);



					 



				Session::flash('message', 'Information Updated Successfully!');



				return redirect()->to('/admin/email_content');



	



	



	}



	



	



	



	/*****************    EMAIL NOTIFICATION  CONTENT   END ****************/



	



	



	



	/******************* SHOW RESTAURANT SUGGESION REQUEST *************/



	



	



	function show_suggestion_list()



	{



		DB::connection()->enableQueryLog();	







		$testimonial_list = DB::table('suggest_restaurant')



			 ->select('*')



			->orderBy('sug_id', 'desc')



			->get();







		



		$data_onview = array('suggest_list' =>$testimonial_list);



		



		return View('admin.suggest_restaurant_list')->with($data_onview);







	}



	



	function suggestion_request_delete($id)



	{



				



		DB::table('suggest_restaurant')->where('sug_id', '=', $id)->delete();



		Session::flash('message', 'Information Deleted Successfully!');



		return Redirect('/admin/suggestion_list');



	}



	



	//*/**********************END ************////////



	



	



	



	/********************* SOCIAL FUNCTION *********************/



	







    public function social_listing(){



    	



		//echo  'TEST';



		DB::connection()->enableQueryLog();	







		$page_list = DB::table('social_link')



			 ->select('*')



			->orderBy('social_id', 'desc')



			->get();







		



		$data_onview = array('social_list' =>$page_list);



		



		return View('admin.social_list')->with($data_onview);



    }



	



	public function show_socila_form()



	{



		if(Route::current()->getParameter('id'))



		{



			 $id = Route::current()->getParameter('id');



				



			$page_detail  = DB::table('social_link')->where('social_id', '=' ,$id)->get();		



			



  		  	$data_onview = array('page_detail' =>$page_detail,



								'id'=>$id); 



						



			return View('admin.social_form')->with($data_onview);



		



		}



		else



		{



				return redirect()->to('/admin/socail_pagelist');



		}



	}



	



	public function get_social_action()



	{



		/*echo '<pre>';



		print_r($_POST);*/		



		



		



		$social_id = Input::get('social_id');



		



			DB::table('social_link')



            ->where('social_id', $social_id)



            ->update(['social_name' => trim(Input::get('social_title')),



					  'social_link'=> trim(Input::get('social_link')),



					  'social_status'=> Input::get('social_status')



					 ]);



					 



				Session::flash('message', 'Information Updated Successfully!');



				return redirect()->to('/admin/socail_pagelist');



		



		



	}



	



}
