<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**

 * 

 */

class AgeVerificationController extends Controller
{

	public function confirmationPage(Request $request)
	{

		return view("above-21");

	}

	public function verified(Request $request)
	{

		setcookie('above21',"true", time() + (86400 * 30), "/");
		return redirect()->intended("/");
	}

   	public function noverified(Request $request)
	{

		return view("come-back-21");

	}	


}