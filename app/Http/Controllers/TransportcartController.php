<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;


use Cart;

class TransportcartController extends Controller
{
   public function __construct(){ 
	    //	$this->middleware('auth');
    }
	
	public function add_tocart(Request $request) 
	{
	
			$date1=date_create(Session::get('cart_bookinginfo')['vehicle_ondate']);
			$date2=date_create(Session::get('cart_bookinginfo')['vehicle_returndate']);
			
			$diff=date_diff($date1,$date2);
			$count_no_days =  $diff->format("%a");
			
			$total_day_qty = $count_no_days+1;
			
	
		Cart::destroy();
		$sub_total_amt = 0;
		$method = $request->method();

		if ($method=='POST') {
			//echo '<pre>';
			//print_r($_POST);
			$price = '';
			$qty =$total_day_qty;
			$name = '';
			$option_name = '';
			
			$trans_id =Input::get('trans_id');
			$vehicle_id =Input::get('vehicle_id') ;
			$rate_id = Input::get('rate_id');
			
			$item_detail  = DB::table('vehicle_rate')->where('rate_id', '=' ,$rate_id)->get();	
			$name = $item_detail[0]->rate_name;
			if($item_detail[0]->rate_diff=='no')
			{
				$price =$item_detail[0]->rate_price;
			}
			if($item_detail[0]->rate_diff=='yes')
			{
				$option_name = Input::get('menu_option');
				
			 	
				if(Input::get('menu_option')=='large')
				{
					$name =$name.nl2br($item_detail[0]->rate_lname);
					$price =$item_detail[0]->rate_lprice;
				}
				if(Input::get('menu_option')=='medium')
				{
					$name =$name.nl2br($item_detail[0]->rate_mname);
					$price =$item_detail[0]->rate_mprice;
				}
				if(Input::get('menu_option')=='small')
				{
					
					$name =$name.nl2br($item_detail[0]->rate_sname);
					$price =$item_detail[0]->rate_sprice;
				}
				
			}
			
			$addon_list='';
			if((Input::get('group_name')) && (!empty(Input::get('group_name'))))
			{
			
				$group_name = Input::get('group_name');
				
				foreach($group_name as $group_list)
				{
					//echo '<br>'.$group_list;
					$option_group = $group_list.'_option';
					if(Input::get($option_group))
					{
						$addon_group = Input::get($option_group);
						foreach($addon_group as $adlist)
						{
							$ad_detail = DB::table('vehicle_addons')->where('addon_id', '=' ,$adlist)->get();	
						//	print_r($ad_detail);
							$ad_name = trim($ad_detail[0]->addon_groupname).': '.trim($ad_detail[0]->addon_name);
							$ad_price = ($ad_detail[0]->addon_price)*$qty ;
							$addon_list[]=array('name'=>$ad_name,'price'=>$ad_price,'id'=>$ad_detail[0]->addon_id);
							
						}
					}
				
				}
				
				
			}
				
			
			Cart::add(array('id'=>$rate_id, 
							'name' => $name,
							'qty' => $qty,
							'price' => ($price*$qty),
							'options'=>['rate_id' => $rate_id, 
										'trans_id' => $trans_id, 		
										'vehicle_id' => $vehicle_id,
										'option_name'=>$option_name,
										'addon_data'=>$addon_list
										]
							)
					);
		}

  	  $cart = Cart::content();
	  
	  $sub_total_price = '0';
	  foreach($cart as $item)
	  {
	  	$sub_total_price = ($sub_total_price)+($item->price);				 
		if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0)){
			foreach($item->options['addon_data'] as $addon){
				$sub_total_price = ($sub_total_price)+($addon['price']);	
			}
		}
	  }
	 
	 $cart_view =  view('transport_cart', array('cart' => $cart))->render();
	 
     return response()->json( array('total_amt' => $sub_total_price, 'cart_view'=>$cart_view) );
	 
	 
	}
	
	
	public function qty_icnrement(Request $request) 
	{
		$method = $request->method();
		$menu_catid = Input::get('menu_cat_id');
		$cart_row_id = Input::get('cart_row_id');
		$rest_id = '';	
		$menu_id = '';	
		$option_name = '';	
														
		$price_one = '';
		$item = Cart::search(function ($cartItem, $rowId) {
			return $cartItem->id === Input::get('menu_cat_id');
		});
		//echo '<pre>';
		//print_r($item);
		foreach( $item as $a)
		{
		
		
		if($cart_row_id==$a->rowId)
		{				
		 $menu_catid = $a->id;
		 $rest_id = $a->options['rest_id'];	
		 $menu_id = $a->options['menu_id'];	
		 $option_name = $a->options['option_name'];		 
		 $new_qty = ($a->qty + 1);
		 
				$item_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$menu_catid)->get();	
				$name = $item_detail[0]->menu_category_name;
				if($item_detail[0]->menu_category_portion=='no')
				{
					$price_one =$item_detail[0]->menu_category_price;
				}			
				elseif($item_detail[0]->menu_category_portion=='yes')
				{
					 $option_name = $a->options['option_name'];
					
				  $option_detail  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_catid)->get();	
					if($option_name=='large')
					{
						$price_one =$option_detail[0]->menu_large_price;
					}
					if($option_name=='medium')
					{
						$price_one =$option_detail[0]->menu_medium_price;
					}
					if($option_name=='small')
					{
						$price_one =$option_detail[0]->menu_small_price;
					}
					
				}
				
				
				
				$addon_list='';
				if(!empty($a->options['addon_data'])&&(count($a->options['addon_data'])>0))
				{
				
					foreach($a->options['addon_data'] as $addon)
					{
					
						$ad_detail = DB::table('menu_category_addon')->where('addon_id', '=' ,$addon['id'])->get();	
				
						$ad_name = trim($ad_detail[0]->addon_groupname).': '.trim($ad_detail[0]->addon_name);
						$ad_price = $new_qty *($ad_detail[0]->addon_price);
						$addon_list[]=array('name'=>$ad_name,'price'=>$ad_price,'id'=>$ad_detail[0]->addon_id);
					
					}
					 
				
					
					
				}
			
				
				
		
		 $new_prtice = 	($new_qty*$price_one);
		 
		 
		 
		 
		 
		 
				
				Cart::update($a->rowId,array('qty' => $new_qty,
											 'price' => $new_prtice,											 
											 'options'=>['menu_catid' => $menu_catid, 
														'rest_id' => $rest_id, 		
														'menu_id' => $menu_id,
														'option_name'=>$option_name,
														'addon_data'=>$addon_list
													  ]
											 )
								);
			}			
				
		}


  	  $cart = Cart::content();
	  
	  
	
	  $sub_total_price = '0';
	  foreach($cart as $item)
	  {
	  	$sub_total_price = ($sub_total_price)+($item->price);				 
		if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0)){
			foreach($item->options['addon_data'] as $addon){
				$sub_total_price = ($sub_total_price)+($addon['price']);	
			}
		}
	  }
	 
	 $cart_view =  view('cart', array('cart' => $cart))->render();
	 
     return response()->json( array('total_amt' => $sub_total_price, 'cart_view'=>$cart_view) );
	 
	}



	
	public function qty_decrease(Request $request) 
	{
		$method = $request->method();
		$rest_id ='';	
		$menu_id = '';	
		$option_name = '';		
		 
		$menu_catid = Input::get('menu_cat_id');
		
		$cart_row_id = Input::get('cart_row_id');
		
		$price_one = '';
		$item = Cart::search(function ($cartItem, $rowId) {
			return $cartItem->id === Input::get('menu_cat_id');
		});
		foreach( $item as $a)
		{
		
			
			
		if($cart_row_id==$a->rowId)
		{	
		 $menu_catid = $a->id;	 
		 
		 $rest_id = $a->options['rest_id'];	
		 $menu_id = $a->options['menu_id'];	
		 $option_name = $a->options['option_name'];		
		 
		 $new_qty = ($a->qty - 1);
		 
			 if($new_qty>0)
			 {
		 
				$item_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$menu_catid)->get();	
				$name = $item_detail[0]->menu_category_name;
				if($item_detail[0]->menu_category_portion=='no')
				{
					$price_one =$item_detail[0]->menu_category_price;
				}						
				elseif($item_detail[0]->menu_category_portion=='yes')
				{
					 $option_name = $a->options['option_name'];
					
				  $option_detail  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_catid)->get();	
					if($option_name=='large')
					{
						$price_one =$option_detail[0]->menu_large_price;
					}
					if($option_name=='medium')
					{
						$price_one =$option_detail[0]->menu_medium_price;
					}
					if($option_name=='small')
					{
						$price_one =$option_detail[0]->menu_small_price;
					}
					
				}
				
				
				$addon_list='';
				if(!empty($a->options['addon_data'])&&(count($a->options['addon_data'])>0))
				{
				
					foreach($a->options['addon_data'] as $addon)
					{
					
						$ad_detail = DB::table('menu_category_addon')->where('addon_id', '=' ,$addon['id'])->get();	
				
						$ad_name = trim($ad_detail[0]->addon_groupname).': '.trim($ad_detail[0]->addon_name);
						$ad_price = $new_qty *($ad_detail[0]->addon_price);
						$addon_list[]=array('name'=>$ad_name,'price'=>$ad_price,'id'=>$ad_detail[0]->addon_id);
					
					}
					 
				
					
					
				}
				
		
		 	$new_prtice = 	($new_qty*$price_one);
				
				 Cart::update($a->rowId,array('qty' => $new_qty,
											 'price' => $new_prtice,											 
											 'options'=>['menu_catid' => $menu_catid, 
														'rest_id' => $rest_id, 		
														'menu_id' => $menu_id,
														'option_name'=>$option_name,
														'addon_data'=>$addon_list
													  ]

											 )
								);
		 	}	
			else
			{
				Cart::remove($a->rowId);
			}						
				
		}
		}

  	$cart = Cart::content();
  	  
	  $sub_total_price = '0';
	  foreach($cart as $item)
	  {
	  	$sub_total_price = ($sub_total_price)+($item->price);				 
		if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0)){
			foreach($item->options['addon_data'] as $addon){
				$sub_total_price = ($sub_total_price)+($addon['price']);	
			}
		}
	  }
	 
	 $cart_view =  view('cart', array('cart' => $cart))->render();
	 
     return response()->json( array('total_amt' => $sub_total_price, 'cart_view'=>$cart_view) );
	 
	}
	
}
