<?php

namespace App\Http\Controllers;

use DB;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Cart;
use Auth;
use Session;
use App\Rest_cart;
use Route;

class CartController extends Controller
{
   public function __construct(){ 
	    //	$this->middleware('auth');
    }
	
	public function add_tocart(Request $request) 
	{
		/*echo '<pre>';
		print_r($_POST['rest_order_type']);die;*/
				
		$addon_array = array();
		
		$user_id = '0';
		$guest_id = '';
		$rest_id =Input::get('rest_id');
		$menu_id =Input::get('menu_id');
		//echo $sub_total =Input::get('sub_total');
		$menu_catid = Input::get('menu_cat_id');
		$food_size_id = Input::get('menu_option');
		$qty = 1;
		$special_text = Input::get('special_text');
		$order_type = $_POST['rest_order_type'];
		$food_addonid='';
	
				
		if((Input::get('group_name')) && (!empty(Input::get('group_name'))))
		{			
			$group_name = Input::get('group_name');
			
			foreach($group_name as $group_list)
			{
				$option_group = $group_list.'_option';
				if(Input::get($option_group))
				{
					$addon_group = Input::get($option_group);
					foreach($addon_group as $adlist)
					{
						array_push($addon_array, $adlist);
					}
				}
			
			}
		}
		if(!empty($addon_array))
		{
			$food_addonid = implode(',',$addon_array);
		}
		
		
		if(!Auth::guest()) 
		{
			$user_id = Auth::user()->id;
		}
		else
		{
			$guest_id = Session::get('_token');
		}
		

        if($user_id){

		$user_ids = trim($user_id);
		$userkey = 'user_id';

		}else{ 

		$user_ids = trim($guest_id);
		$userkey = 'guest_id';

		}

	    $checkatcd = DB::table('rest_cart')
									->select('*')
									->where($userkey, '=' ,trim($user_ids))
									->where('rest_id', '=' ,trim($rest_id))
									->where('menu_id', '=' ,trim($menu_id))
									->where('menu_catid', '=' ,trim($menu_catid))
									->first(); 
        if($checkatcd){ 
        $cart_id = $checkatcd->cart_id;	
		$cart_listing = DB::table('rest_cart');
		$cart_listing = $cart_listing->select('*');
		$cart_listing = $cart_listing->where('cart_id', '=' ,trim($cart_id));
		$cart_listing = $cart_listing->get();
		
		$db_qty = $cart_listing[0]->qty;
		$rest_id = $cart_listing[0]->rest_id;
		
		$new_qty = ($db_qty+1);

		if(!empty($special_text)){
				DB::table('rest_cart')
				->where('cart_id', trim($cart_id))
				->update(['qty' =>  trim($new_qty),'special_instruction' =>  trim($special_text)]);	
		}else{

       		 DB::table('rest_cart')
			 ->where('cart_id', trim($cart_id))
			 ->update(['qty' =>  trim($new_qty)]);	
		}		

        }else{
			
			$cart_data = new Rest_cart;
			$cart_data->user_id	 = trim($user_id);
			$cart_data->guest_id = trim($guest_id);
			$cart_data->rest_id = trim($rest_id);
			$cart_data->menu_id = trim($menu_id);
			$cart_data->menu_catid = trim($menu_catid);
			$cart_data->menu_subcatid = trim($food_size_id);
			$cart_data->menu_addonid = trim($food_addonid);
			$cart_data->qty = trim($qty);
			$cart_data->special_instruction =  trim($special_text);
			$cart_data->order_type = trim($order_type);	
			$cart_data->save();



		}
			
			return $this->view_cart_data($rest_id);
		//}
			
	
		
   }
	
	
	public function qty_icnrement(Request $request) 
	{
		$method = $request->method();
		
		
		$cart_id = Input::get("cart_row_id");	
		$cart_listing = DB::table('rest_cart');
		$cart_listing = $cart_listing->select('*');
		$cart_listing = $cart_listing->where('cart_id', '=' ,trim($cart_id));
		$cart_listing = $cart_listing->get();
		
		$db_qty = $cart_listing[0]->qty;
		$rest_id = $cart_listing[0]->rest_id;
		
		$new_qty = ($db_qty+1);
				DB::table('rest_cart')
				->where('cart_id', trim($cart_id))
				->update(['qty' =>  trim($new_qty)
					 ]);
					 
		return $this->view_cart_data($rest_id);			 
	 
	}



	
	public function qty_decrease(Request $request) 
	{
		$method = $request->method();
		
		
		$cart_id = Input::get("cart_row_id");	
		$cart_listing = DB::table('rest_cart');
		$cart_listing = $cart_listing->select('*');
		$cart_listing = $cart_listing->where('cart_id', '=' ,trim($cart_id));
		$cart_listing = $cart_listing->get();
		//echo "<pre>"; print_r($cart_listing); die;
		//$db_qty
		if(!empty($cart_listing)){
			$db_qty = $cart_listing[0]->qty;
			$rest_id = $cart_listing[0]->rest_id;
		}
		$new_qty = ($db_qty-1);
		if($new_qty>0)
		{
				DB::table('rest_cart')
				->where('cart_id', trim($cart_id))
				->update(['qty' =>  trim($new_qty)
					 ]);
		}
		else
		{
				DB::table('rest_cart')
				->where('cart_id', trim($cart_id))
				->delete();
		}			 
		return $this->view_cart_data($rest_id);	
	}
	
	
	
	
	public function qty_decrease_old(Request $request) 
	{
		$method = $request->method();
		$rest_id ='';	
		$menu_id = '';	
		$option_name = '';		
		 
		$menu_catid = Input::get('menu_cat_id');
		
		$cart_row_id = Input::get('cart_row_id');
		
		$price_one = '';
		$item = Cart::search(function ($cartItem, $rowId) {
			return $cartItem->id === Input::get('menu_cat_id');
		});
		foreach( $item as $a)
		{
		
			
			
		if($cart_row_id==$a->rowId)
		{	
		 $menu_catid = $a->id;	 
		 
		 $rest_id = $a->options['rest_id'];	
		 $menu_id = $a->options['menu_id'];	
		 $option_name = $a->options['option_name'];		
		 
		 $new_qty = ($a->qty - 1);
		 
			 if($new_qty>0)
			 {
		 
				$item_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$menu_catid)->get();	
				$name = $item_detail[0]->menu_category_name;
				if($item_detail[0]->menu_category_portion=='no')
				{
					$price_one =$item_detail[0]->menu_category_price;
				}						
				elseif($item_detail[0]->menu_category_portion=='yes')
				{
					 $option_name = $a->options['option_name'];
					
				  $option_detail  = DB::table('menu_category_item')->where('menu_category', '=' ,$menu_catid)->get();	
					if($option_name=='large')
					{
						$price_one =$option_detail[0]->menu_large_price;
					}
					if($option_name=='medium')
					{
						$price_one =$option_detail[0]->menu_medium_price;
					}
					if($option_name=='small')
					{
						$price_one =$option_detail[0]->menu_small_price;
					}
					
				}
				
				
				$addon_list=array();
				if(!empty($a->options['addon_data'])&&(count($a->options['addon_data'])>0))
				{
				
					foreach($a->options['addon_data'] as $addon)
					{
					
						$ad_detail = DB::table('menu_category_addon')->where('addon_id', '=' ,$addon['id'])->get();	
				
						$ad_name = trim($ad_detail[0]->addon_groupname).': '.trim($ad_detail[0]->addon_name);
						$ad_price = $new_qty *($ad_detail[0]->addon_price);
						array_push($addon_list, array('name'=>$ad_name,'price'=>$ad_price,'id'=>$ad_detail[0]->addon_id));
					
					}
					 
				
					
					
				}
				
		
		 	$new_prtice = 	($new_qty*$price_one);
				
				 Cart::update($a->rowId,array('qty' => $new_qty,
											 'price' => $new_prtice,											 
											 'options'=>['menu_catid' => $menu_catid, 
														'rest_id' => $rest_id, 		
														'menu_id' => $menu_id,
														'option_name'=>$option_name,
														'addon_data'=>$addon_list
													  ]

											 )
								);
		 	}	
			else
			{
				Cart::remove($a->rowId);
			}						
				
		}
		}

  	$cart = Cart::content();
  	  
	  $sub_total_price = '0';
	  foreach($cart as $item)
	  {
	  	$sub_total_price = ($sub_total_price)+($item->price);				 
		if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0)){
			foreach($item->options['addon_data'] as $addon){
				$sub_total_price = ($sub_total_price)+($addon['price']);	
			}
		}
	  }
	 
	 $cart_view =  view('cart', array('cart' => $cart))->render();
	 
     return response()->json( array('total_amt' => $sub_total_price, 'cart_view'=>$cart_view) );
	 
	}
	
	
	
	public function show_cart(Request $request) 
	{
		DB::enableQueryLog(); 
	
		$sub_total_amt = 0;
		$method = $request->method();

		$rest_id = Input::get('rest_id');
		
		  if(!Auth::guest()) 
		  {
			$user_id = Auth::user()->id;
		 
		
		 
		 
		  $cart_record_temp = DB::table('temp_cart')
								->select('*')
								->where('user_id', '=' ,trim($user_id))
								->where('rest_cartid', '=' ,'0')
								->where('rest_id', '=' ,trim($rest_id))
								->get();
				$total_record_temp = count($cart_record_temp);	
			
				if($total_record_temp>0)
				{
					$cart =  $cart_record_temp[0]->cart_detail;
				}
				else
				{
					$cart = Cart::content();
				}
		 
		 }
	 	else
		{
			$cart = Cart::content();
		}
	  
	  $sub_total_price = '0';
	  foreach($cart as $item)
	  {
	  	$sub_total_price = ($sub_total_price)+($item->price);				 
		if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0)){
			foreach($item->options['addon_data'] as $addon){
				$sub_total_price = ($sub_total_price)+($addon['price']);	
			}
		}
	  }
	  
	  
	 $cart_view =  view('cart', array('cart' => $cart))->render();
	 
	 
	 
	  $service_tax='0';
	  
	  if($sub_total_price>0)
	  {
	  		$text_rest  = Input::get('rest_tax');
			
			$service_tax = number_format((($sub_total_price*$text_rest)/100),2);
	  }
	  
	 
	 
	 /************************************************************************/
	 
	 $current_promo = array();
		$array_amt=array();	
			$promo_list = DB::table('promotion')		
						->where('promo_restid', '=' ,$rest_id)
						->where('promo_status', '=' ,'1')
						->orderBy('promo_on', 'asc')
						->orderBy('promo_id', 'desc')					
						->groupBy('promo_id')
						->get(); 	 
									
				if($promo_list)
				{
				
				
			
				$c=0;
					
					foreach($promo_list as $plist )
					{
						$current_promo1 = array();
						
						if($plist->promo_for=='order')
						{
							$current_promo1 = array();
							
							if($plist->promo_on=='schedul')
							{
									 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
							
									$db_day = '';
									
									if(!empty($plist->promo_day))
									{
										$db_day = explode(',',$plist->promo_day);
									}
									
								if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
								{
									
									if(
										($plist->promo_start<=date('Y-m-d')) && 
										($plist->promo_end>=date('Y-m-d')) && 
										(empty($plist->promo_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_price)))
										)
									{
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);						
									}
									
									
									if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
									 && (!empty($plist->promo_day))
									 && (in_array($day_name,$db_day) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_price))))
									 
									 )
									{
									
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);					
									}
									
									
								}
								elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_price))))
								{
								
								
								
									$db_day = explode(',',$plist->promo_day);
									
									if(in_array($day_name,$db_day))
									{
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);
									}
								
								}
								
								
								
							}
							else
							{
							
								if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_price))
								{		
									
									array_push($array_amt, $plist->promo_buy);
									$current_promo1['promo_id'] = $plist->promo_id;
									$current_promo1['promo_restid'] = $plist->promo_restid;
									$current_promo1['promo_for'] = $plist->promo_for;
									$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
									$current_promo1['promo_on'] = $plist->promo_on;
									$current_promo1['promo_desc'] = $plist->promo_desc;
									$current_promo1['promo_mode'] = $plist->promo_mode;
									$current_promo1['promo_buy'] = $plist->promo_buy;
									$current_promo1['promo_value'] = $plist->promo_value;
									$current_promo1['promo_end'] = $plist->promo_end;
									$current_promo1['promo_start'] = $plist->promo_start;				
									array_push($current_promo, $current_promo1);
								}
							}
						}
						
					}
				}
		
			
			//$promo_list = $current_promo;
			
			$promo_id = 0;
			
			
			
	 $offer_amt = 0;
	$cal_sub_totla = $sub_total_price;
	 if((count($current_promo)>0) && (!empty($array_amt))) {	
			
			  	$maxs = max($array_amt);
			  	$key_name = array_search ($maxs, $array_amt);			  
				$k=0;$i=0;
				foreach($current_promo as $pval)
				{
					if($key_name==$k)
					{
						$promo_id = $pval['promo_id'];
				
						if($pval['promo_mode']=='$')
						{
							$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
							$offer_amt = ($offer_amt+$pval['promo_value']);
							$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);
						}
						
						if(($pval['promo_mode']=='%') )
						{	$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
							$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);										
							$cal_sub_totla = ($cal_sub_totla - $promo_val_per);									
							$offer_amt = ($offer_amt+$promo_val_per);
						}
						
						if(($pval['promo_mode']=='0') )
						{
							$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
						}
					}
					$k++;
				}
				
			}
			else{			
				$offer_applied_on_this_cart= '';
				$current_promo = array();
			}
			
	 $tax_for_service=((trim($sub_total_cart)*$service_charge)/100);
	 /************************************************************************/
	 
     return response()->json( array('order_offer_on'=>$promo_id,'discount_amt'=>$offer_amt,'discount_txt'=>$offer_applied_on_this_cart,'promo_list' => json_encode($current_promo),'food_promo_list_cal' =>'','food_promo_applied_cal' =>'','service_tax' => $tax_for_service, 'total_amt' => $sub_total_price,'cart_view'=>$cart_view) );
	 
	 
	}
	
	
	public function view_cart_data($rest_id)
	{
	
		Cart::destroy();
	
		$rest_id =$rest_id;
		$user_id = '0';
		$guest_id = '';
		$json_data_session = array();

		$food_cart_amount = array();
		$food_cart_qty = array();
		$group_name = array();
		$group_list = array();
		$addon_list = array();
			
		if(!Auth::guest()) 
		{
			$user_id = Auth::user()->id;
		}
		else
		{
			$guest_id = Session::get('_token');
		}
					  
			DB::enableQueryLog(); 				
				
				$offer_amt=0;
				$offer_applied_on_this_cart='';
				$current_promo=array();
				$service_tax=0;
				$sub_total_price=0;
				$promo_id=0;
				$offer_applied_on_food='';
				$food_current_promo = array();
				$json_data_session=array();
				$sub_total_cart=0;
		
				$cart_food_id='';
				
				$cart_detail ='';
				$cart_listing = DB::table('rest_cart');
				$cart_listing = $cart_listing->select('*');
				$cart_listing = $cart_listing->where('user_id', '=' ,trim($user_id));
				$cart_listing = $cart_listing->where('guest_id', '=' ,trim($guest_id));
				$cart_listing = $cart_listing->where('rest_id', '=' ,trim($rest_id));
				$cart_listing = $cart_listing->orderBy('cart_id', 'asc');
				$cart_listing = $cart_listing->get();
				$service_charge="";
				if($cart_listing)
				{
						
					$delivery_fee = 0;	
					
					$rest_detail = DB::table('restaurant')		
							->where('rest_id', '=' ,$rest_id)
							->where('rest_status', '!=' , 'INACTIVE')
							->orderBy('rest_id', 'asc')				
							->groupBy('rest_id')
							->get(); 
							
					$promo_list = DB::table('promotion')		
									->where('promo_restid', '=' ,$rest_id)
									->where('promo_status', '=' ,'1')
									->orderBy('promo_buy', 'desc')					
									->groupBy('promo_id')
									->get();
										
							
					$service_charge = $rest_detail[0]->rest_servicetax;
				
					
					if($cart_listing[0]->order_type=='Delivery')
					{
						$delivery_fee = $rest_detail[0]->rest_mindelivery;			
					}
				

					$sub_total_cart = 0;
					$json_data_session = array();
					$addon_list =array();
					
					$food_current_promo = array();
				
						foreach($cart_listing as $clist)
						{	
								/********FOOD DETAILS /******/
								
								
								$rest_id  = $clist->rest_id;
								$menu_id = $clist->menu_id;
								$menu_cate_id =$clist->menu_catid;
								$menu_subcate_id = $clist->menu_subcatid;
								$addon_id = $clist->menu_addonid;
								$foodsize_price =0;
								$food_addonPrice = 0; 
								$total_price =0 ;
								
								$item_name_cc = '';
								$item_price_cc = '';
	
								$offer_on_menu_item = '';
								

								/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/
							
								if(($user_id>0) && ($user_id!=''))
								{
									foreach($promo_list as $plist )
									{
									
										if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))
										{
											$current_promo1 = array();
											
											array_push($food_cart_amount, $total_price);
											array_push($food_cart_qty, $clist->qty);
											
											if($plist->promo_on=='schedul')
											{
													 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
											
													$db_day = '';
													
													if(!empty($plist->promo_day))
													{
														$db_day = explode(',',$plist->promo_day);
													}
													
												if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
												{
													
													if(
														($plist->promo_start<=date('Y-m-d')) && 
														($plist->promo_end>=date('Y-m-d')) && 
														(empty($plist->promo_day)) && 
														($plist->promo_buy<=$clist->qty)
														)
													{
														$current_promo1['promo_id'] = $plist->promo_id;
														$current_promo1['promo_restid'] = $plist->promo_restid;
														$current_promo1['promo_for'] = $plist->promo_for;
														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
														$current_promo1['promo_on'] = $plist->promo_on;
														$current_promo1['promo_desc'] = $plist->promo_desc;
														$current_promo1['promo_mode'] = $plist->promo_mode;
														$current_promo1['promo_buy'] = $plist->promo_buy;
														$current_promo1['promo_value'] = $plist->promo_value;
														$current_promo1['promo_end'] = $plist->promo_end;
														$current_promo1['promo_start'] = $plist->promo_start;
														array_push($food_current_promo, $current_promo1);
														$offer_on_menu_item = $plist->promo_desc;

													}
													
													
													if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
													 && (!empty($plist->promo_day))
													 && (in_array($day_name,$db_day))
													 
													 )
													{
													
														
														array_push($array_amt, $plist->promo_buy);
														$current_promo1['promo_id'] = $plist->promo_id;
														$current_promo1['promo_restid'] = $plist->promo_restid;
														$current_promo1['promo_for'] = $plist->promo_for;
														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
														$current_promo1['promo_on'] = $plist->promo_on;
														$current_promo1['promo_desc'] = $plist->promo_desc;
														$current_promo1['promo_mode'] = $plist->promo_mode;
														$current_promo1['promo_buy'] = $plist->promo_buy;
														$current_promo1['promo_value'] = $plist->promo_value;
														$current_promo1['promo_end'] = $plist->promo_end;
														$current_promo1['promo_start'] = $plist->promo_start;
														array_push($food_current_promo, $current_promo1);	
														$offer_on_menu_item = $plist->promo_desc;					
													}
													
													
												}
												elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))
												{
												
												
												
													$db_day = explode(',',$plist->promo_day);
													
													if(in_array($day_name,$db_day))
													{
														
														array_push($array_amt, $plist->promo_buy);
														$current_promo1['promo_id'] = $plist->promo_id;
														$current_promo1['promo_restid'] = $plist->promo_restid;
														$current_promo1['promo_for'] = $plist->promo_for;
														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
														$current_promo1['promo_on'] = $plist->promo_on;
														$current_promo1['promo_desc'] = $plist->promo_desc;
														$current_promo1['promo_mode'] = $plist->promo_mode;
														$current_promo1['promo_buy'] = $plist->promo_buy;
														$current_promo1['promo_value'] = $plist->promo_value;
														$current_promo1['promo_end'] = $plist->promo_end;
														$current_promo1['promo_start'] = $plist->promo_start;
														array_push($food_current_promo, $current_promo1);
														$offer_on_menu_item = $plist->promo_desc;
													}
												
												}
												
												
												
											}
											elseif($plist->promo_on=='qty')
											{
											
												if(($plist->promo_buy<=$clist->qty))
												{		
													$current_promo1['promo_id'] = $plist->promo_id;
													$current_promo1['promo_restid'] = $plist->promo_restid;
													$current_promo1['promo_for'] = $plist->promo_for;
													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
													$current_promo1['promo_on'] = $plist->promo_on;
													$current_promo1['promo_desc'] = $plist->promo_desc;
													$current_promo1['promo_mode'] = $plist->promo_mode;
													$current_promo1['promo_buy'] = $plist->promo_buy;
													$current_promo1['promo_value'] = $plist->promo_value;
													$current_promo1['promo_end'] = $plist->promo_end;
													$current_promo1['promo_start'] = $plist->promo_start;	
													array_push($food_current_promo, $current_promo1);
													$offer_on_menu_item = $plist->promo_desc;
												}
											}
										}
									}
								}
								/*************************** END ******************************/
									
														
							
								$menu_list = DB::table('menu')		
								->where('restaurant_id', '=' ,$clist->rest_id)		
								->where('menu_id', '=' ,$clist->menu_id)
								->where('menu_status', '=' ,'1')
								->orderBy('menu_order', 'asc')
								->get();
								
								// dd(DB::getQueryLog()); 				
							
								$menu_array = array();
								$food_array=array();
											
								if(!empty($menu_list))
								{
									
									$menu_id = $menu_list[0]->menu_id;
									$menu_name = $menu_list[0]->menu_name;
									$menu_item_count = 1;
									$food_detail=array();
									
									
									$menu_cat_detail = DB::table('menu_category')		
											->where('menu_category.rest_id', '=' ,$rest_id)
											->where('menu_category.menu_id', '=' ,$menu_id)
											->where('menu_category.menu_category_id', '=' ,$menu_cate_id)
											->where('menu_category.menu_cat_status', '=' ,'1')
											->select('menu_category.*')
											->orderBy('menu_category.menu_category_id', 'asc')
											->get();
												

									$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;
									$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;
									$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;
									$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;
									$food_detail['food_name_ar'] = $item_name_cc_ar = $menu_cat_detail[0]->menu_category_name_ar;
									$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;					
									$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;
									$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;
									

									$food_detail['food_subitem_title']='';
																
									if($menu_cat_detail[0]->menu_category_portion=='yes')
									{	
										$food_detail['food_subitem_title'] = 'Choose a size';
										
										
										$food_size = DB::table('category_item')		
													->where('rest_id', '=' ,$rest_id)
													->where('menu_id', '=' ,$menu_id)
													->where('menu_category', '=' ,$menu_cate_id)
													->where('menu_cat_itm_id', '=' ,$menu_subcate_id)
													->where('menu_item_status', '=' ,'1')						
													->select('*')
													->orderBy('menu_cat_itm_id', 'asc')						
													->groupBy('menu_item_title')
													->get();
													
												
										$food_detail['food_size_detail']=$food_size;
										 $item_price_cc =$food_size[0]->menu_item_price;
										
										$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);						
										$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);
									
									}
									else
									{	
										$food_detail['food_size_detail']=array();	
										$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);			
									}
									$addon_list=array();
									if(!empty($addon_id)){		
									
									
										$addons = DB::table('menu_category_addon')		
												->where('addon_restid', '=' ,$rest_id)
												->where('addon_menuid', '=' ,$menu_id)
												->where('addon_status', '=' ,'1')						
												->where('addon_menucatid', '=' ,$menu_cate_id)
												->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
												->select('*')
												->orderBy('addon_id', 'asc')						
												->groupBy('addon_groupname')
												->get();
												
												$lang = Session::get('locale');
											
												if(!empty($addons))
												{
														
													foreach($addons as $ad_list)
													{
													
														$option_type = 0;
												
														if(substr($ad_list->addon_groupname, -1)=='*') 
														{
														   $option_type = 1;
														}	
													    		
													    array_push($group_name, $ad_list->addon_groupname);	
													
														$ff['addon_gropname'] = $ad_list->addon_groupname;
														$ff['addon_type'] = $ad_list->addon_option;
														$ff['addon_mandatory_or_not'] = $option_type;
														
														$addon_group = DB::table('menu_category_addon')		
															->where('addon_restid', '=' ,$rest_id)
															->where('addon_menuid', '=' ,$menu_id)
															->where('addon_status', '=' ,'1')	
															->where('addon_groupname', '=' ,$ad_list->addon_groupname)						
															->where('addon_menucatid', '=' ,$menu_cate_id)
															->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
															->select('*')
															->orderBy('addon_id', 'asc')	
															->get();
														array_push($group_list, $addon_group);
														$addon_group_list =array();
														foreach($addon_group as $group_list_loop)
														{
													
															array_push($addon_group_list, array('addon_id'=>$group_list_loop->addon_id,
															'addon_menucatid'=>$group_list_loop->addon_menucatid,
															'addon_groupname'=>$group_list_loop->addon_groupname,
															'addon_option'=>$group_list_loop->addon_option,
															'addon_name'=>$group_list_loop->addon_name,
															'addon_name_ar'=>$group_list_loop->addon_name_ar,
															'addon_price'=>$group_list_loop->addon_price,
															'addon_status'=>$group_list_loop->addon_status
															));
															
															
															array_push($addon_list, array('name'=>$group_list_loop->addon_name,
																'name_ar'=>$group_list_loop->addon_name_ar,
															'price'=>$group_list_loop->addon_price,
															'id'=>$group_list_loop->addon_id));
															
															
															$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);					
															$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';
														
														}
														$ff['addon_detail'] = $addon_group_list;
														$food_detail['food_addon'] = array();
														array_push($food_detail['food_addon'], $ff);	
														
														
														
																							
													}
													
												}	
									}
									else
									{
										$food_detail['food_addon'] = array();
									}								
									
									array_push($food_array, array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item));
								}
								
								/******** FOOD DETAIL END ****/
							
								$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));

									Cart::add(array(
										'id'=>$clist->menu_catid, 
										'name' => $item_name_cc,
										'name_ar' => $item_name_cc_ar,
										'qty' => $clist->qty,
										'price' => number_format(($item_price_cc*$clist->qty),2),
										'options'=>['menu_catid' => $clist->menu_catid, 
													'instraction' => $clist->special_instruction,
													'rest_id' => $clist->rest_id, 		
													'menu_id' => $clist->menu_id,
													'option_name'=>$clist->menu_subcatid,
													'addon_data'=>$addon_list,
													'offer_on_item'=>$offer_on_menu_item
													]
										)
									);
						
									
							$json_data_session[$clist->cart_id] =	array("rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,"name"=>$item_name_cc,'name_ar' => $item_name_cc_ar,"qty"=>$clist->qty,"price"=>$item_price_cc,"instruction"=>$clist->special_instruction,			    "options"=> array ( "menu_catid"=>$clist->menu_catid,
												"rest_id"=>$clist->rest_id,
												"menu_id"=>$clist->menu_id,
												"option_name"=> $clist->menu_subcatid,
												"addon_data"=>$addon_list,
												"offer_name"=>$offer_on_menu_item),
										        "tax"=>'',
			           					        "subtotal"=>$item_price_cc
									);	
							
								}
				
								/*print_r($food_current_promo);
								print_r($food_amount);*/
							
					
							//$offer_sub_total = trim($sub_total_cart)-trim($offer_amt);
							$serv_tax  = (($sub_total_cart*$service_charge)/100);
								
								
									/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/
							$current_promo = array();
							$offer_applied_on_this_cart='';

					if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))
					{
						 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));				
						
								/*$promo_list = DB::table('promotion')		
									->where('promo_restid', '=' ,$rest_id)
									->where('promo_status', '=' ,'1')
									->where('promo_for', '=' ,'order')	
									->orderBy('promo_buy', 'desc')					
									->groupBy('promo_id')
									->get();*/ 	 
							
							
							// dd(DB::getQueryLog()); 	
							//print_r($promo_list);
									//	
							//	exit;
					
							$current_promo = array();
							$array_amt = array();
							
							foreach($promo_list as $plist )
							{
							
								if($plist->promo_for=='order')
								{
									$current_promo1 = array();
									
									if($plist->promo_on=='schedul')
									{
											 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
									
											$db_day = '';
											
											if(!empty($plist->promo_day))
											{
												$db_day = explode(',',$plist->promo_day);
											}
											
										if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
										{
											
											if(
												($plist->promo_start<=date('Y-m-d')) && 
												($plist->promo_end>=date('Y-m-d')) && 
												(empty($plist->promo_day)) && 
												((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))
												)
											{
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($current_promo, $current_promo1);						
											}
											
											
											if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
											 && (!empty($plist->promo_day))
											 && (in_array($day_name,$db_day) && 
												((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
											 
											 )
											{
											
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($current_promo, $current_promo1);					
											}
											
											
										}
										elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) && 
												((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
										{
										
										
										
											$db_day = explode(',',$plist->promo_day);
											
											if(in_array($day_name,$db_day))
											{
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($current_promo, $current_promo1);
											}
										
										}
																											
									}
									else
									{
									
										if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))
										{		
											
											array_push($array_amt, $plist->promo_buy);
											$current_promo1['promo_id'] = $plist->promo_id;
											$current_promo1['promo_restid'] = $plist->promo_restid;
											$current_promo1['promo_for'] = $plist->promo_for;
											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
											$current_promo1['promo_on'] = $plist->promo_on;
											$current_promo1['promo_desc'] = $plist->promo_desc;
											$current_promo1['promo_mode'] = $plist->promo_mode;
											$current_promo1['promo_buy'] = $plist->promo_buy;
											$current_promo1['promo_value'] = $plist->promo_value;
											$current_promo1['promo_end'] = $plist->promo_end;
											$current_promo1['promo_start'] = $plist->promo_start;

											array_push($current_promo, $current_promo1);
										}
									}
								}
							}		
					}
					else
					{
						$current_promo = array();
						$offer_applied_on_this_cart='';
					}
						
						$offer_amt = 0;
				 		$cal_sub_totla = $sub_total_cart;
				 					 
					if((count($current_promo)>0) && (!empty($array_amt))) {	
					
					  	$maxs = max($array_amt);
					  	$key_name = array_search ($maxs, $array_amt);			  
						$k=0;$i=0;
						foreach($current_promo as $pval)
						{
							if($key_name==$k)
							{
						
								if($pval['promo_mode']=='$')
								{
									$i++;
									$offer_applied_on_this_cart=$pval['promo_desc'];
									$offer_amt = ($offer_amt+$pval['promo_value']);
									$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);
								}
								
								if(($pval['promo_mode']=='%') )
								{	$i++;
									$offer_applied_on_this_cart=$pval['promo_desc'];
									$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);										
									$cal_sub_totla = ($cal_sub_totla - $promo_val_per);									
									$offer_amt = ($offer_amt+$promo_val_per);
								}
								
								if(($pval['promo_mode']=='0') )
								{
									$i++;
									$offer_applied_on_this_cart=$pval['promo_desc'];
								}
							}
							$k++;
						}
						
					}
					else{			
						$offer_applied_on_this_cart='';
						$current_promo = array();
					}
						
				
				
					/** PROMOTIONS CODE END  **/
					
					
					/******************  FOOD ITEM  */
					/*$food_disocunt
					
					$food_current_promo*/
					
					$food_offer_amt = '';
					$offer_applied_on_food= '';
					
					
					if(!empty($food_current_promo) && (count($food_current_promo)>0))
					{
									
						$k=0;$i=0;
						foreach($food_current_promo as $pval)
						{
							
							$item_price = $food_cart_amount[$k];					
							$item_qty = $food_cart_qty[$k];
							$cal_sub_total=number_format(($item_price*$item_qty),2);
						
								if($pval['promo_mode']=='$')
								{
									$i++;
									$offer_applied_on_food=$pval['promo_desc'];
									$food_offer_amt = ($offer_amt+$pval['promo_value']);
								}
								
								if(($pval['promo_mode']=='%') )
								{	$i++;
									$offer_applied_on_food=$pval['promo_desc'];
									$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);						
									$food_offer_amt = ($offer_amt+$promo_val_per);
								}
								
								if(($pval['promo_mode']=='0') )
								{
									$i++;
									$offer_applied_on_food=$pval['promo_desc'];
								}
							$k++;	
							
						}
						
					
					}
					else
					{
						$food_current_promo= array();
					}
					
					/*******************  END  ****/
					
					
					if(((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt) < (float)$delivery_fee)
					{
						$total_amt = number_format(($delivery_fee+$serv_tax),2);
					}
					else
					{
					
						$aa_total = ((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt)+(float)$serv_tax;
						$total_amt = number_format(($aa_total),2);

					}

					
					/*print_r($current_promo);			
					print_r(json_encode($json_data_session));
					exit;*/
					
					DB::table('rest_cart')	
						->where('user_id', '=' ,trim($user_id))
						->where('guest_id', '=' ,trim($guest_id))
						->where('rest_id', '=' ,trim($rest_id))		
						->update(['cart_detail' => (json_encode($json_data_session)),
						  'offer_detail'=> (json_encode($current_promo)),
						  'offer_amt'=> trim($offer_amt),  
						  
						  'offer_applied'=>$offer_applied_on_this_cart,
						  'food_promo_list'=>json_encode($food_current_promo),
						  'food_offer_applied'=>json_encode($offer_applied_on_food),
						  
						  'service_tax_amt'=>$serv_tax,
						  'delivery_amt'=> trim($delivery_fee),
						  'subtotal'=> trim($sub_total_cart),
						  'total_amt'=> trim($total_amt)
						 ]);
						Session::set('order_guest_id',trim($guest_id));
						Session::set('order_user_id',trim($user_id));
						Session::set('order_rest_id',trim($rest_id));
						 
					
					/*
							$response['message'] = "All deatil of cart data";
							$response['cart_detail'] = $cart_detail;
							$response['other_detail'] = array(
							'subtotal'=> number_format($sub_total_cart,2),
							'rest_service' =>$service_charge,
							'delivery_fee'=>number_format($delivery_fee,2),
							'service_tax'=>number_format($serv_tax,2),
							'promo_list'=>$current_promo,
							'discount_amt'=>number_format($offer_amt,2),
							'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,
							'food_promo_list'=>$food_current_promo,
							'offer_applied_on_item'=>$offer_applied_on_food,
							'total_amt'=>number_format($total_amt,2));
							$response['ok'] = 1;	*/
						
						
						
			}

		 if(!Auth::guest()) 
	      {
	        $user_ids = Auth::user()->id;
	        $userkey = 'user_id';
	      }
	      else
	      {
	        $user_ids = Session::get('_token');
	        $userkey = 'guest_id';
	      } 	

		  $checkatcd = DB::table('rest_cart')           
		  ->select('*')               
		  ->where($userkey, '=' ,trim($user_ids))         
		  ->where('rest_id', '=' ,trim($rest_id))          
		  ->get();

		  $cart_count = count($checkatcd);	
		
		  $cart_view =  view('cart', array('cart' => $json_data_session))->render();
		 
		/*  return response()->json( array('order_offer_on'=>'','discount_amt'=>'','discount_txt'=>'','service_tax' =>'', 'total_amt' =>'','promo_list' =>'','food_promo_list_cal' =>'','food_promo_applied_cal' =>'','cart_view'=>$cart_view) );*/
		  	$tax_for_service=((trim($sub_total_cart)*$service_charge)/100);
		       return response()->json( array('order_offer_on'=>$promo_id,
			   	'discount_amt'=>$offer_amt,
				'discount_txt'=>$offer_applied_on_this_cart,
				'promo_list' => json_encode($current_promo),
				'food_promo_list_cal' =>json_encode($food_current_promo),
				'food_promo_applied_cal' =>$offer_applied_on_food,
				'service_tax' => number_format($tax_for_service,2),
				'total_amt' => $sub_total_cart,
				'cart_view'=>$cart_view,
				'cart_status'=>'2',
				'cart_count'=>$cart_count
				) );								 				  
		  
		  /*****************************/	  
		
	}
	
	public function show_cart_detail_onload()
	{
		
		
		Cart::destroy();
		 $rest_id =Input::get('rest_id');

		 $food_cart_amount = array();
		 $food_cart_qty = array();
		 $group_name = array();
		 $group_list = array();
		
		
		$user_id = '0';
		$guest_id = '';
		
		
		if(!Auth::guest()) 
		{
			$user_id = Auth::user()->id;
		}
		else
		{
			$guest_id = Session::get('_token');
		}
		//$cart = 'test_data';
		
		
		  
		  
		/******************************/
		  
		  
		DB::enableQueryLog(); 				
		
		
		
				$offer_amt=0;
				$offer_applied_on_this_cart='';
				$current_promo=array();
				$service_tax=0;
				$sub_total_price=0;
				$promo_id=0;
				$offer_applied_on_food='';
				$food_current_promo = array();
				$json_data_session=array();
				$sub_total_cart=0;
		
		$cart_food_id='';
		
		$cart_detail ='';
		$cart_listing = DB::table('rest_cart');
		$cart_listing = $cart_listing->select('*');
		$cart_listing = $cart_listing->where('user_id', '=' ,trim($user_id));
		$cart_listing = $cart_listing->where('guest_id', '=' ,trim($guest_id));
		$cart_listing = $cart_listing->where('rest_id', '=' ,trim($rest_id));
		$cart_listing = $cart_listing->orderBy('cart_id', 'asc');
		$cart_listing = $cart_listing->get();
		$service_charge= "";
			if($cart_listing)
			{
					
				$delivery_fee = 0;	
				
				$rest_detail = DB::table('restaurant')		
						->where('rest_id', '=' ,$rest_id)
						->where('rest_status', '!=' , 'INACTIVE')
						->orderBy('rest_id', 'asc')				
						->groupBy('rest_id')
						->get(); 
						
				$promo_list = DB::table('promotion')		
								->where('promo_restid', '=' ,$rest_id)
								->where('promo_status', '=' ,'1')
								->orderBy('promo_buy', 'desc')					
								->groupBy('promo_id')
								->get();
									
						
				$service_charge = $rest_detail[0]->rest_servicetax;
				
				if($cart_listing[0]->order_type=='delivery')
				{
					$delivery_fee = $rest_detail[0]->rest_mindelivery;			
				}
			

					$sub_total_cart = 0;
					$json_data_session = array();
					$addon_list =array();
					
					$food_current_promo = array();
			
					foreach($cart_listing as $clist)
					{	
					
					
						/********FOOD DETAILS /******/
						
						
						$rest_id  = $clist->rest_id;
						$menu_id = $clist->menu_id;
						$menu_cate_id =$clist->menu_catid;
						$menu_subcate_id = $clist->menu_subcatid;
						$addon_id = $clist->menu_addonid;
						$foodsize_price =0;
						$food_addonPrice = 0; 
						$total_price =0 ;
						
						$item_name_cc = '';
						$item_price_cc = '';
						
						
						$offer_on_menu_item = '';
						
						
						
						/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/
						
						if(($user_id>0) && ($user_id!=''))
						{
							foreach($promo_list as $plist )
							{
							
								if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))
								{
									$current_promo1 = array();
									
									array_push($food_cart_amount, $total_price);
									array_push($food_cart_qty, $clist->qty);
									
									if($plist->promo_on=='schedul')
									{
											 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
									
											$db_day = '';
											
											if(!empty($plist->promo_day))
											{
												$db_day = explode(',',$plist->promo_day);
											}
											
										if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
										{
											
											if(
												($plist->promo_start<=date('Y-m-d')) && 
												($plist->promo_end>=date('Y-m-d')) && 
												(empty($plist->promo_day)) && 
												($plist->promo_buy<=$clist->qty)
												)
											{
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);
												$offer_on_menu_item = $plist->promo_desc;						
											}
											
											
											if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
											 && (!empty($plist->promo_day))
											 && (in_array($day_name,$db_day))
											 
											 )
											{
											
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);	
												$offer_on_menu_item = $plist->promo_desc;					
											}
											
											
										}
										elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))
										{
										
										
										
											$db_day = explode(',',$plist->promo_day);
											
											if(in_array($day_name,$db_day))
											{
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);
												$offer_on_menu_item = $plist->promo_desc;
											}
										
										}
										
										
										
									}
									elseif($plist->promo_on=='qty')
									{
									
										if(($plist->promo_buy<=$clist->qty))
										{		
											$current_promo1['promo_id'] = $plist->promo_id;
											$current_promo1['promo_restid'] = $plist->promo_restid;
											$current_promo1['promo_for'] = $plist->promo_for;
											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
											$current_promo1['promo_on'] = $plist->promo_on;
											$current_promo1['promo_desc'] = $plist->promo_desc;
											$current_promo1['promo_mode'] = $plist->promo_mode;
											$current_promo1['promo_buy'] = $plist->promo_buy;
											$current_promo1['promo_value'] = $plist->promo_value;
											$current_promo1['promo_end'] = $plist->promo_end;
											$current_promo1['promo_start'] = $plist->promo_start;	
											array_push($food_current_promo, $current_promo1);
											$offer_on_menu_item = $plist->promo_desc;
										}
									}
								}
							}
						}
						/*************************** END ******************************/
							
						
						$menu_list = DB::table('menu')		
							->where('restaurant_id', '=' ,$clist->rest_id)		
							->where('menu_id', '=' ,$clist->menu_id)
							->where('menu_status', '=' ,'1')
							->orderBy('menu_order', 'asc')
							->get();
							
				// dd(DB::getQueryLog()); 				
			
				$menu_array = array();
				$food_array=array();
							
				if(!empty($menu_list))
				{
					
					$menu_id = $menu_list[0]->menu_id;
					$menu_name = $menu_list[0]->menu_name;
					$menu_item_count = 1;
					$food_detail='';
					
					
					$menu_cat_detail = DB::table('menu_category')		
							->where('menu_category.rest_id', '=' ,$rest_id)
							->where('menu_category.menu_id', '=' ,$menu_id)
							->where('menu_category.menu_category_id', '=' ,$menu_cate_id)
							->where('menu_category.menu_cat_status', '=' ,'1')
							->select('menu_category.*')
							->orderBy('menu_category.menu_category_id', 'asc')
							->get();
								
					
					
					
					$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;
					$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;
					$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;
					$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;
					$food_detail['food_name_ar'] = $item_name_cc_ar = $menu_cat_detail[0]->menu_category_name_ar;
					$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;					
					$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;
					$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;
					
					
					
					
					
					$food_detail['food_subitem_title']='';
												
					if($menu_cat_detail[0]->menu_category_portion=='yes')
					{	
						$food_detail['food_subitem_title'] = 'Choose a size';
						
						
						$food_size = DB::table('category_item')		
									->where('rest_id', '=' ,$rest_id)
									->where('menu_id', '=' ,$menu_id)
									->where('menu_category', '=' ,$menu_cate_id)
									->where('menu_cat_itm_id', '=' ,$menu_subcate_id)
									->where('menu_item_status', '=' ,'1')						
									->select('*')
									->orderBy('menu_cat_itm_id', 'asc')						
									->groupBy('menu_item_title')
									->get();
									
								
						$food_detail['food_size_detail']=$food_size;
						 $item_price_cc =$food_size[0]->menu_item_price;
						
						$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);						
						$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);
					
					}
					else
					{	
						$food_detail['food_size_detail']=array();	
						$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);			
					}
					$addon_list=array();
					if(!empty($addon_id)){		
					
					
						$addons = DB::table('menu_category_addon')		
								->where('addon_restid', '=' ,$rest_id)
								->where('addon_menuid', '=' ,$menu_id)
								->where('addon_status', '=' ,'1')						
								->where('addon_menucatid', '=' ,$menu_cate_id)
								->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
								->select('*')
								->orderBy('addon_id', 'asc')						
								->groupBy('addon_groupname')
								->get();
								
								$lang = Session::get('locale');

								if(!empty($addons))
								{
										
									foreach($addons as $ad_list)
									{
										$option_type = 0;
										if(Session::get('locale')=='ar'){
											if(substr($ad_list->addon_groupname_ar, -1)=='*') 
											{
												 $option_type = 1;
											}
									    }else{
										    if(substr($ad_list->addon_groupname, -1)=='*') 
											{
												 $option_type = 1;
											}	
									    }			

									    array_push($group_name, $ad_list->addon_groupname);	
									   
										$ff['addon_gropname'] = $ad_list->addon_groupname;
										$ff['addon_type'] = $ad_list->addon_option;
										$ff['addon_mandatory_or_not'] = $option_type;
										
										$addon_group = DB::table('menu_category_addon')		
											->where('addon_restid', '=' ,$rest_id)
											->where('addon_menuid', '=' ,$menu_id)
											->where('addon_status', '=' ,'1')	
											->where('addon_groupname', '=' ,$ad_list->addon_groupname)						
											->where('addon_menucatid', '=' ,$menu_cate_id)
											->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
											->select('*')
											->orderBy('addon_id', 'asc')	
											->get();
										array_push($group_list, $addon_group);
										$addon_group_list =array();
										foreach($addon_group as $group_list_loop)
										{
											
											array_push($addon_group_list, array('addon_id'=>$group_list_loop->addon_id,
											'addon_menucatid'=>$group_list_loop->addon_menucatid,
											'addon_groupname'=>$group_list_loop->addon_groupname,
											'addon_option'=>$group_list_loop->addon_option,
											'addon_name'=>$group_list_loop->addon_name,
											'addon_name_ar'=>$group_list_loop->addon_name_ar,
											'addon_price'=>$group_list_loop->addon_price,
											'addon_status'=>$group_list_loop->addon_status
											));
											
											
											array_push($addon_list, array('name'=>$group_list_loop->addon_groupname.': '.$group_list_loop->addon_name,
											'name_ar'=>$group_list_loop->addon_groupname_ar.': '.$group_list_loop->addon_name_ar,
											'price'=>$group_list_loop->addon_price,
											'id'=>$group_list_loop->addon_id));
											
											
											$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);					
											$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';
										
										}
										$ff['addon_detail'] = $addon_group_list;
										
										$food_detail['food_addon'] = array();
										array_push($food_detail['food_addon'], $ff);	
										
										
										
																			
									}
									
								}	
					}
					else
					{
						$food_detail['food_addon'] = array();
					}								
					
					array_push($food_array, array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item));
				}
						
						/******** FOOD DETAIL END ****/
					
					$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));
					
						Cart::add(array(
							'id'=>$clist->menu_catid, 
							'name' => $item_name_cc,
							'name_ar' => $item_name_cc_ar,
							'qty' => $clist->qty,
							'price' => number_format(($item_price_cc*$clist->qty),2),
							'options'=>['menu_catid' => $clist->menu_catid, 
										'instraction' => $clist->special_instruction,
										'rest_id' => $clist->rest_id, 		
										'menu_id' => $clist->menu_id,
										'option_name'=>$clist->menu_subcatid,
										'addon_data'=>$addon_list,
										'offer_on_item'=>$offer_on_menu_item
										]
							)
					);
					
								
						$json_data_session[$clist->cart_id] =	array(
																"rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,	   
																"name"=>$item_name_cc,"name_ar"=>$item_name_cc_ar,"qty"=>$clist->qty,
																"price"=>$item_price_cc,
																"instruction"=>$clist->special_instruction,			    
																"options"=> array ( 
																		"menu_catid"=>$clist->menu_catid,
																		"rest_id"=>$clist->rest_id,
																		"menu_id"=>$clist->menu_id,
																		"option_name"=> $clist->menu_subcatid,
																		"addon_data"=>$addon_list,
																		"offer_name"=>$offer_on_menu_item),
																		"tax"=>'',
																		"subtotal"=>$item_price_cc
																	);	

					}
			
				
				
				
				
				
				
				/*print_r($food_current_promo);
				print_r($food_amount);*/
			
				
			
			
					/*$offer_sub_total = trim($sub_total_cart)-trim($offer_amt);
					$serv_tax  = (($offer_sub_total*$service_charge)/100);*/
					
					$serv_tax  = (($sub_total_cart*$service_charge)/100);
						
					/*echo  	"sub".$sub_total_cart; 
					echo "service".$serv_tax;*/
							/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/
					$current_promo = array();
					$offer_applied_on_this_cart='';
			
			
			

			
				
				if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))
				{
					 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));				
					
					/*$promo_list = DB::table('promotion')		
								->where('promo_restid', '=' ,$rest_id)
								->where('promo_status', '=' ,'1')
								->where('promo_for', '=' ,'order')	
								->orderBy('promo_buy', 'desc')					
								->groupBy('promo_id')
								->get();*/ 	 
						
						
						// dd(DB::getQueryLog()); 	
						//print_r($promo_list);
								//	
					//	exit;
				
					$current_promo = array();
					$array_amt = array();
					
					foreach($promo_list as $plist )
					{
					
						if($plist->promo_for=='order')
						{
							$current_promo1 = array();
							
							if($plist->promo_on=='schedul')
							{
									 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
							
									$db_day = '';
									
									if(!empty($plist->promo_day))
									{
										$db_day = explode(',',$plist->promo_day);
									}
									
								if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
								{
									
									if(
										($plist->promo_start<=date('Y-m-d')) && 
										($plist->promo_end>=date('Y-m-d')) && 
										(empty($plist->promo_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))
										)
									{
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);					
									}
									
									
									if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
									 && (!empty($plist->promo_day))
									 && (in_array($day_name,$db_day) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
									 
									 )
									{
									
									
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);					
									}
									
									
								}
								elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
								{
								
								
								
									$db_day = explode(',',$plist->promo_day);
									
									if(in_array($day_name,$db_day))
									{
									
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);
									}
								
								}
								
								
								
							}
							else
							{
							
								if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))
								{		
									
									array_push($array_amt, $plist->promo_buy);
									$current_promo1['promo_id'] = $plist->promo_id;
									$current_promo1['promo_restid'] = $plist->promo_restid;
									$current_promo1['promo_for'] = $plist->promo_for;
									$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
									$current_promo1['promo_on'] = $plist->promo_on;
									$current_promo1['promo_desc'] = $plist->promo_desc;
									$current_promo1['promo_mode'] = $plist->promo_mode;
									$current_promo1['promo_buy'] = $plist->promo_buy;
									$current_promo1['promo_value'] = $plist->promo_value;
									$current_promo1['promo_end'] = $plist->promo_end;
									$current_promo1['promo_start'] = $plist->promo_start;

									array_push($current_promo, $current_promo1);
								}
							}
						}
					}		
				}
				else
				{
					$current_promo = array();
					$offer_applied_on_this_cart='';
				}
			
			
			
					$offer_amt = 0;
			 		$cal_sub_totla = $sub_total_cart;
			 	
			 
				if((count($current_promo)>0) && (!empty($array_amt))) {	
				
				  	$maxs = max($array_amt);
				  	$key_name = array_search ($maxs, $array_amt);			  
					$k=0;$i=0;
					foreach($current_promo as $pval)
					{
						if($key_name==$k)
						{
					
							if($pval['promo_mode']=='$')
							{
								$i++;
								$offer_applied_on_this_cart=$pval['promo_desc'];
								$offer_amt = ($offer_amt+$pval['promo_value']);
								$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);
							}
							
							if(($pval['promo_mode']=='%') )
							{	$i++;
								$offer_applied_on_this_cart=$pval['promo_desc'];
								$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);										
								$cal_sub_totla = ($cal_sub_totla - $promo_val_per);									
								$offer_amt = ($offer_amt+$promo_val_per);
							}
							
							if(($pval['promo_mode']=='0') )
							{
								$i++;
								$offer_applied_on_this_cart=$pval['promo_desc'];
							}
						}
						$k++;
					}
					
				}
				else{			
					$offer_applied_on_this_cart='';
					$current_promo = array();
				}
			
			
			
			
			
			
			
			
			
					/** PROMOTIONS CODE END  **/
					
					
					/******************  FOOD ITEM  */
					/*$food_disocunt
					
					$food_current_promo*/
					
					$food_offer_amt = '';
					$offer_applied_on_food= '';
			
			
				if(!empty($food_current_promo) && (count($food_current_promo)>0))
				{
								
					$k=0;$i=0;
					foreach($food_current_promo as $pval)
					{
						
						$item_price = $food_cart_amount[$k];					
						$item_qty = $food_cart_qty[$k];
						$cal_sub_total=number_format(($item_price*$item_qty),2);
					
							if($pval['promo_mode']=='$')
							{
								$i++;
								$offer_applied_on_food=$pval['promo_desc'];
								$food_offer_amt = ($offer_amt+$pval['promo_value']);
							}
							
							if(($pval['promo_mode']=='%') )
							{	$i++;
								$offer_applied_on_food=$pval['promo_desc'];
								$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);						
								$food_offer_amt = ($offer_amt+$promo_val_per);
							}
							
							if(($pval['promo_mode']=='0') )
							{
								$i++;
								$offer_applied_on_food=$pval['promo_desc'];
							}
						$k++;	
						
					}
					
				
				}
				else
				{
					$food_current_promo= array();
				}
			
				/*******************  END  ****/
				
				
				if(((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt) < (float)$delivery_fee)
				{
					$total_amt = number_format(($delivery_fee+$serv_tax),2);
				}
				else
				{
				
					$aa_total = ((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt)+(float)$serv_tax;
					$total_amt = number_format(($aa_total),2);
				}
			
			
			
			
				/*print_r($current_promo);			
				print_r(json_encode($json_data_session));
				exit;*/
			
				DB::table('rest_cart')	
					->where('user_id', '=' ,trim($user_id))
					->where('guest_id', '=' ,trim($guest_id))
					->where('rest_id', '=' ,trim($rest_id))		
					->update(['cart_detail' => (json_encode($json_data_session)),
					  'offer_detail'=> (json_encode($current_promo)),
					  'offer_amt'=> trim($offer_amt),  
					  
					  'offer_applied'=>$offer_applied_on_this_cart,
					  'food_promo_list'=>json_encode($food_current_promo),
					  'food_offer_applied'=>json_encode($offer_applied_on_food),
					  
					  'service_tax_amt'=>trim($serv_tax),
					  'delivery_amt'=> trim($delivery_fee),
					  'subtotal'=> trim($sub_total_cart),
					  'total_amt'=> trim($total_amt)
					 ]);
					 
				
					
					$tax_for_service=((trim($sub_total_cart)*$service_charge)/100);

						$response['message'] = "All deatil of cart data";
						$response['cart_detail'] = $cart_detail;
						$response['other_detail'] = array(
						'subtotal'=> number_format($sub_total_cart,2),
						'rest_service' =>$service_charge,
						'delivery_fee'=>number_format($delivery_fee,2),
						'service_tax'=> $tax_for_service,
						'promo_list'=>$current_promo,
						'discount_amt'=>number_format($offer_amt,2),
						'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,
						'food_promo_list'=>$food_current_promo,
						'offer_applied_on_item'=>$offer_applied_on_food,
						'total_amt'=>number_format($total_amt,2));
						$response['ok'] = 1;	
						
						
						
			}
			
			
		
				$cart_view =  view('cart', array('cart' => $json_data_session))->render();
				 
				/*  return response()->json( array('order_offer_on'=>'','discount_amt'=>'','discount_txt'=>'','service_tax' =>'', 'total_amt' =>'','promo_list' =>'','food_promo_list_cal' =>'','food_promo_applied_cal' =>'','cart_view'=>$cart_view) );*/

			  	$tax_for_service= ((trim($sub_total_cart)* (double) $service_charge)/100);
			       
		       return response()->json( array('order_offer_on'=>$promo_id,
			   	'discount_amt'=>$offer_amt,
				'discount_txt'=>$offer_applied_on_this_cart,
				'promo_list' => json_encode($current_promo),
				'food_promo_list_cal' =>json_encode($food_current_promo),
				'food_promo_applied_cal' =>$offer_applied_on_food,
				'service_tax' =>  number_format(($tax_for_service),2), 
				'total_amt' => $sub_total_cart,
				'cart_view'=>$cart_view,
				'cart_status'=>'2') );
				
				
			   
			   
		  
		  
		  
		  /*****************************/
		  
		
	
		  
		
	
	}
	
	
	/*****************  AFTER LOGIN CHECK ISSUE *************************/
	
	
	public function manage_cart_login()
	{
		
		Cart::destroy();
		 $rest_id =Input::get('rest_id');
		
		
		$user_id = '0';
		$guest_id = '';

		$food_cart_amount = array();
		$food_cart_qty = array();
		$group_name = array();
		$group_list = array();
		
		
		if(!Auth::guest()) 
		{
			$user_id = Auth::user()->id;
		}
		else
		{
			$guest_id = Session::get('_token');
		}
		//$cart = 'test_data';
		
		
		  
		  
		  
		  
		  /******************************/
		  
		  
		DB::enableQueryLog(); 				
		
				$offer_amt=0;
				$offer_applied_on_this_cart='';
				$current_promo=array();
				$service_tax=0;
				$sub_total_price=0;
				$promo_id=0;
				$offer_applied_on_food='';
				$food_current_promo = array();
				$json_data_session=array();
				$sub_total_cart=0;
		
		$cart_food_id='';
		
		$cart_detail ='';
		$cart_listing = DB::table('rest_cart');
		$cart_listing = $cart_listing->select('*');
		$cart_listing = $cart_listing->where('user_id', '=' ,trim($user_id));
		$cart_listing = $cart_listing->where('guest_id', '=' ,trim($guest_id));
		$cart_listing = $cart_listing->where('rest_id', '=' ,trim($rest_id));
		$cart_listing = $cart_listing->orderBy('cart_id', 'asc');
		$cart_listing = $cart_listing->get();
		
			if($cart_listing)
			{
					
				$delivery_fee = 0;	
				
				$rest_detail = DB::table('restaurant')		
						->where('rest_id', '=' ,$rest_id)
						->where('rest_status', '!=' , 'INACTIVE')
						->orderBy('rest_id', 'asc')				
						->groupBy('rest_id')
						->get(); 
						
				$promo_list = DB::table('promotion')		
								->where('promo_restid', '=' ,$rest_id)
								->where('promo_status', '=' ,'1')
								->orderBy('promo_buy', 'desc')					
								->groupBy('promo_id')
								->get();
									
						
				$service_charge = $rest_detail[0]->rest_servicetax;
				
				
				if($cart_listing[0]->order_type=='delivery')
				{
					$delivery_fee = $rest_detail[0]->rest_mindelivery;			
				}
			
	
			
			
			
			$sub_total_cart = 0;
			$json_data_session = array();
			$addon_list = array();
			
			$food_current_promo = array();
			
					foreach($cart_listing as $clist)
					{	
					
					
						/********FOOD DETAILS /******/
						
						
						$rest_id  = $clist->rest_id;
						$menu_id = $clist->menu_id;
						$menu_cate_id =$clist->menu_catid;
						$menu_subcate_id = $clist->menu_subcatid;
						$addon_id = $clist->menu_addonid;
						$foodsize_price =0;
						$food_addonPrice = 0; 
						$total_price =0 ;
						
						$item_name_cc = '';
						$item_name_cc_ar = '';
						$item_price_cc = '';
						
						
						$offer_on_menu_item = '';
						
						
						
						/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/
						
						if(($user_id>0) && ($user_id!=''))
						{
							foreach($promo_list as $plist )
							{
							
								if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))
								{
									$current_promo1 = array();
									
									array_push($food_cart_amount, $total_price);
									array_push($food_cart_qty, $clist->qty);
									
									if($plist->promo_on=='schedul')
									{
											 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
									
											$db_day = '';
											
											if(!empty($plist->promo_day))
											{
												$db_day = explode(',',$plist->promo_day);
											}
											
										if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
										{
											
											if(
												($plist->promo_start<=date('Y-m-d')) && 
												($plist->promo_end>=date('Y-m-d')) && 
												(empty($plist->promo_day)) && 
												($plist->promo_buy<=$clist->qty)
												)
											{
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);
												$offer_on_menu_item = $plist->promo_desc;						
											}
											
											
											if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
											 && (!empty($plist->promo_day))
											 && (in_array($day_name,$db_day))
											 
											 )
											{
											
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);	
												$offer_on_menu_item = $plist->promo_desc;					
											}
											
											
										}
										elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))
										{
										
										
										
											$db_day = explode(',',$plist->promo_day);
											
											if(in_array($day_name,$db_day))
											{
												
												array_push($array_amt, $plist->promo_buy);
												$current_promo1['promo_id'] = $plist->promo_id;
												$current_promo1['promo_restid'] = $plist->promo_restid;
												$current_promo1['promo_for'] = $plist->promo_for;
												$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
												$current_promo1['promo_on'] = $plist->promo_on;
												$current_promo1['promo_desc'] = $plist->promo_desc;
												$current_promo1['promo_mode'] = $plist->promo_mode;
												$current_promo1['promo_buy'] = $plist->promo_buy;
												$current_promo1['promo_value'] = $plist->promo_value;
												$current_promo1['promo_end'] = $plist->promo_end;
												$current_promo1['promo_start'] = $plist->promo_start;
												array_push($food_current_promo, $current_promo1);
												$offer_on_menu_item = $plist->promo_desc;
											}
										
										}
										
										
										
									}
									elseif($plist->promo_on=='qty')
									{
									
										if(($plist->promo_buy<=$clist->qty))
										{		
											$current_promo1['promo_id'] = $plist->promo_id;
											$current_promo1['promo_restid'] = $plist->promo_restid;
											$current_promo1['promo_for'] = $plist->promo_for;
											$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
											$current_promo1['promo_on'] = $plist->promo_on;
											$current_promo1['promo_desc'] = $plist->promo_desc;
											$current_promo1['promo_mode'] = $plist->promo_mode;
											$current_promo1['promo_buy'] = $plist->promo_buy;
											$current_promo1['promo_value'] = $plist->promo_value;
											$current_promo1['promo_end'] = $plist->promo_end;
											$current_promo1['promo_start'] = $plist->promo_start;	
											array_push($food_current_promo, $current_promo1);
											$offer_on_menu_item = $plist->promo_desc;
										}
									}
								}
							}
						}
						/*************************** END ******************************/
							
							
							
						
						
						$menu_list = DB::table('menu')		
							->where('restaurant_id', '=' ,$clist->rest_id)		
							->where('menu_id', '=' ,$clist->menu_id)
							->where('menu_status', '=' ,'1')
							->orderBy('menu_order', 'asc')
							->get();
							
				// dd(DB::getQueryLog()); 				
			
				$menu_array = array();
				$food_array= array();
							
				if(!empty($menu_list))
				{
					
					$menu_id = $menu_list[0]->menu_id;
					$menu_name = $menu_list[0]->menu_name;
					$menu_item_count = 1;
					$food_detail='';
					
					
					$menu_cat_detail = DB::table('menu_category')		
							->where('menu_category.rest_id', '=' ,$rest_id)
							->where('menu_category.menu_id', '=' ,$menu_id)
							->where('menu_category.menu_category_id', '=' ,$menu_cate_id)
							->where('menu_category.menu_cat_status', '=' ,'1')
							->select('menu_category.*')
							->orderBy('menu_category.menu_category_id', 'asc')
							->get();
								
					
					
					
					$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;
					$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;
					$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;
					$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;
					$food_detail['food_name_ar'] = $item_name_cc_ar = $menu_cat_detail[0]->menu_category_name_ar;
					$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;					
					$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;
					$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;
					
					
					
					
					
					$food_detail['food_subitem_title']='';
												
					if($menu_cat_detail[0]->menu_category_portion=='yes')
					{	
						$food_detail['food_subitem_title'] = 'Choose a size';
						
						
						$food_size = DB::table('category_item')		
									->where('rest_id', '=' ,$rest_id)
									->where('menu_id', '=' ,$menu_id)
									->where('menu_category', '=' ,$menu_cate_id)
									->where('menu_cat_itm_id', '=' ,$menu_subcate_id)
									->where('menu_item_status', '=' ,'1')						
									->select('*')
									->orderBy('menu_cat_itm_id', 'asc')						
									->groupBy('menu_item_title')
									->get();
									
								
						$food_detail['food_size_detail']=$food_size;
						 $item_price_cc =$food_size[0]->menu_item_price;
						
						$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);						
						$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);
					
					}
					else
					{	
						$food_detail['food_size_detail']=array();	
						$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);			
					}
					$addon_list=array();
					if(!empty($addon_id)){		
					
					
						$addons = DB::table('menu_category_addon')		
								->where('addon_restid', '=' ,$rest_id)
								->where('addon_menuid', '=' ,$menu_id)
								->where('addon_status', '=' ,'1')						
								->where('addon_menucatid', '=' ,$menu_cate_id)
								->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
								->select('*')
								->orderBy('addon_id', 'asc')						
								->groupBy('addon_groupname')
								->get();
								
							
								if(!empty($addons))
								{
										
									foreach($addons as $ad_list)
									{
									
										$option_type = 0;
										if(substr($ad_list->addon_groupname, -1)=='*') 
										{
											 $option_type = 1;
										}

										array_push($group_name, $ad_list->addon_groupname);	

										$ff['addon_gropname'] = $ad_list->addon_groupname;
										$ff['addon_type'] = $ad_list->addon_option;
										$ff['addon_mandatory_or_not'] = $option_type;
										
										$addon_group = DB::table('menu_category_addon')		
											->where('addon_restid', '=' ,$rest_id)
											->where('addon_menuid', '=' ,$menu_id)
											->where('addon_status', '=' ,'1')	
											->where('addon_groupname', '=' ,$ad_list->addon_groupname)						
											->where('addon_menucatid', '=' ,$menu_cate_id)
											->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')
											->select('*')
											->orderBy('addon_id', 'asc')	
											->get();
										array_push($group_list, $addon_group);
										$addon_group_list =array();
										foreach($addon_group as $group_list_loop)
										{
										
											array_push($addon_group_list, array('addon_id'=>$group_list_loop->addon_id,
											'addon_menucatid'=>$group_list_loop->addon_menucatid,
											'addon_groupname'=>$group_list_loop->addon_groupname,
											'addon_option'=>$group_list_loop->addon_option,
											'addon_name'=>$group_list_loop->addon_name,
											'addon_price'=>$group_list_loop->addon_price,
											'addon_status'=>$group_list_loop->addon_status
											));
											
											
											array_push($addon_list, array('name'=>$group_list_loop->addon_groupname.': '.$group_list_loop->addon_name,
											'price'=>$group_list_loop->addon_price,
											'id'=>$group_list_loop->addon_id));
											
											
											$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);					
											$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';
										
										}
										$ff['addon_detail'] = $addon_group_list;
										
										$food_detail['food_addon'] = array();
										array_push($food_detail['food_addon'], $ff);	
										
										
										
																			
									}
									
								}	
					}
					else
					{
						$food_detail['food_addon'] = array();
					}								
					
					array_push($food_array, array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item));
				}
						
						/******** FOOD DETAIL END ****/
					
					$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));
										
						Cart::add(array(
							'id'=>$clist->menu_catid, 
							'name' => $item_name_cc,
							'name_ar' => $item_name_cc_ar,
							'qty' => $clist->qty,
							'price' => $item_price_cc,
							'options'=>['menu_catid' => $clist->menu_catid, 
										'instraction' => $clist->special_instruction,
										'rest_id' => $clist->rest_id, 		
										'menu_id' => $clist->menu_id,
										'option_name'=>$clist->menu_subcatid,
										'addon_data'=>$addon_list,
										'offer_on_item'=>$offer_on_menu_item
										]
							)
					);
					
								
						$json_data_session[$clist->cart_id] =	array("rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,	   "name"=>$item_name_cc,"qty"=>$clist->qty,"price"=>$item_price_cc,"instruction"=>$clist->special_instruction,			    "options"=> array ( "menu_catid"=>$clist->menu_catid,
									"rest_id"=>$clist->rest_id,
									"menu_id"=>$clist->menu_id,
									"option_name"=> $clist->menu_subcatid,
									"addon_data"=>$addon_list,
									"offer_name"=>$offer_on_menu_item),
							 "tax"=>'',
           					 "subtotal"=>$item_price_cc
						);	
						
					}
			
				
				
				
				
				
				
				/*print_r($food_current_promo);
				print_r($food_amount);*/
			
				
			
			
			
			$serv_tax  = (($sub_total_cart*$service_charge)/100);
				
				
					/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/
			$current_promo = array();
			$offer_applied_on_this_cart='';
			
			
			

			
				
				if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))
				{
					 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));				
					
					/*$promo_list = DB::table('promotion')		
								->where('promo_restid', '=' ,$rest_id)
								->where('promo_status', '=' ,'1')
								->where('promo_for', '=' ,'order')	
								->orderBy('promo_buy', 'desc')					
								->groupBy('promo_id')
								->get();*/ 	 
						
						
						// dd(DB::getQueryLog()); 	
						//print_r($promo_list);
								//	
					//	exit;
				
					$current_promo = array();
					$array_amt = array();
					
					foreach($promo_list as $plist )
					{
					
						if($plist->promo_for=='order')
						{
							$current_promo1 = array();
							
							if($plist->promo_on=='schedul')
							{
									 $day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));						
							
									$db_day = '';
									
									if(!empty($plist->promo_day))
									{
										$db_day = explode(',',$plist->promo_day);
									}
									
								if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))
								{
									
									if(
										($plist->promo_start<=date('Y-m-d')) && 
										($plist->promo_end>=date('Y-m-d')) && 
										(empty($plist->promo_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))
										)
									{
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);					
									}
									
									
									if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))
									 && (!empty($plist->promo_day))
									 && (in_array($day_name,$db_day) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
									 
									 )
									{
									
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);					
									}
									
									
								}
								elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) && 
										((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))
								{
								
								
								
									$db_day = explode(',',$plist->promo_day);
									
									if(in_array($day_name,$db_day))
									{
										
										array_push($array_amt, $plist->promo_buy);
										$current_promo1['promo_id'] = $plist->promo_id;
										$current_promo1['promo_restid'] = $plist->promo_restid;
										$current_promo1['promo_for'] = $plist->promo_for;
										$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
										$current_promo1['promo_on'] = $plist->promo_on;
										$current_promo1['promo_desc'] = $plist->promo_desc;
										$current_promo1['promo_mode'] = $plist->promo_mode;
										$current_promo1['promo_buy'] = $plist->promo_buy;
										$current_promo1['promo_value'] = $plist->promo_value;
										$current_promo1['promo_end'] = $plist->promo_end;
										$current_promo1['promo_start'] = $plist->promo_start;
										array_push($current_promo, $current_promo1);
									}
								
								}
								
								
								
							}
							else
							{
							
								if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))
								{		
									
									array_push($array_amt, $plist->promo_buy);
									$current_promo1['promo_id'] = $plist->promo_id;
									$current_promo1['promo_restid'] = $plist->promo_restid;
									$current_promo1['promo_for'] = $plist->promo_for;
									$current_promo1['promo_menu_item'] = $plist->promo_menu_item;
									$current_promo1['promo_on'] = $plist->promo_on;
									$current_promo1['promo_desc'] = $plist->promo_desc;
									$current_promo1['promo_mode'] = $plist->promo_mode;
									$current_promo1['promo_buy'] = $plist->promo_buy;
									$current_promo1['promo_value'] = $plist->promo_value;
									$current_promo1['promo_end'] = $plist->promo_end;
									$current_promo1['promo_start'] = $plist->promo_start;
									
									
									array_push($current_promo, $current_promo1);
								}
							}
						}
					}		
				}
				else
				{
					$current_promo = array();
					$offer_applied_on_this_cart='';
				}
			
			
			
					$offer_amt = 0;
			 		$cal_sub_totla = $sub_total_cart;
			 	
			 
			if((count($current_promo)>0) && (!empty($array_amt))) {	
			
			  	$maxs = max($array_amt);
			  	$key_name = array_search ($maxs, $array_amt);			  
				$k=0;$i=0;
				foreach($current_promo as $pval)
				{
					if($key_name==$k)
					{
				
						if($pval['promo_mode']=='$')
						{
							$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
							$offer_amt = ($offer_amt+$pval['promo_value']);
							$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);
						}
						
						if(($pval['promo_mode']=='%') )
						{	$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
							$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);										
							$cal_sub_totla = ($cal_sub_totla - $promo_val_per);									
							$offer_amt = ($offer_amt+$promo_val_per);
						}
						
						if(($pval['promo_mode']=='0') )
						{
							$i++;
							$offer_applied_on_this_cart=$pval['promo_desc'];
						}
					}
					$k++;
				}
				
			}
			else{			
				$offer_applied_on_this_cart='';
				$current_promo = array();
			}
			
			
			
			
			
			
			
			
			
			/** PROMOTIONS CODE END  **/
			
			
			/******************  FOOD ITEM  */
			/*$food_disocunt
			
			$food_current_promo*/
			
			$food_offer_amt = '';
			$offer_applied_on_food= '';
			
			
			if(!empty($food_current_promo) && (count($food_current_promo)>0))
			{
							
				$k=0;$i=0;
				foreach($food_current_promo as $pval)
				{
					
					$item_price = $food_cart_amount[$k];					
					$item_qty = $food_cart_qty[$k];
					$cal_sub_total=number_format(($item_price*$item_qty),2);
				
						if($pval['promo_mode']=='$')
						{
							$i++;
							$offer_applied_on_food=$pval['promo_desc'];
							$food_offer_amt = ($offer_amt+$pval['promo_value']);
						}
						
						if(($pval['promo_mode']=='%') )
						{	$i++;
							$offer_applied_on_food=$pval['promo_desc'];
							$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);						
							$food_offer_amt = ($offer_amt+$promo_val_per);
						}
						
						if(($pval['promo_mode']=='0') )
						{
							$i++;
							$offer_applied_on_food=$pval['promo_desc'];
						}
					$k++;	
					
				}
				
			
			}
			else
			{
				$food_current_promo= array();
			}
			
			/*******************  END  ****/
			
			
			if(((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt) < (float)$delivery_fee)
			{
				$total_amt = number_format(($delivery_fee+$serv_tax),2);
			}
			else
			{
			
				$aa_total = ((float)$sub_total_cart-(float)$offer_amt-(float)$food_offer_amt)+(float)$serv_tax;
				$total_amt = number_format(($aa_total),2);
			}
			
			
			
			
			/*print_r($current_promo);			
			print_r(json_encode($json_data_session));
			exit;*/
			
			DB::table('rest_cart')	
				->where('user_id', '=' ,trim($user_id))
				->where('guest_id', '=' ,trim($guest_id))
				->where('rest_id', '=' ,trim($rest_id))		
		->update(['cart_detail' => (json_encode($json_data_session)),
				  'offer_detail'=> (json_encode($current_promo)),
				  'offer_amt'=> trim($offer_amt),  
				  
				  'offer_applied'=>$offer_applied_on_this_cart,
				  'food_promo_list'=>json_encode($food_current_promo),
				  'food_offer_applied'=>json_encode($offer_applied_on_food),
				  
				  'service_tax_amt'=>trim($serv_tax),
				  'delivery_amt'=> trim($delivery_fee),
				  'subtotal'=> trim($sub_total_cart),
				  'total_amt'=> trim($total_amt)
				 ]);
				 
			
			$tax_for_service=((trim($sub_total_cart)*$service_charge)/100);
					$response['message'] = "All deatil of cart data";
					$response['cart_detail'] = $cart_detail;
					$response['other_detail'] = array(
					'subtotal'=> number_format($sub_total_cart,2),
					'rest_service' =>$service_charge,
					'delivery_fee'=>number_format($delivery_fee,2),
					'service_tax'=> number_format($tax_for_service,2),
					'promo_list'=>$current_promo,
					'discount_amt'=>number_format($offer_amt,2),
					'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,
					'food_promo_list'=>$food_current_promo,
					'offer_applied_on_item'=>$offer_applied_on_food,
					'total_amt'=>number_format($total_amt,2));
					$response['ok'] = 1;	
					
					
					
			}
			
			
		
		
		 
		/*  return response()->json( array('order_offer_on'=>'','discount_amt'=>'','discount_txt'=>'','service_tax' =>'', 'total_amt' =>'','promo_list' =>'','food_promo_list_cal' =>'','food_promo_applied_cal' =>'','cart_view'=>$cart_view) );*/
		  $tax_for_service=((trim($sub_total_cart)*$service_charge)/100);
		       return response()->json( array('order_offer_on'=>$promo_id,
			   	'discount_amt'=>$offer_amt,
				'discount_txt'=>$offer_applied_on_this_cart,
				'promo_list' => json_encode($current_promo),
				'food_promo_list_cal' =>json_encode($food_current_promo),
				'food_promo_applied_cal' =>$offer_applied_on_food,
				'service_tax' =>  number_format($tax_for_service,2), 
				'total_amt' => $sub_total_cart) );
				
				
			   
			   
		  
		  
		  
		  /*****************************/
		  
		
	
		  
		
	
	}

	Public function get_delivery_chrg(){

		$rest_subrub =  $_POST['subrub'];
		//Route::current()->getParameter('subrub');



		$rest_title =  $_POST['title'];
		//Route::current()->getParameter('restname');
		$rest_detail = DB::table('restaurant')	

					->select('restaurant.*',

					DB::raw("(SELECT GROUP_CONCAT(cuisine_name SEPARATOR ', ') AS `combined_A` FROM `cuisine` WHERE FIND_IN_SET( cuisine_id,restaurant.rest_cuisine)) as cuisine_name")

						)	

					->where('restaurant.rest_status', '!=' , 'INACTIVE')

					->where('restaurant.rest_metatag', '=' , $rest_title)

					->get();

					echo json_encode($rest_detail[0]);
	}



	
	
	/******************* AFTER LOGIN CHECK ISSUE ***********************/
	
}
