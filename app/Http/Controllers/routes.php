<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/payment/process', 'PaymentsController@process')->name('payment.process');
Route::get('/payment/cancel', 'PaymentsController@cancel')->name('payment.cancel');
Route::get('/form', function () { return view('bt'); });
Route::post('/payment/SetSession', 'PaymentsController@SetSession');
//Route::get('/demo','GeneralController@main_page');
Route::get('/demo', function () { return redirect('/demo/home'); });
//Route::get('/home', function () { return redirect('/demo/home'); });
Route::get('/demo/', function () { return redirect('/demo/home'); });
Route::get('/','GeneralController@main_page');
Route::get('/copy_google_data','GeneralController@show_auto_form');
Route::post('/post_google_data','GeneralController@get_form_data');
Route::get('/update_list_copy','GeneralController@update_list_copy');
Route::get('/content/{page_url}','GeneralController@show_content_page');
Route::get('/home','GeneralController@main_page');
Route::get('/terms_condition','GeneralController@show_terms_condition');
Route::get('/privacy_policy','GeneralController@show_privacy_policy');
//Route::get('foo/bar/{id}', ['middleware'=>'auth', 'uses'=>'FooController@show']);
Route::get('/restaurant_listing/cuisine/{cuisine_name}','ListController@restaurant_listing');
Route::get('/restaurant_listing/location/{surbru_name}','ListController@restaurant_listing');
Route::get('/restaurant_listing','ListController@restaurant_listing');
Route::get('/restaurant_listing/{surbru}','ListController@restaurant_listing');
Route::get('/get_suburblist','GeneralController@show_suburblist');
Route::get('/smtp_mail','MyMailer@index');
Route::get('/send_notification_test', 'MyMailer@notification_test');
Route::get('/test_mail','FrontController@test_mail');
/* HOME PAGE SEARCH LIST */
Route::post('/restaurantslisting','ListController@home_search');
/* HOMEPAGE SEARCH LIST END */
//Route::get('/restaurant_detail/{id}','ListController@restaurant_detail');
Route::post('/check_user_duplicateemail','Auth\AuthController@user_duplicate_email');
Route::get('/test','Auth\TestController@test');
Route::post('/test','Auth\TestController@test');
Route::post('/resendotp','Auth\AuthController@resend_otp');
Route::post('/check_vendor_duplicateemail','OwnerAuth\AuthController@vendor_duplicate_email');
Route::post('auth/login', 'FrontController@authenticate');
Route::post('user_register', 'FrontController@register');
Route::post('/check_user_login', 'FrontController@check_user_login');

// 18-11-2019
Route::post('/user_password/user_forgot_password','FrontController@user_sentOTP');
Route::post('/user_password/user_enter_otp','FrontController@VerifyUserOTP');
Route::get('/user_password/reset_your_user_password','FrontController@setUserNewPasswordForm');
Route::post('/user_password/reset_your_user_password','FrontController@setUserNewPassword');
// 18-11-2019

Route::group(['middlewareGroups' => ['web']], function () {
	Route::auth();
	Route::get('/admin/general_setting','AdminController@showsetting');
	Route::post('/admin/update_setting','AdminController@update_admin_setting');
	Route::get('/admin/active_log','AdminController@showactivelog');
	Route::get('/admin/user_log_delete/{id}','AdminController@showactivelog_delete');
	Route::get('/admin/viewmenu_log','AdminController@showmenulog');
	Route::get('/admin/menu_log_delete/{id}','AdminController@showmenulog_delete');
//Login Routes...
	Route::get('/admin','AdminAuth\AuthController@showLoginForm');
	Route::get('/admin/','AdminAuth\AuthController@showLoginForm');
	Route::get('/admin/login/','AdminAuth\AuthController@showLoginForm');
	Route::post('/admin/login','AdminAuth\AuthController@login');
	Route::get('/admin/logout','AdminAuth\AuthController@logout');
	Route::get('/admin/password/reset','AdminAuth\AuthController@forgotpwd');
	Route::get('/admin/dashboard', 'AdminController@index');
	Route::get('/admin/change_password', 'AdminController@showPasswordForm');
	Route::post('/admin/update_account', 'AdminController@updateprofile_action');
	Route::post('/admin/change_password', 'AdminController@updatepassword');

	Route::post('/admin/transfer-vendor', 'AdminwalletController@transfer_vendor');

	Route::post('/admin/request-amount-form', 'AdminwalletController@request_amount_form');


	/*sub-admin start*/
	// Route::get('/sub-admin','SubadminAuth\AuthController@showLoginForm');
	// Route::get('/sub-admin/','SubadminAuth\AuthController@showLoginForm');
	// Route::get('/sub-admin/login/','SubadminAuth\AuthController@showLoginForm');
	// Route::post('/sub-admin/login','SubadminAuth\AuthController@login');
	// Route::get('/sub-admin/logout','SubadminAuth\AuthController@logout');
	// Route::get('/sub-admin/password/reset','AdminAuth\AuthController@Subadminforgotpwd');
	// Route::get('/sub-admin','SubadminController@loginform');
	// Route::get('/sub-admin/login','SubadminController@loginform');
	// Route::post('/sub-admin/login','SubadminController@login_action');
	// Route::get('/sub-admin/dashboard','SubadminController@index');
	// Route::get('/sub-admin/logout','SubadminController@logout');
	// Route::get('/sub-admin/change_password', 'SubadminController@showPasswordForm');
	// Route::post('/sub-admin/update_account', 'SubadminController@updateprofile_action');
	// Route::post('/sub-admin/change_password', 'SubadminController@updatepassword');
	// Route::get('/sub-admin/general_setting','SubadminController@showsetting');
	// Route::post('/sub-admin/update_setting','SubadminController@update_admin_setting');
	/*sub-admin end*/




	/*SUB-ADMIN START*/
	// Route::get('/sub-admin','SubadminAuth\AuthController@showLoginForm');
	// Route::get('/sub-admin/','SubadminAuth\AuthController@showLoginForm');
	// Route::get('/sub-admin/login/','SubadminAuth\AuthController@showLoginForm');
	// Route::post('/sub-admin/login','SubadminAuth\AuthController@login');
	// Route::get('/sub-admin/logout','SubadminAuth\AuthController@logout');
	// Route::get('/sub-admin/password/reset','AdminAuth\AuthController@Subadminforgotpwd');
	Route::get('/sub-admin','subadmin_controllers\SubadminController@loginform');
	Route::get('/sub-admin/login','subadmin_controllers\SubadminController@loginform');
	Route::post('/sub-admin/login','subadmin_controllers\SubadminController@login_action');
	Route::get('/sub-admin/dashboard','subadmin_controllers\SubadminController@index');
	Route::get('/sub-admin/logout','subadmin_controllers\SubadminController@logout');
	Route::get('/sub-admin/change_password', 'subadmin_controllers\SubadminController@showPasswordForm');
	Route::post('/sub-admin/update_account', 'subadmin_controllers\SubadminController@updateprofile_action');
	Route::post('/sub-admin/change_password', 'subadmin_controllers\SubadminController@updatepassword');
	Route::get('/sub-admin/general_setting','subadmin_controllers\SubadminController@showsetting');
	Route::post('/sub-admin/update_setting','subadmin_controllers\SubadminController@update_admin_setting');


	// sub-admin user module

	Route::get('/sub-admin/userlist', 'subadmin_controllers\SubadminUserController@user_list');
	Route::get('/sub-admin/user-form', 'subadmin_controllers\SubadminUserController@user_form');
	Route::get('/sub-admin/user-form/{id}', 'subadmin_controllers\SubadminUserController@user_form');
	Route::post('/sub-admin/check_user_duplicateemail','subadmin_controllers\SubadminUserController@user_duplicate_email');
	Route::post('/sub-admin/user_action','subadmin_controllers\SubadminUserController@user_action');
	Route::get('/sub-admin/user_delete/{id}', 'subadmin_controllers\SubadminUserController@user_delete');
	Route::get('/sub-admin/user_orderlist/{userid}','subadmin_controllers\SubadminorderController@show_orderlist');

	// sub-admin Area module

	Route::get('/sub-admin/area_list', 'subadmin_controllers\SubadminareaController@area_List');
	Route::get('/sub-admin/area-form', 'subadmin_controllers\SubadminareaController@area_form');
	Route::post('/sub-admin/area-form', 'subadmin_controllers\SubadminareaController@addarea');
	Route::get('/sub-admin/area-view/{id}', 'subadmin_controllers\SubadminareaController@ViewArea');
	Route::get('/sub-admin/area_delete/{id}', 'subadmin_controllers\SubadminareaController@area_delete');
	Route::get('/sub-admin/area-edit-form/{id}', 'subadmin_controllers\SubadminareaController@EditareaForm');
	Route::post('/sub-admin/area-edit-form', 'subadmin_controllers\SubadminareaController@UpdateAreaDetail');

	// sub-admin Vendor module

	Route::get('/sub-admin/vendorlist','subadmin_controllers\SubadminVendorController@vendor_list');
	Route::get('/sub-admin/vendor-form','subadmin_controllers\SubadminVendorController@vendor_form');
	Route::get('/sub-admin/vendor-form/{id}','subadmin_controllers\SubadminVendorController@vendor_form');
	Route::post('/sub-admin/check_vendor_duplicateemail','subadmin_controllers\SubadminVendorController@vendor_duplicate_email');
	Route::post('/sub-admin/vendor_action','subadmin_controllers\SubadminVendorController@vendor_action');
	Route::get('/sub-admin/vendor_delete/{id}','subadmin_controllers\SubadminVendorController@vendor_delete');
	Route::get('/sub-admin/vendor_profile/{id}','subadmin_controllers\SubadminVendorController@view_vendor');
	Route::post('/sub-admin/vendor_search','subadmin_controllers\SubadminVendorController@vendor_search');

	// sub-admin restaurant module

	Route::get('/sub-admin/restaurant_list', 'subadmin_controllers\SubadminresturantController@restaurant_list');
	Route::post('/sub-admin/restaurant_search', 'subadmin_controllers\SubadminresturantController@restaurant_search');
	Route::get('/sub-admin/restaurant-form', 'subadmin_controllers\SubadminresturantController@restaurant_form');
	Route::get('/sub-admin/vendor-rest-form/{vendor_id}', 'subadmin_controllers\SubadminresturantController@restaurant_form');
	Route::get('/sub-admin/restaurant-form/{id}', 'subadmin_controllers\SubadminresturantController@restaurant_form');
	Route::post('/sub-admin/restaurant_action','subadmin_controllers\SubadminresturantController@restaurant_action');
	Route::get('/sub-admin/restaurant_delete/{id}', 'subadmin_controllers\SubadminresturantController@restaurant_delete');
	Route::get('/sub-admin/restaurant_view/{id}', 'subadmin_controllers\SubadminResturantDetailController@view_restaurant');
	Route::get('sub-admin/menu_list', 'subadmin_controllers\SubadminresturantController@menu_list');
	Route::get('/sub-admin/menu-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_form');
	Route::post('/sub-admin/menu_action','subadmin_controllers\SubadminresturantController@menu_action');
	Route::get('/sub-admin/menu_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_delete');
	Route::get('sub-admin/menu_categorylist', 'subadmin_controllers\SubadminresturantController@menu_category_list');
	Route::get('/sub-admin/category-form', 'subadmin_controllers\SubadminresturantController@menu_category_form');
	Route::get('/sub-admin/category-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_category_form');
	Route::get('/sub-admin/menu_cat_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_category_delete');
	Route::post('/sub-admin/get_menulist','subadmin_controllers\SubadminresturantController@get_menulist');
	Route::get('sub-admin/menu_cate_item_list', 'subadmin_controllers\SubadminresturantController@menu_item_list');
	Route::get('/sub-admin/category-item-form', 'subadmin_controllers\SubadminresturantController@menu_item_form');
	Route::get('/sub-admin/category-item-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_item_form');
	Route::post('/sub-admin/category_item_action','subadmin_controllers\SubadminresturantController@menu_item_action');
	Route::get('/sub-admin/menu_cat_item_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_item_delete');
	Route::post('/sub-admin/get_categorylist','subadmin_controllers\SubadminresturantController@get_categorylist');
	Route::get('/sub-admin/get_suburblist','subadmin_controllers\SubadminresturantController@show_suburblist');
	/* MENU ,ITEM AND ADONS CATEGORY FUNCTIONALITY WITH AJAX START */
	Route::post('/sub-admin/menu_ajax_form', 'subadmin_controllers\SubadminresturantController@ajax_menu_form');
	Route::post('/sub-admin/menu_ajax_itemform', 'subadmin_controllers\SubadminresturantController@ajax_menu_item_form');
	Route::post('/sub-admin/show_menudetail', 'subadmin_controllers\SubadminresturantController@ajax_show_menudetail');
	Route::post('/sub-admin/update_sortorder', 'subadmin_controllers\SubadminresturantController@ajax_update_sortorder');
	Route::post('/sub-admin/update_menu_item', 'subadmin_controllers\SubadminresturantController@ajax_update_menu_category');
	Route::post('/sub-admin/addons_list', 'subadmin_controllers\SubadminresturantController@ajax_addons_list');
	Route::post('/sub-admin/addons_form', 'subadmin_controllers\SubadminresturantController@ajax_addons_form');
	Route::post('/sub-admin/addon_action', 'subadmin_controllers\SubadminresturantController@ajax_addons_action');
    //Route::get('/admin/ajax_menu_form/{rest_id}', 'AdminresturantController@menu_form');
	Route::post('/sub-admin/service_form', 'subadmin_controllers\SubadminresturantController@ajax_service_form');
	Route::post('/sub-admin/service_action', 'subadmin_controllers\SubadminresturantController@service_action');
	Route::post('/sub-admin/promo_form', 'subadmin_controllers\SubadminresturantController@ajax_promo_form');
	Route::post('/sub-admin/promo_action', 'subadmin_controllers\SubadminresturantController@promo_action');
	Route::post('/sub-admin/bank_form', 'subadmin_controllers\SubadminresturantController@ajax_bank_form');
	Route::post('/sub-admin/bank_action', 'subadmin_controllers\SubadminresturantController@bank_action');
	Route::post('/sub-admin/category_action','subadmin_controllers\SubadminresturantController@menu_category_action');
	Route::post('/sub-admin/show_popular','subadmin_controllers\SubadminresturantController@menu_category_popularlist');

	//menu template module

	Route::get('/sub-admin/template_menu_list/{id}','subadmin_controllers\SubadminmenutemplateController@show_menu_list');
	Route::post('/sub-admin/template_menu_ajax_form', 'subadmin_controllers\SubadminmenutemplateController@ajax_menu_form');
	Route::post('/sub-admin/template_menu_action','subadmin_controllers\SubadminmenutemplateController@menu_action');
	Route::post('/sub-admin/template_show_menudetail', 'subadmin_controllers\SubadminmenutemplateController@ajax_show_menudetail');
	Route::post('/sub-admin/template_update_sortorder', 'subadmin_controllers\SubadminmenutemplateController@ajax_update_sortorder');
	Route::post('/sub-admin/template_show_popular','subadmin_controllers\SubadminmenutemplateController@menu_category_popularlist');
	Route::post('/sub-admin/template_menu_ajax_itemform', 'subadmin_controllers\SubadminmenutemplateController@ajax_menu_item_form');
	Route::post('/sub-admin/template_update_menu_item', 'subadmin_controllers\SubadminmenutemplateController@ajax_update_menu_category');
	Route::post('/sub-admin/template_category_action','subadmin_controllers\SubadminmenutemplateController@menu_category_action');
	Route::post('/sub-admin/template_addons_list', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_list');
	Route::post('/sub-admin/template_addons_form', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_form');
	Route::post('/sub-admin/template_addon_action', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_action');
	Route::post('/sub-admin/menutemplate_action', 'subadmin_controllers\SubadminmenutemplateController@get_template_action');


	Route::get('/sub-admin/menu_template','subadmin_controllers\SubadminmenutemplateController@show_menutemplate_list');
	Route::get('/sub-admin/get_templatename','subadmin_controllers\SubadminmenutemplateController@get_template_form');
	Route::get('/sub-admin/get_templatename/{id}', 'subadmin_controllers\SubadminmenutemplateController@get_template_form');
	Route::post('/sub-admin/menu_template_action','subadmin_controllers\SubadminmenutemplateController@template_action');
	Route::get('/sub-admin/template_menu_delete/{id}','subadmin_controllers\SubadminmenutemplateController@template_delete');
	Route::get('/sub-admin/delete_template_menus/{id}','subadmin_controllers\SubadminmenutemplateController@deletetemplate_menus');



	//sub-admin order module

	Route::get('/sub-admin/orderlisting','subadmin_controllers\SubadminorderController@show_orderlist');
	Route::get('/sub-admin/order_view','subadmin_controllers\SubadminorderController@show_orderdetail');
	Route::post('/sub-admin/order_status_update','subadmin_controllers\SubadminorderController@update_order_action');
	Route::post('/sub-admin/order_search_list','subadmin_controllers\SubadminorderController@ajax_search_list');
	Route::get('/sub-admin/user_orderlist/{userid}','subadmin_controllers\SubadminorderController@show_orderlist');
	Route::post('/sub-admin/order_report','subadmin_controllers\SubadminorderController@show_orderreport');



	Route::get('/sub-admin/orderlisting','subadmin_controllers\SubadminorderController@show_orderlist');
	Route::get('/sub-admin/order_view','subadmin_controllers\SubadminorderController@show_orderdetail');
	Route::post('/sub-admin/order_status_update','subadmin_controllers\SubadminorderController@update_order_action');
	Route::post('/sub-admin/order_search_list','subadmin_controllers\SubadminorderController@ajax_search_list');
	Route::get('/sub-admin/user_orderlist/{userid}','subadmin_controllers\SubadminorderController@show_orderlist');
	Route::post('/sub-admin/order_report','subadmin_controllers\SubadminorderController@show_orderreport');



	// sub-admin review list

	/* MANAGE REVIEW AND REATING */
	Route::get('/sub-admin/review_list','subadmin_controllers\SubadminreviewController@show_reviewlist');
	Route::post('/sub-admin/review_update','subadmin_controllers\SubadminreviewController@show_review_action');
	Route::post('/sub-admin/review_search_list','subadmin_controllers\SubadminreviewController@ajax_search_list');
	Route::get('/sub-admin/review_delete/{id}', 'subadmin_controllers\SubadminreviewController@review_delete');


	//sub-admin payment module

	/* MANAGE PAYMENTLISTING */
	Route::get('/sub-admin/paymentlisting','subadmin_controllers\SubadminorderController@show_paymentlist');
	Route::get('/sub-admin/payment_view','subadmin_controllers\SubadminorderController@show_paymentdetail');
	Route::post('/sub-admin/payment_search_list','subadmin_controllers\SubadminorderController@ajax_show_paymentlist');
	Route::post('/sub-admin/payment_report','subadmin_controllers\SubadminorderController@show_paytmentreport');
	Route::post('/sub-admin/payment_status_update','subadmin_controllers\SubadminorderController@update_payment_action');


	//sub-admin content management

	/* MANAGE CONTENT PAGES */
	Route::get('/sub-admin/content_pagelist', 'subadmin_controllers\SubadminPagesController@page_listing');
	Route::get('/sub-admin/page-form/{id}', 'subadmin_controllers\SubadminPagesController@page_form');
	Route::post('/sub-admin/page_action','subadmin_controllers\SubadminPagesController@page_action');
	Route::get('/sub-admin/socail_pagelist', 'subadmin_controllers\SubadminPagesController@social_listing');
	Route::get('/sub-admin/social-form/{id}', 'subadmin_controllers\SubadminPagesController@show_socila_form');
	Route::post('/sub-admin/social_action','subadmin_controllers\SubadminPagesController@get_social_action');


	Route::get('/sub-admin/testimonial_list','subadmin_controllers\SubadminPagesController@get_testiminial');
	Route::get('/sub-admin/testimonial-form', 'subadmin_controllers\SubadminPagesController@get_testiminial_form');
	Route::get('/sub-admin/testimonial-form/{id}', 'subadmin_controllers\SubadminPagesController@get_testiminial_form');
	Route::post('/sub-admin/testimonial_action','subadmin_controllers\SubadminPagesController@get_testiminial_action');
	Route::get('/sub-admin/testimonial_delete/{id}','subadmin_controllers\SubadminPagesController@testimonial_delete');
	Route::get('/sub-admin/homepage_content', 'subadmin_controllers\SubadminPagesController@home_content');
	Route::get('/sub-admin/content-form/{id}', 'subadmin_controllers\SubadminPagesController@show_content_form');
	Route::post('/sub-admin/homepage_action','subadmin_controllers\SubadminPagesController@homepage_action');



	// sub-admin send notification module

	Route::get('/sub-admin/send_notification', 'subadmin_controllers\SubadminPagesController@show_send_notification');
	Route::post('/sub-admin/send_notification_action', 'subadmin_controllers\SubadminPagesController@notification_action');

	// sub-admin email module

	Route::get('/sub-admin/email_content', 'subadmin_controllers\SubadminPagesController@email_content');
	Route::get('/sub-admin/email_content_form/{id}', 'subadmin_controllers\SubadminPagesController@show_email_form');
	Route::post('/sub-admin/email_content_action','subadmin_controllers\SubadminPagesController@email_content_action');

	// sub-admin delivery man management module

	/*Delivery man management start*/
	Route::get('/sub-admin/delivery_manlist', 'subadmin_controllers\SubadmindeliverymanController@user_list');
	Route::get('/sub-admin/delivery_man-form', 'subadmin_controllers\SubadmindeliverymanController@user_form');
	Route::post('/sub-admin/delivery_man-form', 'subadmin_controllers\SubadmindeliverymanController@user_action');
	Route::get('/sub-admin/deliveryman_delete/{id}', 'subadmin_controllers\SubadmindeliverymanController@Deliveryman_delete');
	Route::get('/sub-admin/deliveryman-edit-form/{id}', 'subadmin_controllers\SubadmindeliverymanController@user_form');
	Route::post('/sub-admin/deliveryman-edit-form', 'subadmin_controllers\SubadmindeliverymanController@UpdateSubAdminDetail');
	Route::get('/sub-admin/deliveryman-view/{id}', 'subadmin_controllers\SubadmindeliverymanController@ViewDeliveryman');
	Route::post('/sub-admin/check_deliveryman_duplicateemail','subadmin_controllers\SubadmindeliverymanController@deliveryman_duplicate_email');
	/*Deliver man management end*/
	/*SUB-ADMIN END*/




	// Admin Permissions module


	Route::get('/admin/permissions_list', 'AdminPermissionsController@permission_list');
	Route::post('/admin/permission-form', 'AdminPermissionsController@permission_action');
	//Route::get('/admin/permission-form/{id}', 'AdminPermissionsController@permission_form');
	//Route::get('/admin/permission_delete/{id}', 'AdminPermissionsController@Deliveryman_delete');
	///Route::get('/admin/permission-edit-form/{id}', 'AdminPermissionsController@user_form');
	//Route::post('/admin/permission-edit-form', 'AdminPermissionsController@UpdateSubAdminDetail');
	//Route::get('/admin/permission-view/{id}', 'AdminPermissionsController@ViewDeliveryman');



	/*My routs start*/
	Route::get('/admin/password/reset_password','AdminAuth\AuthController@forgetPassword');
	Route::post('/admin/password/reset_password','AdminAuth\AuthController@sentOTP');
	Route::get('/admin/password/enter_otp','AdminAuth\AuthController@VerifyOTPForm');
	Route::post('/admin/password/enter_otp','AdminAuth\AuthController@VerifyOTP');
	Route::get('/admin/password/reset_your_password','AdminAuth\AuthController@setNewPasswordForm');
	Route::post('/admin/password/reset_your_password','AdminAuth\AuthController@setNewPassword');
	/*End*/
	/* MANAGE USER DETAIL */
	Route::get('/admin/subadmin_list', 'AdminsubController@user_list');
	Route::get('/admin/subadmin-form', 'AdminsubController@user_form');
	/*Subadmin start*/
	Route::post('/admin/subadmin-form', 'AdminsubController@subAdmin_form');
	Route::get('/admin/subadmin_delete/{id}', 'AdminsubController@subadmin_delete');
//Route::post('/admin/subadmin_delete/{id}', 'AdminsubController@subadmin_delete');
	Route::get('/admin/subadmin-edit-form/{id}', 'AdminsubController@EditsubadminForm');
	Route::post('/admin/subadmin-edit-form', 'AdminsubController@UpdateSubAdminDetail');
	Route::get('/admin/subadmin-view/{id}', 'AdminsubController@ViewSubadmin');
	Route::post('/admin/check_suadmin_duplicateemail','AdminsubController@sub_adminduplicate_email');
	/*Subadmin end*/
	/*Area management start*/
	Route::get('/admin/area_list', 'AdminareaController@area_List');
	Route::get('/admin/area-form', 'AdminareaController@area_form');
	Route::post('/admin/area-form', 'AdminareaController@addarea');
	Route::get('/admin/area-view/{id}', 'AdminareaController@ViewArea');
	Route::get('/admin/area_delete/{id}', 'AdminareaController@area_delete');
	Route::get('/admin/area-edit-form/{id}', 'AdminareaController@EditareaForm');
	Route::post('/admin/area-edit-form', 'AdminareaController@UpdateAreaDetail');
	/*Area management end*/

	/* Coupon code start */

    Route::get('/admin/coupon_code', 'AdminareaController@coupon_code');
	Route::get('/admin/coupon-form', 'AdminareaController@coupon_form');
	Route::post('/admin/coupon-form', 'AdminareaController@add_coupon_code');
	Route::get('/admin/coupon_code_delete/{id}', 'AdminareaController@coupon_delete');
	Route::get('/admin/coupon-edit-form/{id}', 'AdminareaController@coupon_edit_form');
	Route::post('/admin/coupon-edit-form', 'AdminareaController@update_coupon_detail');

	/* Coupon code end */

	Route::get('/admin/subadmin-form/{id}', 'AdminsubController@user_form');
	Route::post('/admin/check_subadmin_duplicateemail','AdminsubController@user_duplicate_email');
	Route::post('/admin/subadmin_action','AdminsubController@user_action');
//Route::get('/admin/subadmin_delete/{id}', 'AdminsubController@user_delete');
	/* MANAGE USER DETAIL */
	Route::get('/admin/userlist', 'AdminUserController@user_list');
	Route::get('/admin/user-form', 'AdminUserController@user_form');
	Route::get('/admin/user-form/{id}', 'AdminUserController@user_form');
	Route::post('/admin/check_user_duplicateemail','AdminUserController@user_duplicate_email');
	Route::post('/admin/user_action','AdminUserController@user_action');
	Route::get('/admin/user_delete/{id}', 'AdminUserController@user_delete');


	/*Delivery man management start*/
	Route::get('/admin/delivery_manlist', 'AdmindeliverymanController@user_list');
	Route::get('/admin/delivery_man-form', 'AdmindeliverymanController@user_form');
	Route::post('/admin/delivery_man-form', 'AdmindeliverymanController@user_action');
	Route::get('/admin/deliveryman-edit-form/{id}', 'AdmindeliverymanController@user_form');
	Route::get('/admin/deliveryman-view/{id}', 'AdmindeliverymanController@ViewDeliveryman');
	Route::post('/admin/check_deliveryman_duplicateemail','AdmindeliverymanController@deliveryman_duplicate_email');
	/*Deliver man management end*/



	/* MANAGE VENDOR DETAIL */
	Route::get('/admin/vendorlist', 'AdminVendorController@vendor_list');
	Route::get('/admin/vendor-form', 'AdminVendorController@vendor_form');
	Route::get('/admin/vendor-form/{id}', 'AdminVendorController@vendor_form');
	Route::post('/admin/check_vendor_duplicateemail','AdminVendorController@vendor_duplicate_email');
	Route::post('/admin/vendor_action','AdminVendorController@vendor_action');
	Route::get('/admin/vendor_delete/{id}', 'AdminVendorController@vendor_delete');
	Route::get('/admin/vendor_profile/{id}', 'AdminVendorController@view_vendor');
	Route::post('/admin/vendor_search', 'AdminVendorController@vendor_search');
	/* Vendor Category */
	Route::get('/admin/vendor_category_list', 'AdminVendorController@vendor_cate_list');
	Route::get('/admin/vendor_category_form', 'AdminVendorController@vendor_cate_form');
	Route::get('/admin/vendor_category_delete/{id}', 'AdminVendorController@vendor_cate_delete');
	Route::post('/admin/vendor_cate_action','AdminVendorController@vendor_cate_action');
	/* MANAGE CUISINE DETAIL */
	Route::get('/admin/cuisinelist', 'AdminCuisineController@cuisine_list');
	Route::get('/admin/cuisine-form', 'AdminCuisineController@cuisine_form');
	Route::get('/admin/cuisine-form/{id}', 'AdminCuisineController@cuisine_form');
	Route::post('/admin/cuisine_action','AdminCuisineController@cuisine_action');
	Route::get('/admin/cuisine_delete/{id}', 'AdminCuisineController@cuisine_delete');
	/* MANAGE GOOGLE REATAUANRT DATA BASED ON CUISINE DETAIL */
	Route::get('/admin/view_google_form', 'AdminCuisineController@show_google_form');
	Route::post('/admin/get_google_data', 'AdminCuisineController@get_form_data');
	/* MANAGE CONTENT PAGES */
	Route::get('/admin/content_pagelist', 'AdminPagesController@page_listing');

    Route::get('/admin/page-form', 'AdminPagesController@page_form');
    Route::get('/admin/page_delete/{id}','AdminPagesController@page_delete');

	Route::get('/admin/page-form/{id}', 'AdminPagesController@page_form');
	Route::post('/admin/page_action','AdminPagesController@page_action');
	Route::get('/admin/socail_pagelist', 'AdminPagesController@social_listing');
	Route::get('/admin/social-form/{id}', 'AdminPagesController@show_socila_form');
	Route::post('/admin/social_action','AdminPagesController@get_social_action');
	/* MANAGE RESTAURANT*/
	Route::get('/admin/restaurant_list', 'AdminresturantsController@restaurant_list');
	Route::post('/admin/restaurant_search', 'AdminResturantDetailController@restaurant_search');
	Route::get('/admin/restaurant-form', 'AdminresturantsController@restaurant_form');
	Route::get('/admin/vendor-rest-form/{vendor_id}', 'AdminresturantsController@restaurant_form');
	Route::get('/admin/restaurant-form/{id}', 'AdminresturantsController@restaurant_form');
	Route::post('/admin/restaurant_action','AdminresturantsController@restaurant_action');
	Route::get('/admin/restaurant_delete/{id}', 'AdminresturantsController@restaurant_delete');

//Route::get('/admin/delete_restaurant/{id}', 'AdminresturantsController@restaurant_delete');
//	Route::get('/admin/restaurant_view/{id}', 'AdminresturantsController@view_restaurant');
	Route::get('/admin/restaurant_view/{id}', 'AdminResturantDetailController@view_restaurant');
	Route::get('admin/menu_list', 'AdminresturantsController@menu_list');
// Route::get('/admin/menu-form', 'AdminresturantsController@menu_form');
	Route::get('/admin/menu-form/{id}', 'AdminresturantsController@menu_form');
	Route::post('/admin/menu_action','AdminresturantsController@menu_action');
	Route::get('/admin/menu_delete/{id}', 'AdminresturantsController@menu_delete');
	Route::get('admin/menu_categorylist', 'AdminresturantsController@menu_category_list');
	Route::get('/admin/category-form', 'AdminresturantsController@menu_category_form');
	Route::get('/admin/category-form/{id}', 'AdminresturantsController@menu_category_form');
	Route::get('/admin/menu_cat_delete/{id}', 'AdminresturantsController@menu_category_delete');
	Route::post('/admin/get_menulist','AdminresturantsController@get_menulist');
	Route::get('admin/menu_cate_item_list', 'AdminresturantsController@menu_item_list');
	Route::get('/admin/category-item-form', 'AdminresturantsController@menu_item_form');
	Route::get('/admin/category-item-form/{id}', 'AdminresturantsController@menu_item_form');
	Route::post('/admin/category_item_action','AdminresturantsController@menu_item_action');
	Route::get('/admin/menu_cat_item_delete/{id}', 'AdminresturantsController@menu_item_delete');
	Route::post('/admin/get_categorylist','AdminresturantsController@get_categorylist');
	/*LOCATION */
	Route::get('/admin/get_suburblist','AdminresturantsController@show_suburblist');
	/* MENU ,ITEM AND ADONS CATEGORY FUNCTIONALITY WITH AJAX START */
	Route::post('/admin/menu_ajax_form', 'AdminresturantsController@ajax_menu_form');
	Route::post('/admin/menu_ajax_itemform', 'AdminresturantsController@ajax_menu_item_form');
	Route::post('/admin/show_menudetail', 'AdminresturantsController@ajax_show_menudetail');
	Route::post('/admin/update_sortorder', 'AdminresturantsController@ajax_update_sortorder');
	Route::post('/admin/update_menu_item', 'AdminresturantsController@ajax_update_menu_category');
	Route::post('/admin/addons_list', 'AdminresturantsController@ajax_addons_list');
	Route::post('/admin/addons_form', 'AdminresturantsController@ajax_addons_form');
	Route::post('/admin/addon_action', 'AdminresturantsController@ajax_addons_action');
//Route::get('/admin/ajax_menu_form/{rest_id}', 'AdminresturantsController@menu_form');
	Route::post('/admin/service_form', 'AdminresturantsController@ajax_service_form');
	Route::post('/admin/service_action', 'AdminresturantsController@service_action');
	Route::post('/admin/promo_form', 'AdminresturantsController@ajax_promo_form');
	Route::post('/admin/promo_action', 'AdminresturantsController@promo_action');
	Route::post('/admin/bank_form', 'AdminresturantsController@ajax_bank_form');
	Route::post('/admin/bank_action', 'AdminresturantsController@bank_action');
	Route::post('/admin/category_action','AdminresturantsController@menu_category_action');
	Route::post('/admin/show_popular','AdminresturantsController@menu_category_popularlist');
	/* END */
	/* MANAGE ORDERLISTING */
	Route::get('/admin/orderlisting','AdminorderController@show_orderlist');
	Route::get('/admin/order_view','AdminorderController@show_orderdetail');
	Route::post('/admin/order_status_update','AdminorderController@update_order_action');
	Route::post('/admin/assign_order','AdminorderController@assign_order_action');
	Route::post('/admin/order_search_list','AdminorderController@ajax_search_list');
	Route::get('/admin/user_orderlist/{userid}','AdminorderController@show_orderlist');
	Route::post('/admin/order_report','AdminorderController@show_orderreport');
	/* MANAGE PAYMENTLISTING */
	Route::get('/admin/paymentlisting','AdminorderController@show_paymentlist');
	Route::get('/admin/payment_view','AdminorderController@show_paymentdetail');
	Route::post('/admin/payment_search_list','AdminorderController@ajax_show_paymentlist');
	Route::post('/admin/payment_report','AdminorderController@show_paytmentreport');
	Route::post('/admin/payment_status_update','AdminorderController@update_payment_action');
	/* MANAGE REVIEW AND REATING */
	Route::get('/admin/review_list','AdminreviewController@show_reviewlist');
	Route::post('/admin/review_update','AdminreviewController@show_review_action');
	Route::post('/admin/review_search_list','AdminreviewController@ajax_search_list');
	Route::get('/admin/review_delete/{id}', 'AdminreviewController@review_delete');
	/* FRONT USER AFTER LOGIN SHOW USER ACCOUNT*/
	Route::get('/myaccount','UserController@dashboard');
	Route::get('/update_account','UserController@update_account_form');
	Route::post('/update_account_action','UserController@updateaccount_action');
	Route::get('/update_password','UserController@change_password');
	Route::post('/password_action', 'UserController@updatepassword_action');
	Route::get('/userlogout', 'FrontController@logout');

//Route::get('/usermail', 'FrontController@sendmail');

	Route::get('/myorder','UserController@order_listing');
	Route::get('/orderreview/{orderid}','UserController@show_reviewfrm');
	Route::post('/order_review_action', 'UserController@orderreview_action');
	Route::post('/show_order_detail','UserController@get_order_detail');
	Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' );
	Route::get ( '/callback/{service}', 'SocialAuthController@callback' );

	Route::get('/payment_history','UserController@payment_history');
    Route::get('/my_wallet','UserController@my_wallet');
    Route::post('/add_wallet','UserController@add_wallet');
    Route::post('/payment_wallet','UserController@payment_wallet');
    Route::post('/payment_wallet_paypal','UserController@payment_wallet_paypal');

    Route::post('/ws/add_money_wallet','WsController@add_money_wallet');
    Route::post('/ws/my_wallet','WsController@my_wallet');
    Route::post('/ws/coupon_code_apply','WsController@coupon_code_apply');

/*Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');*/
//Route::get('auth/facebook', 'SocialAuthController@redirect')->name('facebook.login');
//Route::get('auth/facebook/callback', 'SocialAuthController@callback');

//login via facebook
Route::get('auth/facebook', 'SocialAuthController@redirect')->name('facebook.login');
Route::get('auth/facebook/callback', 'SocialAuthController@callback');
//
//login via google
Route::get('auth/google', 'SocialAuthController@redirectToGoogle')->name('google.login');
Route::get('auth/google/callback', 'SocialAuthController@handleGoogleCallback');
//

Route::get('test', 'SocialAuthController@test');
Route::get('auth/google/callback', 'SocialAuthController@handleGoogleCallback')->name('google.login');
Route::get('google-user', 'SocialAuthController@listGoogleUser');
/*Route::get('glogin',array('as'=>'glogin','uses'=>'UserController@googleLogin'));
Route::get('google-user',array('as'=>'user.glist','uses'=>'UserController@listGoogleUser'));*/
//$s = 'social.';
//Route::get('/social/redirect/{provider}',   ['as' => $s . 'redirect',   'uses' => 'SocialController@getSocialRedirect']);
//Route::get('/social/handle/{provider}',     ['as' => $s . 'handle',     'uses' => 'SocialController@getSocialHandle']);
/* TRANSPORT MODULE START   */
Route::get('/admin/transport_list','AdmintransportController@show_transportlist');
Route::post('/admin/transport_search','AdmintransportController@show_result');
Route::get('/admin/transport-form/{vendor_id}','AdmintransportController@show_transportfrm');
Route::post('/admin/transport_action','AdmintransportController@transport_action');
Route::get('/admin/transport_view/{transport_id}','AdmintransportController@show_transportdetail');
Route::get('/admin/transport-update/{transport_id}','AdmintransportController@show_transportfrm');
Route::post('/admin/transport_service_form', 'AdmintransportController@ajax_service_form');
Route::post('/admin/transport_service_action', 'AdmintransportController@service_action');
Route::post('/admin/transport_promo_form', 'AdmintransportController@ajax_promo_form');
Route::post('/admin/transport_promo_action', 'AdmintransportController@promo_action');
Route::post('/admin/transport_bank_form', 'AdmintransportController@ajax_bank_form');
Route::post('/admin/transport_bank_action', 'AdmintransportController@bank_action');
Route::get('/admin/vehicle_category_list', 'AdminvehicleController@category_list');
Route::get('/admin/vehicle_category-form', 'AdminvehicleController@category_form');
Route::get('/admin/vehicle_category-form/{id}', 'AdminvehicleController@category_form');
Route::post('/admin/vehicle_category_action','AdminvehicleController@category_action');
Route::get('/admin/vehicle_class_list', 'AdminvehicleController@class_list');
Route::get('/admin/vehicle_class-form', 'AdminvehicleController@class_form');
Route::get('/admin/vehicle_class-form/{id}', 'AdminvehicleController@class_form');
Route::post('/admin/vehicle_class_action','AdminvehicleController@class_action');
Route::post('/admin/vehicle_ajax_form', 'AdmintransportController@vehicle_form');
Route::post('/admin/vehicle_action', 'AdmintransportController@vehicle_action');
Route::post('/admin/findmodel', 'AdmintransportController@find_model');
Route::post('/admin/show_vehicledetail', 'AdmintransportController@ajax_vehicle_detail');
Route::post('/admin/update_vehicle_sortorder', 'AdmintransportController@ajax_update_sortorder');
Route::post('/admin/vehicle_charge_form', 'AdmintransportController@ajax_vehicle_charge_form');
Route::post('/admin/vehicle_charge_action','AdmintransportController@vehicle_charge_action');
Route::post('/admin/update_vehicle_images','AdmintransportController@vehicle_image_update');
Route::post('/admin/vehicle_addons_list', 'AdmintransportController@ajax_addons_list');
Route::post('/admin/vehicle_addons_form', 'AdmintransportController@ajax_addons_form');
Route::post('/admin/vehicle_addon_action', 'AdmintransportController@ajax_addons_action');
/* MANAGE ORDERLISTING */
Route::get('/admin/transport_order','AdmintransportorderController@show_orderlist');
Route::get('/admin/transport_order_view','AdmintransportorderController@show_orderdetail');
Route::post('/admin/transport_order_status_update','AdmintransportorderController@update_order_action');
Route::post('/admin/transport_order_search_list','AdmintransportorderController@ajax_search_list');
Route::post('/admin/transport_order_report','AdmintransportorderController@show_orderreport');

/* MANAGE PAYMENTLISTING */
Route::get('/admin/transport_payment','AdmintransportorderController@show_paymentlist');
Route::get('/admin/transport_payment_view','AdmintransportorderController@show_paymentdetail');
Route::post('/admin/transport_payment_search_list','AdmintransportorderController@ajax_show_paymentlist');
Route::post('/admin/transport_payment_report','AdmintransportorderController@show_paytmentreport');
Route::post('/admin/transport_payment_status_update','AdmintransportorderController@update_payment_action');
Route::get('/admin/transport_review_list','AdmintransportorderController@show_reviewlist');
Route::post('/admin/transport_review_search_list','AdmintransportorderController@ajax_review_search_list');
Route::post('/admin/transport_review_update','AdmintransportorderController@show_review_action');
Route::get('/mytransportorder','UserController@transport_order_listing');
//Route::get('/orderreview/{orderid}','UserController@show_reviewfrm');
Route::post('/transport_order_review_action', 'UserController@transport_orderreview_action');
/*****************************************  ADMIN ROUTES FOR REST APP PROJECT  *********************************************/
Route::get('/admin/testimonial_list','AdminPagesController@get_testiminial');
Route::get('/admin/testimonial-form', 'AdminPagesController@get_testiminial_form');
Route::get('/admin/testimonial-form/{id}', 'AdminPagesController@get_testiminial_form');
Route::post('/admin/testimonial_action','AdminPagesController@get_testiminial_action');
Route::get('/admin/testimonial_delete/{id}','AdminPagesController@testimonial_delete');
Route::get('/admin/homepage_content', 'AdminPagesController@home_content');
Route::get('/admin/content-form/{id}', 'AdminPagesController@show_content_form');
Route::post('/admin/homepage_action','AdminPagesController@homepage_action');
Route::get('/admin/coupon_request', 'AdminPagesController@show_coupon_request');
Route::get('/admin/coupon_delete/{id}', 'AdminPagesController@coupon_request_delete');
Route::post('/admin/delete_all_offerrequest', 'AdminPagesController@delete_all_coupon');
Route::get('/admin/send_notification', 'AdminPagesController@show_send_notification');
Route::post('/admin/send_notification_action', 'AdminPagesController@notification_action');
Route::get('/admin/email_content', 'AdminPagesController@email_content');
Route::get('/admin/email_content_form/{id}', 'AdminPagesController@show_email_form');
Route::post('/admin/email_content_action','AdminPagesController@email_content_action');
Route::get('/admin/suggestion_list', 'AdminPagesController@show_suggestion_list');
Route::get('/admin/suggestion_delete/{id}', 'AdminPagesController@suggestion_request_delete');
/********************************* MENU TEMPLATE ************************************************/
Route::get('/admin/menu_template','AdminmenutemplateController@show_menutemplate_list');
Route::get('/admin/get_templatename','AdminmenutemplateController@get_template_form');
Route::get('/admin/get_templatename/{id}', 'AdminmenutemplateController@get_template_form');
Route::post('/admin/menu_template_action','AdminmenutemplateController@template_action');
Route::get('/admin/template_menu_delete/{id}','AdminmenutemplateController@template_delete');

/*My template menu start*/

Route::get('/admin/delete_template_menus/{id}','AdminmenutemplateController@deletetemplate_menus');

/*My template menu end*/

Route::get('/admin/template_menu_list/{id}','AdminmenutemplateController@show_menu_list');
Route::post('/admin/template_menu_ajax_form', 'AdminmenutemplateController@ajax_menu_form');
Route::post('/admin/template_menu_action','AdminmenutemplateController@menu_action');
Route::post('/admin/template_show_menudetail', 'AdminmenutemplateController@ajax_show_menudetail');
Route::post('/admin/template_update_sortorder', 'AdminmenutemplateController@ajax_update_sortorder');
Route::post('/admin/template_show_popular','AdminmenutemplateController@menu_category_popularlist');
Route::post('/admin/template_menu_ajax_itemform', 'AdminmenutemplateController@ajax_menu_item_form');
Route::post('/admin/template_update_menu_item', 'AdminmenutemplateController@ajax_update_menu_category');
Route::post('/admin/template_category_action','AdminmenutemplateController@menu_category_action');
Route::post('/admin/template_addons_list', 'AdminmenutemplateController@ajax_addons_list');
Route::post('/admin/template_addons_form', 'AdminmenutemplateController@ajax_addons_form');
Route::post('/admin/template_addon_action', 'AdminmenutemplateController@ajax_addons_action');
Route::post('/admin/menutemplate_action', 'AdminresturantsController@get_template_action');

Route::get('/admin/my_wallet', 'AdminwalletController@my_wallet');
Route::get('/admin/dm_wallet', 'AdminwalletController@dm_wallet');
Route::post('/dm/bank-setup', 'AdminwalletController@bank_setup');

/*****************************************  ADMIN ROUTES  *********************************************/
/* TRANSPORT MODULE END   */
});

Route::group(['middlewareGroups' => ['vendor']], function () {
	Route::get('/vendor','OwnerAuth\AuthController@showLoginForm');
//Login Routes...
//Route::get('/vendor','OwnerAuth\AuthController@showLoginForm');
//Route::get('/vendor','OwnerAuth\AuthController@showLoginForm');
	Route::get('/vendor/login/','OwnerAuth\AuthController@showLoginForm');
	Route::post('/vendor/login','OwnerAuth\AuthController@login');
	Route::get('/vendor/logout','OwnerAuth\AuthController@logout');
	Route::get('/vendor/password/reset','OwnerAuth\AuthController@forgotpwd');
	Route::get('/vendor/forgot_password','OwnerAuth\AuthController@show_forgotpwd');
	Route::post('/vendor/password/forgot_password','OwnerAuth\AuthController@sentOTP');
	Route::get('/vendor/password/enter_otp','OwnerAuth\AuthController@VerifyOTPForm');
	Route::post('/vendor/password/enter_otp','OwnerAuth\AuthController@VerifyOTP');
	Route::get('/vendor/password/reset_your_password','OwnerAuth\AuthController@setNewPasswordForm');
	Route::post('/vendor/password/reset_your_password','OwnerAuth\AuthController@setNewPassword');

// Route::get('/vendor/dashboard', 'OwnerAuth\OwnerController@index');
	Route::get('/vendor/dashboard', 'RestOwner\OwnerController@index');
	Route::get('/vendor/update_profile', 'RestOwner\OwnerController@view_profile');
	Route::post('/vendor/update_profile', 'RestOwner\OwnerController@update_profile_action');
	Route::get('/vendor/change_password', 'RestOwner\OwnerController@showPasswordForm');
	Route::post('/vendor/change_password', 'RestOwner\OwnerController@updatepassword');
	/* MANAGE RESTAURENT */
	Route::get('/vendor/restaurant_list', 'RestOwner\OwnerrestaurantController@restaurant_list');
	Route::post('/vendor/restaurant_search', 'RestOwner\OwnerrestaurantController@restaurant_search');
	Route::get('/vendor/restaurant-form', 'RestOwner\OwnerrestaurantController@restaurant_form');
	Route::get('/vendor/restaurant-branch-form/{id}', 'RestOwner\OwnerrestaurantController@restaurant_branch_form');
	Route::get('/vendor/restaurant-form/{id}', 'RestOwner\OwnerrestaurantController@restaurant_form');
	Route::post('/vendor/restaurant_action','RestOwner\OwnerrestaurantController@restaurant_action');
	Route::post('/vendor/restaurant_branch_action','RestOwner\OwnerrestaurantController@restaurant_branch_action');
	Route::get('/vendor/restaurant_delete/{id}', 'RestOwner\OwnerrestaurantController@restaurant_delete');
	Route::get('/vendor/restaurant_view/{id}', 'RestOwner\OwnerrestaurantController@view_restaurant');
	Route::post('/vendor/menu_ajax_form', 'RestOwner\OwnerrestaurantController@ajax_menu_form');
	Route::post('/vendor/menu_action','RestOwner\OwnerrestaurantController@menu_action');
	Route::post('/vendor/show_menudetail', 'RestOwner\OwnerrestaurantController@ajax_show_menudetail');
	Route::post('/vendor/menu_ajax_itemform', 'RestOwner\OwnerrestaurantController@ajax_menu_item_form');
	Route::post('/vendor/category_action','RestOwner\OwnerrestaurantController@menu_category_action');
	Route::post('/vendor/update_menu_item', 'RestOwner\OwnerrestaurantController@ajax_update_menu_category');
	Route::post('/vendor/update_sortorder', 'RestOwner\OwnerrestaurantController@ajax_update_sortorder');
	Route::post('/vendor/show_popular','RestOwner\OwnerrestaurantController@menu_category_popularlist');
	Route::post('/vendor/addons_list', 'RestOwner\OwnerrestaurantController@ajax_addons_list');
	Route::post('/vendor/addons_form', 'RestOwner\OwnerrestaurantController@ajax_addons_form');
	Route::post('/vendor/addon_action', 'RestOwner\OwnerrestaurantController@ajax_addons_action');
	Route::post('/vendor/promo_form', 'RestOwner\OwnerrestaurantController@ajax_promo_form');
	Route::post('/vendor/promo_action', 'RestOwner\OwnerrestaurantController@promo_action');
	/* MANAGE ORDERLISTING */
	Route::get('/vendor/orderlisting', 'RestOwner\OwnerorderController@show_orderlist');
	Route::get('/vendor/order_view', 'RestOwner\OwnerorderController@show_orderdetail');
	Route::post('/vendor/order_status_update','RestOwner\OwnerorderController@update_order_action');
	Route::post('/vendor/order_address_update','RestOwner\OwnerorderController@update_order_address');
	Route::post('/vendor/order_status_update_notification','RestOwner\OwnerorderController@update_order_action_notification');
	Route::post('/vendor/order_search_list','RestOwner\OwnerorderController@ajax_search_list');
	Route::get('/vendor/user_orderlist/{userid}','RestOwner\OwnerorderController@show_orderlist');
	Route::post('/vendor/order_report','RestOwner\OwnerorderController@show_orderreport');
	/* MANAGE Scheduled Order */
	Route::get('/vendor/scheduled_order', 'RestOwner\OwnerorderController@show_scheduled_order');
	Route::get('/vendor/scheduled_order_view', 'RestOwner\OwnerorderController@show_scheduled_order_detail');
	Route::post('/vendor/scheduled_order_search_list','RestOwner\OwnerorderController@ajax_scheduled_search_list');

	/* MANAGE PAYMENTLISTING */
	Route::get('/vendor/paymentlisting', 'RestOwner\OwnerorderController@show_paymentlist');
	Route::get('/vendor/payment_view','RestOwner\OwnerorderController@show_paymentdetail');
	Route::post('/vendor/payment_search_list','RestOwner\OwnerorderController@ajax_show_paymentlist');
	Route::post('/vendor/payment_report','RestOwner\OwnerorderController@show_paytmentreport');
	Route::post('/vendor/payment_status_update','RestOwner\OwnerorderController@update_payment_action');
	/* MANAGE REVIEW AND REATING */
	Route::get('/vendor/review_list','RestOwner\OwnerreviewController@show_reviewlist');
	Route::post('/vendor/review_update','RestOwner\OwnerreviewController@show_review_action');
	Route::post('/vendor/review_search_list','RestOwner\OwnerreviewController@ajax_search_list');
	Route::get('/vendor/review_delete/{id}', 'RestOwner\OwnerreviewController@review_delete');
	/* Review and rating reports */
	Route::get('/vendor/review_rating_report','RestOwner\OwnerreviewController@show_reviewlist_report');
	Route::post('/vendor/review_search_list_report','RestOwner\OwnerreviewController@ajax_search_list_report');
	/* MANAGE Send Notifiaction*/
	Route::get('/vendor/send_notification', 'RestOwner\OwnerController@show_send_notification');
	Route::post('/vendor/send_notification_action', 'RestOwner\OwnerController@notification_action');
	/* VIEW MENU TEMPLATES */
	Route::get('/vendor/view_menu_template', 'RestOwner\OwnermenutemplateController@show_menutemplate_list');
	Route::get('/vendor/view_menu_list/{id}', 'RestOwner\OwnermenutemplateController@show_menu_list');
	Route::post('/vendor/template_show_menudetail', 'RestOwner\OwnermenutemplateController@ajax_show_menudetail');
	Route::post('/vendor/template_addons_list', 'RestOwner\OwnermenutemplateController@ajax_addons_list');
	Route::post('/vendor/template_show_popular','RestOwner\OwnermenutemplateController@menu_category_popularlist');
	Route::post('/vendor/menutemplate_action', 'RestOwner\OwnerrestaurantController@get_template_action');

	Route::get('/vendor/my_wallet', 'RestOwner\OwnerController@my_wallet');
	Route::post('/vendor/request-amount', 'RestOwner\OwnerController@request_amount');
	Route::post('/vendor/bank-setup', 'RestOwner\OwnerController@bank_setup');

	Route::post('/ws/vendor/my_wallet', 'WsController@vendor_my_wallet');
	Route::post('/ws/vendor/request-amount', 'WsController@vendor_request_amount');
	Route::post('/ws/vendor/bank-setup', 'WsController@vendor_bank_setup');
	Route::post('/ws/vendor/request-history', 'WsController@vendor_request_history');
	Route::post('/ws/vendor/get_bank_details', 'WsController@get_bank_details');

	
});
//Route::auth();
Route::get('/{subrub}/{restname}','ListController@restaurant_detail');
Route::get('/{subrub}/{restname}/{review}','ListController@restaurant_review');
/*re-order start*/
Route::get('/reorder','ListController@show_reorder');
/*re-order end*/
Route::post('/restaurant_detail/dinein','ListController@get_dineinfrm');
Route::post('/cart','CartController@add_tocart');
Route::post('/cart/increment','CartController@qty_icnrement');
Route::post('/cart/decrease','CartController@qty_decrease');
Route::post('/cart/get_cart','CartController@show_cart');
Route::post('/get_delivery','CartController@get_delivery_chrg');
Route::post('/post_checkout_rest','CheckoutController@rest_order_submit');
Route::get('/checkout','CheckoutController@view_checkout_order');
Route::post('/checkout/showTime','CheckoutController@view_delivery_time');
Route::post('/checkout/getDeliveryTimeStatus','CheckoutController@getDeliveryTimeStatus');
Route::post('/submit_checkoutdetail','CheckoutController@get_checkout_detail');
Route::get('/payment','CheckoutController@payment_option_page');
Route::post('/checkout/cash_payment_process','CheckoutController@get_cash_payment');
Route::post('/checkout/wallet_payment_process','CheckoutController@get_wallet_payment');
Route::get('/order_detail','OrderuserController@view_order_detail');
Route::get('/order_success/{tx?}','OrderuserController@sucess_order');
Route::get('/order_cancel','OrderuserController@cancel_order');
Route::post('/become_partner','GeneralController@get_partnerdetail');
Route::post('/show_other_data','ListController@get_addons');
Route::post('/checkout/coupon_code_apply','CheckoutController@coupon_code_apply');

//Route::get('/home', 'HomeController@index');
/* TRANSPORTS ROUTES START */
Route::get('/transport_listing','TransportController@get_transportlist');
Route::get('/get_location_from','TransportController@get_from_locationlist');
Route::get('/get_location','TransportController@get_locationlist');
Route::get('/show_returntime','TransportController@show_returntime_drop');
Route::get('/setval_session','TransportController@show_search');
Route::get('/{subrub}/{vehicle}/{class}/{make}/{vehicle_id}','TransportController@vehicle_detail');
Route::get('/{subrub}/{vehicle}/{class}/{make}/{vehicle_id}/{review}','TransportController@vehicle_review');
Route::post('/transport_cart','TransportcartController@add_tocart');
Route::get('/reset_transportinfo','TransportController@transport_search_reset');
Route::post('/show_rate_addons','TransportController@get_addons');
Route::post('/post_checkout','TransportController@order_submit');
Route::get('/transport_checkout','TransportController@show_checkoutpage');
Route::post('/submit_personaldetail','TransportController@userdetail_submit');
Route::get('/transport_payment','TransportController@show_paymentpage');
Route::get('/transport_order_cancel','TransportController@show_order_cancel');
Route::get('/transport_order_success','TransportController@show_order_success');
/* END */
/**************************************************************************************
********************************** webservise start ***********************************
**************************************************************************************/
/*Route::post('webservice/register', 'WsController@register');
Route::post('webservice/login', 'WsController@login');
Route::post('webservice/updateprofile', 'WsController@update_profile');
Route::post('webservice/devicetoken', 'WsController@device_token');
Route::post('webservice/updatepassword', 'WsController@change_password');
Route::post('webservice/mail_testing', 'WsController@testemail');*/

//Route::post('/webservice/viewpagecontent','WsController@viewpagecontent');
//Route::post('/webservice/mail_testing','WsController@testemail');
//Route::post('/webservice/register','WsController@register');
//Route::post('/webservice/newtest','WsController@newtest');

/*My webservice start*/
// 29-8-2019
// Route::get('test','WsController@test_reg');
// Route::post('register','WsController@register');
// Route::post('verify_OTP','WsController@verify_OTP');
// Route::post('resent_OTP','WsController@resent_OTP');
// Route::post('forget_passwrod','WsController@forget_pwd');
// Route::post('resetpassword','WsController@reset_password');
// Route::post('login','WsController@login');
// Route::post('restaurant_search','WsController@search_restaurant');
// Route::post('soical_signup','WsController@get_soical_signup');
// Route::post('view_profile','WsController@viewprofile');
// Route::post('updateprofile','WsController@update_profile');
// // 30-8-2019
// Route::post('viewpagecontent','WsController@viewpagecontent');
// Route::post('devicetoken','WsController@device_token');
// // 31-8-2019
// Route::post('make_favourite_restaurent','WsController@make_favourite');
// Route::post('remove_favourite_restaurent','WsController@remove_favourite');
// Route::post('restaurantreview','WsController@restaurant_review_listing');
// Route::post('food_search','WsController@food_rest_search');
// Route::post('restaurant_food_search','WsController@food_search');
// Route::post('updatepassword','WsController@change_password');
// Route::post('favourite_restaurent_list','WsController@favourite_list');

// // 3-9-2019

// Route::post('fooddetail','WsController@restaurant_food_detail');
// Route::post('view_notification','WsController@show_notification');
// Route::post('restaurant_open','WsController@show_restaurant_open');
// Route::post('update_order_type','WsController@get_update_order_type');
// Route::post('post_review','WsController@post_order_review');
// Route::post('view_order','WsController@show_order_detail');
// Route::post('verify_delivery_address','WsController@get_verify_delivery_address');
// Route::post('newdelivery_address','WsController@add_delivery_address');
// Route::post('restaurantdetail','WsController@restaurant_detail');

// // 4-9-2019

// Route::post('addcart','WsController@food_addcart');
// Route::post('updatecart','WsController@food_updatecart');
// Route::post('add_removecart','WsController@food_add_removecart');
// Route::post('make_favourite_food','WsController@make_food_favourite');
// Route::post('remove_favourite_food','WsController@remove_food_favourite');
// Route::post('getcart','WsController@food_getcartdeatil');


// // 5-9-2019

// Route::post('save_order','WsController@get_save_order');
// Route::post('deletefood','WsController@food_itemdelete');

Route::get('/ws','WsController@index');
Route::post('/ws/register','WsController@register');
Route::post('/ws/verify_OTP','WsController@verify_OTP');
Route::post('/ws/resent_OTP','WsController@resent_OTP');
Route::post('/ws/forget_passwrod','WsController@forget_pwd');
Route::post('/ws/resetpassword','WsController@reset_password');
Route::post('/ws/login','WsController@login');

/* Vendor API routs start*/

Route::post('/ws/vendor-register','WsController@vendor_register');
Route::post('/ws/vendor-login','WsController@vendor_login');
Route::post('/ws/vendor-forget-passwrod','WsController@vendor_forget_pwd');
Route::post('/ws/vendor-verify-otp','WsController@vendor_verify_OTP');
Route::post('/ws/vendor_category','WsController@vendor_categories');
//Route::post('/ws/vendor-resetpassword','WsController@reset_password');
Route::post('/ws/vendor-view-profile','WsController@vendor_viewprofile');
Route::post('/ws/vendor-edit-profile','WsController@vendor_update_profile');

Route::post('/ws/vendorOrder-list','WsController@show_vendorOrderlist');
Route::post('/ws/vendor_inprogressOrder-list','WsController@vendor_inprogressOrderlist');
Route::post('/ws/vendor-accept_or_reject_order','WsController@vendor_order_accept_or_reject');

Route::post('/ws/vendorOrder-detail','WsController@show_order_detail_vendor');
Route::post('/ws/vendorHistoryOrder-list','WsController@vendor_HistoryOrderlist');
Route::post('/ws/vendor_devicetoken','WsController@vendor_device_token');
Route::post('/ws/vendor_notification_list','WsController@show_vendor_notification');


/* Vendor API routs end*/


/*Delivery man API routs start*/
// 9/9/2019
Route::post('/ws/deliveryman-login','WsController@deliverman_login');
Route::post('/ws/forgetpassword','WsController@forgetPassword');
Route::post('/ws/deliveryman-profile','WsController@viewdeliverymanprofile');
Route::post('/ws/updatedeliveryman-profile','WsController@deliverymanupdate_profile');
Route::post('/ws/getdeliveryman-location','WsController@deliverymanget_location');
Route::post('/ws/assignedorder-list','WsController@show_deliverymanOrderlist');
Route::post('/ws/deliverymanorder-detail','WsController@show_order_detail_deliveryman');
Route::post('/ws/order-acceptreject','WsController@accept_reject_order');
Route::post('/ws/updateorder-status','WsController@update_order_status');
Route::post('/ws/verify_order','WsController@verify_order_code');
Route::post('/ws/Orderhistory-list','WsController@show_deliverymanOrderHistorylist');
Route::post('/ws/Orderhistory-detail','WsController@show_historyorder_detail');
Route::post('/ws/fromto-location','WsController@from_to_location');
Route::post('/ws/get-updatelocation','WsController@get_current_location');
Route::post('/ws/from_to-show_locationtouser','WsController@show_locationtion_user');
Route::post('/ws/deliveryman_devicetoken','WsController@deliveryman_device_token');
Route::post('/ws/dm_view_notification','WsController@dm_show_notification');

/*Delivery man API routs end*/
Route::post('/ws/restaurant_search','WsController@search_restaurant');
Route::post('/ws/soical_signup','WsController@get_soical_signup');
Route::post('/ws/view_profile','WsController@viewprofile');
Route::post('/ws/updateprofile','WsController@update_profile');
// 30-8-2019
Route::post('/ws/viewpagecontent','WsController@viewpagecontent');
Route::post('/ws/devicetoken','WsController@device_token');
// 31-8-2019
Route::post('/ws/make_favourite_restaurent','WsController@make_favourite');
Route::post('/ws/remove_favourite_restaurent','WsController@remove_favourite');
Route::post('/ws/restaurantreview','WsController@restaurant_review_listing');
Route::post('/ws/food_search','WsController@food_rest_search');
Route::post('/ws/restaurant_food_search','WsController@food_search');
Route::post('/ws/updatepassword','WsController@change_password');
Route::post('/ws/favourite_restaurent_list','WsController@favourite_list');

// 3-9-2019

Route::post('/ws/fooddetail','WsController@restaurant_food_detail');
Route::post('/ws/view_notification','WsController@show_notification');
Route::post('/ws/restaurant_open','WsController@show_restaurant_open');
Route::post('/ws/update_order_type','WsController@get_update_order_type');
Route::post('/ws/post_review','WsController@post_order_review');
Route::post('/ws/view_order','WsController@show_order_detail');
Route::post('/ws/verify_delivery_address','WsController@get_verify_delivery_address');
Route::post('/ws/newdelivery_address','WsController@add_delivery_address');
Route::post('/ws/restaurantdetail','WsController@restaurant_detail');

// 4-9-2019

Route::post('/ws/addcart','WsController@food_addcart');
Route::post('/ws/updatecart','WsController@food_updatecart');
Route::post('/ws/add_removecart','WsController@food_add_removecart');
Route::post('/ws/removecart', 'WsController@food_removecart');
Route::post('/ws/make_favourite_food','WsController@make_food_favourite');
Route::post('/ws/remove_favourite_food','WsController@remove_food_favourite');
Route::post('/ws/getcart','WsController@food_getcartdeatil');


// 5-9-2019

Route::post('/ws/save_order','WsController@get_save_order');
Route::post('/ws/deletefood','WsController@food_itemdelete');
Route::post('/ws/orderlisting','WsController@show_order_listing');

// 6-9-2019
Route::post('/ws/get_token','WsController@getToken');
Route::post('/ws/card_payment','WsController@payCardPayment');
Route::post('/ws/send_factor_otp','WsController@two_factor_otp');
Route::post('/ws/varify_factor_otp','WsController@get_varify_factor_otp');
Route::post('/ws/resent_factor_otp','WsController@get_resent_factor_otp');


// 7-9-2019
Route::post('/ws/content_pages','WsController@show_contentpage_link');
Route::post('/ws/notification_status','WsController@update_notification_status');
Route::post('/ws/checkout_verificaiton_status','WsController@update_checkout_verificaiton_status');

Route::post('/ws/guest_user_otp','WsController@show_guest_user_otp');
Route::post('/ws/varify_guest_user_otp','WsController@get_varify_guest_otp');
Route::post('/ws/guest_user_resentotp','WsController@show_guest_user_resentotp');


// 27-9-2019
Route::post('ws/delivery_address_list','WsController@show_delivery_address');
Route::post('ws/delete_delivery_address','WsController@delete_delivery_address');
Route::post('ws/update_delivery_address','WsController@update_delivery_address');


/*My webservice end*/
/* Route::post('/webservice/login','WsController@login');
Route::post('webservice/verify_OTP','WsController@verify_OTP');
Route::post('webservice/resent_OTP','WsController@resent_OTP');
Route::post('webservice/forget_passwrod','WsController@forget_pwd');
Route::post('webservice/view_profile','WsController@viewprofile');
Route::post('webservice/devicetoken','WsController@device_token');
Route::post('webservice/updateprofile','WsController@update_profile');
Route::post('webservice/updatepassword','WsController@change_password');


Route::post('webservice/restaurant_search',
	array('uses' => 'WsController@search_restaurant','middleware' => ['checkHeader']));
Route::post('webservice/restaurantdetail',
	array('uses' => 'WsController@restaurant_detail','middleware' => ['checkHeader']));
Route::post('webservice/restaurantreview',
	array('uses' => 'WsController@restaurant_review_listing','middleware' => ['checkHeader']));
Route::post('webservice/fooddetail',
	array('uses' => 'WsController@restaurant_food_detail','middleware' => ['checkHeader']));
Route::post('webservice/food_search',
	array('uses' => 'WsController@food_rest_search','middleware' => ['checkHeader']));
Route::post('webservice/restaurant_food_search',
	array('uses' => 'WsController@food_search','middleware' => ['checkHeader']));
Route::post('webservice/resetpassword',
	array('uses' => 'WsController@reset_password','middleware' => ['checkHeader']));
Route::post('webservice/addcart',
	array('uses' => 'WsController@food_addcart','middleware' => ['checkHeader']));
Route::post('webservice/updatecart',
	array('uses' => 'WsController@food_updatecart','middleware' => ['checkHeader']));
Route::post('webservice/deletefood',
	array('uses' => 'WsController@food_itemdelete','middleware' => ['checkHeader']));
Route::post('webservice/removecart',
	array('uses' => 'WsController@food_removecart','middleware' => ['checkHeader']));
Route::post('webservice/getcart',
	array('uses' => 'WsController@food_getcartdeatil','middleware' => ['checkHeader']));
Route::post('webservice/make_favourite_restaurent',
	array('uses' => 'WsController@make_favourite','middleware' => ['checkHeader']));
Route::post('webservice/remove_favourite_restaurent',
	array('uses' => 'WsController@remove_favourite','middleware' => ['checkHeader']));
Route::post('webservice/favourite_restaurent_list',
	array('uses' => 'WsController@favourite_list','middleware' => ['checkHeader']));
Route::post('webservice/post_review',
	array('uses' => 'WsController@post_order_review','middleware' => ['checkHeader']));
Route::post('webservice/orderlisting',
	array('uses' => 'WsController@show_order_listing','middleware' => ['checkHeader']));
Route::post('webservice/notification_status',
	array('uses' => 'WsController@update_notification_status','middleware' => ['checkHeader']));
Route::post('webservice/make_favourite_food',
	array('uses' => 'WsController@make_food_favourite','middleware' => ['checkHeader']));
Route::post('webservice/remove_favourite_food',
	array('uses' => 'WsController@remove_food_favourite','middleware' => ['checkHeader']));
Route::post('webservice/content_pages',
	array('uses' => 'WsController@show_contentpage_link','middleware' => ['checkHeader']));
Route::post('webservice/add_removecart',
	array('uses' => 'WsController@food_add_removecart','middleware' => ['checkHeader']));
Route::post('webservice/social_login',
	array('uses' => 'WsController@show_soical_login','middleware' => ['checkHeader']));
Route::post('webservice/restaurant_open',
	array('uses' => 'WsController@show_restaurant_open','middleware' => ['checkHeader']));
Route::post('webservice/update_order_type',
	array('uses' => 'WsController@get_update_order_type','middleware' => ['checkHeader']));
Route::post('webservice/delivery_address_list',
	array('uses' => 'WsController@show_delivery_address','middleware' => ['checkHeader']));
Route::post('webservice/newdelivery_address',
	array('uses' => 'WsController@add_delivery_address','middleware' => ['checkHeader']));
Route::post('webservice/update_delivery_address',
	array('uses' => 'WsController@update_delivery_address','middleware' => ['checkHeader']));
Route::post('webservice/delete_delivery_address',
	array('uses' => 'WsController@delete_delivery_address','middleware' => ['checkHeader']));
Route::post('webservice/verify_delivery_address',
	array('uses' => 'WsController@get_verify_delivery_address','middleware' => ['checkHeader']));
Route::post('webservice/save_order',
	array('uses' => 'WsController@get_save_order','middleware' => ['checkHeader']));
Route::post('webservice/save_paypal_responce',
	array('uses' => 'WsController@get_paypal_responce','middleware' => ['checkHeader']));
Route::post('webservice/view_order',
	array('uses' => 'WsController@show_order_detail','middleware' => ['checkHeader']));
Route::post('webservice/view_notification',
	array('uses' => 'WsController@show_notification','middleware' => ['checkHeader']));
Route::post('webservice/update_cart_user',
	array('uses' => 'WsController@update_temp_userid','middleware' => ['checkHeader']));
Route::post('webservice/checkout_verificaiton_status',
	array('uses' => 'WsController@update_checkout_verificaiton_status','middleware' => ['checkHeader']));
Route::post('webservice/soical_signup',
	array('uses' => 'WsController@get_soical_signup','middleware' => ['checkHeader']));
Route::post('webservice/send_factor_otp',
	array('uses' => 'WsController@two_factor_otp','middleware' => ['checkHeader']));
Route::post('webservice/varify_factor_otp',
	array('uses' => 'WsController@get_varify_factor_otp','middleware' => ['checkHeader']));
Route::post('webservice/resent_factor_otp',
	array('uses' => 'WsController@get_resent_factor_otp','middleware' => ['checkHeader']));
Route::post('webservice/guest_user_otp',
	array('uses' => 'WsController@show_guest_user_otp','middleware' => ['checkHeader']));
Route::post('webservice/varify_guest_user_otp',
	array('uses' => 'WsController@get_varify_guest_otp','middleware' => ['checkHeader']));
Route::post('webservice/guest_user_resentotp',
	array('uses' => 'WsController@show_guest_user_resentotp','middleware' => ['checkHeader']));
Route::post('webservice/get_token',
	array('uses' => 'WsController@getToken'));
Route::post('webservice/card_payment',
	array('uses' => 'WsController@payCardPayment'));
Route::post('webservice/cancel',
array('uses' => 'WsController@cancel')); */
/**************************************************************************************
********************************** webservise end ***********************************
**************************************************************************************/
//Route::post('/checkout/cash_payment_process','CheckoutController@get_cash_payment');
Route::post('/checkout/cart_payment_process','CheckoutController@get_cart_payment');
Route::get('/show_privacy_policy','GeneralController@webservice_privacy_policy');
Route::get('/show_privacy_policy_ar','GeneralController@webservice_privacy_policy_ar');
Route::get('/show_terms_condition','GeneralController@webservice_terms_condition');
Route::get('/show_terms_condition_ar','GeneralController@webservice_terms_condition_ar');
Route::get('/show_about','GeneralController@webservice_about');
Route::get('/show_about_ar','GeneralController@webservice_about_ar');
Route::get('/show_contact','GeneralController@webservice_contactus');
Route::get('/show_contact_ar','GeneralController@webservice_contactus_ar');

// content pages api
Route::post('/ws/ws_show_privacy_policy','WsController@ws_privacy_policy');
Route::post('/ws/ws_show_terms_condition','WsController@ws_terms_condition');
Route::post('/ws/ws_show_about','WsController@ws_about');
Route::post('/ws/ws_show_contact','WsController@ws_contactus');
// 

Route::post('/ws/area_manager_detail','WsController@area_manager_detail');

/******************NEW ROUES FOR RESTAPP******/
Route::get('/favorite_restaurant_list','UserController@show_favorite_restaurant');
Route::post('/get_current_locationdata','ListController@get_current_location');
Route::get('/favourite','ListController@make_favourite');
Route::post('/google_rest_insert','ListController@add_google_rest');
Route::post('/restaurantslisting_map','ListController@home_search_google');
Route::get('/ipn_notify','OrderuserController@get_ipn_date');
Route::post('/ipn_notify','OrderuserController@get_ipn_date_post');
Route::post('/checkout/paypal_payment_process','CheckoutController@get_paypal_payment');
Route::post('/send_coupon_request','GeneralController@get_coupon_request');
Route::post('/send_app_link','GeneralController@get_app_request');
Route::post('/send_restaurant_request','GeneralController@get_restaurant_request');
Route::post('/checkout/send_otp_verify','CheckoutController@send_otp_email');
Route::post('/checkout/check_submit_otp','CheckoutController@show_otp_verification');
Route::post('/checkout/check_delivery_address','CheckoutController@find_delivery_address');
Route::post('/checkout/delete_delivery_address','CheckoutController@delete_delivery_address');
Route::post('/cart/show_cart_data','CartController@show_cart_detail_onload');
Route::post('/check_registration_otp','CartController@show_cart_detail_onload');
Route::post('/user_registration_otp','Auth\AuthController@get_otp_login');
Route::get('/vendor_confirm','HomeController@vendor_order_confirm');
/*****************END**********************/
Route::get('/daily_report', array('uses' => 'DailyReportController@index'));
Route::get('/test_report', array('uses' => 'TestReportController@index'));

//Route::get('/admin/user_orderlist/{userid}','AdmintransportorderController@show_orderlist');
// Route::get('/en','GeneralController@main_page')->name('switchLan');
// Route::get('/ar','GeneralController@main_page')->name('switchLan');
Route::get('/{locale}',function($locale){
    Session::put('locale',$locale);
    return redirect()->back();
})->name('switchLan');  //add name to router