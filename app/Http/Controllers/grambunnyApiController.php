<?php

namespace App\Http\Controllers;

use App\Order;

use App\Rating;

use App\Vendor;

use App\User;

use App\UserRating;

use App\MerchantRating;

use App\Productservice;

use Illuminate\Support\Arr;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

class grambunnyApiController extends Controller
{

    public function merchantOrders(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "merchant_id" => "required|numeric",

            //"type" => "required|in:0,1",

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }

       $currentpage = $request->current_page;
       $limit = 10;

       $pegeno = ($currentpage-1)*$limit;

        $orders = Order::where("vendor_id", $request->merchant_id)->whereIn("status", array('0','1','3'))->orderBy('id', 'desc')->paginate($limit);

        if($currentpage > 1){

         $pegeno=Order::where("vendor_id", $request->merchant_id)->whereIn("status", array('0','1','3'))->orderBy('id', 'desc')->paginate($pegeno)->last()->id;

          $orders = Order::where("vendor_id", $request->merchant_id)->whereIn("status", array('0','1','3'))->where('id', '<', $pegeno)->orderBy('id', 'desc')->paginate($limit);

         }
    
        $total = Order::where("vendor_id", $request->merchant_id)->whereIn("status", array('0','1','3'))->count();

        $totalpage = ceil($total / $limit);//$total%$limit;

        $vendor_status =  Vendor::where('vendor_id', '=' ,$request->merchant_id)->value('vendor_status');

        foreach ($orders as $key => $value) {
            $search = null;
            $replace = '""';
            array_walk($value,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            );
        } 

        if ($orders->count() > 0) {

            return response()->json(["message" => "Product Order List", "ok" => 1,"totalpage"=>$totalpage, "data" => $orders,"vendor_status"=>$vendor_status]);

        } else {

            if($currentpage>$totalpage && $totalpage!=0){

             return response()->json(["message" => "No More Order!", "ok" => 0,"totalpage"=>$totalpage, "data" => [],"vendor_status"=>$vendor_status]);   

            }else{

            return response()->json(["message" => "No Product Orders Found.", "ok" => 0,"totalpage"=>$totalpage, "data" => [],"vendor_status"=>$vendor_status]);
            }

        }

    }


    public function orderPsList(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "order_id" => "required|numeric",

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }

        $orderItems = Productservice::select('product_service.*', 'orders_ps.ps_qty')->join('orders_ps', 'orders_ps.ps_id', '=', 'product_service.id')->where('orders_ps.order_id', $request->order_id)->where('product_service.status', 1)->latest()->paginate(10)->toArray();
       
        return response()->json(["ok" => 1, "message" => "Order All Product and services", "data" => $orderItems]);

    }

    public function MerchantItemsList(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "merchant_id" => "required|numeric",

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }

        $items = Productservice::where('vendor_id', $request->merchant_id)->latest()->paginate(10)->toArray();

        $items['ps_list'] = $items['data'];

        $filtered = Arr::except($items, ['data']);

        return response()->json(["ok" => 1, "message" => "All Product and services", "data" => $filtered]);

    }



    public function merchant_to_user_location(Request $request)
    {

        $order = Order::where('vendor_id', $request->merchant_id)

            ->where('id', $request->order_id)

            ->first();

        $merchant = Vendor::where('vendor_id', $request->merchant_id)->first();  

        if(empty($order)){

            return response()->json([

            "ok" => 0,

            "message" => "Merchant Id or order Id mismatch",

            "data" => [],

        ]);    

        }

        return response()->json([

            "ok" => 1,

            "message" => "Merchant to user location",

            "data" => [

                "merchant_location" => [

                    "latitude" => $merchant->lat,

                    "longitude" => $merchant->lng,

                ],

                "user_location" => [

                   
                    "latitude" => $order->latitude,

                    "longitude" => $order->longitude,
                ],

            ],

        ]);



    }



    public function OrdersHistory(Request $request)
    {

       $currentpage = $request->current_page;
       $limit = 10;

       $data = array();

       $pegeno = ($currentpage-1)*$limit;

       $order_history_list = Order::where('vendor_id', '=', $request->merchant_id)->where("product_type","!=","3")->whereIn('status', [2,4])->orderBy('id', 'desc')->paginate($limit);

        if($currentpage > 1){

         $pegeno=Order::where("vendor_id", $request->merchant_id)->where("product_type","!=","3")->whereIn("status", [2,4])->orderBy('id', 'desc')->paginate($pegeno)->last()->id;

         $order_history_list = Order::where('vendor_id', '=', $request->merchant_id)->where("product_type","!=","3")->whereIn('status', [2,4])->where('id', '<', $pegeno)->orderBy('id', 'desc')->paginate($limit);

         }

        foreach ($order_history_list as $key => $value) {
            $search = null;
            $replace = '""';
            array_walk($value,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            );
        } 

        $total = Order::where("vendor_id", $request->merchant_id)->where("product_type","!=","3")->whereIn("status", [2,4])->count();

        $totalpage = ceil($total / $limit);//$total%$limit;

         foreach ($order_history_list as $key => $value) {

            $data['order_history_list'][] = $value;

         }


        //$order_history_list['order_history_list'] = $data;

        //$filtered = Arr::except($order_history_list, ['data']);


        if ($order_history_list->count() > 0) {

           return response()->json(["message" => "Product order history list", "ok" => 1,"totalpage"=>$totalpage, "data" => $data]);

        }else{


        if($currentpage > $totalpage && $totalpage!=0){

        return response()->json(["message" => "No more order history", "ok" => 0,"totalpage"=>$totalpage, "data" => $data]);

        }else{ 


        return response()->json(["message" => "Not found order history", "ok" => 0,"totalpage"=>$totalpage, "data" => $data]);
      }

  }

    }



    public function TxnHistory(Request $request)
    {

       $currentpage = $request->current_page;
       $limit = 10;

       $pegeno = ($currentpage-1)*$limit;

       $order_history_list = Order::where('vendor_id', '=', $request->merchant_id)->where('status', '4')->orderBy('id', 'desc')->paginate($limit); 
       
        if($currentpage > 1){

         $pegeno=Order::where("vendor_id", $request->merchant_id)->where('status', '4')->orderBy('id', 'desc')->paginate($pegeno)->last()->id;

         $order_history_list = Order::where('vendor_id', '=', $request->merchant_id)->where('status', '4')->where('id', '<', $pegeno)->orderBy('id', 'desc')->paginate($limit); 

         }


        foreach ($order_history_list as $key => $value) {
          
            $search = null;
            $replace = '""';
            array_walk($value,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            );

        }


        $list = $order_history_list->each(function ($orders) {

            $product = $orders->product;

            //$orders->productImageURL = $product->ImageURL;

            //$orders->productName = $product->name;

            $orders->setAppends([]);

            return $orders;

        });

        $total = Order::where("vendor_id", $request->merchant_id)->where('status', '4')->count();

        $totalpage = ceil($total / $limit);//$total%$limit;

        $historyArr = $order_history_list->toArray();

        $historyArr['txn_history_list'] = $historyArr['data'];

        $filtered = Arr::except($historyArr, ['data']);


        if ($order_history_list->count() > 0) {

            return response()->json(["message" => "Product transaction history list", "ok" => 1,"totalpage"=>$totalpage,"data" => $filtered]);

        }else{


        if($currentpage>$totalpage && $totalpage!=0){

         return response()->json(["message" => "No, More Transection History!", "ok" => 0,"totalpage"=>$totalpage,"data" => $filtered]);

        }else{

         return response()->json(["message" => "Not found transaction history", "ok" => 0,"totalpage"=>$totalpage,"data" => $filtered]);
        }

    }



    }



    public function productRatings(Request $request)

    {

        $validator = Validator::make($request->all(), [

            "ps_id" => "required|numeric",

        ],["ps_id.required" => "Product/service ID is required."]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }



        $product=Productservice::find($request->ps_id);

        $product->setAppends(["categoryname","ImageURL","subcategoryname","ratings"]);

        return response()->json(["ok" => 1,"message" => "Product Rating and reviews","product" => $product]);

    }


    public function merchantRatings(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "merchant_id" => "required|numeric",

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }


        $vendorpn=Vendor::find($request->merchant_id)->only([

            "vendor_id","fullname","profileURL","description","avg_rating","rating_count","created_at" 

        ]);

    $vendor = array(
    "vendor_id" => $vendorpn['vendor_id'],
    "fullname" => isset($vendorpn['fullname'])?$vendorpn['fullname']:"",
    "profileURL" => isset($vendorpn['profileURL'])?$vendorpn['profileURL']:"",
    "description" => isset($vendorpn['description'])?$vendorpn['description']:"",
    "avg_rating" => isset($vendorpn['avg_rating'])?(string)$vendorpn['avg_rating']:"",
    "rating_count" => isset($vendorpn['rating_count'])?(string)$vendorpn['rating_count']:"",
    "created_at" => isset($vendorpn['created_at'])?$vendorpn['created_at']:""
    );

        $reviews=MerchantRating::where("merchant_id",$request->merchant_id)->where("status",1);

        $rating= $reviews->get();

        $sum= $rating->sum("rating");

            if ($rating->count() > 0) {

                $total=$sum/$rating->count();

                $totalRating= round($total);

            }

            else{

                $totalRating= 0;

            }


        // pagination code
        
       $currentpage = $request->current_page;

       $limit = 10;

       $pegeno = ($currentpage-1)*$limit;

       $merchantreview = MerchantRating::where('merchant_id', '=', $request->merchant_id)->where("status",1)->orderBy('id', 'desc')->paginate($limit); 

        if($currentpage > 1){

        $pegeno=MerchantRating::where("merchant_id", $request->merchant_id)->where("status",1)->orderBy('id', 'desc')->paginate($pegeno)->last()->id;

        $merchantreview = MerchantRating::where('merchant_id', '=', $request->merchant_id)->where("status",1)->where('id', '<', $pegeno)->orderBy('id', 'desc')->paginate($limit); 

         }

         foreach ($merchantreview as $key => $value) {

            
          
            $search = null;
            $replace = '';
            array_walk($value,
            function (&$v) use ($search, $replace){
                $v = str_replace($search, $replace, $v);    
                }                                                                     
            );

        } 

        $total = MerchantRating::where("merchant_id", $request->merchant_id)->where("status",1)->count();

        $totalpage = ceil($total / $limit);//$total%$limit;

        // close pagination code   
             
        $vendor["reviews"]=$merchantreview;

        $vendor["rating"]=$totalRating;

        if($currentpage>$totalpage){

         return response()->json(["ok" => 0,"message" => "No More Review!","totalpage"=>$totalpage,"merchant" => $vendor],200);  

        }else{

         return response()->json(["ok" => 1,"message" => "Merchant reviews and ratings.","totalpage"=>$totalpage,"merchant" => $vendor],200);
        }

    }



    public function rateUser(Request $request)
    {

        $validator = Validator::make($request->all(), [

            "merchant_id" => "required|numeric",
            "user_id" => "required|numeric",
            "rating" => "required|numeric|max:5|min:1",
            "review" => "required",
            "status" => "in:0,1",
            "order_id" => "required|numeric"

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

        }

        $user_id = $request->user_id;
        
        $user_review_exist = UserRating::where('user_id', $user_id)->where('merchant_id', $request->merchant_id)->count();

        if($user_review_exist == 0){

        $userRating=new UserRating();
        $userRating->merchant_id = $request->merchant_id;
        $userRating->user_id = $request->user_id;
        $userRating->status = 0;
        $userRating->order_id = $request->order_id;
        $userRating->rating = $request->rating;
        $userRating->review = $request->review;
        $userRating->save();

        return response()->json(["ok" => 1,"message" => "Thank you for your review and rating."]);

         }else{

         return response()->json(["ok" => 1,"message" => "Rating and review already given."]);

        }

    }

    public function chatuserlist(Request $request){

        $merchantid = $request->merchant_id; 

        $chat_user = DB::table('orders')

                ->where("vendor_id",$merchantid)

                ->whereNotIn("status",array('2', '4'))

                ->orderBy('orders.id', 'DESC')

                ->groupBy('orders.user_id')

                ->get(); 



        foreach ($chat_user as $key => $value) { 

            $customer = DB::table('users')->where("id",$value->user_id)->first(); 

            $customer_id = $customer->id;

            $name = $customer->name;

            $last_name = $customer->lname;

     

            if($customer->profile_image){ 

                $profile = url('/public/uploads/user/'.$customer->profile_image) ;

            }else{ $profile = url('/public/uploads/user/user.jpg') ; }



            $customers[] = array('customer_id' => $customer_id,'name' => $name,'last_name' => $last_name,'profile' => $profile);



        }                      



        if($chat_user->count() > 0) {                   



        $response['message'] = "Customer List";

        $response['ok'] = 1;

        $response['data'] = array('customer_list'=>$customers);



        }else{



        $response['message'] = "Customer List Not Found";

        $response['ok'] = 0;

        $response['data'] = array('customer_list'=>'');



     }



        return $response;



    }





    public function chathistory(Request $request){



        $merchantid = $request->merchant_id;

        $customerid = $request->customer_id;  



        $getchat = DB::table('chat_system')       

            ->where('merchant_id', '=' ,$merchantid)

            ->where('customer_id', '=' ,$customerid)

            ->orderBy('chat_id', 'asc')

            ->get();



        if($getchat->count() > 0) {                   



        $response['message'] = "Chat List";

        $response['ok'] = 1;

        $response['data'] = array('customer_chat'=>$getchat);



        }else{



        $response['message'] = "Chat List Not Found";

        $response['ok'] = 0;

        $response['data'] = array('customer_chat'=>'');



     }



        return $response;



    }



    public function chatsystem(Request $request){



        $merchantid = $request->merchant_id;

        $customerid = $request->customer_id;

        $message = $request->message;   



        $date = date('Y-m-d H:i:s');



        DB::table('chat_system')->insert([

        'merchant_id' => $merchantid,

        'customer_id' => $customerid,

        'message' => $message,

        'chat_type' => 'receiver',

        'read_status' => 0,

        'created_at' => $date    



        ]);



        $getchat = DB::table('chat_system')       

                    ->where('merchant_id', '=' ,$merchantid)

                    ->where('customer_id', '=' ,$customerid)

                    ->orderBy('chat_id', 'asc')

                    ->get();



        if($getchat->count() > 0) {                   



        $response['message'] = "Chat List";

        $response['ok'] = 1;

        $response['data'] = array('customer_chat'=>$getchat);



        }else{



        $response['message'] = "Chat List Not Found";

        $response['ok'] = 0;

        $response['data'] = array('customer_chat'=>'');



     }



        return $response;



    } 


    public function merchantSuspendOrders(Request $request)

    {

        $validator = Validator::make($request->all(), [

            "merchant_id" => "required|numeric"

            //"type" => "required|in:0,1",

        ]);

        if ($validator->fails()) {

            return response()->json(["ok" => "0", "message" => $validator->messages()], 200);

        }



        // $products = Order::where('vendor_id', $request->merchant_id)->groupBy("ps_id")->pluck("ps_id")->toArray();

        // $filterProducts = Productservice::where("type", $request->type)->whereIn("id", $products)->pluck("id")->toArray();

        // $orders = Order::where('vendor_id', $request->merchant_id)->whereIn("ps_id", $filterProducts)->paginate(10);



        $orders = Order::where("vendor_id", $request->merchant_id)->where("status", 8)->orderBy("id","DESC")->paginate(10);

        

        $vendor_status =  Vendor::where('vendor_id', '=' ,$request->merchant_id)->value('vendor_status');



        if ($orders->count() > 0) {

            return response()->json(["message" => "Product/Service Order List", "ok" => "1", "data" => $orders,"vendor_status"=>$vendor_status]);

        } else {

            return response()->json(["message" => "No Product/Service Orders Found.", "ok" => "0", "data" => [],"vendor_status"=>$vendor_status]);

        }

    }
}

