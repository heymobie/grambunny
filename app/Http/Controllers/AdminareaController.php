<?php
namespace App\Http\Controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Route;
class AdminareaController extends Controller
{
	public function __construct(){
		$this->middleware('admin');
	}
	/*areas list*/
	public function area_List()
	{
		DB::connection()->enableQueryLog();
		$area_list = DB::table('area_management')
		->select('*')
		->orderBy('area_id', 'desc')
		->get();
		$data_onview = array('area_list' =>$area_list);
		return View('admin.area_list')->with($data_onview);
	}
	/*Add area form*/
	public function area_form()
	{
		return View('admin.area_form');
	}
	/*Update area details*/
	public function UpdateAreaDetail(Request $request)
	{
		$postData = Request::all();
		$area_id = $postData['area_id'];
		$AreaData = array(
			'area_name' => trim($postData['address']),
			'place_id' => trim($postData['place_ID']),
			'area_lat' => trim($postData['r_lat']),
			'area_long' => trim($postData['r_lang']),
			'area_city' => trim($postData['city']),
			'area_state' => trim($postData['state']),
			'area_country' =>trim($postData['country']),
			'route' => trim($postData['route']),
			'zipcode' => trim($postData['zipcode']),
			'update_at'=>date('y-m-d h:i:s')
		);
		$affected = DB::table('area_management')
		->where('area_id', $area_id)
		->update($AreaData);
		if($affected){
			Session::flash('message', 'Area details updated sucessfully!');
			return redirect()->to('/admin/area_list');
		}
	}
	/*Delete area*/
	public function area_delete($id)
	{
		DB::table('area_management')->where('area_id', '=', $id)->delete();
		Session::flash('message', 'Area deleted sucessfully!');
		return redirect()->to('/admin/area_list');
	}
	/*Edit area*/
	public function EditareaForm()
	{
		if(Route::current()->getParameter('id'))
		{
			$area_id = Route::current()->getParameter('id');
			$area_detail  = DB::table('area_management')->where('area_id', '=' ,$area_id)->get();
			$data_onview = array(
				'area_detail' =>$area_detail,
				'area_id'=>$area_id,
			);
			return View('admin.area_edit_form')->with($data_onview);
		}
	}
	/*add new area details*/
	public function addarea(Request $request)
	{
		$postData = Request::all();

		$AreaData = array(
			'area_name' => trim($postData['address']),
			'place_id' => trim($postData['place_ID']),
			'area_lat' => trim($postData['r_lat']),
			'area_long' => trim($postData['r_lang']),
			'area_city' => trim($postData['city']),
			'area_state' => trim($postData['state']),
			'area_country' =>trim($postData['country']),
			'route' => trim($postData['route']),
			'zipcode' => trim($postData['zipcode']),
		);

		if($postData['zipcode']){

		$affected = DB::table('area_management')->insert($AreaData);

		if($affected){
			Session::flash('message', 'Area added sucessfully!');
			return redirect()->to('/admin/area_list');
		}

	    }else{

         	Session::flash('message', 'Please enter zip code!');
			return redirect()->to('/admin/area_list');

	    }

	
	}
	/*View area details*/
	public function ViewArea()
	{
		$area_id =	Route::current()->getParameter('id');
		$Areadetail  = DB::table('area_management')->where('area_id', '=' ,$area_id)->get();
		$data_onview = array('Areadetail' =>$Areadetail);
		return View('admin.area_view')->with($data_onview);
	}

	/* Coupon code start */ 

	public function coupon_code()
	{
		DB::connection()->enableQueryLog();
		$coupon_list = DB::table('coupon_code')
		->select('*')
		->orderBy('id', 'desc')
		->get();
		$data_onview = array('coupon_list' =>$coupon_list);
		return View('admin.couponcode_list')->with($data_onview);
	}

	public function coupon_form()
	{
		return View('admin.couponcode_form');
	}

    public function add_coupon_code(Request $request)
	{
		$postData = Request::all();

		$checkdata = DB::table('coupon_code')->where('coupon', '=', trim($postData['coupon']))->get();

        if(count($checkdata)>0)
        {   	
           Session::flash('message', 'Coupon code already exists!');
			return redirect()->to('/admin/coupon_code');

        }else{	

		$CouponData = array(
			'coupon' => trim($postData['coupon']),
			'amount' => trim($postData['amount']),
			'apply_min_amount' => trim($postData['mamount']),
			'status' => trim($postData['coupon_status']),
			'valid_till' => '',
			'per_user' => '',
		);
		$affected = DB::table('coupon_code')->insert($CouponData);
		if($affected){
			Session::flash('message', 'Coupon added sucessfully!');
			return redirect()->to('/admin/coupon_code');
		}
	}	


	}

   public function coupon_delete($id)
	{
		DB::table('coupon_code')->where('id', '=', $id)->delete();
		Session::flash('message', 'Coupon deleted sucessfully!');
		return redirect()->to('/admin/coupon_code');
	}

	public function coupon_edit_form()
	{
		if(Route::current()->getParameter('id'))
		{
			$id = Route::current()->getParameter('id');
			$coupon_detail  = DB::table('coupon_code')->where('id', '=' ,$id)->get();
			$data_onview = array(
				'coupon_detail' =>$coupon_detail,
				'coupon_id'=>$id,
			);
			return View('admin.coupon_edit_form')->with($data_onview);
		}
	}

   public function update_coupon_detail(Request $request)
	{
		$postData = Request::all();
		$coupon_id = $postData['coupon_id'];

		$CouponData = array(
			'coupon' => trim($postData['coupon']),
			'amount' => trim($postData['amount']),
			'apply_min_amount' => trim($postData['mamount']),
			'status' => trim($postData['coupon_status']),
			'valid_till' => '',
			'per_user' => '',
		);

		$affected = DB::table('coupon_code')
		->where('id', $coupon_id)
		->update($CouponData);
		if($affected){
			Session::flash('message', 'Coupon details updated sucessfully!');
			return redirect()->to('/admin/coupon_code');
		}
	}

	/* Coupon code end */

}