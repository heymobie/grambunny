<?php

namespace App\Http\Controllers;

use Stripe;

use App\Coupon_code;

use App\Coupon_code_apply;

use App\Delivery_address;

use App\Device_token;

use App\Favourite_food;

use App\Favourite_restaurant;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Notification_list;

use App\Order;

use App\Order_payment;

use App\Productservice;

use App\Orderdetail;

use App\Cart;

use App\Proservicescate;

use App\Proservices_subcate;

use App\Restaurant;

use App\Review;

use App\Temp_cart;

use App\User;

use App\User_otpdetail;

use App\Vendor;

use App\Rating;

use App\MerchantRating;

use App\UserRating;

use App\Advertisement;

use Auth;

use Braintree_Transaction;

use Carbon\Carbon;

//use Intervention\Image\Facades\Image as Image;

use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Auth\Authenticatable;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

//use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use PHPMailer\PHPMailer;

use Route;

use Session;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Arr;

use Illuminate\Support\Str;

use Mail;


class NewApiController extends Controller
{
	public function driver_list(Request $request){

		$response= array();
		$banner_image = [];
		$latitude = $request->latitude;

		$longitude = $request->longitude;

		$keyword = $request->keyword;

		$searchby = $request->searchby;

		$iconurl = url('/').'/public/uploads/';

		$url = url('/').'/public/uploads/vendor/profile/';

		$product_image = url('/').'/public/uploads/product/';

		$advertisement_img_url = url('/').'/public/uploads/advertisement/';

		$data['driver_list'] = array();

		$vendorlist = array();

		$product_list = array();

		$distance = 50;

		$vendors = array();

		$vendorid = array();

		$vendoridp =array();

		$categoryid =array();


		if(!empty($request->keyword)){
			$keywords = $request->keyword;


			$apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
            // Get JSON results from this request
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($keywords).'&sensor=false&key='.$apiKey);
            $geo = json_decode($geo, true); // Convert the JSON to an array

            if (isset($geo['status']) && ($geo['status'] == 'OK')) {
            $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
            $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
            }


            $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));



            $vendorsr = Arr::pluck($results,"vendor_id");
            $vendorlist = array();

			 foreach ($results as $key => $value) {

            	$vendorlogin = Vendor::where("vendor_id",$value->vendor_id)->first();

	            if(($vendorlogin->login_status==1) && ($value->distance<=$vendorlogin->service_radius)){

	             $vendors[]= $value->vendor_id;
	             $distance1[]= $value->distance;

	            }

          	}

          	//print_r($distance1);

             $vendorlist1=DB::table('vendor')->whereIn("vendor_id",$vendors)->where('login_status','=',1)->groupBy('vendor_id')->get();

           //print_r($results);

            $i=0;
            foreach ($vendorlist1 as $key=>$value) {

               if($value->map_icon){

                 $mapicon = $iconurl.$value->map_icon;

                }else{

                  $mapicon = $iconurl.'map.png';

                }
               if($value->profile_img1){

                 $profile_img1 = $url.$value->profile_img1;

                }else{

                  $profile_img1 = $iconurl.'user.jpg';

                }

                $search = null;
		        $replace = '""';
		        array_walk($value,
		        function (&$v) use ($search, $replace){
		            $v = str_replace($search, $replace, $v);
		            }
		        );

            	$vendorlist[] =  array("vendor_id"=> $value->vendor_id,
                "unique_id"=> $value->unique_id,
                "name"=> $value->name,
                "username"=> $value->username,
                "last_name"=> $value->last_name,
                "business_name"=> $value->business_name,
                "email"=> $value->email,
                "mailing_address"=> $value->mailing_address,
                "address"=> $value->address,
                "avg_rating"=> $value->avg_rating,
                "rating_count"=> $value->rating_count,
                "lat"=> $value->lat,
                "lng"=> $value->lng,
                "address1"=> $value->address1,
                "suburb"=> $value->suburb,
                "state"=> $value->state,
                "city"=> $value->city,
                "zipcode"=> $value->zipcode,
                "mob_no"=> $value->mob_no,
                "vendor_status"=> $value->vendor_status,
                "login_status"=> $value->login_status,
                "vendor_type"=> $value->vendor_type,
                "wallet_amount"=> $value->wallet_amount,
                "deviceid"=> $value->deviceid,
                "devicetype"=> $value->devicetype,
                "password"=> $value->password,
                "market_area"=> $value->market_area,
                "service_radius"=> $value->service_radius,
                "driver_license"=> $value->driver_license,
                "license_expiry"=> $value->license_expiry,
                "license_front"=> $value->license_front,
                "license_back"=> $value->license_back,
                "ssn"=> $value->ssn,
                "dob"=> $value->dob,
				"profile_img1"=> $profile_img1,
				"profile_img2"=> !empty($value->profile_img2)?$url.$value->profile_img2:"",
				"profile_img3"=> !empty($value->profile_img3)?$url.$value->profile_img3:"",
				"profile_img4"=> !empty($value->profile_img4)?$url.$value->profile_img4:"",
                "type"=> $value->type,
                "category_id"=> $value->category_id,
                "sub_category_id"=> $value->sub_category_id,
                "service"=> $value->service,
                "permit_type"=> $value->permit_type,
                "permit_number"=> $value->permit_number,
                "permit_expiry"=> $value->permit_expiry,
                "description"=> $value->description,
                "remember_token"=> $value->remember_token,
                "otp"=> $value->otp,
                "forgetpass_request"=> $value->forgetpass_request,
                "forgetpass_request_status"=> $value->forgetpass_request_status,
                "plan_id"=> $value->plan_id,
                "plan_purchased"=> $value->plan_purchased,
                "plan_expiry"=> $value->plan_expiry,
                "txn_id"=> $value->txn_id,
                "created_at"=> $value->created_at,
                "updated_at"=> $value->updated_at,
                "stripe_id"=> $value->stripe_id,
                "make"=> $value->make,
                "model"=> $value->model,
                "color"=> $value->color,
                "year"=> $value->year,
                "license_plate"=> $value->license_plate,
                "views"=> $value->views,
                "sales_tax"=> $value->sales_tax,
                "excise_tax"=> $value->excise_tax,
                "city_tax"=> $value->city_tax,
                "commission_rate"=> $value->commission_rate,
                "delivery_fee"=> $value->delivery_fee,
                "distance"=> number_format($distance1[$i],1),
                "map_icon"=> $mapicon,
                "type_of_merchant"=> $value->type_of_merchant
            );
            $i++;
           }

         // print_r($results);




			foreach ($vendorlist1 as  $value) {
				$vendorid[]=$value->vendor_id;
			}


			if($searchby){
				$category=DB::table('product_service_category')->where("category",'LIKE','%'.$searchby.'%')->get();


				if(count($category)>0){
					foreach ($category as  $cat) {
						$categoryid[]=$cat->id;
					}
					$product_list  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.name as vendorname','vendor.last_name as vendorlastname','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)->whereIn('product_service.category_id', $categoryid)
					->where("status",1)
					->where("vendor.login_status",1)
					->orderByDesc('id')
		        	->limit(10)
					->get();
				}
				else{
					//print_r($vendorid);
				$product_list1  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
				->where("status",1)
				->where("vendor.login_status",1)
				->Where('brands','LIKE','%'.$searchby.'%')
				->orderByDesc('id')
	        	->limit(10)
				->get();



				$product_list2  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					->Where('product_service.name','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();

					$product_list3  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					 ->Where('types','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();

					$product_list4  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					 ->Where('product_code','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();



					if(count($product_list1)!=0){

						$product_list = $product_list1;
					}
					elseif(count($product_list2)!=0){

						$product_list = $product_list2;
					}
					elseif(count($product_list3)!=0){

						$product_list = $product_list3;
					}
					elseif(count($product_list4)!=0){

						$product_list = $product_list4;
					}



				//print_r($product_list);

				}
			}else{

				$product_list  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
				->where("status",1)
				->where("vendor.login_status",1)
				->where("quantity",'>',0)
				->orderByDesc('id')
	        	->limit(10)
				->get();
			}
		}

		elseif(!empty($latitude) && !empty($longitude)){

		$vendors = array();
		$results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor Where vendor.login_status=1 HAVING distance < ' . $distance . ' ORDER BY distance'));



		// print_r($results);

		// die;
      if($results){

      $vendors= Arr::pluck($results,"vendor_id");

      $vendorsr = Arr::pluck($results,"vendor_id");

     		foreach ($results as $key => $value) {

            	$vendorlogin = Vendor::where("vendor_id",$value->vendor_id)->first();

	            if(($vendorlogin->login_status==1) && ($value->distance<=$vendorlogin->service_radius)){

	             $vendors[]= $value->vendor_id;
	             $distance1[]= $value->distance;

	            }

          	}


       }

       if($vendors){
       		$limit = 10;
       		$currentpage = $request->current_page;
       		$pegeno = ($currentpage-1)*$limit;
       		$vendorlist1=DB::table('vendor')->where("login_status",'=',1)->whereIn("vendor_id",$vendors)->groupBy('vendor_id')->paginate($limit);

       		$vendorlist = array();

       		if($currentpage > 1){

			 $pegeno=DB::table('vendor')->where("login_status",'=',1)->whereIn("vendor_id",$vendors)->groupBy('vendor_id')->paginate($pegeno)->last()->id;

			 $notification_detail = DB::table('vendor')->where("login_status",'=',1)->whereIn("vendor_id",$vendors)->where('vendor_id', '<', $pegeno)->groupBy('vendor_id')->paginate($limit);

			 }
			 $total = DB::table('vendor')->where("login_status",'=',1)->whereIn("vendor_id",$vendors)->count();

			 $totalpage = ceil($total / $limit);//$total%$limit;



            $i=0;
            foreach ($vendorlist1 as $key=>$value) {


                if($value->map_icon){

                 $mapicon = $iconurl.$value->map_icon;

                }else{

                  $mapicon = $iconurl.'map.png';

                }
				if($value->profile_img1){

					$profile_img1 = $url.$value->profile_img1;

				   }else{

					 $profile_img1 = $iconurl.'user.jpg';

				   }

            	$search = null;
		        $replace = '""';
		        array_walk($value,
		        function (&$v) use ($search, $replace){
		            $v = str_replace($search, $replace, $v);
		            }
		        );

            	$vendorlist[] =  array("vendor_id"=> $value->vendor_id,
                "unique_id"=> $value->unique_id,
                "name"=> $value->name,
                "username"=> $value->username,
                "last_name"=> $value->last_name,
                "business_name"=> $value->business_name,
                "email"=> $value->email,
                "mailing_address"=> $value->mailing_address,
                "address"=> $value->address,
                "avg_rating"=> $value->avg_rating,
                "rating_count"=> $value->rating_count,
                "lat"=> $value->lat,
                "lng"=> $value->lng,
                "address1"=> $value->address1,
                "suburb"=> $value->suburb,
                "state"=> $value->state,
                "city"=> $value->city,
                "zipcode"=> $value->zipcode,
                "mob_no"=> $value->mob_no,
                "vendor_status"=> $value->vendor_status,
                "login_status"=> $value->login_status,
                "vendor_type"=> $value->vendor_type,
                "wallet_amount"=> $value->wallet_amount,
                "deviceid"=> $value->deviceid,
                "devicetype"=> $value->devicetype,
                "password"=> $value->password,
                "market_area"=> $value->market_area,
                "service_radius"=> $value->service_radius,
                "driver_license"=> $value->driver_license,
                "license_expiry"=> $value->license_expiry,
                "license_front"=> $value->license_front,
                "license_back"=> $value->license_back,
                "ssn"=> $value->ssn,
                "dob"=> $value->dob,
				"profile_img1"=> $profile_img1,
				"profile_img2"=> !empty($value->profile_img2)?$url.$value->profile_img2:"",
				"profile_img3"=> !empty($value->profile_img3)?$url.$value->profile_img3:"",
				"profile_img4"=> !empty($value->profile_img4)?$url.$value->profile_img4:"",
                "type"=> $value->type,
                "category_id"=> $value->category_id,
                "sub_category_id"=> $value->sub_category_id,
                "service"=> $value->service,
                "permit_type"=> $value->permit_type,
                "permit_number"=> $value->permit_number,
                "permit_expiry"=> $value->permit_expiry,
                "description"=> $value->description,
                "remember_token"=> $value->remember_token,
                "otp"=> $value->otp,
                "forgetpass_request"=> $value->forgetpass_request,
                "forgetpass_request_status"=> $value->forgetpass_request_status,
                "plan_id"=> $value->plan_id,
                "plan_purchased"=> $value->plan_purchased,
                "plan_expiry"=> $value->plan_expiry,
                "txn_id"=> $value->txn_id,
                "created_at"=> $value->created_at,
                "updated_at"=> $value->updated_at,
                "stripe_id"=> $value->stripe_id,
                "make"=> $value->make,
                "model"=> $value->model,
                "color"=> $value->color,
                "year"=> $value->year,
                "license_plate"=> $value->license_plate,
                "views"=> $value->views,
                "sales_tax"=> $value->sales_tax,
                "excise_tax"=> $value->excise_tax,
                "city_tax"=> $value->city_tax,
                "commission_rate"=> $value->commission_rate,
                "delivery_fee"=> $value->delivery_fee,
                "distance"=> number_format($distance1[$i],1),
                "map_icon"=> $mapicon,
                "type_of_merchant"=> $value->type_of_merchant
            );
            $i++;
           }


       		// $vendorlist=DB::table('vendor')->where('login_status',1)->where(
         //  	function($query) use ($keywords) {
         //     	return $query->where("zipcode",'like','%'.$keywords.'%')->orWhere("city",'like','%'.$keywords.'%');
         //    })->get();

       		//$vendorlist=DB::table('vendor')->whereIn("vendor_id",$vendors)->groupBy('vendor_id')->get();

       		if($searchby){
				$category=DB::table('product_service_category')->where("category",'LIKE','%'.$searchby.'%')->get();

				$vendorp=DB::table('vendor')->where("name",$searchby)->get();

				//print_r($category);
				if(count($category)!=0){
					//echo "hgjlgjh";
					foreach ($category as  $cat) {
						$categoryid[]=$cat->id;
					}
					$product_list  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendors)->whereIn('product_service.category_id', $categoryid)
					->where("status",1)
					->where("vendor.login_status",1)
					->orderByDesc('id')
		        	->limit(10);
					

					



					$search1 = null;
			        $replace1 = '""';
			        array_walk($product_list,
			        function (&$v1) use ($search1, $replace1){
			            $v1 = str_replace($search1, $replace1, $v1);
			            }
			        );


				}
				else{



					$product_list1  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
				->where("status",1)
				->where("vendor.login_status",1)
				->Where('brands','LIKE','%'.$searchby.'%')
				->orderByDesc('id')
	        	->limit(10)
				->get();



				$product_list2  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					->Where('product_service.name','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();

					$product_list3  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					 ->Where('types','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();

					$product_list4  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendorid)
					->where("status",1)
					->where("vendor.login_status",1)
					 ->Where('product_code','LIKE','%'.$searchby.'%')
					->orderByDesc('id')
		        	->limit(10)
					->get();


					if(count($product_list1)!=0){

						$product_list = $product_list1;
					}
					elseif(count($product_list2)!=0){

						$product_list = $product_list2;
					}
					elseif(count($product_list3)!=0){

						$product_list = $product_list3;
					}
					elseif(count($product_list4)!=0){

						$product_list = $product_list4;
					}

					$search = null;
			        $replace = '""';
			        array_walk($product_list,
			        function (&$v) use ($search, $replace){
			            $v = str_replace($search, $replace, $v);
			            }
			        );

				}
			}else{

			$product_list  = $query = Productservice::join('vendor', 'product_service.vendor_id','=','vendor.vendor_id')->select('*','vendor.username as username','vendor.vendor_id as vid','vendor.last_name as last_name','product_service.vendor_id as vendor_id','product_service.name as name','vendor.name as vendorname','vendor.last_name as vendorlastname','product_service.category_id as category_id','product_service.sub_category_id')->whereIn('product_service.vendor_id', $vendors)
			->where("status",1)
			->where("vendor.login_status",1)
			->where("quantity",'>',0)
			->orderByDesc('id')
        	->limit(10)
			->get();


			}


   			}

   		}

   		foreach ($product_list as $key => $value) {

          	    $search = null;
		        $replace = '""';
		        array_walk($value,
		        function (&$v) use ($search, $replace){
		            $v = str_replace($search, $replace, $v);
		            }
		        );

	        }


   		$advertisement1=Advertisement::where("status",1)->where("id",1)->select('image')->first();

  		$advertisement2=Advertisement::where("status",1)->where("id",2)->select('image')->first();
  		// $advertisement2=Advertisement::where("status",1)->where("id",2)->first();
  		$advertis1= array('image', 'url');
  		$advertise1=array($advertisement1->image,$advertisement_img_url);

  		$advertis2= array('image', 'url');
  		$advertise2=array($advertisement2->image,$advertisement_img_url);

  		$adver1[] = array_combine($advertis1,$advertise1);

  		$adve2[] = array_combine($advertis2,$advertise2);

  		$advertisement = array_merge($adver1,$adve2);

  		if(!empty($advertisement)){
  			$advertisement = $advertisement;

  			foreach ($advertisement as $key => $value) {

          	    $search = null;
		        $replace = '""';
		        array_walk($value,
		        function (&$v) use ($search, $replace){
		            $v = str_replace($search, $replace, $v);
		            }
		        );

	        }

  		}else{
  			$advertisement =array();
  		}

  		$bannerimg = DB::table('admin_setting')->where("id",'=',1)->value('banner_image');


  		$bannerimage = $advertisement_img_url.$bannerimg;


		if(count($vendorlist)>0){
			$response['message'] = "Driver List";
			$response['img_url'] = $url;
			$response['product_url'] = $product_image;
			$response['totalpage'] = $totalpage;
			// $response['banner_image'] = $bannerimage;
			$banner_image[] = $bannerimage;
			$response['status'] = 1;
			if($currentpage>$totalpage){
				$response['data'] = array('message'=>'No More Driver List!','advertisement'=>$advertisement,'banner_image'=>$banner_image);
			}else{
				$response['data'] = array('driver_list'=>$vendorlist,'advertisement'=>$advertisement,'banner_image'=>$banner_image);
			}
			
		}else{

			$response['message'] = "No data found";
			$response['status'] = 0;
			$response['totalpage'] = $totalpage;
			$response['data'] = array('driver_detail'=>$vendorlist,'advertisement'=>$advertisement,'banner_image'=>$banner_image);
		}

		echo json_encode($response);

	}
}