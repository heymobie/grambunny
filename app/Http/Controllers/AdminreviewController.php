<?php

namespace App\Http\Controllers;

Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use Route;
use File;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Restaurant;
use App\Menu;
use App\Menu_category;
use App\Menu_category_item;
use App\Menu_category_addon;
use App\Service;
use App\Bankdetail;
use App\Promotion; 
use App\Rating;
use App\MerchantRating;
use App\UserRating;
use App\Productservice;
use App\Vendor;
use App\User;

class AdminreviewController extends Controller

{



    public function __construct(){

    	$this->middleware('admin');

    }

	

	

	public function show_reviewlist()

	{
						
		$review_detail  =  DB::table('ratings')		
						->leftJoin('users', 'ratings.user_id', '=', 'users.id')
						->leftJoin('product_service', 'product_service.id', '=', 'ratings.ps_id')
						//->where('review.re_orderid', '>' ,'0')
						->select('users.*', 'product_service.name as ps_name', 'ratings.status as rat_status'
							, 'ratings.rating','ratings.review as rat_comment', 'ratings.id as re_id'
							)
						->orderBy('ratings.id', 'asc')
						->get();						
		

		$data_onview = array('review_detail'=>$review_detail); 

		return View('admin.review_list')->with($data_onview);	

	}

	public function show_reviewmerchantlist()

	{
						
		$review_detail  =  DB::table('merchant_ratings')		
						->leftJoin('users', 'merchant_ratings.user_id', '=', 'users.id')
						->leftJoin('vendor', 'vendor.vendor_id', '=', 'merchant_ratings.merchant_id')
						//->where('review.re_orderid', '>' ,'0')
						->select('users.*', 'vendor.name as v_fname', 'vendor.last_name as v_lname', 'merchant_ratings.status as rat_status'
							, 'merchant_ratings.rating','merchant_ratings.review as rat_comment', 'merchant_ratings.id as re_id'
							)
						->orderBy('merchant_ratings.id', 'asc')
						->get();						
		

		$data_onview = array('review_detail'=>$review_detail); 

		return View('admin.review_merchant_list')->with($data_onview);	

	}

	public function show_review_merchant_action(Request $reuset)
	{

		$revew_status = Input::get('revew_status');	
		$id = Input::get('re_id');

		DB::table('merchant_ratings')
            ->where('id', Input::get('re_id'))
            ->update(['status' => $revew_status,
					  'updated_at'=>  date('Y-m-d H:i:s')
					 ]);

	        $merchant_id = DB::table('merchant_ratings')->where('id', '=', $id)->value('merchant_id');
		    $no_of_times_rated = MerchantRating::where('merchant_id', $merchant_id)->where('status', 1)->count();
	        $rating_sum = MerchantRating::where('merchant_id', $merchant_id)->where('status', 1)->sum('rating');  

	       if(!empty($no_of_times_rated)){

	        $avg_rating = round($rating_sum/$no_of_times_rated);

	        }else{
	          
	          $avg_rating = 0;

	        }

	     Vendor::where('vendor_id', $merchant_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );


		Session::flash('message', 'Review Updated Successfully!');

		return redirect()->to('/admin/review-merchant-list');
	}


	function review_merchant_delete($id)
	{

        $merchant_id = DB::table('merchant_ratings')->where('id', '=', $id)->value('merchant_id');

		DB::table('merchant_ratings')->where('id', '=', $id)->delete();
				
		$no_of_times_rated = MerchantRating::where('merchant_id', $merchant_id)->where('status', 1)->count();
	    $rating_sum = MerchantRating::where('merchant_id', $merchant_id)->where('status', 1)->sum('rating');  

	    if(!empty($no_of_times_rated)){

	        $avg_rating = round($rating_sum/$no_of_times_rated);

	    }else{
	          
	        $avg_rating = 0;

	    }

	    Vendor::where('vendor_id', $merchant_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );

		Session::flash('message', 'Information Deleted Successfully!');
		return Redirect('/admin/review-merchant-list');
	}

/* product review */

	public function show_review_action(Request $reuset)

	{
		$revew_status = Input::get('revew_status');	
		$id = Input::get('re_id');

		DB::table('ratings')->where('id', $id)->update(['status' => $revew_status,'updated_at'=>  date('Y-m-d H:i:s')]);

	        $ps_id = DB::table('ratings')->where('id', '=', $id)->value('ps_id');
		
		    $no_of_times_rated = Rating::where('ps_id', $ps_id)->where('status', 1)->count();

	        $rating_sum = Rating::where('ps_id', $ps_id)->where('status', 1)->sum('rating');

	        if(!empty($no_of_times_rated)){

	        $avg_rating = round($rating_sum/$no_of_times_rated);

	        }else{
	          
	          $avg_rating = 0;

	        }

            Productservice::where('id', $ps_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );

		    Session::flash('message', 'Review Updated Successfully!');

		    return redirect()->to('/admin/review_list');
	}

	function review_delete($id)
	{

		$ps_id = DB::table('ratings')->where('id', '=', $id)->value('ps_id');

		DB::table('ratings')->where('id', '=', $id)->delete();

	    $no_of_times_rated = Rating::where('ps_id', $ps_id)->where('status', 1)->count();

	    $rating_sum = Rating::where('ps_id', $ps_id)->where('status', 1)->sum('rating');

	    if(!empty($no_of_times_rated)){

	         $avg_rating = round($rating_sum/$no_of_times_rated);

	        }else{
	          
	          $avg_rating = 0;

	        }


	    Productservice::where('id', $ps_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );

        
		Session::flash('message', 'Information Deleted Successfully!');
		return Redirect('/admin/review_list');
	}



/* Customer review list */ 

	public function show_review_customer_list()

	{
						
		$review_detail  =  DB::table('user_ratings')		
						->orderBy('user_ratings.id', 'asc')
						->get();										
		

		$data_onview = array('review_detail'=>$review_detail); 

		return View('admin.review_customer_list')->with($data_onview);	

	}

	public function show_review_customer_action(Request $reuset)
	{

		$revew_status = Input::get('revew_status');	
		$id = Input::get('re_id');

		DB::table('user_ratings')
            ->where('id', Input::get('re_id'))
            ->update(['status' => $revew_status,
					  'updated_at'=>  date('Y-m-d H:i:s')
					 ]);

       	$user_id = DB::table('user_ratings')->where('id', '=', $id)->value('user_id');
		$no_of_times_rated = UserRating::where('user_id', $user_id)->where('status', 1)->count();
	    $rating_sum = UserRating::where('user_id', $user_id)->where('status', 1)->sum('rating'); 

	       if(!empty($no_of_times_rated)){

	        $avg_rating = round($rating_sum/$no_of_times_rated);

	        }else{
	          
	          $avg_rating = 0;

	        }


	    User::where('id', $user_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );


		Session::flash('message', 'Review Updated Successfully!');

		return redirect()->to('/admin/review-customer-list');
	}


	function review_customer_delete($id)
	{
		
       	$user_id = DB::table('user_ratings')->where('id', '=', $id)->value('user_id');

       	DB::table('user_ratings')->where('id', '=', $id)->delete();

		$no_of_times_rated = UserRating::where('user_id', $user_id)->where('status', 1)->count();
	    $rating_sum = UserRating::where('user_id', $user_id)->where('status', 1)->sum('rating'); 

	       if(!empty($no_of_times_rated)){

	        $avg_rating = round($rating_sum/$no_of_times_rated);

	        }else{
	          
	          $avg_rating = 0;

	        }

	     User::where('id', $user_id)->update( [ 'avg_rating' => $avg_rating, 'rating_count' => $no_of_times_rated ] );

		Session::flash('message', 'Information Deleted Successfully!');
		return Redirect('/admin/review-customer-list');
	}

	/* end of customer review */


	public function ajax_search_list()

	{

		//echo '<pre>';

		//print_r($_POST);

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = trim(Input::get('order_status'));

		$user_id = Input::get('user_id');


		$review_detail  = DB::table('review');		

		$review_detail  = $review_detail->leftJoin('restaurant', 'review.re_restid', '=', 'restaurant.rest_id');

		$review_detail  = $review_detail->leftJoin('users', 'review.re_userid', '=', 'users.id');

		$review_detail  = $review_detail->leftJoin('order', 'review.re_orderid', '=', 'order.order_id');

		
		$review_detail  = $review_detail->where('review.re_orderid', '>' ,'0');


		if(!empty($status) && ($status!='All'))

		{

		  $review_detail = $review_detail->where('review.re_status', '=', $status);

		}	
	

		/* Get Data between Date */


		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $review_detail = $review_detail->where('review.created_at', '>=', date('Y-m-d',strtotime($from_date)));

		}	


		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $review_detail = $review_detail->where('review.created_at', '<=',date('Y-m-d',strtotime($to_date)));

		}		


		/* End */
			

		$review_detail = $review_detail->select('*');
		$review_detail = $review_detail->orderBy('review.re_id', 'asc');
		$review_detail = $review_detail->get();	

		$data_onview = array('review_detail'=>$review_detail); 				
		return View('admin.ajax.review_list')->with($data_onview);

		//print_r($data_onview);	

	}
	

}

