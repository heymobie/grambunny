<?php

namespace App\Http\Controllers;


Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Cuisine;
use Route; 
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;
use App\Restaurant; 
use App\Review;


class AdminCuisineController extends Controller
{
    public function __construct(){
    	$this->middleware('admin');
    }
	
	
	public function cuisine_list()
	{
		DB::connection()->enableQueryLog();	

		$cuisine_list = DB::table('cuisine')
			 ->select('*')
			->orderBy('cuisine_id', 'desc')
			->get();

		
    $data_onview = array('cuisine_list' =>$cuisine_list);
	
		return View('admin.cuisine_list')->with($data_onview);
	} 
	
	public function cuisine_form()
	{
		
		if(Route::current()->getParameter('id'))
		{
			 $id = Route::current()->getParameter('id');
				
			$cuisine_detail  = DB::table('cuisine')->where('cuisine_id', '=' ,$id)->get();		
			
  		  	$data_onview = array('cuisine_detail' =>$cuisine_detail,
								'id'=>$id); 
						
			return View('admin.cuisine_form')->with($data_onview);
		
		}
		else
		{
			$id =0;
  		  	$data_onview = array('id'=>$id); 
			return View('admin.cuisine_form')->with($data_onview);
		}
	}
	public function cuisine_action(Request $request)
	{
		/*echo '<pre>';
		print_r($_POST);
		print_r($_FILES);
		exit;*/
		//echo "<pre>";print_r($request->all()); die;
		$cuisine_id = Input::get('cuisine_id');
		$new_cuisine='';
		
		
		 
 /////****************************** CUISINE IMAGE ***************/
		
		if(isset($_FILES['cuisine_image']) && (!empty($_FILES['cuisine_image']['name'][0]))){

		        $destinationPath = 'uploads/cuisine/';

				$image = $request->file('cuisine_image');

				$extension 		= 	$image->getClientOriginalExtension();

    			$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();

				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

				{

					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();				

					$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/cuisine/'.$thumbName);;

					asset($destinationPath . $thumbName);					

					$new_cuisine = $thumbName;

				}

				else

				{

					Session::flash('message_error', 'Image type is invalide!'); 

					

					if($cuisine_id>0){

					return redirect()->to('/admin/cuisine-form/'.$rest_id);

					}

					else

					{

						return redirect()->to('/admin/cuisinelist');

					}

				}

				

		}

		elseif(!empty($cuisine_old_logo))
		{

			 $new_cuisine = $rest_old_banner;

		}
		
		
 
 /////////////// END ***********************************************/
 
 
 
		
		if($cuisine_id==0)
		{
		
			$input['cuisine_name'] = Input::get('cuisine_name');
			
			// Must not already exist in the `email` column of `users` table
			$rules = array('cuisine_name' => 'unique:cuisine,cuisine_name');
			
			$validator = Validator::make($input, $rules);
			Session::flash('error', 'Duplicate Cuisine!'); 
			
			if ($validator->fails()) {		
				return redirect()->to('/admin/cuisine-form');
			}
			else {
					$cui = new Cuisine;	
					$cui->cuisine_name = Input::get('cuisine_name');
					$cui->cuisine_name_ar = Input::get('cuisine_name_ar');
					$cui->cuisine_status = Input::get('cuisine_status');
					$cui->cuisine_image = $new_cuisine;
					$cui->save();
					$cui_id = $cui->cuisine_id;
					
					Session::flash('message', 'Cuisine Inserted Sucessfully!'); 
					return redirect()->to('/admin/cuisinelist');
			}
		}
		else
		{
			DB::table('cuisine')
            ->where('cuisine_id', $cuisine_id)
            ->update(['cuisine_name_ar' => Input::get('cuisine_name_ar'),
            		  'cuisine_name' => Input::get('cuisine_name'),
					  'cuisine_status'=> Input::get('cuisine_status'),
					  'cuisine_image'=>  $new_cuisine
					 ]);
					 
				Session::flash('message', 'Cuisine Information Updated Sucessfully!');
				return redirect()->to('/admin/cuisinelist');
		}
		
	}
	public function cuisine_delete($id)
	{
	
	
		//echo $id;
		
	
		DB::table('cuisine')->where('cuisine_id', '=', $id)->delete();
			Session::flash('message', 'Cuisine Deleted Sucessfully!');
		
		return Redirect('/admin/cuisinelist');
	
		
	}
		
	/***************/
	// GOOGLE DATA BASED ON CUSINE START 
	/********/
	
	public function show_google_form()
	{
		
		$cuisine_list = DB::table('cuisine')
			 ->select('*')
			->where('cuisine_status','=', '1')
			->orderBy('cuisine_id', 'desc')
			->get();

		
    	$data_onview = array('cuisine_list' =>$cuisine_list);
	
		return View('admin.google_cuisine_form')->with($data_onview);
	}
	
	
	function get_form_data()
   {
	 $zipcode = trim(Input::get('location_post'));
	 $cusine_id = trim(Input::get('cusine_id'));
	 $distance_range = trim(Input::get('distange_range'));
	 
	 
			 $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$zipcode";
			//echo '<br>';
				$json_data = file_get_contents($url);
				$result = json_decode($json_data, TRUE);
				$latitude = $result['results'][0]['geometry']['location']['lat'];
				$longitude = $result['results'][0]['geometry']['location']['lng'];


	 $cuisine = DB::table('cuisine')		
				->select('*')	
				->where('cuisine_status', '=' , '1')
				->where('cuisine_id', '=' , $cusine_id)
				->orderBy('cuisine_id', 'desc')
				->get();
				
		
		
		
		 $c=0;		

					
	  $data_onview=array('cuisine'=> $cuisine,'distance_range'=> $distance_range,'latitude'=> $latitude,'longitude'=> $longitude);
	 
	  return view('admin.viewmap_list')->with($data_onview);				
   }
   
   
	/***************/
	// GOOGLE DATA BASED ON CUSINE END 
	/********/
	
}
