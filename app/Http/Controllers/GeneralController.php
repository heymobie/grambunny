<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use DB;
use App\Restaurant;
use App\User;
use \Carbon\Carbon;
use App\Category_item;
use App\Coupon_request;
use App\Suggest_restaurant;
use Mail;
use App\Rest_time;
use App\Vendor;
use App\Proservicescate;
use App\Productservice;
use Hash;
use App\Testimonial;
use App\Advertisement;
use PHPMailer\PHPMailer;

class GeneralController extends Controller
{

  public function __construct()
  {
  }

  public function main_page()
  {

    $testimonials = Testimonial::where("status", 1)->get();

    $categories = Proservicescate::where("status", 1)->get();


    $products = Productservice::select(array('product_service.*', DB::raw('COUNT(product_service.id) as count')))->take(5)->join('orders_ps as ops', 'ops.ps_id', '=', 'product_service.id')->groupBy('ops.ps_id')->orderBy('count', 'desc')->where("status", 1)->where("stock", 1)->get();

    $services = Productservice::select(array('product_service.*', DB::raw('COUNT(product_service.id) as count')))->take(5)->join('orders_ps as ops', 'ops.ps_id', '=', 'product_service.id')->groupBy('ops.ps_id')->orderBy('count', 'desc')->where("type", 1)->where("status", 1)->where("stock", 1)->get();

    $productsdft = Productservice::take(5)->orderBy('id', 'desc')->where("type", 0)->where("status", 1)->where("stock", 1)->get();
    $servicesdft = Productservice::take(5)->orderBy('id', 'desc')->where("type", 1)->where("status", 1)->where("stock", 1)->get();

    $service_provider = Vendor::take(6)->where("vendor_status", 1)->get();

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();

    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $ps_block1 = Advertisement::where("status", 1)->where("id", 3)->first();

    $ps_block2 = Advertisement::where("status", 1)->where("id", 4)->first();

    $ps_block3 = Advertisement::where("status", 1)->where("id", 5)->first();

    $ps_block4 = Advertisement::where("status", 1)->where("id", 6)->first();

    $distance = 100;

    // $ip = $_SERVER['REMOTE_ADDR']; //'173.51.210.102'; //

    $ip = $this->get_client_ip();

    $curl_handle = curl_init();

    curl_setopt($curl_handle, CURLOPT_URL, 'http://www.geoplugin.net/php.gp?ip=' . $ip);

    curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);

    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($curl_handle, CURLOPT_USERAGENT, 'herbariumdelivery');

    $query = curl_exec($curl_handle);

    curl_close($curl_handle);

    $location = (unserialize($query));

    if (!empty(Session::get('nearlat'))) {

      $latitude =  Session::get('nearlat');
    } else {
      $latitude =  $location['geoplugin_latitude'];
    }

    if (!empty(Session::get('nearlng'))) {

      $longitude =  Session::get('nearlng');
    } else {
      $longitude = $location['geoplugin_longitude'];
    }


    if (empty($latitude)) {
      $latitude = "33.989819";
    }
    if (empty($longitude)) {
      $longitude = "-117.732582";
    }


    $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

    //$vendors= Arr::pluck($results,"vendor_id");

    $vendors = array();

    foreach ($results as $key => $value) {

      $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

      if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

        $vendors[] = $value->vendor_id;
      }
    }

    $perPage = 25; // Number of records per page
    $offset = ($page - 1) * $perPage;
    $vendors = array_slice($vendors, $offset, $perPage);

    $vendorsDetials = array();

    foreach ($vendors as $key => $vId) {

      $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
    }

    $listview = $vendorsDetials->map(function ($vendor) use ($results) {
      foreach ($results as $result) {
        if ($result->vendor_id == $vendor->vendor_id) {
          $vendor["distance"] = round($result->distance, 1) . " Miles";
        }
      }
      return $vendor;
    });

    //print_r($vendors); die;

    //$verdorslist= Vendor::find($vendors);   
    $verdorslist = Vendor::whereIn("vendor_id", $vendors)->where("login_status", 1)->get();

    $category = '';

    //echo "<pre>";print_r($verdorslist);die;
    return view('home', ["testimonials" => $testimonials, "verdorslist" => $verdorslist, "listview" => $listview, "categories" => $categories, "products"  => $products, "services" => $services, "productsdft" => $productsdft,  "servicesdft" => $servicesdft, "service_provider" => $service_provider, "advertisement1" => $advertisement1, "advertisement2" => $advertisement2, "ps_block1" => $ps_block1, "ps_block2" => $ps_block2, "ps_block3"  => $ps_block3, "ps_block4"  => $ps_block4, "latitude" => $latitude, "longitude"  => $longitude, "category"  => $category, "results" => $results]);
  }

  public function homenewpage()
  {

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $bannerimage = DB::table('admin_setting')->where('id', 1)->first();

    $banner_image_multiple = DB::table('multiple_banner_image')->get();

    $banner_img_url = url('/') . '/public/uploads/advertisement/';

    $bannerimages = $banner_img_url . $bannerimage->banner_image;

    $distance = 100;
    $latitude = '33.9898';
    $longitude = '117.7326';
    $page = 1;
    $perPage = 25;

    // $ip = $_SERVER['REMOTE_ADDR']; //'173.51.210.102'; //

    if (empty(Session::get('mylatitude'))) {

      $ip = $this->get_client_ip();

      $curl_handle = curl_init();

      curl_setopt($curl_handle, CURLOPT_URL, 'https://www.grambunny.com/location/public/display-user?ip=' . $ip);

      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);

      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'grambunny');

      $query = curl_exec($curl_handle);

      curl_close($curl_handle);

      // $location = (unserialize($query));

      $location = explode(",", $query);

      if (isset($location[0]) && isset($location[1])) {
        $latitude = $location[0];
        $longitude = $location[1];
      } else {
        $latitude = '34.03026980';
        $longitude = '-118.23087030';
      }

      Session::put('mylatitude', $latitude);
      Session::put('mylongitude', $longitude);

      Session::put('latitudegeo', $latitude);
      Session::put('longitudegeo', $longitude);
    } else {

      $latitude = Session::get('mylatitude');
      $longitude = Session::get('mylongitude');
    }


    $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

    //print_r($results);

    $vendors = array();

    foreach ($results as $key => $value) {

      $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

      if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

        $vendors[] = $value->vendor_id;
      }
    }

    $offsets = ($page) * $perPage;
    $totalvdrs = array_slice($vendors, $offsets, $perPage);

    $offset = ($page - 1) * $perPage;
    $vendors = array_slice($vendors, $offset, $perPage);

    // $items=Productservice::where("vendor_id",$value->vendor_id)->where("status",1)->where("stock",1)->where("quantity",">",0)->paginate(24);

    $vendorsDetials = array();

    foreach ($vendors as $key => $vId) {

      $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
    }

    $categories = Proservicescate::where("status", 1)->get();

    //dd($categories);die;

    //$verdorslist= Vendor::find($vendors);   
    //$verdorslist= Vendor::whereIn("vendor_id",$vendors)->where("login_status",1)->get();  

    $category = '';

    $page = DB::table('pages')->where('page_id', 1)->first();

    $totalv = count($totalvdrs);

    return view('homenew', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "vendors" => $vendors, "results" => $results, "banner_image" => $bannerimages, "banner_image_multiple" => $banner_image_multiple, 'page_data' => $page, 'total' => $totalv]);
  }


  public function searchproductonload_new(Request $request)
  {

    $searchpro = $request->all();
    $date = date('Y-m-d H:i:s');
    $latitude = $searchpro['data']['latitude'];
    $longitude = $searchpro['data']['longitude'];
    $category = '';
    $page = 1;
    $perPage = 25;

    Session::put('latitudegeo', $latitude);
    Session::put('longitudegeo', $longitude);

    $distance = 100;

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $vendors = array();

    $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

    $vendors = array();

    foreach ($results as $key => $value) {

      $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

      if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

        $vendors[] = $value->vendor_id;
      }
    }


    $offsets = ($page) * $perPage;
    $totalvdrs = array_slice($vendors, $offsets, $perPage);

    $offset = ($page - 1) * $perPage;
    $vendors = array_slice($vendors, $offset, $perPage);

    $vendorsDetials = array();

    foreach ($vendors as $key => $vId) {

      $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
    }

    $categories = Proservicescate::where("status", 1)->get();

    $totalv = count($totalvdrs);

    //print_r($vendorsDetials);

    return view('home_ajaxnew', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "scategory" => $category, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "results" => $results, "total" => $totalv, "pages" => $page]);
  }

  public function ajaxsearchproduct_new(Request $request)
  {

    $searchpro = $request->all();
    $date = date('Y-m-d H:i:s');
    $searchpbr = $searchpro['data']['searchpbr'];
    $cityzip = $searchpro['data']['cityzip'];
    $category = $searchpro['data']['category'];
    if (isset($searchpro['data']['pages'])) {
      $page = $searchpro['data']['pages'];
    } else {
      $page = 1;
    }

    $perPage = 25;

    if ($category == 'all') {
      $category = '';
    }

    $latitudegeo = Session::get('latitudegeo');
    $longitudegeo = Session::get('longitudegeo');


    $distance = 100;

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $vendors = array();

    $latitude = '';
    $longitude = '';

    if (!empty($searchpbr) || !empty($cityzip) || !empty($category)) {

      if (!empty($searchpbr) && !empty($cityzip)) {

        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
        // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($cityzip) . '&sensor=false&key=' . $apiKey);
        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {
          $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
          $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }

        /* if(@auth()->guard("user")->user()->id)
            {
              $latitude = auth()->guard("user")->user()->user_lat;
              $longitude= auth()->guard("user")->user()->user_long;
            } */

        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id"); 

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

          if (empty($serarchm)) {

            $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
          }

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            if (!empty($serarchm)) {

              $vendors[] = $value->vendor_id;
            }
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        if (empty($latitude) && empty($longitude)) {

          $latitude = $latitudegeo;
          $longitude = $longitudegeo;

          $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

          //$vendors = Arr::pluck($results,"vendor_id");

          $vendors = array();

          foreach ($results as $key => $value) {

            $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

            $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

            if (empty($serarchm)) {

              $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
            }

            if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

              if (!empty($serarchm)) {

                $vendors[] = $value->vendor_id;
              }
            }
          }

          $offsets = ($page) * $perPage;
          $totalvdrs = array_slice($vendors, $offsets, $perPage);

          $offset = ($page - 1) * $perPage;
          $vendors = array_slice($vendors, $offset, $perPage);
        }

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      } elseif (!empty($searchpbr)) {

        /* $mlatlong = Vendor::where('username', 'like', '%' . $searchpbr . '%')->first();

    if(empty($mlatlong)){

    $mlatlong = Vendor::where('name', 'like', '%' . $searchpbr . '%')->first();

    }

    if(!empty($mlatlong->city) || !empty($mlatlong->zipcode)){  

    $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';  
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($mlatlong->city).'&sensor=true&key='.$apiKey);
    $geo = json_decode($geo, true); 

    if (isset($geo['status']) && ($geo['status'] == 'OK')) {

    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];

    }else{

    $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';  
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($mlatlong->zipcode).'&sensor=true&key='.$apiKey);
    $geo = json_decode($geo, true); 

    if (isset($geo['status']) && ($geo['status'] == 'OK')) {

    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];

    }else{ 

    $latitude = $latitudegeo;
    $longitude = $longitudegeo;

    }

    }

    }else{ */

        $latitude = $latitudegeo;
        $longitude = $longitudegeo;

        // }


        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id");

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

          if (empty($serarchm)) {

            $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
          }

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            if (!empty($serarchm)) {

              $vendors[] = $value->vendor_id;
            }
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      } elseif (!empty($cityzip)) {

        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
        // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($cityzip) . '&sensor=false&key=' . $apiKey);
        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {
          $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
          $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }

        /* if(@auth()->guard("user")->user()->id)
          {
            $latitude = auth()->guard("user")->user()->user_lat;
            $longitude= auth()->guard("user")->user()->user_long;
          } */


        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id");     

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            $vendors[] = $value->vendor_id;
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }


        if (empty($latitude) && empty($longitude)) {

          $latitude = $latitudegeo;
          $longitude = $longitudegeo;

          $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

          //$vendors = Arr::pluck($results,"vendor_id");

          $vendors = array();

          foreach ($results as $key => $value) {

            $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

            if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

              $vendors[] = $value->vendor_id;
            }
          }

          $offsets = ($page) * $perPage;
          $totalvdrs = array_slice($vendors, $offsets, $perPage);

          $offset = ($page - 1) * $perPage;
          $vendors = array_slice($vendors, $offset, $perPage);

          $vendorsDetials = array();

          foreach ($vendors as $key => $vId) {

            $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
          }
        }

        $categories = Proservicescate::where("status", 1)->get();
      } else {

        $latitude = $latitudegeo;
        $longitude = $longitudegeo;

        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id"); 

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            $vendors[] = $value->vendor_id;
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      }
    } else {

      $latitude = $latitudegeo;
      $longitude = $longitudegeo;


      $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

      //$vendors = Arr::pluck($results,"vendor_id");

      $vendors = array();

      foreach ($results as $key => $value) {

        $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

        if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

          $vendors[] = $value->vendor_id;
        }
      }

      $offsets = ($page) * $perPage;
      $totalvdrs = array_slice($vendors, $offsets, $perPage);

      $offset = ($page - 1) * $perPage;
      $vendors = array_slice($vendors, $offset, $perPage);

      $vendorsDetials = array();

      foreach ($vendors as $key => $vId) {

        $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
      }


      $categories = Proservicescate::where("status", 1)->get();
    }

    $totalv = count($totalvdrs);

    return view('home_ajaxnew', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "scategory" => $category, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "results" => $results, "total" => $totalv, "pages" => $page]);
  }


  public function home_page()
  {

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $bannerimage = DB::table('admin_setting')->where('id', 1)->first();

    $banner_image_multiple = DB::table('multiple_banner_image')->get();

    $banner_img_url = url('/') . '/public/uploads/advertisement/';

    $bannerimages = $banner_img_url . $bannerimage->banner_image;

    $distance = 100;
    $latitude = '33.9898';
    $longitude = '117.7326';
    $page = 1;
    $perPage = 25;

    // $ip = $_SERVER['REMOTE_ADDR']; //'173.51.210.102'; //

    if (empty(Session::get('mylatitude'))) {

      $ip = $this->get_client_ip();

      $curl_handle = curl_init();

      curl_setopt($curl_handle, CURLOPT_URL, 'https://www.grambunny.com/location/public/display-user?ip=' . $ip);

      curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);

      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

      curl_setopt($curl_handle, CURLOPT_USERAGENT, 'grambunny');

      $query = curl_exec($curl_handle);

      curl_close($curl_handle);

      // $location = (unserialize($query));

      $location = explode(",", $query);

      if (isset($location[0]) && isset($location[1])) {
        $latitude = $location[0];
        $longitude = $location[1];
      } else {
        $latitude = '34.03026980';
        $longitude = '-118.23087030';
      }

      Session::put('mylatitude', $latitude);
      Session::put('mylongitude', $longitude);

      Session::put('latitudegeo', $latitude);
      Session::put('longitudegeo', $longitude);
    } else {

      $latitude = Session::get('mylatitude');
      $longitude = Session::get('mylongitude');
    }


    $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

    //print_r($results);

    $vendors = array();

    foreach ($results as $key => $value) {

      $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

      if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

        $vendors[] = $value->vendor_id;
      }
    }

    $offsets = ($page) * $perPage;
    $totalvdrs = array_slice($vendors, $offsets, $perPage);

    $offset = ($page - 1) * $perPage;
    $vendors = array_slice($vendors, $offset, $perPage);

    // $items=Productservice::where("vendor_id",$value->vendor_id)->where("status",1)->where("stock",1)->where("quantity",">",0)->paginate(24);

    $vendorsDetials = array();

    foreach ($vendors as $key => $vId) {

      $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
    }

    $categories = Proservicescate::where("status", 1)->get();

    //dd($categories);die;

    //$verdorslist= Vendor::find($vendors);   
    //$verdorslist= Vendor::whereIn("vendor_id",$vendors)->where("login_status",1)->get();  

    $category = '';

    $page = DB::table('pages')->where('page_id', 1)->first();

    $totalv = count($totalvdrs);

    return view('home', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "vendors" => $vendors, "results" => $results, "banner_image" => $bannerimages, "banner_image_multiple" => $banner_image_multiple, 'page_data' => $page, 'total' => $totalv]);
  }


  public function searchproductonload(Request $request)
  {

    $searchpro = $request->all();
    $date = date('Y-m-d H:i:s');
    $latitude = $searchpro['data']['latitude'];
    $longitude = $searchpro['data']['longitude'];
    $category = '';
    $page = 1;
    $perPage = 25;

    Session::put('latitudegeo', $latitude);
    Session::put('longitudegeo', $longitude);

    $distance = 100;

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $vendors = array();

    $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

    $vendors = array();

    foreach ($results as $key => $value) {

      $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

      if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

        $vendors[] = $value->vendor_id;
      }
    }


    $offsets = ($page) * $perPage;
    $totalvdrs = array_slice($vendors, $offsets, $perPage);

    $offset = ($page - 1) * $perPage;
    $vendors = array_slice($vendors, $offset, $perPage);

    $vendorsDetials = array();

    foreach ($vendors as $key => $vId) {

      $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
    }

    $categories = Proservicescate::where("status", 1)->get();

    $totalv = count($totalvdrs);

    return view('home_ajax', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "scategory" => $category, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "results" => $results, "total" => $totalv, "pages" => $page]);
  }

  public function ajaxsearchproduct(Request $request)
  {

    $searchpro = $request->all();
    $date = date('Y-m-d H:i:s');
    $searchpbr = $searchpro['data']['searchpbr'];
    $cityzip = $searchpro['data']['cityzip'];
    $category = $searchpro['data']['category'];
    if (isset($searchpro['data']['pages'])) {
      $page = $searchpro['data']['pages'];
    } else {
      $page = 1;
    }

    $perPage = 25;

    if ($category == 'all') {
      $category = '';
    }

    $latitudegeo = Session::get('latitudegeo');
    $longitudegeo = Session::get('longitudegeo');

    $distance = 100;

    $advertisement1 = Advertisement::where("status", 1)->where("id", 1)->first();
    $advertisement2 = Advertisement::where("status", 1)->where("id", 2)->first();

    $vendors = array();

    $latitude = '';
    $longitude = '';

    if (!empty($searchpbr) || !empty($cityzip) || !empty($category)) {

      if (!empty($searchpbr) && !empty($cityzip)) {

        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
        // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($cityzip) . '&sensor=false&key=' . $apiKey);
        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {
          $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
          $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }

        /* if(@auth()->guard("user")->user()->id)
            {
              $latitude = auth()->guard("user")->user()->user_lat;
              $longitude= auth()->guard("user")->user()->user_long;
            } */


        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id"); 

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

          if (empty($serarchm)) {

            $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
          }

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            if (!empty($serarchm)) {

              $vendors[] = $value->vendor_id;
            }
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        if (empty($latitude) && empty($longitude)) {

          $latitude = $latitudegeo;
          $longitude = $longitudegeo;

          $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

          //$vendors = Arr::pluck($results,"vendor_id");

          $vendors = array();

          foreach ($results as $key => $value) {

            $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

            $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

            if (empty($serarchm)) {

              $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
            }

            if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

              if (!empty($serarchm)) {

                $vendors[] = $value->vendor_id;
              }
            }
          }

          $offsets = ($page) * $perPage;
          $totalvdrs = array_slice($vendors, $offsets, $perPage);

          $offset = ($page - 1) * $perPage;
          $vendors = array_slice($vendors, $offset, $perPage);
        }

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      } elseif (!empty($searchpbr)) {

        /* $mlatlong = Vendor::where('username', 'like', '%' . $searchpbr . '%')->first();

    if(empty($mlatlong)){

    $mlatlong = Vendor::where('name', 'like', '%' . $searchpbr . '%')->first();

    }

    if(!empty($mlatlong->city) || !empty($mlatlong->zipcode)){  

    $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';  
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($mlatlong->city).'&sensor=true&key='.$apiKey);
    $geo = json_decode($geo, true); 

    if (isset($geo['status']) && ($geo['status'] == 'OK')) {

    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];

    }else{

    $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';  
    $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($mlatlong->zipcode).'&sensor=true&key='.$apiKey);
    $geo = json_decode($geo, true); 

    if (isset($geo['status']) && ($geo['status'] == 'OK')) {

    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];

    }else{ 

    $latitude = $latitudegeo;
    $longitude = $longitudegeo;

    }

    }

    }else{ */

        $latitude = $latitudegeo;
        $longitude = $longitudegeo;

        // }


        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id");

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          $serarchm = Vendor::where('username', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();

          if (empty($serarchm)) {

            $serarchm = Vendor::where('name', 'like', '%' . $searchpbr . '%')->where("vendor_id", $value->vendor_id)->first();
          }

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            if (!empty($serarchm)) {

              $vendors[] = $value->vendor_id;
            }
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      } elseif (!empty($cityzip)) {

        $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'; // Google maps now requires an API key.
        // Get JSON results from this request
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($cityzip) . '&sensor=false&key=' . $apiKey);
        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {
          $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
          $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
        }

        /* if(@auth()->guard("user")->user()->id)
          {
            $latitude = auth()->guard("user")->user()->user_lat;
            $longitude= auth()->guard("user")->user()->user_long;
          } */


        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id");     

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            $vendors[] = $value->vendor_id;
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }


        if (empty($latitude) && empty($longitude)) {

          $latitude = $latitudegeo;
          $longitude = $longitudegeo;

          $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

          //$vendors = Arr::pluck($results,"vendor_id");

          $vendors = array();

          foreach ($results as $key => $value) {

            $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

            if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

              $vendors[] = $value->vendor_id;
            }
          }

          $offsets = ($page) * $perPage;
          $totalvdrs = array_slice($vendors, $offsets, $perPage);

          $offset = ($page - 1) * $perPage;
          $vendors = array_slice($vendors, $offset, $perPage);

          $vendorsDetials = array();

          foreach ($vendors as $key => $vId) {

            $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
          }
        }

        $categories = Proservicescate::where("status", 1)->get();
      } else {

        $latitude = $latitudegeo;
        $longitude = $longitudegeo;

        $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

        //$vendors = Arr::pluck($results,"vendor_id"); 

        $vendors = array();

        foreach ($results as $key => $value) {

          $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

          if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

            $vendors[] = $value->vendor_id;
          }
        }

        $offsets = ($page) * $perPage;
        $totalvdrs = array_slice($vendors, $offsets, $perPage);

        $offset = ($page - 1) * $perPage;
        $vendors = array_slice($vendors, $offset, $perPage);

        $vendorsDetials = array();

        foreach ($vendors as $key => $vId) {

          $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
        }

        $categories = Proservicescate::where("status", 1)->get();
      }
    } else {

      $latitude = $latitudegeo;
      $longitude = $longitudegeo;


      $results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));

      //$vendors = Arr::pluck($results,"vendor_id");

      $vendors = array();

      foreach ($results as $key => $value) {

        $vendorlogin = Vendor::where("vendor_id", $value->vendor_id)->first();

        if (($vendorlogin->login_status == 1) && ($value->distance <= $vendorlogin->service_radius)) {

          $vendors[] = $value->vendor_id;
        }
      }

      $offsets = ($page) * $perPage;
      $totalvdrs = array_slice($vendors, $offsets, $perPage);

      $offset = ($page - 1) * $perPage;
      $vendors = array_slice($vendors, $offset, $perPage);

      $vendorsDetials = array();

      foreach ($vendors as $key => $vId) {

        $vendorsDetials[] = Vendor::where("vendor_id", $vId)->first();
      }


      $categories = Proservicescate::where("status", 1)->get();
    }

    $totalv = count($totalvdrs);

    return view('home_ajax', ["verdorslist" => $vendorsDetials, "categories"  => $categories, "scategory" => $category, "advertisement1"  => $advertisement1, "advertisement2" => $advertisement2, "latitude" => $latitude, "longitude"  => $longitude, "results" => $results, "total" => $totalv, "pages" => $page]);
  }


  public  function get_client_ip()
  {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
    else
      $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }


  public function webservice_privacy_policy()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '5')
      ->get();
    $data_onview = array('page_detail' => $page_detail);

    return view('webservice_content_page')->with($data_onview);
  }

  public function webservice_terms_condition()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '4')
      ->get();
    $data_onview = array('page_detail' => $page_detail);

    return view('webservice_content_page')->with($data_onview);
  }


  public function webservice_about()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '1')
      ->get();

    $data_onview = array('page_detail' => $page_detail);

    return view('webservice_content_page')->with($data_onview);
  }

  public function webservice_contactus()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '2')
      ->get();
    $data_onview = array('page_detail' => $page_detail);
    return view('webservice_content_page')->with($data_onview);
  }

  public function webservice_help_page()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '3')
      ->get();
    $data_onview = array('page_detail' => $page_detail);
    return view('webservice_content_page')->with($data_onview);
  }

  public function webservice_faq_page()
  {
    $page_detail  = DB::table('pages')
      ->where('page_id', '=', '6')
      ->get();
    $data_onview = array('page_detail' => $page_detail);
    return view('webservice_content_page')->with($data_onview);
  }
}
