<?php

namespace App\Http\Controllers\AdminAuth;

use App\Admin;

use App\Http\Controllers\Controller;

use DB;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use PHPMailer\PHPMailer;

use Redirect;

use Session;

use Validator;

use Mail;



class AuthController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Registration & Login Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users, as well as the

    | authentication of existing users. By default, this controller uses

    | a simple trait to add these behaviors. Why don't you explore it?

    |

    */



 

    /**

     * Where to redirect users after login / registration.

     *

     * @var string

     */





    protected $redirectTo = '/admin/dashboard';

    protected $redirectAfterLogout = '/admin/login';

    protected $guard = 'admin';





    /**

     * Create a new authentication controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);

    }



    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */





    public function login(Request $request)
    {

        $request->validate([

            "email" => "required",

            "password" => "required"

        ]);

        $remember = $request->remember;

        if (Auth::guard("admin")->attempt(["email" => $request->email,"password" => $request->password])) {

            if (Auth::guard("admin")->user()->status==0) {

                Auth::guard("admin")->logout();

                return redirect()->back()->with("warning","Your account is not activated by administrator.");   

            }

        if($remember){

        setcookie("email", $request->email, time() + 3600); 

        setcookie("password", $request->password, time() + 3600);

        setcookie("remember", 1, time() + 3600);

         }else{

         setcookie ("email", "", time() - 3600);
         setcookie ("password", "", time() - 3600);
         setcookie ("remember", "", time() - 3600);

         }    

            return redirect()->route("admin.dashboard");

        }else{

             Session::flash('message', "Credentails do not matches our record");

            return redirect()->back()->withErros(["email" => "Credentails do not matches our record."]);

        }

    }

    public function showLoginForm()
    {

      if (view()->exists('auth.authenticate')) {

       return view('auth.authenticate');

   }

   return view('admin.auth.login');

}



public function logout()

{

    Auth::guard("admin")->logout();

    return redirect()->back();

}





protected function validator(array $data)

{

    return Validator::make($data, [

        'name' => 'required|max:255',

        'email' => 'required|email|max:255|unique:users',

        'password' => 'required|min:6|confirmed',

    ]);

}



    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return User

     */

    protected function create(array $data)

    {

        return User::create([

            'name' => $data['name'],

            'email' => $data['email'],

            'password' => bcrypt($data['password']),

        ]);

    }







    public function forgotpwd()

    {

      if (view()->exists('auth.authenticate')) {

       return view('auth.authenticate');

   }



   return view('admin.auth.reset');

}



/*Forget password*/

public function forgetPassword()

{

    if (view()->exists('auth.authenticate')) {

     return view('auth.authenticate');

 }

 return view('admin.auth.reset_pass');

}



public function generateRandomString($length = 6) {

   $characters = '0123456789';

   $charactersLength = strlen($characters);

   $randomString = '';

   for ($i = 0; $i < $length; $i++) {

       $randomString .= $characters[rand(0, $charactersLength - 1)];

   }

   return $randomString;

}



/*Send otp after enter email*/

public function sentOTP(Request $request)

{

 $AdminEmail = $request->input('email');

 $Admin = DB::table('admins')->where('email',$AdminEmail)->first();

 if(!empty($Admin)){

    $Admin_id = $Admin->id;

    $admin_phone = $Admin->mobile_no;

    $otp_code = trim($this->generateRandomString(6));

    //$address = "votivemobile.sumit@gmail.com"; //"deepaksanidhya@gmail.com";

   // $message = "OTP is ".$otp_code;

    $subject = "Forgot Password OTP";

    $usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',2)->first();

    $address = $AdminEmail; //"deepaksanidhya@gmail.com";

    $message = 'Hello ,'.$usr_fgtpss_email_cotnt->email_content.$otp_code;

    $smstext = $usr_fgtpss_email_cotnt->email_content.$otp_code;

    $subject = $usr_fgtpss_email_cotnt->email_title;

    $semail = $this->sendMail($address,$subject,$message);

    if(!empty($admin_phone)){ $this->sendSms($smstext, $admin_phone); }

        if(!empty($otp_code)){

             //$data=array('otp'=>$otp_code);

             DB::table('admins')

             ->where('id',$Admin_id)

             ->update(['otp'=>$otp_code]);

             Session::flash('Succes', 'OTP has been sent successfully.');

             session()->flash('Admin_id', $Admin_id);


             return redirect()->to('/admin/password/enter_otp');

         }else{

            Session::flash('Error', 'Email sent failed.');

            return redirect()->back();

        }

 

    }else{

        Session::flash('Error', 'Please enter your registered email.');

        return redirect()->back();

    }

}


    function sendSms($smstext, $to)
    {

        $phone = str_replace("(", "", $to);
        $phoneno = str_replace(") ", "", $phone);
        $phonenos = str_replace("-", "", $phoneno);

        $mobile = '1' . $phonenos;

        // 12013002832 , 19133505810

        $data = array(
            'from' => '12013002832',
            'text' => $smstext,
            'to' => $mobile,
            'api_key' => 'c2aa3522',
            'api_secret' => 'o5NWJ25e5FbszCxH'

        );

        $sendsms = json_encode($data, JSON_FORCE_OBJECT);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.nexmo.com/sms/json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $sendsms,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;

    }


/*Enter OTP */

public function VerifyOTPForm()

{

    session()->keep(['Admin_id']);

    return view('admin.auth.enter_your_otp');

}



/*OTP submit*/

public function VerifyOTP(Request $request)

{

    $enterOTP = $request->input('user_otp');

    $AdminID = $request->input('AdminID');

    $AdminData = DB::table('admins')->where('id',$AdminID)->first();

    $saveOTP = $AdminData->otp;

    $adminID = $AdminData->id;

    if($saveOTP==$enterOTP){

        //echo "Match";

        Session::flash('Succes', 'Your OTP has been verified successfully.');

        session()->flash('adminID', $adminID);

        return redirect()->to('/admin/password/reset_your_password');

    }else{

        Session::flash('Otp_Error', 'OTP did not match.');

        session()->keep(['Admin_id']);

        return redirect()->back();

    }

}

public function setNewPasswordForm()

{

    session()->keep(['adminID']);

    return view('admin.auth.set_new_pass');

}

public function setNewPassword(Request $request)

{

    $new_pass = $request->input('new_pass');

    $adminID = $request->input('AdminID');

    $affected = DB::table('admins')

    ->where('id',$adminID)

    ->update(['password'=>bcrypt($new_pass)]);

    if($affected){

     //    $AdminData = DB::table('admins')->where('id',$adminID)->first();

     //    $email = $Admin->email;

     //    $name = $Admin->name;

     //    $msg_reg_otp = 'Dear '.trim($name).', Congratulations your password has been changed successfully.';

     //    Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

     //     $message->from('info@gmail.com', 'Online order food');

     //     $otp_email = trim($email);

     //     $message->to($otp_email);

     //     $message->subject('Passowrd successfully changed.');

     // });

        Session::flash('PassSucChange', 'Your password has been changed successfully.');

        return redirect('/admin');

    }else{

        Session::flash('Change_pass', 'Somthing is wrong! please try again.');

        session()->keep(['adminID']);

        return redirect()->back();

    }

}



  public function sendMail($address,$subject,$message)

    {
        $data['address'] = $address;
        $data['subject'] = $subject;

        Mail::send("emails.api_email",["apimsg" => $otpmessage],function($message) use ($data){

        $message->from(config("app.webmail"), config("app.mailname"));

        $message->subject($data['subject']);

        $message->to($data['address']);

        });


         return true;

             /* Mail::send('emails.order_detail', $data, function ($message) use ($data) {

                $message->from('info@grambunny.com', 'www.grambunny.com');

                $message->to($address);

                $message->subject($subject);

            }); */

           /* $mail = new PHPMailer\PHPMailer();
            $mail->IsSMTP(); // telling the class to use SMTP

            //$mail->Host       = "smtp.gmail.com"; // SMTP server

            //$mail->SMTPDebug  = 1;                     // enables SMTP debug information (for testing)

            $mail->SMTPAuth   = true;                 // enable SMTP authentication
            $mail->Port       = 25;                    // set the SMTP server port
            $mail->Host       = "mail.conceptline.org"; // SMTP server
            $mail->Username   = "app@grambunny.com";     // SMTP server username
            $mail->Password   = "z4g&nF22";            // SMTP server password
            $mail->SetFrom('app@grambunny.com', 'grambunny');
            $mail->Subject = $subject;
            $mail->MsgHTML($message);
            $mail->IsHTML(true); // send as HTML
            $address = $address;
            $mail->AddAddress($address);

           // $mail->AddCC('votivedeepak.php@gmail.com');

            if(!$mail->Send()) {
            //return $mail->ErrorInfo ;
            return false;   

            } else {

            return true;

            } */

    }



}

