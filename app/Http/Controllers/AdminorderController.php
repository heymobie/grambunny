<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

use App\Order_history;

use App\Order_pay_history;

use App\Notification_list;

use App\Http\Requests;

Use DB;

use Hash;

use Session;

use Redirect;

use Validator;

use Route;

use Mail;
use App\Order;


class AdminorderController extends Controller
{

    public function __construct(){

    	$this->middleware('admin');
   }

   	public function merchant_order(Request $request)
	{

		

	if($request->id==''){

		

	$vendor_list = DB::table('vendor')

			->select('*') 

			->orderBy('vendor_id', 'asc')

			->get();



	  }else{

	  

    $vendor_list = DB::table('vendor')

			 ->where('type', '=' ,$request->id)

			 ->orderBy('vendor_id', 'asc')

			 ->get();

	  }		 

	

    $data_onview = array('vendor_list' =>$vendor_list,'searchid' =>$request->id);

	

		return View('admin.merchant_order')->with($data_onview);

	} 


	public function customer_orderlist(Request $request){

  		if($request->sid==''){

  		$order_detail  = DB::table('orders')

		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		// ->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->select('orders.*')
		->groupBy('orders.id')		
	  	->where('orders.product_type', '!=' ,3)

		->where('orders.user_id', '=' ,$request->id)

		->orderBy('orders.id', 'desc')
        
        ->get();
		//->simplePaginate(10);


	   }else{


       	$order_detail  = DB::table('orders')

		 ->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		 // ->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		 ->select('orders.*')
		 ->groupBy('orders.id')		
		
         ->where('orders.product_type', '!=' ,3)

		->where('orders.user_id', '=' ,$request->id)

		->where('orders.status', '=' ,$request->sid)

		->orderBy('orders.id', 'desc')

		->get();

		//->simplePaginate(10);

	   }

  		$data_onview = array('order_detail' =>$order_detail,'vendor_id' =>$request->id,'searchid' =>$request->sid);  

		return View('admin.customer_order_list')->with($data_onview);

	}


	 public function event_orderlist(Request $request){

		if($request->sid==''){

		  $event_detail  = DB::table('orders')

			->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
			// ->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
			->select('orders.*')
			->distinct('orders.id')
			->where('orders.product_type', '=' ,3)

		  ->where('orders.user_id', '=' ,$request->id)
		  ->orderBy('orders.id', 'desc')
		  ->get();

		 }else{

			 $event_detail  = DB::table('orders')

			->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
			// ->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
			->select('orders.*')
			->distinct('orders.id')
			
		  ->where('orders.product_type', '=' ,3)
		  ->where('orders.user_id', '=' ,$request->id)
		  ->where('status', '=' ,$request->sid)
		  ->orderBy('orders.id', 'desc')
		  ->get();
		 }
			$data_onview = array('event_detail' =>$event_detail,'vendor_id' =>$request->id,'searchid' =>$request->sid);  
		 	return View('admin.customer_event_list')->with($data_onview);
	  }




  	public function show_orderlist(Request $request){


  		if($request->sid==''){


  			$order_detail  = DB::table('orders')

		//->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		//->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		//->select('product_service.type','orders.id','orders.order_id','orders.user_id','orders.first_name','orders.last_name','orders.total','orders.status','orders.created_at')
		//->distinct('orders.id')

		->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->select('product_service.type','orders.*')
		->groupBy('orders.id')		
	  	->where('product_service.type', '!=' ,3)

		->where('orders.vendor_id', '=' ,$request->id)

		->orderBy('orders.id', 'desc')

		->get();

		//->simplePaginate(10);


	   }else{


       	$order_detail  = DB::table('orders')

       	//->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		//->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		//->select('product_service.type','orders.id','orders.order_id','orders.user_id','orders.first_name','orders.last_name','orders.total','orders.status','orders.created_at')
		//->distinct('orders.id')


		 ->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		 ->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		 ->select('product_service.type','orders.*')
		 ->groupBy('orders.id')		
		
         ->where('product_service.type', '!=' ,3)

		->where('orders.vendor_id', '=' ,$request->id)

		->where('orders.status', '=' ,$request->sid)

		->orderBy('orders.id', 'desc')

		->get();

		//->simplePaginate(10);

	   }

		// $order_detail  = DB::table('orders')

		// ->where('vendor_id', '=' ,$request->id)

		// ->orderBy('id', 'desc')

		// ->simplePaginate(10);


	  //  }else{


    //    	$order_detail  = DB::table('orders')

		// ->where('vendor_id', '=' ,$request->id)

		// ->where('status', '=' ,$request->sid)

		// ->orderBy('id', 'desc')

		// ->simplePaginate(10);


	  //  }


  		$data_onview = array('order_detail' =>$order_detail,'vendor_id' =>$request->id,'searchid' =>$request->sid);  

		return View('admin.order_list')->with($data_onview);

	}


	public function show_eventlist(Request $request){
		if($request->sid==''){
		  $event_detail  = DB::table('orders')

			->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
			->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
			->select('product_service.type','orders.*')
			->distinct('orders.id')
			->where('product_service.type', '=' ,3)

		  ->where('orders.vendor_id', '=' ,$request->id)
		  ->orderBy('orders.id', 'desc')
		  ->get();
		 }else{
			 $event_detail  = DB::table('orders')

			->leftJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
			->leftJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
			->select('product_service.type','orders.*')
			->distinct('orders.id')
			
		  ->where('product_service.type', '=' ,3)
		  ->where('orders.vendor_id', '=' ,$request->id)
		  ->where('status', '=' ,$request->sid)
		  ->orderBy('orders.id', 'desc')
		  ->get();
		 }
			$data_onview = array('event_detail' =>$event_detail,'vendor_id' =>$request->id,'searchid' =>$request->sid);  
		 	return View('admin.event_list')->with($data_onview);
	  }



	public function order_details(Request $request)
	{

		$orderid = $request->id;

		$order_counts = DB::table('orders')

		->where('id', '=' ,$orderid)

		->get();

		if(count($order_counts)>0){

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->first();

		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();			

		$vendor_order = DB::table('orders')

		->where('id', '=' ,$orderid)

		->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')

		->first();

		$data_onview = array('order_detail' =>$order_detail, 'vendor_order' =>$vendor_order,'ps_list' =>$pslist);  					 		
		return View('admin.reporting_view')->with($data_onview);

	}else{

		return redirect()->to('/admin/dashboard');
	}

	}

	public function update_order_action1()
	{

		    $order_status = Input::get('order_status');

			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$getvalue = DB::table('orders')->where('order_id',$order_id)->first();

			$user_id = $getvalue->user_id;

		if($order_status==1){

			$carddata = DB::table('users_card')->where('order_id',$order_id)->where('user_id',$getvalue->user_id)->where('transaction_type','05')->first();

        if(!empty($carddata)){

            $data = array(

              'gateway_id'=> 'G15552-42',
              'password'=> "V2wX5h1ov8iar0FR7ScRaePz2xZemfnF",
              'transaction_type'=> '00',
              'amount'=> $carddata->amount,
              'cardholder_name'=> $carddata->cardholder_name,
              'cc_number'=> $carddata->cc_number,
              'cc_expiry'=> $carddata->cc_expiry,
              'cvd_code'=> $carddata->cvd_code,
              'cvd_presence_ind'=> '1',
              'reference_no'=> $carddata->reference_no,
              'customer_ref'=> $order_id,
              'currency_code'=> 'USD',
              'credit_card_type'=> $carddata->credit_card_type,

    );
   
	$payload = json_encode($data, JSON_FORCE_OBJECT);

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.globalgatewaye4.firstdata.com/transaction",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>$payload,
	  CURLOPT_HTTPHEADER => array(
	    "Content-Type: application/json"
	  ),
	));


	$json_response = curl_exec($curl);	
	
	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	$response = json_decode($json_response, true);

	curl_close($curl);

	$cc_number = substr($carddata->cc_number,-4,4);

	if($response['transaction_approved']=='1'){

	DB::table('users_card')->where('order_id',$order_id)->where('user_id',$getvalue->user_id)->update([

        'cc_number' => $cc_number,
        'cvd_code' => '000',
		'transaction_type' => $response['transaction_type'],
		'transaction_tag' =>  strval($response['transaction_tag']),
		'authorization_num' => $response['authorization_num'],
		'transaction_approved' => $response['transaction_approved'],
		'bank_message' => $response['bank_message'],
		'updated_at' => date('Y-m-d H:i:s')

		]);

	 Session::flash('message', 'Payment has been successfully processed');

     }else{

    Session::flash('errormsg', $json_response); 	

    return redirect()->to('/admin/order-details/'.$id); // return redirect()->to('/merchant/order-list/'); 

     }

      }


	}

		$date = date('Y-m-d H:i:s');

		if( Input::get('return_flag') == 1){


			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$order_status = Input::get('order_status');

			$merchant_reason = Input::get('merchant_reason');

			DB::table('orders')->where('order_id',$order_id)->update(['status' =>$order_status,'status' =>$order_status,'return_reason_merchant'=> $merchant_reason,'updated_at'=> $date ]);

			return redirect()->to('/admin/return-list');

		}else{

			$order_id = Input::get('order_id');

			$id = Input::get('id');

			$order_status = Input::get('order_status');


			$cancel_reason = '';

			if(Input::get('cancel_reason'))

			{

				$cancel_reason = Input::get('cancel_reason');

			}


			DB::table('orders')->where('order_id',$order_id)->update(['status' =>$order_status, 'cancel_reason' => $cancel_reason,'updated_at'=> $date ]);

			if($order_status==2){

			 $getvalue1 = DB::table('orders_ps')->leftJoin('orders', 'orders_ps.order_id', '=', 'orders.id')->where('orders.order_id',$order_id)->select('*','orders_ps.ps_id as ps_id','orders_ps.ps_qty as ps_qty')->get();
			 	
			 	 foreach ($getvalue1 as $key => $value) {
			 	 	
			 	 	$getproductvalue = DB::table('product_service')->where('id',$value->ps_id)->first();
			 	 	

			 	 	if(!empty($getproductvalue)){
			  	

			 	 	$quantity = $getproductvalue->quantity+$value->ps_qty;

			 	 	DB::table('product_service')->where('id',$value->ps_id)->update(['quantity' =>$quantity]);

			 	 //	$soldqty = $getproductvalue->popular-$value->ps_qty;

			 	 	
                    DB::table('orders_ps')->where('order_id',$value->id)->update(['cancel_status' =>1]);
			 	 	//DB::table('orders_ps')->where('order_id',$value->id)->update(['ps_qty' =>0]);

			 	  }
				
				}
				
				}

			$order_statuss = '';


			 if($order_status==0){ $order_statuss="Pending"; }
			 if($order_status==1){ $order_statuss="Accept"; }
			 if($order_status==2){ $order_statuss="Cancelled"; }
			 if($order_status==3){ $order_statuss="On the way"; }
			 if($order_status==4){ $order_statuss="Delivered"; }

			 $getvalue = DB::table('orders')->where('order_id',$order_id)->first();

			 $user_divice_token = DB::table('users')->where('id', '=', $getvalue->user_id)->first(); 

            if($user_divice_token->devicetoken)
            {

                 $title = "Hey ".$user_divice_token->name." ".$user_divice_token->lname." your order is ".$order_statuss;

                 $tag = "Order detail";

                 $token = $user_divice_token->devicetoken;

                 $orid = $id;

                 $vid = $getvalue->vendor_id;

                 $notistatus =  $user_divice_token->notification;

                 if($notistatus==1){

                 $response = $this->sendNotification($token,$tag,$title);

                 }
                 
                $this->save_notification($user_id,$title,$orid,$tag,$vid);
					
			
            }  


			return redirect()->to('/admin/order-details/'.$id);

		}					

   }

	/* Reporting list */ 


  	public function show_reporting_list(Request $request){

	$order_detail  = DB::table('orders')->orderBy('id', 'desc')->simplePaginate(10);

	$order_count  = DB::table('orders')->get();

	$salecount = count($order_count);


  	$data_onview = array('order_detail' =>$order_detail,'vendor_id' =>0,'searchid' =>'','salecount' =>$salecount);  
	return View('admin.reporting_list')->with($data_onview);

	}

	/* End reporting list */


	 public function payment_list(Request $request){

  		if($request->id==''){

		$order_detail  = DB::table('orders')

		->orderBy('id', 'desc')

		->get();

	   }else{

       	$order_detail  = DB::table('orders')

		->where('status', '=' ,$request->id)

		->orderBy('id', 'desc')

		->get();

	   }

  		$data_onview = array('order_detail' =>$order_detail,'searchid' =>$request->id);  

		return View('admin.payment_list')->with($data_onview);

	}



	public function membership_management(Request $request)

	{

		

  		if($request->id==''){



		$vendor_detail  = DB::table('vendor')

		->orderBy('vendor_id', 'desc')

		->simplePaginate(10);



	   }



  		$data_onview = array('vendor_detail' =>$vendor_detail,'searchid' =>$request->id);  

		return View('admin.membership_management')->with($data_onview);

	}  





	public function membership_details(Request $request)

	{


		$vendor_id = $request->id;

		$vendor_detail = DB::table('vendor')

		->where('vendor_id', '=' ,$vendor_id)

		->join('membership_transactions', 'membership_transactions.stripe_id', '=', 'vendor.stripe_id')

		->first();

		

  		$data_onview = array('vendor_detail' =>$vendor_detail);  					 		

		return View('admin.membership_details')->with($data_onview);



	}


	public function membership_update(Request $request)

	{


		 $vendor_id = $request->id;

		 $vendor_detail = DB::table('vendor')

		->where('vendor_id', '=' ,$vendor_id)

		->first();

		

  		$data_onview = array('vendor_detail' =>$vendor_detail);  					 		

		return View('admin.membership_update')->with($data_onview);



	}

   public function membership_action(Request $request)

	{


		 $vendor_id = $request->vendor_id;
		 $purchase = $request->purchase;
		 $expiry = $request->expiry;
		 $plan_id = 1;


		  DB::table('vendor')

		->where('vendor_id', '=' ,$vendor_id)->update(['plan_id' => 1,'plan_purchased' => $purchase,'plan_expiry' => $expiry]); 


  		Session::flash('message', 'Plan update Sucessfully!'); 

		return redirect()->to('/admin/membership-management');


	}



	public function payment_details(Request $request)
	{

		$orderid = $request->id;

		$order_count = DB::table('orders')

		->where('id', '=' ,$orderid)

		->get();

		if(count($order_count)>0){

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->first();



		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();			



  		$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist);  					 		

		return View('admin.payment_view')->with($data_onview);

}else{
	return redirect()->to('/admin/payment-list');
}

	}

// bank details

	function payment_setting()
	{

		$payment_mode = DB::table('admin_setting')
		                         ->where('id', '=' ,1)
								 ->value('payment_mode');

													
  		$data_onview = array('payment_mode'=>$payment_mode); 
			   
    	return view('admin.payment_setting')->with($data_onview);

	} 


    public function payment_setup()
	{

		$payment_mode = Input::get('livesand');

			DB::table('admin_setting')
            ->where('id', '=' ,1)
            ->update(['payment_mode' => $payment_mode]);

			Session::flash('message', 'Payment Mode update sucessfully!'); 

			return redirect()->to('/admin/payment-setting');

	}	



	public function bank_list(Request $request){



		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;



		$vendor_bank_detail = DB::table('vendor_bank_details')->simplePaginate(10);

        $vid = count($vendor_bank_detail);														

  		$data_onview = array('vendor_bank_detail'=>$vendor_bank_detail,'vid'=>$vid); 



		return View('admin.bank_list')->with($data_onview);

	} 



	public function bank_details(Request $request)



	{



		$vendor_id = $request->id;

		$order_detail = DB::table('vendor_bank_detail')

		->where('vendor_id', '=' ,$orderid)

		->first();



		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();			



  		$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist);  					 		

		return View('admin.bank_view')->with($data_onview);



	}	





	function send_notification_ios($token,$title,$body)

	{

		$url = "https://fcm.googleapis.com/fcm/send";

		$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

		

		$headers = array();

		$headers[] = 'Content-Type: application/json';

		$headers[] = 'Authorization: key='. $serverKey;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));

		

		$result = curl_exec($ch);

		//echo $result;die;

		if ($result === FALSE) {

		return false;

		}

		curl_close($ch); 



	}

	



			function sendNotification($target, $title, $description){



			$url = 'https://fcm.googleapis.com/fcm/send';



			$server_key = 'AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1';



			$data =  array("title"=>$title, "message"=>$description);

			

			$fields = array();

			$fields['data'] = $data;



			if(is_array($target)){

			$fields['registration_ids'] = $target;

			}else{

			$fields['to'] = $target;

			}

			//header with content_type api key

			$headers = array(

			'Content-Type:application/json',

			 'Authorization:key='.$server_key

			);

			//CURL request to route notification to FCM connection server (provided by Google)	

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);

			curl_setopt($ch, CURLOPT_POST, true);

			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));



			$result = curl_exec($ch);

			//echo $result;die;

			if ($result === FALSE) {

			return false;

			}

			curl_close($ch); 



	}





	public function assign_order_action()

	{



        $status = 0;

        $arstatus = 0;

        $dm_id = trim(Input::get('dm_id'));

        $orderid = trim(Input::get('aorder_id'));



       $verification  = DB::table('order')->where('order_id', '=' ,$orderid)->get();  		

	   $vercode = $verification[0]->verification_code;

	   $order_uniqueid = $verification[0]->order_uniqueid;



	   $userData = array(

					'dm_id' => $dm_id,

					'order_id' => $orderid,

					'order_status' => $status,

					'verification_code' => $vercode,

					'accept_reject_status' => $arstatus,

					'received_order_time'=>date('y-m-d h:i:s'),

					'pickup_order_time'=>'0',

					'deliver_order_time'=>'0'

				);

  

    $dm_status = DB::table('delivery_man_orders')

		->where('delivery_man_orders.order_id', '=', $orderid)

		->select('*')

		->get();	



	  if(empty($dm_status)){ 



       $affected = DB::table('delivery_man_orders')->insert($userData); 



       $uorder = DB::table('order')->where('order_id',$orderid)->update(['order_status' => 7]); 



	   }else{



       $affected = DB::table('delivery_man_orders')->where('order_id',$orderid)->update(['dm_id' =>$dm_id,'order_status' => $status,'accept_reject_status' => $arstatus]);



	   }



			// Send delivery man notification 



           $token_data = DB::table('deliveryman_device_token')

             ->where('userid', '=', trim($dm_id))

             ->where('notification_status', '=', 1)

             ->get();



           $odr_assn_noti_cotnt = DB::table('email_content')->where('email_id',12)->first();



             $target = '';

             $devicetype = '';

             $deviceid = '';



			 if($token_data){



			 $target = $token_data[0]->devicetoken;



			 $devicetype = $token_data[0]->devicetype;	



			 $deviceid = $token_data[0]->deviceid;	



			 }

   

		   $notification_title = $odr_assn_noti_cotnt->email_title .$order_uniqueid;

	       $notification_description = $odr_assn_noti_cotnt->email_content;



			if($devicetype=='android')

			{

				$sendnotify = $this->sendNotification($target, $notification_title, $notification_description);

			}

			elseif($devicetype=='iphone')

			{

				$sendnotify = $this->send_notification_ios($target,$notification_title, $notification_description);

			}

	          



	       $Notification=array(

					'noti_userid'=>	trim($dm_id),

					'noti_guestid'=>'',

					'noti_title'=>	trim($notification_title),

					'noti_desc'=>trim($notification_description),

					'noti_deviceid'=>trim($deviceid),

					'noti_devicetype'=>	trim($devicetype),

					'noti_read'=>0,

					'created_at'=>date('Y-m-d H:i:s')

				);



			DB::table('deliveryman_notification_list')->insert($Notification); 





	   return $status;



	}	

	



	public function update_order_action()

	{

	

		$order_id = Input::get('order_id');



		$old_status = Input::get('old_status');



		$new_order_status = Input::get('order_status');

		

		$user_name ='';

		$email_content = '';

		$rest_name = '';	



		DB::enableQueryLog(); 	



		$order_cancel_reason = '';



		if(Input::get('order_cancel_reason'))

		{



			$order_cancel_reason = Input::get('order_cancel_reason');	



		}

	

		$order_detail  =  DB::table('order')			

						->where('order_id', '=', $order_id)

						->select('*')

						->orderBy('order.order_create', 'desc')

						->get();

						

		$rest_detail = DB::table('restaurant')		

						->select('*')	

						->where('rest_id', '=' ,$order_detail[0]->rest_id)

						->get();			

						

				$user_name = $order_detail[0]->order_fname.' '.$order_detail[0]->order_lname;

				$user_email = $order_detail[0]->order_email;

				$user_contact = $order_detail[0]->order_tel;

				$rest_name = $rest_detail[0]->rest_name;

								

						

			/********** SEND NOTIFCATON TO USER ACCORDING TO ORDER STAUS **************/

			

			if($new_order_status==2)  //cancel

			{

			

				

				$email_subject = 'Your order is cancel.';

				$email_content_detail = DB::table('email_content')		

										->select('*')	

										->where('email_id', '=' ,'5')

										->get();

				$emailcontent =$email_content_detail[0]->email_content;								

				$searchArray = array("order_id", "order_uniqueid", "f_name");

 

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			

			if($new_order_status==4) //Confirmed

			{

				

				

				$email_subject = 'Your order is confirmed.';

				$email_content_detail = DB::table('email_content')		

										->select('*')	

										->where('email_id', '=' ,'6')

										->get();

				$emailcontent =$email_content_detail[0]->email_content;	

				$searchArray = array("order_id", "order_uniqueid", "f_name");

 

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);



			}

			

			if($new_order_status==5) //Completed

			{

				

				

				$email_subject = 'Your order is completed.';

				$email_content_detail = DB::table('email_content')		

										->select('*')	

										->where('email_id', '=' ,'4')

										->get();

				$emailcontent =$email_content_detail[0]->email_content;

				$searchArray = array("order_id", "order_uniqueid", "f_name");

 

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);	

			

			}

			

			if($new_order_status==6) //Reject

			{

				

				$email_subject = 'Your order is reject.';

				$email_content_detail = DB::table('email_content')		

										->select('*')	

										->where('email_id', '=' ,'7')

										->get();

				$emailcontent =$email_content_detail[0]->email_content;	

				$searchArray = array("order_id", "order_uniqueid", "f_name");

 

				$replaceArray = array($order_id, $order_detail[0]->order_uniqueid, $user_name);

				$email_content = str_replace($searchArray, $replaceArray, $emailcontent);

			}

			

			

			

			$data = array(			

						  'user_name'=>$user_name,

					      'user_email'=>$user_email,				

			              'email_content'=>$email_content,				

			              'user_contact'=>$user_contact	,				

			              'email_subject'=>$email_subject,			

			              'order_detail'=>$order_detail,		

			              'rest_detail'=>$rest_detail

						  );	

			if(!empty($user_email))		

			{

				//echo 'test';	

					

			   echo Mail::send('emails.orderstatus_email', $data, function ($message) use ($data) {

				   

				   

			   $message->from('info@grambunny.com','grambunny');

			   $message->to($data['user_email']);	

						$message->bcc('votiveshweta@gmail.com');

						$message->bcc('votivemobile.pankaj@gmail.com');

						$message->subject($data['email_subject']);	

					   });	

			}	

						

			if(($order_detail[0]->user_id>0) || (!empty($order_detail[0]->order_guestid)))	

			{						

				//$user_detail  =  DB::table('users')->where('id', '=', $order_detail[0]->user_id)->get();

				

				$notification_title =$email_subject;

				$notification_description = $email_content;						

						//$token_data = DB::table('device_token')->where('userid', '=', trim($order_detail[0]->user_id))->get();

						

					if($order_detail[0]->user_id>0)	{	

						$token_data = DB::table('device_token')->where('userid', '=', trim($order_detail[0]->user_id))->where('devicetype', '=', trim($order_detail[0]->order_device))->get();

					}

					elseif(!empty($order_detail[0]->order_guestid))

					{

						$token_data = DB::table('device_token')->where('guest_id', '=', trim($order_detail[0]->order_guestid))->where('devicetype', '=', trim($order_detail[0]->order_device))->get();

					}

							if($token_data)							

							{

								$user_id = $order_detail[0]->user_id;

								$target = $token_data[0]->devicetoken;

								

								$device_type = $token_data[0]->devicetype;

								$device_id = $token_data[0]->deviceid;

								$guest_id = $order_detail[0]->order_guestid;

								

								//$this->sendNotification($target,$user_id, $notification_title, $notification_description,$device_type);							

								

								 if($device_type=='android')

								{



								$this->sendNotification($target, $notification_title, $notification_description);



								}

								elseif($device_type=='iphone')

								{



							  $this->send_notification_ios($target,$notification_title, $notification_description);



								}

											

											

								$notifiy = new Notification_list;

								$notifiy->noti_userid = $user_id;

								$notifiy->noti_guestid = $guest_id;	

								$notifiy->noti_title = $notification_title;	

								$notifiy->noti_desc = $notification_description;	

								$notifiy->noti_deviceid = $device_id;	

								$notifiy->noti_devicetype = $device_type;	

								$notifiy->noti_read = '0';

								$notifiy->save();						

								

								$noti_id = $notifiy->noti_id;	

							}					

			}

			

			

			

			

			

			

			

			/**********************************   END  *****************************/



						



		/* SAVE HISTORY OF OLD ORDER */



				$ohis = new Order_history;



				$ohis->order_id = $order_detail[0]->order_id;



				$ohis->old_status =$order_detail[0]->order_status;



				$ohis->old_status_date = $order_detail[0]->updated_at;



				$ohis->old_status_date = $order_detail[0]->updated_at;



				$ohis->save();



				



				



				



		/* UPDATE PAYMENT TABLE BASED ON ORDER CANCEL OR REJECT OR COMPLETED BY PARTNER START  */		



		



		if($new_order_status == '2' || $new_order_status == '6')



		{



			



			$payment_detail  =  DB::table('order_payment')		



							->where('pay_orderid', '=', $order_id)



							->select('*')



							->get();



			



				$pay_his = new Order_pay_history;



				$pay_his->pay_id = $payment_detail[0]->pay_id;



				$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;



				$pay_his->pay_user_id = $payment_detail[0]->pay_userid;



				$pay_his->pay_old_status = $payment_detail[0]->pay_status;



				$pay_his->old_status_date = $payment_detail[0]->created_at;



				$pay_his->order_history_created	= date('Y-m-d');



				$pay_his->save();



				



				DB::table('order_payment')->where('pay_orderid',$order_id)



								  ->update(['pay_status' =>'2' ]



								  );				



				//print_r(DB::getQueryLog());					  



		}	



		



			



		if($new_order_status == '5')



		{



			



			$payment_detail  =  DB::table('order_payment')		



							->where('pay_orderid', '=', $order_id)



							->select('*')



							->get();



			



				$pay_his = new Order_pay_history;



				$pay_his->pay_id = $payment_detail[0]->pay_id;



				$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;



				$pay_his->pay_user_id = $payment_detail[0]->pay_userid;



				$pay_his->pay_old_status = $payment_detail[0]->pay_status;



				$pay_his->old_status_date = $payment_detail[0]->created_at;



				$pay_his->order_history_created	= date('Y-m-d');



				$pay_his->save();



				



				DB::table('order_payment')->where('pay_orderid',$order_id)



								  ->update(['pay_status' =>'3' ]



								  );				



			//	print_r(DB::getQueryLog());						  



		}					  



		



		/*                            END                              */



				



				



		/* UPDATE STATUS IN TABLE START */



			DB::table('order')->where('order_id',$order_id)



							  ->update(['order_status' =>$new_order_status,



							  			'order_cancel_reason' =>$order_cancel_reason ]



							  );				



			//	print_r(DB::getQueryLog());		



			echo 'Status Updted!';

								

	}



   public function ajax_search_list()

	{



		$from_date=Input::get('fromdate');;



		$to_date=Input::get('todate');;



		$status = Input::get('order_status');



		$area_order = Input::get('area_order');



		$order_item = Input::get('order_item');



		$user_id = Input::get('user_id');

  

		

		if(!empty($order_item)){



		  	$order_detail  =  DB::table('order');		

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

			$order_detail = $order_detail->leftJoin('menu_category', 'order.rest_id', '=', 'menu_category.rest_id');

		

			$order_detail  =  $order_detail->where('menu_category.menu_category_name', '=', $order_item);

			/* Get Data between Date */

			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			$order_detail = $order_detail->where('order.order_create', '>=', date('Y-m-d 23:59:00',strtotime($from_date)));

			}	



			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.order_create', '<=',date('Y-m-d 23:59:00',strtotime($to_date)));

			}		

			/* End */

		

			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();

			//echo "<pre>"; print_r($order_detail); die;



			$new_order_detail = array();

			$orderitemval = '';

		   

		    foreach ($order_detail as $key => $orderdetail) {

		    

			$orderitems = json_decode($orderdetail->order_carditem);



			foreach ($orderitems as $key => $value) {



				if($order_item==$value->name){



               $orderitemval = $value->name;



				}



			} 



			if($orderitemval!=''){



			$new_order_detail[] = $orderdetail;



		    }



			$orderitemval = '';



		  }



		  //print_r($new_order_detail); die;



	  		$data_onview = array('order_detail'=>$new_order_detail,'orderstatus'=>$status); 

			return View('admin.ajax.order_list')->with($data_onview);

				

		 }else if(!empty($area_order)){  



			$order_detail  =  DB::table('order');		

			$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

			$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');			

			/* Get Data between Date */

			if(!empty($from_date) && ($from_date!='0000-00-00'))

			{

			$order_detail = $order_detail->where('order.order_create', '>=', date('Y-m-d 23:59:00',strtotime($from_date)));

			}	



			if(!empty($to_date) && ($to_date!='0000-00-00'))

			{

			  $order_detail = $order_detail->where('order.order_create', '<=',date('Y-m-d 23:59:00',strtotime($to_date)));

			}		

			/* End */

			$order_detail  =  $order_detail->where('order.order_deliverysurbur', '=', $area_order);		

			$order_detail = $order_detail->select('*');

			$order_detail = $order_detail->orderBy('order.order_create', 'desc');

			$order_detail = $order_detail->get();	

	  		$data_onview = array('order_detail'=>$order_detail,'orderstatus'=>$status); 

			return View('admin.ajax.order_list')->with($data_onview);	



		}else if($status!=11 && $status!=12 && $status!=13 && $status!=14 && $status!=15 && $status!=16 ){ 



		$order_detail  =  DB::table('order');		

		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

		/*USER BASE SEARC FORM USER PAGE  */

		if($user_id>0)

		{

			$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

		}

		/* Status Search */

		if(!empty($status) && ($status>0))

		{

		  $order_detail = $order_detail->where('order.order_status', '=', $status);

		}	

		/* Get Data between Date */

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		$order_detail = $order_detail->where('order.order_create', '>=', date('Y-m-d 23:59:00',strtotime($from_date)));

		}	



		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('order.order_create', '<=',date('Y-m-d 23:59:00',strtotime($to_date)));

		}		

		/* End */

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('order.order_create', 'desc');

		$order_detail = $order_detail->get();	

  		$data_onview = array('order_detail'=>$order_detail,'orderstatus'=>$status); 

		return View('admin.ajax.order_list')->with($data_onview);



	}else{





    	$order_detail  =  DB::table('order');		

		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

		/*USER BASE SEARC FORM USER PAGE  */

		if($user_id>0)

		{

			$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

		}	

		/* Get Data between Date */

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		$order_detail = $order_detail->where('order.order_create', '>=', date('Y-m-d 23:59:00',strtotime($from_date)));

		}	



		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('order.order_create', '<=',date('Y-m-d 23:59:00',strtotime($to_date)));

		}		

		/* End */



		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('order.order_create', 'desc');

		$order_detail = $order_detail->get();	

  		$data_onview = array('order_detail'=>$order_detail,'orderstatus'=>$status); 

		return View('admin.ajax.order_list')->with($data_onview);



	}



  }


	public function show_orderreport()
	{

		//echo '<pre>';

		//print_r($_POST);

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		$order_detail  =  DB::table('order');		

		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

		/*USER BASE SEARC FORM USER PAGE  */

		if($user_id>0)
		{

			$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

		}


		/* Status Search */

		if(!empty($status) && ($status>0))
		{

		  $order_detail = $order_detail->where('order.order_status', '=', $status);

		}	

		/* Get Data between Date */

		if(!empty($from_date) && ($from_date!='0000-00-00'))
		{

		  $order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));


		}	


		if(!empty($to_date) && ($to_date!='0000-00-00'))
		{

		  $order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		/* End */

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('order.order_create', 'desc');

		$order_detail = $order_detail->get();	


  		//$data_onview = array('order_detail'=>$order_detail); 


		$title = 'Order Report';

		$all_array = array();

		if(!empty($order_detail))
		{

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)
			{	

				 $sr++;

				 $status = '';

				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				 if($data_list->order_status=='2'){ $status = 'Cancelled'; }

				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; }

				 if($data_list->order_status=='6'){ $status = 'Reject'; }

				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }

				  $cart_price = 0;

				  $sumary = '';

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))
		{

			$order_carditem = json_decode($data_list->order_carditem, true);	

				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";

				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
				 {

					foreach($item['options']['addon_data'] as $addon)
					{

					 $cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 } 


			}

		}


				$all_array[] = array('0'=>$sr,



									'1'=>$data_list->order_id,



									'2'=>$data_list->order_create,



									'3'=>$data_list->rest_name,



									'4'=>$status,



									'5'=>$data_list->order_fname.' '.$data_list->order_lname,



									'6'=>$data_list->order_tel,



									'7'=>$sumary,



									'8'=>'$'.$cart_price,



									);



			}


			$data = $all_array ;


		}



		else



		{



			$data = array(



		   			array('No REcod Found')



			);



		}



		$filename = "order_report.csv";



		$fp = fopen('php://output', 'w');



		//$header[] ='';



		$header =array('',	$title, '', '');


		header('Content-type: application/csv');



		header('Content-Disposition: attachment; filename='.$filename);



		fputcsv($fp, $header);



		$header2 =array('SNo.', 'OrderNo', 'OrderReq_date', 'Restaurant','Status','UserName','Contact No', 'Order Sumary','TotalAmount');

		fputcsv($fp, $header2);

		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }	

	}


	public function show_paymentlist()
	{


		$payment_detail = '';

		$payment_detail  =  DB::table('order_payment')		



						->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')



						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')



						->leftJoin('users', 'order.user_id', '=', 'users.id')



						->where('order_payment.pay_status', '>', 0)



						->select('*')



						->orderBy('order_payment.pay_id', 'desc')



						->get();





				//echo '<pre>'		;



	//print_r($payment_detail );



	//exit;

		



  		$data_onview = array('payment_detail'=>$payment_detail); 	



		return View('admin.payment_list')->with($data_onview);



	}



	



	public function show_paymentdetail()



	{



		$pay_id = $_REQUEST['order'];



	    $payment_detail = '';



		$payment_detail  =  DB::table('order_payment')		



						->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id')



						->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')



						->leftJoin('users', 'order.user_id', '=', 'users.id')



						->where('order_payment.pay_status', '>', 0)



						->where('order_payment.pay_id', '=', $pay_id)



						->select('*','order.created_at as order_submit_on')



						->orderBy('order_payment.pay_id', 'desc')



						->get();



						



				//echo '<pre>'		;



	



	//print_r($payment_detail );



	//exit;



		if(count($payment_detail>0)){



		



		



		$order_create_date = $payment_detail[0]->order_submit_on;



		



		



	$order_history_submit =  DB::table('order_history')	



							->where('order_history.order_id', '=', $payment_detail[0]->order_id)	



							->where('order_history.old_status', '=', '1')	



							->select('*')



							->get();	



							



		if(!empty($order_history_submit))



		{



			$order_create_date	= $order_history_submit[0]->old_status_date;						



		}



		



		



		/* ORDER COMPLETED DATE */



		



		



		



		$order_can_comp_reg = $payment_detail[0]->order_submit_on;



		



		



			$order_history_complete =  DB::table('order_history')	



							->where('order_history.order_id', '=', $payment_detail[0]->order_id)	



							->where('order_history.old_status', '=', '5')	



							->select('*')



							->get();	



			if(!empty($order_history_complete))



			{



				$order_can_comp_reg	= $order_history_complete[0]->old_status_date;						



			}			



		



							/* END */



		



		



	



		$order_history_detail  =  DB::table('order_history')		



						->where('order_history.order_id', '=', $payment_detail[0]->order_id)



						->where('order_history.old_status', '=', '5')



						->select('*')



						->orderBy('order_history.order_history_id', 'desc')



						->get();



						



  		$data_onview = array('order_detail'=>$payment_detail,'order_history_detail'=>$order_history_detail,'order_create_date'=>$order_create_date,'order_can_comp_reg'=>$order_can_comp_reg); 



						



						



		//return View('admin.payment_list')->with($data_onview);



		return View('admin.payment_view')->with($data_onview);



		}



		else



		{



			



		}



	}















	public function ajax_show_paymentlist()



	{



		//echo '<pre>';



		//print_r($_POST);



		$from_date=Input::get('fromdate');;



		$to_date=Input::get('todate');;



		$status = Input::get('order_status');



		$user_id = Input::get('user_id');



		



		



		



		$order_detail  =  DB::table('order_payment')	;	



		$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');	



		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');



		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



		$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);



		



		



		



		



		/* Status Search */



		



		if(!empty($status) && ($status>0))



		{



			



			if($status==7)



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');



			}



			elseif($status==6)



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');



		  		$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '5');



			}



			else



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);



			}



		}	



			



		/* Get Data between Date */



		



		



		



		if(!empty($from_date) && ($from_date!='0000-00-00'))



		{



		  $order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));



		}	



		



		



		if(!empty($to_date) && ($to_date!='0000-00-00'))



		{



		  $order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));



		}		



		



		/* End */



		



				



						



		$order_detail = $order_detail->select('*');



		$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');



		$order_detail = $order_detail->get();	



			



		







		



  		$data_onview = array('order_detail'=>$order_detail); 



		



		return View('admin.ajax.payment_list')->with($data_onview);



		



		//print_r($data_onview);



		



	}



	



	public function show_paytmentreport()



	{



		



		$from_date=Input::get('fromdate');;



		$to_date=Input::get('todate');;



		$status = Input::get('order_status');



		$user_id = Input::get('user_id');



		



		



		$order_detail  =  DB::table('order_payment')	;	



		$order_detail = $order_detail->leftJoin('order', 'order_payment.pay_orderid', '=', 'order.order_id');			



		$order_detail = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');



		$order_detail = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');



		



		$order_detail = $order_detail->where('order_payment.pay_status', '>', 0);



		



		



		/* Status Search */



		



		if(!empty($status) && ($status>0))



		{



			



			if($status==4)



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '4');



		  		$order_detail = $order_detail->orwhere('order_payment.pay_status', '=', '3');



			}



			elseif($status==5)



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', '2');



			}



			else



			{



		  		$order_detail = $order_detail->where('order_payment.pay_status', '=', $status);



			}



		}	



			



		/* Get Data between Date */



		



		



		



		if(!empty($from_date) && ($from_date!='0000-00-00'))



		{



		  $order_detail = $order_detail->where('order_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));



		}	



		



		



		if(!empty($to_date) && ($to_date!='0000-00-00'))



		{



		  $order_detail = $order_detail->where('order_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));



		}		



		



		/* End */



		



				



						



		$order_detail = $order_detail->select('*');



		$order_detail = $order_detail->orderBy('order_payment.pay_id', 'desc');



		$order_detail = $order_detail->get();	



			



		







		



  		//$data_onview = array('order_detail'=>$order_detail); 



		



		



		$title = 'Payment Report';



		



		



		



		$all_array = array();



		if(!empty($order_detail))



		{



		



		



			$sr=0;



			$total_amt = '';



			foreach($order_detail as $data_list)



			{	$sr++;



				 



				  $payment_status = '';



				  



		$order_can_comp_reg_date	= '';



		



				if($data_list->pay_status=='1') { $payment_status = 'Complete'; }



				if($data_list->pay_status=='2') { $payment_status = 'ToRefund'; } 



				if($data_list->pay_status=='3') { $payment_status = 'ToPayout'; }



				if($data_list->pay_status=='4') { $payment_status = 'Completed Refund'; }



				if($data_list->pay_status=='5') { $payment_status = 'Completed Payout'; }



				 



				 



				 $status = '';



				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }



				 if($data_list->order_status=='2'){ $status = 'Cancelled'; $order_can_comp_reg_date	= $data_list->created_at; }



				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }



				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }



				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; $order_can_comp_reg_date	= $data_list->created_at;}



				 if($data_list->order_status=='6'){ $status = 'Reject'; $order_can_comp_reg_date	= $data_list->created_at; }



				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }



				 



				 



				  $cart_price = 0;



				  $sumary = '';



				  



				  



				  /* ORDER SUMMARY AND CALCULATE TOTAL AMOUNT WITH COMMISSION AMOUNT */



		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))



		{



			$order_carditem = json_decode($data_list->order_carditem, true);	



						



				foreach($order_carditem as $item){



				  $cart_price = $cart_price+$item['price'];



					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";



				



				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))



				 {



					foreach($item['options']['addon_data'] as $addon)



					{



						$cart_price = $cart_price+$addon['price'];



					 $sumary .=$addon['name'].' ';



					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}



					 $sumary .="\n";



					 }



				 } 



					



			}



		}



		



		



		



		



		$sumary .="\n  Sub Total : $".number_format($cart_price,2);



		$sumary .="\n";



		$sumary .="\n  Delivery Fee : $".number_format($data_list->order_deliveryfee,2);



											



		if($data_list->order_remaning_delivery>0)



		{



			$sumary .="\n";



			$sumary .="\n $".number_format($data_list->order_min_delivery,2).' min Delivery amount';



			$sumary .=" Remaining Amount :$".number_format($data_list->order_remaning_delivery,2).'';



			$sumary .="\n";



		}



		if($order_detail[0]->order_promo_cal>0)



		{



			$sumary .=" Dels : -$".number_format($data_list->order_promo_cal,2).'';



		}							 



				



				



						



					



		$promo_amt_cal= '0.00';



		



		$total_amt= '';



		



		if($data_list->order_promo_cal>0)



		{



			$promo_amt_cal= $data_list->order_promo_cal;



		}



		



		



		$total_amt= $cart_price+$data_list->order_deliveryfee-$promo_amt_cal;



		



		if(($data_list->order_remaning_delivery>0) && 



		($data_list->order_min_delivery>0) && 



		(($cart_price-$data_list->order_promo_cal)<$data_list->order_min_delivery)



		)



		{



			$total_amt= ($data_list->order_min_delivery+$data_list->order_deliveryfee);



		}	



			



		$Commission_amt = '0.00';



		if($data_list->pay_status=='3' || $data_list->pay_status=='5' )	



		{



			$Commission_amt =	number_format((($total_amt*$data_list->rest_commission)/100),2);		



		}



		



					/*  END   */



					



					



	    /*  ORDER PICKUP DATE AND TIME */				



		



		$order_picup_date = '';



		



		if(($data_list->order_typetime=='later') || ($data_list->order_pickdate!='0000-00-00'))



		{



			$order_picup_date = $data_list->order_pickdate.' '.substr($data_list->order_picktime, 0, 2).':'.substr($data_list->order_picktime, 2, 3);



		}



		else



		{



		



			$order_history =  DB::table('order_history')	



								->where('order_history.order_id', '=', $data_list->order_id)	



								->where('order_history.old_status', '=', '1')	



								->select('*')



								->get();	



										



			if(!empty($order_history))



			{



				$order_picup_date	= $order_history[0]->old_status_date;						



			}



			



		}



		



		



		



		/*    END   */



		



		



		



		/* ORDER COMPLETE /REJECT /CANCEL DATE AND TIME */



		$order_history_comp =  DB::table('order_history')	



								->where('order_history.order_id', '=', $data_list->order_id)	



								->where('order_history.old_status', '=', '5')	



								->select('*')



								->get();	



										



			if(!empty($order_history_comp))



			{



				$order_can_comp_reg_date	= $order_history_comp[0]->old_status_date;						



			}



			



		



		



		/*        END        */



		



				$all_array[] = array('0'=>$sr,



									'1'=>$data_list->order_id,



									'2'=>$data_list->order_create,



									'3'=>$data_list->rest_name,



									'4'=>$status,



									'5'=>$data_list->order_fname.' '.$data_list->order_lname,



									'6'=>$data_list->order_tel,



									'7'=>$sumary,



									'8'=>'$'.$total_amt,



									'9'=>$payment_status,



									'10'=>$order_picup_date,



									'11'=>$order_can_comp_reg_date,



									'12'=>'$'.$total_amt,



									'13'=>$data_list->rest_commission,



									'14'=>$Commission_amt,



									'15'=>$data_list->pay_tx



									);



				



			



			}



			$data = $all_array ;



		}



		else



		{



			$data = array(



		   			array('No REcod Found')



			);



		}



		



	



	



	



		$filename = "payment_report.csv";



		$fp = fopen('php://output', 'w');



		//$header[] ='';



	







		$header =array('',	$title, '', '');



			



		header('Content-type: application/csv');



		header('Content-Disposition: attachment; filename='.$filename);



		fputcsv($fp, $header);



		



		$header2 =array('SNo.', 



		'OrderNo', 



		'OrderReq_date', 



		'Restaurant',



		'Status',



		'UserName',



		'Contact No', 



		'Order Sumary',



		'TotalAmount',



		'Payment Status',



		'Order DineIn/Pickup/Delivery Date',



		'OrderComplete /Cancle /Rejcte Date',		



		'Order Payout/Refund Amount',



		'Commission',



		'Commission Amount',



		'Paypal Ref'



		



		



		);



		fputcsv($fp, $header2);



	



		



		    foreach ($data as $row) {



				fputcsv($fp, $row);



		 }



		



	}



	



	



	public function update_payment_action()



	{



		//print_r($_POST);



		



	







		



		//pay_outgoing_Ref



		



		



		$pay_id = Input::get('pay_id');



		$order_id = Input::get('order_id');



		$old_status = Input::get('old_status');



		$new_pay_status = Input::get('pay_status');



		$outgoing_reference = Input::get('outgoing_transaction_reference');



		



		



		//DB::enableQueryLog(); 



		



		



		$payment_detail  =  DB::table('order_payment')		



							->where('pay_orderid', '=', $order_id)



							->where('pay_id', '=', $pay_id)



							->select('*')



							->get();



			



		$pay_his = new Order_pay_history;



		$pay_his->pay_id = $payment_detail[0]->pay_id;



		$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;



		$pay_his->pay_user_id = $payment_detail[0]->pay_userid;



		$pay_his->pay_old_status = $payment_detail[0]->pay_status;



		$pay_his->old_status_date = $payment_detail[0]->created_at;



		$pay_his->order_history_created	= date('Y-m-d');



		$pay_his->save();







				



				



		/* UPDATE STATUS IN TABLE START */



			DB::table('order_payment')->where('pay_id',$pay_id)



							  ->update(['pay_status' =>$new_pay_status,



							  			'pay_outgoing_Ref' =>$outgoing_reference ]



							  );				



			//	print_r(DB::getQueryLog());		



			echo 'Status Updted!';



	}

	public function show_ordertoday(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
			/* $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('id', 'desc')
	        ->paginate(20); */

	        $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(20);

		    $countday = Order::whereIn('status',  array(4) )
	        ->orderBy('id', 'desc')
	        ->get();

	        $sumday = Order::whereIn('status',  array(4) )
	        ->orderBy('id', 'desc')
	        ->sum('total');

	        $data_onview = array('order_detail'=>$orders,'searchid'=>'','sumday'=>$sumday, 'countday'=>$countday,'order_date_filter'=>'');

	       	return View('admin.report_list')->with($data_onview);
		

        // $orders = Order::whereDate('created_at', NOW())->where('vendor_id', $vendor_id)

        // ->orderBy('id', 'desc')

        // ->whereIn('status',  array(4) )

        // ->paginate(10);

        
        // $week = Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('vendor_id', $vendor_id)
        // ->orderBy('id', 'desc')
        // ->whereIn('status',  array(4) )
        // ->paginate(10);


        //  $month = Order::whereMonth('created_at', NOW())->where('vendor_id', $vendor_id)
        // ->orderBy('id', 'desc')
        // ->whereIn('status',  array(4) )
        // ->paginate(10);


	}

	

	public function filter_order(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		//$order_id = Input::post('order_id');
		if(!empty($_GET['order_id'])){
			$order_id = $_GET['order_id'];
		}else{
			$order_id = Input::post('order_id');
		}
		
		if(!empty($order_id)){

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$order_id.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id,'order_id_filter'=>$order_id);

	       // $data_onview = array('order_detail'=>$orders,'order'=>$order,'searchid'=>$customer_name); 

	        if(!empty($_GET['order_id'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			} 

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}

	public function filter_order_status(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}

		if(!empty($_GET['order_date'])){
			$order_date = $_GET['order_date'];
		}else{
			$order_date = Input::post('order_date');
		}

		if(!empty($_GET['customer_name'])){
			$customer_name = $_GET['customer_name'];
		}else{
			$customer_name = Input::post('customer_name');
		}

		if(!empty($_GET['merchant'])){
			$merchant = $_GET['merchant'];
		}else{
			$merchant = Input::post('merchant');
		}

		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($order_status_serach) || ($order_status_serach==0)){
			if($order_status_serach==5){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')->select('*','orders.created_at as orderdate')->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor.name', 'LIKE', '%'.$merchant.'%' )->where('users.name', 'LIKE', '%'.$customer_name.'%' )->orderBy('orders.id', 'desc')
	        ->get();
	    		}else{
	    			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')->select('*','orders.created_at as orderdate')->where('status',  $order_status_serach )->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor.name', 'LIKE', '%'.$merchant.'%' )->where('users.name', 'LIKE', '%'.$customer_name.'%' )->orderBy('orders.id', 'desc')
	        ->get();
	    		}

		 

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_status_serach,'order_status_serach'=>$order_status_serach,'order_date_filter'=>''); 

	        if(!empty($_GET['order_status_serach'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			} 

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('status',  $order_status_serach )->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_status_serach, 'order_status_serach'=>$order_status_serach,'order_date_filter'=>''); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}

	public function filter_order_customer(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		if(!empty($_GET['customer_name'])){
			$customer_name = $_GET['customer_name'];
		}else{
			$customer_name = Input::post('customer_name');
		}

		if(!empty($_GET['order_date'])){
			$order_date = $_GET['order_date'];
		}else{
			$order_date = Input::post('order_date');
		}

		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}

		if(!empty($_GET['merchant'])){
			$merchant_name = $_GET['merchant'];
		}else{
			$merchant_name = Input::post('merchant');
		}

		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($customer_name)){

			if($order_status_serach==5){

			$order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->where('vendor.name', 'LIKE', '%'.$merchant_name.'%')->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get();
	    }else{

	    	$order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->where('vendor.name', 'LIKE', '%'.$merchant_name.'%')->where('orders.status', 'LIKE', '%'.$order_status_serach.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get();


	       }

	       if($order_status_serach==5){

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);

	        }else{

	        	$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->where('orders.status', 'LIKE', '%'.$order_status_serach.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);

	        }

			// $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        // ->orderBy('orders.id', 'desc')
	        // ->whereBetween('orders.created_at', [$data_to, $data_from])
	        // ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        // ->get();

			// $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        // ->orderBy('orders.id', 'desc')
	        // ->whereBetween('orders.created_at', [$data_to, $data_from])
	        // ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        // ->paginate(10);
	        $data_onview = array('order_detail'=>$orders,'order'=>$order,'searchid'=>$customer_name,'customer_name'=>$customer_name); 

	        if(!empty($_GET['customer_name'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			}
	        	
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);
	        $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_name); 
	        return View('admin.ajax.filter')->with($data_onview);	
		}
	}

	public function filter_order_id(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$customer_id = Input::post('customer_id');
		
		if(!empty($customer_id)){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->where('users.id', 'LIKE', '%'.$customer_id.'%' )->select('*',"users.id as userid")
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_id); 

	        return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_id); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}


	public function filter_order_merchant(Request $request)

	{

		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}
		
		if(!empty($_GET['merchant'])){
			$merchant = $_GET['merchant'];
		}else{
			$merchant = Input::post('merchant');
		}

		if(!empty($_GET['order_date'])){
			$order_date = $_GET['order_date'];
		}else{
			$order_date = Input::post('order_date');
		}

		if(!empty($_GET['customer_name'])){
			$customer_name = $_GET['customer_name'];
		}else{
			$customer_name = Input::post('customer_name');
		}

		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($merchant)){

			if($order_status_serach==5){

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
			->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
	        ->where('vendor.name', 'LIKE', '%'.$merchant.'%' )
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )
	        ->get();

	    }else{

	    	$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	    	->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
	        ->where('vendor.name', 'LIKE', '%'.$merchant.'%' )
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )
	        ->where('orders.status', 'LIKE', '%'.$order_status_serach.'%' )
	        ->get();
	    }
			// $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        // ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        // ->whereBetween('orders.created_at', [$data_to, $data_from])
	        // ->where('vendor.name', 'LIKE', '%'.$customer_name.'%' )
	        // ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_name, 'merchant'=>$customer_name); 

	        if(!empty($_GET['merchant'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			}

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_name); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}


	public function filter_order_coupon(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		//$customer_name = Input::post('merchant');
		if(!empty($_GET['coupon'])){
			$coupon = $_GET['coupon'];
		}else{
			$coupon = Input::post('coupon');
		}
		
		
		if(!empty($coupon)){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('coupon_code', 'coupon_code.id', '=', 'orders.coupon_id')
			->where('coupon_code.coupon', 'LIKE', '%'.$coupon.'%' )->select('*','orders.created_at as orderdate')
	        ->orderBy('orders.id', 'desc')
	        ->paginate(10);


	        $data_onview = array('order_detail'=>$orders,'searchid'=>$coupon, 'coupon'=>$coupon); 

	        if(!empty($_GET['coupon'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			}

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$coupon); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}

	public function filter_order_date(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		if(!empty($_GET['customer_name'])){
            $customer_name = $_GET['customer_name'];
        }else{
            $customer_name = Input::post('customer_name');
        }

        if(!empty($_GET['merchant'])){
            $merchant_name = $_GET['merchant'];
        }else{
            $merchant_name = Input::post('merchant');
        }

       // $order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
           
		}else{
			$order_status_serach = Input::post('order_status_serach');
			
		}
		$order_date = Input::post('order_date');

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(!empty($order_date)){

			 if($order_status_serach==5){
		  $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
			->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor.name', 'LIKE', '%'.$merchant_name.'%')->where('users.name', 'LIKE', '%'.$customer_name.'%')->select('*','orders.created_at as orderdate')
	        ->get();
           }else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
			->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])->where('vendor.name', 'LIKE', '%'.$merchant_name.'%')->where('users.name', 'LIKE', '%'.$customer_name.'%' )->where('orders.status', 'LIKE', '%'.$order_status_serach.'%' )->select('*','orders.created_at as orderdate')
	        ->get();
	    }

			// $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        // ->orderBy('orders.id', 'desc')
	        // ->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
	        // ->get();

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date,'order_date'=>$order_date); 

	         if(!empty($_GET['order_date'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.filter')->with($data_onview);
			}

	       // return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->orderBy('id', 'desc')
	        ->get();

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date); 

	        return View('admin.ajax.filter')->with($data_onview);	
		}

	}

	public function export_in_excel(Request $request){

		$order_detail  = DB::table('orders')->orderBy('id', 'desc')->get();


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'Merchant ID', 'Merchant Name', 'Customer ID', 'Customer Name', 'Order Date', 'Order Status', 'Order Sub-Total', 'Order Discount', 'Order Delivery Fee', 'Order Excise Tax', 'Order City Tax', 'Order Sales Tax', 'Total Order Amount', 'Payment Type', 'Transaction ID', 'Payment Status', 'Coupon Code', 'Commission Rate(%)', 'Merchant Amount'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

     $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); 

        $lineData = array($value->order_id, $value->vendor_id, $merchant_name, $value->user_id, $value->first_name.' '.$value->last_name, $createdat, $orstatus, $value->sub_total, $value->promo_amount, $value->delivery_fee, $value->excise_tax, $value->city_tax, $value->service_tax, $value->total, $value->payment_method, $value->txn_id, $value->pay_status, $coupon_code, $commission_rate, $tmerchantamnt);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
		
	}

	public function mertc_flat_file(Request $request){

		$order_detail  = DB::table('orders')->orderBy('id', 'desc')->get();

		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
	
		$fields = array('Sales Date/Time', 'Customer_type', 'Patient License Number', 'Caregiver License Number', 'Identification Method', 'Package Label', 'Quantity', 'Unit Of Measure', 'Unit of THC Percent', 'Unit of THC Content', 'Unit of THC Content Unit of THC Percent', 'Unit Weight', 'Unit Weight Unit of Measure', 'Total Amount', 'Invoice Number', 'Price', 'Excise Tax', 'City Tax', 'Country Tax', 'Municipal Tax', 'Discount Amount', 'SubTotal', 'Sales Tax'); 

		$excelData = implode("\t", array_values($fields)) . "\n";

		// $excelData .= '<style>th { background-color: #A0A0A0; }</style>'; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();

		$users = DB::table('users')->where('id','=',$value->user_id)->first();

		/*$product_service = DB::table('product_service')->where('vendor_id','=',$value->vendor_id)->first();

		   if (isset($product_service->product_code)) {
           $product_code = $product_service->product_code;
              } else {
             $product_code = '';
            } */

           if (isset($product_service->unit)) {
           $product_unit = $product_service->unit;
               } else {
              $product_unit = '';
           }

        if(isset($product_service->potency_thc)){$potency_thc = $product_service->potency_thc;}else{$potency_thc ='';}
		
		$leave_blank = '';

		$leave_blank1 = '""';

	$productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');

    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

    $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); 

      if(isset($value->coupon_id)){

      $coupon_id = $value->coupon_id;

      $dtype = DB::table('coupon_code')->where('id','=',$coupon_id)->value('discount');

      $camount = DB::table('coupon_code')->where('id','=',$coupon_id)->value('amount');

      $totalitem = DB::table('orders_ps')->where('order_id','=',$value->id)->sum('ps_qty');

      $capply = 1;   

      }else{ $capply = 0; } 


       foreach ($productinfo as $key => $pdata) {

       $totalprice = 0;	

       if($capply==1){

       $itemprice = $pdata->ps_qty*$pdata->price;

	   if($dtype==1){ //Percentage

	   $itemdiscount = ($itemprice*$camount)/100;	

       $totalprice = $itemprice-$itemdiscount; 

       }else{ 

       	$per_item = $camount/$totalitem;

       	$itemdiscount = $pdata->ps_qty*$per_item;
 
        $totalprice = $itemprice-$itemdiscount;

       }

      }else{

       $totalprice = $pdata->ps_qty*$pdata->price;

      } 

       $lineData = array($createdat, $users->customer_type, $leave_blank, $leave_blank, $leave_blank, $product_code, $pdata->ps_qty, 'Each', $leave_blank,$leave_blank, $potency_thc, $leave_blank, $product_unit, $totalprice, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1);

        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
        
			    }     

			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 
         
		header("Content-Type: application/vnd.ms-excel"); 

		header("Content-Disposition: attachment; filename=\"$fileName\"");
		 
		echo $excelData; 
		 
		exit;
		
	}


		public function METRC_in_excel_filter(Request $request){
			
		$searchid = $request->searchid;

		$filter = $request->filter;

		
		if(!empty($searchid) && $filter==='order_id_export'){

			$order_detail  = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$searchid.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	    }

		if(!empty($searchid) && $filter==='order_customer_export'){

			$order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	    }

	    if(!empty($searchid) && $filter==='order_merchant_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->where('vendor.name', 'LIKE', '%'.$searchid.'%')->select('*','orders.created_at as orderdate')->orderBy('orders.id', 'desc')
	        ->get();

	    }

	    if(isset($searchid) && $filter==='order_status_export'){
			
		    		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->where('status',  $searchid)->orderBy('id', 'desc')
		        ->get();

	    	
	    }
	    
	    if(!empty($searchid) && $filter==='order_date_export'){


	    		$date = explode('_', $searchid);

				$data_to = $date[0];

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

					$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();


				$date = explode('_', $searchid);

				$data_to = $date[0];

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

					$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();			
				
		}

		if(!empty($searchid) && $filter=='order_coupon_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('coupon_code', 'coupon_code.id', '=', 'orders.coupon_id')
			->where('coupon_code.coupon', 'LIKE', '%'.$searchid.'%' )->select('*','orders.created_at as orderdate',"orders.id as id")
	        ->orderBy('orders.id', 'desc')
	        ->get();

	    }
		

		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Sales Date/Time', 'Customer_type', 'Patient License Number', 'Caregiver License Number', 'Identification Method', 'Package Label', 'Quantity', 'Unit Of Measure', 'Unit of THC Percent', 'Unit of THC Content', 'Unit of THC Content Unit of THC Percent', 'Unit Weight', 'Unit Weight Unit of Measure', 'Total Amount', 'Invoice Number', 'Price', 'Excise Tax', 'City Tax', 'Country Tax', 'Municipal Tax', 'Discount Amount', 'SubTotal', 'Sales Tax'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 

			if($order_detail){ 
			
foreach ($order_detail as $key => $value) { 
$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();

		$users = DB::table('users')->where('id','=',$value->user_id)->first();

		$product_service = DB::table('product_service')->where('vendor_id','=',$value->vendor_id)->first();

		   if (isset($product_service->product_code)) {
           $product_code = $product_service->product_code;
              } else {
             $product_code = '';
            }

           if (isset($product_service->unit)) {
           $product_unit = $product_service->unit;
               } else {
              $product_unit = '';
           }

        if(isset($product_service->potency_thc)){$potency_thc = $product_service->potency_thc;}else{$potency_thc ='';}
		
		$leave_blank = '';

		$leave_blank1 = '""';

	$productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');
    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

     $orderdate = date("Y-m-d g:iA", strtotime($value->orderdate)); 

      if(isset($value->coupon_id)){

      $coupon_id = $value->coupon_id;

      $dtype = DB::table('coupon_code')->where('id','=',$coupon_id)->value('discount');

      $camount = DB::table('coupon_code')->where('id','=',$coupon_id)->value('amount');

      $totalitem = DB::table('orders_ps')->where('order_id','=',$value->id)->sum('ps_qty');

      $capply = 1;   

      }else{ $capply = 0; } 

       foreach ($productinfo as $key => $pdata) {

       $totalprice = 0;	

       if($capply==1){

       $itemprice = $pdata->ps_qty*$pdata->price;

	   if($dtype==1){ //Percentage

	   $itemdiscount = ($itemprice*$camount)/100;	

       $totalprice = $itemprice-$itemdiscount; 

       }else{ 

       	$per_item = $camount/$totalitem;

       	$itemdiscount = $pdata->ps_qty*$per_item;
 
        $totalprice = $itemprice-$itemdiscount;

       }

      }else{

       $totalprice = $pdata->ps_qty*$pdata->price;

      } 

       $lineData = array($orderdate, $users->customer_type, $leave_blank, $leave_blank, $leave_blank, $product_code, $pdata->ps_qty, 'Each', $leave_blank,$leave_blank, $potency_thc, $leave_blank, $product_unit, $totalprice, '""', $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1, $leave_blank1);

        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
        
			    }  

			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;

	}

// 	public function mertc_flat_file(Request $request){

// 		$vendor_id =   $request->vendor_id;

// 		$order_id =   $request->id;

// 		$user_id =   $request->user_id;
	
// 		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 

// 		if(!empty($vendor_id)){ 
			 
// 	$vendors = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

// 	$orders = DB::table('orders')->where('id','=',$order_id)->first();

// 	$users = DB::table('users')->where('id','=',$user_id)->first();

// 	$product_service = DB::table('product_service')->where('vendor_id','=',$vendor_id)->first();

// 	$permit_number =$vendors->permit_number;

// 	$leave_blank = "Leave Blank";

// 	$ps_qty =   $orders->ps_qty;
   
   //  $created_at =$vendors->created_at;

   //  $invoice_number = rand();

   //  // $commission_rate = $vendors->commission_rate;

   //  //  $com_amount = $orders->total*$commission_rate/100;
   //  //  $merchant_amount = $value->total - $com_amount; 
   //  //  $tmerchantamnt = number_format($merchant_amount, 2); 

   //  $coupon_code = '';

   //  if(!empty($value->coupon_id)){

   //   $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

   //   } 

   // $fields = array('','Description','Data Simple'); 

// 		$excelData = implode("\t", array_values($fields)) . "\n"; 
   //     $lineData[] = '';
   //        $lineData[] =  "\n"; 

   //        $lineData['Sales Date/Time'] = 'Sales Date/Time';
   //        $lineData['created_at'] = $created_at . "\n";

   //        $lineData['Customer_type'] = 'Customer_type';
   //        $lineData['Customer_type1'] = $users->customer_type. "\n";

   //        $lineData['Patient License Numbers'] = 'Patient License Number';
   //        $lineData['Patient License Number1'] = ''. "\n";

   //        $lineData['Caregiver License Numbers'] = 'Caregiver License Number';
   //        $lineData['Caregiver License Number1'] = ''. "\n";

   //        $lineData['Identification Methods'] = 'Identification Method';
   //        $lineData['Identification Method1'] = ''. "\n";

   //        $lineData['Package Label'] = 'Package Label';
   //        $lineData['Package Label'] = $product_service->product_code. "\n";

   //        $lineData['Quantitys'] = 'Quantity';
   //        $lineData['Quantity1'] = $ps_qty . "\n";

   //        $lineData['Unit Of Measures'] = 'Unit Of Measure';
   //        $lineData['Unit Of Measure1'] = $product_service->unit. "\n";

   //        $lineData['Unit of THC Percents'] = 'Unit of THC Percent';
   //        $lineData['Unit of THC Percent1'] = ''. "\n";

   //        $lineData['Unit of THC Contents'] = 'Unit of THC Content';
   //        $lineData['Unit of THC Content1'] = ''. "\n";

   //        $lineData['Unit of THC Content Unit of THC Percents'] = 'Unit of THC Content Unit of THC Percent';
   //        $lineData['Unit of THC Content Unit of THC Percent1'] = $product_service->potency_thc. "\n";

   //        $lineData['Unit Weights'] = 'Unit Weight';
   //        $lineData['Unit Weight1'] = ''. "\n";

   //        $lineData['Unit Weight Unit of Measures'] = 'Unit Weight Unit of Measure';
   //        $lineData['Unit Weight Unit of Measure1'] = $product_service->unit. "\n";

   //        $lineData['Total Amounts'] = 'Total Amount';
   //        $lineData['Total Amount1'] = $orders->total. "\n";

   //        $lineData['Invoice Numbers'] = 'Invoice Number';
   //        $lineData['Invoice Number1'] = $orders->order_id. "\n";

   //        $lineData['Prices'] = 'Price';
   //        $lineData['Price1'] = ''. "\n";

   //        $lineData['Excise Taxs'] = 'Excise Tax';
   //        $lineData['Excise Tax1'] = ''.  "\n";

   //        $lineData['City Taxs'] = 'City Tax';
   //        $lineData['City Tax1'] = ''. "\n";

   //         $lineData['Country Taxs'] = 'Country Tax';
   //         $lineData['Country Tax1'] = ''. "\n";

   //        $lineData['Municipal Taxs'] = 'Municipal Tax';
   //        $lineData['Municipal Tax1'] = ''. "\n";

   //        $lineData['Discount Amounts'] = 'Discount Amount';
   //        $lineData['Discount Amount1'] = ''. "\n";

   //        $lineData['SubTotals'] = 'SubTotal';
   //        $lineData['SubTotal1'] = '' . "\n";

   //        $lineData['Sales Taxs'] = 'Sales Tax';
   //         $lineData['Sales Tax1'] = '';
        
     
   //      $excelData .= implode("\t", array_values($lineData)) . "\n"; 

// 			}else{ 

// 			    $excelData .= 'No records found...'. "\n"; 
// 			} 
// 					// Headers for download 
// 		header("Content-Type: application/vnd.ms-excel"); 
// 		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
// 		// Render excel data 
// 		echo $excelData; 
		 
// 		exit;
		
// 	}


	public function export_in_excel_filter(Request $request){
			
		$searchid = $request->searchid;

		$filter = $request->filter;

		
		if(!empty($searchid) && $filter==='order_id_export'){

			$order_detail  = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$searchid.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	    }

		if(!empty($searchid) && $filter==='order_customer_export'){

			$order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	    }

	    if(!empty($searchid) && $filter==='order_merchant_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->where('vendor.name', 'LIKE', '%'.$searchid.'%')->select('*','orders.created_at as orderdate')->orderBy('orders.id', 'desc')
	        ->get();

	    }

	    if(isset($searchid) && $filter==='order_status_export'){
			
		    		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->where('status',  $searchid)->orderBy('id', 'desc')
		        ->get();

	    	
	    }
	    
	    if(!empty($searchid) && $filter==='order_date_export'){

				$date = explode('_', $searchid);

				$data_to = $date[0];

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

					$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();


				
		}

		if(!empty($searchid) && $filter=='order_coupon_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('coupon_code', 'coupon_code.id', '=', 'orders.coupon_id')
			->where('coupon_code.coupon', 'LIKE', '%'.$searchid.'%' )->select('*','orders.created_at as orderdate',"orders.id as id")
	        ->orderBy('orders.id', 'desc')
	        ->get();

	    }
		

		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'Merchant ID', 'Merchant Name', 'Customer ID', 'Customer Name', 'Order Date', 'Order Status', 'Order Sub-Total', 'Order Discount', 'Order Delivery Fee', 'Order Excise Tax', 'Order City Tax', 'Order Sales Tax', 'Total Order Amount', 'Payment Type', 'Transaction ID', 'Payment Status', 'Coupon Code', 'Commission Rate(%)', 'Merchant Amount'); 

		$excelData = implode("\t", array_values($fields)) . "\n"; 	

			if($order_detail){ 
			
			foreach ($order_detail as $key => $value) { 

			$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $user_last_name = DB::table('orders')->where('user_id','=',$value->user_id)->value('last_name');

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

        $orderdate = date("Y-m-d g:iA", strtotime($value->orderdate));

        $lineData = array($value->order_id, $value->vendor_id, $merchant_name, $value->user_id, $value->first_name.' '.$user_last_name, $orderdate, $orstatus, $value->sub_total, $value->promo_amount, $value->delivery_fee, $value->excise_tax, $value->city_tax, $value->service_tax, $value->total, $value->payment_method, $value->txn_id, $value->pay_status, $coupon_code, $commission_rate, $tmerchantamnt);


       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;

	}

	public function all_order_list(Request $request){
		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->orderBy('id', 'desc')->get();

		$order_detail = array('order_detail'=>$order_detail); 


		return View('admin.allorderlist')->with($order_detail);	
	}

	public function filter_order_data(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_data = Input::post('order_data');
		
		if(!empty($order_data)){

			if($order_data == 'all'){
				$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->orderBy('id', 'desc')->get();
			}else{
				$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->orderBy('id', 'desc')->paginate($order_data);
			}
			

	        $data_onview = array('order_detail'=>$order_detail,'order_data'=>$order_data); 

	        if(!empty($_GET['order_data'])){
				return View('admin.allorderlist')->with($data_onview);
			}else{
				return View('admin.ajax.filterorderdata')->with($data_onview);
			}

	        //return View('admin.ajax.filterorderdata')->with($data_onview);	
	        
		}else{
			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->orderBy('id', 'desc')->paginate(10);

	          $data_onview = array('order_detail'=>$order_detail); 

	        return View('admin.ajax.filterorderdata')->with($data_onview);	
		}

	}		
	
   	public function show_product_report(Request $request)
	{
		//$getdata = $request->all();
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('id', 'desc')
	        ->paginate(20);

	        $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(20);

		    $countday = Order::whereIn('status',  array(4) )
	        ->orderBy('id', 'desc')
	        ->get();

	        $sumday = Order::whereIn('status',  array(4) )
	        ->orderBy('id', 'desc')
	        ->sum('total');

	        $data_onview = array('order_detail'=>$orders,'searchid'=>'','sumday'=>$sumday, 'countday'=>$countday,'order_date_filter'=>'');

	       	return View('admin.product_report_list')->with($data_onview);
		

	}

 	public function product_export_in_excel(Request $request){

		$order_detail  = DB::table('orders')->orderBy('id', 'desc')->get();


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'UPC/UID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'List Price', 'Discount', 'Price', 'Order Date');

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

    $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); 

    $productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');
    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

      if(isset($value->coupon_id)){

      $coupon_id = $value->coupon_id;

      $dtype = DB::table('coupon_code')->where('id','=',$coupon_id)->value('discount');

      $camount = DB::table('coupon_code')->where('id','=',$coupon_id)->value('amount');

      $totalitem = DB::table('orders_ps')->where('order_id','=',$value->id)->sum('ps_qty');

      $capply = 1;   

      }else{ $capply = 0; } 


    $username = DB::table('users')->where('id','=',$value->user_id)->value('name');

    $userlname = DB::table('users')->where('id','=',$value->user_id)->value('lname');

     foreach ($productinfo as $key => $pdata) {

       $totalprice = 0;	

       if($capply==1){

       $itemprice = $pdata->ps_qty*$pdata->price;

	   if($dtype==1){ //Percentage

	   $itemdiscount = ($itemprice*$camount)/100;	

       $totalprice = $itemprice-$itemdiscount; 

       }else{ 

       	$per_item = $camount/$totalitem;

       	$itemdiscount = $pdata->ps_qty*$per_item;
 
        $totalprice = $itemprice-$itemdiscount;

       }

      }else{

       $itemdiscount = 0;	

       $totalprice = $pdata->ps_qty*$pdata->price;

      } 

	   //$totalprice = '$'.$pdata->ps_qty*$pdata->price;
	   //$unitprice = '$'.$pdata->price;

       $totalprice = number_format($totalprice, 2);
       $itemdiscount = number_format($itemdiscount, 2); 

        $lineData = array($value->order_id, $product_code, $merchant_name, $username.' '.$userlname, $orstatus, $pdata->name, $pdata->ps_qty,$pdata->price, $itemdiscount, $totalprice, $createdat);

       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}    

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
		
	}


	public function product_export_in_excel_filter(Request $request){
			
		$searchid = $request->searchid;

		$filter = $request->filter;

		
		if(!empty($searchid) && $filter==='order_id_export'){

			$order_detail  = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$searchid.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	    }

		if(!empty($searchid) && $filter==='order_customer_export'){

			$order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	     if(count($order_detail)==0){

          $order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.lname', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	     }   

	    }


	    if(!empty($searchid) && $filter==='order_merchant_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->where('vendor.name', 'LIKE', '%'.$searchid.'%')->select('*','orders.created_at as orderdate')->orderBy('orders.id', 'desc')
	        ->get();

	    }

	    if(isset($searchid) && $filter==='order_status_export'){
			
		    		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->where('status',  $searchid)->orderBy('id', 'desc')
		        ->get();

	    	
	    }
	    
	    if(!empty($searchid) && $filter==='order_date_export'){

	    //$order_date = Session::get('order_date');
	    $order_id = Session::get('order_id');
	    $order_status = Session::get('order_status');
	    $customer_name = Session::get('customer_name');
	    $merchant_name = Session::get('merchant_name');

				$date = explode('_', $searchid);

				$data_to = $date[0];

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

                if($order_status==5){

					$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();

			 }else{

             		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->where('orders.status',  $order_status)
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();

			 }		
				
		}

		if(!empty($searchid) && $filter=='order_coupon_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('coupon_code', 'coupon_code.id', '=', 'orders.coupon_id')
			->where('coupon_code.coupon', 'LIKE', '%'.$searchid.'%' )->select('*','orders.created_at as orderdate',"orders.id as id")
	        ->orderBy('orders.id', 'desc')
	        ->get();

	    }
		

		// Excel file name for download 
	$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 

	$fields = array('Order ID', 'UPC/UID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'List Price', 'Discount', 'Price', 'Order Date'); 

	$excelData = implode("\t", array_values($fields)) . "\n"; 

	if($order_detail){ 
			
	foreach ($order_detail as $key => $value) { 

	$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   
    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;

    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $user_last_name = DB::table('orders')->where('user_id','=',$value->user_id)->value('last_name');

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

    $orderdate = date("Y-m-d g:iA", strtotime($value->orderdate));

    $productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');

    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

      if(isset($value->coupon_id)){

      $coupon_id = $value->coupon_id;

      $dtype = DB::table('coupon_code')->where('id','=',$coupon_id)->value('discount');

      $camount = DB::table('coupon_code')->where('id','=',$coupon_id)->value('amount');

      $totalitem = DB::table('orders_ps')->where('order_id','=',$value->id)->sum('ps_qty');

      $capply = 1;   

      }else{ $capply = 0; } 


    $username = DB::table('users')->where('id','=',$value->user_id)->value('name');

    $userlname = DB::table('users')->where('id','=',$value->user_id)->value('lname');

      foreach ($productinfo as $key => $pdata) {

      $totalprice = 0;	

       if($capply==1){

       $itemprice = $pdata->ps_qty*$pdata->price;

	   if($dtype==1){ //Percentage

	   $itemdiscount = ($itemprice*$camount)/100;	

       $totalprice = $itemprice-$itemdiscount; 

       }else{ 

       	$per_item = $camount/$totalitem;

       	$itemdiscount = $pdata->ps_qty*$per_item;
 
        $totalprice = $itemprice-$itemdiscount;

       }

      }else{

       $itemdiscount = 0;
       $totalprice = $pdata->ps_qty*$pdata->price;

      } 

	   $totalprice = number_format($totalprice, 2);
	   $itemdiscount = number_format($itemdiscount, 2); 

        $lineData = array($value->order_id, $product_code, $merchant_name, $username.' '.$userlname, $orstatus, $pdata->name, $pdata->ps_qty,$pdata->price, $itemdiscount, $totalprice, $orderdate);

       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}    

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;

	}	

   	public function metrc_export_in_excel(Request $request){

		$order_detail  = DB::table('orders')->orderBy('id', 'desc')->get();


		// Excel file name for download 
		$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 
		$fields = array('Order ID', 'UPC/UID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'Unit Price', 'Order Date');

		$excelData = implode("\t", array_values($fields)) . "\n"; 

		if($order_detail){ 
			
		foreach ($order_detail as $key => $value) { 

		$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   

    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;


    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

    $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); 

    $productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');
    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

    $username = DB::table('users')->where('id','=',$value->user_id)->value('name');

    $userlname = DB::table('users')->where('id','=',$value->user_id)->value('lname');

     foreach ($productinfo as $key => $pdata) {

	   $totalprice = '$'.$pdata->ps_qty*$pdata->price;
	   $unitprice = '$'.$pdata->price;

        $lineData = array($value->order_id, $product_code, $merchant_name, $username.' '.$userlname, $orstatus, $pdata->name, $pdata->ps_qty, $unitprice, $createdat);

       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}    

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;
		
	}


	public function metrc_export_in_excel_filter(Request $request){
			
		$searchid = $request->searchid;

		$filter = $request->filter;

		
		if(!empty($searchid) && $filter==='order_id_export'){

			$order_detail  = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$searchid.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	    }

		if(!empty($searchid) && $filter==='order_customer_export'){

			$order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	     if(count($order_detail)==0){

          $order_detail  = $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.lname', 'LIKE', '%'.$searchid.'%')->select('*',"users.id as userid","orders.id as id",'orders.created_at as orderdate')
	        ->get();

	     }   

	    }


	    if(!empty($searchid) && $filter==='order_merchant_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->where('vendor.name', 'LIKE', '%'.$searchid.'%')->select('*','orders.created_at as orderdate')->orderBy('orders.id', 'desc')
	        ->get();

	    }

	    if(isset($searchid) && $filter==='order_status_export'){
			
		    		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->where('status',  $searchid)->orderBy('id', 'desc')
		        ->get();

	    	
	    }
	    
	    if(!empty($searchid) && $filter==='order_date_export'){

	    //$order_date = Session::get('order_date');
	    $order_id = Session::get('order_id');
	    $order_status = Session::get('order_status');
	    $customer_name = Session::get('customer_name');
	    $merchant_name = Session::get('merchant_name');

				$date = explode('_', $searchid);

				$data_to = $date[0];

				$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));

                if($order_status==5){

					$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();

			 }else{

             		$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
					->orderBy('orders.id', 'desc')
					->where('orders.status',  $order_status)
					->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
					->get();

			 }		
				
		}

		if(!empty($searchid) && $filter=='order_coupon_export'){

			$order_detail = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('coupon_code', 'coupon_code.id', '=', 'orders.coupon_id')
			->where('coupon_code.coupon', 'LIKE', '%'.$searchid.'%' )->select('*','orders.created_at as orderdate',"orders.id as id")
	        ->orderBy('orders.id', 'desc')
	        ->get();

	    }
		

		// Excel file name for download 
	$fileName = "Orders_" . date('Y-m-d') . ".xls"; 
		 
		// Column names 

	$fields = array('Order ID', 'UPC/UID', 'Merchant', 'Customer', 'Status', 'Product Name', 'Qty', 'Price', 'Order Date'); 

	$excelData = implode("\t", array_values($fields)) . "\n"; 

	if($order_detail){ 
			
	foreach ($order_detail as $key => $value) { 

	$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   
    $merchant_name =$vendors->name.' '.$vendors->last_name;

    $commission_rate = $vendors->commission_rate;

    if($value->status==0){ $orstatus = 'Pending'; }
    if($value->status==1){ $orstatus = 'Accept'; }
    if($value->status==2){ $orstatus = 'Cancelled'; }
    if($value->status==3){ $orstatus = 'On the way'; }
    if($value->status==4){ $orstatus = 'Delivered'; }
    if($value->status==5){ $orstatus = 'Requested for return'; }
    if($value->status==6){ $orstatus = 'Return request accepted'; }
    if($value->status==7){ $orstatus = 'Return request declined'; }

    $com_amount = $value->total*$commission_rate/100;
    $merchant_amount = $value->total - $com_amount; 
    $tmerchantamnt = number_format($merchant_amount, 2); 

    $user_last_name = DB::table('orders')->where('user_id','=',$value->user_id)->value('last_name');

    $coupon_code = '';

    if(!empty($value->coupon_id)){

     $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->value('coupon');

     } 

    $orderdate = date("Y-m-d g:iA", strtotime($value->orderdate));

    $productinfo = DB::table('orders_ps')->where('order_id','=',$value->id)->get();

    $dps_id = DB::table('orders_ps')->where('order_id','=',$value->id)->value('ps_id');

    $product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

    $username = DB::table('users')->where('id','=',$value->user_id)->value('name');

    $userlname = DB::table('users')->where('id','=',$value->user_id)->value('lname');

      foreach ($productinfo as $key => $pdata) {

	   $totalprice = '$'.$pdata->ps_qty*$pdata->price;
	   $unitprice = '$'.$pdata->price;    

        $lineData = array($value->order_id, $product_code, $merchant_name, $username.' '.$userlname, $orstatus, $pdata->name, $pdata->ps_qty, $unitprice, $orderdate);

       // array_walk($lineData, 'filterData'); 
        $excelData .= implode("\t", array_values($lineData)) . "\n"; 
			    } 

			}    

			}else{ 

			    $excelData .= 'No records found...'. "\n"; 
			} 

			
					// Headers for download 
		header("Content-Type: application/vnd.ms-excel"); 
		header("Content-Disposition: attachment; filename=\"$fileName\""); 
		 
		// Render excel data 
		echo $excelData; 
		 
		exit;

	}	

	public function product_filter_order(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		//$order_id = Input::post('order_id');
		if(!empty($_GET['order_id'])){
			$order_id = $_GET['order_id'];
		}else{
			$order_id = Input::post('order_id');
		}
		
		if(!empty($order_id)){

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('order_id','LIKE','%'.$order_id.'%')->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id,'order_id_filter'=>$order_id);

	       // $data_onview = array('order_detail'=>$orders,'order'=>$order,'searchid'=>$customer_name); 

	        if(!empty($_GET['order_id'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.product_filter')->with($data_onview);
			} 

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->orderBy('id', 'desc')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_id); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}

	}

	public function product_filter_order_status(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_status_serach = Input::post('order_status_serach');

		if(!empty($_GET['order_status_serach'])){
			$order_status_serach = $_GET['order_status_serach'];
		}else{
			$order_status_serach = Input::post('order_status_serach');
		}
		
		if(!empty($order_status_serach) || ($order_status_serach==0)){
			if($order_status_serach==5){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->orderBy('id', 'desc')
	        ->get();
	    		}else{
	    			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->select('*','orders.created_at as orderdate')->where('status',  $order_status_serach )->orderBy('id', 'desc')
	        ->get();
	    		}

		 

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_status_serach,'order_status_serach'=>$order_status_serach,'order_date_filter'=>''); 

	        if(!empty($_GET['order_status_serach'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.product_filter')->with($data_onview);
			} 

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('status',  $order_status_serach )->select('*','orders.created_at as orderdate')
	        ->orderBy('id', 'desc')
	        ->get();

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_status_serach, 'order_status_serach'=>$order_status_serach,'order_date_filter'=>''); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}

	}

	public function product_filter_order_customer(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		if(!empty($_GET['customer_name'])){
			$customer_name = $_GET['customer_name'];
		}else{
			$customer_name = Input::post('customer_name');
		}


		if(!empty($customer_name)){

			$order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get();

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);

	        if(count($order)==0){

           	$order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.lname', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get();

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.lname', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);

	        }
	        
	        $data_onview = array('order_detail'=>$orders,'order'=>$order,'searchid'=>$customer_name,'customer_name'=>$customer_name); 

	        if(!empty($_GET['customer_name'])){

				return View('admin.report_list')->with($data_onview);

			}else{

				return View('admin.ajax.product_filter')->with($data_onview);
			}
	        	
		}else{

			 $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get();

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'order'=>$order,'searchid'=>$customer_name); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}
	}

	public function product_filter_order_id(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$customer_id = Input::post('customer_id');
		
		if(!empty($customer_id)){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->where('users.id', 'LIKE', '%'.$customer_id.'%' )->select('*',"users.id as userid")
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_id); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_id); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}

	}


	public function product_filter_order_merchant(Request $request)

	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		//$customer_name = Input::post('merchant');
		if(!empty($_GET['merchant'])){
			$customer_name = $_GET['merchant'];
		}else{
			$customer_name = Input::post('merchant');
		}
		
		
		if(!empty($customer_name)){
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->where('vendor.name', 'LIKE', '%'.$customer_name.'%' )
	        ->paginate(10);

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_name, 'merchant'=>$customer_name); 

	        if(!empty($_GET['merchant'])){
				return View('admin.report_list')->with($data_onview);
			}else{
				return View('admin.ajax.product_filter')->with($data_onview);
			}

	        //return View('admin.ajax.filter')->with($data_onview);	
	        
		}else{
			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where('vendor_id', $vendor_id)
	        ->orderBy('id', 'desc')->select('*','orders.created_at as orderdate')
	        ->paginate(10);

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$customer_name); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}

	}


	public function product_filter_order_date(Request $request)
	{
		//$vendor_id = Auth::guard('vendor')->user()->vendor_id;
		$order_date = Input::post('order_date');
		Session::put('order_date', $order_date);
		$order_id = Input::post('order_id');
		Session::put('order_id', $order_id);
        $order_status = Input::post('order_status');
        Session::put('order_status', $order_status);
        $customer_name = Input::post('customer_name');
        Session::put('customer_name', $customer_name);
        $merchant_name = Input::post('merchant_name');
        Session::put('merchant_name', $merchant_name);

        $customer = explode(" ",$customer_name);

        if(count($customer)==2){

        if(isset($customer[0])){ $cfirstname = $customer[0]; }else{ $cfirstname = ''; }     
        if(isset($customer[1])){ $clastname = $customer[1]; }else{ $clastname = ''; }

        }else{

        $cfirstname = $customer[0];
        $clastname = '';

        }



        $merchant = explode(" ",$merchant_name);

        if(count($merchant)==2){

        if(isset($merchant[0])){ $vfirstname = $merchant[0]; }else{ $vfirstname = ''; }
        
        if(isset($merchant[1])){ $vlastname = $merchant[1]; }else{ $vlastname = ''; }

        }else{

         $vfirstname = $merchant[0];
         $vlastname = '';

        }

		$from_order_date = Input::post('from_order_date');

		$date = explode('_', $order_date);

		$data_to = $date[0];

		$data_from = date('Y-m-d', strtotime($date[1]. ' + 1 days'));//$date[1];date('Y-m-d', strtotime($date[1]. ' + 1 days'));
		
		if(($order_date!='_') || !empty($order_id) || ($order_status!=5) || !empty($customer_name) || !empty($merchant_name)){

          if($order_status!=5){

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('orders.status',  $order_status)
	        ->where('orders.order_id','LIKE','%'.$order_id.'%')
            ->where('orders.first_name', 'LIKE', '%'.$cfirstname.'%')
            ->where('orders.last_name', 'LIKE', '%'.$clastname.'%')
            ->where('vendor.name', 'LIKE', '%'.$vfirstname.'%')
            ->where('vendor.last_name', 'LIKE', '%'.$vlastname.'%')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
	        ->get(); 

	      }else{

          	$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('orders.order_id','LIKE','%'.$order_id.'%')
            ->where('orders.first_name', 'LIKE', '%'.$cfirstname.'%')
            ->where('orders.last_name', 'LIKE', '%'.$clastname.'%')
            ->where('vendor.name', 'LIKE', '%'.$vfirstname.'%')
            ->where('vendor.last_name', 'LIKE', '%'.$vlastname.'%')
	        ->whereBetween('orders.created_at', [$data_to, $data_from])->select('*','orders.created_at as orderdate')
	        ->get(); 

	      }

	        //merchant_last_name

	        /* $order = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->leftJoin('users', 'orders.user_id', '=', 'users.id')
	        ->orderBy('orders.id', 'desc')
	        ->where('users.name', 'LIKE', '%'.$customer_name.'%' )->select('*',"users.id as userid",'orders.created_at as orderdate')
	        ->get(); */

	        /* $orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')
	        ->orderBy('orders.id', 'desc')->select('*','orders.created_at as orderdate')
	        ->where('vendor.name', 'LIKE', '%'.$customer_name.'%' )
	        ->paginate(10); */
     

	        $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date,'order_date'=>$order_date); 

	       
			return View('admin.ajax.product_filter')->with($data_onview);
			      
		}else{

			$orders = Order::leftJoin('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->orderBy('id', 'desc')
	        ->get();

	          $data_onview = array('order_detail'=>$orders,'searchid'=>$order_date,'order_date'=>$order_date); 

	        return View('admin.ajax.product_filter')->with($data_onview);	
		}

	}	

}



