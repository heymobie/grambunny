<?php

namespace App\Http\Controllers;





Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route;

use File;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image as Image;



use App\Template;

use App\Template_menu;

use App\Template_menu_category;

use App\Template_category_item;

use App\Template_menu_category_addon;




class AdminmenutemplateController extends Controller

{



	public function __construct(){

		$this->middleware('admin');

	}


	public function show_menutemplate_list()
	{

		$template_detail  = DB::table('template')->get();

		$data_onview =array('template_detail'=>$template_detail);

		return View('admin.menu_template_list')->with($data_onview);
	}

	public function get_template_form()
	{

		if(Route::current()->getParameter('id'))
		{

			$id = Route::current()->getParameter('id');

			$template_detail  = DB::table('template')->where('template_id', '=' ,$id)->get();
			$data_onview = array(
				'template_detail' =>$template_detail,
				'id'=>$id
			);

		}
		else
		{
			$id =0;
			$data_onview = array('id'=>$id);
		}
		return View('admin.menu_template_form')->with($data_onview);
	}


	public function template_action()
	{
		$template_id = Input::get('template_id');


		if($template_id==0)
		{
			$temp_menu = new Template;
			$temp_menu->template_name = Input::get('template_name');
			$temp_menu->template_status = Input::get('template_status');
			$temp_menu->save();

			$template_id = $temp_menu->template_id;

			Session::flash('message', 'Menu Template Name Inserted Sucessfully!');

			return redirect()->to('/admin/menu_template');
		}
		else
		{

			DB::table('template')
			->where('template_id', $template_id)
			->update(['template_name' =>Input::get('template_name'),
				'template_status' =>Input::get('template_status')
			]);

			session()->flash('message', 'Menu Template Name Updated Sucessfully!');

			return redirect()->to('/admin/menu_template');
		}
	}


	function template_delete($id)
	{

		DB::table('template')->where('template_id', '=', $id)->delete();
		Session::flash('message', 'Menu Template Deleted Sucessfully!');
		return Redirect('/admin/menu_template');
	}


	/*Create for delete menus*/
	function deletetemplate_menus($id)
	{	// template
		// template_menu
		// template_menu_category
		$template_cate = DB::table('template_menu')
		->where('template_id', '=' ,$id)
		->select('*')
		->get();
		foreach ($template_cate as $cate) {
			$cate->template_id;
			$cate->menu_id;
			DB::table('template_menu_category')
			->where('template_id', '=', $cate->template_id)
			->where('menu_id', '=', $cate->menu_id)
			->delete();
			DB::table('template_menu')
			->where('template_id', '=', $id)
			->delete();

		}
		DB::table('template')->where('template_id', '=', $id)->delete();
		Session::flash('message', 'Menu Template And its All Menus Deleted Sucessfully!');
		return Redirect('/admin/menu_template');
	}

	/*************************/


	public function show_menu_list()
	{
		if(Route::current()->getParameter('id'))
		{
			$id = Route::current()->getParameter('id');



			$template_list = DB::table('template')
			->where('template_id', '=' ,$id)
			->select('*')
			->get();


			$menu_list = DB::table('template_menu')
			->where('template_id', '=' ,$id)
			->orderBy('menu_order', 'asc')
			->get();

			$menu_cat_detail = '';

			if(!empty($menu_list)){

				$menu_cat_detail = DB::table('template_menu_category')

				->where('template_id', '=' ,$id)

				->where('menu_id', '=' ,$menu_list[0]->menu_id)

				->select('*')

				->orderBy('menu_category_id', 'asc')

				->get();

			}



			$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'id'=>$id,'menu_cate_detail'=>$menu_cat_detail);


			return View('admin.template.menulist')->with($data_onview);
		}
		else
		{
			return redirect()->to('/admin/menu_template');
		}
	}



	public function ajax_menu_form()
	{

/*	echo '<pre>';
		print_r($_POST);
		exit;*/
		$id =0;
		if(Input::get('menu'))
		{

			$template_id =  Input::get('template_id');

			$menu_id =  Input::get('menu');

			$id = Input::get('menu');

			$menu_detail  = DB::table('template_menu')->where('menu_id', '=' ,$menu_id)->get();



			$data_onview = array('template_id' =>$template_id,
				'menu_detail' =>$menu_detail,
				'id'=>$id);

		}
		else
		{

			$template_id =  Input::get('template_id');
			$id =0;

			$data_onview = array('template_id' =>$template_id,
				'menu_detail' =>'',
				'id'=>$id);
		}



		return View('admin.template.menu_form')->with($data_onview);

	}



	public function menu_action(Request $request)
	{



	/*	echo'<pre>';

		print_r($_POST);
		print_r($_FILES);

		exit;*/

		if(Input::get('from')=='back'){





			$template_id = 	Input::get('template_id');

			$template_list = DB::table('template')
			->where('template_id', '=' ,$template_id)
			->select('*')
			->get();

			$menu_list = DB::table('template_menu')
			->where('template_id', '=' ,$template_id)
			->orderBy('menu_order', 'asc')
			->get();

			$menu_id = $menu_list[0]->menu_id;

			$menu_detail = DB::table('template_menu')
			->where('template_id', '=' ,$template_id)
			->where('menu_id', '=' ,$menu_id)
			->orderBy('menu_order', 'asc')
			->get();




			$menu_cat_detail = '';

			if(!empty($menu_list)){

				$menu_cat_detail = DB::table('template_menu_category')

				->where('template_id', '=' ,$template_id)
				->where('menu_id', '=' ,$menu_list[0]->menu_id)
				->select('*')
				->orderBy('menu_category_id', 'asc')
				->get();

			}




			$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);



			return View('admin.template.menu_list')->with($data_onview);







		}

		else
		{



			$menu_id = Input::get('menu_id');

			$new_fileName = '';




			/*****RESTAURANT IMAGE ***/

			if(isset($_FILES['menu_image']) && (!empty($_FILES['menu_image']['name'][0]))){





				if(!empty($menu_image_old)){



					$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/menu/'.$menu_image_old;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;



					if(file_exists($file_remove)){

						unlink($file_remove);

					}





				}





				$destinationPath = 'uploads/menu/';

				$image = $request->file('menu_image');

				$extension 		= 	$image->getClientOriginalExtension();

				$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();





				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

				{

					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();



					$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/menu/'.$thumbName);;

					asset($destinationPath . $thumbName);



					$new_fileName = $thumbName;

				}

				else

				{

					Session::flash('message_error', 'Image type is invalide!');



				/*	if($rest_id>0){

					return redirect()->to('/admin/restaurant-form/'.$rest_id);

					}

					else

					{

						return redirect()->to('/admin/vendor-rest-form/'.Input::get('vendor_id'));

					}*/

				}



			}

			elseif(!empty($menu_image_old))
			{

				$new_fileName = $menu_image_old;

			}
 ///****** END **//







			if($menu_id==0)
			{

				$template_id =  Input::get('template_id');

				$menu_max_value  = DB::table('template_menu')
				->where('template_id', '=' ,$template_id)
				->select(\DB::raw('MAX(template_menu.menu_order) as max_order'))
				->get();

				$max_order_no = $menu_max_value[0]->max_order;

				$menu = new Template_menu;

				$menu->template_id = Input::get('template_id');

				$menu->menu_name =  Input::get('menu_name');

				$menu->menu_desc =  Input::get('menu_desc');

				$menu->menu_status =  Input::get('menu_status');

				$menu->menu_order =  ($max_order_no+1);

				$menu->menu_image = $new_fileName;

				$menu->save();

				$menu_id = $menu->menu_id;



				Session::flash('menu_message', 'Template Menu Inserted Sucessfully!');



					//return redirect()->to('/admin/menu_list');





				$template_list = DB::table('template')
				->where('template_id', '=' ,$template_id)
				->select('*')
				->get();


				$menu_detail = DB::table('template_menu')
				->where('template_id', '=' ,$template_id)
				->where('menu_id', '=' ,$menu_id)
				->orderBy('menu_order', 'asc')
				->get();



				$menu_list = DB::table('template_menu')
				->where('template_id', '=' ,$template_id)
				->orderBy('menu_order', 'asc')
				->get();

				$menu_cat_detail = '';

				if(!empty($menu_list)){

					$menu_cat_detail = DB::table('template_menu_category')

					->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_list[0]->menu_id)
					->select('*')
					->orderBy('menu_category_id', 'asc')
					->get();

				}







				$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);







				   //  $data_onview = array('menu_list'=>$menu_list,'res_id'=>$res_id);



				if(Input::get('from')=='submit'){

					return View('admin.template.menu_list')->with($data_onview);

				}

				elseif(Input::get('from')=='addnext'){

					return View('admin.template.menu_list_form')->with($data_onview);

				}





			}

			else
			{

				$template_id =  Input::get('template_id');
				if(Input::get('from')=='update'){

					DB::table('template_menu')

					->where('menu_id', $menu_id)

					->update(['template_id' =>Input::get('template_id'),

						'menu_name'=>Input::get('menu_name'),

						'menu_desc'=>Input::get('menu_desc'),

						'menu_image'=>$new_fileName,

						'menu_status'=>Input::get('menu_status')

					]);





					session()->flash('menu_message', 'Template Menu Updated Sucessfully!');

				}


				$template_list = DB::table('template')
				->where('template_id', '=' ,$template_id)
				->select('*')
				->get();


				$menu_detail = DB::table('template_menu')
				->where('template_id', '=' ,$template_id)
				->where('menu_id', '=' ,$menu_id)
				->orderBy('menu_order', 'asc')
				->get();



				$menu_list = DB::table('template_menu')
				->where('template_id', '=' ,$template_id)
				->orderBy('menu_order', 'asc')
				->get();

				$menu_cat_detail = '';

				if(!empty($menu_list)){

					$menu_cat_detail = DB::table('template_menu_category')

					->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_list[0]->menu_id)
					->select('*')
					->orderBy('menu_category_id', 'asc')
					->get();

				}


				$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);



				return View('admin.template.menu_list')->with($data_onview);





			}

		}

	}




	public function ajax_update_sortorder()
	{

		foreach ($_POST['listItem'] as $position => $item)
		{
			DB::table('template_menu')
			->where('template_id', Input::get('template_id'))
			->where('menu_id', $item)
			->update(['menu_order' =>($position+1)
		]);
		}
	}


	public function ajax_show_menudetail()
	{


		$menu_id = Input::get('menu');
		$template_id = Input::get('template_id');

		$template_list  = DB::table('template')->where('template_id', '=' ,$template_id)->get();

		$menu_list = DB::table('template_menu')
		->where('template_id', '=' ,$template_id)
		->orderBy('menu_order', 'asc')
		->get();



		$menu_detail = DB::table('template_menu')
		->where('template_id', '=' ,$template_id)
		->where('menu_id', '=' ,$menu_id)
		->orderBy('menu_id', 'desc')
		->get();

		$menu_cat_detail = '';



		if(!empty($menu_list)){

			$menu_cat_detail = DB::table('template_menu_category')
			->where('template_id', '=' ,$template_id)
			->where('menu_id', '=' ,$menu_id)
			->select('*')
			->orderBy('menu_category_id', 'asc')
			->get();

		}







		$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);

		return View('admin.template.menu_list')->with($data_onview);

	}


	/******  category form and functionalityt */

	public function ajax_menu_item_form()
	{

		$template_id =  Input::get('template_id');
		$menu_id =  Input::get('menu_id');
		$id =0;

		$menu_detail = DB::table('template_menu')
		->where('menu_id', '=' ,$menu_id)
		->get();

		$data_onview = array('template_id' =>$template_id,
			'menu_id' =>$menu_id,
			'menu_name' =>$menu_detail[0]->menu_name,
			'id'=>$id);

		return View('admin.template.menu_form_item')->with($data_onview);



	}

	public function menu_category_action(Request $request)
	{
		/*echo '<pre>';
		print_r($_POST);
		exit;*/


		$menu_category_id = Input::get('menu_category_id');



		if(Input::get('from')=='back'){


			$menu_id = Input::get('menu_id');
			$template_id = Input::get('template_id');

			$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

			$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)
			->orderBy('menu_order', 'asc')
			->get();

			$menu_detail = DB::table('template_menu')->where('template_id', '=' ,$template_id)
			->where('menu_id', '=' ,$menu_id)->orderBy('menu_id', 'desc')
			->get();


			$menu_cat_detail = '';



			if(!empty($menu_list)){

				$menu_cat_detail = DB::table('template_menu_category')
				->where('template_id', '=' ,$template_id)->where('menu_id', '=' ,$menu_id)
				->select('*')->orderBy('menu_category_id', 'asc')
				->get();

			}



			$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);

			return View('admin.template.menu_list')->with($data_onview);

		}
		else
		{

			if($menu_category_id==0)
			{
				$menu = new  Template_menu_category ;
				$menu->template_id = Input::get('template_id');
				$menu->menu_id =  Input::get('menu_id');
				$menu->menu_category_name =  Input::get('menu_name');
				$menu->menu_category_desc =  Input::get('menu_desc');
				$menu->menu_cat_popular =  Input::get('menu_cat_popular');
				$menu->menu_cat_diet =  Input::get('menu_cat_diet');
				$menu->menu_category_price =  Input::get('menu_price');
				$menu->menu_category_portion = Input::get('diff_size');
				$menu->menu_cat_status = Input::get('menu_cat_status');
				$menu->save();

				$menu_category_id = $menu->menu_category_id;



				if(Input::get('diff_size')=='yes')
				{

					for($c=0;$c<count($_POST['item_id']);$c++)
					{
						if((!empty(trim($_POST['item_title'][$c]))))
						{
							$menu_sub_itme = new  Template_category_item;

							$menu_sub_itme->template_id	 =Input::get('template_id');
							$menu_sub_itme->menu_id	 = Input::get('menu_id');
							$menu_sub_itme->menu_category	 = $menu_category_id;
							$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];
							$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];
							$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];
							$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');
							$menu_sub_itme->save();
						}
					}

				}


				Session::flash('menu_message', 'Menu Item Inserted Sucessfully!');

				$template_id = Input::get('template_id');
				/* START */

				$menu_id = Input::get('menu_id');

				$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

				$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)
				->orderBy('menu_order', 'asc')->get();


				$menu_detail = DB::table('template_menu')->where('template_id', '=' ,$template_id)
				->where('menu_id', '=' ,$menu_id)->orderBy('menu_id', 'desc')->get();

				$menu_cat_detail = '';

				if(!empty($menu_list)){

					$menu_cat_detail = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_id)->orderBy('menu_category_id', 'asc')
					->get();

				}




				/* END */



				if(Input::get('from')=='submit'){

					$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);

					return View('admin.template.menu_list')->with($data_onview);

				}

				elseif(Input::get('from')=='addnext'){



					$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail,'id'=>'0');

					return View('admin.template.menu_item_list_form')->with($data_onview);

				}
			}
			else
			{


				if(Input::get('from')=='update'){
					DB::table('template_menu_category')
					->where('menu_category_id', $menu_category_id)

					->update(['menu_category_name'=>Input::get('menu_name'),
						'menu_category_price' =>  Input::get('menu_price'),
						'menu_category_desc'=>Input::get('menu_desc'),
						'menu_category_portion'=>Input::get('diff_size'),
						'menu_cat_status'=>Input::get('menu_cat_status'),
						'menu_cat_popular'=> Input::get('menu_cat_popular'),
						'menu_cat_diet'=> Input::get('menu_cat_diet')
					]);



					if(Input::get('diff_size')=='yes')
					{
						for($c=0;$c<count($_POST['item_id']);$c++)
						{


							if($_POST['item_id'][$c]==0)
							{
								if((!empty(trim($_POST['item_title'][$c]))))
								{
									$menu_sub_itme = new  Template_category_item;
									$menu_sub_itme->template_id	 =Input::get('template_id');
									$menu_sub_itme->menu_id	 = Input::get('menu_id');
									$menu_sub_itme->menu_category	 = $menu_category_id;
									$menu_sub_itme->menu_item_title	 = $_POST['item_title'][$c];
									$menu_sub_itme->menu_item_price	 = $_POST['item_price'][$c];
									$menu_sub_itme->menu_item_status = $_POST['item_status'][$c];
									$menu_sub_itme->menu_cat_itm_price =Input::get('menu_price');
									$menu_sub_itme->save();
								}
							}
							else
							{

								DB::table('template_category_item')
								->where('menu_cat_itm_id',$_POST['item_id'][$c])
								->update(['menu_item_title'=>$_POST['item_title'][$c],
									'menu_item_price' =>$_POST['item_price'][$c],
									'menu_item_status'=>$_POST['item_status'][$c]
								]);

							}
						}
					}
					elseif(Input::get('diff_size')=='no')
					{

						$addon_list  = DB::table('template_category_item')->where('menu_category', '=' ,$menu_category_id)->get();

						if(!empty($addon_list))
						{
							DB::table('template_category_item')->where('menu_category', '=', $menu_category_id)->delete();

						}
					}
					session()->flash('menu_message', 'Menu Item Updated Sucessfully!');
				}



				$menu_id = Input::get('menu_id');

				$template_id = Input::get('template_id');



				$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

				$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)
				->orderBy('menu_order', 'asc')->get();

				$menu_detail = DB::table('template_menu')->where('template_id', '=' ,$template_id)
				->where('menu_id', '=' ,$menu_id)->orderBy('menu_id', 'desc')
				->get();

				$menu_cat_detail = '';



				if(!empty($menu_list)){

					$menu_cat_detail = DB::table('template_menu_category')
					->where('template_id', '=' ,$template_id)
					->where('menu_id', '=' ,$menu_id)
					->select('*')
					->orderBy('menu_category_id', 'asc')
					->get();

				}



				$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>$menu_id,'menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);

				return View('admin.template.menu_list')->with($data_onview);


			}

		}

	}




	public function ajax_update_menu_category()
	{

		//echo '<pre>';

		//print_r($_POST);



		$template_id =Input::get('rest');
		$menu_id=Input::get('menu');

		$menu_cate_id = Input::get('menu_itme');

		$menu_detail = DB::table('template_menu')->where('menu_id', '=' ,$menu_id)->get();

		$cate_detail  = DB::table('template_menu_category')->where('menu_category_id', '=' ,$menu_cate_id)->get();

		$menu_cat_addon = '';


		$data_onview = array('template_id' =>$template_id,
			'menu_id' =>$menu_id,
			'menu_name' =>$menu_detail[0]->menu_name,
			'cate_detail'=>$cate_detail,
			"menu_cat_addon"=>$menu_cat_addon,
			'id'=>$menu_cate_id);



		return View('admin.template.update_menu_form_item')->with($data_onview);

	}


	public function menu_category_form()
	{

		$rest_detail  = DB::table('restaurant')->get();



		if(Route::current()->getParameter('id'))

		{

			$id = Route::current()->getParameter('id');

			$cate_detail  = DB::table('menu_category')->where('menu_category_id', '=' ,$id)->get();

			$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$cate_detail[0]->rest_id)->get();

			$data_onview = array('rest_detail'=>$rest_detail,

				'menu_detail'=>$menu_detail,

				'cate_detail'=>$cate_detail,

				'id'=>$id);

		}

		else

		{

			$id =0;

			$data_onview = array('rest_detail'=>$rest_detail,

				'menu_detail'=>'',

				'id'=>$id);

		}

		return View('admin.menu_category_form')->with($data_onview);

	}






	/* SHOW POPULAR LIST START */

	function menu_category_popularlist()
	{
		$template_id = Input::get('template_id');

		$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

		$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->orderBy('menu_order', 'asc')
		->get();

		$menu_detail = '';

		$menu_cat_detail = '';

		if(!empty($menu_list)){

			$menu_cat_detail = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
			->where('menu_cat_popular', '=' ,'1')->orderBy('menu_category_id', 'asc')
			->get();

		}


		$data_onview = array('template_detail' =>$template_list,'menu_list'=>$menu_list,'template_id'=>$template_id,'menu_id'=>'','menu_cate_detail'=>$menu_cat_detail,'menu_detail'=>$menu_detail);



		return View('admin.template.popular_menu_item_list')->with($data_onview);

	}

	/* POPUPLAR LIST END */




	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS START */

	public function ajax_addons_list()
	{

		//echo '<pre>';

		//print_r($_POST);



		$addon_list = '';

		$template_id = Input::get('rest');
		$menu_id =Input::get('menu');
		$menu_item = Input::get('menu_itme');

		$addon_list = DB::table('template_menu_category_addon')
		->where('addon_templateid', '=' ,$template_id)
		->where('addon_menuid', '=' ,$menu_id)
		->where('addon_menucatid', '=' ,$menu_item)
		->orderBy('addon_id', 'asc')
		->get();

		$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();

		$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->where('menu_id', '=' ,$menu_id)
		->get();



		$item_detil = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
		->where('menu_category_id', '=' ,$menu_item)->where('menu_id', '=' ,$menu_id)
		->get();


		$template_name = $template_list[0]->template_name;

		$menu_name = $menu_list[0]->menu_name;

		$item_name = $item_detil[0]->menu_category_name;


		$data_onview = array('addon_list'=>$addon_list,
			'template_name'=>$template_name,
			'menu_name'=>$menu_name,
			'item_name'=>$item_name,
			'template_id'=>$template_id,
			'menu_id'=>$menu_id,
			'menu_item'=>$menu_item,
		);

		return View('admin.template.addon_list')->with($data_onview);;
	}



	public function ajax_addons_form()
	{
		//print_r($_POST);

		$id =0;

		$addon_detail = '';

		if(Input::get('addon_id'))
		{

			$addon_id = Input::get('addon_id');

			$id = Input::get('addon_id');

			$addon_detail  = DB::table('template_menu_category_addon')->where('addon_id', '=' ,$addon_id)->get();

		}



		$addon_list = '';



		$template_id = Input::get('template_id');

		$menu_id =Input::get('menu');

		$menu_item = Input::get('menu_itme');


		$template_list = DB::table('template')->where('template_id', '=' ,$template_id)->get();


		$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->where('menu_id', '=' ,$menu_id)
		->get();


		$item_detil = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
		->where('menu_category_id', '=' ,$menu_item)->where('menu_id', '=' ,$menu_id)
		->get();



		$template_name = $template_list[0]->template_name;

		$menu_name = $menu_list[0]->menu_name;

		$item_name = $item_detil[0]->menu_category_name;



		$data_onview = array('addon_detail'=>$addon_detail,
			'template_name'=>$template_name,
			'menu_name'=>$menu_name,
			'item_name'=>$item_name,
			'template_id'=>$template_id,
			'menu_id'=>$menu_id,
			'menu_item'=>$menu_item,
			'id'=>$id
		);
		return View('admin.template.addon_form')->with($data_onview);

	}

	public function ajax_addons_action(Request $request)
	{



		/*echo '<pre>';

		print_r($_POST);

		exit;*/



		$addon_id = Input::get('addon_id');

		$def_addon = '0';

		if(isset($_POST['addon_default']))
		{
			$def_addon = '1';
		}





		if($addon_id==0)

		{





			$addon = new Template_menu_category_addon;

			$addon->addon_templateid = Input::get('addon_templateid');

			$addon->addon_menuid =Input::get('addon_menuid');

			$addon->addon_menucatid = Input::get('addon_menucatid');

			$addon->addon_name = trim(Input::get('addon_name'));

			$addon->addon_price =  Input::get('addon_price');

			$addon->addon_status =  Input::get('addon_status');

			$addon->addon_groupname = strtoupper(trim(Input::get('addon_groupname')));

			$addon->addon_option =  Input::get('addon_option');
			$addon->addon_default =  $def_addon;



			$addon->save();

			$addon_id = $addon->addon_id;



			Session::flash('addon_message', 'Addon Inserted Sucessfully!');



				//$rest_id =  Input::get('service_restid');











			/* FEATCH DATA FROM TABLE FOR SHOW */



			$addon_list = '';



			$template_id = Input::get('addon_templateid');

			$menu_id = Input::get('addon_menuid');

			$menu_item = Input::get('addon_menucatid');



			$template_list  = DB::table('template')->where('template_id', '=' ,$template_id)->get();



			$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)
			->where('menu_id', '=' ,$menu_id)
			->get();



			$item_detil = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
			->where('menu_category_id', '=' ,$menu_item)->where('menu_id', '=' ,$menu_id)
			->get();


			$template_name = $template_list[0]->template_name;

			$menu_name = $menu_list[0]->menu_name;

			$item_name = $item_detil[0]->menu_category_name;











			/* END */









			if(Input::get('from')=='submit'){



				$addon_list = DB::table('template_menu_category_addon')

				->where('addon_templateid', '=' ,$template_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_menucatid', '=' ,$menu_item)

				->orderBy('addon_id', 'asc')

				->get();



				$data_onview = array('addon_list'=>$addon_list,
					'template_name'=>$template_name,
					'menu_name'=>$menu_name,
					'item_name'=>$item_name,
					'template_id'=>$template_id,
					'menu_id'=>$menu_id,
					'menu_item'=>$menu_item

				);


				return View('admin.template.addon_list')->with($data_onview);

			}

			elseif(Input::get('from')=='addnext'){



				$id = 0;

				$addon_detail = '';

				$data_onview = array('addon_detail'=>$addon_detail,

					'template_name'=>$template_name,

					'menu_name'=>$menu_name,

					'item_name'=>$item_name,

					'template_id'=>$template_id,

					'menu_id'=>$menu_id,

					'menu_item'=>$menu_item,

					'id'=>$id

				);

				return View('admin.template.addon_form')->with($data_onview);

			}







		}

		else
		{

			$template_id = Input::get('addon_templateid');

			$menu_id = Input::get('addon_menuid');

			$menu_item = Input::get('addon_menucatid');



			$template_list  = DB::table('template')->where('template_id', '=' ,$template_id)->get();



			$menu_list = DB::table('template_menu')->where('template_id', '=' ,$template_id)->where('menu_id', '=' ,$menu_id)
			->get();



			$item_detil = DB::table('template_menu_category')->where('template_id', '=' ,$template_id)
			->where('menu_category_id', '=' ,$menu_item)->where('menu_id', '=' ,$menu_id)
			->get();

			$template_name = $template_list[0]->template_name;

			$menu_name = $menu_list[0]->menu_name;

			$item_name = $item_detil[0]->menu_category_name;



			if(Input::get('from')=='update'){



				DB::table('template_menu_category_addon')

				->where('addon_id', $addon_id)

				->update(['addon_name' =>trim(Input::get('addon_name')),

					'addon_price'=>Input::get('addon_price'),

					'addon_status'=>Input::get('addon_status'),

					'addon_groupname'=>strtoupper(trim(Input::get('addon_groupname'))),

					'addon_option'=>  Input::get('addon_option'),
					'addon_default'=>  $def_addon
				]);

				Session::flash('addon_message', 'Addon Update Sucessfully!');

			}



			/* FEATCH DATA FROM TABLE FOR SHOW */



			$addon_list = DB::table('template_menu_category_addon')
			->where('addon_templateid', '=' ,$template_id)
			->where('addon_menuid', '=' ,$menu_id)
			->where('addon_menucatid', '=' ,$menu_item)
			->orderBy('addon_id', 'asc')
			->get();

			$data_onview = array('addon_list'=>$addon_list,
				'template_name'=>$template_name,
				'menu_name'=>$menu_name,
				'item_name'=>$item_name,
				'template_id'=>$template_id,
				'menu_id'=>$menu_id,
				'menu_item'=>$menu_item
			);



			return View('admin.template.addon_list')->with($data_onview);



		}



	}



	/* ADDONS LISTING ADD EDIT FUNCTIONALITY FUNCTIONS END */



	/*********************************************  RESTAURENT MENU DATA ***************************************************/

	public function restaurant_list()

	{

		DB::connection()->enableQueryLog();



		$rest_list = DB::table('restaurant')

		->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')

		->select('restaurant.*','vendor.name')

		->orderBy('restaurant.google_type', 'asc')
		->orderBy('restaurant.rest_id', 'desc')

		->get();







		$data_onview = array('rest_list' =>$rest_list);

		return View('admin.restaurant_list')->with($data_onview);

	}



	public function restaurant_search()

	{



		$rest_list = DB::table('restaurant');

		$rest_list = $rest_list->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id');



		if( (Input::get('rest_cont')) && (!empty(Input::get('rest_cont'))))

		{



			$rest_list = $rest_list->where('restaurant.rest_contact', 'like' ,Input::get('rest_cont'));

			$rest_list = $rest_list->orwhere('restaurant.rest_landline', 'like' ,Input::get('rest_cont'));

		}



		if( (Input::get('rest_name')) && (!empty(Input::get('rest_name'))))

		{

			$rest_list = $rest_list->where('restaurant.rest_name', 'like' ,'%'.Input::get('rest_name').'%');

		}



		$rest_list = $rest_list->select('restaurant.*','vendor.name');

		$rest_list = $rest_list->orderBy('restaurant.rest_id', 'desc');

		$rest_list = $rest_list->get();



		$data_onview = array('rest_list' =>$rest_list);

		return View('admin.ajax.restaurant_list')->with($data_onview);



	}



	public function restaurant_form()

	{



		/*$vendor_detail = DB::table('vendor')

			 ->select('*')

			->orderBy('vendor_id', 'desc')

			->get();*/



			$mon_from='';

			$mon_to='';

			$tues_from='';

			$tues_to='';

			$wed_from='';

			$wed_to='';

			$thu_from='';

			$thu_to='';

			$fri_from='';

			$fri_to='';

			$sat_from='';

			$sat_to='';

			$sun_from='';

			$sun_to='';



			if(Route::current()->getParameter('id'))

			{

				$id = Route::current()->getParameter('id');



				$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$id)->get();

				if(($id>0) && (!empty($rest_detail[0]->rest_mon))){

					$v = explode('_',$rest_detail[0]->rest_mon);

					$mon_from=$v[0];

					$mon_to=$v[1];;

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_tues))){

					$v = explode('_',$rest_detail[0]->rest_tues);

					$tues_from=$v[0];

					$tues_to=$v[1];;

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_wed))){

					$v = explode('_',$rest_detail[0]->rest_wed);

					$wed_from=$v[0];

					$wed_to=$v[1];;

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_thus))){

					$v = explode('_',$rest_detail[0]->rest_thus);

					$thu_from=$v[0];

					$thu_to=$v[1];;

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_fri))){

					$v = explode('_',$rest_detail[0]->rest_fri);

					$fri_from=$v[0];

					$fri_to=$v[1];

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_sat))){

					$v = explode('_',$rest_detail[0]->rest_sat);

					$sat_from=$v[0];

					$sat_to=$v[1];

				}



				if(($id>0) && (!empty($rest_detail[0]->rest_sun))){

					$v = explode('_',$rest_detail[0]->rest_sun);

					$sun_from=$v[0];

					$sun_to=$v[1];

				}



				$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();





				$data_onview = array('rest_detail' =>$rest_detail,

					'mon_from' =>$mon_from,

					'mon_to' =>$mon_to,

					'tues_from' =>$tues_from,

					'tues_to' =>$tues_to,

					'wed_from' =>$wed_from,

					'wed_to' =>$wed_to,

					'thu_from' =>$thu_from,

					'thu_to' =>$thu_to,

					'fri_from' =>$fri_from,

					'fri_to' =>$fri_to,

					'sat_from' =>$sat_from,

					'sat_to' =>$sat_to,

					'sun_from' =>$sun_from,

					'sun_to' =>$sun_to,

					'cuisine_list'=>$cuisine_list,

					'id'=>$id);

			}

			else

			{

				$vendor_id =  Route::current()->getParameter('vendor_id');





				$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();

				$id =0;

				$data_onview = array('vendor_id' =>$vendor_id,

					'mon_from' =>$mon_from,

					'mon_to' =>$mon_to,

					'tues_from' =>$tues_from,

					'tues_to' =>$tues_to,

					'wed_from' =>$wed_from,

					'wed_to' =>$wed_to,

					'thu_from' =>$thu_from,

					'thu_to' =>$thu_to,

					'fri_from' =>$fri_from,

					'fri_to' =>$fri_to,

					'sat_from' =>$sat_from,

					'sat_to' =>$sat_to,

					'sun_from' =>$sun_from,

					'sun_to' =>$sun_to,

					'cuisine_list'=>$cuisine_list,

					'id'=>$id);



			}



			return View('admin.restaurant_form')->with($data_onview);

		}



		function clean_string($string){



			$string1 = strtolower($string);

		//Make alphanumeric (removes all other characters)

			$string1 = preg_replace("/[^a-z0-9_\s-]/", "", $string1);

		//Clean up multiple dashes or whitespaces

			$string1 = preg_replace("/[\s-]+/", " ", $string1);

		//Convert whitespaces and underscore to dash

			$string1 = preg_replace("/[\s_]/", "-", $string1);

			if( !empty($string1) || $string1 != '' ){

				return $string1;

			}else{

				return $string;

			}



		}







		public function restaurant_action(Request $request)

		{

		/*echo '<pre>';

		print_r($_POST);

		print_r($_FILES);

		exit;*/

		$meta = $this->clean_string(trim(Input::get(['rest_name'])));



		$rest_id = Input::get('rest_id');

		$rest_old_logo = Input::get('rest_old_logo');

		$new_fileName = '';

		$rest_old_banner = Input::get('rest_old_banner');

		$new_banner = '';






		/********************     CALCULATE LAT AND LONG ACCORDING TO ADDRESS ************************/

		$latitude='';
		$longitude='';



  $address =  Input::get('rest_address').", ".Input::get('rest_suburb').", ".Input::get('rest_state'); // Google HQ
  $prepAddr = str_replace(' ','+',$address);
  $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
  $output= json_decode($geocode);
  if(empty($output->results[0])){
  	$latitude= 0;
  	$longitude= 0;
  }
  else{
  	$latitude = $output->results[0]->geometry->location->lat;
  	$longitude = $output->results[0]->geometry->location->lng;
  }




  /********************     CALCULATE LAT AND LONG ACCORDING TO ADDRESS END   ************************/




  /*****RESTAURANT IMAGE ***/

  if(isset($_FILES['rest_logo']) && (!empty($_FILES['rest_logo']['name'][0]))){





  	if(!empty($rest_old_logo)){



  		$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;



  		if(file_exists($file_remove)){

  			unlink($file_remove);

  		}





  	}





  	$destinationPath = 'uploads/reataurant/';

  	$image = $request->file('rest_logo');

  	$extension 		= 	$image->getClientOriginalExtension();

  	$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();





  	if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

  	{

  		$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();



  		$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/'.$thumbName);;

  		asset($destinationPath . $thumbName);



  		$new_fileName = $thumbName;

  	}

  	else

  	{

  		Session::flash('message_error', 'Image type is invalide!');



  		if($rest_id>0){

  			return redirect()->to('/admin/restaurant-form/'.$rest_id);

  		}

  		else

  		{

  			return redirect()->to('/admin/vendor-rest-form/'.Input::get('vendor_id'));

  		}

  	}



  }

  elseif(!empty($rest_old_logo))

  {

  	$new_fileName = $rest_old_logo;

  }
 ///****** END **//



 /////****************************** BANNER IMAGE ***************/

  if(isset($_FILES['rest_banner']) && (!empty($_FILES['rest_banner']['name'][0]))){





				/*if(!empty($rest_old_logo)){



					 $file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 //$file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;



					if(file_exists($file_remove)){

					 unlink($file_remove);

					}





				}*/





				$destinationPath = 'uploads/reataurant/banner/';

				$image = $request->file('rest_banner');

				$extension 		= 	$image->getClientOriginalExtension();

				$imageRealPath 	= 	$image->getRealPath();

    		//	$thumbName 		= 	'thumb_'. $image->getClientOriginalName();





				if(($extension=='JPEG') ||($extension=='JPG') || ($extension=='GIF') || ($extension=='PNG') || ($extension=='png') || ($extension=='gif') || ($extension=='jpeg') || ($extension=='jpg'))

				{

					$thumbName = md5(microtime() . $image->getClientOriginalName()) . "." . $image->getClientOriginalExtension();



					//$img = Image::make($imageRealPath)->resize('140','140')->save('uploads/reataurant/'.$thumbName);;

					$img = Image::make($imageRealPath)->save('uploads/reataurant/banner/'.$thumbName);;

					asset($destinationPath . $thumbName);



					$new_banner = $thumbName;

				}

				else

				{

					Session::flash('message_error', 'Image type is invalide!');



					if($rest_id>0){

						return redirect()->to('/admin/restaurant-form/'.$rest_id);

					}

					else

					{

						return redirect()->to('/admin/vendor-rest-form/'.Input::get('vendor_id'));

					}

				}



			}

			elseif(!empty($rest_old_logo))

			{

				$new_banner = $rest_old_banner;

			}



 /////////////// END ***********************************************/

			$rest_cuisine_data='';
			if(Input::get('rest_cuisine')){

				$rest_cuisine_data = implode(',',Input::get('rest_cuisine'));

			}


			$rest_type_data='';

			if(Input::get('rest_type')){

				$rest_type_data = implode(',',Input::get('rest_type'));

			}


			if(Input::get('rest_service')){

				$rest_service = implode(',',Input::get('rest_service'));

			}

			else

			{

				$rest_service ='';

			}



			if(Input::get('close_day')){

				$rest_close = implode(',',Input::get('close_day'));

			}

			else

			{

				$rest_close ='';

			}



			$today_special='0';

			if(Input::get('rest_special'))

			{

				$today_special='1';

			}







			if($rest_id==0)

			{









				$rest = new Restaurant;

				$rest->rest_name = Input::get('rest_name');

				$rest->vendor_id =Input::get('vendor_id');

				$rest->rest_address = Input::get('rest_address');

				$rest->rest_city = Input::get('rest_suburb');

				$rest->rest_zip_code =  Input::get('rest_zip_code');

				$rest->rest_desc =  Input::get('rest_desc');

				$rest->rest_logo = $new_fileName;

				$rest->rest_banner = $new_banner;

				$rest->rest_service =$rest_service;

				$rest->rest_state = Input::get('rest_state');

				$rest->rest_suburb =Input::get('rest_suburb');

				$rest->rest_address2 ='';

				$rest->rest_contact =Input::get('rest_contact');

				$rest->rest_special = $today_special;

				$rest->rest_contact =Input::get('rest_contact');

				$rest->rest_cuisine = $rest_cuisine_data;

				$rest->rest_mindelivery =Input::get('rest_mindelivery');

				$rest->rest_landline =Input::get('rest_landline');

				$rest->rest_commission =Input::get('rest_commission');

				$rest->rest_email =Input::get('rest_email');

				$rest->rest_status =Input::get('rest_status');

				$rest->rest_classi =Input::get('rest_classi');



				$rest->rest_metatag =$meta;















				$rest->rest_mon =Input::get('mon_from').'_'.Input::get('mon_to');

				$rest->rest_tues =Input::get('tues_from').'_'.Input::get('tues_to');

				$rest->rest_wed =Input::get('wed_from').'_'.Input::get('wed_to');

				$rest->rest_thus =Input::get('thu_from').'_'.Input::get('thu_to');

				$rest->rest_fri =Input::get('fri_from').'_'.Input::get('fri_to');

				$rest->rest_sat =Input::get('sat_from').'_'.Input::get('sat_to');

				$rest->rest_sun =Input::get('sun_from').'_'.Input::get('sun_to');

				$rest->rest_close =$rest_close;





				$rest->rest_delivery_from = Input::get('delivery_from');

				$rest->rest_delivery_to = Input::get('delivery_to');

				$rest->rest_holiday_from = Input::get('holiday_from');

				$rest->rest_holiday_to = Input::get('holiday_to');

				$rest->rest_price_level = Input::get('rest_price_level');
				$rest->rest_servicetax = Input::get('rest_servicetax');
				$rest->rest_partial_pay = Input::get('rest_partial_pay');
				$rest->rest_partial_percent = Input::get('rest_partial_percent');
				$rest->rest_cash_deliver = Input::get('rest_cash_deliver');



				$rest->rest_delivery_upto = Input::get('rest_delivery_upto');
				$rest->rest_min_orderamt = Input::get('rest_min_orderamt');



				$rest->rest_lat = $latitude;
				$rest->rest_long = $longitude;


				$rest->rest_type = $rest_type_data;



				$rest->save();

				$rest_id = $rest->id;



				Session::flash('message', 'Restaurant Inserted Sucessfully!');

				//return redirect()->to('/admin/restaurant_list');

				return redirect()->to('/admin/vendor_profile/'.Input::get('vendor_id'));



			}

			else

			{

				DB::table('restaurant')

				->where('rest_id', $rest_id)

				->update(['rest_name' =>Input::get('rest_name'),

					'vendor_id' =>Input::get('vendor_id'),

					'rest_address'=>Input::get('rest_address'),

					'rest_city'=>Input::get('rest_suburb'),

					'rest_zip_code'=>Input::get('rest_zip_code'),

					'rest_desc'=>Input::get('rest_desc'),

					'rest_service'=>$rest_service,

					'rest_state'=>Input::get('rest_state'),

					'rest_suburb'=>Input::get('rest_suburb'),

					'rest_address2'=>'',

					'rest_contact'=>Input::get('rest_contact'),

					'rest_special' => $today_special,



					'rest_lat' => $latitude,
					'rest_long' => $longitude,



					'rest_mon'=>Input::get('mon_from').'_'.Input::get('mon_to'),

					'rest_tues'=>Input::get('tues_from').'_'.Input::get('tues_to'),

					'rest_wed'=>Input::get('wed_from').'_'.Input::get('wed_to'),

					'rest_thus'=>Input::get('thu_from').'_'.Input::get('thu_to'),

					'rest_fri'=>Input::get('fri_from').'_'.Input::get('fri_to'),

					'rest_sat'=>Input::get('sat_from').'_'.Input::get('sat_to'),

					'rest_sun'=>Input::get('sun_from').'_'.Input::get('sun_to'),



					'rest_cuisine'=>$rest_cuisine_data,

					'rest_mindelivery'=>Input::get('rest_mindelivery'),

					'rest_landline'=>Input::get('rest_landline'),

					'rest_commission'=>Input::get('rest_commission'),

					'rest_email'=>Input::get('rest_email'),

					'rest_status'=>Input::get('rest_status'),

					'rest_classi'=>Input::get('rest_classi'),



					'rest_close'=>$rest_close,



					'rest_logo'=>$new_fileName,

					'rest_banner'=> $new_banner,

					'rest_metatag' => $meta,

					'rest_delivery_from' => Input::get('delivery_from'),

					'rest_delivery_to' =>  Input::get('delivery_to'),

					'rest_holiday_from' =>  Input::get('holiday_from'),

					'rest_holiday_to' =>  Input::get('holiday_to'),

					'rest_type' =>   $rest_type_data,

					'rest_price_level' =>Input::get('rest_price_level'),
					'rest_servicetax' =>Input::get('rest_servicetax'),
					'rest_partial_pay' => Input::get('rest_partial_pay'),
					'rest_partial_percent' => Input::get('rest_partial_percent'),
					'rest_cash_deliver' => Input::get('rest_cash_deliver'),
					'rest_delivery_upto' => Input::get('rest_delivery_upto'),
					'rest_min_orderamt' => Input::get('rest_min_orderamt')
				]);





				session()->flash('message', 'Restaurant Updated Sucessfully!');

				//return redirect()->to('/admin/restaurant_list');



				return redirect()->to('/admin/restaurant_view/'.$rest_id);

			}

		}



		public function restaurant_delete($rest_id)

		{

			$rest_id;

			$rest_detail  = DB::table('restaurant')->where('rest_id', '=' ,$rest_id)->get();



			if(!empty($rest_detail))

			{

				$rest_old_logo = $rest_detail[0]->rest_logo;




				$file_remove =$_SERVER["DOCUMENT_ROOT"].'online_food_ordering/uploads/reataurant/'.$rest_old_logo;
			 // $file_remove =$_SERVER["DOCUMENT_ROOT"].'shouta_party/uploads/reataurant/'.$rest_old_logo;

				if(file_exists($file_remove)){

					unlink($file_remove);

				}





				$menu =  DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();

				if(!empty($menu))

				{

					DB::table('menu')->where('restaurant_id', '=', $rest_id)->delete();



				}



				$menu_cat =  DB::table('menu_category')->where('rest_id', '=' ,$rest_id)->get();

				if(!empty($menu_cat))

				{

					DB::table('menu_category')->where('rest_id', '=', $rest_id)->delete();

				}



				DB::table('restaurant')->where('rest_id', '=', $rest_id)->delete();

				return Redirect('/admin/restaurant_list');

			}

		}





		public function view_restaurant()

		{

			$id = Route::current()->getParameter('id');



			$rest_list = DB::table('restaurant')

			->leftJoin('vendor', 'restaurant.vendor_id', '=', 'vendor.vendor_id')

				//	->leftJoin('cuisine', 'restaurant.rest_cuisine', '=', 'cuisine.cuisine_id')
			->leftJoin('cuisine', function($join){
				$join->on(DB::raw("find_in_set(cuisine.cuisine_id, restaurant.rest_cuisine)",DB::raw(''),DB::raw('')));
			})

			->where('rest_id', '=' ,$id)

			->select('restaurant.*','Group_concat(DISTINCT(cuisine.cuisine_name)) AS cuisine_name','vendor.name')
//'cuisine.cuisine_name'
			->orderBy('restaurant.rest_id', 'desc')

			->get();





			$menu_list = DB::table('menu')

			->where('restaurant_id', '=' ,$id)

			->orderBy('menu_order', 'asc')

			->get();





			$service_list = DB::table('service')

			->where('service_restid', '=' ,$id)

			->orderBy('service_id', 'desc')

			->get();

			$promo_list = DB::table('promotion')

			->where('promo_restid', '=' ,$id)

			->orderBy('promo_id', 'desc')

			->get();

			$bank_list = DB::table('bankdetail')

			->where('bank_restid', '=' ,$id)

			->orderBy('bank_id', 'desc')

			->get();



			$menu_cat_detail = '';



			if(!empty($menu_list)){

				$menu_cat_detail = DB::table('menu_category')

				->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

				->where('menu_category.rest_id', '=' ,$id)

				->where('menu_category.menu_id', '=' ,$menu_list[0]->menu_id)

				->select('menu_category.*','menu_category_item.*')

				->orderBy('menu_category.menu_category_id', 'asc')

				->get();

			}







			$data_onview = array('rest_detail' =>$rest_list,'menu_list'=>$menu_list,'id'=>$id,'menu_cate_detail'=>$menu_cat_detail,'service_list'=>$service_list,'promo_list'=>$promo_list,'bank_list'=>$bank_list);

			return View('admin.restaurant_view')->with($data_onview);



		}








		/* MANU FUNCTION */





		public function menu_list()

		{

			DB::connection()->enableQueryLog();



			$menu_list = DB::table('menu')

			->leftJoin('restaurant', 'menu.restaurant_id', '=', 'restaurant.rest_id')

			->select('menu.*','restaurant.rest_name' )

			->orderBy('menu_order', 'asc')

			->get();



			$data_onview = array('menu_list' =>$menu_list);

			return View('admin.menu_list')->with($data_onview);

		}





		public function menu_form()

		{

			$rest_detail  = DB::table('restaurant')->get();



			if(Route::current()->getParameter('id'))

			{

				$id = Route::current()->getParameter('id');

				$menu_detail  = DB::table('menu')->where('menu_id', '=' ,$id)->get();

				$data_onview = array('rest_detail' =>$rest_detail,

					'menu_detail'=>$menu_detail,

					'id'=>$id);

			}

			else

			{

				$id =0;

				$data_onview = array('rest_detail' =>$rest_detail,

					'id'=>$id);

			}

			return View('admin.menu_form')->with($data_onview);

		}






		public function menu_delete($menu_id)

		{

			$menu_cat =  DB::table('menu_category')->where('menu_id', '=' ,$menu_id)->get();

			if(!empty($menu_cat))

			{

				DB::table('menu_category')->where('menu_id', '=', $menu_id)->delete();

			}



			DB::table('menu')->where('menu_id', '=', $menu_id)->delete();

			return Redirect('/admin/menu_list');

		}





		/* MENU CATEGORY FUNCTIONALITY */



		public function menu_category_list()

		{

			DB::connection()->enableQueryLog();



			$menu_cat_list = DB::table('menu_category')

			->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')

			->leftJoin('restaurant', 'menu_category.rest_id', '=', 'restaurant.rest_id')

			->select('menu_category.*','restaurant.rest_name','menu.menu_name' )

			->orderBy('menu.menu_id', 'desc')

			->get();



			$data_onview = array('category_list' =>$menu_cat_list);

			return View('admin.menu_category_list')->with($data_onview);

		}








		public function get_menulist()

		{

			$rest_id =Input::get('restaurant_id');





			$menu_detail  = DB::table('menu')->where('restaurant_id', '=' ,$rest_id)->get();

			$menu_select = '';





			if(!empty($menu_detail)){



				$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >

				<option value="">Select Restaurant </option>';



				foreach($menu_detail as $menu)

				{

					$menu_select .='<option value="'.$menu->menu_id.'">'.$menu->menu_name.'</option>';

				}



				$menu_select .='</select> ';



			}

			else

			{

				$menu_select = '<select name="menu_id" id="menu_id" required="required"class="form-control" >

				<option value="">Select Restaurant </option>

				</select> ';

			}



			echo $menu_select ;



		}









		/* MANAGE MENU CATEGORY ITEM FUNCTIONALITY  */





		public function get_categorylist()

		{

			$rest_id =Input::get('restaurant_id');

			$menu_id =Input::get('menu_id');





			$cate_detail  = DB::table('menu_category')

			->where('rest_id', '=' ,$rest_id)

			->where('menu_id', '=' ,$menu_id)

			->get();

			$cate_select = '';





			if(!empty($cate_detail)){



				$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >

				<option value="">Select Menu Category </option>';



				foreach($cate_detail as $menu)

				{

					$cate_select .='<option value="'.$menu->menu_category_id.'">'.$menu->menu_category_name.'</option>';

				}



				$cate_select .='</select> ';



			}

			else

			{

				$cate_select = '<select name="menu_category" id="menu_category" required="required"class="form-control" >

				<option value="">Select Menu Category  </option>

				</select> ';

			}



			echo $cate_select ;



		}


































		/* AJAX OTHER FUNCTION */


	}

