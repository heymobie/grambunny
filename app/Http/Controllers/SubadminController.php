<?php
namespace App\Http\Controllers;
use Request;
Use DB;
use Hash;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Illuminate\Support\Facades\Input;

class SubadminController extends Controller
{
	public function __construct(){
		//$this->middleware('subadmin');
	}
	public function index(){
		//die('sub-admin');
    	// return Auth::guard('admin')->user();
		$string_date= '';
		$string_user = '';
		$string_order = '';
		$string_completed = '';
		$string_payment = '';
		//$string_date = '';
		$string_vehicle_order='';
		$string_vehicle_payment='';
		$string_vehicle_completed='';
		$m= date("m");
		$de= date("d");
		$y= date("Y");
		for($i=6; $i>=0; $i--){
			/* WEEK DATE */
			$today_date = date('Y-m-d',mktime(0,0,0,$m,($de-$i),$y));
//$string_date .= '"'.$today_date.'",';
			$string_date .= "'".$today_date."', ";
			/* USER DATA */
			$user_list = DB::table('users')
			->select('*')
			->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'), '=' ,$today_date)
			->orderBy('id', 'desc')
			->get();
			$string_user .= count($user_list).',';
			/* ORDER DATA */
			$order_list = DB::table('order')
			->select('*')
			->where('order_create', '=' ,$today_date)
			->get();
			$string_order .= count($order_list).',';
			/* PAYMENT DATA */
			$payment_list = DB::table('order_payment')
			->select('*',DB::raw('SUM(pay_amt) as total_sales'))
			->where('created_at', '=' ,$today_date)
			->get();
			if(empty($payment_list[0]->total_sales))
			{
				$string_payment .= "0.00, ";
			}
			else
			{
				$string_payment .= $payment_list[0]->total_sales.", ";
			}
			/* ORDER SUMITED  DATA */
			$order_SUBMIT_list = DB::table('order')
			->select('*')
			->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'), '=' ,$today_date)
			->where('order_status', '=' ,'5')
			->get();
			$string_completed .= count($order_SUBMIT_list).',';
			/*  VEHCILE GRAPH COUNTER */
			/* COUNT VEHICLE ORDER DATA */
			$vehicle_order_list = DB::table('transport_order_history')
			->select('*')
			->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'), '=' ,$today_date)
			->where('order_status', '=' ,'1')
			->get();
			$string_vehicle_order .= count($vehicle_order_list).',';
			/* PAYMENT DATA */
			$vehicle_payment_list = DB::table('transport_payment')
			->select('*',DB::raw('SUM(pay_amt) as total_sales'))
			->where('created_at', '=' ,$today_date)
			->get();
			if(empty($vehicle_payment_list[0]->total_sales))
			{
				$string_vehicle_payment .= "0.00, ";
			}
			else
			{
				$string_vehicle_payment .= $vehicle_payment_list[0]->total_sales.", ";
			}
			/* ORDER SUMITED  DATA */
			$order_SUBMIT_list = DB::table('transport_order_history')
			->select('*')
			->where(DB::raw('DATE_FORMAT(created_at,"%Y-%m-%d")'), '=' ,$today_date)
			->where('order_status', '=' ,'5')
			->get();
			$string_vehicle_completed .= count($order_SUBMIT_list).',';
			/**/
//echo "<br>";
		}
		$string_date = rtrim($string_date,', ');
		$string_user = rtrim($string_user,',');
		$string_order = rtrim($string_order,',');
		$string_payment = rtrim($string_payment,', ');
		$string_completed = rtrim($string_completed,',');
		$string_vehicle_order = rtrim($string_vehicle_order,',');
		$string_vehicle_payment = rtrim($string_vehicle_payment,',');
		$string_vehicle_completed = rtrim($string_vehicle_completed,',');
		$data_onview = array('string_date' =>$string_date,
			'string_user'=>$string_user,
			'string_order'=>$string_order,
			'string_payment'=>$string_payment,
			'string_completed'=>$string_completed,
			'string_vehicle_order'=>$string_vehicle_order,
			'string_vehicle_payment'=>$string_vehicle_payment,
			'string_vehicle_completed'=>$string_vehicle_completed
		);
		return view('subadmin.dashboard')->with($data_onview);
	}

	public function loginform()
	{
		return view('subadmin.auth.login');
	}


	public function login_action(Request $request)
	{
		$userdata = array(
			'email'     => Input::get('email'),
			'password'  => Input::get('password')
		);
		if(Auth::guard('subadmin')->attempt($userdata)){
			$userRole = Auth::guard('subadmin')->user()->role;
			$userStatus = Auth::guard('subadmin')->user()->status;
			if($userRole!=2){
				Session::flash('Errormessage', 'You do not have access as a sub-admin.');
				return redirect()->to('/sub-admin');
			}else if($userStatus!=1){
				Session::flash('Errormessage', 'Your account is deactivated by Admin.');
				return redirect()->to('/sub-admin');
			}else{

				/* expire in 1 hour */
				if(!empty($_POST["remember"])) {
					setcookie ("email",$_POST["email"],time()+ 3600);
					setcookie ("password",$_POST["password"],time()+ 3600);
					setcookie ("remember",$_POST["remember"],time()+ 3600);
                //echo "Cookies Set Successfuly";
				} else {
					setcookie("email","");
					setcookie("password","");
					setcookie("remember","");
                //echo "Cookies Not Set";
				}
				return redirect()->to('/sub-admin/dashboard');
			}
		}else{
			Session::flash('Errormessage', 'Invalid username or password.');
			return Redirect::to('/sub-admin');

		}
	}


	public function logout(Request $request) {
		Auth::logout();
		return Redirect::to('/sub-admin');
	}


	public function showPasswordForm(){
		return view('subadmin.changepassword');
	}
	public function updatepassword()
	{
		$subadmin_id = Auth::guard('subadmin')->user()->id;
		$credentials = [
			'password' => Request::get('old_password')
		];
		if(Auth::guard('subadmin')->attempt($credentials))
		{
			DB::table('admins')
			->where('id', $subadmin_id)
			->update(['password' =>  Hash::make(Request::get('password'))]);
			Session::flash('Succes', 'Password Update Sucessfully.');
			return redirect()->to('/sub-admin/change_password');
		}
		else
		{
			Session::flash('Error', 'Failed to update password.');
			return redirect()->to("/sub-admin/change_password");
		}
	}

	function showactivelog()
	{
		$log_data = DB::table('login_history')
		->Join('users', 'login_history.log_userid', '=', 'users.id')
		->orderBy('login_history.log_id','desc')
		->get();
		$data_onview = array('log_data' =>$log_data
	);
		return view('admin.active_loglist')->with($data_onview);
	}
	public function showactivelog_delete($id)
	{
		DB::table('login_history')->where('log_id', '=', $id)->delete();
		Session::flash('message', 'User Log Information Deleted Sucessfully.');
		return Redirect('/admin/active_log');
	}
	function showmenulog()
	{
		$log_data = DB::table('view_menu_history')
		->select('view_menu_history.*', 'users.name','users.lname','restaurant.rest_name')
		->leftJoin('users', 'view_menu_history.user_id', '=', 'users.id')
		->Join('restaurant', 'view_menu_history.rest_id', '=', 'restaurant.rest_id')
		->get();
		$data_onview = array('log_data' =>$log_data	);
		return view('admin.menu_loglist')->with($data_onview);
	}
	public function showmenulog_delete($id)
	{
		DB::table('view_menu_history')->where('id', '=', $id)->delete();
		Session::flash('message', 'User Viewmenu Log Information Deleted Sucessfully.');
		return Redirect('/admin/viewmenu_log');
	}
	public function showsetting()
	{
		//show_data
		$admin_detail = DB::table('admins')
		->select('*')
		->get();
		$data_onview = array('admin_detail' =>$admin_detail	);
		return view('subadmin.general_setting_form')->with($data_onview);
	}
	public function update_admin_setting()
	{
		$subadmin_id = Auth::guard('subadmin')->user()->id;
		DB::table('admins')
		->where('id', $subadmin_id)
		->update(['show_data' => (Request::get('show_data'))]);
		Session::flash('Succes', 'Setting Update Sucessfully.');
		return redirect()->to('/sub-admin/general_setting');
	}
}
