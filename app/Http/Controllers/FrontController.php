<?php

namespace App\Http\Controllers;

use App\Exceptions\Handler;

use App\Http\Requests;

use App\User;

use Auth;

use Carbon\Carbon;

use Cart;

use DB;

use Faker\Provider\Uuid;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Foundation\Auth\ThrottlesLogins;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Input;

//use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Validator;

//use PHPMailer\PHPMailer;

use Session;

use Illuminate\Contracts\Encryption\DecryptException;

use Illuminate\Support\Facades\Mail;

class FrontController extends Controller

{



	/** To register user

	 * @param  Request

	 * @return HTTP redirect

	 *

	 */

	public function signInPage()

	{

		return view("auth.login");

	}


	public function guest_register()
	{

		$state_list = DB::table('states')->get();

  		$data = array('state_list'=>$state_list);

		return view("auth.guest_register")->with($data);

	}


	public function webRegisterguest(Request $request)
	{

		//print_r($request->dob);die;

		$request->all();

		$this->validate($request,[

		 	"firstname"	=> "required|min:3",

		 	"lastname"	=> "required|min:3",

		 	"email"		=> "required|email",

		 //	"city"		=> "required",

		 //	"dob"		=> "required|date",

		 	"phone"		=> "required|min:10"

		 	//"password"	=> "required|min:6"

		]);


	 $useremail  = DB::table('users')->where('email', '=' , $request->email)->get();

        if(count($useremail)>0){

       	DB::table('users')
        ->where('email',$request->email)
		->update(['token'=>$request->_token, 'name'=>$request->firstname, 'lname'=>$request->lastname, 'user_mob'=>$request->phone]);

		$last_id = DB::table('users')
        ->where('email',$request->email)
		->value('id');

	    Session::put('guest_id', $last_id);

        }else{

        $password = 'GB123456';

		$user= new User();

		$user->token = $request->_token;//$token=str_random(16);

		$user->name = $request->firstname;

		$user->lname = $request->lastname;

		$user->email = $request->email;

		$user->password = Hash::make($password);

		$user->user_mob = $request->phone;

		$user->user_address = '';

		$user->user_city = '';

		$user->user_states = '';

		$user->user_zipcode = '';

		$user->dob = '';

		$user->license_front = '';

		$user->license_back = '';

		$user->marijuana_card = '';

		$user->customer_type = '';

		$user->profile_image = "user.jpg";

		$user->user_lat =  '';

		$user->user_long = '';

		$user->user_status = 1;

		$user->user_role = 2;

		$user->email_verified_at = Carbon::now();

		/* Mail::send('auth.emails.email-verification',$user->toArray(), function($message) use ($user)

        {

            $message->from(config("app.webmail"), config("app.mailname"));

            $message->subject("Verify Your Email");

            $message->to($user->email);

        }); */

		$user->save();

		$last_id =  $user->id;

	    Session::put('guest_id', $last_id);

	   }

        return redirect()->to('/checkout');


		//return redirect()->route("registrationSuccess",["id" => $user->id]);

	}



	public function signUpPage()

	{

		$state_list = DB::table('states')->get();

  		$data = array('state_list'=>$state_list);

		return view("auth.register")->with($data);

	}


	public function webRegister(Request $request)
	{

		$request->all();

		$this->validate($request,[

		 	"firstname"	=> "required|min:1",

		 	"email"		=> "required|email|unique:users",

		 	"city"		=> "required",

		 	"dob"		=> "required|date",

		 	"phone"		=> "required|min:10",

		 	"password"	=> "required|min:6"

		]);

         $marijuana_card = '';

		$useremail  = DB::table('vendor')->where('email', '=' , Input::get('email'))->first();


        if(!empty($useremail)){

        return redirect()->back()->withErrors(["email" => "The email has already been taken."])->withInput();

        }



      if(preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $request->dob)) {

       }else{

      return redirect()->back()->withErrors(["dob" => "Please use valid date format (mm/dd/yyyy)"])->withInput();

       }

		$date=Carbon::now();

        $minDate= $date->subYear(21);

		$dob=Carbon::createFromFormat("m/d/Y",$request->dob);

		$dob = date("Y-m-d", strtotime($request->dob));

		 if (!$minDate->greaterThan($dob)) {

		return redirect()->back()->withErrors(["dob" => "Date must be greater then 21 Year."])->withInput();

		 }

		//print_r($dob );die;
		$address = Input::get('address');
		// $user_city = Input::get('user_city');
		// $user_states = Input::get('user_states');
		$zipcode = Input::get('zipcode');

		/* $addressnew = $zipcode;

		$url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

		$response = file_get_contents($url);

		$data = json_decode($response);
		if(!empty($data->results)){
			$geo=$data->results[0]->geometry->location;
		}else{
			$url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$addressnew."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

			$response = file_get_contents($url);

			$data = json_decode($response);
			$geo=$data->results[0]->geometry->location;

		} */

	

		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/user/",$license_front=str_random(16).'.'.$request->license_front->extension()) : "";

		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/user/",$license_back=str_random(16).'.'.$request->license_back->extension()) : "";

		$request->hasFile("marijuana_card") ? $request->file("marijuana_card")->move("public/uploads/user/",$marijuana_card=str_random(16).'.'.$request->marijuana_card->extension()) : "";


		$user= new User();

		$user->token = $token=str_random(16);

		$user->name = $request->firstname;

		$user->lname = $request->lastname;

		$user->email = $request->email;

		$user->password = Hash::make($request->password);

		$user->user_mob = $request->phone;

		$user->user_address = $request->address;

		$user->user_city = $request->city;

		$user->user_states = $request->state;

		$user->user_zipcode = $request->zipcode;

		$user->dob = $dob;

		// $user->license_front = $license_front;

		// $user->license_back = $license_back;

		// $user->marijuana_card = $marijuana_card;

		// $user->customer_type = $request->customer_type;

		$user->profile_image = "user.jpg";

		$user->user_lat =  '';

		$user->user_long = '';

		$user->user_status = 1;

		$user->email_verified_at = '';//Carbon::now();


		Mail::send('auth.emails.email-verification', $user->toArray(), function($message) use ($user) {

			$message->from(config("app.webmail"), config("app.mailname"));
			$message->subject("Verify Your Email");
			$message->to($user->email);
			//$message->cc('votivedeepak.php@gmail.com');
		});

		//echo $smailcheck; die;


		    $user->save();

		    $phone = str_replace("(","",$request->phone);
		    $phoneno = str_replace(") ","",$phone);
		    $phonenos = str_replace("-","",$phoneno);

		    $mobile = '1'.$phonenos;

		    Session::put('user_mob', $request->phone);

		    $token = encrypt($token);

			$smstext = 'Please Click https://www.grambunny.com/email-verification/'.$token.' to verify your mobile.' ;

			//$msg_reg_otp ='Please <a href="{{ route("EmailVerificationLink",["token" => encrypt($token)]) }}">Click here</a> to verify your mobile.' ;

			$curl = curl_init();

			$from = "12013002832";
			$to =  $mobile; // "16263495815";

   	   $data = array(
              'from'=> '12013002832',
              'text'=> $smstext,
              'to'=> $to,
              'api_key'=> 'c2aa3522',
              'api_secret'=> 'o5NWJ25e5FbszCxH'

           );

	     $sendsms = json_encode($data, JSON_FORCE_OBJECT);


          $curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://rest.nexmo.com/sms/json",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS =>$sendsms,
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json"
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);


		return redirect()->route("registrationSuccess",["id" => $user->id]);

	}



	public function registrationSuccess(Request $request){

		return view("auth.success");

	}



	public function verifyEmail(Request $request)

    {

        $token=decrypt($request->token);

        $user=User::where("token",$token)->first();

        if (is_null($user->email_verified_at)) {

         	$user->email_verified_at=Carbon::now();

          	$user->update();

		    Auth::guard("user")->loginUsingId($user->id);

            return redirect()->route("home")->with("emailVerify",true);

        }

        else{

			Auth::guard("user")->loginUsingId($user->id);

            return redirect()->route("home")->with("alreadyverify",true);

        }

        return abort(404);

    }

    public function webAuth(Request $request) {

			$this->validate($request,[

				"email" => "required|email",

				"password" => "required"

			]);


            if (Auth::guard("user")->attempt(["email" => $request->email,"password" => $request->password,"user_status" => 1])) {

				if ($request->has("remember_me")) {

					setcookie('remember',json_encode(["email" => $request->email,"password" => $request->password]), time() + (86400 * 30), "/");

				}

				else{

					setcookie ("remember","",time()-3600,"/");

				}

				if (is_null(Auth::guard("user")->user()->email_verified_at)) {

					$this->sendVerificationEmail(Auth::guard("user")->user());

					Auth::guard("user")->logout();

					return redirect()->back()->with("warning","Email verification link has been send to your mail");

				}



				if($request->has("redirect_url") AND $request->get("redirect_url")!=''){

				$user_id = auth()->guard('user')->user()->id;

				$existuser = DB::table('cart')->where('user_id','=',strval($user_id))->get();

                if(count($existuser)>0){

                foreach ($existuser as $key => $value) {

                $getcdata = DB::table('cart')->where('user_id',Session::get("_token"))->where('ps_id',$value->ps_id)->first();

                if(!empty($getcdata->quantity)){

                $cqty = $value->quantity+$getcdata->quantity;

                DB::table('cart')->where('user_id',$user_id)->where('ps_id',$value->ps_id)->update(['quantity' => $cqty]);

                DB::table("cart")->where("user_id",$getcdata->user_id)->where('ps_id',$getcdata->ps_id)->delete();

                }

                }

                }else{

                DB::table('cart')->where('user_id',Session::get("_token"))->update(['user_id' => $user_id]);

                }

				if($request->get("redirect_url") == 'https://www.grambunny.com/cart'){

                	$url= "https://www.grambunny.com/checkout";
                	return redirect($url);

                }else{

                	return redirect($request->get("redirect_url"));

                }

				}else{

					return redirect()->route("userAccount");

				}

			}

			else{

				return redirect()->back()->withErrors(["email" => "Credentials do not match our records!","password" => " "])->withInput();

			}

		}



		// public function webAuthAjax(Request $request){

		// 	$validator=Validator::make($request->all(),[

		// 		"email" => "required|email",

		// 		"password" => "required"

		// 	]);

		// 	if ($validator->fails()) {

		// 		return response()->json(["errors" => $validator->errors(),"code" => 0],401);

		// 	}



		// }



		public function sendVerificationEmail($user)

		{

			if (is_null($user->email_verified_at)) {

				Mail::send('auth.emails.email-verification',$user->toArray(), function($message) use ($user){

		            $message->from(config("app.webmail"), config("app.mailname"));

		            $message->subject("Verify Your Email");

		            $message->to($user->email);

	        	});

	        	return true;

			}

			return false;

		}



    public function forgotPassword(Request $request)

    {

    	return view("auth.passwords.email");

    }



    public function sendPasswordResetLink(Request $request)

    {

    	$this->validate($request,[

    		"email" => "required|email"

    	]);

      	$customer = User::where('email',$request->email)->first();

      	if (!$customer) return redirect()->back()->withErrors(['email' => 'Email address not found!'])->withInput();

		$genrateToken=str_random(60);

		DB::table('password_resets')->insert([

		  'email' => $request->email,

		  'token' => $genrateToken,

		  'created_at' => Carbon::now()

		]);

		Mail::send('auth.emails.password', ["firstname" => $customer->name,"token" => $genrateToken],function ($message) use ($customer){

			$message->to($customer->email);

			$message->subject('Forgot password');

			$message->from(config("app.webmail"), config("app.mailname"));

		});

		return redirect()->back()->with("success" , "Reset password link has been sent to your email address.");

  	}



  	public function ResetPasswordPage(Request $request)

	{



		try {

			$token=decrypt($request->token);

		} catch (DecryptException $e) {

	      	return view("auth.passwords.reset",["invalid"=>"Reset password link has been expired or Invalid!"]);

			 // replace this with 404 page

		}

	    $validateToken=DB::table("password_resets")->where("token",$token)->first();

	    if (!$validateToken) {

	      return view("auth.passwords.reset")->with("invalid","Reset password link has been expired or Invalid!");

	    }

	    $currentTime = Carbon::now();

	    if ($currentTime->diffInMinutes($validateToken->created_at) < 15) {

	      return view("auth.passwords.reset")->with("token",$token);

	    }

	    else {

	      return view("auth.passwords.reset")->with("invalid","Reset password link has been expired or Invalid!");

	    }



  	}



  	public function updatePassword(Request $request){

  		try {

			$token=decrypt($request->token);



		} catch (DecryptException $e) {

	      	return redirect()->back()->with("invalid","Reset password link has been expired or Invalid!");

		}



		$this->validate($request, [

		    'password' => 'required|min:6||confirmed',

		]);



		$password=$request->password;

        $tokenData=DB::table("password_resets")->where("token",$token)->first();

        if (is_null($tokenData)) {

	      	return redirect()->back()->with("invalid","Reset password link has been expired or Invalid!");

        }

        $user=User::where("email",$tokenData->email)->first();

        $user->password=Hash::make($password);

        if ($user->update()) {

          	$tokenData=DB::table("password_resets")->where("email",$user->email)->delete();

        }

        return redirect()->route("signInPage")->with("success" ,"Password updated successfully!");

  	}



  	public function userLogout()
  	{


  		if(isset(auth()->guard('user')->user()->id)){

  		$user_id = auth()->guard('user')->user()->id;

  		DB::table('cart')->where('user_id',$user_id)->delete();

  		Auth::guard("user")->logout();

  		Session::forget('cart_uid');

  	    }

  		return redirect()->route("home");

  	}



	//include('../../smtp/class.phpmailer.php');

public function test_mail(){

	$sendmail = redirect()->action('MyMailer@index');

	if($sendmail){ echo "success" ;}else{ echo "faild"; }

		/* $data['vendor_name'] = 'Deepak';

		$data['email_content'] = 'This is test content.';

		$inData = array();

		$res = Mail::send('emails.registration_email', $data, function ($message) {

			    //$m->from('hello@app.com', 'Your Application');

			   // $m->to($user->email, $user->name)->subject('Your Reminder!');

				$message->from('app@grambunny.com', 'grambunny');

				$message->to('deepaksanidhya@gmail.com');

				//$message->bcc('votiveshweta@gmail.com');

				//$message->bcc('votivemobile.pankaj@gmail.com');

				$message->subject('Registration on www.grambunny.com');

			});

		var_dump($res); */

}



  public function about_us()
  	{


  		$page = DB::table('pages')->where('page_id',7)->first();

  		$testimonials = DB::table('testimonial')->where("status",1)->get();

		$data_onview = array('page_data' =>$page,'testimonials' =>$testimonials);

    	return view('about-us')->with($data_onview);

  }
  public function about_us_mobile()
  	{


  		$page = DB::table('pages')->where('page_id',7)->first();

  		$testimonials = DB::table('testimonial')->where("status",1)->get();

		$data_onview = array('page_data' =>$page,'testimonials' =>$testimonials);

    	return view('about-us-mobile')->with($data_onview);

  }

    	  public function contact_us_mobile()

  	{
  		$page = DB::table('pages')->where('page_id',2)->first();
  		$testimonials = DB::table('testimonial')->where("status",1)->get();
  		$data_onview = array('page_data' =>$page,'testimonials' =>$testimonials);
  		return view('contact-us-mobile')->with($data_onview);
  	}

  	  	  	  public function help_mobile()

  	{
  		$page = DB::table('pages')->where('page_id',3)->first();
  		$testimonials = DB::table('testimonial')->where("status",1)->get();
  		$data_onview = array('page_data' =>$page,'testimonials' =>$testimonials);
  		return view('help-page')->with($data_onview);
  	}



    public function shop_now()
  	{

  		$get_category = DB::table('product_service_category')->get();

		$data_onview = array('product_category' => $get_category);

    	return view('shop-now')->with($data_onview);

  }

    public function productdetail()
  	{

  		$page = DB::table('pages')->where('page_id',8)->first();

		$data_onview = array('page_data' => $page);

    	return view('productdetail')->with($data_onview);

      }



    public function terms()

  	{



  		$page = DB::table('pages')->where('page_id',4)->first();



		$data_onview = array('page_data' =>$page);



    	return view('terms')->with($data_onview);

    }


    public function terms_mobile()
  	{

  		$page = DB::table('pages')->where('page_id',4)->first();

		$data_onview = array('page_data' =>$page);

    	return view('terms-mobile')->with($data_onview);

    }






    public function privacy_policy()

  	{



  		$page = DB::table('pages')->where('page_id',5)->first();



		$data_onview = array('page_data' =>$page);



    	return view('privacy-policy')->with($data_onview);

    }

    public function privacy_policy_mobile()

  	{



  		$page = DB::table('pages')->where('page_id',5)->first();



		$data_onview = array('page_data' =>$page);



    	return view('privacy-policy-mobile')->with($data_onview);

    }



    public function faq()

  	{

  		$page = DB::table('pages')->where('page_id',6)->first();

		$faq = DB::table('faq')->where('status',1)->get();

		$data_onview = array('page_data' =>$page,'faq_data' =>$faq);

    	return view('faq-page')->with($data_onview);

    }

    public function faq_mobile()

  	{

  		$page = DB::table('pages')->where('page_id',6)->first();

		$faq = DB::table('faq')->where('status',1)->get();

		$data_onview = array('page_data' =>$page,'faq_data' =>$faq);

    	return view('faq-page-mobile')->with($data_onview);

    }


    public function contact_us()

  	{

  		$page = DB::table('pages')->where('page_id',2)->first();

		$data_onview = array('page_data' =>$page);

    	return view('contact-us')->with($data_onview);

    }

    public function contactus(Request $request)
  	{

        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $email = $request->get('email');
        $subject = $request->get('subject');
        $message = $request->get('message');

            $mailData = [
			  'name' => $fname." ".$lname,
			  'email' => $email,
			  'subject' => $subject,
			  'messagess' => $message
			];

			/* Mail::send('emails.contact_us', $mailData, function($message) use($mailData) {
			    $message -> to('info@grambunny.com'); //info@grambunny.com
			    $message -> subject('Grambunny - Contact Us');
			    $message -> from($mailData['email']);
			}); */

           /* Mail::send('emails.contact_us', $data, function ($message) use ($data) {

                $message->from('info@grambunny.com', 'www.grambunny.com');

                $message->to($data['email']);

                $message->subject('grambunny - Contact Us');

            }); */


		Session::flash('message', 'Thank you.');

		return redirect()->to('/contact-us');

    }

   public function help()

  	{

  		$page = DB::table('pages')->where('page_id',3)->first();

		$data_onview = array('page_data' =>$page);

    	return view('help-page')->with($data_onview);

    }

			/********************************************************************************************************************/

			public function register(Request $request) {

				if ($request->isMethod('post')) {

					$carrier_id = '';

					$carrier_name = '';

					$carrier_email = '';

					$carrier_id =  Input::get('user_mob_type');

					switch($carrier_id)

					{

						case 1:

						$carrier_id = '1';

						$carrier_name = 'Alltel';

						$carrier_email = '@message.alltel.com';

						break;

						case 2:

						$carrier_id = '2';

						$carrier_name = 'AT&T';

						$carrier_email = '@txt.att.net';

						break;

						case 3:

						$carrier_id = '3';

						$carrier_name = 'Boost Mobile';

						$carrier_email = '@myboostmobile.com';

						break;

						case 4:

						$carrier_id = '4';

						$carrier_name = 'Sprint';

						$carrier_email = '@messaging.sprintpcs.com';

						break;

						case 5:

						$carrier_id = '5';

						$carrier_name = 'T-Mobile';

						$carrier_email = '@tmomail.net';

						break;

						case 6:

						$carrier_id = '6';

						$carrier_name = 'U.S. Cellular';

						$carrier_email = '@email.uscc.net';

						break;

						case 7:

						$carrier_id = '7';

						$carrier_name = 'Verizon';

						$carrier_email = '@vtext.com';

						break;

						case 8:

						$carrier_id = '8';

						$carrier_name = 'Virgin Mobile';

						$carrier_email = '@vmobl.com';

						break;

						case 9:

						$carrier_id = '9';

						$carrier_name = 'Republic Wireless';

						$carrier_email = '@text.republicwireless.com';

						break;

					}

					User::create([

						'name' => Input::get('name'),

						'lname' => Input::get('lname'),

						'email' => Input::get('email'),

						'user_mob' => Input::get('user_mob'),

						'user_status' => '1',

						'password' => bcrypt(Input::get('password')),

						'user_pass'=> md5(Input::get('password')),

						'carrier_id'=> $carrier_id,

						'carrier_name'=> $carrier_name,

						'carrier_email'=> $carrier_email,

						'dummy1'=> Input::get('dummy1'),

						'dummy2'=>Input::get('dummy2'),

						'dummy3'=> Input::get('dummy3')

					]);

					/******************* SEND EMAIL AFTER PAYMENT  *******************/

					$email_content_detail = DB::table('email_content')

					->select('*')

					->where('email_id', '=' ,'1')

					->get();

					$username  = Input::get('name').' '.Input::get('lname');

					if(empty($username))

					{

						$aa_u = explode('@',Input::get('email'));

						$username  = $aa_u[0];

					}

					$data = array(

						'vendor_name'=>$username ,

						'vendor_email'=>Input::get('email'),

						'email_content'=>$email_content_detail[0]->email_content

					);

					Mail::send('emails.registration_email', $data, function ($message) use ($data) {

            //$m->from('hello@app.com', 'Your Application');

           // $m->to($user->email, $user->name)->subject('Your Reminder!');

						$message->from('app@grambunny.com', 'grambunny');

						$message->to($data['vendor_email']);

						$message->bcc('votiveshweta@gmail.com');

						$message->bcc('votivemobile.pankaj@gmail.com');

						$message->subject('Registration on www.grambunny.com');

					});

					/******************* SEND EMAIL AFTER PAYMENT  *******************/

				}

		//return Redirect::away('login');

				if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password'),'user_status'=>'1'])) 	{

					$user_id =  Auth::user()->id;

					$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

					$cart_user = '';

					$cart_user['deliveri_time']='';

					$cart_user['pickup_date'] = '';

					$cart_user['pickup_time'] ='';

					$cart_user['order_instruction'] = '';

					$cart_user['firstname_order'] = trim($user_detail[0]->name);

					$cart_user['lastname_order'] = trim($user_detail[0]->lname);

					$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

					$cart_user['email_order'] = trim($user_detail[0]->email);

					$cart_user['address_order'] = trim($user_detail[0]->user_address);

					$cart_user['city_order'] = trim($user_detail[0]->user_city);

					$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

					$cart_user['user_type'] = trim(1);

					$cart_user['user_id'] = trim($user_detail[0]->id);

		//print_r($cart_user);

		//exit;

			//return redirect()->intended('checkout');

					$cart = Cart::content();

					if(Cart::count()){

						/*$cart_user['deliveri_time']='';

						$cart_user['pickup_date'] = '';

						$cart_user['pickup_time'] ='';

						$cart_user['order_instruction'] = '';

						$cart_user['rest_meta_url']=trim(Session::get('rest_meta_url'));

						$cart_user['rest_id']=trim(Session::get('rest_id'));

						$cart_user['service_area'] = trim(Session::get('service_area'));

						$cart_user['service_type'] = trim(Session::get('service_option'));

						$cart_user['firstname_order'] = trim($user_detail[0]->name);

						$cart_user['lastname_order'] = trim($user_detail[0]->lname);

						$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

						$cart_user['email_order'] = trim($user_detail[0]->email);

						$cart_user['address_order'] = trim($user_detail[0]->user_address);

						$cart_user['city_order'] = trim($user_detail[0]->user_city);

						$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

						$cart_user['user_type'] = trim(1);

						$cart_user['user_id'] = trim($user_detail[0]->id);*/

						$cart_user['delivery_add1'] =trim(Session::get('cart_userinfo')['delivery_add1']);

						$cart_user['delivery_add2'] =trim(Session::get('cart_userinfo')['delivery_add2']);

						$cart_user['delivery_surbur'] =trim(Session::get('cart_userinfo')['delivery_surbur']);

						$cart_user['delivery_postcode'] =trim(Session::get('cart_userinfo')['delivery_postcode']);

						$cart_user['delivery_number'] =trim(Session::get('cart_userinfo')['delivery_number']);

						$cart_user['delivery_fee'] =trim(Session::get('cart_userinfo')['delivery_fee']);

						$cart_user['sub_total'] =trim(Session::get('cart_userinfo')['sub_total']);

						$cart_user['service_tax'] =trim(Session::get('cart_userinfo')['service_tax']);

						$cart_user['delivery_fee_min'] =trim(Session::get('cart_userinfo')['delivery_fee_min']);

						$cart_user['delivery_fee_min_remaing'] =trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

						$cart_user['total_charge'] =trim(Session::get('cart_userinfo')['total_charge']);

						/***** DISCOUNT (PROMOTION) START */

						$cart_user['promo_discount'] = trim(Session::get('cart_userinfo')['promo_discount']);

						$cart_user['promo_discount_id'] =trim(Session::get('cart_userinfo')['promo_discount_id']);

						$cart_user['promo_discount_text'] =trim(Session::get('cart_userinfo')['promo_discount_text']);

						$cart_user['order_promo_detail'] = trim(Session::get('cart_userinfo')['order_promo_detail']);

						$cart_user['order_food_promo'] = trim(Session::get('cart_userinfo')['order_food_promo']);

						$cart_user['order_food_promo_applied'] = trim(Session::get('cart_userinfo')['order_food_promo_applied']);

						$cart_user['order_pmt_type'] ='1';

						/********** END *******/

						$cart_user['pickup_date'] =trim(Session::get('cart_userinfo')['pickup_date']);;

						$cart_user['pickup_time'] =trim(Session::get('cart_userinfo')['pickup_time']);;

						$cart_user['pickup_time_formate'] =trim(Session::get('cart_userinfo')['pickup_time_formate']);;

						$cart_user['rest_meta_url']=trim(Session::get('cart_userinfo')['rest_meta_url']);;

						$cart_user['rest_id']=trim(Session::get('cart_userinfo')['rest_id']);;

						$cart_user['deliveri_time']='';

						$cart_user['pickup_date'] = date('Y-m-d');

						$cart_user['rest_open_status'] =trim(Session::get('cart_userinfo')['rest_open_status']);

						$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

						$cart_user['firstname_order'] = trim($user_detail[0]->name);

						$cart_user['lastname_order'] = trim($user_detail[0]->lname);

						$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

						$cart_user['email_order'] = trim($user_detail[0]->email);

						$cart_user['address_order'] = trim($user_detail[0]->user_address);

						$cart_user['city_order'] = trim($user_detail[0]->user_city);

						$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

						$cart_user['user_type'] = trim(1);

						$cart_user['user_id'] =trim($user_detail[0]->id);

						$cart_user['cart_data'] = $cart;

						$cart_user['service_area'] = '';

						$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

						/**  PARTICLA PAYMENT PROCESS ***/

						$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

						$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

						$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

						$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);

						/**  PARTICLA PAYMENT PROCESS END ***/

						Session::set('cart_userinfo',$cart_user);

						Session::set('cart_userinfo', $cart_user);

				//echo $_SERVER['HTTP_REFERER'];

					//	$path = explode('online_food_ordering',$_SERVER['HTTP_REFERER']);

					//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

						$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

						//print_r($path);

						//$new_path = explode('?',$path[1]);

						//echo $path[0];

					//exit;

						if(($path[1]=='/checkout') || ($path[1]=='/payment'))

						{

							//return view('re_checkout');

							return redirect()->to('/checkout');

						}

						else

						{

							return redirect()->to($path[1]);

						}

					}

					else

					{

						Session::set('cart_userinfo', $cart_user);

			//	return redirect()->to('myaccount');

				//$path = explode('online_food_ordering',$_SERVER['HTTP_REFERER']);

					//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

						$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

						$new_path = explode('?',$path[1]);

						if(($new_path[0]=='/demo/order_cancel') || ($new_path[0]=='/demo/order_success') ||

							($new_path[0]=='/demo/transport_order_cancel') || ($new_path[0]=='/demo/transport_order_success')

						)

						{

							return redirect()->to('myaccount');

						}

						else

						{

							return redirect()->to($path[1]);

						}

					}

				}

			}

		public function check_user_login() {

				$cart = Cart::content();

				$uemail = trim(Input::get('user_login'));

				$upassword = trim(Input::get('password'));

				$FILD_NAME = '';

				if(is_numeric ($uemail))

				{

					$FILD_NAME = 'user_mob';

				}

				else

				{

					$FILD_NAME = 'email';

				}

				$uremember = Input::get('remember');

				if($uremember=='on'){

				setcookie('useremail', $uemail, time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('upassword', $upassword, time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('uremember', $uremember, time() + (86400 * 30), "/"); // 86400 = 1 day

				}else{

			   	setcookie('useremail', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('upassword', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('uremember', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				}

				if (Auth::attempt([$FILD_NAME => $uemail, 'password' => $upassword,'user_status'=>'1']))

				{

					$user_id =  Auth::user()->id;

					return $user_id;

			     }else{

			     	return '0';

			     }

		}







public function user_sentOTP(Request $request) {

	$useremail = $request->input('email');

	$user = DB::table('users')->where('email',$useremail)->first();

	$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',2)->first();

	if(!empty($user)){

		$user_id = $user->id;

		/*********** EMAIL FOR SEN OTP START *************/

		$otp_code = trim($this->generateRandomString(6));

		$address = $useremail; //"deepaksanidhya@gmail.com";

		$subject = $usr_fgtpss_email_cotnt->email_title;

		$message = $usr_fgtpss_email_cotnt->email_content.$otp_code;

		$semail = $this->sendMail($address,$subject,$message);

		if(!empty($otp_code)){

			DB::table('users')

				->where('id',$user_id)

				->update(['user_otp'=>$otp_code]);

			Session::flash('Succes', 'OTP has been sent successfully.');

			session()->flash('user_id', $user_id);

			// return redirect()->to('/enter_your_user_otp');

			return view('enter_your_user_otp');

		}else{

			Session::flash('Error', 'Email sent failed.');

			return redirect()->back();

		}

	} else {

		Session::flash('Error', 'Please enter your registered email.');

		return redirect()->back();

	}

}

public function generateRandomString($length = 6) {

   $characters = '0123456789';

   $charactersLength = strlen($characters);

   $randomString = '';

   for ($i = 0; $i < $length; $i++) {

       $randomString .= $characters[rand(0, $charactersLength - 1)];

   }

   return $randomString;

}

public function sendMail($address,$subject,$message)

{

    $mail = new PHPMailer\PHPMailer();

    $mail->IsSMTP(); // telling the class to use SMTP

    $mail->SMTPAuth   = true; // enable SMTP authentication

    $mail->Port       = 25;   // set the SMTP server port

    $mail->Host       = "mail.conceptline.org"; // SMTP server

    $mail->Username   = "app@grambunny.com";     // SMTP server username

    $mail->Password   = "^Lrhz018";            // SMTP server password

    $mail->SetFrom('app@grambunny.com', 'grambunny');

    $mail->Subject = $subject;

    $mail->MsgHTML($message);

    $mail->IsHTML(true); // send as HTML

    $address = $address;

    $mail->AddAddress($address);

   // $mail->AddCC('votivedeepak.php@gmail.com');

    if(!$mail->Send()) {

    //return $mail->ErrorInfo ;

    	return false;

    } else {

    	return true;

    }

}

/*OTP submit*/

public function VerifyUserOTP(Request $request)

{

    $enterOTP = $request->input('user_otp');

    $userID = $request->input('userID');

    $UserData = DB::table('users')->where('id',$userID)->first();

    $saveOTP = $UserData->user_otp;

    $userID = $UserData->id;

    //echo "<pre>1...."; print_r($saveOTP);

    //echo "<pre>2...."; print_r($enterOTP); die;

    if($saveOTP==$enterOTP){

        //echo "Match";

        Session::flash('Succes', 'Your OTP has been verified successfully.');

        session()->flash('userID', $userID);

        return redirect()->to('/user_password/reset_your_user_password');

    }else{

        Session::flash('Otp_Error', 'OTP did not match.');

        session()->keep(['user_id']);

        return redirect()->back();

    }

}

public function setUserNewPasswordForm()

{

    session()->keep(['userID']);

    return view('set_user_new_pass');

}

public function setUserNewPassword(Request $request)

{

	//echo "<pre>"; print_r($request->all()); die;

    $new_pass = $request->input('new_pass');

    $userID = $request->input('userID');

    $affected = DB::table('users')

    ->where('id',$userID)

    ->update(['password'=>bcrypt($new_pass)]);

    if($affected){

        Session::flash('Succes', 'Your password has been changed successfully.');

        return redirect()->back();

    }else{

        Session::flash('Change_pass', 'Somthing is wrong! please try again.');

        session()->keep(['userID']);

        return redirect()->back();

    }

}

			public function authenticate() {

				$cart = Cart::content();

				$FILD_NAME = '';

				if(is_numeric ( Input::get('email')))

				{

					$FILD_NAME = 'user_mob';

				}

				else

				{

					$FILD_NAME = 'email';

				}

				$uremember = Input::get('remember');

                $uemail = Input::get('email');

				$upassword = Input::get('password');

				if($uremember=='on'){

				setcookie('useremail', $uemail, time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('upassword', $upassword, time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('uremember', $uremember, time() + (86400 * 30), "/"); // 86400 = 1 day

				}else{

                //unset($_COOKIE['useremail']);

                //unset($_COOKIE['upassword']);

                //unset($_COOKIE['uremember']);

			   	setcookie('useremail', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('upassword', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				setcookie('uremember', '', time() + (86400 * 30), "/"); // 86400 = 1 day

				}

				if (Auth::attempt([$FILD_NAME => Input::get('email'), 'password' => Input::get('password'),'user_status'=>'1']))

				{

					$user_id =  Auth::user()->id;

					/** MANAGE LOG START **/

					$log_data['log_type'] = 'normal' ;

					$log_data['log_userid'] =$user_id ;

					$log_data['log_date'] = date('Y-m-d H:i');

					$log_data['log_time']= date('Y-m-d H:i');

					$log_data['log_by'] = 'Website' ;

					$log_data['device_id'] = '' ;

					$log_data['device_type'] = '' ;

			//$log_detail = DB::table('login_history')->insert($log_data);

					$log_id =$id = DB::table('login_history')->insertGetId($log_data);

					$update_log =  DB::table('users')->where('id', '=' ,$user_id)->update(['log_id'=>$log_id]);

					/** MANAGE LOG END **/

					$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

					/***** UPDATE CART WHEN GUSET USER IS SIGN-IN  ****/

					$guest_id = Session::get('_token');

					$cart_listing = DB::table('rest_cart');

					$cart_listing = $cart_listing->select('*');

					$cart_listing = $cart_listing->where('guest_id', '=' ,trim($guest_id));

					$cart_listing = $cart_listing->get();

					if($cart_listing)

					{

						DB::table('rest_cart')

						->where('guest_id', trim($guest_id))

						->update(['user_id' =>  trim($user_id),

							'guest_id' =>  trim('')

						]);

					}

					/******************         END           ********/

					$cart_user = '';

		//echo '<pre>';

	//print_r($cart);

	//print_r(Session::All());

					$cart_user['delivery_add1'] =trim(Session::get('cart_userinfo')['delivery_add1']);

					$cart_user['delivery_add2'] =trim(Session::get('cart_userinfo')['delivery_add2']);

					$cart_user['delivery_surbur'] =trim(Session::get('cart_userinfo')['delivery_surbur']);

					$cart_user['delivery_postcode'] =trim(Session::get('cart_userinfo')['delivery_postcode']);

					$cart_user['delivery_number'] =trim(Session::get('cart_userinfo')['delivery_number']);

					$cart_user['delivery_fee'] =trim(Session::get('cart_userinfo')['delivery_fee']);

					$cart_user['sub_total'] =trim(Session::get('cart_userinfo')['sub_total']);

					$cart_user['service_tax'] =trim(Session::get('cart_userinfo')['service_tax']);

					$cart_user['delivery_fee_min'] =trim(Session::get('cart_userinfo')['delivery_fee_min']);

					$cart_user['delivery_fee_min_remaing'] =trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

					$cart_user['total_charge'] =trim(Session::get('cart_userinfo')['total_charge']);

					/***** DISCOUNT (PROMOTION) START */

					$cart_user['promo_discount'] = trim(Session::get('cart_userinfo')['promo_discount']);

					$cart_user['promo_discount_id'] =trim(Session::get('cart_userinfo')['promo_discount_id']);

					$cart_user['promo_discount_text'] =trim(Session::get('cart_userinfo')['promo_discount_text']);

					$cart_user['order_promo_detail'] = trim(Session::get('cart_userinfo')['order_promo_detail']);

					$cart_user['order_food_promo'] = trim(Session::get('cart_userinfo')['order_food_promo']);

					$cart_user['order_food_promo_applied'] = trim(Session::get('cart_userinfo')['order_food_promo_applied']);

					$cart_user['order_pmt_type'] ='1';

					/********** END *******/

					$cart_user['pickup_date'] =trim(Session::get('cart_userinfo')['pickup_date']);;

					$cart_user['pickup_time'] =trim(Session::get('cart_userinfo')['pickup_time']);;

					$cart_user['pickup_time_formate'] =trim(Session::get('cart_userinfo')['pickup_time_formate']);;

					$cart_user['rest_meta_url']=trim(Session::get('cart_userinfo')['rest_meta_url']);;

					$cart_user['rest_id']=trim(Session::get('cart_userinfo')['rest_id']);;

					$cart_user['deliveri_time']='';

					$cart_user['pickup_date'] = date('Y-m-d');

					$cart_user['rest_open_status'] =trim(Session::get('cart_userinfo')['rest_open_status']);

					$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

					$cart_user['firstname_order'] = trim($user_detail[0]->name);

					$cart_user['lastname_order'] = trim($user_detail[0]->lname);

					$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

					$cart_user['email_order'] = trim($user_detail[0]->email);

					$cart_user['address_order'] = trim($user_detail[0]->user_address);

					$cart_user['city_order'] = trim($user_detail[0]->user_city);

					$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

					$cart_user['user_type'] = trim(1);

					$cart_user['user_id'] =trim($user_detail[0]->id);

					$cart_user['service_area'] = '';

					$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

					/**  PARTICLA PAYMENT PROCESS ***/

					$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

					$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

					$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

					$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);

					/**  PARTICLA PAYMENT PROCESS END ***/

			//return redirect()->intended('checkout');

					if(Cart::count()){

						$transport_data=0;

						$restaurant_data=1;

						$response= array();

						$rest_id ='';

						foreach($cart as $item){

							if(isset($item->options['rest_id']) && ($item->options['rest_id']!=''))

							{

								$rest_id=$item->options['rest_id'];

								break;

							}

						}

						$guest_id = '';

						if(($transport_data==0) && ($restaurant_data==1))

						{

							DB::enableQueryLog();

							$offer_amt=0;

							$offer_applied_on_this_cart='';

							$current_promo='';

							$service_tax=0;

							$sub_total_price=0;

							$promo_id=0;

							$offer_applied_on_food='';

							$food_current_promo = '';

							$json_data_session='';

							$sub_total_cart=0;

							$cart_food_id='';

							$cart_detail ='';

							$cart_listing = DB::table('rest_cart');

							$cart_listing = $cart_listing->select('*');

							$cart_listing = $cart_listing->where('user_id', '=' ,trim($user_id));

							$cart_listing = $cart_listing->where('guest_id', '=' ,trim($guest_id));

							$cart_listing = $cart_listing->where('rest_id', '=' ,trim($rest_id));

							$cart_listing = $cart_listing->orderBy('cart_id', 'asc');

							$cart_listing = $cart_listing->get();

			//print_r($cart_listing)	;

		//exit;

							if($cart_listing)

							{

								Cart::destroy();

								$delivery_fee = 0;

								$rest_detail = DB::table('restaurant')

								->where('rest_id', '=' ,$rest_id)

								->where('rest_status', '!=' , 'INACTIVE')

								->orderBy('rest_id', 'asc')

								->groupBy('rest_id')

								->get();

								$promo_list = DB::table('promotion')

								->where('promo_restid', '=' ,$rest_id)

								->where('promo_status', '=' ,'1')

								->orderBy('promo_buy', 'desc')

								->groupBy('promo_id')

								->get();

								$service_charge = $rest_detail[0]->rest_servicetax;

								if($cart_listing[0]->order_type=='delivery')

								{

									$delivery_fee = $rest_detail[0]->rest_mindelivery;

								}

								$sub_total_cart = 0;

								$json_data_session = '';

								$addon_list ='';

								$food_current_promo = '';

								foreach($cart_listing as $clist)

								{

									/********FOOD DETAILS /******/

									$rest_id  = $clist->rest_id;

									$menu_id = $clist->menu_id;

									$menu_cate_id =$clist->menu_catid;

									$menu_subcate_id = $clist->menu_subcatid;

									$addon_id = $clist->menu_addonid;

									$foodsize_price =0;

									$food_addonPrice = 0;

									$total_price =0 ;

									$item_name_cc = '';

									$item_name_cc_ar = '';

									$item_price_cc = '';

									$offer_on_menu_item = '';

									/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/

									foreach($promo_list as $plist )

									{

										if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))

										{

											$current_promo1 = '';

											$food_cart_amount[] = $total_price;

											$food_cart_qty[] =$clist->qty;

											if($plist->promo_on=='schedul')

											{

												$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

												$db_day = '';

												if(!empty($plist->promo_day))

												{

													$db_day = explode(',',$plist->promo_day);

												}

												if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

												{

													if(

														($plist->promo_start<=date('Y-m-d')) &&

														($plist->promo_end>=date('Y-m-d')) &&

														(empty($plist->promo_day)) &&

														($plist->promo_buy<=$clist->qty)

													)

													{

														$current_promo1['promo_id'] = $plist->promo_id;

														$current_promo1['promo_restid'] = $plist->promo_restid;

														$current_promo1['promo_for'] = $plist->promo_for;

														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

														$current_promo1['promo_on'] = $plist->promo_on;

														$current_promo1['promo_desc'] = $plist->promo_desc;

														$current_promo1['promo_mode'] = $plist->promo_mode;

														$current_promo1['promo_buy'] = $plist->promo_buy;

														$current_promo1['promo_value'] = $plist->promo_value;

														$current_promo1['promo_end'] = $plist->promo_end;

														$current_promo1['promo_start'] = $plist->promo_start;

														$food_current_promo[] = $current_promo1;

														$offer_on_menu_item = $plist->promo_desc;

													}

													if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

														&& (!empty($plist->promo_day))

														&& (in_array($day_name,$db_day))

													)

													{

														$array_amt[]=$plist->promo_buy;

														$current_promo1['promo_id'] = $plist->promo_id;

														$current_promo1['promo_restid'] = $plist->promo_restid;

														$current_promo1['promo_for'] = $plist->promo_for;

														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

														$current_promo1['promo_on'] = $plist->promo_on;

														$current_promo1['promo_desc'] = $plist->promo_desc;

														$current_promo1['promo_mode'] = $plist->promo_mode;

														$current_promo1['promo_buy'] = $plist->promo_buy;

														$current_promo1['promo_value'] = $plist->promo_value;

														$current_promo1['promo_end'] = $plist->promo_end;

														$current_promo1['promo_start'] = $plist->promo_start;

														$food_current_promo[] = $current_promo1;

														$offer_on_menu_item = $plist->promo_desc;

													}

												}

												elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))

												{

													$db_day = explode(',',$plist->promo_day);

													if(in_array($day_name,$db_day))

													{

														$array_amt[]=$plist->promo_buy;

														$current_promo1['promo_id'] = $plist->promo_id;

														$current_promo1['promo_restid'] = $plist->promo_restid;

														$current_promo1['promo_for'] = $plist->promo_for;

														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

														$current_promo1['promo_on'] = $plist->promo_on;

														$current_promo1['promo_desc'] = $plist->promo_desc;

														$current_promo1['promo_mode'] = $plist->promo_mode;

														$current_promo1['promo_buy'] = $plist->promo_buy;

														$current_promo1['promo_value'] = $plist->promo_value;

														$current_promo1['promo_end'] = $plist->promo_end;

														$current_promo1['promo_start'] = $plist->promo_start;

														$food_current_promo[] = $current_promo1;

														$offer_on_menu_item = $plist->promo_desc;

													}

												}

											}

											elseif($plist->promo_on=='qty')

											{

												if(($plist->promo_buy<=$clist->qty))

												{

													$current_promo1['promo_id'] = $plist->promo_id;

													$current_promo1['promo_restid'] = $plist->promo_restid;

													$current_promo1['promo_for'] = $plist->promo_for;

													$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

													$current_promo1['promo_on'] = $plist->promo_on;

													$current_promo1['promo_desc'] = $plist->promo_desc;

													$current_promo1['promo_mode'] = $plist->promo_mode;

													$current_promo1['promo_buy'] = $plist->promo_buy;

													$current_promo1['promo_value'] = $plist->promo_value;

													$current_promo1['promo_end'] = $plist->promo_end;

													$current_promo1['promo_start'] = $plist->promo_start;

													$food_current_promo[] = $current_promo1;

													$offer_on_menu_item = $plist->promo_desc;

												}

											}

										}

									}

									/*************************** END ******************************/

									$menu_list = DB::table('menu')

									->where('restaurant_id', '=' ,$clist->rest_id)

									->where('menu_id', '=' ,$clist->menu_id)

									->where('menu_status', '=' ,'1')

									->orderBy('menu_order', 'asc')

									->get();

						// dd(DB::getQueryLog());

									$menu_array = '';

									$food_array='';

									if(!empty($menu_list))

									{

										$menu_id = $menu_list[0]->menu_id;

										$menu_name = $menu_list[0]->menu_name;

										$menu_item_count = 1;

										$food_detail='';

										$menu_cat_detail = DB::table('menu_category')

										->where('menu_category.rest_id', '=' ,$rest_id)

										->where('menu_category.menu_id', '=' ,$menu_id)

										->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

										->where('menu_category.menu_cat_status', '=' ,'1')

										->select('menu_category.*')

										->orderBy('menu_category.menu_category_id', 'asc')

										->get();

										$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

										$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

										$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

										$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

										$food_detail['food_name_ar'] = $item_name_cc_ar = $menu_cat_detail[0]->menu_category_name_ar;

										$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

										$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

										$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

										$food_detail['food_subitem_title']='';

										if($menu_cat_detail[0]->menu_category_portion=='yes')

										{

											$food_detail['food_subitem_title'] = 'Choose a size';

											$food_size = DB::table('category_item')

											->where('rest_id', '=' ,$rest_id)

											->where('menu_id', '=' ,$menu_id)

											->where('menu_category', '=' ,$menu_cate_id)

											->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

											->where('menu_item_status', '=' ,'1')

											->select('*')

											->orderBy('menu_cat_itm_id', 'asc')

											->groupBy('menu_item_title')

											->get();

											$food_detail['food_size_detail']=$food_size;

											$item_price_cc =$food_size[0]->menu_item_price;

											$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

											$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

										}

										else

										{

											$food_detail['food_size_detail']=array();

											$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

										}

										$addon_list='';

										if(!empty($addon_id)){

											$addons = DB::table('menu_category_addon')

											->where('addon_restid', '=' ,$rest_id)

											->where('addon_menuid', '=' ,$menu_id)

											->where('addon_status', '=' ,'1')

											->where('addon_menucatid', '=' ,$menu_cate_id)

											->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

											->select('*')

											->orderBy('addon_id', 'asc')

											->groupBy('addon_groupname')

											->get();

											if(!empty($addons))

											{

												foreach($addons as $ad_list)

												{

													$option_type = 0;

													if(substr($ad_list->addon_groupname, -1)=='*')

													{

														$option_type = 1;

													}

													$group_name[]=$ad_list->addon_groupname;

													$ff['addon_gropname'] = $ad_list->addon_groupname;

													$ff['addon_type'] = $ad_list->addon_option;

													$ff['addon_mandatory_or_not'] = $option_type;

													$addon_group = DB::table('menu_category_addon')

													->where('addon_restid', '=' ,$rest_id)

													->where('addon_menuid', '=' ,$menu_id)

													->where('addon_status', '=' ,'1')

													->where('addon_groupname', '=' ,$ad_list->addon_groupname)

													->where('addon_menucatid', '=' ,$menu_cate_id)

													->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

													->select('*')

													->orderBy('addon_id', 'asc')

													->get();

													$group_list[]=$addon_group;

													$addon_group_list ='';

													foreach($addon_group as $group_list_loop)

													{

													//$addon_group_list[]=array('')

														$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

															'addon_menucatid'=>$group_list_loop->addon_menucatid,

															'addon_groupname'=>$group_list_loop->addon_groupname,

															'addon_option'=>$group_list_loop->addon_option,

															'addon_name'=>$group_list_loop->addon_name,

															'addon_price'=>$group_list_loop->addon_price,

															'addon_status'=>$group_list_loop->addon_status

														);

														$addon_list[]=array('name'=>$group_list_loop->addon_groupname.': '.$group_list_loop->addon_name,

															'price'=>$group_list_loop->addon_price,

															'id'=>$group_list_loop->addon_id);

														$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

														$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

													}

													$ff['addon_detail'] = $addon_group_list;

													$food_detail['food_addon'][]=$ff;

												}

											}

										}

										else

										{

											$food_detail['food_addon'] = array();

										}

										$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

									}

									/******** FOOD DETAIL END ****/

							//echo $clist->qty;

							//echo '<br>';

									$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));

							/*	$cart_detail[] = array(

										"cart_id"=> $clist->cart_id,

										"rest_cartid"=> $clist->rest_cartid,

										"user_id" => $clist->user_id,

										"guest_id" => $clist->guest_id,

										"rest_id"=> $clist->rest_id,

										"menu_id"=> $clist->menu_id,

										"food_id"=> $clist->menu_catid,

										"food_size_id"=> $clist->menu_subcatid,

										"food_addonid"=> $clist->menu_addonid,

										"qty"=> $clist->qty,

										"special_instruction"=>$clist->special_instruction,

										"deviceid"=> $clist->deviceid,

										"devicetype"=> $clist->devicetype,

										"food_detail"=>$food_array,

										"foodsize_price"=>$foodsize_price,

										"food_addonPrice"=>$food_addonPrice,

										"total_price"=>$total_price

									);*/

									Cart::add(array(

										'id'=>$clist->menu_catid,

										'name' => $item_name_cc,

										'name_ar' => $item_name_cc_ar,

										'qty' => $clist->qty,

										'price' => number_format(($item_price_cc*$clist->qty),2),

										'options'=>['menu_catid' => $clist->menu_catid,

										'instraction' => $clist->special_instruction,

										'rest_id' => $clist->rest_id,

										'menu_id' => $clist->menu_id,

										'option_name'=>$clist->menu_subcatid,

										'addon_data'=>$addon_list,

										'offer_on_item'=>$offer_on_menu_item

									]

								)

								);

									$json_data_session[$clist->cart_id] =	array("rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,	   "name"=>$item_name_cc,"qty"=>$clist->qty,"price"=>$item_price_cc,"instruction"=>$clist->special_instruction,			    "options"=> array ( "menu_catid"=>$clist->menu_catid,

										"rest_id"=>$clist->rest_id,

										"menu_id"=>$clist->menu_id,

										"option_name"=> $clist->menu_subcatid,

										"addon_data"=>$addon_list,

										"offer_name"=>$offer_on_menu_item),

									"tax"=>'',

									"subtotal"=>$item_price_cc

								);

								}

						/*print_r($food_current_promo);

						print_r($food_amount);*/

						$serv_tax  = (($sub_total_cart*$service_charge)/100);

						/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/

						$current_promo = '';

						$offer_applied_on_this_cart='';

						if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))

						{

							$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

							/*$promo_list = DB::table('promotion')

										->where('promo_restid', '=' ,$rest_id)

										->where('promo_status', '=' ,'1')

										->where('promo_for', '=' ,'order')

										->orderBy('promo_buy', 'desc')

										->groupBy('promo_id')

										->get();*/

								// dd(DB::getQueryLog());

								//print_r($promo_list);

										//

							//	exit;

										$current_promo = '';

										$array_amt = '';

										foreach($promo_list as $plist )

										{

											if($plist->promo_for=='order')

											{

												$current_promo1 = '';

												if($plist->promo_on=='schedul')

												{

													$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

													$db_day = '';

													if(!empty($plist->promo_day))

													{

														$db_day = explode(',',$plist->promo_day);

													}

													if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

													{

														if(

															($plist->promo_start<=date('Y-m-d')) &&

															($plist->promo_end>=date('Y-m-d')) &&

															(empty($plist->promo_day)) &&

															((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))

														)

														{

															$array_amt[]=$plist->promo_buy;

															$current_promo1['promo_id'] = $plist->promo_id;

															$current_promo1['promo_restid'] = $plist->promo_restid;

															$current_promo1['promo_for'] = $plist->promo_for;

															$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

															$current_promo1['promo_on'] = $plist->promo_on;

															$current_promo1['promo_desc'] = $plist->promo_desc;

															$current_promo1['promo_mode'] = $plist->promo_mode;

															$current_promo1['promo_buy'] = $plist->promo_buy;

															$current_promo1['promo_value'] = $plist->promo_value;

															$current_promo1['promo_end'] = $plist->promo_end;

															$current_promo1['promo_start'] = $plist->promo_start;

															$current_promo[] = $current_promo1;

														}

														if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

															&& (!empty($plist->promo_day))

															&& (in_array($day_name,$db_day) &&

																((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

														)

														{

															$array_amt[]=$plist->promo_buy;

															$current_promo1['promo_id'] = $plist->promo_id;

															$current_promo1['promo_restid'] = $plist->promo_restid;

															$current_promo1['promo_for'] = $plist->promo_for;

															$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

															$current_promo1['promo_on'] = $plist->promo_on;

															$current_promo1['promo_desc'] = $plist->promo_desc;

															$current_promo1['promo_mode'] = $plist->promo_mode;

															$current_promo1['promo_buy'] = $plist->promo_buy;

															$current_promo1['promo_value'] = $plist->promo_value;

															$current_promo1['promo_end'] = $plist->promo_end;

															$current_promo1['promo_start'] = $plist->promo_start;

															$current_promo[] = $current_promo1;

														}

													}

													elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) &&

														((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

													{

														$db_day = explode(',',$plist->promo_day);

														if(in_array($day_name,$db_day))

														{

															$array_amt[]=$plist->promo_buy;

															$current_promo1['promo_id'] = $plist->promo_id;

															$current_promo1['promo_restid'] = $plist->promo_restid;

															$current_promo1['promo_for'] = $plist->promo_for;

															$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

															$current_promo1['promo_on'] = $plist->promo_on;

															$current_promo1['promo_desc'] = $plist->promo_desc;

															$current_promo1['promo_mode'] = $plist->promo_mode;

															$current_promo1['promo_buy'] = $plist->promo_buy;

															$current_promo1['promo_value'] = $plist->promo_value;

															$current_promo1['promo_end'] = $plist->promo_end;

															$current_promo1['promo_start'] = $plist->promo_start;

															$current_promo[] = $current_promo1;

														}

													}

												}

												else

												{

													if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))

													{

														$array_amt[]=$plist->promo_buy;

														$current_promo1['promo_id'] = $plist->promo_id;

														$current_promo1['promo_restid'] = $plist->promo_restid;

														$current_promo1['promo_for'] = $plist->promo_for;

														$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

														$current_promo1['promo_on'] = $plist->promo_on;

														$current_promo1['promo_desc'] = $plist->promo_desc;

														$current_promo1['promo_mode'] = $plist->promo_mode;

														$current_promo1['promo_buy'] = $plist->promo_buy;

														$current_promo1['promo_value'] = $plist->promo_value;

														$current_promo1['promo_end'] = $plist->promo_end;

														$current_promo1['promo_start'] = $plist->promo_start;

														$current_promo[] = $current_promo1;

													}

												}

											}

										}

									}

									else

									{

										$current_promo = array();

										$offer_applied_on_this_cart='';

									}

									$offer_amt = 0;

									$cal_sub_totla = $sub_total_cart;

									if((count($current_promo)>0) && (!empty($array_amt))) {

										$maxs = max($array_amt);

										$key_name = array_search ($maxs, $array_amt);

										$k=0;$i=0;

										foreach($current_promo as $pval)

										{

											if($key_name==$k)

											{

												if($pval['promo_mode']=='$')

												{

													$i++;

													$offer_applied_on_this_cart=$pval['promo_desc'];

													$offer_amt = ($offer_amt+$pval['promo_value']);

													$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);

												}

												if(($pval['promo_mode']=='%') )

													{	$i++;

														$offer_applied_on_this_cart=$pval['promo_desc'];

														$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);

														$cal_sub_totla = ($cal_sub_totla - $promo_val_per);

														$offer_amt = ($offer_amt+$promo_val_per);

													}

													if(($pval['promo_mode']=='0') )

													{

														$i++;

														$offer_applied_on_this_cart=$pval['promo_desc'];

													}

												}

												$k++;

											}

										}

										else{

											$offer_applied_on_this_cart='';

											$current_promo = array();

										}

										/** PROMOTIONS CODE END  **/

										/******************  FOOD ITEM  */

					/*$food_disocunt

					$food_current_promo*/

					$food_offer_amt = '';

					$offer_applied_on_food= '';

					if(!empty($food_current_promo) && (count($food_current_promo)>0))

					{

						$k=0;$i=0;

						foreach($food_current_promo as $pval)

						{

							$item_price = $food_cart_amount[$k];

							$item_qty = $food_cart_qty[$k];

							$cal_sub_total=number_format(($item_price*$item_qty),2);

							if($pval['promo_mode']=='$')

							{

								$i++;

								$offer_applied_on_food=$pval['promo_desc'];

								$food_offer_amt = ($offer_amt+$pval['promo_value']);

							}

							if(($pval['promo_mode']=='%') )

								{	$i++;

									$offer_applied_on_food=$pval['promo_desc'];

									$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);

									$food_offer_amt = ($offer_amt+$promo_val_per);

								}

								if(($pval['promo_mode']=='0') )

								{

									$i++;

									$offer_applied_on_food=$pval['promo_desc'];

								}

								$k++;

							}

						}

						else

						{

							$food_current_promo= array();

						}

						/*******************  END  ****/

						if(($sub_total_cart-$offer_amt-$food_offer_amt)<$delivery_fee)

						{

							$total_amt = number_format(($delivery_fee+$serv_tax),2);

						}

						else

						{

							$aa_total = ($sub_total_cart-$offer_amt-$food_offer_amt)+$serv_tax;

							$total_amt = number_format(($aa_total),2);

						}

					/*print_r($current_promo);

					print_r(json_encode($json_data_session));

					exit;*/

					DB::table('rest_cart')

					->where('user_id', '=' ,trim($user_id))

					->where('guest_id', '=' ,trim($guest_id))

					->where('rest_id', '=' ,trim($rest_id))

					->update(['cart_detail' => (json_encode($json_data_session)),

						'offer_detail'=> (json_encode($current_promo)),

						'offer_amt'=> trim($offer_amt),

						'offer_applied'=>$offer_applied_on_this_cart,

						'food_promo_list'=>json_encode($food_current_promo),

						'food_offer_applied'=>json_encode($offer_applied_on_food),

						'service_tax_amt'=>trim($serv_tax),

						'delivery_amt'=> trim($delivery_fee),

						'subtotal'=> trim($sub_total_cart),

						'total_amt'=> trim($total_amt)

					]);

					$response['message'] = "All deatil of cart data";

					$response['cart_detail'] = $cart_detail;

					$response['other_detail'] = array(

						'subtotal'=> number_format($sub_total_cart,2),

						'rest_service' =>$service_charge,

						'delivery_fee'=>number_format($delivery_fee,2),

						'service_tax'=>number_format($serv_tax,2),

						'promo_list'=>$current_promo,

						'discount_amt'=>number_format($offer_amt,2),

						'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,

						'food_promo_list'=>$food_current_promo,

						'offer_applied_on_item'=>$offer_applied_on_food,

						'total_amt'=>number_format($total_amt,2));

					$response['ok'] = 1;

					/*********************************  SET ALL CART SESSION AFTER LOGIN ***************************************/

					$cart_user['delivery_fee'] =trim(number_format($delivery_fee,2));

					$cart_user['sub_total'] =trim(number_format($sub_total_cart,2));

					$cart_user['service_tax'] =trim(number_format($serv_tax,2));

					$cart_user['delivery_fee_min'] =trim(number_format($delivery_fee,2));

					$cart_user['delivery_fee_min_remaing'] =trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

					$cart_user['total_charge'] =trim(number_format($total_amt,2));

					/***** DISCOUNT (PROMOTION) START */

					$cart_user['promo_discount'] = trim(number_format($offer_amt,2));

					$cart_user['promo_discount_id'] =trim(Session::get('cart_userinfo')['promo_discount_id']);

					$cart_user['promo_discount_text'] =trim($offer_applied_on_this_cart);

					$cart_user['order_promo_detail'] = ($current_promo);

					$cart_user['order_food_promo'] = ($food_current_promo);

					$cart_user['order_food_promo_applied'] = trim($offer_applied_on_food);

					$cart_user['order_pmt_type'] ='1';

					/********** END *******/

					$cart = Cart::content();

					$cart_user['cart_data'] = $cart;

					/**  PARTICLA PAYMENT PROCESS ***/

					if(Session::get('cart_userinfo')['partical_amt']>0)

					{

						$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

						$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

						$partical_amt =number_format((($total_amt*Session::get('cart_userinfo')['partical_percent'])/100),2);

						$partical_remaning_amt =number_format(($total_amt-$partical_amt),2);

						$cart_user['partical_amt'] = trim($partical_amt);

						$cart_user['partical_remaning_amt'] = trim($partical_remaning_amt);

					}

					else

					{

						$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

						$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

						$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

						$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);

					}

					/**  PARTICLA PAYMENT PROCESS END ***/

					/**/

					$cart_user['delivery_add1'] =trim(Session::get('cart_userinfo')['delivery_add1']);

					$cart_user['delivery_add2'] =trim(Session::get('cart_userinfo')['delivery_add2']);

					$cart_user['delivery_surbur'] =trim(Session::get('cart_userinfo')['delivery_surbur']);

					$cart_user['delivery_postcode'] =trim(Session::get('cart_userinfo')['delivery_postcode']);

					$cart_user['delivery_number'] =trim(Session::get('cart_userinfo')['delivery_number']);

					$cart_user['pickup_date'] =trim(Session::get('cart_userinfo')['pickup_date']);;

					$cart_user['pickup_time'] =trim(Session::get('cart_userinfo')['pickup_time']);;

					$cart_user['pickup_time_formate'] =trim(Session::get('cart_userinfo')['pickup_time_formate']);;

					$cart_user['rest_meta_url']=trim(Session::get('cart_userinfo')['rest_meta_url']);;

					$cart_user['rest_id']=trim(Session::get('cart_userinfo')['rest_id']);;

					$cart_user['deliveri_time']='';

					$cart_user['pickup_date'] = date('Y-m-d');

					$cart_user['rest_open_status'] =trim(Session::get('cart_userinfo')['rest_open_status']);

					$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

					$cart_user['firstname_order'] = trim($user_detail[0]->name);

					$cart_user['lastname_order'] = trim($user_detail[0]->lname);

					$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

					$cart_user['email_order'] = trim($user_detail[0]->email);

					$cart_user['address_order'] = trim($user_detail[0]->user_address);

					$cart_user['city_order'] = trim($user_detail[0]->user_city);

					$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

					$cart_user['user_type'] = trim(1);

					$cart_user['user_id'] =trim($user_detail[0]->id);

					$cart_user['service_area'] = '';

					$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

				/**

				/*********************************  END *****************************************/

				  //print_r($cart_user);

			}

			Session::set('cart_userinfo',$cart_user);

				//	Session::set('cart_userinfo', $cart_user);

			$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

					//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

			$new_path = explode('?',$path[1]);

			if($path[1]=='/checkout')

			{

						//return view('re_checkout');

				return redirect()->to('/checkout');

			}

			elseif($path[1]=='/payment')

			{

				return redirect()->to('/checkout');

			}

			else

			{

				return redirect()->to($path[1]);

			}

		}

	}

	else

	{

			/*echo 'Test';

			exit;*/

			Session::set('cart_userinfo', $cart_user);

			//	return redirect()->to('myaccount');

				//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

			$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

			$new_path = explode('?',$path[1]);

			//print_r($path);

			//exit;

			//return redirect()->to($path[1]);

			if(($new_path[0]=='/demo/order_cancel') || ($new_path[0]=='/demo/order_success')  ||

				($new_path[0]=='/demo/transport_order_cancel') || ($new_path[0]=='/demo/transport_order_success')

			)

			{

				return redirect()->to('myaccount');

			}

			else

			{

				return redirect()->to($path[1]);

			}

		}

	} else {

			//Session::flash('login_message_error', 'Old Password Not Match!');

		Session::flash('login_message_error', 'Invalid  email/mobile number  or password!');

			//return view('login', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));

			//return redirect()->to('/');

			//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

		$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

		$new_path = explode('?',$path[1]);

			//print_r($new_path); die;

			//return redirect()->to($path[1]);

		if($new_path[0]=='/demo/order_cancel')

		{

			return redirect()->to('/');

		}

		elseif($new_path[0]=='/demo/order_success')

		{

			return redirect()->to('/');

		}

		elseif($new_path[0]=='/demo/transport_order_cancel')

		{

			return redirect()->to('/');

		}

		elseif($new_path[0]=='/demo/transport_order_success')

		{

			return redirect()->to('/');

		}

		else

		{

			if(Cart::count()){

				$transport_data=0;

				$restaurant_data=1;

				/* CHECK WHICH SESSION IS IN WORKING START */

				foreach($cart as $item){

								/*Cart::destroy();

								$cart_session_val = '0';*/

								if(isset($item->options['vehicle_id']) && ($item->options['vehicle_id']!=''))

								{

									$transport_data=1;

									$restaurant_data=0;

									break;

								}

							}

							/* CHECK WHICH SESSION IS IN WORKING END */

						//return view('re_checkout');

							if( ($transport_data==0) && ($restaurant_data==1))

							{

								return redirect()->to('/checkout');

							}

							elseif( ($transport_data==1) && ($restaurant_data==0))

							{

								return redirect()->to($path[1]);

							}

						}

						else

						{

							return redirect()->to($path[1]);

						}

					//return redirect()->to($path[1]);

					}

				}

			}

			public function authenticate_1() {

				$cart = Cart::content();

				$FILD_NAME = '';

				if(is_numeric ( Input::get('email')))

				{

					$FILD_NAME = 'user_mob';

				}

				else

				{

					$FILD_NAME = 'email';

				}

				if (Auth::attempt([$FILD_NAME => Input::get('email'), 'password' => Input::get('password'),'user_status'=>'1']))

				{

					$user_id =  Auth::user()->id;

					$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

					$cart_user = '';

		/*echo '<pre>';

	print_r($_POST);

	print_r(Session::All());

	exit;*/

	$cart_user['delivery_add1'] =trim(Session::get('cart_userinfo')['delivery_add1']);

	$cart_user['delivery_add2'] =trim(Session::get('cart_userinfo')['delivery_add2']);

	$cart_user['delivery_surbur'] =trim(Session::get('cart_userinfo')['delivery_surbur']);

	$cart_user['delivery_postcode'] =trim(Session::get('cart_userinfo')['delivery_postcode']);

	$cart_user['delivery_number'] =trim(Session::get('cart_userinfo')['delivery_number']);

	$cart_user['delivery_fee'] =trim(Session::get('cart_userinfo')['delivery_fee']);

	$cart_user['sub_total'] =trim(Session::get('cart_userinfo')['sub_total']);

	$cart_user['service_tax'] =trim(Session::get('cart_userinfo')['service_tax']);

	$cart_user['delivery_fee_min'] =trim(Session::get('cart_userinfo')['delivery_fee_min']);

	$cart_user['delivery_fee_min_remaing'] =trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

	$cart_user['total_charge'] =trim(Session::get('cart_userinfo')['total_charge']);

	/***** DISCOUNT (PROMOTION) START */

	$cart_user['promo_discount'] = trim(Session::get('cart_userinfo')['promo_discount']);

	$cart_user['promo_discount_id'] =trim(Session::get('cart_userinfo')['promo_discount_id']);

	$cart_user['promo_discount_text'] =trim(Session::get('cart_userinfo')['promo_discount_text']);

	$cart_user['order_promo_detail'] = trim(Session::get('cart_userinfo')['order_promo_detail']);

	$cart_user['order_food_promo'] = trim(Session::get('cart_userinfo')['order_food_promo']);

	$cart_user['order_food_promo_applied'] = trim(Session::get('cart_userinfo')['order_food_promo_applied']);

	$cart_user['order_pmt_type'] ='1';

	/********** END *******/

	$cart_user['pickup_date'] =trim(Session::get('cart_userinfo')['pickup_date']);;

	$cart_user['pickup_time'] =trim(Session::get('cart_userinfo')['pickup_time']);;

	$cart_user['pickup_time_formate'] =trim(Session::get('cart_userinfo')['pickup_time_formate']);;

	$cart_user['rest_meta_url']=trim(Session::get('cart_userinfo')['rest_meta_url']);;

	$cart_user['rest_id']=trim(Session::get('cart_userinfo')['rest_id']);;

	$cart_user['deliveri_time']='';

	$cart_user['pickup_date'] = date('Y-m-d');

	$cart_user['rest_open_status'] =trim(Session::get('cart_userinfo')['rest_open_status']);

	$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

	$cart_user['firstname_order'] = trim($user_detail[0]->name);

	$cart_user['lastname_order'] = trim($user_detail[0]->lname);

	$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

	$cart_user['email_order'] = trim($user_detail[0]->email);

	$cart_user['address_order'] = trim($user_detail[0]->user_address);

	$cart_user['city_order'] = trim($user_detail[0]->user_city);

	$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

	$cart_user['user_type'] = trim(1);

	$cart_user['user_id'] =trim($user_detail[0]->id);

	$cart_user['service_area'] = '';

	$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

	/**  PARTICLA PAYMENT PROCESS ***/

	$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

	$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

	$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

	$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);

	/**  PARTICLA PAYMENT PROCESS END ***/

			//return redirect()->intended('checkout');

	if(Cart::count()){

		$transport_data=0;

		$restaurant_data=1;

		if(($transport_data==0) && ($restaurant_data==1))

		{

			$cart_user['delivery_add1'] =trim(Session::get('cart_userinfo')['delivery_add1']);

			$cart_user['delivery_add2'] =trim(Session::get('cart_userinfo')['delivery_add2']);

			$cart_user['delivery_surbur'] =trim(Session::get('cart_userinfo')['delivery_surbur']);

			$cart_user['delivery_postcode'] =trim(Session::get('cart_userinfo')['delivery_postcode']);

			$cart_user['delivery_number'] =trim(Session::get('cart_userinfo')['delivery_number']);

			$cart_user['delivery_fee'] =trim(Session::get('cart_userinfo')['delivery_fee']);

			$cart_user['sub_total'] =trim(Session::get('cart_userinfo')['sub_total']);

			$cart_user['service_tax'] =trim(Session::get('cart_userinfo')['service_tax']);

			$cart_user['delivery_fee_min'] =trim(Session::get('cart_userinfo')['delivery_fee_min']);

			$cart_user['delivery_fee_min_remaing'] =trim(Session::get('cart_userinfo')['delivery_fee_min_remaing']);

			$cart_user['total_charge'] =trim(Session::get('cart_userinfo')['total_charge']);

			/***** DISCOUNT (PROMOTION) START */

			$cart_user['promo_discount'] = trim(Session::get('cart_userinfo')['promo_discount']);

			$cart_user['promo_discount_id'] =trim(Session::get('cart_userinfo')['promo_discount_id']);

			$cart_user['promo_discount_text'] =trim(Session::get('cart_userinfo')['promo_discount_text']);

			$cart_user['order_promo_detail'] = trim(Session::get('cart_userinfo')['order_promo_detail']);

			$cart_user['order_food_promo'] = trim(Session::get('cart_userinfo')['order_food_promo']);

			$cart_user['order_food_promo_applied'] = trim(Session::get('cart_userinfo')['order_food_promo_applied']);

			$cart_user['order_pmt_type'] ='1';

			/********** END *******/

			$cart_user['pickup_date'] =trim(Session::get('cart_userinfo')['pickup_date']);;

			$cart_user['pickup_time'] =trim(Session::get('cart_userinfo')['pickup_time']);;

			$cart_user['pickup_time_formate'] =trim(Session::get('cart_userinfo')['pickup_time_formate']);;

			$cart_user['rest_meta_url']=trim(Session::get('cart_userinfo')['rest_meta_url']);;

			$cart_user['rest_id']=trim(Session::get('cart_userinfo')['rest_id']);;

			$cart_user['deliveri_time']='';

			$cart_user['pickup_date'] = date('Y-m-d');

			$cart_user['rest_open_status'] =trim(Session::get('cart_userinfo')['rest_open_status']);

			$cart_user['order_instruction'] = trim(Session::get('cart_userinfo')['order_instruction']);

			$cart_user['firstname_order'] = trim($user_detail[0]->name);

			$cart_user['lastname_order'] = trim($user_detail[0]->lname);

			$cart_user['tel_order'] = trim($user_detail[0]->user_mob);

			$cart_user['email_order'] = trim($user_detail[0]->email);

			$cart_user['address_order'] = trim($user_detail[0]->user_address);

			$cart_user['city_order'] = trim($user_detail[0]->user_city);

			$cart_user['pcode_order'] = trim($user_detail[0]->user_zipcode);

			$cart_user['user_type'] = trim(1);

			$cart_user['user_id'] =trim($user_detail[0]->id);

			$cart_user['cart_data'] = $cart;

			$cart_user['service_area'] = '';

			$cart_user['service_type'] = trim(Session::get('cart_userinfo')['service_type']);

			/**  PARTICLA PAYMENT PROCESS ***/

			$cart_user['partical_payment_alow'] = trim(Session::get('cart_userinfo')['partical_payment_alow']);

			$cart_user['partical_percent'] =trim(Session::get('cart_userinfo')['partical_percent']);

			$cart_user['partical_amt'] = trim(Session::get('cart_userinfo')['partical_amt']);

			$cart_user['partical_remaning_amt'] = trim(Session::get('cart_userinfo')['partical_remaning_amt']);

			/**  PARTICLA PAYMENT PROCESS END ***/

			Session::set('cart_userinfo',$cart_user);

				//	Session::set('cart_userinfo', $cart_user);

			$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

					//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

			$new_path = explode('?',$path[1]);

			if($path[1]=='/demo/checkout')

			{

						//return view('re_checkout');

				return redirect()->to('/checkout');

			}

			elseif($path[1]=='/payment')

			{

				return redirect()->to('/checkout');

			}

			else

			{

				return redirect()->to($path[1]);

			}

		}

	}

	else

	{

		Session::set('cart_userinfo', $cart_user);

			//	return redirect()->to('myaccount');

				//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

		$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

		$new_path = explode('?',$path[1]);

			//print_r($path);

			//exit;

			//return redirect()->to($path[1]);

		if(($new_path[0]=='/demo/order_cancel') || ($new_path[0]=='/demo/order_success')  ||

			($new_path[0]=='/demo/transport_order_cancel') || ($new_path[0]=='/demo/transport_order_success')

		)

		{

			return redirect()->to('myaccount');

		}

		else

		{

			return redirect()->to($path[1]);

		}

	}

} else {

			//Session::flash('login_message_error', 'Old Password Not Match!');

	Session::flash('login_message_error', 'Invalid  email/mobile number  or password!');

			//return view('login', array('title' => 'Welcome', 'description' => '', 'page' => 'home'));

			//return redirect()->to('/');

			//$path = explode('shouta_party',$_SERVER['HTTP_REFERER']);

	$path = explode('www.grambunny.com',$_SERVER['HTTP_REFERER']);

	$new_path = explode('?',$path[1]);

			//return redirect()->to($path[1]);

	if($new_path[0]=='/demo/order_cancel')

	{

		return redirect()->to('/');

	}

	elseif($new_path[0]=='/demo/order_success')

	{

		return redirect()->to('/');

	}

	elseif($new_path[0]=='/demo/transport_order_cancel')

	{

		return redirect()->to('/');

	}

	elseif($new_path[0]=='/demo/transport_order_success')

	{

		return redirect()->to('/');

	}

	else

	{

		if(Cart::count()){

			$transport_data=0;

			$restaurant_data=1;

			/* CHECK WHICH SESSION IS IN WORKING START */

			foreach($cart as $item){

								/*Cart::destroy();

								$cart_session_val = '0';*/

								if(isset($item->options['vehicle_id']) && ($item->options['vehicle_id']!=''))

								{

									$transport_data=1;

									$restaurant_data=0;

									break;

								}

							}

							/* CHECK WHICH SESSION IS IN WORKING END */

						//return view('re_checkout');

							if( ($transport_data==0) && ($restaurant_data==1))

							{

								return redirect()->to('/checkout');

							}

							elseif( ($transport_data==1) && ($restaurant_data==0))

							{

								return redirect()->to($path[1]);

							}

						}

						else

						{

							return redirect()->to($path[1]);

						}

					//return redirect()->to($path[1]);

					}

				}

			}

			public function logout() {

				$user_id =  Auth::user()->id;

				$user_detail = 	 DB::table('users')->where('id', '=' ,$user_id)->get();

				$log_id =$user_detail[0]->log_id;

				/** MANAGE LOG START **/

				$log_data['log_outtime'] = date('Y-m-d H:i');

				$update_log =  DB::table('login_history')->where('log_id', '=' ,$log_id)->update($log_data);

				/** MANAGE LOG END **/

				Auth::logout();

				Session::flush();

				return redirect()->to('/home');

			}

		public function videos_popup_play(Request $request){

           $video_id = $request->video_id;

          $videos_popup= DB::table('vendor')->where('vendor_id', '=', $video_id)->first();

          $data['videopath'] = \URL::to('').'/public/'.$videos_popup->video;

         $data['videos'] = $videos_popup;
       $returnHTML = view('videos_popup_play')->with($data)->render();
       return response()->json($returnHTML);

}

 public function customvalidation(Request $request)
    {

      $data['email'] = $request->email;

      $validation1 =  DB::table('users')->where('email', $request->email)->get();

      $emailn = count($validation1);

      if(empty($request->email)){

          $emailn = 2;
      }


        return response()->json(['emailcount' => $emailn]);
    }

		}

