<?php



namespace App\Http\Controllers;





Use DB;

use Hash;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Validator;

use Route; 

use Illuminate\Http\Request;



use App\Transport_order_history;

use App\Transport_order;



use App\Transport_payment;

use App\Transport_payment_history;

class AdmintransportorderController extends Controller

{

    public function __construct(){

    	$this->middleware('admin');

    }


	public function show_orderlist()

	{

	 	/*

		DB::connection()->enableQueryLog();	



			$cuisine_list = DB::table('cuisine')

			 ->select('*')

			->orderBy('cuisine_id', 'desc')

			->get();

		

        $data_onview = array('cuisine_list' =>$cuisine_list);

		

		*/

		

		$user_id = 0;

		if(Route::current()->getParameter('userid'))

		{

			$user_id = Route::current()->getParameter('userid');

		}

		

		$order_detail  =  DB::table('transport_order')	;	

		$order_detail  = $order_detail->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id');

		$order_detail  = $order_detail->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id');

		$order_detail  = $order_detail->leftJoin('users', 'transport_order.user_id', '=', 'users.id');

		

		if($user_id>0)

		{

			$order_detail  =  $order_detail->where('transport_order.user_id', '=', $user_id);

		}

		

		$order_detail  =  $order_detail->where('transport_order.order_status', '!=', '8');

						

		$order_detail  = $order_detail->select('*');

		$order_detail  = $order_detail->orderBy('transport_order.order_create', 'desc');

		$order_detail  = $order_detail->get();	

			

		



		

  		$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id); 

		

		return View('admin.transport_order_list')->with($data_onview);

		

		

		

	} 

	

	public function show_orderdetail()

	{

		//echo 'test';

		

		$order_id = $_REQUEST['order'];

		

		

		

		$order_detail  =  DB::table('transport_order')		

						->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id')

						->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id')

						->leftJoin('users', 'transport_order.user_id', '=', 'users.id')

						->leftJoin('transport_payment',

						 'transport_order.order_id', '=', 'transport_payment.pay_orderid')

						->where('transport_order.order_id', '=', $order_id)

						->select('transport_order.*','transport.transport_name', 'transport.transport_address', 'transport.transport_address2', 'transport.transport_suburb', 'transport.transport_state', 'transport.transport_pcode', 'transport.transport_contact','transport_payment.pay_tx','vehicle.vehicle_rego')

						->orderBy('transport_order.order_create', 'desc')

						->get();

						

						

	$order_history =  DB::table('transport_order_history')	

							->where('transport_order_history.order_id', '=', $order_id)	

							->where('transport_order_history.order_status', '=', '1')	

							->select('*')

							->get();	

									

		if(!empty($order_history))

		{

			$order_create_date	= $order_history[0]->created_at;						

		}

		else

		{

			$order_create_date	= $order_detail[0]->created_at;		

		}	

						

		//echo '<pre>'				;

		//print_r($order_detail )		;		

		

		$data_onview = array('order_detail'=>$order_detail,'order_create_date'=>$order_create_date); 

		return View('admin.transport_order_view')->with($data_onview);

						

	}

	

	public function update_order_action()

	{

		$order_id = Input::get('order_id');

		$old_status = Input::get('old_status');

		$new_order_status = Input::get('order_status');

		

		DB::enableQueryLog(); 

		

		

		

		$order_cancel_reason = '';

		if(Input::get('order_cancel_reason'))

		{

			$order_cancel_reason = Input::get('order_cancel_reason');	

		}

		

		

	

	

		$order_detail  =  DB::table('transport_order')		

						->where('order_id', '=', $order_id)

						->select('*')

						->orderBy('transport_order.order_create', 'desc')

						->get();

						

				

				

		/* UPDATE PAYMENT TABLE BASED ON ORDER CANCEL OR REJECT OR COMPLETED BY PARTNER START  */		

		

		if($new_order_status == '2' || $new_order_status == '6')

		{

			

			$payment_detail  =  DB::table('transport_payment')		

							->where('pay_orderid', '=', $order_id)

							->select('*')

							->get();

			

				$pay_his = new Transport_payment_history;

				

				

				$pay_his->pay_id = $payment_detail[0]->pay_id;

				$pay_his->pay_orderid = $payment_detail[0]->pay_orderid;

				$pay_his->pay_userid = $payment_detail[0]->pay_userid;

				$pay_his->pay_tx = $payment_detail[0]->pay_tx;

				$pay_his->pay_st = $payment_detail[0]->pay_st;

				$pay_his->pay_amt = $payment_detail[0]->pay_amt;

				$pay_his->pay_cc = $payment_detail[0]->pay_cc;

				$pay_his->pay_status = '2';

				$pay_his->pay_outgoing_Ref = '';

				$pay_his->pay_commision = '';

				$pay_his->pay_payout_amount = '';

				$pay_his->pay_update = date ('Y-m-d');				

				$pay_his->save();

				

				DB::table('transport_payment')->where('pay_orderid',$order_id)

								  ->update(['pay_status' =>'2' ]

								  );				

				//print_r(DB::getQueryLog());					  

		}	

		

			

		if($new_order_status == '5')

		{

				

			$payment_detail  =  DB::table('transport_payment')		

							->where('pay_orderid', '=', $order_id)

							->select('*')

							->get();

							

							

			

			$transport_detail  =  DB::table('transport')		

						->where('transport_id', '=', $order_detail[0]->trans_id)

						->select('*')

						->get();

						

				$commission = $transport_detail[0]->transport_commission;

				$commission_amt = (($payment_detail[0]->pay_amt*$commission)/100);

				$payout_amount = ($payment_detail[0]->pay_amt-$commission_amt);			

						

			

				

				DB::table('transport_payment')->where('pay_orderid',$order_id)

								  ->update(

								  	['pay_status' =>'3',

									 'pay_commision'=>$commission ,

									 'pay_commision_amt'=>$commission_amt ,

									 'pay_payout_amount'=>$payout_amount 

									 ]

								  );

								  

				$pay_his = new Transport_payment_history;

				

				

				$pay_his->pay_id = $payment_detail[0]->pay_id;

				$pay_his->pay_orderid = $payment_detail[0]->pay_orderid;

				$pay_his->pay_userid = $payment_detail[0]->pay_userid;

				$pay_his->pay_tx = $payment_detail[0]->pay_tx;

				$pay_his->pay_st = $payment_detail[0]->pay_st;

				$pay_his->pay_amt = $payment_detail[0]->pay_amt;

				$pay_his->pay_cc = $payment_detail[0]->pay_cc;

				$pay_his->pay_status = '3';

				$pay_his->pay_outgoing_Ref = '';

				$pay_his->pay_commision = $commission;

				$pay_his->pay_commision_amt=$commission_amt;

				$pay_his->pay_payout_amount = $payout_amount;

				$pay_his->pay_update = date ('Y-m-d');				

				$pay_his->save();				  

								  				

			//	print_r(DB::getQueryLog());						  

		}					  

		

		/*                            END                              */

				

				

		/* UPDATE STATUS IN TABLE START */

			DB::table('transport_order')->where('order_id',$order_id)

							  ->update(['order_status' =>$new_order_status,

							  			'order_cancel_reason' =>$order_cancel_reason ]

							  );				

			//	print_r(DB::getQueryLog());	

			

		/* SAVE HISTORY OF OLD ORDER */

				$ohis = new Transport_order_history;

				$ohis->order_id	= $order_detail[0]->order_id;

				$ohis->user_type	= $order_detail[0]->user_type;

				$ohis->user_id		= $order_detail[0]->user_id;

				$ohis->trans_id	= $order_detail[0]->trans_id;

				$ohis->vehicle_id	= $order_detail[0]->vehicle_id;

				$ohis->order_fname	= $order_detail[0]->order_fname;

				$ohis->order_lname	= $order_detail[0]->order_lname;

				$ohis->order_email	= $order_detail[0]->order_email;

				$ohis->order_tel	= $order_detail[0]->order_tel;

				$ohis->order_address	= $order_detail[0]->order_address;

				$ohis->order_city	= $order_detail[0]->order_city;

				$ohis->order_pcode	= $order_detail[0]->order_pcode;

				$ohis->order_from_location	= $order_detail[0]->order_from_location;

				$ohis->order_to_location	= $order_detail[0]->order_to_location;

				$ohis->order_ondate	= $order_detail[0]->order_ondate;

				$ohis->order_ontime	= $order_detail[0]->order_ontime;

				$ohis->order_returndate	= $order_detail[0]->order_returndate;

				$ohis->order_returntime	= $order_detail[0]->order_returntime;

				$ohis->total_days	= $order_detail[0]->total_days;

				$ohis->total_night	= $order_detail[0]->total_night;

				$ohis->each_day_allownce	= $order_detail[0]->each_day_allownce;

				$ohis->total_allownce	= $order_detail[0]->total_allownce;

				$ohis->order_pickarea	= $order_detail[0]->order_pickarea;

				$ohis->order_instruction	= $order_detail[0]->order_instruction;

				$ohis->order_cancel_reason	= $order_detail[0]->order_cancel_reason;

				$ohis->order_create	= $order_detail[0]->order_create;

				$ohis->order_status	= $new_order_status;

				$ohis->order_deliveryfee	= $order_detail[0]->order_deliveryfee;

				

				$ohis->order_deliveryadd1	= $order_detail[0]->order_deliveryadd1;

				$ohis->order_deliveryadd2	= $order_detail[0]->order_deliveryadd2;

				$ohis->order_deliverysurbur	= $order_detail[0]->order_deliverysurbur;

				$ohis->order_deliverypcode	= $order_detail[0]->order_deliverypcode;

				$ohis->order_deliverynumber	= $order_detail[0]->order_deliverynumber;

				$ohis->order_min_delivery	= $order_detail[0]->order_min_delivery;

				$ohis->order_remaning_delivery	= $order_detail[0]->order_remaning_delivery;

				$ohis->order_subtotal	= $order_detail[0]->order_subtotal;

				$ohis->order_total_amt	= $order_detail[0]->order_total_amt;

				$ohis->order_carditem	= $order_detail[0]->order_carditem;

				$ohis->order_promo_mode	= $order_detail[0]->order_promo_mode;

				$ohis->order_promo_val	= $order_detail[0]->order_promo_val;

				$ohis->order_promo_cal = $order_detail[0]->order_promo_cal;

				$ohis->save();			

			echo 'Status Updted!';

							

		

	}

	

	public function ajax_search_list()

	{

		//echo '<pre>';

		//print_r($_POST);

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		

		

		

		$order_detail  =  DB::table('transport_order');		

		$order_detail  = $order_detail->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id');

		$order_detail  = $order_detail->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id');

		$order_detail  = $order_detail->leftJoin('users', 'transport_order.user_id', '=', 'users.id');

		

		/*USER BASE SEARC FORM USER PAGE  */

		

		if($user_id>0)

		{

			$order_detail  =  $order_detail->where('transport_order.user_id', '=', $user_id);

		}



		

		

		/* Status Search */

		

		if(!empty($status) && ($status>0))

		{

		  $order_detail = $order_detail->where('transport_order.order_status', '=', $status);

		}	

			

		/* Get Data between Date */

		

		

		

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

		}	

		

		

		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		

		/* End */

		

				

						

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('transport_order.order_create', 'desc');

		$order_detail = $order_detail->get();	

			

		



		

  		$data_onview = array('order_detail'=>$order_detail); 

		

		return View('admin.transport.order_list')->with($data_onview);

		

		//print_r($data_onview);

		

	}

	

	public function show_orderreport()

	{

		//echo '<pre>';

		//print_r($_POST);

		

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		

		

		

		$order_detail  =  DB::table('transport_order');	

		$order_detail  = $order_detail->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id');

		$order_detail  = $order_detail->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id');

		$order_detail  = $order_detail->leftJoin('users', 'transport_order.user_id', '=', 'users.id');

		

			

//		  $order_detail = $order_detail->where('transport_order.order_status', '!=','8');

		

		/*USER BASE SEARC FORM USER PAGE  */

		

		if($user_id>0)

		{

			$order_detail  =  $order_detail->where('transport_order.user_id', '=', $user_id);

		}



		

		

		/* Status Search */

		

		if(!empty($status) && ($status>0))

		{

		  $order_detail = $order_detail->where('transport_order.order_status', '=', $status);

		}	

			

		/* Get Data between Date */

		

		

		

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

		}	

		

		

		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		

		/* End */

		

				

						

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('transport_order.order_create', 'desc');

		$order_detail = $order_detail->get();	

			

		



		

  		//$data_onview = array('order_detail'=>$order_detail); 

		

		

		$title = 'Transport Order Report';

		

		

		

		$all_array = array();

		if(!empty($order_detail))

		{

		

		

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)

			{	$sr++;

				 

				 $status = '';

				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				 if($data_list->order_status=='2'){ $status = 'Cancelled'; }

				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; }

				 if($data_list->order_status=='6'){ $status = 'Reject'; }

				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }

				 

				 

				  $cart_price = 0;

				  $sumary = '';

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

		{

			$order_carditem = json_decode($data_list->order_carditem, true);	

						

				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";

				

				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

				 {

					foreach($item['options']['addon_data'] as $addon)

					{

						$cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 } 

					

			}

		}

									 

				 

				$all_array[] = array('0'=>$sr,

									'1'=>$data_list->order_id,

									'2'=>$data_list->order_create,

									'3'=>$data_list->transport_name,

									'4'=>$data_list->vehicle_rego,

									'5'=>$status,

									'6'=>$data_list->order_fname.' '.$data_list->order_lname,

									'7'=>$data_list->order_tel,

									'8'=>$sumary,

									'9'=>'$'.$cart_price,

									);

				

			

			}

			$data = $all_array ;

		}

		else

		{

			$data = array(

		   			array('No REcod Found')

			);

		}

		

	

	

	

		$filename = "Transportorder_report.csv";

		$fp = fopen('php://output', 'w');

		//$header[] ='';

	



		$header =array('',	$title, '', '');

			

		header('Content-type: application/csv');

		header('Content-Disposition: attachment; filename='.$filename);

		fputcsv($fp, $header);

		

		$header2 =array('SNo.', 'OrderNo', 'OrderReq_date', 'Transport', 'Vehicle','Status','UserName','Contact No', 'Order Sumary','TotalAmount');

		fputcsv($fp, $header2);

	

		

		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }

		

		

	}

	

	public function show_paymentlist()

	{

	

	

	

		$payment_detail = '';

	

		$payment_detail  =  DB::table('transport_payment')		

						->leftJoin('transport_order', 'transport_payment.pay_orderid', '=', 'transport_order.order_id')

						->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id')

						->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id')

						->leftJoin('users', 'transport_order.user_id', '=', 'users.id')

						->where('transport_payment.pay_status', '>', 0)

						->select('*')

						->orderBy('transport_payment.pay_id', 'desc')

						->get();

						

				//echo '<pre>'		;

	

	//print_r($payment_detail );

	//exit;

		

  		$data_onview = array('payment_detail'=>$payment_detail); 

		

		return View('admin.transport_payment_list')->with($data_onview);

	}

	

	public function show_paymentdetail()

	{

	

		

		$pay_id = $_REQUEST['order'];

	    $payment_detail = '';

	

		$payment_detail  =  DB::table('transport_payment')		

						->leftJoin('transport_order', 'transport_payment.pay_orderid', '=', 'transport_order.order_id')

						->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id')

						->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id')

						->leftJoin('users', 'transport_order.user_id', '=', 'users.id')

						->where('transport_payment.pay_status', '>', 0)

						->where('transport_payment.pay_id', '=', $pay_id)

						->select('*','transport_order.created_at as order_submit_on')

						->orderBy('transport_payment.pay_id', 'desc')

						->get();

						

				//echo '<pre>'		;

	

	//print_r($payment_detail );

	//exit;

		if(count($payment_detail>0)){

		

		

		

	$order_history_submit =  DB::table('transport_order_history')	

							->where('transport_order_history.order_id', '=', $payment_detail[0]->order_id)	

							->where('transport_order_history.order_status', '=', '1')	

							->select('*')

							->get();	

							

		if(!empty($order_history_submit))

		{

			$order_create_date	= $order_history_submit[0]->created_at;						

		}

		

		

		/* ORDER COMPLETED DATE */

		

		

		

		$order_can_comp_reg = $payment_detail[0]->order_submit_on;

		

		

			$order_history_complete =  DB::table('transport_order_history')	

							->where('transport_order_history.order_id', '=', $payment_detail[0]->order_id)	

							->where('transport_order_history.order_status', '=', '5')	

							->select('*')

							->get();	

			if(!empty($order_history_complete))

			{

				$order_can_comp_reg	= $order_history_complete[0]->created_at;						

			}			

		

							/* END */

		

		

	

		$order_history_detail  =  DB::table('transport_order_history')		

						->where('transport_order_history.order_id', '=', $payment_detail[0]->order_id)

						->where('transport_order_history.order_status', '=', '5')

						->select('*')

						->orderBy('transport_order_history.order_history_id', 'desc')

						->get();

						

  		$data_onview = array('order_detail'=>$payment_detail,'order_history_detail'=>$order_history_detail,'order_create_date'=>$order_create_date,'order_can_comp_reg'=>$order_can_comp_reg); 

						

						

		//return View('admin.payment_list')->with($data_onview);

		return View('admin.transport_payment_view')->with($data_onview);

		}

		else

		{

			

		}

	}







	public function ajax_show_paymentlist()

	{

		//echo '<pre>';

		//print_r($_POST);

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		

		

		

		

						

		$order_detail  =  DB::table('transport_payment')	;	

		$order_detail = $order_detail->leftJoin('transport_order', 'transport_payment.pay_orderid', '=', 'transport_order.order_id')	;

		$order_detail = $order_detail->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id');

			$order_detail = $order_detail->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id');

		$order_detail = $order_detail->leftJoin('users', 'transport_order.user_id', '=', 'users.id');

		$order_detail = $order_detail->where('transport_payment.pay_status', '>', 0);

		

		

		

		

		/* Status Search */

		

		if(!empty($status) && ($status>0))

		{

			

			if($status==7)

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', '2');

			}

			elseif($status==6)

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', '4');

		  		$order_detail = $order_detail->orwhere('transport_payment.pay_status', '=', '5');

			}

			else

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', $status);

			}

		}	

			

		/* Get Data between Date */

		

		

		

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

		}	

		

		

		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		

		/* End */

		

				

						

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('transport_payment.pay_id', 'desc');

		$order_detail = $order_detail->get();	

			

		//print_r($order_detail);



		

  		$data_onview = array('payment_detail'=>$order_detail); 

		

		return View('admin.transport.payment_list')->with($data_onview);

		

		//print_r($data_onview);

		

	}

	

	public function show_paytmentreport()

	{

		

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');

		$status = Input::get('order_status');

		$user_id = Input::get('user_id');

		

		

		$order_detail  =  DB::table('transport_payment')	;	

		$order_detail = $order_detail->leftJoin('transport_order', 'transport_payment.pay_orderid', '=', 'transport_order.order_id')	;

		$order_detail = $order_detail->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id');

			$order_detail = $order_detail->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id');

		$order_detail = $order_detail->leftJoin('users', 'transport_order.user_id', '=', 'users.id');

				

		$order_detail = $order_detail->where('transport_payment.pay_status', '>', 0);

		

		

		/* Status Search */

		

		if(!empty($status) && ($status>0))

		{

			

			if($status==4)

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', '4');

		  		$order_detail = $order_detail->orwhere('transport_payment.pay_status', '=', '3');

			}

			elseif($status==5)

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', '2');

			}

			else

			{

		  		$order_detail = $order_detail->where('transport_payment.pay_status', '=', $status);

			}

		}	

			

		/* Get Data between Date */

		

		

		

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_payment.pay_update', '>=', date('Y-m-d',strtotime($from_date)));

		}	

		

		

		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $order_detail = $order_detail->where('transport_payment.pay_update', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		

		/* End */

		

				

						

		$order_detail = $order_detail->select('*');

		$order_detail = $order_detail->orderBy('transport_payment.pay_id', 'desc');

		$order_detail = $order_detail->get();	

			

		



		

  		//$data_onview = array('order_detail'=>$order_detail); 

		

		

		$title = 'Payment Report';

		

		

		

		$all_array = array();

		if(!empty($order_detail))

		{

		

		

			$sr=0;

			$total_amt = '';

			foreach($order_detail as $data_list)

			{	$sr++;

				 

				  $payment_status = '';

				  

		$order_can_comp_reg_date	= '';

		

				if($data_list->pay_status=='1') { $payment_status = 'Complete'; }

				if($data_list->pay_status=='2') { $payment_status = 'ToRefund'; } 

				if($data_list->pay_status=='3') { $payment_status = 'ToPayout'; }

				if($data_list->pay_status=='4') { $payment_status = 'Completed Refund'; }

				if($data_list->pay_status=='5') { $payment_status = 'Completed Payout'; }

				 

				 

				 $status = '';

				 if($data_list->order_status=='1'){ $status = 'Submit From Website'; }

				 if($data_list->order_status=='2'){ $status = 'Cancelled'; $order_can_comp_reg_date	= $data_list->created_at; }

				 if($data_list->order_status=='3'){ $status = 'Sent to Partner'; }

				 if($data_list->order_status=='4'){ $status = 'Partner Confirmed'; }

				 if($data_list->order_status=='5'){ $status = 'Partner Completed'; $order_can_comp_reg_date	= $data_list->created_at;}

				 if($data_list->order_status=='6'){ $status = 'Reject'; $order_can_comp_reg_date	= $data_list->created_at; }

				 if($data_list->order_status=='7'){ $status = 'Review Completed'; }

				 

				 

				  $cart_price = 0;

				  $sumary = '';

				  

				  

				  /* ORDER SUMMARY AND CALCULATE TOTAL AMOUNT WITH COMMISSION AMOUNT */

		if(isset($data_list->order_carditem) && (count($data_list->order_carditem)))

		{

			$order_carditem = json_decode($data_list->order_carditem, true);	

						

				foreach($order_carditem as $item){

				  $cart_price = $cart_price+$item['price'];

					$sumary .=$item['qty'].'  x '.$item['name'].' =  $'.$item['price'].' '."\n";

				

				 if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

				 {

					foreach($item['options']['addon_data'] as $addon)

					{

						$cart_price = $cart_price+$addon['price'];

					 $sumary .=$addon['name'].' ';

					 if($addon['price']>0){$sumary .=' = $'.$addon['price'];}

					 $sumary .="\n";

					 }

				 } 

					

			}

		}

		

		

		

		

		$sumary .="\n  Sub Total : $".number_format($cart_price,2);

		$sumary .="\n";

		$sumary .="\n  Delivery Fee : $".number_format($data_list->order_deliveryfee,2);

											

		if($data_list->total_night>0)

		{

			$sumary .="\n";

			$sumary .=" Driver overnight allownce :$".number_format($data_list->total_allownce,2).'';

			$sumary .="\n";

		}

		if($order_detail[0]->order_promo_cal>0)

		{

			$sumary .=" Dels : -$".number_format($data_list->order_promo_cal,2).'';

		}							 

				

				

						

					

		

		

		$total_amt= $order_detail[0]->pay_amt;

		

		

			

		$Commission_amt = '0.00';

		if($data_list->pay_status=='3' || $data_list->pay_status=='5' )	

		{

			$Commission_amt =	number_format((($total_amt*$data_list->transport_commission)/100),2);		

		}

		

					/*  END   */

					

					

	    /*  ORDER PICKUP DATE AND TIME */				

		

		$order_picup_date = '';

		$order_picup_date	= $data_list->order_ondate.' '.$data_list->order_ontime	;			

			

		

		

		/*    END   */

		

		

		

		/* ORDER COMPLETE /REJECT /CANCEL DATE AND TIME */

		$order_history_comp =  DB::table('transport_order_history')	

								->where('transport_order_history.order_id', '=', $data_list->order_id)	

								->where('transport_order_history.order_status', '=', '5')	

								->select('*')

								->get();	

										

			if(!empty($order_history_comp))

			{

				$order_can_comp_reg_date	= $order_history_comp[0]->created_at;						

			}

			

		

		

		/*        END        */

		

				$all_array[] = array('0'=>$sr,

									'1'=>$data_list->order_id,

									'2'=>$data_list->order_create,

									'3'=>$data_list->transport_name,

									'4'=>$data_list->vehicle_rego,

									'5'=>$status,

									'6'=>$data_list->order_fname.' '.$data_list->order_lname,

									'7'=>$data_list->order_tel,

									'8'=>$sumary,

									'9'=>'$'.$total_amt,

									'10'=>$payment_status,

									'11'=>$order_picup_date,

									'12'=>$order_can_comp_reg_date,

									'13'=>'$'.$total_amt,

									'14'=>$data_list->transport_commission.'%',

									'15'=>$Commission_amt,

									'16'=>$data_list->pay_tx

									);

				

			

			}

			$data = $all_array ;

		}

		else

		{

			$data = array(

		   			array('No REcod Found')

			);

		}

		

	

	

	

		$filename = "transport_payment_report.csv";

		$fp = fopen('php://output', 'w');

		//$header[] ='';

	



		$header =array('',	$title, '', '');

			

		header('Content-type: application/csv');

		header('Content-Disposition: attachment; filename='.$filename);

		fputcsv($fp, $header);

		

		$header2 =array('SNo.', 

		'OrderNo', 

		'OrderReq_date', 

		'Transport',

		'Vehicle',

		'Status',

		'UserName',

		'Contact No', 

		'Order Sumary',

		'TotalAmount',

		'Payment Status',

		'Order DineIn/Pickup/Delivery Date',

		'OrderComplete /Cancle /Rejcte Date',		

		'Order Payout/Refund Amount',

		'Commission',

		'Commission Amount',

		'Paypal Ref'

		

		

		);

		fputcsv($fp, $header2);

	

		

		    foreach ($data as $row) {

				fputcsv($fp, $row);

		 }

		

	}

	

	

	public function update_payment_action()

	{

		//print_r($_POST);

		

	



		

		//pay_outgoing_Ref

		

		

		$pay_id = Input::get('pay_id');

		$order_id = Input::get('order_id');

		$old_status = Input::get('old_status');

		$new_pay_status = Input::get('pay_status');

		$outgoing_reference = Input::get('outgoing_transaction_reference');

		

		

		//DB::enableQueryLog(); 

		

		

		$payment_detail  =  DB::table('transport_payment')		

							->where('pay_orderid', '=', $order_id)

							->where('pay_id', '=', $pay_id)

							->select('*')

							->get();

			

		/*$pay_his = new Order_pay_history;

		$pay_his->pay_id = $payment_detail[0]->pay_id;

		$pay_his->pay_order_id = $payment_detail[0]->pay_orderid;

		$pay_his->pay_user_id = $payment_detail[0]->pay_userid;

		$pay_his->pay_old_status = $payment_detail[0]->pay_status;

		$pay_his->old_status_date = $payment_detail[0]->created_at;

		$pay_his->order_history_created	= date('Y-m-d');

		$pay_his->save();*/

		

		

		

				$pay_his = new Transport_payment_history;

				

				

				$pay_his->pay_id = $payment_detail[0]->pay_id;

				$pay_his->pay_orderid = $payment_detail[0]->pay_orderid;

				$pay_his->pay_userid = $payment_detail[0]->pay_userid;

				$pay_his->pay_tx = $payment_detail[0]->pay_tx;

				$pay_his->pay_st = $payment_detail[0]->pay_st;

				$pay_his->pay_amt = $payment_detail[0]->pay_amt;

				$pay_his->pay_cc = $payment_detail[0]->pay_cc;

				$pay_his->pay_status = $new_pay_status;

				$pay_his->pay_outgoing_Ref = '';

				$pay_his->pay_commision = '';

				$pay_his->pay_payout_amount = '';

				$pay_his->pay_update = date ('Y-m-d');				

				$pay_his->save();



				

				

		/* UPDATE STATUS IN TABLE START */

			DB::table('transport_payment')->where('pay_id',$pay_id)

							  ->update(['pay_status' =>$new_pay_status,

							  			'pay_outgoing_Ref' =>$outgoing_reference ]

							  );				

			//	print_r(DB::getQueryLog());		

			echo 'Status Updted!';

							

							

	}

	

	

	

	

	

	/* REVIEW FUNCTIONS START   */

	

	

	

	

	public function show_reviewlist()

	{

		//echo '<pre>';

		$review_detail  =  DB::table('transport_review')						

						->leftJoin('transport', 'transport_review.re_transid', '=', 'transport.transport_id')

						->leftJoin('vehicle', 'transport_review.re_vehicleid', '=', 'vehicle.vehicle_id')

						->leftJoin('users', 'transport_review.re_userid', '=', 'users.id')

						->leftJoin('transport_order', 'transport_review.re_orderid', '=', 'transport_order.order_id')

						->where('transport_review.re_status', '=' ,'SUBMIT')

						->select('*')

						->orderBy('transport_review.re_id', 'asc')

						->get();	

			

		//print_r($order_detail);	

		

		$data_onview = array('review_detail'=>$review_detail); 

		return View('admin.transport_review_list')->with($data_onview);	

	}

	public function show_review_action()

	{

		//echo '<pre>';

		//print_r($_POST);

		$reason = '';

		if(Input::get('reason') && (!empty(Input::get('reason'))) && (Input::get('revew_status')=='REJECT'))

		{

			$reason =Input::get('reason');

		}

		DB::table('transport_review')

            ->where('re_id', Input::get('re_id'))

            ->update(['re_status' => Input::get('revew_status'),

					  're_rejectreson'=>  $reason,

					  're_rejectdate'=>  date('Y-m-d')

					 ]);

					 

		 Session::flash('message', 'Review Updated Successfully!');

		 return redirect()->to('/admin/transport_review_list');

		 

	}

	

	

	

	public function ajax_review_search_list()

	{

		//echo '<pre>';

		//print_r($_POST);

		$from_date=Input::get('fromdate');;

		$to_date=Input::get('todate');;

		$status = trim(Input::get('order_status'));

		$user_id = Input::get('user_id');

		

			

						

						

		$review_detail  = DB::table('transport_review');		

		

		$review_detail  = $review_detail->leftJoin('transport', 'transport_review.re_transid', '=', 'transport.transport_id');

		$review_detail  = $review_detail->leftJoin('vehicle', 'transport_review.re_vehicleid', '=', 'vehicle.vehicle_id');

		$review_detail  = $review_detail->leftJoin('users', 'transport_review.re_userid', '=', 'users.id');

		$review_detail  = $review_detail->leftJoin('transport_order', 'transport_review.re_orderid', '=', 'transport_order.order_id');

		

		

		

		if(!empty($status) && ($status!='All'))

		{

		  $review_detail = $review_detail->where('transport_review.re_status', '=', $status);

		}	

			

		/* Get Data between Date */

		

		

		

		if(!empty($from_date) && ($from_date!='0000-00-00'))

		{

		  $review_detail = $review_detail->where('transport_review.created_at', '>=', date('Y-m-d',strtotime($from_date)));

		}	

		

		

		if(!empty($to_date) && ($to_date!='0000-00-00'))

		{

		  $review_detail = $review_detail->where('transport_review.created_at', '<=',date('Y-m-d',strtotime($to_date)));

		}		

		

		/* End */

		

				

						

		$review_detail = $review_detail->select('*');

		$review_detail = $review_detail->orderBy('transport_review.re_id', 'asc');

		$review_detail = $review_detail->get();	

			

		



		

		$data_onview = array('review_detail'=>$review_detail); 				

		

		return View('admin.transport.review_list')->with($data_onview);

		

		//print_r($data_onview);

		

	}

	/* END                     */





}

