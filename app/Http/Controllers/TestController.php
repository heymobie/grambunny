<?php

namespace App\Http\Controllers;

use App\Coupon_code;

use App\Coupon_code_apply;

use App\Delivery_address;

use App\Device_token;

use App\Favourite_food;

use App\Favourite_restaurant;

use App\Http\Controllers\Controller;

use App\Http\Requests;

use App\Notification_list;

use App\Order;

use App\Order_payment;

use App\Productservice;

use App\Cart;

use App\Proservicescate; 

use App\Proservices_subcate; 

use App\Restaurant;

use App\Review;

use App\Temp_cart;

use App\User;

use App\User_otpdetail;

use App\Vendor;

use Auth;

use Braintree_Transaction;

use Carbon\Carbon;

//use Intervention\Image\Facades\Image as Image; 

use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Auth\Authenticatable;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Hash;

//use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Input;

use PHPMailer\PHPMailer;

use Session;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Arr;

use Illuminate\Support\Str;
use Mail;



class TestController extends Controller
{

private $param;

public function __construct()

{

//blockio init


if ($_POST)

{

$this->param = $_POST;

}else{

$this->param = json_decode(file_get_contents("php://input"),true);

}

//$this->param = (array)json_decode(file_get_contents('php://input'), true);

}

public function index()

{

echo "Welcome Webservices" ;

}


public function product_service_list(Request $request){


        $response= array();


		$vendor_id = Input::get('merchant_id');

		//$ps_type = "product";


	    $ps_list = Productservice::where('vendor_id', '=' ,$vendor_id)

					//->where('type', '=' ,$ps_type)

					->orderBy('id', 'desc')

					->get();


	    if($ps_list->count() > 0)	{					


		$response['message'] = "Product service list";

		$response['status']= 1;

		$response['data'] = array('ps_lists'=>$ps_list);


	    }else{


        $response['message'] = "Product service list not found";

		$response['status']= 0;

		$response['data'] = array('ps_lists'=>'');


	}


        return $response;


}


public function product_service_add(Request $request){


        $response= array();

		$vendor_id = Input::get('merchant_id');

		$date = date('Y-m-d H:i:s');

		$title = Input::get('ps_name');


		//print_r($request->product_images);


		$category_id  = DB::table('vendor')

						->where('vendor_id', '=' ,$vendor_id)

						->value('category_id');	


        $ps_type = DB::table('vendor')

						->where('vendor_id', '=' ,$vendor_id)

						->value('type');	


		$product_code = Str::random(8);

		$slug = Str::slug($title.' '.$product_code, '-');


		    $validation=Validator::make($request->all(),[

		        	"ps_image" => "required|image",

		        ],[

		        	"ps_image.image" => "The file must be an image.",

		        ]);



        if ($validation->fails()) {    

        	if ($validation->messages()->has("ps_image")) {

        		return response()->json(["message"=>$validation->messages()->first("ps_image"),"ok" => 0]); 

        	}

		}

                if ($request->hasFile("ps_image")) {

                	$productImage=$request->ps_image;


                	$image = Image::make($productImage->getRealPath());    

					$image->resize(200,200)->save('public/uploads/product/'.$psimage=time().'.'.$productImage->getClientOriginalExtension());


					$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(16).'.'.$productImage->getClientOriginalExtension());


                }


				$product = new Productservice; 

				$product->vendor_id = $vendor_id;

				$product->type = $ps_type;	

				$product->category_id = $category_id;

				$product->sub_category_id = Input::get('sub_category_id');

				$product->name = Input::get('ps_name');

				$product->slug = $slug;

				$product->description =  Input::get('ps_desc');

				$product->price =  Input::get('ps_price');

				$product->unit =  Input::get('unit');

				$product->image =  $psimage;

				$product->product_code =  $product_code;

				$product->status =  Input::get('status');

				$product->stock =  Input::get('stock');

				$product->created_at = $date;

				$product->save();


				DB::table('ps_images')->insert([

				'name' => $imageTlarg,

				'ps_id' => $product->id,

				'thumb' => 1,

				'created_at' => $date,

				'updated_at' => $date	

				]); 


		if($request->hasFile("product_images")) {		

		$images = $request->file('product_images');

        foreach($images as $key => $file) {

					$name=str_random().$file->getClientOriginalName();

					$image=$file;

					$image->move('public/uploads/product/',$name); 

					DB::table('ps_images')->insert([

						'name' => $name,

						'ps_id' => $product->id,

						'thumb' => NULL,

						'created_at' => $date,

						'updated_at' => $date	

					]); 

				}

			}	


                if($ps_type=='0'){

				$response['message'] = "Product Added Sucessfully!";

				$response['status']= 1;



			   }else{



			   	$response['message'] = "Service Added Sucessfully!";

				$response['status']= 1;



			   }



               return $response;



}



public function product_service_details(Request $request){

        $response= array();

		$vendor_id = Input::get('merchant_id');

		$ps_id = Input::get('ps_id');

	    $ps_list = Productservice::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$ps_id)

					->get();

	    if($ps_list->count() > 0){					

		$response['message'] = "Product service details";

		$response['status']= 1;

		$response['data'] = array('ps_details'=>$ps_list);

	    }else{

        $response['message'] = "Product service detail not found";

		$response['status']= 0;

		$response['data'] = array('ps_lists'=>'');

	}

        return $response;
}



public function product_service_update(Request $request){



        $response= array();



		$vendor_id = Input::get('merchant_id');

		$ps_id = Input::get('ps_id');

		$date = date('Y-m-d H:i:s');

		$title = Input::get('name');

	

		$product_code = Str::random(8);

		$slug = Str::slug($title.' '.$product_code, '-');

		$imageTlarg = '';



	

               if ($request->hasFile("ps_image")) {



               $validation=Validator::make($request->all(),[

		        	"ps_image" => "required|image",

		        ],[

		        	"ps_image.image" => "The file must be an image.",

		        ]);



               if ($validation->fails()) {    

        	   if ($validation->messages()->has("ps_image")) {

        	   return response()->json(["message"=>$validation->messages()->first("ps_image"),"ok" => 0]); 

        	    }

		       }



                	$productImage=$request->ps_image;



                	$image = Image::make($productImage->getRealPath());    

					$image->resize(200,200)->save('public/uploads/product/'.$psimage=time().'.'.$productImage->getClientOriginalExtension());



					$productImage->move(public_path("uploads/product/"),$imageTlarg=str_random(16).'.'.$productImage->getClientOriginalExtension());



                }else{



            	  $psimage = Productservice::where('id', '=' ,$ps_id)->value('image');



                }



			$proserup = Productservice::where('id', $ps_id)

            ->update(['sub_category_id' =>Input::get('sub_category_id'), 

            	      'name' =>Input::get('ps_name'),

            	      'slug' =>$slug,

					  'description' =>Input::get('ps_desc'),

					  'price'=>Input::get('ps_price'),

					  'image'=>$psimage,

					  'unit'=>Input::get('unit'),

					  'status'=>Input::get('status'),

					  'stock'=>Input::get('stock'),

					  'updated_at'=>$date

				

					 ]);  



			if(!empty($imageTlarg)){



				DB::table('ps_images')

				->where('ps_id', $ps_id)

				->where('thumb', 1)

				->update([

				'name' => $imageTlarg,

				]);



			   }

			   

			if($request->hasFile("product_images")) {



			$gldelete = DB::table('ps_images')

			->where('ps_id', '=', $ps_id)

			->delete();		

			   

			$images = $request->file('product_images');



            foreach($images as $key => $file) {



					$name=str_random().$file->getClientOriginalName();

					$image=$file;

					$image->move('public/uploads/product/',$name); 



					DB::table('ps_images')->insert([

						'name' => $name,

						'ps_id' => $ps_id,

						'thumb' => NULL,

						'created_at' => $date,

						'updated_at' => $date	

					]); 

				}



			 }	    

	 			

               if($proserup){



                if(Input::get('ps_type')=='0'){



				$response['message'] = "Product Update Sucessfully!";

				$response['status']= 1;



			   }else{



			   	$response['message'] = "Service Update Sucessfully!";

				$response['status']= 1;



			   }

			}else{



                $response['message'] = "Error!";

				$response['status']= 0;



			}



               return $response;



}



public function product_service_delete(Request $request){



        $response= array();



		$vendor_id = Input::get('merchant_id');

		$ps_id = Input::get('ps_id');



		$psdelete = Productservice::where('id', '=', $ps_id)

		->where('vendor_id', '=', $vendor_id)

		->delete();			



	    if($psdelete)	{					



		$response['message'] = "Data Delete Sucessfully!";

		$response['status']= 1;



	    }else{



        $response['message'] = "Error!";

		$response['status']= 0;



	}



        return $response;



}



public function product_service_cat_list(Request $request){



        $response= array();



		//$vendor_id = Input::get('merchant_id');

		

		$ps_type = Input::get('ps_type');



	    $ps_cat = Proservicescate::where('type', '=' ,$ps_type)

					->where('status', '=' ,1)

					//->orderBy('id', 'desc')

					->get();



	    if($ps_cat->count() > 0)	{					



		$response['message'] = "Product service category list";

		$response['status']= 1;

		$response['data'] = array('ps_cat_lists'=>$ps_cat);



	    }else{



        $response['message'] = "Product service category list not found";

		$response['status']= 0;

		$response['data'] = array('ps_cat_lists'=>'');



	}



        return $response;



}


	public function cat_products_list(Request $request){

		$response= array();

		$category_id = $request->category_id;

		$response['message'] = "Product category list";

		return $response;

	}


public function product_service_sub_cat_list(Request $request){

        $response= array();

        $category_id  = DB::table('vendor')

						->where('vendor_id', '=' ,$request->merchant_id)

						->value('category_id');	

		$pssub_cat  = DB::table('vendor')

						->where('vendor_id', '=' ,$request->merchant_id)

						->value('sub_category_id');

		$sub_cat = json_decode($pssub_cat);																

		$ps_subcat = DB::table('product_service_sub_category')

							->whereIn('id',$sub_cat)

							->orderBy('id', 'asc')

							->get();					

	    if($ps_subcat->count() > 0)	{					

		$response['message'] = "Product service sub category list";

		$response['status']= 1;

		$response['data'] = array('ps_sub_cat_lists'=>$ps_subcat);

	    }else{

        $response['message'] = "Product service sub category list not found";

		$response['status']= 0;

		$response['data'] = array('ps_sub_cat_lists'=>'');

	}

        return $response;
}



public function product_service_subcategory(Request $request){



        $response= array();															



		$ps_subcat = DB::table('product_service_sub_category')

							->where('category_id',$request->category_id)

							->orderBy('id', 'asc')

							->get();					



	    if($ps_subcat->count() > 0)	{					



		$response['message'] = "Sub category list of category";

		$response['status']= 1;

		$response['data'] = array('sub_category'=>$ps_subcat);



	    }else{



        $response['message'] = "Sub category list of category not found";

		$response['status']= 0;

		$response['data'] = array('sub_category'=>'');



	}



        return $response;



}



public function get_unit(Request $request){



        $response= array();



        $product_unit  = DB::table('product_unit')

						->where('status', '=' ,1)

						->get();



	    if($product_unit->count() > 0)	{					



		$response['message'] = "Product service unit list";

		$response['status']= 1;

		$response['data'] = array('ps_unit_lists'=>$product_unit);



	    }else{



        $response['message'] = "Product service unit list not found";

		$response['status']= 0;

		$response['data'] = array('ps_unit_lists'=>'');



	}



        return $response;



}



public function order_list(Request $request){



        $response= array();



		$vendor_id = Input::get('merchant_id');

		//$ps_type = "product";



	    $order_list = Order::where('vendor_id', '=' ,$vendor_id)

	                ->whereIn('status',[0,1,3])

					->orderBy('id', 'desc')

					->get();



	    if($order_list->count() > 0){					



		$response['message'] = "Product service order list";

		$response['status']= 1;

		$response['data'] = array('order_list'=>$order_list);



	    }else{



        $response['message'] = "Product service order list not found";

		$response['status']= 0;

		$response['data'] = array('order_list'=>'');



	}



        return $response;



}


public function order_details(Request $request){

        $response= array();

		$vendor_id = Input::get('merchant_id');

		$order_id = Input::get('order_id');

	    $order_detail = Order::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$order_id)

					->get();

		$orderItems = Productservice::select('product_service.*', 'orders_ps.ps_qty')

			->join('orders_ps', 'orders_ps.ps_id', '=', 'product_service.id')->where('orders_ps.order_id', $order_id)->where('product_service.status', 1)->get();

	    if($order_detail->count() > 0){					

		$response['message'] = "Product service order detail";

		$response['status']= 1;

		$response['data'] = array('order_detail'=>$order_detail, 'order_items' => $orderItems);


	    }else{

        $response['message'] = "Product service order detail not found";

		$response['status']= 0;

		$response['data'] = array('order_detail'=>'');

	}

        return $response;

}



public function order_history_list(Request $request){



        $response= array();



		$vendor_id = Input::get('merchant_id');

		//$ps_type = "product";



	    $order_history_list = Order::where('vendor_id', '=' ,$vendor_id)

	                ->whereIn('status',[2,4])

					->orderBy('id', 'desc')

					->get();



	    if($order_history_list->count() > 0){					



		$response['message'] = "Product service order history list";

		$response['status']= 1;

		$response['data'] = array('order_history_list'=>$order_history_list);



	    }else{



        $response['message'] = "Product service order history list not found";

		$response['status']= 0;

		$response['data'] = array('order_history_list'=>'');



	}



        return $response;



}



public function order_history_details(Request $request){



        $response= array();



		$vendor_id = Input::get('merchant_id');

		$order_id = Input::get('order_id');



	    $order_history_detail = Order::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$order_id)

					->get();



	    if($order_history_detail->count() > 0){					



		$response['message'] = "Product service order history detail";

		$response['status']= 1;

		$response['data'] = array('order_history_detail'=>$order_history_detail);



	    }else{



        $response['message'] = "Product service order history detail not found";

		$response['status']= 0;

		$response['data'] = array('order_history_detail'=>'');



	}



        return $response;



}



public function merchant_to_user_location(Request $request){



        $response= array();



		$vendor_id = $request->merchant_id;

		$order_id = $request->order_id;



	    $mulocation = Order::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$order_id)

					->first();



		$userlat = $mulocation->latitude;

		$userlong = $mulocation->longitude;

		$ulocation = array('latitude' => $userlat,'longitude' => $userlong);	

        $mlocation = array('latitude' =>  $mulocation->vendor->lat,'longitude' => $mulocation->vendor->lng);		



	    if($mulocation->count() > 0){					



		$response['message'] = "Merchant to user location";

		$response['status']= 1;

		$response['data'] = array('merchant_location'=>$mlocation,'user_location'=>$ulocation);



	    }else{



        $response['message'] = "Merchant to user location";

		$response['status']= 0;

		$response['data'] = array('location_detail'=>'');



	}



        return $response;



}



public function update_merchant_location(Request $request){



        $response= array();



		$vendor_id = $request->merchant_id;

		$latitude = $request->latitude;

		$longitude = $request->longitude;

        $mlocation = '';



     if(!empty($latitude) && !empty($longitude)){



     $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=true&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';

     

     $json = @file_get_contents($url);

     $data=json_decode($json);

     if($data){ $status = $data->status; }else{ $status=''; }



     if($status=="OK")

     {

       $market_area = $data->results[2]->formatted_address;

     }else{ $market_area = Vendor::where('vendor_id', '=' ,$vendor_id)->value('market_area'); }	



	    $mlocation = Vendor::where('vendor_id', '=' ,$vendor_id)

            ->update(['lat' => $latitude,'lng' => $longitude,'market_area' => $market_area]);



         $mlocation = 1; 

             

        }    



	    if($mlocation){					



		$response['message'] = "Merchant location update successfully";

		$response['status']= 1;



	    }else{



        $response['message'] = "Error!";

		$response['status']= 0;



	}



        return $response;



}



public function code_verification(Request $request){



        $response= array();



		$vendor_id = $request->merchant_id;

		$order_id = $request->order_id;

		$code = $request->code;



	    $codev = Order::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$order_id)

					->value('verification_code');		



	    if($code==$codev){	



	    $status = Order::where('vendor_id', '=' ,$vendor_id)

	        ->where('id', '=' ,$order_id)

            ->update(['status' => 4]);				



		$response['message'] = "Order verification successfully";

		$response['status']= 1;



        $divice_token = DB::table('vendor_device_token')

		->where('userid', '=', $vendor_id)

		->where('notification_status', '=',1)

		->first();



        if($divice_token->devicetoken){



           	$title = "Order Delivered";

		    $body = "Your order have been successfully delivered"; 

        	$token = $divice_token->devicetoken; 



        	//$this->sendNotification($token,$title,$body);

        	//$this->testNotification();



	    $url = "https://fcm.googleapis.com/fcm/send";

		$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



		$headers = array();

		$headers[] = 'Content-Type: application/json';

		$headers[] = 'Authorization: key='. $serverKey;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));

		$result = curl_exec($ch);

		curl_close($ch); 



       }	



	   }else{



        $response['message'] = "Invalid Verification Code!";

		$response['status']= 0;



	}



        return $response;



}



public function order_status(Request $request){



        $response= array();



		$vendor_id = $request->merchant_id;

		$order_id = $request->order_id;

		$status = $request->status;

		$ustatus = '';

		$date = date('Y-m-d H:i:s');



        if(!empty($status)){

        	

		$ustatus = Order::where('vendor_id', '=' ,$vendor_id)

	        ->where('id', '=' ,$order_id)

            ->update(['status' => $status,'updated_at' => $date]);



        }  		



	    if($ustatus){				



		$response['message'] = "Order status change successfully";

		$response['status']= 1;



	    }else{



        $response['message'] = "Error!";

		$response['status']= 0;



	}



        return $response;



}



public function vendor_update_password(Request $request)

	{

		$validation=Validator::make($request->all(),[

			"vendor_id"	=> "required",

            'old_password' => "required",

            'password' => 'required|confirmed|min:8',

            'password_confirmation' => "required"

        ]);



        if ($validation->fails()) {    

        	if ($validation->messages()->has("password")) {

        		return response()->json(["message"=>$validation->messages()->first("password"),"ok" => 0]); 

        	}

		}



		$vendor=Vendor::find($request->vendor_id);

        if (!Hash::check($request->old_password, $vendor->password)) {

        	return response()->json(["message" => "Old Password is incorrect","ok" => 0]);

        }



        $vendor->password=Hash::make($request->password);

        $vendor->update();

        return response()->json(["message"=>"Password updated successfully!","ok" => 1]);

	}





// 29-8-2019 start

function generateRandomString($length = 6) {

$characters = '0123456789';

$charactersLength = strlen($characters);

$randomString = '';

for ($i = 0; $i < $length; $i++) {

$randomString .= $characters[rand(0, $charactersLength - 1)];

}

return $randomString;

}

function order_uniqueid($length = 6) {

$characters = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';

$charactersLength = strlen($characters);

$randomString = '';

for ($i = 0; $i < $length; $i++) {

$randomString .= $characters[rand(0, $charactersLength - 1)];

}

return $randomString;

}



/* Start vendor api */



public function vendor_register(Request $request) {



$data['firstname'] = '';

$data['email'] = '';

$data['password'] = '';

$data['phone'] = '';





if(isset($this->param["fname"]))

{

$data['firstname'] = $this->param["fname"];

}



if(isset($this->param["lname"]))

{

$data['lastname'] = $this->param["lname"];

}

if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

if(isset($this->param["password"]))

{

$data['password'] = $password  = $this->param["password"];

}



if(isset($this->param["mobileno"]))

{

$data['phone'] = $this->param["mobileno"];

}





$response = array();



if(empty($data['phone'])){

$response['errorcode'] = "10002";

$response['status']= 0;

$response['message']="Moblie No:Required parameter missing";

}else if(empty($data['password'])){

$response['errorcode'] = "10003";

$response['status']= 0;

$response['message']="password:Required parameter missing";

}elseif (!empty($data)) {



$user_detail  = DB::table('vendor')->where('mob_no', '=' ,$this->param['mobileno'])->get();

$user_detail_email  = DB::table('vendor')->where('email', '=' ,$this->param['email'])->get();

$response='';



if(!empty($user_detail)){

$response['errorcode'] = "10005";

$response['status']= 0;

$response['message']="Mobile No already exists";

$response['data'] = array('user_detail'=>$user_detail);

}elseif(($user_detail_email) && (!empty($this->param['email']))){

$response['errorcode'] = "10006";

$response['status']= 0;

$response['message']="Email already exists";

$response['data'] = array('user_detail'=>$user_detail_email);

}else{



$vendor = new Vendor;

$vendor->name = trim($this->param['fname']);;

$vendor->last_name = trim($this->param['lname']);

$vendor->email = trim($this->param['email']);;

$vendor->mob_no = trim($this->param['mobileno']);

$vendor->vendor_status =  '0';

$vendor->vendor_type = trim($this->param['vendor_type']);

$vendor->deviceid = trim($this->param['deviceid']);

$vendor->devicetype = trim($this->param['devicetype']);

$vendor->password =  Hash::make(trim($this->param['password']));

$vendor->save();



$vendor_id = $vendor->vendor_id;



if($vendor){



$user_id = $vendor_id;



$user_data = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();



$response['message'] = "You have registered successfully. we'll be in touch with you soon.";

$response['status']= 1;

$response['data'] = array('vendor_detail'=>$user_data);

}else{

$response['message']="vendor not created.";

$response['status']= 0;

$response['errorcode'] = "10007";

}

}

}

header('Content-type: application/json');

echo json_encode($response);

}



public function vendor_login(Request $request){

$response= array();

$data_post['username'] = '';

$data_post['password'] = '';

if(isset($this->param['mobileno'])){

$data_post['username'] = $this->param['mobileno'];

}

if(isset($this->param['password'])){

$data_post['password'] = $this->param['password'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/

if(empty($data_post['username'])){

$response['errorcode'] = "40001";

$response['status']= 0;

$response['message']="Mobileno:Required parameter missing";

}elseif(empty($data_post['password'])){

$response['errorcode'] = "40002";

$response['status']= 0;

$response['message'] = "Password: Required parameter missing";

}

elseif(!empty($data_post)){

$FILD_NAME = '';

if(is_numeric ($this->param['mobileno']))

{

$FILD_NAME = 'mob_no';

}

else

{

$FILD_NAME = 'email';

}



$user_data  =Vendor::where($FILD_NAME, '=' ,$this->param['mobileno'])->get();



if(count($user_data)>0)
{


if (Auth::guard('vendor')->attempt([$FILD_NAME => $this->param['mobileno'],

'password' => $this->param['password'],'vendor_status'=>'1']))

{

$vendor_id = Auth::guard('vendor')->user()->vendor_id;

$deviceid = $this->param['deviceid'];

$devicetype = $this->param['devicetype'];

$login_status = 1;//$this->param['login_status'];

DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['deviceid'=>$deviceid,'devicetype'=>$devicetype,'login_status'=>$login_status]);

DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

$vendor_detail  = Vendor::where('vendor_id', '=' ,$vendor_id)->get();

$response['message'] = "You have logged in successfully";

$response['status']= 1;

$response['data'] = array('user_detail'=>$vendor_detail);

}else{

if($user_data[0]->vendor_status!=1){

	$response['errorcode'] = "40004";

	$response['status']= 2;

	$response['data'] = array('user_detail'=>$user_data);

	$response['message'] ='Sorry.Your acoount is not active.';

}else{

	$response['errorcode'] = "40003";

	$response['status']= 0;

	$response['message'] = "Invalid Password!";
}

}

}

else

{

$response['errorcode'] = "40005";

$response['status']= 0;

$response['message'] = " Number/Email Does Not Exists in Our System, so please Registration then login!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}


public function vendorlogout(Request $request){

$response= array();

$vendor_id = $this->param['vendor_id']; 

$login_status = 0;//$this->param['login_status'];


DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

$vendor_detail  = Vendor::where('vendor_id', '=' ,$vendor_id)->get();


$response['message'] = "You have login status change successfully";

$response['status']= 1;

$response['data'] = array('merchant_detail'=>$vendor_detail);


header('Content-type: application/json');

echo json_encode($response);

}

public function vendor_logout(Request $request){

$response= array();

$vendor_id = $request->vendor_id; //$this->param['vendor_id']; 


$login_status = 0;//$this->param['login_status'];


DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

DB::table('product_service')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

$vendor_detail  = Vendor::where('vendor_id', '=' ,$vendor_id)->get();


$response['message'] = "You have login status change successfully";

$response['status']= 1;

//$response['data'] = array('merchant_detail'=>$vendor_detail);

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : FORGOT PASSWORD

//	FUNCTION USE :  app user FORGOT PASSWORD functionality

//	FUNCTION ERRORCODE : 50000

/****************************************************/

public function vendor_forget_pwd_xyz(Request $request)

{

$response= array();

$data_post['username'] = '';

if(isset($this->param['mobileno'])){

$data_post['username'] = $this->param['mobileno'];

}

if(empty($data_post['username'])){

$response['errorcode'] = "50001";

$response['status']= 0;

$response['message'] = "Mobile No: Required parameter missing";

}

elseif(!empty($data_post)){

$user_detail = DB::table('vendor')->where('mob_no', '=' ,$this->param['mobileno'])->get();

if(!empty($user_detail)){

$otp_code = trim($this->generateRandomString(6));

//$otp_code='123456';

$email = trim($user_detail[0]->email);

if($user_detail[0]->vendor_status==1){

$user_id =  $user_detail[0]->vendor_id;

$otp_data = new User_otpdetail;

$otp_data->vendor_id = trim($user_id);

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/



$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',2)->first();



$address = $email; //"deepaksahuphp@gmail.com";

$message = 'Hello '.trim($user_detail[0]->name).', '.$usr_fgtpss_email_cotnt->email_content.$otp_code;

$subject = $usr_fgtpss_email_cotnt->email_title;

$semail = $this->sendMail($address,$subject,$message);



/********** EMAIL FOR SEND OTP END  *************/

$user_data = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

	//$response['message'] = "Forget Pwd: Mobile Number Varify!";

$response['message'] = "OTP has been sent on your email. Please verify it.";

$response['status']= 1;

//$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

}

else

{

$response['errorcode'] = "50002";

$response['status']= 2;

$response['message'] ='Sorry.Your acoount is not active.';

}

}

else

{

$response['errorcode'] = "50003";

$response['status']= 0;

//$response['message'] ='Sorry.Your mobile is not registered ';

$response['message'] ='The mobile number is not registered with us. Please try again. ';

}

}

header('Content-type: application/json');

echo json_encode($response);

}



public function vendor_verify_OTP (Request $request){

$response= array();

$data_post['username'] = '';

$data_post['otp'] = '';

$data_post['from'] = '';


if(isset($this->param['mobileno'])){

$data_post['username'] = $this->param['mobileno'];

}

if(isset($this->param['otp'])){

$data_post['otp'] = $this->param['otp'];

}

if(isset($this->param['from'])){

$data_post['from'] = $this->param['from'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/



if(empty($data_post['otp'])){

$response['errorcode'] = "20001";

$response['status']= 0;

$response['message'] = "OTP: Required parameter missing";

}elseif(empty($data_post['username'])){

$response['errorcode'] = "20002";

$response['status']= 0;

$response['message']="mobileno:Required parameter missing";

}elseif(empty($data_post['from'])){

$response['errorcode'] = "20002";

$response['status']= 0;

$response['message']="From:Required parameter missing";

}

elseif(!empty($data_post)){



$otp_detail = DB::table('user_otpdetail')

->where('user_mob', '=' ,trim($this->param['mobileno']))

->where('otp_number', '=' ,trim($this->param['otp']))

->get();

if($otp_detail)

{



$user_id = $otp_detail[0]->vendor_id;



//$user_detail = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();



/* if($this->param['from']=='signup')

{

DB::table('vendor')

->where('vendor_id', $user_id)

->update(['vendor_status' => trim('0')

]);



$user_detail = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();



DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

$response['message']='Sign In: Success';

$response['data'] = array('vendor_detail'=>$user_detail,'from'=>'signup');

$response['status']= 1;





}else */



if(($this->param['from']=='signin') || ($this->param['from']=='forgetpwd'))

{



/* DB::table('vendor')

->where('vendor_id', $user_id)

->update(['vendor_status' => trim('1')]); */



$user_detail = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();



if($user_detail[0]->vendor_status==1)

{

	//$user_detail = DB::table('vendor')->where('vendor_id', '=' ,$user_id)->get();



	DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

	//$response['message']='Sign In: Success';



	$response['message']='Sign In: Success';

	$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

	$response['status']= 1;

}

else

{

	$response['errorcode'] = "20004";

	$response['status']= 2;

	$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

	$response['message'] ='Sorry.Your acoount is not active.';

}

}



}

else

{

$response['errorcode'] = "20003";

$response['data'] = array('from'=>$this->param['from']);

$response['status']= 0;

$response['message'] = "Invalid OTP. Please enter a valid otp";

}



}



header('Content-type: application/json');

echo json_encode($response);

}



public function vendor_categories(Request $request){

$response= array();

$status = 1;

$cate_detail = DB::table('vendor_category')->where('status', '=' ,$status)->get();

$response['message'] = "Vendor Category";

$response['status']= 1;

$response['data'] = array('category_detail'=>$cate_detail);

return $response;

}

public function vendor_viewprofile(Request $request){

$data_post['vendor_id'] = '';

if(isset($request->vendorid)){

$data_post['vendor_id'] = $request->vendorid;

$vendor_id = $request->vendorid;

}

if(empty($data_post['vendor_id'])){

$response['errorcode'] = "70001";

$response['status']= 0;

$response['message'] = "Vendor Id: Required parameter missing";

}elseif(!empty($data_post))

{

$vendor_id = $request->vendorid;

$vendor_detail = Vendor::where('vendor_id', '=' ,$vendor_id)->get();



if(!empty($vendor_detail)){

if($vendor_detail[0]->vendor_status==1){



$response['message'] = "View Profile : View vendor detail";

$response['status']= 1;

$response['data'] = array('user_detail'=>$vendor_detail);

}

else

{

$response['errorcode'] = "70002";

$response['status']= 2;

$response['message'] ='Sorry.Your account is not active.';

}

}

else

{

$response['errorcode'] = "70003";

$response['status']= 0;

$response['message'] ='Sorry.Invalide Vendor Id!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/*

public function vendor_viewprofile (Request $request){

$data_post['vendor_id'] = '';

if(isset($this->param['vendorid'])){

$data_post['vendor_id'] = $this->param['vendorid'];

$vendor_id = $this->param['vendorid'];

}

if(empty($data_post['vendor_id'])){

$response['errorcode'] = "70001";

$response['status']= 0;

$response['message'] = "Vendor Id: Required parameter missing";

}elseif(!empty($data_post))

{

$vendor_id = $this->param['vendorid'];

$vendor_detail = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();

if(!empty($vendor_detail)){

if($vendor_detail[0]->vendor_status==1){



$response['message'] = "View Profile : View vendor detail";

$response['status']= 1;

$response['data'] = array('vendor_detail'=>$vendor_detail);

}

else

{

$response['errorcode'] = "70002";

$response['status']= 2;

$response['message'] ='Sorry.Your account is not active.';

}

}

else

{

$response['errorcode'] = "70003";

$response['status']= 0;

$response['message'] ='Sorry.Invalide Vendor Id!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}

*/



public function vendor_update_profile (Request $request){

	//return $request->file("profile");

	$vendor_id = Input::get('vendorid');

	$data['vendor_id'] = '';

	$data['firstname'] = '';

	$data['address'] = '';

	$data['city'] = '';

	$data['postcode'] = '';



    $market_area = Input::get('market_area');



	$url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$market_area."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

	$response = file_get_contents($url);



	$data = json_decode($response, true);



	if($data['status'] == 'OK')

	{ 

	

    $geo = $data['results'][0]['geometry']['location'];

    $lat = $geo['lat'];

    $lng = $geo['lng'];



    }else{



         $lat = '';

         $lng = '';

    }



    if ($request->hasFile("license_front")) {

		$licensefront=str_random(16).'.jpg';

		$request->file("license_front")->move("public/uploads/vendor/license/",$licensefront);

	}

		if ($request->hasFile("license_back")) {

		$licenseback=str_random(16).'.jpg';

		$request->file("license_back")->move("public/uploads/vendor/license/",$licenseback);

	}



	if ($request->hasFile("profile")) {

		$profile1=str_random(16).'.jpg';

		$request->file("profile")->move("public/uploads/vendor/profile/",$profile1);

	}

		if ($request->hasFile("profile_img2")) {

		$profile2=str_random(16).'.jpg';

		$request->file("profile_img2")->move("public/uploads/vendor/profile/",$profile2);

	}

		if ($request->hasFile("profile_img3")) {

		$profile3=str_random(16).'.jpg';

		$request->file("profile_img3")->move("public/uploads/vendor/profile/",$profile3);

	}

		if ($request->hasFile("profile_img4")) {

		$profile4=str_random(16).'.jpg';

		$request->file("profile_img4")->move("public/uploads/vendor/profile/",$profile4);

	}



	$subcategory = $this->param["subcategory"]; //Input::get('subcategory');



	$sub_category = $subcategory;



    //$sub_category = json_encode($subcategory);



	if(isset($request->vendorid))

	{

	$data['vendor_id'] = $request->vendorid;

	}

	if(isset($this->param["fname"]))

	{

	$data['firstname'] = $this->param["fname"];

	}

	if(isset($this->param["address"]))

	{

	$data['address'] =  $this->param["address"];

	}

	if(isset($this->param["city"]))

	{

	$data['city'] = $password  = $this->param["city"];

	}

	if(isset($this->param["postcode"]))

	{

	$data['postcode'] = $this->param["postcode"];

	}

	$response = array();

	if(empty($data['firstname'])){

	$response['errorcode'] = "60001";

	$response['status']= 0;

	$response['message']="Name:Required parameter missing";

	}else if(empty($data['vendor_id'])){

	$response['errorcode'] = "60002";

	$response['status']= 0;

	$response['message']="Merchant Id:Required parameter missing";

	}else if(empty($lat)){

	$response['errorcode'] = "60003";

	$response['status']= 0;

	$response['message']="Invalid Market Area";

	}elseif (!empty($data)) {



	$vendor_status = 0;	



	$vendor=Vendor::find($vendor_id);

    $vendor->name=$this->param['fname'];

    $vendor->last_name=$this->param['lname'];

    $vendor->address=$this->param['address'];

    $vendor->mailing_address=$this->param['mailing_address'];

    $vendor->city=$this->param['city'];

    $vendor->state=$this->param['state'];

    $vendor->zipcode=$this->param['postcode'];

    $vendor->mob_no=$this->param['mobile'];

    $vendor->market_area=$this->param['market_area'];

    $vendor->service_radius=$this->param['service_radius'];



    $vendor->business_name=$request->business;

    $vendor->vendor_status=$vendor_status;



    $vendor->driver_license=$request->driver_license;

    $vendor->license_expiry=$request->license_expiry;

    $vendor->ssn=$request->ssn;

    $vendor->dob=$request->dob;

    $vendor->type=$request->type;

    $vendor->category_id=$request->category;

    $vendor->sub_category_id=$sub_category;

    $vendor->sales_tax=$request->sales_tax;

    $vendor->permit_type=$request->permit_type;

    $vendor->permit_number=$request->permit_number;

    $vendor->permit_expiry=$request->permit_expiry;

    $vendor->description=$request->description;

    $vendor->make=$request->make;

    $vendor->model=$request->model;

    $vendor->color=$request->color;

    $vendor->year=$request->year;

    $vendor->license_plate=$request->license_plate;



    if(!empty($lat)){ $vendor->lat=$lat; }

	if(!empty($lng)){ $vendor->lng=$lng; }



	if ($request->hasFile("license_front")) {

        $vendor->license_front=$licensefront;

    }

        if ($request->hasFile("license_back")) {

        $vendor->license_back=$licenseback;

    }



    if ($request->hasFile("profile")) {

        $vendor->profile_img1=$profile1;

    }



    if ($request->hasFile("profile_img2")) {

        $vendor->profile_img2=$profile2;

    }



    if ($request->hasFile("profile_img3")) {

        $vendor->profile_img3=$profile3;

    }



    if ($request->hasFile("profile_img4")) {

        $vendor->profile_img4=$profile4;

    }



    $vendor->update();



	$vendor_data = Vendor::where('vendor_id', '=' ,$vendor_id)->get();

	$response['message'] = "Profile is successfully updated";

	$response['status']= 1;

	$response['data'] = array('user_detail'=>$vendor_data);

}




header('Content-type: application/json');

echo json_encode($response);

}



/*

public function vendor_update_profile (Request $request){



$vendor_id = Input::get('vendorid');

$data['vendor_id'] = '';

$data['firstname'] = '';

$data['address'] = '';

$data['city'] = '';

$data['postcode'] = '';



if(isset($this->param["vendorid"]))

{

$data['vendor_id'] = $this->param["vendorid"];

}

if(isset($this->param["fname"]))

{

$data['firstname'] = $this->param["fname"];

}

if(isset($this->param["address"]))

{

$data['address'] =  $this->param["address"];

}

if(isset($this->param["city"]))

{

$data['city'] = $password  = $this->param["city"];

}

if(isset($this->param["postcode"]))

{

$data['postcode'] = $this->param["postcode"];

}

$response = array();

if(empty($data['firstname'])){

$response['errorcode'] = "60001";

$response['status']= 0;

$response['message']="Full name:Required parameter missing";

}else if(empty($data['vendor_id'])){

$response['errorcode'] = "60002";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif (!empty($data)) {

DB::table('vendor')

->where('vendor_id', $vendor_id)

->update(['name' => trim($this->param['fname']),

'last_name' => trim($this->param['lname']),

'email' => trim($this->param['email']),

'address'=>  trim($data['address']),

'suburb'=>  trim($data['city']),

'state'=>  trim($this->param['state']),

'zipcode'=>  trim($data['postcode']),

'mob_no'=>  trim($this->param['mobile'])

]);



$vendor_data = DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->get();

$response['message'] = "Profile is successfully updated";

$response['status']= 1;

$response['data'] = array('vendor_detail'=>$vendor_data);

}



header('Content-type: application/json');

echo json_encode($response);

}



*/



// End vendor api */



public function register(Request $request) {

if(isset($request->fname))
{

$data['firstname'] = $request->fname;

}

if(isset($request->email))
{

$data['email'] =  $request->email;

}

if(isset($request->password))
{

$data['password'] = $password  = $request->password;

}

if(isset($request->mobileno))
{

$data['phone'] = $request->mobileno;

}


$response = array();


if(empty($data['phone'])){

$response['error'] = "10002";

$response['status']= 0;

$response['message']="Moblie No:Required parameter missing";

}else if(empty($data['password'])){

$response['error'] = "10003";

$response['status']= 0;

$response['message']="password:Required parameter missing";

}else{

$user_detail  = DB::table('users')->where('user_mob', '=' ,$request->mobileno)->get();

$user_detail_email  = DB::table('users')->where('email', '=' ,$request->email)->get();

if(count($user_detail) >0 ){ 

$response['error'] = "10004";	

$response['status']= '0';

$response['message']="Mobile No already exists";

$response['data'] = array('user_detail'=>$user_detail);

}else if(count($user_detail_email) >0 ){

$response['error'] = "10005";	

$response['status']= '0';

$response['message']="Email already exists";

$response['data'] = array('user_detail'=>$user_detail_email);

}else{

$result = new User;

$result->name = trim($request->fname);

$result->lname = trim($request->lname);

$result->email = trim($request->email);

$result->password = bcrypt($request->password);

$result->user_pass = md5($request->password);

$result->user_mob = trim($request->mobileno);

$result->dob = $request->dob;
$result->user_address = $request->address;
$result->user_city = $request->city;
$result->user_states = $request->state;
$result->user_zipcode = $request->zipcode;
$result->user_role = '1';

$result->deviceid = "";

$result->devicetype = trim($request->devicetype);

$result->devicetoken = trim($request->devicetoken);

$result->os_name = trim($request->os_name);

$result->user_status = '0';

$result->checkout_verificaiton = '1';

$result->save();

if($result){

//	$user_id = $result->id;

$otp_code = trim($this->generateRandomString(6));

$user_id = $result->id;

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($request->mobileno);

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/

$rgst_vrfy_email_cotnt = DB::table('email_content')->where('email_id',1)->first();

$message = $rgst_vrfy_email_cotnt->email_content.$otp_code;

$emailSend = trim($request->email);

$address = $emailSend;

$message = 'Hello '.trim($request->fname).', '.$rgst_vrfy_email_cotnt->email_content.$otp_code;

$subject = $rgst_vrfy_email_cotnt->email_title;

//echo $address.'-'.$subject.'-'.$message; 

$semail = $this->sendMail($address,$subject,$message);


/*********** Push Notification FOR SEND OTP END *************/

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

//$response['message'] = "User successfully created.";

$response['message'] = "You have registered successfully. OTP has been sent on your email address. Please verify it";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

}else{

$response['message']="User not created.";

$response['status']= 0;

$response['errorcode'] = "10007";

}

}

}

header('Content-type: application/json');

echo json_encode($response);

}



public function login(Request $request){

$response= array();

$data_post['username'] = '';

$data_post['password'] = '';

if(isset($this->param['mobileno'])){

$data_post['username'] = $this->param['mobileno'];

}

if(isset($this->param['password'])){

$data_post['password'] = $this->param['password'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/

if(empty($data_post['username'])){

$response['errorcode'] = "40001";

$response['status'] = 0;

$response['message']="Mobileno:Required parameter missing";

}elseif(empty($data_post['password'])){

$response['errorcode'] = "40002";

$response['status'] = 0;

$response['message'] = "Password: Required parameter missing";

}

elseif(!empty($data_post)){

$FILD_NAME = '';

if(is_numeric ($this->param['mobileno']))

{

$FILD_NAME = 'user_mob';

}

else

{

$FILD_NAME = 'email';

}

$check_mobile_no  = DB::table('users')->where($FILD_NAME, '=' ,$this->param['mobileno'])->get();

if(count($check_mobile_no)>0)

{

$otp_code = trim($this->generateRandomString(6));

if(Auth::attempt([$FILD_NAME => $this->param['mobileno'], 'password' => $this->param['password']]))

{

$user_id =  Auth::user()->id;

$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

if($user_detail[0]->user_status==1){

	/** MANAGE LOG START **/

	$log_data['log_type'] = 'normal' ;

	$log_data['log_userid'] =$user_id ;

	$log_data['log_date'] = date('Y-m-d H:i');

	$log_data['log_time']= date('Y-m-d H:i');

	$log_data['log_by'] = Input::get('devicetype'); 

	$log_data['device_id'] = '';//Input::get('deviceid') ;

	$log_data['device_type'] = Input::get('devicetype');//Input::get('device_name') ; 

	$log_data['device_os_name'] = Input::get('os_name') ;

	$devicetoken=Input::get('devicetoken');
	//$log_detail = DB::table('login_history')->insert($log_data);

	$log_id =$id = DB::table('login_history')->insertGetId($log_data);

	$update_log =  DB::table('users')->where('id', '=' ,$user_id)->update(['log_id'=>$log_id, 'devicetoken'=>$devicetoken]);

	/* cart id update */

	$cart_id = Input::get('cart_id');

	if(!empty($cart_id)){ DB::table('cart')->where('user_id',$cart_id)->update(['user_id' => $user_id]); }

	/** MANAGE LOG END **/

	$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

//	$response['message'] = "Sign In: Success";

	$response['message'] = "You have logged in successfully";

	$response['status'] = 1;

	$response['data'] = array('user_detail'=>$user_data);

}else{

	/*********** EMAIL FOR SEN OTP START *************/

	$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',8)->first();

	$subject = $usr_fgtpss_email_cotnt->email_title;

	$message = $usr_fgtpss_email_cotnt->email_content.$otp_code;

	$fullname = $user_detail[0]->name;

	$carrier_email = $user_detail[0]->carrier_email;

	$user_email_send_mail= $user_detail[0]->email;

	$msg_reg_otp ='Hello '.trim($fullname).', '.$usr_fgtpss_email_cotnt->email_content.$otp_code;

	if($FILD_NAME=='user_mob'){

		$otp_data = new User_otpdetail;

		$otp_data->user_id = trim($user_id);

		$otp_data->user_mob = trim($this->param['mobileno']);

		$otp_data->otp_number =$otp_code;

		$otp_data->save();

		$otp_inserted_id = $otp_data->otp_id;

		// Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

		// 	$message->from('info@grambunny.com', 'grambunny');

		// 	$otp_email = trim($this->param['mobileno']).$carrier_email;

		// 	$message->to($otp_email);

		// 	$message->bcc('votiveshweta@gmail.com');

		// 	$message->bcc('votivemobile.pankaj@gmail.com');

		// 	$message->bcc('votivemobile.dilip@gmail.com');

		// 	$message->bcc('votiveiphone.hariom@gmail.com');

		// 	$message->bcc('zubaer.votive@gmail.com');

		// 	$message->subject('Two Factor Auth OTP');

		// });

		/*********** EMAIL FOR SEN OTP START *************/

		$otp_email = trim($this->param['mobileno']).$carrier_email;

		$address = $otp_email; //"deepaksahuphp@gmail.com";

		$message = $msg_reg_otp;

		$subject = $usr_fgtpss_email_cotnt->email_title;

		$semail = $this->sendMail($address,$subject,$message);

		/********** EMAIL FOR SEND OTP END  *************/

	}

	if($FILD_NAME=='email'){

		$otp_data = new User_otpdetail;

		$otp_data->user_id = trim($user_id);

		$otp_data->user_email = trim($this->param['mobileno']);

		$otp_data->otp_number =$otp_code;

		$otp_data->save();

		$otp_inserted_id = $otp_data->otp_id;

		// Mail::raw($msg_reg_otp, function ($message) use ($user_email_send_mail){

		// 	$message->from('info@grambunny.com', 'grambunny');

		// 	$otp_email = $user_email_send_mail;

		// 	$message->to($otp_email);

		// 	$message->bcc('votiveshweta@gmail.com');

		// 	$message->bcc('votivemobile.pankaj@gmail.com');

		// 	$message->bcc('votivemobile.dilip@gmail.com');

		// 	$message->bcc('votiveiphone.hariom@gmail.com');

		// 	$message->bcc('zubaer.votive@gmail.com');

		// 	$message->subject('Two Factor Auth OTP');

		// });

		/*********** EMAIL FOR SEN OTP START *************/

		$otp_email = $user_email_send_mail;

		$address = $otp_email; //"deepaksahuphp@gmail.com";

		$message = $msg_reg_otp;

		$subject = $usr_fgtpss_email_cotnt->email_title;

		$semail = $this->sendMail($address,$subject,$message);

		/********** EMAIL FOR SEND OTP END  *************/

	}

	/********** EMAIL FOR SEN OTP END  *************/

	$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

	$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

	$response['errorcode'] = "40004";

	$response['status'] = 2;

	$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

	$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

	/*$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

	$response['errorcode'] = "40004";

	$response['status']= 2;

	$response['data'] = array('user_detail'=>$user_data);

	$response['message'] ='Sorry.Your acoount is not active';		*/

}

}else{

if(is_numeric ($this->param['mobileno']))
{

	if($check_mobile_no[0]->user_mob!=  $this->param['mobileno'])

	{

		$response['errorcode'] = "40006";

		$response['status'] = 0;

		$response['message'] = "Invalid Mobile Number/Email!";

	}

	else

	{

		$response['errorcode'] = "40003";

		$response['status'] = 0;

		$response['message'] = "Invalid Password!";

	}

}else{

	if($check_mobile_no[0]->email!=  $this->param['mobileno'])
	{

		$response['errorcode'] = "40006";

		$response['status'] = 0;

		$response['message'] = "Invalid Mobile Number/Email!";

	}else{

		$response['errorcode'] = "40003";

		$response['status'] = 0;

		$response['message'] = "Invalid Password!";

	}

}

}

}else{

$response['errorcode'] = "40005";

$response['status'] = 0;

$response['message'] = "Number/Email Does Not Exists in Our System, so please Registration then login!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}


/************************************************************/

//	FUNCTION NAME : Verify OTP

//	FUNCTION USE :  OTP Verification data

//	FUNCTION ERRORCODE : 20000

/****************************************************/


public function logout(Request $request){

$response= array();

$user_id = $request->user_id; //$this->param['vendor_id']; 

$login_status = 0;//$this->param['login_status'];

//DB::table('vendor')->where('vendor_id', '=' ,$vendor_id)->update(['login_status'=>$login_status]);

$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

Auth::guard("user")->logout();


$response['message'] = "You have logged out successfully";

$response['status'] = 1;

$response['data'] = array('customer_detail'=>$user_detail);

header('Content-type: application/json');

echo json_encode($response);

}


public function verify_OTP (Request $request){

$response= array();

$data_post['username'] = '';

$data_post['otp'] = '';

$data_post['from'] = '';


if(isset($this->param['mobileno'])){

$data_post['username'] = $this->param['mobileno'];

}

if(isset($this->param['otp'])){

$data_post['otp'] = $this->param['otp'];

}

if(isset($this->param['from'])){

$data_post['from'] = $this->param['from'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/



if(empty($data_post['otp'])){

$response['errorcode'] = "20001";

$response['status'] = 0;

$response['message'] = "OTP: Required parameter missing";

}elseif(empty($data_post['username'])){

$response['errorcode'] = "20002";

$response['status'] = 0;

$response['message']="mobileno:Required parameter missing";

}elseif(empty($data_post['from'])){

$response['errorcode'] = "20002";

$response['status'] = 0;

$response['message']="From:Required parameter missing";

}

elseif(!empty($data_post)){

$otp_detail = DB::table('user_otpdetail')

->where('user_mob', '=' ,trim($this->param['mobileno']))

->where('otp_number', '=' ,trim($this->param['otp']))

->get();

if(count($otp_detail) >0)
{

$user_id = $otp_detail[0]->user_id;

$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

if($this->param['from']=='signup')
{

DB::table('users')

->where('id', $user_id)

->update(['user_status' => trim('1')

]);

$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

$response['message']='Sign In: Success';

$response['data'] = array('user_detail'=>$user_detail,'from'=>'signup');

$response['status'] = 1;

}

elseif(($this->param['from']=='signin') || ($this->param['from']=='forgetpwd'))
{

DB::table('users')

->where('id', $user_id)

->update(['user_status' => trim('1')

]);


$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

if($user_detail[0]->user_status==1)
{

	$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

	DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();

	//$response['message']='Sign In: Success';

	$response['message']='Otp Verification: Success';

	$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

	$response['status'] = 1;

}else{

	$response['errorcode'] = "20004";

	$response['status'] = 2;

	$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

	$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

}

}


}else{

$response['errorcode'] = "20003";

$response['data'] = array('from'=>$this->param['from']);

$response['status'] = 0;

$response['message'] = "Invalid OTP. Please enter a valid otp";

}

}

header('Content-type: application/json');

echo json_encode($response);

}


public function add_mobile_no(Request $request){

$response= array();

$data_post['user_id'] = '';

$data_post['user_mobile'] = '';

if(isset($this->param['user_id'])){

$data_post['user_id'] = $this->param['user_id'];

}


if(isset($this->param['user_mobile'])){

$data_post['user_mobile'] = $this->param['user_mobile'];

}


if(empty($data_post['user_id'])){

$response['errorcode'] = "20001";

$response['status'] = 0;

$response['message'] = "UserId: Required parameter missing";

}elseif(empty($data_post['user_mobile'])){

$response['errorcode'] = "20002";

$response['status'] = 0;

$response['message']="Mobileno:Required parameter missing";

}elseif(!empty($data_post)){



$get_user = DB::table('users')

->where('id', '=' ,trim($this->param['user_id']))

->first();



if(count($get_user)>0){



$usermobile = trim($this->param['user_mobile']);

$otp_code = trim($this->generateRandomString(6));





DB::table('users')

->where('id',trim($this->param['user_id']))

->update(['user_mob' => $usermobile,'user_otp' => $otp_code]);



$mobl_vrfyn_email_cotnt = DB::table('email_content')->where('email_id',20)->first();



$address = $get_user->email; 

$subject = $mobl_vrfyn_email_cotnt->email_title;

$message = $mobl_vrfyn_email_cotnt->email_content.$otp_code;



$semail = $this->sendMail($address,$subject,$message); 

$response['status'] = 1;

$response['message'] = "OTP has been sent on your email address. Please verify it.";

}else{



$response['errorcode'] = "20001";

$response['status'] = 0;

$response['message'] = "Invalid User Id."; 



}



}



header('Content-type: application/json');

echo json_encode($response);

}





public function resend_mobile_otp(Request $request){



$response= array();



$data_post['user_id'] = '';

$data_post['user_mobile'] = '';



if(isset($this->param['user_id'])){

$data_post['user_id'] = $this->param['user_id'];

}



if(isset($this->param['user_mobile'])){

$data_post['user_mobile'] = $this->param['user_mobile'];

}



if(empty($data_post['user_id'])){

$response['errorcode'] = "20001";

$response['status'] = 0;

$response['message'] = "UserId: Required parameter missing";

}elseif(empty($data_post['user_mobile'])){

$response['errorcode'] = "20002";

$response['status'] = 0;

$response['message']="Mobileno:Required parameter missing";

}elseif(!empty($data_post)){



$get_user = DB::table('users')

->where('id', '=' ,trim($this->param['user_id']))

->first();



if(count($get_user)>0){



$usermobile = trim($this->param['user_mobile']);

$otp_code = trim($this->generateRandomString(6));





DB::table('users')

->where('id',trim($this->param['user_id']))

->update(['user_mob' => $usermobile,'user_otp' => $otp_code]);



$mbl_vrfy_email_cotnt = DB::table('email_content')->where('email_id',20)->first();



$address = $get_user->email; 

$subject = 'Resend '.$mbl_vrfy_email_cotnt->email_title;

$message = $mbl_vrfy_email_cotnt->email_content.$otp_code;



$semail = $this->sendMail($address,$subject,$message); 

$response['status'] = 1;

$response['message'] = "Resend OTP has been sent on your email address. Please verify it.";

}else{



$response['errorcode'] = "20001";

$response['status'] = 0;

$response['message'] = "Invalid User Id."; 



}



}



header('Content-type: application/json');

echo json_encode($response);

}	



public function verify_mobile_no(Request $request){



$response= array();



$data_post['user_id'] = '';

$data_post['user_otp'] = '';



if(isset($this->param['user_id'])){

$data_post['user_id'] = $this->param['user_id'];

}



if(isset($this->param['user_otp'])){

$data_post['user_otp'] = $this->param['user_otp'];

}



if(empty($data_post['user_id'])){

$response['errorcode'] = "20001";

$response['status']= 0;

$response['message'] = "UserId: Required parameter missing";

}elseif(empty($data_post['user_otp'])){

$response['errorcode'] = "20002";

$response['status']= 0;

$response['message']="Userotp:Required parameter missing";

}elseif(!empty($data_post)){



$user_otp = trim($this->param['user_otp']);   

$get_mobile = DB::table('users')

->where('id', '=' ,trim($this->param['user_id']))

->where('user_otp', '=' ,trim($user_otp))

->value('user_mob');



if(count($get_mobile)>0){



DB::table('users')

->where('id',trim($this->param['user_id']))

->update(['user_status' =>  '1']);



$response['mobileno'] = $get_mobile;

$response['status']= 1;

$response['message'] = "OTP verifyed!";

}else{



$response['errorcode'] = "20001";

$response['status']= 0;

$response['message'] = "Invalid Otp."; 



}



}



header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Resent OTP

//	FUNCTION USE :  Resent OTP  data

//	FUNCTION ERRORCODE : 30000

/****************************************************/





public function resent_OTP (Request $request){

$response= array();

$data_post['username'] = '';

$data_post['from'] = '';

if(isset($this->param['mobileno']))

{

$data_post['username'] = $this->param['mobileno'];

}

if(isset($this->param['from']))

{

$data_post['from'] = $this->param['from'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/

if(empty($data_post['username'])){

$response['errorcode'] = "30001";

$response['status']= 0;

$response['message'] = "Mobile No: Required parameter missing";

}elseif(empty($data_post['from'])){

$response['errorcode'] = "30002";

$response['status']= 0;

$response['message']="From:Required parameter missing";

}

elseif(!empty($data_post)){

$user_detail = DB::table('users')->where('email', '=' ,$this->param['mobileno'])->get();



if(!empty($user_detail))

{

$user_id = $user_detail[0]->id;

$carrier_email = $user_detail[0]->carrier_email;

$fullname = $user_detail[0]->name;

$otp_code = trim($this->generateRandomString(6));

$type_form = $this->param['from'];

if($this->param['from']=='signup' || $this->param['from']=='reset')

{

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->otp_number = trim($otp_code);

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

$user_data = DB::table('users')->where('email', '=' ,$this->param['mobileno'])->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

//$response['message'] = "Resend OTP.";

$response['message'] = "OTP has been sent on your mobile number. Please verify it.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail,'from'=>$this->param['from']);

}elseif(($this->param['from']=='signin') || ($this->param['from']=='forgetpwd'))

{

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->otp_number = trim($otp_code);

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

$user_data = DB::table('users')->where('email', '=' ,$this->param['mobileno'])->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

if($user_detail[0]->user_status==1)

{

	//$response['message'] = "Resend OTP.";

	$response['message'] = "OTP has been sent on your mobile number. Please verify it.";

	$response['status']= 1;

	$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail,'from'=>$this->param['from']);

}

else

{

	$response['errorcode'] = "30004";

	$response['status']= 2;

	$response['data'] = array('user_detail'=>$user_detail,'from'=>$this->param['from']);

	$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

}

}

// /*********** EMAIL FOR SEN OTP START *************/

// $subject_otp = 'Resend OTP for '.$type_form;

// $msg_reg_otp ='Dear  '.trim($fullname).', The verification code resend for '.$type_form .'  on grambunny is : '.$otp_code ;

// Mail::raw($msg_reg_otp, function ($message) use ($carrier_email,$subject_otp){

// 	$message->from('info@grambunny.com', 'grambunny');

// 	$otp_email = trim($this->param['mobileno']).$carrier_email;

// 	$message->to($otp_email);

// 	$message->bcc('votiveshweta@gmail.com');

// 	$message->bcc('votivemobile.pankaj@gmail.com');

// 	$message->bcc('votivemobile.dilip@gmail.com');

// 	$message->bcc('votiveiphone.hariom@gmail.com');

// 	$message->bcc('zubaer.votive@gmail.com');

// 	$message->subject($subject_otp);

// });

// /********** EMAIL FOR SEN OTP END  *************/

/*********** EMAIL FOR SEN OTP START *************/

$rsnd_otp_email_cotnt = DB::table('email_content')->where('email_id',8)->first();



$address = $user_detail[0]->email; //"deepaksahuphp@gmail.com";

$message = 'Hello  '.trim($fullname).', '.$rsnd_otp_email_cotnt->email_content .$type_form .'is : '.$otp_code ;

$subject = $rsnd_otp_email_cotnt->email_title .$type_form;

$semail = $this->sendMail($address,$subject,$message);



$response['message'] = "OTP has been sent on your email address. Please verify it.";

$response['status']= 1;



/********** EMAIL FOR SEND OTP END  *************/

}

else

{

$response['errorcode'] = "30003";

$response['data'] = array('from'=>$this->param['from']);

$response['status']= 0;

$response['message'] = "Invalid Mobile Number!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : FORGOT PASSWORD

//	FUNCTION USE :  app user FORGOT PASSWORD functionality

//	FUNCTION ERRORCODE : 50000

/****************************************************/

public function forget_pwd(Request $request)
{

$response = array();

//$mobilenum = trim($this->format_phone_normal($request->mobileno));

//$mobilenum = trim($this->format_phone_string($mobile));

$uemailid = $request->emailid;

$data_post['username'] = '';

if(isset($uemailid)){

$data_post['username'] = $uemailid;

}

if(empty($data_post['username'])){

$response['errorcode'] = "50001";

$response['status']= 0;

$response['message'] = "Mobile No: Required parameter missing";

}else{

$user_detail = DB::table('users')->where('email',$uemailid)->get(); 

//echo "<pre>";print_r($user_detail[0]->email); die;

if(count($user_detail) >0){

$otp_code = trim($this->generateRandomString(6));

//$otp_code='123456';

$carrier_email = trim($user_detail[0]->email);

if($user_detail[0]->user_status==1){

$user_id =  $user_detail[0]->id;

$user_mob =  $user_detail[0]->user_mob;

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($user_mob);

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/

$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',2)->first();

$emailSend = trim($user_detail[0]->email);

$address = $emailSend; //"deepaksahuphp@gmail.com";

$message = 'Hello '.trim($user_detail[0]->name).', '.$usr_fgtpss_email_cotnt->email_content.$otp_code;

$subject = $usr_fgtpss_email_cotnt->email_title;

$semail = $this->sendMail($address,$subject,$message);


 	/* Mail::send("emails.api_email",["apimsg" => $message],function($message) use ($address){

 		$message->from(config("app.webmail"), config("app.mailname"));

        $message->subject('Forgot Password');

        $message->to($address);

 	}); */


/********** EMAIL FOR SEND OTP END  *************/

/*********** Push Notification FOR SEND OTP END *************/

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

	//$response['message'] = "Forget Pwd: Mobile Number Varify!";

$response['message'] = "OTP has been sent on your email. Please verify it.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

}

else
{	

$response['errorcode'] = "50002";

$response['status']= 2;

$response['message'] ='Sorry.Your acoount is not active. Please verify your email Id via OTP.';

}

}else{

$response['errorcode'] = "50003";

$response['status']= 0;

$response['message'] ='The email Id is not registered with us. Please try again. ';

}

}

header('Content-type: application/json');

echo json_encode($response);

}

public function format_phone_normal($mobileno)
{

$str1 = str_replace(' ','',$mobileno); 
$str2 = str_replace('-','',$str1); 
$str3 = str_replace('(','',$str2); 
$mobile = str_replace(')','',$str3); 

return $mobile;

}


public function format_phone_string($s) {

$rx = "/
    (1)?\D*     # optional country code
    (\d{3})?\D* # optional area code
    (\d{3})\D*  # first three
    (\d{4})     # last four
    (?:\D+|$)   # extension delimiter or EOL
    (\d*)       # optional extension
/x";
preg_match($rx, $s, $matches);
if(!isset($matches[0])) return false;

$country = $matches[1];
$area = '('.$matches[2].')';
$three = $matches[3];
$four = $matches[4];
$ext = $matches[5];

$out = "$three-$four";
if(!empty($area)) $out = "$area $out";
if(!empty($country)) $out = "+$country-$out";
if(!empty($ext)) $out .= "x$ext";

// check that no digits were truncated
// if (preg_replace('/\D/', '', $s) != preg_replace('/\D/', '', $out)) return false;
return $out;
}


/************************************************************/

//	FUNCTION NAME : Reset_password

//	FUNCTION USE :  app user onfromation update

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function reset_password (Request $request){

$user_id = Input::get('userid');

$data['user_id'] = Input::get("userid");

$data['newpassword'] = Input::get("newpassword");

$response = array();

if(empty($data['newpassword'])){

$response['errorcode'] = "200001";

$response['status']= 0;

$response['message']="New password:Required parameter missing";

}elseif(empty($data['user_id'])){

$response['message']="user id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "200002";

}elseif(!empty($data)) {

DB::table('users')

->where('id', $user_id)

->update(['password' =>  Hash::make(trim(Input::get('newpassword'))),

'user_pass'=> md5(trim(Input::get('newpassword')))

]);

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "Password updated sucessfully.";

$response['status']= 1;

$response['data'] = $user_data;

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : Search_restaurant

//	FUNCTION USE :  SHOW ALL RESTAURANT LIST AND IF SEARCH WITH ANY CONDITION

//	FUNCTION ERRORCODE : 100000

/****************************************************/



public function get_address(Request $request)

{

DB::enableQueryLog();



$data['latitude'] = $latitude = Input::get("latitude");

$data['longitude'] = $longitude = Input::get("longitude");



$response = array();

if(empty($data['latitude'])){

$response['errorcode'] = "200001";

$response['status']= 0;

$response['message']="Latitude:Required parameter missing";

}elseif(empty($data['longitude'])){

$response['message']="Longitude:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "200002";

}elseif(!empty($data)) {



//$address = $this->getaddress($latitude,$longitude);

$get_dbaddress = DB::table('delivery_address')

->select("*")

->where("del_lat","=",$latitude)

->where("del_long","=",$longitude)

->first();



$lat = number_format(floor($latitude*100)/100,2, '.', ''); 

$long = number_format(floor($longitude*100)/100,2, '.', '');  



$get_address = DB::table('delivery_address')

->select("*")

->where('del_lat','like','%' . $lat . '%')

->where('del_long','like','%' . $long . '%')

->first();



$latlong = '';

$naddress = '';



/* if(!empty($address)){ $naddress = $address; } */



if(!empty($get_dbaddress)){  



$naddress = $get_dbaddress->del_address;



}else if(!empty($get_address)){



$naddress = 'Near by:-'.$get_address->del_address;



}else{



$latlong = $lat.','.$long ;



}



$response['message'] = "Address created sucessfully.";

$response['status']= 1;

$response['data'] = array('current_address' => $naddress,'lat_long' => $latlong);

}

header('Content-type: application/json');

echo json_encode($response);

}



public function getaddress($latitude,$longitude){



$lat = trim($latitude);

$long = trim($longitude);



/* $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($long).'&sensor=false&key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE';

$json = @file_get_contents($url);

$data=json_decode($json);



return $data; */





$geocode = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE";

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $geocode);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_PROXYPORT, 3128);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

$response = curl_exec($ch);

curl_close($ch);

$output = json_decode($response);

$dataarray = get_object_vars($output);

if ($dataarray['status'] != 'ZERO_RESULTS' && $dataarray['status'] != 'INVALID_REQUEST') {

if (isset($dataarray['results'][0]->formatted_address)) {



$address = $dataarray['results'][0]->formatted_address;



} else { $address = '0'; }

} else { $address = '0'; }



return $address;

}



public function search_restaurant()

{

DB::enableQueryLog();

$data['add_lat'] = '';

$data['add_lng'] = '';

$data['loc_address'] = '';



$lang_id = Input::get('lang_id');



$city = $this->getcity(Input::get('loc_address')); 



if(empty($city)){ $city = 'gaza strip';}



if(isset($this->param["add_lat"]))

{

$data['add_lat'] = $this->param["add_lat"];

}

if(isset($this->param["add_lng"]))

{

$data['add_lng'] =  $this->param["add_lng"];

}

if(isset($this->param["loc_address"]))

{

$data['loc_address'] =  $this->param["loc_address"];

}



if(isset($city))

{

$data['city'] = $city;

}



$response = array();

if(empty($data['add_lat'])){

$response['errorcode'] = "100001";

$response['status']= 0;

$response['message'] = "Address Lat: Required parameter missing";

}

elseif(empty($data['add_lng'])){

$response['errorcode'] = "100002";

$response['status']= 0;

$response['message'] = "Address Lng: Required parameter missing";

}elseif(empty($data['loc_address'])){

$response['errorcode'] = "100003";

$response['status']= 0;

$response['message'] = "Address : Required parameter missing";

}elseif(empty($data['city'])){

$response['errorcode'] = "100004";

$response['status']= 0;

$response['message'] = "Address : Enter valid address";

}elseif(!empty($data))

{



//$city = $this->getcity(Input::get('loc_address'));



$locaddressed = '';



$stradd = trim(Input::get('loc_address'));



$straddress = explode(":-",$stradd); 



foreach ($straddress as $key => $value) {



$locaddressed = $value;



}



$get_dbaddress = DB::table('delivery_address') 

->select("*")

->where("del_userid","=",Input::get('userid'))

->where("del_address","=",$locaddressed)

->first();



$get_dbaddressall = DB::table('delivery_address') 

->select("*")

->where("del_userid","=",Input::get('userid'))

->get();



$addcount = count($get_dbaddressall);



if($addcount>4){



$get_add_first = DB::table('delivery_address')->where("del_userid","=",Input::get('userid'))->first();



DB::table('delivery_address')->where('del_id', '=', trim($get_add_first->del_id))->delete();



}





if(empty($get_dbaddress)){	



$del_address=array(

'del_userid'=> trim(Input::get('userid')),

'del_guestid'=> trim(Input::get('guest_id')),

'del_address'=>	trim($locaddressed),

'del_lat'=> trim(Input::get('add_lat')),

'del_long'=> trim(Input::get('add_lng')),

'deviceid'=> trim(Input::get('deviceid')),

'devicetype'=> trim(Input::get('devicetype')),

'created_at'=>date('Y-m-d H:i:s')

);



DB::table('delivery_address')->insert($del_address);



}





$test_sql = '';

$lat = Input::get('add_lat');

$long = Input::get('add_lng');

$distancein_miles = 5;

//$keyword = Input::get('keyword_search');

//$keyword = Input::get('keywords');

$distancein_miles =$this->param['distanceinmiles'];

$keyword =$this->param['keywords'];

$open_status_search  = $this->param['open_status'];

/*************************/



if(!empty($distancein_miles)){



$count_data =count(DB::table('search_restaurant_view')

->select(DB::raw("*,

(((acos(sin((".$lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$long." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance"))

->having("distance","<",$distancein_miles )

->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE')

->groupBy('search_restaurant_view.rest_id')

->get());	



}else{



$count_data =count(DB::table('search_restaurant_view')

->where('rest_city', '=' , $city)	

->get());

}	



$google_data = '';



/************************/

$limit = 10;

/*if((Input::get('per_page')))

{

$limit = Input::get('per_page');

}*/

$cuisine_list  = DB::table('cuisine')->where('cuisine_status', '=' ,'1')->orderBy('cuisine_name', 'asc')->get();



$cuisine_listnew = array();



if ($lang_id == 'ar') {

foreach ($cuisine_list as $key => $value) {

$cuisine_lists = array(

"cuisine_id" => $value->cuisine_id,

"cuisine_name" => $value->cuisine_name_ar,

"cuisine_image" => $value->cuisine_image,

"cuisine_status" => $value->cuisine_status,

"cuisine_lang" => $value->cuisine_lang,

"created_at" => $value->created_at,

"updated_at" => $value->updated_at,

);		

$cuisine_listnew[] = $cuisine_lists;		

}



}else{

foreach ($cuisine_list as $key => $value) {	

$cuisine_lists = array(

"cuisine_id" => $value->cuisine_id,

"cuisine_name" => $value->cuisine_name,

"cuisine_image" => $value->cuisine_image,

"cuisine_status" => $value->cuisine_status,

"cuisine_lang" => $value->cuisine_lang,

"created_at" => $value->created_at,

"updated_at" => $value->updated_at,

);		

$cuisine_listnew[] = $cuisine_lists;		

}

}





$rest_listing = DB::table('search_restaurant_view');



if(!empty($lat) && ($long)) 

{



if(!empty($distancein_miles)){



$rest_listing = $rest_listing->select(DB::raw("*,(((acos(sin((".$lat."*pi()/180)) * sin((`rest_lat`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`rest_lat`*pi()/180)) * cos(((".$long." - `rest_long`)* pi()/180))))*180/pi())*60*1.1515) as distance, (SELECT `rest_time`.`rest_id` FROM `rest_time` WHERE `rest_time`.`rest_id` = `search_restaurant_view`.`rest_id` and `rest_time`.`day` = '".date('D')."' and `rest_time`.`open_time` <= '".date('H:i:s')."' and `rest_time`.`close_time` >= '".date('H:i:s')."'  limit 1 ) as 'open_status' "));

$rest_listing = $rest_listing->having("distance","<",$distancein_miles);	



}else{

$rest_listing = $rest_listing->where('search_restaurant_view.rest_city', '=' ,$city);



} 



}

else

{

$rest_listing = $rest_listing->select('*');

}



	$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');



	if( $this->param['reset']==0){

		if(($open_status_search!='') && ($open_status_search==1))

		{

			$cc_time['cc_time'] = date('H:i:s');

			$cc_time['cc_day'] = date('D');

		

			$rest_listing = $rest_listing->whereIn('search_restaurant_view.rest_id',function($query) use ($cc_time) {

				$query->select('rest_time.rest_id')

				->from('rest_time')

				->where('rest_time.day', '=' , $cc_time['cc_day'])

				->where('rest_time.open_time', '<' , $cc_time['cc_time'] )

				->where('rest_time.close_time', '>' , $cc_time['cc_time'] );

			});

		}

		elseif(($open_status_search!='') && ($open_status_search==0))

		{

			$cc_time['cc_time'] = date('H:i:s');

			$cc_time['cc_day'] = date('D');

		

			$rest_listing = $rest_listing->whereNOTIn('search_restaurant_view.rest_id',function($query) use ($cc_time) {

				$query->select('rest_time.rest_id')

				->from('rest_time')

				->where('rest_time.day', '=' , $cc_time['cc_day'])

				->where('rest_time.open_time', '<' ,$cc_time['cc_time'] )

				->where('rest_time.close_time', '>' , $cc_time['cc_time'] );

			});

		}



if(!empty($keyword))

{

if(!empty($keyword))

{

$rest_listing = $rest_listing->whereIn('search_restaurant_view.rest_id',function($query) use ($keyword) {

	$query->select('food_rest_name_view.rest_id')

	->from('food_rest_name_view')

	->where('food_rest_name_view.rest_food', 'like' , '%'.$keyword.'%');

});

}

}

$foodType = $this->param['food_type'];

$foodType_post = 0;

//print_r($foodType);

if(empty($foodType))

{

$foodType_post=0;

}

else

{

$foodType_post=1;

}

if(isset($foodType) && (count($foodType)>0) && ($foodType_post==1) )

{

$rest_listing = $rest_listing->where(function($query) {

$l=0;

foreach($this->param['food_type'] as $ftype )

{

	if($ftype>0)

	{

		if($l>0){

			$query = $query->orWhereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

		}

		else

		{

			$query = $query->whereRaw('FIND_IN_SET("'.$ftype.'",search_restaurant_view.rest_cuisine)');

		}



}

$l++;

}

});

}

if(!empty($this->param['rating']))

{

$rest_listing = $rest_listing->where('search_restaurant_view.totalrating', '=' ,$this->param['rating']);

}

if(!empty($this->param['price_level']))

{

$rest_listing = $rest_listing->where('search_restaurant_view.rest_price_level', '=' ,$this->param['price_level']);

}

if($this->param['sortedby']>0)

{

if($this->param['sortedby']=='1')	 //1.Rating(Ascending) (value=1)

{

$rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'asc');

}

if($this->param['sortedby']=='2')	 //2.Rating(Descending) (value=2)

{

	$rest_listing = $rest_listing->orderBy('search_restaurant_view.totalrating', 'desc');

}

if($this->param['sortedby']=='3')	 //2.Rating(Descending) (value=2)

{

	$rest_listing = $rest_listing->orderBy('distance', 'asc');

}

if($this->param['sortedby']=='4')	 //2.Rating(Descending) (value=2)

{

	$rest_listing = $rest_listing->orderBy('distance', 'desc');

}

// $rest_listing->orderBy('distance', 'asc');

if($this->param['sortedby']=='5')		 //5.Delivery Min(Ascending) fees,(value=5)

{

	$rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'asc');

}

if($this->param['sortedby']=='6')		 //6.Delivery Min(Descending) fees,(value=6)

{

	$rest_listing = $rest_listing->orderBy('search_restaurant_view.rest_mindelivery', 'desc');

}



}

}

$rest_listing = $rest_listing->orderBy('search_restaurant_view.google_type', 'asc');

$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

$test_sql  = $rest_listing;

$test_sql  = $test_sql->get();

$rest_listing = $rest_listing->simplePaginate($limit);

$total_restaurant =count($rest_listing) ;

//	 dd(DB::getQueryLog());

$rest_data = '';

$open_staus = 0 ;

$tot_rec = count($test_sql);

// print_r($test_sql);

$lastPage  = ceil($tot_rec/$limit);

foreach($rest_listing as $listing)

{

/** FAVOURIT OPTION **/

$rest_favourite= 0;

if(isset($this->param['userid']) && ($this->param['userid']>0))

{

$favourate_listing = DB::table('favourite_restaurant')

->where('favrest_userid', '=' ,$this->param['userid'])

->where('favrest_status', '=' ,'1')

->where('favrest_restid', '=' ,$listing->rest_id)

->get();

if($favourate_listing)

{

	$rest_favourite= 1;

}

}

/**  END   ***/

$rating = 0;

if($listing->totalrating=='NULL')

{

$rating = 0;

}

else

{

$rating = round($listing->totalrating);

}

$open_staus = 0 ;// 0-close / 1-open Calcualtion reamning

/********************/

/*******************/

$dist_km = '';

if(@$listing->distance)

{

$dist_km = $listing->distance;

}

$logo_rest = '';

if($listing->google_type==1)

{

$logo_rest = $listing->rest_logo;

}

else

{

$logo_rest = url('/').'/uploads/reataurant/'.$listing->rest_logo;

/**************************** TIME OPEN STATUS  *******/

$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

$rest_end = '';

$old_day = 0;

$current_time =$tt=  strtotime(date("H:i"));

if(!empty($listing->open_status))

{

$open_staus = 1;

}



/*********************  TIME OPEN STATUS END ***************/

}

$cuisine_name = '';

if(!empty($listing->cuisine_name))

{

$cuisine_name =	$listing->cuisine_name;

}



if($lang_id == 'ar') {

$rest_name = $listing->rest_name_ar;

} else {

$rest_name = $listing->rest_name;

}



$rest_data[] = array(

'rest_id' => $listing->rest_id,

'rest_name' => $rest_name,

'rest_metatag' => $listing->rest_metatag,

'rest_logo' => trim($logo_rest),

'rest_address' => $listing->rest_address,

'rest_address2' => $listing->rest_address2,

'rest_city' => $listing->rest_city,

'rest_zip_code' => $listing->rest_zip_code,

'rest_suburb' => $listing->rest_suburb,

'rest_state' => $listing->rest_state,

'rest_service'=>$listing->rest_service,

'rest_mindelivery'=>$listing->rest_mindelivery,

'rest_status'=>$listing->rest_status,

'rest_email'=>$listing->rest_email,

'cuisine_name'=>$cuisine_name,

'total_review'=>$listing->totalreview,

'total_rating'=>$rating,

'distance'=>$dist_km,

'open_status'=>$open_staus,

'rest_favourite'=>$rest_favourite,

'rest_lat'=>$listing->rest_lat,

'rest_long'=>$listing->rest_long,

'rest_google'=>$listing->google_type,

'rest_price_level'=>$listing->rest_price_level,

'rest_min_orderamt' => trim($listing->rest_min_orderamt)

);

}

$rest_detail_format['total'] = $tot_rec  ;

$rest_detail_format['per_page'] =$rest_listing->perPage();

$rest_detail_format['current_page'] =$rest_listing->currentPage();;

$rest_detail_format['last_page'] =$lastPage;

$rest_detail_format['data'] =$rest_data;

if($total_restaurant>0)

{

$response['message'] = "All restaurant list";

$response['status']= 1;

$response['cuisine_list'] = $cuisine_listnew;

$response['rest_listing'] = $rest_detail_format;

//$response['rest_listing_main'] = $rest_listing;

}

else

{

$response['message']="No restaurant found!";

$response['status']= 0;

$response['errorcode'] = "100001";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : show_soical_login

//	FUNCTION USE :  Remove all old restaurnt item and insert new

//	FUNCTION ERRORCODE : 460000

/****************************************************/



public function get_soical_signup()

{

$data['firstname'] = '';

$data['email'] = '';

$data['password'] = '';

$data['facebookid'] = '';

$data['phone'] = '';

$data['deviceid'] = '';

$data['devicetype'] = '';

$data['carrier_name'] = '';

$carrier_id = '';

$carrier_name = '';

$carrier_email = '';

if(isset($this->param["fname"]))

{

$data['firstname'] = $this->param["fname"];

}

if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

if(isset($this->param["password"]))

{

$data['password'] = $password  = $this->param["password"];

}

if(isset($this->param["facebookid"]))

{

$data['facebookid'] = $this->param["facebookid"];

}

if(isset($this->param["mobileno"]))

{

$data['phone'] = $this->param["mobileno"];

}

if(isset($this->param["carrier_name"]))

{

$data['carrier_name'] = $carrier_id = $this->param["carrier_name"];

switch($carrier_id)

{

case 1:

$carrier_id = '1';

$carrier_name = 'Alltel';

$carrier_email = '@message.alltel.com';

break;

case 2:

$carrier_id = '2';

$carrier_name = 'AT&T';

$carrier_email = '@txt.att.net';

break;

case 3:

$carrier_id = '3';

$carrier_name = 'Boost Mobile';

$carrier_email = '@myboostmobile.com';

break;

case 4:

$carrier_id = '4';

$carrier_name = 'Sprint';

$carrier_email = '@messaging.sprintpcs.com';

break;

case 5:

$carrier_id = '5';

$carrier_name = 'T-Mobile';

$carrier_email = '@tmomail.net';

break;

case 6:

$carrier_id = '6';

$carrier_name = 'U.S. Cellular';

$carrier_email = '@email.uscc.net';

break;

case 7:

$carrier_id = '7';

$carrier_name = 'Verizon';

$carrier_email = '@vtext.com';

break;

case 8:

$carrier_id = '8';

$carrier_name = 'Virgin Mobile';

$carrier_email = '@vmobl.com';

break;

case 9:

$carrier_id = '9';

$carrier_name = 'Republic Wireless';

$carrier_email = '@text.republicwireless.com';

break;

}

}

$response = array();

if((empty($data['phone'])) && (empty($data['email']))){

$response['errorcode'] = "460001";

$response['status']= 0;

$response['message']="Moblie No / Email Id :Required parameter missing";

}elseif (!empty($data)) {

$response='';

$user_detail  = '';

$user_detail_email  = '';

if((isset($this->param["mobileno"]))&& (!empty($this->param['mobileno'])))

{

$user_detail  = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

}

if((isset($this->param["email"]))&& (!empty($this->param['email'])))

{

$user_detail_email  = DB::table('users')->where('email', '=' ,$this->param['email'])->get();

}

if((isset($this->param["mobileno"]))&& (!empty($this->param['mobileno'])) && (!empty( $user_detail)))

{

$response['errorcode'] = "460002";

$response['status']= 1;

$response['message']="Mobile No already exists";

$response['data'] = array('user_detail'=>$user_detail);

}elseif( (isset($this->param['email']))&& (!empty($this->param['email'])) && (!empty($user_detail_email)))

{

/*	 DB::enableQueryLog();

print_r($user_detail_email);

dd(DB::getQueryLog());

exit;*/

$response['errorcode'] = "460006";

$response['status']= 1;

$response['message']="Email already exists";

$response['data'] = array('user_detail'=>$user_detail_email);

}else{

$result = new User;

$result->name = trim($this->param['fname']);

$result->lname = trim($this->param['lname']);

$result->email = trim($this->param['email']);

$result->password = bcrypt($this->param['password']);

$result->user_pass = md5($this->param['password']);

$result->user_mob = trim($this->param['mobileno']);

$result->user_fbid = trim($this->param['facebookid']);

$result->user_gpid = trim($this->param['googleplusid']);

$result->user_role = '1';

$result->deviceid = trim($this->param['deviceid']);

$result->devicetype = trim($this->param['devicetype']);

$result->user_status = '0';

$result->carrier_id = $carrier_id;

$result->carrier_name =$carrier_name;

$result->carrier_email = $carrier_email;

$result->dummy1 = trim($this->param['dummy1']);

$result->dummy2 = trim($this->param['dummy2']);

$result->dummy3 = trim($this->param['dummy3']);

$result->checkout_verificaiton = trim($this->param['checkout_verificaiton']);

$result->save();

if($result){

$user_id = $result->id;

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "You have registered  successfully.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data);

}else{

$response['message']="User not created.";

$response['status']= 0;

$response['errorcode'] = "460007";

}

}

}

header('Content-type: application/json');

//echo json_encode($result);

echo json_encode($response);

}





//	FUNCTION NAME : View User Profile

//	FUNCTION USE :  app user onfromation update

//	FUNCTION ERRORCODE : 70000

/****************************************************/

public function viewprofile (Request $request){

$data_post['user_id'] = '';

if(isset($this->param['userid'])){

$data_post['user_id'] = $this->param['userid'];

$user_id = $this->param['userid'];

}

if(empty($data_post['user_id'])){

$response['errorcode'] = "70001";

$response['status']= 0;

$response['message'] = "User Id: Required parameter missing";

}elseif(!empty($data_post))

{

$user_id = $this->param['userid'];

$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

if(!empty($user_detail)){

if($user_detail[0]->user_status==1){

$dummy1_id='0';$dummy1_name='House';

$dummy2_id='0';$dummy2_name='House';

$dummy3_id='0';$dummy3_name='House';

//$dummy1_name = $user_detail[0]->dummy1;

if(($user_detail[0]->dummy1>0) && ($user_detail[0]->dummy1!='House')){

$user_dummy1  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy1)->get();

$dummy1_name = $user_dummy1[0]->name.' '.$user_dummy1[0]->lname;

$dummy1_id = $user_detail[0]->dummy1;

}

if(($user_detail[0]->dummy2>0) && ($user_detail[0]->dummy2!='House')){

$user_dummy2  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy2)->get();

$dummy2_name = $user_dummy2[0]->name.' '.$user_dummy2[0]->lname;

$dummy2_id = $user_detail[0]->dummy2;

}

if(($user_detail[0]->dummy3>0) && ($user_detail[0]->dummy3!='House')){

$user_dummy3  = DB::table('users')->where('id', '=' ,$user_detail[0]->dummy3)->get();

$dummy3_name = $user_dummy3[0]->name.' '.$user_dummy3[0]->lname;

$dummy3_id = $user_detail[0]->dummy3;

}

$response['message'] = "View Profile : View user detail";

$response['status']= 1;

$response['data'] = array(

'user_detail'=>$user_detail,

'dummy1_id' =>$dummy1_id,

'dummy1_name' =>trim($dummy1_name),

'dummy2_id' =>$dummy2_id,

'dummy2_name' =>trim($dummy2_name),

'dummy3_id' =>$dummy3_id,

'dummy3_name' =>trim($dummy3_name)

);

}

else

{

$response['errorcode'] = "70002";

$response['status']= 2;

$response['message'] ='Sorry.Your account is not active. Please verify your mobile number via OTP.';

}

}

else

{

$response['errorcode'] = "70003";

$response['status']= 0;

$response['message'] ='Sorry.Invalide user Id!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Update_profile

//	FUNCTION USE :  app user onfromation update

//	FUNCTION ERRORCODE : 60000

/****************************************************/

public function update_profile (Request $request){

$user_id = Input::get('userid');

$data['user_id'] = '';

$data['firstname'] = '';

$data['address'] = '';

$data['city'] = '';

$data['postcode'] = '';

if(isset($this->param["userid"]))

{

$data['user_id'] = $this->param["userid"];

}

if(isset($this->param["fname"]))

{

$data['firstname'] = $this->param["fname"];

}

if(isset($this->param["address"]))

{

$data['address'] =  $this->param["address"];

}

if(isset($this->param["city"]))

{

$data['city'] = $password  = $this->param["city"];

}

if(isset($this->param["postcode"]))

{

$data['postcode'] = $this->param["postcode"];

}

$response = array();

if(empty($data['firstname'])){

$response['errorcode'] = "60001";

$response['status']= 0;

$response['message']="Full name:Required parameter missing";

}else if(empty($data['user_id'])){

$response['errorcode'] = "60002";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif (!empty($data)) {

DB::table('users')

->where('id', $user_id)

->update(['name' => trim($this->param['fname']),

'lname' => trim($this->param['lname']),

'user_address'=>  trim($data['address']),

'user_city'=>  trim($data['city']),

'user_zipcode'=>  trim($data['postcode'])

]);

	 /*,

	  'dummy1'=>trim($this->param['dummy1']),

	  'dummy2'=> trim($this->param['dummy2']),

	  'dummy3'=> trim($this->param['dummy3'])*/

	  $user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

	  $response['message'] = "Profile is successfully updated";

	  $response['status']= 1;

	  $response['data'] = array('user_detail'=>$user_data);

	}

	header('Content-type: application/json');

	echo json_encode($response);

}





// 30-8-2019



/************************************************************/

//	FUNCTION NAME : Device_token

//	FUNCTION USE :  Store device tokan and other info

//	FUNCTION ERRORCODE : 80000

/****************************************************/

public function device_token(Request $request){

	$data['userid'] = $user_id = Input::get("userid");

	$data['guest_id'] = $guest_id =Input::get("guest_id");

	$data['devicetoken'] = Input::get("devicetoken");

	$data['deviceid'] = Input::get("deviceid");

	$data['devicetype'] = Input::get("devicetype");

	$response = array();

//		if(empty($data['userid']))

	if(	(($user_id==0) || empty($user_id)) && (empty($guest_id)) )

	{

		$response['errorcode'] = "80001";

		$response['status']= 0;

		$response['message']="user id /Guest id :Required parameter missing";

	}elseif(empty($data['devicetoken'])){

		$response['errorcode'] = "80002";

		$response['status']= 0;

		$response['message']="device token:Required parameter missing";

	}else if(empty($data['deviceid'])){

		$response['errorcode'] = "80003";

		$response['status']= 0;

		$response['message']="device id:Required parameter missing";

	}else if(empty($data['devicetype'])){

		$response['errorcode'] = "80004";

		$response['status']= 0;

		$response['message']="device type:Required parameter missing";

	}elseif (!empty($data)){

/*$result = new Device_token;

$result->userid = trim(Input::get('userid'));

$result->devicetoken = trim(Input::get('devicetype'));

$result->deviceid = trim(Input::get('deviceid'));

$result->devicetype = trim(Input::get('devicetype'));

$result->save();*/

/*$user_detail = DB::table('device_token')

		->where('userid', '=' ,trim(Input::get('userid')))

		->where('deviceid', '=' ,trim(Input::get('deviceid')))

		->where('devicetype', '=' ,trim(Input::get('devicetype')))

		->get();*/

		if(Input::get('devicetype')=='android')

		{

			if(($user_id>0) &&(!empty($user_id)))

			{

				$user_detail = DB::table('device_token')

				->where('userid', '=' ,trim($user_id))

				//->where('deviceid', '=' ,trim(Input::get('deviceid')))

				//->where('devicetype', '=' ,trim(Input::get('devicetype')))

				->get();

			}

			elseif(!empty($guest_id))

			{

				$user_detail = DB::table('device_token')

				->where('guest_id', '=' ,trim($guest_id))

				//->where('deviceid', '=' ,trim(Input::get('deviceid')))

				//->where('devicetype', '=' ,trim(Input::get('devicetype')))

				->get();

			}

			if(count($user_detail)>0)

			{

				if(($user_id>0) &&(!empty($user_id)))

				{

					DB::table('device_token')

					->where('userid', '=' ,trim(Input::get('userid')))

					//->where('deviceid', '=' ,trim(Input::get('deviceid')))

					//->where('devicetype', '=' ,trim(Input::get('devicetype')))

			       ->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype'))]);





				}

				elseif(!empty($guest_id))

				{

					DB::table('device_token')

					->where('guest_id', '=' ,trim($guest_id))

					//->where('deviceid', '=' ,trim(Input::get('deviceid')))

					//->where('devicetype', '=' ,trim(Input::get('devicetype')))

					->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype'))]);

				}

				$response['message']="device Token Updated sucessfully!";

			}

			else

			{

				$result = new Device_token;

				$result->userid = trim($user_id);

				$result->guest_id = trim($guest_id);

				$result->devicetoken = trim(Input::get('devicetoken'));

				$result->notification_status = trim(1);

				$result->deviceid = trim(Input::get('deviceid'));

				$result->devicetype = trim(Input::get('devicetype'));

				$result->save();

				$response['message']="device Token sucessfully added!";

			}

			$response['status']= 1;

		}

		elseif(Input::get('devicetype')=='iphone')

		{

			if(($user_id>0) &&(!empty($user_id)))

			{

				$user_detail = DB::table('device_token')

				->where('userid', '=' ,trim($user_id))

				//->where('devicetype', '=' ,trim(Input::get('devicetype')))

				->get();

			}

			elseif(!empty($guest_id))

			{

				$user_detail = DB::table('device_token')

				->where('guest_id', '=' ,trim($guest_id))

				//->where('devicetype', '=' ,trim(Input::get('devicetype')))

				->get();

			}

			if(count($user_detail)>0)

			{

				if(($user_id>0) &&(!empty($user_id)))

				{

					DB::table('device_token')

					->where('userid', '=' ,trim(Input::get('userid')))

					//->where('devicetype', '=' ,trim(Input::get('devicetype')))

					->update(['devicetoken' => trim($data['devicetoken']),

						'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype'))]);

				}

				elseif(!empty($guest_id))

				{

					DB::table('device_token')

					->where('guest_id', '=' ,trim($guest_id))

					//->where('devicetype', '=' ,trim(Input::get('devicetype')))

					->update(['devicetoken' => trim($data['devicetoken']),

						'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype'))]);

				}

				$response['message']="device Token Updated sucessfully!";

			}

			else

			{

				$result = new Device_token;

				$result->userid = trim($user_id);

				$result->guest_id = trim($guest_id);

				$result->devicetoken = trim(Input::get('devicetoken'));

				$result->notification_status = trim(1);

				$result->deviceid = trim(Input::get('deviceid'));

				$result->devicetype = trim(Input::get('devicetype'));

				$result->save();

				$response['message']="device Token sucessfully added!";

			}

			$response['status']= 1;

		}

	}

	header('Content-type: application/json');

	echo json_encode($response);

}









public function viewpagecontent(){

	$home_content_api = DB::table('home_content')

	->select('*')

	->where('home_id', '=' , '5')

	->get();

	$response['message'] = "View home page content";

	$response['status']= 1;

	$response['data'] = array(

		'home_content'=>$home_content_api[0]->home_content

	);

	header('Content-type: application/json');

	echo json_encode($response);

}









// 31-8-2019

/************************************************************/

//	FUNCTION NAME : Make_favourite

//	FUNCTION USE :  app user favourite any restaurant

//	FUNCTION ERRORCODE : 210000

/****************************************************/

public function make_favourite()

{

	$data['user_id'] = $user_id =Input::get("userid");

	$data['rest_id'] = $rest_id = Input::get("rest_id");

	$response = array();

	if(empty($data['user_id'])){

		$response['errorcode'] = "210001";

		$response['status']= 0;

		$response['message']="User Id:Required parameter missing";

	}elseif(empty($data['rest_id'])){

		$response['message']="Restaurant id:Required parameter missing";

		$response['status']= 0;

		$response['errorcode'] = "210002";

	}elseif(!empty($data)) {

		$user_favourite = DB::table('favourite_restaurant')

		->where('favrest_restid', '=' ,$rest_id)

		->where('favrest_userid', '=' ,$user_id)

		->select('*')

		->orderBy('favrest_userid', 'asc')

		->get();

		if($user_favourite)

		{

			DB::table('favourite_restaurant')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->update(['favrest_status' => '1'

		]);

		}

		else

		{

			$fav_data = new Favourite_restaurant;

			$fav_data->favrest_restid = trim($rest_id);

			$fav_data->favrest_userid = trim($user_id);

			$fav_data->favrest_status = '1';

			$fav_data->save();

		}

//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

		$response['message'] = "Restaurant make as Favourite sucessfully.";

		$response['status']= 1;

//$response['data'] = $user_data;

	}

	header('Content-type: application/json');

	echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : Remove_favourite

//	FUNCTION USE :  app user favourite any restaurant

//	FUNCTION ERRORCODE : 220000

/****************************************************/

public function remove_favourite()

{

	$data['user_id'] = $user_id =Input::get("userid");

	$data['rest_id'] = $rest_id = Input::get("rest_id");

	$response = array();

	if(empty($data['user_id'])){

		$response['errorcode'] = "220001";

		$response['status']= 0;

		$response['message']="User Id:Required parameter missing";

	}elseif(empty($data['rest_id'])){

		$response['message']="Restaurant id:Required parameter missing";

		$response['status']= 0;

		$response['errorcode'] = "220002";

	}elseif(!empty($data)) {

		$user_favourite = DB::table('favourite_restaurant')

		->where('favrest_restid', '=' ,$rest_id)

		->where('favrest_userid', '=' ,$user_id)

		->select('*')

		->orderBy('favrest_userid', 'asc')

		->get();

		if($user_favourite)

		{

			DB::table('favourite_restaurant')

			->where('favrest_restid', '=' ,$rest_id)

			->where('favrest_userid', '=' ,$user_id)

			->update(['favrest_status' => '0'

		]);

		}

//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

		$response['message'] = "Restaurant remove as favourite sucessfully.";

		$response['status']= 1;

//$response['data'] = $user_data;

	}

	header('Content-type: application/json');

	echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Restaurant_review

//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

//	FUNCTION ERRORCODE : 120000

/****************************************************/

public function restaurant_review_listing()

{

	$data['restaurentid'] = $rest_id = Input::get("restaurentid");



	if(empty($data['restaurentid'])){

		$response['errorcode'] = "120001";

		$response['status']= 0;

		$response['message']="Restaurant Id:Required parameter missing";

	}elseif (!empty($data)){



		$rest_listing = DB::table('search_restaurant_view');

		$rest_listing = $rest_listing->select('*');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

		$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

		$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

		$rest_listing = $rest_listing->get();

		if($rest_listing){



			/* COUNT CALCULATION OF RATING  FOR REVIEW /FOOD/ ETC */

			$rest_food_good_rat ='0';

			$rest_delivery_ontime_rat ='0';

			



			$rest_food_good ='0';

			$rest_delivery_ontime ='0';

			$rest_order_accurate ='0';



			$count_review = DB::table('review')

			->where('re_restid', '=' ,$rest_id)

			->where('re_status', '=' ,'PUBLISHED')

			->orderBy('re_id', 'desc')

			->count();



			if($count_review>0)

			{



		   /* $sql_food = DB::table('review')

			 			->select(DB::raw("round((count(`re_food_good`) / ".$count_review."),0) as  food_good"))

						->where('re_restid', '=' , $rest_id)

						->where('re_food_good', '=' , '1')

						->get();



			 

			$sql_delivery_ontime = DB::table('review')

			 			->select(DB::raw("round((count(`re_delivery_ontime`) / ".$count_review."),0) as  delivery_ontime"))

						->where('re_restid', '=' , $rest_id)

						->where('re_delivery_ontime', '=' , '1')

						->get();



			 

			$sql_rder_accurate = DB::table('review')

			 			->select(DB::raw("round((count(`re_order_accurate`) / ".$count_review."),0) as  order_accurate"))

						->where('re_restid', '=' , $rest_id)

						->where('re_order_accurate', '=' , '1')

						->get(); */



			    $re_food_good  = DB::table('review')->where('re_restid', '=' ,$rest_id)->where('re_status', '=' , 'PUBLISHED')->avg('re_food_good');	



				$rest_food_good_rat = round($re_food_good);



				$re_delivery_ontime  = DB::table('review')->where('re_restid', '=' ,$rest_id)->where('re_status', '=' , 'PUBLISHED')->avg('re_delivery_ontime');



				$rest_delivery_ontime_rat = round($re_delivery_ontime);



				$re_rating  = DB::table('review')->where('re_restid', '=' ,$rest_id)->where('re_status', '=' , 'PUBLISHED')->avg('re_rating');





			    $rest_food_good = $re_food_good*20;



				$rest_delivery_ontime = $re_delivery_ontime*20;



				$rest_order_accurate = $re_rating*20;



			}

			/* END */

			/*RESTAURNT DETAILDATA ARRAY */

			$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

			$rest_data["vendor_id"] = trim($rest_listing[0]->vendor_id);

			$rest_data["rest_name"] = trim($rest_listing[0]->rest_name);

			$rest_data["rest_metatag"] = trim($rest_listing[0]->rest_metatag);

			$rest_data["rest_logo"] = trim(url('/').'/uploads/reataurant/'.$rest_listing[0]->rest_logo);

			$rest_data["rest_address"] = trim($rest_listing[0]->rest_address);

			$rest_data["rest_address2"] = trim($rest_listing[0]->rest_address2);

			$rest_data["rest_city"] = trim($rest_listing[0]->rest_city);

			$rest_data["rest_zip_code"] = trim($rest_listing[0]->rest_zip_code);

			$rest_data["rest_suburb"] = trim($rest_listing[0]->rest_suburb);

			$rest_data["rest_state"] = trim($rest_listing[0]->rest_state);

			$rest_data["rest_desc"] = trim($rest_listing[0]->rest_desc);

			$rest_data["rest_contact"] = trim($rest_listing[0]->rest_contact);

			$rest_data["rest_service"] = trim($rest_listing[0]->rest_service);

			$rest_data["rest_create"] = trim($rest_listing[0]->rest_create);

			$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

			$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

			$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

			$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

			$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

			$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

			$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

			$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

			$rest_data["rest_cuisine"] = trim($rest_listing[0]->rest_cuisine);

			$rest_data["rest_mindelivery"] = trim($rest_listing[0]->rest_mindelivery);

			$rest_data["rest_landline"] = trim($rest_listing[0]->rest_landline);

			$rest_data["rest_commission"] = trim($rest_listing[0]->rest_commission);

			$rest_data["rest_email"] = trim($rest_listing[0]->rest_email);

			$rest_data["rest_status"] = trim($rest_listing[0]->rest_status);

			$rest_data["rest_classi"] = trim($rest_listing[0]->rest_classi);

			$rest_data["rest_special"] = trim($rest_listing[0]->rest_special);

			$rest_data["rest_delivery_from"] = trim($rest_listing[0]->rest_delivery_from);

			$rest_data["rest_delivery_to"] = trim($rest_listing[0]->rest_delivery_to);

			$rest_data["rest_holiday_from"] = trim($rest_listing[0]->rest_holiday_from);

			$rest_data["rest_holiday_to"] = trim($rest_listing[0]->rest_holiday_to);

			$rest_data["rest_openstatus"] = trim($rest_listing[0]->rest_openstatus);

			$rest_data["rest_banner"] = trim(url('/').'/uploads/reataurant/banner/'.$rest_listing[0]->rest_banner);

			$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

			$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

			$rest_data["created_at"] = trim($rest_listing[0]->created_at);

			$rest_data["updated_at"] = trim($rest_listing[0]->updated_at);

			$totalrating = 0;

			if(!empty($rest_listing[0]->totalrating)){$totalrating = $rest_listing[0]->totalrating; }

			$rest_data["totalrating"] = trim($totalrating);

			$rest_data["totalreview"] = trim($rest_listing[0]->totalreview);

			$rest_data["cuisine_name"] = trim($rest_listing[0]->cuisine_name);

			$rest_data["rest_price_level"] = trim($rest_listing[0]->rest_price_level);

			$rest_data["rest_servicetax"] = trim($rest_listing[0]->rest_servicetax);

			$rest_data["rest_cash_deliver"] = trim($rest_listing[0]->rest_cash_deliver);

			$rest_data["rest_partial_pay"] = trim($rest_listing[0]->rest_partial_pay);

			$rest_partial_percent = 0;

			if($rest_listing[0]->rest_partial_pay==1)

			{

				$rest_partial_percent = $rest_listing[0]->rest_partial_percent;

			}

			$rest_data["rest_partial_percent"] = trim($rest_partial_percent);

			$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);



			$rest_data["total_food_rating"] = trim('');

			$rest_data["total_delivery_rating"] = trim('');



			$rest_data["rest_food_good"] = trim('');

			$rest_data["rest_on_time"] = trim('');

			$rest_data["rest_order_accurate"] = trim('');



			if($rest_food_good_rat>0){

				$rest_data["total_food_rating"] = trim($rest_food_good_rat);

			}

			if($rest_delivery_ontime_rat>0){

				$rest_data["total_delivery_rating"] = trim($rest_delivery_ontime_rat);

			}



			$lang_id = Input::get('lang_id');



			if ($lang_id == 'ar') {

				$food_was_good = "كان الطعام جيد";

				$delivery_was_on_time = "كان ٪ التسليم في الوقت المحدد";

				$restaurant_was_good = "مطعم كان جيدا";

			} else {

				$food_was_good = "% Food was good";

				$delivery_was_on_time = "% Delivery was on time";

				$restaurant_was_good = "% Restaurant was good";

			}



			if($rest_food_good>0){	

				$rest_data["rest_food_good"] = trim($rest_food_good.$food_was_good);

			}

			if($rest_delivery_ontime>0){	

				$rest_data["rest_on_time"] = trim($rest_delivery_ontime.$delivery_was_on_time);

			}



			 if($rest_order_accurate>0){	

				$rest_data["rest_order_accurate"] = trim($rest_order_accurate .$restaurant_was_good);

			} 





			$rest_data["rest_min_orderamt"] = trim($rest_listing[0]->rest_min_orderamt);

			/* END **/

			$limit = 10;

			if((Input::get('per_page')))

			{

				$limit = Input::get('per_page');

			}

			$review_data = DB::table('review')

			->Join('users', 'review.re_userid', '=', 'users.id')

			->select('review.*','users.name','users.lname')

			->where('review.re_restid', '=' , $rest_id)

			->where('review.re_status', '=' , 'PUBLISHED' )

			->orderBy('re_id', 'desc')

			->paginate($limit);

			if($review_data)

			{

				$review_data_list ='';

				foreach($review_data as $listing)

				{

					$review_data_list[]=array(

						're_id' => $listing->re_id,

						're_orderid' => $listing->re_orderid,

						're_userid' => $listing->re_userid,

						're_restid' => $listing->re_restid,

						're_content' => $listing->re_content,

						're_status' => $listing->re_status,

						're_rejectreson' => $listing->re_rejectreson,

						're_rating' => $listing->re_rating,

						'created_at' =>date('d-m-Y',strtotime($listing->created_at)),

						'updated_at' =>date('d-m-Y',strtotime($listing->updated_at)),

						'name' => $listing->name,

						'lname' => $listing->lname

					);

				}

				if($review_data->total()==0)

				{

					$review_data_list =array();

				}

				$review_list['total'] =$review_data->total() ;

				$review_list['per_page'] =$review_data->perPage();

				$review_list['current_page'] =$review_data->currentPage();;

				$review_list['last_page'] =$review_data->lastPage();

				$review_list['data'] =$review_data_list;

				$review_list_counter = $review_data->total();

			}

			else

			{

				$review_list = array();

				$review_list_counter =0;

			}

			$response['message'] = "Restaurant Detail";

			$response['status']= 1;

			$response['restaurant_detail'] = $rest_data;

			$response['review_list'] = $review_list;

			$response['review_list_count'] = $review_list_counter;

			}

			else

			{

				$response['errorcode'] = "120002";

				$response['status']= 0;

				$response['message']="Restaurant Id: Invalid Restaurant ID";

			}

			}

			header('Content-type: application/json');

			echo json_encode($response);

			}



/************************************************************/

//	FUNCTION NAME : Food_autosuggest

//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

//	FUNCTION ERRORCODE : 180000

/****************************************************/



public function food_rest_search()

{

DB::enableQueryLog();

$test_sql = '';

$keyword = Input::get('search_by');

$lat ='';

$long = '';

$rest_listing = DB::table('food_rest_name_view');

if(!empty($keyword))

{

$rest_listing = $rest_listing->where('food_rest_name_view.rest_food', 'like' , '%'.$keyword.'%');

}

$rest_listing = $rest_listing->orderBy('food_rest_name_view.rest_food', 'asc');

$rest_listing = $rest_listing->groupBy('food_rest_name_view.rest_food');

$test_sql  = $rest_listing;

$rest_listing = $rest_listing->get();

// dd(DB::getQueryLog());

$total_restaurant =count($rest_listing) ;

$rest_data = '';

$open_staus = 0 ;

if($total_restaurant>0)

{

$response['message'] = "All restaurant list";

$response['status']= 1;

$response['search_by'] = $keyword;

$response['food_listing'] = $rest_listing;

}

else

{

$response['message']="No result found!";

$response['status']= 0;

$response['errorcode'] = "180001";

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : food_search

//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

//	FUNCTION ERRORCODE : 190000

/****************************************************/

public function food_search()

{

DB::enableQueryLog();

$test_sql = '';

$rest_id = Input::get('restaurentid');

$keyword = Input::get('search_by');

$lat ='';

$long = '';

$total_rec_counter=0;

$food_detail ='';

$menu_list = DB::table('menu_detail');

$menu_list = $menu_list->where('menu_detail.restaurant_id', '=' , $rest_id);

$menu_list = $menu_list->where('menu_detail.menu_status', '=' , '1');

if(!empty($keyword))

{

$menu_list = $menu_list->where('menu_detail.menu_name', 'like' , '%'.$keyword.'%');

}

$menu_list = $menu_list->groupBy('menu_detail.menu_id');

$test_sql  = $menu_list;

$menu_list = $menu_list->get();

$total_restaurant =count($menu_list) ;

if(count($menu_list)>0)

{

$total_rec_counter++;

foreach($menu_list as $mlist)

{

$menu_id = $mlist->menu_id;

$item_listing = DB::table('menu_category');

$item_listing = $item_listing->where('menu_category.rest_id', '=' , $rest_id);

$item_listing = $item_listing->where('menu_category.menu_id', '=' , $menu_id);

$item_listing = $item_listing->where('menu_category.menu_cat_status', '=' , '1');

$item_listing = $item_listing->groupBy('menu_category.menu_category_id');

$test_sql  = $item_listing;

$item_listing = $item_listing->get();

if(count($item_listing)>0)

{

foreach($item_listing as $ilist)

{

$price = '0.00';

if($ilist->menu_category_portion=='no')

{

	$price = $ilist->menu_category_price;

}

$food_detail[]=array('foodRestId'=>$ilist->rest_id,

	'foodMenuId'=>$ilist->menu_id,

	'foodId'=>$ilist->menu_category_id,

	'foodName'=>$ilist->menu_category_name,

	'foodPrice'=>$price

);

}

}

}

}

elseif(count($menu_list)==0)

{

$item_listing = DB::table('menu_category');

$item_listing = $item_listing->where('menu_category.rest_id', '=' , $rest_id);

if(!empty($keyword))

{

$item_listing = $item_listing->where('menu_category.menu_category_name', 'like' , '%'.$keyword.'%');

}

$item_listing = $item_listing->where('menu_category.menu_cat_status', '=' , '1');

$item_listing = $item_listing->groupBy('menu_category.menu_category_id');

$test_sql  = $item_listing;

$item_listing = $item_listing->get();

if(count($item_listing)>0)

{

$total_rec_counter++;

foreach($item_listing as $ilist)

{

$price = '0.00';

if($ilist->menu_category_portion=='no')

{

$price = $ilist->menu_category_price;

}

$food_detail[]=array('foodRestId'=>$ilist->rest_id,

'foodMenuId'=>$ilist->menu_id,

'foodId'=>$ilist->menu_category_id,

'foodName'=>$ilist->menu_category_name,

'foodPrice'=>$price

);

}

}

}

// dd(DB::getQueryLog());

$rest_data = '';

$open_staus = 0 ;

if(empty($food_detail)){

$food_detail = array();

}

if($total_rec_counter>0)

{

$response['message'] = "All restaurant list";

$response['status']= 1;

$response['search_by'] = $keyword;

$response['rest_id'] = $rest_id;

$response['food_listing'] = $food_detail;

}

else

{

$response['message']="No result found!";

$response['status']= 0;

$response['errorcode'] = "190001";

$response['food_listing'] = $food_detail;

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Change_password

//	FUNCTION USE :  Update app user password

//	FUNCTION ERRORCODE : 90000

/****************************************************/

public function change_password()

{

$user_id = Input::post('userid');

$data['user_id'] = Input::post("userid");

$data['newpassword'] = Input::post("newpassword");

$data['oldpassword'] = Input::post("oldpassword");

$response = array();

if(empty($data['oldpassword'])){

$response['errorcode'] = "50001";

$response['status']= 0;

$response['message']="Old password:Required parameter missing";

}elseif(empty($data['newpassword'])){

$response['message']="New password:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "50002";

}elseif(empty($data['user_id'])){

$response['message']="user id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "50003";

}elseif(!empty($data)) {

$credentials = ['password' => trim(Input::get('oldpassword')),'id' =>  $user_id];

if( Auth::attempt($credentials))

{

DB::table('users')

->where('id', $user_id)

->update(['password' =>  Hash::make(trim(Input::get('newpassword'))),

'user_pass'=> md5(trim(Input::get('newpassword')))

]);

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "Password updated sucessfully.";

$response['status']= 1;

$response['data'] = $user_data;

}

else

{

$response['message']="Old Password Not Match";

$response['status']= 0;

$response['errorcode'] = "50004";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Favourite_list

//	FUNCTION USE :  app user favourite any restaurant

//	FUNCTION ERRORCODE : 230000

/****************************************************/

public function favourite_list()

{

$data['user_id'] = $user_id =Input::get("userid");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "230001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)) {

$limit = 10;

/*if((Input::get('per_page')))

{

$limit = Input::get('per_page');

}*/

$user_favourite = DB::table('favourite_restaurant')

->leftJoin('search_restaurant_view', 'favourite_restaurant.favrest_restid', '=', 'search_restaurant_view.rest_id')

->where('favrest_userid', '=' ,$user_id)

->where('favrest_status', '=' ,'1')

->select('*')

->orderBy('favrest_userid', 'asc')

->paginate($limit)	;

$user_favourite_counter = $user_favourite->total();

			//->get();

if($user_favourite_counter>0)

{

$rest_data ='';

foreach($user_favourite as $listing)

{

$rating = 0;

if($listing->totalrating=='NULL')

{

	$rating = 0;

}

else

{

	$rating = round($listing->totalrating);

}

$open_staus = 1 ;// Calcualtion reamning

$dist_km = '';

if(@$listing->distance)

{

	$dist_km = $listing->distance;

}

/** FAVOURIT OPTION **/

$rest_favourite= 1;

/**  END   ***/

$cuisine_name = '';

if(!empty($listing->cuisine_name))

{

	$cuisine_name =	$listing->cuisine_name;

}

$rest_data[] = array(

	'rest_id' => $listing->rest_id,

	'rest_name' => $listing->rest_name,

	'rest_metatag' => $listing->rest_metatag,

	'rest_logo' => trim(url('/').'/uploads/reataurant/'.$listing->rest_logo),

	'rest_address' => $listing->rest_address,

	'rest_address2' => $listing->rest_address2,

	'rest_city' => $listing->rest_city,

	'rest_zip_code' => $listing->rest_zip_code,

	'rest_suburb' => $listing->rest_suburb,

	'rest_state' => $listing->rest_state,

	'rest_service'=>$listing->rest_service,

	'rest_mindelivery'=>$listing->rest_mindelivery,

	'rest_status'=>$listing->rest_status,

	'rest_email'=>$listing->rest_email,

	'cuisine_name'=>$cuisine_name,

	'total_review'=>$listing->totalreview,

	'total_rating'=>$rating,

	'distance'=>$dist_km,

	'open_status'=>$open_staus,

	'rest_favourite'=>$rest_favourite,

	'rest_min_orderamt' => trim($listing->rest_min_orderamt)

);

}

$rest_detail_format['total'] =$user_favourite->total() ;

$rest_detail_format['per_page'] =$user_favourite->perPage();

$rest_detail_format['current_page'] =$user_favourite->currentPage();;

$rest_detail_format['last_page'] =$user_favourite->lastPage();

$rest_detail_format['data'] =$rest_data;

$response['message'] = "Restaurant List.";

$response['status']= 1;

$response['restaurant_list'] =  $rest_detail_format;

}

else

{

$response['message']="No favourite restaurant list found ";

$response['status']= 0;

$response['errorcode'] = "230003";

}

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : Add_order_review

//	FUNCTION USE :  app user favourite any restaurant

//	FUNCTION ERRORCODE : 240000

/****************************************************/

public function post_order_review()

{

$data['user_id'] = $user_id =Input::get("userid");

$data['order_id'] = $order_id =Input::get("order_id");

$data['review_text'] = $review_text =Input::get("review_text");

$data['rest_id'] = $rest_id =Input::get("rest_id");

$data['rating'] = $rating =Input::get("rating");

$rate_food = Input::get("rate_food");

$rate_delviery =Input::get("rate_delivery");

$rate_order = 1;





$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "240001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(empty($data['order_id'])){

$response['errorcode'] = "240002";

$response['status']= 0;

$response['message']="Order Id:Required parameter missing";

}elseif(!empty($data)) {



$order_detail  =  DB::table('order')

->where('user_id', '=' ,$user_id)

->where('order_id', '=' ,$order_id)

->where('order_status', '=' ,'11')

->get();



if(count($order_detail )>0)

{



$rest_id = $order_detail[0]->rest_id;



$review_counter  =  DB::table('review')

->where('re_userid', '=' ,$user_id)

->where('re_orderid', '=' ,$order_id)

->get();



if(count($review_counter)>0){

	$response['errorcode'] = "240006";

	$response['status']= 0;

	$response['message']="You post review on this order!";

}

else

{

	$order_re = new Review;

	$order_re->re_orderid = $order_id;

	$order_re->re_userid = $user_id;

	$order_re->re_content = $review_text ;

	$order_re->re_restid = $rest_id;

	$order_re->re_rating = $rating;

	$order_re->re_food_good = $rate_food;

	$order_re->re_delivery_ontime = $rate_delviery;

	$order_re->re_order_accurate = $rate_order;

	$order_re->re_status = 'SUBMIT';

	$order_re->save();

	$re_id = $order_re->re_id;

	$response['message'] = "Your Review Submited!";

	$response['status']= 1;

}

}

else

{

$response['errorcode'] = "240005";

$response['status']= 0;

$response['message']="Your order is not completed!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : Show_order_listing

//	FUNCTION USE :  app user favourite any restaurant

//	FUNCTION ERRORCODE : 250000

/****************************************************/

public function show_order_listing()

{

$data['user_id'] = $user_id =Input::get("userid");

$data['lang_id'] = $lang_id =Input::get("lang_id");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)) {

$limit = 10;

/*if((Input::get('per_page')))

{

$limit = Input::get('per_page');

}*/

$order_detail  =  DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('user_id', '=' ,$user_id)

->where('order.order_status', '!=' ,'2')

->select('*')

->orderBy('order.order_id', 'desc')

->groupBy('order.order_id')

->paginate($limit)	;

$order_counter = $order_detail->total();



if(count($order_detail )>0)

{

$order_detail_list = '';

foreach($order_detail as $order_list)

{

$logo = '';

if(!empty($order_list->rest_logo)){

	$logo =  url('/').'/uploads/reataurant/'.$order_list->rest_logo;

}

else

{

	$logo =  url('/').'design/front/img/logo.png';

}

$order_menu = '';

$cart_price = 0;

$order_deliveryfee	= '0.00';

if($order_list->order_type=='Delivery')

{

	$order_deliveryfee	= number_format($order_list->order_deliveryfee,2);

}

$Food_name='';

if(isset($order_list->order_carditem) && (count($order_list->order_carditem)))

{

	$order_carditem = json_decode($order_list->order_carditem, true);

	foreach($order_carditem as $item)

	{

		$Food_name .= $item['name'].'<br>';

		$cart_price = $cart_price+$item['price'];

		$order_menu .= $item['qty'].' x '.$item['name'].', ';

		if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

		{

			foreach($item['options']['addon_data'] as $addon)

			{

				$Food_name .=$addon['name'].'<br>';

				$cart_price = $cart_price+$addon['price'];

				$order_menu .= $addon['name'];

			}

		}

	}

}

//$total_amt = number_format(($cart_price+$order_deliveryfee),2);

$total_amt = number_format($order_list->order_total,2);

$order_status_text='';



if($order_list->order_status=='1') { $order_status_text='Order Pending';}

if($order_list->order_status=='4') { $order_status_text='Confirmed Orders'; }

if($order_list->order_status=='5') { $order_status_text='Ready Orders'; }

if($order_list->order_status=='6') { $order_status_text='Cancelled Orders'; }

if($order_list->order_status=='7') { $order_status_text='Ready Orders'; }

if($order_list->order_status=='8') { $order_status_text='Ready Orders'; }

if($order_list->order_status=='9') { $order_status_text='Picked Orders'; }

if($order_list->order_status=='10') { $order_status_text='On The Way Orders'; }

if($order_list->order_status=='11') { $order_status_text='Delivered Orders'; }



$review_post=0;

if(!empty($order_list->re_id))

{

	$review_post=1;

}

if ($lang_id == 'ar') {

	$rest_name = $order_list->rest_name_ar;

} else {

	$rest_name = $order_list->rest_name;

}

$order_detail_list[]= array(

	'order_id'=>$order_list->order_id,

	'order_uniqueid'=>$order_list->order_uniqueid,

	'user_id'=>$user_id,

	'rest_id'=>$order_list->rest_id,

	'rest_name'=>$rest_name,

	'food_name'=>$Food_name,

	'rest_logo'=>$logo ,

	'order_date'=>date('d-m-Y',strtotime($order_list->order_create)),

	'order_type'=>$order_list->order_type,

	'order_status'=>$order_list->order_status,

	'order_status_text'=>$order_status_text,

	'order_deliveryfee'=>$order_deliveryfee,

	'order_subtotal'=> number_format($cart_price,2),

	'order_total'=> $total_amt,

	'review_flag'=>$review_post

);

}

$order_detail_format['total'] =$order_detail->total() ;

$order_detail_format['per_page'] =$order_detail->perPage();

$order_detail_format['current_page'] =$order_detail->currentPage();;

$order_detail_format['last_page'] =$order_detail->lastPage();

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : Food_removecart

//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

//	FUNCTION ERRORCODE : 260000

/****************************************************/

public function food_removecart()

{

$data['user_id'] = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = Input::get("rest_id");

$data['menu_id'] = Input::get("menu_id");

$data['menu_catid'] = Input::get("menu_catid");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

if(empty($data['rest_id'])){

$response['errorcode'] = "260001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "260002";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "260003";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(!empty($data)){

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();

if($cart_listing)

{

DB::table('temp_cart')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('rest_id', '=' ,trim($this->param['rest_id']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->delete();

$response['message'] = "Cart  Deleted.";

$response['status']= 1;

}

else

{

$response['errorcode'] = "26000";

$response['status']= 0;

$response['message']="Invalide User Id OR Device Id";

}

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : update_notification_status

//	FUNCTION USE :  USER NOTIFICATION STAUTS

//	FUNCTION ERRORCODE : 270000

/****************************************************/

public function update_notification_status()

{



DB::enableQueryLog();



$data['userid'] = $user_id =Input::get("userid");

$data['status'] = $ustatus =Input::get("status");





if( ($user_id==0) || empty($user_id) )

{

$response['errorcode'] = "270001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(($data['status']=='') || ($data['status']<0)){

$response['errorcode'] = "270002";

$response['status']= 0;

$response['message']="Notofication Status:Required parameter missing";

}elseif(!empty($data)){



$tablename = "vendor_device_token" ;



DB::table($tablename)

->where('userid', $user_id)

->update(['notification_status' =>$ustatus,]);



$device_data = DB::table($tablename)

 ->where('userid', '=', trim($user_id))

 ->get();



if(!empty($device_data))

{



$response['message'] = "Notification status successfully updated.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$device_data);

}

else

{

$response['errorcode'] = "270005";

$response['status']= 0;

$response['message']="This user not registered";

}

}

  header('Content-type: application/json');

  echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : Make_food_favourite

//	FUNCTION USE :  app user favourite any restaurant food

//	FUNCTION ERRORCODE : 280000

/****************************************************/

public function make_food_favourite()

{

$data['user_id'] = $user_id =Input::get("userid");

$data['rest_id'] = $rest_id = Input::get("rest_id");

$data['food_id'] = $food_id = Input::get("food_id");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "280001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(empty($data['rest_id'])){

$response['message']="Restaurant id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "280002";

}elseif(empty($data['rest_id'])){

$response['message']="Food id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "280003";

}elseif(!empty($data)) {

$user_favourite = DB::table('favourite_food')

->where('favrest_restid', '=' ,$rest_id)

->where('favrest_userid', '=' ,$user_id)

->where('favrest_menucatid', '=' ,$food_id)

->select('*')

->orderBy('favrest_userid', 'asc')

->get();

if($user_favourite)

{

DB::table('favourite_food')

->where('favrest_restid', '=' ,$rest_id)

->where('favrest_userid', '=' ,$user_id)

->where('favrest_menucatid', '=' ,$food_id)

->update(['favrest_status' => '1'

]);

}

else

{

$fav_data = new Favourite_food;

$fav_data->favrest_restid = trim($rest_id);

$fav_data->favrest_userid = trim($user_id);

$fav_data->favrest_menucatid = trim($food_id);

$fav_data->favrest_status = '1';

$fav_data->save();

}

//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "Restaurant food make as Favourite sucessfully.";

$response['status']= 1;

//$response['data'] = $user_data;

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : show_contentpage_link

//	FUNCTION USE :  Show all content pages

//	FUNCTION ERRORCODE : 300000

/****************************************************/

public function show_contentpage_link()

{


$response['message'] = "check all pages link";

$response['page_data'] =array(

'aboutus'=>url('/').'/show_about',

'contactus'=>url('/').'/show_contact',

'termsconditions'=>url('/').'/show_terms_condition',

'privacypolicy'=>url('/').'/show_privacy_policy',

'help'=>url('/').'/show_help_page',

'faq'=>url('/').'/show_faq_page',

);



$response['status']= 1;

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : food_add_removecart

//	FUNCTION USE :  Remove all old restaurnt item and insert new

//	FUNCTION ERRORCODE : 310000

/****************************************************/

public function food_add_removecart()

{

$data['user_id'] =$rest_id = Input::get("user_id");

$data['guest_id'] =$rest_id = Input::get("guest_id");

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['menu_id'] =$rest_id = Input::get("menu_id");

$data['menu_catid'] =$rest_id = Input::get("food_id");

$data['menu_subcatid'] =$rest_id = Input::get("food_size_id");

$data['menu_addonid'] =$rest_id = Input::get("food_addonid");

$data['qty'] =$rest_id = Input::get("qty");

$data['deviceid'] =$rest_id = Input::get("deviceid");

$data['devicetype'] =$rest_id = Input::get("devicetype");

if(empty($data['rest_id'])){

$response['errorcode'] = "310001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['menu_id'])){

$response['errorcode'] = "310002";

$response['status']= 0;

$response['message']="Menu Id:Required parameter missing";

}elseif(empty($data['menu_catid'])){

$response['errorcode'] = "310003";

$response['status']= 0;

$response['message']="Food Id:Required parameter missing";

}elseif(empty($data['qty'])){

$response['errorcode'] = "310004";

$response['status']= 0;

$response['message']="Food Quantity:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "310005";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "310006";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(!empty($data)){

DB::table('temp_cart')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('rest_id', '!=' ,trim($this->param['rest_id']))

->delete();

$rest_cartid = '';

$rest_cartid = (date('ymdHis'));

$cart_data = new Temp_cart;

$cart_data->user_id	 = trim($this->param['user_id']);

$cart_data->rest_cartid	 = trim($rest_cartid);

$cart_data->guest_id = trim($this->param['guest_id']);

$cart_data->rest_id = trim($this->param['rest_id']);

$cart_data->menu_id = trim($this->param['menu_id']);

$cart_data->menu_catid = trim($this->param['food_id']);

$cart_data->menu_subcatid = trim($this->param['food_size_id']);

$cart_data->menu_addonid = trim($this->param['food_addonid']);

$cart_data->qty = trim($this->param['qty']);

$cart_data->deviceid = trim($this->param['deviceid']);

$cart_data->devicetype= trim($this->param['devicetype']);

$cart_data->save();

$response['message'] = "Item add in cart sucessfully";

$response['status']= 1;

$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : show_soical_login

//	FUNCTION USE :  Remove all old restaurnt item and insert new

//	FUNCTION ERRORCODE : 320000

/****************************************************/

public function show_soical_login()

{

$data['emailid'] = '';

$data['mobileno'] = '';

$data['socialtype'] = '';

$data['deviceid'] = '';

$data['devicetype'] = '';

if(isset($this->param["emailid"]))

{

$data['emailid'] = $this->param["emailid"];

}

if(isset($this->param["mobileno"]))

{

$data['mobileno'] =  $this->param["mobileno"];

}

$response = array();

if( (empty($data['emailid'])) && (empty($data['mobileno'])))

{

$response['errorcode'] = "320001";

$response['status']= 0;

$response['message']="Mobile Number or Email Id  Required parameter ";

}elseif (!empty($data)) {

$user_detail_counter = 0;

$user_detail_email_counter = 0;

if(!empty($this->param['mobileno']))

{

$user_detail_counter = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->count();

}

if(!empty($this->param['emailid']))

{

$user_detail_email_counter = DB::table('users')->where('email', '=' ,$this->param['emailid'])->count();

}

$response='';

if(($user_detail_counter>0) && (!empty($this->param['mobileno']))){

$user_detail  = DB::table('users')->where('user_mob', '=' ,$this->param['mobileno'])->get();

$user_id = $user_detail[0]->id;

if($user_detail[0]->user_status==1){

$response['message']='Sign In: Success';

$response['data'] = $user_detail;

$response['status']= 1;

$response['Status'] = 1;

}

else{

$otp_code = trim($this->generateRandomString(6));

$user_id = $user_detail[0]->id;

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($user_detail[0]->user_mob);

			$otp_data->otp_number =  $otp_code; //trim('123456');

			$otp_data->save();

			/*********** EMAIL FOR SEN OTP START *************/

			$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',22)->first();

			

			$full_name =  $user_detail[0]->user_fname.' '.$user_detail[0]->user_lname;

			$msg_reg_otp = 'Congrats, '.trim($full_name).$usr_fgtpss_email_cotnt->email_content.$otp_code;

// 			Mail::raw($msg_reg_otp, function ($message){

// //$message->to('contact@contact.com');

// 				$message->from('info@grambunny.com', 'grambunny');

// 				$otp_email = trim($user_detail[0]->user_mob).$carrier_email;

// 				$message->to($otp_email);

// 				$message->bcc('votiveshweta@gmail.com');

// 				$message->bcc('votivemobile.pankaj@gmail.com');

// 				$message->bcc('votivemobile.dilip@gmail.com');

// 				$message->bcc('votiveiphone.hariom@gmail.com');

// 				$message->bcc('zubaer.votive@gmail.com');

// 				$message->subject('Registration OTP');

// 			});

			/********** EMAIL FOR SEN OTP END  *************/

			

			/*********** EMAIL FOR SEN OTP START *********/

			$otp_email = trim($user_detail[0]->user_mob).$carrier_email;

			$address = $otp_email;

			$message = $msg_reg_otp;

			$subject = $usr_fgtpss_email_cotnt->email_title;

			$semail = $this->sendMail($address,$subject,$message);

			/********** EMAIL FOR SEND OTP END  **********/



			$otp_inserted_id = $otp_data->otp_id;

			$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

			$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

			$response['errorcode'] = "320002";

			$response['status']= 2;

			$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

			$response['message'] ='Sorry.Your acoount is not active. Please verify your mobile number via OTP.';

		}

	}elseif(($user_detail_email_counter>0) && (!empty($this->param['emailid']))){

		$user_detail_email  = DB::table('users')->where('email', '=' ,$this->param['emailid'])->get();

		$user_id = $user_detail_email[0]->id;

		if($user_detail_email[0]->user_status==1){

			$response['message']='Sign In: Success';

			$response['data'] = $user_detail_email;

			$response['status']= 1;

			$response['Status'] = 1;

		}

		else{

			$response['errorcode'] = "320003";

			$response['status']= 2;

			if(!empty($user_detail_email[0]->user_mob))

			{

				$otp_code = trim($this->generateRandomString(6));

				$user_id = $user_detail_email[0]->id;

				$otp_data = new User_otpdetail;

				$otp_data->user_id = trim($user_id);

				$otp_data->user_mob = trim($user_detail_email[0]->user_mob);

			$otp_data->otp_number =  $otp_code; //trim('123456');

			$otp_data->save();

			/*********** EMAIL FOR SEN OTP START *************/

			$usr_fgtpss_email_cotnt = DB::table('email_content')->where('email_id',22)->first();



			$full_name =  $user_detail_email[0]->user_fname.' '.$user_detail_email[0]->user_lname;

			$msg_reg_otp = 'Congrats, '.trim($full_name).$usr_fgtpss_email_cotnt->email_content.$otp_code;

			// Mail::raw($msg_reg_otp, function ($message){

			// //$message->to('contact@contact.com');

			// 	$message->from('info@grambunny.com', 'grambunny');

			// 	$otp_email = trim($user_detail_email[0]->user_mob).$carrier_email;

			// 	$message->to($otp_email);

			// 	$message->bcc('votiveshweta@gmail.com');

			// 	$message->bcc('votivemobile.pankaj@gmail.com');

			// 	$message->bcc('votivemobile.dilip@gmail.com');

			// 	$message->bcc('votiveiphone.hariom@gmail.com');

			// 	$message->bcc('zubaer.votive@gmail.com');

			// 	$message->subject('Registration OTP');

			// });

			/********** EMAIL FOR SEN OTP END  *************/



			/*********** EMAIL FOR SEN OTP START *********/

			$otp_email = trim($user_detail_email[0]->user_mob).$carrier_email;

			$address = $otp_email;

			$message = $msg_reg_otp;

			$subject = $usr_fgtpss_email_cotnt->email_title;

			$semail = $this->sendMail($address,$subject,$message);

			/********** EMAIL FOR SEND OTP END  **********/



			$otp_inserted_id = $otp_data->otp_id;

			$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

			$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

			$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

		}

		$response['message'] ='Sorry.Your account is not active. Please verify your mobile number via OTP.';

	}

}elseif(($user_detail_counter==0) && ($user_detail_email_counter==0)){

	$response['message']='User not exist!';

	$response['status']= 1;

	$response['Status'] = 0;

}

}

header('Content-type: application/json');

echo json_encode($response);

}

/************************************************************/

//	FUNCTION NAME : show_soical_login

//	FUNCTION USE :  Remove all old restaurnt item and insert new

//	FUNCTION ERRORCODE : 330000

/****************************************************/

public function show_restaurant_open_05_jan_2018()

{

//print_r( $this->param);

//exit;

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['deli_date'] = $deli_date = Input::get("date");

$data['deli_time'] = $deli_time = Input::get("time");

if(empty($data['rest_id'])){

$response['errorcode'] = "330001";

$response['status']= 0;

$response['message']="Restaura

d:Required parameter missing";

}elseif(empty($data['deli_date'])){

$response['errorcode'] = "330002";

$response['status']= 0;

$response['message']="Date : Required parameter missing";

}elseif(empty($data['deli_time'])){

$response['errorcode'] = "330003";

$response['status']= 0;

$response['message']="Time : Required parameter missing";

}elseif (!empty($data)){

$rest_listing = DB::table('restaurant');

$rest_listing = $rest_listing->select('*');

$rest_listing = $rest_listing->where('rest_status', '!=' , 'INACTIVE');

$rest_listing = $rest_listing->where('rest_id', '=' ,$rest_id);

$rest_listing = $rest_listing->groupBy('rest_id');

$rest_listing = $rest_listing->get();

$cart_item_count='0';

$rest_cart_id='';

/*RESTAURNT DETAILDATA ARRAY */

$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

$open_status = '0'; //0-open 1-closed

$open_time = '';

$day_name =  strtolower(date("D",strtotime($deli_date)));

$date_time = $deli_date.' '.$deli_time;

$current_time = $tt=  strtotime($date_time);

if($day_name=='sun')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_sat)){

$rest_time2 = explode('_',$rest_listing[0]->rest_sat);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_sun);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_sat)){

$rest_time2 = explode('_',$rest_listing[0]->rest_sat);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='mon')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_sun)){

$rest_time2 = explode('_',$rest_listing[0]->rest_sun);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_mon);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_sun)){

$rest_time2 = explode('_',$rest_listing[0]->rest_sun);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='tue')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_mon)){

$rest_time2 = explode('_',$rest_listing[0]->rest_mon);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_tues);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_mon)){

$rest_time2 = explode('_',$rest_listing[0]->rest_mon);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='wed')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_tues)){

$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_wed);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_tues)){

$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='thu')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_wed)){

$rest_time2 = explode('_',$rest_listing[0]->rest_wed);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_thus);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_wed)){

$rest_time2 = explode('_',$rest_listing[0]->rest_wed);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='fri')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_tues)){

$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_fri);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_tues)){

$rest_time2 = explode('_',$rest_listing[0]->rest_tues);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

if($day_name=='sat')

{

if((!empty($rest_listing[0]->rest_close)) && (in_array($day_name,(explode(',',$rest_listing[0]->rest_close)) )))

{

if(!empty($rest_listing[0]->rest_fri)){

$rest_time2 = explode('_',$rest_listing[0]->rest_fri);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

$rest_time1 = explode('_',$rest_listing[0]->rest_sat);

$start_time = date("H:i", (strtotime($rest_time1[0])));

$end_time = date("H:i", (strtotime($rest_time1[1])));

if($tt<strtotime($deli_date.' '.$start_time))

{

if(!empty($rest_listing[0]->rest_fri)){

$rest_time2 = explode('_',$rest_listing[0]->rest_fri);

$start_time = date("H:i", (strtotime($rest_time2[0])));

$end_time = date("H:i", (strtotime($rest_time2[1])));

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' -1 day'));

$s = $new_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

else

{

if(strtotime($deli_date.' '.$start_time) > strtotime($deli_date.' '.$end_time))

{

$new_date  = date('Y-m-d', strtotime($deli_date . ' +1 day'));

$s = $deli_date.' '.$start_time;

$e = $new_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

else

{

$s = $deli_date.' '.$start_time;

$e = $deli_date.' '.$end_time;

$daytime1 =strtotime($s);

$daytime3 = strtotime($e);

}

}

}

}

$currentTime = $tt;

if( ((!empty($daytime1) && $currentTime > $daytime1)) && ((!empty($daytime3) && $currentTime < $daytime3)) ){

$open_status = 0;

$status_msg= "Restaurant is open";

}

else

{

$open_status = 1;

$status_msg= "Restaurant is close";

}

/** PROMOTIONS CODE END  **/

$rest_data['open_status']=$open_status;

$rest_data['open_time']=$open_time;

$rest_data['status_msg']=$status_msg;

///print_r($rest_data);

//exit;

if($rest_listing){

$response['message'] = "Restaurant OPEN Detail";

$response['status']= 1;

$response['restaurant_detail'] = $rest_data;//$rest_listing;

}

else

{

$response['errorcode'] = "110002";

$response['status']= 0;

$response['message']="Restaurant Id: Invalid Restaurant ID";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Restaurant_review

//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

//	FUNCTION ERRORCODE : 130000

/****************************************************/

public function restaurant_food_detail()

{

DB::enableQueryLog();

$data['restaurentid'] = $rest_id = Input::get("restaurentid");

$data['food_menuid'] = $menu_id = Input::get("food_menuid");

$data['foodid'] = $menu_cate_id = Input::get("foodid");

$data['user_id'] = $user_id = Input::get("userid");

$data['lang_id'] = $lang_id = Input::get("lang_id");

if(empty($data['foodid'])){

$response['errorcode'] = "130001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['restaurentid'])){

$response['errorcode'] = "130002";

$response['status']= 0;

$response['message']="Food MenuId:Required parameter missing";

}elseif(empty($data['restaurentid'])){

$response['errorcode'] = "130003";

$response['status']= 0;

$response['message']="Food Id:Required parameter missing";

}elseif (!empty($data)){

$fav_status = '0';

if($user_id>0)

{

$fav_listing = DB::table('favourite_food')

->select('*')

->where('favrest_restid', '=' ,$rest_id)

->where('favrest_userid', '=' ,$user_id)

->where('favrest_menucatid', '=' ,$menu_cate_id)

->get();

if($fav_listing)

{

$fav_status = $fav_listing[0]->favrest_status;

}

}

$rest_listing = DB::table('search_restaurant_view');

$rest_listing = $rest_listing->select('*');

$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

$rest_listing = $rest_listing->get();

if($rest_listing){

$menu_list = DB::table('menu')

->where('restaurant_id', '=' ,$rest_id)

->where('menu_id', '=' ,$menu_id)

->where('menu_status', '=' ,'1')

->orderBy('menu_order', 'asc')

->get();





// echo "<pre>";

// print_r($menu_list);

// echo "</pre>";

// die;





// dd(DB::getQueryLog());

$menu_array = '';

$food_array='';

$popular_item = '';

$popular_item_count =0;

$popular_item_count_11 =0;

if(!empty($menu_list))

{

$popular_food_detail = '';

$popular_food_data='';

foreach($menu_list as $mlist)

{

$menu_id = $mlist->menu_id;

if ($lang_id == 'ar') {

	$menu_name = $mlist->menu_name_ar;

	$menu_desc = $mlist->menu_desc_ar;

} else {

	$menu_name = $mlist->menu_name;

	$menu_desc = $mlist->menu_desc;

}



$menu_detail =$mlist;

$menu_item_detail = '';

$menu_item_name = '';

$menu_item_desc = '';

$menu_item_price = '';

$food_detail = '';

$food_data='';

$counter_popular =0 ;

$menu_cat_detail = DB::table('menu_category')

->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

->where('menu_category.rest_id', '=' ,$rest_id)

->where('menu_category.menu_id', '=' ,$menu_id)

->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

->where('menu_category.menu_cat_status', '=' ,'1')

->select('menu_category.*','menu_category_item.*')

->orderBy('menu_category.menu_category_id', 'asc')

->get();



// echo "<pre>";

// print_r($menu_cat_detail);

// echo "</pre>";

// die;



$menu_item_count =count($menu_cat_detail);



$item_addons = '';

if(!empty($menu_cat_detail))

{

	foreach($menu_cat_detail as $menu_item)

	{

		$food_data['food_id'] = $menu_item->menu_category_id;

		$food_data['food_popular'] = $menu_item->menu_cat_popular;

		$food_data['food_diet'] = $menu_item->menu_cat_diet;



		if ($lang_id == 'ar') {

			$food_data['food_name'] = $menu_item->menu_category_name_ar;

		} else {

			$food_data['food_name'] = $menu_item->menu_category_name;

		}

		

		$food_data['food_price'] = $menu_item->menu_category_price;



		if(!empty($menu_item->menu_category_image)){

		$food_data['menu_image'] = trim(url('/').'/uploads/reataurant/menu/'.$menu_item->menu_category_image);

		}

		else

		{

		$food_data['menu_image'] =trim(url('/').'/uploads/reataurant/menu/menu.jpg');

		}



		if ($lang_id == 'ar') {

			$food_data['food_desc'] = $menu_item->menu_category_desc_ar;

		} else {

			$food_data['food_desc'] = $menu_item->menu_category_desc;

		}

		

		$food_data['food_subitem'] = $menu_item->menu_category_portion;

		$food_data['food_subitem_title']='';

		$food_data['food_size_detail']='';

		if($menu_item->menu_category_portion=='yes')

		{

			$food_data['food_subitem_title']='Choose a size';

			$food_size = DB::table('category_item')

			->where('rest_id', '=' ,$rest_id)

			->where('menu_id', '=' ,$menu_id)

			->where('menu_category', '=' ,$menu_item->menu_category_id)

			->where('menu_item_status', '=' ,'1')

			->select('*')

			->orderBy('menu_cat_itm_id', 'asc')

			->groupBy('menu_item_title')

			->get();

			$food_data['food_size_detail']=$food_size;

		}

		else

		{

			$food_data['food_size_detail']=array();

		}

		$group_name= '';

		$group_list = '';

		$ff_popular = '';

		$popular_food_data='';

		$addons = DB::table('menu_category_addon')

		->where('addon_restid', '=' ,$rest_id)

		->where('addon_menuid', '=' ,$menu_id)

		->where('addon_status', '=' ,'1')

		->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

		->select('*')

		->orderBy('addon_id', 'asc')

		->groupBy('addon_groupname')

		->get();

		if(!empty($addons))

		{

			foreach($addons as $ad_list)

			{

				$option_type = 0;

				if(substr($ad_list->addon_groupname, -1)=='*')

				{

					$option_type = 1;

				}

				$group_name[]=$ad_list->addon_groupname;

				if ($lang_id == 'ar') {

					$addon_groupname = $ad_list->addon_groupname_ar;

				} else {

					$addon_groupname = $ad_list->addon_groupname;

				}

				$ff['addon_gropname'] = $addon_groupname;

				$ff['addon_type'] = $ad_list->addon_option;

				$ff['addon_mandatory_or_not'] = $option_type;

				$addon_group = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_groupname', '=' ,$ad_list->addon_groupname)

				->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

				->select('*')

				->orderBy('addon_id', 'asc')

				->get();

				$group_list[]=$addon_group;

				$addon_group_list ='';

				foreach($addon_group as $group_list_loop)

				{

						//$addon_group_list[]=array('')

					if ($lang_id == 'ar') {

						$addon_groupname = $group_list_loop->addon_groupname_ar;

						$addon_name = $group_list_loop->addon_name_ar;

					} else {

						$addon_groupname = $group_list_loop->addon_groupname;

						$addon_name = $group_list_loop->addon_name;

					}

					$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

						'addon_menucatid'=>$group_list_loop->addon_menucatid,

						'addon_groupname'=>$addon_groupname,

						'addon_option'=>$group_list_loop->addon_option,

						'addon_name'=>$addon_name,

						'addon_price'=>$group_list_loop->addon_price,

						'addon_status'=>$group_list_loop->addon_status

					);

				}

				$ff['addon_detail'] = $addon_group_list;

				$food_data['food_addon'][]=$ff;

				if($menu_item->menu_cat_popular==1)

				{

					$ff_popular['addon_detail'] = $addon_group_list;

					$ff_popular['addon_gropname'] = $ad_list->addon_groupname;

					$ff_popular['addon_type'] = $ad_list->addon_option;

					$popular_food_data['food_addon'][] = $ff_popular;

				}

			}

		}

		else

		{

			$food_data['food_addon']=array();

			$ff['addon_gropname'] = '';

		}

		$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);

		$food_detail[] = $food_data;

	}

	$promo_list = DB::table('promotion')

	->where('promo_restid', '=' ,$rest_id)

	->where('promo_for', '=' ,'each_food')

	->where('promo_menu_item', '=' ,$menu_cate_id)

	->where('promo_status', '=' ,'1')

	->orderBy('promo_buy', 'desc')

	->groupBy('promo_id')

	->get();

	/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/

	$current_promo='';

	if(($user_id>0) && ($user_id!=''))

	{

		foreach($promo_list as $plist )

		{

			if($plist->promo_for=='each_food')

			{

				$current_promo1 = '';

				if($plist->promo_on=='schedul')

				{

					$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

					$db_day = '';

					if(!empty($plist->promo_day))

					{

						$db_day = explode(',',$plist->promo_day);

					}

					if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

					{

						if(

							($plist->promo_start<=date('Y-m-d')) &&

							($plist->promo_end>=date('Y-m-d')) &&

							(empty($plist->promo_day))

						)

						{

							$current_promo1['promo_id'] = $plist->promo_id;

							$current_promo1['promo_restid'] = $plist->promo_restid;

							$current_promo1['promo_for'] = $plist->promo_for;

							$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

							$current_promo1['promo_on'] = $plist->promo_on;

							$current_promo1['promo_desc'] = $plist->promo_desc;

							$current_promo1['promo_mode'] = $plist->promo_mode;

							$current_promo1['promo_buy'] = $plist->promo_buy;

							$current_promo1['promo_value'] = $plist->promo_value;

							$current_promo1['promo_end'] = $plist->promo_end;

							$current_promo1['promo_start'] = $plist->promo_start;

							$current_promo[] = $current_promo1;

						}

						if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

							&& (!empty($plist->promo_day))

							&& (in_array($day_name,$db_day) )

						)

						{

							$array_amt[]=$plist->promo_buy;

							$current_promo1['promo_id'] = $plist->promo_id;

							$current_promo1['promo_restid'] = $plist->promo_restid;

							$current_promo1['promo_for'] = $plist->promo_for;

							$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

							$current_promo1['promo_on'] = $plist->promo_on;

							$current_promo1['promo_desc'] = $plist->promo_desc;

							$current_promo1['promo_mode'] = $plist->promo_mode;

							$current_promo1['promo_buy'] = $plist->promo_buy;

							$current_promo1['promo_value'] = $plist->promo_value;

							$current_promo1['promo_end'] = $plist->promo_end;

							$current_promo1['promo_start'] = $plist->promo_start;

							$current_promo[] = $current_promo1;

						}

					}

					elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))

					{

						$db_day = explode(',',$plist->promo_day);

						if(in_array($day_name,$db_day))

						{

							$array_amt[]=$plist->promo_buy;

							$current_promo1['promo_id'] = $plist->promo_id;

							$current_promo1['promo_restid'] = $plist->promo_restid;

							$current_promo1['promo_for'] = $plist->promo_for;

							$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

							$current_promo1['promo_on'] = $plist->promo_on;

							$current_promo1['promo_desc'] = $plist->promo_desc;

							$current_promo1['promo_mode'] = $plist->promo_mode;

							$current_promo1['promo_buy'] = $plist->promo_buy;

							$current_promo1['promo_value'] = $plist->promo_value;

							$current_promo1['promo_end'] = $plist->promo_end;

							$current_promo1['promo_start'] = $plist->promo_start;

							$current_promo[] = $current_promo1;

						}

					}

				}

				elseif($plist->promo_on=='qty')

				{

					$current_promo1['promo_id'] = $plist->promo_id;

					$current_promo1['promo_restid'] = $plist->promo_restid;

					$current_promo1['promo_for'] = $plist->promo_for;

					$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

					$current_promo1['promo_on'] = $plist->promo_on;

					$current_promo1['promo_desc'] = $plist->promo_desc;

					$current_promo1['promo_mode'] = $plist->promo_mode;

					$current_promo1['promo_buy'] = $plist->promo_buy;

					$current_promo1['promo_value'] = $plist->promo_value;

					$current_promo1['promo_end'] = $plist->promo_end;

					$current_promo1['promo_start'] = $plist->promo_start;

					$current_promo[] = $current_promo1;

				}

			}

		}

		if(empty($current_promo)){$current_promo=array();}

	}

	else

	{

		$current_promo=array();

	}



$special_instruction = "";

$deviceid = Input::get("deviceid");

$devicetype = Input::get("devicetype");



$special_ins = DB::table('temp_cart')

->where('user_id', '=' ,$user_id)

->where('rest_id', '=' ,$rest_id)

->where('menu_id', '=' ,$menu_id)

->where('menu_catid', '=' ,$menu_cate_id)

->where('deviceid', '=' ,$deviceid)

->where('devicetype', '=' ,$devicetype)

->value('special_instruction');



if($special_ins){ $special_instruction = $special_ins; }







	/*************************** END ******************************/

	$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'fav_status'=>$fav_status,'special_instruction'=>$special_instruction,'food_detail'=>$food_detail,'promo_list'=>$current_promo);

	$response['message'] = "Restaurant Detail";

	$response['status']= 1;

	//	$response['restaurant_detail'] = $rest_listing;

		//$response['popular_menu_detal'] = $popular_item;

	$response['menu_detal'] = $food_array;

}

else

{

	$response['errorcode'] = "130005";

	$response['status']= 0;

	$response['message']="Food Id: Invalide Food ID";

}

}

}

else

{

$response['errorcode'] = "130004";

$response['status']= 0;

$response['message']="Menu Id: Invalide Menu ID";

}

}

else

{

$response['errorcode'] = "130004";

$response['status']= 0;

$response['message']="Restaurant Id: Invalid Restaurant ID";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : show_notification

//	FUNCTION USE :  Show all notification listing

//	FUNCTION ERRORCODE : 440000

/****************************************************/



function show_notification()

{

$response = array();

$data['user_id'] =	$user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

if(empty($data['user_id'])){

$response['errorcode'] = "430001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}

elseif (!empty($data)){

$limit = 10;

//$notification_detail = DB::table('notification_list')->where('noti_userid', $user_id)->get();

$notification_detail = DB::table('notification_list')

->where('noti_userid', '=' ,$user_id)

->select('*')

->orderBy('noti_id', 'desc')

->paginate($limit)	;

$notification_counter = $notification_detail->total();

if($notification_counter>0)

{

$notification_data='';

foreach($notification_detail as $notify)

{


$ago_time = $this->calculate_time_span($notify->updated_at) ;


$notification_data[] = array(

'noti_id' => $notify->noti_id,

'noti_userid' => $notify->noti_userid,

'noti_guestid' => $notify->noti_guestid,

'noti_title' => $notify->noti_title,

'noti_desc' => $notify->noti_desc,

'noti_deviceid' => $notify->noti_deviceid,

'noti_read' => $notify->noti_read,

'ago_time' => $ago_time,

'created_at' => date("H:i A d-m-Y", (strtotime($notify->created_at))),

'updated_at' => date("H:i A d-m-Y", (strtotime($notify->updated_at)))

);

}

$rest_detail_format['total'] =$notification_detail->total() ;

$rest_detail_format['per_page'] =$notification_detail->perPage();

$rest_detail_format['current_page'] =$notification_detail->currentPage();;

$rest_detail_format['last_page'] =$notification_detail->lastPage();

$rest_detail_format['data'] =$notification_data;

/*for ($i=0; $i < $rest_detail_format['per_page']; $i++) {

	$rest_detail_format['data'][$i]->created_at = date("H:i A d-m-Y", (strtotime($rest_detail_format['data'][$i]->created_at)));

}*/

$response['user_id']=$user_id;

$response['notification_detail']=$rest_detail_format;

$response['status']= 1;

$response['message']='View Your Order!';

}

else

{

$response['user_id']=$user_id;

$response['errorcode'] = "430002";

$response['status']= 0;

$response['message']='notification list not found!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Deliveryman show_notification

//	FUNCTION USE :  Show all notification listing

//	FUNCTION ERRORCODE : 440000

/****************************************************/



function dm_show_notification()

{



$response = array();

$data['vendor_id'] =	$vendor_id = Input::get("userid");



if(empty($data['vendor_id'])){

$response['errorcode'] = "430001";

$response['status']= 0;

$response['message']="Merchant Id:Required parameter missing";

}

elseif (!empty($data)){

$limit = 10;



$notification_detail = DB::table('orders')

->where('vendor_id', '=' ,$vendor_id)

->orderBy('id', 'desc')

->paginate($limit)	;



$notification_counter = $notification_detail->total();



if($notification_counter>0)

{



$notification_data = array();



foreach($notification_detail as $notify)

{



$ago_time = $this->calculate_time_span($notify->updated_at) ;


$notification_data[] = array(

'noti_id' => $notify->id,

'noti_userid' => $notify->user_id,

'ps_id' => $notify->ps_id,

'order_id' => $notify->order_id,

'status' => $notify->status,

'total' => $notify->total,

'txn_id' => $notify->txn_id,

'pay_status' => $notify->pay_status,

'ago_time' => $ago_time,

'created_at' => date("H:i A d-m-Y", (strtotime($notify->created_at))),

'updated_at' => date("H:i A d-m-Y", (strtotime($notify->updated_at)))

);



} 



$rest_detail_format['total'] =$notification_detail->total() ;

$rest_detail_format['per_page'] =$notification_detail->perPage();

$rest_detail_format['current_page'] =$notification_detail->currentPage();;

$rest_detail_format['last_page'] =$notification_detail->lastPage();

$rest_detail_format['data'] =$notification_data;



$response['user_id']=$vendor_id;

$response['notification_detail']=$rest_detail_format;

$response['status']= 1;

$response['message']='View Your Order!';

}

else

{

$response['user_id']=$vendor_id;

$response['errorcode'] = "430002";

$response['status']= 0;

$response['message']='notification list not found!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}	



// Get day ago date time



function calculate_time_span($date) {

$seconds = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

$months = floor($seconds / (3600 * 24 * 30));

$day = floor($seconds / (3600 * 24));

$hours = floor($seconds / 3600);

$mins = floor(($seconds - ($hours * 3600)) / 60);

$secs = floor($seconds % 60);



if ($seconds < 60)

$time = $secs . " secs ago";

else if ($seconds < 60 * 60) {

$time = $mins . " min ago"; 

if($mins > 1) {

$time = $mins . " mins ago"; 

}

}

else if ($seconds < 24 * 60 * 60) {

$time = $hours . " hour ago"; 

if($hours > 1) {

$time = $hours . " hours ago"; 

}

}

else if ($day > cal_days_in_month(CAL_GREGORIAN, date('m'), date('y'))) {

$time = $months . " month ago";

if($months > 1) {

$time = $months . " months ago";

}

}  

else {

$time = $day . " day ago";

if($day > 1) {

$time = $day . " days ago"; 

}



}

return $time;

}





/************************************************************/

//	FUNCTION NAME : show_restaurant_open

//	FUNCTION USE :  show restaurant open

//	FUNCTION ERRORCODE : 350000

/****************************************************/



public function show_restaurant_open()

{

DB::enableQueryLog();

//print_r( $this->param);

//exit;

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['deli_date'] = $deli_date = Input::get("date");

$data['deli_time'] = $deli_time = Input::get("time");

if(empty($data['rest_id'])){

$response['errorcode'] = "330001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['deli_date'])){

$response['errorcode'] = "330002";

$response['status']= 0;

$response['message']="Date : Required parameter missing";

}elseif(empty($data['deli_time'])){

$response['errorcode'] = "330003";

$response['status']= 0;

$response['message']="Time : Required parameter missing";

}elseif (!empty($data)){

$rest_listing = DB::table('restaurant');

$rest_listing = $rest_listing->select('*');

$rest_listing = $rest_listing->where('rest_status', '!=' , 'INACTIVE');

$rest_listing = $rest_listing->where('rest_id', '=' ,$rest_id);

$rest_listing = $rest_listing->groupBy('rest_id');

$rest_listing = $rest_listing->get();

$cart_item_count='0';

$rest_cart_id='';

/*RESTAURNT DETAILDATA ARRAY */

$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

$rest_data["rest_close"] = '';//trim($rest_listing[0]->rest_close);

$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

$open_status = '0'; //1-open 0-closed 05-jan-2018 New

$open_time = '';

$day_name =  strtolower(date("D",strtotime($deli_date)));

$date_time = $deli_date.' '.$deli_time;

$date_time = $deli_date.' '.date('H:i:s',strtotime($deli_time));

$current_time = $tt=  strtotime($date_time);

$open_detail_listing = DB::table('rest_time');

$open_detail_listing = $open_detail_listing->select('*');

$open_detail_listing = $open_detail_listing->where('rest_id', '=' ,$rest_id);

$open_detail_listing = $open_detail_listing->where('day', '=' ,(date("D",strtotime($deli_date))));

$open_detail_listing = $open_detail_listing->where('open_time', '<=' ,date('H:i:s',strtotime($deli_time)));

$open_detail_listing = $open_detail_listing->where('close_time', '>=' ,date('H:i:s',strtotime($deli_time)));

$open_detail_listing = $open_detail_listing->get();

// dd(DB::getQueryLog());

if($open_detail_listing)

{

$open_status = 1;

$status_msg= "Restaurant is open";

}

else

{

$open_status = 0;

$status_msg= "Restaurant is close";

}

/** PROMOTIONS CODE END  **/

$rest_data['open_status']=$open_status;

$rest_data['open_time']=$open_time;

$rest_data['status_msg']=$status_msg;

///print_r($rest_data);

//exit;

if($rest_listing){

$response['message'] = "Restaurant OPEN Detail";

$response['status']= 1;

$response['restaurant_detail'] = $rest_data;//$rest_listing;

}

else

{

$response['errorcode'] = "110002";

$response['status']= 0;

$response['message']="Restaurant Id: Invalid Restaurant ID";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : get_update_order_type

//	FUNCTION USE :  Update Order type it's pickup or delivery

//	FUNCTION ERRORCODE : 380000

/****************************************************/

function get_update_order_type()

{

$data['user_id'] = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = Input::get("rest_id");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

$data['rest_cartid'] = Input::get("rest_cartid");

$data['order_type'] = Input::get("order_type");

if(empty($data['rest_id'])){

$response['errorcode'] = "380001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "380002";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "38003";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "380004";

$response['status']= 0;

$response['message']="Rest Cart Id:Required parameter missing";

}elseif(empty($data['order_type'])){

$response['errorcode'] = "380005";

$response['status']= 0;

$response['message']="Order Type:Required parameter missing";

}elseif(!empty($data)){

$rest_id =trim($this->param['rest_id']);

$user_id =trim($this->param['user_id']);

$cart_detail ='';

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();

if($cart_listing)

{

/********************** UPDATE SELECTED FILE IF ANY ADDRESS IS SELECTED*****************************/

$address_id =trim($this->param['address_id']);

if(!empty($address_id))

{

DB::table('delivery_address')

->where('del_userid', '=' ,trim($this->param['user_id']))

->update([

'del_selected' => '0'

]);

DB::table('delivery_address')

->where('del_id', '=' ,trim($this->param['address_id']))

->where('del_userid', '=' ,trim($this->param['user_id']))

->update([

'del_selected' => '1'

]);

}

/********************** END*****************************/

DB::table('temp_cart')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('rest_cartid', '=' ,trim($this->param['rest_cartid']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->update([

'order_type' => trim($this->param['order_type'])

]);

$response['message'] = "Cart order type updated!";

$response['status']= 1;

}

else

{

$response['errorcode'] = "380006";

$response['status']= 0;

$response['message']="This cart is empty";

}

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : get_verify_delivery_address

//	FUNCTION USE :  Verify delivery address is correct or not

//	FUNCTION ERRORCODE : 390000

/****************************************************/

function get_verify_delivery_address()

{

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['address_lat'] = $address_lat = Input::get("address_lat");

$data['address_lang'] = $address_lang = Input::get("address_lang");

if(empty($data['rest_id'])){

$response['errorcode'] = "390001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['address_lat'])){

$response['errorcode'] = "390002";

$response['status']= 0;

$response['message']="Address Latitute: Required parameter missing";

}elseif(empty($data['address_lang'])){

$response['errorcode'] = "390003";

$response['status']= 0;

$response['message']="Address Langitute: Required parameter missing";

}

elseif (!empty($data)){

$rest_listing = DB::table('search_restaurant_view')

->select("*")

->where("rest_id","=",$rest_id)

->get();

if($rest_listing)

{

$diff_distance =  $this->distance($rest_listing[0]->rest_lat, $rest_listing[0]->rest_long, $address_lat, $address_lang, "m");

if( $diff_distance<$rest_listing[0]->rest_delivery_upto)

{

$response['message'] = "Address verified successfully!";

$response['rest_data'] = array('rest_delivery_upto'=>$rest_listing[0]->rest_delivery_upto,'distance'=>$diff_distance);

$response['status']= 1;

}

else

{

$response['errorcode'] = "390004";

$response['message'] = "Restaurant not give delivery on your address!";

$response['status']= 0;

}

}

else

{

$response['errorcode'] = "390005";

$response['message'] = "Restaurant not available";

$response['status']= 0;

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : add_delivery_address

//	FUNCTION USE :  Addd new delivery address

//	FUNCTION ERRORCODE : 350000

/****************************************************/



public function add_delivery_address()

{

$data['rest_id'] = $rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['street_address'] = $street_address = Input::get("street_address");

$data['city'] = $city = Input::get("city");

$data['state'] = $state = Input::get("state");

$data['zipcode'] = $zipcode = Input::get("zipcode");

$data['country'] = $country = Input::get("country");

$data['phone_no'] = $address_lat = Input::get("phone_no");

$data['address_lat'] = $address_lat = Input::get("address_lat");

$data['address_lang'] = $address_lang = Input::get("address_lang");

if(empty($data['rest_id'])){

$response['errorcode'] = "340001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['street_address'])){

$response['errorcode'] = "340002";

$response['status']= 0;

$response['message']="Street Address : Required parameter missing";

}elseif(empty($data['city'])){

$response['errorcode'] = "340003";

$response['status']= 0;

$response['message']="City: Required parameter missing";

}elseif(empty($data['state'])){

$response['errorcode'] = "340004";

$response['status']= 0;

$response['message']="State: Required parameter missing";

}elseif(empty($data['zipcode'])){

$response['errorcode'] = "340005";

$response['status']= 0;

$response['message']="Zipcode: Required parameter missing";

}elseif(empty($data['country'])){

$response['errorcode'] = "340006";

$response['status']= 0;

$response['message']="country: Required parameter missing";

}elseif(empty($data['phone_no'])){

$response['errorcode'] = "340007";

$response['status']= 0;

$response['message']="Contact Number: Required parameter missing";

}

elseif (!empty($data)){

$rest_listing = DB::table('search_restaurant_view')

->select("*")

->where("rest_id","=",$rest_id)

->get();

if($rest_listing)

{



$diff_distance =  $this->distance($rest_listing[0]->rest_lat, $rest_listing[0]->rest_long, $address_lat, $address_lang, "m");



if( $diff_distance<$rest_listing[0]->rest_delivery_upto)

{



$get_dbaddress = DB::table('delivery_address') 

->select("*")

->where("del_userid","=",trim($this->param['userid']))

->where("del_address","=",trim($this->param['street_address']))

->first();



$get_dbaddressall = DB::table('delivery_address') 

->select("*")

->where("del_userid","=",trim($this->param['userid']))

->get();



$addcount = count($get_dbaddressall);



if($addcount>4){



$get_add_first = DB::table('delivery_address')->where("del_userid","=",trim($this->param['userid']))->first();



DB::table('delivery_address')->where('del_id', '=', trim($get_add_first->del_id))->delete();



}



if(empty($get_dbaddress)){



$result = new Delivery_address;

$result->del_userid = trim($this->param['userid']);

$result->del_guestid = trim($this->param['guest_id']);

$result->del_contactno = trim($this->param['phone_no']);

$result->del_address = trim($this->param['street_address']);

$result->del_city = trim($this->param['city']);

$result->del_zipcode = trim($this->param['zipcode']);

$result->del_state = trim($this->param['state']);

$result->del_country = trim($this->param['country']);

$result->del_lat = trim($this->param['address_lat']);

$result->del_long = trim($this->param['address_lang']);

$result->del_selected = '0';

$result->deviceid = trim($this->param['deviceid']);

$result->devicetype = trim($this->param['devicetype']);

$result->save();

$add_id =$result->del_id;

}else{



$add_id =$get_dbaddress->del_id;



}



$address_data = DB::table('delivery_address')->where('del_id', '=' ,$add_id)->get();

$response['message'] = "Delivery address added successfully!";

$response['address_data'] = $address_data;

$response['status']= 1;

}

else

{

$response['errorcode'] = "340008";

$response['message'] = "Restaurant not give delivery on your address!";

$response['status']= 0;

}

}

else

{

$response['errorcode'] = "340009";

$response['message'] = "Restaurant not available";

$response['status']= 0;

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Restaurant_detail

//	FUNCTION USE :  RESTAURANT DETAIL WITH IT'S MENU AND ADDONS

//	FUNCTION ERRORCODE : 110000

/****************************************************/

public function restaurant_detail()

{

$data['restaurentid'] =$rest_id = Input::get("restaurentid");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$device_name = Input::get("device_name");

$device_osname = Input::get("device_osname");

$loc_address = Input::get("loc_address");

$lang_id = Input::get("lang_id");



/* $city = $this->getcity(Input::get('loc_address'));



if($city){



$area_id = $city;



}else{ */



$city = DB::table('restaurant')

->where('rest_id', '=' , $rest_id) 

->value('rest_city');



$area_id = $city;  



// } 





/* $area_id = DB::table('area_management')

->where('area_city', '=' , $city) 

->value('area_id'); */



/*elseif(($user_id=='') || ($user_id<0)){

$response['errorcode'] = "110003";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}*/

if(empty($data['restaurentid'])) {

$response['errorcode'] = "110001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

} elseif (!empty($data)) {

$rest_listing = DB::table('search_restaurant_view');

$rest_listing = $rest_listing->select('*');

$rest_listing = $rest_listing->where('search_restaurant_view.rest_status', '!=' , 'INACTIVE');

$rest_listing = $rest_listing->where('search_restaurant_view.rest_id', '=' ,$rest_id);

$rest_listing = $rest_listing->groupBy('search_restaurant_view.rest_id');

$rest_listing = $rest_listing->get();

$cart_item_count='0';

$rest_cart_id='';

if( ($user_id>0) || ($guest_id!=''))

{

$cart_rest_listing = DB::table('temp_cart')

->select('*')

->where('user_id', '=' ,trim($user_id))

->where('guest_id', '=' ,trim($guest_id))

->where('rest_id', '=' ,trim($rest_id))

->where('deviceid', '=' ,trim($deviceid))

->where('devicetype', '=' ,trim($devicetype))

->get();

if(count($cart_rest_listing)>0)

{

$rest_cart_id = $cart_rest_listing[0]->rest_cartid;

$cart_item_count = count($cart_rest_listing);

}

}

/************ VIEW MENU LOG HISTORY ***************/

$data_view_history=array('user_id'=>$user_id,

'guest_id'=>$guest_id,

'rest_id'=>$rest_id,

'menu_id'=>'',

'submenu_id'=>'',

'device_type'=>$devicetype,

'device_name'=>$device_name,

'device_os'=>$device_osname,

'view_date'=>date('Y-m-d H:i:s'),

'view_time'=>date('Y-m-d H:i:s')

);

DB::table('view_menu_history')->insert($data_view_history);

/*************** VIEW MENU LOG HISTORY END *********************/

/* COUNT CALCULATION OF RATING  FOR REVIEW /FOOD/ ETC */

$rest_food_good ='0';

$rest_delivery_ontime ='0';

$rest_order_accurate ='0';

$count_review = DB::table('review')

->where('re_restid', '=' ,$rest_id)

->where('re_status', '=' ,'PUBLISHED')

->orderBy('re_id', 'desc')

->count();

if($count_review>0)

{

$sql_food = DB::table('review')

->select(DB::raw("round((count(`re_food_good`) / ".$count_review."),0) as  food_good"))

->where('re_restid', '=' , $rest_id)

->where('re_food_good', '=' , '1')

->get();

$sql_delivery_ontime = DB::table('review')

->select(DB::raw("round((count(`re_delivery_ontime`) / ".$count_review."),0) as  delivery_ontime"))

->where('re_restid', '=' , $rest_id)

->where('re_delivery_ontime', '=' , '1')

->get();

$sql_rder_accurate = DB::table('review')

->select(DB::raw("round((count(`re_order_accurate`) / ".$count_review."),0) as  order_accurate"))

->where('re_restid', '=' , $rest_id)

->where('re_order_accurate', '=' , '1')

->get();

$rest_food_good = $sql_food[0]->food_good;

$rest_delivery_ontime = $sql_delivery_ontime[0]->delivery_ontime;

$rest_order_accurate = $sql_rder_accurate[0]->order_accurate;

}

/* END */

/*RESTAURNT DETAILDATA ARRAY */

$rest_data["rest_cart_id"] = trim($rest_cart_id);

$rest_data["cart_item_count"] = trim($cart_item_count);

$rest_data["rest_id"] = trim($rest_listing[0]->rest_id);

$rest_data["vendor_id"] = trim($rest_listing[0]->vendor_id);

if ($lang_id == 'ar') {

$rest_data["rest_name"] = trim($rest_listing[0]->rest_name_ar);

}else{

$rest_data["rest_name"] = trim($rest_listing[0]->rest_name);

}			

$rest_data["rest_metatag"] = trim($rest_listing[0]->rest_metatag);

$rest_data["rest_logo"] = trim(url('/').'/uploads/reataurant/'.$rest_listing[0]->rest_logo);

$rest_data["rest_address"] = trim($rest_listing[0]->rest_address);

$rest_data["rest_address2"] = trim($rest_listing[0]->rest_address2);

$rest_data["rest_city"] = trim($rest_listing[0]->rest_city);

$rest_data["rest_zip_code"] = trim($rest_listing[0]->rest_zip_code);

$rest_data["rest_suburb"] = trim($rest_listing[0]->rest_suburb);

$rest_data["rest_state"] = trim($rest_listing[0]->rest_state);

if ($lang_id == 'ar') {

$rest_data["rest_desc"] = trim($rest_listing[0]->rest_desc_ar);

}else{

$rest_data["rest_desc"] = trim($rest_listing[0]->rest_desc);

}



$rest_data["rest_contact"] = trim($rest_listing[0]->rest_contact);

if ($lang_id == 'ar') {

	$rest_data["rest_service"] = "توصيل";

}else{

	$rest_data["rest_service"] = trim($rest_listing[0]->rest_service);

}



$rest_data["rest_create"] = trim($rest_listing[0]->rest_create);

$rest_data["rest_mon"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_mon));

$rest_data["rest_tues"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_tues));

$rest_data["rest_wed"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_wed));

$rest_data["rest_thus"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_thus));

$rest_data["rest_fri"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_fri));

$rest_data["rest_sat"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sat));

$rest_data["rest_sun"] = trim(str_replace('_',' to ',$rest_listing[0]->rest_sun));

$rest_data["rest_close"] = trim($rest_listing[0]->rest_close);

$rest_data["rest_cuisine"] = trim($rest_listing[0]->rest_cuisine);

$rest_data["rest_mindelivery"] = trim($rest_listing[0]->rest_mindelivery);

$rest_data["rest_landline"] = trim($rest_listing[0]->rest_landline);

$rest_data["rest_commission"] = trim($rest_listing[0]->rest_commission);

$rest_data["rest_email"] = trim($rest_listing[0]->rest_email);

$rest_data["rest_status"] = trim($rest_listing[0]->rest_status);

$rest_data["rest_classi"] = trim($rest_listing[0]->rest_classi);

$rest_data["rest_special"] = trim($rest_listing[0]->rest_special);

$rest_data["rest_delivery_from"] = trim($rest_listing[0]->rest_delivery_from);

$rest_data["rest_delivery_to"] = trim($rest_listing[0]->rest_delivery_to);

$rest_data["rest_holiday_from"] = trim($rest_listing[0]->rest_holiday_from);

$rest_data["rest_holiday_to"] = trim($rest_listing[0]->rest_holiday_to);

$rest_data["rest_openstatus"] = trim($rest_listing[0]->rest_openstatus);

if(!empty($rest_listing[0]->rest_banner)){

$rest_data["rest_banner"] = trim(url('/').'/uploads/reataurant/banner/'.$rest_listing[0]->rest_banner);

}

else

{

$rest_data["rest_banner"] =trim(url('/').'/uploads/reataurant/banner/sub_header_2.jpg');

}

$rest_data["rest_lat"] = trim($rest_listing[0]->rest_lat);

$rest_data["rest_long"] = trim($rest_listing[0]->rest_long);

$rest_data["created_at"] = trim($rest_listing[0]->created_at);

$rest_data["updated_at"] = trim($rest_listing[0]->updated_at);

$totalrating = 0;

if(!empty($rest_listing[0]->totalrating)){$totalrating = $rest_listing[0]->totalrating; }

$rest_data["totalrating"] = trim($totalrating);

//$rest_data["totalrating"] = trim($rest_listing[0]->totalrating);

$rest_data["totalreview"] = trim($rest_listing[0]->totalreview);

$rest_data["cuisine_name"] = trim($rest_listing[0]->cuisine_name);

$rest_data["rest_price_level"] = trim($rest_listing[0]->rest_price_level);

$rest_data["rest_servicetax"] = trim($rest_listing[0]->rest_servicetax);

$rest_data["rest_cash_deliver"] = trim($rest_listing[0]->rest_cash_deliver);

$rest_data["rest_partial_pay"] = trim($rest_listing[0]->rest_partial_pay);

$rest_partial_percent = 0;

if($rest_listing[0]->rest_partial_pay==1)

{

$rest_partial_percent = $rest_listing[0]->rest_partial_percent;

}

$rest_data["rest_partial_percent"] = trim($rest_partial_percent);

$rest_data["rest_delivery_area"] = trim($rest_listing[0]->rest_delivery_upto);

/*$rest_data["rest_food_good"] = trim('92% Food was good');

$rest_data["rest_on_time"] = trim('89% Delivery was on time');

$rest_data["rest_order_accurate"] = trim('98% Order was accurate');*/

$rest_data["rest_food_good"] = trim('');

$rest_data["rest_on_time"] = trim('');

$rest_data["rest_order_accurate"] = trim('');



if ($lang_id == 'ar') {

	$food_was_good = "كان الطعام جيد";

	$delivery_was_on_time = "كان ٪ التسليم في الوقت المحدد";

	$order_was_accurate = "كان ترتيب ٪ دقيقة";

} else {

	$food_was_good = "% Food was good";

	$delivery_was_on_time = "% Delivery was on time";

	$order_was_accurate = "% Order was accurate";

}

if($rest_food_good>0){

$rest_data["rest_food_good"] = trim($rest_food_good.$food_was_good);

}

if($rest_delivery_ontime>0){

$rest_data["rest_on_time"] = trim($rest_delivery_ontime.$delivery_was_on_time);

}

if($rest_order_accurate>0){

$rest_data["rest_order_accurate"] = trim($rest_order_accurate.$order_was_accurate);

}

$rest_data["rest_min_orderamt"] = trim($rest_listing[0]->rest_min_orderamt);

/* END **/

if($rest_listing) {

/*$menu_list = DB::table('menu')

			->where('restaurant_id', '=' ,$rest_id)

			->where('menu_status', '=' ,'1')

			->orderBy('menu_order', 'asc')

			->get();*/

			$menu_list = DB::table('menu_category')

			->leftJoin('menu', 'menu_category.menu_id', '=', 'menu.menu_id')

			->where('menu_category.rest_id', '=' ,$rest_id)

			->where('menu_category.area_id', '=' ,$area_id)

			->where('menu_category.menu_cat_status', '=' ,'1')

			->where('menu.restaurant_id', '=' ,$rest_id)

			->where('menu.menu_status', '=' ,'1')

			->select('menu.*')

			->orderBy('menu.menu_id', 'asc')

			->groupBy('menu.menu_id')

			->get();



if ($lang_id == 'ar') {

	foreach ($menu_list as $key => $value) {

		$menu_lists = array(

            "menu_id" => $value->menu_id,

            "restaurant_id" => $value->restaurant_id,

            "menu_order" => $value->menu_order,

            "area_id" => $value->area_id,

            "menu_name" => $value->menu_name_ar,

            "menu_image" => $value->menu_image,

            "menu_desc" => $value->menu_desc_ar,

            "menu_create" => $value->menu_create,

            "menu_status" => $value->menu_status,

            "template_id" => $value->template_id,

            "created_at" => $value->created_at,

            "updated_at" => $value->updated_at,

		);		

		$menu_listnew[] = $menu_lists;		

	}



}else{

	foreach ($menu_list as $key => $value) {	

		$menu_lists = array(

			"menu_id" => $value->menu_id,

            "restaurant_id" => $value->restaurant_id,

            "menu_order" => $value->menu_order,

            "area_id" => $value->area_id,

            "menu_name" => $value->menu_name,

            "menu_image" => $value->menu_image,

            "menu_desc" => $value->menu_desc,

            "menu_create" => $value->menu_create,

            "menu_status" => $value->menu_status,

            "template_id" => $value->template_id,

            "created_at" => $value->created_at,

            "updated_at" => $value->updated_at,

		);		

		$menu_listnew[] = $menu_lists;		

	}

}



			$menu_array = '';

			$food_array=array();

			$popular_item = '';

			$popular_item_count =0;

			$popular_item_count_11 =0;

			if(!empty($menu_list))

			{

				$popular_food_detail = array();

				$popular_food_data='';

				foreach($menu_list as $mlist)

				{

					$menu_id = $mlist->menu_id;

					if ($lang_id == 'ar') {

						$menu_name = $mlist->menu_name_ar;

						$menu_desc = $mlist->menu_desc_ar;

					} else {

						$menu_name = $mlist->menu_name;

						$menu_desc = $mlist->menu_desc;

					}

					

					$menu_detail =$mlist;

					$menu_item_detail = '';

					$menu_item_name = '';

					$menu_item_desc = '';

					$menu_item_price = '';



					$food_detail = array();

					$food_data=array();

					$counter_popular =0 ;

					$menu_cat_detail = DB::table('menu_category')

					->leftJoin('menu_category_item', 'menu_category.menu_category_id', '=', 'menu_category_item.menu_category')

					->where('menu_category.rest_id', '=' ,$rest_id)

					->where('menu_category.menu_id', '=' ,$menu_id)

					->where('menu_category.menu_cat_status', '=' ,'1')

					->where('menu_category.area_id', '=' ,$area_id)

					->select('menu_category.*','menu_category_item.*')

					->orderBy('menu_category.menu_category_id', 'asc')

					->get();

					$menu_item_count =count($menu_cat_detail);

					$item_addons = array();

					if(!empty($menu_cat_detail))

					{

						foreach($menu_cat_detail as $menu_item)

						{

							$food_data['food_menuid'] = $menu_id;

							$food_data['food_id'] = $menu_item->menu_category_id;

							$food_data['food_popular'] = $menu_item->menu_cat_popular;

							$food_data['food_diet'] = $menu_item->menu_cat_diet;

							if ($lang_id == 'ar') {

								$food_data['food_name'] = $menu_item->menu_category_name_ar;

							}else{

								$food_data['food_name'] = $menu_item->menu_category_name;

							}							

							$food_data['food_price'] = $menu_item->menu_category_price;



					if(!empty($menu_item->menu_category_image)){

					$food_data['menu_image'] = trim(url('/').'/uploads/reataurant/menu/'.$menu_item->menu_category_image);

					}

					else

					{

					$food_data['menu_image'] =trim(url('/').'/uploads/reataurant/menu/menu.jpg');

					}

							

							if ($lang_id == 'ar') {

								$food_data['food_desc'] = $menu_item->menu_category_desc_ar;

							}else{

								$food_data['food_desc'] = $menu_item->menu_category_desc;

							}



							$food_data['food_subitem'] = $menu_item->menu_category_portion;

							$food_data['food_size_detail']='';

							$pp_ff = '';

							if($menu_item->menu_category_portion=='yes')

							{

								$food_size = DB::table('category_item')

								->where('rest_id', '=' ,$rest_id)

								->where('menu_id', '=' ,$menu_id)

								->where('menu_category', '=' ,$menu_item->menu_category_id)

								->where('menu_item_status', '=' ,'1')

								->select('*')

								->orderBy('menu_cat_itm_id', 'asc')

								->groupBy('menu_item_title')

								->get();

								if(count($food_size)>0)

								{

									$food_data['food_size_detail']=$food_size;

									$pp_ff =$food_size;

								}

								else

								{

									$food_data['food_size_detail']=array();

									$pp_ff =array();

								}

							}

							else

							{

								$food_data['food_size_detail']=array();

								$pp_ff =array();

							}

							$group_name= '';

							$group_list = '';

							$ff_popular = '';

							$popular_food_data=array();

				/*$addons = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

				->select('*')

				->orderBy('addon_id', 'asc')

				->groupBy('addon_groupname')

				->get();

				if(!empty($addons))

				{

					foreach($addons as $ad_list)

					{

						$group_name[]=$ad_list->addon_groupname;

						$ff['addon_gropname'] = $ad_list->addon_groupname;

						$addon_group = DB::table('menu_category_addon')

							->where('addon_restid', '=' ,$rest_id)

							->where('addon_menuid', '=' ,$menu_id)

							->where('addon_status', '=' ,'1')

							->where('addon_groupname', '=' ,$ad_list->addon_groupname)

							->where('addon_menucatid', '=' ,$menu_item->menu_category_id)

							->select('*')

							->orderBy('addon_id', 'asc')

							->get();

						$group_list[]=$addon_group;

						$addon_group_list ='';

						foreach($addon_group as $group_list_loop)

						{

							//$addon_group_list[]=array('')

							$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

							'addon_menucatid'=>$group_list_loop->addon_menucatid,

							'addon_groupname'=>$group_list_loop->addon_groupname,

							'addon_option'=>$group_list_loop->addon_option,

							'addon_name'=>$group_list_loop->addon_name,

							'addon_price'=>$group_list_loop->addon_price,

							'addon_status'=>$group_list_loop->addon_status

							);

						}

						$ff['addon_detail'] = $addon_group_list;

						$food_data['food_addon'][]=$ff;

						if($menu_item->menu_cat_popular==1)

						{

							$ff_popular['addon_detail'] = $addon_group_list;

							$ff_popular['addon_gropname'] = $ad_list->addon_groupname;

							$popular_food_data['food_addon'][] = $ff_popular;

						}

					}

				}

				else

				{

						$food_data['food_addon']=array();

						$ff['addon_gropname'] = '';

						if($menu_item->menu_cat_popular==1)

						{

							$ff_popular['addon_detail'] = '';

							$ff_popular['addon_gropname'] = '';

							$popular_food_data['food_addon']=array();

						}

					}*/

					$item_addons[] = array('group_name'=>$group_name,'group_list'=>$group_list);

					$food_detail[] = $food_data;

					if($menu_item->menu_cat_popular==1){

						$popular_item_count_11 ++;

						$counter_popular++;

						$popular_food_data['food_menuid'] = $menu_id;

						$popular_food_data['food_id'] = $menu_item->menu_category_id;

						$popular_food_data['food_popular'] = $menu_item->menu_cat_popular;

						$popular_food_data['food_diet'] = $menu_item->menu_cat_diet;

						if ($lang_id == 'ar') {

							$popular_food_data['food_name'] = $menu_item->menu_category_name_ar;

						} else {

							$popular_food_data['food_name'] = $menu_item->menu_category_name;	

						}

						

						$popular_food_data['food_price'] = $menu_item->menu_category_price;



					if(!empty($menu_item->menu_category_image)){

					$popular_food_data['menu_image'] = trim(url('/').'/uploads/reataurant/menu/'.$menu_item->menu_category_image);

					}

					else

					{

					$popular_food_data['menu_image'] =trim(url('/').'/uploads/reataurant/menu/menu.jpg');

					}

						if ($lang_id == 'ar') {

							$popular_food_data['food_desc'] = $menu_item->menu_category_desc_ar;

						} else {

							$popular_food_data['food_desc'] = $menu_item->menu_category_desc;

						}

						

						$popular_food_data['food_subitem'] = $menu_item->menu_category_portion;

						$popular_food_data['food_size_detail']=	$pp_ff;

						$popular_food_detail[] = $popular_food_data;

					}

				}

			}

			else

			{

				$food_detail=array();

			}

			$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail);

			if($counter_popular>0){

				$popular_item = array('popular_item_count'=>$popular_item_count_11,'popular_food_detail'=>$popular_food_detail);

			}

		}

	}

	else

	{

		$food_array=array();

	}

	if(isset($popular_item['popular_item_count']) && ($popular_item['popular_item_count']>0))

	{

		if ($lang_id == 'ar') {

			$popular_menu_name = "جمع";

		} else {

			$popular_menu_name = "Popular";

		}

		$new_populate_array = array('menu_id'=>'0','menu_name'=>$popular_menu_name,'food_counter'=>$popular_item['popular_item_count'],'food_detail'=>$popular_item['popular_food_detail']);

		array_unshift($food_array,$new_populate_array );

	}

	/** PROMOTIONS CODE START  **/

	$current_promo = array();

//if(($user_id>0) && ($user_id!=''))

//echo $rest_id;		{

	$promo_list = DB::table('promotion')

	->where('promo_restid', '=' ,$rest_id)

	->where('promo_status', '=' ,'1')

	->orderBy('promo_on', 'asc')

	->orderBy('promo_id', 'desc')

	->groupBy('promo_id')

	->get();

//$current_promo = array();

	if($promo_list)

	{

		foreach($promo_list as $plist)

		{

			$current_promo1 = array();

			if($plist->promo_on=='schedul')

			{

				$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

				if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

				{

					if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d')) && (empty($plist->promo_day)))

					{

						$current_promo1['promo_id'] = $plist->promo_id;

						$current_promo1['promo_restid'] = $plist->promo_restid;

						$current_promo1['promo_for'] = $plist->promo_for;

						$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

						$current_promo1['promo_on'] = $plist->promo_on;

						$current_promo1['promo_desc'] = $plist->promo_desc;

						$current_promo1['promo_mode'] = $plist->promo_mode;

						$current_promo1['promo_buy'] = $plist->promo_buy;

						$current_promo1['promo_value'] = $plist->promo_value;

						$current_promo[] = $current_promo1;

					}

					$db_day = explode(',',$plist->promo_day);

					if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

						&& (!empty($plist->promo_day))

						&& (in_array($day_name,$db_day))

					)

					{

						$current_promo1['promo_id'] = $plist->promo_id;

						$current_promo1['promo_restid'] = $plist->promo_restid;

						$current_promo1['promo_for'] = $plist->promo_for;

						$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

						$current_promo1['promo_on'] = $plist->promo_on;

						$current_promo1['promo_desc'] = $plist->promo_desc;

						$current_promo1['promo_mode'] = $plist->promo_mode;

						$current_promo1['promo_buy'] = $plist->promo_buy;

						$current_promo1['promo_value'] = $plist->promo_value;

						$current_promo1['promo_end'] = $plist->promo_end;

						$current_promo1['promo_start'] = $plist->promo_start;

						$current_promo[] = $current_promo1;

					}

				}

				elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day)))

				{

					$db_day = explode(',',$plist->promo_day);

					if(in_array($day_name,$db_day))

					{

						$current_promo1['promo_id'] = $plist->promo_id;

						$current_promo1['promo_restid'] = $plist->promo_restid;

						$current_promo1['promo_for'] = $plist->promo_for;

						$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

						$current_promo1['promo_on'] = $plist->promo_on;

						$current_promo1['promo_desc'] = $plist->promo_desc;

						$current_promo1['promo_mode'] = $plist->promo_mode;

						$current_promo1['promo_buy'] = $plist->promo_buy;

						$current_promo1['promo_value'] = $plist->promo_value;

						$current_promo1['promo_end'] = $plist->promo_end;

						$current_promo1['promo_start'] = $plist->promo_start;

						$current_promo[] = $current_promo1;

					}

				}

			}

			else

			{

				$current_promo1['promo_id'] = $plist->promo_id;

				$current_promo1['promo_restid'] = $plist->promo_restid;

				$current_promo1['promo_for'] = $plist->promo_for;

				$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

				$current_promo1['promo_on'] = $plist->promo_on;

				$current_promo1['promo_desc'] = $plist->promo_desc;

				$current_promo1['promo_mode'] = $plist->promo_mode;

				$current_promo1['promo_buy'] = $plist->promo_buy;

				$current_promo1['promo_value'] = $plist->promo_value;

				$current_promo1['promo_end'] = $plist->promo_end;

				$current_promo1['promo_start'] = $plist->promo_start;

				$current_promo[] = $current_promo1;

			}

		}

	}

/*else

{

	$current_promo = array();

}*/

/*}

else

{

	$current_promo = array();

}*/

/** PROMOTIONS CODE END  **/



$response['message'] = "Restaurant Detail";

$response['status']= 1;

$response['restaurant_detail'] = $rest_data;//$rest_listing;

$response['current_promo']  = $current_promo;//$Promostion listing;

$response['menu_detal'] = $food_array;

$response['menu_list'] = $menu_listnew;

}

else

{

$response['errorcode'] = "110002";

$response['status']= 0;

$response['message']="Restaurant Id: Invalid Restaurant ID";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



public function food_addcart()
{

$data['user_id'] =$user_id = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = $rest_id = Input::get("rest_id");

$data['menu_id'] = Input::get("menu_id");

$data['menu_catid'] = Input::get("food_id");

$data['menu_subcatid'] = Input::get("food_size_id");

$data['menu_addonid'] = Input::get("food_addonid");

$data['qty']  = Input::get("qty");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

$data['special_instruction'] = Input::get("special_instruction");

$data['lang_id']  = Input::get("lang_id");

if(empty($data['rest_id'])){

$response['errorcode'] = "140001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['menu_id'])){

$response['errorcode'] = "140002";

$response['status']= 0;

$response['message']="Menu Id:Required parameter missing";

}elseif(empty($data['menu_catid'])){

$response['errorcode'] = "140003";

$response['status']= 0;

$response['message']="Food Id:Required parameter missing";

}elseif(empty($data['qty'])){

$response['errorcode'] = "140004";

$response['status']= 0;

$response['message']="Food Quantity:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "140005";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "140006";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(!empty($data)){

$rest_cartid = '';

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

$cart_listing = $cart_listing->where('rest_id', '!=' ,trim($this->param['rest_id']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();

if($cart_listing)

{

$response['errorcode'] = "140007";

$response['status']= 0;

$response['message']="Adding this item to your cart will remove all other items that aren't from this restaurant. Do you wish to continue?";

}

else

{



$cart_rest_listing = DB::table('temp_cart')

->select('*')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('rest_id', '=' ,trim($this->param['rest_id']))

->where('menu_id', '=' ,trim($this->param['menu_id']))

->where('menu_catid', '=' ,trim($this->param['food_id']))

//->where('menu_subcatid', '=' ,trim($this->param['food_size_id']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->first();



$cartcount = count($cart_rest_listing);



if($cartcount>0)

{

	

$qty = (($cart_rest_listing->qty)+$this->param['qty']);

$temp_id = $cart_rest_listing->cart_id;

$rest_cartid = $cart_rest_listing->rest_cartid; 



DB::table('temp_cart')

->where('cart_id', trim($temp_id))

->update(['user_id' =>  trim($this->param['user_id']),

		  'guest_id' => trim($this->param['guest_id']),

		  'rest_id' =>  trim($this->param['rest_id']),

		  'menu_id' => trim($this->param['menu_id']),

		  'menu_catid' =>  trim($this->param['food_id']),

		  'menu_subcatid' =>  trim($this->param['food_size_id']),

		  'menu_addonid' =>  trim($this->param['food_addonid']),

		  'qty' =>  trim($qty),

		  'special_instruction' =>  trim($this->param['special_instruction'])

	 ]);



}

else

{



$cart_record = DB::table('temp_cart')

->select('*')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('rest_id', '=' ,trim($this->param['rest_id']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->first();



$cartcnt = count($cart_record);	



if($cartcnt>0){



$rest_cartid = $cart_record->rest_cartid;	



}else{



$rest_cartid = (date('ymdHis'));



}



$cart_data = new Temp_cart;

$cart_data->user_id	 = trim($this->param['user_id']);

$cart_data->rest_cartid	 = trim($rest_cartid);

$cart_data->guest_id = trim($this->param['guest_id']);

$cart_data->rest_id = trim($this->param['rest_id']);

$cart_data->menu_id = trim($this->param['menu_id']);

$cart_data->menu_catid = trim($this->param['food_id']);

$cart_data->menu_subcatid = trim($this->param['food_size_id']);

$cart_data->menu_addonid = trim($this->param['food_addonid']);

$cart_data->qty = trim($this->param['qty']);

$cart_data->special_instruction =  trim($this->param['special_instruction']);

$cart_data->deviceid = trim($this->param['deviceid']);

$cart_data->devicetype= trim($this->param['devicetype']);

$cart_data->save();



 }



$response['message'] = "Item add in cart sucessfully";

$response['status']= 1;

$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);



  }



 }

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Food_Update_Tocard

//	FUNCTION USE :  RESTAURANT FOOD ITEM ADD IN CART

//	FUNCTION ERRORCODE : 150000

/****************************************************/

public function food_updatecart()

{

$data['cart_id'] = Input::get("cart_id");

$data['rest_cartid']  =$rest_cartid = Input::get("rest_cartid");

$data['user_id'] = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = Input::get("rest_id");

$data['menu_id'] = Input::get("menu_id");

$data['menu_catid'] = Input::get("food_id");

$data['menu_subcatid'] =Input::get("menu_subcatid");

$data['menu_addonid'] =Input::get("menu_addonid");

$data['qty'] = Input::get("qty");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] =Input::get("devicetype");

$data['special_instruction'] = Input::get("special_instruction");

if(empty($data['cart_id'])){

$response['errorcode'] = "150000";

$response['status']= 0;

$response['message']="Cart Id:Required parameter missing";

}else if(empty($data['rest_id'])){

$response['errorcode'] = "150001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['menu_id'])){

$response['errorcode'] = "150002";

$response['status']= 0;

$response['message']="Menu Id:Required parameter missing";

}elseif(empty($data['menu_catid'])){

$response['errorcode'] = "150003";

$response['status']= 0;

$response['message']="Food Id:Required parameter missing";

}elseif(empty($data['qty'])){

$response['errorcode'] = "150004";

$response['status']= 0;

$response['message']="Food Quantity:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "150005";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "150006";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "150007";

$response['status']= 0;

$response['message']="Rest Cart Id:Required parameter missing";

}elseif(!empty($data)){

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();

if($cart_listing)

{

DB::table('temp_cart')

->where('cart_id', trim($this->param['cart_id']))

->where('rest_cartid', trim($this->param['rest_cartid']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->update(['user_id' =>  trim($this->param['user_id']),

'guest_id' => trim($this->param['guest_id']),

'rest_id' =>  trim($this->param['rest_id']),

'menu_id' => trim($this->param['menu_id']),

'menu_catid' =>  trim($this->param['food_id']),

'menu_subcatid' =>  trim($this->param['food_size_id']),

'menu_addonid' =>  trim($this->param['food_addonid']),

'qty' =>  trim($this->param['qty']),

'special_instruction' =>  trim($this->param['special_instruction'])

]);

$cart_detail = DB::table('temp_cart')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->get();

$response['message'] = "Cart Updated.";

$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

$response['status']= 1;

}

else

{

$response['errorcode'] = "150008";

$response['status']= 0;

$response['message']="This rest cart not available in our data base!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : Remove_food_favourite

//	FUNCTION USE :  app user remove favourite  restaurant food

//	FUNCTION ERRORCODE : 290000

/****************************************************/

public function remove_food_favourite()

{

$data['user_id'] = $user_id =Input::get("userid");

$data['rest_id'] = $rest_id = Input::get("rest_id");

$data['food_id'] = $food_id = Input::get("food_id");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "290001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(empty($data['rest_id'])){

$response['message']="Restaurant id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "290002";

}elseif(empty($data['food_id'])){

$response['message']="Food id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "290003";

}elseif(!empty($data)) {

$user_favourite = DB::table('favourite_food')

->where('favrest_restid', '=' ,$rest_id)

->where('favrest_userid', '=' ,$user_id)

->where('favrest_menucatid', '=' ,$food_id)

->select('*')

->orderBy('favrest_userid', 'asc')

->get();

if($user_favourite)

{

DB::table('favourite_food')

->where('favrest_restid', '=' ,$rest_id)

->where('favrest_userid', '=' ,$user_id)

->where('favrest_menucatid', '=' ,$food_id)

->update(['favrest_status' => '0'

]);

}

//$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "Restaurant food remove as favourite sucessfully.";

$response['status']= 1;

//$response['data'] = $user_data;

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : Food_getdetail

//	FUNCTION USE :  RESTAURANT FOOD REMOVE FROM CART

//	FUNCTION ERRORCODE : 170000

/****************************************************/

public function food_getcartdeatil()

{

DB::enableQueryLog();

$data['user_id'] = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = Input::get("rest_id");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

$data['rest_cartid'] =$rest_cartid = Input::get("rest_cartid");

$data['lang_id'] = $lang_id = Input::get("lang_id");





if(empty($data['rest_id'])){

$response['errorcode'] = "170001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "170002";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "170003";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "170004";

$response['status']= 0;

$response['message']="Rest Cart Id:Required parameter missing";

}elseif(!empty($data)){



$rest_id =trim($this->param['rest_id']);

$user_id =trim($this->param['user_id']);



$cart_food_id='';

$cart_detail ='';

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('guest_id', '=' ,trim($this->param['guest_id']));

$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

$cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();



if($cart_listing)

{

$delivery_fee = 0;

$rest_detail = DB::table('restaurant')

->where('rest_id', '=' ,$rest_id)

->where('rest_status', '!=' , 'INACTIVE')

->orderBy('rest_id', 'asc')

->groupBy('rest_id')

->get();

$promo_list = DB::table('promotion')

->where('promo_restid', '=' ,$rest_id)

->where('promo_status', '=' ,'1')

->orderBy('promo_buy', 'desc')

->groupBy('promo_id')

->get();

$service_charge = $rest_detail[0]->rest_servicetax;

if($cart_listing[0]->order_type=='delivery')

{

$delivery_fee = $rest_detail[0]->rest_mindelivery;

}



$sub_total_cart = 0;

$json_data_session = '';

$addon_list ='';

$food_current_promo = '';

foreach($cart_listing as $clist)

{

/********FOOD DETAILS /******/

$rest_id  = $clist->rest_id;

$menu_id = $clist->menu_id;

$menu_cate_id =$clist->menu_catid;

$menu_subcate_id = $clist->menu_subcatid;

$addon_id = $clist->menu_addonid;

$coupon_amount = $clist->coupon_amount;

$coupon_status = $clist->coupon_status;



$foodsize_price =0;

$food_addonPrice = 0;

$total_price =0 ;

$item_name_cc = '';

$item_price_cc = '';

$offer_on_menu_item = '';

/******************* CHECK PROMOTION FOR FOOD AND CALCULATE AMT ***************************/

	//if(($user_id>0) && ($user_id!='')){

foreach($promo_list as $plist )

{

	if(($plist->promo_for=='each_food') && ($clist->menu_catid==$plist->promo_menu_item))

	{

		$current_promo1 = '';

		$food_cart_amount[] = $total_price;

		$food_cart_qty[] =$clist->qty;

		if($plist->promo_on=='schedul')

		{

			$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

			$db_day = '';

			if(!empty($plist->promo_day))

			{

				$db_day = explode(',',$plist->promo_day);

			}

			if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

			{

				if(

					($plist->promo_start<=date('Y-m-d')) &&

					($plist->promo_end>=date('Y-m-d')) &&

					(empty($plist->promo_day)) &&

					($plist->promo_buy<=$clist->qty)

				)

				{

					$current_promo1['promo_id'] = $plist->promo_id;

					$current_promo1['promo_restid'] = $plist->promo_restid;

					$current_promo1['promo_for'] = $plist->promo_for;

					$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

					$current_promo1['promo_on'] = $plist->promo_on;

					$current_promo1['promo_desc'] = $plist->promo_desc;

					$current_promo1['promo_mode'] = $plist->promo_mode;

					$current_promo1['promo_buy'] = $plist->promo_buy;

					$current_promo1['promo_value'] = $plist->promo_value;

					$current_promo1['promo_end'] = $plist->promo_end;

					$current_promo1['promo_start'] = $plist->promo_start;

					$food_current_promo[] = $current_promo1;

					$offer_on_menu_item = $plist->promo_desc;

				}

				if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

					&& (!empty($plist->promo_day))

					&& (in_array($day_name,$db_day))

				)

				{

					$array_amt[]=$plist->promo_buy;

					$current_promo1['promo_id'] = $plist->promo_id;

					$current_promo1['promo_restid'] = $plist->promo_restid;

					$current_promo1['promo_for'] = $plist->promo_for;

					$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

					$current_promo1['promo_on'] = $plist->promo_on;

					$current_promo1['promo_desc'] = $plist->promo_desc;

					$current_promo1['promo_mode'] = $plist->promo_mode;

					$current_promo1['promo_buy'] = $plist->promo_buy;

					$current_promo1['promo_value'] = $plist->promo_value;

					$current_promo1['promo_end'] = $plist->promo_end;

					$current_promo1['promo_start'] = $plist->promo_start;

					$food_current_promo[] = $current_promo1;

					$offer_on_menu_item = $plist->promo_desc;

				}

			}

			elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)))

			{

				$db_day = explode(',',$plist->promo_day);

				if(in_array($day_name,$db_day))

				{

					$array_amt[]=$plist->promo_buy;

					$current_promo1['promo_id'] = $plist->promo_id;

					$current_promo1['promo_restid'] = $plist->promo_restid;

					$current_promo1['promo_for'] = $plist->promo_for;

					$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

					$current_promo1['promo_on'] = $plist->promo_on;

					$current_promo1['promo_desc'] = $plist->promo_desc;

					$current_promo1['promo_mode'] = $plist->promo_mode;

					$current_promo1['promo_buy'] = $plist->promo_buy;

					$current_promo1['promo_value'] = $plist->promo_value;

					$current_promo1['promo_end'] = $plist->promo_end;

					$current_promo1['promo_start'] = $plist->promo_start;

					$food_current_promo[] = $current_promo1;

					$offer_on_menu_item = $plist->promo_desc;

				}

			}

		}

		elseif($plist->promo_on=='qty')

		{

			if(($plist->promo_buy<=$clist->qty))

			{

				$current_promo1['promo_id'] = $plist->promo_id;

				$current_promo1['promo_restid'] = $plist->promo_restid;

				$current_promo1['promo_for'] = $plist->promo_for;

				$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

				$current_promo1['promo_on'] = $plist->promo_on;

				$current_promo1['promo_desc'] = $plist->promo_desc;

				$current_promo1['promo_mode'] = $plist->promo_mode;

				$current_promo1['promo_buy'] = $plist->promo_buy;

				$current_promo1['promo_value'] = $plist->promo_value;

				$current_promo1['promo_end'] = $plist->promo_end;

				$current_promo1['promo_start'] = $plist->promo_start;

				$food_current_promo[] = $current_promo1;

				$offer_on_menu_item = $plist->promo_desc;

			}

		}

	}

}

	//}

/*************************** END ******************************/

$menu_list = DB::table('menu')

->where('restaurant_id', '=' ,$clist->rest_id)

->where('menu_id', '=' ,$clist->menu_id)

->where('menu_status', '=' ,'1')

->orderBy('menu_order', 'asc')

->get();



// dd(DB::getQueryLog());

$menu_array = '';

$food_array = '';

if(!empty($menu_list))

{

	$menu_id = $menu_list[0]->menu_id;

	if ($lang_id == 'ar') {

		$menu_name = $menu_list[0]->menu_name_ar;

	} else {

		$menu_name = $menu_list[0]->menu_name;	

	}

	

	$menu_item_count = 1;

	$food_detail=array();

	$menu_cat_detail = DB::table('menu_category')

	->where('menu_category.rest_id', '=' ,$rest_id)

	->where('menu_category.menu_id', '=' ,$menu_id)

	->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

	->where('menu_category.menu_cat_status', '=' ,'1')

	->select('menu_category.*')

	->orderBy('menu_category.menu_category_id', 'asc')

	->get();

	$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

	$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

	$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

	if ($lang_id == 'ar') {

		$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name_ar;

	} else {

		$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

	}

	

	$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

	if ($lang_id == 'ar') {

		$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc_ar;

	} else {

		$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

	}

	

	$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

	$food_detail['food_subitem_title']='';

	if($menu_cat_detail[0]->menu_category_portion=='yes')

	{

		$food_detail['food_subitem_title'] = 'Choose a size';

		$food_size = DB::table('category_item')

		->where('rest_id', '=' ,$rest_id)

		->where('menu_id', '=' ,$menu_id)

		->where('menu_category', '=' ,$menu_cate_id)

		->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

		->where('menu_item_status', '=' ,'1')

		->select('*')

		->orderBy('menu_cat_itm_id', 'asc')

		->groupBy('menu_item_title')

		->get();

		$food_detail['food_size_detail']=$food_size;

		$item_price_cc =$food_size[0]->menu_item_price;

		$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

		$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

	}

	else

	{

		$food_detail['food_size_detail']=array();

		$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

	}

	$addon_list='';

	if(!empty($addon_id)){

		$addons = DB::table('menu_category_addon')

		->where('addon_restid', '=' ,$rest_id)

		->where('addon_menuid', '=' ,$menu_id)

		->where('addon_status', '=' ,'1')

		->where('addon_menucatid', '=' ,$menu_cate_id)

		->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

		->select('*')

		->orderBy('addon_id', 'asc')

		->groupBy('addon_groupname')

		->get();

		if(!empty($addons))

		{

			foreach($addons as $ad_list)

			{

				$option_type = 0;

				if(substr($ad_list->addon_groupname, -1)=='*')

				{

					$option_type = 1;

				}

				$group_name[]=$ad_list->addon_groupname;

				if ($lang_id == 'ar') {

					$ff['addon_gropname'] = $ad_list->addon_groupname_ar;

				} else {

					$ff['addon_gropname'] = $ad_list->addon_groupname;

				}

				

				$ff['addon_type'] = $ad_list->addon_option;

				$ff['addon_mandatory_or_not'] = $option_type;

				$addon_group = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_groupname', '=' ,$ad_list->addon_groupname)

				->where('addon_menucatid', '=' ,$menu_cate_id)

				->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

				->select('*')

				->orderBy('addon_id', 'asc')

				->get();

				$group_list[]=$addon_group;

				$addon_group_list ='';

				foreach($addon_group as $group_list_loop)

				{

						//$addon_group_list[]=array('')

					if ($lang_id == 'ar') {

						$addon_groupname = $group_list_loop->addon_groupname_ar;

						$addon_name = $group_list_loop->addon_name_ar;

					} else {

						$addon_groupname = $group_list_loop->addon_groupname;

						$addon_name = $group_list_loop->addon_name;

					}

					$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

						'addon_menucatid'=>$group_list_loop->addon_menucatid,

						'addon_groupname'=>$addon_groupname,

						'addon_option'=>$group_list_loop->addon_option,

						'addon_name'=>$addon_name,

						'addon_price'=>$group_list_loop->addon_price,

						'addon_status'=>$group_list_loop->addon_status

					);

					$addon_list[]=array('name'=>$group_list_loop->addon_name,

						'price'=>$group_list_loop->addon_price,

						'id'=>$group_list_loop->addon_id);

					$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

					$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

				}

				$ff['addon_detail'] = $addon_group_list;

				$food_detail['food_addon'][]=$ff;

			}

		}

	}

	else

	{

		$food_detail['food_addon'] = array();

	}





	$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);



	//return $food_array ;

}

/******** FOOD DETAIL END ****/

$sub_total_cart = ($sub_total_cart+($total_price*$clist->qty));

$cart_detail[] = array(

	"cart_id"=> $clist->cart_id,

	"rest_cartid"=> $clist->rest_cartid,

	"user_id" => $clist->user_id,

	"guest_id" => $clist->guest_id,

	"rest_id"=> $clist->rest_id,

	"menu_id"=> $clist->menu_id,

	"food_id"=> $clist->menu_catid,

	"food_size_id"=> $clist->menu_subcatid,

	"food_addonid"=> $clist->menu_addonid,

	"qty"=> $clist->qty,

	"special_instruction"=>$clist->special_instruction,

	"deviceid"=> $clist->deviceid,

	"devicetype"=> $clist->devicetype,

	"food_detail"=> $food_array,

	"foodsize_price"=>$foodsize_price,

	"food_addonPrice"=>$food_addonPrice,

	"total_price"=>$total_price

);



//return 	$cart_detail;



$json_data_session[$clist->cart_id] =	array("rowId"=>$clist->cart_id,"id"=>$clist->menu_catid,	   "name"=>$item_name_cc,"qty"=>$clist->qty,"price"=>$item_price_cc,"instruction"=>$clist->special_instruction,			    "options"=> array ( "menu_catid"=>$clist->menu_catid,

	"rest_id"=>$clist->rest_id,

	"menu_id"=>$clist->menu_id,

	"option_name"=> $clist->menu_subcatid,

	"addon_data"=>$addon_list,

	"offer_name"=>$offer_on_menu_item),

"tax"=>'',

"subtotal"=>$item_price_cc

);

}

/*print_r($food_current_promo);

print_r($food_amount);*/

$serv_tax  = (($sub_total_cart*$service_charge)/100);

/** PROMOTIONS CODE START FOR ORDER TOTAL AMOUNT **/

$current_promo = '';

$offer_applied_on_this_cart='';

//if(($user_id>0) && ($user_id!='')){

if( ($sub_total_cart>=$rest_detail[0]->rest_min_orderamt))

{

$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

/*$promo_list = DB::table('promotion')

			->where('promo_restid', '=' ,$rest_id)

			->where('promo_status', '=' ,'1')

			->where('promo_for', '=' ,'order')

			->orderBy('promo_buy', 'desc')

			->groupBy('promo_id')

			->get();*/

	// dd(DB::getQueryLog());

	//print_r($promo_list);

			//

//	exit;

			$current_promo = '';

			$array_amt = '';

			foreach($promo_list as $plist )

			{

				if($plist->promo_for=='order')

				{

					$current_promo1 = '';

					if($plist->promo_on=='schedul')

					{

						$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));

						$db_day = '';

						if(!empty($plist->promo_day))

						{

							$db_day = explode(',',$plist->promo_day);

						}

						if(($plist->promo_start!='0000-00-00') && ($plist->promo_end!='0000-00-00'))

						{

							if(

								($plist->promo_start<=date('Y-m-d')) &&

								($plist->promo_end>=date('Y-m-d')) &&

								(empty($plist->promo_day)) &&

								((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart)))

							)

							{

								$array_amt[]=$plist->promo_buy;

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo1['promo_end'] = $plist->promo_end;

								$current_promo1['promo_start'] = $plist->promo_start;

								$current_promo[] = $current_promo1;

							}

							if(($plist->promo_start<=date('Y-m-d')) && ($plist->promo_end>=date('Y-m-d'))

								&& (!empty($plist->promo_day))

								&& (in_array($day_name,$db_day) &&

									((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

							)

							{

								$array_amt[]=$plist->promo_buy;

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo1['promo_end'] = $plist->promo_end;

								$current_promo1['promo_start'] = $plist->promo_start;

								$current_promo[] = $current_promo1;

							}

						}

						elseif(($plist->promo_start=='0000-00-00') && ($plist->promo_end=='0000-00-00') && (!empty($plist->promo_day))&& (in_array($day_name,$db_day)) &&

							((($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))))

						{

							$db_day = explode(',',$plist->promo_day);

							if(in_array($day_name,$db_day))

							{

								$array_amt[]=$plist->promo_buy;

								$current_promo1['promo_id'] = $plist->promo_id;

								$current_promo1['promo_restid'] = $plist->promo_restid;

								$current_promo1['promo_for'] = $plist->promo_for;

								$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

								$current_promo1['promo_on'] = $plist->promo_on;

								$current_promo1['promo_desc'] = $plist->promo_desc;

								$current_promo1['promo_mode'] = $plist->promo_mode;

								$current_promo1['promo_buy'] = $plist->promo_buy;

								$current_promo1['promo_value'] = $plist->promo_value;

								$current_promo1['promo_end'] = $plist->promo_end;

								$current_promo1['promo_start'] = $plist->promo_start;

								$current_promo[] = $current_promo1;

							}

						}

					}

					else

					{

						if(($plist->promo_buy==0) || ($plist->promo_buy<=$sub_total_cart))

						{

							$array_amt[]=$plist->promo_buy;

							$current_promo1['promo_id'] = $plist->promo_id;

							$current_promo1['promo_restid'] = $plist->promo_restid;

							$current_promo1['promo_for'] = $plist->promo_for;

							$current_promo1['promo_menu_item'] = $plist->promo_menu_item;

							$current_promo1['promo_on'] = $plist->promo_on;

							$current_promo1['promo_desc'] = $plist->promo_desc;

							$current_promo1['promo_mode'] = $plist->promo_mode;

							$current_promo1['promo_buy'] = $plist->promo_buy;

							$current_promo1['promo_value'] = $plist->promo_value;

							$current_promo1['promo_end'] = $plist->promo_end;

							$current_promo1['promo_start'] = $plist->promo_start;

							$current_promo[] = $current_promo1;

						}

					}

				}

			}

		}

		else

		{

			$current_promo = array();

			$offer_applied_on_this_cart='';

		}

/*}

else

{

$current_promo = array();

$offer_applied_on_this_cart= '';

}*/

$offer_amt = 0;

$cal_sub_totla = $sub_total_cart;

if((count($current_promo)>0) && (!empty($array_amt))) {

$maxs = max($array_amt);

$key_name = array_search ($maxs, $array_amt);

$k=0;$i=0;

foreach($current_promo as $pval)

{

if($key_name==$k)

{

	if($pval['promo_mode']=='$')

	{

		$i++;

		$offer_applied_on_this_cart=$pval['promo_desc'];

		$offer_amt = ($offer_amt+$pval['promo_value']);

		$cal_sub_totla = ($cal_sub_totla- $pval['promo_value']);

	}

	if(($pval['promo_mode']=='%') )

		{	$i++;

			$offer_applied_on_this_cart=$pval['promo_desc'];

			$promo_val_per = (($cal_sub_totla*$pval['promo_value'])/100);

			$cal_sub_totla = ($cal_sub_totla - $promo_val_per);

			$offer_amt = ($offer_amt+$promo_val_per);

		}

		if(($pval['promo_mode']=='0') )

		{

			$i++;

			$offer_applied_on_this_cart=$pval['promo_desc'];

		}

	}

	$k++;

}

}

else{

$offer_applied_on_this_cart='';

$current_promo = array();

}

/** PROMOTIONS CODE END  **/

/******************  FOOD ITEM  */

/*$food_disocunt

$food_current_promo*/

$food_offer_amt = '';

$offer_applied_on_food= '';

if(!empty($food_current_promo) && (count($food_current_promo)>0))

{

$k=0;$i=0;

foreach($food_current_promo as $pval)

{

$item_price = $food_cart_amount[$k];

$item_qty = $food_cart_qty[$k];

$cal_sub_total=number_format(($item_price*$item_qty),2);

if($pval['promo_mode']=='$')

{

	$i++;

	$offer_applied_on_food=$pval['promo_desc'];

	$food_offer_amt = ($offer_amt+$pval['promo_value']);

}

if(($pval['promo_mode']=='%') )

	{	$i++;

		$offer_applied_on_food=$pval['promo_desc'];

		$promo_val_per = (($cal_sub_total*$pval['promo_value'])/100);

		$food_offer_amt = ($offer_amt+$promo_val_per);

	}

	if(($pval['promo_mode']=='0') )

	{

		$i++;

		$offer_applied_on_food=$pval['promo_desc'];

	}

	$k++;

}

}

else

{

$food_current_promo= array();

}

/*******************  END  ****/

if(($sub_total_cart-$offer_amt-$food_offer_amt)<$delivery_fee)

{

$total_amt =($delivery_fee+$serv_tax);

}

else

{

$aa_total = ($sub_total_cart-$offer_amt-$food_offer_amt)+$serv_tax;

$total_amt = str_replace( ',', '',$aa_total);

}





DB::table('temp_cart')

->where('rest_cartid', '=' ,$rest_cartid)

->update(['cart_detail' => (json_encode($json_data_session)),

'offer_detail'=> (json_encode($current_promo)),

'offer_amt'=> trim($offer_amt),

'offer_applied'=>$offer_applied_on_this_cart,

'food_promo_list'=>json_encode($food_current_promo),

'food_offer_applied'=>json_encode($offer_applied_on_food),

'service_tax_amt'=>trim($serv_tax),

'delivery_amt'=> trim($delivery_fee),

'subtotal'=> trim($sub_total_cart),

'total_amt'=> str_replace( ',', '',$total_amt)

]);



$coupon_amount = $clist->coupon_amount;

$coupon_status = $clist->coupon_status;



$response['message'] = "All deatil of cart data";

$response['cart_detail'] = $cart_detail;

$response['other_detail'] = array(

'subtotal'=> trim($sub_total_cart),

'rest_service' =>$service_charge,

'delivery_fee'=>number_format($delivery_fee,2),

'service_tax'=>number_format($serv_tax,2),

'promo_list'=>$current_promo,

'discount_amt'=>number_format($offer_amt,2),

'offer_applied_on_this_cart'=>$offer_applied_on_this_cart,

'food_promo_list'=>$food_current_promo,

'offer_applied_on_item'=>$offer_applied_on_food,

'total_amt'=>str_replace( ',', '',number_format($total_amt,2)),

'coupon_status' => $coupon_status,

'coupon_amount' => $coupon_amount,

);

$response['status']= 1;

}

else

{

$response['errorcode'] = "170005";

$response['status']= 0;

$response['message']="Your cart is empty!";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





function generateRandomStringfor_verification($length = 6) {

$characters = '0123456789';

$charactersLength = strlen($characters);

$randomString = '';

for ($i = 0; $i < $length; $i++) {

$randomString .= $characters[rand(0, $charactersLength - 1)];

}

return $randomString;

}





function get_save_order(Request $request)

{



$response = array();

$data['user_id'] = $user_id = Input::get("user_id");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['guest_name'] = $guest_name = Input::get("guest_name");

$data['guest_email'] = $guest_email = Input::get("guest_email");

$data['guest_mobileno'] = $guest_mobileno = Input::get("guest_mobileno");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['rest_cartid'] = $rest_cartid = Input::get("rest_cartid");

$data['rest_id'] = $rest_id = Input::get("rest_id");

$data['paymenttype'] = $paymenttype = Input::get("paymenttype");

$data['order_tip'] = $order_tip = Input::get("order_tip");

$data['ordertype'] = $ordertype = Input::get("ordertype");

$data['payment_mode'] = $payment_mode = Input::get("payment_mode");

$data['card_number'] = $card_number = Input::get("card_number");

$data['card_name'] = $card_name = Input::get("card_name");

$data['card_expdate'] = $card_expdate = Input::get("card_expdate");

$data['card_cvv'] = $card_cvv = Input::get("card_cvv");

$data['order_date'] = $order_date = Input::get("order_date");

$data['order_time'] = $order_time = Input::get("order_time");

$data['order_delivery_address'] = $order_delivery_address = Input::get("order_delivery_address");

$data['order_delivery_city'] = $order_delivery_city = "0";

$data['order_delivery_state'] = $order_delivery_state = "0";

$data['order_delivery_country'] = $order_delivery_country = "0";

$data['order_delivery_zipcode'] = $order_delivery_zipcode = "0";



$data['order_lat'] = $order_lat = Input::get("order_lat");

$data['order_long'] = $order_long = Input::get("order_long");



$data['order_delivery_contact'] = $order_delivery_contact = Input::get("order_delivery_contact");

$data['order_instruction'] = $order_instruction = Input::get("order_instruction");

$data['partial_pay_amt'] = $partial_pay_amt = Input::get("partial_pay_amt");

$data['partial_remain_amt'] = $partial_remain_amt = Input::get("partial_remain_amt");

if(empty($data['rest_id'])){

$response['errorcode'] = "400001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "400002";

$response['status']= 0;

$response['message']="Cart Id : Required parameter missing";

}elseif(empty($data['paymenttype'])){

$response['errorcode'] = "400003";

$response['status']= 0;

$response['message']="Payment Type: Required parameter missing";

}

elseif(empty($data['ordertype'])){

$response['errorcode'] = "400004";

$response['status']= 0;

$response['message']="Order Type: Required parameter missing";

}

elseif(empty($data['payment_mode'])){

$response['errorcode'] = "400005";

$response['status']= 0;

$response['message']="Payment Mode: Required parameter missing";

}

elseif(($this->param['asap_or_not']== '0') && (empty($data['order_date']))){

$response['errorcode'] = "400006";

$response['status']= 0;

$response['message']="Order Date: Required parameter missing";

}

elseif(($this->param['asap_or_not']== '0') && (empty($data['order_time']))){

$response['errorcode'] = "400007";

$response['status']= 0;

$response['message']="Order Time: Required parameter missing";

}

elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_address']))){

$response['errorcode'] = "400008";

$response['status']= 0;

$response['message']="Delivery Address: Required parameter missing";

}

// elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_city']))){

// 	$response['errorcode'] = "400009";

// 	$response['status']= 0;

// 	$response['message']="Delivery City: Required parameter missing";

// }

// elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_state']))){

// 	$response['errorcode'] = "4000010";

// 	$response['status']= 0;

// 	$response['message']="Delivery State: Required parameter missing";

// }

// elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_country']))){

// 	$response['errorcode'] = "4000011";

// 	$response['status']= 0;

// 	$response['message']="Delivery Country: Required parameter missing";

// }

// elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_delivery_zipcode']))){

// 	$response['errorcode'] = "4000012";

// 	$response['status']= 0;

// 	$response['message']="Delivery Postcode: Required parameter missing";

// }

/*lat long start*/

elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_lat']))){

$response['errorcode'] = "4000014";

$response['status']= 0;

$response['message']="Delivery Latitude: Required parameter missing";

}

elseif(($this->param['ordertype']== 'Delivery') &&(empty($data['order_long']))){

$response['errorcode'] = "4000015";

$response['status']= 0;

$response['message']="Delivery Longitude: Required parameter missing";

}

/*lat long end*/

elseif(empty($data['order_delivery_contact'])){

$response['errorcode'] = "4000013";

$response['status']= 0;

$response['message']="Contact Number: Required parameter missing";

}

elseif (!empty($data)){



$user_detail = DB::table('users')->select('*')->where('id', '=' ,$user_id)->get();

$order_id = ''; 

$unq_id = '';



DB::enableQueryLog();

$cart_detail ='';

$cart_listing = DB::table('temp_cart');

$cart_listing = $cart_listing->select('*');

$cart_listing = $cart_listing->where('user_id', '=' ,trim($this->param['user_id']));

$cart_listing = $cart_listing->where('rest_id', '=' ,trim($this->param['rest_id']));

// $cart_listing = $cart_listing->where('rest_cartid', '=' ,trim($this->param['rest_cartid']));

$cart_listing = $cart_listing->where('deviceid', '=' ,trim($this->param['deviceid']));

$cart_listing = $cart_listing->where('devicetype', '=' ,trim($this->param['devicetype']));

$cart_listing = $cart_listing->get();



//$cart_listing = DB::table('temp_cart')->select->get();



$user_detail = DB::table('users')->select('*')->where('id', '=' ,$user_id)->get();

$rest_detail = DB::table('restaurant')->select('*')->where('rest_id', '=' ,$rest_id)->get();



if(count($cart_listing)>0){



$cart =$cart_listing[0]->cart_detail;

$promo_detail =$cart_listing[0]->offer_detail;

$delivery_fee = $cart_listing[0]->delivery_amt;

$promo_amt_cal= $cart_listing[0]->offer_amt;

$sub_total= $cart_listing[0]->subtotal;

$service_tax = $cart_listing[0]->service_tax_amt;

$total_amt = $cart_listing[0]->total_amt;

$order_promo_applied = $cart_listing[0]->offer_applied; ;

$order_food_promo = $cart_listing[0]->food_promo_list; ;

$order_food_promo_applied =$cart_listing[0]->food_offer_applied; ;

$asap_or_not = trim($this->param['asap_or_not']);

$order_typetime = '';

if($asap_or_not==0)

{

$order_typetime = 'later';

}

if($asap_or_not==1)

{

$order_typetime = 'asp';

}

$orderdata = new Order;

$orderdata->rest_id	           =  $rest_id;

$orderdata->order_restcartid   = $cart_listing[0]->rest_cartid;



$coupon_code = Input::get("coupon_code");



if($user_id>0)

{

$order_username   = $user_detail[0]->name;

$orderdata->coupon_code	       = $coupon_code;

$orderdata->user_type	       = '1';

$orderdata->user_id	           = $user_id ;

$orderdata->order_fname	       = $user_detail[0]->name;

$orderdata->order_lname		   = $user_detail[0]->lname;

$orderdata->order_email		   = $user_detail[0]->email;

$orderdata->order_tel		   = $user_detail[0]->user_mob;

$orderdata->order_carrier_email = $user_detail[0]->carrier_email;

$orderdata->order_address	   = $user_detail[0]->user_address;

$orderdata->order_city	       = $user_detail[0]->user_city;

$orderdata->order_pcode		   = $user_detail[0]->user_zipcode;

$orderdata->order_lat		   = $order_lat;

$orderdata->order_long		   = $order_long;

$orderdata->verification_code  = trim($this->generateRandomStringfor_verification(6));



}

else

{

$orderdata->coupon_code	       = $coupon_code;

$orderdata->user_type	       = '0';

$order_username   = $guest_name;

$carrier_email = '';

$orderdata->order_guestid	   = $guest_id;

$orderdata->order_fname	       = $guest_name;

$orderdata->order_lname		   = '';

$orderdata->order_carrier_email = $carrier_email;

$orderdata->order_email		   = $guest_email;

$orderdata->order_tel		   = $guest_mobileno;

$orderdata->order_address	   = '';

$orderdata->order_city	       = '';

$orderdata->order_pcode		   = '';

$orderdata->order_lat		   = $order_lat;

$orderdata->order_long		   = $order_long;

$orderdata->verification_code  = trim($this->generateRandomStringfor_verification(6));

}



$orderdata->order_type		   = $ordertype;

$orderdata->order_typetime	   = $order_typetime;

$orderdata->order_pickdate	   = $order_date;

$orderdata->order_picktime	   = $order_time;

$orderdata->order_pickarea	   = '';

$orderdata->order_instruction  = $order_instruction;

$orderdata->order_status       = '1';

$orderdata->order_carditem     = $cart;

$orderdata->order_promo_detail  = $promo_detail;

$orderdata->order_deliveryfee =$delivery_fee;

//$orderdata->order_create	   = date('Y-m-d');

$orderdata->order_create	   = date('Y-m-d H:i:s');

$orderdata->order_promo_mode = '';

$orderdata->order_promo_val = '';

$orderdata->order_promo_cal = $promo_amt_cal;

$orderdata->order_subtotal_amt = $sub_total;

$orderdata->order_service_tax = $service_tax;



if($payment_mode==1){

$payment_pay_status = '1';

$orderdata->order_pmt_type= $payment_mode;

$orderdata->order_partial_percent = '0.00';

$orderdata->order_partial_payment = '0.00';

$orderdata->order_partial_remain = '0.00';

$orderdata->order_paid_amt   = $total_amt;

$orderdata->order_remaing_amt	= '0.00';

}

else

{

$orderdata->order_pmt_type= '2';

$payment_pay_status = '2';

$orderdata->order_partial_percent = $rest_detail[0]->rest_partial_percent;

$orderdata->order_partial_payment = $partial_pay_amt;

$orderdata->order_partial_remain = $partial_remain_amt;

$orderdata->order_paid_amt   =  $partial_pay_amt;

$orderdata->order_remaing_amt	= $partial_remain_amt;

}

if($paymenttype=='Cash')

{

$orderdata->order_paid_amt   =  '0.00';

$orderdata->order_remaing_amt	= $total_amt;

}

/***** PARTIAL PAYMENT END*****/

$orderdata->order_total = $total_amt;

$orderdata->order_tip =  $order_tip;

$orderdata->order_pmt_method= $paymenttype;

$orderdata->order_deliveryadd1= $order_delivery_address;

$orderdata->order_deliveryadd2= '';

$orderdata->order_deliverysurbur= $order_delivery_city;

$orderdata->order_deliverypcode= $order_delivery_zipcode;

$orderdata->order_deliverynumber=  $order_delivery_contact;

$orderdata->order_promo_applied = $order_promo_applied ;

$orderdata->order_food_promo = $order_food_promo ;

$orderdata->order_food_promo_applied =$order_food_promo_applied;

$orderdata->order_device =trim($this->param['devicetype']);

$orderdata->order_deviceid =trim($this->param['deviceid'])	;

$orderdata->order_devicename =trim($this->param['device_name']);

$orderdata->order_device_os=trim($this->param['os_name']);

$email_subject = 'Your order received.';

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'3')

->get();

$email_content =$email_content_detail[0]->email_content;

if($paymenttype=='Paypal')

{

$orderdata->save();

$order_id = $orderdata->order_id;

$paydata = new Order_payment;

$paydata->pay_orderid = $order_id;

$paydata->pay_method =$paymenttype;

$paydata->pay_userid = $user_id ;

$paydata->pay_tx = '';

$paydata->pay_st = '';

$paydata->pay_amt ='';

$paydata->pay_cc = '';

$paydata->pay_status=$payment_pay_status;

$paydata->save();

$pay_id = $paydata->pay_id;

$response['order_id']=$order_id;

$response['status']= 1;

$response['message']="Order save successfully!";

}

elseif($paymenttype=='Cash')

{



$unq_id = trim($this->order_uniqueid(6));

$orderdata->order_uniqueid= trim($unq_id);

$orderdata->save();

$order_id = $orderdata->order_id;

$paydata = new Order_payment;

$paydata->pay_orderid = $order_id;

$paydata->pay_method =$paymenttype;

$paydata->pay_userid = $user_id ;

$paydata->pay_tx = '';

$paydata->pay_st = '';

$paydata->pay_amt = $total_amt;

$paydata->pay_cc = 'USD';

$paydata->pay_status=$payment_pay_status;

$paydata->save();

$pay_id = $paydata->pay_id;

DB::table('temp_cart')->where('rest_cartid', '=', trim($rest_cartid))->delete();

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'3')

->get();



//$email_content = "hello raveena jaodn";

$email_content = $email_content_detail[0]->email_content;

$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

$replaceArray = array($order_id, $unq_id);

$msg = str_replace($searchArray, $replaceArray, $email_content);



$response['order_id']=$order_id;

$response['order_uniqueid']=$unq_id;

$response['status']= 1;

$response['message']='Your order received';

}

elseif($paymenttype=='Card')

{

$order_detail = DB::table('order')->select('*')->where('order_restcartid', '=' ,$rest_cartid)->get();

if($order_detail)

{

		//$order_id = $this->param['order_id'];

$order_id = $order_detail[0]->order_id;

}

else

{

$orderdata->save();

$order_id = $orderdata->order_id;

}

/************************** CART DATA START ***********************/

$result_array = '';

$EXPDATE = $card_expdate;

$request_params = array

(

'METHOD' => 'DoDirectPayment',

			/*'USER' => $API_USERNAME,

			'PWD' => $API_PASSWORD,

			'SIGNATURE' => $API_SIGNATURE,

			'VERSION' =>$API_VERSION,

			'PAYMENTACTION' => 'Sale',

			'IPADDRESS' => $_SERVER['REMOTE_ADDR'],*/

			'CREDITCARDTYPE' => $card_name,

			'ACCT' => $card_number,

			'EXPDATE' => $EXPDATE,

			'CVV2' => $card_cvv,

			'FIRSTNAME' => $order_username,

			'STREET' => $order_delivery_address,

			'STATE' => $order_delivery_state,

			'COUNTRYCODE' => '',

			'AMT' => $total_amt,

			'CURRENCYCODE' => 'USD',

			'DESC' => 'Testing Payments Pro'

		);



		if(isset($this->param['ACK']))

		{

			if($this->param['ACK'] =='submitted_for_settlement')

			{

					//echo  $this->param['ACK'];die;

				$unq_id = $this->order_uniqueid(6);

				DB::table('order')->where('order_id', $order_id)->update(['order_uniqueid' => trim($unq_id)]);

				$paydata = new Order_payment;

				$paydata->pay_orderid = $order_id;

				$paydata->pay_method =$paymenttype;

				$paydata->pay_userid = $user_id ;

				$paydata->pay_tx = $this->param['TRANSACTIONID'];

				$paydata->pay_st =$this->param['ACK'];

				$paydata->pay_amt = $total_amt;

				$paydata->pay_cc = $this->param['CURRENCYCODE'];

				$paydata->pay_status=$payment_pay_status;

				$paydata->save();

				$pay_id = $paydata->pay_id;

				DB::table('temp_cart')->where('rest_cartid', '=', trim($rest_cartid))->delete();

				/******************** SEND NOTIFICATION FOR RECEIVE ORDER  *************************/

				$notification_title = 'Order received';

	//$notification_description = 'We are receive your order sucessfully!';

				$email_content_detail = DB::table('email_content')

				->select('*')

				->where('email_id', '=' ,'3')

				->get();

	//$email_content = "hello raveena jaodn";

				$email_content = $email_content_detail[0]->email_content;

				$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

				$replaceArray = array($order_id, $unq_id);

				$notification_description = str_replace($searchArray, $replaceArray, $email_content);



				$response['order_id']=$order_id;

				$response['order_uniqueid']=$unq_id;

				$response['status']= 1;

				$response['message']='Your order successfully save';

			}

			else

			{

				$response['order_id']=$order_id;

				$response['errorcode'] = "4000014";

				$response['status']= 0;

				$response['message']=$this->param['ACK'].' '.$$this->param['error'];

			}

		}

		/************************** CART DATA END ***********************/

	}



if($paymenttype=='Wallet')

{



$wallet_amount  =  DB::table('users')		

->where('id', '=' ,$user_id)

->value('wallet_amount');



if($wallet_amount>=$total_amt){			



$totalamt = $wallet_amount-$total_amt;



DB::table('users')

->where('id', $user_id)

->update(['wallet_amount' => $totalamt]);



$wallettxnin = array(

	'user_id'=>	$user_id,

	'order_id' => 0,

	'wallet_txn' => $total_amt,

	'cash_back'=> 0,

	'txn_method'=> 'Wallet',

	'txn_id'=> '',

	'paid_received'=> 'paid',

	'txn_status'=> 1

    );



DB::table('wallet_transaction')->insert($wallettxnin);



$unq_id = trim($this->order_uniqueid(6));

$orderdata->order_uniqueid= trim($unq_id);

$orderdata->save();

$order_id = $orderdata->order_id;

$paydata = new Order_payment;

$paydata->pay_orderid = $order_id;

$paydata->pay_method =$paymenttype;

$paydata->pay_userid = $user_id ;

$paydata->pay_tx = '';

$paydata->pay_st = '';

$paydata->pay_amt = $total_amt;

$paydata->pay_cc = 'USD';

$paydata->pay_status=$payment_pay_status;

$paydata->save();

$pay_id = $paydata->pay_id;

DB::table('temp_cart')->where('rest_cartid', '=', trim($rest_cartid))->delete();

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'3')

->get();



//$email_content = "hello raveena jaodn";

$email_content = $email_content_detail[0]->email_content;

$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

$replaceArray = array($order_id, $unq_id);

$msg = str_replace($searchArray, $replaceArray, $email_content);



$response['order_id']=$order_id;

$response['order_uniqueid']=$unq_id;

$response['status']= 1;

$response['message']='Your order received';

}else{



$response['order_id']=$order_id;

$response['order_uniqueid']=$unq_id;

$response['status']= 0;

$response['message']='Wallet balance is insufficiant'; 



}



}	





 $order_uniqueid = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('order_uniqueid'); 

   

 $token_data = DB::table('device_token')

 ->where('userid', '=', trim($user_id))

 ->where('notification_status', '=', 1)

 ->get();



 if(empty($token_data)){



 $token_data = DB::table('device_token')

               ->where('guest_id', '=', trim($user_id))

               ->get();

  }



 if($token_data){



 $target = trim($token_data[0]->devicetoken);



 $devicetype = $token_data[0]->devicetype;	



 $deviceid = $token_data[0]->deviceid;	



 }



$notification_content = DB::table('email_content')->where('email_id',13)->get();

$notification_title = $notification_content[0]->email_title.' '.$order_uniqueid;

$notification_description = $notification_content[0]->email_content;



$sendnotify = $this->send_notification_ios($target,$notification_title,$notification_description);



if($sendnotify==0){



	   $title = $notification_title;

	   $body = $notification_description;

	   $token = $target;



   		$url = "https://fcm.googleapis.com/fcm/send";



		$server_key = 'AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1';

	

		$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

		$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



		$headers = array(

		'Content-Type:application/json',

		 'Authorization:key='.$server_key

		);

		

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));

		$result = curl_exec($ch);

		curl_close($ch);

}



$notifiy = new Notification_list;

$notifiy->noti_userid = $user_id;

$notifiy->noti_title = $notification_title;

$notifiy->noti_desc = $notification_description;

$notifiy->noti_deviceid = $deviceid;

$notifiy->noti_devicetype = $devicetype;

$notifiy->save();



$vendor_id = DB::table('restaurant')

       ->where('rest_id', '=', trim($rest_id))

       ->value('vendor_id'); 



 $token_data = DB::table('vendor_device_token')

 ->where('userid', '=', trim($vendor_id))

 ->where('notification_status', '=', 1)

 ->get();



 if($token_data){



 $target = $token_data[0]->devicetoken;



 $devicetype = $token_data[0]->devicetype;	



 $deviceid = $token_data[0]->deviceid;	



 }



$notification_title = 'New Order Received No: '.$order_uniqueid;

$notification_description = $response['message'];



$sendnotify = $this->send_notification_ios($target,$notification_title,$notification_description);



$Notification=array(

		'noti_userid'=>	trim($vendor_id),

		'noti_guestid'=>'',

		'noti_title'=>	trim($notification_title),

		'noti_desc'=>trim($notification_description),

		'noti_deviceid'=>trim($deviceid),

		'noti_devicetype'=>	trim($devicetype),

		'noti_read'=>0,

		'created_at'=>date('Y-m-d H:i:s')

	);



DB::table('vendor_notification_list')->insert($Notification);



}

else

{

	$response['errorcode'] = "4000015";

	$response['status']= 0;

	$response['message']="Invalid cart id";

}

}

header('Content-type: application/json');

echo json_encode($response);



}		





/************************************************************/

//	FUNCTION NAME : Food_Delete

//	FUNCTION USE :  RESTAURANT FOOD ITEM ADD IN CART

//	FUNCTION ERRORCODE : 160000

/****************************************************/

public function food_itemdelete()

{

$data['rest_cartid']=$rest_cartid = Input::get("rest_cartid");

$data['cart_id'] = Input::get("cart_id");

$data['user_id'] = Input::get("user_id");

$data['guest_id'] = Input::get("guest_id");

$data['rest_id'] = Input::get("rest_id");

$data['menu_id'] = Input::get("menu_id");

$data['menu_catid'] = Input::get("menu_catid");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

if(empty($data['cart_id'])){

$response['errorcode'] = "160000";

$response['status']= 0;

$response['message']="Cart Id:Required parameter missing";

}else if(empty($data['rest_id'])){

$response['errorcode'] = "160001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "160002";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "160003";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "160004";

$response['status']= 0;

$response['message']="Rest Cart Id:Required parameter missing";

}elseif(!empty($data)){

DB::table('temp_cart')

->where('user_id', '=' ,trim($this->param['user_id']))

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('cart_id', '=' ,trim($this->param['cart_id']))

->where('rest_cartid', '=' ,trim($this->param['rest_cartid']))

->where('deviceid', '=' ,trim($this->param['deviceid']))

->where('devicetype', '=' ,trim($this->param['devicetype']))

->delete();

$response['message'] = "Cart Item Deleted.";

$response['status']= 1;

$response['cart_detail'] = array('rest_cartid'=>$rest_cartid);

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : add_delivery_address

//	FUNCTION USE :  Addd new delivery address

//	FUNCTION ERRORCODE : 340000

/****************************************************/



function distance($lat1, $lon1, $lat2, $lon2, $unit)

{

$theta = $lon1 - $lon2;

$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));

$dist = acos($dist);

$dist = rad2deg($dist);

$miles = $dist * 60 * 1.1515;

$unit = strtoupper($unit);

if ($unit == "K")

{

return ($miles * 1.609344);

}

else

{

return $miles;

}

}







/************************************************************/

//	FUNCTION NAME : show_order_detail

//	FUNCTION USE :  Show particular  order detail

//	FUNCTION ERRORCODE : 420000

/****************************************************/



function show_order_detail()

{

$response = array();

$data['user_id'] =$user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['orderid'] = $order_id = Input::get("orderid");

/*if(empty($data['user_id'])){

$response['errorcode'] = "420001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}else*/

if(empty($data['orderid'])){

$response['errorcode'] = "420002";

$response['status']= 0;

$response['message']="Order Id : Required parameter missing";

}

elseif (!empty($data)){



$get_review_status = DB::table('review')

->where('re_orderid',$order_id)

->get();



$review_status = count($get_review_status);



$order_detail = DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('order_id', $order_id)->get();

if(count($order_detail)>0)

{

$order_tip = $order_detail[0]->order_tip;

$cart_tiem = json_decode($order_detail[0]->order_carditem, true);

//print_r($cart_tiem);

$sub_total_cart = 0;

foreach($cart_tiem as $c)

{

$addon_cart_id='';

$rest_id  = $order_detail[0]->rest_id;

$menu_id =  $c['options']['menu_id'];

$menu_cate_id = $c['options']['menu_catid'];

$menu_subcate_id =  $c['options']['option_name'];

$addon_id = '';

$addon_data = $c['options']['addon_data'];

$addon_id = '';

if(!empty($addon_data))

{

	foreach($addon_data as $ad)

	{

		$addon_cart_id[] =$ad['id'];

	}

		//print_r( $addon_cart_id);

	$addon_id = implode(',',$addon_cart_id);

}

$foodsize_price =0;

$food_addonPrice = 0;

$total_price =0 ;

$item_name_cc = '';

$item_price_cc = '';

$offer_on_menu_item = '';

$menu_list = DB::table('menu')

->where('restaurant_id', '=' ,$rest_id)

->where('menu_id', '=' ,$menu_id)

->where('menu_status', '=' ,'1')

->orderBy('menu_order', 'asc')

->get();

// dd(DB::getQueryLog());

$menu_array = '';

$food_array='';

if(!empty($menu_list))

{

	$menu_id = $menu_list[0]->menu_id;

	$menu_name = $menu_list[0]->menu_name;

	$menu_item_count = 1;

	$food_detail='';

	$menu_cat_detail = DB::table('menu_category')

	->where('menu_category.rest_id', '=' ,$rest_id)

	->where('menu_category.menu_id', '=' ,$menu_id)

	->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

	->where('menu_category.menu_cat_status', '=' ,'1')

	->select('menu_category.*')

	->orderBy('menu_category.menu_category_id', 'asc')

	->get();

	$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

	$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

	$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

	$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

	$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

	$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

	$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

	$food_detail['food_subitem_title']='';

	if($menu_cat_detail[0]->menu_category_portion=='yes')

	{

		$food_detail['food_subitem_title'] = 'Choose a size';

		$food_size = DB::table('category_item')

		->where('rest_id', '=' ,$rest_id)

		->where('menu_id', '=' ,$menu_id)

		->where('menu_category', '=' ,$menu_cate_id)

		->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

		->where('menu_item_status', '=' ,'1')

		->select('*')

		->orderBy('menu_cat_itm_id', 'asc')

		->groupBy('menu_item_title')

		->get();

		$food_detail['food_size_detail']=$food_size;

		$item_price_cc =$food_size[0]->menu_item_price;

		$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

		$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

	}

	else

	{

		$food_detail['food_size_detail']=array();

		$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

	}

	$addon_list='';

	if(!empty($addon_id)){

		$addons = DB::table('menu_category_addon')

		->where('addon_restid', '=' ,$rest_id)

		->where('addon_menuid', '=' ,$menu_id)

		->where('addon_status', '=' ,'1')

		->where('addon_menucatid', '=' ,$menu_cate_id)

		->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

		->select('*')

		->orderBy('addon_id', 'asc')

		->groupBy('addon_groupname')

		->get();

		if(!empty($addons))

		{

			foreach($addons as $ad_list)

			{

				$option_type = 0;

				if(substr($ad_list->addon_groupname, -1)=='*')

				{

					$option_type = 1;

				}

				$group_name[]=$ad_list->addon_groupname;

				$ff['addon_gropname'] = $ad_list->addon_groupname;

				$ff['addon_type'] = $ad_list->addon_option;

				$ff['addon_mandatory_or_not'] = $option_type;

				$addon_group = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_groupname', '=' ,$ad_list->addon_groupname)

				->where('addon_menucatid', '=' ,$menu_cate_id)

				->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

				->select('*')

				->orderBy('addon_id', 'asc')

				->get();

				$group_list[]=$addon_group;

				$addon_group_list ='';

				foreach($addon_group as $group_list_loop)

				{

						//$addon_group_list[]=array('')

					$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

						'addon_menucatid'=>$group_list_loop->addon_menucatid,

						'addon_groupname'=>$group_list_loop->addon_groupname,

						'addon_option'=>$group_list_loop->addon_option,

						'addon_name'=>$group_list_loop->addon_name,

						'addon_price'=>$group_list_loop->addon_price,

						'addon_status'=>$group_list_loop->addon_status

					);

					$addon_list[]=array('name'=>$group_list_loop->addon_name,

						'price'=>$group_list_loop->addon_price,

						'id'=>$group_list_loop->addon_id);

					$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

					$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

				}

				$ff['addon_detail'] = $addon_group_list;

				$food_detail['food_addon'][]=$ff;

			}

		}

	}

	else

	{

		$food_detail['food_addon'] = array();

	}

	$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

}

/******** FOOD DETAIL END ****/

$sub_total_cart = ($sub_total_cart+($total_price*$c['qty']));

$special_instruction='';

if(!empty(@$c['instruction'])){$special_instruction=@$c['instruction'];}

$cart_detail[] = array(

	"cart_id"=> $c['rowId'],

	"rest_cartid"=>'',

	"user_id" => $order_detail[0]->user_id,

	"guest_id" => '',

	"rest_id"=> $order_detail[0]->rest_id,

	"menu_id"=> $c['options']['menu_id'],

	"food_id"=> $c['options']['menu_catid'],

	"food_size_id"=> $c['options']['option_name'],

	"food_addonid"=> '',

	"qty"=>  $c['qty'],

	"special_instruction"=>$special_instruction,

	"food_detail"=>$food_array,

	"foodsize_price"=>$foodsize_price,

	"food_addonPrice"=>$food_addonPrice,

	"total_price"=>$total_price,

	"order_tip"=>$order_tip

);

}

$review_post=0;

if(!empty($order_detail[0]->re_id))

{

$review_post=1;

}

$order_detail_data['order_id']=$order_detail[0]->order_id;

$order_detail_data['order_restcartid']=$order_detail[0]->order_restcartid;

$order_detail_data['order_uniqueid']=$order_detail[0]->order_uniqueid;

$order_detail_data['user_type']=$order_detail[0]->user_type;

$order_detail_data['user_id']=$order_detail[0]->user_id;

$order_detail_data['rest_id']=$order_detail[0]->rest_id;

$order_detail_data['rest_name']=$order_detail[0]->rest_name;

$order_detail_data['rest_address']=$order_detail[0]->rest_address.' '.$order_detail[0]->rest_zip_code;

$order_detail_data['rest_logo']=trim(url('/').'/uploads/reataurant/'.$order_detail[0]->rest_logo);

$order_detail_data['order_fname']=$order_detail[0]->order_fname;

$order_detail_data['order_lname']=$order_detail[0]->order_lname;

$order_detail_data['order_email']=$order_detail[0]->order_email;

$order_detail_data['order_tel']=$order_detail[0]->order_tel;

$order_detail_data['order_address']=$order_detail[0]->order_address;

$order_detail_data['order_city']=$order_detail[0]->order_city;

$order_detail_data['order_pcode']=$order_detail[0]->order_pcode;

$order_detail_data['order_type']=$order_detail[0]->order_type;

$order_detail_data['order_typetime']=$order_detail[0]->order_typetime;

$order_detail_data['order_tablepic']=$order_detail[0]->order_tablepic;

$order_detail_data['order_pickdate']=date('d-m-Y',strtotime($order_detail[0]->order_pickdate));

$order_detail_data['order_picktime']=$order_detail[0]->order_picktime;

$order_detail_data['order_picktime_formate']=$order_detail[0]->order_picktime_formate;

$order_detail_data['order_pickarea']=$order_detail[0]->order_pickarea;

$order_detail_data['order_instruction']=$order_detail[0]->order_instruction;

$order_detail_data['order_cancel_reason']=$order_detail[0]->order_cancel_reason;

$order_detail_data['order_create']=date('d-m-Y',strtotime($order_detail[0]->order_create));

$order_detail_data['order_status']=$order_detail[0]->order_status;

$order_detail_data['order_deliveryfee']=$order_detail[0]->order_deliveryfee;

$order_detail_data['order_deliveryadd1']=$order_detail[0]->order_deliveryadd1;

$order_detail_data['order_deliveryadd2']=$order_detail[0]->order_deliveryadd2;

$order_detail_data['order_deliverysurbur']=$order_detail[0]->order_deliverysurbur;

$order_detail_data['order_deliverypcode']=$order_detail[0]->order_deliverypcode;

$order_detail_data['order_deliverynumber']=$order_detail[0]->order_deliverynumber;

$order_detail_data['order_min_delivery']=$order_detail[0]->order_min_delivery;

$order_detail_data['order_remaning_delivery']=$order_detail[0]->order_remaning_delivery;

$order_detail_data['order_subtotal_amt']=$order_detail[0]->order_subtotal_amt;

$order_detail_data['order_carditem']= json_decode($order_detail[0]->order_carditem, true);

$order_detail_data['order_promo_mode']=$order_detail[0]->order_promo_mode;

$order_detail_data['order_promo_val']=$order_detail[0]->order_promo_val;

$order_detail_data['order_promo_cal']=$order_detail[0]->order_promo_cal;

$order_detail_data['order_promo_detail']=json_decode($order_detail[0]->order_promo_detail);

$order_detail_data['order_service_tax']=$order_detail[0]->order_service_tax;

$order_detail_data['order_pmt_type']=$order_detail[0]->order_pmt_type;

$order_detail_data['order_partial_percent']=$order_detail[0]->order_partial_percent;

$order_detail_data['order_partial_payment']=$order_detail[0]->order_partial_payment;

$order_detail_data['order_partial_remain']=$order_detail[0]->order_partial_remain;

$order_detail_data['order_pmt_method']=$order_detail[0]->order_pmt_method;

$order_detail_data['order_total']=$order_detail[0]->order_total;

$order_detail_data['order_tip']=$order_detail[0]->order_tip;

$order_detail_data['order_grand_total']=$order_detail[0]->order_tip+$order_detail[0]->order_total;

$order_detail_data['review_flag']=$review_post;

$order_detail_data['review_status']=$review_status;



$dm_id  = DB::table('delivery_man_orders')->where('order_id', '=' ,$order_id)->value('dm_id');

$dm_name  = DB::table('deliveryman')->where('id', '=' ,$dm_id)->value('name');

$dmn_name = "";

if($dm_name){ 



$dmn_name = $dm_name; 

}



$order_detail_data['delivery_man']=$dmn_name;



$response['order_id']=$order_id;

$response['order_detail']=$order_detail_data;

$response['cart_detail']=$cart_detail;

$response['offer_aplied']=$order_detail[0]->order_promo_applied;

$response['food_offer_aplied']=json_decode($order_detail[0]->order_food_promo_applied);

$response['status']= 1;

$response['message']='View Your Order!';

}

else

{

$response['order_id']=$order_id;

$response['errorcode'] = "420003";

$response['status']= 0;

$response['message']='Invalid Order id!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}





public function getToken(){

\Braintree_Configuration::environment(env('BRAINTREE_ENV'));

\Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));

\Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));

\Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));

$clientToken = \Braintree_ClientToken::generate();

header('Content-type: application/json');

echo json_encode($clientToken);

}



public function payCardPayment(){

$data['nonce'] ="";

$data['amount'] = "";

$data['wamount'] = "";

$data['user_id'] = "";

$nonce =$this->param['nonce'];

$amount =$this->param['amount'];

$wamount =$this->param['wamount'];

$user_id =$this->param['user_id'];





$status = Braintree_Transaction::sale([

'amount' => $amount,

'paymentMethodNonce' => $nonce,

'options' => [

'submitForSettlement' => True

]

]);





if($wamount>0){



$trans_id = "grambunny"; // $status->id; 	



$wallet_amount  =  DB::table('users')		

	->where('id', '=' ,$user_id)

	->value('wallet_amount');



if($wallet_amount>=$wamount){					



$totalamt = $wallet_amount-$wamount;



DB::table('users')

->where('id', $user_id)

->update(['wallet_amount' => $totalamt]);



$wallettxnin = array(

'user_id'=>	$user_id,

'order_id' => 0,

'wallet_txn' => $wamount,

'cash_back'=> 0,

'txn_method'=> 'Wallet',

'txn_id'=> $trans_id,

'paid_received'=> 'paid',

'txn_status'=> 1

);



DB::table('wallet_transaction')->insert($wallettxnin);



}



}



echo json_encode($status);



}





public function add_money_wallet(){

$data['nonce'] ="";

$data['amount'] = "";

$data['user_id'] = "";

$nonce =$this->param['nonce'];

$amount =$this->param['amount'];

$user_id =$this->param['user_id'];

$status = Braintree_Transaction::sale([

'amount' => $amount,

'paymentMethodNonce' => $nonce,

'options' => [

'submitForSettlement' => True

]

]);



//return $status->success;



//if($status->success==1){



$trans_id = "grambunny"; // $status->id; 



$wallet_amount  =  DB::table('users')		

	->where('id', '=' ,$user_id)

	->value('wallet_amount');



if(!empty($wallet_amount)){				



$totalamt = $amount+$wallet_amount;



DB::table('users')

->where('id', $user_id)

->update(['wallet_amount' => $totalamt]);



}else{



DB::table('users')

->where('id', $user_id)

->update(['wallet_amount' => $amount]);



} 



$wallettxnin = array(

'user_id'=>	$user_id,

'order_id' => 0,

'wallet_txn' => $amount,

'cash_back'=> 0,

'txn_method'=> 'Card',

'txn_id'=> $trans_id,

'paid_received'=> 'received',

'txn_status'=> 1

);



DB::table('wallet_transaction')->insert($wallettxnin); 



//}



echo json_encode($status);

}





function my_wallet()

{

//		echo 'we are doing testing';



$user_id =  Input::get("user_id");



$checkuser  = DB::table('users')->where('id', '=' ,$user_id)->get();



$ucount = count($checkuser);



if(($user_id!='') && ($ucount==1)){	



$wallet_detail  =  DB::table('wallet_transaction')		

	->where('user_id', '=' ,$user_id)

	->orderBy('id', 'desc')

	->get();



$user_detail  =  DB::table('users')		

	->where('id', '=' ,$user_id)

	->value('wallet_amount');								



$response['message'] = "success";

$response['status']= 1;

$response['data'] = array('wallet_detail'=>$wallet_detail,'wallet_amount'=>$user_detail); 



}else{

	$response['message']="Invelid User";

	$response['status']= 0;

	$response['errorcode'] = "470004";

}



return $response;

}





function vendor_my_wallet()

{

//		echo 'we are doing testing';



$vendor_id = Input::get("vendor_id");



$check_vendor  =  DB::table('vendor')		

->where('vendor_id', '=' ,$vendor_id)

->get();	





if($check_vendor){	



$wallet_amount  =  DB::table('vendor')		

	->where('vendor_id', '=' ,$vendor_id)

	->value('wallet_amount');	



$vendor_payment_detail	= DB::table('admin_vendor_transaction')

->where('vendor_id', '=' ,$vendor_id)

->orderBy('id', 'desc')

->get();



$existBank = DB::table('vendor_bank_details')->where('vendor_id', '=', $vendor_id)->first();



if($existBank === null){

$bankStatus = 0;

} else {

$bankStatus = 1;

}





$response['message'] = "success";

$response['status']= 1;

$response['data'] = array('wallet_amount'=>$wallet_amount,'bankStatus'=>$bankStatus,'payment_summary'=>$vendor_payment_detail); 

}else{

$response['message']="Invelid Vendor";

$response['status']= 0;

$response['errorcode'] = "470004";

}



return $response;



}



public function vendor_request_amount()

{



$vendor_id = Input::get("vendor_id");



$request_amount = Input::get('request_amount');	



$check_vendor  =  DB::table('vendor')		

->where('vendor_id', '=' ,$vendor_id)

->get();	





if($check_vendor){	



$vamount  =  DB::table('vendor')   

->where('vendor_id', '=', $vendor_id)

->value('wallet_amount');





if($vamount >= $request_amount)

{



$requestdata = array(

'vendor_id' => $vendor_id,

'request_amount' => $request_amount,

'transfer_status' => 0,

'request_to' => 1,

'txn_method' => 'bank',



);



$affected = DB::table('vendor_payment_request')->insert($requestdata);



$response['message'] = "Payment Request Sucessfully Submitted";

$response['status']= 1;

$response['data'] = array('request_amount'=>$request_amount); 



}

else

{	



$response['message']="Request amount greater then your wallet amount!";

$response['status']= 0;

$response['errorcode'] = "470004"; 



}



}else{

$response['message']="Invelid Vendor";

$response['status']= 0;

$response['errorcode'] = "470004";

}



return $response;



}



public function vendor_bank_setup()

{



$vendor_id = Input::get("vendor_id");



$holder_name = Input::get('holder_name');

$bank_name = Input::get('bank_name');	

$branch = Input::get('branch');	

$account_number = Input::get('account_number');	

$ifsc_code = Input::get('ifsc_code');



$data['holder_name'] = $holder_name;

$data['bank_name'] = $bank_name;

$data['branch'] = $branch;

$data['account_number'] = $account_number;

$data['ifsc_code'] = $ifsc_code;



$response = array();



if(empty($data['holder_name'])){

$response['errorcode'] = "10001";

$response['status']= 0;

$response['message']="Holder Name:Required parameter missing";

}else if(empty($data['bank_name'])){

$response['errorcode'] = "10002";

$response['status']= 0;

$response['message']="Bank Name:Required parameter missing";

}else if(empty($data['branch'])){

$response['errorcode'] = "10003";

$response['status']= 0;

$response['message']="Branch:Required parameter missing";

}else if(empty($data['account_number'])){

$response['errorcode'] = "10004";

$response['status']= 0;

$response['message']="Account Number:Required parameter missing";

}else if(empty($data['ifsc_code'])){

$response['errorcode'] = "10005";

$response['status']= 0;

$response['message']="Ifsc Code:Required parameter missing";

}elseif (!empty($data)) {





$check_vendor  =  DB::table('vendor')		

->where('vendor_id', '=' ,$vendor_id)

->get();



if($check_vendor){		



$existVendor = DB::table('vendor_bank_details')->where('vendor_id', '=', $vendor_id)->first();



if($existVendor === null)

{



DB::table('vendor_bank_details')->insert(

['vendor_id' => $vendor_id,

'holder_name' => $holder_name,

'bank_name' => $bank_name,

'branch' => $branch,

'account_number' => $account_number,

'ifsc' => $ifsc_code]

);



$bankdetail = DB::table('vendor_bank_details')->where('vendor_id', '=' ,$vendor_id)->get();



$response['message'] = "Record Added Sucessfully";

$response['status']= 1;

$response['data'] = array('bank_detail'=>$bankdetail); 



}

else

{	



DB::table('vendor_bank_details')

->where('vendor_id', $vendor_id)

->update(['holder_name' => $holder_name,

'bank_name' => $bank_name,

'branch' => $branch,

'account_number' => $account_number,

'ifsc' => $ifsc_code]);	



$bankdetail = DB::table('vendor_bank_details')->where('vendor_id', '=' ,$vendor_id)->get();           



$response['message'] = "Record Updated Sucessfully";

$response['status']= 1;

$response['data'] = array('bank_detail'=>$bankdetail); 



}



}else{

$response['message']="Invelid Vendor";

$response['status']= 0;

$response['errorcode'] = "470004";

}



return $response;	

} 



header('Content-type: application/json');

echo json_encode($response);



}



public function vendor_request_history()

{



$vendor_id = Input::get("vendor_id");



$check_vendor  =  DB::table('vendor')		

->where('vendor_id', '=' ,$vendor_id)

->get();	





if($check_vendor){	 



$request_details = DB::table('vendor')

->Join('vendor_bank_details', 'vendor.vendor_id', '=', 'vendor_bank_details.vendor_id')

->Join('vendor_payment_request', 'vendor.vendor_id', '=', 'vendor_payment_request.vendor_id')

->where('vendor_bank_details.vendor_id', '=', $vendor_id)

->select('vendor.name','vendor.wallet_amount','vendor_bank_details.*','vendor_payment_request.*')

->get();



$response['message'] = "Success";

$response['status']= 1;

$response['data'] = array('vendor_request_history'=>$request_details); 



}else{



$response['message']="Invelid Vendor";

$response['status']= 0;

$response['errorcode'] = "470004";



}



return $response;	





}



public function get_bank_details()

{



$vendor_id = Input::get("vendor_id");

$check_vendor  =  DB::table('vendor')		

->where('vendor_id', '=' ,$vendor_id)

->get();			



if($check_vendor){	 



$bank_details = DB::table('vendor_bank_details')

		->where('vendor_id', '=', $vendor_id)

		->get();



foreach ($bank_details as $key => $value) {



$bank_detail = $value;

			

}				



$response['message'] = "Success";

$response['status']= 1;

$response['data'] = array('bank_details'=>$bank_detail); 



}else{

$response['message']="Invelid Vendor";

$response['status']= 0;

$response['errorcode'] = "470004";

}

return $response;	

}







/************************************************************/

//	FUNCTION NAME : get_save_order

//	FUNCTION USE :  SAVE ORDER FROM TEMP CART TO ORDER TABLE

//	FUNCTION ERRORCODE : 400000

/****************************************************/



/* function sendNotification_bk($target, $title, $description){



$url = 'https://fcm.googleapis.com/fcm/send';



$server_key = 'AAAAAegtwkE:APA91bF3S7SD92k4DagjC6VQftHrBK_EQRk6FUiHv6SHmvh5B7lKLjDFJIrOFcI2rU-r2NS-IrLpXGZmshWY1ilj2V5vqWzC_JPzSXneaO5PiQVDVx13KAhC3JSqLXf-toUimS2pQJV1';



$data =  array("title"=>$title, "message"=>$description);



$fields = array();

$fields['data'] = $data;



if(is_array($target)){

$fields['registration_ids'] = $target;

}else{

$fields['to'] = $target;

}

//header with content_type api key

$headers = array(

'Content-Type:application/json',

'Authorization:key='.$server_key

);

//CURL request to route notification to FCM connection server (provided by Google)	

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));



$result = curl_exec($ch);

//echo $result;die;

if ($result === FALSE) {

return false;

}

curl_close($ch); 



}*/



function testNotification()

{



$vendor_id = 41;

$divice_token = DB::table('vendor_device_token')

		->where('userid', '=', $vendor_id)

		->where('notification_status', '=',1)

		->first();



$title = "Order Recieved";

$body = "Your order have been successfully created";



//if($divice_token){ 



//$token = $divice_token->devicetoken;



$token = "ffWVM_6f7Ew:APA91bGlJbQ7CY0ytveMp8PGk-5LiB-G6zhVsSTUAcNEehqllvuR8VD_JlyRUmhUjUXu9ewVhzdmE1mSqdvNZSXE6p-5cNp87vSyYLnzZPTOqPNyXnHX3i8VJBDtaZEmLXth1d7KwkRN"; 



$url = "https://fcm.googleapis.com/fcm/send";

$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



$headers = array();

$headers[] = 'Content-Type: application/json';

$headers[] = 'Authorization: key='. $serverKey;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));



$result = curl_exec($ch);



return $result; 



if ($result === FALSE) {

return false;

}

curl_close($ch); 



//}else{ return "Divice token not found" ; }



}



function sendNotification($token,$title,$body)

{



$url = "https://fcm.googleapis.com/fcm/send";

$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



$headers = array();

$headers[] = 'Content-Type: application/json';

$headers[] = 'Authorization: key='. $serverKey;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));



$result = curl_exec($ch);

//echo $result;die;

if ($result === FALSE) {

return false;

}

curl_close($ch); 



}



function send_notification_ios($token,$title,$body)

{



$url = "https://fcm.googleapis.com/fcm/send";



$serverKey = 'AAAA0G7VdBs:APA91bGMg0bVnk7pqZS6yEt5F93YYYlLQlGiVHHUOBMPwLEw8V2pEl3Nfa-lrOAZp-ofvM0fnCZTjTOnOB5Mo0jTxoeI7lUmMNy6U2sdhJgrbKr3WDTc2_uKSXCY7YARm4JG7TwQ1sBV';



$notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');

$arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');



$headers = array(

'Content-Type:application/json',

 'Authorization:key='.$serverKey

);



$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_POST, true);

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayToSend));



$result = curl_exec($ch);

//echo $result;die;

if ($result === FALSE) {

return 0;

}

curl_close($ch); 



return 1;



}



/************************************************************/

//	FUNCTION NAME : Register

//	FUNCTION USE :  user registration from app..

//	FUNCTION ERRORCODE : 470000

/****************************************************/

public function two_factor_otp(Request $request) {

$data['mobileno'] = '';

$data['email'] = '';

$data['userid'] = '';

$data['deviceid'] = '';

$data['devicetype'] = '';

if(isset($this->param["mobileno"]))

{

$data['mobileno'] = $this->param["mobileno"];

}

if(isset($this->param["userid"]))

{

$data['userid'] =  $this->param["userid"];

}

if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

$response = array();

if((empty($this->param["mobileno"])) && (empty($this->param["email"]))){

$response['errorcode'] = "470002";

$response['status']= 0;

$response['message']="Moblie No/Email :Required parameter missing";

}else if(empty($data['userid'])){

$response['errorcode'] = "470003";

$response['status']= 0;

$response['message']="User Id :Required parameter missing";

}

elseif (!empty($data)) {

$user_detail  = DB::table('users')

->where('user_mob', '=' ,$this->param['mobileno'])

->where('id', '=' ,$this->param['userid'])

->get();

$user_detail_email  = DB::table('users')

->where('email', '=' ,$this->param['email'])

->where('id', '=' ,$this->param['userid'])

->get();

$response='';

if(($user_detail) || ($user_detail_email) ){

$sucess_message = '';

$carrier_email = '';

$user_email_send_mail = '';

$user_id = $this->param["userid"];

$fullname='';

if(($user_detail) &&($user_detail_email))

{

$fullname=$user_detail[0]->name.' '.$user_detail[0]->lname;

$carrier_email = $user_detail[0]->carrier_email;

$user_email_send_mail = $user_detail[0]->email;

$sucess_message = "OTP has been sent on your mobile number/Email. Please verify it";

}

elseif(($user_detail) &&(!$user_detail_email))

{

$fullname=$user_detail[0]->name.' '.$user_detail[0]->lname;

$carrier_email = $user_detail[0]->carrier_email;

$user_email_send_mail = $user_detail[0]->email;

$sucess_message = "OTP has been sent on your mobile number. Please verify it";

}

elseif((!$user_detail) &&($user_detail_email))

{

$carrier_email = $user_detail_email[0]->carrier_email;

$user_email_send_mail = $user_detail_email[0]->email;

$fullname=$user_detail_email[0]->name.' '.$user_detail_email[0]->lname;

$sucess_message = "OTP has been sent on your Email. Please verify it";

}

$otp_code = trim($this->generateRandomString(6));

//$otp_code = '123456';

$user_id = $this->param['userid'];

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->otp_for = trim('two_foctor_otp');

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->user_email = trim($this->param['email']);

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'8')

->get();

	//$email_content = "hello raveena jaodn";

$email_content = $email_content_detail[0]->email_content;

	/*$searchArray = array("{{order_id}}", "{{order_uniqueid}}");

	$replaceArray = array($order_id, $unq_id);

	$notification_description = str_replace($searchArray, $replaceArray, $email_content);*/

	$msg_reg_otp =$email_content.''.$otp_code;

//$msg_reg_otp ='Dear  '.trim($fullname).', The verification code  for two factor auth  on grambunny is : '.$otp_code ;

	if(!empty($carrier_email)){

		//Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

		// 	$message->from('info@grambunny.com', 'grambunny');

		// 	$otp_email = trim($this->param['mobileno']).$carrier_email;

		// 	$message->to($otp_email);

		// 	$message->bcc('votiveshweta@gmail.com');

		// 	$message->bcc('votivemobile.pankaj@gmail.com');

		// 	$message->bcc('votivemobile.dilip@gmail.com');

		// 	$message->bcc('votiveiphone.hariom@gmail.com');

		// 	$message->bcc('zubaer.votive@gmail.com');

		// 	$message->subject('Two Factor Auth OTP');

		// });

		/*********** EMAIL FOR SEN OTP START *************/

		$otp_email = trim($this->param['mobileno']).$carrier_email;

		$address = $otp_email; //"deepaksahuphp@gmail.com";

		$message = $msg_reg_otp;

		$subject = "Two Factor Auth". $email_content_detail[0]->email_title;

		$semail = $this->sendMail($address,$subject,$message);

		/********** EMAIL FOR SEND OTP END  *************/

	}

	if(!empty($user_email_send_mail)){

		// Mail::raw($msg_reg_otp, function ($message) use ($user_email_send_mail){

		// 	$message->from('info@grambunny.com', 'grambunny');

		// 	$otp_email = $user_email_send_mail;

		// 	$message->to($otp_email);

		// 	$message->bcc('votiveshweta@gmail.com');

		// 	$message->bcc('votivemobile.pankaj@gmail.com');

		// 	$message->bcc('votivemobile.dilip@gmail.com');

		// 	$message->bcc('votiveiphone.hariom@gmail.com');

		// 	$message->bcc('zubaer.votive@gmail.com');

		// 	$message->subject('Two Factor Auth OTP');

		// });

		/*********** EMAIL FOR SEN OTP START *************/

		$otp_email = $user_email_send_mail;

		$address = $otp_email; //"deepaksahuphp@gmail.com";

		$message = $msg_reg_otp;

		$subject = "Two Factor Auth". $email_content_detail[0]->email_title;

		$semail = $this->sendMail($address,$subject,$message);

		/********** EMAIL FOR SEND OTP END  *************/

	}

	/********** EMAIL FOR SEN OTP END  *************/

	$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

	$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

//$response['message'] = "User successfully created.";

//$response['message'] = "You have registered  successfully.  OTP has been sent on your mobile number. Please verify it";

	$response['message'] = $sucess_message;

	$response['status']= 1;

	$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

}else{

	$response['message']="User not created.";

	$response['status']= 0;

	$response['errorcode'] = "470004";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : Factor Verify OTP

//	FUNCTION USE :  OTP Verification data

//	FUNCTION ERRORCODE : 480000

/****************************************************/

public function get_varify_factor_otp (Request $request){

$response= array();

$data_post['userid'] = '';

$data_post['otp'] = '';

$data_post['from'] = '';

if(isset($this->param['userid'])){

$data_post['userid'] = $this->param['userid'];

}

if(isset($this->param['otp'])){

$data_post['otp'] = $this->param['otp'];

}

if(isset($this->param['from'])){

$data_post['from'] = $this->param['from'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/

if(empty($data_post['otp'])){

$response['errorcode'] = "480001";

$response['status']= 0;

$response['message'] = "OTP: Required parameter missing";

}elseif(empty($data_post['userid'])){

$response['errorcode'] = "480002";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}

elseif(!empty($data_post)){

$otp_detail = DB::table('user_otpdetail')

->where('user_id', '=' ,trim($this->param['userid']))

->where('otp_number', '=' ,trim($this->param['otp']))

->where('otp_for', '=' ,trim('two_foctor_otp'))

->get();

if($otp_detail)

{

$user_id = $otp_detail[0]->user_id;

$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

DB::table('user_otpdetail')->where('user_id', '=', trim($this->param['userid']))

->where('otp_for', '=' ,trim('two_foctor_otp'))->delete();

$response['message']='OTP Verified Successfully!';

$response['data'] = array('user_detail'=>$user_detail);

$response['status']= 1;

}

else

{

$response['errorcode'] = "480003";

//$response['data'] = array('from'=>$this->param['from']);

$response['status']= 0;

$response['message'] = "Invalid OTP. Please enter a valid otp";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Resent OTP

//	FUNCTION USE :  Resent OTP  data

//	FUNCTION ERRORCODE :490000

/****************************************************/

public function get_resent_factor_otp (Request $request){

$data['mobileno'] = '';

$data['email'] = '';

$data['userid'] = '';

$data['deviceid'] = '';

$data['devicetype'] = '';

if(isset($this->param["mobileno"]))

{

$data['mobileno'] = $this->param["mobileno"];

}

if(isset($this->param["userid"]))

{

$data['userid'] =  $this->param["userid"];

}

if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

$response = array();

if((empty($this->param["mobileno"])) && (empty($this->param["email"]))){

$response['errorcode'] = "490002";

$response['status']= 0;

$response['message']="Moblie No/Email :Required parameter missing";

}else if(empty($data['userid'])){

$response['errorcode'] = "490003";

$response['status']= 0;

$response['message']="User Id :Required parameter missing";

}

elseif (!empty($data)) {

$user_detail  = DB::table('users')

->where('user_mob', '=' ,$this->param['mobileno'])

->where('id', '=' ,$this->param['userid'])

->get();

$user_detail_email  = DB::table('users')

->where('email', '=' ,$this->param['email'])

->where('id', '=' ,$this->param['userid'])

->get();

$response='';

if(($user_detail) || ($user_detail_email) ){

$sucess_message = '';

$carrier_email = '';

$user_email_send_mail = '';

$user_id = $this->param["userid"];

$fullname='';

if(($user_detail) &&($user_detail_email))

{

$fullname=$user_detail[0]->name.' '.$user_detail[0]->lname;

$carrier_email = $user_detail[0]->carrier_email;

$user_email_send_mail = $user_detail[0]->email;

$sucess_message = "OTP has been sent on your mobile number/Email. Please verify it";

}

elseif(($user_detail) &&(!$user_detail_email))

{

$fullname=$user_detail[0]->name.' '.$user_detail[0]->lname;

$carrier_email = $user_detail[0]->carrier_email;

$user_email_send_mail = $user_detail[0]->email;

$sucess_message = "OTP has been sent on your mobile number. Please verify it";

}

elseif((!$user_detail) &&($user_detail_email))

{

$carrier_email = $user_detail_email[0]->carrier_email;

$user_email_send_mail = $user_detail_email[0]->email;

$fullname=$user_detail_email[0]->name.' '.$user_detail_email[0]->lname;

$sucess_message = "OTP has been sent on your Email. Please verify it";

}

$otp_code = trim($this->generateRandomString(6));

//$otp_code = '123456';

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->otp_for = trim('two_foctor_otp');

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->user_email = trim($this->param['email']);

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'8')

->get();

$email_content = $email_content_detail[0]->email_content;

$msg_reg_otp =$email_content.''.$otp_code ;

//$msg_reg_otp ='Dear  '.trim($fullname).', The verification code resend for two factor auth  on grambunny is : '.$otp_code ;

if(!empty($carrier_email)){

// Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

// 	$message->from('info@grambunny.com', 'grambunny');

// 	$otp_email = trim($this->param['mobileno']).$carrier_email;

// 	$message->to($otp_email);

// 	$message->bcc('votiveshweta@gmail.com');

// 	$message->bcc('votivemobile.pankaj@gmail.com');

// 	$message->bcc('votivemobile.dilip@gmail.com');

// 	$message->bcc('votiveiphone.hariom@gmail.com');

// 	$message->bcc('zubaer.votive@gmail.com');

// 	$message->subject('Resent OTP for two factor auth' );

// });

/*********** EMAIL FOR SEN OTP START *************/

$otp_email = trim($this->param['mobileno']).$carrier_email;

$otp_email = $otp_email;

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Resent OTP for two factor auth.";

$semail = $this->sendMail($address,$subject,$message);

/********** EMAIL FOR SEND OTP END  *************/

}

if(!empty($user_email_send_mail)){

// Mail::raw($msg_reg_otp, function ($message) use ($user_email_send_mail){

// 	$message->from('info@grambunny.com', 'grambunny');

// 	$otp_email = $user_email_send_mail;

// 	$message->to($otp_email);

// 	$message->bcc('votiveshweta@gmail.com');

// 	$message->bcc('votivemobile.pankaj@gmail.com');

// 	$message->bcc('votivemobile.dilip@gmail.com');

// 	$message->bcc('votiveiphone.hariom@gmail.com');

// 	$message->bcc('zubaer.votive@gmail.com');

// 	$message->subject('Resent OTP for two factor auth ');

// });

/*********** EMAIL FOR SEN OTP START *************/

$otp_email = $user_email_send_mail;

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Resent OTP for two factor auth.";

$semail = $this->sendMail($address,$subject,$message);

/********** EMAIL FOR SEND OTP END  *************/

}

/********** EMAIL FOR SEN OTP END  *************/

$user_data = DB::table('users')->where('id', '=' ,$user_id)->get();

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

//$response['message'] = "User successfully created.";

//$response['message'] = "You have registered  successfully.  OTP has been sent on your mobile number. Please verify it";

$response['message'] = $sucess_message;

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data,'otp_detail'=>$otp_detail);

}else{

$response['message']="User not created.";

$response['status']= 0;

$response['errorcode'] = "490004";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : show_guest_user_otp

//	FUNCTION USE :  Send otp for guest user email/mobile verifiy

//	FUNCTION ERRORCODE :500000

/****************************************************/

function show_guest_user_otp()

{

$data['email'] = '';

$data['phone'] = '';

$data['deviceid'] = '';

$data['devicetype'] = '';

$data['carrier_name'] = '';

$carrier_id = '';

$carrier_name = '';

$carrier_email = '';

if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

if(isset($this->param["mobileno"]))

{

$data['phone'] = $this->param["mobileno"];

}

if(isset($this->param["carrier_name"]))

{

$data['carrier_name'] = $carrier_id = $this->param["carrier_name"];

switch($carrier_id)

{

case 1:

$carrier_id = '1';

$carrier_name = 'Alltel';

$carrier_email = '@message.alltel.com';

break;

case 2:

$carrier_id = '2';

$carrier_name = 'AT&T';

$carrier_email = '@txt.att.net';

break;

case 3:

$carrier_id = '3';

$carrier_name = 'Boost Mobile';

$carrier_email = '@myboostmobile.com';

break;

case 4:

$carrier_id = '4';

$carrier_name = 'Sprint';

$carrier_email = '@messaging.sprintpcs.com';

break;

case 5:

$carrier_id = '5';

$carrier_name = 'T-Mobile';

$carrier_email = '@tmomail.net';

break;

case 6:

$carrier_id = '6';

$carrier_name = 'U.S. Cellular';

$carrier_email = '@email.uscc.net';

break;

case 7:

$carrier_id = '7';

$carrier_name = 'Verizon';

$carrier_email = '@vtext.com';

break;

case 8:

$carrier_id = '8';

$carrier_name = 'Virgin Mobile';

$carrier_email = '@vmobl.com';

break;

case 9:

$carrier_id = '9';

$carrier_name = 'Republic Wireless';

$carrier_email = '@text.republicwireless.com';

break;

}

}

$response = array();

if((empty($data['phone'])) && (empty($data['email'])) ){

$response['errorcode'] = "500001";

$response['status']= 0;

$response['message']="Moblie No:Required parameter missing";

}else if( (!empty($data['phone'])) &&(empty($data['carrier_name']))){

$response['errorcode'] = "500002";

$response['status']= 0;

$response['message']="Carrier Name:Required parameter missing";

}elseif (!empty($data)) {

$response='';

//	$user_id = $result->id;

$otp_code = trim($this->generateRandomString(6));

//$otp_code = '123456';

$user_id =0;

$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->guest_id = trim($this->param['guest_id']);

$otp_data->user_email = trim($this->param['email']);

$otp_data->otp_for = trim('Guset_user_verification');

$otp_data->otp_number = $otp_code;

$otp_data->save();

$otp_inserted_id = $otp_data->otp_id;

/*********** EMAIL FOR SEN OTP START *************/

$email_content_detail = DB::table('email_content')

->select('*')

->where('email_id', '=' ,'8')

->get();

$email_content = $email_content_detail[0]->email_content;

$msg_reg_otp =$email_content.''.$otp_code ;

//$msg_reg_otp ='Dear  User, The verification code send form grambunny is : '.$otp_code ;

if(!empty($this->param['mobileno'])){

// Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){

// 	$message->from('info@grambunny.com', 'grambunny');

// 	$otp_email = trim($this->param['mobileno']).$carrier_email;

// 	$message->to($otp_email);

// 	$message->bcc('votiveshweta@gmail.com');

// 	$message->bcc('votivemobile.pankaj@gmail.com');

// 	$message->bcc('votivemobile.dilip@gmail.com');

// 	$message->bcc('votiveiphone.hariom@gmail.com');

// 	$message->bcc('zubaer.votive@gmail.com');

// 	$message->subject('Guest User OTP');

// });

/*********** EMAIL FOR SEN OTP START *************/

$otp_email = trim($this->param['mobileno']).$carrier_email;

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Guest User OTP";

$semail = $this->sendMail($address,$subject,$message);

/********** EMAIL FOR SEND OTP END  *************/

}

if(!empty($this->param['email'])){

// Mail::raw($msg_reg_otp, function ($message) {

// 	$message->from('info@grambunny.com', 'grambunny');

// 	$otp_email = $this->param['email'];

// 	$message->to($otp_email);

// 	$message->bcc('votiveshweta@gmail.com');

// 	$message->bcc('votivemobile.pankaj@gmail.com');

// 	$message->bcc('votivemobile.dilip@gmail.com');

// 	$message->bcc('votiveiphone.hariom@gmail.com');

// 	$message->bcc('zubaer.votive@gmail.com');

// 	$message->subject('Guest User OTP');

// });

/*********** EMAIL FOR SEN OTP START *************/

$otp_email = $this->param['email'];

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Guest User OTP";

$semail = $this->sendMail($address,$subject,$message);

/********** EMAIL FOR SEND OTP END  *************/

}

/********** EMAIL FOR SEN OTP END  *************/

$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

//$response['message'] = "User successfully created.";

$response['message'] = "OTP has been sent on your mobile number/Email. Please verify it.";

$response['status']= 1;

$response['data'] = array('otp_detail'=>$otp_detail);

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : show_guest_user_resentotp

//	FUNCTION USE :  Send otp for guest user email/mobile verifiy 

//	FUNCTION ERRORCODE :520000

/****************************************************/



function show_guest_user_resentotp()

{



$data['email'] = '';

$data['phone'] = '';	

$data['deviceid'] = '';

$data['devicetype'] = '';

$data['carrier_name'] = '';



$carrier_id = '';

$carrier_name = '';

$carrier_email = '';



if(isset($this->param["email"]))

{

$data['email'] =  $this->param["email"];

}

if(isset($this->param["mobileno"]))

{

$data['phone'] = $this->param["mobileno"];

}

if(isset($this->param["carrier_name"]))

{

$data['carrier_name'] = $carrier_id = $this->param["carrier_name"];



switch($carrier_id)

{

case 1: 	

	$carrier_id = '1';

	$carrier_name = 'Alltel';

	$carrier_email = '@message.alltel.com';			

	break;	

case 2: 	

	$carrier_id = '2';

	$carrier_name = 'AT&T';

	$carrier_email = '@txt.att.net';			

	break;	

case 3: 	

	$carrier_id = '3';

	$carrier_name = 'Boost Mobile';

	$carrier_email = '@myboostmobile.com';			

	break;	

case 4: 	

	$carrier_id = '4';

	$carrier_name = 'Sprint';

	$carrier_email = '@messaging.sprintpcs.com';			

	break;	

case 5: 	

	$carrier_id = '5';

	$carrier_name = 'T-Mobile';

	$carrier_email = '@tmomail.net';			

	break;	

case 6: 	

	$carrier_id = '6';

	$carrier_name = 'U.S. Cellular';

	$carrier_email = '@email.uscc.net';			

	break;	

case 7: 	

	$carrier_id = '7';

	$carrier_name = 'Verizon';

	$carrier_email = '@vtext.com';			

	break;	

case 8: 	

	$carrier_id = '8';

	$carrier_name = 'Virgin Mobile';

	$carrier_email = '@vmobl.com';			

	break;	

case 9: 	

	$carrier_id = '9';

	$carrier_name = 'Republic Wireless';

	$carrier_email = '@text.republicwireless.com';			

	break;	

}





}





$response = array();



if((empty($data['phone'])) && (empty($data['email'])) ){

$response['errorcode'] = "520001";

$response['status']= 0;

$response['message']="Moblie No:Required parameter missing";

}else if( (!empty($data['phone'])) &&(empty($data['carrier_name']))){

$response['errorcode'] = "520002";

$response['status']= 0;

$response['message']="Carrier Name:Required parameter missing";

}elseif (!empty($data)) {

	

$response='';



//	$user_id = $result->id;

	

$otp_code = trim($this->generateRandomString(6));



$user_id =0;



$otp_data = new User_otpdetail;

$otp_data->user_id = trim($user_id);

$otp_data->user_mob = trim($this->param['mobileno']);

$otp_data->guest_id = trim($this->param['guest_id']);

$otp_data->user_email = trim($this->param['email']);

$otp_data->otp_for = trim('Guset_user_verification');

$otp_data->otp_number = $otp_code;

$otp_data->save();	



$otp_inserted_id = $otp_data->otp_id;



					

/*********** EMAIL FOR SEN OTP START *************/

$email_content_detail = DB::table('email_content')		

					->select('*')	

					->where('email_id', '=' ,'8')

					->get();

						

$email_content = $email_content_detail[0]->email_content;

					

$msg_reg_otp = $email_content.''.$otp_code ;

//$msg_reg_otp ='Dear  User, The verification code send form grambunny is : '.$otp_code ;



if(!empty($this->param['mobileno'])){



$otp_email = trim($this->param['mobileno']).$carrier_email;

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Guest User OTP";

$semail = $this->sendMail($address,$subject,$message);





}



if(!empty($this->param['email'])){



$otp_email = $this->param['email'];

$address = $otp_email; //"deepaksahuphp@gmail.com";

$message = $msg_reg_otp;

$subject = "Guest User OTP";

$semail = $this->sendMail($address,$subject,$message);



}





/********** EMAIL FOR SEN OTP END  *************/



$otp_detail = DB::table('user_otpdetail')->where('otp_id', '=' ,$otp_inserted_id)->get();

							

//$response['message'] = "User successfully created.";

$response['message'] = "OTP has been sent on your mobile number/Email. Please verify it.";

$response['status']= 1;

$response['data'] = array('otp_detail'=>$otp_detail);



}	







header('Content-type: application/json');

echo json_encode($response);



}



/************************************************************/

//	FUNCTION NAME : get_varify_guest_otp

//	FUNCTION USE :  Send otp for guest user email/mobile verifiy

//	FUNCTION ERRORCODE :5100000

/****************************************************/

function get_varify_guest_otp(Request $request){

$response= array();

$data_post['guest_id'] = '';

$data_post['otp'] = '';

$data_post['from'] = '';

if(isset($this->param['guest_id'])){

$data_post['guest_id'] = $this->param['guest_id'];

}

if(isset($this->param['otp'])){

$data_post['otp'] = $this->param['otp'];

}

/*$data_post['deviceid'] = Input::get('deviceid');

$data_post['devicetype'] = Input::get('devicetype');*/

if(empty($data_post['otp'])){

$response['errorcode'] = "510001";

$response['status']= 0;

$response['message'] = "OTP: Required parameter missing";

}elseif(empty($data_post['guest_id'])){

$response['errorcode'] = "510002";

$response['status']= 0;

$response['message']="Guest Id:Required parameter missing";

}

elseif(!empty($data_post)){

$otp_detail = DB::table('user_otpdetail')

->where('guest_id', '=' ,trim($this->param['guest_id']))

->where('otp_number', '=' ,trim($this->param['otp']))

->get();

if($otp_detail)

{

$guest_id = $otp_detail[0]->guest_id;

DB::table('user_otpdetail')->where('guest_id', '=', trim($this->param['guest_id']))->delete();

$response['message']='OTP Verified Successfully!';

$response['status']= 1;

}

else

{

$response['errorcode'] = "510003";

//$response['data'] = array('from'=>$this->param['from']);

$response['status']= 0;

$response['message'] = "Invalid OTP. Please enter a valid otp";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : show_notification

//	FUNCTION USE :  Show all notification listing

//	FUNCTION ERRORCODE : 440000

/****************************************************/

function update_temp_userid()

{



$response = array();



$data['user_id'] =	$user_id = Input::get("user_id");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['rest_cartid'] = $rest_cartid = Input::get("rest_cartid");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");





if(empty($data['user_id'])){

$response['errorcode'] = "440001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";



}

elseif(empty($data['guest_id'])){

$response['errorcode'] = "440002";

$response['status']= 0;

$response['message']="Guest Id:Required parameter missing";



}

elseif(empty($data['rest_cartid'])){

$response['errorcode'] = "440003";

$response['status']= 0;

$response['message']="RestCart Id:Required parameter missing";



}

elseif (!empty($data)){



$temp_detail = DB::table('temp_cart')->where('rest_cartid', $rest_cartid)->where('guest_id', $guest_id)->get();



if(count($temp_detail)>0)								

{





DB::table('temp_cart')

->where('rest_cartid', $rest_cartid)

->update(['user_id'=>  trim($user_id),

	  'guest_id'=>  trim('')

	 ]);

	 





$response['user_id']=$user_id;	

$response['rest_cartid']= $rest_cartid;	

//$response['notification_detail']=$notification_detail;

$response['status']= 1;

$response['message']='Cart details update sucessfully!';

}

else

{

$response['user_id']=$user_id;	

$response['errorcode'] = "440004";

$response['status']= 0;

$response['message']='Invalid restcart id!';

}

}





header('Content-type: application/json');

echo json_encode($response);			



}





/************************************************************/

//	FUNCTION NAME : update_checkout_verificaiton_status

//	FUNCTION USE :  if user set as yes it mean on checkout page before save order user can check it's mobile number

//					 is valide or not

//	FUNCTION ERRORCODE : 450000

/****************************************************/

public function update_checkout_verificaiton_status()

{

/*print_r($this->param);

exit;*/

DB::enableQueryLog();

$data['userid'] = $user_id =Input::get("userid");

$data['status'] = Input::get("status");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

//	if(empty($data['user_id']))

if( (($user_id==0) || empty($user_id)) )

{

$response['errorcode'] = "450001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(($data['status']=='') || ($data['status']<0)){

$response['errorcode'] = "450002";

$response['status']= 0;

$response['message']="Checkout Verificaiton Status:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "450003";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "450004";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(!empty($data)){

$user_detail = DB::table('users')->where('id', '=' ,$user_id)->get();

if($user_detail)

{

DB::table('users')

->where('id', $user_id)

->update([ 'checkout_verificaiton'=> trim($this->param['status']) ]);

$user_detail= DB::table('users')->where('id', '=' ,$user_id)->get();

$response['message'] = "Checkout Verificaiton status successfully updated.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_detail);

}

else

{

$response['errorcode'] = "450005";

$response['status']= 0;

$response['message']="This user not registered";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

/*Deliverman API's start*/

/************************************************************/



/************************************************************/

//	FUNCTION NAME : deliverman_login

//	FUNCTION USE :  use to login to delivery man in to the system

//	FUNCTION ERRORCODE : 450000

/****************************************************/





/*Delivery man login*/

public function deliverman_login(Request $request)

{

$response= array();

$username = Input::get('username');

$password = Input::get('password');

if(empty($username)){

$response['errorcode'] = "40001";

$response['status']= 0;

$response['message']="Username:Required parameter missing";

header('Content-type: application/json');

return json_encode($response);

}elseif(empty($password)){

$response['errorcode'] = "40002";

$response['status']= 0;

$response['message'] = "Password: Required parameter missing";

header('Content-type: application/json');

return json_encode($response);

}

$check_email  = DB::table('deliveryman')->where('email', '=' ,$username)->get();

if(count($check_email)>0){

//$otp_code = trim($this->generateRandomString(6));

$otp_code = '123456';

$userdata = array(

'email'     => Input::get('username'),

'password'  => Input::get('password')

);

if(Auth::guard('deliveryman')->attempt($userdata)){

$userData = Auth::guard('deliveryman')->user();

$user_id = $userData->id;

$user_detail  = DB::table('deliveryman')->where('id', '=' ,$user_id)->get();

if($user_detail[0]->user_status==1){

/** MANAGE LOG START **/

$log_data['log_type'] = 'normal' ;

$log_data['log_userid'] =$user_id ;

$log_data['log_date'] = date('Y-m-d H:i');

$log_data['log_time']= date('Y-m-d H:i');

$log_data['log_by'] = Input::get('devicetype');

$log_data['device_id'] = Input::get('deviceid');

//$log_data['device_type'] = Input::get('device_name');

//$log_data['device_os_name'] = Input::get('os_name');

$user_data = DB::table('deliveryman')->where('id', '=' ,$user_id)->get(['id','name','lname','email','user_pic','password','user_mob','user_address','user_city','user_zipcode','user_status','deviceid','devicetype','user_role','created_at','updated_at']);

//$user_data = DB::table('deliveryman')->where('id', '=' ,$user_id)->get(['name','surname']);

$response['message'] = "You have logged in successfully";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data);

}else{

$response['errorcode'] = "40004";

$response['status']= 0;

$response['message'] ='Your acoount is not active!';

}

}else{

$response['errorcode'] = "40006";

$response['status']= 0;

$response['message'] = "Invalid Username or Password!";

}

}else{

$response['errorcode'] = "40005";

$response['status']= 0;

$response['message'] = "Email Does Not Exists in Our System, so please contact to admin!";

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : forgetPassword

//	FUNCTION USE :  use to request to admin for forget password

//	FUNCTION ERRORCODE : 450000

/****************************************************/

/*Forget password request to admin*/

public function forgetPassword(Request $request)

{



$response= array();

$email = Input::get('email');

if(empty($email)){

$response['errorcode'] = "40001";

$response['status']= 0;

$response['message']="Email:Required parameter missing";

header('Content-type: application/json');

return json_encode($response);

}

$user_detail  = DB::table('deliveryman')->where('email', '=' ,$email)->get();

if(count($user_detail)>0){

$user_id = $user_detail[0]->id;

$otp_code = trim($this->generateRandomString(6));

$affected = DB::table('deliveryman')

->where('id', $user_id)

->update([ 'forgetpass_request'=>$otp_code,'forgetpass_request_status'=>0]);

if($affected===1){

$response['message'] = "Your request has been sent successfully, the admin will contact you soon!";

$response['status']= 1;

}



}else{

$response['errorcode'] = "40005";

$response['status']= 0;

$response['message'] = "Email Does Not Exists in Our System, so please contact to admin!";

}

header('Content-type: application/json');

echo json_encode($response);

}











/************************************************************/

//	FUNCTION NAME : forgetPassword

//	FUNCTION USE :  use to request to admin for forget password

//	FUNCTION ERRORCODE : 450000

/****************************************************/

/*Forget password request to admin*/

public function vendor_forget_pwd(Request $request)

{



	$response= array();

	$email = Input::get('email');

	if(empty($email)){

		return response()->json(["errorcode" => "40001","ok" => 0,"message" => "Email:Required parameter missing"]);

	}

	$vendor = Vendor::where('email',$request->email)->first(); 

	if (!$vendor) {

		return response()->json(["errorcode" => "40005","ok" => 0,"message" => "Email Does Not Exists in Our System, so please contact to admin!"]);

	}

	$genrateToken=str_random(60);

	DB::table('password_resets')->insert([

      	'email' => $request->email,

      	'token' => $genrateToken, 

      	'created_at' => Carbon::now()

    ]);

	Mail::send('auth.emails.vendor-password-reset', ["firstname" => $vendor->name,"token" => encrypt($genrateToken)],function ($message) use ($vendor){

        $message->to($vendor->email);

        $message->subject('Forgot password');

        $message->from(config("app.webmail"), config("app.mailname"));

    });



 	return response()->json(["ok" => 1,"message" => "Reset password link has been sent to your email address."]);



}





/****************************************************/

//	FUNCTION NAME : View Delivery Man Profile

//	FUNCTION USE :  Use to view profile

//	FUNCTION ERRORCODE : 70000

/****************************************************/

public function viewdeliverymanprofile (Request $request){

$data_post['user_id'] = '';

if(isset($this->param['userid'])){

$data_post['user_id'] = $this->param['userid'];

$user_id = $this->param['userid'];

}

if(empty($data_post['user_id'])){

$response['errorcode'] = "70001";

$response['status']= 0;

$response['message'] = "User Id: Required parameter missing";

}elseif(!empty($data_post))

{

$user_id = $this->param['userid'];

$user_detail = DB::table('deliveryman')->where('id', '=' ,$user_id)->get(['id','name','lname','email','user_pic','password','user_mob','user_address','user_city','user_zipcode','user_status','deviceid','devicetype','user_role','created_at','updated_at']);

if(!empty($user_detail)){

$response['message'] = "View Profile : View delivery man detail";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_detail);

}else{

$response['errorcode'] = "70003";

$response['status']= 0;

$response['message'] ='Invalide user Id!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}





public function generateRandomStringforimages($length = 12) {

$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

$charactersLength = strlen($characters);

$randomString = '';

for ($i = 0; $i < $length; $i++) {

$randomString .= $characters[rand(0, $charactersLength - 1)];

}

return $randomString;

}



/************************************************************/

//	FUNCTION NAME : Update delivery man's profile

//	FUNCTION USE :  Use to update profile of delivery man

//	FUNCTION ERRORCODE : 60000

/****************************************************/

public function deliverymanupdate_profile(Request $request){



$user_id = Input::get('userid');

$data['user_id'] = '';

$data['fname'] = '';

$data['lname'] = '';

$data['profile'] = '';

if(isset($this->param["userid"]))

{

$data['user_id'] = $this->param["userid"];

}

if(isset($this->param["fname"]))

{

$data['fname'] = $this->param["fname"];

}

if(isset($this->param["lname"]))

{

$data['lname'] = $this->param["lname"];

}

if(isset($this->param["profile"]))

{

$data['profile'] = $this->param["profile"];

}

/*for image file*/

$random1 = $this->generateRandomStringforimages(10);

$ext = pathinfo($_FILES['profile']['name'], PATHINFO_EXTENSION);

$file_name1 = $random1.".".$ext;

$dirpath = '././uploads/deliveryman/'.$file_name1;

$filepath = 'https://'.$_SERVER['SERVER_NAME'].'/demo/uploads/deliveryman/'.$file_name1;

if(move_uploaded_file($_FILES['profile']['tmp_name'], $dirpath)){

$pictureFile = $file_name1;

}

if(!empty($pictureFile)){

$picture =$pictureFile;

}else{

$picture ='';

}

/*for image end*/

$response = array();

if(empty($data['fname'])){

$response['errorcode'] = "60001";

$response['status']= 0;

$response['message']="First name:Required parameter missing";

}else if(empty($data['user_id'])){

$response['errorcode'] = "60002";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}else if(empty($data['lname'])){

$response['errorcode'] = "60003";

$response['status']= 0;

$response['message']="Last name:Required parameter missing";

}elseif (!empty($data)) {

DB::table('deliveryman')

->where('id', $user_id)

->update([

'name' => trim($this->param['fname']),

'lname' => trim($this->param['lname']),

'user_pic'=>$filepath

]);

$user_data = DB::table('deliveryman')->where('id', '=' ,$user_id)->get(['id','name','lname','user_pic']);

$response['message'] = "Profile is successfully updated";

$response['status']= 1;

$response['data'] = array('user_detail'=>$user_data);

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : Reset delivery man password

//	FUNCTION USE :  use to update delivery man's password

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function resetdeliveryman_password (Request $request){

$user_id = Input::get('userid');

$data['user_id'] = Input::get("userid");

$data['newpassword'] = Input::get("newpassword");

$response = array();

if(empty($data['newpassword'])){

$response['errorcode'] = "200001";

$response['status']= 0;

$response['message']="New password:Required parameter missing";

}elseif(empty($data['user_id'])){

$response['message']="user id:Required parameter missing";

$response['status']= 0;

$response['errorcode'] = "200002";

}elseif(!empty($data)) {

DB::table('deliveryman')

->where('id', $user_id)

->update(['password' =>  Hash::make(trim(Input::get('newpassword'))),

'user_pass'=> md5(trim(Input::get('newpassword')))

]);

$user_data = DB::table('deliveryman')->where('id', '=' ,$user_id)->get();

$response['message'] = "Password updated sucessfully.";

$response['status']= 1;

$response['data'] = $user_data;

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : update lat and long in the deliveryman detail

//	FUNCTION USE :  use to update delivery man's location lat long

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function deliverymanget_location(Request $request)

{

$user_id = Input::get('userid');

$data['user_id'] = Input::get("userid");

$data['lat'] = Input::get("lat");

$data['long'] = Input::get("long");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "200001";

$response['status']= 0;

$response['message']="user id:Required parameter missing";

}elseif(empty($data['lat'])){

$response['errorcode'] = "200002";

$response['status']= 0;

$response['message']="Latitute:Required parameter missing";

}elseif(empty($data['long'])){

$response['errorcode'] = "200003";

$response['status']= 0;

$response['message']="Langitute:Required parameter missing";

}elseif(!empty($data)){

DB::table('deliveryman')

->where('id', $user_id)

->update([

'lat' => trim(Input::get('lat')),

'long'=> trim(Input::get('long'))

]);

$response['message'] = "Success";

$response['status']= 1;

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : Order list

//	FUNCTION USE :  use to show assigned order list to deliveryman

//	FUNCTION ERRORCODE : 200000

/****************************************************/



public function show_order_listingsdfsdf()

{

$data['user_id'] = $user_id =Input::get("userid");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)) {

$limit = 10;

/*if((Input::get('per_page')))

{

$limit = Input::get('per_page');

}*/

$order_detail  =  DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('user_id', '=' ,$user_id)

->where('order.order_status', '!=' ,'2')

->select('*')

->orderBy('order.order_id', 'desc')

->groupBy('order.order_id')

->paginate($limit)	;

$order_counter = $order_detail->total();



if(count($order_detail )>0)

{

$order_detail_list = '';

foreach($order_detail as $order_list)

{



$order_detail_list[]= array(

	'order_id'=>$order_list->order_id,

	'order_uniqueid'=>$order_list->order_uniqueid,

	'user_id'=>$user_id,

	'rest_id'=>$order_list->rest_id,

	'rest_name'=>$order_list->rest_name,

	'food_name'=>$Food_name,

	'rest_logo'=>$logo ,

	'order_date'=>date('d-m-Y',strtotime($order_list->order_create)),

	'order_type'=>$order_list->order_type,

	'order_status'=>$order_list->order_status,

	'order_status_text'=>$order_status_text,

	'order_deliveryfee'=>$order_deliveryfee,

	'order_subtotal'=> number_format($cart_price,2),

	'order_total'=> $total_amt,

	'review_flag'=>$review_post

);

}

$order_detail_format['total'] =$order_detail->total() ;

$order_detail_format['per_page'] =$order_detail->perPage();

$order_detail_format['current_page'] =$order_detail->currentPage();

$order_detail_format['last_page'] =$order_detail->lastPage();

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : show_deliverymanOrderlist

//	FUNCTION USE :  use to show assigned order list to deliveryman

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function  show_deliverymanOrderlist(Request $request)

{



$data['user_id'] = $user_id =Input::get("userid");

$data['page_no'] = $page_no =Input::get("page_no");



$total_record  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where('delivery_man_orders.dm_id', '=' ,$user_id)

->where('delivery_man_orders.accept_reject_status', '!=',2)

->where('delivery_man_orders.order_status', '!=',3)

->select('*')

->orderBy('order.order_id', 'desc')

->count();



$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)){



$limit = 10;



$offset = $data['page_no'];



$offset = $offset*$limit;

$order_detail  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where('delivery_man_orders.dm_id', '=' ,$user_id)

->where('delivery_man_orders.accept_reject_status', '!=',2)

->where('delivery_man_orders.order_status', '!=',3)

->select('*')

->orderBy('order.order_id', 'desc')

->limit($limit)

->offset($offset)

->get();



//return $order_detail;



if(count($order_detail )>0)

{

$order_detail_list = '';

foreach($order_detail as $order_list)

{



$esttime = date('Y-m-d H:i:s',strtotime($order_list->order_create)+(60*60));



$order_detail_list[]= array(

	'order_id'=>$order_list->order_id,

	'rest_id'=>$order_list->rest_id,

	'rest_name'=>$order_list->rest_name,

	'payment_type'=>$order_list->order_pmt_method,

	'address'=>$order_list->user_address,

	'accept_reject_status'=>$order_list->accept_reject_status,

	'delivery_order_time'=> date('Y-m-d H:i:s',strtotime($order_list->order_create)),

	'estimated_delivery_time' => $esttime,

	'order_uniqueid'=>$order_list->order_uniqueid,

	'order_total'=> $order_list->order_total

	// 'user_id'=>$user_id,

	// 'rest_name'=>$order_list->rest_name,

	// 'food_name'=>$Food_name,

	// 'rest_logo'=>$logo ,

	// 'order_date'=>date('d-m-Y',strtotime($order_list->order_create)),

	// 'order_type'=>$order_list->order_type,

	// 'order_status'=>$order_list->order_status,

	// 'order_status_text'=>$order_status_text,

	// 'order_deliveryfee'=>$order_deliveryfee,

	// 'order_subtotal'=> number_format($cart_price,2),

	//'review_flag'=>$review_post

);

}

// $order_detail_format['total'] =$order_detail->total() ;

// $order_detail_format['per_page'] =$order_detail->perPage();

// $order_detail_format['current_page'] =$order_detail->currentPage();



$order_detail_format['total'] = $total_record;

$order_detail_format['per_page'] = $limit;



if($data['page_no']==null){

$order_detail_format['current_page'] =0;

}else{

$order_detail_format['current_page'] =$data['page_no'];

}





if ($data['page_no'] > 0) {

$last_page = $data['page_no']+1;

} else {

$last_page = $data['page_no'];

}



$lastPage = $total_record/$limit;





$order_detail_format['last_page'] = ceil($lastPage);

//$order_detail_format['next_page'] = $last_page;

//$order_detail_format['last_page'] = $last_page;

//$order_detail_format['last_page'] =$order_detail->lastPage();

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}



}



header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : show_order_detail_deliveryman

//	FUNCTION USE :  Show particular  order detail to delivery man

//	FUNCTION ERRORCODE : 420000

/****************************************************/

function show_order_detail_deliveryman()

{



$response = array();

$data['user_id'] =$user_id = Input::get("userid");

// $data['guest_id'] = $guest_id = Input::get("guest_id");

// $data['deviceid'] = $deviceid = Input::get("deviceid");

// $data['devicetype'] = $devicetype = Input::get("devicetype");

$data['orderid'] = $order_id = Input::get("orderid");

if(empty($data['user_id'])){

$response['errorcode'] = "420001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "420002";

$response['status']= 0;

$response['message']="Order Id : Required parameter missing";

}elseif (!empty($data)){



$order_exist = DB::table('delivery_man_orders')

->where('order_id', $order_id)

->where('dm_id', $user_id)

->get();

//DB::table('user_visits')->groupBy('user_id')->count();

if($order_exist){

$order_detail = DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('order_id', $order_id)->get();





// echo "<pre>";

// print_r($order_detail[0]->order_carditem);

// echo "</pre>";

// die;





if(count($order_detail)>0)

{

$order_tip = $order_detail[0]->order_tip;

$cart_tiem = json_decode($order_detail[0]->order_carditem, true);

//print_r($cart_tiem);











$sub_total_cart = 0;

foreach($cart_tiem as $c)

{

	$addon_cart_id='';

	$rest_id  = $order_detail[0]->rest_id;

	$menu_id =  $c['options']['menu_id'];

	$menu_cate_id = $c['options']['menu_catid'];

	$menu_subcate_id =  $c['options']['option_name'];

	$addon_id = '';

	$addon_data = $c['options']['addon_data'];

	$addon_id = '';

	if(!empty($addon_data))

	{

		foreach($addon_data as $ad)

		{

			$addon_cart_id[] =$ad['id'];

		}

		//print_r( $addon_cart_id);

		$addon_id = implode(',',$addon_cart_id);

	}

	$foodsize_price =0;

	$food_addonPrice = 0;

	$total_price =0 ;

	$item_name_cc = '';

	$item_price_cc = '';

	$offer_on_menu_item = '';

	$menu_list = DB::table('menu')

	->where('restaurant_id', '=' ,$rest_id)

	->where('menu_id', '=' ,$menu_id)

	->where('menu_status', '=' ,'1')

	->orderBy('menu_order', 'asc')

	->get();

// dd(DB::getQueryLog());

	$menu_array = '';

	$food_array='';

	if(!empty($menu_list))

	{

		$menu_id = $menu_list[0]->menu_id;

		$menu_name = $menu_list[0]->menu_name;

		$menu_item_count = 1;

		$food_detail='';

		$menu_cat_detail = DB::table('menu_category')

		->where('menu_category.rest_id', '=' ,$rest_id)

		->where('menu_category.menu_id', '=' ,$menu_id)

		->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

		->where('menu_category.menu_cat_status', '=' ,'1')

		->select('menu_category.*')

		->orderBy('menu_category.menu_category_id', 'asc')

		->get();

		$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

		$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

		$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

		$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

		$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

		$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

		$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

		$food_detail['food_subitem_title']='';

		if($menu_cat_detail[0]->menu_category_portion=='yes')

		{

			$food_detail['food_subitem_title'] = 'Choose a size';

			$food_size = DB::table('category_item')

			->where('rest_id', '=' ,$rest_id)

			->where('menu_id', '=' ,$menu_id)

			->where('menu_category', '=' ,$menu_cate_id)

			->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

			->where('menu_item_status', '=' ,'1')

			->select('*')

			->orderBy('menu_cat_itm_id', 'asc')

			->groupBy('menu_item_title')

			->get();

			$food_detail['food_size_detail']=$food_size;

			$item_price_cc =$food_size[0]->menu_item_price;

			$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

			$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

		}

		else

		{

			$food_detail['food_size_detail']=array();

			$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

		}

		$addon_list='';

		if(!empty($addon_id)){

			$addons = DB::table('menu_category_addon')

			->where('addon_restid', '=' ,$rest_id)

			->where('addon_menuid', '=' ,$menu_id)

			->where('addon_status', '=' ,'1')

			->where('addon_menucatid', '=' ,$menu_cate_id)

			->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

			->select('*')

			->orderBy('addon_id', 'asc')

			->groupBy('addon_groupname')

			->get();

			if(!empty($addons))

			{

				foreach($addons as $ad_list)

				{

					$option_type = 0;

					if(substr($ad_list->addon_groupname, -1)=='*')

					{

						$option_type = 1;

					}

					$group_name[]=$ad_list->addon_groupname;

					$ff['addon_gropname'] = $ad_list->addon_groupname;

					$ff['addon_type'] = $ad_list->addon_option;

					$ff['addon_mandatory_or_not'] = $option_type;

					$addon_group = DB::table('menu_category_addon')

					->where('addon_restid', '=' ,$rest_id)

					->where('addon_menuid', '=' ,$menu_id)

					->where('addon_status', '=' ,'1')

					->where('addon_groupname', '=' ,$ad_list->addon_groupname)

					->where('addon_menucatid', '=' ,$menu_cate_id)

					->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

					->select('*')

					->orderBy('addon_id', 'asc')

					->get();

					$group_list[]=$addon_group;

					$addon_group_list ='';

					foreach($addon_group as $group_list_loop)

					{

						//$addon_group_list[]=array('')

						$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

							'addon_menucatid'=>$group_list_loop->addon_menucatid,

							'addon_groupname'=>$group_list_loop->addon_groupname,

							'addon_option'=>$group_list_loop->addon_option,

							'addon_name'=>$group_list_loop->addon_name,

							'addon_price'=>$group_list_loop->addon_price,

							'addon_status'=>$group_list_loop->addon_status

						);

						$addon_list[]=array('name'=>$group_list_loop->addon_name,

							'price'=>$group_list_loop->addon_price,

							'id'=>$group_list_loop->addon_id);

						$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

						$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

					}

					$ff['addon_detail'] = $addon_group_list;

					$food_detail['food_addon'][]=$ff;

				}

			}

		}

		else

		{

			$food_detail['food_addon'] = array();

		}

		$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

	}

	/******** FOOD DETAIL END ****/

	$sub_total_cart = ($sub_total_cart+($total_price*$c['qty']));

	$special_instruction='';

	if(!empty(@$c['instruction'])){$special_instruction=@$c['instruction'];}

	$cart_detail[] = array(

		"cart_id"=> $c['rowId'],

		"rest_cartid"=>'',

		"user_id" => $order_detail[0]->user_id,

		"guest_id" => '',

		"rest_id"=> $order_detail[0]->rest_id,

		"menu_id"=> $c['options']['menu_id'],

		"food_id"=> $c['options']['menu_catid'],

		"food_size_id"=> $c['options']['option_name'],

		"food_addonid"=> '',

		"qty"=>  $c['qty'],

		"special_instruction"=>$special_instruction,

		"food_detail"=>$food_array,

		"foodsize_price"=>$foodsize_price,

		"food_addonPrice"=>$food_addonPrice,

		"total_price"=>$total_price,

	//"order_tip"=>$order_tip

	);

}

$review_post=0;

if(!empty($order_detail[0]->re_id))

{

	$review_post=1;

}



$orstatus = $order_exist[0]->order_status;;



if($orstatus=='3'){



$estimated_dtime = date('Y-m-d H:i:s',strtotime($order_exist[0]->deliver_order_time));	



}else{



$estimated_dtime = date('Y-m-d H:i:s',strtotime($order_detail[0]->order_create)+(60*60));



}



$order_detail_data['order_id']=$order_detail[0]->order_id;

$order_detail_data['order_restcartid']=$order_detail[0]->order_restcartid;

$order_detail_data['order_uniqueid']=$order_detail[0]->order_uniqueid;

$order_detail_data['user_type']=$order_detail[0]->user_type;

$order_detail_data['user_id']=$order_detail[0]->user_id;

$order_detail_data['rest_id']=$order_detail[0]->rest_id;

$order_detail_data['rest_name']=$order_detail[0]->rest_name;

$order_detail_data['rest_address']=$order_detail[0]->rest_address.' '.$order_detail[0]->rest_zip_code;

$order_detail_data['rest_logo']=trim(url('/').'/uploads/reataurant/'.$order_detail[0]->rest_logo);

$order_detail_data['order_fname']=$order_detail[0]->order_fname;

$order_detail_data['order_lname']=$order_detail[0]->order_lname;

$order_detail_data['order_email']=$order_detail[0]->order_email;

$order_detail_data['order_tel']=$order_detail[0]->order_tel;

$order_detail_data['order_address']=$order_detail[0]->order_address;

$order_detail_data['order_city']=$order_detail[0]->order_city;

$order_detail_data['order_pcode']=$order_detail[0]->order_pcode;

$order_detail_data['order_type']=$order_detail[0]->order_type;

$order_detail_data['order_typetime']=$order_detail[0]->order_typetime;

$order_detail_data['order_tablepic']=$order_detail[0]->order_tablepic;

$order_detail_data['order_pickdate']=date('d-m-Y',strtotime($order_detail[0]->order_pickdate));

$order_detail_data['order_picktime']=$order_detail[0]->order_picktime;

$order_detail_data['order_picktime_formate']=$order_detail[0]->order_picktime_formate;

$order_detail_data['order_pickarea']=$order_detail[0]->order_pickarea;

$order_detail_data['order_instruction']=$order_detail[0]->order_instruction;

$order_detail_data['order_cancel_reason']=$order_detail[0]->order_cancel_reason;

$order_detail_data['order_create']=date('Y-m-d H:i:s',strtotime($order_detail[0]->order_create));

$order_detail_data['estimated_delivery_time'] = $estimated_dtime;

$order_detail_data['order_status']=$order_exist[0]->order_status;

//$order_detail_data['order_status']=$order_detail[0]->order_status;

$order_detail_data['order_deliveryfee']=$order_detail[0]->order_deliveryfee;

$order_detail_data['order_deliveryadd1']=$order_detail[0]->order_deliveryadd1;

$order_detail_data['order_deliveryadd2']=$order_detail[0]->order_deliveryadd2;

$order_detail_data['order_deliverysurbur']=$order_detail[0]->order_deliverysurbur;

$order_detail_data['order_deliverypcode']=$order_detail[0]->order_deliverypcode;

$order_detail_data['order_deliverynumber']=$order_detail[0]->order_deliverynumber;

$order_detail_data['order_min_delivery']=$order_detail[0]->order_min_delivery;

$order_detail_data['order_remaning_delivery']=$order_detail[0]->order_remaning_delivery;

$order_detail_data['order_subtotal_amt']=$order_detail[0]->order_subtotal_amt;







$json = $order_detail[0]->order_carditem;



$jsonarr = json_decode($json);

$new_arr = array();

foreach ($jsonarr as $key => $value)

{

	$new_arr[] = $value;

}

//echo json_encode($new_arr);







/*$order_detail_data['order_carditem']= json_decode($order_detail[0]->order_carditem, true);*/

$order_detail_data['order_carditem']= $new_arr;

$order_detail_data['order_promo_mode']=$order_detail[0]->order_promo_mode;

$order_detail_data['order_promo_val']=$order_detail[0]->order_promo_val;

$order_detail_data['order_promo_cal']=$order_detail[0]->order_promo_cal;

$order_detail_data['order_promo_detail']=json_decode($order_detail[0]->order_promo_detail);

$order_detail_data['order_service_tax']=$order_detail[0]->order_service_tax;

$order_detail_data['order_pmt_type']=$order_detail[0]->order_pmt_type;

$order_detail_data['order_partial_percent']=$order_detail[0]->order_partial_percent;

$order_detail_data['order_partial_payment']=$order_detail[0]->order_partial_payment;

$order_detail_data['order_partial_remain']=$order_detail[0]->order_partial_remain;

$order_detail_data['order_pmt_method']=$order_detail[0]->order_pmt_method;

$order_detail_data['order_total']=$order_detail[0]->order_total;

$order_detail_data['order_tip']=$order_detail[0]->order_tip;

$order_detail_data['order_grand_total']=$order_detail[0]->order_tip+$order_detail[0]->order_total;

//$order_detail_data['review_flag']=$review_post;

$response['order_id']=$order_id;

$response['order_detail']=$order_detail_data;

//$response['cart_detail']=$cart_detail;

$response['Food_detail']=$food_array;



$response['offer_aplied']=$order_detail[0]->order_promo_applied;

$response['food_offer_aplied']=json_decode($order_detail[0]->order_food_promo_applied);

$response['status']= 1;

$response['message']='Order Detail!';

}else{

$response['order_id']=$order_id;

$response['errorcode'] = "420003";

$response['status']= 0;

$response['message']='Invalid Order id!';

}

}else{

$response['user_id']=$user_id;

$response['orderid']=$order_id;

$response['errorcode'] = "420004";

$response['status']= 0;

$response['message']='Invalid user id or order id';

}

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : accept_reject_order

//	FUNCTION USE :  use to update status order accept or reject

//	FUNCTION ERRORCODE : 500000

/****************************************************/

public function accept_reject_order(Request $request)

{

$user_id = Input::get('userid');

$order_id = Input::get('orderid');

$data['user_id'] = Input::get("userid");

$data['orderid'] = Input::get("orderid");

$data['status'] = Input::get("status");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "500001";

$response['status']= 0;

$response['message']="User id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "500002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(empty($data['status'])){

$response['errorcode'] = "500003";

$response['status']= 0;

$response['message']="Status:Required parameter missing";

}elseif(!empty($data)){

$order_existornot = DB::table('delivery_man_orders')

->where('order_id', $order_id)

->where('dm_id', $user_id)

->first();

if(!empty($order_existornot->id)){

$status = trim(Input::get('status'));

if($status==1){

$status=1;



DB::table('delivery_man_orders')

->where('dm_id', $user_id)

->where('order_id', $order_id)

->update([

	'accept_reject_status' =>$status,

]);



DB::table('order')

->where('order_id', $order_id)

->update([

	'order_status' =>8,

]);



$response['message'] = "Order accepted successfully!";

$response['status']= 1;





$userid = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('user_id'); 



$token_data = DB::table('device_token')

       ->where('userid', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();



if(empty($token_data)){



$token_data = DB::table('device_token')

       ->where('guest_id', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();

}



if($token_data){



$target = $token_data[0]->devicetoken;



$devicetype = $token_data[0]->devicetype;	



$deviceid = $token_data[0]->deviceid;	



}



$verify_code = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('verification_code'); 

$fname = DB::table('deliveryman')

       ->where('id', '=', trim($user_id))

       ->value('name'); 

$lname = DB::table('deliveryman')

       ->where('id', '=', trim($user_id))

       ->value('lname');



$notification_content = DB::table('email_content')->where('email_id',24)->get();

$notification_title = $notification_content[0]->email_title .$fname.' '.$lname;

$notification_description = $notification_content[0]->email_content .$verify_code;



$sendnotify = $this->send_notification_ios($target,$notification_title,$notification_description);



//return $sendnotify;



$notifiy = new Notification_list;

$notifiy->noti_userid = $userid;

$notifiy->noti_title = $notification_title;

$notifiy->noti_desc = $notification_description;

$notifiy->noti_deviceid = $deviceid;

$notifiy->noti_devicetype = $devicetype;

$notifiy->save();



}



if($status==2){



$status=2;



DB::table('delivery_man_orders')

->where('dm_id', $user_id)

->where('order_id', $order_id)

->update([

	'accept_reject_status' =>$status,

]);

$response['message'] = "Order rejected successfully!";

$response['status']= 1;

}



}else{

$response['errorcode'] = "500004";

$response['message'] = "Invalid usr id or order id!";

$response['status']= 0;

}





}

header('Content-type: application/json');

echo json_encode($response);

}









/************************************************************/

//	FUNCTION NAME : update_order_status

//	FUNCTION USE :  use to update status of order

//	FUNCTION ERRORCODE : 800000

/****************************************************/

public function update_order_status(Request $request)

{

$user_id = Input::get('userid');

$order_id = Input::get('orderid');

$data['user_id'] = Input::get("userid");

$data['orderid'] = Input::get("orderid");

$data['status'] = Input::get("status");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "800001";

$response['status']= 0;

$response['message']="User id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "800002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(empty($data['status'])){

$response['errorcode'] = "800003";

$response['status']= 0;

$response['message']="Status:Required parameter missing";

}elseif(!empty($data)){

$order_existornot = DB::table('delivery_man_orders')

->where('order_id', $order_id)

->where('dm_id', $user_id)

->first();

if(!empty($order_existornot->id)){

$status = trim(Input::get('status'));

//Pick up 1

if($status==1){

$status=1;

$order_status=9;

}

//On the way 2

if($status==2){

$status=2;

$order_status=10;

}



//Delivered 3

if($status==3){

$status=3;

$order_status=11;

}



DB::table('delivery_man_orders')

->where('dm_id', $user_id)

->where('order_id', $order_id)

->update([

'order_status' =>$status,

]);



DB::table('order')

->where('order_id', $order_id)

->update([

'order_status' =>$order_status,

]);



$response['message'] = "Success";

$response['status']= 1;



}else{

$response['errorcode'] = "800004";

$response['message'] = "Invalid usr id or order id!";

$response['status']= 0;

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : verify_order

//	FUNCTION USE :  use to update status of order

//	FUNCTION ERRORCODE : 800000

/****************************************************/

public function verify_order_code(Request $request)

{

$user_id = Input::get('userid');

$order_id = Input::get('orderid');

$verification_code = Input::get('verification_code');

$data['user_id'] = Input::get("userid");

$data['orderid'] = Input::get("orderid");

$data['verification_code'] = Input::get("verification_code");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "900001";

$response['status']= 0;

$response['message']="User id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "900002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(empty($data['verification_code'])){

$response['errorcode'] = "900003";

$response['status']= 0;

$response['message']="verification code:Required parameter missing";

}elseif(!empty($data)){

$order_existornot = DB::table('delivery_man_orders')

->where('order_id', $order_id)

->where('dm_id', $user_id)

->first();

if(!empty($order_existornot->id)){

if($verification_code==$order_existornot->verification_code){



DB::table('delivery_man_orders')

->where('dm_id', $user_id)

->where('order_id', $order_id)

->update([

	'order_status' =>3,'deliver_order_time' => date('Y-m-d H:i:s'),

]);



DB::table('order')

->where('order_id', $order_id)

->update([

	'order_status' =>11,

]);



$response['message'] = "Order verifyed successfully";

$response['status']= 1;





$userid = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('user_id'); 



$order_uniqueid = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('order_uniqueid');                



$token_data = DB::table('device_token')

       ->where('userid', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();



/* Cash back process */



/* $coupon_code = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('coupon_code'); */



//$ccount = count($coupon_code);



// $codeapply = '';



/* if($ccount==1){



$amount = DB::table('coupon_code')

       ->where('coupon', '=', trim($coupon_code))

       ->value('amount');



$uamount = DB::table('users')

       ->where('id', '=', trim($userid))

       ->value('wallet_amount');               



$toalamt = $amount+$uamount;	               



	DB::table('users')

->where('id', $userid)

->update(['wallet_amount' =>$toalamt,]);  





$wallettxnin = array(

'user_id'=>	$userid,

'order_id' => $order_id,

'wallet_txn' => $amount,

'cash_back'=> $amount,

'txn_method'=> 'Wallet',

'txn_id'=> 'grambunny',

'paid_received'=> 'cashback',

'txn_status'=> 1

);



DB::table('wallet_transaction')->insert($wallettxnin); 	



$codeapply = $amount; 	            



} */



/* end cashback process */

       



if(empty($token_data)){



$token_data = DB::table('device_token')

       ->where('guest_id', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();

}



if($token_data){



$target = $token_data[0]->devicetoken;



$devicetype = $token_data[0]->devicetype;	



$deviceid = $token_data[0]->deviceid;	



}



$notification_content = DB::table('email_content')->where('email_id',25)->get();

$notification_title = $notification_content[0]->email_title .$order_uniqueid;

$notification_description = $notification_content[0]->email_content;



$sendnotify = $this->send_notification_ios($target,$notification_title, $notification_description);



//return $sendnotify;



$notifiy = new Notification_list;

$notifiy->noti_userid = $userid;

$notifiy->noti_title = $notification_title;

$notifiy->noti_desc = $notification_description;

$notifiy->noti_deviceid = $deviceid;

$notifiy->noti_devicetype = $devicetype;

$notifiy->save();



/* if($codeapply){



$notification_title = "Order No: ".$order_uniqueid;

$notification_description = $codeapply.'$ '."Cashback Recieved Your Wallet";



if($devicetype=='android')

{

$sendnotify = $this->sendNotification($target, $notification_title, $notification_description);

}

elseif($devicetype=='iphone')

{

$sendnotify = $this->send_notification_ios($target,$notification_title, $notification_description);

}



//return $sendnotify;



$notifiy = new Notification_list;

$notifiy->noti_userid = $userid;

$notifiy->noti_title = $notification_title;

$notifiy->noti_desc = $notification_description;

$notifiy->noti_deviceid = $deviceid;

$notifiy->noti_devicetype = $devicetype;

$notifiy->save();



} */



// Send vendor notification 



$rest_id = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('rest_id');       



$vendor_id = DB::table('restaurant')

       ->where('rest_id', '=', trim($rest_id))

       ->value('vendor_id'); 



 $token_data = DB::table('vendor_device_token')

 ->where('userid', '=', trim($vendor_id))

 ->where('notification_status', '=', 1)

 ->get();



 if($token_data){



 $target = $token_data[0]->devicetoken;



 $devicetype = $token_data[0]->devicetype;	



 $deviceid = $token_data[0]->deviceid;	



 }



$notification_content = DB::table('email_content')->where('email_id',25)->get();

$notification_title = $notification_content[0]->email_title .$order_uniqueid;

$notification_description = $notification_content[0]->email_content;



$sendnotify = $this->send_notification_ios($target,$notification_title, $notification_description);



$Notification=array(

		'noti_userid'=>	trim($vendor_id),

		'noti_guestid'=>'',

		'noti_title'=>	trim($notification_title),

		'noti_desc'=>trim($notification_description),

		'noti_deviceid'=>trim($deviceid),

		'noti_devicetype'=>	trim($devicetype),

		'noti_read'=>0,

		'created_at'=>date('Y-m-d H:i:s')

	);



DB::table('vendor_notification_list')->insert($Notification);



}else{

$response['errorcode'] = "900004";

$response['message'] = "Invalid verification code!";

$response['status']= 0;

}



}else{

$response['errorcode'] = "900005";

$response['message'] = "Invalid usr id or order id!";

$response['status']= 0;

}

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : show_deliverymanOrderHistorylist

//	FUNCTION USE :  use to show order history list to deliveryman

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function  show_deliverymanOrderHistorylist(Request $request)

{



$data['user_id'] = $user_id =Input::get("userid");

$data['page_no'] = $page_no =Input::get("page_no");



$total_record  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where([

['delivery_man_orders.accept_reject_status', '=',1],

['delivery_man_orders.order_status', '=',3],

['delivery_man_orders.dm_id', '=' ,$user_id]

])

->orwhere([

['delivery_man_orders.accept_reject_status', '=',2],

['delivery_man_orders.dm_id', '=' ,$user_id]

])

->select('*')

->orderBy('order.order_id', 'desc')

->count();





$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)){



$limit = 10;

$offset = $data['page_no'];

/*if((Input::get('per_page')))

{

$limit = Input::get('per_page');

}*/

$offset = $offset*$limit;

$order_detail  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where([

['delivery_man_orders.accept_reject_status', '=',1],

['delivery_man_orders.order_status', '=',3],

['delivery_man_orders.dm_id', '=' ,$user_id]

])

->orwhere([

['delivery_man_orders.accept_reject_status', '=',2],

['delivery_man_orders.dm_id', '=' ,$user_id]

])

->select('*')

->orderBy('order.order_id', 'desc')

->limit($limit)

->offset($offset)

->get();

/*->paginate($limit);*/

//$order_counter = $order_detail->total();

if(count($order_detail )>0)

{

$order_detail_list = '';

foreach($order_detail as $order_list)

{



$order_detail_list[]= array(

	'order_id'=>$order_list->order_id,

	'rest_id'=>$order_list->rest_id,

	'rest_name'=>$order_list->rest_name,

	'payment_type'=>$order_list->order_pmt_method,

	'address'=>$order_list->user_address,

	'order_status'=>$order_list->order_status,

	'accept_reject_status'=>$order_list->accept_reject_status,

	'delivery_order_time'=>$order_list->deliver_order_time,

	'order_uniqueid'=>$order_list->order_uniqueid,

	'order_total'=> $order_list->order_total

	// 'user_id'=>$user_id,

	// 'rest_name'=>$order_list->rest_name,

	// 'food_name'=>$Food_name,

	// 'rest_logo'=>$logo ,

	// 'order_date'=>date('d-m-Y',strtotime($order_list->order_create)),

	// 'order_type'=>$order_list->order_type,

	// 'order_status'=>$order_list->order_status,

	// 'order_status_text'=>$order_status_text,

	// 'order_deliveryfee'=>$order_deliveryfee,

	// 'order_subtotal'=> number_format($cart_price,2),

	//'review_flag'=>$review_post

);

}

//$order_detail_format['total'] =$order_detail->total() ;

//$order_detail_format['per_page'] =$order_detail->perPage();

//$order_detail_format['current_page'] =$order_detail->currentPage();

//$order_detail_format['next_page'] =$order_detail->links();

//$order_detail_format['last_page'] =$order_detail->lastPage();

$order_detail_format['total'] = $total_record;

$order_detail_format['per_page'] = $limit;



if($data['page_no']==null){

$order_detail_format['current_page'] =0;

}else{

$order_detail_format['current_page'] =$data['page_no'];

}

//$order_detail_format['current_page'] =$data['page_no'];



if ($data['page_no'] > 0) {

$last_page = $data['page_no']+1;

} else {

$last_page = $data['page_no'];

}



//$order_detail_format['last_page'] =$order_detail->lastPage();



$lastPage = $total_record/$limit;





$order_detail_format['last_page'] = ceil($lastPage);



//$order_detail_format['next_page'] = $last_page;

//$order_detail_format['last_page'] = $last_page;



$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}



}



header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : show_historyorder_detail

//	FUNCTION USE :  Show particular history  order detail to delivery man

//	FUNCTION ERRORCODE : 420000

/****************************************************/

function show_historyorder_detail()

{



$response = array();

$data['user_id'] =$user_id = Input::get("userid");

// $data['guest_id'] = $guest_id = Input::get("guest_id");

// $data['deviceid'] = $deviceid = Input::get("deviceid");

// $data['devicetype'] = $devicetype = Input::get("devicetype");

$data['orderid'] = $order_id = Input::get("orderid");

if(empty($data['user_id'])){

$response['errorcode'] = "420001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "420002";

$response['status']= 0;

$response['message']="Order Id : Required parameter missing";

}elseif (!empty($data)){



$order_exist = DB::table('delivery_man_orders')

->where('order_id', $order_id)

->where('dm_id', $user_id)

->where('delivery_man_orders.accept_reject_status', '=',2)

->get();

//DB::table('user_visits')->groupBy('user_id')->count();

if($order_exist){

$order_detail = DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('order_id', $order_id)->get();

if(count($order_detail)>0)

{

$order_tip = $order_detail[0]->order_tip;

$cart_tiem = json_decode($order_detail[0]->order_carditem, true);

//print_r($cart_tiem);

$sub_total_cart = 0;

foreach($cart_tiem as $c)

{

	$addon_cart_id='';

	$rest_id  = $order_detail[0]->rest_id;

	$menu_id =  $c['options']['menu_id'];

	$menu_cate_id = $c['options']['menu_catid'];

	$menu_subcate_id =  $c['options']['option_name'];

	$addon_id = '';

	$addon_data = $c['options']['addon_data'];

	$addon_id = '';

	if(!empty($addon_data))

	{

		foreach($addon_data as $ad)

		{

			$addon_cart_id[] =$ad['id'];

		}

		//print_r( $addon_cart_id);

		$addon_id = implode(',',$addon_cart_id);

	}

	$foodsize_price =0;

	$food_addonPrice = 0;

	$total_price =0 ;

	$item_name_cc = '';

	$item_price_cc = '';

	$offer_on_menu_item = '';

	$menu_list = DB::table('menu')

	->where('restaurant_id', '=' ,$rest_id)

	->where('menu_id', '=' ,$menu_id)

	->where('menu_status', '=' ,'1')

	->orderBy('menu_order', 'asc')

	->get();

// dd(DB::getQueryLog());

	$menu_array = '';

	$food_array='';

	if(!empty($menu_list))

	{

		$menu_id = $menu_list[0]->menu_id;

		$menu_name = $menu_list[0]->menu_name;

		$menu_item_count = 1;

		$food_detail='';

		$menu_cat_detail = DB::table('menu_category')

		->where('menu_category.rest_id', '=' ,$rest_id)

		->where('menu_category.menu_id', '=' ,$menu_id)

		->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

		->where('menu_category.menu_cat_status', '=' ,'1')

		->select('menu_category.*')

		->orderBy('menu_category.menu_category_id', 'asc')

		->get();

		$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

		$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

		$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

		$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

		$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

		$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

		$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

		$food_detail['food_subitem_title']='';

		if($menu_cat_detail[0]->menu_category_portion=='yes')

		{

			$food_detail['food_subitem_title'] = 'Choose a size';

			$food_size = DB::table('category_item')

			->where('rest_id', '=' ,$rest_id)

			->where('menu_id', '=' ,$menu_id)

			->where('menu_category', '=' ,$menu_cate_id)

			->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

			->where('menu_item_status', '=' ,'1')

			->select('*')

			->orderBy('menu_cat_itm_id', 'asc')

			->groupBy('menu_item_title')

			->get();

			$food_detail['food_size_detail']=$food_size;

			$item_price_cc =$food_size[0]->menu_item_price;

			$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

			$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

		}

		else

		{

			$food_detail['food_size_detail']=array();

			$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

		}

		$addon_list='';

		if(!empty($addon_id)){

			$addons = DB::table('menu_category_addon')

			->where('addon_restid', '=' ,$rest_id)

			->where('addon_menuid', '=' ,$menu_id)

			->where('addon_status', '=' ,'1')

			->where('addon_menucatid', '=' ,$menu_cate_id)

			->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

			->select('*')

			->orderBy('addon_id', 'asc')

			->groupBy('addon_groupname')

			->get();

			if(!empty($addons))

			{

				foreach($addons as $ad_list)

				{

					$option_type = 0;

					if(substr($ad_list->addon_groupname, -1)=='*')

					{

						$option_type = 1;

					}

					$group_name[]=$ad_list->addon_groupname;

					$ff['addon_gropname'] = $ad_list->addon_groupname;

					$ff['addon_type'] = $ad_list->addon_option;

					$ff['addon_mandatory_or_not'] = $option_type;

					$addon_group = DB::table('menu_category_addon')

					->where('addon_restid', '=' ,$rest_id)

					->where('addon_menuid', '=' ,$menu_id)

					->where('addon_status', '=' ,'1')

					->where('addon_groupname', '=' ,$ad_list->addon_groupname)

					->where('addon_menucatid', '=' ,$menu_cate_id)

					->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

					->select('*')

					->orderBy('addon_id', 'asc')

					->get();

					$group_list[]=$addon_group;

					$addon_group_list ='';

					foreach($addon_group as $group_list_loop)

					{

						//$addon_group_list[]=array('')

						$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

							'addon_menucatid'=>$group_list_loop->addon_menucatid,

							'addon_groupname'=>$group_list_loop->addon_groupname,

							'addon_option'=>$group_list_loop->addon_option,

							'addon_name'=>$group_list_loop->addon_name,

							'addon_price'=>$group_list_loop->addon_price,

							'addon_status'=>$group_list_loop->addon_status

						);

						$addon_list[]=array('name'=>$group_list_loop->addon_name,

							'price'=>$group_list_loop->addon_price,

							'id'=>$group_list_loop->addon_id);

						$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

						$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

					}

					$ff['addon_detail'] = $addon_group_list;

					$food_detail['food_addon'][]=$ff;

				}

			}

		}

		else

		{

			$food_detail['food_addon'] = array();

		}

		$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

	}

	/******** FOOD DETAIL END ****/

	$sub_total_cart = ($sub_total_cart+($total_price*$c['qty']));

	$special_instruction='';

	if(!empty(@$c['instruction'])){$special_instruction=@$c['instruction'];}

	$cart_detail[] = array(

		"cart_id"=> $c['rowId'],

		"rest_cartid"=>'',

		"user_id" => $order_detail[0]->user_id,

		"guest_id" => '',

		"rest_id"=> $order_detail[0]->rest_id,

		"menu_id"=> $c['options']['menu_id'],

		"food_id"=> $c['options']['menu_catid'],

		"food_size_id"=> $c['options']['option_name'],

		"food_addonid"=> '',

		"qty"=>  $c['qty'],

		"special_instruction"=>$special_instruction,

		"food_detail"=>$food_array,

		"foodsize_price"=>$foodsize_price,

		"food_addonPrice"=>$food_addonPrice,

		"total_price"=>$total_price,

	//"order_tip"=>$order_tip

	);

}

$review_post=0;

if(!empty($order_detail[0]->re_id))

{

	$review_post=1;

}

$order_detail_data['order_id']=$order_detail[0]->order_id;

$order_detail_data['order_restcartid']=$order_detail[0]->order_restcartid;

$order_detail_data['order_uniqueid']=$order_detail[0]->order_uniqueid;

$order_detail_data['user_type']=$order_detail[0]->user_type;

$order_detail_data['user_id']=$order_detail[0]->user_id;

$order_detail_data['rest_id']=$order_detail[0]->rest_id;

$order_detail_data['rest_name']=$order_detail[0]->rest_name;

$order_detail_data['rest_address']=$order_detail[0]->rest_address.' '.$order_detail[0]->rest_zip_code;

$order_detail_data['rest_logo']=trim(url('/').'/uploads/reataurant/'.$order_detail[0]->rest_logo);

$order_detail_data['order_fname']=$order_detail[0]->order_fname;

$order_detail_data['order_lname']=$order_detail[0]->order_lname;

$order_detail_data['order_email']=$order_detail[0]->order_email;

$order_detail_data['order_tel']=$order_detail[0]->order_tel;

$order_detail_data['order_address']=$order_detail[0]->order_address;

$order_detail_data['order_city']=$order_detail[0]->order_city;

$order_detail_data['order_pcode']=$order_detail[0]->order_pcode;

$order_detail_data['order_type']=$order_detail[0]->order_type;

$order_detail_data['order_typetime']=$order_detail[0]->order_typetime;

$order_detail_data['order_tablepic']=$order_detail[0]->order_tablepic;

$order_detail_data['order_pickdate']=date('d-m-Y',strtotime($order_detail[0]->order_pickdate));

$order_detail_data['order_picktime']=$order_detail[0]->order_picktime;

$order_detail_data['order_picktime_formate']=$order_detail[0]->order_picktime_formate;

$order_detail_data['order_pickarea']=$order_detail[0]->order_pickarea;

$order_detail_data['order_instruction']=$order_detail[0]->order_instruction;

$order_detail_data['order_cancel_reason']=$order_detail[0]->order_cancel_reason;

$order_detail_data['order_create']=date('d-m-Y',strtotime($order_detail[0]->order_create));

$order_detail_data['order_status']=$order_exist[0]->order_status;

//$order_detail_data['order_status']=$order_detail[0]->order_status;

$order_detail_data['order_deliveryfee']=$order_detail[0]->order_deliveryfee;

$order_detail_data['order_deliveryadd1']=$order_detail[0]->order_deliveryadd1;

$order_detail_data['order_deliveryadd2']=$order_detail[0]->order_deliveryadd2;

$order_detail_data['order_deliverysurbur']=$order_detail[0]->order_deliverysurbur;

$order_detail_data['order_deliverypcode']=$order_detail[0]->order_deliverypcode;

$order_detail_data['order_deliverynumber']=$order_detail[0]->order_deliverynumber;

$order_detail_data['order_min_delivery']=$order_detail[0]->order_min_delivery;

$order_detail_data['order_remaning_delivery']=$order_detail[0]->order_remaning_delivery;

$order_detail_data['order_subtotal_amt']=$order_detail[0]->order_subtotal_amt;

$order_detail_data['order_carditem']= json_decode($order_detail[0]->order_carditem, true);

$order_detail_data['order_promo_mode']=$order_detail[0]->order_promo_mode;

$order_detail_data['order_promo_val']=$order_detail[0]->order_promo_val;

$order_detail_data['order_promo_cal']=$order_detail[0]->order_promo_cal;

$order_detail_data['order_promo_detail']=json_decode($order_detail[0]->order_promo_detail);

$order_detail_data['order_service_tax']=$order_detail[0]->order_service_tax;

$order_detail_data['order_pmt_type']=$order_detail[0]->order_pmt_type;

$order_detail_data['order_partial_percent']=$order_detail[0]->order_partial_percent;

$order_detail_data['order_partial_payment']=$order_detail[0]->order_partial_payment;

$order_detail_data['order_partial_remain']=$order_detail[0]->order_partial_remain;

$order_detail_data['order_pmt_method']=$order_detail[0]->order_pmt_method;

$order_detail_data['order_total']=$order_detail[0]->order_total;

$order_detail_data['order_tip']=$order_detail[0]->order_tip;

$order_detail_data['order_grand_total']=$order_detail[0]->order_tip+$order_detail[0]->order_total;

//$order_detail_data['review_flag']=$review_post;

$response['order_id']=$order_id;

$response['order_detail']=$order_detail_data;

//$response['cart_detail']=$cart_detail;

$response['Food_detail']=$food_array;



$response['offer_aplied']=$order_detail[0]->order_promo_applied;

$response['food_offer_aplied']=json_decode($order_detail[0]->order_food_promo_applied);

$response['status']= 1;

$response['message']='Order Detail!';

}else{

$response['order_id']=$order_id;

$response['errorcode'] = "420003";

$response['status']= 0;

$response['message']='Invalid Order id!';

}

}else{

$response['user_id']=$user_id;

$response['orderid']=$order_id;

$response['errorcode'] = "420004";

$response['status']= 0;

$response['message']='Invalid user id or order id';

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : update lat and long in the deliveryman detail

//	FUNCTION USE :  use to update delivery man's location lat long

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function from_to_location(Request $request)

{

$user_id = Input::get('userid');

$order_id = Input::get('orderid');

$data['user_id'] = Input::get("userid");

$data['orderid'] = Input::get("orderid");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "200001";

$response['status']= 0;

$response['message']="user id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "200002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(!empty($data)){

$order_detail  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where('delivery_man_orders.dm_id', '=' ,$user_id)

->where('delivery_man_orders.order_id','=',$order_id)

->where('delivery_man_orders.order_status','!=',3)

->select('*')

->get();

if(!empty($order_detail[0]->order_id)){

$fromto[]=array(

'rest_lat'=>$order_detail[0]->rest_lat,

'rest_long'=>$order_detail[0]->rest_long,

'order_lat'=>$order_detail[0]->order_lat,

'order_long'=>$order_detail[0]->order_long

);

$location['data'] = $fromto;

$response['location_detail'] = $location;

$response['status']= 1;

}else{

$response['message'] = 'No order found with these order id and user id!';

$response['status']= 1;

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : get_current_location

//	FUNCTION USE :  use to get current location of delivery man

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function get_current_location(Request $request)

{

$order_id = Input::get('orderid');

$data['orderid'] = Input::get("orderid");

$response = array();

if(empty($data['orderid'])){

$response['errorcode'] = "200002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(!empty($data)){

$driver_detail  =  DB::table('delivery_man_orders')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->where('delivery_man_orders.order_id','=',$order_id)

->where('delivery_man_orders.order_status','!=',3)

->select('*')

->get();

if(!empty($driver_detail[0]->order_id)){

$current_location[]=array(

'driver_lat'=>$driver_detail[0]->lat,

'driver_long'=>$driver_detail[0]->long

);

$location['data'] = $current_location;

$response['location_detail'] = $location;

$response['status']= 1;

}else{





$response['message'] = 'No order found with this order id!';

$response['status']= 1;

}

}

header('Content-type: application/json');

echo json_encode($response);

}



/************************************************************/

//	FUNCTION NAME : show_locationtion_user

//	FUNCTION USE :  use to show the from to location to user

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function show_locationtion_user(Request $request)

{



$order_id = Input::get('orderid');

$data['orderid'] = Input::get("orderid");

$response = array();

if(empty($data['orderid'])){

$response['errorcode'] = "200002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(!empty($data)){

$order_detail  =  DB::table('order')

->leftJoin('delivery_man_orders', 'order.order_id', '=', 'delivery_man_orders.order_id')

->leftJoin('deliveryman', 'deliveryman.id', '=', 'delivery_man_orders.dm_id')

->leftJoin('restaurant', 'restaurant.rest_id', '=', 'order.rest_id')

->where('delivery_man_orders.order_id','=',$order_id)

->where('delivery_man_orders.order_status','!=',3)

->select('*')

->get();

if(!empty($order_detail[0]->order_id)){

$fromto[]=array(

'rest_lat'=>$order_detail[0]->rest_lat,

'rest_long'=>$order_detail[0]->rest_long,

'order_lat'=>$order_detail[0]->order_lat,

'order_long'=>$order_detail[0]->order_long

);

$location['data'] = $fromto;

$response['location_detail'] = $location;

$response['status']= 1;

}else{

$response['message'] = 'No order found with this order id!';

$response['status']= 1;

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : show_delivery_address

//	FUNCTION USE :  Addd new delivery address

//	FUNCTION ERRORCODE : 370000

/****************************************************/

public function show_delivery_address()

{

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

if(empty($data['userid'])){

$response['errorcode'] = "370002";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}

elseif (!empty($data)){

$address_listing = DB::table('delivery_address')

->select("*")

->where("del_userid","=",$user_id)

->get();

$response['message']='View All Adddresses!';

$response['data'] = array('address_listing'=>$address_listing);

$response['status']= 1;

}

header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

/*Deliverman API's end*/

/************************************************************/



/*======================================================================*/



/************************************************************/

/*Vendor API's Start*/

/************************************************************/





/************************************************************/

//	FUNCTION NAME : show_vendorOrderlist

//	FUNCTION USE :  use to show new order list to vendor

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function  show_vendorOrderlist(Request $request)

{

$vendor_id = Input::get('userid');

$data['user_id'] = $user_id =Input::get("userid");

$data['page_no'] = $page_no =Input::get("page_no");



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';



if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}



$user_id = 0;



$order_detail='';



if((count($rest_id_array)>0) && (!empty($rest_id_array))){



$order_detail  =  DB::table('order')	;

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');







$order_detail = $order_detail->where(function($query) use ($rest_id_array) {



$l=0;

foreach($rest_id_array as $ftype )

{

if($ftype>0)

{

	if($l>0){

		$query = $query->orWhere('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

	else

	{

		$query = $query->Where('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

}

$l++;

}



});



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  =  $order_detail->where('order.order_status','=',1);

$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.order_id', 'desc');

$order_detail  = $order_detail->get();

}





$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$total_record = count($data_onview['order_detail']);

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)){



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';

if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}

$user_id = 0;

$order_detail='';

if((count($rest_id_array)>0) && (!empty($rest_id_array))){

$order_detail  =  DB::table('order');

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');





//$order_detail = $order_detail->where('order.order_status','=',1);



$order_detail = $order_detail->where(function($query) use ($rest_id_array) {

$l=0;

foreach($rest_id_array as $ftype )

{

	if($ftype>0)

	{

		if($l>0){

			$query = $query->orWhere('order.rest_id','=',$ftype);

		}

		else

		{

			$query = $query->Where('order.rest_id','=',$ftype);

		}

	}

	$l++;

}



});





$limit = 10;

$offset = $data['page_no'];

$offset = $offset*$limit;



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  =  $order_detail->where('order.order_status','=',1);

$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.order_id', 'desc');

$order_detail  = $order_detail->limit($limit);

$order_detail  = $order_detail->offset($offset);

$order_detail  = $order_detail->get();

}

$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$order_detail = $data_onview['order_detail'];



if(!empty($order_detail)){

if(count($order_detail)>0)

{

$order_detail_list = '';







foreach($order_detail as $order_list)

{





	//if($order_list->order_status==1){



	$order_detail_list[]= array(

		'order_id' => $order_list->order_id,

		'order_restcartid' => $order_list->order_restcartid,

		'order_guestid' => $order_list->order_guestid,

		'order_uniqueid' => $order_list->order_uniqueid,

		'verification_code' =>$order_list->verification_code,

		'user_type' => $order_list->user_type,

		'user_id' => $order_list->user_id,

		'rest_id' => $order_list->rest_id,

		'order_fname' =>$order_list->order_fname,

		'order_lname' => $order_list->order_lname,

		'order_email' => $order_list->order_email,

		'order_tel' => $order_list->order_tel,

		'order_carrier_email' =>$order_list->order_carrier_email,

		'order_address' => $order_list->order_address,

		'order_city' => $order_list->order_city,

		'order_pcode' => $order_list->order_pcode,

		'order_type' => $order_list->order_type,

		'order_typetime' => $order_list->order_typetime,

		'order_tablepic' => $order_list->order_tablepic,

		'order_pickdate' => $order_list->order_pickdate,

		'order_picktime' => $order_list->order_picktime,

		'order_picktime_formate' => $order_list->order_picktime_formate,

		'order_pickarea' => $order_list->order_pickarea,

		'order_instruction' => $order_list->order_instruction,

		'order_cancel_reason' => $order_list->order_cancel_reason,

		'order_create' => $order_list->order_create,

		'order_status' => $order_list->order_status,

		'order_deliveryfee' => $order_list->order_deliveryfee,

		'order_deliveryadd1' => $order_list->order_deliveryadd1,

		'order_deliveryadd2' => $order_list->order_deliveryadd2,

		'order_lat' => $order_list->order_lat,

		'order_long' => $order_list->order_long,

		'order_deliverysurbur' => $order_list->order_deliverysurbur,

		'order_deliverypcode' => $order_list->order_deliverypcode,

		'order_deliverynumber' => $order_list->order_deliverynumber,

		'order_min_delivery' => $order_list->order_min_delivery,

		'order_remaning_delivery' => $order_list->order_remaning_delivery,

		'order_subtotal_amt' => $order_list->order_subtotal_amt,

		'order_carditem' => $order_list->order_carditem,

		'order_promo_mode' => $order_list->order_promo_mode,

		'order_promo_val' => $order_list->order_promo_val,

		'order_promo_cal' => $order_list->order_promo_cal,

		'order_promo_detail' => $order_list->order_promo_detail,

		'order_promo_applied' => $order_list->order_promo_applied,

		'order_food_promo' =>$order_list->order_food_promo,

		'order_food_promo_applied' =>$order_list->order_food_promo_applied,

		'order_service_tax' => $order_list->order_service_tax,

		'order_pmt_type' => $order_list->order_pmt_type,

		'order_pmt_method' => $order_list->order_pmt_method,

		'order_partial_percent' => $order_list->order_partial_percent,

		'order_partial_payment' => $order_list->order_partial_payment,

		'order_partial_remain' => $order_list->order_partial_remain,

		'order_paid_amt' => $order_list->order_paid_amt,

		'order_remaing_amt' =>$order_list->order_remaing_amt,

		'order_total' => $order_list->order_total,

		'created_at' => $order_list->created_at,

		'updated_at' => $order_list->updated_at,

		'order_device' => $order_list->order_device,

		'order_deviceid' => $order_list->order_deviceid,

		'order_devicename' => $order_list->order_devicename,

		'order_device_os' => $order_list->order_device_os,

		'rest_id'=>$order_list->rest_id,

		'rest_name'=>$order_list->rest_name,

		'payment_type'=>$order_list->order_pmt_method,



	);





	//}



}



//}



// echo "<pre>";

// print_r($order_detail_list);

// echo "</pre>";

// foreach($order_detail_list as $value){

// 	//print_r($value->order_id);

// 	echo $value['order_id'];

// 	echo "<br>";

// 	// if($value->order_status==1){

// 	// 	echo "New order";

// 	// 	echo "<br>";

// 	// }else{

// 	// 	echo "other order";

// 	// 	echo "<br>";

// 	// }

// }

// die;

//$total_record = count($order_detail_list);







$order_detail_format['total'] = $total_record;

$order_detail_format['per_page'] = $limit;



if($data['page_no']==null){

	$order_detail_format['current_page'] =0;

}else{

	$order_detail_format['current_page'] =$data['page_no'];

}





if ($data['page_no'] > 0) {

	$last_page = $data['page_no']+1;

} else {

	$last_page = $data['page_no'];

}



$lastPage = $total_record/$limit;













$order_detail_format['last_page'] = ceil($lastPage);

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}





}else{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";



}



}



header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : vendor_inprogressOrderlist

//	FUNCTION USE :  use to show in progress order list to vendor

//	FUNCTION ERRORCODE : 200000

/****************************************************/

public function  vendor_inprogressOrderlist(Request $request)

{

$vendor_id = Input::get('userid');

$data['user_id'] = $user_id =Input::get("userid");

$data['page_no'] = $page_no =Input::get("page_no");



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';



if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}



$user_id = 0;



$order_detail='';



if((count($rest_id_array)>0) && (!empty($rest_id_array))){



$order_detail  =  DB::table('order')	;

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');

$order_detail = $order_detail->where(function($query) use ($rest_id_array) {



$l=0;

foreach($rest_id_array as $ftype )

{

if($ftype>0)

{

	if($l>0){

		$query = $query->orWhere('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

	else

	{

		$query = $query->Where('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

}

$l++;

}



});



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  =  $order_detail->where('order.order_status','!=',1);

$order_detail  =  $order_detail->where('order.order_status','!=',6);

$order_detail  =  $order_detail->where('order.order_status','!=',11);

$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.updated_at', 'desc');

$order_detail  = $order_detail->get();

}





$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$total_record = count($data_onview['order_detail']);

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)){



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';

if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}

$user_id = 0;

$order_detail='';

if((count($rest_id_array)>0) && (!empty($rest_id_array))){

$order_detail  =  DB::table('order');

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');

$order_detail = $order_detail->where(function($query) use ($rest_id_array) {

$l=0;

foreach($rest_id_array as $ftype )

{

	if($ftype>0)

	{

		if($l>0){

			$query = $query->orWhere('order.rest_id','=',$ftype);

		}

		else

		{

			$query = $query->Where('order.rest_id','=',$ftype);

		}

	}

	$l++;

}



});





$limit = 10;

$offset = $data['page_no'];

$offset = $offset*$limit;



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

//$order_detail  =  $order_detail->where('order.order_status','=',1);

$order_detail  =  $order_detail->where('order.order_status','!=',1);

$order_detail  =  $order_detail->where('order.order_status','!=',6);

$order_detail  =  $order_detail->where('order.order_status','!=',11);

$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.updated_at', 'desc');

$order_detail  = $order_detail->limit($limit);

$order_detail  = $order_detail->offset($offset);

$order_detail  = $order_detail->get();

}

$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$order_detail = $data_onview['order_detail'];



if(!empty($order_detail)){

if(count($order_detail)>0)

{

$order_detail_list = '';







foreach($order_detail as $order_list)

{









	$order_detail_list[]= array(

		'order_id' => $order_list->order_id,

		'order_restcartid' => $order_list->order_restcartid,

		'order_guestid' => $order_list->order_guestid,

		'order_uniqueid' => $order_list->order_uniqueid,

		'verification_code' =>$order_list->verification_code,

		'user_type' => $order_list->user_type,

		'user_id' => $order_list->user_id,

		'rest_id' => $order_list->rest_id,

		'order_fname' =>$order_list->order_fname,

		'order_lname' => $order_list->order_lname,

		'order_email' => $order_list->order_email,

		'order_tel' => $order_list->order_tel,

		'order_carrier_email' =>$order_list->order_carrier_email,

		'order_address' => $order_list->order_address,

		'order_city' => $order_list->order_city,

		'order_pcode' => $order_list->order_pcode,

		'order_type' => $order_list->order_type,

		'order_typetime' => $order_list->order_typetime,

		'order_tablepic' => $order_list->order_tablepic,

		'order_pickdate' => $order_list->order_pickdate,

		'order_picktime' => $order_list->order_picktime,

		'order_picktime_formate' => $order_list->order_picktime_formate,

		'order_pickarea' => $order_list->order_pickarea,

		'order_instruction' => $order_list->order_instruction,

		'order_cancel_reason' => $order_list->order_cancel_reason,

		'order_create' => $order_list->order_create,

		'order_status' => $order_list->order_status,

		'order_deliveryfee' => $order_list->order_deliveryfee,

		'order_deliveryadd1' => $order_list->order_deliveryadd1,

		'order_deliveryadd2' => $order_list->order_deliveryadd2,

		'order_lat' => $order_list->order_lat,

		'order_long' => $order_list->order_long,

		'order_deliverysurbur' => $order_list->order_deliverysurbur,

		'order_deliverypcode' => $order_list->order_deliverypcode,

		'order_deliverynumber' => $order_list->order_deliverynumber,

		'order_min_delivery' => $order_list->order_min_delivery,

		'order_remaning_delivery' => $order_list->order_remaning_delivery,

		'order_subtotal_amt' => $order_list->order_subtotal_amt,

		'order_carditem' => $order_list->order_carditem,

		'order_promo_mode' => $order_list->order_promo_mode,

		'order_promo_val' => $order_list->order_promo_val,

		'order_promo_cal' => $order_list->order_promo_cal,

		'order_promo_detail' => $order_list->order_promo_detail,

		'order_promo_applied' => $order_list->order_promo_applied,

		'order_food_promo' =>$order_list->order_food_promo,

		'order_food_promo_applied' =>$order_list->order_food_promo_applied,

		'order_service_tax' => $order_list->order_service_tax,

		'order_pmt_type' => $order_list->order_pmt_type,

		'order_pmt_method' => $order_list->order_pmt_method,

		'order_partial_percent' => $order_list->order_partial_percent,

		'order_partial_payment' => $order_list->order_partial_payment,

		'order_partial_remain' => $order_list->order_partial_remain,

		'order_paid_amt' => $order_list->order_paid_amt,

		'order_remaing_amt' =>$order_list->order_remaing_amt,

		'order_total' => $order_list->order_total,

		'created_at' => $order_list->created_at,

		'updated_at' => $order_list->updated_at,

		'order_device' => $order_list->order_device,

		'order_deviceid' => $order_list->order_deviceid,

		'order_devicename' => $order_list->order_devicename,

		'order_device_os' => $order_list->order_device_os,

		'rest_id'=>$order_list->rest_id,

		'rest_name'=>$order_list->rest_name,

		'payment_type'=>$order_list->order_pmt_method,



	);







}











$order_detail_format['total'] = $total_record;

$order_detail_format['per_page'] = $limit;



if($data['page_no']==null){

	$order_detail_format['current_page'] =0;

}else{

	$order_detail_format['current_page'] =$data['page_no'];

}





if ($data['page_no'] > 0) {

	$last_page = $data['page_no']+1;

} else {

	$last_page = $data['page_no'];

}



$lastPage = $total_record/$limit;













$order_detail_format['last_page'] = ceil($lastPage);

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}





}else{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";



}



}



header('Content-type: application/json');

echo json_encode($response);

}







/************************************************************/

//	FUNCTION NAME : vendor_HistoryOrderlist

//	FUNCTION USE :  use to show history order list to vendor

//	FUNCTION ERRORCODE : 200000

/****************************************************/



public function  vendor_HistoryOrderlist(Request $request)

{





$vendor_id = Input::get('userid');

$data['user_id'] = $user_id =Input::get("userid");

$data['page_no'] = $page_no =Input::get("page_no");



$data['from_date'] = $from_date=Input::get('fromdate');

$data['to_date'] = $to_date=Input::get('todate');

$data['status'] = $status = Input::get('order_status');



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';



if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}



$user_id = 0;



$order_detail='';



if((count($rest_id_array)>0) && (!empty($rest_id_array))){



$order_detail  =  DB::table('order')	;

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');

$order_detail = $order_detail->where(function($query) use ($rest_id_array) {



$l=0;

foreach($rest_id_array as $ftype )

{

if($ftype>0)

{

	if($l>0){

		$query = $query->orWhere('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

	else

	{

		$query = $query->Where('order.rest_id','=',$ftype);

		//$query = $query->Where('order.order_status','=',1);

	}

}

$l++;

}



});

/*USER BASE SEARC FORM USER PAGE  */

if($user_id>0)

{

$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

}

/* Status Search */

if(!empty($status) && ($status>0))

{

$order_detail = $order_detail->where('order.order_status', '=', $status);

}

/* Get Data between Date */

if(!empty($from_date) && ($from_date!='0000-00-00'))

{

$order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

}	



if(!empty($to_date) && ($to_date!='0000-00-00'))

{

$order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

}		

/* End */



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);





// $order_detail  =  $order_detail->where('order.order_status','=',6);

// $order_detail  =  $order_detail->where('order.order_status','=',11);

$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.order_id', 'desc');

$order_detail  = $order_detail->get();

}





$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$total_record = count($data_onview['order_detail']);

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "250001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}elseif(!empty($data)){



$vender_detail = DB::table('vendor')

->select('*')

->where('vendor_id', '=' ,$vendor_id)

->get();



$restaurant_detail = DB::table('restaurant')

->select('*')

->where('rest_status', '!=' ,'INACTIVE')

->where('vendor_id', '=' ,$vendor_id)

->get();

$rest_id_array='';

if($restaurant_detail)

{

foreach($restaurant_detail as $rest_data)

{

$rest_id_array[]=$rest_data->rest_id;

}

}

$user_id = 0;

$order_detail='';

if((count($rest_id_array)>0) && (!empty($rest_id_array))){

$order_detail  =  DB::table('order');

$order_detail  = $order_detail->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id');

$order_detail  = $order_detail->leftJoin('users', 'order.user_id', '=', 'users.id');

$order_detail  = $order_detail->leftJoin('order_payment', 'order.order_id', '=', 'order_payment.pay_orderid');

$order_detail = $order_detail->where(function($query) use ($rest_id_array) {

$l=0;

foreach($rest_id_array as $ftype )

{

	if($ftype>0)

	{

		if($l>0){

			$query = $query->orWhere('order.rest_id','=',$ftype);

		}

		else

		{

			$query = $query->Where('order.rest_id','=',$ftype);

		}

	}

	$l++;

}



});



$limit = 10;

$offset = $data['page_no'];

$offset = $offset*$limit;



$order_detail  =  $order_detail->where('order_payment.pay_status', '>', 0);

/*USER BASE SEARC FORM USER PAGE  */

if($user_id>0)

{

$order_detail  =  $order_detail->where('order.user_id', '=', $user_id);

}

/* Status Search */

if(!empty($status) && ($status>0))

{

$order_detail = $order_detail->where('order.order_status', '=', $status);

}

/* Get Data between Date */

if(!empty($from_date) && ($from_date!='0000-00-00'))

{

$order_detail = $order_detail->where('order.created_at', '>=', date('Y-m-d',strtotime($from_date)));

}	



if(!empty($to_date) && ($to_date!='0000-00-00'))

{

$order_detail = $order_detail->where('order.created_at', '<=',date('Y-m-d',strtotime($to_date)));

}		

/* End */



$order_detail  = $order_detail->select('*');

$order_detail  = $order_detail->orderBy('order.order_id', 'desc');

$order_detail  = $order_detail->limit($limit);

$order_detail  = $order_detail->offset($offset);

$order_detail  = $order_detail->get();

}

$data_onview = array('order_detail'=>$order_detail,'userid'=>$user_id);

$order_detail = $data_onview['order_detail'];



if(!empty($order_detail)){

if(count($order_detail)>0)

{

$order_detail_list = '';



foreach($order_detail as $order_list)

{



	$order_detail_list[]= array(

		'order_id' => $order_list->order_id,

		'order_restcartid' => $order_list->order_restcartid,

		'order_guestid' => $order_list->order_guestid,

		'order_uniqueid' => $order_list->order_uniqueid,

		'verification_code' =>$order_list->verification_code,

		'user_type' => $order_list->user_type,

		'user_id' => $order_list->user_id,

		'rest_id' => $order_list->rest_id,

		'order_fname' =>$order_list->order_fname,

		'order_lname' => $order_list->order_lname,

		'order_email' => $order_list->order_email,

		'order_tel' => $order_list->order_tel,

		'order_carrier_email' =>$order_list->order_carrier_email,

		'order_address' => $order_list->order_address,

		'order_city' => $order_list->order_city,

		'order_pcode' => $order_list->order_pcode,

		'order_type' => $order_list->order_type,

		'order_typetime' => $order_list->order_typetime,

		'order_tablepic' => $order_list->order_tablepic,

		'order_pickdate' => $order_list->order_pickdate,

		'order_picktime' => $order_list->order_picktime,

		'order_picktime_formate' => $order_list->order_picktime_formate,

		'order_pickarea' => $order_list->order_pickarea,

		'order_instruction' => $order_list->order_instruction,

		'order_cancel_reason' => $order_list->order_cancel_reason,

		'order_create' => $order_list->order_create,

		'order_status' => $order_list->order_status,

		'order_deliveryfee' => $order_list->order_deliveryfee,

		'order_deliveryadd1' => $order_list->order_deliveryadd1,

		'order_deliveryadd2' => $order_list->order_deliveryadd2,

		'order_lat' => $order_list->order_lat,

		'order_long' => $order_list->order_long,

		'order_deliverysurbur' => $order_list->order_deliverysurbur,

		'order_deliverypcode' => $order_list->order_deliverypcode,

		'order_deliverynumber' => $order_list->order_deliverynumber,

		'order_min_delivery' => $order_list->order_min_delivery,

		'order_remaning_delivery' => $order_list->order_remaning_delivery,

		'order_subtotal_amt' => $order_list->order_subtotal_amt,

		'order_carditem' => $order_list->order_carditem,

		'order_promo_mode' => $order_list->order_promo_mode,

		'order_promo_val' => $order_list->order_promo_val,

		'order_promo_cal' => $order_list->order_promo_cal,

		'order_promo_detail' => $order_list->order_promo_detail,

		'order_promo_applied' => $order_list->order_promo_applied,

		'order_food_promo' =>$order_list->order_food_promo,

		'order_food_promo_applied' =>$order_list->order_food_promo_applied,

		'order_service_tax' => $order_list->order_service_tax,

		'order_pmt_type' => $order_list->order_pmt_type,

		'order_pmt_method' => $order_list->order_pmt_method,

		'order_partial_percent' => $order_list->order_partial_percent,

		'order_partial_payment' => $order_list->order_partial_payment,

		'order_partial_remain' => $order_list->order_partial_remain,

		'order_paid_amt' => $order_list->order_paid_amt,

		'order_remaing_amt' =>$order_list->order_remaing_amt,

		'order_total' => $order_list->order_total,

		'created_at' => $order_list->created_at,

		'updated_at' => $order_list->updated_at,

		'order_device' => $order_list->order_device,

		'order_deviceid' => $order_list->order_deviceid,

		'order_devicename' => $order_list->order_devicename,

		'order_device_os' => $order_list->order_device_os,

		'rest_id'=>$order_list->rest_id,

		'rest_name'=>$order_list->rest_name,

		'payment_type'=>$order_list->order_pmt_method,



	);

}



$order_detail_format['total'] = $total_record;

$order_detail_format['per_page'] = $limit;



if($data['page_no']==null){

	$order_detail_format['current_page'] =0;

}else{

	$order_detail_format['current_page'] =$data['page_no'];

}



if ($data['page_no'] > 0) {

	$last_page = $data['page_no']+1;

} else {

	$last_page = $data['page_no'];

}



$lastPage = $total_record/$limit;



$order_detail_format['last_page'] = ceil($lastPage);

$order_detail_format['data'] =$order_detail_list;

$response['message'] = "Order List.";

$response['status']= 1;

$response['order_list'] =  $order_detail_format;

}

else

{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";

}



}else{

$response['errorcode'] = "250002";

$response['status']= 0;

$response['message']="Order not found";



}



}



header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : vendor_order_accept_or_reject

//	FUNCTION USE :  use to update status order accept or reject by vendor

//	FUNCTION ERRORCODE : 500000

/****************************************************/

public function vendor_order_accept_or_reject(Request $request)

{

$user_id = Input::get('userid');

$order_id = Input::get('orderid');

$data['user_id'] = Input::get("userid");

$data['orderid'] = Input::get("orderid");

$data['status'] = Input::get("status");

$data['resion'] = Input::get("resion");

$response = array();

if(empty($data['user_id'])){

$response['errorcode'] = "500001";

$response['status']= 0;

$response['message']="User id:Required parameter missing";

}elseif(empty($data['orderid'])){

$response['errorcode'] = "500002";

$response['status']= 0;

$response['message']="Order id:Required parameter missing";

}elseif(empty($data['status'])){

$response['errorcode'] = "500003";

$response['status']= 0;

$response['message']="Status:Required parameter missing";

}elseif($data['status']==6 && empty($data['resion'])){

$response['errorcode'] = "500004";

$response['status']= 0;

$response['message']="Resion:Required parameter missing";

}elseif(!empty($data)){

$order_existornot  =  DB::table('order')

->where('order_id', '=', $order_id)

->select('*')

->orderBy('order.order_create', 'desc')

->first();



$userid = DB::table('order')

       ->where('order_id', '=', trim($order_id))

       ->value('user_id'); 



$token_data = DB::table('device_token')

       ->where('userid', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();



if(empty($token_data)){



$token_data = DB::table('device_token')

       ->where('guest_id', '=', trim($userid))

       ->where('notification_status', '=', 1)

       ->get();

}



if($token_data){



$target = $token_data[0]->devicetoken;



$devicetype = $token_data[0]->devicetype;	



$deviceid = $token_data[0]->deviceid;	



}



$notification_description = '';



if(!empty($order_existornot->order_id)){

$status = trim(Input::get('status'));



/*4-Order confirm*/

if($status==4){

$status=4;

DB::table('order')

->where('order_id', $order_id)

->update([

	'order_status' =>$status,

	'updated_at' =>date('Y-m-d H:i:s')

]);

$response['message'] = "Order Confirm!";

$response['status']= 1;



$notification_description = "Thanks your order successfully placed";



}



/*5-Order Ready*/

if($status==5){

$status=5;

DB::table('order')

->where('order_id', $order_id)

->update([

	'order_status' =>$status,

	'updated_at' =>date('Y-m-d H:i:s')

]);

$response['message'] = "Order Ready!";

$response['status']= 1;



$notification_description = "Your order prepared";

}



/*6-Order Cancelled*/

if($status==6){

$status=6;



$resion = trim(Input::get('resion'));



DB::table('order')

->where('order_id', $order_id)

->update([

	'order_status' =>$status,

	'order_cancel_reason'=>$resion,

	'updated_at' =>date('Y-m-d H:i:s')

]);

$response['message'] = "Order Cancelled!";

$response['status']= 1;



$notification_description = $resion;



}



$notification_title = $response['message'];



$sendnotify = $this->send_notification_ios($target,$notification_title,$notification_description);



//return $target;



$notifiy = new Notification_list;

$notifiy->noti_userid = $userid;

$notifiy->noti_title = $response['message'];

$notifiy->noti_desc = $notification_description;

$notifiy->noti_deviceid = $deviceid;

$notifiy->noti_devicetype = $devicetype;

$notifiy->save();



}else{



$response['errorcode'] = "500004";

$response['message'] = "Invalid user id or order id!";

$response['status']= 0;



}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : show_order_detail_vendor

//	FUNCTION USE :  Show particular  order detail to vendor

//	FUNCTION ERRORCODE : 420000

/****************************************************/

function show_order_detail_vendor()

{



$response = array();

//$data['user_id'] =$user_id = Input::get("userid");

// $data['guest_id'] = $guest_id = Input::get("guest_id");

// $data['deviceid'] = $deviceid = Input::get("deviceid");

// $data['devicetype'] = $devicetype = Input::get("devicetype");

$data['orderid'] = $order_id = Input::get("orderid");

// if(empty($data['user_id'])){

// 	$response['errorcode'] = "420001";

// 	$response['status']= 0;

// 	$response['message']="User Id:Required parameter missing";

// }else



if(empty($data['orderid'])){

$response['errorcode'] = "420002";

$response['status']= 0;

$response['message']="Order Id : Required parameter missing";

}elseif (!empty($data)){



// $order_exist = DB::table('delivery_man_orders')

// ->where('order_id', $order_id)

// ->where('dm_id', $user_id)

// ->get();

//DB::table('user_visits')->groupBy('user_id')->count();

//if($order_exist){

$order_detail = DB::table('order')

->leftJoin('restaurant', 'order.rest_id', '=', 'restaurant.rest_id')

->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')

->where('order_id', $order_id)->get();





// echo "<pre>";

// print_r($order_detail[0]->order_carditem);

// echo "</pre>";

// die;







if(count($order_detail)>0)

{

$order_tip = $order_detail[0]->order_tip;

$cart_tiem = json_decode($order_detail[0]->order_carditem, true);

//print_r($cart_tiem);











$sub_total_cart = 0;

foreach($cart_tiem as $c)

{

$addon_cart_id='';

$rest_id  = $order_detail[0]->rest_id;

$menu_id =  $c['options']['menu_id'];

$menu_cate_id = $c['options']['menu_catid'];

$menu_subcate_id =  $c['options']['option_name'];

$addon_id = '';

$addon_data = $c['options']['addon_data'];

$addon_id = '';

if(!empty($addon_data))

{

	foreach($addon_data as $ad)

	{

		$addon_cart_id[] =$ad['id'];

	}

		//print_r( $addon_cart_id);

	$addon_id = implode(',',$addon_cart_id);

}

$foodsize_price =0;

$food_addonPrice = 0;

$total_price =0 ;

$item_name_cc = '';

$item_price_cc = '';

$offer_on_menu_item = '';

$menu_list = DB::table('menu')

->where('restaurant_id', '=' ,$rest_id)

->where('menu_id', '=' ,$menu_id)

->where('menu_status', '=' ,'1')

->orderBy('menu_order', 'asc')

->get();

// dd(DB::getQueryLog());

$menu_array = '';

$food_array='';

if(!empty($menu_list))

{

	$menu_id = $menu_list[0]->menu_id;

	$menu_name = $menu_list[0]->menu_name;

	$menu_item_count = 1;

	$food_detail='';

	$menu_cat_detail = DB::table('menu_category')

	->where('menu_category.rest_id', '=' ,$rest_id)

	->where('menu_category.menu_id', '=' ,$menu_id)

	->where('menu_category.menu_category_id', '=' ,$menu_cate_id)

	->where('menu_category.menu_cat_status', '=' ,'1')

	->select('menu_category.*')

	->orderBy('menu_category.menu_category_id', 'asc')

	->get();

	$food_detail['food_id'] = $menu_cat_detail[0]->menu_category_id;

	$food_detail['food_popular'] = $menu_cat_detail[0]->menu_cat_popular;

	$food_detail['food_diet'] = $menu_cat_detail[0]->menu_cat_diet;

	$food_detail['food_name'] = $item_name_cc = $menu_cat_detail[0]->menu_category_name;

	$food_detail['food_price'] = $item_price_cc = $menu_cat_detail[0]->menu_category_price;

	$food_detail['food_desc'] = $menu_cat_detail[0]->menu_category_desc;

	$food_detail['food_subitem'] = $menu_cat_detail[0]->menu_category_portion;

	$food_detail['food_subitem_title']='';

	if($menu_cat_detail[0]->menu_category_portion=='yes')

	{

		$food_detail['food_subitem_title'] = 'Choose a size';

		$food_size = DB::table('category_item')

		->where('rest_id', '=' ,$rest_id)

		->where('menu_id', '=' ,$menu_id)

		->where('menu_category', '=' ,$menu_cate_id)

		->where('menu_cat_itm_id', '=' ,$menu_subcate_id)

		->where('menu_item_status', '=' ,'1')

		->select('*')

		->orderBy('menu_cat_itm_id', 'asc')

		->groupBy('menu_item_title')

		->get();

		$food_detail['food_size_detail']=$food_size;

		$item_price_cc =$food_size[0]->menu_item_price;

		$total_price = number_format(($total_price + $food_size[0]->menu_item_price),2);

		$foodsize_price = number_format(($foodsize_price+$food_size[0]->menu_item_price),2);

	}

	else

	{

		$food_detail['food_size_detail']=array();

		$total_price = number_format(($total_price +$menu_cat_detail[0]->menu_category_price),2);

	}

	$addon_list='';

	if(!empty($addon_id)){

		$addons = DB::table('menu_category_addon')

		->where('addon_restid', '=' ,$rest_id)

		->where('addon_menuid', '=' ,$menu_id)

		->where('addon_status', '=' ,'1')

		->where('addon_menucatid', '=' ,$menu_cate_id)

		->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

		->select('*')

		->orderBy('addon_id', 'asc')

		->groupBy('addon_groupname')

		->get();

		if(!empty($addons))

		{

			foreach($addons as $ad_list)

			{

				$option_type = 0;

				if(substr($ad_list->addon_groupname, -1)=='*')

				{

					$option_type = 1;

				}

				$group_name[]=$ad_list->addon_groupname;

				$ff['addon_gropname'] = $ad_list->addon_groupname;

				$ff['addon_type'] = $ad_list->addon_option;

				$ff['addon_mandatory_or_not'] = $option_type;

				$addon_group = DB::table('menu_category_addon')

				->where('addon_restid', '=' ,$rest_id)

				->where('addon_menuid', '=' ,$menu_id)

				->where('addon_status', '=' ,'1')

				->where('addon_groupname', '=' ,$ad_list->addon_groupname)

				->where('addon_menucatid', '=' ,$menu_cate_id)

				->whereRaw('FIND_IN_SET(addon_id,"'.$addon_id.'")')

				->select('*')

				->orderBy('addon_id', 'asc')

				->get();

				$group_list[]=$addon_group;

				$addon_group_list ='';

				foreach($addon_group as $group_list_loop)

				{

						//$addon_group_list[]=array('')

					$addon_group_list[]=array('addon_id'=>$group_list_loop->addon_id,

						'addon_menucatid'=>$group_list_loop->addon_menucatid,

						'addon_groupname'=>$group_list_loop->addon_groupname,

						'addon_option'=>$group_list_loop->addon_option,

						'addon_name'=>$group_list_loop->addon_name,

						'addon_price'=>$group_list_loop->addon_price,

						'addon_status'=>$group_list_loop->addon_status

					);

					$addon_list[]=array('name'=>$group_list_loop->addon_name,

						'price'=>$group_list_loop->addon_price,

						'id'=>$group_list_loop->addon_id);

					$total_price = number_format( ($total_price + $group_list_loop->addon_price),2);

					$food_addonPrice = $food_addonPrice.$group_list_loop->addon_price.',';

				}

				$ff['addon_detail'] = $addon_group_list;

				$food_detail['food_addon'][]=$ff;

			}

		}

	}

	else

	{

		$food_detail['food_addon'] = array();

	}

	$food_array[] = array('menu_id'=>$menu_id,'menu_name'=>$menu_name,'food_counter'=>$menu_item_count,'food_detail'=>$food_detail,'offer_on_item'=>$offer_on_menu_item);

}

/******** FOOD DETAIL END ****/

$sub_total_cart = ($sub_total_cart+($total_price*$c['qty']));

$special_instruction='';

if(!empty(@$c['instruction'])){$special_instruction=@$c['instruction'];}

$cart_detail[] = array(

	"cart_id"=> $c['rowId'],

	"rest_cartid"=>'',

	"user_id" => $order_detail[0]->user_id,

	"guest_id" => '',

	"rest_id"=> $order_detail[0]->rest_id,

	"menu_id"=> $c['options']['menu_id'],

	"food_id"=> $c['options']['menu_catid'],

	"food_size_id"=> $c['options']['option_name'],

	"food_addonid"=> '',

	"qty"=>  $c['qty'],

	"special_instruction"=>$special_instruction,

	"food_detail"=>$food_array,

	"foodsize_price"=>$foodsize_price,

	"food_addonPrice"=>$food_addonPrice,

	"total_price"=>$total_price,

	//"order_tip"=>$order_tip

);

}

$review_post=0;

if(!empty($order_detail[0]->re_id))

{

$review_post=1;

}

$order_detail_data['order_id']=$order_detail[0]->order_id;

$order_detail_data['order_restcartid']=$order_detail[0]->order_restcartid;

$order_detail_data['order_uniqueid']=$order_detail[0]->order_uniqueid;

$order_detail_data['user_type']=$order_detail[0]->user_type;

$order_detail_data['user_id']=$order_detail[0]->user_id;

$order_detail_data['rest_id']=$order_detail[0]->rest_id;

$order_detail_data['rest_name']=$order_detail[0]->rest_name;

$order_detail_data['rest_address']=$order_detail[0]->rest_address.' '.$order_detail[0]->rest_zip_code;

$order_detail_data['rest_logo']=trim(url('/').'/uploads/reataurant/'.$order_detail[0]->rest_logo);

$order_detail_data['order_fname']=$order_detail[0]->order_fname;

$order_detail_data['order_lname']=$order_detail[0]->order_lname;

$order_detail_data['order_email']=$order_detail[0]->order_email;

$order_detail_data['order_tel']=$order_detail[0]->order_tel;

$order_detail_data['order_address']=$order_detail[0]->order_address;

$order_detail_data['order_city']=$order_detail[0]->order_city;

$order_detail_data['order_pcode']=$order_detail[0]->order_pcode;

$order_detail_data['order_type']=$order_detail[0]->order_type;

$order_detail_data['order_typetime']=$order_detail[0]->order_typetime;

$order_detail_data['order_tablepic']=$order_detail[0]->order_tablepic;

$order_detail_data['order_pickdate']=date('d-m-Y',strtotime($order_detail[0]->order_pickdate));

$order_detail_data['order_picktime']=$order_detail[0]->order_picktime;

$order_detail_data['order_picktime_formate']=$order_detail[0]->order_picktime_formate;

$order_detail_data['order_pickarea']=$order_detail[0]->order_pickarea;

$order_detail_data['order_instruction']=$order_detail[0]->order_instruction;

$order_detail_data['order_cancel_reason']=$order_detail[0]->order_cancel_reason;

//$order_detail_data['order_create']=date('d-m-Y',strtotime($order_detail[0]->order_create));



$order_detail_data['order_create']=$order_detail[0]->order_create;

//$order_detail_data['order_status']=$order_exist[0]->order_status;

$order_detail_data['order_status']=$order_detail[0]->order_status;

$order_detail_data['order_deliveryfee']=$order_detail[0]->order_deliveryfee;

$order_detail_data['order_deliveryadd1']=$order_detail[0]->order_deliveryadd1;

$order_detail_data['order_deliveryadd2']=$order_detail[0]->order_deliveryadd2;

$order_detail_data['order_deliverysurbur']=$order_detail[0]->order_deliverysurbur;

$order_detail_data['order_deliverypcode']=$order_detail[0]->order_deliverypcode;

$order_detail_data['order_deliverynumber']=$order_detail[0]->order_deliverynumber;

$order_detail_data['order_min_delivery']=$order_detail[0]->order_min_delivery;

$order_detail_data['order_remaning_delivery']=$order_detail[0]->order_remaning_delivery;

$order_detail_data['order_subtotal_amt']=$order_detail[0]->order_subtotal_amt;





$json = $order_detail[0]->order_carditem;

$jsonarr = json_decode($json);



//print_r($jsonarr); die;



$new_arr = array();

$new_array = array();



foreach ($jsonarr as $value)

{



foreach ($value as $key => $value) {



    if($key=='price'){ $value = (string)$value; }

    if($key=='tax'){ $value = (string)$value; }

    if($key=='subtotal'){ $value = (string)$value; }



	$new_array[$key] = $value ; 

	

  }



$new_arr[] = $new_array;		



}



//print_r($new_arr); die;





/*$order_detail_data['order_carditem']= json_decode($order_detail[0]->order_carditem, true);*/



$order_total1 = (string)$order_detail[0]->order_total;

$total_pay = (string)$order_detail[0]->order_tip+$order_detail[0]->order_total;



$order_detail_data['order_carditem']= $new_arr;

$order_detail_data['order_promo_mode']=$order_detail[0]->order_promo_mode;

$order_detail_data['order_promo_val']=$order_detail[0]->order_promo_val;

$order_detail_data['order_promo_cal']=$order_detail[0]->order_promo_cal;

$order_detail_data['order_promo_detail']=json_decode($order_detail[0]->order_promo_detail);

$order_detail_data['order_service_tax']=$order_detail[0]->order_service_tax;

$order_detail_data['order_pmt_type']=$order_detail[0]->order_pmt_type;

$order_detail_data['order_partial_percent']=$order_detail[0]->order_partial_percent;

$order_detail_data['order_partial_payment']=$order_detail[0]->order_partial_payment;

$order_detail_data['order_partial_remain']=$order_detail[0]->order_partial_remain;

$order_detail_data['order_pmt_method']=$order_detail[0]->order_pmt_method;

$order_detail_data['order_total']="$order_total1";

$order_detail_data['order_tip']=$order_detail[0]->order_tip;

$order_detail_data['order_grand_total']="$total_pay";

//$order_detail_data['review_flag']=$review_post;

$response['order_id']=$order_id;

$response['order_detail']=$order_detail_data;

//$response['cart_detail']=$cart_detail;

$response['Food_detail']=$food_array;



$response['offer_aplied']=$order_detail[0]->order_promo_applied;

$response['food_offer_aplied']=json_decode($order_detail[0]->order_food_promo_applied);

$response['status']= 1;

$response['message']='Order Detail!';

}else{

$response['order_id']=$order_id;

$response['errorcode'] = "420003";

$response['status']= 0;

$response['message']='Invalid Order id!';

}

// }else{

// 	$response['user_id']=$user_id;

// 	$response['orderid']=$order_id;

// 	$response['errorcode'] = "420004";

// 	$response['status']= 0;

// 	$response['message']='Invalid user id or order id';

// }

}

header('Content-type: application/json');

echo json_encode($response);

}









/************************************************************/

//	FUNCTION NAME : vendor_device_token

//	FUNCTION USE :  Store device tokan and other info for vendor

//	FUNCTION ERRORCODE : 80000

/****************************************************/

public function vendor_device_token (Request $request){

$data['userid'] = $user_id = Input::get("userid");

$data['devicetoken'] = Input::get("devicetoken");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

$response = array();

if(	(($user_id==0) || empty($user_id)))

{

$response['errorcode'] = "80001";

$response['status']= 0;

$response['message']="user id:Required parameter missing";

}elseif(empty($data['devicetoken'])){

$response['errorcode'] = "80002";

$response['status']= 0;

$response['message']="device token:Required parameter missing";

}else if(empty($data['deviceid'])){

$response['errorcode'] = "80003";

$response['status']= 0;

$response['message']="device id:Required parameter missing";

}else if(empty($data['devicetype'])){

$response['errorcode'] = "80004";

$response['status']= 0;

$response['message']="device type:Required parameter missing";

}elseif (!empty($data)){



if(Input::get('devicetype')=='android')

{

if(($user_id>0) &&(!empty($user_id)))

{

$user_detail = DB::table('vendor_device_token')

->where('userid', '=' ,trim($user_id))

->get();

}



if(count($user_detail)>0)

{

if(($user_id>0) && (!empty($user_id)))

{

	DB::table('vendor_device_token')

	->where('userid', '=' ,trim(Input::get('userid')))

	->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype')),'updated_at' => date('Y-m-d h:i:s')]);

}





$response['message']="device Token Updated sucessfully!";

}

else

{



$deviceToken=array(

	'userid'=>trim($user_id),

	'notification_status'=>trim(1),

	'devicetoken'=>trim(Input::get('devicetoken')),

	'deviceid'=>trim(Input::get('deviceid')),

	'devicetype'=>trim(Input::get('devicetype')),

);





DB::table('vendor_device_token')->insert($deviceToken);





$response['message']="device Token sucessfully added!";

}

$response['status']= 1;

}

elseif(Input::get('devicetype')=='iphone')

{

if(($user_id>0) &&(!empty($user_id)))

{

$user_detail = DB::table('vendor_device_token')

->where('userid', '=' ,trim($user_id))

->get();

}



if(count($user_detail)>0)

{

if(($user_id>0) &&(!empty($user_id)))

{

	DB::table('vendor_device_token')

	->where('userid', '=' ,trim(Input::get('userid')))

    ->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype')),'updated_at' => date('Y-m-d h:i:s')]);

}



$response['message']="device Token Updated sucessfully!";

}

else

{



$deviceToken=array(

	'userid'=>trim($user_id),

	'notification_status'=>trim(1),

	'devicetoken'=>trim(Input::get('devicetoken')),

	'deviceid'=>trim(Input::get('deviceid')),

	'devicetype'=>trim(Input::get('devicetype')),

);





DB::table('vendor_device_token')->insert($deviceToken);



$response['message']="device Token sucessfully added!";

}

$response['status']= 1;

}

}

header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : vendor_device_token

//	FUNCTION USE :  Store device tokan and other info for vendor

//	FUNCTION ERRORCODE : 80000

/****************************************************/

public function deliveryman_device_token (Request $request){

$data['userid'] = $user_id = Input::get("userid");

$data['devicetoken'] = Input::get("devicetoken");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

$response = array();

if(	(($user_id==0) || empty($user_id)))

{

$response['errorcode'] = "80001";

$response['status']= 0;

$response['message']="user id:Required parameter missing";

}elseif(empty($data['devicetoken'])){

$response['errorcode'] = "80002";

$response['status']= 0;

$response['message']="device token:Required parameter missing";

}else if(empty($data['deviceid'])){

$response['errorcode'] = "80003";

$response['status']= 0;

$response['message']="device id:Required parameter missing";

}else if(empty($data['devicetype'])){

$response['errorcode'] = "80004";

$response['status']= 0;

$response['message']="device type:Required parameter missing";

}elseif (!empty($data)){



if(Input::get('devicetype')=='android')

{

if(($user_id>0) &&(!empty($user_id)))

{

$user_detail = DB::table('deliveryman_device_token')

->where('userid', '=' ,trim($user_id))

->get();

}



if(count($user_detail)>0)

{

if(($user_id>0) && (!empty($user_id)))

{

	DB::table('deliveryman_device_token')

	->where('userid', '=' ,trim(Input::get('userid')))

	->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype')),'updated_at' => date('Y-m-d h:i:s')]);

}





$response['message']="device Token Updated sucessfully!";

}

else

{



$deviceToken=array(

	'userid'=>trim($user_id),

	'notification_status'=>trim(1),

	'devicetoken'=>trim(Input::get('devicetoken')),

	'deviceid'=>trim(Input::get('deviceid')),

	'devicetype'=>trim(Input::get('devicetype')),

);





DB::table('deliveryman_device_token')->insert($deviceToken);





$response['message']="device Token sucessfully added!";

}

$response['status']= 1;

}

elseif(Input::get('devicetype')=='iphone')

{

if(($user_id>0) &&(!empty($user_id)))

{

$user_detail = DB::table('deliveryman_device_token')

->where('userid', '=' ,trim($user_id))

->get();

}



if(count($user_detail)>0)

{

if(($user_id>0) &&(!empty($user_id)))

{

	DB::table('deliveryman_device_token')

	->where('userid', '=' ,trim(Input::get('userid')))

    ->update(['devicetoken' => trim($data['devicetoken']),'deviceid' => trim(Input::get('deviceid')),'devicetype' => trim(Input::get('devicetype')),'updated_at' => date('Y-m-d h:i:s')]);

}



$response['message']="device Token Updated sucessfully!";

}

else

{



$deviceToken=array(

	'userid'=>trim($user_id),

	'notification_status'=>trim(1),

	'devicetoken'=>trim(Input::get('devicetoken')),

	'deviceid'=>trim(Input::get('deviceid')),

	'devicetype'=>trim(Input::get('devicetype')),

);





DB::table('deliveryman_device_token')->insert($deviceToken);



$response['message']="device Token sucessfully added!";

}

$response['status']= 1;

}

}

header('Content-type: application/json');

echo json_encode($response);

}	





/************************************************************/

//	FUNCTION NAME : show_vendor_notification

//	FUNCTION USE :  Show all notification listing

//	FUNCTION ERRORCODE : 440000

/****************************************************/



function show_vendor_notification()

{

$response = array();

$data['user_id'] =	$user_id = Input::get("userid");

//$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

if(empty($data['user_id'])){

$response['errorcode'] = "430001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";

}

elseif (!empty($data)){

$limit = 10;

//$notification_detail = DB::table('notification_list')->where('noti_userid', $user_id)->get();

$notification_detail = DB::table('vendor_notification_list')

->where('noti_userid', '=' ,$user_id)

->select('*')

->orderBy('noti_id', 'desc')

->paginate($limit);

$notification_counter = $notification_detail->total();

if($notification_counter>0)

{

$notification_data='';

foreach($notification_detail as $notify)

{



$ago_time = $this->calculate_time_span($notify->updated_at) ;



$notification_data[] = array(

	'noti_id' => $notify->noti_id,

	'noti_userid' => $notify->noti_userid,

	//'noti_guestid' => $notify->noti_guestid,

	'noti_title' => $notify->noti_title,

	'noti_desc' => $notify->noti_desc,

	'noti_deviceid' => $notify->noti_deviceid,

	'noti_read' => $notify->noti_read,

	'ago_time' => $ago_time,

	'created_at' => date("H:i A d-m-Y", (strtotime($notify->created_at))),

	'updated_at' => date("H:i A d-m-Y", (strtotime($notify->updated_at)))

);

}

$rest_detail_format['total'] =$notification_detail->total() ;

$rest_detail_format['per_page'] =$notification_detail->perPage();

$rest_detail_format['current_page'] =$notification_detail->currentPage();;

$rest_detail_format['last_page'] =$notification_detail->lastPage();

$rest_detail_format['data'] =$notification_data;

/*for ($i=0; $i < $rest_detail_format['per_page']; $i++) {

	$rest_detail_format['data'][$i]->created_at = date("H:i A d-m-Y", (strtotime($rest_detail_format['data'][$i]->created_at)));

}*/

$response['user_id']=$user_id;

$response['notification_detail']=$rest_detail_format;

$response['status']= 1;

//$response['message']='View Your Order!';

}

else

{

$response['user_id']=$user_id;

$response['errorcode'] = "430002";

$response['status']= 0;

$response['message']='notification list not found!';

}

}

header('Content-type: application/json');

echo json_encode($response);

}









/************************************************************/

//	FUNCTION NAME : update_vendor_notification_status

//	FUNCTION USE :  USER NOTIFICATION STAUTS

//	FUNCTION ERRORCODE : 270000

/****************************************************/

public function update_vendor_notification_status()

{

/*print_r($this->param);

exit;*/

DB::enableQueryLog();

$data['userid'] = $user_id =Input::get("userid");

//$data['guest_id'] = $guest_id =Input::get("guest_id");

$data['status'] = Input::get("status");

$data['deviceid'] = Input::get("deviceid");

$data['devicetype'] = Input::get("devicetype");

//	if(empty($data['user_id']))

if( (($user_id==0) || empty($user_id)) && (empty($guest_id)))

{

$response['errorcode'] = "270001";

$response['status']= 0;

$response['message']="User Id/Guest Id:Required parameter missing";

}elseif(($data['status']=='') || ($data['status']<0)){

$response['errorcode'] = "270002";

$response['status']= 0;

$response['message']="Notofication Status:Required parameter missing";

}elseif(empty($data['deviceid'])){

$response['errorcode'] = "270003";

$response['status']= 0;

$response['message']="Device Id:Required parameter missing";

}elseif(empty($data['devicetype'])){

$response['errorcode'] = "270004";

$response['status']= 0;

$response['message']="Device type:Required parameter missing";

}elseif(!empty($data)){

$device_listing = DB::table('device_token');

$device_listing = $device_listing->select('*');

if(($user_id>0) &&(!empty($user_id)))

{

$device_listing = $device_listing->where('userid', '=',Input::get("userid"));

}elseif(!empty($guest_id))

{

$device_listing = $device_listing->where('guest_id','=', trim($guest_id));

}

if(Input::get("devicetype")=='android')

{

$device_listing = $device_listing->where('deviceid', '=',Input::get("deviceid"));

}

$device_listing = $device_listing->where('devicetype','=', Input::get("devicetype"));

$device_listing = $device_listing->get();

//dd(DB::getQueryLog());

if(count($device_listing)>0)

{

/*		DB::table('device_token')

->where('userid', Input::get("userid"))

->where('deviceid', Input::get("deviceid"))

->where('devicetype', Input::get("devicetype"))

->update(['notification_status' => trim(Input::get("status"))

]);*/

if(($user_id>0) &&(!empty($user_id)))

{

	//->where('deviceid', Input::get("deviceid"))

DB::table('device_token')

->where('userid', Input::get("userid"))

->where('devicetype', Input::get("devicetype"))

->update(['notification_status' => trim(Input::get("status"))

]);

}elseif(!empty($guest_id))

{

	//->where('deviceid', Input::get("deviceid"))

DB::table('device_token')

->where('guest_id', trim($guest_id))

->where('devicetype', Input::get("devicetype"))

->update(['notification_status' => trim(Input::get("status"))

]);

}

$device_data = DB::table('device_token');

if(($user_id>0) &&(!empty($user_id)))

{

$device_data = $device_data->where('userid', Input::get("userid"));

}elseif(!empty($guest_id))

{

$device_data = $device_data->where('guest_id', '=' ,trim($guest_id));

}

	//$device_data = $device_data->where('deviceid', Input::get("deviceid"));

$device_data = $device_data->where('devicetype', Input::get("devicetype"));

$device_data = $device_data->get();

$response['message'] = "Notification status successfully updated.";

$response['status']= 1;

$response['data'] = array('user_detail'=>$device_data);

}

else

{

$response['errorcode'] = "270005";

$response['status']= 0;

$response['message']="This user not registered";

}

}

header('Content-type: application/json');

echo json_encode($response);

}



public function coupon_code_apply_api()

{



$user_ids = trim(Input::get('user_id'));

$guest_id = trim(Input::get('guest_id'));

$coupon_code = trim(Input::get('coupon_code'));

$rest_cartid = trim(Input::get('rest_cartid'));

$total_amount = trim(Input::get('total_amount'));



if(!empty($user_ids)){



$user_id = $user_ids;

$keys = 'user_id';	



}else{



$user_id = $guest_id;

$keys = 'order_guestid';



}



$check_code = DB::table('coupon_code')		

->where('coupon', '=' ,$coupon_code)

->where('status', '=' ,1)

->get();



$ccount = count($check_code);	



if($ccount>0){



$applied = DB::table('order')		

->where('coupon_code', '=' ,$coupon_code)

->where($keys, '=' ,$user_id)

->get();



$apcount = count($applied);



if($apcount>0){



$response['message']="Coupon code already applied";

$response['status']= 0;

$response['errorcode'] = "470004";	



}else{



$temp_apply = DB::table('coupon_code_apply')		

->where('user_id', '=' ,$user_id)

->where('coupon', '=' ,$coupon_code)

->where('total_amount', '=' ,$total_amount)

->where('temp_status', '=' ,1)

->get();



$tempcount = count($temp_apply);



if($tempcount>0){



$response['message']="Coupon code already applied";

$response['status']= 0;

$response['errorcode'] = "470004";



}else{



/* condition repeat coupon and new coupon apply */



$get_total_amount = DB::table('coupon_code_apply')		

->where('user_id', '=' ,$user_id)

->where('coupon', '=' ,$coupon_code)

->where('temp_status', '=' ,1)

->value('total_amount'); 



$gtamount = count($get_total_amount);





if(($gtamount>0) && ($get_total_amount!=$total_amount)){



DB::table('coupon_code_apply')

	->where('user_id', $user_id)

	->where('coupon', $coupon_code)

	->update(['temp_status' => trim('0')]);



}else{



$dtemp_apply = DB::table('coupon_code_apply')		

->where('user_id', '=' ,$user_id)

->where('total_amount', '=' ,$total_amount)

->where('temp_status', '=' ,1)

->get();



$dtempcount = count($dtemp_apply);



if($dtempcount>0){



$response['message']="Coupon code already applied";

$response['status']= 0;

$response['errorcode'] = "470004";



return $response;



}else{



DB::table('coupon_code_apply')

    ->where('user_id', '=' ,$user_id)

    ->delete();	



 }



}



/* End reapeat coupon condition */





$temp_check = DB::table('coupon_code_apply')		

->where('user_id', '=' ,$user_id)

->where('coupon', '=' ,$coupon_code)

->where('temp_status', '=' ,0)

->get();



//return $temp_check;



$tempcheckc = count($temp_check);	



if($tempcheckc>0){



DB::table('coupon_code_apply')

	->where('user_id', $user_id)

	->where('coupon', $coupon_code)

	->update(['temp_status' => trim('1'),'total_amount' =>$total_amount,'update_date'=>date('Y-m-d H:i:s')]);



}else{



$coupon_details=array(

'user_id'=> $user_id,

'coupon'=> $coupon_code,

'rest_cartid'=>	$rest_cartid,

'temp_status'=> 1,

'total_amount'=> $total_amount,

'apply_date'=>date('Y-m-d H:i:s'),

'update_date'=>date('Y-m-d H:i:s')

);



DB::table('coupon_code_apply')->insert($coupon_details);



}



if(!empty($user_ids)){



$user_id = $user_ids;

$keys = 'user_id';	



}else{



$user_id = $guest_id;

$keys = 'guest_id';



}	



$coupon_status = 1;



DB::table('temp_cart')

	->where($keys, $user_id)

	->update(['coupon_code' =>$coupon_code,'coupon_amount' =>$check_code[0]->amount,'coupon_status' =>$coupon_status]);



$response['message'] = "success";

$response['status']= 1;

$response['data'] = $check_code[0]; 



}





}



}else{



$response['message']="Invelid Coupon Code";

$response['status']= 0;

$response['errorcode'] = "470004";



}



return $response; 



}



public function coupon_code_remove()

{



$user_id = trim(Input::get('user_id'));

$rest_cartid = trim(Input::get('rest_cartid'));



if(!empty($rest_cartid)){



$response['message'] = "Successfully Remove coupon from cart";

$response['status']= 1;	



}else{



$response['message']="Required : Rest Cart Id";

$response['status']= 0;

$response['errorcode'] = "470004";



}



return $response; 



}	





public function testmail()

{



   $address = 'votivedeepak.php@gmail.com';

   $subject = 'Testmail';

   $message = 'testmailSSS';



   $testmail = $this->sendMail($address,$subject,$message);



   return $testmail;



}


/************************************************************/

/*Vendor API's end*/

/************************************************************/

public function sendMail($address,$subject,$otpmessage)
{

	$data['address'] = $address;
	$data['subject'] = $subject;

 	Mail::send("emails.api_email",["apimsg" => $otpmessage],function($message) use ($data){

 		$message->from(config("app.webmail"), config("app.mailname"));

        $message->subject($data['subject']);

        $message->to($data['address']);

 	});

return "1";

/*$mail = new PHPMailer\PHPMailer();

$mail->IsSMTP(); // telling the class to use SMTP

//$mail->Host       = "smtp.gmail.com"; // SMTP server

//$mail->SMTPDebug  = 1;      // enables SMTP debug information (for testing)

$mail->SMTPAuth   = true;                 // enable SMTP authentication

$mail->Port       = 465;                    // set the SMTP server port

$mail->Host       = "mail.conceptline.org"; // SMTP server

$mail->Username   = "app@grambunny.com";     // SMTP server username

$mail->Password   = "z4g&nF22";            // SMTP server password

$mail->SetFrom('app@grambunny.com', 'grambunny');

$mail->Subject = $subject;

$mail->MsgHTML($message);

$mail->IsHTML(true); // send as HTML

$address = $address;

$mail->AddAddress($address);

// $mail->AddCC('votivedeepak.php@gmail.com');

if(!$mail->Send()) {

//return $mail->ErrorInfo ;

return "0";

} else {

return "1";

} */

}



/************************************************************/

//	FUNCTION NAME : update_delivery_address

//	FUNCTION USE :  Addd new delivery address 

//	FUNCTION ERRORCODE : 350000

/****************************************************/	


public function update_delivery_address()

{

$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['del_id'] = $del_id = Input::get("del_id");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");

$data['street_address'] = $street_address = Input::get("street_address");

$data['city'] = $city = Input::get("city");

$data['state'] = $state = Input::get("state");

$data['zipcode'] = $zipcode = Input::get("zipcode");

$data['country'] = $country = Input::get("country");

$data['phone_no'] = $address_lat = Input::get("phone_no");

$data['address_lat'] = $address_lat = Input::get("address_lat");

$data['address_lang'] = $address_lang = Input::get("address_lang");



if(empty($data['rest_id'])){

$response['errorcode'] = "350001";

$response['status']= 0;

$response['message']="Restaurant Id:Required parameter missing";



}elseif(empty($data['street_address'])){

$response['errorcode'] = "350002";

$response['status']= 0;

$response['message']="Street Address : Required parameter missing";



}elseif(empty($data['city'])){

$response['errorcode'] = "350003";

$response['status']= 0;

$response['message']="City: Required parameter missing";



}elseif(empty($data['state'])){

$response['errorcode'] = "350004";

$response['status']= 0;

$response['message']="State: Required parameter missing";



}elseif(empty($data['zipcode'])){

$response['errorcode'] = "350005";

$response['status']= 0;

$response['message']="State: Required parameter missing";



}elseif(empty($data['country'])){

$response['errorcode'] = "350006";

$response['status']= 0;

$response['message']="country: Required parameter missing";



}elseif(empty($data['phone_no'])){

$response['errorcode'] = "350007";

$response['status']= 0;

$response['message']="Contact Number: Required parameter missing";



}

elseif (!empty($data)){



$rest_listing = DB::table('search_restaurant_view')

			->select("*")								

			->where("rest_id","=",$rest_id)

			->get();	

if($rest_listing)

{

$diff_distance =  $this->distance($rest_listing[0]->rest_lat, $rest_listing[0]->rest_long, $address_lat, $address_lang, "m");

if( $diff_distance<$rest_listing[0]->rest_delivery_upto)

{		

													

DB::table('delivery_address')

	->where('del_id', $del_id)

	->update([

			'del_contactno' => trim('1'),

			'del_contactno' =>  trim($this->param['phone_no']),

			'del_address' =>  trim($this->param['street_address']),

			'del_city' =>  trim($this->param['city']),

			'del_zipcode' =>  trim($this->param['zipcode']),

			'del_state' =>  trim($this->param['state']),

			'del_country' =>  trim($this->param['country'])

	 ]);

	 			



$add_id =$del_id; 

			

$address_data = DB::table('delivery_address')->where('del_id', '=' ,$add_id)->get();	



$response['message'] = "Restaurant delivery address update successfully!";



$response['address_data'] = $address_data;

$response['status']= 1;

}

else

{

$response['errorcode'] = "350008";

$response['message'] = "Restaurant not give delivery on your address!";

$response['status']= 0;

}

}

else

{	  

$response['errorcode'] = "350009";

$response['message'] = "Restaurant not available";

$response['status']= 0;

}



}





header('Content-type: application/json');

echo json_encode($response);

}





/************************************************************/

//	FUNCTION NAME : delete_delivery_address

//	FUNCTION USE :  Addd new delivery address 

//	FUNCTION ERRORCODE : 360000

/****************************************************/	

public function delete_delivery_address()

{



$data['rest_id'] =$rest_id = Input::get("rest_id");

$data['userid'] = $user_id = Input::get("userid");

$data['del_id'] = $del_id = Input::get("del_id");

$data['guest_id'] = $guest_id = Input::get("guest_id");

$data['deviceid'] = $deviceid = Input::get("deviceid");

$data['devicetype'] = $devicetype = Input::get("devicetype");



if(empty($data['userid'])){

$response['errorcode'] = "360001";

$response['status']= 0;

$response['message']="User Id:Required parameter missing";



}elseif(empty($data['del_id'])){

$response['errorcode'] = "360002";

$response['status']= 0;

$response['message']="Delivery Address Id:Required parameter missing";



}

elseif (!empty($data)){



DB::table('delivery_address')->where('del_id', '=' ,$del_id)->delete();	



$address_listing = DB::table('delivery_address')

			->select("*")								

			->where("del_userid","=",$user_id)

			->get();

			

			

//DB::table('user_otpdetail')->where('user_mob', '=', trim($this->param['mobileno']))->delete();					

//$response['message']='Adddress Lisitng';					

$response['message']='Address Delete sucessfully. View Other Adddress Lisitng';					

$response['data'] = array('address_listing'=>$address_listing);

$response['status']= 1;





}





header('Content-type: application/json');

echo json_encode($response);





}



public function ws_privacy_policy()

{

// $data['pageid'] = $page_id = Input::get("pageid");



// if(empty($data['pageid'])){

// 	$response['errorcode'] = "360001";

// 	$response['status']= 0;

// 	$response['message']="Page Id:Required parameter missing";



// }elseif (!empty($data)){



$page_detail  = DB::table('pages')

->where('page_id', '=' ,'5')	  

->get();



$response['message']='Private policy page details';					

$response['data'] = $page_detail;

$response['status']= 1;					

//}		



header('Content-type: application/json');

echo json_encode($response);

}   



public function ws_terms_condition()

{

$page_detail  = DB::table('pages')

	->where('page_id', '=' ,'4')	  

	->get();		



$response['message']='Terms & Conditions page details';					

$response['data'] = $page_detail;

$response['status']= 1;					



header('Content-type: application/json');

echo json_encode($response);

}   



public function ws_about()

{

$page_detail  = DB::table('pages')

	->where('page_id', '=' ,'1')  

	->get();		

$response['message']='About-us page details';					

$response['data'] = $page_detail;

$response['status']= 1;					



header('Content-type: application/json');

echo json_encode($response);

}   



public function ws_contactus()

{

$page_detail  = DB::table('pages')

->where('page_id', '=' ,'2')	  

->get();		

$response['message']='Contect-us page details';					

$response['data'] = $page_detail;

$response['status']= 1;					



header('Content-type: application/json');

echo json_encode($response);



}



public function area_manager_detail()

{

$data['zipcode'] = $zipcode = Input::get("zipcode");



if(empty($data['zipcode'])){

$response['errorcode'] = "360001";

$response['status']= 0;

$response['message']="Zipcode : Required parameter missing";



}elseif (!empty($data)){



$manager_detail  = DB::table('admins')

->where('zipcoad', '=' , trim($zipcode))

->where('role', '=' ,'2')

->get();



$response['message']='Your Area manager details';					

$response['data'] = $manager_detail;

$response['status']= 1;					

}		



header('Content-type: application/json');

echo json_encode($response);

}



public function getcity($address){

if(!empty($address)){

//Formatted address

$formattedAddr = str_replace(' ','+',$address);

//Send request and receive json data by address

$geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=true_or_false&key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE'); 

$output1 = json_decode($geocodeFromAddr);



if($output1->status=='OK'){



$latitude  = $output1->results[0]->geometry->location->lat; 

$longitude = $output1->results[0]->geometry->location->lng;



$city = '';



foreach ($output1->results[0]->address_components as $key => $value) {



$newval = $value->types; 



foreach ($newval as $key => $tvalue) 

{



if($tvalue=='locality'){



$city = $value->long_name;



}   



} 



} 



}else{ $city = ''; }



}else{ $city = ''; }



return $city; 

}

public function vendor_update_proceed(Request $request)
{

    $validator = Validator::make($request->all(), [

        "merchant_id" => "required|numeric",
        "status" => "in:0,1"
    ]);

    if ($validator->fails()) {

        return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

    }

    $merchant_id = $request->merchant_id;
    $status = $request->status;
    
    $vendor = DB::table('shopping_cart_status')

						->where('vendor_id',$request->merchant_id)

						->get();

						//print_r($vendor);die;
    if($vendor->count() > 0){
     DB::table('shopping_cart_status')->where('vendor_id', $request->merchant_id)->update(array('status' => $request->status));
     

     $vendor_data = DB::table('shopping_cart_status')

						->where('vendor_id',$request->merchant_id)

						->get();
	 return response()->json(["ok" => 1,"message" => "Status updated sucessfully.",'proceed_data'=>$vendor_data[0]->status]);
     }else{
     DB::table('shopping_cart_status')->insert([
     	'vendor_id' => $request->merchant_id,
	    'status' => '0',
	    'created_at' => date('Y-m-d')
	]);
     $vendor_data = DB::table('shopping_cart_status')

						->where('vendor_id',$request->merchant_id)

						->get();

     return response()->json(["ok" => 1,"message" => "status added sucessfully.",'proceed_data'=>$vendor_data[0]->status]);

    }

}

public function vendor_proceed_status(Request $request)
{

    $validator = Validator::make($request->all(), [

        "merchant_id" => "required|numeric"
    ]);

    if ($validator->fails()) {

        return response()->json(["ok" => 0, "message" => $validator->messages()], 200);

    }

    $merchant_id = $request->merchant_id;
    
    $vendor = DB::table('shopping_cart_status')

						->where('vendor_id',$request->merchant_id)

						->first();

    if(!empty($vendor)){
        //print_r($vendor->status);die;
        //$response['data'] = array('proceed_data'=>$vendor->status);
        return response()->json(["ok" => 1,"message" => "status added successfully.",'proceed_data'=>$vendor->status]);

     }else{

        //$response['data'] = array('proceed_data'=>'1');
        return response()->json(["ok" => 1,"message" => "status added successfully.",'proceed_data'=>'1']);

    }

}

// deep code start for herbenium site


	public function category_list(){
		$response= array();
		$url = url('/').'/public/uploads/category';
		
		$data['category_list'] = array(); 
		$category  = DB::table('product_service_category')->where('status', '=' ,1)->get();
		if(count($category)>0){
			foreach ($category as $value) {
				if(!empty($value->cat_image)){
					$imageurl = $url.'/'.$value->cat_image;
				}else{
					$imageurl = "";
				}
				
				$data['category_list'][]=array(
						'category_id' => $value->id,
						'category' => $value->category,
						'cat_image' => $imageurl,
						'status' => $value->status,
				);
			}
		
			$response['message'] = "Category List";
			$response['status'] = 1;
			$response['data'] = array('category_list'=>$data['category_list']);
		}else{
			$response['message'] = "No data found";
			$response['status'] = 0;
			$response['data'] = array('vendor_detail'=>$data['category_list']);
		}

		echo json_encode($response);

	}


		public function driver_list(Request $request){

		$response= array();

		$latitude = $request->latitude;

		$longitude = $request->longitude;

		$keyword = $request->keyword;

		$url = url('/').'/public/uploads/vendor/profile/';

		$product_image = "https://www.grambunny.com/public/uploads/product/";
		
		$data['driver_list'] = array(); 

		$vendorlist = array();

		$product_list = array();

		$distance = 50;

		$vendors = array();

		if(!empty($request->keyword)){

			$vendorlist=DB::table('vendor')->where("zipcode",$keyword)->orWhere("city",$keyword)->groupBy('vendor_id')->get();
		}

		else if(!empty($latitude) && !empty($longitude)){

		$results = DB::select(DB::raw('SELECT vendor_id, ( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(lat) ) ) ) AS distance FROM vendor HAVING distance < ' . $distance . ' ORDER BY distance'));



       if($results){ $vendors= Arr::pluck($results,"vendor_id"); }



       if($vendors){ $vendorlist=DB::table('vendor')->whereIn("vendor_id",$vendors)->where("login_status",1)->groupBy('vendor_id')->get();  

		// foreach ($vendors as  $value) {
   		
   			//$product_list  = DB::table('product_service')->select('id','vendor_id','category_id','sub_category_id','avg_rating','rating_count','name','slug','description','price','unit','image')
   			$product_list  = $query = Productservice::whereIn('vendor_id', $vendors)
			->where("status",1);
			if(!empty($keyword)){
				$query->orWhere("name",'like','%'.$keyword.'%');
			}
			$query = $query->orderByDesc('id')
        	->limit(10)
			->get();

		//}


   		}

   		

	    //$vendorlist  = DB::table('vendor')->where('vendor_status', '=' ,1)->get();

       //if($vendors){ $vendorlist=DB::table('vendor')->whereIn("vendor_id",$vendors)->where("login_status",1)->groupBy('vendor_id')->get();  }
  		//print_r($vendorlist);
		// die;

        }


		if(count($vendorlist)>0){
		
			$response['message'] = "Driver List";
			$response['img_url'] = $url;
			$response['product_url'] = $product_image;
			$response['status'] = 1;
			$response['data'] = array('driver_list'=>$vendorlist,'product_list'=>$product_list);
		}else{
			$response['message'] = "No data found";
			$response['status'] = 0;
			$response['data'] = array('driver_detail'=>$vendorlist,'product_list'=>$product_list);
		}
		
		echo json_encode($response);

	}


	public function products_list(Request $request){

		$response= array();

		DB::enableQueryLog();

		$categoryid = '';
		$driver_id = '';
		$subcategory_id = '';
		$weight = '';
		$brand = '';
		$type = '';
		$potency_thc = '';
		$potency_cbd = '';

		if($request->category_id){ $categoryid = $request->category_id; }
		if($request->driver_id){ $driver_id = $request->driver_id; }
		if($request->subcategory_id){ $subcategory_id = $request->subcategory_id; }
		if($request->weight){ $weight = $request->weight; }
		if($request->brand){ $brand = $request->brand; }
		if($request->type){ $type = $request->type; }
		if($request->potency_thc){ $potency_thc = $request->potency_thc; }
		if($request->potency_cbd){ $potency_cbd = $request->potency_cbd; }

		$query = Productservice::where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0']);

		if($subcategory_id){ $query->where('sub_category_id', '=' ,$subcategory_id); }
		if($weight){ $query->where('unit', '=' ,$weight); }
		if($brand){ $query->where('brands', '=' ,$brand); }
		if($type){ $query->where('types', '=' ,$type); }
		if($potency_thc){ $query->where('potency_thc', '=' ,$potency_thc); }
		if($potency_cbd){ $query->where('potency_cbd', '=' ,$potency_cbd); }

		$catproduct = $query->get();

		$catproducts = array();

		if(count($catproduct)>0){

		foreach ($catproduct as $key => $value) {

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

		$query = Productservice::join('admin_merchant_product', 'admin_merchant_product.product_id','=','product_service.id')->select('*','admin_merchant_product.vendor_id as vendor_id','product_service.vendor_id as pvendor_id','admin_merchant_product.id as apid','product_service.id as id')->where('category_id', '=' ,$categoryid)
		->where('product_service.id', '=',$adminpro[0]->product_id)
		->where('product_service.vendor_id', '=' ,0);

		if($subcategory_id){ $query->where('sub_category_id', '=' ,$subcategory_id); }
		if($weight){ $query->where('unit', '=' ,$weight); }
		if($brand){ $query->where('brands', '=' ,$brand); }
		if($type){ $query->where('types', '=' ,$type); }
		if($potency_thc){ $query->where('potency_thc', '=' ,$potency_thc); }
		if($potency_cbd){ $query->where('potency_cbd', '=' ,$potency_cbd); }

		$catproduct = $query->first();

	    $catproducts[] = $catproduct ;
		
		}

	  }else{				

         $catproducts[] = $value ;

        }

	   }

	   	$data['message'] = "Product List";
		$data['status'] = 1;
		$data['data'] = array('product_list'=>$catproducts);

      }else{

        $data['message'] = "No Product Found!";
		$data['status'] = 0;
		$data['data'] = array('product_list'=>$catproducts);

		} 

		return $data;

	}


	public function sidebar_filter(Request $request){

		$response= array();

		DB::enableQueryLog();

		$categoryid = $request->category_id;

		$driver_id = $request->driver_id;

		$subcategoryproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0'])
		->groupBy('sub_category_id')
		->get();

		$weightproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0'])
		->groupBy('unit')
		->get();

		$brandproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0'])
		->groupBy('brands')
		->get();

		$typeproduct  = DB::table('product_service')->where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0'])
		->groupBy('types')
		->get();

       $subcategorylist = array();
       $weightlist = array();
       $brandlist = array();
       $typelist = array();


      if(count($subcategoryproduct)>0){

      $category_name = DB::table('product_service_category')->where('id', '=' ,$categoryid)->value('category');	

      $catename = 'All '.$category_name;

      $subcategorylist[] = array('id' => '0','subcategory' => $catename);		

		foreach ($subcategoryproduct as $key => $value) {

		$sub_category_name = DB::table('product_service_sub_category')->where('id', '=' ,$value->sub_category_id)->value('sub_category');

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

	    $subcategorylist[] = array('id' => $value->sub_category_id,'subcategory' => $sub_category_name);
		
		}

	  }else{				

         $subcategorylist[] = array('id' => $value->sub_category_id,'subcategory' => $sub_category_name);

        }

	  }
	}

	if(count($weightproduct)>0){

		foreach ($weightproduct as $key => $value) {

		$weightid = DB::table('product_unit')->where('unit_name', '=' ,trim($value->unit))->value('id');	

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

	    $weightlist[] = array('id' => $weightid,'weight' => $value->unit);
		
		}

	  }else{				

         $weightlist[] = array('id' => $weightid,'weight' => $value->unit);

        }

	  }
	}

    if(count($brandproduct)>0){

    	$brandlist[] = array('id' => '0','brand' => 'All Brands'); 

		foreach ($brandproduct as $key => $value) {

		$brandid = DB::table('product_brands')->where('brand_name', '=' ,trim($value->brands))->value('id');	

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

	    $brandlist[] = array('id' => $brandid,'brand' => $value->brands); 
		
		}

	  }else{				

         $brandlist[] = array('id' => $brandid,'brand' => $value->brands); 

        }

	  }
	}

    if(count($typeproduct)>0){

		foreach ($typeproduct as $key => $value) {

		$typeid = DB::table('product_types')->where('types_name', '=' ,trim($value->types))->value('id');	

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

	    $typelist[] = array('id' => $typeid,'type' => $value->types);
		
		}

	  }else{				

         $typelist[] = array('id' => $typeid,'type' => $value->types);

        }

	  }
	}		


  if(count($subcategoryproduct)>0 || count($weightproduct)>0 || count($brandproduct)>0 || count($typeproduct)>0){	  

    $data['message'] = "Filter List";
	$data['status'] = 1;
	$data['data'] = array('subcategories'=>$subcategorylist,'weights'=>$weightlist,'brands'=>$brandlist,'types'=>$typelist);

     }else{

        $data['message'] = "Error!";
		$data['status'] = 0;
		$data['data'] = array('subcategories'=>'','weights'=>'','brands'=>'','types'=>'');

	} 

		return $data;

	}


	public function filter_products(Request $request){

		$response= array();

		DB::enableQueryLog();

		$categoryid = '';
		$driver_id = '';
		$subcategory_id = '';
		$weight = '';
		$brand = '';
		$type = '';
		$potency_thc = '';
		$potency_cbd = '';
		$popular = '';

		if($request->category_id){ $categoryid = $request->category_id; }
		if($request->driver_id){ $driver_id = $request->driver_id; }
		if($request->subcategory_id){ $subcategory_id = $request->subcategory_id; }
		if($request->weight){ $weight = $request->weight; }
		if($request->brand){ $brand = $request->brand; }
		if($request->type){ $type = $request->type; }
		if($request->potency_thc){ $potency_thc = $request->potency_thc; }
		if($request->potency_cbd){ $potency_cbd = $request->potency_cbd; }
		if($request->popular){ $popular = $request->popular; }

		$query = Productservice::where('category_id', '=' ,$categoryid)
		->whereIn('vendor_id', [$driver_id, '0']);

		if($subcategory_id){ $query->where('sub_category_id', '=' ,$subcategory_id); }
		if($weight){ $query->where('unit', '=' ,$weight); }
		if($brand){ $query->where('brands', '=' ,$brand); }
		if($type){ $query->where('types', '=' ,$type); }
		if($potency_thc){ $query->where('potency_thc', '=' ,$potency_thc); }
		if($potency_cbd){ $query->where('potency_cbd', '=' ,$potency_cbd); }
		if($popular==1){ $query->orderBy('popular', 'desc'); }

		$catproduct = $query->get();

		$catproducts = array();

		if(count($catproduct)>0){

		foreach ($catproduct as $key => $value) {

		if($value->vendor_id==0){	

		$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$value->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();	

		if(count($adminpro) > 0){

	    $catproducts[] = $value ;
		
		}

	  }else{				

         $catproducts[] = $value ;

        }

	   }

	   	$data['message'] = "Product List";
		$data['status'] = 1;
		$data['data'] = array('product_list'=>$catproducts);

      }else{

        $data['message'] = "No Product Found!";
		$data['status'] = 0;
		$data['data'] = array('product_list'=>$catproducts);

		} 

		return $data;

	}	


    public function view_cart(Request $request)
    {

    	DB::enableQueryLog();

        $validator = Validator::make($request->all(), [

            'qty' => 'numeric',

        ]);

        if ($validator->fails()){

           return "Insert qty numeric value only";

        }


        $user_id = ''; 

        if (!empty($request->user_id)){

            $user_id = $request->user_id;

            $user_info = DB::table('users')->where('id','=',$user_id)->first();

            $delivery_addresss = DB::table('delivery_address')->where('del_userid','=',$user_id)->get();

        }else{

            $user_info = array();

            $user_id = Session::get("_token");

            $delivery_addresss = array();

        }


        //$item=Productservice::where("slug",$request->slug)->where("status",1)->first();

        $items=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        $request->session()->put('ps_slug', $request->slug);

        $qty = $request->qty;

        $request->session()->put('cart_uid', $user_id);

        if(empty(session('promo_amount')) || is_null(session('promo_amount'))){

            $promo_amount = 0;

        }else{

            $promo_amount = session('promo_amount');

        }
 

        $state_list = DB::table('states')->get();

        if(sizeof($items)){

        return view("cart",["items" => $items,"qty" => $qty,"promo_amount" => $promo_amount,"user_info" => $user_info,'state_list' => $state_list,'user_id' => $user_id,'delivery_addresss' => $delivery_addresss]);

        }

    }



    public function add_to_cart(Request $request)
    {   

        if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }


        $product_id = (!empty( $request->product_id )) ? $request->product_id : '';

        $vendor_id = (!empty( $request->vendor_id )) ? $request->vendor_id : '';

        $qty = $request->qty;

        //promo amount will be 0 when product added/removed/updated into cart        

        $request->session()->put('promo_amount', 0); 


        if($request->delete){

            DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->delete();

            $crtmessage = "Item deleted successfully"; 

        }elseif($request->decrease){

        $quantiti = DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

        if(($quantiti > 1) && ($quantiti > $qty)){

        $qty = $quantiti-$qty;

         DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->update(['quantity' => $qty]);

        }

        $crtmessage = "Item updated successfully"; 

        }else{

            $cart =  new Cart;

            $count = DB::table('cart')->where('user_id', $user_id)->where('ps_id', $product_id)->where('vendor_id', $vendor_id)->count();

            if( $count > 0 ){

            	$quantiti = DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->value('quantity');

            	$qty = $qty+$quantiti;

                DB::table('cart')->where('ps_id',$product_id)->where('vendor_id',$vendor_id)->where('user_id',$user_id)->update(['quantity' => $qty]);

                $crtmessage = "Item updated successfully";

             }else{

                
               $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->count();

                if( $count == 0){

                    DB::table('cart')->where('vendor_id','!=' ,$vendor_id)->where('user_id',$user_id)->delete();

                }
         
                $cart->user_id = $user_id;
                $cart->ps_id = $product_id;
                $cart->vendor_id = $vendor_id;
                $cart->quantity = $qty;
                $cart->save();

               $crtmessage = "Item added successfully"; 

            }

        }

      $count = DB::table('cart')->where('user_id', $user_id)->where('vendor_id', $vendor_id)->sum('cart.quantity');

        $request->session()->put('ps_qty', $count);

        $request->session()->put('cart_uid', $user_id);

        //return response()->json(["msg" => 'Item Added successfully']);

        if(!empty($count)){

        $data['message'] = $crtmessage;
		$data['status'] = 1;
		$data['quantity'] = $count;
		$data['user_id'] = $user_id;

	    }else{

        $data['message'] = $crtmessage;
		$data['status'] = 1;
		$data['quantity'] = $count;
		$data['user_id'] = $user_id;

	    }

		return $data;

    }



    public function cartCount(Request $request)
    {   

        if(!empty($request->user_id)){

            $user_id = trim($request->user_id);

        }else{

            $user_id = Session::get("_token");
        }

        $cart =  new Cart;

        $count = DB::table('cart')->where('user_id', $user_id)->sum('cart.quantity');

        if(!empty($count)){

        $data['message'] = "Count cart item!";
		$data['status'] = 1;
		$data['quantity'] = $count;

	    }else{

        $data['message'] = "Cart empty";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

    }
    //priyanka 06092021

    public function get_cart_calculation(Request $request)
    { 

    	$promo_code = $request->coupon_code;

    	$driver_id = $request->vendor_id;

    	if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }

        $cart =  new Cart;


         $user_info = DB::table('users')->where('id','=',$user_id)->first();

         if(empty($user_info)){ $user_info = ''; }

         $items=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        
       if(sizeof($items)){

       $ptotalprice = 0;

       foreach ($items as $key => $value) {

        $ptotal = $value->price*$value->quantity;

        $ptotalprice = $ptotalprice+$ptotal;

       }

        $vendor_id = $items[0]->vendor_id; 

        if($vendor_id == 0){
        	$adminpro = DB::table('admin_merchant_product')		
						->where('product_id', '=' ,$items[0]->id)
						->where('vendor_id', '=' ,$driver_id)
						->get();
			$vendor_id = $adminpro[0]->vendor_id; 				
        }else{

        	$vendor_id = $items[0]->vendor_id; 
        }

        $subtotal = number_format($ptotalprice,2);

        if($vendor_id!=0){

        $vids = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();	

        $sales_tax = ($vids->sales_tax)*($subtotal)/100;
        $excise_tax = ($vids->excise_tax)*($subtotal)/100;
        $city_tax = ($vids->city_tax)*($subtotal)/100;
        $delivery_fee = $vids->delivery_fee;

        }else{

        $sales_tax = 0;
        $excise_tax = 0;
        $city_tax = 0;
        $delivery_fee=0;
        }

        

        $grandtotal = $subtotal+$sales_tax+$excise_tax+$city_tax+$delivery_fee;

        $sales_tax = number_format($sales_tax,2);
        $excise_tax = number_format($excise_tax,2);
        $city_tax = number_format($city_tax,2);
        $delivery_fee = number_format($delivery_fee,2);
        $grandtotal = number_format($grandtotal,2);


        if(!empty($promo_code)){
       		 $coupon_info = DB::table('coupon_code')->where('coupon','=',$promo_code)->first();

	        if(!empty($coupon_info->id)){
				if($coupon_info->discount == 1){

				        $promo_amount = number_format($grandtotal*$coupon_info->amount/100,2);

				    }else{

				    	$promo_amount = number_format($coupon_info->amount,2);

				    }

	        }else{

	        		$promo_amount = "0.0";
	        }
    	}else{
    		$promo_amount = "0.0";
    	}
        
        $data['message'] = "Total Cart Calcualtion";
		$data['status'] = 1;
		$data['data'] = array('sales_tax' => $sales_tax,'excise_tax' => $excise_tax,'city_tax' => $city_tax,'delivery_fee'=>$delivery_fee,'subtotal' => $subtotal,'grandtotal' => number_format(($grandtotal-$promo_amount),2), "coupon_amount"=>$promo_amount);

	    }else{

        $data['message'] = "Cart empty";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;


    }


     public function coupon_code_list(Request $request)
    {   

        if(!empty($request->vendor_id)){

	        $user_id = trim($request->vendor_id);

	        $couponlist = DB::table('coupon_code')->where('vendor_id', $user_id)->where('valid_till', '>=', date('Y-m-d'))->where('status',1)->get();

	        if(!empty($couponlist)){
	        $data['flat_rate']= '0';
			$data['parcent']= '1';
	        $data['message'] = "Coupon list";
			$data['status'] = 1;
			$data['couponlist'] = $couponlist;
			

		    }else{

	        $data['message'] = "Coupon list empty";
			$data['status'] = 0;
			$data['quantity'] = 0;

		    }
	   }else{
			$data['message'] = "Vendor Id Not vailid";
			$data['status'] = 0;
			$data['quantity'] = 0;
           
        }

		return $data;

    }

     public function apply_coupon_code(Request $request)
    {

        $promo_code = $request->coupon_code;
        $coupon_id = $request->coupon_id;
        $vendor_id = $request->vendor_id;
        $user_id = $request->user_id;
        $total_amount = $request->total_amount;
        $coupon_info = DB::table('coupon_code')->where('coupon','=',$promo_code)->where('vendor_id','=',$vendor_id)->first();

        if(!empty($coupon_info->id)){

            $order_info = DB::table('orders')->where('coupon_id','=',$coupon_info->id)->where('vendor_id','=',$vendor_id)->where('user_id','=',$user_id)->first();

            $temp_coupon_code_apply = DB::table('temp_coupon_code_apply')->where('vendor_id','=',$vendor_id)->where('user_id','=',$user_id)->first();

            if(empty($order_info->id) && empty($temp_coupon_code_apply)){

                if($coupon_info->status == 1){

                    $start_time = date('d-m-Y',strtotime($coupon_info->created_at));
                    $end_time = date('d-m-Y',strtotime($coupon_info->valid_till));

                    if(strtotime($start_time) <= strtotime(date('Y-m-d')) && strtotime($end_time) >= strtotime(date('Y-m-d'))){

                        if($coupon_info->apply_min_amount > $total_amount){

                            return json_encode(array('status'=>0,'message'=>'To apply promo code sub total should be greater than '.$coupon_info->apply_min_amount));

                        }

                        if($coupon_info->discount == 1){

                                $promo_amount = $total_amount*$coupon_info->amount/100;

                                 DB::table('temp_coupon_code_apply')->insert(['coupon_code_id' => $coupon_id,'coupon_code'=>$promo_code,'user_id'=>$user_id,'vendor_id'=>$vendor_id,'status'=>1,'counpon_amount'=>$promo_amount]);

                                return json_encode(array('status'=>1,'message'=>'Coupon code applied!','coupon_code'=>$promo_code,'coupon_apply_amount'=>$promo_amount));

                        }else{

                        	   DB::table('temp_coupon_code_apply')->insert(['coupon_code_id' => $coupon_id,'coupon_code'=>$promo_code,'user_id'=>$user_id,'vendor_id'=>$vendor_id,'status'=>1,'counpon_amount'=>$coupon_info->amount]);

                            return json_encode(array('status'=>1,'message'=>'Coupon code applied!','coupon_code'=>$promo_code,'coupon_apply_amount'=>number_format($coupon_info->amount,2))); 

                        }

                        

                    } else {

                        return json_encode(array('status'=>0,'message'=>'Validity Expired!'));

                    }



                } else {

                    return json_encode(array('status'=>0,'message'=>'Coupon code is inactive!'));

                }

            } else {

                return json_encode(array('status'=>0,'message'=>'Coupon already used!'));

            }

        } else {

            return json_encode(array('status'=>0,'message'=>'Invalid coupon code!'));

        }

    }


     //end priyanka 06092021


    public function cart_list(Request $request)
    {   

        if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }

        $cart =  new Cart;


         $user_info = DB::table('users')->where('id','=',$user_id)->first();

         if(empty($user_info)){ $user_info = ''; }

         $items=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        
       if(sizeof($items)){

       $ptotalprice = 0;

       foreach ($items as $key => $value) {

        $ptotal = $value->price*$value->quantity;

        $ptotalprice = $ptotalprice+$ptotal;

       }

        $vendor_id = $items[0]->vendor_id; 

        $subtotal = number_format($ptotalprice,2);

        if($vendor_id!=0){

        $vids = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();	

        $sales_tax = ($vids->sales_tax)*($subtotal)/100;
        $excise_tax = ($vids->excise_tax)*($subtotal)/100;
        $city_tax = ($vids->city_tax)*($subtotal)/100;

        }else{

        $sales_tax = 0;
        $excise_tax = 0;
        $city_tax = 0;

        }

        $grandtotal = $subtotal+$sales_tax+$excise_tax+$city_tax;

        $sales_tax = number_format($sales_tax,2);
        $excise_tax = number_format($excise_tax,2);
        $city_tax = number_format($city_tax,2);
        $grandtotal = number_format($grandtotal,2);
        
        $data['message'] = "Total cart item!";
		$data['status'] = 1;
		$data['data'] = array('sales_tax' => $sales_tax,'excise_tax' => $excise_tax,'city_tax' => $city_tax,'subtotal' => $subtotal,'grandtotal' => $grandtotal,'items' => $items,'user_info' => $user_info);

	    }else{

        $data['message'] = "Cart empty";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

    }


    public function cart_checkout(Request $request)
    {   

        if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }

        $cart =  new Cart;


         $user_info = DB::table('users')->where('id','=',$user_id)->first();

         if(empty($user_info)){ $user_info = ''; }

         $items=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->where("crt.user_id",$user_id)->where("product_service.status",1)->get();

        
       if(sizeof($items)){

       $ptotalprice = 0;

       foreach ($items as $key => $value) {

        $ptotal = $value->price*$value->quantity;

        $ptotalprice = $ptotalprice+$ptotal;

       }

        $vendor_id = $items[0]->vendor_id; 

        $vids = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

        $subtotal = number_format($ptotalprice,2);

        $sales_tax = ($vids->sales_tax)*($subtotal)/100;
        $excise_tax = ($vids->excise_tax)*($subtotal)/100;
        $city_tax = ($vids->city_tax)*($subtotal)/100;

        $grandtotal = $subtotal+$sales_tax+$excise_tax+$city_tax;

        $sales_tax = number_format($sales_tax,2);
        $excise_tax = number_format($excise_tax,2);
        $city_tax = number_format($city_tax,2);
        $grandtotal = number_format($grandtotal,2);

        

        $data['message'] = "Checkout Data";
		$data['status'] = 1;
		$data['data'] = array('sales_tax' => $sales_tax,'excise_tax' => $excise_tax,'city_tax' => $city_tax,'subtotal' => $subtotal,'grandtotal' => $grandtotal,'user_info' => $user_info);

	    }else{

        $data['message'] = "Error!";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

    }

      public function submit_order(Request $request)
    {

        //echo "<pre>"; print_r($_POST);die;

        $paymentMethod = $request->pmethodc;

        // $firstName = $request->firstName;

        // $lastName = $request->lastName;

        //$email = $request->email;

        $user_mob = $request->user_mob;

        // $latitude = $request->latitude;

        // $longitude = $request->longitude;

        $address = $request->address;

        $city = $request->city;

        $state = $request->state;

        $zip = $request->zip;

        $addressp = $request->address;
        $cityp = $request->city;
        $statep = $request->state;
        $zipp = $request->zip;

        $addressd = $request->address;
        $cityd = $request->city;
        $stated = $request->state;
        $zipd = $request->zip;
 

        $comment = $request->comment;

        $cc_name = $request->cc_name;

        $cardtype = $request->creditcardtype;

        $cc_number = $request->cc_number;

        $cc_expiration = $request->cc_expiration;

        $cc_cvv = $request->cc_cvv;
        
        $cc_expiry = $request->cc_expire_month.$request->cc_expire_year;

		//item_price = "";

        $item_qty = '';//$request->item_qty;

        $sub_total = $request->sub_total;

        $promo_amount = $request->promo_amount;

        //$final_amount = $request->sub_total+$request->saletax+$request->excisetax+$request->citytax-$request->promo_amount;//$request->final_amount;

        $final_amount = $request->final_amount;

        $saletax = $request->saletax;

        $excisetax = $request->excisetax;

        $citytax = $request->citytax;

        $ps_id = '';//$request->ps_id;

        $vendor_id = $request->vendor_id;

        $user_id = $request->user_id;

        $coupon_id = $request->coupon_id;

        $delivery_time = date('h:i', strtotime('1 hour'));


        $users = DB::table('users')->where('id','=',$user_id)->first();

         $firstName = $users['name'];

        $lastName = $users['lname'];

        $email = $users['email'];



    $nonce = strval(hexdec(bin2hex(openssl_random_pseudo_bytes(4, $cstrong))));
    $timestamp = strval(time()*1000); //time stamp in milli seconds

    $order_id = strtoupper($this->generateRandomString(6));

    $reference_no = rand(100000,999999);
   
    $data = array(
              'gateway_id'=> 'G15552-42',
              'password'=> "V2wX5h1ov8iar0FR7ScRaePz2xZemfnF",
              'transaction_type'=> '05',
              'amount'=> $final_amount,
              'cardholder_name'=> $cc_name,
              'cc_number'=> $cc_number,
              'cc_expiry'=> $cc_expiry,
              'cvd_code'=> $cc_cvv,
              'cvd_presence_ind'=> '1',
              'reference_no'=> $reference_no,
              'customer_ref'=> $order_id,
              'currency_code'=> 'USD',
              'credit_card_type'=> $cardtype,
    );

    if($paymentMethod=='card'){ 
   
    $payload = json_encode($data, JSON_FORCE_OBJECT);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://api.globalgatewaye4.firstdata.com/transaction",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS =>$payload,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json"
      ),
    ));

  if(!empty($cc_cvv)){  

    $json_response = curl_exec($curl);

   }

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    $response = json_decode($json_response, true);

    curl_close($curl);


            if($response['transaction_approved']=='1')
                {

                   $ptxn_status = 'COMPLETE';
                   $ptxn_tag = $response['transaction_tag'];
                   $preceipt_url = $response['authorization_num'];
                   $transactiontag = $response['transaction_tag'];

                   $transaction_type = $response['transaction_type'];
                   $authorization_num = $response['authorization_num'];
                   $transaction_approved = $response['transaction_approved'];
                   $bank_message = $response['bank_message'];


                    $data = array(

                    'user_id' => $user_id,

                    'order_id' => $order_id,

                    'cardholder_name' => $cc_name,

                    'cc_number' => $cc_number,

                    'cc_expiry' => $cc_expiry,

                    'cvd_code' => $cc_cvv,

                    'credit_card_type' => $cardtype,

                    'amount' => $final_amount,

                    'reference_no' => $reference_no,

                    'transaction_type' => $transaction_type,

                    'transaction_tag' => strval($transactiontag),

                    'authorization_num' => $authorization_num,

                    'transaction_approved' => $transaction_approved,

                    'bank_message' => $bank_message,

                    'created_at' => date('Y-m-d H:i:s'),

                    'updated_at' => date('Y-m-d H:i:s')

                );


               DB::table('users_card')->insertGetId($data);
                      
                
                }
                else
                {
                      return json_encode(array('status'=>0,'message'=>$json_response));   

                }

           }else{


                   $cardtype = 'Cash';

                   $response['transaction_approved'] = '1';

                   $ptxn_status = 'COMPLETE';
                   $ptxn_tag = '';
                   $preceipt_url = '';
                   $transactiontag = '';

                   $transaction_type = '';
                   $authorization_num = '';
                   $transaction_approved = '';
                   $bank_message = '';

                }    
            

        $coupon_id = $coupon_id;

        if (empty($coupon_id)) {

            $coupon_id = null;

        }



        $verification_code = rand(1000,9999);

        $data = array(

                    'vendor_id' => $vendor_id,

                    'user_id' => $user_id,

                    'ps_id' => $ps_id,

                    'ps_qty' => (!empty($item_qty) ? $item_qty : 1),

                    'order_id' => $order_id,

                    'address' => $address,

                    'addressp' => $addressp,

                    'addressd' => $addressd,

                    'city' => $city,

                    'cityp' => $cityp,

                    'cityd' => $cityd,

                    'mobile_no' => $user_mob,

                    'email' => $email,

                    'instruction' => $comment,

                    'cancel_reason' => NULL,

                    'status' => 0,

                    'delivery_fee' => NULL,

                    'delivery_time' => $delivery_time,

                    'latitude' => (!empty($latitude) ? $latitude : ''),

                    'longitude' => (!empty($longitude) ? $longitude : ''),

                    'sub_total' => $sub_total,

                    'total' => $final_amount,

                    'service_tax' => $saletax,

                    'excise_tax' => $excisetax,

                    'city_tax' => $citytax,

                    'promo_amount' => $promo_amount,

                    'coupon_id' => $coupon_id,

                    'verification_code' => $verification_code,

                    'txn_id' => strval($ptxn_tag), 

                    'transaction_tag' => (string)$transactiontag,

                    'authorization_num' => $preceipt_url,

                    'receipt_url' => $preceipt_url,

                    'pay_status' => $ptxn_status,

                    'payment_method' => $cardtype,

                    'first_name' => $firstName,

                    'last_name' => $lastName,

                    'state' => $state,

                    'statep' => $statep,

                    'stated' => $stated,

                    'zip' => $zip,

                    'zipp' => $zipp,

                    'zipd' => $zipd,

                    'created_at' => date('Y-m-d H:i:s'),

                    'updated_at' => date('Y-m-d H:i:s')

                );

        $id = DB::table('orders')->insertGetId($data);


        $addr_info = DB::table('delivery_address')->where('del_address','=',$address)->first();

        if (empty($addr_info->del_id)) {

            $addr_data = array(

                        'del_userid' => $user_id,

                        'del_contactno' => $user_mob,

                        'del_address' => $address,

                        'del_zipcode' => $zip,

                        'del_state' => $state,

                        'del_lat' => (!empty($latitude) ? $latitude : ''),

                        'del_long' => (!empty($longitude) ? $longitude : ''),

                        'created_at' => date('Y-m-d H:i:s'),

                        'updated_at' => date('Y-m-d H:i:s')

                    );

            $addr_id = DB::table('delivery_address')->insertGetId($addr_data);

        }

        // if ($sub_total > $final_amount) {

        //     $promo_amount = $sub_total-$final_amount;

        // } else {

        //     $promo_amount = 0; 

        // }



        if (!empty($id)){ 

            // logic to insert values into orders_ps table for multiple product/service order

            $ps_info = DB::table('cart')->where('user_id','=',$user_id)->where('vendor_id','=',$vendor_id)->get();

            $vendor_info = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

            $ps_data = array();

            $ps_ids = array();

            foreach($ps_info as $ps){

                $ps_data['ps_id'] = $ps->ps_id;

                $ps_data['ps_qty'] = $ps->quantity;

                $ps_data['order_id'] = $id;

                $ps_data['created_at'] = date('Y-m-d H:i:s');

                $ps_data['updated_at'] = date('Y-m-d H:i:s');

                $ps_final_data[] = $ps_data;

                $ps_ids[] = $ps->ps_id;

               $popular_count = DB::table('product_service')->where('id','=',$ps->ps_id)->value('popular'); 

               $popquenty = $ps->quantity+$popular_count;

               $popular_update = DB::table('product_service')->where('id','=',$ps->ps_id)->update(['popular' => $popquenty]); 

            }

            $order_ps_id = DB::table('orders_ps')->insert($ps_final_data);

            //end for orders_ps

            $ps_info=Productservice::select('product_service.*', 'crt.quantity')->join('cart as crt', 'crt.ps_id', '=', 'product_service.id')->whereIn('product_service.id',$ps_ids)->where('crt.user_id',$user_id)->get();

            $item_arr =array();    

            foreach( $ps_info as $psinfo){

                $iteminfo['name'] = (!empty($psinfo->name) ? $psinfo->name : '');

                $iteminfo['quantity'] = (!empty($psinfo->quantity) ? $psinfo->quantity : '');

                $iteminfo['price'] = (!empty($psinfo->price) ? $psinfo->price : '');

                $item_arr[] = $iteminfo;

            }


            $data['order_id'] = $order_id;

            $data['item_info'] = $item_arr;

            $data['vendor_email'] = $vendor_info->email;

            $data['mob_no'] = $vendor_info->mob_no; 

            $data['vendor_address'] = $vendor_info->market_area;

            $data['sales_tax'] = $vendor_info->sales_tax; 

            $data['excise_tax'] = $vendor_info->excise_tax; 

            $data['city_tax'] = $vendor_info->city_tax; 

            $data['driver_license'] = $vendor_info->driver_license;

            $data['vendor_name'] = (!empty($vendor_info->name) ? $vendor_info->name : '').' '.(!empty($vendor_info->last_name) ? $vendor_info->last_name : '');

            $data['user_name'] = (!empty($firstName) ? $firstName : '').' '.(!empty($lastName) ? $lastName : '');

            $data['user_email'] = $email;

            $data['order_msg'] = ($response['transaction_approved']=='1' ? '' : 'OOPs! Payment transaction has been failed.');

            $data['order_msg'] = ('');

            $data['payment_method'] = $cardtype;

            $data['order_service_tax'] = $saletax;

            $data['order_excise_tax'] = $excisetax;

            $data['order_city_tax'] = $citytax;

            $data['order_subtotal_amt'] = $sub_total;

            $data['order_promo_cal'] = $promo_amount; 

            $data['order_total'] = $final_amount;

            $data['date_time'] = date('Y-m-d H:i:s'); 
            $data['user_id'] = $user_id;
            $data['address'] = $address;
            $data['city'] = $city;
            $data['state'] = $state;
            $data['zip'] = $zip;

            $data['verification_code'] = $verification_code;

              // Mail::send('emails.order_detail', $data, function ($message) use ($data) {

              //   $message->from('info@grambunny.com', 'www.grambunny.com');

              //   $message->to($data['user_email']);

              //   //$message->cc('nick@grambunny.com');

              //   $message->bcc('nick@grambunny.com');

              //   $message->subject('grambunny - Order Confirmation');

            //}); 

            

            if (!empty($vendor_info->vendor_id)){

                $data['vendor_email'] = $vendor_info->email;

                $vendormob = $vendor_info->mob_no;

                $data['order_msg'] = ('You have received a new order, please accept or reject the order in order management.');

                $data['order_msg'] = ($response['transaction_approved']=='1' ? 'You have received a new order, please accept or reject the order in order management.' : 'OOPs! Client new order payment transaction has been failed.');

                $data['vendor_name'] = (!empty($vendor_info->name) ? $vendor_info->name : '').' '.(!empty($vendor_info->last_name) ? $vendor_info->last_name : '');

               $data['user_name'] = (!empty($firstName) ? $firstName : '').' '.(!empty($lastName) ? $lastName : '');

                 // Mail::send('emails.order_detail', $data, function ($message) use ($data){

                 //    $message->from('info@grambunny.com', 'www.grambunny.com');
                 //    $message->to($data['vendor_email']);
                 //    //$message->cc('nick@grambunny.com');
                 //    $message->bcc('nick@grambunny.com');
      
                 //    $message->subject('grambunny - Client New Order Confirmation');

                //}); 

            }

            $request->session()->forget('promo_amount');

            $request->session()->forget('coupon_id');

            $request->session()->forget('ps_qty');

            $request->session()->forget('promo_qty');

            $request->session()->forget('vendor_id');

            $request->session()->forget('ps_slug');

            $divice_token = DB::table('vendor_device_token')

                                ->where('userid', '=', $vendor_id)

                                ->where('notification_status', '=',1)

                                ->first();

                       

         /* if($divice_token)
            {

                 $title = "Order Recieved";

                 $body = "Your order have been successfully created and verification code is :".$verification_code;

                 $token = $divice_token->devicetoken;

                 $this->sendNotification($token,$title,$body);

            } */



            $ordermsg = $data['order_msg'];

            //$smstext = "Your order have been successfully created and verification code is :".$verification_code;

            $smstext = "Your order have been successfully created";

            //$this->sendSms($smstext,$user_mob,$vendormob,$ordermsg);

            return json_encode(array('status'=>1,'message'=>'Order has been generated successfully!','order_id'=>base64_encode($id)));

        } else { 


            if($response['transaction_approved']=='1'){

                return json_encode(array('status'=>0,'message'=>"Payment has been done. But order has not been generated. Please contact to merchant for this issue."));   

             } else {

                return json_encode(array('status'=>0,'message'=>'Your order could not be processed. Please try again.'));    

             } 
  

        } 

    }

    public function order_list_customer(Request $request)
    {   

        if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }

        $cart =  new Cart;


         $user_info = DB::table('users')->where('id','=',$user_id)->first();

         if(empty($user_info)){ $user_info = ''; }

         $items=DB::table('orders')->select('orders.order_id','orders.id','orders.status','orders.payment_method','orders.pay_status','orders.created_at','orders.total','vendor.vendor_id','vendor.username','vendor.last_name')->join('vendor', 'orders.vendor_id', '=', 'vendor.vendor_id')->where("orders.user_id",$user_id)->get();

        
       if(sizeof($items)){

      
        $vendor_id = $items[0]->vendor_id; 

      

        if($vendor_id!=0){

       
        
        $data['message'] = "Order List!";
		$data['status'] = 1;
		

		$data['data'] = array('items' => $items,'user_info' => $user_info);

	    }else{

        $data['message'] = "Order list empty";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

   	 }
	}

	public function userprofile(Request $request){

		if(!empty($request->user_id)){

            $user_id = $request->user_id;

        }else{

            $user_id = Session::get("_token");
        }

        $user_detail  = DB::table('users')->select('users.id','users.token','users.name','users.lname','users.email','users.profile_image','users.user_mob','users.user_address','users.user_city','users.user_states','users.user_zipcode','users.user_lat','users.user_long','users.avg_rating','users.rating_count','users.user_status','users.devicetype','users.devicetoken')->where('id', '=' ,$user_id)->first();

        if(!empty($user_detail)){

       
        
        $data['message'] = "User Detail";
		$data['status'] = 1;
		

		$data['data'] = array('user_info' => $user_detail);

	    }else{

        $data['message'] = "Invalid User id";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

	}

	public function order_detail(Request $request){

        $response= array();

		$vendor_id = Input::get('vendor_id');

		$order_id = Input::get('order_id');

	    $order_detail = Order::where('vendor_id', '=' ,$vendor_id)

					->where('id', '=' ,$order_id)

					->get();

		$orderItems = Productservice::select('product_service.*', 'orders_ps.ps_qty')

			->join('orders_ps', 'orders_ps.ps_id', '=', 'product_service.id')->where('orders_ps.order_id', $order_id)->where('product_service.status', 1)->get();

	    if($order_detail->count() > 0){					

		$response['message'] = "Product service order detail";

		$response['status']= 1;

		$response['data'] = array('order_detail'=>$order_detail, 'order_items' => $orderItems);


	    }else{

        $response['message'] = "Product service order detail not found";

		$response['status']= 0;

		$response['data'] = array('order_detail'=>'');

	}

        return $response;

}

public function state_list(Request $request){

        $states  = DB::table('states')->get();

        if(!empty($states)){  
        $data['message'] = "States List";
		$data['status'] = 1;
		$data['data'] = array('states_list' => $states);

	    }else{

        $data['message'] = "Data not found";
		$data['status'] = 0;
		$data['quantity'] = 0;

	    }

		return $data;

	}


}




?> 