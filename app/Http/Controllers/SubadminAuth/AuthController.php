<?php
namespace App\Http\Controllers\SubadminAuth;
use Auth;
use App\Subadmin;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
/*Added by me*/
use Illuminate\Http\Request;
use DB;
use Session;
//use Illuminate\Foundation\Auth\Redirect;
//use App\Http\Controllers\Redirect;
use Redirect;
/*Ent*/
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/sub-admin/dashboard';
    protected $redirectAfterLogout = '/sub-admin/login';
    protected $guard = 'subadmin';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function showLoginForm()
    {

        // echo "test";
        // die;

        if (view()->exists('auth.authenticate')) {

            // if (Auth::guard($this->getGuard())->attempt($credentials, true)){
            //     return $this->handleUserWasAuthenticated($request, $throttles);
            // }

            return view('auth.authenticate');

        }




     //return view('admin.auth.reset');
        return view('subadmin.auth.login');
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    public function forgotpwd()
    {
      if (view()->exists('auth.authenticate')) {
       return view('auth.authenticate');
   }
   return view('admin.auth.reset');
}
/*Forget password*/
public function forgetPassword()
{
    if (view()->exists('auth.authenticate')) {
       return view('auth.authenticate');
   }
   return view('admin.auth.reset_pass');
}

public function generateRandomString($length = 6) {
 $characters = '0123456789';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
     $randomString .= $characters[rand(0, $charactersLength - 1)];
 }
 return $randomString;
}

/*Send otp after enter email*/
public function sentOTP(Request $request)
{
   $AdminEmail = $request->input('email');
   $Admin = DB::table('admins')->where('email',$AdminEmail)->first();
   if(!empty($Admin)){
 //    $email = $Admin->email;
 //    $name = $Admin->name;
    $Admin_id = $Admin->id;
 //    /*********** EMAIL FOR SEN OTP START *************/
    $otp_code = trim($this->generateRandomString(6));
 //    $msg_reg_otp = 'Dear '.trim($name).', The verification code for Forgot Password on Online order food is :'.$otp_code ;
 //    Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){
 //       $message->from('info@gmail.com', 'Online order food');
 //       $otp_email = trim($email);
 //       $message->to($otp_email);
 //     // $message->bcc('votiveshweta@gmail.com');
 //     // $message->bcc('votivemobile.pankaj@gmail.com');
 //     // $message->bcc('votivemobile.dilip@gmail.com');
 //     // $message->bcc('zubaer.votive@gmail.com');
 //       $message->subject('Forgot Passowrd OTP');
 //   });
    // $otp_code='';
    if(!empty($otp_code)){
     //$data=array('otp'=>$otp_code);
       DB::table('admins')
       ->where('id',$Admin_id)
       ->update(['otp'=>$otp_code]);
       Session::flash('Succes', 'OTP has been sent successfully.');
       session()->flash('Admin_id', $Admin_id);
     //$data=array('Admin_id'=>$Admin_id);
     //return Redirect::route('/admin/password/enter_otp')->with(['data' => $data]);
       return redirect()->to('/admin/password/enter_otp');
   }else{
    // echo "skdf";
    // die;
    Session::flash('Error', 'Email sent failed.');
    return redirect()->back();
}
    // // if (Mail::failures()){
    //          DB::table('admins')
    //  ->where('id',$Admin_id)
    //  ->update(['otp'=>$otp_code]);
    //  Session::flash('Succes', 'OTP has been sent successfully.');
    //  session()->flash('Admin_id', $Admin_id);
    // //     return redirect()->back();
    // // }
    // Session::flash('Succes', 'Email sent successfully');
    // return redirect()->to('/admin/password/enter_otp');
    //return redirect()->to();
}else{
    Session::flash('Error', 'Please enter your registered email.');
    return redirect()->back();
}
//return view('admin.auth.reset_pass');
}


/*Enter OTP */
public function VerifyOTPForm()
{
    session()->keep(['Admin_id']);
    return view('admin.auth.enter_your_otp');
}

/*OTP submit*/
public function VerifyOTP(Request $request)
{
    $enterOTP = $request->input('user_otp');
    $AdminID = $request->input('AdminID');
    $AdminData = DB::table('admins')->where('id',$AdminID)->first();
    $saveOTP = $AdminData->otp;
    $adminID = $AdminData->id;
    if($saveOTP==$enterOTP){
        //echo "Match";
        Session::flash('Succes', 'Your OTP has been verified successfully.');
        session()->flash('adminID', $adminID);
        return redirect()->to('/admin/password/reset_your_password');
    }else{
        Session::flash('Otp_Error', 'OTP did not match.');
        session()->keep(['Admin_id']);
        return redirect()->back();
    }
}
public function setNewPasswordForm()
{
    session()->keep(['adminID']);
    return view('admin.auth.set_new_pass');
}
public function setNewPassword(Request $request)
{
    $new_pass = $request->input('new_pass');
    $adminID = $request->input('AdminID');
    $affected = DB::table('admins')
    ->where('id',$adminID)
    ->update(['password'=>bcrypt($new_pass)]);
    if($affected){
     //    $AdminData = DB::table('admins')->where('id',$adminID)->first();
     //    $email = $Admin->email;
     //    $name = $Admin->name;
     //    $msg_reg_otp = 'Dear '.trim($name).', Congratulations your password has been changed successfully.';
     //    Mail::raw($msg_reg_otp, function ($message) use ($carrier_email){
     //     $message->from('info@gmail.com', 'Online order food');
     //     $otp_email = trim($email);
     //     $message->to($otp_email);
     //     $message->subject('Passowrd successfully changed.');
     // });
        Session::flash('PassSucChange', 'Your password has been changed successfully.');
        return redirect('/admin');
    }else{
        Session::flash('Change_pass', 'Somthing is wrong! please try again.');
        session()->keep(['adminID']);
        return redirect()->back();
    }
}

public function logout(Request $request) {
    Auth::logout();
    return redirect('/sub-admin');
}



}