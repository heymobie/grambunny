<?php

namespace App\Http\Controllers;

Use DB;

use Hash;

use Route;

use Session;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;

use Redirect;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

use App\Review;

use App\Transport_review;

use Braintree_Transaction;

use App\User;

use Carbon\Carbon;


//use PDF;

class UserController extends Controller
{

    public function __construct(){

    $this->middleware('auth:user');

  }


	public function dashboard()
	{

		$user_id =  Auth::guard("user")->user()->id;

		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

	    $userRating  = DB::table('user_ratings')->where('user_id', '=' ,$user_id)->where('status', '=' ,1)->simplePaginate(2);

		$state_list = DB::table('states')->get();

  		$data_onview = array('user_detail' =>$user_detail,'id'=>$user_id,'userRating'=>$userRating,'state_list'=>$state_list);

		return view('user-dashboard')->with($data_onview);;

	}

	public function my_orders()

	{


		$user_id =  Auth::guard("user")->user()->id;

		$order_detail  = DB::table('orders')

		->where('user_id', '=' ,$user_id)

		->where('product_type', '!=' ,3)

		->orderBy('id', 'desc')

		->simplePaginate(8);


  		$data_onview = array('order_detail' =>$order_detail);

		return view('my-orders')->with($data_onview);

	}

	public function my_events()
	{
	 	$user_id =  Auth::guard("user")->user()->id;
		$order_detail  = DB::table('orders')
		->rightJoin('orders_ps', 'orders_ps.order_id', '=', 'orders.id')
		->rightJoin('product_service', 'product_service.id', '=', 'orders_ps.ps_id')
		->where('product_service.type', '=' ,3)
		->where('user_id', '=' ,$user_id)
		->select('orders.*','product_service.venue_address')
		->distinct('orders.id')
		->orderBy('orders.id', 'desc')
		->simplePaginate(6);
		//dd($order_detail)	;
  		$data_onview = array('order_detail' =>$order_detail);
		return view('my-events')->with($data_onview);
	}


	public function my_invoice(Request $request)

	{

		$user_id =  Auth::guard("user")->user()->id;

		$orderid = $request->id;

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->first();


		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();

		$data = array('order_detail' =>$order_detail,'ps_list' =>$pslist);

       // $pdf = PDF::loadView('myPDF', $data);

       // return $pdf->download('order-invoice.pdf');

	}



	public function order_details(Request $request)
	{

		$user_id =  Auth::guard("user")->user()->id;

		$orderid = $request->id;

		$order_count = DB::table('orders')
		->where('user_id', '=' ,$user_id)
		->where('id', '=' ,$orderid)
		->get();

		if(count($order_count)>0){

		$order_detail = DB::table('orders')
		->where('user_id', '=' ,$user_id)
		->where('id', '=' ,$orderid)
		->first();


	    $multiple_order_id = (int)$order_detail->id;
		$multiple_order_detail = DB::table('book_multiple_ticket')
		->where('order_id', '=' ,$multiple_order_id)
		->get();

		$vendor_order = DB::table('orders')
		->where('user_id', '=' ,$user_id)
		->where('id', '=' ,$orderid)
		->join('vendor', 'vendor.vendor_id', '=', 'orders.vendor_id')
		->first();


		/* $pslist = DB::table('orders_ps')
		->where('order_id', '=' ,$orderid)
		->get(); */

		$pslist = DB::table('orders_ps')
		->leftJoin('product_service', 'product_service.id' , '='  ,'orders_ps.ps_id')
		->select('orders_ps.*','product_service.type','product_service.ticket_service_fee','product_service.ticket_fee','product_service.venue_address')
		->where('order_id', '=' ,$orderid)
		->get();


  		$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist, 'vendor_order' =>$vendor_order,'multiple_order_detail' => $multiple_order_detail);

		return view('order-details')->with($data_onview);

	}else{

	return redirect()->to('/my-orders');

  }

	}



	public function view_invoice(Request $request)

	{

		$user_id =  Auth::guard("user")->user()->id;

		$orderid = $request->id;

		$order_detail = DB::table('orders')

		->where('id', '=' ,$orderid)

		->first();


		$pslist = DB::table('orders_ps')

		->where('order_id', '=' ,$orderid)

		->get();


  		$data_onview = array('order_detail' =>$order_detail,'ps_list' =>$pslist);
		return view('invoice-details')->with($data_onview);

	}


	public function udashboard()

	{



		$user_id =  Auth::guard("user")->user()->id;



		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



  		$data_onview = array('user_detail' =>$user_detail,'id'=>$user_id);



		return view('user_dashboard')->with($data_onview);;



	}



	public function update_account_form()



	{



		$user_id =  Auth::guard("user")->user()->id;

		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();

  		$data_onview = array('user_detail' =>$user_detail,'id'=>$user_id);

		return View('user.update_account')->with($data_onview);



	}

	public function crop_profile_image(Request $request)
	{

		//echo "text";die;

		$image = Input::get('productimg');

		//$vendor_id = Input::get('vendor_id');
		$user_id = Input::get('user_id');

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);

        $profile_img= '1'.time().'.png';
        $path = public_path('uploads/user/'.$profile_img);
        file_put_contents($path, $image);

        DB::table('users')
		//->where('vendor_id', $vendor_id)
		->where('id', $user_id)
		->update(['profile_image' => $profile_img]);

		// DB::table('ps_images')
		// ->where('ps_id', $product_id)
		// ->where('thumb', 1)
		// ->update(['name' => $profile_img]);

        return response()->json(['status'=>$profile_img]);

	}


	public function updateaccount_action(Request $request)

	{

		$request->all();

		$this->validate($request,[

		 //	"firstname"	=> "required|min:3",

		// 	"lastname"	=> "required|min:3",

		// 	"email"		=> "required|email|unique:users",

		 //	"city"		=> "required",

		 	"dob"		=> "required|date",

		 //	"phone"		=> "required|min:10",

		 //	"password"	=> "required|min:6"

		]);

		$user_id = Input::get('user_id');

		$address = Input::get('user_address');

		 //$user_dobs = date("Y/m/d", strtotime($request->user_dob));

		  if(preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $request->dob)) {

       }else{

      return redirect()->back()->withErrors(["dob" => "Please use valid date format"])->withInput();

       }

		$date=Carbon::now();

    $minDate= $date->subYear(21);

		$dob=Carbon::createFromFormat("m/d/Y",$request->dob);

		$dob = date("Y-m-d", strtotime($request->dob));

		if (!$minDate->greaterThan($dob)) {

		return redirect()->back()->withErrors(["dob" => "Date must be greater then 21 Year."])->withInput();

		 }

		$profile_image = $request->profile_old;

		//$profile_image = $profile_old;

     if(empty($profile_image)){ $profile_image = "dummy.png"; }

		$license_front_image = $request->license_front_old;
		$license_back_image = $request->license_back_old;
		$marijuana_card_image = $request->marijuana_card_old;


  $url = preg_replace("/ /", "%20", "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU");

		$response = file_get_contents($url);

		$data = json_decode($response);

		$geo=$data->results[0]->geometry->location;



		$request->hasFile("profile") ? $request->file("profile")->move("public/uploads/user/",$profile_image=str_random(16).'.jpg') : "";

		$request->hasFile("license_front") ? $request->file("license_front")->move("public/uploads/user/",$license_front_image=str_random(16).'.'.$request->license_front->extension()) : "";

		$request->hasFile("license_back") ? $request->file("license_back")->move("public/uploads/user/",$license_back_image=str_random(16).'.'.$request->license_back->extension()) : "";

        $request->hasFile("marijuana_card") ? $request->file("marijuana_card")->move("public/uploads/user/",$marijuana_card_image=str_random(16).'.'.$request->marijuana_card->extension()) : "";

		if(empty($profile_image)){ $profile_image = "user.jpg"; }


			DB::table('users')

            ->where('id', $user_id)

            ->update(['name' => Input::get('name'),

					  'lname'=>  Input::get('lname'),

					  'user_mob'=>  Input::get('user_mob'),

					  'dob'=>  $dob,

					  'user_address'=>  Input::get('user_address'),

					  'user_city'=>  Input::get('user_city'),

					  'user_states'=>  Input::get('user_states'),

					  'user_zipcode'=>  Input::get('user_zipcode'),

					  'license_front'=>  $license_front_image,

					  'license_back'=>  $license_back_image,

					  'customer_type'=>trim(Input::get('customer_type')),

					  'marijuana_card'=>  $marijuana_card_image,

					  'profile_image'=>  $profile_image,

					  'user_lat'=> $geo->lat,

					  'user_long'=> $geo->lng

					 ]);



		 Session::flash('upmessage', 'Account Updated Successfully.');

		 return redirect()->to('/myaccount');



	}


	public function change_password()



	{



		return View('user.update_password');



	}


	public function updatepassword_action(Request $request)

	{

		Session::flash('upassword', 'Password Updated Successfully.');



		$validation=$this->validate($request,[



            'old_password' => "required",



            'password' => 'required|confirmed|min:8',



            'password_confirmation' => "required"



        ]);



        if (!Hash::check($request->old_password, auth()->guard("user")->user()->password)) {



        	return redirect()->back()->withErrors(["old_password" => "Old password is incorrect"])->withInput();



        }



        $customer=User::find(auth()->guard("user")->user()->id);



        $customer->password=Hash::make($request->password);



        $customer->update();



        return redirect()->back()->with("success","Password updated successfully!");



	}







	public function updatepassword_actionNOTINUSE()



	{



		$user_id = Auth::guard("user")->user()->id;







		$credentials = [



                'password' => trim(Input::get('old_password')),



                'id' =>  $user_id



        ];







		if( Auth::attempt($credentials))



		{



			if (trim(Input::get('password')) == trim(Input::get('cnew_password'))) {



					DB::table('users')



	            	->where('id', $user_id)



	            	->update(['password' =>  Hash::make(trim(Input::get('password'))),



							  'user_pass'=> md5(trim(Input::get('password')))



							]);







				Session::flash('message', 'Password update sucessfully.');



				return redirect()->to('/update_password');



			} else {



				Session::flash('message_error', 'New password & confirm password has not matched.');



                return redirect()->to("/update_password");



			}



		}



		else



		{



			Session::flash('message_error', 'Old password has not matched.');



                return redirect()->to("/update_password");



		}



	}







	function order_listing()



	{



//		echo 'we are doing testing';







		$user_id =  Auth::user()->id;







		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



		$order_detail  =  DB::table('order')



						->leftJoin('search_restaurant_view', 'order.rest_id', '=', 'search_restaurant_view.rest_id')



						->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')



						->where('user_id', '=' ,$user_id)



						->where('order.order_status', '!=' ,'2')



						->select('*')



						->orderBy('order.order_id', 'desc')



						->groupBy('order.order_id')



						->get();











  		$data_onview = array('user_detail' =>$user_detail,



		    				 'order_detail'=>$order_detail,



		    				 'id'=>$user_id);



		//echo '<pre>'					 ;



	//print_r($order_detail)	;



//exit;







		return View('user.order_listing')->with($data_onview);



	}







	function payment_history()



	{



//		echo 'we are doing testing';







		$user_id =  Auth::user()->id;







		//$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



		$order_detail  =  DB::table('order_payment')



						->where('pay_userid', '=' ,$user_id)



						->get();







  		$data_onview = array('order_detail'=>$order_detail);







		return View('user.payment_history')->with($data_onview);



	}







	function my_wallet()



	{



//		echo 'we are doing testing';







		$user_id =  Auth::user()->id;







		//$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



		$wallet_detail  =  DB::table('wallet_transaction')



						->where('user_id', '=' ,$user_id)



						->orderBy('id', 'desc')



						->get();







		$user_detail  =  DB::table('users')



						->where('id', '=' ,$user_id)



						->value('wallet_amount');







  		$data_onview = array('wallet_detail'=>$wallet_detail,'wallet_amount'=>$user_detail);







		return View('user.my_wallet')->with($data_onview);



	}











    function payment_wallet()



	{







		$user_id =  Auth::user()->id;



		$wt_token = Session::get('_token');



	 	$card_number =  trim(Session::get('card_number'));



	  	$card_expdate = trim(Session::get('expire_month')).trim(Session::get('expire_year'));



	   	$card_cvv =  trim(Session::get('ccv'));



	   	$card_name = trim(Session::get('name_card_order'));



	   	$trans_id = trim(Session::get('transactionid'));



	   	$payment_status = trim(Session::get('ACK'));



	   	$card_status = trim(Session::get('card_status'));



	    $total_amt = trim(Session::get('total_amt'));







        //wallet_transaction







       if($payment_status){







	    $wallet_amount  =  DB::table('users')



						->where('id', '=' ,$user_id)



						->value('wallet_amount');







		if(!empty($wallet_amount)){







		$totalamt = $total_amt+$wallet_amount;







			DB::table('users')



            ->where('id', $user_id)



            ->update(['wallet_amount' => $totalamt]);







          }else{







            DB::table('users')



            ->where('id', $user_id)



            ->update(['wallet_amount' => $total_amt]);







          }







       $wallettxnin = array(



				'user_id'=>	$user_id,



				'order_id' => 0,



				'wallet_txn' => $total_amt,



				'cash_back'=> 0,



				'txn_method'=> 'Card',



				'txn_id'=> $trans_id,



				'paid_received'=> 'received',



				'txn_status'=> 1



			    );







		DB::table('wallet_transaction')->insert($wallettxnin);







	    Session::flash('message', 'Payment added Sucessfully.');







		return response()->json(array('order_id'=>$trans_id,'status'=>'1','order_uniqueid'=>$wt_token));







		}else{







			return response()->json(array('status'=>'0','message'=>"Payment is not submitted Properly! Please Payment again.", 'amt'=>$total_amt, 'trans_id'=>Session::get('transactionid')));



		}











	}











	public function payment_wallet_paypal()



	{







      	$user_id =  Auth::user()->id;



		$wt_token = Session::get('_token');



	 	$card_number =  trim(Session::get('card_number'));



	  	$card_expdate = trim(Session::get('expire_month')).trim(Session::get('expire_year'));



	   	$card_cvv =  trim(Session::get('ccv'));



	   	$card_name = trim(Session::get('name_card_order'));



	   	$trans_id = trim(Session::get('transactionid'));



	   	$payment_status = trim(Session::get('ACK'));



	   	$card_status = trim(Session::get('card_status'));



	    $total_amt = trim(Session::get('total_amt'));







        //wallet_transaction







       if($payment_status){







	    $wallet_amount  =  DB::table('users')



						->where('id', '=' ,$user_id)



						->value('wallet_amount');







		if(!empty($wallet_amount)){







		$totalamt = $total_amt+$wallet_amount;







			DB::table('users')



            ->where('id', $user_id)



            ->update(['wallet_amount' => $totalamt]);







          }else{







            DB::table('users')



            ->where('id', $user_id)



            ->update(['wallet_amount' => $total_amt]);







          }







       $wallettxnin = array(



				'user_id'=>	$user_id,



				'order_id' => 0,



				'wallet_txn' => $total_amt,



				'cash_back'=> 0,



				'txn_method'=> 'Paypal',



				'txn_id'=> $trans_id,



				'paid_received'=> 'received',



				'txn_status'=> 1



			    );







		DB::table('wallet_transaction')->insert($wallettxnin);







	    Session::flash('message', 'Payment added Sucessfully.');







		return response()->json(array('order_id'=>$trans_id,'status'=>'1','order_uniqueid'=>$wt_token));







		}else{







			return response()->json(array('status'=>'0','message'=>"Payment is not submitted Properly! Please Payment again.", 'amt'=>$total_amt, 'trans_id'=>Session::get('transactionid')));



		}











	}











   function add_wallet()



	{







        $add_wamount = Input::get('add_wamount');



		$user_id =  Auth::user()->id;







		$data['nonce'] ="";



		$data['amount'] = "";



		$nonce = "sandbox_9q9nzbs5_9ydqx2j9s9rhj4qc";



		$amount = $add_wamount;



		$status = Braintree_Transaction::sale([



			'amount' => $amount,



			'paymentMethodNonce' => $nonce,



			'options' => [



				'submitForSettlement' => True



			]



		]);







		//echo $status; die;











		$wallet_amount  =  DB::table('wallet_transaction')



						->where('user_id', '=' ,$user_id)



						->value('wallet_amount');











		if(!empty($wallet_amount)){







		$totalamt = $add_wamount+$wallet_amount;







			DB::table('wallet_transaction')



            ->where('user_id', $user_id)



            ->update(['wallet_amount' => $totalamt]);







          }else{







			    $wallettxn = array(



				'user_id'=>	$user_id,



				'wallet_amount'=> $add_wamount,



				'cash_back'=> 0,



				'status'=> 1



			    );







				DB::table('wallet_transaction')->insert($wallettxn);







          }







	    $wallet_detail  =  DB::table('wallet_transaction')



						->where('user_id', '=' ,$user_id)



						->get();







  		$data_onview = array('wallet_detail'=>$wallet_detail);







		return View('user.my_wallet')->with($data_onview);



	}











	function show_reviewfrm()



	{







		 $user_id =  Auth::user()->id;



	//	echo ' ORDERID = '.Route::current()->getParameter('orderid');



		if(Route::current()->getParameter('orderid'))



		{



		 $order_id = Route::current()->getParameter('orderid');



		 $order_detail  =  DB::table('order')



							->where('user_id', '=' ,$user_id)



							->where('order_id', '=' ,$order_id)



							->select('*')



							->orderBy('order.order_create', 'desc')



							->get();



			if(!empty($order_detail))



			{



				$data_onview = array('user_id' =>$user_id,



									 'order_detail'=>$order_detail,



									 'order_id'=>$order_id);







				return View('user.review_form')->with($data_onview);



			}



			else



			{



			 return redirect()->to("/myorder");



			}



		}



		else



		{



			 return redirect()->to("/myorder");



		}











	}







	function orderreview_action()



	{



		/*echo '<pre>';



		print_r($_POST);



		exit;*/







		$order_re = new Review;



		$order_re->re_orderid = Input::get('order_id');



		$order_re->re_userid = Input::get('user_id');



		$order_re->re_content = Input::get('review_text');



		$order_re->re_restid = Input::get('rest_id');



		$order_re->re_rating = Input::get('range');



		$order_re->re_food_good = Input::get('re_food_good');



		$order_re->re_delivery_ontime = Input::get('re_delivery_ontime');



		$order_re->re_order_accurate = Input::get('re_order_accurate');







		$order_re->re_status = 'SUBMIT';



		$order_re->save();



		$re_id = $order_re->re_id;







		Session::flash('message', 'Your Review Submited.');



       return redirect()->to("/myorder");



	}











	function transport_order_listing()



	{







		$user_id =  Auth::user()->id;







		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();







		$order_detail  =  DB::table('transport_order')



						->leftJoin('transport', 'transport_order.trans_id', '=', 'transport.transport_id')



						->leftJoin('vehicle', 'transport_order.vehicle_id', '=', 'vehicle.vehicle_id')



						->leftJoin('vehicle_cate', 'vehicle.vehicle_catid', '=', 'vehicle_cate.vcate_id')



		  				->leftJoin('vehicle_class', 'vehicle.vehicle_classid', '=', 'vehicle_class.vclass_id')



		  				->leftJoin('vehicle_make', 'vehicle.vehicle_make', '=', 'vehicle_make.make_id')



		 				->leftJoin('vehicle_model', 'vehicle.vehicle_model', '=', 'vehicle_model.model_id')







						->leftJoin('transport_review', 'transport_order.order_id', '=', 'transport_review.re_orderid')



						->where('transport_order.user_id', '=' ,$user_id)



						->where('transport_order.order_status', '!=' ,'2')



						->where('transport_order.order_status', '!=' ,'8')



						->select('*')



						->orderBy('transport_order.order_id', 'desc')



						->groupBy('transport_order.order_id')



						->get();











  		$data_onview = array('user_detail' =>$user_detail,



		    				 'order_detail'=>$order_detail,



		    				 'id'=>$user_id);



		//echo '<pre>'					 ;



	//print_r($order_detail)	;



//exit;







		return View('user.transport_order_listing')->with($data_onview);



	}







	function transport_orderreview_action()



	{



		/*echo '<pre>';



		print_r($_POST);



		exit;*/







		$order_re = new  Transport_review;



		$order_re->re_orderid = Input::get('order_id');



		$order_re->re_userid = Input::get('user_id');



		$order_re->re_vehicleid = Input::get('vehicle_id');



		$order_re->re_transid = Input::get('trans_id');



		$order_re->re_content = Input::get('review_text');



		$order_re->re_rating = Input::get('range');







		$order_re->re_status = 'SUBMIT';



		$order_re->save();



		$re_id = $order_re->re_id;







		Session::flash('message', 'Your Review Submited.');



       return redirect()->to("/mytransportorder");







	}











	function show_favorite_restaurant()



	{











		$user_id =  Auth::user()->id;







		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



		$fav_restaurant  =  DB::table('favourite_restaurant')



						->leftJoin('search_restaurant_view', 'favourite_restaurant.favrest_restid', '=', 'search_restaurant_view.rest_id')



						->where('favourite_restaurant.favrest_userid', '=' ,$user_id)



						->where('favourite_restaurant.favrest_status', '=' ,'1')



						->select('*')



						->orderBy('favourite_restaurant.favrest_id', 'desc')



						->groupBy('favourite_restaurant.favrest_id')



						->get();



















  		$data_onview = array('user_detail' =>$user_detail,



		    				 'restaurant_list'=>$fav_restaurant,



		    				 'id'=>$user_id);



		/*echo '<pre>'					 ;



	print_r($fav_restaurant)	;



exit;



			*/



		return View('user.favorite_listing')->with($data_onview);







	}











	function get_order_detail()



	{



			//echo '<pre>';



	//	print_r($_POST);







		$user_id =  Auth::user()->id;







		$order_id  =  Input::get('order_id');



		$rest_id  =  Input::get('rest_id');







		//echo $order_id = $orderid;







		$user_detail  = DB::table('users')->where('id', '=' ,$user_id)->get();



		$order_detail  =  DB::table('order')



						->leftJoin('search_restaurant_view', 'order.rest_id', '=', 'search_restaurant_view.rest_id')



						->leftJoin('review', 'order.order_id', '=', 'review.re_orderid')



						->where('user_id', '=' ,$user_id)



						->where('order.order_id', '=' ,$order_id)



						//->where('order.rest_id', '=' ,$rest_id)



						->select('*')



						->orderBy('order.order_id', 'desc')



						->groupBy('order.order_id')



						->get();











		 $cart_view =  view('user.order_view', array('detail_data' => $order_detail))->render();







		//echo "<pre>"; print_r($cart_view); die;







		   return response()->json( array('cart_view'=>$cart_view) );



	}



}



