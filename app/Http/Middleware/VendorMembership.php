<?php
namespace App\Http\Middleware;
use Closure ;
use Auth, Session ;

class VendorMembership{
    
  public function handle($request, Closure $next){
  		if (Auth::guard('vendor')->user()->membership["status"]==0) {
      		return redirect()->route("vendor.membershipPlans");
      	}
      	else{
        	return $next($request);
      	}
    }
}
