<?php
namespace App\Http\Middleware;  
  
use Closure;  
use Illuminate\Contracts\Auth\Guard;  
use Response;  
  
class checkHeader  
{  
    /** 
     * The Guard implementation. 
     * 
     * @var Guard 
     */  
  
    /** 
     * Handle an incoming request. 
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @param  \Closure  $next 
     * @return mixed  GRUBPOINTSAUTHKEY //HTTP_X_HARDIK GRUBPOINTSAUTHKEY
     */ 
    public function handle($request, Closure $next)  
    {  
		
        if(!isset($_SERVER['HTTP_X_GRUBPOINTSAUTHKEY'])){  
            return Response::json(array('error'=>'Unauthorized'));  
        }  
  
        if($_SERVER['HTTP_X_GRUBPOINTSAUTHKEY'] != '123456'){  
            return Response::json(array('error'=>'Wrong Authorized Header'));  
        }  
  
        return $next($request);  
    }  
}
