<?php
namespace App\Http\Middleware;
use Closure ;
use Auth, Session ;

class CustomerGuestMiddleware{
    
  public function handle($request, Closure $next){
      if (Auth::guard("user")->check()) {
        	return redirect()->route("homepage");
      } 
      else {
      	if (isset($_COOKIE["above21"])) {
        	return $next($request);
      	}
      	else{
      		return redirect()->route("above21");
      	}
      }
      
    }
  }
