<?php
namespace App\Http\Middleware;
use Closure ;
use Auth, Session ;

class AgeVerification{
    
  public function handle($request, Closure $next){
   
      	if (isset($_COOKIE["above21"])) {
        	return $next($request);
      	}
      	else{
      		return redirect()->route("above21");
      	}
    }
  }
