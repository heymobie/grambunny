<?php

namespace App\Http;

class Helper
{
    public static function calculate_time_span($date) {
        $seconds = strtotime(date('Y-m-d H:i:s')) - strtotime($date);
        $months = floor($seconds / (3600 * 24 * 30));
        $day = floor($seconds / (3600 * 24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours * 3600)) / 60);
        $secs = floor($seconds % 60);
        
        if ($seconds < 60)
            $time = $secs . " secs ago";
        else if ($seconds < 60 * 60) {
            $time = $mins . " min ago";
            if($mins > 1) {
                $time = $mins . " mins ago";
            }
        }
        else if ($seconds < 24 * 60 * 60) {
            $time = $hours . " hour ago";
            if($hours > 1) {
                $time = $hours . " hours ago";
            }
        }
        else if ($day > cal_days_in_month(CAL_GREGORIAN, date('m'), date('y'))) {
            $time = $months . " month ago";
            if($months > 1) {
                $time = $months . " months ago";
            }
        }  
        else {
            $time = $day . " day ago";
            if($day > 1) {
                $time = $day . " days ago";
            }
        }
        return $time;
    }
}
