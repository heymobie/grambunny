<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Deliveryman extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    /*
    protected $fillable = [
        'name','lname', 'email', 'password', 'user_status','user_pass','user_mob','carrier_id','carrier_name','carrier_email'
    ];
    */


    protected $fillable = [
        'name','lname', 'email', 'password', 'user_mob','user_address','user_city','user_zipcode','is_admin','user_status','user_pass','deviceid','devicetype','carrier_id','carrier_name','carrier_email','api_token','user_role','user_fbid','user_gpid','restrictions','dummy1','dummy2','dummy3','checkout_verificaiton','log_id','created_at','updated_at'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $table = 'deliveryman';
}
