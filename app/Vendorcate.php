<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorcate extends Model
{
    //
    public $timestamps = false;

    protected $table = 'vendor_category';
    
    protected $primaryKey = 'id';

}
