<?php



namespace App;



use Illuminate\Database\Eloquent\Model;



class Proservicescate extends Model

{

    //

    public $timestamps = false;



    protected $table = 'product_service_category';

    protected $primaryKey = 'id';

    protected $appends= ["subcategory"];



    

    public function getSubCategoryAttribute()

    {

    	return Proservices_subcate::where("category_id",$this->id)->get();

    }

}

