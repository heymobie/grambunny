<?php

namespace App;

use App\Rating;

use App\Vendor;

use App\Proservicescate;

use App\Proservices_subcate;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Productservice extends Model
{

    //

    protected $table = 'product_service';

    protected $primaryKey = 'id';

   // protected $appends=["ratings","categoryname","ImageURL","subcategoryname","vendor","stockStatus","images"];

    protected $appends=["categoryname","ImageURL","subcategoryname","images","totalprice","Vendor"];


    public function getCategoryNameAttribute(){

    	$category=Proservicescate::find($this->category_id);

		if ($category) {

			return $category->category;

		}

			return null;

		}


        public function getmerchantnameAttribute(){

    	$vids = Vendor::find($this->vendor_id);

		if ($vids) {

			return $vids->name.' '.$vids->last_name;

		}

			return 'Admin';

		}


    public function getsubcategorynameAttribute() {

		$subcategory=Proservices_subcate::find($this->sub_category_id);

		if ($subcategory) {

			return $subcategory->sub_category;

		}

		return "";

    }


        public function gettotalpriceAttribute() {

		$price=$this->price;
		$quantity=$this->quantity;

		$subtotal = $price*$quantity;

		$subtotal = number_format($subtotal,2);

		return $subtotal;

    }



    public function getImageURLAttribute(){

      	return asset("/public/uploads/product")."/".$this->image;

	}




	public function getVendorAttribute(){
        // $payment_cash = "";
        $payment_square_payment = "";
		$vendordtls = Vendor::find($this->vendor_id);
		// $vendordtls = Vendor::find($this->vendor_id);

		//$vendordtls = DB::table("vendor")->where("vendor_id",$this->vendor_id)->get();
		$paymentstatus = DB::table("payment_setting")->where("vendor_id",$this->vendor_id)->first();




      $vendordata = array(
                 "vendor_id" => $vendordtls['vendor_id'],
                 "unique_id" => isset($vendordtls['unique_id'])?$vendordtls['unique_id']:"",
                    "name" => isset($vendordtls['name'])?$vendordtls['name']:"",
                    "username" => isset($vendordtls['username'])?$vendordtls['username']:"",
                    "last_name" => isset($vendordtls['last_name'])?$vendordtls['last_name']:"",
                    "business_name" => isset($vendordtls['business_name'])?$vendordtls['business_name']:"",
                    "email" => isset($vendordtls['email'])?$vendordtls['email']:"",
                    "mailing_address" => isset($vendordtls['mailing_address'])?$vendordtls['mailing_address']:"",
                    "address" => isset($vendordtls['address'])?$vendordtls['address']:"",
                    "avg_rating" => isset($vendordtls['avg_rating'])? (string)$vendordtls['avg_rating']:"",
                    "rating_count" => isset($vendordtls['rating_count'])? (string)$vendordtls['rating_count']:"",
                    "lat" => isset($vendordtls['lat'])?$vendordtls['lat']:"",
                    "lng" => isset($vendordtls['lng'])?$vendordtls['lng']:"",
                    "address1" =>isset($vendordtls['address1'])?$vendordtls['address1']:"",
                 "suburb" =>isset($vendordtls['suburb'])?$vendordtls['suburb']:"",
                    "state" => isset($vendordtls['state'])?$vendordtls['state']:"",
                    "city" => isset($vendordtls['city'])?$vendordtls['city']:"",
                    "zipcode" => isset($vendordtls['zipcode'])?$vendordtls['zipcode']:"",
                    "mob_no" => isset($vendordtls['mob_no'])?$vendordtls['mob_no']:"",
                    "contact_no" => isset($vendordtls['contact_no'])?$vendordtls['contact_no']:"",
                    "vendor_status" => isset($vendordtls['vendor_status'])? (string)$vendordtls['vendor_status']:"",
                    "login_status" => isset($vendordtls['login_status'])? (string)$vendordtls['login_status']:"",
                    "vendor_type" => isset($vendordtls['vendor_type'])?$vendordtls['vendor_type']:"",
                    "wallet_amount" => isset($vendordtls['wallet_amount'])?$vendordtls['wallet_amount']:"",
                    "deviceid" => isset($vendordtls['deviceid'])?$vendordtls['deviceid']:"",
                    "devicetype" => isset($vendordtls['devicetype'])?$vendordtls['devicetype']:"",
                    "market_area" => isset($vendordtls['market_area'])?$vendordtls['market_area']:"",
                    "service_radius" => isset($vendordtls['service_radius'])?$vendordtls['service_radius']:"",
                    "driver_license" => isset($vendordtls['driver_license'])?$vendordtls['driver_license']:"",
                    "license_expiry" => isset($vendordtls['license_expiry'])?$vendordtls['license_expiry']:"",
                    "license_front" => isset($vendordtls['license_front'])?$vendordtls['license_front']:"",
                    "license_back" => isset($vendordtls['license_back'])?$vendordtls['license_back']:"",
                    "ssn" => isset($vendordtls['ssn'])?$vendordtls['ssn']:"",
                    "dob" => isset($vendordtls['dob'])?$vendordtls['dob']:"",
                    "profile_img1" => isset($vendordtls['profile_img1'])?$vendordtls['profile_img1']:"",
                    "profile_img2" => isset($vendordtls['profile_img2'])?$vendordtls['profile_img2']:"",
                    "profile_img3" => isset($vendordtls['profile_img3'])?$vendordtls['profile_img3']:"",
                    "profile_img4" => isset($vendordtls['profile_img4'])?$vendordtls['profile_img4']:"",
                    "type" => isset($vendordtls['type'])? (string)$vendordtls['type']:"",
                    "category_id" => isset($vendordtls['category_id'])? (string)$vendordtls['category_id']:"",
                    "sub_category_id" => isset($vendordtls['sub_category_id'])?$vendordtls['sub_category_id']:"",
                    "service" => isset($vendordtls['service'])?$vendordtls['service']:"",
                    "permit_type" => isset($vendordtls['permit_type'])?$vendordtls['permit_type']:"",
                    "permit_number" => isset($vendordtls['permit_number'])?$vendordtls['permit_number']:"",
                    "permit_expiry" => isset($vendordtls['permit_expiry'])?$vendordtls['permit_expiry']:"",
                    "description" => isset($vendordtls['description'])?$vendordtls['description']:"",
                    "otp" => isset($vendordtls['otp'])?$vendordtls['otp']:"",
                    "forgetpass_request" => isset($vendordtls['forgetpass_request'])?$vendordtls['forgetpass_request']:"",
                    "forgetpass_request_status" => isset($vendordtls['forgetpass_request_status'])?$vendordtls['forgetpass_request_status']:"",
                    "plan_id" => isset($vendordtls['plan_id'])?$vendordtls['plan_id']:"",
                    "plan_purchased" => isset($vendordtls['plan_purchased'])?$vendordtls['plan_purchased']:"",
                    "plan_expiry" => isset($vendordtls['plan_expiry'])?$vendordtls['plan_expiry']:"",
                    "txn_id" => isset($vendordtls['txn_id'])?$vendordtls['txn_id']:"",
                    "created_at" => isset($vendordtls['created_at'])?$vendordtls['created_at']:"",
                    "updated_at" => isset($vendordtls['updated_at'])?$vendordtls['updated_at']:"",
                    "stripe_id" => isset($vendordtls['stripe_id'])?$vendordtls['stripe_id']:"",
                    "make" => isset($vendordtls['make'])?$vendordtls['make']:"",
                    "model" => isset($vendordtls['model'])?$vendordtls['model']:"",
                    "color" => isset($vendordtls['color'])?$vendordtls['color']:"",
                    "year" => isset($vendordtls['year'])?$vendordtls['year']:"",
                    "license_plate" => isset($vendordtls['license_plate'])?$vendordtls['license_plate']:"",
                    "views" => isset($vendordtls['views'])? (string)$vendordtls['views']:"",
                    "sales_tax" => isset($vendordtls['sales_tax'])? (string)$vendordtls['sales_tax']:"",
                    "excise_tax" => isset($vendordtls['excise_tax'])? (string)$vendordtls['excise_tax']:"",
                    "city_tax" => isset($vendordtls['city_tax'])? (string)$vendordtls['city_tax']:"",
                    "commission_rate" => isset($vendordtls['commission_rate'])? (string)$vendordtls['commission_rate']:"",
                    "delivery_fee" => isset($vendordtls['delivery_fee'])? (string)$vendordtls['delivery_fee']:"",
                    "minimum_order_amount" => isset($vendordtls['minimum_order_amount'])? (string)$vendordtls['minimum_order_amount']:"",
                    "map_icon" => isset($vendordtls['map_icon'])?$vendordtls['map_icon']:"",
                    "type_of_merchant" => isset($vendordtls['type_of_merchant'])?$vendordtls['type_of_merchant']:"",
                    "cash_card" => isset($vendordtls['cash_card'])? (string)$vendordtls['cash_card']:"",
                    "licensefrontURL" => isset($vendordtls['licensefrontURL'])?$vendordtls['licensefrontURL']:"",
                    "licensebackURL" => isset($vendordtls['licensebackURL'])?$vendordtls['licensebackURL']:"",
                    "profileURL" => isset($vendordtls['profileURL'])?$vendordtls['profileURL']:"",
                    "profile2URL" => isset($vendordtls['profile2URL'])?$vendordtls['profile2URL']:"",
                    "profile3URL" => isset($vendordtls['profile3URL'])?$vendordtls['profile3URL']:"",
                    "profile4URL" => isset($vendordtls['profile4URL'])?$vendordtls['profile4URL']:"",
                    "membership" => isset($vendordtls['membership'])?$vendordtls['membership']:"",
                    "fullname" => isset($vendordtls['fullname'])?$vendordtls['fullname']:"",
                    "Category" => isset($vendordtls['Category'])?$vendordtls['Category']:"",
                    "Subcategory" => isset($vendordtls['Subcategory'])?$vendordtls['Subcategory']:"",

                    "payment_cash" => !empty($paymentstatus->cash) || isset($paymentstatus->cash) ? $paymentstatus->cash : null,
                    "payment_swipe" => !empty($paymentstatus->swipe) || isset($paymentstatus->swipe) ? $paymentstatus->swipe : null,
                    "credit_card" => !empty($paymentstatus->credit_card) || isset($paymentstatus->credit_card) ? $paymentstatus->credit_card : null,
                    "square_payment" => !empty($paymentstatus->square_payment) || isset($paymentstatus->square_payment) ? $paymentstatus->square_payment : null

                         );

      return $vendordata;

	}



	public function getStockStatusAttribute()

	{

		return $this->stock==1? ["class" => "","status"=>"In Stock"]: ["class" => "text-danger","status"=>"Out of Stock"];

	}



	public function getImagesAttribute()
	{

		$images = DB::table("ps_images")->where("ps_id",$this->id)->get();

		$imagesWithPath=$images->map(function($image){

	    $image->path=asset("/public/uploads/product")."/".$image->name;

		return $image;

		});



		foreach ($imagesWithPath as $key => $value) {

                $search = null;
                $replace = '""';
                array_walk($value,
                function (&$v) use ($search, $replace){
                    $v = str_replace($search, $replace, $v);
                    }
                );

          /* $imagesWithP[] = array(
                              'id' => isset($value->id)?$value->id:"",
                              'ps_id' => isset($value->ps_id)?$value->ps_id:"",
                              'name' => isset($value->name)?$value->name:"",
                              'thumb' => isset($value->thumb)?$value->thumb:"",
                              'created_at' => isset($value->created_at)?$value->created_at:"",
                              'updated_at' => isset($value->updated_at)?$value->updated_at:"",
                              'path' => isset($value->path)?$value->path:""
                           ); */

          }


		return $imagesWithPath;

	}


	public function getRatingsAttribute()
    {

		$rating= Rating::where("type",1)->where("ps_id",$this->id)->where("status",1)->get();

        $sum= $rating->sum("rating");

        if ($rating->count() > 0) {

            $total=$sum/$rating->count();

            $totalRating= number_format( $total,1);

        }else{

            $totalRating= 0;

		}

        $reviews= Rating::where("type",1)->where("ps_id",$this->id)->where("status",1)->latest()->paginate(15);

		return ["reviews" => $reviews,"total" => $totalRating];

	}



}

