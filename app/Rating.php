<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $appends=["user"];

    public function getUserAttribute()
    {
        return User::find($this->user_id);
    }
}
