<?php
// use Illuminate\Http\Request;
use App\Http\Controllers\NewApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });Route::post('ws/register', 'Api\UserController@register');


Route::get("api/category-list", "Api\CategoryController@category_list");


Route::post('conversations/store', 'ConversationController@store');
Route::post('/ws/driver-list', 'NewApiController@driver_list');