<?php

use App\Vendor;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Mail;


/*

|--------------------------------------------------------------------------

| Application Routes

|--------------------------------------------------------------------------

|

| Here is where you can register all of the routes for an application.

| It's a breeze. Simply tell Laravel the URIs it should respond to

| and give it the controller to call when that URI is requested.

|

*/

// New Pages route
Route::post('/ws/user/delete', 'WsController@user_delete')->middleware('CheckToken');


Route::get('/auth/redirect/{provider}', 'SocialController@redirect')->name("social.auth");

Route::get('/auth/{provider}/callback/', 'SocialController@callback');

// clear application cache
Route::get('/clear-cache', function () {
	Artisan::call('cache:clear');
	return "Application cache flushed";
});

// clear route cache
Route::get('/clear-route-cache', function () {
	Artisan::call('route:clear');
	return "Route cache file removed";
});

// clear view compiled files
Route::get('/clear-view-compiled-cache', function () {
	Artisan::call('view:clear');
	return "View compiled files removed";
});

// clear config files
Route::get('/clear-config-cache', function () {
	Artisan::call('config:clear');
	return "Configuration cache file removed";
});


Route::get("/test", function () {

	Mail::send("emails.test", [], function ($message) {

		$message->from(config("app.webmail"), config("app.mailname"));

		$message->subject("Test Email");

		$message->to("votivedeepak.php@gmail.com");
	});
});


Route::get("/seller-detail", function () {
	return view("seller-details");
});

Route::get("/product-detail", function () {
	return view("product-details");
});

Route::get("/product-list", function () {
	return view("product-list");
});

Route::get("/checkout-page", function () {
	return view("checkout-page");
});

Route::get("/product-services-list", function () {
	return view("product-services-listing");
});

Route::post('/searchproduct', 'GeneralController@ajaxsearchproduct');

Route::post('/searchproductonload', 'GeneralController@searchproductonload');

Route::post('/searchproduct_new', 'GeneralController@ajaxsearchproduct_new');

Route::post('/searchproductonload_new', 'GeneralController@searchproductonload_new');

Route::get("/email-template", function () {
	return view("emails.email");
});

Route::get('/user-dashboard', 'UserController@udashboard');

Route::get('/my-orders', 'UserController@my_orders')->name("myorders");

Route::get('/my-events', 'UserController@my_events')->name("myevents");

Route::get('/order-details/{id}', 'UserController@order_details')->name("orderdetails");

Route::get('/view-invoice/{id}', 'UserController@view_invoice')->name("viewinvoice");

//Route::get('/download-invoice/{id}','UserController@my_invoice');

Route::get('/about-us', 'FrontController@about_us')->name("page.aboutUs");

Route::get('/terms', 'FrontController@terms')->name("page.terms");

Route::get('/privacy-policy', 'FrontController@privacy_policy')->name("page.privacy");

//start 20092021
Route::get('/privacy-policy-mobile', 'FrontController@privacy_policy_mobile');
Route::get('/faq-mobile', 'FrontController@faq_mobile');
Route::get('/terms-mobile', 'FrontController@terms_mobile');
Route::get('/about-us-mobile', 'FrontController@about_us_mobile');
Route::get('/contact-us-mobile', 'FrontController@contact_us_mobile');
Route::get('/help-mobile', 'FrontController@help_mobile');

//end 20092021

Route::get('/faq', 'FrontController@faq')->name("page.faq");

Route::get('/contact-us', 'FrontController@contact_us')->name("page.contactUs");

Route::post('/contact-us', 'FrontController@contactus')->name("page.contacts");

Route::get('/help', 'FrontController@help')->name("page.help");

Route::get('/shop', 'FrontController@shop_now')->name("page.shopnow");

Route::get('/category/{id}', 'FrontController@productdetail')->name("page.productdetail");

Route::get('/productdetail', 'FrontController@productdetail')->name("page.productdetail");

Route::any('/user/videos_popup_play', 'FrontController@videos_popup_play')->name('videos_popup_play');

Route::get("/cmd", function () {

	//return view("product-details");

	//return Hash::make("Votive@123");

	//config:clear

	//cache:clear

	// return config:cache;
	return Artisan::call('config:cache');


	// return config("mail");

	return Artisan::call('config:clear');
});

Route::get('/token', function () {
	return csrf_token();
});

//Routes for WSCController
Route::post('/ws/order_list_customer', 'WscController@order_list_customer')->middleware('CheckToken');
Route::post('/ws/order_detail', 'WscController@order_detail')->middleware('CheckToken');
Route::post('/ws/add_to_cart', 'WscController@add_to_cart')->middleware('CheckToken');
Route::post('/ws/customer/product-rating', 'WscController@product_rating');
Route::post('/ws/customer/quantity-increase', 'WscController@quantity_increase');
Route::post('/ws/customer/delete_product', 'WscController@delete_product');

Route::post('/ws/driver-list', 'WsController@driver_list');
// Route::post('/ws/driver-list1', 'NewApiController@driver_list');
Route::post('/ws/driver_list_new','WsController@driver_list_new');

Route::post('/ws/category-list', 'WsController@category_list')->name('categorylist');
//Route::post('/ws/driver-list', 'WsController@driver_list')->name('driver_list');

Route::post('/ws/get-category-product', 'WsController@products_list');
Route::post('/ws/get-sidebar-filter', 'WsController@sidebar_filter');

Route::post('/ws/product-details', 'WsController@product_details');

Route::post('/ws/notification_list', 'WsController@notification_list');

Route::post('/ws/get-filter-products', 'WsController@filter_products'); // Test api

Route::post('/ws/cart_count', 'WsController@cartCount')->middleware('CheckToken');
Route::get('/ws/cartlist/{user_id}', 'WsController@cart_list')->middleware('CheckToken');
Route::post('/ws/cart_checkout', 'WsController@cart_checkout')->middleware('CheckToken');

///priyanka06092021
Route::post('/ws/get_cart_calculation', 'WsController@get_cart_calculation')->middleware('CheckToken');
Route::get('/ws/coupon_code_list/{vendor_id}/{user_id}', 'WsController@coupon_code_list')->middleware('CheckToken');
Route::post('/ws/apply_coupon_code', 'WsController@apply_coupon_code')->middleware('CheckToken');
Route::post('/ws/submit_order', 'WsController@submit_order')->middleware('CheckToken');
Route::post('/ws/userprofile', 'WsController@userprofile')->middleware('CheckToken');
Route::get('/ws/state_list', 'WsController@state_list')->middleware('CheckToken');
Route::post('/ws/setting', 'WsController@setting')->middleware('CheckToken');
Route::post('/ws/vendor_latlng', 'WsController@vendor_latlng')->middleware('CheckToken');
Route::post('/ws/related_product', 'WsController@related_product');
///end priyanka06092021

Route::post('/ws/merchant/order-list', 'grambunnyApiController@merchantOrders');

Route::post('/ws/order/order-ps-list', 'grambunnyApiController@orderPsList');

Route::post('/ws/merchant/merchant-to-user/', 'grambunnyApiController@merchant_to_user_location');

Route::post('/ws/merchant/ps-list', 'grambunnyApiController@MerchantItemsList');

Route::post('/ws/merchant/order-history-list', 'grambunnyApiController@OrdersHistory');

Route::post('/ws/merchant/transaction-history', 'grambunnyApiController@TxnHistory');

Route::post('/ws/merchant/product-ratings', 'grambunnyApiController@productRatings');

Route::post('/ws/merchant/merchant-ratings', 'grambunnyApiController@merchantRatings');

Route::post('/ws/merchant/rate-user', 'grambunnyApiController@rateUser');

Route::post('/ws/customer/merchant-rating', 'WsController@merchant_ratings');
Route::post('/ws/customer/ratings-user-list', 'WsController@user_ratings_list');
Route::post('/ws/merchant/customer-rating', 'WsController@customer_ratings');

Route::post('/ws/merchant/return-order-update', 'grambunnyApiController@returnOrderUpdate');

Route::post('/ws/merchant/chat_user_list', 'grambunnyApiController@chatuserlist');

Route::post('/ws/merchant/chat_history', 'grambunnyApiController@chathistory');

Route::post('/ws/merchant/chat', 'grambunnyApiController@chatsystem');

Route::post('/ws/merchant/scan-qrcode', 'WsController@scan_qrcode');
Route::post('/ws/event_order_list', 'WsController@event_order_list')->middleware('CheckToken');

Route::post('/ws/event_order_detail', 'WsController@event_order_detail')->middleware('CheckToken');
Route::post('/ws/merchant/event-order-list', 'heymobieApiController@merchantOrdersEvent');
Route::post('/ws/merchant/event_history', 'WsController@event_history_order');
Route::post('/ws/merchant/event-history-details', 'WsController@event_history_details');


Route::post('/chatsystem', 'grambunny\AjaxController@chatsystem');

Route::post('/chathistory', 'grambunny\AjaxController@chathistory');

Route::post('/chatmlist', 'grambunny\AjaxController@chatmlist');


Route::post('/mchatsystem', 'grambunny\ChatController@chatsystem');

Route::post('/mchathistory', 'grambunny\ChatController@chathistory');

Route::post('/mchatmlist', 'grambunny\ChatController@chatmlist');


Route::get('merchant/membership/purchase', 'Vendor\VendorMembershipController@paymentPage')->name("membership.purchase");

Route::post('merchant/membership/stripe-payment', 'Vendor\VendorMembershipController@stripePost')->name('stripe.post');

Route::get('/payment/process', 'PaymentsController@process')->name('payment.process');

Route::get('/payment/cancel', 'PaymentsController@cancel')->name('payment.cancel');

//Route::get('/form', function () { return view('bt'); });

Route::post('/payment/SetSession', 'PaymentsController@SetSession');

Route::get('/', 'GeneralController@home_page')->name("home")->middleware("ageVerification");

Route::get('/', 'GeneralController@home_page')->name("homepage")->middleware("ageVerification");

Route::get('/main', 'GeneralController@homenewpage')->name("home")->middleware("ageVerification");;

Route::get('/copy_google_data', 'GeneralController@show_auto_form');

Route::post('/post_google_data', 'GeneralController@get_form_data');

Route::get('/update_list_copy', 'GeneralController@update_list_copy');

Route::get('/content/{page_url}', 'GeneralController@show_content_page');

Route::get('/home', 'GeneralController@home_page')->name("home")->middleware("ageVerification");;

//Route::get('/stores','GeneralController@allstore');

Route::get('/terms_condition', 'GeneralController@show_terms_condition');

Route::get('/privacy_policy', 'GeneralController@show_privacy_policy');

Route::get('/restaurant_listing/cuisine/{cuisine_name}', 'ListController@restaurant_listing');

Route::get('/restaurant_listing/location/{surbru_name}', 'ListController@restaurant_listing');

Route::get('/restaurant_listing', 'ListController@restaurant_listing');

Route::get('/restaurant_listing/{surbru}', 'ListController@restaurant_listing');

Route::get('/get_suburblist', 'GeneralController@show_suburblist');

Route::get('/smtp_mail', 'MyMailer@index');

Route::get('/send_notification_test', 'WsController@testNotificationroot');

Route::get("/send_sms", "WsController@sendSms");

Route::get('/test_mail', 'FrontController@test_mail');

/* HOME PAGE SEARCH LIST */

Route::post('/restaurantslisting', 'ListController@home_search');

/* HOMEPAGE SEARCH LIST END */

Route::post('/check_user_duplicateemail', 'Auth\AuthController@user_duplicate_email');

//Route::get('/test','Auth\TestController@test');

//Route::post('/test','Auth\TestController@test');

Route::post('/resendotp', 'Auth\AuthController@resend_otp');

Route::post('/check_vendor_duplicateemail', 'OwnerAuth\AuthController@vendor_duplicate_email');

Route::post('auth/login', 'FrontController@authenticate');

Route::post('user_register', 'FrontController@register');

Route::post('/check_user_login', 'FrontController@check_user_login');

Route::group(['prefix' => '/',  'middleware' => ['customerGuest', "ageVerification"]], function () {

	Route::get("/sign-in", [

		"uses"	=> "FrontController@signInPage",

		"as"	=> "signInPage"

	]);

	Route::get("/sign-up", [

		"uses"	=> "FrontController@signUpPage",

		"as"	=> "signUpPage"

	]);

	Route::post("/web-auth", [

		"uses"	=> "FrontController@webAuth",

		"as"	=> "webAuth"

	]);

	Route::post("/web-register", [

		"uses"	=> "FrontController@webRegister",

		"as"	=> "webRegister"

	]);

	Route::get("/sign-up/success/{id}", [

		"uses"	=> "FrontController@registrationSuccess",

		"as"	=> "registrationSuccess"

	]);

	Route::get("/email-verification/{token}", [

		"uses"	=> "FrontController@verifyEmail",

		"as"	=> "EmailVerificationLink"

	]);

	Route::get("/forgot-password", [

		"uses"	=> "FrontController@forgotPassword",

		"as"	=> "forgotPassword"

	]);

	Route::post("/forgot-password-reset-link", [

		"uses"	=> "FrontController@sendPasswordResetLink",

		"as"	=> "sendPasswordResetLink"

	]);

	Route::get("/reset-password/{token}", [

		"uses"	=> "FrontController@ResetPasswordPage",

		"as"	=> "ResetPasswordPage"

	]);

	Route::post("/reset.password/{token}", [

		"uses"	=> "FrontController@updatePassword",

		"as"	=> "reset.Passwordupdate"

	]);

	Route::get("/sign-up-guest", [

		"uses"  => "FrontController@guest_register",

		"as"    => "guest_register"

	]);

	Route::post("/web-register-guest", [

		"uses"  => "FrontController@webRegisterguest",

		"as"    => "webRegisterguest"

	]);
});


Route::get("/user-logout", [

	"uses"	=> "FrontController@userLogout",

	"as"	=> "userLogout"

]);

Route::get("/above-21", [

	"uses"	=> "AgeVerificationController@confirmationPage",

	"as"	=> "above21"

]);



Route::get("/above-21-verification", [

	"uses"	=> "AgeVerificationController@verified",

	"as"	=> "above21-verify"

]);



Route::get("/come-back-21", [

	"uses"	=> "AgeVerificationController@noverified",

	"as"	=> "come-back-21"

]);



Route::get("/merchant/registration/{step}/", [

	"uses"	=> "Vendor\VendorAuthController@registrationForm",

	"as"	=> "vendorRegistration"

]);

Route::get("/merchant/registration/verify-email/{data}", [

	"uses"	=> "Vendor\VendorAuthController@verifyVendorEmail",

	"as"	=> "verifyVendorEmail"

]);

Route::post("/merchant/registration.first", [

	"uses"	=> "Vendor\VendorAuthController@firstStep",

	"as"	=> "vendorRegistration.firststep"

]);

Route::post("/merchant/registration.second", [

	"uses"	=> "Vendor\VendorAuthController@secondStep",

	"as"	=> "vendorRegistration.secondstep"

]);

Route::post("/merchant/registration.third", [

	"uses"	=> "Vendor\VendorAuthController@thirdStep",

	"as"	=> "vendorRegistration.thirdStep"

]);

Route::get("/merchant/registration/success/{id}", [

	"uses"	=> "Vendor\VendorAuthController@success",

	"as"	=> "vendorRegistration.success"

]);

Route::post('/merchant/crop_imagesave1', 'Vendor\VendorAuthController@crop_image_save');

Route::post('/merchant/subcategory', 'Vendor\VendorAuthController@subcategory_list');

Route::get("/all-states.json", "grambunny\AjaxController@states");

Route::get("/near-by", "grambunny\ProductsController@products");

Route::get("/near-by.json", "grambunny\ProductsController@getProducts");

Route::get("/filtered-ps.json", "grambunny\ProductsController@filteredPs");

Route::get("/filter-init.json", "grambunny\ProductsController@filterInit");

Route::get("/get-category-type.json", "grambunny\ProductsController@getCategoryType");

Route::get("/filter.json", "grambunny\ProductsController@filterResults");

Route::post("/nearby-merchants.json", "grambunny\ProductsController@nearbyMerchants");

Route::get("/nearbyMerchantstest", "grambunny\ProductsController@nearbyMerchantstest");

Route::get("/{slug}/details/", "grambunny\ProductsController@productDetails")->name("productDetails");

Route::get("/{username}/{slug}/detail/", "grambunny\ProductsController@productDetail")->name("productDetail");

Route::get("{username}/merchant", "grambunny\MerchantProfileController@profile")->name("merchantProfile");

Route::get("{username}/store", "grambunny\MerchantProfileController@profile")->name("merchantServices"); //services


Route::get("/cart", "grambunny\CheckoutController@cart")->name("cart");

Route::get("/checkout", "grambunny\CheckoutController@checkout")->name("checkout");

Route::post('/submit_order', 'grambunny\CheckoutController@submit_order')->name("submit_order");

Route::get('/createpdf', 'grambunny\CheckoutController@createpdf');

Route::get("thank_you_page/{id}", "grambunny\CheckoutController@thank_you_page")->name("thank_you_page");

Route::get("payment_successful", "grambunny\CheckoutController@successful")->name("successful");

Route::get("payment_failed", "grambunny\CheckoutController@failed")->name("failed");

Route::get("track-order/{id}", "grambunny\CheckoutController@track_order")->name("track_order");

Route::get("location/", "grambunny\CheckoutController@location");

Route::get("/sms", "grambunny\CheckoutController@sendSms");


Route::post('/check_same_merchant', 'grambunny\CheckoutController@check_same_merchant');

Route::post('/quantity', 'grambunny\CheckoutController@quantity');

Route::post('/addtocart', 'grambunny\CheckoutController@addtocart');

Route::post('{slug}/checkout/billing', 'grambunny\CheckoutController@billing')->name('checkout.billing');

Route::post('checkout.validate-coupon.json', 'grambunny\CheckoutController@validateCouponCode');

Route::get("/get-categories.json", "grambunny\AjaxController@getCategories");

Route::get("/get-sub-categories.json", "grambunny\AjaxController@getSubCategories");
Route::get("/get-merchant.json", "grambunny\AjaxController@getMerchants");

Route::post("/save-user-ps-review", "grambunny\ProductsController@saveUserPsReview")->name("save-user-ps-review");

Route::post("/save-merchant-review", "grambunny\MerchantProfileController@saveMerchantReview")->name("save-merchant-review");

Route::post("/save-customer-review", "grambunny\MerchantProfileController@saveCustomerReview")->name("save-customer-review");

Route::post('/apply_coupon', 'grambunny\CheckoutController@apply_coupon')->name("apply_coupon");


// 18-11-2019

Route::post('/user_password/user_forgot_password', 'FrontController@user_sentOTP');

Route::post('/user_password/user_enter_otp', 'FrontController@VerifyUserOTP');

Route::get('/user_password/reset_your_user_password', 'FrontController@setUserNewPasswordForm');

Route::post('/user_password/reset_your_user_password', 'FrontController@setUserNewPassword');

// 18-11-2019

Route::post('/admin/custom-validation', 'AdminUserController@customvalidation');

Route::post('/custom-validation', 'FrontController@customvalidation');

Route::post('/admin/crop_product_image_save', 'AdminVendorController@crop_product_image_save');

Route::post('/admin/gallery_crop_image_save', 'AdminVendorController@gallery_crop_image_save');

Route::get('/admin/orders_show', 'AdminController@orders_show');

Route::post('/admin/chart_filter_order_date', 'AdminController@chart_filter_order_date');

Route::get('/admin/users_show', 'AdminController@users_show');

Route::post('/admin/user_chart_filter_order_date', 'AdminController@user_chart_filter_order_date');

Route::group(['middlewareGroups' => ['web']], function () {

	Route::auth();

	Route::get('/admin/general_setting', 'AdminController@showsetting');

	Route::post('/admin/update_setting', 'AdminController@update_admin_setting');

	Route::get('/admin/active_log', 'AdminController@showactivelog');

	Route::get('/admin/user_log_delete/{id}', 'AdminController@showactivelog_delete');

	Route::get('/admin/viewmenu_log', 'AdminController@showmenulog');

	Route::get('/admin/menu_log_delete/{id}', 'AdminController@showmenulog_delete');

	Route::get('/admin/order_report', 'AdminorderController@show_ordertoday');
	Route::post('/admin/filter_order', 'AdminorderController@filter_order');
	Route::post('/admin/filter_order_status', 'AdminorderController@filter_order_status');
	Route::post('/admin/filter_order_customer', 'AdminorderController@filter_order_customer');
	Route::post('/admin/filter_order_date', 'AdminorderController@filter_order_date');

	Route::post('/admin/filter_order_coupon', 'AdminorderController@filter_order_coupon');

	Route::post('/admin/filter_order_merchant', 'AdminorderController@filter_order_merchant');
	Route::get('/admin/export_in_excel', 'AdminorderController@export_in_excel');
	Route::get('/admin/export_in_excel_filter/{searchid}/{filter}', 'AdminorderController@export_in_excel_filter');

	// Route::get('/admin/mertc_flat_file/{vendor_id}/{user_id}/{id}', 'AdminorderController@mertc_flat_file');
	Route::get('/admin/mertc_flat_file', 'AdminorderController@mertc_flat_file');

	Route::get('/admin/METRC_in_excel_filter/{searchid}/{filter}', 'AdminorderController@METRC_in_excel_filter');

	Route::get('/admin/product_report', 'AdminorderController@show_product_report');

	//Route::post('/admin/product_report', 'AdminorderController@show_product_report');

	Route::post('/admin/product_filter_order', 'AdminorderController@product_filter_order');

	Route::post('/admin/product_filter_order_status', 'AdminorderController@product_filter_order_status');

	Route::post('/admin/product_filter_order_customer', 'AdminorderController@product_filter_order_customer');

	Route::post('/admin/product_filter_order_date', 'AdminorderController@product_filter_order_date');

	Route::post('/admin/product_filter_order_coupon', 'AdminorderController@product_filter_order_coupon');

	Route::post('/admin/product_filter_order_merchant', 'AdminorderController@product_filter_order_merchant');

	Route::get('/admin/product_export_in_excel', 'AdminorderController@product_export_in_excel');
	Route::get('/admin/product_export_in_excel_filter/{searchid}/{filter}', 'AdminorderController@product_export_in_excel_filter');


	Route::get('/admin/metrc_export_in_excel', 'AdminorderController@metrc_export_in_excel');
	Route::get('/admin/metrc_export_in_excel_filter/{searchid}/{filter}', 'AdminorderController@metrc_export_in_excel_filter');


	Route::get('/admin/filter_order_customer', 'AdminorderController@filter_order_customer');
	Route::get('/admin/filter_order_merchant', 'AdminorderController@filter_order_merchant');
	Route::get('/admin/filter_order_status', 'AdminorderController@filter_order_status');
	Route::get('/admin/filter_order_date', 'AdminorderController@filter_order_date');
	Route::get('/admin/filter_order_coupon', 'AdminorderController@filter_order_coupon');

	//start all order route priyanka 04/12/2021
	Route::post('/admin/filter_order_data', 'AdminorderController@filter_order_data');

	Route::get('/admin/filter_order_data', 'AdminorderController@filter_order_data');

	Route::get('/admin/allorder', 'AdminorderController@all_order_list');
	//end all order route priyanka 04/12/2021

	//Login Routes...

	Route::get('/admin', 'AdminAuth\AuthController@showLoginForm');

	Route::get('/admin/', 'AdminAuth\AuthController@showLoginForm');

	Route::get('/admin/login/', 'AdminAuth\AuthController@showLoginForm');

	Route::post('/admin/login', 'AdminAuth\AuthController@login');

	Route::get('/admin/logout', 'AdminAuth\AuthController@logout');

	Route::get('/admin/password/reset', 'AdminAuth\AuthController@forgotpwd');

	Route::get('/admin/dashboard', 'AdminController@index')->name("admin.dashboard");

	Route::get('/admin/change_password', 'AdminController@showPasswordForm');

	Route::post('/admin/update_account', 'AdminController@updateprofile_action');

	Route::post('/admin/change_password', 'AdminController@updatepassword');

	Route::post('/admin/transfer-vendor', 'AdminwalletController@transfer_vendor');

	Route::post('/admin/request-amount-form', 'AdminwalletController@request_amount_form');



	Route::get('/sub-admin', 'subadmin_controllers\SubadminController@loginform');

	Route::get('/sub-admin/login', 'subadmin_controllers\SubadminController@loginform');

	Route::post('/sub-admin/login', 'subadmin_controllers\SubadminController@login_action');

	Route::get('/sub-admin/dashboard', 'subadmin_controllers\SubadminController@index');

	Route::get('/sub-admin/logout', 'subadmin_controllers\SubadminController@logout');

	Route::get('/sub-admin/change_password', 'subadmin_controllers\SubadminController@showPasswordForm');

	Route::post('/sub-admin/update_account', 'subadmin_controllers\SubadminController@updateprofile_action');

	Route::post('/sub-admin/change_password', 'subadmin_controllers\SubadminController@updatepassword');

	Route::get('/sub-admin/general_setting', 'subadmin_controllers\SubadminController@showsetting');

	Route::post('/sub-admin/update_setting', 'subadmin_controllers\SubadminController@update_admin_setting');


	// sub-admin user module


	Route::get('/sub-admin/userlist', 'subadmin_controllers\SubadminUserController@user_list');

	Route::get('/sub-admin/user-form', 'subadmin_controllers\SubadminUserController@user_form');

	Route::get('/sub-admin/user-form/{id}', 'subadmin_controllers\SubadminUserController@user_form');

	Route::post('/sub-admin/check_user_duplicateemail', 'subadmin_controllers\SubadminUserController@user_duplicate_email');

	Route::post('/sub-admin/user_action', 'subadmin_controllers\SubadminUserController@user_action');

	Route::get('/sub-admin/user_delete/{id}', 'subadmin_controllers\SubadminUserController@user_delete');

	Route::get('/sub-admin/user_orderlist/{userid}', 'subadmin_controllers\SubadminorderController@show_orderlist');


	// sub-admin Area module


	Route::get('/sub-admin/area_list', 'subadmin_controllers\SubadminareaController@area_List');

	Route::get('/sub-admin/area-form', 'subadmin_controllers\SubadminareaController@area_form');

	Route::post('/sub-admin/area-form', 'subadmin_controllers\SubadminareaController@addarea');

	Route::get('/sub-admin/area-view/{id}', 'subadmin_controllers\SubadminareaController@ViewArea');

	Route::get('/sub-admin/area_delete/{id}', 'subadmin_controllers\SubadminareaController@area_delete');

	Route::get('/sub-admin/area-edit-form/{id}', 'subadmin_controllers\SubadminareaController@EditareaForm');

	Route::post('/sub-admin/area-edit-form', 'subadmin_controllers\SubadminareaController@UpdateAreaDetail');


	// sub-admin Vendor module



	Route::get('/sub-admin/vendorlist', 'subadmin_controllers\SubadminVendorController@vendor_list');

	Route::get('/sub-admin/vendor-form', 'subadmin_controllers\SubadminVendorController@vendor_form');

	Route::get('/sub-admin/vendor-form/{id}', 'subadmin_controllers\SubadminVendorController@vendor_form');

	Route::post('/sub-admin/check_vendor_duplicateemail', 'subadmin_controllers\SubadminVendorController@vendor_duplicate_email');

	Route::post('/sub-admin/vendor_action', 'subadmin_controllers\SubadminVendorController@vendor_action');

	Route::get('/sub-admin/vendor_delete/{id}', 'subadmin_controllers\SubadminVendorController@vendor_delete');

	Route::get('/sub-admin/vendor_profile/{id}', 'subadmin_controllers\SubadminVendorController@view_vendor');

	Route::post('/sub-admin/vendor_search', 'subadmin_controllers\SubadminVendorController@vendor_search');



	// sub-admin restaurant module



	Route::get('/sub-admin/restaurant_list', 'subadmin_controllers\SubadminresturantController@restaurant_list');

	Route::post('/sub-admin/restaurant_search', 'subadmin_controllers\SubadminresturantController@restaurant_search');

	Route::get('/sub-admin/restaurant-form', 'subadmin_controllers\SubadminresturantController@restaurant_form');

	Route::get('/sub-admin/vendor-rest-form/{vendor_id}', 'subadmin_controllers\SubadminresturantController@restaurant_form');

	Route::get('/sub-admin/restaurant-form/{id}', 'subadmin_controllers\SubadminresturantController@restaurant_form');

	Route::post('/sub-admin/restaurant_action', 'subadmin_controllers\SubadminresturantController@restaurant_action');

	Route::get('/sub-admin/restaurant_delete/{id}', 'subadmin_controllers\SubadminresturantController@restaurant_delete');

	Route::get('/sub-admin/restaurant_view/{id}', 'subadmin_controllers\SubadminResturantDetailController@view_restaurant');

	Route::get('sub-admin/menu_list', 'subadmin_controllers\SubadminresturantController@menu_list');

	Route::get('/sub-admin/menu-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_form');

	Route::post('/sub-admin/menu_action', 'subadmin_controllers\SubadminresturantController@menu_action');

	Route::get('/sub-admin/menu_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_delete');

	Route::get('sub-admin/menu_categorylist', 'subadmin_controllers\SubadminresturantController@menu_category_list');

	Route::get('/sub-admin/category-form', 'subadmin_controllers\SubadminresturantController@menu_category_form');

	Route::get('/sub-admin/category-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_category_form');

	Route::get('/sub-admin/menu_cat_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_category_delete');

	Route::post('/sub-admin/get_menulist', 'subadmin_controllers\SubadminresturantController@get_menulist');

	Route::get('sub-admin/menu_cate_item_list', 'subadmin_controllers\SubadminresturantController@menu_item_list');

	Route::get('/sub-admin/category-item-form', 'subadmin_controllers\SubadminresturantController@menu_item_form');

	Route::get('/sub-admin/category-item-form/{id}', 'subadmin_controllers\SubadminresturantController@menu_item_form');

	Route::post('/sub-admin/category_item_action', 'subadmin_controllers\SubadminresturantController@menu_item_action');

	Route::get('/sub-admin/menu_cat_item_delete/{id}', 'subadmin_controllers\SubadminresturantController@menu_item_delete');

	Route::post('/sub-admin/get_categorylist', 'subadmin_controllers\SubadminresturantController@get_categorylist');

	Route::get('/sub-admin/get_suburblist', 'subadmin_controllers\SubadminresturantController@show_suburblist');

	/* MENU ,ITEM AND ADONS CATEGORY FUNCTIONALITY WITH AJAX START */

	Route::post('/sub-admin/menu_ajax_form', 'subadmin_controllers\SubadminresturantController@ajax_menu_form');

	Route::post('/sub-admin/menu_ajax_itemform', 'subadmin_controllers\SubadminresturantController@ajax_menu_item_form');

	Route::post('/sub-admin/show_menudetail', 'subadmin_controllers\SubadminresturantController@ajax_show_menudetail');

	Route::post('/sub-admin/update_sortorder', 'subadmin_controllers\SubadminresturantController@ajax_update_sortorder');

	Route::post('/sub-admin/update_menu_item', 'subadmin_controllers\SubadminresturantController@ajax_update_menu_category');

	Route::post('/sub-admin/addons_list', 'subadmin_controllers\SubadminresturantController@ajax_addons_list');

	Route::post('/sub-admin/addons_form', 'subadmin_controllers\SubadminresturantController@ajax_addons_form');

	Route::post('/sub-admin/addon_action', 'subadmin_controllers\SubadminresturantController@ajax_addons_action');

	Route::post('/sub-admin/service_form', 'subadmin_controllers\SubadminresturantController@ajax_service_form');

	Route::post('/sub-admin/service_action', 'subadmin_controllers\SubadminresturantController@service_action');

	Route::post('/sub-admin/promo_form', 'subadmin_controllers\SubadminresturantController@ajax_promo_form');

	Route::post('/sub-admin/promo_action', 'subadmin_controllers\SubadminresturantController@promo_action');

	Route::post('/sub-admin/bank_form', 'subadmin_controllers\SubadminresturantController@ajax_bank_form');

	Route::post('/sub-admin/bank_action', 'subadmin_controllers\SubadminresturantController@bank_action');

	Route::post('/sub-admin/category_action', 'subadmin_controllers\SubadminresturantController@menu_category_action');

	Route::post('/sub-admin/show_popular', 'subadmin_controllers\SubadminresturantController@menu_category_popularlist');

	//menu template module

	Route::get('/sub-admin/template_menu_list/{id}', 'subadmin_controllers\SubadminmenutemplateController@show_menu_list');

	Route::post('/sub-admin/template_menu_ajax_form', 'subadmin_controllers\SubadminmenutemplateController@ajax_menu_form');

	Route::post('/sub-admin/template_menu_action', 'subadmin_controllers\SubadminmenutemplateController@menu_action');

	Route::post('/sub-admin/template_show_menudetail', 'subadmin_controllers\SubadminmenutemplateController@ajax_show_menudetail');

	Route::post('/sub-admin/template_update_sortorder', 'subadmin_controllers\SubadminmenutemplateController@ajax_update_sortorder');

	Route::post('/sub-admin/template_show_popular', 'subadmin_controllers\SubadminmenutemplateController@menu_category_popularlist');

	Route::post('/sub-admin/template_menu_ajax_itemform', 'subadmin_controllers\SubadminmenutemplateController@ajax_menu_item_form');

	Route::post('/sub-admin/template_update_menu_item', 'subadmin_controllers\SubadminmenutemplateController@ajax_update_menu_category');

	Route::post('/sub-admin/template_category_action', 'subadmin_controllers\SubadminmenutemplateController@menu_category_action');

	Route::post('/sub-admin/template_addons_list', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_list');

	Route::post('/sub-admin/template_addons_form', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_form');

	Route::post('/sub-admin/template_addon_action', 'subadmin_controllers\SubadminmenutemplateController@ajax_addons_action');

	Route::post('/sub-admin/menutemplate_action', 'subadmin_controllers\SubadminmenutemplateController@get_template_action');



	Route::get('/sub-admin/menu_template', 'subadmin_controllers\SubadminmenutemplateController@show_menutemplate_list');

	Route::get('/sub-admin/get_templatename', 'subadmin_controllers\SubadminmenutemplateController@get_template_form');

	Route::get('/sub-admin/get_templatename/{id}', 'subadmin_controllers\SubadminmenutemplateController@get_template_form');

	Route::post('/sub-admin/menu_template_action', 'subadmin_controllers\SubadminmenutemplateController@template_action');

	Route::get('/sub-admin/template_menu_delete/{id}', 'subadmin_controllers\SubadminmenutemplateController@template_delete');

	Route::get('/sub-admin/delete_template_menus/{id}', 'subadmin_controllers\SubadminmenutemplateController@deletetemplate_menus');


	//sub-admin order module


	Route::get('/sub-admin/orderlisting', 'subadmin_controllers\SubadminorderController@show_orderlist');

	Route::get('/sub-admin/order_view', 'subadmin_controllers\SubadminorderController@show_orderdetail');

	Route::post('/sub-admin/order_status_update', 'subadmin_controllers\SubadminorderController@update_order_action');

	Route::post('/sub-admin/order_search_list', 'subadmin_controllers\SubadminorderController@ajax_search_list');

	Route::get('/sub-admin/user_orderlist/{userid}', 'subadmin_controllers\SubadminorderController@show_orderlist');

	Route::post('/sub-admin/order_report', 'subadmin_controllers\SubadminorderController@show_orderreport');



	Route::get('/sub-admin/orderlisting', 'subadmin_controllers\SubadminorderController@show_orderlist');

	Route::get('/sub-admin/order_view', 'subadmin_controllers\SubadminorderController@show_orderdetail');

	Route::post('/sub-admin/order_status_update', 'subadmin_controllers\SubadminorderController@update_order_action');

	Route::post('/sub-admin/order_search_list', 'subadmin_controllers\SubadminorderController@ajax_search_list');

	Route::get('/sub-admin/user_orderlist/{userid}', 'subadmin_controllers\SubadminorderController@show_orderlist');

	Route::post('/sub-admin/order_report', 'subadmin_controllers\SubadminorderController@show_orderreport');


	// sub-admin review list

	/* MANAGE REVIEW AND REATING */

	Route::get('/sub-admin/review_list', 'subadmin_controllers\SubadminreviewController@show_reviewlist');

	Route::post('/sub-admin/review_update', 'subadmin_controllers\SubadminreviewController@show_review_action');

	Route::post('/sub-admin/review_search_list', 'subadmin_controllers\SubadminreviewController@ajax_search_list');

	Route::get('/sub-admin/review_delete/{id}', 'subadmin_controllers\SubadminreviewController@review_delete');


	//sub-admin payment module

	/* MANAGE PAYMENTLISTING */

	Route::get('/sub-admin/paymentlisting', 'subadmin_controllers\SubadminorderController@show_paymentlist');

	Route::get('/sub-admin/payment_view', 'subadmin_controllers\SubadminorderController@show_paymentdetail');

	Route::post('/sub-admin/payment_search_list', 'subadmin_controllers\SubadminorderController@ajax_show_paymentlist');

	Route::post('/sub-admin/payment_report', 'subadmin_controllers\SubadminorderController@show_paytmentreport');

	Route::post('/sub-admin/payment_status_update', 'subadmin_controllers\SubadminorderController@update_payment_action');


	//sub-admin content management

	/* MANAGE CONTENT PAGES */

	Route::get('/sub-admin/content_pagelist', 'subadmin_controllers\SubadminPagesController@page_listing');

	Route::get('/sub-admin/page-form/{id}', 'subadmin_controllers\SubadminPagesController@page_form');

	Route::post('/sub-admin/page_action', 'subadmin_controllers\SubadminPagesController@page_action');

	Route::get('/sub-admin/socail_pagelist', 'subadmin_controllers\SubadminPagesController@social_listing');

	Route::get('/sub-admin/social-form/{id}', 'subadmin_controllers\SubadminPagesController@show_socila_form');

	Route::post('/sub-admin/social_action', 'subadmin_controllers\SubadminPagesController@get_social_action');


	Route::get('/sub-admin/testimonial_list', 'subadmin_controllers\SubadminPagesController@get_testiminial');

	Route::get('/sub-admin/testimonial-form', 'subadmin_controllers\SubadminPagesController@get_testiminial_form');

	Route::get('/sub-admin/testimonial-form/{id}', 'subadmin_controllers\SubadminPagesController@get_testiminial_form');

	Route::post('/sub-admin/testimonial_action', 'subadmin_controllers\SubadminPagesController@get_testiminial_action');

	Route::get('/sub-admin/testimonial_delete/{id}', 'subadmin_controllers\SubadminPagesController@testimonial_delete');

	Route::get('/sub-admin/homepage_content', 'subadmin_controllers\SubadminPagesController@home_content');

	Route::get('/sub-admin/content-form/{id}', 'subadmin_controllers\SubadminPagesController@show_content_form');

	Route::post('/sub-admin/homepage_action', 'subadmin_controllers\SubadminPagesController@homepage_action');



	// sub-admin send notification module



	Route::get('/sub-admin/send_notification', 'subadmin_controllers\SubadminPagesController@show_send_notification');

	Route::post('/sub-admin/send_notification_action', 'subadmin_controllers\SubadminPagesController@notification_action');



	// sub-admin email module



	Route::get('/sub-admin/email_content', 'subadmin_controllers\SubadminPagesController@email_content');

	Route::get('/sub-admin/email_content_form/{id}', 'subadmin_controllers\SubadminPagesController@show_email_form');

	Route::post('/sub-admin/email_content_action', 'subadmin_controllers\SubadminPagesController@email_content_action');



	// sub-admin delivery man management module



	/*Delivery man management start*/

	Route::get('/sub-admin/delivery_manlist', 'subadmin_controllers\SubadmindeliverymanController@user_list');

	Route::get('/sub-admin/delivery_man-form', 'subadmin_controllers\SubadmindeliverymanController@user_form');

	Route::post('/sub-admin/delivery_man-form', 'subadmin_controllers\SubadmindeliverymanController@user_action');

	Route::get('/sub-admin/deliveryman_delete/{id}', 'subadmin_controllers\SubadmindeliverymanController@Deliveryman_delete');

	Route::get('/sub-admin/deliveryman-edit-form/{id}', 'subadmin_controllers\SubadmindeliverymanController@user_form');

	Route::post('/sub-admin/deliveryman-edit-form', 'subadmin_controllers\SubadmindeliverymanController@UpdateSubAdminDetail');

	Route::get('/sub-admin/deliveryman-view/{id}', 'subadmin_controllers\SubadmindeliverymanController@ViewDeliveryman');

	Route::post('/sub-admin/check_deliveryman_duplicateemail', 'subadmin_controllers\SubadmindeliverymanController@deliveryman_duplicate_email');

	/*Deliver man management end*/

	/*SUB-ADMIN END*/

	// Admin Permissions module


	Route::get('/admin/permissions_list', 'AdminPermissionsController@permission_list');

	Route::post('/admin/permission-form', 'AdminPermissionsController@permission_action');


	/*My routs start*/

	Route::get('/admin/password/reset_password', 'AdminAuth\AuthController@forgetPassword');

	Route::post('/admin/password/reset_password', 'AdminAuth\AuthController@sentOTP');

	Route::get('/admin/password/enter_otp', 'AdminAuth\AuthController@VerifyOTPForm');

	Route::post('/admin/password/enter_otp', 'AdminAuth\AuthController@VerifyOTP');

	Route::get('/admin/password/reset_your_password', 'AdminAuth\AuthController@setNewPasswordForm');

	Route::post('/admin/password/reset_your_password', 'AdminAuth\AuthController@setNewPassword');

	/*End*/

	/* MANAGE USER DETAIL */

	Route::get('/admin/subadmin_list', 'AdminsubController@user_list');

	Route::get('/admin/subadmin-form', 'AdminsubController@user_form');

	/*Subadmin start*/

	Route::post('/admin/subadmin-form', 'AdminsubController@subAdmin_form');

	Route::get('/admin/subadmin_delete/{id}', 'AdminsubController@subadmin_delete');

	Route::get('/admin/subadmin-edit-form/{id}', 'AdminsubController@EditsubadminForm');

	Route::post('/admin/subadmin-edit-form', 'AdminsubController@UpdateSubAdminDetail');

	Route::get('/admin/subadmin-view/{id}', 'AdminsubController@ViewSubadmin');

	Route::post('/admin/check_suadmin_duplicateemail', 'AdminsubController@sub_adminduplicate_email');

	/*Subadmin end*/

	/*Area management start*/

	Route::get('/admin/area_list', 'AdminareaController@area_List');

	Route::get('/admin/area-form', 'AdminareaController@area_form');

	Route::post('/admin/area-form', 'AdminareaController@addarea');

	Route::get('/admin/area-view/{id}', 'AdminareaController@ViewArea');

	Route::get('/admin/area_delete/{id}', 'AdminareaController@area_delete');

	Route::get('/admin/area-edit-form/{id}', 'AdminareaController@EditareaForm');

	Route::post('/admin/area-edit-form', 'AdminareaController@UpdateAreaDetail');

	/*Area management end*/



	/* Coupon code start */



	Route::get('/admin/coupon_code', 'AdminareaController@coupon_code');

	Route::get('/admin/coupon-form', 'AdminareaController@coupon_form');

	Route::post('/admin/coupon-form', 'AdminareaController@add_coupon_code');

	Route::get('/admin/coupon_code_delete/{id}', 'AdminareaController@coupon_delete');

	Route::get('/admin/coupon-edit-form/{id}', 'AdminareaController@coupon_edit_form');

	Route::post('/admin/coupon-edit-form', 'AdminareaController@update_coupon_detail');



	/* Coupon code end */



	Route::get('/admin/subadmin-form/{id}', 'AdminsubController@user_form');

	Route::post('/admin/check_subadmin_duplicateemail', 'AdminsubController@user_duplicate_email');

	Route::post('/admin/subadmin_action', 'AdminsubController@user_action');

	/* MANAGE USER DETAIL */

	Route::get('/admin/userlist', 'AdminUserController@user_list');

	Route::get('/admin/abandoned-cart', 'AdminUserController@abandoned_cart');

	Route::get('/admin/abandoned-cart-view/{id}', 'AdminUserController@abandoned_cart_view');

	Route::get('/admin/customer_export_in_excel', 'AdminUserController@export_in_excel');

	Route::get('/admin/adminlist', 'AdminUserController@admin_list');

	Route::get('/admin/admin-form', 'AdminUserController@admin_form');

	Route::get('/admin/admin-form/{id}', 'AdminUserController@admin_form');

	Route::post('/admin/admin_action', 'AdminUserController@admin_action');

	Route::get('/admin/admin_delete/{id}', 'AdminUserController@admin_delete');

	Route::get('/admin/user-form', 'AdminUserController@user_form');

	Route::get('/admin/user-form/{id}', 'AdminUserController@user_form');

	Route::post('/admin/crop_profile_image_save', 'AdminUserController@crop_profile_image_save');

	Route::post('/admin/check_user_duplicateemail', 'AdminUserController@user_duplicate_email');

	Route::post('/admin/user_action', 'AdminUserController@user_action');

	Route::get('/admin/user_delete/{id}', 'AdminUserController@user_delete');



	/*Delivery man management start*/

	Route::get('/admin/delivery_manlist', 'AdmindeliverymanController@user_list');

	Route::get('/admin/delivery_man-form', 'AdmindeliverymanController@user_form');

	Route::post('/admin/delivery_man-form', 'AdmindeliverymanController@user_action');

	Route::get('/admin/deliveryman-edit-form/{id}', 'AdmindeliverymanController@user_form');

	Route::get('/admin/deliveryman-view/{id}', 'AdmindeliverymanController@ViewDeliveryman');

	Route::post('/admin/check_deliveryman_duplicateemail', 'AdmindeliverymanController@deliveryman_duplicate_email');

	Route::get('/admin/deliveryman_delete/{id}', 'AdmindeliverymanController@Deliveryman_delete');



	/* MANAGE VENDOR DETAIL */

	Route::get('/admin/merchant-list', 'AdminVendorController@merchant_list');

	Route::get('/admin/merchant_export_in_excel', 'AdminVendorController@export_in_excel');

	Route::get('/admin/vendor_fgt_pass_req_list', 'AdminVendorController@vendor_pass_req_list');

	Route::get('/admin/merchant-form', 'AdminVendorController@merchant_form');

	Route::get('/admin/merchant-form-view/{id}', 'AdminVendorController@merchant_form_view');

	Route::get('/admin/merchant-form/{id}', 'AdminVendorController@merchant_form');

	Route::post('/admin/check_vendor_duplicateemail', 'AdminVendorController@vendor_duplicate_email');

	Route::post('/admin/merchant_action', 'AdminVendorController@merchant_action');

	Route::post('/admin/crop_image_save', 'AdminVendorController@crop_image_save');

	Route::get('/admin/merchant_delete/{id}', 'AdminVendorController@merchant_delete');

	Route::get('/admin/merchant-profile/{id}', 'AdminVendorController@merchant_view');

	Route::get('/admin/coupan-list', 'AdminVendorController@coupon_code');

	Route::get('/admin/coupon-add', 'AdminVendorController@coupon_form');

	Route::post('/admin/coupon-add', 'AdminVendorController@add_coupon_code');

	Route::get('/admin/coupon_deletes/{id}', 'AdminVendorController@coupon_delete');

	Route::get('/admin/coupon-edit/{id}', 'AdminVendorController@coupon_edit_form');

	Route::post('/admin/coupon-edit', 'AdminVendorController@update_coupon_detail');



	Route::post('/admin/vendor_search', 'AdminVendorController@vendor_search');

	/* Vendor Category */

	Route::get('/admin/product_service_unit', 'AdminVendorController@product_service_unit');

	Route::get('/admin/event_service_unit', 'AdminVendorController@event_service_unit');

	Route::get('/admin/ps_unit_form', 'AdminVendorController@ps_unit_form');

	Route::get('/admin/event_unit_form', 'AdminVendorController@event_unit_form');

	Route::get('/admin/unit_delete/{id}', 'AdminVendorController@unit_delete');

	Route::get('/admin/event_unit_delete/{id}', 'AdminVendorController@event_unit_delete');

	Route::post('/admin/ps_unit_action', 'AdminVendorController@ps_unit_action');

	Route::post('/admin/event_unit_action', 'AdminVendorController@event_unit_action');
	Route::get('/admin/product_brands', 'AdminVendorController@product_brands');
	Route::get('/admin/product_brand_form', 'AdminVendorController@product_brand_form');
	Route::get('/admin/product_brand_delete/{id}', 'AdminVendorController@product_brand_delete');
	Route::post('/admin/product_brand_action', 'AdminVendorController@product_brand_action');

	Route::get('/admin/product_types', 'AdminVendorController@product_types');
	Route::get('/admin/product_type_form', 'AdminVendorController@product_type_form');
	Route::get('/admin/product_type_delete/{id}', 'AdminVendorController@product_type_delete');
	Route::post('/admin/product_type_action', 'AdminVendorController@product_type_action');


	Route::get('/admin/product_service_category', 'AdminVendorController@proservices_cate_list')->name("category.view");

	Route::get('/admin/product_service_category_form', 'AdminVendorController@proservices_cate_form');

	Route::get('/admin/product_service_category_form/{id}', 'AdminVendorController@proservices_cate_form');

	Route::get('/admin/product_service_category_delete/{id}', 'AdminVendorController@proservices_cate_delete');

	Route::post('/admin/product_service_cate_action', 'AdminVendorController@proservices_cate_action');



	Route::post('/admin/category', 'AdminVendorController@category_list');

	Route::post('/admin/subcategory', 'AdminVendorController@subcategory_list');

	Route::post('/admin/subcategory_add', 'AdminVendorController@subcategory_list_add');

	Route::post('/admin/sub-category', 'AdminVendorController@subcategorylist');

	Route::post('/merchant/category', 'RestOwner\OwnerController@category_list');

	Route::get('/merchant/payment-setting', 'RestOwner\OwnerController@payment_setting');

	//Route::post('/merchant/subcategory','RestOwner\OwnerController@subcategory_list');

	Route::post('/merchant/payment-setup', 'RestOwner\OwnerController@payment_setup');

	Route::get('/admin/product_service_sub_category/{category}', 'AdminVendorController@proservices_sub_cate_list')->name("subcategory.view");

	Route::get('/admin/product_service_sub_category_form/{id}', 'AdminVendorController@proservices_sub_cate_form');

	Route::get('/admin/product_service_sub_category_edit/{id}', 'AdminVendorController@proservices_sub_cate_edit');

	Route::get('/admin/product_service_sub_category_delete/{id}', 'AdminVendorController@proservices_sub_cate_delete');

	Route::post('/admin/product_service_sub_cate_action', 'AdminVendorController@proservices_sub_cate_action');



	Route::post('/admin/product_service_sub_cate_edit_action', 'AdminVendorController@proservices_sub_cate_edit_action');


	/* Manage product service */

	Route::get('/admin/product-service-merchant-list', 'AdminVendorController@product_service_merchant_list');

	Route::get('/admin/product-merchant-list', 'AdminVendorController@product_merchant_list');

	Route::get('/admin/coupan-merchant-list', 'AdminVendorController@coupan_merchant_list');

	Route::get('/admin/merchant-product-service-list/{id}', 'AdminVendorController@merchant_product_service_list');

	Route::get('/admin/merchant-coupan-service-list/{id}', 'AdminVendorController@merchant_coupan_service_list');

	Route::get('/admin/merchant-product-inventory-list/{id}', 'AdminVendorController@merchant_product_inventory_list');

	Route::post('/admin/inventory-product-action', 'AdminVendorController@inventory_product_action');

	Route::get('/admin/merchant-inventory-copy/{id}', 'AdminVendorController@merchant_inventory_copy');
	Route::post('/admin/add-vendor-product-action', 'AdminVendorController@vendor_product_action');

	Route::get('/admin/all-product-list', 'AdminVendorController@all_product_list');
	Route::get('/admin/my-product-list', 'AdminVendorController@my_product_list');
	Route::get('/admin/merchant-product-list', 'AdminVendorController@merchant_product_list');

	Route::get('/admin/merchant-product-service-form/{id}', 'AdminVendorController@merchant_product_service_form');
	Route::get('/admin/merchant-product-service-edit/{id}', 'AdminVendorController@merchant_product_service_edit');
	Route::get('/admin/merchant-product-service-delete/{id}', 'AdminVendorController@merchant_product_service_delete');
	Route::post('/admin/merchant-product-service-action', 'AdminVendorController@merchant_product_service_action');


	Route::get('/admin/product-form', 'AdminVendorController@product_form');
	Route::get('/admin/product-edit/{id}', 'AdminVendorController@product_edit');
	Route::get('/admin/product-delete/{id}', 'AdminVendorController@product_delete');
	Route::post('/admin/product-action', 'AdminVendorController@product_action');

	/* admin view of merchant events start*/

	Route::get('/admin/event-management-list', 'AdminVendorController@event_management_list');
	Route::get('/admin/event-management-add', 'AdminVendorController@event_management_add');
	Route::get('/admin/event-management-edit/{id}', 'AdminVendorController@event_management_edit');
	Route::get('/admin/event-management-delete/{id}', 'AdminVendorController@event_management_delete');
	Route::post('/admin/event-management-action', 'AdminVendorController@event_management_action');
	Route::get('/admin/event-management-gallery-delete/{id}', 'AdminVendorController@event_management_gallery_delete');


	Route::get('/admin/event-merchant-list', 'AdminVendorController@event_merchant_list');
	Route::get('/admin/event-product-service-list/{id}', 'AdminVendorController@event_product_service_list');
	Route::get('/admin/event-product-service-list/', 'AdminVendorController@event_product_service_list');
	Route::get('/admin/event-product-service-form/{id}', 'AdminVendorController@event_product_service_form');
	Route::get('/admin/event-product-service-edit/{id}', 'AdminVendorController@event_product_service_edit');
	Route::get('/admin/event-product-service-delete/{id}', 'AdminVendorController@event_product_service_delete');
	Route::post('/admin/event-product-service-action', 'AdminVendorController@event_product_service_action');
	Route::get('/admin/event-product-service-gallery-delete/{id}', 'AdminVendorController@event_product_service_gallery_delete');

	Route::post('/admin/crop_event_image_save', 'AdminVendorController@crop_event_image_save');

	Route::post('/admin/gallery_event_crop_image_save', 'AdminVendorController@gallery_event_crop_image_save');

	Route::get('/admin/product_event_category', 'AdminVendorController@event_cate_list')->name("category.view");

	Route::get('/admin/event_category_form', 'AdminVendorController@event_cate_form');

	Route::get('/admin/event_category_form/{id}', 'AdminVendorController@event_cate_form');

	Route::post('/admin/event_cate_action', 'AdminVendorController@event_cate_action');

	Route::get('/admin/event_category_delete/{id}', 'AdminVendorController@event_cate_delete');

	Route::get('/admin/event_sub_category/{category}', 'AdminVendorController@events_sub_cate_list')->name("event_subcategory.view");

	Route::get('/admin/event_sub_category_form/{id}', 'AdminVendorController@events_sub_cate_form');

	Route::get('/admin/event_sub_category_edit/{id}', 'AdminVendorController@events_sub_cate_edit');

	Route::post('/admin/event_sub_cate_action', 'AdminVendorController@events_sub_cate_action');

	Route::post('/admin/event_sub_cate_edit_action', 'AdminVendorController@events_sub_cate_edit_action');

	Route::get('/admin/event_sub_category_delete/{id}', 'AdminVendorController@events_sub_cate_delete');

	/* admin view of merchant events end*/


	Route::get('/admin/merchant-logged-status', 'AdminVendorController@merchant_logged_status');
	Route::get('/admin/merchant-logged-status-change/{id}', 'AdminVendorController@merchant_logged_status_change');

	Route::get('/admin/banner-image', 'AdminVendorController@banner_image');
	Route::post('/admin/banner-image-change', 'AdminVendorController@banner_image_change');

	Route::any('/admin/delete_multi_image', 'AdminVendorController@delete_multi_image');

	Route::get('/admin/product-gallery-delete/{id}', 'AdminVendorController@product_gallery_delete');
	Route::get('/admin/merchant_pic_delete/{picno}/{mid}', 'AdminVendorController@merchant_photo_delete');
	Route::get('/merchant/merchant_pic_delete/{picno}/{mid}', 'RestOwner\OwnerrestaurantController@merchant_photo_delete');

	/* End product and services */



	/* MANAGE CUISINE DETAIL */

	Route::get('/admin/cuisinelist', 'AdminCuisineController@cuisine_list');

	Route::get('/admin/cuisine-form', 'AdminCuisineController@cuisine_form');

	Route::get('/admin/cuisine-form/{id}', 'AdminCuisineController@cuisine_form');

	Route::post('/admin/cuisine_action', 'AdminCuisineController@cuisine_action');

	Route::get('/admin/cuisine_delete/{id}', 'AdminCuisineController@cuisine_delete');

	/* MANAGE GOOGLE REATAUANRT DATA BASED ON CUISINE DETAIL */

	Route::get('/admin/view_google_form', 'AdminCuisineController@show_google_form');

	Route::post('/admin/get_google_data', 'AdminCuisineController@get_form_data');

	/* MANAGE CONTENT PAGES */

	Route::get('/admin/content_pagelist', 'AdminPagesController@page_listing');

	Route::get('/admin/page-form', 'AdminPagesController@page_form');

	Route::get('/admin/page_delete/{id}', 'AdminPagesController@page_delete');

	Route::get('/admin/page-form/{id}', 'AdminPagesController@page_form');

	Route::post('/admin/page_action', 'AdminPagesController@page_action');

	/* faq management */

	Route::get('/admin/faq', 'AdminPagesController@faq_listing');

	Route::get('/admin/faq-form', 'AdminPagesController@faq_form');

	Route::get('/admin/faq_delete/{id}', 'AdminPagesController@faq_delete');

	Route::get('/admin/faq-form/{id}', 'AdminPagesController@faq_form');

	Route::post('/admin/faq_action', 'AdminPagesController@faq_action');


	Route::get('/admin/socail_pagelist', 'AdminPagesController@social_listing');

	Route::get('/admin/social-form/{id}', 'AdminPagesController@show_socila_form');

	Route::post('/admin/social_action', 'AdminPagesController@get_social_action');

	/* MANAGE RESTAURANT*/

	//Route::get('/admin/product-service-management', 'AdminresturantsController@product_service_management');

	Route::post('/admin/restaurant_search', 'AdminResturantDetailController@restaurant_search');

	Route::get('/admin/restaurant-form', 'AdminresturantsController@restaurant_form');

	Route::get('/admin/vendor-rest-form/{vendor_id}', 'AdminresturantsController@restaurant_form');

	Route::get('/admin/restaurant-form/{id}', 'AdminresturantsController@restaurant_form');

	Route::post('/admin/restaurant_action', 'AdminresturantsController@restaurant_action');

	Route::get('/admin/restaurant_delete/{id}', 'AdminresturantsController@restaurant_delete');

	Route::get('/admin/restaurant_view/{id}', 'AdminResturantDetailController@view_restaurant');

	Route::get('admin/menu_list', 'AdminresturantsController@menu_list');

	Route::get('/admin/menu-form/{id}', 'AdminresturantsController@menu_form');

	Route::post('/admin/menu_action', 'AdminresturantsController@menu_action');

	Route::get('/admin/menu_delete/{id}', 'AdminresturantsController@menu_delete');

	Route::get('admin/menu_categorylist', 'AdminresturantsController@menu_category_list');

	Route::get('/admin/category-form', 'AdminresturantsController@menu_category_form');

	Route::get('/admin/category-form/{id}', 'AdminresturantsController@menu_category_form');

	Route::get('/admin/menu_cat_delete/{id}', 'AdminresturantsController@menu_category_delete');

	Route::post('/admin/get_menulist', 'AdminresturantsController@get_menulist');

	Route::get('admin/menu_cate_item_list', 'AdminresturantsController@menu_item_list');

	Route::get('/admin/category-item-form', 'AdminresturantsController@menu_item_form');

	Route::get('/admin/category-item-form/{id}', 'AdminresturantsController@menu_item_form');

	Route::post('/admin/category_item_action', 'AdminresturantsController@menu_item_action');

	Route::get('/admin/menu_cat_item_delete/{id}', 'AdminresturantsController@menu_item_delete');

	Route::post('/admin/get_categorylist', 'AdminresturantsController@get_categorylist');

	/*LOCATION */

	Route::get('/admin/get_suburblist', 'AdminresturantsController@show_suburblist');

	/* MENU ,ITEM AND ADONS CATEGORY FUNCTIONALITY WITH AJAX START */

	Route::post('/admin/menu_ajax_form', 'AdminresturantsController@ajax_menu_form');

	Route::post('/admin/menu_ajax_itemform', 'AdminresturantsController@ajax_menu_item_form');

	Route::post('/admin/show_menudetail', 'AdminresturantsController@ajax_show_menudetail');

	Route::post('/admin/update_sortorder', 'AdminresturantsController@ajax_update_sortorder');

	Route::post('/admin/update_menu_item', 'AdminresturantsController@ajax_update_menu_category');

	Route::post('/admin/addons_list', 'AdminresturantsController@ajax_addons_list');

	Route::post('/admin/addons_form', 'AdminresturantsController@ajax_addons_form');

	Route::post('/admin/addon_action', 'AdminresturantsController@ajax_addons_action');

	Route::post('/admin/service_form', 'AdminresturantsController@ajax_service_form');

	Route::post('/admin/service_action', 'AdminresturantsController@service_action');

	Route::post('/admin/promo_form', 'AdminresturantsController@ajax_promo_form');

	Route::post('/admin/promo_action', 'AdminresturantsController@promo_action');

	Route::post('/admin/bank_form', 'AdminresturantsController@ajax_bank_form');

	Route::post('/admin/bank_action', 'AdminresturantsController@bank_action');

	Route::post('/admin/category_action', 'AdminresturantsController@menu_category_action');

	Route::post('/admin/show_popular', 'AdminresturantsController@menu_category_popularlist');

	/* END */

	/* MANAGE ORDERLISTING */

	Route::get('/admin/merchant-order', 'AdminorderController@merchant_order');

	Route::get('/admin/merchant-order/{id}', 'AdminorderController@merchant_order');

	Route::get('/admin/order-list/{id}', 'AdminorderController@show_orderlist');

	Route::get('/admin/order-list/{id}/{sid}', 'AdminorderController@show_orderlist');

	Route::get('/admin/customer-order-list/{id}', 'AdminorderController@customer_orderlist');

	Route::get('/admin/customer-event-list/{id}', 'AdminorderController@event_orderlist');

	Route::get('/admin/order-details/{id}', 'AdminorderController@order_details');

	Route::get('/admin/event-list/{id}', 'AdminorderController@show_eventlist');

	Route::post('/admin/status-update', 'AdminorderController@update_order_action1');

	//Route::get('/admin/order_view','AdminorderController@show_orderdetail');

	Route::get('/admin/reporting-list', 'AdminorderController@show_reporting_list');

	Route::get('/admin/reporting-list/{id}', 'AdminorderController@show_reporting_list');

	Route::get('/admin/reporting-details/{id}', 'AdminorderController@order_details');

	Route::get('/admin/payment-list/', 'AdminorderController@payment_list');

	Route::get('/admin/payment-list/{id}/', 'AdminorderController@payment_list');

	Route::get('/admin/payment-details/{id}', 'AdminorderController@payment_details');

	Route::get('/admin/payment-setting', 'AdminorderController@payment_setting');

	Route::post('/admin/payment-setup', 'AdminorderController@payment_setup');

	//Review List

	Route::get('/admin/review-list/', 'AdminreviewController@show_reviewlist');



	//membership management

	Route::get('/admin/membership-management/', 'AdminorderController@membership_management');

	Route::get('/admin/membership-management/{id}/', 'AdminorderController@membership_management');

	Route::get('/admin/membership-details/{id}', 'AdminorderController@membership_details');

	Route::get('/admin/membership-update/{id}', 'AdminorderController@membership_update');

	Route::post('/admin/membership-update', 'AdminorderController@membership_action');



	//payment history

	Route::get('/admin/merchant-bank-list/', 'AdminorderController@bank_list');

	Route::get('/admin/merchant-bank-list/{id}/', 'AdminorderController@bank_list');

	Route::get('/admin/merchant-bank-details/{id}', 'AdminorderController@bank_details');

	//bank details

	Route::post('/admin/order_status_update', 'AdminorderController@update_order_action');

	Route::post('/admin/assign_order', 'AdminorderController@assign_order_action');

	Route::post('/admin/order_search_list', 'AdminorderController@ajax_search_list');

	Route::get('/admin/user_orderlist/{userid}', 'AdminorderController@show_orderlist');

	Route::post('/admin/order_report', 'AdminorderController@show_orderreport');

	/* MANAGE PAYMENTLISTING */

	Route::get('/admin/paymentlisting', 'AdminorderController@show_paymentlist');

	Route::get('/admin/payment_view', 'AdminorderController@show_paymentdetail');

	Route::post('/admin/payment_search_list', 'AdminorderController@ajax_show_paymentlist');

	Route::post('/admin/payment_report', 'AdminorderController@show_paytmentreport');

	Route::post('/admin/payment_status_update', 'AdminorderController@update_payment_action');



	/* MANAGE REVIEW AND REATING */

	Route::get('/admin/review_list', 'AdminreviewController@show_reviewlist');

	Route::post('/admin/review_update/{id}', 'AdminreviewController@show_review_action');

	Route::post('/admin/review_search_list', 'AdminreviewController@ajax_search_list');

	Route::get('/admin/review_delete/{id}', 'AdminreviewController@review_delete');


	/* MANAGE Merchant REVIEW AND REATING */

	Route::get('/admin/review-merchant-list', 'AdminreviewController@show_reviewmerchantlist');

	Route::post('/admin/review_merchant_update/{id}', 'AdminreviewController@show_review_merchant_action');

	Route::get('/admin/review_merchant_delete/{id}', 'AdminreviewController@review_merchant_delete');

	/* MANAGE Customer REVIEW AND REATING */

	Route::get('/admin/review-customer-list', 'AdminreviewController@show_review_customer_list');

	Route::post('/admin/review_customer_update/{id}', 'AdminreviewController@show_review_customer_action');
	Route::get('/admin/review_customer_delete/{id}', 'AdminreviewController@review_customer_delete');

	/* FRONT USER AFTER LOGIN SHOW USER ACCOUNT*/

	Route::get('/myaccount', 'UserController@dashboard')->name("userAccount");

	Route::get('/update_account', 'UserController@update_account_form')->name("userUpdateAccount");

	Route::post('/crop_profile_image', 'UserController@crop_profile_image');

	Route::get('/update_password', 'UserController@change_password')->name("userUpdatePassword");

	Route::post('/update_account_action', 'UserController@updateaccount_action');

	Route::post('/password_action', 'UserController@updatepassword_action');

	Route::get('/userlogout', 'FrontController@logout');



	//Route::get('/usermail', 'FrontController@sendmail');



	Route::get('/myorder', 'UserController@order_listing');

	Route::get('/orderreview/{orderid}', 'UserController@show_reviewfrm');

	Route::post('/order_review_action', 'UserController@orderreview_action');

	Route::post('/show_order_detail', 'UserController@get_order_detail');

	Route::get('/redirect/{service}', 'SocialAuthController@redirect');

	Route::get('/callback/{service}', 'SocialAuthController@callback');



	Route::get('/payment_history', 'UserController@payment_history');

	Route::get('/my_wallet', 'UserController@my_wallet');

	Route::post('/add_wallet', 'UserController@add_wallet');

	Route::post('/payment_wallet', 'UserController@payment_wallet');

	Route::post('/payment_wallet_paypal', 'UserController@payment_wallet_paypal');



	Route::post('/ws/add_money_wallet', 'WsController@add_money_wallet');

	Route::post('/ws/my_wallet', 'WsController@my_wallet');

	Route::post('/ws/coupon_code_apply', 'WsController@coupon_code_apply_api');

	Route::post('/ws/coupon_code_remove', 'WsController@coupon_code_remove');



	// Route::get('auth/facebook', 'SocialAuthController@redirect')->name('facebook.login');

	// Route::get('auth/facebook/callback', 'SocialAuthController@callback');



	// Route::get('auth/google', 'SocialAuthController@redirectToGoogle')->name('google.login');

	// Route::get('auth/google/callback', 'SocialAuthController@handleGoogleCallback');

	// Route::get('google-user', 'SocialAuthController@listGoogleUser');



	Route::get('/admin/transport_list', 'AdmintransportController@show_transportlist');

	Route::post('/admin/transport_search', 'AdmintransportController@show_result');

	Route::get('/admin/transport-form/{vendor_id}', 'AdmintransportController@show_transportfrm');

	Route::post('/admin/transport_action', 'AdmintransportController@transport_action');

	Route::get('/admin/transport_view/{transport_id}', 'AdmintransportController@show_transportdetail');

	Route::get('/admin/transport-update/{transport_id}', 'AdmintransportController@show_transportfrm');

	Route::post('/admin/transport_service_form', 'AdmintransportController@ajax_service_form');

	Route::post('/admin/transport_service_action', 'AdmintransportController@service_action');

	Route::post('/admin/transport_promo_form', 'AdmintransportController@ajax_promo_form');

	Route::post('/admin/transport_promo_action', 'AdmintransportController@promo_action');

	Route::post('/admin/transport_bank_form', 'AdmintransportController@ajax_bank_form');

	Route::post('/admin/transport_bank_action', 'AdmintransportController@bank_action');

	Route::get('/admin/vehicle_category_list', 'AdminvehicleController@category_list');

	Route::get('/admin/vehicle_category-form', 'AdminvehicleController@category_form');

	Route::get('/admin/vehicle_category-form/{id}', 'AdminvehicleController@category_form');

	Route::post('/admin/vehicle_category_action', 'AdminvehicleController@category_action');

	Route::get('/admin/vehicle_class_list', 'AdminvehicleController@class_list');

	Route::get('/admin/vehicle_class-form', 'AdminvehicleController@class_form');

	Route::get('/admin/vehicle_class-form/{id}', 'AdminvehicleController@class_form');

	Route::post('/admin/vehicle_class_action', 'AdminvehicleController@class_action');

	Route::post('/admin/vehicle_ajax_form', 'AdmintransportController@vehicle_form');

	Route::post('/admin/vehicle_action', 'AdmintransportController@vehicle_action');

	Route::post('/admin/findmodel', 'AdmintransportController@find_model');

	Route::post('/admin/show_vehicledetail', 'AdmintransportController@ajax_vehicle_detail');

	Route::post('/admin/update_vehicle_sortorder', 'AdmintransportController@ajax_update_sortorder');

	Route::post('/admin/vehicle_charge_form', 'AdmintransportController@ajax_vehicle_charge_form');

	Route::post('/admin/vehicle_charge_action', 'AdmintransportController@vehicle_charge_action');

	Route::post('/admin/update_vehicle_images', 'AdmintransportController@vehicle_image_update');

	Route::post('/admin/vehicle_addons_list', 'AdmintransportController@ajax_addons_list');

	Route::post('/admin/vehicle_addons_form', 'AdmintransportController@ajax_addons_form');

	Route::post('/admin/vehicle_addon_action', 'AdmintransportController@ajax_addons_action');

	/* MANAGE ORDERLISTING */

	Route::get('/admin/transport_order', 'AdmintransportorderController@show_orderlist');

	Route::get('/admin/transport_order_view', 'AdmintransportorderController@show_orderdetail');

	Route::post('/admin/transport_order_status_update', 'AdmintransportorderController@update_order_action');

	Route::post('/admin/transport_order_search_list', 'AdmintransportorderController@ajax_search_list');

	Route::post('/admin/transport_order_report', 'AdmintransportorderController@show_orderreport');



	/* MANAGE PAYMENTLISTING */

	Route::get('/admin/transport_payment', 'AdmintransportorderController@show_paymentlist');

	Route::get('/admin/transport_payment_view', 'AdmintransportorderController@show_paymentdetail');

	Route::post('/admin/transport_payment_search_list', 'AdmintransportorderController@ajax_show_paymentlist');

	Route::post('/admin/transport_payment_report', 'AdmintransportorderController@show_paytmentreport');

	Route::post('/admin/transport_payment_status_update', 'AdmintransportorderController@update_payment_action');

	Route::get('/admin/transport_review_list', 'AdmintransportorderController@show_reviewlist');

	Route::post('/admin/transport_review_search_list', 'AdmintransportorderController@ajax_review_search_list');

	Route::post('/admin/transport_review_update', 'AdmintransportorderController@show_review_action');

	Route::get('/mytransportorder', 'UserController@transport_order_listing');

	//Route::get('/orderreview/{orderid}','UserController@show_reviewfrm');

	Route::post('/transport_order_review_action', 'UserController@transport_orderreview_action');

	/********************  ADMIN ROUTES FOR REST APP PROJECT  ***********************/

	Route::get('/admin/testimonial_list', 'AdminPagesController@get_testiminial');

	Route::get('/admin/testimonial-form', 'AdminPagesController@get_testiminial_form');

	Route::get('/admin/testimonial-form/{id}', 'AdminPagesController@get_testiminial_form');

	Route::post('/admin/testimonial_action', 'AdminPagesController@get_testiminial_action');

	Route::get('/admin/testimonial_delete/{id}', 'AdminPagesController@testimonial_delete');


	Route::get('/admin/advertisement-list', 'AdminPagesController@get_advertisement');

	Route::get('/admin/advertisement-form', 'AdminPagesController@get_advertisement_form');

	Route::get('/admin/advertisement-form/{id}', 'AdminPagesController@get_advertisement_form');

	Route::post('/admin/advertisement-action', 'AdminPagesController@get_advertisement_action');

	Route::get('/admin/advertisement-delete/{id}', 'AdminPagesController@advertisement_delete');


	Route::get('/admin/homepage_content', 'AdminPagesController@home_content');

	Route::get('/admin/content-form/{id}', 'AdminPagesController@show_content_form');

	Route::post('/admin/homepage_action', 'AdminPagesController@homepage_action');

	Route::get('/admin/coupon_request', 'AdminPagesController@show_coupon_request');

	Route::get('/admin/coupon_delete/{id}', 'AdminPagesController@coupon_request_delete');

	Route::post('/admin/delete_all_offerrequest', 'AdminPagesController@delete_all_coupon');

	Route::get('/admin/send_notification', 'AdminPagesController@show_send_notification');

	Route::post('/admin/send_notification_action', 'AdminPagesController@notification_action');

	Route::get('/admin/email_content', 'AdminPagesController@email_content');

	Route::get('/admin/email_content_form/{id}', 'AdminPagesController@show_email_form');

	Route::post('/admin/email_content_action', 'AdminPagesController@email_content_action');

	Route::get('/admin/suggestion_list', 'AdminPagesController@show_suggestion_list');

	Route::get('/admin/suggestion_delete/{id}', 'AdminPagesController@suggestion_request_delete');

	/******************** MENU TEMPLATE **************************/

	/******************** Notification List **************************/

	Route::get('/admin/notification_list', 'AdminPagesController@notification_list');

	Route::get('/admin/delete_vendor_notification/{id}', 'AdminPagesController@delete_vendor_notification');

	Route::get('/admin/delete_deliveryman_notification/{id}', 'AdminPagesController@delete_deliveryman_notification');

	Route::get('/admin/delete_user_notification/{id}', 'AdminPagesController@delete_user_notification');

	Route::get('/admin/read_user_notification', 'AdminPagesController@read_user_notification');

	Route::get('/admin/read_dman_notification', 'AdminPagesController@read_dman_notification');

	Route::get('/admin/read_vendor_notification', 'AdminPagesController@read_vendor_notification');

	/******************** Notification List **************************/



	Route::get('/admin/menu_template', 'AdminmenutemplateController@show_menutemplate_list');

	Route::get('/admin/get_templatename', 'AdminmenutemplateController@get_template_form');

	Route::get('/admin/get_templatename/{id}', 'AdminmenutemplateController@get_template_form');

	Route::post('/admin/menu_template_action', 'AdminmenutemplateController@template_action');

	Route::get('/admin/template_menu_delete/{id}', 'AdminmenutemplateController@template_delete');



	/*My template menu start*/



	Route::get('/admin/delete_template_menus/{id}', 'AdminmenutemplateController@deletetemplate_menus');



	/*My template menu end*/



	Route::get('/admin/template_menu_list/{id}', 'AdminmenutemplateController@show_menu_list');

	Route::post('/admin/template_menu_ajax_form', 'AdminmenutemplateController@ajax_menu_form');

	Route::post('/admin/template_menu_action', 'AdminmenutemplateController@menu_action');

	Route::post('/admin/template_show_menudetail', 'AdminmenutemplateController@ajax_show_menudetail');

	Route::post('/admin/template_update_sortorder', 'AdminmenutemplateController@ajax_update_sortorder');

	Route::post('/admin/template_show_popular', 'AdminmenutemplateController@menu_category_popularlist');

	Route::post('/admin/template_menu_ajax_itemform', 'AdminmenutemplateController@ajax_menu_item_form');

	Route::post('/admin/template_update_menu_item', 'AdminmenutemplateController@ajax_update_menu_category');

	Route::post('/admin/template_category_action', 'AdminmenutemplateController@menu_category_action');

	Route::post('/admin/template_addons_list', 'AdminmenutemplateController@ajax_addons_list');

	Route::post('/admin/template_addons_form', 'AdminmenutemplateController@ajax_addons_form');

	Route::post('/admin/template_addon_action', 'AdminmenutemplateController@ajax_addons_action');

	Route::post('/admin/menutemplate_action', 'AdminresturantsController@get_template_action');

	Route::get('/admin/my_wallet', 'AdminwalletController@my_wallet');

	Route::get('/admin/dm_wallet', 'AdminwalletController@dm_wallet');

	Route::post('/dm/bank-setup', 'AdminwalletController@bank_setup');

	Route::get('/admin/dm_wallet/{id}', 'AdminwalletController@dm_wallet');

	Route::get('/admin/dm_status/{id}', 'AdminwalletController@dm_status');



	/*****************************************  ADMIN ROUTES  *********************************************/

	/* TRANSPORT MODULE END   */
});


Route::get('/merchant', function () {

	if (Auth::guard("vendor")->check()) {

		return redirect()->route("merchant.dashboard");
	} else {

		return redirect()->route("merchant.login");
	}
});


Route::group(['middleware' => ['vendorGuest']], function () {

	Route::get('/merchant/', 'OwnerAuth\AuthController@showLoginForm')->name("vendor.login");

	Route::get('/merchant/login/', 'OwnerAuth\AuthController@showLoginForm')->name("merchant.login");

	Route::get('/merchant/register', 'OwnerAuth\AuthController@registration')->name("merchant.register");

	Route::post('/merchant/login', 'OwnerAuth\AuthController@login')->name("merchant.auth");

	Route::get('/merchant/password/reset', 'OwnerAuth\AuthController@forgotpwd');

	Route::get('/merchant/forgot_password', 'OwnerAuth\AuthController@show_forgotpwd');

	Route::post('/merchant/password/forgot_password', 'OwnerAuth\AuthController@sendPasswordResetLink')->name("vendor.sendPasswordResetLink");

	Route::get('/merchant/password/reset/{token}', 'OwnerAuth\AuthController@resetPassword')->name("vendor.resetPassword");

	Route::post('/merchant/password/enter_otp', 'OwnerAuth\AuthController@VerifyOTP');

	Route::get('/merchant/password/reset_your_password', 'OwnerAuth\AuthController@setNewPasswordForm');

	Route::post('/merchant/password/reset', 'OwnerAuth\AuthController@updatePassword')->name("vendor.updatePassword");
});

Route::get('/merchant/logout', 'OwnerAuth\AuthController@logout')->name("merchant.logout");

Route::get("/merchant/membership/plans", "Vendor\VendorMembershipController@plans")->name("vendor.membershipPlans")->middleware("vendorAuth");

//Route::group(["middleware" => ["vendorAuth","vendorMembership"]],function (){

Route::get('/merchant/dashboard', 'RestOwner\OwnerController@index')->name("merchant.dashboard");

Route::get('/merchant/update_profile', 'RestOwner\OwnerController@view_profile')->name("merchant.updateProfilePage");

Route::post('/merchant/crop_image_save', 'RestOwner\OwnerController@crop_image_save');

Route::post('/merchant/update_profile', 'RestOwner\OwnerController@update_profile_action')->name("merchant.updateProfile");

Route::get('/merchant/change_password', 'RestOwner\OwnerController@showPasswordForm')->name("merchant.updatePasswordPage");

Route::post('/merchant/change_password', 'RestOwner\OwnerController@updatepassword')->name("merchant.changePassword");

Route::get('/merchant/shopping-cart', 'RestOwner\OwnerController@shopping_cart');

Route::post('/merchant/shop_action', 'RestOwner\OwnerController@shop_action');

Route::get('/merchant/account_delete', 'RestOwner\OwnerController@show_account');

Route::post('/merchant/account_delete', 'RestOwner\OwnerController@account_delete')->name("merchant.deleteaccount");

/* MANAGE RESTAURENT */

Route::get('/merchant/product-list', 'RestOwner\OwnerrestaurantController@product_service_list');

Route::get('/merchant/all-product-list', 'RestOwner\OwnerrestaurantController@all_product_list');
Route::get('/merchant/my-product-list', 'RestOwner\OwnerrestaurantController@my_product_list');
Route::get('/merchant/admin-product-list', 'RestOwner\OwnerrestaurantController@admin_product_list');

Route::get('/merchant/add-admin-product', 'RestOwner\OwnerrestaurantController@add_admin_product');

Route::post('/merchant/crop_product_image_save', 'RestOwner\OwnerController@crop_product_image_save');

Route::post('/merchant/gallery_crop_image_save', 'RestOwner\OwnerController@gallery_crop_image_save');

Route::get('/merchant/product-form', 'RestOwner\OwnerrestaurantController@product_service_form');

Route::get('/merchant/product-form/{id}', 'RestOwner\OwnerrestaurantController@product_service_form');

Route::get('/merchant/admin-merchant-product-form/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_product_form');

Route::get('/merchant/admin-merchant-product-view/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_product_view');

Route::get('/merchant/merchant-product-view/{id}', 'RestOwner\OwnerrestaurantController@merchant_product_view');

Route::get('/merchant/merchant-event-view/{id}', 'RestOwner\OwnerrestaurantController@merchant_event_view');

Route::get('/merchant/merchant-product-inventory/{id}', 'RestOwner\OwnerrestaurantController@inventory_product');

Route::get('/merchant/merchant-event-inventory/{id}', 'RestOwner\OwnerrestaurantController@inventory_event');

Route::post('/merchant/product-action', 'RestOwner\OwnerrestaurantController@product_service_action');

Route::post('/merchant/inventory-product-action', 'RestOwner\OwnerrestaurantController@inventory_product_action');

Route::post('/merchant/inventory-event-action', 'RestOwner\OwnerrestaurantController@inventory_event_action');

Route::post('/merchant/add-admin-product-action', 'RestOwner\OwnerrestaurantController@admin_product_action');

Route::post('/merchant/add-admin-product-update', 'RestOwner\OwnerrestaurantController@admin_product_update');

Route::get('/merchant/product-delete/{id}', 'RestOwner\OwnerrestaurantController@product_service_delete');

Route::get('/merchant/admin-merchant-product-delete/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_product_delete');

Route::get('/merchant/all-event-list', 'RestOwner\OwnerrestaurantController@all_event_list');

Route::get('/merchant/my-event-list', 'RestOwner\OwnerrestaurantController@my_event_list');

Route::get('/merchant/admin-event-list', 'RestOwner\OwnerrestaurantController@admin_event_list');

Route::get('/merchant/add-admin-event', 'RestOwner\OwnerrestaurantController@add_admin_event');

Route::post('/merchant/crop_product_image_save', 'RestOwner\OwnerController@crop_product_image_save');

Route::post('/merchant/crop_event_image_save', 'RestOwner\OwnerrestaurantController@crop_event_image_save');

Route::post('/merchant/gallery_crop_image_save', 'RestOwner\OwnerController@gallery_crop_image_save');

Route::post('/merchant/gallery_event_crop_image_save', 'RestOwner\OwnerrestaurantController@gallery_event_crop_image_save');

Route::get('/merchant/event-form', 'RestOwner\OwnerrestaurantController@event_service_form');

Route::post('/merchant/event-management-action', 'RestOwner\OwnerrestaurantController@event_service_action');

Route::get('/merchant/event-edit/{id}', 'RestOwner\OwnerrestaurantController@event_service_edit');

Route::get('/merchant/event-service-delete/{id}', 'RestOwner\OwnerrestaurantController@event_service_delete');

Route::get('/merchant/admin-merchant-event-form/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_event_form');

Route::get('/merchant/admin-merchant-event-view/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_event_view');

Route::post('/merchant/add-admin-event-update', 'RestOwner\OwnerrestaurantController@admin_event_update');

Route::post('/merchant/add-admin-event-action', 'RestOwner\OwnerrestaurantController@admin_event_action');

Route::get('/merchant/admin-merchant-event-delete/{id}', 'RestOwner\OwnerrestaurantController@admin_merchant_event_delete');

Route::get('/merchant/orders_show', 'RestOwner\OwnerController@orders_show');

Route::post('/merchant/chart_filter_order_date', 'RestOwner\OwnerController@chart_filter_order_date');

Route::get('/merchant/product-view/{id}', 'RestOwner\OwnerrestaurantController@view_restaurant');

Route::post('/merchant/menu_ajax_form', 'RestOwner\OwnerrestaurantController@ajax_menu_form');

Route::post('/merchant/menu_action', 'RestOwner\OwnerrestaurantController@menu_action');

Route::post('/merchant/show_menudetail', 'RestOwner\OwnerrestaurantController@ajax_show_menudetail');

Route::post('/merchant/menu_ajax_itemform', 'RestOwner\OwnerrestaurantController@ajax_menu_item_form');

Route::post('/merchant/category_action', 'RestOwner\OwnerrestaurantController@menu_category_action');

Route::post('/merchant/update_menu_item', 'RestOwner\OwnerrestaurantController@ajax_update_menu_category');

Route::post('/merchant/update_sortorder', 'RestOwner\OwnerrestaurantController@ajax_update_sortorder');

Route::post('/merchant/show_popular', 'RestOwner\OwnerrestaurantController@menu_category_popularlist');

Route::post('/merchant/addons_list', 'RestOwner\OwnerrestaurantController@ajax_addons_list');

Route::post('/merchant/addons_form', 'RestOwner\OwnerrestaurantController@ajax_addons_form');

Route::post('/merchant/addon_action', 'RestOwner\OwnerrestaurantController@ajax_addons_action');

Route::post('/merchant/promo_form', 'RestOwner\OwnerrestaurantController@ajax_promo_form');

Route::post('/merchant/promo_action', 'RestOwner\OwnerrestaurantController@promo_action');

/* service start */



Route::post('/merchant/sub-category', 'RestOwner\OwnerrestaurantController@subcategory_list');

Route::post('/merchant/sub-category-show', 'RestOwner\OwnerrestaurantController@subcategory_list_show');

Route::get('/merchant/productimage/{id}', 'RestOwner\OwnerrestaurantController@product_gallery_delete');

Route::get('/merchant/serviceimage/{id}', 'RestOwner\OwnerrestaurantController@service_gallery_delete');



Route::get('/merchant/service-list', 'RestOwner\OwnerrestaurantController@service_list')->name("merchant.serviceList");

Route::get('/merchant/service-form', 'RestOwner\OwnerrestaurantController@service_form')->name("merchant.serviceCreate");

Route::get('/merchant/service-form/{id}/', 'RestOwner\OwnerrestaurantController@service_form');

Route::post('/merchant/service-action', 'RestOwner\OwnerrestaurantController@service_action');

Route::get('/merchant/service_delete/{id}', 'RestOwner\OwnerrestaurantController@service_delete');

Route::get('/merchant/service_view/{id}', 'RestOwner\OwnerrestaurantController@view_service');

/* Coupon code start */

//priyanka17092021


Route::get('/merchant/order_report', 'RestOwner\OwnerorderController@show_ordertoday');
Route::post('/merchant/filter_order', 'RestOwner\OwnerorderController@filter_order');
Route::get('/merchant/filter_order_status', 'RestOwner\OwnerorderController@filter_order_status');
Route::post('/merchant/filter_order_status', 'RestOwner\OwnerorderController@filter_order_status');
Route::post('/merchant/filter_order_customer', 'RestOwner\OwnerorderController@filter_order_customer');
Route::post('/merchant/filter_order_date', 'RestOwner\OwnerorderController@filter_order_date');

Route::get('/merchant/export_in_excel', 'RestOwner\OwnerorderController@export_in_excel');

Route::get('/merchant/export_in_excel_filter/{searchid}/{filter}', 'RestOwner\OwnerorderController@export_in_excel_filter');

Route::get('/merchant/monthly', 'RestOwner\OwnerorderController@monthly');
Route::get('/merchant/today', 'RestOwner\OwnerorderController@today');
Route::get('/merchant/week', 'RestOwner\OwnerorderController@week');
//priyanka17092021
Route::get('/merchant/coupon-code', 'RestOwner\OwnerrestaurantController@coupon_code');

Route::get('/merchant/coupon-form', 'RestOwner\OwnerrestaurantController@coupon_form');

Route::post('/merchant/coupon-form', 'RestOwner\OwnerrestaurantController@add_coupon_code');

Route::get('/merchant/coupon_code_delete/{id}', 'RestOwner\OwnerrestaurantController@coupon_delete');

Route::get('/merchant/coupon-edit-form/{id}', 'RestOwner\OwnerrestaurantController@coupon_edit_form');

Route::post('/merchant/coupon-edit-form', 'RestOwner\OwnerrestaurantController@update_coupon_detail');


/* Coupon code end */

/* MANAGE ORDERLISTING */

Route::get('/merchant/return-list/', 'RestOwner\OwnerorderController@show_returnlist');

Route::get('/merchant/return-list/{id}', 'RestOwner\OwnerorderController@show_returnlist');


Route::get('/merchant/order-list', 'RestOwner\OwnerorderController@show_orderlist');

Route::get('/merchant/event-list', 'RestOwner\OwnerorderController@show_event_list');

Route::get('/merchant/event-details/{id}', 'RestOwner\OwnerorderController@show_event_detail');

Route::get('/merchant/order-list/{id}', 'RestOwner\OwnerorderController@show_orderlist');

Route::get('/merchant/order-details/{id}', 'RestOwner\OwnerorderController@show_orderdetail');

Route::post('/merchant/status-update', 'RestOwner\OwnerorderController@update_order_action');


Route::get('/merchant/payment-list/', 'RestOwner\OwnerorderController@payment_list');

Route::get('/merchant/payment-list/{id}/', 'RestOwner\OwnerorderController@payment_list');

Route::get('/merchant/payment-details/{id}', 'RestOwner\OwnerorderController@payment_details');


Route::post('/merchant/order_address_update', 'RestOwner\OwnerorderController@update_order_address');

Route::post('/merchant/order_status_update_notification', 'RestOwner\OwnerorderController@update_order_action_notification');

Route::post('/merchant/order_search_list', 'RestOwner\OwnerorderController@ajax_search_list');

Route::get('/merchant/user_orderlist/{userid}', 'RestOwner\OwnerorderController@show_orderlist');

Route::post('/merchant/order_report', 'RestOwner\OwnerorderController@show_orderreport');

Route::get('/merchant/product_report', 'RestOwner\OwnerorderController@show_product_report');

Route::post('/merchant/product_report', 'RestOwner\OwnerorderController@post_product_report');

Route::get('/merchant/product_export', 'RestOwner\OwnerorderController@product_export_excel');

Route::get('/merchant/product_export_filter/{searchid}/{filter}', 'RestOwner\OwnerorderController@product_export_filter_excel');

Route::post('/merchant/product_filter_order', 'RestOwner\OwnerorderController@product_filter_order');

Route::get('/merchant/product_filter_order_status', 'RestOwner\OwnerorderController@product_filter_order_status');

Route::post('/merchant/product_filter_order_status', 'RestOwner\OwnerorderController@product_filter_order_status');

Route::post('/merchant/product_filter_order_customer', 'RestOwner\OwnerorderController@product_filter_order_customer');

Route::post('/merchant/product_filter_order_date', 'RestOwner\OwnerorderController@product_filter_order_date');

/* MANAGE Scheduled Order */

Route::get('/merchant/scheduled_order', 'RestOwner\OwnerorderController@show_scheduled_order');

Route::get('/merchant/scheduled_order_view', 'RestOwner\OwnerorderController@show_scheduled_order_detail');

Route::post('/merchant/scheduled_order_search_list', 'RestOwner\OwnerorderController@ajax_scheduled_search_list');



/* MANAGE PAYMENTLISTING */

//Route::get('/merchant/paymentlisting', 'RestOwner\OwnerorderController@show_paymentlist')->name("merchant.paymentList");

//Route::get('/merchant/payment_view','RestOwner\OwnerorderController@show_paymentdetail');

Route::post('/merchant/payment_search_list', 'RestOwner\OwnerorderController@ajax_show_paymentlist');

Route::post('/merchant/payment_report', 'RestOwner\OwnerorderController@show_paytmentreport');

Route::post('/merchant/payment_status_update', 'RestOwner\OwnerorderController@update_payment_action');

/* MANAGE REVIEW AND REATING */

Route::get('/merchant/review_list', 'RestOwner\OwnerreviewController@show_reviewlist');

Route::post('/merchant/review_update', 'RestOwner\OwnerreviewController@show_review_action');

Route::post('/merchant/review_search_list', 'RestOwner\OwnerreviewController@ajax_search_list');

Route::get('/merchant/review_delete/{id}', 'RestOwner\OwnerreviewController@review_delete');

/* Review and rating reports */

Route::get('/merchant/review_rating_report', 'RestOwner\OwnerreviewController@show_reviewlist_report');

Route::post('/merchant/review_search_list_report', 'RestOwner\OwnerreviewController@ajax_search_list_report');

/* MANAGE Send Notifiaction*/

Route::get('/merchant/send_notification', 'RestOwner\OwnerController@show_send_notification');

Route::post('/merchant/send_notification_action', 'RestOwner\OwnerController@notification_action');

/* VIEW MENU TEMPLATES */

Route::get('/merchant/view_menu_template', 'RestOwner\OwnermenutemplateController@show_menutemplate_list');

Route::get('/merchant/view_menu_list/{id}', 'RestOwner\OwnermenutemplateController@show_menu_list');

Route::post('/merchant/template_show_menudetail', 'RestOwner\OwnermenutemplateController@ajax_show_menudetail');

Route::post('/merchant/template_addons_list', 'RestOwner\OwnermenutemplateController@ajax_addons_list');

Route::post('/merchant/template_show_popular', 'RestOwner\OwnermenutemplateController@menu_category_popularlist');

Route::post('/merchant/menutemplate_action', 'RestOwner\OwnerrestaurantController@get_template_action');



Route::get('/merchant/my_wallet', 'RestOwner\OwnerController@my_wallet')->name("merchant.wallet");

Route::post('/merchant/request-amount', 'RestOwner\OwnerController@request_amount');



Route::get('/merchant/bank-account', 'RestOwner\OwnerController@bank_account');

Route::post('/merchant/bank-setup', 'RestOwner\OwnerController@bank_setup');


Route::get('/merchant/social-media', 'RestOwner\OwnerController@social_media');
Route::post('/merchant/social-setup', 'RestOwner\OwnerController@social_setup');


Route::post('/ws/merchant/my_wallet', 'WsController@vendor_my_wallet');

Route::post('/ws/vendor/request-amount', 'WsController@vendor_request_amount');

Route::post('/ws/vendor/bank-setup', 'WsController@vendor_bank_setup');

Route::post('/ws/vendor/request-history', 'WsController@vendor_request_history');

Route::post('/ws/vendor/get_bank_details', 'WsController@get_bank_details');

// });



// Route::auth();

//Route::get('/{subrub}/{restname}','ListController@restaurant_detail');

//Route::get('/{subrub}/{restname}/{review}','ListController@restaurant_review');

/*re-order start*/

Route::get('/reorder', 'ListController@show_reorder');

/*re-order end*/

Route::post('/restaurant_detail/dinein', 'ListController@get_dineinfrm');

//Route::post('/cart','CartController@add_tocart');

Route::post('/cart/increment', 'CartController@qty_icnrement');

Route::post('/cart/decrease', 'CartController@qty_decrease');

Route::post('/cart/get_cart', 'CartController@show_cart');

Route::post('/get_delivery', 'CartController@get_delivery_chrg');

Route::post('/post_checkout_rest', 'CheckoutController@rest_order_submit');

//Route::get('/checkout','CheckoutController@view_checkout_order');

Route::post('/checkout/showTime', 'CheckoutController@view_delivery_time');

Route::post('/checkout/getDeliveryTimeStatus', 'CheckoutController@getDeliveryTimeStatus');

Route::post('/submit_checkoutdetail', 'CheckoutController@get_checkout_detail');

Route::get('/payment', 'CheckoutController@payment_option_page');

Route::post('/checkout/cash_payment_process', 'CheckoutController@get_cash_payment');

Route::post('/submit-return-request', 'OrderuserController@submit_return_request');

Route::post('/checkout/wallet_payment_process', 'CheckoutController@get_wallet_payment');

Route::get('/order_detail', 'OrderuserController@view_order_detail');

Route::get('/order_success/{tx?}', 'OrderuserController@sucess_order');

Route::get('/order_cancel', 'OrderuserController@cancel_order');

Route::post('/become_partner', 'GeneralController@get_partnerdetail');

Route::post('/show_other_data', 'ListController@get_addons');

Route::post('/checkout/coupon_code_apply', 'CheckoutController@coupon_code_apply');


Route::get('/ws', 'WsController@index');

Route::post('/ws/register', 'WsController@register')->middleware('CheckToken');

Route::post('/ws/verify_OTP', 'WsController@verify_OTP')->middleware('CheckToken');

Route::post('/ws/resent_OTP', 'WsController@resent_OTP')->middleware('CheckToken');

Route::post('/ws/forget_passwrod', 'WsController@forget_pwd')->middleware('CheckToken');

Route::post('/ws/resetpassword', 'WsController@reset_password')->middleware('CheckToken');

Route::post('/ws/login', 'WsController@login')->middleware('CheckToken');

Route::post('/ws/logout', 'WsController@logout')->middleware('CheckToken');

/* Vendor API routs start*/

Route::post('/ws/vendor-register', 'WsController@vendor_register');

Route::post('/ws/vendor-login', 'WsController@vendor_login');

Route::post('/ws/vendor-logout', 'WsController@vendorlogout');

Route::get('/ws/vendor-logout', 'WsController@vendor_logout');

Route::post('/ws/vendor-forget-passwrod', 'WsController@vendor_forget_pwd');

Route::post('/ws/vendor-verify-otp', 'WsController@vendor_verify_OTP');

Route::post('/ws/vendor_category', 'WsController@vendor_categories');

Route::post('/ws/vendor-view-profile', 'WsController@vendor_viewprofile');

Route::post('/ws/vendor-edit-profile', 'WsController@vendor_update_profile');

Route::post('/ws/vendorOrder-list', 'WsController@show_vendorOrderlist');

Route::post('/ws/vendor_inprogressOrder-list', 'WsController@vendor_inprogressOrderlist');

Route::post('/ws/vendor-accept_or_reject_order', 'WsController@vendor_order_accept_or_reject');

Route::post('/ws/vendorOrder-detail', 'WsController@show_order_detail_vendor');

Route::post('/ws/vendorHistoryOrder-list', 'WsController@vendor_HistoryOrderlist');

Route::post('/ws/vendor_devicetoken', 'WsController@vendor_device_token');

Route::post('/ws/vendor_notification_list', 'WsController@show_vendor_notification');

Route::post("/ws/vendor_update_password", "WsController@vendor_update_password");



Route::post('/ws/merchant/ps-add', 'WsController@product_service_add');

Route::post('/ws/merchant/ps-details', 'WsController@product_service_details');

Route::post('/ws/merchant/ps-update', 'WsController@product_service_update');

Route::post('/ws/merchant/ps-delete', 'WsController@product_service_delete');

Route::post('/ws/merchant/ps-cat-list', 'WsController@product_service_cat_list');

Route::post('/ws/merchant/ps-sub-cat-list', 'WsController@product_service_sub_cat_list');

Route::post('/ws/merchant/ps-subcategory', 'WsController@product_service_subcategory');

Route::post('/ws/merchant/unit', 'WsController@get_unit');

Route::post('/ws/merchant/delete', 'WsController@merchant_delete');

Route::post('/ws/merchant/order-details', 'WsController@order_details');

Route::post('/ws/merchant/order-history-details', 'WsController@order_history_details');

// Route::post('/ws/merchant/merchant-to-user','WsController@merchant_to_user_location');

Route::post('/ws/merchant/update-merchant-location', 'WsController@update_merchant_location');

Route::post('/ws/merchant/code-verification', 'WsController@code_verification');

Route::post('/ws/merchant/order-status', 'WsController@order_status');



/* Vendor API routs end*/



/*Delivery man API routs start*/

// 9/9/2019

Route::post('/ws/deliveryman-login', 'WsController@deliverman_login');

Route::post('/ws/forgetpassword', 'WsController@forgetPassword');

Route::post('/ws/deliveryman-profile', 'WsController@viewdeliverymanprofile');

Route::post('/ws/updatedeliveryman-profile', 'WsController@deliverymanupdate_profile');

Route::post('/ws/getdeliveryman-location', 'WsController@deliverymanget_location');

Route::post('/ws/assignedorder-list', 'WsController@show_deliverymanOrderlist');

Route::post('/ws/deliverymanorder-detail', 'WsController@show_order_detail_deliveryman');

Route::post('/ws/order-acceptreject', 'WsController@accept_reject_order');

Route::post('/ws/updateorder-status', 'WsController@update_order_status');

Route::post('/ws/verify_order', 'WsController@verify_order_code');

Route::post('/ws/Orderhistory-list', 'WsController@show_deliverymanOrderHistorylist');

Route::post('/ws/Orderhistory-detail', 'WsController@show_historyorder_detail');

Route::post('/ws/fromto-location', 'WsController@from_to_location');

Route::post('/ws/get-updatelocation', 'WsController@get_current_location');

Route::post('/ws/from_to-show_locationtouser', 'WsController@show_locationtion_user');

Route::post('/ws/deliveryman_devicetoken', 'WsController@deliveryman_device_token');

Route::post('/ws/dm_view_notification', 'WsController@dm_show_notification');



/*Delivery man API routs end*/

Route::post('/ws/restaurant_search', 'WsController@search_restaurant');

Route::post('/ws/soical_signup', 'WsController@get_soical_signup');

Route::post('/ws/view_profile', 'WsController@viewprofile');

Route::post('/ws/updateprofile', 'WsController@update_profile');

Route::post('/ws/updateprofileimg', 'WsController@update_profileimg');

Route::post('/ws/get_address', 'WsController@get_address');

// 30-8-2019

Route::post('/ws/viewpagecontent', 'WsController@viewpagecontent');

Route::post('/ws/devicetoken', 'WsController@device_token');

// 31-8-2019

Route::post('/ws/make_favourite_restaurent', 'WsController@make_favourite');

Route::post('/ws/remove_favourite_restaurent', 'WsController@remove_favourite');

Route::post('/ws/restaurantreview', 'WsController@restaurant_review_listing');

Route::post('/ws/food_search', 'WsController@food_rest_search');

Route::post('/ws/restaurant_food_search', 'WsController@food_search');

Route::post('/ws/add_mobile_no', 'WsController@add_mobile_no');

Route::post('/ws/resend_mobile_otp', 'WsController@resend_mobile_otp');

Route::post('/ws/verify_mobile_no', 'WsController@verify_mobile_no');

Route::post('/ws/updatepassword', 'WsController@change_password');

Route::post('/ws/favourite_restaurent_list', 'WsController@favourite_list');


// 3-9-2019


Route::post('/ws/fooddetail', 'WsController@restaurant_food_detail');

Route::post('/ws/view_notification', 'WsController@show_notification');

Route::post('/ws/restaurant_open', 'WsController@show_restaurant_open');

Route::post('/ws/update_order_type', 'WsController@get_update_order_type');

Route::post('/ws/post_review', 'WsController@post_order_review');

Route::post('/ws/view_order', 'WsController@show_order_detail');

Route::post('/ws/verify_delivery_address', 'WsController@get_verify_delivery_address');

Route::post('/ws/newdelivery_address', 'WsController@add_delivery_address');

Route::post('/ws/restaurantdetail', 'WsController@restaurant_detail');


// 4-9-2019


Route::post('/ws/addcart', 'WsController@food_addcart');

Route::post('/ws/updatecart', 'WsController@food_updatecart');

Route::post('/ws/add_removecart', 'WsController@food_add_removecart');

Route::post('/ws/removecart', 'WsController@food_removecart');

Route::post('/ws/make_favourite_food', 'WsController@make_food_favourite');

Route::post('/ws/remove_favourite_food', 'WsController@remove_food_favourite');

Route::post('/ws/getcart', 'WsController@food_getcartdeatil');


// 5-9-2019


Route::post('/ws/save_order', 'WsController@get_save_order');

Route::post('/ws/deletefood', 'WsController@food_itemdelete');

Route::post('/ws/orderlisting', 'WsController@show_order_listing');


// 6-9-2019

Route::post('/ws/get_token', 'WsController@getToken');

Route::post('/ws/card_payment', 'WsController@payCardPayment');

Route::post('/ws/send_factor_otp', 'WsController@two_factor_otp');

Route::post('/ws/varify_factor_otp', 'WsController@get_varify_factor_otp');

Route::post('/ws/resent_factor_otp', 'WsController@get_resent_factor_otp');


// 7-9-2019

Route::post('/ws/content_pages', 'WsController@show_contentpage_link');

Route::post('/ws/notification_status', 'WsController@update_notification_status');

Route::post('/ws/checkout_verificaiton_status', 'WsController@update_checkout_verificaiton_status');

Route::post('ws/update_cart_user', 'WsController@update_temp_userid');


Route::post('/ws/guest_user_otp', 'WsController@show_guest_user_otp');

Route::post('/ws/varify_guest_user_otp', 'WsController@get_varify_guest_otp');

Route::post('/ws/guest_user_resentotp', 'WsController@show_guest_user_resentotp');


// 27-9-2019

Route::post('ws/delivery_address_list', 'WsController@show_delivery_address');

Route::post('ws/delete_delivery_address', 'WsController@delete_delivery_address');

Route::post('ws/update_delivery_address', 'WsController@update_delivery_address');


// content pages api

Route::post('/ws/ws_show_privacy_policy', 'WsController@ws_privacy_policy');

Route::post('/ws/ws_show_terms_condition', 'WsController@ws_terms_condition');

Route::post('/ws/ws_show_about', 'WsController@ws_about');

Route::post('/ws/ws_show_help', 'WsController@ws_help');

Route::post('/ws/ws_show_faq', 'WsController@ws_faq');

Route::post('/ws/ws_show_contact', 'WsController@ws_contactus');

Route::post('/ws/area_manager_detail', 'WsController@area_manager_detail');


//01-01-2021
Route::post('/ws/vendor_update_proceed', 'WsController@vendor_update_proceed');

Route::post('/ws/vendor_proceed_status', 'WsController@vendor_proceed_status');

Route::post('/ws/contactus_action', 'WsController@contactus_action');


//04-01-2021

Route::post('/ws/merchant/order-suspend-list', 'grambunnyApiController@merchantSuspendOrders');

/**************************************************************************************

 ********************************** webservise end ***********************************

 **************************************************************************************/

//Route::post('/checkout/cash_payment_process','CheckoutController@get_cash_payment');

Route::post('/checkout/cart_payment_process', 'CheckoutController@get_cart_payment');

Route::get('/show_privacy_policy', 'GeneralController@webservice_privacy_policy');

Route::get('/show_privacy_policy_ar', 'GeneralController@webservice_privacy_policy_ar');

Route::get('/show_terms_condition', 'GeneralController@webservice_terms_condition');

Route::get('/show_terms_condition_ar', 'GeneralController@webservice_terms_condition_ar');

Route::get('/show_about', 'GeneralController@webservice_about');

Route::get('/show_about_ar', 'GeneralController@webservice_about_ar');

Route::get('/show_contact', 'GeneralController@webservice_contactus');

Route::get('/show_contact_ar', 'GeneralController@webservice_contactus_ar');

Route::get('/show_help_page', 'GeneralController@webservice_help_page');

Route::get('/show_faq_page', 'GeneralController@webservice_faq_page');


/******************NEW ROUES FOR RESTAPP******/

Route::get('/favorite_restaurant_list', 'UserController@show_favorite_restaurant');

Route::post('/get_current_locationdata', 'ListController@get_current_location');

Route::get('/favourite', 'ListController@make_favourite');

Route::post('/google_rest_insert', 'ListController@add_google_rest');

Route::post('/restaurantslisting_map', 'ListController@home_search_google');

Route::get('/ipn_notify', 'OrderuserController@get_ipn_date');

Route::post('/ipn_notify', 'OrderuserController@get_ipn_date_post');

Route::post('/checkout/paypal_payment_process', 'CheckoutController@get_paypal_payment');

Route::post('/send_coupon_request', 'GeneralController@get_coupon_request');

Route::post('/send_app_link', 'GeneralController@get_app_request');

Route::post('/send_restaurant_request', 'GeneralController@get_restaurant_request');

Route::post('/checkout/send_otp_verify', 'CheckoutController@send_otp_email');

Route::post('/checkout/check_submit_otp', 'CheckoutController@show_otp_verification');

Route::post('/checkout/check_delivery_address', 'CheckoutController@find_delivery_address');

Route::post('/checkout/delete_delivery_address', 'CheckoutController@delete_delivery_address');

Route::post('/cart/show_cart_data', 'CartController@show_cart_detail_onload');

Route::post('/check_registration_otp', 'CartController@show_cart_detail_onload');

Route::post('/user_registration_otp', 'Auth\AuthController@get_otp_login');

Route::get('/vendor_confirm', 'HomeController@vendor_order_confirm');

/*****************END**********************/

Route::get('/daily_report', array('uses' => 'DailyReportController@index'));

Route::get('/test_report', array('uses' => 'TestReportController@index'));

Route::get('/{locale}', function ($locale) {

	Session::put('locale', $locale);

	return redirect()->back();
})->name('switchLan');  //add name to router

//how to use query when the data is in the form of json object in database column using laravel