<?php
//$order_total = number_format($order_total,2);
$json = file_get_contents('php://input');
$data = json_decode($json);


//header("Content-Type:application/json");
//$data = json_decode(file_get_contents('php://input'), true);
//print_r($data);

//ob_end_clean();
require('fpdf.php');  
$pdf = new FPDF();  
$pdf->setMargins(15, 44, 11.7);
$pdf->AddPage();

$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$pdf->SetLineWidth(0.8);
$pdf->SetDrawColor(211,211,211);

$top_margin = 10;
$side_margin = 30;
$pdf->Line($top_margin, $side_margin, 13,$side_margin); // 50mm from each edge
$pdf->Line($top_margin + 91, $height - 237 -$side_margin,$width-$top_margin  ,$height -237 -$side_margin);
$pdf->Line($top_margin , $height - 120 -$side_margin,$width-$top_margin ,$height -120 -$side_margin); 
$pdf->Line($top_margin, $side_margin,$top_margin,$height-150); // Vetical line at left 
$pdf->Line($width-$top_margin, $side_margin,$width-$top_margin,$height-150); // Vetical line at Right

$pdf->SetX(10); 
$pdf->SetY(10); 
$pdf->Ln();
$pdf->Image('https://grambunny.com/public/uploads/qrcode/logobar.png', 15, 11);

$pdf->SetFont('Arial','BU',14);
$pdf->SetTextColor(48,48,48);
$pdf->SetX(40);
$pdf->Cell(55,38, 'Grambunny Ticket Booth'); 

$pdf->SetX(102);
$pdf->SetTextColor(48,48,48);
$pdf->Cell(10,35, 'Ticket# '.$data->ticket_id);  

$pdf->SetTextColor(48,48,48);
$pdf->Cell(5);
$pdf->Ln(0);
$pdf->SetX(40);
$pdf->Cell(55,48, 'Order# ' . $data->order_id);  

$pdf->Cell(10);
$pdf->Ln(30);
$pdf->Image($data->qrcodeimg, 132, 32,55);
$pdf->Cell(10);
$pdf->Cell(10);

$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(1);
$pdf->MultiCell(120,5, $data->event_name ,'',false);  

$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(0); 
$pdf->MultiCell(125,10,'GENERAL ADMISSION :  ' .'$'. number_format($data->price,2) );  
$pdf->Cell(10);
$pdf->Ln(0);


$pdf->SetTextColor(104,104,104);
$pdf->MultiCell(125,10,'Name: '  );  

$pdf->SetTextColor(48,48,48);
$pdf->MultiCell(125,0, $data->user_name );

$pdf->SetTextColor(104,104,104);
$pdf->Cell(10);
$pdf->Ln(4);
$pdf->Cell(125,10, 'Location ');  


$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(11);
//$pdf->SetX(30);
$pdf->Cell(125,0, $data->venue_name ); 

$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(4);
$pdf->Cell(175,5, $data->venue_address); 

$pdf->SetTextColor(104,104,104);
$pdf->Cell(10);
$pdf->Ln(9);
$pdf->Cell(10,5, 'Date');  


$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(3);
$pdf->Cell(10,10, date( "F d, Y (l)", strtotime($data->event_date)));
$pdf->Cell(10);
$pdf->Ln(6);
$pdf->Cell(10,10, $data->event_start_time . ' to ' .  $data->event_end_time . '  PST') ;  
$pdf->Line($top_margin + 7, $height - 152 -$side_margin,$width-$top_margin - 7 ,$height -152 -$side_margin);   

$pdf->SetTextColor(104,104,104);
$pdf->Cell(10);
$pdf->Ln(14);
$pdf->Cell(10,10, 'Description' );  

$pdf->SetTextColor(48,48,48);
$pdf->Cell(10);
$pdf->Ln(5);
//$pdf->Cell(10,10,$data->description);  
$pdf->MultiCell(175,10, $data->description ); 

$random_no = rand(0,1000);

//$pdf->Output($random_no.'.pdf','F');
$createpdf = $pdf->Output('gen_file/'.$data->generated_filename.'.pdf','F');

?>