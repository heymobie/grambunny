@extends("layouts.heyMobie")
@section("styles")
{{-- styles --}}


  <link rel="stylesheet" type="text/css" href="css/xzoom.css" media="all" /> 

@endsection

@section("content")
  {{-- content goes here --}}

<section class="section-full bg-gray about-info" id="sf-provider-info">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4">

          <div class="xzoom-container">
          <img class="xzoom" id="xzoom-default" src="{{ $item->images[0]->path }}" xoriginal="{{ $item->images[0]->path }}" />

            <div class="xzoom-thumbs">
              
                @foreach($item->images as $image)
            <a href="{{ $image->path }}"><img class="xzoom-gallery" width="80" src="{{ $image->path }}"  xpreview="{{ $image->path }}" title=""></a>
                @endforeach
            </div>

          </div> 
          <!-- <div class="sf-provider-des">
            <div class="sf-thum-bx overlay-black-light">
              <img src="https://images-na.ssl-images-amazon.com/images/I/71Xp-K4MMBL._SX569_.jpg" alt="">
            </div>
            <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button>
          </div> -->
        </div>
        <div class="col-md-8 col-sm-8 col-xs-8">
          <div class="sf-about-box">
            <h2 class="sf-title">{{ $item->name }}</h2>
            
            <div class="sf-provider-cat">
              <strong>By: </strong>
              <a href="{{ route("merchantServices",["username" =>  $item->vendor->username]) }}">{{ $item->vendor->fullname}}</a>        
            </div>
            <div class="star-rating">
              <div class=" text-warning">
                <?php 
                  $userRatingInitial = 0;
                  if($totalUserRating != '0')
                    $userRatingInitial = $totalUserRating / count($ratingReviewData);
                  $userRating = round($userRatingInitial);
                
                  if($userRating == 1)
                    echo '<i class="fa fa-star"></i>';
                  else if($userRating == 2){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($userRating == 3){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($userRating == 4){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($userRating == 5){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($userRating == '0'){
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>'; 
                  }
                ?>
              </div>
              <span> {{$userRatingInitial}} out of 5 stars | {{count($ratingReviewData)}} ratings </span>
            </div>

            <div class="proPrice">
              <div class="cutPrice">${{ $item->price}}</div>
              {{-- <div class="mainPrice">$110.00</div> --}}
            </div>

          <div class="avilblStock {{ $item->stockStatus["class"] }}">{{ $item->stockStatus["status"] }}</div>

              <p>
                {{ $item->description }}
              </p>

         <!--{!! csrf_field() !!}-->  

         <div class="qtycss"> 

          <label for="quantity" class="a-native-dropdown">Quantity : </label>
          <select name="quantity" autocomplete="off" id="quantity" tabindex="0" class="a-native-dropdown">
           <option value="1" <?php if($ps_qty==1){ ?>selected="selected"<?php } ?>>1</option>
           <option value="2" <?php if($ps_qty==2){ ?>selected="selected"<?php } ?>>2</option>
           <option value="3" <?php if($ps_qty==3){ ?>selected="selected"<?php } ?>>3</option>
           <option value="4" <?php if($ps_qty==4){ ?>selected="selected"<?php } ?>>4</option>
           <option value="5" <?php if($ps_qty==5){ ?>selected="selected"<?php } ?>>5</option>
           <option value="6" <?php if($ps_qty==6){ ?>selected="selected"<?php } ?>>6</option>
           <option value="7" <?php if($ps_qty==7){ ?>selected="selected"<?php } ?>>7</option>
           <option value="8" <?php if($ps_qty==8){ ?>selected="selected"<?php } ?>>8</option>
           <option value="9" <?php if($ps_qty==9){ ?>selected="selected"<?php } ?>>9</option>
           <option value="10" <?php if($ps_qty==10){ ?>selected="selected"<?php } ?>>10</option>
           <option value="11" <?php if($ps_qty==11){ ?>selected="selected"<?php } ?>>11</option>
           <option value="12" <?php if($ps_qty==12){ ?>selected="selected"<?php } ?>>12</option>
           <option value="13" <?php if($ps_qty==13){ ?>selected="selected"<?php } ?>>13</option>
           <option value="14" <?php if($ps_qty==14){ ?>selected="selected"<?php } ?>>14</option>
           <option value="15" <?php if($ps_qty==15){ ?>selected="selected"<?php } ?>>15</option>
           <option value="16" <?php if($ps_qty==16){ ?>selected="selected"<?php } ?>>16</option>
           <option value="17" <?php if($ps_qty==17){ ?>selected="selected"<?php } ?>>17</option>
           <option value="18" <?php if($ps_qty==18){ ?>selected="selected"<?php } ?>>18</option>
           <option value="19" <?php if($ps_qty==19){ ?>selected="selected"<?php } ?>>19</option>
           <option value="20" <?php if($ps_qty==20){ ?>selected="selected"<?php } ?>>20</option>

           </select>
            <div class="buycss"> 
               <!-- <a class="btn btn-sm btn-primary sf-review-btn" href="{ route("checkout",["slug" => $item->slug]) }}" >Buy Now</a> -->

               <a onclick="return apply_qty();" class="btn btn-sm btn-primary sf-review-btn" href="{{ url('/').'/'.$item->slug.'/checkout' }}" >Buy Now</a>
            </div>

          </div>

              {{-- <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button> --}}


          </div>
      </div>
    </div>
    
    <div class="row">

      <div class="col-md-12"  data-aos="fade-up" data-aos-delay="300">
          
        </div>

      
      <div class="col-md-8">
        <div id="demo">
           <h2>Similer Products</h2>
          <div class="">
            <div class="row">
              <div class="col-md-12">
                <div class="block-13">
            <div class="owl-carousel similer_prod">
            @foreach($finalSimilarItems as $similarItem)
              <div class="d-block d-md-flex listing vertical">
                <div class="item">
                    <div class="ctimg_show">
                      <img src="{{ $similarItem['imageURL'] }}" alt="">
                    </div>
                    <div class="smilrProdct">
                      <a href="{{ route("productDetails",["slug" => $similarItem['slug']]) }}"> {{ $similarItem['name'] }}</a>
                      <div class=" text-warning">
                        <?php
                            if($similarItem['avgRating'] == 1)
                    echo '<i class="fa fa-star"></i>';
                  else if($similarItem['avgRating'] == 2){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($similarItem['avgRating'] == 3){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($similarItem['avgRating'] == 4){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($similarItem['avgRating'] == 5){
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                    echo '<i class="fa fa-star"></i>';
                  }else if($similarItem['avgRating'] == '0'){
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>';
                    echo '<i class="fa fa-star-o"></i>'; 
                  }
                        ?>
                      </div>
                    <p>{{ $similarItem['price'] }}</p>
                    </div>
                  </div>
              </div>
              @endforeach         
            </div>
          </div>
              </div>
            </div>
          </div>            
        </div> 

        <div class="row for-Ratingsec">
          <div class="col-sm-12">
            <div class="rating-block">
              <h4>Average product rating</h4>
              <h2 class="bold padding-bottom-7">{{$userRatingInitial}} <small>/ 5</small></h2>
              <?php
                  if($userRating == 1){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
            }else if($userRating == 2){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }else if($userRating == 3){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';

                  }else if($userRating == 4){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }else if($userRating == 5){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }else if($userRating == '0'){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
            }
              ?>
              {{-- <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button> --}}
            </div>
          </div>     
        </div>      
        
        <div class="row">
          <div class="col-sm-12">
            @if(count($ratingReviewData))
            <div class="review-block">
              
                <?php $i = 1; ?>
                @foreach ($ratingReviewData as $data)
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="bcx_lft">
                          @if(!empty($data->profile_image))
                            <img src="{{url('public/uploads/user/'.$data->profile_image)}}" class="img-rounded" height="60" width="60">
                          @else
                            <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                          @endif
                        </div>
                        <div class="bcx_rg">
                          <div class="review-block-name">{{$data->name.$data->lname}}</div>
                          <div class="review-block-date">{{date('l d, Y', strtotime ( $data->created_at ))}} <br> {{Helper::calculate_time_span($data->created_at)}}</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="review-block-rate review-blockOne">
                          <?php
                  if($data->rating == 1){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
            }else if($data->rating == 2){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }else if($data->rating == 3){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';

                  }else if($data->rating == 4){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }else if($data->rating == 5){
                    echo '<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>
              <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              </button>';
                  }
              ?>
                        </div>
                        <div class="review-block-title">Customer Review</div>
                        <div class="review-block-description">{{$data->review}}</div>
                      </div>
                    </div>
                    <?php $i++; ?>
                    @if($i != count($ratingReviewData))
                      <hr/>
                    @endif
                @endforeach
            </div>
            @endif
            @if(Auth::guard("user")->check())
              <div class="post-review-box" id="post-review-box">
                <div class="col-md-12">
                  <h2>Write your review</h2>
                  <div id="message-box" style="display:none;">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <span id="message"></span>
                  </div>
                    <form accept-charset="UTF-8" id="review-rating-form" action="javascript:void(0);" method="post">
                        {{csrf_field()}}
                        <input id="ratings-hidden" name="rating" type="hidden"> 
                        <input id="ps-id" name="ps_id" type="hidden" value="{{$item->id}}"> 
                        <input id="type" name="type" type="hidden" value="{{$item->type}}"> 
                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5" required></textarea>    
                        <div class="text-right">
                            <div class="stars starrr" data-rating="0"></div>
                          <!--  <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="">Cancel</a> -->
                          <button class="btn btn-success btn-lg" type="button" id="submit-review">Submit Review</button>
                        </div>
                    </form>
                </div>
              </div>
            @endif
          </div>
        </div>
      </div>

      <div class="col-md-4 sidebar-wrapper">
        <div class="widget widget_text">
          <h4 class="widget-title">Seller Info</h4>              
            <div class="contact-info-widget">
              <div class="single-info fix">
                <h5>Name</h5>
              <p><a href="mailto:hy@convs.ku" target="_blank">{{ $item->vendor->fullname }}</a></p>
              </div>
              <div class="single-info fix">
                <h5>Address</h5>
              <p>{{ $item->vendor->address }}</p>
              </div>
              <div class="single-info fix">
                <h5>E-mail</h5>
                <p><a href="mailto:hy@convs.ku" target="_blank">{{ $item->vendor->email }}</a> / <a href="mailto:fy@convs.ku" target="_blank">{{ $item->vendor->mailing_address }}</a></p>
              </div>
              <div class="single-info fix">
                <h5>Phone</h5>
                <p><a href="tel:{{ $item->vendor->mob_no}}">{{ $item->vendor->mob_no}}</a>   </p>
              </div>
             
            </div>
        </div>
      </div>
            

      </div>
    </div>
</section>



@endsection

@section("scripts")
{{-- styles --}}

<script type="text/javascript">

$('#quantity').on('change', function() {
 
var qty = $(this).val();

$.ajax({
type: "post",
headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
data: {
        "_token": "{{ csrf_token() }}",
        "qty": qty
        },
url: "{{url('/quantity')}}",       

success: function(msg) { 

}
});

}); 

   function apply_qty(){
      var qty = $('#quantity').val();
      $.ajax({
         type: "post",
         headers: {
            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')
         },
         data: {
            "_token": "<?php echo csrf_token(); ?>",
            "qty": qty
         },
         url: "<?php echo url('/quantity'); ?>",       
         success: function(msg) {}
      });
      return true;
   }

   $('#submit-review').on('click', function() {
  $.ajax({
    type: "post",
    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
    data: $("#review-rating-form").serialize(),
    url: "{{url('/save-user-ps-review')}}",       
    success: function(data) {
      $("#message-box").show();
      $("#message-box").removeClass();
      if(data.ok){
        $("#message-box").addClass("alert alert-success alert-dismissable");
        $("#message").text(data.message);
      }else{
        $("#message-box").addClass("alert alert-danger alert-dismissable");
        $("#message").text(data.message);
      }
    }
  });
});

</script>

@endsection