$.validator.addMethod(

    "passwordStrength",

    function (value, element) {  return true;

        if (this.optional(element)) {

            return true;

        } else if (!/[A-Z]/.test(value)) {

            return false;

        } else if (!/[a-z]/.test(value)) {

            return false;

        } else if (!/[0-9]/.test(value)) {

            return false;

        } else if (!/[~^#%$&=!\-@._><+}{[,\]()"':/\\;|\?]/.test(value)) {

            return false;

        }

        return true;

    },

    "Password should be combination of lowercase,uppercase, number and special characters"

);

$(".vendor-firststep").validate({

    ignore: [],

    rules: {

        email: {

            required: true,

            email: true

        },

        password: {

            required: true,

            minlength: 8,

            passwordStrength: true

        },

        confimr_password:{

			equalTo: "#password"

		}

    },

    messages: {

        email: {

            required: "Email address is required",

            email: "Please enter a valid email address"

        },

        password: {

            required: "Password is required"

        }

    },

    submitHandler: function (form) {

        form.submit();

    }

});



$(".user-register").validate({

    ignore: [],

    rules: {

        password: {

            required: true,

            minlength: 8,

            passwordStrength: true

        }

    },

    messages: {

        password: {

            required: "Password is required"

        }

    },

    submitHandler: function (form) {

        form.submit();

    }

});



$(".user-reset-password-form").validate({

    ignore: [],

    rules: {

        password: {

            required: true,

            minlength: 8,

            passwordStrength: true

		},

		password_confirmation:{

			equalTo: "#password"

		}

    },

    messages: {

        password: {

            required: "Password is required"

        }

    },

    submitHandler: function (form) {

        form.submit();

    }

});

$(".merchant-reset-password-form").validate({

    ignore: [],

    rules: {

        password: {

            required: true,

            minlength: 8,

            passwordStrength: true

		},

		password_confirmation:{

			equalTo: "#password"

		}

    },

    messages: {

        password: {

            required: "Password is required"

        }

    },

    submitHandler: function (form) {

        form.submit();

    }

});

