@extends('layouts.heymobie')

@section('styles')

<style type="text/css">
  @media only screen and (max-width: 340px) {
    .cnt_shop a {
      padding: 10px 5px 10px !important;
      font-size: 12px !important;
    }
  }

  .parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

  }

  section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image: url(http://heymobie.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

  }

  #short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

  }

  #sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

  }

  #sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

  }



  .error {

    color: red;

    font-weight: normal;

  }



  .hide {

    display: none;

  }

  span.cnt_shop {
    padding-right: 15px;
  }

  .shopcartd {
    text-align: right;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
  }

  .cnt_shop a {

    text-decoration: none;
    background-color: #231f20;
    border-radius: 3px;
    border: none;
    color: #fff;
    padding: 10px 20px 10px;
    font-size: 14px;
    outline: none;

  }

div#customize {
    padding: 10px;
    font-weight: 700;
}


</style>


<style type="text/css">

  .price-mid span{
font-size: 14px !important;
  }
.price-mid h3 {
    font-weight: 700;
    width: 350px !important;
}
.size-box h3 {
    padding: 1px;
    font-size: 16px;
    font-weight: 700;
}
.price-mid {
    display: flex;
  /*  justify-content: space-between;*/
    align-items: center;
    width: 100%;
    padding-top: 15px;
}
.pro_name {
    display: flex;
    align-items: center;
 
    padding-bottom: 5px;
}
.custome_body{
  padding: 14px;
}
.size-box .form-check {
    position: relative;
    display: block;
    padding-left: 9px;
}
.pro_name span{
  font-size: 14px;
}
.pro_name h3{
  width: 346px !important;
}
.size-box p {
    margin-left: 8px !important;
}
.size-box input{
      margin-top: 6px;
}

.custome_body .form-check p{
font-size: 14px;
margin-bottom: 0px !important;

}
.custome_body label.form-check-label {
    margin-left: 7px;
    display: flex;
  /*  justify-content: space-between;*/
    align-items: center;
}
.custome_body label.form-check-label div{
width: 330px;
  }

.modal-header button {
    position: absolute;
    right: 11px;
    font-size: 25px;
}
.form-check-label p{ float: left; }
.addonpms{ 
}

</style>

@endsection

@section('scripts')

@endsection

@section('content')

<!-- <section id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335" class="parallax-window Serv_Prod_Banner"><div id="subheader"><div id="sub_content" class="animated zoomIn"><h1><span class="restaunt_countrt">View Cart</span></h1> <div id="position"><div class="container"><ul class="brad-home"><li><a href="http://heymobie.com/home">Home</a></li> <li>Product</li></ul></div></div></div></div></section> -->

<section class="section-full checkuot_Sec about-info">

  <div class="container">

    <div class="cart-tab table-responsive">
      <div id="qtyidmsg"></div>
      <table class="shop_table table-bordered">

        <thead>

          <tr>

            <th class="product-name">Image</th>

            <th class="product-name">Product Name</th>

            <th class="product-price">Price</th>

            <th class="product-quantity">Quantity</th>

            <th class="product-subtotal">Total</th>

            <th class="product-name">Delete</th>

          </tr>

        </thead>

        <tbody>

          @foreach($items as $item)

             <?php if($item->addon_type==0){ ?>

              <?php //echo $item->ps_id; ?>

             <?php $count_product =DB::table('cart')->where('addon_product_id', $item->ps_id)->where('addon_type',1)->where('user_id',$user_id)->get(); 

            if(count($count_product)>0){ 

            $proprice = $item->quantity * $item->price; 
            $addonprice = 0; 

            $sproprice = $item->price;
            $saddonprice = 0; 

            foreach($items as $item1){

            if(($item1->addon_type==1) && ($item1->addon_product_id==$item->ps_id)){
         
              $addonprice = $addonprice+($item1->quantity * $item1->price);
              $saddonprice = $saddonprice+($item1->price);

             } 

             }

             $productprice = $proprice+$addonprice;

             $itemprice = $sproprice+$saddonprice; 

             }else{

             $productprice = $item->quantity * $item->price;

             $itemprice = $item->price;

             }
      
             ?>
            <tr class="cart-form-item <?php //if(count($count_product)>0){ echo 'addon_row';}?> addon_row">

          <!-- <tr class="cart-form-item"> -->

          <input type="hidden" name="purl" id="purl" value="{{ url('/').'/checkout' }}">

          <input type="hidden" name="product_id" id="product_id" value="{{ $item->id }}">

          <input type="hidden" name="proprice" id="proprice_{{ $item->id }}" value="{{ $item->price }}">

          <input type="hidden" name="vendor_id" id="vendor_id" value="{{ $vendor_id }}">

          <input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}">

            <td class="product-thumbnail">

              <a href="{{ url('/').'/'.$vendor_id.'/'.$item->slug.'/detail' }}" style="width: 100%; float: left; margin-bottom: 10px;">

                <img width="90" height="90" src="{{ $item->imageURL }}" class="img_thumb" alt="">

              </a>

              <?php if($item->type==0 || $item->type==1){ ?> 

              <div type="button" class="btn btn-primary my-example-model" data-toggle="modal" data-target="#fullHeightModalRight{{$item->ps_id}}" style="background-color: #333; border: 1px solid #333;">Customize</div>
               <!-- <div id="customize" class="cmpop" onclick="customize({{ $item->ps_id }})" style="cursor: pointer;">Customize</div> -->

             <?php } ?>

             </td> 

            <td class="product-name">

              <a href="{{ url('/').'/'.$vendor_id.'/'.$item->slug.'/detail' }}">{{$item->name}}</a>

            </td>

            <td class="product-price">

              <span class="price-amount">

          <span class="price-currencySymbol">$</span><span id="psprice_{{ $item->id }}">{{ number_format( $itemprice, 2) }} </span>
 
              </span>

            </td>

            <td class="product-quantity" style="width:200px;">

              <div class="quantity">

                <input type="hidden" name="remainingqty" id="remainingqty" value="<?php echo $item->pquantity; ?>">

                <input type="hidden" name="cartqtyold" id="cartqtyold" value="<?php echo $item->quantity; ?>">

                <div class="value-button" id="decrease" onclick="decreaseValue({{ $item->id }}, {{ $vendor_id }})" value="Decrease Value">-</div>

                <input disabled="disabled" type="number" id="number_{{ $item->id }}" class="number" value="{{ $item->quantity }}" />

                <div class="value-button" id="increase" onclick="increaseValue({{ $item->id }}, {{ $vendor_id }})" value="Increase Value">+</div>

              </div>

            </td>

            <td class="product-subtotal">

              <span class="price-amount">

                <span class="price-currencySymbol">$</span><span id="totalprice_{{ $item->id }}">{{ number_format( $productprice, 2) }} </span>

              </span>

            </td>

            <td class="product-remove">

              <a href="javascript:void(0)" class="remove" title="" onclick="return remove_prod({{ $item->id }},{{ $vendor_id }});"><i class="fa fa-times" aria-hidden="true"></i></a>

            </td>

          </tr>
      

            <?php } ?>

          @endforeach

          <?php $vendor = DB::table('shopping_cart_status')->where('vendor_id', $vendor_id)->first();

          //echo"hii";print_r($vendor);die;
          ?>
        </tbody>

      </table>

      <div class="shopcartd">
        <span class="cnt_shop"><a href="{{ url()->previous() }}">Back</a></span>


        <span class="cnt_shop"><a href="<?php echo url('/'); ?>">Continue Shopping</a></span>

        <!-- href="javascript:history.back()" if(!empty($vendor)){ }-->

        <?php if (!empty($vendor) && $vendor->status == "0") { ?>

          <input type="submit" class="button_right disabled" onclick="return proceed_checkout();" name="update_cart" value="Check Out" disabled style="background: #999999">
          <br>

        <?php } else { ?>

          <?php if(Auth::guard("user")->check()) { ?>

            <input type="submit" class="button_right" onclick="return proceed_checkout();" name="update_cart" value="Check Out">

          <?php }elseif(!empty(Session::get('guest_id'))){ ?>

          <input type="submit" class="button_right" onclick="return proceed_checkout();" name="update_cart" value="Check Out">

         <?php }else{ ?>

            <input type="submit" class="button_right" onclick="return checkout_btn();" name="update_cart" value="Check Out">

          <?php } ?>

        <?php } ?>

      </div>

      <?php if (!empty($vendor) && $vendor->status == "0") { ?> <div class="msgornot" style="float: right;margin-top: 5px;">Sorry, we are not taking the order.</div> <?php } ?>


    </div>

  </div>

</section>
        
        <?php foreach($items as $item){ ?>

             <?php if($item->addon_type==0){ ?>

              <?php $sproprice = 0; ?>

             <?php $count_product =DB::table('cart')->where('addon_product_id', $item->ps_id)->where('addon_type',1)->where('user_id',$user_id)->get(); 

            if(count($count_product)>0){ 

            $proprice = $item->quantity * $item->price; 
            $addonprice = 0; 

            $sproprice = $item->price;
            $saddonprice = 0; 

            foreach($items as $item1){

            if(($item1->addon_type==1) && ($item1->addon_product_id==$item->ps_id)){
         
              $addonprice = $addonprice+($item1->quantity * $item1->price);
              $saddonprice = $saddonprice+($item1->price);

             } 

             }

             $productprice = $proprice+$addonprice;

             $itemprice = $sproprice+$saddonprice; 

             }else{

             $productprice = $item->quantity * $item->price;

             $itemprice = $item->price;

             $sproprice = $item->price;

             }
      
             ?>

<!-- Full Height Modal Right -->
<div class="modal fade right" id="fullHeightModalRight{{$item->ps_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
  <div class="modal-dialog mt-5 " role="document">


    <div class="modal-content">
      <div class="modal-header p-0" style="border-bottom: 1px dashed #FFF;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-header" style="border-bottom: 1px dashed #ccc;">
     <div class="price-mid">
        <h3 class="modal-title w-100" id="myModalLabel">Customize &nbsp; &nbsp; "{{$item->name}}"</h3>
         <span>$ <span id="totalpaddon_{{$item->id}}">{{$itemprice}}</span>
              </span>
      </div>
      <!--   <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button> -->
      </div>
   <div class="modal-body custome_body">
   <div class="pro_name">
     <h3>{{$item->name}} </h3> 
    <span>$ {{$sproprice}}</span>  
     </div>
     <?php

     $addonItems = DB::table('product_service')->where("id",$item->ps_id)->value('category_addon_id'); 

     $addonItemids = json_decode($addonItems); 

     if(!empty($addonItemids)){ ?>

     <div class="size-box">

     <?php  

     foreach ($addonItemids as $key => $value) {
  
      $category = DB::table('product_service_category_addon')->where("id",$value)->value('category');
      ?>

    <h3>{{$category}}<small></small> </h3> 

    <?php   

    $addon = DB::table('product_service')->where("category_addon_id",$value)->where("addon_type",1)->get();

  foreach ($addon as $key => $value) { ?>

   <?php $checkaddon = DB::table('cart')->where("ps_id",$value->id)->where("addon_product_id",$item->ps_id)->where("addon_type",1)->where('user_id',$user_id)->count(); ?>

  <div class="form-check">
  <label class="form-check-label">
  <div>

  <input type="checkbox" class="form-check-input mr-1" onclick="cusption({{$value->id }},{{$item->ps_id}},{{$item->id}})" name="cusption" value="{{$value->id}}"<?php if($checkaddon>0){ echo "checked"; }?>> <p>{{$value->name}}</p>

</div>
  <span class="addonpms">$ {{$value->price}}</span>
  </label>
  </div> 

  <?php  }    

      }  

  echo "</div>";

     }

  ?>   

       <!--  <div class="modalrow">

            <span class="product-name">
              <a href="{{ url('/').'/'.$vendor_id.'/'.$item->slug.'/detail' }}">{{$item->name}}</a>
            </span>

            <span class="product-price">
            <span class="price-amount">
            <span class="price-currencySymbol"> $</span>{{$itemprice}}
              </span>
            </span>
        
        </div> -->

        <?php /* foreach($items as $item1){ ?>

        <?php if(($item1->addon_type==1) && ($item1->addon_product_id==$item->ps_id)){ ?>


          <a href="#">{{$item1->name}}</a>

  
            <span class="product-price">

              <span class="price-amount">

                <span class="price-currencySymbol">$</span>{{$item1->price}}

              </span>

            </span>

            <span class="product-remove">

              <a href="javascript:void(0)" class="remove" title="" onclick="return remove_prod({{ $item1->id }},{{ $vendor_id }});"><i class="fa fa-times" aria-hidden="true"></i></a>

            </span>     
         
   
            <?php  } ?>

            <?php } */ ?>


      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Full Height Modal Right -->

<!-- Modal -->

<?php } ?>
<?php } ?>

@endsection

@section('scrips')


@endsection

<script type="text/javascript">

  
  function cusption(addon_id, ps_id, item_id) {

    var qty = 1;//$('#quantity').val(); // 

    var product_id = addon_id;

    var vendor_id = $('#vendor_id').val(); 

    var addon_product_id = ps_id;

    var itemid = item_id;

    var addon_type = 1;

    var purl = $('#purl').val();

    $.ajax({

      type: "post",

      headers: {

        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

      },

      data: {

        "_token": "{{ csrf_token() }}",

        "qty": qty,

        "product_id": product_id,

        "vendor_id": vendor_id,

        "addon_type": addon_type,

        "addon_product_id": addon_product_id

      },

      url: "{{url('/cartoptionadd')}}",

      success: function(msg) {

      var totalprice = msg.total; 
      var pprice = msg.pprice; 

      $('#totalpaddon_'+itemid).html(pprice); 
      $('#psprice_'+itemid).html(pprice);
      $('#totalprice_'+itemid).html(totalprice);

      }

    });

  }


  function customize(ps_id) {

  $("."+"addondp"+ps_id).toggle('slow');

  }
   
  function checkout_btn() {

    var curl = $('#cloginurl').attr('href');

    window.location.href = curl;

    //alert('Info! You need to be login to perform this action.');

  }
</script>

<script>
  function proceed_checkout() {

    var purl = $('#purl').val();

    window.location.replace(purl);

  }



  function increaseValue(product_id, vendor_id) {

    var qty = $('#session_qty').html();

    var int_qty = parseInt(qty) + parseInt(1);

    //$('#session_qty').text(int_qty);
    var quantity1 = qty.replace("(", "");
    var quantity2 = parseInt(quantity1.replace(")", "")) + 1;
    var remainqty = parseInt($('#remainingqty').val());
    //alert(remainqty +''+ quantity2);
    if (remainqty < quantity2) {
      $('#qtyidmsg').html('<h3 style="color:red"><b>Product available Quantity is only : ' + remainqty + '<b></h3>');
      // return false; 
    } else {


      var value = parseInt(document.getElementById('number_' + product_id).value, 10);

      value = isNaN(value) ? 0 : value;

      value++;

      document.getElementById('number_' + product_id).value = value;



      update_quantity(value, product_id, vendor_id);
    }

  }



  function decreaseValue(product_id, vendor_id) {



    var value = parseInt(document.getElementById('number_' + product_id).value, 10);



    if (value != 1) {

      $('#qtyidmsg').hide();

      var qty = $('#session_qty').text();

      var int_qty = parseInt(qty) - parseInt(1);

      //$('#session_qty').text(int_qty);


      value = isNaN(value) ? 0 : value;

      value < 1 ? value = 1 : '';

      value--;

      document.getElementById('number_' + product_id).value = value;

      update_quantity(value, product_id, vendor_id);

    }

  }



  function update_quantity(quantity, product_id, vendor_id) {

    $.ajax({

      type: "post",

      headers: {

        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

      },

      data: {

        "_token": "{{ csrf_token() }}",

        "qty": quantity,

        "product_id": product_id,

        "vendor_id": vendor_id,

        "update": 1,

      },

      url: "{{url('/quantity')}}",

      success: function(msg) {

        $('#session_qty').text('(' + msg.cartcount + ')');

        //var proprice = $('#proprice_' + product_id).val();
        //var total = proprice * quantity;
        //$('#totalprice_' + product_id).text(total.toFixed(2));

         window.location.reload();

      }

    });

  }



  function remove_prod(product_id, vendor_id) {

   // alert(product_id);

    $.ajax({

      type: "post",

      headers: {

        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

      },

      data: {

        "_token": "{{ csrf_token() }}",

        "product_id": product_id,

        "vendor_id": vendor_id,

        "delete": 1

      },

      url: "{{url('/quantity')}}",

      success: function(msg) {

        window.location.reload();

      }

    });

  }

</script>