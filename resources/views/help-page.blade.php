@extends("layouts.grambunny")

@section("content")

<section id="ordr-grab-sec">

  <div class="container">   

    <div class="row  wow fadeInUp" data-wow-delay="0.1s">

      <div class="col-lg-12">

        <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">

          <!-- <span>Grab it features //</span> -->

          <h2>{{@$page_data->page_title}}</h2>
 
          <p>{{@$page_data->sub_title}}</p>


        </div>

      </div>

    </div>

  </div>

</section>

<section class="support">

      <!--<div class="container">

        <div class="row">

          <div class="col-lg-8">

            <div class="help-topic-list wow fadeInUp" data-wow-delay="0.1s">

              <div class="row">



                

                <div class="col-lg-12">

            <div class="web-heading wow fadeInUp support-head" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">

          

              <h2>Help Topics</h2>              

            </div>

          </div>



                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.1s">

                    <div class="topic-icon">

                      <i class="fa fa-rocket"></i>

                    </div>

                    <div class="topic-text">

                      <h2>Get started</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>

                

                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.2s">

                    <div class="topic-icon">

                      <i class="fa fa-lightbulb-o"></i>

                    </div>

                    <div class="topic-text">

                      <h2>General info</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>



                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.3s">

                    <div class="topic-icon">

                      <i class="fa fa-shopping-cart"></i>

                    </div>

                    <div class="topic-text">

                      <h2>For Customer</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>



                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.4s">

                    <div class="topic-icon">

                      <i class="fa fa-briefcase"></i>

                    </div>

                    <div class="topic-text">

                      <h2>For Seller</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>



                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.5s">

                    <div class="topic-icon">

                      <i class="fa fa-money"></i>

                    </div>

                    <div class="topic-text">

                      <h2>Payment</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>



                <div class="col-lg-6">

                  <div class="help-topic wow fadeInUp" data-wow-delay="0.6s">

                    <div class="topic-icon">

                      <i class="fa fa-mobile"></i>

                    </div>

                    <div class="topic-text">

                      <h2>Mobile App</h2>

                      <p>Learn how to set up your account, make offers, and earn money on the road.</p>

                      <a href="#">Read More</a>

                    </div>

                  </div>

                </div>



              </div>

              

            </div>

          </div>

          <div class="col-lg-4">

            <div class="populr-topics wow fadeInUp" data-wow-delay="0.4s">

              <h2>Populer Topics</h2>

              <p><a href="#">How to set up your account.</a></p>

              <p><a href="#">Duis auete irure dolor.</a></p>

              <p><a href="#">Consequuntur magni dolores.</a></p>

              <p><a href="#">Sed do eiusmod tempor incididunt ut.</a></p>

              <p><a href="#">Exercitation ullamco laboris.</a></p>

            </div>

            <div class="populr-topics wow fadeInUp" data-wow-delay="0.5s">

              <h2>Need Support</h2>

              <p>Can't find the answer you're looking for? Don't worry we're here to help!</p>

              <button class="color-btn">Contact Support</button>

            </div>

          </div>

        </div>

      </div>-->

</section>



<section class="support-text">

	<div class="container">

		<div class="row">

              <div class="col-lg-12 col-xl-12">

            <div class="d-flex dc-py-15 flex-column flex-xl-row">

                <div class="sl-privacydetails">


                    <div class="sl-privacydetails__description">

                    <?php echo $page_data->page_content; ?>

                    </div>

                </div>

               

            </div>
          </div>



				<!--<div class="col-lg-12 col-xl-12">

            <div class="d-flex dc-py-15 flex-column flex-xl-row">

                <div class="sl-privacydetails">

                    <div class="sl-privacydetails__title">

                        <h3>Sed ut perspiciatis unde omnis?</h3>

                    </div>

                    <div class="sl-privacydetails__description">

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis auete irure dolor in reprehenderit in voluptate velit.</p>

                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicoe quia consequuntur magni dolores eos qui</p>

                    </div>

                </div>

               

            </div>

             <div class="sl-privacydetails">

                <div class="sl-privacydetails__title">

                    <h3>Ut enim ad minim veniam</h3>

                </div>

                <div class="sl-privacydetails__description">

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>

                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>

                </div>

            </div>

             <div class="sl-privacydetails">

                <div class="sl-privacydetails__title">

                    <h3>Sed ut perspiciatis</h3>

                </div>

                <div class="sl-privacydetails__description">

                    <p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.</p>

                    <p>Weuia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eostateums qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sitam amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>

                </div>

            </div>

        </div>-->

			</div>



	</div>

</section>





@endsection