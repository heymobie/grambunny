@extends("layouts.grambunny")

@section("styles")

{{-- styles --}}

<link rel="stylesheet" type="text/css" href="css/xzoom.css" media="all" />

<link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">

<style type="text/css">
  .sf-about-box .qtycss .buycss {

    display: block;
  }

  button.btn.btn-sm.btn-primary.sf-review-btn {
    height: 46px;
  }

.texy-c button.btn.btn-sm.btn-primary.sf-review-btn {
    height: auto !important;
  }

  input#promo_code {
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
  }

  .myOrderDtelBtn {
    height: 46px !important;
  }

  .input-group-append button#btnRedeem {
    background: #444648 !important;
    border: 1px solid #444648 !important;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
  }

  .input-group {
    border: 2px solid #444648;
    border-radius: 6px;
    padding: 4px;
  }

  input#quantity {
    height: 29px;
  }

  button.quantity-left-minus.btn-number {
    border: unset;
    background-color: unset;
  }

  input#quantity {
    border: unset;
    border-left: 1px solid #444648;
    border-right: 1px solid #444648;
    text-align: center;
    color: #444648;
  }

  span.input-group-btn {
    border: unset !important;
  }

  button.quantity-right-plus.btn-number {
    border: unset;
    background-color: unset;
    color: #444648;
  }

  span.glyphicon.glyphicon-plus {
    font-size: 16px;
    font-weight: normal !important;
    padding-top: 2px;
  }

  span.glyphicon.glyphicon-minus {
    font-size: 16px;
    padding-top: 2px;
    color: #444648;
  }

  .input-group {}

  @media only screen and (max-width: 640px) {
    .myOrderDtelBtn {
      width: auto !important;
    }
  }

  .review-block {
    background-color: #f5f5f5;
    border: 1px solid #ddd;
    padding: 15px;
    border-radius: 3px;
    margin-bottom: 15px;
    margin-top: 10px;
  }

  /*.myOrderDtelBtn {
    width: 22% ;
}*/
  button.btn.btn-sm.btn-primary.sf-review-btn {
    float: left;
   /* width: 20%;*/
  }

  .myOrderDtelBtn a {

    border: 1px solid #231f20;

    background-color: #231f20;

    color: #fff;

    font-weight: 700;

    border-radius: 25px;

    padding: 8px 25px;

    min-width: 100px;

    cursor: pointer;

    text-decoration: none;

  }

  .myOrderDtelBtn a:hover {

    background-color: #231f20;
    color: #fff;

  }

  .myOrderDtelBtn {
    float: none;
    text-align: center;
    vertical-align: -8px;
    margin-left: 30px;
  }

  #qtyidmsg p {
    margin-top: 5px;
    margin-bottom: -10px !important;
  }

 /* Dropdown css */
 
 .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}

span.optionprice {
    float: right;
    margin-right: 5px;
}

input.optioncls {
    margin-left: 3px;
}

.mmcl p{ text-transform: capitalize; }

.mmcl span{ font-weight: bold; }

.RcutPrice {
    font-size: 14px;
    font-weight: 600;
    width: 138px;
    float: left;
}
.cutPricei {
    width: 228px;
    float: left;
}


/* Dropdown close */

.locationfield i {
    position: relative;
    top: 33px;
    left: 13px;
}
.locationfield {
    float: left;
    width: 100%;
    display: flex;
}
.serch-bar-boc.serch-bar-event {
    width: 43%;
    margin-bottom: 25px;
    position: absolute;
    /* left: -165px; */
    right: 40px !important;
    top: -8px;
    border: 1px solid #f2f2f2;
    padding: 4px 15px;
}
.proPrice b {
    font-family: 'PT Sans Narrow', sans-serif;
    font-size: 21px;
    color: #000;
    letter-spacing: 1px;
}
.about-info .sf-about-box p {
    float: left;
    width: 100%;
    font-size: 14px;
    line-height: 1.4;
    margin-bottom: 0;
    letter-spacing: 0.05em;
    /* font-family: 'PT Sans Narrow', sans-serif; */
    margin-bottom: 2px;
    color: #333333;
}

</style>

@endsection

@section("content")

{{-- content goes here --}}

<?php //$date = date('Y-m-d H:i:s'); echo $date; ?>

<section class="section-full bg-gray about-info" id="sf-provider-info">

  <div class="container">

    <div class="row">

      <div class="col-md-4 product-left">

        <div class="xzoom-container">
          <?php $first_image_full_path = asset('public/uploads/product')."/".$first_image ; ?>
          <?php   if( count($item->images) > 0){ ?>

            <!--
          <div class="img_wdt_product">
            <img class="xzoom" id="xzoom-default" src="{{ $item->images[0]->path }}" xoriginal="{{ $item->images[0]->path }}" />
          </div>
          <div class="xzoom-thumbs">

            @foreach($item->images as $image)

            <a href="{{ $image->path }}"><img class="xzoom-gallery" width="80" src="{{ $image->path }}" xpreview="{{ $image->path }}" title=""></a>

            @endforeach

          </div>
        -->

        <div class="img_wdt_product">
            <?php //$first_image_full_path = asset('public/uploads/product')."/".$first_image ; ?>
            <img class="xzoom" id="xzoom-default" src="{{ $first_image_full_path }}" xoriginal="{{ $first_image_full_path }}" />
          </div>
          <div class="xzoom-thumbs">

            <a href={{$first_image_full_path}}><img class="xzoom-gallery" width="80" src={{$first_image_full_path}} xpreview={{$first_image_full_path}} title=""></a>

            @foreach($item->images as $image)

            <a href="{{ $image->path }}"><img class="xzoom-gallery" width="80" src="{{ $image->path }}" xpreview="{{ $image->path }}" title=""></a>

            @endforeach

          </div>


      <?php }


      else{   ?>
            <div class="img_wdt_product">
            
            <!--
            <img class="xzoom" id="xzoom-default" src="{{asset('public/uploads/product/')}}/product.jpg" xoriginal="{{asset('public/uploads/product/')}}/product.jpg" />
          -->
          <?php if(!empty($first_image_full_path)){
            ?>
            <img class="xzoom" id="xzoom-default" src="{{$first_image_full_path}}" xoriginal="{{$first_image_full_path}}" />
            <?php
          } else{
?>
  <img class="xzoom" id="xzoom-default" src="{{asset('public/uploads/product/')}}/product.jpg" xoriginal="{{asset('public/uploads/product/')}}/product.jpg" />
<?php

          } ?>


          </div>
          <div class="xzoom-thumbs">

          </div>
          <?php
          } 


       ?>
        </div>

        <!-- <div class="sf-provider-des">

            <div class="sf-thum-bx overlay-black-light">

              <img src="https://images-na.ssl-images-amazon.com/images/I/71Xp-K4MMBL._SX569_.jpg" alt="">

            </div>

            <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button>

          </div> -->

      </div>

      <div class="col-md-8 product-rigt">

        <div class="sf-about-box">

          <h2 class="sf-title">{{ $item->name }}</h2>

         <!-- <h6 class="product-title-btype"> UPC# {{ $item->product_code }}</h6> -->

          <div class="star-rating">

            <div class=" text-warning">

              <?php

              //$userRatingInitial = 0;

              //if($totalUserRating != '0')

              //$userRatingInitial = $totalUserRating / count($ratingReviewData);

              //$userRating = round($userRatingInitial);

              $userRatingInitial = $userRating = $item->avg_rating;

              if ($userRating == 1) {

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

              }else if($userRating == 2) {

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

              }else if ($userRating == 3){

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

              }else if ($userRating == 4){

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star-o"></i>';

              }else if ($userRating == 5){

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

                echo '<i class="fa fa-star"></i>';

              }else if ($userRating == '0'){

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';

                echo '<i class="fa fa-star-o"></i>';
              }

              ?>

            </div>

            <span> {{$userRatingInitial}} out of 5 stars | {{ $item->rating_count }} ratings </span>

          </div>

          <!-- <h6 class="product-title-btype"> BRAND: {{ $item->brands }} | TYPE: {{ $item->types }} | THC: {{ $item->potency_thc }}% | CBD: {{ $item->potency_cbd }}%</h6> -->

          <div class="proPrice">

            <?php $fairprice = $item->price + ($item->quantity*1); ?>

            <input type="hidden" name="ditemprice" id="ditemprice" value="{{$item->price}}">

            <input type="hidden" name="ditemqty" id="ditemqty" value="{{$item->quantity}}">

            <p><span class="RcutPrice">Ticket Price :</span><span class="cutPricei"> ${{$item->price}}</span></p>  

            <p><span class="RcutPrice">Ticket Fee :</span><span class="cutPricei"> ${{$item->ticket_fee}}</span></p>   

            <p><span class="RcutPrice">Ticket Service Fee :</span><span class="cutPricei"> ${{$item->ticket_service_fee}}</span></p>     

            <p id="estmiles" style="display: none;"><span class="RcutPrice">Distance : </span><span class="cutPricei"> <span id="estmiletot">1</span> (M)</span></p>

            <p id="estprice" style="display: none;"><span class="RcutPrice">Estimate Price :</span><span class="cutPricei"> $<span id="estpricetot">{{$fairprice}}</span></span></p>

          </div>

          <?php /* if ($item->quantity > 0) { ?>
            <div class="avilblStock">In Stock</div>
          <?php } else { ?>
            <div class="avilblStock">Out of stock</div>
          <?php } */ ?>

          <h6 class="product-title">Booking information</h6>
        
         <p> <span class="RcutPrice">Venue Name :</span>
          <span class="cutPricei"> {{ $item->venue_name }} </span>  </p>

          <p><span class="RcutPrice"> Event Date :</span>
            <span class="cutPricei"> {{ date('m/d/Y', strtotime($item->event_date))  }}</span></p>

            
           <p><span class="RcutPrice">Event Start time :</span>
            <span class="cutPricei"> {{ $item->event_start_time }}</span></p>

           <p><span class="RcutPrice">Event End Time :</span>
            <span class="cutPricei"> {{ $item->event_end_time }}  </span> </p>        
           <p><span class="RcutPrice">Venue Address :</span>
            <span class="cutPricei"> {{ $item->venue_address }}</span></p>

           <p><span class="RcutPrice">Seating Area :</span>
            <span class="cutPricei"> {{ $item->seating_area }}  </span>          
          </p>

  <div class="locationfield">
  <div class="serch-bar-boc serch-bar-event">
    <div class="arival-desination">
      <div class="proPrice">
            <b>Event Description :</b> <p> {{ $item->description }}   </p>        
            <br><b>Event Notes/Remarks :</b> <p> {{ $item->notes_remarks }} </p>         

      </div>
  </div>
    <div class="arival-desination">            
  </div> 

            <div class="buycss">
          <div class="">
          <div class=""> 
          <input type="hidden" name="optionid" id="optionid" value=""> 
          <input type="hidden" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="<?php echo $item->quantity; ?>">

          <span class="instruction">
             <p>Special Instructions</p>
            <textarea id="sinstructions" name="sinstructions" rows="2" cols="50"></textarea >
          </span>
          </div>

          </div>

        </div>            
</div>
</div>


          <!--{!! csrf_field() !!}-->

          <div class="qtycss">
 


            <div class="buycss ">

              <!-- <a class="btn btn-sm btn-primary sf-review-btn" href="{ route("checkout",["slug" => $item->slug]) }}" >Book Ride Now</a> -->

              <input type="hidden" name="product_id" id="product_id" value="{{ $item->id }}">

              <input type="hidden" name="vendor_id" id="vendor_id" value="{{ $vendor_id }}">

              <input type="hidden" name="purl" id="purl" value="{{ url('/').'/cart' }}">

              <?php if ($item->stock == 1) { ?>

                <button onclick="return apply_qty();" class="btn btn-sm btn-primary sf-review-btn"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                  </span> Purchase Ticket</button>

              <?php }else{ ?>

                <button class="btn btn-sm btn-primary sf-review-btn disablebtn" disabled="disabled">Purchase Ticket</button>

              <?php } ?>

              <span class="myOrderDtelBtn"><a href="javascript:history.back()"><span><i class="fa fa-arrow-left" aria-hidden="true"></i>
                  </span>Back to Store</a></span>

            </div>
   
                  
          </div>

          {{-- <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button> --}}


        </div>

      </div>


<!-- 
<div class="locationfield">
  <div class="serch-bar-boc">
    <div class="arival-desination">
    
      <div class="proPrice">
            <br><b>Event Description :</b>  {{ $item->description }}           
            <br><b>Event Notes/Remarks :</b> {{ $item->notes_remarks }}
            

      </div>
  </div>
    <div class="arival-desination">            
  </div>            
</div>
</div> -->


     <input type="hidden" id="startlat" value="">
     <input type="hidden" id="startlong" value="">
     <input type="hidden" id="endlat" value="">
     <input type="hidden" id="endlong" value="">

    </div>

    <div class="row">
      <div class="col-md-12 rating-product">
        <div class="row for-Ratingsec">

          <div class="col-sm-12">
            <h2 style="border-bottom: 1px solid #dddddd; ">Booking rating</h2>
            <div class="rating-block deeprating">

              <h4>Average rating</h4>

              <h2 class="bold padding-bottom-7">{{$userRatingInitial}} <small>/ 5</small></h2>

              <?php

              if ($userRating == 1) {

                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
              } else if ($userRating == 2) {

                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
              } else if ($userRating == 3) {

                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
              } else if ($userRating == 4) {

                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
              } else if ($userRating == 5) {

                echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
              } else if ($userRating == '0') {

                echo '<i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
              }

              ?>



            </div>

          </div>

        </div>



        <div class="row">

          <div class="col-sm-12">

            @if(count($ratingReviewData))

            <div class="review-block">



              <?php $i = 1; ?>

              @foreach ($ratingReviewData as $data)

              <div class="row">

                <div class="col-sm-12">

                  <div class="bcx_lft">

                    @if(!empty($data->profile_image))

                    <img src="{{url('public/uploads/user/'.$data->profile_image)}}" class="img-rounded" height="60" width="60">

                    @else

                    <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">

                    @endif

                  </div>

                  <div class="bcx_rg">

                    <div class="review-block-name">{{$data->name.' '.$data->lname}}</div>

                    <div class="review-block-date">{{date('l d, Y', strtotime ( $data->created_at ))}} <br> {{Helper::calculate_time_span($data->created_at)}}</div>

                  </div>

                </div>

                <div class="col-sm-12 review-blockOne">

                  <div class="review-block-rate deeprevblock">



                    <?php

                    if ($data->rating == 1) {

                      echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
                    } else if ($data->rating == 2) {

                      echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
                    } else if ($data->rating == 3) {

                      echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
                    } else if ($data->rating == 4) {

                      echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';
                    } else if ($data->rating == 5) {

                      echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';
                    }

                    ?>



                  </div>

                  <div class="review-block-title">Customer Review</div>

                  <div class="review-block-description">{{$data->review}}</div>

                </div>

              </div>

              <?php $i++; ?>

              @if($i != count($ratingReviewData))

              <hr />

              @endif

              @endforeach

            </div>

            @endif

            @if(Auth::guard("user")->check())

            <div class="post-review-box" id="post-review-box">

              <div class="col-md-12">

                <h2>Write your review</h2>

                <div class="msg-box">

                  <div id="message-box" style="display:none;">

                    <i class="fa fa-check"></i>

                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                    <span id="message"></span>

                  </div>

                </div>

                <form accept-charset="UTF-8" id="review-rating-form" action="javascript:void(0);" method="post">

                  {{csrf_field()}}

                  <input id="ratings-hidden" name="rating" type="hidden">

                  <input id="ps-id" name="ps_id" type="hidden" value="{{$item->id}}">

                  <input id="type" name="type" type="hidden" value="{{$item->type}}">

                  <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5" required></textarea>

                  <div class="text-right">

                    <div class="stars starrr" data-rating="0"></div>

                    <!--  <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="">Cancel</a> -->

                    <button class="btn btn-success btn-lg" type="button" id="submit-review">Submit Review</button>

                  </div>

                </form>

              </div>

            </div>

            @endif

          </div>

        </div>
      </div>
    </div>
  </div>

</section>


@endsection

@section("scripts")

{{-- styles --}}


<script type="text/javascript">

var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true; 

    $(".optioncls").prop("checked", false);

  }else{
    checkboxes.style.display = "none";
    expanded = false;
  }
}


var last;

document.addEventListener('input',(e)=>{

if(e.target.getAttribute('name')=="coption"){

$('#optionid').val(e.target.getAttribute('value'));

$('#applyoption').show();

if(last)
last.checked=false;
}
e.target.checked=true;
last=e.target;
})


  /* $('#quantity').on('change', function() {

var qty = $(this).val();

$.ajax({

type: "post",

headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

data: {

        "_token": "{{ csrf_token() }}",

        "qty": qty

        },

url: "{{url('/quantity')}}",       



success: function(msg) { 



}

});



}); */



  function apply_qty() {

    var qty = $('#quantity').val(); // 

    var remainqty = parseInt($('#remainingqty').val());

    var cartqtyold = $('#cartqtyold').val();

    var totalqty = parseInt(qty) + parseInt(cartqtyold);

    if (remainqty < totalqty) {

      $('#qtyidmsg').show();
      //alert('Selected quantity not available at this store');

      return false;

    }

    if (remainqty == 0) {
      $('#qtyidmsg').show();
      //alert('Selected quantity not available at this store');

      return false;
    }

    $('#qtyidmsg').hide();

    var product_id = $('#product_id').val();

    var vendor_id = $('#vendor_id').val();

    var purl = $('#purl').val();

    var sinstruction = $('#sinstructions').val();

    $.ajax({

      type: "post",

      headers: {

        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

      },

      data: {

        "_token": "{{ csrf_token() }}",

        "qty": qty,

        "product_id": product_id,

        "vendor_id": vendor_id,

        "sinstruction": sinstruction

      },

      url: "{{url('/quantity')}}",

      success: function(msg) {

        window.location.replace(purl);

      }

    });

  }



  function apply_qty_addon() {

    var qty = 1;//$('#quantity').val(); // 

    var remainqty = parseInt($('#remainingqty').val());

    var cartqtyold = $('#cartqtyold').val();

    var totalqty = parseInt(qty) + parseInt(cartqtyold);


    if (remainqty < totalqty) {

      $('#qtyidmsg').show();
      //alert('Selected quantity not available at this store');

      return false;

    }

    if (remainqty == 0) {
      $('#qtyidmsg').show();
      //alert('Selected quantity not available at this store');

      return false;
    }

    $('#qtyidmsg').hide();

    var product_id = $('#optionid').val();

    var vendor_id = $('#vendor_id').val();

    var purl = $('#purl').val();

    $.ajax({

      type: "post",

      headers: {

        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

      },

      data: {

        "_token": "{{ csrf_token() }}",

        "qty": qty,

        "product_id": product_id,

        "vendor_id": vendor_id

      },

      url: "{{url('/quantity')}}",

      success: function(msg) {

       var crtqty = $('#session_qty').text(); 

       var crtqty1 = crtqty.split("(");
       var crtqty2 = crtqty1[1].split(")");
       var cartqty = parseInt(crtqty2[0])+parseInt(1);

       $('#session_qty').html('('+cartqty+')'); 

       var aocls = '.checkicon';

       $(aocls).show(); 

       $("input.optioncls").prop('disabled', true);

        //window.location.replace(purl);

      }

    });

  }


  $('#submit-review').on('click', function() {

    $.ajax({

      type: "post",

      headers: {
        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
      },

      data: $("#review-rating-form").serialize(),

      url: "{{url('/save-user-ps-review')}}",

      success: function(data) {

        $("#message-box").show();

        $("#message-box").removeClass();

        if (data.ok) {

          $("#message-box").addClass("alert alert-success alert-dismissable");

          $("#message").text(data.message);

        } else {

          var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message">' + data.message + '</span></div>';

          $('.msg-box').html(html);

          $("#message-box").addClass("alert alert-danger alert-dismissable");

          $("#message").text(data.message);

        }

      }

    });

  });
</script>

<!-- quantity script  -->
<script type="text/javascript">
  $(document).ready(function() {

    var quantitiy = 0;
    $('.quantity-right-plus').click(function(e) {

      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      var quantity = parseInt($('#quantity').val());

      // If is not undefined

      var remainqty = $('#remainingqty').val();

      if (quantity < remainqty) {

        $('#quantity').val(quantity + 1);

        $('#qtyidmsg').hide();

      } else {

        $('#qtyidmsg').show();

      }

      // Increment

    });

    $('.quantity-left-minus').click(function(e) {
      // Stop acting like a button
      e.preventDefault();
      // Get the field name
      var quantity = parseInt($('#quantity').val());

      // If is not undefined

      $('#qtyidmsg').hide();

      // Increment
      if (quantity > 1) {
        $('#quantity').val(quantity - 1);
      }
    });

  });
</script>

<script type="text/javascript">

function initAutocomplete() {

  autocomplete = new google.maps.places.Autocomplete(
 
    (document.getElementById('autocomplete')), {
      types: ['geocode']
    });

    alert("Latitude: "+autocomplete[0].geometry.location.lat());
  alert("Longitude: "+autocomplete[0].geometry.location.lng());

} 
 
google.maps.event.addDomListener(window, "load", initAutocomplete);

function initAutocomplete1() {

  autocomplete1 = new google.maps.places.Autocomplete(
 
    (document.getElementById('autocomplete1')), {
      types: ['geocode']
    });

} 
 
google.maps.event.addDomListener(window, "load", initAutocomplete1);


$('#estimateprice').click(function(e) {

var from = $('#autocomplete').val();

var to = $('#autocomplete1').val();

if(from!=='' && to!==''){

var geocoder = new google.maps.Geocoder();

var slatitude='';
var slongitude='';
var elatitude='';
var elongitude='';

geocoder.geocode( {'address': from}, function(results, status) {

  if (status == google.maps.GeocoderStatus.OK) {
    slatitude = results[0].geometry.location.lat();
    slongitude = results[0].geometry.location.lng();
    $('#startlat').val(slatitude);
    $('#startlong').val(slongitude);

   geocoder.geocode( {'address': to}, function(results, status) {

  if(status == google.maps.GeocoderStatus.OK){
    elatitude = results[0].geometry.location.lat();
    elongitude = results[0].geometry.location.lng();
    $('#endlat').val(elatitude);
    $('#endlong').val(elongitude); 

   var unit = "M";

   //var totaldist = calcCrow(slatitude, slongitude, elatitude, elongitude); 

var totaldist = distance(slatitude, slongitude, elatitude, elongitude, unit);

var ditemprice = $('#ditemprice').val();
var ditemqty = $('#ditemqty').val();

var totalp = parseInt(ditemprice)+(parseInt(ditemqty)*parseInt(totaldist)); 

$('#estmiletot').text(totaldist);
$('#estmiles').show();

$('#estpricetot').text(totalp);
$('#estprice').show();
  
  } 

  }); 

  } 

  });  
}

});


function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
       // if (unit=="N") { dist = dist * 0.8684 }
        return Math.round(dist);
    }
}



    function calcCrow(lat1, lon1, lat2, lon2) 
    {
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
    }

    // Converts numeric degrees to radians
    function toRad(Value) 
    {
        return Value * Math.PI / 180;
    }

</script>

@endsection