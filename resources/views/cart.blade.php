@extends('layouts.grambunny')

@section('styles')

<style type="text/css">
@media only screen and (max-width: 340px) {
  .cnt_shop a {
    padding: 10px 5px 10px !important;
    font-size: 12px !important;
}
}
.parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

}

section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image:url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

}

#short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

}

#sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

}

#sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

}



.error{

  color: red;

  font-weight: normal;

}



.hide{

  display: none;

}

span.cnt_shop {
    padding-right: 15px;
}

.shopcartd {
    text-align: right;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
}
.cnt_shop a{

    text-decoration: none;
    background-color: #231f20;
    border-radius: 3px;
    border: none;
    color: #fff;
    padding: 10px 20px 10px;
    font-size: 14px;
    outline: none;

}


</style>



@endsection



@section('scripts')



@endsection



@section('content')



<!-- <section id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335" class="parallax-window Serv_Prod_Banner"><div id="subheader"><div id="sub_content" class="animated zoomIn"><h1><span class="restaunt_countrt">View Cart</span></h1> <div id="position"><div class="container"><ul class="brad-home"><li><a href="https://www.grambunny.com/home">Home</a></li> <li>Product</li></ul></div></div></div></div></section> -->



<section class="section-full checkuot_Sec about-info">

	<div class="container">

    <div class="cart-tab table-responsive">
      <div id="qtyidmsg"></div>
      <table class="shop_table table-bordered">

        <thead>

          <tr>

              <th class="product-name" >Image</th>

              <th class="product-name" >Product Name</th>

              <th class="product-price">Price</th>

              <th class="product-quantity">Quantity</th>

              <th class="product-subtotal">Total</th>

              <th class="product-name" >Delete</th>

          </tr>

        </thead>

        <tbody>



          @foreach($items as $item)

          <tr class="cart-form-item">

            <input type="hidden" name="purl" id="purl" value="{{ url('/').'/checkout' }}">

            <input type="hidden" name="product_id" id="product_id" value="{{ $item->id }}">

            <input type="hidden" name="proprice" id="proprice_{{ $item->id }}" value="{{ $item->price }}">

            <input type="hidden" name="vendor_id"  id="vendor_id"  value="{{ $item->vendor_id }}">


            <td class="product-thumbnail">

              <a  href="{{ url('/').'/'.$item->vendor_id.'/'.$item->slug.'/detail' }}">

                <img width="90" height="90" src="{{ $item->imageURL }}" class="img_thumb" alt="">

              </a>

            </td>

            <td class="product-name">

              <a href="{{ url('/').'/'.$item->vendor_id.'/'.$item->slug.'/detail' }}">{{$item->name}}</a>

            </td>

            <td class="product-price">

              <span class="price-amount">

                <span class="price-currencySymbol">$</span>{{$item->price}}

              </span>

            </td>

            <td class="product-quantity" style="width:200px;">

            <div class="quantity">

              <input type="hidden" name="remainingqty" id="remainingqty" value="<?php  echo $item->pquantity; ?>">

          <input type="hidden" name="cartqtyold" id="cartqtyold" value="<?php  echo $item->quantity; ?>">

            <div class="value-button" id="decrease" onclick="decreaseValue({{ $item->id }}, {{ $item->pquantity }})" value="Decrease Value">-</div>

            <input disabled="disabled" type="number" id="number_{{ $item->id }}" class="number" value="{{ $item->quantity }}" />

            <div class="value-button" id="increase" onclick="increaseValue({{ $item->id }}, {{ $item->pquantity }})" value="Increase Value">+</div>

            </div>

            </td>

            <td class="product-subtotal">

              <span class="price-amount">

              <span class="price-currencySymbol">$</span><span id="totalprice_{{ $item->id }}">{{ number_format( $item->quantity * $item->price, 2) }} </span>

              </span>

            </td>

                        <td class="product-remove">

              <a href="#" class="remove" title="" onclick="return remove_prod({{ $item->id }},{{ $item->vendor_id }});"><i class="fa fa-times" aria-hidden="true"></i></a>

            </td>

          </tr>

          @endforeach

          <?php $vendor = DB::table('shopping_cart_status')->where('vendor_id', $item->vendor_id)->first();

            $vendorsDetials=DB::table('vendor')->where("vendor_id",$item->vendor_id)->where("login_status",1)->groupBy('vendor_id')->get();

            //echo"hii";print_r($vendor);die;
           ?>
        </tbody>

      </table>

      <div class="shopcartd">
          <?php $baseurl = url('/'); ?>


         <span class="cnt_shop"><a href="<?php echo $baseurl;?>/{{$vendorsDetials[0]->username}}/store">Continue Shopping</a></span>
      <!-- <span class="cnt_shop"><a href="<?php echo url('/');?>">Continue Shopping</a></span> -->

      <!-- href="javascript:history.back()" if(!empty($vendor)){ }-->

      <?php if(!empty($vendor) && $vendor->status== "0"){ ?>

        <input type="submit" class="button_right disabled" onclick="return proceed_checkout();" name="update_cart" value="Check Out" disabled style="background: #999999">
        <br>

      <?php }else{ ?>

      <?php if(Auth::guard("user")->check()){ ?>

        <input type="submit" class="button_right" onclick="return proceed_checkout();" name="update_cart" value="Check Out">

        <?php }else{ ?>

        <input type="submit" class="button_right 7788" onclick="return checkout_btn();" name="update_cart" value="Check Out">

        <?php } ?>

      <?php } ?>

     </div>

    <?php if(!empty($vendor) && $vendor->status== "0"){ ?> <div class="msgornot" style="float: right;margin-top: 5px;">Sorry, we are not taking the order.</div> <?php } ?>


    </div>

  </div>

</section>



<!-- Modal -->

@endsection

@section('scrips')


@endsection

<script type="text/javascript">

  function checkout_btn(){

    var curl =  $('#cloginurl').attr('href');

    window.location.href = curl;

    //alert('Info! You need to be login to perform this action.');

  }


</script>

<script>


  function proceed_checkout(){

    var purl = $('#purl').val();

    window.location.replace(purl);

  }



  function increaseValue(product_id, remainqty) {

    //var qty =  $('#session_qty').html();

    var qty = document.getElementById('number_'+product_id).value;

    var vendor_id = $('#vendor_id').val();

    var int_qty = parseInt(qty) + parseInt(1);

    //$('#session_qty').text(int_qty);
    //var quantity1= qty.replace("(", "");
    var quantity2 = parseInt(qty) + parseInt(1);
    //var remainqty = parseInt($('#remainingqty').val());
   //alert(remainqty +''+ quantity2);
    if(remainqty < quantity2){
      $('#qtyidmsg').html('<h3 style="color:red"><b>Product available Quantity is only : '+ remainqty +'<b></h3>');
     // return false;
    }else{

    var value = parseInt(document.getElementById('number_'+product_id).value, 10);

    value = isNaN(value) ? 0 : value;

    value++;

    document.getElementById('number_'+product_id).value = value;

    update_quantity(value,product_id, vendor_id);
  }

}



function decreaseValue(product_id, remainqty) {

  var value = parseInt(document.getElementById('number_'+product_id).value, 10);

  var vendor_id = $('#vendor_id').val();

  if( value != 1){

     $('#qtyidmsg').hide();

    var qty =  $('#session_qty').text();

    var int_qty = parseInt(qty) - parseInt(1);

    //$('#session_qty').text(int_qty);


    value = isNaN(value) ? 0 : value;

    value < 1 ? value = 1 : '';

    value--;

    document.getElementById('number_'+product_id).value = value;

    update_quantity(value,product_id, vendor_id);

  }

}



function update_quantity(quantity, product_id, vendor_id){

  $.ajax({

         type: "post",

     headers: {

        'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

     },

     data: {

        "_token": "{{ csrf_token() }}",

        "qty": quantity,

        "product_id": product_id,

        "vendor_id": vendor_id,

        "update" : 1,

     },

     url: "{{url('/quantity')}}",

     success: function(msg) {

        $('#session_qty').text('('+msg.cartcount+')');

        var proprice = $('#proprice_'+product_id).val();

        var total =  proprice * quantity;

        $('#totalprice_'+product_id).text(total.toFixed(2));

    }

  });

}



function remove_prod( product_id, vendor_id){

      $.ajax({

         type: "post",

         headers: {

            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

         },

         data: {

            "_token": "{{ csrf_token() }}",

            "product_id": product_id,

            "vendor_id": vendor_id,

            "delete" : 1

         },

         url: "{{url('/quantity')}}",

         success: function(msg) {

            window.location.reload();

         }

      });

}



</script>