@extends("layouts.grambunny")
@section("styles")

@endsection
@section("content")
<link rel="stylesheet" type="text/css" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/home.css")}}?var=<?php echo rand();?>">
<div class=" top_search_sec overlay">
   <div class="container">
      <div class="row">
      	<div class="bg_color-serach">
         <div class="col-md-9">
            <form class="home_page_banner">
               <div class="col-md-6">
                  <input type="text" name="" class="form-control divider_rght" placeholder="Products,retailers,brands and more">
                  <span class="serach_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
               </div>
               <div class="col-md-6">
                  <input type="text" name="" class="form-control mb_mg_top" placeholder="Chino Hills, CA" style="border-left:1px solid #232f3e !important;">
                  <span class="serach_icon mb_icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
               </div>
            </form>
         </div>
         <div class="col-md-3 mb_list_view">
            <ul class="rgt_map_list">
          
               <li class="map-view"><a href="" class="active_map">Map View</a></li>
                 	<li class="list_view" style="border-left: 1px solid #ddd;" ><a href="">List View</a></li>
               
            </ul>
         </div>
     </div>
      </div>
   </div>
</div>
<!-- <category secttion start> -->
<div class="container">
   <div class="row">
      <div class="col-md-12">
      <div class="category_sect desktop-view_homepage_cat_sect">
         <ul>
            <li class="category-active"><a href="#"><input type ="radio" value="Flowers"><span>Flowers </span></a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Pre-Rolls</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Vaporizers </span></a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Concentrates</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Edibles</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Topicals</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>CBD</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Accessories</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Fashion</span> </a></li>
            <li><a href="#"><input type ="radio" value="Flowers"><span>Others</a></li>
         </ul>
      </div>
     <!--  mobile view cate. menu statrt -->
     <div class="w3-dropdown-click mobile-view-homepage_cate_section">
  <button onclick="myFunction()" class="w3-btn">Category </button>
  <div id="Demo" class="w3-dropdown-content w3-card-4">
   <ul>
            <li><a href="#">Flowers </a></li>
            <li><a href="#">Pre-Rolls </a></li>
            <li><a href="#">Vaporizers </a></li>
            <li><a href="#">Concentrates </a></li>
            <li><a href="#">Edibles </a></li>
            <li><a href="#">Topicals </a></li>
            <li><a href="#">CBD </a></li>
            <li><a href="#">Accessories </a></li>
            <li><a href="#">Fashion </a></li>
            <li><a href="#">Others</a></li>
         </ul>
  </div>
</div>
<!-- mobile view menu script -->
<script>
function myFunction() {
    var x = document.getElementById("Demo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>
   </div>
    </div>
</div>
<!-- <category secttion end> -->
<!-- <map secttion start> -->
<div class="container">
   <div class="row">
      <div class="col-md-12">
      <div class="map">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d105863.47290768065!2d-117.73777766236465!3d33.98653494925544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c333293d7c8927%3A0x3fdade98a0dd6785!2sChino%2C%20CA%2C%20USA!5e0!3m2!1sen!2sin!4v1630667973846!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      </div>
   </div>
</div>
<!-- <map secttion end> -->
<!-- <baaner ands & product secttion start> -->
<div class="container">
   <div class="row">
      <div class="b_add">
         <div class="col-md-6">
            <div class="adds_banner">
               <img src="{{asset("public/uploads/advertisement/".$advertisement1->image)}}"> 
            </div>
         </div>
         <div class="col-md-6 ads_rgt">
            <div class="adds_banner">
               <img src="{{asset("public/uploads/advertisement/".$advertisement2->image)}}"> 
            </div>
         </div>
      </div>
      <div class="product-listing-section" data-aos="fade-up" data-aos-delay="200">
         <div class="row row_listing">
            <div class="col-md-12">
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr1.jpg">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                     <span>5.0(2)</span>
                  </div>
                 
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr2.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr3.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr4.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr5.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
         <!-- </div>
         <div class="row row_listing"> -->
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr1.jpg">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                     <span>5.0(2)</span>
                  </div>
                 
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr2.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr3.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr4.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
            <div class="col-md-2 listing_width_eq">
               <a href="#">
               <div class="prduc_img">
                  <img src="https://www.grambunny.com/public/assets/images/pr5.png">
               </div>
               <div class="wish_list">
                  <span><i class="fa fa-heart" aria-hidden="true"></i></span>
               </div>
               <div class="descrption_set">
                  <div class="first_des">
                     <p class="f_left">Pre roll</p>
                     <p class="f_right">20mi</p>
                  </div>
                  <div class="second_des">
                     <h3>1g King size Pre roll</h3>
                     <p><span class="rgt_chk"><i class="fa fa-check-circle" aria-hidden="true"></i></span><span class="rgt_cont">kushgram</span></p>
                  </div>
                  <div class="rating">
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star checked"></span>
                     <span class="fa fa-star"></span>
                     <span class="fa fa-star"></span>
                  </div>
                
               </div>
            </a>
            </div>
         </div>
      </div>
      </div>
   </div>

</div>
<!-- < baaner ands & product secttion end> -->   
@endsection
@section("scripts")
<!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU&libraries=places&callback=initAutocomplete"></script>-->
@endsection
