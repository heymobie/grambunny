@extends("layouts.app")
@section("styles")
{{-- styles --}}
@endsection

@section("content")
	{{-- content goes here --}}

<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">
    <div id="subheader">
        <!-- <div id="sub_content" class="animated zoomIn">
          	<h1><span class="restaunt_countrt">Category Name</h1>
			    <h6>
        		<input type="submit" id="btnChangeLocation" name="btnChangeLocation" class="btn_1" value="Change Location">
        	</h6>
        </div> -->
    </div>
</section>


<section class="bg-gray seller_ProSer-info">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="sl-appointment">
                <div class="sl-appointment__img">
                    <img src="https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_xl_50287718-600x600.jpg" alt="Image Description">
                </div>
                <div class="sl-appointment__content">
                    <div class="sl-slider__tags">
                        <a href="javascript:void(0);" class="sl-bg-red-orange">Featured</a>
                        <a href="javascript:void(0);" class="sl-bg-green">Verified</a>
                    </div>
                    <h5>Best Cleaning Services In Your Area</h5>
                    <h3>DUSTLY CLEANING SERVICES</h3>
                    <div class="sl-appointment__feature">
                        <div class="sl-featureRating">
                            <div class="text-warning">
                                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                            </div>
                            <em>(1642 Feedback)</em>
                        </div>
                        <div class="sl-appointment__location">
                            <em>Location: <a href="javascript:void(0);">Get Direction</a></em>
                        </div>
                    </div>
                    <div class="sl-detail">
                        <div class="sl-detail__date">
                            <em><i class="fa fa-calendar"></i>Since: Jun 27, 2010</em>
                        </div>
                        <div class="sl-detail__view">
                            <em><i class="fa fa-eye"></i>15,063 Viewed</em>
                        </div>
                        <div class="sl-detail__report">
                            <em><a href="javascript:void(0);"><i class="fa fa-exclamation-triangle"></i><span class="sl-alert-color">Report now</span></a></em>
                        </div>
                        <div class="sl-detail__report">
                            <em><a href="javascript:void(0);"><i class="fa fa-share-alt"></i>Share Profile</a></em>
                        </div>
                    </div>
                </div>
               <!--  <div class="sl-appointment__note">
                    <a href="javascript:void(0);" class="btn sl-btn" data-toggle="modal" data-target="#appointmentPopup">Make Appointment</a>
                </div> -->

                <div class="shared-bx_social">
                  <ul class="share-social-bx">
                      <li class="fb">
                        <a href="javascript:;"> <i class="fa fa-facebook"></i>Share</a>
                      </li>
                      <li class="tw">
                        <a href="javascript:;"> <i class="fa fa-twitter"></i>Share</a>
                      </li>
                      <li class="lin">
                        <a href="javascript:;"> <i class="fa fa-linkedin"></i>Share</a>
                      </li>
                      <li class="pin">
                        <a href="javascript:;"> <i class="fa fa-pinterest"></i>Share</a>
                      </li>
                      <li class="gp">
                        <a href="javascript:;"> <i class="fa fa-google-plus"></i>Share</a>
                      </li>
                  </ul>
                  <div class="viewSeller">
                    <a href="javascript:;">View More</a>
                  </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</section>


<section class="bg-gray seller_ProSer">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="searchResults">


            <div class="row"> 
              <div class="col-md-12 col-sm-12">
                <div class="resto-near">                                
                                <h4>OUR SERVICES</h4>                                
                                </div>                                
                          </div>
              <div class="col-md-3 col-sm-6 equal-col">
                <div class="sf-search-result-girds" id="proid-30">
                                      
                          <div class="sf-featured-top">
                              <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_45198006-600x450.jpg)"></div>

                              <div class="sf-overlay-box"></div>

                              <span class="sf-categories-label"><a href="#">Event Organizer</a></span>

                              <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

                    <a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
                              
                              <div class="sf-featured-info">
                                  <div class="sf-featured-sign">Featured</div>
                                  <div class="sf-featured-provider">Robert M</div>
                                  <div class="sf-featured-address"><i class="fa fa-map-marker"></i>  Ballston Lake, United States </div>
                                   <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                              </div>

                    <a href="" class="sf-profile-link"></a>

                          </div>
                          
                          <div class="sf-featured-bot">
                              <div class="sf-featured-comapny">CLASSIC EVENT</div>
                              <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
                              <div class="btn-group sf-provider-tooltip" >
                      <a href="#">Read More</a>
                    </div>
                          </div>
                          
                      </div>
               </div>
               <div class="col-md-3 col-sm-6 equal-col">
                <div class="sf-search-result-girds" id="proid-30">
                                      
                          <div class="sf-featured-top">
                              <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_56063034-3-600x450.jpg)"></div>

                              <div class="sf-overlay-box"></div>

                              <span class="sf-categories-label"><a href="#">IT/WEB DEVELOPMENT</a></span>

                              <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

                    <a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
                              
                              <div class="sf-featured-info">
                                  
                                  <div class="sf-featured-provider">James Miller</div>
                                  <div class="sf-featured-address"><i class="fa fa-map-marker"></i>   Cambridge, United Kingdom </div>
                                   <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                              </div>

                    <a href="" class="sf-profile-link"></a>

                          </div>
                          
                          <div class="sf-featured-bot">
                              <div class="sf-featured-comapny">AONE THEME WEB</div>
                              <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
                              <div class="btn-group sf-provider-tooltip" >
                      <a href="#">Read More</a>
                    </div>
                          </div>
                          
                      </div>
               </div>
               <div class="col-md-3 col-sm-6 equal-col">
                <div class="sf-search-result-girds" id="proid-30">
                                      
                          <div class="sf-featured-top">
                              <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_38993507-600x450.jpg)"></div>

                              <div class="sf-overlay-box"></div>

                              <span class="sf-categories-label"><a href="#"> INTERIOR DECORATOR</a></span>

                              <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

                    <a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
                              
                              <div class="sf-featured-info">
                                  
                                  <div class="sf-featured-provider">Joshep Desoja</div>
                                  <div class="sf-featured-address"><i class="fa fa-map-marker"></i>  Brooklyn, United States </div>
                                   <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                              </div>

                    <a href="" class="sf-profile-link"></a>

                          </div>
                          
                          <div class="sf-featured-bot">
                              <div class="sf-featured-comapny">RED PLAINS PERSONAL</div>
                              <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
                              <div class="btn-group sf-provider-tooltip" >
                      <a href="#">Read More</a>
                    </div>
                          </div>
                          
                      </div>
               </div>
               <div class="col-md-3 col-sm-6 equal-col">
                <div class="sf-search-result-girds" id="proid-30">
                                      
                          <div class="sf-featured-top">
                              <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_56063034-600x450.jpg)"></div>

                              <div class="sf-overlay-box"></div>

                              <span class="sf-categories-label"><a href="#">IT/WEB DEVELOPMENT</a></span>

                              <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

                    <a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
                              
                              <div class="sf-featured-info">
                                  
                                  <div class="sf-featured-provider">Dayle Stayn</div>
                                  <div class="sf-featured-address"><i class="fa fa-map-marker"></i> Brooklyn, United States </div>
                                   <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                              </div>

                    <a href="" class="sf-profile-link"></a>

                          </div>
                          
                          <div class="sf-featured-bot">
                              <div class="sf-featured-comapny">PIXEL PERFECT WEB</div>
                              <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
                              <div class="btn-group sf-provider-tooltip" >
                      <a href="#">Read More</a>
                    </div>
                          </div>
                          
                      </div>
               </div>
            </div> 


            <div class="row">
              <div class="col-md-12 col-sm-12">
                <div class="resto-near">                                
                                <h4>OUR PRODUCTS</h4>                                
                                </div>                                
                          </div>
              <div class="col-md-3 col-sm-6 equal-col">
                <div class="sl-featuredProducts--post">
                                  <figure>
                                      <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-08.jpg" alt="Image Description">
                                      <figcaption>
                                          <div class="sl-slider__tags">
                                              <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                          </div>
                                          <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                      </figcaption>
                                  </figure>
                                  <div class="sl-featuredProducts--post__content">
                                      <div class="sl-featuredProducts--post__title">
                                          <h6>Kensington Contour 2.0 Backpack</h6>
                                      </div>
                                      <div class="sl-featuredProducts--post__price">
                                          <h5>$12.19</h5>
                                          <h6>$19.99</h6>
                                      </div>
                                      <div class="sl-featureRating">
                                          <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                                          <em>(1887 Feedback)</em>
                                      </div>
                                      <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                      <button class="btn sl-btn">Add To Cart</button>
                                  </div>
                              </div>
              </div>
              <div class="col-md-3 col-sm-6 equal-col">
                <div class="sl-featuredProducts--post">
                                <figure>
                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-06.jpg" alt="Image Description">
                                    <figcaption>
                                        <div class="sl-slider__tags">
                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                        </div>
                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                    </figcaption>
                                </figure>
                                <div class="sl-featuredProducts--post__content">
                                    <div class="sl-featuredProducts--post__title">
                                        <h6>Kensington Contour 2.0 Backpack</h6>
                                    </div>
                                    <div class="sl-featuredProducts--post__price">
                                        <h5>$12.19</h5>
                                        <h6>$19.99</h6>
                                    </div>
                                    <div class="sl-featureRating">
                                        <div class="star-rating">
                          <span class="fa fa-star" data-rating="1"></span>
                          <span class="fa fa-star" data-rating="2"></span>
                          <span class="fa fa-star" data-rating="3"></span>
                          <span class="fa fa-star" data-rating="4"></span>
                          <span class="fa fa-star" data-rating="5"></span>
                          <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                        </div>
                                        <em>(1887 Feedback)</em>
                                    </div>
                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                    <button class="btn sl-btn">Add To Cart</button>
                                 </div>
                              </div>
                          </div>
                          <div class="col-md-3 col-sm-6 equal-col">
                <div class="sl-featuredProducts--post">
                                  <figure>
                                      <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-02.jpg" alt="Image Description">
                                      <figcaption>
                                          <div class="sl-slider__tags">
                                              <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                          </div>
                                          <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                      </figcaption>
                                  </figure>
                                  <div class="sl-featuredProducts--post__content">
                                      <div class="sl-featuredProducts--post__title">
                                          <h6>Kensington Contour 2.0 Backpack</h6>
                                      </div>
                                      <div class="sl-featuredProducts--post__price">
                                          <h5>$12.19</h5>
                                          <h6>$19.99</h6>
                                      </div>
                                      <div class="sl-featureRating">
                                          <div class="star-rating">
                            <span class="fa fa-star" data-rating="1"></span>
                            <span class="fa fa-star" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                          </div>
                                          <em>(1887 Feedback)</em>
                                      </div>
                                      <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                      <button class="btn sl-btn">Add To Cart</button>
                                  </div>
                              </div>
              </div>
              <div class="col-md-3 col-sm-6 equal-col">
                <div class="sl-featuredProducts--post">
                                <figure>
                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-03.jpg" alt="Image Description">
                                    <figcaption>
                                        <div class="sl-slider__tags">
                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                        </div>
                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                    </figcaption>
                                </figure>
                                <div class="sl-featuredProducts--post__content">
                                    <div class="sl-featuredProducts--post__title">
                                        <h6>Kensington Contour 2.0 Backpack</h6>
                                    </div>
                                    <div class="sl-featuredProducts--post__price">
                                        <h5>$12.19</h5>
                                        <h6>$19.99</h6>
                                    </div>
                                    <div class="sl-featureRating">
                                        <div class="star-rating">
                          <span class="fa fa-star" data-rating="1"></span>
                          <span class="fa fa-star" data-rating="2"></span>
                          <span class="fa fa-star" data-rating="3"></span>
                          <span class="fa fa-star" data-rating="4"></span>
                          <span class="fa fa-star" data-rating="5"></span>
                          <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                        </div>
                                        <em>(1887 Feedback)</em>
                                    </div>
                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                    <button class="btn sl-btn">Add To Cart</button>
                                 </div>
                              </div>
                          </div>
            </div>

                </div>
      </div>
    </div>
  </div>
</section>



@endsection

@section("scripts")
{{-- styles --}}
@endsection