@extends("layouts.owner")

@section("other_css")



@endsection

@section("content")

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

		Membership Plans

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/vendor/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>

			<li class="active">Membership</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

        @if(is_null(Auth::guard("vendor")->user()->plan_id))

		<div class="alert alert-success" role="alert">

			<h4 class="alert-heading">Hello <b>{{ Auth::guard("vendor")->user()->fullname }}</b> Welcome to Our services.</h4>
			<p class="mb-0">Please Choose any of these plans to continue using our service.</p>

		</div>

        @elseif(Auth::guard("vendor")->user()->membership["status"]==0)

        <div class="alert alert-warning" role="alert">

            <h4 class="alert-heading">Plan Expire!</h4>

            <p>Hello <b>{{ Auth::guard("vendor")->user()->fullname }}</b> your plan has been expired.</p>

            <hr>

            <p class="mb-0">Please Choose any of these plans to continue using our service.</p>

        </div>

        @elseif(Auth::guard("vendor")->user()->membership["status"]==1)

        <div class="alert alert-info" role="alert">

            <p><b>Info:</b> your plan will expire On {{ date("d M,Y",strtotime(Auth::guard("vendor")->user()->plan_expiry)) }}</p>

        </div>

	   @endif

		<div class="container bg-inner-bx">

    <div class="row">

        <div class="col-md-4" style="padding-left: 10px;">

            <div class="panel panel-primary " >

                <div class="panel-heading">

                    <h3 class="panel-title">

                        Hey Mobie Starter</h3>

                </div>

                <div class="panel-body">

                    <div class="the-price">

                        <h1>

                            $35<span class="subscript">/month</span></h1>

                        

                    </div>

                   {{--  <table class="table">

                        <tr>

                            <td>

                                1 Account

                            </td>

                        </tr>

                        <tr class="active">

                            <td>

                                1 Project

                            </td>

                        </tr>

                        <tr>

                            <td>

                                100K API Access

                            </td>

                        </tr>

                        <tr class="active">

                            <td>

                                100MB Storage

                            </td>

                        </tr>

                        <tr>

                            <td>

                                Custom Cloud Services

                            </td>

                        </tr>

                        <tr class="active">

                            <td>

                                Weekly Reports

                            </td>

                        </tr>

                    </table> --}}

                </div>

                <div class="panel-footer">

                    <a href="{{route("membership.purchase")}}" class="btn btn-success" role="button">Purchase</a>

                 </div>

            </div>

        </div>

       

    </div>

</div>



	</section>

</aside>
<style type="text/css">
    .content {
    overflow: hidden;
}
</style>
@endsection

 