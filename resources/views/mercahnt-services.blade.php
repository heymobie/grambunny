@extends("layouts.grambunny") 

@section("title","Products offered by $merchant->fullname")

@section("content")

{{-- content goes here --}}


<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">

    <div id="subheader"></div>

</section>


<section class="bg-gray seller_ProSer-info">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="sl-appointment">

                <div class="sl-appointment__img">

                <img src="{{ $merchant->profileURL }}" alt="Image Description"/>

                    </div>

                    <div class="sl-appointment__content">

                        <div class="sl-slider__tags ">


              <?php if($merchant->vendor_status==1){ ?>    

              <a href="javascript:void(0);" class="sl-bg-green">Verified</a>

              <?php } ?>

                        </div>

                    <div class="infoh">    

                        <h3>{{ $merchant->fullname }}</h3>

                        <h5>{{ $merchant->business_name }}</h5>

                        <h5>{{ $merchant->mailing_address }}, <?php if(!empty($merchant->address)){ ?>{{ $merchant->address }}, <?php } ?>{{ $merchant->city }}, {{ $merchant->state }} {{ $merchant->zipcode }}</h5> 
                     </div>   

                        <div class="starview">

                        <div class="sl-appointment__feature">

                            <div class="sl-featureRating">

                                <div class="text-warning">

                                    

                                    <?php

                    if($merchant->avg_rating == 1){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                  }else if($merchant->avg_rating == 2){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                  }else if($merchant->avg_rating == 3){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                  }else if($merchant->avg_rating == 4){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                  }else if($merchant->avg_rating == 5){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                  }else if($merchant->avg_rating == '0'){

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                  }

                        ?>



                                </div>

                                <em>({{ $merchant->rating_count }} )</em>

                            </div>

                            {{--

                            <div class="sl-appointment__location">

                                <em

                                    >Location:

                                    <a href="javascript:void(0);"

                                        >Get Direction</a

                                    ></em

                                >

                            </div>

                            --}}

                        </div>

                        <div class="sl-detail">

                            <div class="sl-detail__date">

                                <em

                                    ><i class="fa fa-calendar"></i>Since:

                                    {{ $merchant->created_at->format("M d, Y") }}</em

                                >

                            </div>

                            <div class="sl-detail__view">

                                <em><i class="fa fa-eye"></i>{{ $merchant->views }} Viewed</em>

                            </div>

                            <!--<div class="sl-detail__report">

                                <em

                                    ><a href="javascript:void(0);"

                                        ><i

                                            class="fa fa-exclamation-triangle"

                                        ></i

                                        ><span class="sl-alert-color"

                                            >Report now</span

                                        ></a

                                    ></em

                                >

                            </div>

                            <div class="sl-detail__report">

                                <em

                                    ><a href="javascript:void(0);"

                                        ><i class="fa fa-share-alt"></i>Share

                                        Profile</a

                                    ></em

                                >

                            </div>-->

                        </div>

                    </div>

                    </div>

                    <!--  <div class="sl-appointment__note">

                    <a href="javascript:void(0);" class="btn sl-btn" data-toggle="modal" data-target="#appointmentPopup">Make Appointment</a>

                </div> -->



                    <div class="shared-bx_social">

                        <!--<ul class="share-social-bx">

                            <li class="fb">

                                <a href="javascript:;">

                                    <i class="fa fa-facebook"></i>Share</a

                                >

                            </li>

                            <li class="tw">

                                <a href="javascript:;">

                                    <i class="fa fa-twitter"></i>Share</a

                                >

                            </li>

                            <li class="lin">

                                <a href="javascript:;">

                                    <i class="fa fa-linkedin"></i>Share</a

                                >

                            </li>

                            <li class="pin">

                                <a href="javascript:;">

                                    <i class="fa fa-pinterest"></i>Share</a

                                >

                            </li>

                            <li class="gp">

                                <a href="javascript:;">

                                    <i class="fa fa-google-plus"></i>Share</a

                                >

                            </li>

                        </ul>-->

                        <div class="viewSeller">

                        <a href="{{ route("merchantProfile",["username" => $merchant->username]) }}">View More</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



<section class="bg-gray seller_ProSer">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="">

                    <div class="row">

                        <div class="col-md-12 col-sm-12">

                            <div class="resto-near">

                                <h4>PRODUCTS</h4>

                            </div>

                        </div>

                        <?php ///echo $merchant->profileURL; ?>
           @foreach($items as $item)

            <?php 

            //$imageurls =  DB::table('ps_images')->where("ps_id",$item->id)->where("thumb",1)->get();

            $imageurls =  DB::table('ps_images')->where("ps_id",$item->id)->get();

            ?>

            <?php 

               /* if (!empty($imageurls[0]->name)) {
                    $imgpath = asset("/public/uploads/product")."/".$imageurls[0]->name;
                }else{
                    //$imgpath = asset("/public/uploads/vendor/profile/gU8Hi0Bo8PImrWjk.jpg");
                    $imgpath = $merchant->profileURL;
                } */

             $imgpath = asset("/public/uploads/product")."/".$item->image;

            ?>

            <?php $countpro = ''; ?>

        <?php if($item->vendor_id==0){ 

        $mydetails =  DB::table('admin_merchant_product')->where("product_id",$item->id)->where("vendor_id",$merchant->vendor_id)->get();

        //print_r(count($mydetails));

        if(count($mydetails)>0){ ?>

              
                        <div class="col-md-2 col-sm-6 equal-col">

                        <div class="sl-featuredProducts--post">

                        <figure>

                         <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}"> <img src="{{ $imgpath }}" alt="Image Description"/></a>

                        <figcaption class="fg_mb_view">

                        <div class="sl-slider__tags mb_viwe_slag">

                        <a class="sl-bg-red-orange">{{ $item->subcategoryname }}</a>

                        </div>

                                </figcaption>

                                </figure>

                             <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}">   

                                <div class="sl-featuredProducts--post__content">

                                    <div class="sl-featuredProducts--post__title">

                                        <h6>{{ $item->name }}</h6>

                                    </div>

                                    <div

                                        class="sl-featuredProducts--post__price"

                                    >

                                       <h5>${{ $item->price }} / {{ $item->unit }}</h5>                    

                                    </div>

                                    <div class="sl-featureRating  ">

                                        <div class="star-rating">


                                            <?php

                                                if($item->avg_rating == 1){

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                       

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                }elseif($item->avg_rating == 2){

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';        

                                                }elseif($item->avg_rating == 3){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';           

                                                }elseif($item->avg_rating == 4){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';                

                                                }elseif($item->avg_rating == 5){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>'; 

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';                

                                                }else{

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                

                                                }

                                            ?>

                                            

                                            <input

                                                type="hidden"

                                                name="whatever1"

                                                class="rating-value"

                                                value="2.56"

                                            />

                                        </div>

                                        <em>({{ $item->rating_count }})</em>

                                    </div>


                                </div>

                                 </a>

                            </div>

                        </div>


                    <?php }

                }else{ ?> 

     
                        <div class="col-md-2 col-sm-6 equal-col">

                        <div class="sl-featuredProducts--post">

                        <figure>

                         <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}"> <img src="{{ $imgpath }}" alt="Image Description"/></a>

                        <figcaption class="fg_mb_view">

                        <div class="sl-slider__tags mb_viwe_slag">

                        <a class="sl-bg-red-orange">{{ $item->subcategoryname }}</a>

                        </div>

                                </figcaption>

                                </figure>

                             <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}">   

                                <div class="sl-featuredProducts--post__content">

                                    <div

                                        class="sl-featuredProducts--post__title"

                                    >

                                        <h6>{{ $item->name }}</h6>

                                    </div>

                                    <div

                                        class="sl-featuredProducts--post__price"

                                    >

                                       <h5>${{ $item->price }} / {{ $item->unit }}</h5>

                                         

                                    </div>

                                    <div class="sl-featureRating  ">

                                        <div class="star-rating">





                                            <?php

                                                if($item->avg_rating == 1){

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                       

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                }elseif($item->avg_rating == 2){

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                       

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';        

                                                }elseif($item->avg_rating == 3){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';           

                                                }elseif($item->avg_rating == 4){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';                

                                                }elseif($item->avg_rating == 5){

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>'; 

                                                    echo '<span

                                                        class="fa fa-star"

                                                        

                                                        ></span>';                

                                                }else{

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                    echo '<span

                                                        class="fa fa-star-o"

                                                        

                                                    ></span>';

                                                

                                                }

                                            ?>

                                            

                                            <input

                                                type="hidden"

                                                name="whatever1"

                                                class="rating-value"

                                                value="2.56"

                                            />

                                        </div>

                                        <em>({{ $item->rating_count }})</em>

                                    </div>


                                </div>

                                 </a>

                            </div>

                        </div>

                   <?php }  ?>

                        @endforeach

            </div>

            <div class="text-center" style="margin-top: 20px;">{{$items->links()}}</div>

                </div>

            </div>

        </div>

    </div>

</section>



@endsection @section("scripts")

{{-- styles --}}

@endsection

