@extends('layouts.subadmin')

@section('content')
<style>
.divition, .sub-drage
{
	border: 1px solid #666666;
	overflow:hidden;
}
.sub-drage
{
	margin-bottom: 6px;
	background: #cccccc;
	position:relative;
}
.sub-drage a.menu_edit_link {
	left: 4px;
	position: absolute;
	top: 6px;
	color:#000000;
}
.sub-drage p a {
	display: block;
	padding: 6px 0;
	color:#000;
}
.sub-drage.selected
{
	border:1px solid #3c8dbc;
	background: #666666;
	color: #fff !important;
}
.sub-drage.selected p a
{
	color: #fff !important;
}
.sub-drage p
{
	border-left:1px solid #666666;
	margin:0 0 0 20px;
	padding-left: 5px;
}
.drage
{
	overflow:hidden;
}
.col-sm-4.border
{
	border-right: 1px solid #ccc;
}
.col-sm-8.border {
	border-left: 1px solid #ccc;
}
.my-panel-data .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
{
	background: #3c8dbc;
	color: #fff;
}
.my-panel-data .nav-tabs>li
{
	margin: 0;
}
.my-panel-data .nav-tabs>li>a
{
	border-radius: 4px;
}
.my-panel-data .nav-tabs
{
	border:  none;
}
.my-panel-data .panel-heading
{
	padding: 12px;
	background-color: transparent !important;
	border: none;
}
.my-panel-data .panel-body {
	border-top: 2px solid #3c8dbc;
}
.my-panel-data .panel
{
	border: none;
	border-radius: 0;
	box-shadow: none;
}
.check_input_service .icheckbox_minimal.checked.disabled {
	background-position: -40px 0;
}
</style>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vendor {{$vendor_detail[0]->name.' '.$vendor_detail[0]->last_name }} Profile Page
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Vendor Profile Page</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Vendor {{$vendor_detail[0]->name.' '.$vendor_detail[0]->last_name}} Profile Page</h3>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive">

					@if(Session::has('message'))

					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message')}}
					</div>
					@endif


					<table style="border:1px solid #666666; width:100%">
						<thead>
							<tr>
								<td style="vertical-align:top" >
									<table  width="100%" cellpadding="5" cellspacing="5">
										<tr>
											<td> Name:</td>
											<td><b> {{$vendor_detail[0]->name.' '.$vendor_detail[0]->last_name}}</b></td>
										</tr>
										<tr>
											<td>Contact No:</td>
											<td> {{$vendor_detail[0]->mob_no }}</td>
										</tr>
										<tr>
											<td>Email:</td>
											<td> {{$vendor_detail[0]->email }}</td>
										</tr>
									</table>
								</td>
								<td >
									<table  width="100%" cellpadding="5" cellspacing="5" >

										<tr>
											<td>Address Line1:</td>
											<td> {{$vendor_detail[0]->address }}</td>
										</tr>
													 <!-- <tr>
													 	<td>Address Line2:</td>
													 	<td> {{$vendor_detail[0]->address1 }}</td>
													 </tr> -->
													 <tr>
													 	<td>Suburb:</td>
													 	<td> {{$vendor_detail[0]->suburb }}</td>
													 </tr>
													 <tr>
													 	<td>State:</td>
													 	<td> {{$vendor_detail[0]->state }}</td>
													 </tr>
													 <tr>
													 	<td>PostCode:</td>
													 	<td> {{$vendor_detail[0]->zipcode }}</td>
													 </tr>
													</table>

												</td>
											</tr>
											<tr>
												<td>Status:
													<span style="padding-left:50px; font-weight:bold;">
														@if($vendor_detail[0]->vendor_status=='1')
														Active
														@else
														Inactive
														@endif

													</span></td>
													<td align="right">
														<a class="btn btn-primary" href="{{url('sub-admin/vendor-form/')}}/{{ $vendor_detail[0]->vendor_id }}">Edit</a>   </td>
													</tr>
													<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
													</tr>

												</thead>
												<tbody>

												</tbody>

											</table>

											<div>

											</div>



										</div>



									</div><!-- /.box -->



									<section>
										<div class="row">
											<div class="my-panel-data">
												<div class="col-md-12">
													<div class="panel with-nav-tabs panel-default">
														<div class="panel-heading">
															<ul class="nav nav-tabs">
																<li class="active"><a href="#tab1default" data-toggle="tab">Restaurant</a></li>
																<!--<li><a href="#tab2default" data-toggle="tab">Transport</a></li>-->

															</ul>
														</div>
														<div class="panel-body">
															<div class="tab-content">
																<div class="tab-pane fade in active" id="tab1default">
																	<div class="row" id="ajax_div">
																		<div class="col-sm-12">
																			<div class="divition">
																				<div>
																					<a class="btn btn-primary" href="{{url('sub-admin/vendor-rest-form')}}/{{ $vendor_detail[0]->vendor_id }}">Add Restaurant</a>
																					<br /><br />
																				</div>
																				<div class="row">
																					<div class="col-md-12"  align="center">
																						<div class="box">
																							<div class="box-header">
																								<h3 class="box-title">Restaurants List</h3>
																							</div><!-- /.box-header -->
																							<div class="box-body no-padding">

																								<?php $i=1; ?>
																								<table id="example2" class="table table-bordered table-hover">
																									<tr>
																										<th style="width: 10px">#</th>
																										<th>Name</th>
																										<th>City</th>
																										<th>Status</th>
																									</tr>
																									@if(!empty($rest_list))
																									@foreach($rest_list as $list)
																									<tr>
																										<td>{{$i}}</td>
																										<td><a href="{{url('sub-admin/restaurant_view/')}}/{{ $list->rest_id }}">{{$list->rest_name}}</a></td>
																										<td>{{$list->rest_city}}</td>
																										<td>
																											@if($list->rest_status=='PUBLISHED')
																											<span class="label label-primary">PUBLISHED</span>
																											@elseif($list->rest_status=='ACTIVE')

																											<span class="label label-danger">{{$list->rest_status}}</span>
																											@elseif($list->rest_status=='INACTIVE')

																											<span class="label label-danger">{{$list->rest_status}}</span>
																										@endif</td>
																									</tr>

																									<?php $i++; ?>
																									@endforeach

																									@else
																									<tr>
																										<td colspan="3"> Restaurant Not Found! </td>
																									</tr>
																									@endif

																								</table>


																							</div><!-- /.box-body -->
																						</div><!-- /.box -->

																					</div>

																				</div>

																			</div>
																		</div>
																	</div>
																</div>
																<div class="tab-pane fade" id="tab2default">
																	<div id="Servicearea">
																		<div class="col-xs-12">
																			<div class="box">
																				<div class="box-header">
																					<h3 class="box-title">Transport Listing</h3><br />
																					<div style="margin-top:10px;">
																						<a  href="{{url('sub-admin/transport-form')}}/{{ $vendor_detail[0]->vendor_id }}"  class="btn btn-primary" style="color:#FFFFFF"> Add Transport</a>
																					</div>

																				</div><!-- /.box-header -->
																				<div class="box-body table-responsive">

																					@if(Session::has('serivce_message'))

																					<div class="alert alert-success alert-dismissable">
																						<i class="fa fa-check"></i>
																						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
																						{{Session::get('serivce_message')}}
																					</div>
																					@endif


																					<table id="example2" class="table table-bordered table-hover">
																						<thead>
																							<tr>
																								<th>SrNo</th>
																								<th>Name</th>
																								<th>suburb</th>
																								<th>Status</th>
																							</tr>
																						</thead>
																						<tbody>
																							@if($transport_list)<?php $c=1;?>
																							@foreach($transport_list as $tport )
																							<tr>
																								<td>{{$c++}}</td>
																								<td><a href="{{url('sub-admin/transport_view/')}}/{{ $tport->transport_id }}">{{$tport->transport_name}}</a></td>
																								<td>{{$tport->transport_suburb}}</td>
																								<td>
																									@if($tport->transport_status=='PUBLISHED')
																									<span class="label label-primary">PUBLISHED</span>
																									@elseif($tport->transport_status=='ACTIVE')

																									<span class="label label-danger">{{$tport->transport_status}}</span>
																									@elseif($tport->transport_status=='INACTIVE')

																									<span class="label label-danger">{{$tport->transport_status}}</span>
																									@endif
																								</td>


																							</tr>
																							@endforeach
																							@endif
																						</tbody>
																						<tfoot>
																							<tr>
																								<th>&nbsp;</th>
																								<th>&nbsp;</th>
																								<th>&nbsp;</th>
																								<th>&nbsp;</th>
																							</tr>
																						</tfoot>
																					</table>



																				</div><!-- /.box-body -->
																			</div><!-- /.box -->
																		</div>
																	</div>

																</div>


															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
									<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />


								</div>




							</section><!-- /.content -->
						</aside><!-- /.right-side -->



						@endsection



						@section('js_bottom')

						<!-- jQuery 2.0.2 -->
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
						<!-- jQuery UI 1.10.3 -->
						<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
						<!-- Bootstrap -->
						<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
						<!-- Bootstrap WYSIHTML5 -->
						<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

						<!-- AdminLTE App -->
						<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

						<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
						<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

						<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

						@stop