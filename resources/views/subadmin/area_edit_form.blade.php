@extends('layouts.subadmin')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Edit Area
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Edit Area</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Edit Area</h3>
				</div><!-- /.box-header -->
				<p style="float: left;text-align: center;width: 100%;">
					@if(Session::has('message'))
					{{Session::get('message')}}
				@endif </p>
				<!-- form start -->
				@if(Session::has('errmessage'))
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					{{Session::get('errmessage')}}
				</div>
				@endif
				<form  id="editArea" role="form" method="POST"  action="{{ url('/sub-admin/area-edit-form') }}" autocomplete="off">
					<input type="hidden" name="area_id" value="{{$area_id}}">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label>Enter area name</label>
							<input type="text" class="form-control" name="address" id="autocomplete"   onFocus="geolocate()" value="{{$area_detail[0]->area_name}}">
							<?php echo $errors->first('address'); ?>
							<input type="hidden"  id="street_number" name="street_number"/>
							<input type="hidden" class="field" id="route" name="route" />
							<input type="hidden"  id="country" name="country" />
							<input type="hidden" class="form-control" name="address1" id="address1" value="" />
							<input type="hidden" name="state" id="administrative_area_level_1" value="" />
							<input type="hidden" id="place_ID" name="place_ID" class="form-control" >
						</div>
						<div class="form-group">
							<label>City</label>
							<input type="text" id="locality" name="city" class="form-control" value="{{$area_detail[0]->area_city}}">
							<?php echo $errors->first('city'); ?>
						</div>
						<div class="form-group">
							<label>Zip code</label>
							<input type="text" id="postal_code" name="zipcode" class="form-control"  number="number" maxlength="6" value="{{$area_detail[0]->zipcode}}" />
							<?php echo $errors->first('zipcode'); ?>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<input type="submit" class="btn btn-primary"  value="Submit"  />
						<!-- <input type="button" class="btn btn-primary" value="Submit"   /> -->
						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
<style>
#ajax_parner_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_parner_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
}
#addons-modal.modal {
	z-index: 999;
}
</style>
<div id="ajax_parner_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
@section('js_bottom')
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!-- form validation start -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script> -->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#editArea").validate({
				rules: {
					address: {
						required: true,
					}
				},
				messages: {
					address: {
						required:'Please enter area name.',
					}
				}
			});
		});
	</script>
	<!-- form validation end -->
	<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };
      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        //console.log('initAutocomplete');
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        //console.log('fillInAddress');
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var placeID = place.place_id;
        		$('#place_ID').val(placeID);
        		//console.log(placeID);
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;
        		//console.log(place.address_components);
			//alert(val);
		}
	}
}
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate(){
      	//alert('test');
      	//console.log('testsfdsdf');
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
  @stop