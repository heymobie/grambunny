
	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Promotion Form->{{$transport_detail[0]->transport_name}}</h3>
                                </div><!-- /.box-header -->
							
                                <!-- form start -->
								
								<form  role="form" method="POST" id="promo_frm" action="#" enctype="multipart/form-data">    
								<input type="hidden" name="promo_id" value="{{$id}}" />
								<input type="hidden" name="promo_restid" value="{{$transport_id}}" />
							
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">On Order </label>
                                           <input type="text" class="form-control" name="promo_on" id="promo_on" value="@if($id>0){{$promo_detail[0]->tport_promo_on}}@endif" required="required" number="number">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<input type="text" class="form-control" name="promo_desc" id="	promo_desc" value="@if($id>0){{$promo_detail[0]->tport_promo_desc}}@endif" required="required" >												
                                        </div>
										
										
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Mode</label>
												
										<select name="promo_mode" id="promo_mode"  class="form-control">
										 <option value="%" @if(($id>0) && ($promo_detail[0]->tport_promo_mode=='%')) selected="selected"@endif> % </option>
										 <option value="$" @if(($id>0) && ($promo_detail[0]->tport_promo_mode=='$')) selected="selected"@endif>$ </option>
										</select>		
												
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Value</label>
											<input type="text" class="form-control" name="promo_value" id="	promo_value" value="@if($id>0){{$promo_detail[0]->tport_promo_value}}@endif" required="required" number="number">												
                                        </div>
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Start Date</label>
											<!--<input type="text" id="datepicker" class="date_textbox" />-->
											<input type="text" class="form-control date_textbox" name="promo_start" id="promo_start" value="@if($id>0){{$promo_detail[0]->tport_promo_start}}@endif" required="required">												
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">End Date</label>
											<input type="text" class="form-control date_textbox" name="promo_end" id="promo_end" value="@if($id>0){{$promo_detail[0]->tport_promo_end}}@endif" required="required">	
											<div id="error_msg" style="color:#FF0000; display:none;"></div>											
                                        </div>
										
										
										
										
										
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
												
										<select name="promo_status" id="promo_status"  class="form-control">
										 <option value="1" @if(($id>0) && ($promo_detail[0]->tport_promo_status==1)) selected="selected"@endif>Active </option>
										 <option value="0" @if(($id>0) && ($promo_detail[0]->tport_promo_status==0)) selected="selected"@endif>Inactive </option>										</select>		
												
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									@if($id>0)
										<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@else
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									@endif
										
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->




<link rel="stylesheet" href="{{ url('/') }}/design/admin/css/datepicker/datepicker3.css">



		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!--<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script> 
  <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
 
  
<script>
 var dateToday =  new Date();
$(document).ready(function () {
	$('.date_textbox').datepicker({
		format: "yyyy-mm-dd",
		 startDate: dateToday,
		 autoclose: true
	});  
});



		function check_frm(tpy)
		{
		
			 $('#error_msg').hide();
			 
			 
			var frmdate = $('#promo_start').val(); 
			var todate = $('#promo_end').val(); 
		
		
			if(tpy=='back')
			{
				var	valid = true;
			}
			else
			{	
				if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
				{//compare end <=, not >=
					//your code here
					//alert("From date will be big from to date!");
					$('#error_msg').html('From date will be big from to date!');
					$('#error_msg').show();
					var	valid = false;
				}
				else
				{
					var form = $("#promo_frm");
					form.validate();
					var valid =	form.valid();
				}	
			}
		
		
			if(valid)
			{		
				 $("#ajax_favorite_loddder").show();	
				var frm_val = 'from='+tpy+'&'+$('#promo_frm').serialize();				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/transport_promo_action')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#Promotions').html(msg);
					}
				});
			}
			else
			{
				return false;
			}		
		}
		</script>
