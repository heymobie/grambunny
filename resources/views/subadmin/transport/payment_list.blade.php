<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Mode</th>
			<th>Transport Name</th>
			<th>Vehicle</th>
			<th>Transaction  No</th>
			<th>Status</th>
			<!--<th>Cust_Submit_date</th>-->
			<th>Last Status Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	@if(!empty($payment_detail)) 
	 @foreach ($payment_detail as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>Paypal</td>
			
			<td><a href="{{url('admin/transport_view/')}}/{{$list->transport_id}}">{{ $list->transport_name}}</a></td>
			<td>{{ $list->vehicle_rego}}</td>
			<td>{{ $list->pay_tx}}</td>
			<td>		
@if($list->pay_status=='1') Complete @endif
@if($list->pay_status=='2') ToRefund @endif
@if($list->pay_status=='3') ToPayout @endif
@if($list->pay_status=='4') Completed Refund @endif
@if($list->pay_status=='5') Completed Payout @endif
			
			</td>
			<td>{{ $list->pay_update }}</td>
		 <td> 
			  <a title="Update" href="{{url('admin/transport_payment_view?order='.$list->pay_id)}}" ><i class="fa fa-edit"></i></a>
			 
				 
			</td>
		</tr> 

	<?php $i++; ?>
	@endforeach	   
	@endif                                     
	</tbody>
</table>	