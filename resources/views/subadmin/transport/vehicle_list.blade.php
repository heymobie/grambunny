<div class="col-sm-12">			
	@if(Session::has('menu_message'))
	
	<div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
		{{Session::get('menu_message')}}
	</div>
	@endif 
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Vehicle List</h4>
			<div class="drage" id="sortable" data-rest="{{$trans_id}}">
			
																			
			@if(!empty($vehicle_list))
			  @foreach($vehicle_list as $list)
				<div class="sub-drage @if($list->vehicle_id==$vehicle_id)selected @endif " id="listItem_{{$list->vehicle_id}}">
				<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$trans_id}}" data-menu="{{$list->vehicle_id}}" id="update_vehicle-{{$list->vehicle_id}}"><i class="fa fa-edit"></i></a>
				
					<p><a href="javascript:void(0)" id="menu_cat_list-{{$list->vehicle_id}}" data-menu="{{$list->vehicle_id}}"  data-rest="{{$trans_id}}">{{$list->vehicle_rego}}</a></p>
				</div>
			  @endforeach
			@endif
				
			</div>	
			<form id="res_btnfrm" method="post">
			<input type="hidden" name="transport_id" value="{{$trans_id}}" />
			{!! csrf_field() !!}
			<a class="btn btn-primary" href="javascript:void(0)" id="add_vehicle" data-restid="{{$trans_id}}">Add Vehicle</a> 	
			</form>											
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">
											
					@if(!empty($vehicle_list))
					
			
						<table border="0" width="100%" cellpadding="2" cellspacing="2">
						<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Status</td>
													<td>
															@if($vehicle_detail[0]->vehicle_status=='1') Active
															@else Inactive @endif
													</td>
													</tr>
												</table>
											</td>	
										</tr>
										<tr>
											<td>
											<table width="100%" cellpadding="2" cellspacing="2">
											 <tr>
												 <td>
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
														<td><?php					
						$img_detail = DB::table('vehicle_img')		
						->where('vehicle_id', '=' ,$vehicle_detail[0]->vehicle_id)
						->where('isCover', '=' ,'1')
						->get();						
						if(count($img_detail)>0){
						?>
						
						  <img src="{{ url('/') }}/uploads/vehicle/{{$img_detail[0]->imgPath }}" height="120" width="100" /> 
						 <?php }else{?> 	Add Image <?php }?></td>
														<td>  </td>
														</tr>
													</table>
												</td>	
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Rego:</td>
															<td>{{$vehicle_detail[0]->vehicle_rego}}</td>
														</tr>
														<tr>
															<td>Vehicle Make:</td>
															<td>{{$vehicle_detail[0]->make_name}}</td>
														</tr>
														<tr>
															<td>Vehicle Model:</td>
															<td>{{$vehicle_detail[0]->model_name}}</td>
														</tr>
														<tr>
															<td>Year:</td>
															<td>{{$vehicle_detail[0]->vehicle_year}}</td>
														</tr>
														<tr>
															<td>Vehicle Category:</td>
															<td>{{$vehicle_detail[0]->vcate_name}}</td>
														</tr>
													</table>
												</td>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Vehicle Class:</td>
															<td>{{$vehicle_detail[0]->vclass_name}}</td>
														</tr>
														<tr>
															<td>Max Pax:</td>
															<td>{{$vehicle_detail[0]->vehicle_pax}}</td>
														</tr>
														<tr>
															<td>Max Large Bags:</td>
															<td>{{$vehicle_detail[0]->vehicle_largebag}}</td>
														</tr>
														<tr>
															<td>Max Small Bags:</td>
															<td>{{$vehicle_detail[0]->vehicle_smallbag}}</td>
														</tr>
														<tr>
															<td>Minimum Rates $:</td>
															<td>{{$vehicle_detail[0]->vehicle_minrate}}</td>
														</tr>
													</table>
												</td>
											</tr>
											</table>	
											</td>	
										</tr>
										<tr>
											<td  style="border:1px solid #000000;">
												<table border="1" width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>A/C:</td>
														<td>@if($vehicle_detail[0]->vehicle_ac=='1') Yes
															@else no @endif
														</td>
														<td>Music System:</td>
														<td>@if($vehicle_detail[0]->vehicle_music=='1') Yes
															@else no @endif
														</td>
														<td>Video System:</td>
														<td>
															@if($vehicle_detail[0]->vehicle_video=='1') Yes
															@else no @endif
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Description:</td>
														<td>{{$vehicle_detail[0]->vehcile_desc}}</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Driver first Name:</td>
														<td>{{$vehicle_detail[0]->vehicle_drvierfname}}</td>
														</tr>
													<tr>
														<td>Driver Last Name:</td>
														<td>{{$vehicle_detail[0]->vehicle_driverlanme}}</td>
													</tr>
													<tr>
														<td>Driver Mobile No:</td>
														<td>{{$vehicle_detail[0]->vehicle_mobno}}</td>
													</tr>
													<tr>
														<td>Driver Overnight Allowance $ :</td>
														<td>{{$vehicle_detail[0]->vehicle_allownce}}</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										
										
									</table>
								<br />	
					<form id="itm_btnfrm" method="post">
					{!! csrf_field() !!}
			<input type="hidden" name="trans_id" value="{{$trans_id}}" />
			<input type="hidden" name="vehicle_id" value="{{$vehicle_id}}" />
			<a class="btn btn-primary" href="javascript:void(0)" id="add_charges">Add Rates</a> 
			</form>
			
			<br />	
				@if(!empty($rate_detail))
						<div style="border:1px solid #666666;">
							<?php $c = 1;?>
							<table width="100%" cellpadding="5" cellspacing="5" border="0" >
							
							@foreach($rate_detail as $rate_list)
							
							<tr>
							<td width="10%" valign="top">
							<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$rate_list->rate_vehicleid}}" data-rest="{{$rate_list->rate_transid}}" data-item="{{$rate_list->rate_id}}" id="update_item_data-{{$rate_list->rate_id}}">Edit</a>
							<br />
							<br />
							
							
							@if($rate_list->rate_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif
							<br />
							<br />

							</td>
							<td width="5%" valign="top">{{$c++}}</td>
								<td width="25%" valign="top">{{str_replace('_',' ',$rate_list->rate_name)}}: <br />
								{{$rate_list->rate_desc}}</td>
								<td  width="20%">
								@if($rate_list->rate_diff=='no')
								${{$rate_list->rate_price}}
								@elseif($rate_list->rate_diff=='yes')
								
								@if($rate_list->rate_lprice>0)
								{{$rate_list->rate_lname}} : ${{$rate_list->rate_lprice}} <br />
								@endif
								
								@if($rate_list->rate_mprice>0)
								{{$rate_list->rate_mname}} : ${{$rate_list->rate_mprice}}<br />
								@endif
								@if($rate_list->rate_sprice>0)
								{{$rate_list->rate_sname}} : ${{$rate_list->rate_sprice}}
								@endif
								
								
								 
								@endif
								</td>
								<td  width="20%">
								<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$rate_list->rate_vehicleid}}" data-rest="{{$rate_list->rate_transid}}" data-item="{{$rate_list->rate_id}}" id="addon_list_data-{{$rate_list->rate_id}}" >Add/Edit Addons</a> 
								</td>
							</tr>
						@endforeach
						<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
							</table>
						</div>
						@endif		
			
					@endif
				</div>												
			</div>
		</div>
	</div>
</div>
<script>

$( function() {
    $( "#sortable" ).sortable({
		
  	update:  function (event, ui) {
	        var sort_data = $("#sortable").sortable("serialize");
	        var trans_id = $("#sortable").attr("data-rest");
			
			var data = 'trans_id='+trans_id+'&'+sort_data;
			
				$("#ajax_favorite_loddder").show();	
            $.ajax({
                data: data,
                type: 'POST',
                url: "{{url('/admin/update_vehicle_sortorder')}}",
				success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				 //alert(msg);
				}
            });
	}
	 
     // revert: true
    });
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
} );


</script>