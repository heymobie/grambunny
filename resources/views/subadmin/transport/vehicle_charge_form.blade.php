	<!-- Main content -->
	<section class="content">
	<div class="col-xd-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Vehicle Rates Form -> {{$vehicle_rego}}</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_cate_frm" action="{{ url('/admin/vehicle_charge_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="rate_id" value="{{$rate_id}}" />
								<input type="hidden" name="rate_vehicleid" value="{{$vehicle_id}}" />
								<input type="hidden" name="rate_transid" value="{{$trans_id}}" />
								{!! csrf_field() !!}
                                    <div class="box-body">										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                          <select name="rate_name" id="rate_name">
												<option  value="Single_day_Hire" @if(($rate_id>0) && ($charg_detail[0]->rate_name=='Single_day_Hire'))selected="selected" @endif >Single-day Hire</option>			
												<option value="Multi_day_Hire" @if(($rate_id>0) && ($charg_detail[0]->rate_name=='Multi_day_Hire')) selected="selected" @endif>Multi-day Hire</option>										
											</select>
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="rate_desc" id="rate_desc" required="required">@if($rate_id>0){{ $charg_detail[0]->rate_desc }} @endif</textarea>																
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Available in diffrent ways</label>	
											<select name="rate_diff" id="diff_size">
												<option value="no" @if(($rate_id>0) && ($charg_detail[0]->rate_diff=='no')) selected="selected" @endif>No</option>			
												<option value="yes" @if(($rate_id>0) && ($charg_detail[0]->rate_diff=='yes')) selected="selected" @endif>Yes</option>										
											</select>
                                        </div>
										
										
										<div id="diff_size_div" style=" <?php if(($rate_id>0) && ($charg_detail[0]->rate_diff=='yes') ){ ?> display:block<?php } else{?>display:none<?php }?>" >
											 <div class="form-group">
											 	<table width="100%" cellpadding="2" cellspacing="2" id="portion_size_table"> 
													<tr>
														<th>Name</th>
														<th>Display Order</th>
														<th>Price</th>
														<th>Status</th>
													</tr>
													<tr><td><input type="text" name="rate_lname" class="form-control" value="@if($rate_id>0) {{$charg_detail[0]->rate_lname}}@endif" required="required"/></td>
														<td>1</td>
														<td><input type="text" name="rate_lprice" class="form-control"  value="@if($rate_id>0) {{$charg_detail[0]->rate_lprice}}@endif" required="required"/></td>
														<td>
														<select class="form-control" name="rate_lstatus">
															<option value="1"@if(($rate_id>0) && ($charg_detail[0]->rate_lstatus=='1')) selected="selected" @endif>Active</option>
															<option value="0"@if(($rate_id>0) && ($charg_detail[0]->rate_lstatus=='0')) selected="selected" @endif>Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td><input type="text" name="rate_mname" class="form-control" value="@if($rate_id>0) {{$charg_detail[0]->rate_mname}}@endif"/></td>
														<td>2</td>
														<td><input type="text" name="rate_mprice" class="form-control" value="@if($rate_id>0) {{$charg_detail[0]->rate_mprice}}@endif"/></td>
														<td>
														<select class="form-control" name="rate_mstatus">
															<option value="1" @if(($rate_id>0) && ($charg_detail[0]->rate_mstatus=='1')) selected="selected" @endif>Active</option>
															<option value="0" @if(($rate_id>0) && ($charg_detail[0]->rate_mstatus=='0')) selected="selected" @endif>Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td><input type="text" name="rate_sname" class="form-control" value="@if($rate_id>0) {{$charg_detail[0]->rate_sname}}@endif"/></td>
														<td>3</td>
														<td><input type="text" name="rate_sprice" class="form-control" value="@if($rate_id>0) {{$charg_detail[0]->rate_sprice}}@endif"/></td>
														<td>
														<select class="form-control" name="rate_sstatus">
															<option value="1" @if(($rate_id>0) && ($charg_detail[0]->rate_sstatus=='1')) selected="selected" @endif>Active</option>
															<option value="0" @if(($rate_id>0) && ($charg_detail[0]->rate_sstatus=='0')) selected="selected" @endif>Inactive</option>
														</select>
														</td>
													</tr>
												</table>
											 </div>
										</div>
										
										
										
                                        <div class="form-group" id="main_price_div"   <?php if((($rate_id>0) && ($charg_detail[0]->rate_diff=='no')) || ($rate_id==0) ) {?>style="display:block"<?php } else{?>style="display:none"<?php }?> >
                                            <label for="exampleInputEmail1">Item Price</label>
                                           <input type="text" name="rate_price" id="rate_price" class="form-control" value="@if($rate_id>0){{$charg_detail[0]->rate_price}}@endif" required="required" number="number">
                                        </div>
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
																								
											<select name="rate_status" id="rate_status" class="form-control">
											<option value="1" @if(($rate_id>0) && ($charg_detail[0]->rate_status=='1')) selected="selected" @endif>Active</option>
											<option value="0" @if(($rate_id>0) && ($charg_detail[0]->rate_status=='0')) selected="selected" @endif>Inactive</option>
											</select>															
                                        </div>
										
                                       
                                      
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									
									
									@if($rate_id>0)
									<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@else
									
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 @endif
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>	
	</section><!-- /.content -->

<script>
  $(document).on('change', '#diff_size', function(){ 
  	if($(this).find(":selected").val()=='yes'){
  		$('#main_price_div').hide();
  		$('#diff_size_div').show();
	}
	else if($(this).find(":selected").val()=='no')
	{
  		$('#diff_size_div').hide();
  		$('#main_price_div').show();
		
	}
  
 });
 

 
function check_frm(tpy)
{

	if(tpy=='back')
	{
		var	valid = true;
	}
	else
	{		
			
		var form = $("#rest_cate_frm");
		form.validate();
		var valid =	form.valid();
	}
	
	
	if(valid)
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = 'from='+tpy+'&'+$('#rest_cate_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/vehicle_charge_action')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#ajax_div').html(msg);
			}
		});
	}
	else
	{
		return false;
	}		
}
</script>

