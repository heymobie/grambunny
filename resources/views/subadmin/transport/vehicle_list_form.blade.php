<div class="col-sm-12">
	<p style="float: left;text-align: center;width: 100%;">				
	@if(Session::has('menu_message'))
		{{Session::get('menu_message')}}
	@endif 
	</p>
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Vehicle List</h4>
			<div class="drage" id="sortable" data-rest="{{$trans_id}}">
																						
			@if(!empty($vehicle_list))
			  @foreach($vehicle_list as $list)
				<div class="sub-drage @if($list->vehicle_id==$vehicle_id)selected @endif " id="listItem_{{$list->vehicle_id}}">
				<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$trans_id}}" data-menu="{{$list->vehicle_id}}" id="update_vehicle-{{$list->vehicle_id}}"><i class="fa fa-edit"></i></a>
				
					<p><a href="javascript:void(0)" id="menu_cat_list-{{$list->vehicle_id}}" data-menu="{{$list->vehicle_id}}"  data-rest="{{$trans_id}}">{{$list->vehicle_rego}}</a></p>
				</div>
			  @endforeach
			@endif
				
			</div>	
			<form id="res_btnfrm" method="post">
			<input type="hidden" name="transport_id" value="{{$trans_id}}" />
			{!! csrf_field() !!}
			<a class="btn btn-primary" href="javascript:void(0)" id="add_vehicle" data-restid="{{$trans_id}}">Add Vehicle</a> 	
			</form>											
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">
					<section class="content">
	<div class="col-xd-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Transport Vehicle Form</h3>
                                </div><!-- /.box-header -->
							
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_frm" action="{{ url('/admin/menu_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="vehicle_transid" value="{{$trans_id}}" />
							
								{!! csrf_field() !!}
                                    <div class="box-body">
									<table border="0" width="100%" cellpadding="2" cellspacing="2">
										<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Add Image</td>
													<td> <input type="file" />  </td>
													</tr>
												</table>
											</td>	
										</tr>
										<tr>
											<td>
											<table width="100%" cellpadding="2" cellspacing="2">
											 <tr>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Rego</td>
															<td><input type="text" class="form-control" name="vehicle_rego" id="vehicle_rego" value="" required="required"></td>
														</tr>
														<tr>
															<td>Vehicle Make</td>
															<td>
									<select class="form-control" name="vehicle_make" id="vehicle_make" required="required">
									<option value="">Select Make</option>			
									
										<?php if(count($make_list)>0)
										{
											foreach($make_list as $mlist)
											{
											?>
											<option value="{{$mlist->make_id}}">{{$mlist->make_name}}</option>				
											<?php
											}
										}
										?>
									</select>
																
															</td>
														</tr>
														<tr>
															<td>Vehicle Model</td>
															<td id="model_selection">
															
									<select class="form-control" name="vehicle_model" id="vehicle_model" required="required">
									<option value="">Select Model</option>			
									
										<?php if(count($model_list)>0)
										{
											foreach($model_list as $mlist)
											{
											?>
											<option value="{{$mlist->model_id}}">{{$mlist->model_name}}</option>				
											<?php
											}
										}
										?>
									</select>
															</td>
														</tr>
														<tr>
															<td>Year</td>
															<td><select class="form-control" name="vehicle_year" id="vehicle_year" required="required">
									<option value="">Select Year</option>			
									
										<?php 
											for($y=date('Y');$y>=1900;$y--)
											{
											?>
											<option value="{{$y}}">{{$y}}</option>				
											<?php
											}
										?>
									</select></td>
														</tr>
														<tr>
															<td>Vehicle Category</td>
															<td>
								<select class="form-control" name="vehicle_catid" id="vehicle_catid" required="required">
									<option value="">Select Category</option>			
									
										<?php if(count($cate_list)>0)
										{
											foreach($cate_list as $clist)
											{
											?>
											<option value="{{$clist->vcate_id}}">{{$clist->vcate_name}}</option>				
											<?php
											}
										}
										?>
									</select></td>
														</tr>
													</table>
												</td>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Vehicle Class</td>
															<td>
								<select class="form-control" name="vehicle_classid" id="vehicle_classid" required="required">
									<option value="">Select Class</option>			
									
										<?php if(count($class_list)>0)
										{
											foreach($class_list as $clist)
											{
											?>
											<option value="{{$clist->vclass_id}}">{{$clist->vclass_name}}</option>				
											<?php
											}
										}
										?>
									</select></td>
														</tr>
														<tr>
															<td>Max Pax</td>
															<td><input type="number" class="form-control" name="vehicle_pax" id="vehicle_pax" value="" required="required" number="number" min="0" ></td>
														</tr>
														<tr>
															<td>Max Large Bags</td>
															<td><input type="number" class="form-control" name="vehicle_largebag" id="vehicle_largebag" value="" required="required" min="0"></td>
														</tr>
														<tr>
															<td>Max Small Bags</td>
															<td><input type="number" class="form-control" name="vehicle_smallbag" id="vehicle_smallbag" value="" required="required" min="0"></td>
														</tr>
														<tr>
															<td>Minimum Rates $</td>
															<td><input type="number" class="form-control" name="vehicle_minrate" id="vehicle_minrate" value="" required="required" min="0"></td>
														</tr>
													</table>
												</td>
											</tr>
											</table>	
											</td>	
										</tr>
										<tr>
											<td  style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>A/C</td>
														<td>
															<select name="vehicle_ac" id="vehicle_ac" class="form-control">
																<option value="1">Yes</option>
																<option value="0">No</option>
															</select>
														</td>
														<td>Music System</td>
														<td>
															<select name="vehicle_music" id="vehicle_music" class="form-control">
																<option value="1">Yes</option>
																<option value="0">No</option>
															</select>
														</td>
														<td>Video System</td>
														<td>
															<select name="vehicle_video" id="vehicle_video" class="form-control">
																<option value="1">Yes</option>
																<option value="0">No</option>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Description</td>
														<td>
															<textarea name="vehcile_desc" id="vehcile_desc" class="form-control"></textarea>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Driver first Name</td>
														<td><input type="text" class="form-control" name="vehicle_drvierfname" id="vehicle_drvierfname" value="" required="required">
														</td>
														</tr>
													<tr>
														<td>Driver Last Name</td>
														<td><input type="text"  id="vehicle_driverlanme" class="form-control" name="vehicle_driverlanme" value="" required="required">
														</td>
													</tr>
													<tr>
														<td>Driver Mobile No</td>
														<td><input type="text" class="form-control" name="vehicle_mobno" id="vehicle_mobno" value="" required="required" maxlength="10">
														</td>
													</tr>
													<tr>
														<td>Driver Overnight Allowance $</td>
														<td><input type="text" class="form-control" name="vehicle_allownce" id="vehicle_allownce" value="" required="required" number="number">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Status</td>
													<td> <select name="vehicle_status" id="vehicle_status" class="form-control">
											<option value="1">Active</option>
											<option value="0">Inactive</option>
											</select></td>
													</tr>
												</table>
											</td>	
										</tr>
										
										
									</table>
									
                                        
                                        
                                       
                                        
										
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									
										
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section>
				</div>												
			</div>
		</div>
	</div>
</div>


<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>

$( function() {
    $( "#sortable" ).sortable({
		
  	update:  function (event, ui) {
	        var sort_data = $("#sortable").sortable("serialize");
	        var trans_id = $("#sortable").attr("data-rest");
			
			var data = 'trans_id='+trans_id+'&'+sort_data;
			
				$("#ajax_favorite_loddder").show();	
            $.ajax({
                data: data,
                type: 'POST',
                url: "{{url('/admin/update_vehicle_sortorder')}}",
				success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				 //alert(msg);
				}
            });
	}
	 
     // revert: true
    });
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
} );


		

		function check_frm(tpy)
		{
		
			if(tpy=='back')
			{
				var	valid = true;
			}
			else
			{		
					
				var form = $("#rest_frm");
					form.validate();
				var valid =	form.valid();
			}
			
			
			if(valid)
			{		
				 $("#ajax_favorite_loddder").show();	
				var frm_val = 'from='+tpy+'&'+$('#rest_frm').serialize();				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/vehicle_action')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#ajax_div').html(msg);
					}
				});
			}
			else
			{
				return false;
			}		
		}

 $(document).on('change', '#vehicle_make', function(){ 	
 var make_id = 	 $(this).find('option:selected').val();
 
 
 		var frm_val = 'make_id='+make_id;				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/findmodel')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#model_selection').html(msg);
					}
				});
				
 });	
</script>

