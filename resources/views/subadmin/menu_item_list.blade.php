@extends('layouts.subadmin')

@section("other_css")
<!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Restaurant Menu Item Addon List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Menu Item Addon List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Restaurant Menu Item Addon Listing</h3>
                        <div style="float:right; margin-right:10px; margin-top:10px;">
                           <a href="{{url('sub-admin/category-item-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New</a>
                       </div>

                   </div><!-- /.box-header -->
                   <div class="box-body table-responsive">

                      @if(Session::has('message'))

                      <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                          {{Session::get('message')}}
                      </div>
                      @endif

                      <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>SrNo</th>
                                <th>Addon Name</th>
                                <th>Menu Name</th>
                                <th>Menu Category Name</th>
                                <th>Restaurant Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $i=1; ?>
                          @if(!empty($category_item_list))
                          @foreach ($category_item_list as $list)
                          <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $list->menu_cat_itm_name}}</td>
                            <td>{{ $list->menu_category_name}}</td>
                            <td>{{ $list->menu_name}}</td>
                            <td>{{ $list->rest_name}}</td>
                            <td>
                               <a href="{{url('sub-admin/category-item-form/')}}/{{ $list->menu_cat_itm_id }}">
                                   <i class="fa fa-edit"></i></a>
                                   <a href="{{url('sub-admin/menu_cat_item_delete/')}}/{{ $list->menu_cat_itm_id }}" onclick="return delete_wal()" ><i class="fa fa-trash-o"></i></a>
                               </td>
                           </tr>
                           <?php $i++; ?>
                           @endforeach
                           @endif
                       </tbody>
                       <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

</section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>
@stop
