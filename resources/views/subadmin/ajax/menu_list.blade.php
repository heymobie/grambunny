<div class="col-sm-12">
	<p style="float: left;text-align: center;width: 100%;">
		@if(Session::has('menu_message'))
		{{Session::get('menu_message')}}
		@endif
	</p>
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Menu Category</h4>
			<div class="drage" id="sortable" data-rest="{{$rest_id}}">
				<div class="sub-drage">
					<p>
						<a title="Edit" href="javascript:void(0)" data-rest="{{$rest_id}}" id="show_popular_item">Popular</a>
					</p>
				</div>
				@if(!empty($menu_list))
				@foreach($menu_list as $mlist)
				<div class="sub-drage @if($mlist->menu_id==$menu_id)selected @endif " id="listItem_{{$mlist->menu_id}}">
					<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$rest_id}}" data-menu="{{$mlist->menu_id}}" id="update_menu-{{$mlist->menu_id}}"><i class="fa fa-edit"></i></a>
					<p><a href="javascript:void(0)" id="menu_cat_list-{{$mlist->menu_id}}" data-menu="{{$mlist->menu_id}}"  data-rest="{{$rest_id}}">{{$mlist->menu_name}}</a></p>
				</div>
				@endforeach
				@endif
			</div>
			<form id="res_btnfrm" method="post">
				<input type="hidden" name="rest_id" value="{{$rest_id}}" />
				<input type="hidden" name="aria_id" id="aria_id" value="@if($aria_id){{$aria_id}}@endif">
				{!! csrf_field() !!}
				<a class="btn btn-primary" href="javascript:void(0)" id="add_menu" data-restid="{{$rest_id}}">Add Menu Category</a>
			</form>
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">
					@if(!empty($menu_list))
					<form id="itm_btnfrm" method="post">
						{!! csrf_field() !!}
						<input type="hidden" name="rest_id" value="{{$rest_id}}" />
						<input type="hidden" name="menu_id" value="{{$menu_id}}" />
						<input type="hidden" name="menu_aria_id" id="menu_aria_id" value="@if($aria_id){{$aria_id}}@endif">
						<a class="btn btn-primary" href="javascript:void(0)"  id="add_menu_item">Add Menu Item</a>
					</form>
					<h4>{{$menu_detail[0]->menu_name}}@if($menu_detail[0]->menu_status==1)
						<span class="label label-success">Active</span>
						@else
						<span class="label label-danger">Inactive</span>
					@endif </h4>
					<p>{{$menu_detail[0]->menu_desc}}</p>
					@if(!empty($menu_cate_detail))
					<div style="border:1px solid #666666;">
						<?php $c = 1;?>
						<table width="100%" cellpadding="5" cellspacing="5" border="1" bordercolor="#ddd" >
							@foreach($menu_cate_detail as $cat_list)
							<tr>
								<td width="5%" valign="top">{{$c++}}</td>
								<td width="25%" valign="top">{{$cat_list->menu_category_name}}: <br />{{$cat_list->menu_category_desc}}</td>
								<td  width="20%" align="right">
									@if($cat_list->menu_category_portion=='no')
									${{$cat_list->menu_category_price}}
									@elseif($cat_list->menu_category_portion=='yes')
									<?php
									$menu_sub_itme = DB::table('category_item')
									->where('rest_id', '=' ,$rest_id)
									->where('menu_id', '=' ,$menu_id)
									->where('menu_category', '=' ,$cat_list->menu_category_id)
									->orderBy('menu_cat_itm_id', 'asc')
									->get();
									if($menu_sub_itme)
									{
										foreach($menu_sub_itme as  $msi){
											echo $msi->menu_item_title.' : $'.$msi->menu_item_price.'<br />';
										}
									}
									?>
									@endif
								</td>
								<td  width="20%" align="center">
									<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$menu_id}}" data-rest="{{$rest_id}}" data-item="{{$cat_list->menu_category_id}}" id="addon_list_data-{{$cat_list->menu_category_id}}" >Add/Edit Addons</a>
								</td>
								<td width="10%" valign="top">
									<a class="btn btn-primary" style="margin-bottom: 5px;" href="javascript:void(0)" data-menu="{{$menu_id}}" data-rest="{{$rest_id}}" data-item="{{$cat_list->menu_category_id}}" id="update_item_data-{{$cat_list->menu_category_id}}">Edit</a>
									@if($cat_list->menu_cat_status==1)
									<span class="label label-success" style="float:left;padding: 10px;">Active</span>
									@else
									<span class="label label-danger" style="float:left;padding: 10px;">Inactive</span>
									@endif

								<!-- @if($cat_list->menu_cat_diet==1)
								<span class="label label-success" style="float:left;padding: 10px;">&nbsp;</span>
								@elseif($cat_list->menu_cat_diet==2)
								<span class="label label-danger" style="float:left;padding: 10px;">&nbsp;</span>
								@endif -->

								</td>
							</tr>
							@endforeach
						</table>
					</div>
					@endif
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$( function() {
		$( "#sortable" ).sortable({
			update:  function (event, ui) {
				var sort_data = $("#sortable").sortable("serialize");
				var rest_id = $("#sortable").attr("data-rest");
				var data = 'rest_id='+rest_id+'&'+sort_data;
				$("#ajax_favorite_loddder").show();
				$.ajax({
					data: data,
					type: 'POST',
					url: "{{url('/sub-admin/update_sortorder')}}",
					success: function(msg) {
						$("#ajax_favorite_loddder").hide();
				 //alert(msg);
				}
			});
			}
     // revert: true
 });
		$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});
		$( "ul, li" ).disableSelection();
	} );
</script>