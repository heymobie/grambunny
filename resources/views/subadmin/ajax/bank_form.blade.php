	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">BankDetail Form->{{$rest_detail[0]->rest_name}}</h3>
				</div><!-- /.box-header -->
				<!-- form start -->
				<form  role="form" method="POST" id="bank_frm" action="#" enctype="multipart/form-data">
					<input type="hidden" name="bank_id" value="{{$id}}" />
					<input type="hidden" name="bank_restid" value="{{$rest_id}}" />
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Name</label>
							<input type="text" class="form-control" name="bank_uname" id="	bank_uname" value="@if($id>0){{$bank_detail[0]->bank_uname}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Bank</label>
							<input type="text" class="form-control" name="bank_name" id="	bank_name" value="@if($id>0){{$bank_detail[0]->bank_name}}@endif" required="required" >
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Branch</label>
							<input type="text" class="form-control" name="bank_branch" id="	bank_branch" value="@if($id>0){{$bank_detail[0]->bank_branch}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">BSB</label>
							<input type="text" class="form-control" name="bank_bsb" id="bank_bsb" value="@if($id>0){{$bank_detail[0]->bank_bsb}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Account</label>
							<input type="text" class="form-control" name="bank_acc" id="bank_acc" value="@if($id>0){{$bank_detail[0]->bank_acc}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="bank_status" id="bank_status"  class="form-control">
								<option value="1" @if(($id>0) && ($bank_detail[0]->bank_status==1)) selected="selected"@endif>Active </option>
								<option value="0" @if(($id>0) && ($bank_detail[0]->bank_status==0)) selected="selected"@endif>Inactive </option>										</select>
							</div>
						</div><!-- /.box-body -->
						<div class="box-footer">
							@if($id>0)
							<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
							<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
							@else
							<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
							<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
							<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
							@endif
						</div>
					</form>
				</div><!-- /.box -->
			</div>
		</section><!-- /.content -->
		<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script>
			function check_frm(tpy)
			{
				if(tpy=='back')
				{
					var	valid = true;
				}
				else
				{
					var form = $("#bank_frm");
					form.validate();
					var valid =	form.valid();
				}
				if(valid)
				{
					$("#ajax_favorite_loddder").show();
					var frm_val = 'from='+tpy+'&'+$('#bank_frm').serialize();
					$.ajax({
						type: "POST",
						url: "{{url('/sub-admin/bank_action')}}",
						data: frm_val,
						success: function(msg) {
							$("#ajax_favorite_loddder").hide();
							$('#BankDetail').html(msg);
						}
					});
				}
				else
				{
					return false;
				}
			}
		</script>