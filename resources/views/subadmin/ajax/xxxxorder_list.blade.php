<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Order No</th>
			<th>Order Date Time</th>
			<th>User</th>
			<th>Restaurant</th>
			<th>Status</th>
			<th>Device Type</th><th>Device Name</th>																	<th>Device OS</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php $i=1; ?>
		@if(!empty($order_detail))
		@foreach ($order_detail as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
			<td>{{ $list->created_at}}</td>
			<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
			<td>{{ $list->rest_name}}</td>
			<td>

				@if($list->order_status=='1') Submit  @endif
				@if($list->order_status=='2') Cancelled @endif

				@if($list->order_status=='4') Order Confirmed @endif
				@if($list->order_status=='5') Order Completed @endif
				@if($list->order_status=='6') Reject @endif
				@if($list->order_status=='7') Review Completed @endif

			</td>

			<td>{{ $list->order_device}}</td>
			<td>{{ $list->order_devicename}}</td>
			<td>{{$list->order_device_os}}</td>

			<td>
				<a title="Update" href="{{url('sub-admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>


				<!-- <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>-->


			</td>
		</tr>

		<?php $i++; ?>
		@endforeach
		@else
		No record Found!
		@endif
	</tbody>
</table>

<script type="text/javascript">
	$(function() {
		$("#example1").dataTable();
		$('#example2').dataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
</script>