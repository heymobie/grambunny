<?php
$promo_desc_qty	='';
$promo_buy_qty	= '';
$promo_mode_qty = '';
$promo_offer_qty = '';
$promo_desc_cost ='';
$promo_mode_cost='';
$promo_value_cost='';
$promo_offer_cost='';
$promo_desc_range ='';
$promo_mode_range = '';
$promo_buy_range ='';
$promo_offer_range ='';
$promo_day = '';
$promo_start = '';
$promo_end ='';
if(($id>0) && ($promo_detail[0]->promo_on=='qty'))
{
	$promo_desc_qty	=$promo_detail[0]->promo_desc;
	$promo_buy_qty	= $promo_detail[0]->promo_buy;
	$promo_mode_qty =  $promo_detail[0]->promo_mode;
	$promo_offer_qty = $promo_detail[0]->promo_value;
}
if(($id>0) && ($promo_detail[0]->promo_on=='total_amt'))
{
	$promo_desc_cost =$promo_detail[0]->promo_desc;
	$promo_mode_cost=$promo_detail[0]->promo_mode;
	$promo_value_cost=$promo_detail[0]->promo_buy;
	$promo_offer_cost=$promo_detail[0]->promo_value;
}
if(($id>0) && ($promo_detail[0]->promo_on=='schedul'))
{
	$promo_desc_range =$promo_detail[0]->promo_desc;
	$promo_mode_range = $promo_detail[0]->promo_mode;
	$promo_buy_range = $promo_detail[0]->promo_buy;
	$promo_offer_range =$promo_detail[0]->promo_value;
	$promo_day =  $promo_detail[0]->promo_day;
	if(!empty($promo_detail[0]->promo_day))
	{
		$promo_day = explode(',',$promo_detail[0]->promo_day);
	}
	$promo_start = $promo_detail[0]->promo_start;
	$promo_end = $promo_detail[0]->promo_end;
}
?>
<!-- Main content -->
<section class="content">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Promotion Form->{{$rest_detail[0]->rest_name}}</h3>
			</div>
			<!-- form start -->
			<form  role="form" method="POST" id="promo_frm" action="#" enctype="multipart/form-data">
				<input type="hidden" name="promo_id" value="{{$id}}" />
				<input type="hidden" name="promo_restid" value="{{$rest_id}}" />
				{!! csrf_field() !!}
				<div class="box-body">
					<div class="form-group">
						<label for="exampleInputEmail1">Offer/Promotion for </label>
						<select name="promo_for" id="promo_for"  class="form-control" required="required">
							<option value="" > Select Promotion </option>
							<option value="order" @if(($id>0) && ($promo_detail[0]->promo_for=='order')) selected="selected"@endif> Order </option>
							<option value="each_food" @if(($id>0) && ($promo_detail[0]->promo_for=='each_food')) selected="selected"@endif>  Food Item </option>
						</select>
						<!-- <option value="each_food" @if(($id>0) && ($promo_detail[0]->promo_for=='each_food')) selected="selected"@endif> Each food Item </option>-->
						<!-- <option value="special_food" @if(($id>0) && ($promo_detail[0]->promo_for=='special_food')) selected="selected"@endif> Special Food Item </option>	-->
					</div>
					<div class="form-group" id="menu_item_div" style=" <?php if(($id>0) && ($promo_detail[0]->promo_for=='order')){?>display:none <?php }?>">
						<label for="exampleInputEmail1">Item Name  </label>
						<select name="promo_menu_item" id="promo_menu_item" class="form-control">
							<option value="" > Select Item </option>
							<?php foreach($rest_menu as $item){
								$sel = '';
								if(($id>0)&&($item->menu_category_id==$promo_detail[0]->promo_menu_item)) {
									$sel ='selected="selected"';
								}
								?>
								<option value="<?php echo $item->menu_category_id;?>"  <?php echo $sel;?>> <?php echo $item->menu_category_name; ?> </option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Offer/Promotion On </label>
						<span id="promo_qty" <?php if(($id>0) && ($promo_detail[0]->promo_for=='order')){?>
							style="display:none" <?php }?>	> <input type="radio" name="promo_on"  class="check_promo_on" value="qty"  required="required" @if(($id>0) && ($promo_detail[0]->promo_on=='qty')) checked="checked" @endif /> Quentity </span>
							<span id="promo_total" <?php if(($id>0) && ($promo_detail[0]->promo_for=='each_food')){?>
								style="display:none" <?php }?>> <input type="radio" name="promo_on" class="check_promo_on" value="total_amt"  required="required" @if(($id>0) && ($promo_detail[0]->promo_on=='total_amt')) checked="checked" "@endif  /> Total Cost</span>
								<span> <input type="radio" name="promo_on" value="schedul" class="check_promo_on"  required="required" @if(($id>0) && ($promo_detail[0]->promo_on=='schedul')) checked="checked" @endif /> Schedule (Time Range)</span>
							</div>
							<div id="qty_div" style=" <?php if(($id>0) && ($promo_detail[0]->promo_on=='qty')){?>
								display:block <?php }else{?>display:none<?php }?>">
								<div class="form-group">
									<label for="exampleInputEmail1">Description</label>
									<input type="text" class="form-control" name="promo_desc_qty" id="promo_desc_qty" value="{{$promo_desc_qty}}" required="required" >
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Discount/Offer Mode</label>
									<select name="promo_mode_qty" id="promo_mode_qty"  class="form-control">
										<option value="%" @if($promo_mode_qty=='%') selected="selected"@endif> % </option>
										<option value="$" @if($promo_mode_qty=='$') selected="selected"@endif>$ </option>
										<!--<option value="0" @if($promo_mode_qty=='0') selected="selected"@endif>Free (item)</option>-->
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Buy quantity </label>
									<input type="text" class="form-control" name="promo_buy_qty" id="promo_buy_qty" value="{{$promo_buy_qty}}" required="required" number="number">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Offer/Discount Quantity or itme </label>
									<input type="text" class="form-control" name="promo_offer_qty" id="promo_offer_qty" value="{{$promo_offer_qty}}" required="required">
								</div>
							</div>
							<div id="total_amt_div"  style=" <?php if(($id>0) && ($promo_detail[0]->promo_on=='total_amt')){?>
								display:block <?php }else{?>display:none<?php }?>">
								<div class="form-group">
									<label for="exampleInputEmail1">Description</label>
									<input type="text" class="form-control" name="promo_desc_cost" id="promo_desc_cost" value="{{$promo_desc_cost}}" required="required" >
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Discount/Offer Mode</label>
									<select name="promo_mode_cost" id="promo_mode_cost"  class="form-control">
										<option value="%" @if($promo_mode_cost=='%') selected="selected"@endif> % </option>
										<option value="$" @if($promo_mode_cost=='$') selected="selected"@endif>$ </option>
										<!--<option value="0" @if($promo_mode_cost=='0') selected="selected"@endif>Free (item)</option>-->
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Total Order amount of (If admin want to give offer on total  amount of order then insert 0 )</label>
									<input type="text" class="form-control" name="promo_value_cost" id="promo_value_cost" value="{{$promo_value_cost}}" required="required" number="number">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Discount Amount/Item </label>
									<input type="text" class="form-control" name="promo_offer_cost" id="promo_offer_cost" value="{{$promo_offer_cost}}" required="required" >
								</div>
							</div>
							<div id="time_range"  style=" <?php if(($id>0) && ($promo_detail[0]->promo_on=='schedul')){?>
								display:block <?php }else{?>display:none<?php }?>">
								<div class="form-group">
									<label for="exampleInputEmail1">Description</label>
									<input type="text" class="form-control" name="promo_desc_range" id="promo_desc_range" value="{{$promo_desc_range}}" required="required" >
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Discount/Offer Mode</label>
									<select name="promo_mode_range" id="promo_mode_range"  class="form-control">
										<option value="%" @if($promo_mode_range=='%') selected="selected"@endif> % </option>
										<option value="$" @if($promo_mode_range=='$') selected="selected"@endif>$ </option>
										<!--<option value="0" @if($promo_mode_range=='0') selected="selected"@endif>Free (item)</option>-->
									</select>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Total Order amount of (If admin want to give offer on total  amount of order then insert 0 )</label>
									<input type="text" class="form-control" name="promo_buy_range" id="promo_buy_range" value="{{$promo_buy_range}}" required="required" number="number">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Off/Discount </label>
									<input type="text" class="form-control" name="promo_offer_range" id="promo_offer_range" value="{{$promo_offer_range}}" required="required" >
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Days </label>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="mon" <?php if ( !empty($promo_day) &&(in_array('mon',$promo_day))){ echo 'checked="checked"';} ?> /> Mon</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="tue" <?php if( !empty($promo_day) && (in_array('tue',$promo_day))){ echo 'checked="checked"';} ?>/> Tues</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="wed" <?php if( !empty($promo_day) &&(in_array('wed',$promo_day))){ echo 'checked="checked"';} ?>/> Wed</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="thu" <?php if( !empty($promo_day) &&(in_array('thu',$promo_day))){ echo 'checked="checked"';} ?>/> Thus</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="fri" <?php if( !empty($promo_day) &&(in_array('fri',$promo_day))){ echo 'checked="checked"';} ?>/> Fri</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="sat" <?php if( !empty($promo_day) &&(in_array('sat',$promo_day))){ echo 'checked="checked"';} ?>/> Sat</span>
									<span><input type="checkbox" class="promo_day" name="day_name[]" value="sun" <?php if( !empty($promo_day) &&(in_array('sun',$promo_day))){ echo 'checked="checked"';} ?>/> Sun</span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Start Date</label>
									<input type="text" class="form-control date_textbox" name="promo_start" id="promo_start" value="{{$promo_start}}">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">End Date</label>
									<input type="text" class="form-control date_textbox" name="promo_end" id="promo_end" value="{{$promo_end}}">
									<div id="error_msg" style="color:#FF0000; display:none;"></div>
								</div>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Status</label>
								<select name="promo_status" id="promo_status"  class="form-control">
									<option value="1" @if(($id>0) && ($promo_detail[0]->promo_status==1)) selected="selected"@endif>Active </option>
									<option value="0" @if(($id>0) && ($promo_detail[0]->promo_status==0)) selected="selected"@endif>Inactive </option>										</select>
								</div>
							</div><!-- /.box-body -->
							<div class="box-footer">
								@if($id>0)
								<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
								<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
								@else
								<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
								<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
								<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
								@endif
							</div>
						</form>
					</div><!-- /.box -->
				</div>
			</section><!-- /.content -->
			<link rel="stylesheet" href="{{ url('/') }}/design/admin/css/datepicker/datepicker3.css">
			<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
			<!--<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> -->
			<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
			<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
			<script>
				var dateToday =  new Date();
				$(document).ready(function () {
					$('.date_textbox').datepicker({
						format: "yyyy-mm-dd",
						startDate: dateToday,
						autoclose: true
					});
				});
				function check_frm(tpy)
				{
					$('#error_msg').hide();
					var frmdate = $('#promo_start').val();
					var todate = $('#promo_end').val();
					if(tpy=='back')
					{
						var	valid = true;
					}
					else
					{
						var selected = $(".check_promo_on:checked").val();
				//	alert(selected);
					//alert($('.promo_day').is(':checked'));
					/* if ($('.promo_day').is(':checked')) {
						//prevent the default form submit if it is not checked
						e.preventDefault();
					}*/
			//alert(frmdate)
			//alert(todate)
		//return false;
		if(selected=='schedul')
		{
			var day = $('.promo_day').is(':checked');
			if((day==false) && ((frmdate=='') ||(frmdate=='0000-00-00')) && ((todate=='') ||(todate=='0000-00-00')))
			{
				alert("Please select date or day!");
				var	valid = false;
			}
			else
			{
				if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
						{//compare end <=, not >=
							//your code here
							//alert("From date will be big from to date!");
							$('#error_msg').html('From date will be big from to date!');
							$('#error_msg').show();
							var	valid = false;
						}
						else
						{
							var form = $("#promo_frm");
							form.validate();
							var valid =	form.valid();
						}
					}
				}
				else
				{
					var form = $("#promo_frm");
					form.validate();
					var valid =	form.valid();
				}
			}
			if(valid)
			{
				$("#ajax_favorite_loddder").show();
				var frm_val = 'from='+tpy+'&'+$('#promo_frm').serialize();
				$.ajax({
					type: "POST",
					url: "{{url('/sub-admin/promo_action')}}",
					data: frm_val,
					success: function(msg) {
						$("#ajax_favorite_loddder").hide();
						$('#Promotions').html(msg);
					}
				});
			}
			else
			{
				return false;
			}
		}
		$(document).on('change', '#promo_for', function(){
	//alert($(this).val());
	$selected_val = $(this).val();
	$('#time_range').hide();
	$('#total_amt_div').hide();
	$('#qty_div').hide();
	if($selected_val=='order')
	{
		$('#promo_qty').hide();
		$('#promo_total').show();
		$('#menu_item_div').hide();
		$("#promo_menu_item").prop('required',false);
		<?php if(($id>0) && ($promo_detail[0]->promo_on=='schedul')){?>
			$('#time_range').show();
		<?php }elseif(($id>0) && ($promo_detail[0]->promo_on=='total_amt')){?>
			$('#total_amt_div').show();
		<?php }?>
	}
	if(($selected_val=='each_food') || ($selected_val=='special_food'))
	{
		$('#promo_qty').show();
		$('#menu_item_div').show();
		$('#promo_total').hide();
		$("#promo_menu_item").prop('required',true);
		<?php if(($id>0) && ($promo_detail[0]->promo_on=='schedul')){?>
			$('#time_range').show();
		<?php }elseif(($id>0) && ($promo_detail[0]->promo_on=='qty')){?>
			$('#qty_div').show();
		<?php }?>
	}
});
		$(document).on('change', '.check_promo_on', function(){
	//alert($(this).val());
	$selected_val = $(this).val();
	if($selected_val=='qty')
	{
		$('#time_range').hide();
		$('#total_amt_div').hide();
		$('#qty_div').show();
	}
	if($selected_val=='total_amt')
	{
		$('#time_range').hide();
		$('#total_amt_div').show();
		$('#qty_div').hide();
	}
	if($selected_val=='schedul')
	{
		$('#time_range').show();
		$('#total_amt_div').hide();
		$('#qty_div').hide();
	}
});
</script>