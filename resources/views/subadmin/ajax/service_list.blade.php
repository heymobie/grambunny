<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Service Areas Listing</h3><br />
			<div style="margin-top:10px;">
				<a  href="javascript:void(0)" data-rest="{{$rest_id}}" id="service_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Suburb</a>
			</div>
		</div><!-- /.box-header -->
		<div class="box-body table-responsive">
			@if(Session::has('serivce_message'))
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('serivce_message')}}
			</div>
			@endif
			<table id="example2" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>SrNo</th>
						<th>Suburb</th>
						<th>PostCode</th>
						<th>Delivery Charge</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if($service_list)<?php $c=1;?>
					@foreach($service_list as $slist )
					<tr>
						<td>{{$c++}}</td>
						<td>{{$slist->service_suburb}}</td>
						<td>{{$slist->service_postcode}}</td>
						<td>{{$slist->service_charge}}</td>
						<td>
							@if($slist->service_status==1)
							<span class="label label-success">Active</span>
							@else
							<span class="label label-danger">Inactive</span>
							@endif
						</td>
						<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$rest_id}}" data-service="{{$slist->service_id}}" id="update_service-{{$slist->service_id}}">Edit</a></td>
					</tr>
					@endforeach
					@endif
				</tbody>
				<tfoot>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
				</tfoot>
			</table>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
	function check_frm(tpy)
	{
		var form = $("#service_frm");
		form.validate();
		var valid =	form.valid();
		if(valid)
		{
			$("#ajax_favorite_loddder").show();
			var frm_val = 'from='+tpy+'&'+$('#service_frm').serialize();
			$.ajax({
				type: "POST",
				url: "{{url('/sub-admin/service_action')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#Servicearea').html(msg);
				}
			});
		}
		else
		{
			return false;
		}
	}
</script>