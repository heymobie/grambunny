<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Name</th>
			<th>Email</th>
			<th>Contact No</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php $i=1; ?>
		@foreach ($vendor_list as $list)
		<?php
		$rest_count = 	 DB::table('restaurant')->where('vendor_id', '=' ,$list->vendor_id)->count();
		?>
		<tr>
			<td>{{ $i }}</td>
			<td>{{ $list->name}}</td>
			<td>{{ $list->email }}</td>
			<td>{{ $list->mob_no }}</td>
			<td>@if($list->vendor_status==1)
				<span class="label label-success">Active</span>
				@else
				<span class="label label-danger">Inactive</span>@endif</td>
				<td>
					<a href="{{url('sub-admin/vendor-form/')}}/{{ $list->vendor_id }}">
						<i class="fa fa-edit"></i></a>
						<a title="View" href="{{url('sub-admin/vendor_profile/')}}/{{ $list->vendor_id }}"><i class="fa fa-eye"></i></a>
						<a title="Delete Vendor" href="{{url('sub-admin/vendor_delete')}}/{{ $list->vendor_id }}" onclick="return delete_parent('<?php echo  $rest_count ;?>')">  <i class="fa fa-trash-o"></i></a>
					</td>
				</tr>
				<?php $i++; ?>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
		</table>
		<script type="text/javascript">
			$(function() {
				$("#example1").dataTable();
				$('#example2').dataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
			});
		</script>