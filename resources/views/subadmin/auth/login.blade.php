<style type="text/css">
.fogpass{
    display: inline-block;
    /* border: 1px solid red; */
    margin: -10px;
    float: left;
    margin-left: 32px;
}
</style>

@extends('layouts.subadminapp')
@section('content')
<div class="form-box" id="login-box">
    <div class="header">Sign in</div>
    <form  role="form" id="signin" method="POST" action="{{ url('/sub-admin/login') }}" autocomplete="off">
     {{ csrf_field() }}
     <div class="body bg-gray">

        @if(Session::has('PassSucChange'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('PassSucChange') }}
        </div>
        @endif


        @if(Session::has('Errormessage'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Errormessage') }}
        </div>
        @endif

        @if ($errors->has('email'))
        <span class="help-block">
            <strong style="color:#FF0000">{{ $errors->first('email') }}</strong>
        </span>
        @endif
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>" required="required">
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" placeholder="Password" class="form-control" name="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>">
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div>
            <div class="form-group fogpass">
                <input type="checkbox" name="remember" <?php if(isset($_COOKIE["remember"])) { echo 'checked'; } ?>/> Remember me
            </div>
            <!-- <div class="form-group fogpass">
                <a href="{{url('/sub-admin/password/reset_password')}}">
                    Forgot Password ?
                </a>
            </div> -->
        </div>
    </div>
    <div class="footer">
        <button type="submit" id="sign_in" class="btn bg-olive btn-block">Sign in</button>
        <!-- <p><a href="{{ url('/admin/password/reset') }}">I forgot my password</a></p>-->
    </div>
</form>
</div>
@endsection
