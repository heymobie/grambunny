<style type="text/css">
.fogpass{
    display: inline-block;
    /* border: 1px solid red; */
    margin: -10px;
    float: left;
    margin-left: 32px;
}
</style>

@extends('layouts.adminapp')
@section('content')
<div class="form-box" id="login-box">
    <div class="header">Enter OTP</div>
    <form  role="form" id="EnterOPT" method="POST" action="{{ url('/admin/password/enter_otp') }}">
     {{ csrf_field() }}
     <div class="body bg-gray">
        <input type="hidden" class="form-control" name="AdminID" value="{{ Session::get('Admin_id') }}">
        @if(Session::has('Succes'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Succes') }}
        </div>
        @endif
        @if (Session::has('Otp_Error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Otp_Error') }}
        </div>
        @endif
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="text" class="form-control" placeholder="OTP" name="user_otp" >
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <!-- <input id="password" type="password" placeholder="Password" class="form-control" name="password" required="required"> -->
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    <!-- <div>
        <div class="form-group fogpass">
            <input type="checkbox" name="remember"/> Remember me
        </div>
        <div class="form-group fogpass">
            <a href="{{url('/admin/password/reset_password')}}">
                Forgot Password ?
            </a>
        </div>
    </div> -->
</div>
<div class="footer">
    <button type="submit" id="sign_in" class="btn bg-olive btn-block">Submit</button>
    <!-- <p><a href="{{ url('/admin/password/reset') }}">I forgot my password</a></p>-->
</div>
</form>
</div>
@endsection

<!-- form validation start -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
       $("#EnterOPT").validate({
        rules: {
            user_otp:{
               required:true,
               number:true,
           }
       },
       messages: {
        user_otp:{
            required:'Please enter your OTP.',
            number:'Please enter valid OTP.',
        }
    }
});
   });
</script>
<!-- form validation end -->