@extends('layouts.subadmin')
@section("other_css")
<!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<meta name="_token" content="{!! csrf_token() !!}"/>
@stop
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order Management
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Order Listing</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			@if(Session::has('message'))
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('message')}}
			</div>
			@endif
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Order Listing</h3>
						<div style="float:right; margin-right:10px; margin-top:10px;">
							<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
						</div>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive">
						<form name="search_frm" id="search_frm" method="post" >
							{!! csrf_field() !!}
							<input type="hidden" name="user_id" id="user_id" value="{{$userid}}" />
							<div>
								<table cellpadding="5" cellspacing="5" width="100%" style="border:1px solid #999999" class="table">
									<tr>
										<td width="50%">
											<table cellpadding="5" cellspacing="5" width="100%">
												<tr>
													<td>Delivery Status/Order History :</td>
													<td>
														<select name="order_status" id="order_status"class="form-control">
															<option value="0">All </option>
															<option value="1">New Order</option>
															<option value="4">Confirmed Order</option>
															<option value="5">Ready Order</option>
															<option value="6">Cancelled Order</option>
															<option value="11">Pending Order</option>
															<option value="12">Accepted Order</option>
															<option value="13">Rejected Order</option>
															<option value="14">Picked Order</option>
															<option value="15">On The Way Order</option>
															<option value="16">Delivered Order</option>
														</select>
													</td>
												</tr>
											</table>
										</td>
										<td width="50%">
											<table cellpadding="5" cellspacing="5" width="100%">
												<tr>
													<td>From Date: </td>
													<td>
														<input type="text" name="fromdate" id="fromdate" value=""  class="form-control datepicker"/>
													</td>
												</tr>
												<tr>
													<td>To Date: </td>
													<td>
														<input type="text" name="todate" id="todate" value="" class="form-control datepicker"/>
													</td>
												</tr>
											</table>
											<div id="error_msg" style="color:#FF0000; display:none;"></div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" />
										</td>
									</tr>
								</table>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
				<div>
					<section>
						<div class="row">
							<div class="my-panel-data">
								<div class="col-xs-12">
									<div class="box">
										<div class="box-body table-responsive">
												<!--<div style="float:right; margin:5px;">
												<input class="btn btn-primary" type="button" id="generate_file"  value="Export">
											</div>-->
											<div id="restaurant_list">
												<table id="example2" class="table table-bordered table-hover">
													<thead>
														<tr>
															<th>SrNo</th>
															<th>Order No</th>
															<th>Order Date Time</th>
															<th>Scheduled Order</th>
															<th>User</th>
															<th>Restaurant</th>
															<th>Status</th>
															<th>Device Type</th>
															<th>Device Name</th>
															<th>Device OS</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<?php $i=1; ?>
														@if((!empty($order_detail)))
														@foreach ($order_detail as $list)
														<tr>
															<td>{{ $i }}</td>
															<td>{{ $list->order_id}}
																({{$list->order_uniqueid}})
															</td>
															<td>{{ $list->created_at}}</td>
															<td>{{ $list->order_pickdate}} {{ $list->order_picktime}}</td>
															<td>
																<?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
																<td>{{ $list->rest_name}}</td>
																<td>
																	<?php
																	$orderid = $list->order_id;
																	$dmorder_status = '';
																	$dmar_status = '';
																	$dmstatus = DB::table('delivery_man_orders')
																	->where('delivery_man_orders.order_id', '=', $orderid)
																	->select('*')
																	->get();
																	if(!empty($dmstatus)){
																		$dmorder_status = $dmstatus[0]->order_status;
																		$dmar_status = $dmstatus[0]->accept_reject_status;
																	}
																	?>
																	<?php if($list->order_status=='1'){ ?>
																		<span style="display: block;" class="label label-info deepinfo">New Order</span>
																	<?php } ?>
																	<?php if($list->order_status=='4'){ ?>
																		<span style="display: block;" class="label label-primary deepinfo">Confirmed Order</span>
																	<?php } ?>
																	<?php
																	if($dmorder_status=='0' && $dmar_status=='0'){ ?>
																		<span style="display: block;" class="label label-info deepinfo">Order Pending</span>
																	<?php }else if($dmorder_status=='0' && $dmar_status=='1'){ ?>
																		<span style="display: block;" class="label label-primary deepinfo">Order Accepted</span>
																	<?php }else if($dmorder_status=='0' && $dmar_status=='2'){ ?>
																		<span style="display: block;" class="label label-danger deepinfo">Order Rejected</span>
																	<?php }else if($dmorder_status=='1'){ ?>
																		<span style="display: block;" class="label label-info deepinfo">Order Picked</span>
																	<?php }else if($dmorder_status=='2'){ ?>
																		<span style="display: block;" class="label label-primary deepinfo">Order On The Way</span>
																	<?php }else if($dmorder_status=='3'){ ?>
																		<span style="display: block;" class="label label-success deepinfo">Order Delivered</span>
																	<?php }else if($list->order_status=='5'){ ?>
																		<span style="display: block;" class="label label-success deepinfo">Order Ready</span>
																	<?php } ?>
																	<?php if($list->order_status=='6'){ ?>
																		<span style="display: block;" class="label label-danger deepinfo">Cancelled</span>
																	<?php } ?>
																</td>
																<td>{{ $list->order_device}}</td>
																<td>{{ $list->order_devicename}}</td>
																<td>{{ $list->order_device_os}}</td>
																<td>
																	<a title="Update" href="{{url('sub-admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
																	<!-- <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>-->
																</td>
															</tr>
															<?php $i++; ?>
															@endforeach
															@endif
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
	@stop
	@section('js_bottom')
	<style>
	#ajax_favorite_loddder {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background:rgba(27, 26, 26, 0.48);
		z-index: 1001;
	}
	#ajax_favorite_loddder img {
		top: 50%;
		left: 46.5%;
		position: absolute;
	}
	.footer-wrapper {
		float: left;
		width: 100%;
		/*display: none;*/
	}
	#addons-modal.modal {
		z-index: 999;
	}
	.modal-backdrop {
		z-index: 998 !important;
	}
</style>
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#example1").dataTable();
		$('#example2').dataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
	$( function() {
		var dateToday = new Date();
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			autoclose: true,
		});
	});
	$(document).on('click', '#search_btn', function(){
		$('#error_msg').hide();
		var frmdate = $('#fromdate').val();
		var todate = $('#todate').val();
		if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
	{//compare end <=, not >=
		//your code here
		$('#error_msg').html('From date will be big from to date!');
		$('#error_msg').show();
		//alert("From date will be big from to date!");
	}
	else
	{
		$("#ajax_favorite_loddder").show();
		var frm_val = $('#search_frm').serialize();
		$.ajax({
			type: "POST",
			url: "{{url('/sub-admin/order_search_list')}}",
			data: frm_val,
			success: function(msg) {
				$("#ajax_favorite_loddder").hide();
			//alert(msg)
			$('#restaurant_list').html(msg);
		}
	});
	}
});
	$(document).on('click', '#generate_file', function(){
		$('#search_frm').attr('action', "{{url('/sub-admin/order_report')}}");
		$('#search_frm').attr('target', '_blank').submit();
	});
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>
@stop
