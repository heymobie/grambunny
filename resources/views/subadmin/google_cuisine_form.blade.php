@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Cuisine Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Cuisine Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Cuisine Form</h3>
                                </div><!-- /.box-header -->
								
					 @if(Session::has('error'))
					 <div class="alert alert-danger alert-dismissable">				
								{{Session::get('error')}}
								</div>
							@endif 
                                <!-- form start -->
								
								<form  role="form" enctype="multipart/form-data" method="POST" id="cuisine_frm" action="{{ url('/admin/get_google_data')}}">    
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Enter your zipcode or adress</label>
                                           <input type="text" class="form-control" name="location_post" id="location_post" value="" required="required" placeholder="Enter your zipcode or adress" >
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Enter distange range (in Meters)</label>
                                           <input type="text" class="form-control" name="distange_range" id="distange_range" value="" required="required" placeholder="Enter distange range (in Meters)" number="number" >
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Cuisine Name</label>
                                        </div>
										@if(!empty($cuisine_list))
										<div class="form-group">
										
											 @foreach($cuisine_list as $Clist)												 
											  <input type="radio"  required="required" class="form-control" name="cusine_id" id="cusine_id" value="{{$Clist->cuisine_id}}"> {{$Clist->cuisine_name}}&nbsp;
											  
											 	
											 @endforeach
												
                                        </div>
										@endif 		
										
										
										
										
                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		
										<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script>
		function check_email()
		{
		
		var form = $("#cuisine_frm");
		form.validate();
			var valid =	form.valid();
			
			
			if(valid){	
						
				$(form).submit();
				return true;	
			}
			else
			{
				return false;
			}		
		}
		</script>
@stop