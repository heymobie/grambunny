<div class="col-sm-12">

	<p style="float: left;text-align: center;width: 100%;">
		@if(Session::has('menu_message'))
		{{Session::get('menu_message')}}
	@endif </p>
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Menu Category</h4>
			<div class="drage" id="sortable" data-rest="{{$template_id}}">

				<div class="sub-drage ">
					<p>
						<a title="Edit" href="javascript:void(0)" data-rest="{{$template_id}}" id="show_popular_item">Popular</a>
					</p>
				</div>

				@if(!empty($menu_list))
				@foreach($menu_list as $mlist)
				<div class="sub-drage @if($mlist->menu_id==$menu_id)selected @endif"  id="listItem_{{$mlist->menu_id}}">

					<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$template_detail[0]->template_id}}" data-menu="{{$mlist->menu_id}}" id="update_menu-{{$mlist->menu_id}}"><i class="fa fa-edit"></i></a>

					<p><a href="javascript:void(0)" id="menu_cat_list-{{$mlist->menu_id}}" data-menu="{{$mlist->menu_id}}"  data-rest="{{$template_detail[0]->template_id}}">{{$mlist->menu_name}}</a></p>
				</div>
				@endforeach
				@endif

			</div>
			<form id="res_btnfrm" method="post">
				<input type="hidden" name="template_id" value="{{$template_id}}" />
				{!! csrf_field() !!}
				<a class="btn btn-primary" href="javascript:void(0)" id="add_menu" data-restid="{{$template_id}}">Add Menu Category</a>
			</form>
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">

					<section class="content">
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="box box-primary">
								<div class="box-header">
									<h3 class="box-title">Template Menu Form</h3>
								</div><!-- /.box-header -->

								<!-- form start -->

								<form  role="form" method="POST" id="rest_frm" action="{{ url('/sub-admin/menu_action') }}" enctype="multipart/form-data">
									<input type="hidden" name="template_id" value="{{$template_id}}" />

									{!! csrf_field() !!}
									<div class="box-body">

										<div class="form-group">
											<label for="exampleInputEmail1">Menu name</label>
											<input type="text" class="form-control" name="menu_name" id="	menu_name" value="" required="required">
										</div>

										<div class="form-group">
											<label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="menu_desc" id="menu_desc" ></textarea>
										</div>



										<div class="form-group">
											<label for="exampleInputEmail1">Status</label>

											<select name="menu_status" id="menu_status" class="form-control">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>

									</div><!-- /.box-body -->

									<div class="box-footer">
										<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
										<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
										<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />


									</div>
								</form>

							</div><!-- /.box -->


						</div>


					</section>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script>
	function check_frm(tpy)
	{



		if(tpy=='back')
		{
			var	valid = true;
		}
		else
		{

			var form = $("#rest_frm");
			form.validate();
			var valid =	form.valid();
		}

		if(valid)
		{
			$("#ajax_favorite_loddder").show();
			var frm_val = 'from='+tpy+'&'+$('#rest_frm').serialize();
			$.ajax({
				type: "POST",
				url: "{{url('/sub-admin/template_menu_action')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();

					$('#ajax_div').html(msg);
				}
			});
		}
		else
		{
			return false;
		}
	}
	$( function() {
		$( "#sortable" ).sortable({

			update:  function (event, ui) {
				var sort_data = $("#sortable").sortable("serialize");
				var template_id = $("#sortable").attr("data-rest");

				var data = 'template_id='+template_id+'&'+sort_data;

				$("#ajax_favorite_loddder").show();
				$.ajax({
					data: data,
					type: 'POST',
					url: "{{url('/sub-admin/template_update_sortorder')}}",
					success: function(msg) {

						$("#ajax_favorite_loddder").hide();
				 //alert(msg);
				}
			});
			}

     // revert: true
 });
		$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});
		$( "ul, li" ).disableSelection();
	} );


	$(document).on('click', '#add_menu', function(){
		alert('tttt');
		var template_id = $(this).attr('data-restid');
		if(template_id>0)
		{

			$("#ajax_favorite_loddder").show();
			var frm_val = $('#res_btnfrm').serialize();
			$.ajax({
				type: "post",
				url: "{{url('/sub-admin/template_menu_ajax_form')}}",
				data: frm_val,
				success: function(msg) {

					$("#ajax_favorite_loddder").hide();
					$('#show_allview').html(msg);
				}
			});
		}
	});

</script>