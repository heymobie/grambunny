@extends('layouts.subadmin')

@section('content')
<meta name="_token" content="{!! csrf_token() !!}"/>
<!-- Right side column. Contains the navbar and content of the page -->
<style>
.divition, .sub-drage
{
	border: 1px solid #666666;
	overflow:hidden;
}
.sub-drage
{
	margin-bottom: 6px;
	background: #cccccc;
	position:relative;
}
.sub-drage a.menu_edit_link {
	left: 4px;
	position: absolute;
	top: 6px;
	color:#000000;
}
.sub-drage p a {
	display: block;
	padding: 6px 0;
	color:#000;
}
.sub-drage.selected
{
	border:1px solid #3c8dbc;
	background: #666666;
	color: #fff !important;
}
.sub-drage.selected p a
{
	color: #fff !important;
}
.sub-drage p
{
	border-left:1px solid #666666;
	margin:0 0 0 20px;
	padding-left: 5px;
}
.drage
{
	overflow:hidden;
}
.col-sm-4.border
{
	border-right: 1px solid #ccc;
}
.col-sm-8.border {
	border-left: 1px solid #ccc;
}
.my-panel-data .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
{
	background: #3c8dbc;
	color: #fff;
}
.my-panel-data .nav-tabs>li
{
	margin: 0;
}
.my-panel-data .nav-tabs>li>a
{
	border-radius: 4px;
}
.my-panel-data .nav-tabs
{
	border:  none;
}
.my-panel-data .panel-heading
{
	padding: 12px;
	background-color: transparent !important;
	border: none;
}
.my-panel-data .panel-body {
	border-top: 2px solid #3c8dbc;
}
.my-panel-data .panel
{
	border: none;
	border-radius: 0;
	box-shadow: none;
}
.check_input_service .icheckbox_minimal.checked.disabled {
	background-position: -40px 0;
}
</style>
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Manage Menu </li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			@if(Session::has('message'))

			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('message')}}
			</div>
			@endif

			<!-- /.box -->


			<div>

				<section>
					<div class="row">
						<div class="my-panel-data">
							<div class="col-md-12">
								<div class="panel with-nav-tabs panel-default">
									<div class="panel-heading">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab1default" data-toggle="tab">
												<?php echo $template_detail[0]->template_name.' Template Menus' ;?></a>
											</li>
											<li><a class="btn btn-primary" href="<?php echo url('/sub-admin/menu_template');?>" >View Template listing</a> 	</li>
										<!--
												<li><a href="#tab2default" data-toggle="tab">Service Areas</a></li>
												<li><a href="#tab3default" data-toggle="tab">Promotions</a></li>
												<li><a href="#tab4default" data-toggle="tab">Bank Details</a></li>-->
											</ul>
										</div>
										<div class="panel-body">
											<div class="tab-content">
												<div class="tab-pane fade in active" id="tab1default">
													<div class="row" id="ajax_div">
														<div class="col-sm-12">
															<div class="divition">
																<div class="col-sm-4 border">
																	<h4>Menu Category</h4>
																	<div class="drage" id="sortable"  data-rest="{{$template_detail[0]->template_id}}">


																		<div class="sub-drage selected">
																			<p>
																				<a title="Edit" href="javascript:void(0)" data-rest="{{$template_detail[0]->template_id}}" id="show_popular_item">Popular</a>
																			</p>
																		</div>

																		@if(!empty($menu_list))
																		<?php $c = 1;?>
																		@foreach($menu_list as $mlist)
																		<div class="sub-drage" id="listItem_{{$mlist->menu_id}}">
																			<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$template_detail[0]->template_id}}" data-menu="{{$mlist->menu_id}}" id="update_menu-{{$mlist->menu_id}}"><i class="fa fa-edit"></i></a>
																			<p><a href="javascript:void(0)" id="menu_cat_list-{{$mlist->menu_id}}" data-menu="{{$mlist->menu_id}}"  data-rest="{{$template_detail[0]->template_id}}">{{$mlist->menu_name}}</a></p>
																		</div>
																		<?php $c++;?>
																		@endforeach
																		@endif

																	</div>
																	<form id="res_btnfrm" method="post">
																		<input type="hidden" name="template_id" value="{{$template_detail[0]->template_id}}" />
																		{!! csrf_field() !!}
																		<a class="btn btn-primary" href="javascript:void(0)" id="add_menu" data-templateid="{{$template_detail[0]->template_id}}">Add Menu Category</a>
																	</form>
																</div>
																<div class="col-sm-8 border">
																	<div class="divtion-data" id="show_allview">
																		<div class="containt-box">

																			@if(!empty($menu_list))

																			<h4>Popular

																				<span class="label label-success">Active</span>
																			</h4>
																			<p>&nbsp;</p>

																			@if(count($menu_cate_detail)>0)
																			<div style="border:1px solid #666666;">
																				<?php $c = 1;?>
																				<table width="100%" cellpadding="5" cellspacing="5"  border="1" bordercolor="#ddd" >

																					@foreach($menu_cate_detail as $cat_list)

																					<tr>

																						<td width="5%" valign="top">{{$c++}}</td>

																						<td width="25%" valign="top">{{$cat_list->menu_category_name}}: <br />{{$cat_list->menu_category_desc}}</td>
																						<td  width="20%">
																							@if($cat_list->menu_category_portion=='no')
																							${{$cat_list->menu_category_price}}
																							@elseif($cat_list->menu_category_portion=='yes')


																							<?php
																							$menu_sub_itme = DB::table('template_category_item')
																							->where('template_id', '=' ,$template_detail[0]->template_id)
																							->where('menu_category', '=' ,$cat_list->menu_category_id)
																							->orderBy('menu_cat_itm_id', 'asc')
																							->get();


																							if($menu_sub_itme)
																							{
																								foreach($menu_sub_itme as  $msi){
																									echo $msi->menu_item_title.' : $'.$msi->menu_item_price.'<br />';
																								}
																							}

																							?>






																							@endif
																						</td>
																						<td width="20%" valign="top">
																							<br /><br />
																							@if($cat_list->menu_cat_status==1)
																							<span class="label label-success">Active</span>
																							@else
																							<span class="label label-danger">Inactive</span>
																							@endif
																							<br /><br />

																							@if($cat_list->menu_cat_diet==1)
																							<span class="label label-success">&nbsp;</span>
																							@elseif($cat_list->menu_cat_diet==2)
																							<span class="label label-danger">&nbsp;</span>
																							@endif


																						</td>
																					</tr>
																					@endforeach
																					<tr>
																						<th></th>
																						<th></th>
																						<th></th>
																						<th></th>
																					</tr>
																				</table>
																			</div>
																			@endif

																			@endif
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>



											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>

				</div>

			</div>

		</section><!-- /.content -->
	</aside><!-- /.right-side -->



	@endsection


	@section('js_bottom')
	<!-- Start of the property info details div -->


	<style>

	#ajax_favorite_loddder {

		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background:rgba(27, 26, 26, 0.48);
		z-index: 1001;
	}
	#ajax_favorite_loddder img {
		top: 50%;
		left: 46.5%;
		position: absolute;
	}

	.footer-wrapper {
		float: left;
		width: 100%;
		/*display: none;*/
	}
	#addons-modal.modal {
		z-index: 999;
	}
	.modal-backdrop {

		z-index: 998 !important;
	}
</style>

<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>




<!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="addons-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="addons_modal_data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
			</div>
			<form action="#" method="post">
				<div class="modal-body">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">TO:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email TO">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">CC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email CC">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">BCC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email BCC">
						</div>
					</div>
					<div class="form-group">
						<textarea name="message" id="email_message" class="form-control" placeholder="Message" style="height: 120px;"></textarea>
					</div>
					<div class="form-group">
						<div class="btn btn-success btn-file">
							<i class="fa fa-paperclip"></i> Attachment
							<input type="file" name="attachment"/>
						</div>
						<p class="help-block">Max. 32MB</p>
					</div>

				</div>
				<div class="modal-footer clearfix">

					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

					<button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- jQuery UI 1.10.3 -->
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>


<script>
	function check_frm()
	{

		var form = $("#rest_frm");
		form.validate();
		var valid =	form.valid();
		if(valid)
		{
			$(form).submit();
			return true;
		}
		else
		{
			return false;
		}
	}
</script>
<script>

	/* MENU TABE START */
	$( function() {
		$( "#sortable" ).sortable({
			update:  function (event, ui) {
				var sort_data = $("#sortable").sortable("serialize");
				var template_id = $("#sortable").attr("data-rest");

				var data = 'template_id='+template_id+'&'+sort_data;

				$("#ajax_favorite_loddder").show();
				$.ajax({
					data: data,
					type: 'POST',
					url: "{{url('/sub-admin/template_update_sortorder')}}",
					success: function(msg) {

						$("#ajax_favorite_loddder").hide();
				 //alert(msg);
				}
			});
			}
      //revert: true
  });

		$( "#draggable" ).draggable({
			connectToSortable: "#sortable",
			helper: "clone",
			revert: "invalid"
		});
		$( "ul, li" ).disableSelection();
	} );

	$(document).on('click', '#add_menu', function(){
		var template_id = $(this).attr('data-templateid');
		if(template_id>0)
		{

			$("#ajax_favorite_loddder").show();
			var frm_val = $('#res_btnfrm').serialize();
			$.ajax({
				type: "post",
				url: "{{url('/sub-admin/template_menu_ajax_form')}}",
				data: frm_val,
				success: function(msg) {

					$("#ajax_favorite_loddder").hide();
					$('#show_allview').html(msg);
				}
			});
		}
	});

	$(document).on('click', '[id^="update_menu-"]', function() {
		var menu =$(this).attr('data-menu');
		var template_id = $(this).attr('data-rest') ;
		$("#ajax_favorite_loddder").show();
	 //var frm_val = $('#res_btnfrm').serialize();

	 var frm_val = "template_id="+template_id+"&menu="+menu;
	 $.ajax({
	 	type: "post",
	 	url: "{{url('/sub-admin/template_menu_ajax_form')}}",
	 	data: frm_val,
	 	success: function(msg) {

	 		$("#ajax_favorite_loddder").hide();
	 		$('#show_allview').html(msg);
	 	}
	 });


	});


	/*  MENU ITEM */
	$(document).on('click', '#add_menu_item', function(){


		$("#ajax_favorite_loddder").show();
		var frm_val = $('#itm_btnfrm').serialize();
		$.ajax({
			type: "post",
			url: "{{url('/sub-admin/template_menu_ajax_itemform')}}",
			data: frm_val,
			success: function(msg) {

				$("#ajax_favorite_loddder").hide();
				$('#show_allview').html(msg);
			}
		});


	});

	$(document).on('click', '[id^="menu_cat_list-"]', function() {
		var menu =$(this).attr('data-menu');
		var template_id = $(this).attr('data-rest') ;
		$("#ajax_favorite_loddder").show();

		var frm_val = "menu="+menu+"&template_id="+template_id;
		$.ajax({
			type: "post",
			url: "{{url('/sub-admin/template_show_menudetail')}}",
			data: frm_val,
			success: function(msg) {

				$("#ajax_favorite_loddder").hide();
				$('#ajax_div').html(msg);
			}
		});
	});

	/*UPDATE MENU ITEMS */
	$(document).on('click', '[id^="update_item_data-"]', function() {
		var menu =$(this).attr('data-menu');
		var rest = $(this).attr('data-rest') ;
		var item_id = $(this).attr('data-item') ;
		$("#ajax_favorite_loddder").show();
	 //var frm_val = $('#res_btnfrm').serialize();

	 var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id;
	 $.ajax({
	 	type: "post",
	 	url: "{{url('/sub-admin/template_update_menu_item')}}",
	 	data: frm_val,
	 	success: function(msg) {

	 		$("#ajax_favorite_loddder").hide();
	 		$('#show_allview').html(msg);
	 	}
	 });
	});


	/* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */
	$(document).on('click', '[id^="addon_list_data-"]', function() {



		var menu =$(this).attr('data-menu');
		var rest = $(this).attr('data-rest') ;
		var item_id = $(this).attr('data-item') ;
		$("#ajax_favorite_loddder").show();

		var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id;
		$.ajax({
			type: "post",
			url: "{{url('/sub-admin/template_addons_list')}}",
			data: frm_val,
			success: function(msg) {
				$("#ajax_favorite_loddder").hide();
				$('#addons_modal_data').html(msg);
				$("#addons-modal").modal('show');
			}
		});

	});


	$(document).on('click', '#add_addons', function() {

//	$("#ajax_favorite_loddder").show();
	 //$("#addons-modal").modal('show');
	// $("#addons-modal").modal('hide');

	var menu =$(this).attr('data-menu');
	var rest = $(this).attr('data-rest') ;
	var item_id = $(this).attr('data-item') ;
	$("#ajax_favorite_loddder").show();

	var frm_val = "template_id="+rest+"&menu="+menu+"&menu_itme="+item_id;
	$.ajax({
		type: "post",
		url: "{{url('/sub-admin/template_addons_form')}}",
		data: frm_val,
		success: function(msg) {

			$("#ajax_favorite_loddder").hide();
			$('#addons_modal_data').html(msg);
			$("#addons-modal").modal('show');

		}
	});

});

	$(document).on('click', '[id^="addon_update_data-"]', function() {


		var menu =$(this).attr('data-menu');
		var rest = $(this).attr('data-rest') ;
		var item_id = $(this).attr('data-item') ;
		var addon_id = $(this).attr('data-addon') ;
		$("#ajax_favorite_loddder").show();

		var frm_val = "template_id="+rest+"&menu="+menu+"&menu_itme="+item_id+"&addon_id="+addon_id;
		$.ajax({
			type: "post",
			url: "{{url('/sub-admin/template_addons_form')}}",
			data: frm_val,
			success: function(msg) {

				$("#ajax_favorite_loddder").hide();
				$('#addons_modal_data').html(msg);
				$("#addons-modal").modal('show');

			}
		});


	});





	/* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */

	/* MENU TAB END */



	/* PROMOTIONS TAB END*/




	/* BANKDETAIL TAE END */

	$(document).on('click', '[id^="show_popular_item"]', function(){


		$("#ajax_favorite_loddder").show();

		var template_id = $(this).attr('data-rest');
		var frm_val = "template_id="+template_id;
		$.ajax({
			type: "post",
			url: "{{url('/sub-admin/template_show_popular')}}",
			data: frm_val,
			success: function(msg) {

				$("#ajax_favorite_loddder").hide();
				$('#ajax_div').html(msg);
			}
		});
	});




	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>
@stop