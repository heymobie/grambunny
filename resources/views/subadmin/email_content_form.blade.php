@extends('layouts.subadmin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Email/Notification Template Content Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Email/Notification Template Content Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Email/Notification Template Content Form</h3>
				</div><!-- /.box-header -->

				@if(Session::has('error'))
				<div class="alert alert-danger alert-dismissable">
					{{Session::get('error')}}
				</div>
				@endif
				<!-- form start -->

				<form  role="form" method="POST" id="cuisine_frm" action="{{ url('/sub-admin/email_content_action') }}">
					<input type="hidden" name="email_id" value="{{$id}}" />

					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Title</label>
							<input type="text" class="form-control" id="email_title" name="email_title"  value="@if($id>0){{$page_detail[0]->email_title}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Content</label>
							<textarea name="email_content" id="email_content" class="form-control"> @if($id>0){{$page_detail[0]->email_content}}@endif</textarea>
						</div>
						<div class="form-group">
							<label for="variable">Variables to use</label>
							<div><label>For Order Id: order_id</label></div>
							<div><label>For Order Unique Id: order_uniqueid</label></div>
							<!-- <div><label>For User Name: f_name</label></div> -->
						</div>






					</div> <!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />


						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>

			</div><!-- /.box -->


		</div>


	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<!-- CK Editor -->


<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



@stop