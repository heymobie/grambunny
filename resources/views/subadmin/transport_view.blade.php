@extends('layouts.admin')

@section('content')
<meta name="_token" content="{!! csrf_token() !!}"/>
<!-- Right side column. Contains the navbar and content of the page -->
<style>
	.divition, .sub-drage
	{
		border: 1px solid #666666;
		overflow:hidden;
	}
	.sub-drage
	{
		margin-bottom: 6px;
		background: #cccccc;
		position:relative;
	}
	.sub-drage a.menu_edit_link {
		left: 4px;
		position: absolute;
		top: 6px;
		color:#000000;
	}
	.sub-drage p a {
		display: block;
		padding: 6px 0;
		color:#000;
	}
	.sub-drage.selected
	{
		border:1px solid #3c8dbc;
		background: #666666;
		color: #fff !important;
	}
	.sub-drage.selected p a
	{
		color: #fff !important;
	}
	.sub-drage p
	{
		border-left:1px solid #666666;
		margin:0 0 0 20px;
		padding-left: 5px;
	}
	.drage
	{
		overflow:hidden;
	}
	.col-sm-4.border
	{
		border-right: 1px solid #ccc;
	}
	.col-sm-8.border {
		border-left: 1px solid #ccc;
	}
	.my-panel-data .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
	{
		background: #3c8dbc;
		color: #fff;
	}	
	.my-panel-data .nav-tabs>li
	{
		margin: 0;
	}
	.my-panel-data .nav-tabs>li>a
	{
		border-radius: 4px;
	}
	.my-panel-data .nav-tabs
	{
		border:  none;
	}
	.my-panel-data .panel-heading
	{
		padding: 12px;
		background-color: transparent !important;
		border: none;
	}
	.my-panel-data .panel-body {
	    border-top: 2px solid #3c8dbc;
	}
	.my-panel-data .panel
	{
		border: none;
		border-radius: 0;
		box-shadow: none;
	}	
	.check_input_service .icheckbox_minimal.checked.disabled {
		background-position: -40px 0;
	}
</style>
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Transport Profile Page
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Transport Profile Page</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
							 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Transport {{$transport_detail[0]->transport_name }} Profile Page</h3>
                                </div><!-- /.box-header -->
				  <div class="box-body table-responsive">
					
                                    <table  width="100%" cellpadding="5" cellspacing="5" style="border:1px solid #666666; width:100%">
										<!--<tr>
											<td align="center"> RATING : 4.5 </td>
										</tr>-->
										<tr>
											<td>
												<table width="100%" cellpadding="5" cellspacing="5" border="0">
													<tr>
														<td valign="top" width="15%">
														@if(!empty($transport_detail[0]->transport_logo))
														<img src="{{ url('/') }}/uploads/transport/{{ $transport_detail[0]->transport_logo }}" width="100px;" height="100px;">
														@else														
														<img src="{{ url('/') }}/design/front/img/logo.png" width="100px;">
														@endif
														</td>
														<td valign="top"><table  width="100%" cellpadding="5" cellspacing="5">
													 <tr>
													 	<td>Transport Name:</td>
														<td> {{$transport_detail[0]->transport_name}}</td>
													</tr>
													 <tr>
													 	<td>Contact Landline:</td>
														<td> {{$transport_detail[0]->transport_landline}}</td>
													 </tr>
													 <tr>
													 	<td>Contact Mob:</td>
														<td> {{$transport_detail[0]->transport_contact}}</td>
													 </tr>
													 <tr>
													 	<td>Email:</td>
														<td> {{$transport_detail[0]->transport_email}}</td>
													 </tr>
													</table></td>
														<td  valign="middle">
															<b>Description</b>
															<p style="border:1px solid #CCCCCC">
															{{str_limit($transport_detail[0]->transport_desc,200)}}
															</p>
														</td>
														<td valign="middle"><table  width="100%" cellpadding="5" cellspacing="5">
														
														 <tr>
															<td>Status:</td>
															<td>{{$transport_detail[0]->transport_status}}</td>
														 </tr>
														 <tr>
															<td>Classification:</td>
															<td>
															@if($transport_detail[0]->transport_classi=='1') Sponsored @endif
															@if($transport_detail[0]->transport_classi=='2') Popular @endif
															@if($transport_detail[0]->transport_classi=='3') Special @endif
															@if($transport_detail[0]->transport_classi=='4') New @endif
															@if($transport_detail[0]->transport_classi=='5') Standard @endif
															
															</td>
														 </tr>
														 <tr>
															<td>Commission:</td>
															<td>{{$transport_detail[0]->transport_commission}}%</td>
														 </tr>
												</table>
														</td>	
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table  width="100%" cellpadding="5" cellspacing="5" border="0">
													<tr>
														<td valign="top">
															<table  width="100%" cellpadding="5" cellspacing="5">
													 
													 <tr>
													 	<td>Address Line1:</td>
														<td> {{$transport_detail[0]->transport_address }}</td>
													 </tr>
													 <tr>
													 	<td>Address Line2:</td>
														<td> {{$transport_detail[0]->transport_address2 }}</td>
													 </tr>
													 <tr>
													 	<td>Suburb:</td>
														<td> {{$transport_detail[0]->transport_suburb }}</td>
													 </tr>
													 <tr>
													 	<td>City:</td>
														<td> {{$transport_detail[0]->transport_city }}</td>
													 </tr>
													 <tr>
													 	<td>State:</td>
														<td> {{$transport_detail[0]->transport_state }}</td>
													 </tr>
													 <tr>
													 	<td>PostCode:</td>
														<td> {{$transport_detail[0]->transport_pcode}}</td>
													 </tr>
													</table>
														</td>
														<td>
															<table width="100%" cellpadding="5" cellspacing="5">
													
														
													   <tr>
															<td>Vender:</td>
															<td><a href="{{url('admin/vendor_profile/')}}/{{ $transport_detail[0]->transport_vendor}}">{{$transport_detail[0]->name}}</a></td>
														</tr>
														<tr>
														<td colspan="2" align="center">
														<a class="btn btn-primary" href="{{url('admin/transport-update/')}}/{{ $transport_detail[0]->transport_id }}" style="float:right;">Edit</a> 
														</td>
														</tr>
												 </table>
														</td>
														<td  valign="bottom">
															
													       
														</td>
														<td valign="top">
															<table  width="100%" cellpadding="5" cellspacing="5" style="border:1px solid #CCCCCC">
															 <tr>
																<td style="background:#CCCCCC;"><B>Open Hours</B></td>
															</tr>
															
															@if(!empty($transport_detail[0]->transport_mon))														
															 <tr>
																<td>Monday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_mon)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('mon',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
																
															 </tr>
															@endif 
															@if(!empty($transport_detail[0]->transport_tues))														
															 <tr>
																<td>Tuesday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_tues)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('tue',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															
															@if(!empty($transport_detail[0]->transport_wed))														
															 <tr>
																<td>Wednesday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_wed)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('wed',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															
															@if(!empty($transport_detail[0]->transport_thus))														
															 <tr>
																<td>Thusday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_thus)}}  @if((!empty($transport_detail[0]->rest_close)) && ( in_array('thu',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															
															@if(!empty($transport_detail[0]->transport_fri))														
															 <tr>
																<td>Friday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_fri)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('fri',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															
															@if(!empty($transport_detail[0]->transport_sat))														
															 <tr>
																<td>Saturday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_sat)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('sat',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															
															@if(!empty($transport_detail[0]->transport_sun))														
															 <tr>
																<td>Sunday - {{ str_replace('_', ' to ',$transport_detail[0]->transport_sun)}}  @if((!empty($transport_detail[0]->transport_close)) && ( in_array('sun',(explode(',',$transport_detail[0]->transport_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
															 </tr>
															@endif 
															</table>
														</td>
													</tr>
												</table>											
																									
										    </td>
										</tr>
										<tr>
											<td>
												<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);" />				
													
											</td>
										</tr>
									</table>
								
								<div>
									
								</div>	
									
									
									
                                </div>
								
								
								
                            </div><!-- /.box -->
							
							
							<div>
							
							<section>
								<div class="row">
									<div class="my-panel-data">
										<div class="col-md-12">
											<div class="panel with-nav-tabs panel-default">
												<div class="panel-heading">
														<ul class="nav nav-tabs">
															<li class="active"><a href="#tab1default" data-toggle="tab">Vehicles</a></li>
															<li><a href="#tab2default" data-toggle="tab">Service Areas</a></li>
															<li><a href="#tab3default" data-toggle="tab">Promotions</a></li>
															<li><a href="#tab4default" data-toggle="tab">Bank Details</a></li>
														</ul>
												</div>
												<div class="panel-body">
													<div class="tab-content">
														<div class="tab-pane fade in active" id="tab1default">
														  <div class="row" id="ajax_div">
															<div class="col-sm-12">
																<div class="divition">
																	<div class="col-sm-4 border">
																		<h4>Vehicle List</h4>
																		<div class="drage" id="sortable"  data-rest="{{$transport_detail[0]->transport_id}}">
																		
																		
																			
																		@if(!empty($vehicle_list))
																		<?php $c = 1;?>
																		  @foreach($vehicle_list as $vlist)
																			<div class="sub-drage" id="listItem_{{$vlist->vehicle_id}}">
																				<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$transport_detail[0]->transport_id}}" data-menu="{{$vlist->vehicle_id}}" id="update_vehicle-{{$vlist->vehicle_id}}"><i class="fa fa-edit"></i></a>
																				<p><a href="javascript:void(0)" id="menu_cat_list-{{$vlist->vehicle_id}}" data-menu="{{$vlist->vehicle_id}}"  data-rest="{{$transport_detail[0]->transport_id}}">{{$vlist->vehicle_rego}}</a></p>
																			</div>
																			<?php $c++;?>
																	  @endforeach
																		@endif
																			
																		</div>	
																		<form id="res_btnfrm" method="post">
																		<input type="hidden" name="transport_id" value="{{$transport_detail[0]->transport_id}}" />
																		{!! csrf_field() !!}
																		<a class="btn btn-primary" href="javascript:void(0)" id="add_vehicle" data-restid="{{$transport_detail[0]->transport_id}}">Add Vehicle</a> 	
																		</form>											
																	</div>
																	<div class="col-sm-8 border">
																		<div class="divtion-data" id="show_allview">
																		<div class="containt-box">
																									
																	@if(!empty($vehicle_list))
																	
															
									<table border="0" width="100%" cellpadding="2" cellspacing="2">
									<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Status</td>
													<td>
															@if($vehicle_detail[0]->vehicle_status=='1') Active
															@else Inactive @endif
													</td>
													</tr>
												</table>
											</td>	
										</tr>
										
										<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
											 <tr>
												 <td>
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
														<td>
																<?php					
						$img_detail = DB::table('vehicle_img')		
						->where('vehicle_id', '=' ,$vehicle_detail[0]->vehicle_id)
						->where('isCover', '=' ,'1')
						->get();
						
						if(count($img_detail)>0){
						?>
						
						  <img src="{{ url('/') }}/uploads/vehicle/{{$img_detail[0]->imgPath }}" height="120" width="100" /> 
						 <?php }else{?> 	Add Image <?php }?>
						  
						  
													</td>
														<td>  </td>
														</tr>
													</table>
												</td>	
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Rego:</td>
															<td>{{$vehicle_detail[0]->vehicle_rego}}</td>
														</tr>
														<tr>
															<td>Vehicle Make:</td>
															<td>{{$vehicle_detail[0]->make_name}}</td>
														</tr>
														<tr>
															<td>Vehicle Model:</td>
															<td>{{$vehicle_detail[0]->model_name}}</td>
														</tr>
														<tr>
															<td>Year:</td>
															<td>{{$vehicle_detail[0]->vehicle_year}}</td>
														</tr>
														<tr>
															<td>Vehicle Category:</td>
															<td>{{$vehicle_detail[0]->vcate_name}}</td>
														</tr>
													</table>
												</td>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Vehicle Class:</td>
															<td>{{$vehicle_detail[0]->vclass_name}}</td>
														</tr>
														<tr>
															<td>Max Pax:</td>
															<td>{{$vehicle_detail[0]->vehicle_pax}}</td>
														</tr>
														<tr>
															<td>Max Large Bags:</td>
															<td>{{$vehicle_detail[0]->vehicle_largebag}}</td>
														</tr>
														<tr>
															<td>Max Small Bags:</td>
															<td>{{$vehicle_detail[0]->vehicle_smallbag}}</td>
														</tr>
														<tr>
															<td>Minimum Rates $:</td>
															<td>{{$vehicle_detail[0]->vehicle_minrate}}</td>
														</tr>
													</table>
												</td>
											</tr>
											</table>	
											</td>	
										</tr>
										<tr>
											<td  style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>A/C:</td>
														<td>@if($vehicle_detail[0]->vehicle_ac=='1') Yes
															@else no @endif
														</td>
														<td>Music System:</td>
														<td>@if($vehicle_detail[0]->vehicle_music=='1') Yes
															@else no @endif
														</td>
														<td>Video System:</td>
														<td>
															@if($vehicle_detail[0]->vehicle_video=='1') Yes
															@else no @endif
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Description:</td>
														<td>{{$vehicle_detail[0]->vehcile_desc}}</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Driver first Name:</td>
														<td>{{$vehicle_detail[0]->vehicle_drvierfname}}</td>
														</tr>
													<tr>
														<td>Driver Last Name:</td>
														<td>{{$vehicle_detail[0]->vehicle_driverlanme}}</td>
													</tr>
													<tr>
														<td>Driver Mobile No:</td>
														<td>{{$vehicle_detail[0]->vehicle_mobno}}</td>
													</tr>
													<tr>
														<td>Driver Overnight Allowance $:</td>
														<td>{{$vehicle_detail[0]->vehicle_allownce}}</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										
										
									</table>
									<br />
									<form id="itm_btnfrm" method="post">
																	{!! csrf_field() !!}
															<input type="hidden" name="trans_id" value="{{$transport_detail[0]->transport_id}}" />
															<input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}" />
															<a class="btn btn-primary" href="javascript:void(0)"  id="add_charges">Add Rates</a> 
															</form>
									<br />
									
									
					
						@if(!empty($rate_detail))
						<div style="border:1px solid #666666;">
							<?php $c = 1;?>
							<table width="100%" cellpadding="5" cellspacing="5" border="0" >
							
							@foreach($rate_detail as $rate_list)
							
							<tr>
							<td width="10%" valign="top">
							<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$rate_list->rate_vehicleid}}" data-rest="{{$rate_list->rate_transid}}" data-item="{{$rate_list->rate_id}}" id="update_item_data-{{$rate_list->rate_id}}">Edit</a>
							<br />
							<br />
							
							
							@if($rate_list->rate_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif
							<br />
							<br />

							</td>
							<td width="5%" valign="top">{{$c++}}</td>
								<td width="25%" valign="top">{{str_replace('_',' ',$rate_list->rate_name)}}: <br />
								{{$rate_list->rate_desc}}</td>
								<td  width="20%">
								@if($rate_list->rate_diff=='no')
								${{$rate_list->rate_price}}
								@elseif($rate_list->rate_diff=='yes')
								
								@if($rate_list->rate_lprice>0)
								{{$rate_list->rate_lname}} : ${{$rate_list->rate_lprice}} <br />
								@endif
								
								@if($rate_list->rate_mprice>0)
								{{$rate_list->rate_mname}} : ${{$rate_list->rate_mprice}}<br />
								@endif
								@if($rate_list->rate_sprice>0)
								{{$rate_list->rate_sname}} : ${{$rate_list->rate_sprice}}
								@endif
								
								
								 
								@endif
								</td>
								<td  width="20%">
								<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$rate_list->rate_vehicleid}}" data-rest="{{$rate_list->rate_transid}}" data-item="{{$rate_list->rate_id}}" id="addon_list_data-{{$rate_list->rate_id}}" >Add/Edit Addons</a> 
								</td>
							</tr>
						@endforeach
						<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
							</table>
						</div>
						@endif						
															
															
															
															
															
														
																	
																	@endif
																	
																	
																	
																	
																	
																		</div>
																															
																		</div>
																	</div>
																</div>
															</div>
														</div>
														</div>
														<div class="tab-pane fade" id="tab2default">
															<div id="Servicearea">					
																<div class="col-xs-12">
																	<div class="box">
																		<div class="box-header">
																			<h3 class="box-title">Service Areas Listing</h3><br />
																			<div style="margin-top:10px;">
																			<a  href="javascript:void(0)" data-rest="{{$transport_detail[0]->transport_id}}" id="service_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Suburb</a>
																				</div>
																			
																		</div><!-- /.box-header -->
																		<div class="box-body table-responsive">
														
															 @if(Session::has('serivce_message'))
															 
															 <div class="alert alert-success alert-dismissable">
																  <i class="fa fa-check"></i>
																   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
																			   {{Session::get('serivce_message')}}
															 </div>
															@endif 
																	
																			<table id="example2" class="table table-bordered table-hover">
																				<thead>
																					<tr>
																						<th>SrNo</th>
																						<th>Suburb</th>
																						<th>PostCode</th>
																						<th>Delivery Charge</th>
																						<th>Status</th>
																						<th>Action</th>
																					</tr>
																				</thead>
																				<tbody>
																				@if($service_list)<?php $c=1;?>
																				  @foreach($service_list as $slist )
																					<tr>
																						<td>{{$c++}}</td>
																						<td>{{$slist->tport_service_suburb}}</td>
																						<td>{{$slist->tport_service_postcode}}</td>
																						<td>{{$slist->tport_service_charge}}</td>
																						<td>
																		@if($slist->tport_service_status==1) 
																			<span class="label label-success">Active</span>
																		@else 
																			<span class="label label-danger">Inactive</span>
																		@endif
																						</td>
																						<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$transport_detail[0]->transport_id}}" data-service="{{$slist->tport_service_id}}" id="update_service-{{$slist->tport_service_id}}">Edit</a></td>
																					</tr>	
																				  @endforeach
																				@endif  		
																				</tbody>
																				<tfoot>
																					<tr>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																					</tr>
																				</tfoot>
																			</table>
																		</div><!-- /.box-body -->
																	</div><!-- /.box -->
																</div>
															</div>
															
														</div>
														<div class="tab-pane fade" id="tab3default">
														
														<div id="Promotions">					
																<div class="col-xs-12">
																	<div class="box">
																		<div class="box-header">
																			<h3 class="box-title">Promotions Listing</h3><br />
																			<div style="margin-top:10px;">
																			<a  href="javascript:void(0)" data-rest="{{$transport_detail[0]->transport_id}}" id="promo_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Promo</a>
																				</div>
																			
																		</div><!-- /.box-header -->
																		<div class="box-body table-responsive">
														
															 @if(Session::has('promo_message'))
															 
															 <div class="alert alert-success alert-dismissable">
																  <i class="fa fa-check"></i>
																   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
																			   {{Session::get('promo_message')}}
															 </div>
															@endif 
																	
																			<table id="example2" class="table table-bordered table-hover">
																				<thead>
																					<tr>
																						<th>SrNo</th>
																						<th>On Order</th>
																						<th>Description</th>
																						<th>Mode</th>
																						<th>Value</th>
																						<th>Start</th>
																						<th>End</th>
																						<th>Status</th>
																						<th>Action</th>
																					</tr>
																				</thead>
																				<tbody>																																					
																				@if($promo_list)<?php $c=1;?>
																				  @foreach($promo_list as $plist )
																					<tr>
																						<td>{{$c++}}</td>
																						<td>{{$plist->tport_promo_on}}</td>
																						<td>{{$plist->tport_promo_desc}}</td>
																						<td>{{$plist->tport_promo_mode}}</td>
																						<td>{{$plist->tport_promo_value}}</td>
																						<td>{{$plist->tport_promo_start}}</td>
																						<td>{{$plist->tport_promo_end}}</td>
																						<td>
																							@if($plist->tport_promo_status==1) 
																								<span class="label label-success">Active</span>
																							@else 
																								<span class="label label-danger">Inactive</span>
																							@endif
																						</td>
																						<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$transport_detail[0]->transport_id}}" data-promo="{{$plist->tport_promo_id}}" id="update_promo-{{$plist->tport_promo_id}}">Edit</a></td>
																					</tr>	
																				  @endforeach
																				@endif    		
																				</tbody>
																				<tfoot>
																					<tr>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																					</tr>
																				</tfoot>
																			</table>
																		</div><!-- /.box-body -->
																	</div><!-- /.box -->
																</div>
															</div>
														</div>
														<div class="tab-pane fade" id="tab4default">
														<div id="BankDetail">					
																<div class="col-xs-12">
																	<div class="box">
																		<div class="box-header">
																			<h3 class="box-title">Bank Detail Listing</h3><br />
																			<div style="margin-top:10px;">
																			<a  href="javascript:void(0)" data-rest="{{$transport_detail[0]->transport_id}}" id="bank_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Details</a>
																				</div>
																			
																		</div><!-- /.box-header -->
																		<div class="box-body table-responsive">
														
															 @if(Session::has('bank_message'))
															 
															 <div class="alert alert-success alert-dismissable">
																  <i class="fa fa-check"></i>
																   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
																			   {{Session::get('bank_message')}}
															 </div>
															@endif 
																	
																			<table id="example2" class="table table-bordered table-hover">
																				<thead>
																					<tr>
																						<th>SrNo</th>
																						<th>Name</th>
																						<th>Bank</th>
																						<th>Branch</th>
																						<th>BSB</th>
																						<th>Account</th>
																						<th>Status</th>
																						<th>Action</th>
																					</tr>
																				</thead>
																				<tbody>																				
																					@if($bank_list)<?php $c=1;?>
																					  @foreach($bank_list as $blist )
																						<tr>
																							<td>{{$c++}}</td>
																							<td>{{$blist->tprot_bank_uname}}</td>
																							<td>{{$blist->tprot_bank_name}}</td>
																							<td>{{$blist->tprot_bank_branch}}</td>
																							<td>{{$blist->tprot_bank_bsb}}</td>
																							<td>{{$blist->tprot_bank_acc}}</td>
																							<td>
																								@if($blist->tprot_bank_status==1) 
																									<span class="label label-success">Active</span>
																								@else 
																									<span class="label label-danger">Inactive</span>
																								@endif
																							</td>
																							<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$transport_detail[0]->transport_id}}" data-bank="{{$blist->tprot_bank_id}}" id="update_bank-{{$blist->tprot_bank_id}}">Edit</a></td>
																						</tr>	
																					  @endforeach
																					@endif 		
																				</tbody>
																				<tfoot>
																					<tr>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																						<th>&nbsp;</th>
																					</tr>
																				</tfoot>
																			</table>
																		</div><!-- /.box-body -->
																	</div><!-- /.box -->
																</div>
															</div></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>  
							</section>

						</div>
						

					
        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection

<!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="addons-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="addons_modal_data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
			</div>
			<form action="#" method="post">
				<div class="modal-body">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">TO:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email TO">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">CC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email CC">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">BCC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email BCC">
						</div>
					</div>
					<div class="form-group">
						<textarea name="message" id="email_message" class="form-control" placeholder="Message" style="height: 120px;"></textarea>
					</div>
					<div class="form-group">
						<div class="btn btn-success btn-file">
							<i class="fa fa-paperclip"></i> Attachment
							<input type="file" name="attachment"/>
						</div>
						<p class="help-block">Max. 32MB</p>
					</div>

				</div>
				<div class="modal-footer clearfix">

					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

					<button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('js_bottom')
			<!-- Start of the property info details div -->
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script>
		function check_frm()
		{
		
		var form = $("#rest_frm");
				form.validate();
			var valid =	form.valid();
			if(valid)
			{		
				$(form).submit();
				return true;	
			}
			else
			{
				return false;
			}		
		}
		</script>
		 <script>
		 
		 /* MENU TABE START */
  $( function() {
    $( "#sortable" ).sortable({
        update:  function (event, ui) {
	        var sort_data = $("#sortable").sortable("serialize");
	        var transport_id = $("#sortable").attr("data-rest");
			
			var data = 'trans_id='+transport_id+'&'+sort_data;
			
				$("#ajax_favorite_loddder").show();	
            $.ajax({
                data: data,
                type: 'POST',
                url: "{{url('/admin/update_vehicle_sortorder')}}",
				success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				 //alert(msg);
				}
            });
	}
      //revert: true
    });
	
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
  } );
  
 $(document).on('click', '#add_vehicle', function(){ 
	var transport_id = $(this).attr('data-restid');
	//alert(transport_id);
	if(transport_id>0)
	{
		
	 $("#ajax_favorite_loddder").show();	
	 var frm_val = $('#res_btnfrm').serialize();	
		$.ajax({
		type: "post",
		url: "{{url('/admin/vehicle_ajax_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#show_allview').html(msg);
			}
		});
	}
 }); 
 
 $(document).on('click', '[id^="update_vehicle-"]', function() {
	var vehicle_id =$(this).attr('data-menu');
 	var transport_id = $(this).attr('data-rest') ;
	 $("#ajax_favorite_loddder").show();	
	 //var frm_val = $('#res_btnfrm').serialize();
	 
	 	var frm_val = "transport_id="+transport_id+"&vehicle_id="+vehicle_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/vehicle_ajax_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#show_allview').html(msg);
			}
		});
		
 
 }); 
 
 
 /*  MENU ITEM */
  $(document).on('click', '#add_charges', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();	
	 var frm_val = $('#itm_btnfrm').serialize();	
		$.ajax({
		type: "post",
		url: "{{url('/admin/vehicle_charge_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#show_allview').html(msg);
			}
		});
 
 
 });
  
  $(document).on('click', '[id^="menu_cat_list-"]', function() {
 	var vehicle_id =$(this).attr('data-menu');
 	var transport_id = $(this).attr('data-rest') ;
	 $("#ajax_favorite_loddder").show();	
	 
	 	var frm_val = "vehicle_id="+vehicle_id+"&transport_id="+transport_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/show_vehicledetail')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#ajax_div').html(msg);
			}
		});
});

/*UPDATE MENU ITEMS */
  $(document).on('click', '[id^="update_item_data-"]', function() {
 	var vehicle_id =$(this).attr('data-menu');
 	var trans_id = $(this).attr('data-rest') ;
 	var rate_id = $(this).attr('data-item') ;
	 $("#ajax_favorite_loddder").show();	
	 //var frm_val = $('#res_btnfrm').serialize();
	 
	 	var frm_val = "trans_id="+trans_id+"&vehicle_id="+vehicle_id+"&rate_id="+rate_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/vehicle_charge_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#show_allview').html(msg);
			}
		});
});


  /* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */
$(document).on('click', '[id^="addon_list_data-"]', function() { 

//	$("#ajax_favorite_loddder").show();
	 //$("#addons-modal").modal('show');
	 	
	var vehicle_id =$(this).attr('data-menu');
 	var trans_id = $(this).attr('data-rest') ;
 	var rate_id = $(this).attr('data-item') ;
	 $("#ajax_favorite_loddder").show();	
	 
	var frm_val = "trans_id="+trans_id+"&vehicle_id="+vehicle_id+"&rate_id="+rate_id;
	$.ajax({
	type: "post",
	url: "{{url('/admin/vehicle_addons_list')}}",
	data: frm_val,
		success: function(msg) {
			
			$("#ajax_favorite_loddder").hide();	
			
			$('#addons_modal_data').html(msg);
			
			$("#addons-modal").modal('show');
			
		}
	});
	
});


$(document).on('click', '#add_addons', function() { 

//	$("#ajax_favorite_loddder").show();
	 //$("#addons-modal").modal('show');
	// $("#addons-modal").modal('hide');
	 	
	var vehicle_id =$(this).attr('data-menu');
 	var trans_id = $(this).attr('data-rest') ;
 	var rate_id = $(this).attr('data-item') ;
	 $("#ajax_favorite_loddder").show();	
	 
	var frm_val = "trans_id="+trans_id+"&vehicle_id="+vehicle_id+"&rate_id="+rate_id;
	$.ajax({
	type: "post",
	url: "{{url('/admin/vehicle_addons_form')}}",
	data: frm_val,
		success: function(msg) {
			
			$("#ajax_favorite_loddder").hide();				
			$('#addons_modal_data').html(msg);			
			$("#addons-modal").modal('show');
			
		}
	});
	
});

$(document).on('click', '[id^="addon_update_data-"]', function() {
 
	 	
	var vehicle_id =$(this).attr('data-menu');
 	var trans_id = $(this).attr('data-rest') ;
 	var rate_id = $(this).attr('data-item') ;
 	var addon_id = $(this).attr('data-addon') ;
	 $("#ajax_favorite_loddder").show();	
	 
	var frm_val = "trans_id="+trans_id+"&vehicle_id="+vehicle_id+"&rate_id="+rate_id+"&addon_id="+addon_id;
	$.ajax({
	type: "post",
	url: "{{url('/admin/vehicle_addons_form')}}",
	data: frm_val,
		success: function(msg) {
			
			$("#ajax_favorite_loddder").hide();				
			$('#addons_modal_data').html(msg);			
			$("#addons-modal").modal('show');
			
		}
	});
	

});




  
  /* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */

/* MENU TAB END */

/* SERVICE TAB START */
$(document).on('click', '#service_btn', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
		var frm_val = "transport_id="+transport_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_service_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#Servicearea').html(msg);
			}
		});
 
});


$(document).on('click', '[id^="update_service-"]', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
	 	var service_id = $(this).attr('data-service');	
		var frm_val = "transport_id="+transport_id+"&service_id="+service_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_service_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#Servicearea').html(msg);
			}
		});
 
});


/* SERVICE TAB EBD */


/* PROMOSTIONS TAE START */
$(document).on('click', '#promo_btn', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
		var frm_val = "transport_id="+transport_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_promo_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#Promotions').html(msg);
			}
		});
 
});


$(document).on('click', '[id^="update_promo-"]', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
	 	var promo_id = $(this).attr('data-promo');	
		var frm_val = "transport_id="+transport_id+"&promo_id="+promo_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_promo_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#Promotions').html(msg);
			}
		});
 
});



/* PROMOTIONS TAB END*/




/* BANKDETAIL TAE START */

$(document).on('click', '#bank_btn', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
		var frm_val = "transport_id="+transport_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_bank_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#BankDetail').html(msg);
			}
		});
 
});


$(document).on('click', '[id^="update_bank-"]', function(){ 
  
		
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
	 	var bank_id = $(this).attr('data-bank');	
		var frm_val = "transport_id="+transport_id+"&bank_id="+bank_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/transport_bank_form')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#BankDetail').html(msg);
			}
		});
 
});

/* BANKDETAIL TAE END */
  
 $(document).on('click', '[id^="show_popular_item"]', function(){  
  
 	
	 $("#ajax_favorite_loddder").show();
	 
	 	var transport_id = $(this).attr('data-rest');	
		var frm_val = "transport_id="+transport_id;
		$.ajax({
		type: "post",
		url: "{{url('/admin/show_popular')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				$('#ajax_div').html(msg);
			}
		});
});  
  
  
  
  
  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
  </script>
@stop