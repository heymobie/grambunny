@extends('layouts.admin')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vendor Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Vendor Form</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Vendor Form</h3>
				</div><!-- /.box-header -->
				<p style="float: left;text-align: center;width: 100%;">
					@if(Session::has('message'))
					{{Session::get('message')}}
				@endif </p>
				<!-- form start -->
				<form  role="form" method="POST" id="user_frm" action="{{ url('/admin/vendor_action') }}">
					<input type="hidden" name="vendor_id" value="{{$id}} " />
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">First Name</label>
							<input type="text" class="form-control" name="name" id="name" value="@if($id>0){{$user_detail[0]->name}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Last Name</label>
							<input type="text" class="form-control" name="last_name" id="last_name" value="@if($id>0){{$user_detail[0]->last_name}}@endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Contact Number</label>
							<input type="text" class="form-control" name="mob_no" id="mob_no" value="@if($id>0){{$user_detail[0]->mob_no}}@endif" required="required" number="number" maxlength="10">
							<div id="error_msg" style="color:#FF0000; display:none;"></div>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" name="email" id="email" required="required" value="@if($id>0){{$user_detail[0]->email}}@endif" @if($id>0)  readonly="readonly" @endif>
							<div id="email_msg" style="display:none; color:#FF0000">An account with this email is already registered</div>
						</div>
						@if($id==0)
						<div class="form-group">
							<label for="exampleInputEmail1">Password</label>
							<input type="password" class="form-control" name="password" id="password" required="required">
						</div>
						@endif
						<div class="form-group">
							<label for="exampleInputEmail1">Address Line1</label>
							<input type="text" class="form-control" name="address" id="autocomplete" value="@if($id>0){{$user_detail[0]->address}}@endif" required="required" onFocus="geolocate()">
							<input type="hidden"  id="street_number" name="street_number"/>
							<input type="hidden" class="field" id="route" name="route" />
							<input type="hidden"  id="country" name="country" />
							<input type="hidden" class="form-control" name="address1" id="address1" value="" />
							<input type="hidden" class="form-control" name="state" id="state" value="" />
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">City</label>
							<input type="text" class="form-control" name="suburb" id="locality" value="@if($id>0){{$user_detail[0]->suburb}}@endif" >
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">State</label>
							<input type="text" class="form-control" name="state" id="administrative_area_level_1" value="@if($id>0){{$user_detail[0]->state}}@endif" />
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Post code</label>
							<input type="text" class="form-control" name="zipcode" id="postal_code" value="@if($id>0){{$user_detail[0]->zipcode}}@endif"  number="number">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="vendor_status" id="vendor_status" class="form-control">
								<option value="1" @if(($id>0)&& ($user_detail[0]->vendor_status=='1')) selected="selected" @endif>Active</option>
								<option value="0" @if(($id>0)&& ($user_detail[0]->vendor_status=='0')) selected="selected" @endif>Inactive</option>
							</select>
						</div>
					</div> <!-- /.box-body -->
					<div class="box-footer">
						@if($id==0)
						<input type="button" class="btn btn-primary" onclick="check_email();" value="Submit"  />
						@else
						<input type="button" class="btn btn-primary" value="Submit"  onclick="check_number();"/>
						@endif
						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js_bottom')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ui-widget-content {
	max-height: 221px;
	overflow-y: scroll;
}
.ui-menu .ui-menu-item {
	padding: 5px;
}
.ui-menu-item:nth-child(2n) {
	background-color: #f1f1f1;
}
</style>
<style>
#ajax_favorite_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_favorite_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
	/*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	z-index: 998 !important;
}
</style>
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	function check_email()
	{
		//alert('asdfsf');
		$('#error_msg').hide();
		var form = $("#user_frm");
		form.validate({
			rules: {
				name:{
					required:true,
				},
				last_name: {
					required: true,
				},
				mob_no: {
					required: true,
				},
				email: {
					required: true,
					email:true
				},
				password: {
					required: true,
				},
				address: {
					required: true,
				},
				state: {
					required: true,
				},
				suburb: {
					required: true,
				},
				zipcode: {
					required: true,
				}
			},
			messages: {
				name:{
					required:'Please enter name.',
				},
				last_name: {
					required:'Please enter last name.',
				},
				mob_no: {
					required:'Please enter mobile number.',
				},
				email: {
					required:'Please enter email address.',
					email:'Please enter an valid email address.',
				},
				password: {
					required:'Please enter password.',
				},
				address: {
					required:'Please enter postal address.',
				},
				state: {
					required:'Please enter state.',
				},
				suburb: {
					required:'Please enter city name.',
				},
				zipcode: {
					required:'Please enter zipcoad.',
				}
			}
		});
		var valid =	form.valid();
		if(valid){
			$("#ajax_parner_loddder").show();
				/*var regExp = /^0[0-9].*$/;
				if(regExp.test($("#mob_no").val()))
					{*/
					/*$(form).submit();
					return true;*/
					var frm_val = $('#user_frm').serialize();
					$.ajax({
						type: "POST",
						url: "./check_vendor_duplicateemail",
						data: frm_val,
						success: function(msg) {
							if(msg=='1')
							{
								$("#ajax_parner_loddder").hide();
								$('#email_msg').show();
								return false;
							}
							else if(msg=='2')
							{
								$(form).submit();
								return true;
							}
						}
					});
				/*}
				else
				{
					//alert('Contact number start with 0.');
					$('#error_msg').html('Contact number start with 0.');
					$('#error_msg').show();
					return false;
				}*/
			}
			else
			{
				return false;
			}
		}
		function check_number()
		{
			$('#error_msg').hide();
			var form = $("#user_frm");
			form.validate({
				rules: {
					name:{
						required:true,
					},
					last_name: {
						required: true,
					},
					mob_no: {
						required: true,
					},
					email: {
						required: true,
						email:true
					},
					password: {
						required: true,
					},
					address: {
						required: true,
					},
					state: {
						required: true,
					},
					suburb: {
						required: true,
					},
					zipcode: {
						required: true,
					}
				},
				messages: {
					name:{
						required:'Please enter name.',
					},
					last_name: {
						required:'Please enter last name.',
					},
					mob_no: {
						required:'Please enter mobile number.',
					},
					email: {
						required:'Please enter email address.',
						email:'Please enter an valid email address.',
					},
					password: {
						required:'Please enter password.',
					},
					address: {
						required:'Please enter postal address.',
					},
					state: {
						required:'Please enter state.',
					},
					suburb: {
						required:'Please enter city name.',
					},
					zipcode: {
						required:'Please enter zipcoad.',
					}
				}
			});
			var valid =	form.valid();
			if(valid){
				/*var regExp = /^0[0-9].*$/;
				if(regExp.test($("#mob_no").val()))
					{*/
						$(form).submit();
						return true;
				/*}
				else
				{
					//alert('Contact number start with 0.');
					$('#error_msg').html('Contact number start with 0.');
					$('#error_msg').show();
					return false;
				}*/
			}
			else
			{
				return false;
			}
		}
	</script>
	<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };
      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;
			//alert(val);
		}
	}
}
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
  @stop