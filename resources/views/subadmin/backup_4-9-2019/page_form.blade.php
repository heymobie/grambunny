@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Page Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Page Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Page Form</h3>
                                </div><!-- /.box-header -->
								
					 @if(Session::has('error'))
					 <div class="alert alert-danger alert-dismissable">				
								{{Session::get('error')}}
								</div>
							@endif 
                                <!-- form start -->
								
								<form  role="form" method="POST" id="cuisine_frm" action="{{ url('/admin/page_action') }}">    
								<input type="hidden" name="page_id" value="{{$id}} " /> 
								<input type="hidden" name="page_old_title" value="{{$page_detail[0]->page_title}}" /> 
								<input type="hidden" name="page_url" value="{{$page_detail[0]->page_url}}" />
								
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                           <input type="text" class="form-control" id="page_title" name="page_title"  value="@if($id>0){{$page_detail[0]->page_title}}@endif" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Content</label>
											<textarea name="page_content" id="page_content" class="form-control"> @if($id>0){{$page_detail[0]->page_content}}@endif</textarea>
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
												
										<select name="page_status" id="page_status"  class="form-control">
										 <option value="1" @if(($id>0) && ($page_detail[0]->page_status==1)) selected="selected"@endif>Active </option>
										 <option value="0" @if(($id>0) && ($page_detail[0]->page_status==0)) selected="selected"@endif>Inactive </option>
										</select>		
												
                                        </div>
										
										
										
										
                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		
										<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />
									
									 
									 	<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		     
        <!-- CK Editor -->
        <script src="{{ url('/') }}/design/admin/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<?php /*?><script type="text/javascript" src="{{ url('/') }}/design/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
		selector: "#page_content",
		theme: "modern",
		height : 300,
		//file_browser_callback : 'myFileBrowser',
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor moxiemanager",
			"insertdatetime media table contextmenu paste jbimages"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
		toolbar2: "print preview  | forecolor backcolor emoticons",
		image_advtab: true,
		relative_urls: false,

	});
	
	
</script><?php */?>

<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('page_content');
	CKEDITOR.config.allowedContent = true; 
	CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);
	//bootstrap WYSIHTML5 - text editor
	$(".textarea").wysihtml5();
});


function check_email()
{

	var form = $("#cuisine_frm");
	form.validate();
	var valid =	form.valid();
	if(valid){			
					$(form).submit();
					return true;					
	}
	else
	{
		return false;
	}		
}
		</script>
@stop