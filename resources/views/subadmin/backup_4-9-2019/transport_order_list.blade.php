@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Transport Order Management
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Order Listing</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Order Management</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
										</div>
									
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
									
				<form name="search_frm" id="search_frm" method="post" >
				
								{!! csrf_field() !!}
				<input type="hidden" name="user_id" id="user_id" value="{{$userid}}" />
                                <div>
								<table cellpadding="5" cellspacing="5" width="100%" style="border:1px solid #999999" class="table">
								<tr>
									<td width="50%">
									<table cellpadding="5" cellspacing="5" width="100%">
										
									<tr>
									<td>Order Status: </td> 
									<td>
										<select name="order_status" id="order_status"class="form-control">
											<option value="0">All </option>
											<option value="1">All Pending Action</option>
											<option value="3">All Sent to Transport</option>
											<option value="4">All Confirmed by Transport</option>
											<option value="5">All Completed</option>
											<option value="6">Rejected</option>
											<option value="2">Cancelled</option>
										</select>
									</td> 
									</tr>
									</table>
									</td>
									<td width="50%">
									<table cellpadding="5" cellspacing="5" width="100%">
										
									<tr>
									<td>From Date: </td>
									<td>
										<input type="text" name="fromdate" id="fromdate" value=""  class="form-control datepicker"/>
									</td> 
									</tr>
									<tr>
									<td>To Date: </td>
									<td>
										<input type="text" name="todate" id="todate" value="" class="form-control datepicker"/>
									</td> 
									</tr>
									
									</table>
									
									
											<div id="error_msg" style="color:#FF0000; display:none;"></div>
										
									</td>
									</tr>
									
									<tr>
										<td colspan="2">
									
									<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" />
									
									</td>
									</tr>
								</table>
                                </div>
								</form>
								
									
								
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
							<div>
							<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">
										    <div class="box">
												<div class="box-body table-responsive">
													<div style="float:right; margin:5px;"> 
												<input class="btn btn-primary" type="button" id="generate_file"  value="Export">
												    </div>
													<div id="restaurant_list">							
														 <table id="example2" class="table table-bordered table-hover">
															<thead>
																<tr>
																	<th>SrNo</th>
																	<th>Order_no</th>
																	<th>OrderReq_date</th>
																	<th>Transport</th>
																	<th>Vehicle</th>
																	<th>Status</th>
																	<th>Cust_Submit_date</th>
																	<!--<th>Last_Staus_Date</th>-->
																	<th>Action</th>
																</tr>
															</thead>
															<tbody>										
															<?php $i=1; ?>
															@if(!empty($order_detail)) 
															 @foreach ($order_detail as $list)
																<tr>
																	<td>{{ $i }}</td>
																	<td>{{ $list->order_id}}</td>
																	<td>{{ $list->order_create}}</td>
																	<td><a href="{{url('admin/transport_view/')}}/{{$list->transport_id}}">{{ $list->transport_name}}</a></td>
																	<td>{{ $list->vehicle_rego}}</td>
																	<td>
																	
									 @if($list->order_status=='1') Submit From Website @endif
									 @if($list->order_status=='2') Cancelled @endif
									 @if($list->order_status=='3') Sent to Partner @endif
									 @if($list->order_status=='4') Partner Confirmed @endif
									 @if($list->order_status=='5') Partner Completed @endif
									 @if($list->order_status=='6') Reject @endif
									 @if($list->order_status=='7') Review Completed @endif
																	
																	</td>
																	<td>{{ $list->order_create }}</td>
														<!--<td><?php echo date('Y-m-d',strtotime($list->created_at))?></td>-->
																	 <td> 
																	  <a title="Update" href="{{url('admin/transport_order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
																	 
																	 
																	 <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>
																	 
																		 
																	</td>
																</tr> 
<div class="modal fade" id="order-{{$list->order_id}}" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title choice">Order Details</h4>
      </div>
      <div class="modal-body popup-ctn">
	   
		<h5><strong>Transport   summary</strong></h5>
		<div>
			<div>Name: {{$list->transport_name}}</div>
			<div>Address: {{$list->transport_address}} {{$list->transport_suburb}} {{$list->transport_state}} {{$list->transport_pcode}}</div>
			<div>Contact No: {{$list->transport_contact}}</div>
			
			<div>Vehicle Name : {{$list->vehicle_rego}}</div>
		</div>
		
		
		<h5><strong>User summary</strong></h5>
		
		<div>
			<div>Name: {{$list->order_fname}} {{$list->order_lname}}</div>
			<div>Address: {{$list->order_address}} {{$list->order_city}} {{$list->order_pcode}}</div>
			<div>Contact No: {{$list->order_tel}}</div>
			<div>Email: {{$list->order_email}}</div>
		</div>	
         <h5><strong>Your order summary</strong></h5>
		<div>	
			<?php $cart_price = 0;?>
		@if(isset($list->order_carditem) && (count($list->order_carditem)))
		 <?php	$order_carditem = json_decode($list->order_carditem, true);?>	
		 
		<div>
					 
						
						 @foreach($order_carditem as $item)
						<?php  $cart_price = $cart_price+$item['price'];?>
						 <div>
						  <strong>{{$item['qty']}}  x</strong> {{$item['name']}} 
						  
						  <strong class="pull-right">${{$item['price']}}</strong>
						</div>
						
						 @if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
							@foreach($item['options']['addon_data'] as $addon)
								
							  <?php $cart_price = $cart_price+$addon['price'];?>
							 <div>
							  {{$addon['name']}}
							  <strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong>
							</div>
							 @endforeach
						 @endif 
							
						 @endforeach
						 
							
							<div>
							 	TotalAmount:
							  <strong class="pull-right">${{$cart_price}}</strong>
							</div>
					</div>
		@endif
		</div>
      </div>
    </div>
  </div>
</div>										
															<?php $i++; ?>
															@endforeach	   
															@endif                                     
															</tbody>
														</table>	
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>					
						  </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
		 dateFormat: 'yy-mm-dd',
		 autoclose: true,
		});
  });	
  
  
  
$(document).on('click', '#search_btn', function(){ 
	
 $('#error_msg').hide(); 	  
	 
var frmdate = $('#fromdate').val(); 
var todate = $('#todate').val(); 
	
	if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
	{//compare end <=, not >=
		//your code here
		$('#error_msg').html('From date will be big from to date!');
		$('#error_msg').show();
		//alert("From date will be big from to date!");
	}
	else
	{	
		
	 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/transport_order_search_list')}}",
		data: frm_val,
		success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			//alert(msg)
				$('#restaurant_list').html(msg);
			}
		});
	}		
}); 		
			

  
$(document).on('click', '#generate_file', function(){ 		



 $('#search_frm').attr('action', "{{url('/admin/transport_order_report')}}");
 
 $('#search_frm').attr('target', '_blank').submit();

		
}); 	

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>
@stop
