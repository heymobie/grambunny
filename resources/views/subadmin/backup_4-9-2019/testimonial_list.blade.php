@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Testimonial List
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Testimonial List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Testimonial Listing</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<a href="{{url('admin/testimonial-form')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New </a>
									</div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
								
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
							
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; ?>
										 @foreach ($testimonial_list as $list)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $list->name}}</td>
                                                <td>
												
												@if(!empty($list->image))
											
											<img src="{{ url('/') }}/uploads/testimonial/{{ $list->image }}" width="50px;" height="50px;">
											@endif	
												
												</td>
                                                <td>
												@if($list->status==1) 
												<span class="label label-success">Active</span>
												 @else 
												
												<span class="label label-danger">Inactive</span>@endif
												</td>
                                                <td>
												 <a href="{{url('admin/testimonial-form/')}}/{{ $list->id }}">
												 <i class="fa fa-edit"></i></a>  
												 
												  <a title="Delete User" href="{{url('admin/testimonial_delete')}}/{{ $list->id }}"onclick="return delete_wal()"><i class="fa fa-trash-o"></i></a>
												 
												  <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#update_review-{{$list->id}}"><i class="fa fa-eye"></i></a>  
												
<div class="modal fade" id="update_review-{{$list->id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="addons_modal_data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Testimonial</h4>
			</div>
			
								{!! csrf_field() !!}
				<div class="box-body">
					<div class="popup_divcall">
						<label>Name:</label> {{ $list->name }}
					</div>
					<div class="popup_divcall">
						<label>&nbsp;</label> 
					</div>
					
					<div class="popup_divcall">
						<label>Image:</label>
							@if(!empty($list->image))											
							<img src="{{ url('/') }}/uploads/testimonial/{{ $list->image }}" width="50px;" height="50px;">
							@endif
					</div>
					
					<div class="popup_divcall">
						<label>&nbsp;</label> 
					</div>
					
					<div class="popup_divcall">
						<label>Content:</label> {{ $list->content}}
					</div>
					

				</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
												 
												</td>
                                            </tr> 										
									 <?php $i++; ?>
									@endforeach	                                      
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>                                             
												<th>&nbsp;</th>                                           
												<th>&nbsp;</th>                                          
												<th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop

@section('js_bottom')

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
@stop
