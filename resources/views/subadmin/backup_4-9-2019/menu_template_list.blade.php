@extends('layouts.admin')

@section("other_css")
<!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Template Management
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Menu Template Management</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"> Menu Template Management</h3>
						<div style="float:right; margin-right:10px; margin-top:10px;">
							<a href="{{url('admin/get_templatename')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New </a>
						</div>

					</div><!-- /.box-header -->
					<div class="box-body table-responsive">

						@if(Session::has('message'))

						<div class="alert alert-success alert-dismissable">
							<i class="fa fa-check"></i>
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
							{{Session::get('message')}}
						</div>
						@endif

						<table id="example2" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>SrNo</th>
									<th>Template Name</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($template_detail)
								{
									$i=0;
									foreach($template_detail as $template_data)
										{ $i++;

											$rest_count = 	 DB::table('template_menu')->where('template_id', '=' ,$template_data->template_id)->count();
											?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo $template_data->template_name;?></td>
												<td><?php if($template_data->template_status==1){?>
													<span class="label label-success">Active</span>
												<?php }else{?>
													<span class="label label-danger">Inactive</span>
												<?php }?>

											</td>
											<td>
												<a href="{{url('admin/get_templatename')}}/{{ $template_data->template_id }}"class="btn btn-primary" style="color:#FFFFFF"> Edit</a>
												<a href="{{url('admin/template_menu_list')}}/{{ $template_data->template_id }}" class="btn btn-primary" style="color:#FFFFFF"> Manage Menu</a>

												<!-- <a href="{{url('admin/template_menu_delete')}}/{{ $template_data->template_id }}" class="btn btn-primary" style="color:#FFFFFF" onclick="return delete_template('<?php echo  $rest_count ;?>')"> Delete sdsf</a> -->


												<a href="{{url('admin/delete_template_menus')}}/{{ $template_data->template_id }}" class="btn btn-primary" style="color:#FFFFFF" onclick="return delete_template_menus('<?php echo   $template_data->template_id;?>')"> Delete</a>

											</td>
										</tr>
										<?php
									}
								}
								?>

							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
								</tr>
							</tfoot>
						</table>




					</div><!-- /.box-body -->
				</div><!-- /.box -->
				<div>

				</div>
			</div>
		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>

#ajax_favorite_loddder {

	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_favorite_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}

.footer-wrapper {
	float: left;
	width: 100%;
	/*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {

	z-index: 998 !important;
}
</style>

<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<!-- page script -->
<script type="text/javascript">
	function check_frm()
	{


		$('#error_msg').hide();

		var form = $("#search_frm");
		form.validate();
		var valid =	form.valid();
		if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
		{
			$("#ajax_favorite_loddder").show();
			var frm_val = $('#search_frm').serialize();
			$.ajax({
				type: "POST",
				url: "{{url('/admin/vendor_search')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();

					$('#vender_search_list').html(msg);
				}
			});
		}
		else
		{
		//alert('Please insert any one value');

		$('#error_msg').html('Please insert any one value');
		$('#error_msg').show();
		return false;
	}
}

$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>
@stop
