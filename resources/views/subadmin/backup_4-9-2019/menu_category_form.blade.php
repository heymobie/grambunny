@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Restaurant Menu Item Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Restaurant Menu Item Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Restaurant Menu Item Form</h3>
                                </div><!-- /.box-header -->
								<p style="float: left;text-align: center;width: 100%;">				
					 @if(Session::has('message'))
								{{Session::get('message')}}
							@endif </p>
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_cate_frm" action="{{ url('/admin/category_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="menu_category_id" value="{{$id}}" />
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Restaurant Name</label>
											<select name="restaurant_id" id="restaurant_id" required="required"class="form-control">
												<option value="">Select Restaurant </option>
											  @foreach($rest_detail as $list) 
												  <option value="{{$list->rest_id}}"   @if(($id>0) && ($list->rest_id==$cate_detail[0]->rest_id)) selected="selected" @endif >{{$list->rest_name}}</option>
											  @endforeach											
											</select> 
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">MenuItem Name</label>
											<div id="rest_menu">
											<select name="menu_id" id="menu_id" required="required"class="form-control" >
												<option value="">Select Restaurant </option>
											@if(!empty($menu_detail))	
											  @foreach($menu_detail as $list) 
					  <option value="{{$list->menu_id}}"  @if(($id>0) && ($list->menu_id==$cate_detail[0]->menu_id)) selected="selected" @endif>{{$list->menu_name}}</option>
											  @endforeach				
											@endif  							
											</select> 
											</div>
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Menu Item name</label>
                                           <input type="text" class="form-control" name="menu_name" id="	menu_name" value="@if($id>0){{$cate_detail[0]->menu_category_name}}@endif" required="required">
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Item Price</label>
                                           <input type="text" class="form-control" name="menu_price" id="	menu_price" value="@if($id>0){{$cate_detail[0]->menu_category_price}}@endif" required="required" number="number">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="menu_desc" id="menu_desc" required="required">@if($id>0){{$cate_detail[0]->menu_category_desc}}@endif</textarea>																
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
										<input type="button" class="btn btn-primary" value="Submit" onclick="check_frm()" />
									<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script>
			
		 $(document).on('change', '#restaurant_id', function(){ 
		 	var selectedValue = $(this).val();
			if(selectedValue>0)
			{
			 var frm_val = $("#rest_cate_frm").serialize();
				$.ajax({
				type: "POST",
				url: "{{url('/admin/get_menulist') }}",
				data: frm_val,
					success: function(msg) {
						$('#rest_menu').html(msg);
					}
				});
			}
		 });
		 
		 
		function check_frm()
		{
		
		var form = $("#rest_cate_frm");
				form.validate();
			var valid =	form.valid();
			if(valid)
			{		
				$(form).submit();
				return true;	
			}
			else
			{
				return false;
			}		
		}
		
		</script>
@stop