@extends('layouts.admin')

@section('content')


<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<br /><br /><br />

<!--<div id="container_transport" style="min-width: 310px; height: 400px; margin: 0 auto"></div>-->
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection


@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
		<link rel="stylesheet" type="text/css" href="/css/result-light.css">
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>



<script type="text/javascript">

$(function () {


    $('#container').highcharts({
        title: {
            text: 'Restaurant Data',
            x: -20 //center
        },
        subtitle: {
            text: 'Time Period 7 days',
            x: -20
        },
        xAxis: {
     // categories: ['2016/20/2012', 'Feb', 'Mar', 'Apr', 'May', //'Jun',
            //  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			 //categories:['2017-02-03','2017-02-04','2017-02-05','2017-02-06','2017-02-07','2017-02-08','2017-02-09']
			categories:[<?php echo $string_date;?>]		
			
        },
        yAxis: {
            title: {
                //text: 'new participants'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
           // valueSuffix: ' participants'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
		{
			name:'Register User',
			data:[<?php echo $string_user;?>]
			
		},{
			name:'Order Submited',
			data:[<?php echo $string_order;?>]
			
		},{
			name:'order Completed',
			data:[<?php echo $string_completed;?>]
			
		},{
			name:'Total Payment',
			data:[<?php echo $string_payment;?>]
			
		},		]
    });
    $('#container_transport').highcharts({
        title: {
            text: 'Vehicle Data',
            x: -20 //center
        },
        subtitle: {
            text: 'Time Period 7 days',
            x: -20
        },
        xAxis: {
     // categories: ['2016/20/2012', 'Feb', 'Mar', 'Apr', 'May', //'Jun',
            //  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			 //categories:['2017-02-03','2017-02-04','2017-02-05','2017-02-06','2017-02-07','2017-02-08','2017-02-09']
			categories:[<?php echo $string_date;?>]		
			
        },
        yAxis: {
            title: {
                //text: 'new participants'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
           // valueSuffix: ' participants'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
		{
			name:'Vehicle Order Submited',
			data:[<?php echo $string_vehicle_order;?>]
			
		},{
			name:'Vehicle order Completed',
			data:[<?php echo $string_vehicle_completed;?>]
			
		},{
			name:'Vehicle Total Payment',
			data:[<?php echo $string_vehicle_payment;?>]
			
		},		]
    });
});

</script>

@stop