@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Payment View
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Payment View</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					
                        <div class="col-xs-12">
                            <div class="box" style="display:block;float:left;padding-bottom:10px;">
                                <div class="box-header">
                                    <h3 class="box-title">Payment View</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
										</div>
									
                                </div><!-- /.box-header -->
                                <div class="box-body  table-responsive">
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-5">
										Status: 
										
										
									 @if($order_detail[0]->pay_status=='1') Complete @endif
									 @if($order_detail[0]->pay_status=='2') ToRefund @endif
									 @if($order_detail[0]->pay_status=='3') ToPayout @endif
									 @if($order_detail[0]->pay_status=='4') Completed Refund @endif
									 @if($order_detail[0]->pay_status=='5') Completed Payout @endif
									 
									  
									  
									  </div>
									  <form name="order_update" id="order_update" method="post">
									  <div class="col-md-7">
									  Change Status to: 
					 <input type="hidden" name="order_id" id="order_id" value="{{$order_detail[0]->order_id}}" />
					 <input type="hidden" name="pay_id" id="pay_id" value="{{$order_detail[0]->pay_id}}" />
					 <input type="hidden" name="old_status" id="old_status" value="{{$order_detail[0]->pay_status}}" />
					
					<select name="pay_status" id="pay_status">
					
					<option value="1" @if($order_detail[0]->pay_status=='1') selected="selected"@endif>Complete</option>
					<option value="2" @if($order_detail[0]->pay_status=='2') selected="selected"@endif>ToRefund</option>
					<option value="3" @if($order_detail[0]->pay_status=='3') selected="selected"@endif>ToPayout</option>
					<option value="4" @if($order_detail[0]->pay_status=='4') selected="selected"@endif >Completed Refund</option>
					<option value="5" @if($order_detail[0]->pay_status=='5') selected="selected"@endif>Completed Payout</option>
						
						
				</select>
									
										<input type="button" value="Submit" id="update_status" 	@if(
					($order_detail[0]->pay_status == "4" ) ||($order_detail[0]->pay_status == "5" )
										   )
										disabled="disabled" @endif />
									   
									    <div class="col-md-7" id="capture_reference" style="display:none @if(($order_detail[0]->pay_status == "4" ) || ($order_detail[0]->pay_status == "5" )) display:block @endif">  
									Outgoing Transaction Reference
									 <input type="text" name="outgoing_transaction_reference" id="outgoing_transaction_reference"  required="required" value="{{$order_detail[0]->pay_outgoing_Ref}}"/>
									 </div>	
									 
									 <div id="error_msg" style="color:#FF0000; display:none;"></div>
									 </div>
										</form>
									</div>
									
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-6">
									  	@if(!empty($order_detail[0]->rest_logo))	
											<img src="{{ url('/') }}/uploads/reataurant/{{ $order_detail[0]->rest_logo }}" width="50px;" height="50px;">
										@else
											<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >
										@endif		
									  </div>
									  <div class="col-md-6">
									  
									   <strong style="padding-top:5px;text-align:center"> Order Details</strong>
									   </div>
									</div>
								
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-2"> Transport:</div> 
											<div class="col-md-9">{{$order_detail[0]->transport_name}}</div>
										<div class="col-md-2"> Address:</div>
											<div class="col-md-9">{{$order_detail[0]->transport_address.' '.$order_detail[0]->transport_address2}}
											{{$order_detail[0]->transport_suburb}}
											{{$order_detail[0]->transport_state}}
											{{$order_detail[0]->transport_pcode}}
											</div>
										<div class="col-md-2"> Contact No:</div> 
											<div class="col-md-9">{{$order_detail[0]->transport_contact}}</div>
										
										<div class="col-md-2"> Vehicle Name:</div> 
											<div class="col-md-9">{{$order_detail[0]->vehicle_rego}}</div>	
									</div>
								
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-6">&nbsp;</div> 
										<div class="col-md-6">
											Request Submeted Date: {{$order_create_date}}
										</div>	
										
										<div class="col-md-3">Order Number:</div> 
											<div class="col-md-9">{{$order_detail[0]->order_id}}</div>
										
										<div class="col-md-3">Booking Type:</div> 
											<div class="col-md-9">{{str_replace('_',' ',$order_detail[0]->pick_bookingtype)}}&nbsp;</div>
										
										<div class="col-md-3"> Pick-Up Contact Name:</div> 
										<div class="col-md-9">
											{{$order_detail[0]->pick_contact_name}}&nbsp;
										</div>
										
										<div class="col-md-3"> Pick-Up Contact No:</div> 
										<div class="col-md-9">{{$order_detail[0]->pick_contact_no}}&nbsp;
										</div>
										<div class="col-md-3"> Pick-Up Address:</div> 
										<div class="col-md-9">&nbsp;
										
										<?php if(!empty($order_detail[0]->pick_address1)){?>	
											{{$order_detail[0]->pick_address1}}<br />
											{{$order_detail[0]->pick_address2}}<br />
											{{$order_detail[0]->pick_suburb}}, 
											{{$order_detail[0]->pick_pcode}}, <br />
											{{$order_detail[0]->pick_district}}, 
											{{$order_detail[0]->pick_state}}
											<?php }?>
										</div>
										<div class="col-md-3"> Pick-Up Date & Time:</div> 
											<div class="col-md-9">
												{{$order_detail[0]->order_ondate}}&nbsp;
												{{$order_detail[0]->order_ontime}}
											</div>
										<div class="col-md-3"> To-suburb details:</div>
										<div class="col-md-9">&nbsp;
												<?php if(!empty($order_detail[0]->pick_to_suburb)){?>	
											{{$order_detail[0]->pick_to_suburb}}, 
											{{$order_detail[0]->pick_to_pcode}}, 
											{{$order_detail[0]->pick_to_district}}, 
											{{$order_detail[0]->pick_to_state}}
										<?php }?>
										</div>
											
										<!--<div class="col-md-3"> Amount:</div>
											 <div class="col-md-9">&nbsp;</div>-->
									</div>
									
								
									
									<div class="col-md-12" style="border:1px solid #333333;">
											 
										<div class="col-md-3"> Customer Name: </div>
											<div class="col-md-9">
										{{$order_detail[0]->order_fname}} 
											{{$order_detail[0]->order_lname}} 
										</div> 
											
										<div class="col-md-3"> Mobile No:</div>
											 <div class="col-md-9">{{$order_detail[0]->order_tel}} </div>
										<!--<div class="col-md-3"> Landline:</div>
											<div class="col-md-9">&nbsp;</div>-->
										<div class="col-md-3"> Customer Comment:</div>
											 <div class="col-md-9">{{$order_detail[0]->order_instruction}}&nbsp; </div>
											 
											 
										
										<div class="col-md-3">Payment Method</div>
											 <div class="col-md-9"> Paypal</div>
											 
										
									<div class="col-md-3"> Payment Transection Id</div>
									 <div class="col-md-9" >{{$order_detail[0]->pay_tx}} </div>
									</div>
									
									<div class="col-md-12" style="border:1px solid #333333;">
										<table width="100%" border="1" cellpadding="2" cellspacing="2">
											<tr>
												<td width="25%" align="center"><b>Item Details</b></td>
												<td width="15%" align="center"><b>Unit Size</b></td>
												<td width="10%" align="center"><b>Days</b></td>
												<td width="10%" align="center"><b>Unit Price</b></td>
												<td width="10%" align="center"><b>Total</b></td>
											</tr>
											<?php
											 $cart_price = 0;
										$order_carditem = json_decode($order_detail[0]->order_carditem, true);?>	
											 @foreach($order_carditem as $item)
											<?php  $cart_price = $cart_price+$item['price'];
												$addon_price = '';
												$addon_name = '';											
											?>
											 @if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
							@foreach($item['options']['addon_data'] as $addon)
								
							  <?php $cart_price = $cart_price+$addon['price'];
							  $addon_price = $addon['price'];
							  $addon_name = $addon['name'];
							  
							  ?>
							
							 @endforeach
						 @endif
						 
											<tr>
												<td width="25%" align="left">{{str_replace('_',' ',$item['name'])}} 
												@if(!empty($addon_name))
												 <div style="padding:5px;">{{$addon_name}}</div>
												@endif
												</td>
												<td width="15%" align="center">{{$item['options']['option_name']}}</td>
												<td width="10%" align="center">{{$item['qty']}}</td>
												<td width="10%" align="right">${{number_format($item['price']/$item['qty'],2)}}
												@if(!empty($addon_price) && !empty($addon_name)&&($addon_price>0))
												<div style="padding:5px;">${{number_format(($addon_price/$item['qty']),2)}}</div>												
												@endif
												</td>
												<td width="10%" align="right">
												${{number_format($item['price'],2)}}
												 
												@if(!empty($addon_price) && !empty($addon_name)&&($addon_price>0))
												 <div style="padding:5px;">${{number_format($addon_price,2)}}</div>										
												@endif
												
												</td>
											</tr>
											@endforeach
											<tr>
												<td width="25%" align="right"><b>Sub Total</b></td>
												<td colspan="4" align="right">${{number_format($cart_price,2)}}</td>
											</tr>
											<tr>
												<td width="25%" align="right"><b>Delivery Fee</b></td>
												<td colspan="4" align="right">${{number_format($order_detail[0]->order_deliveryfee,2)}}</td>
											</tr>
											
												
											@if($order_detail[0]->total_night>0)
											<tr>
											  <td width="25%" align="right"> <b>Driver Overnight Allowance</b>
											   </td>
												<td colspan="4" align="right">${{number_format($order_detail[0]->	total_allownce,2)}}
													</td>
											 
											</tr>
											@endif
											
											
											
											@if($order_detail[0]->order_promo_cal>0)
											<tr>
											  <td width="25%" align="right"> 
											   Discount
											  </td>
												<td colspan="4" align="right">-${{number_format($order_detail[0]->	order_promo_cal,2)}}
													</td>
											 
											</tr>
											@endif
											
											
											


											<tr>
												<td width="25%" align="right"><b>Total Charge</b></td>
												<td colspan="4" align="right">${{number_format($order_detail[0]->order_total_amt,2)}}</td>
											</tr>
										</table>
									</div>
									<div style="float:right; margin:5px;"> 
										<input class="btn btn-primary" type="button" value="Go Back"  onClick="history.go(-1);">
									</div>

                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
							<div>				
						  </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#example1").dataTable();
		$('#example2').dataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
			
$(document).on('change', '#pay_status', function(){ 
$('#error_msg').hide(); 

	var select_val = $("#pay_status option:selected" ).val(); 
 
	if(select_val=='4' || select_val =='5')
	{
		$('#capture_reference').show();
	}
	else
	{
		$('#capture_reference').hide();
	}
 
 
});

$(document).on('click', '#update_status', function(){ 

	$('#error_msg').hide(); 
 
 var order_id = $('#order_id').val();	
 var old_status = $('#old_status').val();	
 var new_status = $("#pay_status option:selected" ).val();
 /*alert(order_id);
		alert(old_status);
		alert(new_status);*/
		
  if(new_status=='1')
  {
  	//alert('Please change status');
	$('#error_msg').html('Please change status.');
	$('#error_msg').show();
	return false; 
  }	
  else if((old_status=='2') && (new_status!='4'))
  {
  	//alert('After ToRefund only select Completed Refund');
	$('#error_msg').html('After ToRefund only select Completed Refund.');
	$('#error_msg').show();
	return false; 
  }
  else if((old_status=='3') && (new_status!='5'))
  {
  	//alert('After ToPayout only select Completed Payout');
	$('#error_msg').html('After ToPayout only select Completed Payout.');
	$('#error_msg').show();
	return false; 
  }
  else if(new_status>1)
  {
  
  
  var form = $("#order_update");
	form.validate();
	var valid =	form.valid();
	
		if(valid)
		{
				
			$("#ajax_favorite_loddder").show();
			 
				//var rest_id = $(this).attr('data-rest');	
				//var promo_id = $(this).attr('data-promo');	
				var frm_val = $('#order_update').serialize();	
				$.ajax({
				type: "post",
				url : "{{url('/admin/transport_payment_status_update')}}",
				data: frm_val,
				success: function(msg) {
						
						$("#ajax_favorite_loddder").hide();	
						//$('#Promotions').html(msg);
						//alert(msg);
						location.reload();
					}
				});
		}
		
	
		
  }		
 
});

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>
@stop
