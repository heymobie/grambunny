@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Page List
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Page List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Page Listing</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/cuisine-form')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New Cuisine</a>-->
									</div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
								
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
							
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>
                                                <th>Title</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; ?>
										 @foreach ($page_list as $list)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $list->page_title}}</td>
                                                <td>
												@if($list->page_status==1) 
												<span class="label label-success">Active</span>
												 @else 
												
												<span class="label label-danger">Inactive</span>@endif
												</td>
                                                <td>
												 <a href="{{url('admin/page-form/')}}/{{ $list->page_id }}">
												 <i class="fa fa-edit"></i></a>  
												</td>
                                            </tr> 										
									 <?php $i++; ?>
									@endforeach	                                      
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>                                                <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop

@section('js_bottom')

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
@stop
