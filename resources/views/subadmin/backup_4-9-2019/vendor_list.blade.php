@extends('layouts.admin')
@section("other_css")
<!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vendor Management
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Vendor Management</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Vendor Management</h3>
					</div><!-- /.box-header -->
					<div class="box-body table-responsive">
						@if(Session::has('message'))
						<div class="alert alert-success alert-dismissable">
							<i class="fa fa-check"></i>
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
							{{Session::get('message')}}
						</div>
						@endif
						<form name="search_frm" id="search_frm" method="post" action="administrator/property-list" onsubmit="return check_search();">
							{!! csrf_field() !!}
							<div >
								<table cellpadding="5" cellspacing="5" width="100%" style="border:0px solid #999999" class="table">
									<tr>
									<!--<td width="50%">
										<table cellpadding="5" cellspacing="5" width="100%">
										<tr>
										<td>Vendor Cont No: </td>
										<td ><input type="text" name="vendor_cont" id="vendor_cont" class="form-control" value="" />
										</td>
										</tr>
										<tr>
										<td>Vendor Email</td>
										<td ><input type="email" name="vendor_email" id="vendor_email" class="form-control" value=""  />
											</td>
										</tr>
										<tr>
										<td>&nbsp;</td>
										<td>
										   <div id="error_msg" style="color:#FF0000; display:none;"></div>
										<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" onclick="check_frm()" /></td>
										</tr>
										</table>
									</td>-->
									<td width="100%">
										<div style="float:right; margin-right:10px; margin-top:10px;">
											<a href="{{url('admin/vendor-form')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New Vendor</a>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
			<div>
				<section>
					<div class="row">
						<div class="my-panel-data">
							<div class="col-xs-12">
								<div class="box">
									<div class="box-body table-responsive">
										<div id="vender_search_list">
											<table id="example2" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>SrNo</th>
														<th>Name</th>
														<th>Email</th>
														<th>Contact No</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=1; ?>
													@foreach ($vendor_list as $list)
													<?php
													$rest_count = 	 DB::table('restaurant')->where('vendor_id', '=' ,$list->vendor_id)->count();
													?>
													<tr>
														<td>{{ $i }}</td>
														<td>{{ $list->name}}</td>
														<td>{{ $list->email }}</td>
														<td>{{ $list->mob_no }}</td>
														<td>@if($list->vendor_status==1)
															<span class="label label-success">Active</span>
															@else
															<span class="label label-danger">Inactive</span>@endif</td>
															<td>
																<a href="{{url('admin/vendor-form/')}}/{{ $list->vendor_id }}">
																	<i class="fa fa-edit"></i></a>
																	<a title="View" href="{{url('admin/vendor_profile/')}}/{{ $list->vendor_id }}"><i class="fa fa-eye"></i></a>
																	<a title="Delete Vendor" href="{{url('admin/vendor_delete')}}/{{ $list->vendor_id }}" onclick="return delete_parent('  <?php echo  $rest_count ;?>')">  <i class="fa fa-trash-o"></i></a>
																</td>
															</tr>
															<?php $i++; ?>
															@endforeach
														</tbody>
														<tfoot>
															<tr>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section><!-- /.content -->
	</aside><!-- /.right-side -->
	@stop
	@section('js_bottom')
	<style>
	#ajax_favorite_loddder {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background:rgba(27, 26, 26, 0.48);
		z-index: 1001;
	}
	#ajax_favorite_loddder img {
		top: 50%;
		left: 46.5%;
		position: absolute;
	}
	.footer-wrapper {
		float: left;
		width: 100%;
		/*display: none;*/
	}
	#addons-modal.modal {
		z-index: 999;
	}
	.modal-backdrop {
		z-index: 998 !important;
	}
</style>
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!-- page script -->
<script type="text/javascript">
	function check_frm()
	{
		$('#error_msg').hide();
		var form = $("#search_frm");
		form.validate();
		var valid =	form.valid();
		if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
		{
			$("#ajax_favorite_loddder").show();
			var frm_val = $('#search_frm').serialize();
			$.ajax({
				type: "POST",
				url: "{{url('/admin/vendor_search')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#vender_search_list').html(msg);
				}
			});
		}
		else
		{
		//alert('Please insert any one value');
		$('#error_msg').html('Please insert any one value');
		$('#error_msg').show();
		return false;
	}
}
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>
@stop