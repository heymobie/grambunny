@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			User Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">User Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">User Form</h3>
				</div><!-- /.box-header -->
				<p style="float: left;text-align: center;width: 100%;">
					@if(Session::has('message'))
					{{Session::get('message')}}
				@endif </p>
				<!-- form start -->
				<?php
				// echo "<pre>";
				// print_r($user_detail);
				// echo "</pre>";

				?>


				<form  role="form" method="POST" id="user_frm" action="{{ url('/admin/user_action') }}">
					<input type="hidden" name="user_id" value="{{$id}} " />

					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Name</label>
							<input type="text" class="form-control" name="name" id="name" value="@if($id>0) {{$user_detail[0]->name}} @endif" required="required">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Last Name</label>
							<input type="text" class="form-control" name="lname" id="lname" value="@if($id>0) {{$user_detail[0]->lname}} @endif" required="required">
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Mobile Number form</label>
							<select name="user_mob_type" id="user_mob_type"  class="form-control" @if(($id>0) && (!empty($user_detail[0]->carrier_id))) disabled="true" @endif>
								<option value="1" <?php if(($id>0)  && ($user_detail[0]->carrier_id=='1')) {echo 'selected="selected"';}?> >Alltel</option>
								<option value="2" <?php if(($id>0) && ($user_detail[0]->carrier_id=='2')) {echo 'selected="selected"';}?>>AT&T</option>
								<option value="3" <?php if(($id>0) && ($user_detail[0]->carrier_id=='3')) {echo 'selected="selected"';}?>>Boost Mobile</option>
								<option value="4" <?php if(($id>0) && ($user_detail[0]->carrier_id=='4')) {echo 'selected="selected"';}?>>Sprint</option>
								<option value="5" <?php if(($id>0) && ($user_detail[0]->carrier_id=='5')) {echo 'selected="selected"';}?>>T-Mobile</option>
								<option value="6" <?php if(($id>0) && ($user_detail[0]->carrier_id=='6')) {echo 'selected="selected"';}?>>U.S. Cellular</option>
								<option value="7" <?php if(($id>0) && ($user_detail[0]->carrier_id=='7')) {echo 'selected="selected"';}?>>Verizon</option>
								<option value="8" <?php if(($id>0) && ($user_detail[0]->carrier_id=='8')) {echo 'selected="selected"';}?>>Virgin Mobile</option>
								<option value="9" <?php if(($id>0) && ($user_detail[0]->carrier_id=='9')) {echo 'selected="selected"';}?>>Republic Wireless</option>
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Mobile Number</label>
							<input type="text" class="form-control form-white"  name="user_mob" id="user_mob" required number="number" maxlength="10" value="<?php if($id>0) { echo trim($user_detail[0]->user_mob);}?>"  @if(($id>0) && (!empty(trim($user_detail[0]->user_mob))) ) readonly="readonly" @endif >
							<div id="number_msg" class="eror_msg" style="display:none; color:#FF0000"><b>An account with this phone number is already registered.</b></div>

						</div>


						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" name="email" id="email" required="required" value="@if($id>0) {{$user_detail[0]->email}} @endif" @if($id>0)  readonly="readonly" @endif>
							<div id="email_msg" style="display:none; color:#FF0000"><b>An account with this email is already registered.</b></div>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">User status</label>
							<select name="user_status" id="user_status"  class="form-control">
								<option value="1" <?php if(($id>0) && ($user_detail[0]->user_status=='1')) {echo 'selected="selected"';}?>>Active</option>
								<option value="0" <?php if(($id>0) && ($user_detail[0]->user_status=='0')) {echo 'selected="selected"';}?>>Deactive</option>
							</select>
						</div>



						@if($id==0)
						<div class="form-group">
							<label for="exampleInputEmail1">Password</label>
							<input type="password" class="form-control" name="password" id="password" required="required">
						</div>
						@endif


						<div class="form-group">
							<label>Your full address</label>

							<input type="text" class="form-control" name="user_address" id="autocomplete" value="@if($id>0){{$user_detail[0]->user_address}}@endif" required="required" onFocus="geolocate()">


							<input type="hidden"  id="street_number" name="street_number"/>

							<input type="hidden" class="field" id="route" name="route" />
							<input type="hidden"  id="country" name="country" />
							<input type="hidden" class="form-control" name="address1" id="address1" value="" />
							<input type="hidden" name="state" id="administrative_area_level_1" value="" />


						</div>
						<div class="form-group">
							<label>City</label>
							<input type="text" id="locality" name="user_city" class="form-control" value="@if($id>0){{$user_detail[0]->user_city}}@endif">
						</div>
						<div class="form-group">
							<label>Postal code</label>
							<input type="text" id="postal_code" name="user_zipcode" class="form-control" value="@if($id>0){{$user_detail[0]->user_zipcode}}@endif" number="number" maxlength="6" />
						</div>

						<div class="form-group">

							<div class="row">
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label>Dummy-1</label>
										<select name="dummy1" id="dummy1" class="form-control">
											<option  value="0" @if(($id>0)&& (($user_detail[0]->dummy1=='0') ||($user_detail[0]->dummy1=='House') )) selected="selected" @endif>House</option>
											<?php
											if($user_list)
											{
												foreach($user_list as $ulist)
												{
													?>
													<option  value="<?php echo $ulist->id;?>" @if(($id>0)&& ($user_detail[0]->dummy1==$ulist->id)) selected="selected" @endif><?php echo $ulist->name;?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label>Dummy-2</label>
										<select name="dummy2" id="dummy2" class="form-control">
											<option  value="0" @if(($id>0)&& (($user_detail[0]->dummy2=='0') ||($user_detail[0]->dummy2=='House') )) selected="selected" @endif>House</option>
											<?php
											if($user_list)
											{
												foreach($user_list as $ulist)
												{
													?>
													<option  value="<?php echo $ulist->id;?>" @if(($id>0)&& ($user_detail[0]->dummy2==$ulist->id)) selected="selected" @endif><?php echo $ulist->name;?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-4">
									<div class="form-group">
										<label>Dummy-3</label>
										<select name="dummy3" id="dummy3" class="form-control">
											<option  value="0" @if(($id>0)&& (($user_detail[0]->dummy3=='0') ||($user_detail[0]->dummy3=='House'))) selected="selected" @endif>House</option>
											<?php
											if($user_list)
											{
												foreach($user_list as $ulist)
												{
													?>
													<option  value="<?php echo $ulist->id;?>" @if(($id>0)&& ($user_detail[0]->dummy3==$ulist->id)) selected="selected" @endif><?php echo $ulist->name;?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
						</div>




					</div><!-- /.box-body -->

					<div class="box-footer">
						@if($id==0)
						<input type="button" class="btn btn-primary" onclick="check_email();" value="Submit"  />
						@else
						<input type="button" class="btn btn-primary" value="Submit"  onclick="check_valid();" />

						@endif

						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>

			</div><!-- /.box -->


		</div>


	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection

<style>
#ajax_parner_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_parner_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
}
#addons-modal.modal {
	z-index: 999;
}
</style>

<div id="ajax_parner_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>



@section('js_bottom')

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
	function check_email()
	{


		var dummy1 = $('#dummy1').val();
		var dummy2 = $('#dummy2').val();
		var dummy3 = $('#dummy3').val();


		var form = $("#user_frm");
		form.validate();
		var valid =	form.valid();
		if(valid){


			var dummy_counter = 0;

			if((dummy1==0) && (dummy2==0) && (dummy3==0))
			{
				dummy_counter = 1;
			}
			else
			{

				if( (dummy1==0) || (dummy2==0) || (dummy3==0) )
				{
					alert('Default user not match with other User , So Please select only default user or  different user!');
					dummy_counter = 0;
				}
				else if( (dummy1==dummy2) || (dummy2==dummy3) || (dummy1==dummy3) )
				{
					alert('User can not be same, So Please select different user!');
					dummy_counter = 0;
				}
				else
				{
					dummy_counter = 1;
				}

			}

			if(dummy_counter==1)
			{

				$("#ajax_parner_loddder").show();

				var frm_val = $('#user_frm').serialize();
				$.ajax({
					type: "POST",
					url: "./check_user_duplicateemail",
					data: frm_val,
					success: function(msg) {

						$("#ajax_parner_loddder").hide();
						if(msg=='1')
						{
							$('#email_msg').show();
							$('#number_msg').show();
							return false;
						}
						else if(msg=='2')
						{
							$('#email_msg').show();
							$('#number_msg').hide();
							return false;
						}
						else if(msg=='3')
						{
							$('#number_msg').show();
							$('#email_msg').hide();
							return false;
						}
						else if(msg=='4')
						{
							$(form).submit();
							return true;
						}
					}
				});
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}


	function check_valid()
	{
		var form = $("#user_frm");
		form.validate();
		var valid =	form.valid();
		if(valid){


			var dummy1 = $('#dummy1').val();
			var dummy2 = $('#dummy2').val();
			var dummy3 = $('#dummy3').val();

			var dummy_counter = 0;

			if((dummy1==0) && (dummy2==0) && (dummy3==0))
			{
				dummy_counter = 1;
			}
			else
			{

				if( (dummy1==0) || (dummy2==0) || (dummy3==0) )
				{
					alert('Default user not match with other User , So Please select only default user or  different user!');
					dummy_counter = 0;
				}
				else if( (dummy1==dummy2) || (dummy2==dummy3) || (dummy1==dummy3) )
				{
					alert('User can not be same, So Please select different user!');
					dummy_counter = 0;
				}
				else
				{
					dummy_counter = 1;
				}

			}

			if(dummy_counter==1)
			{
				var frm_val = $('#user_frm').serialize();
				$(form).submit();
			}
			else
			{
				return false;
			}


				/*var frm_val = $('#user_frm').serialize();
				$(form).submit();*/
			}
			else
			{
				return false;
			}
		}
	</script>

	<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;

			//alert(val);
		}
	}
}

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }
  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
  @stop