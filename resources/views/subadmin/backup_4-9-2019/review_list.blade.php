@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <style>
  .popup_divcall{
	  margin: 10px;  
  }
  </style>
   <meta name="_token" content="{!! csrf_token() !!}"/>

@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Review & Rating  List
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Review List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					<div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Review & Rating  Management</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
										</div>
									
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
									
									<form name="search_frm" id="search_frm" method="post" >
									
													{!! csrf_field() !!}
													<div>
													<table cellpadding="5" cellspacing="5" width="100%" style="border:1px solid #999999" class="table">
													<tr>
														<td width="50%">
														<table cellpadding="5" cellspacing="5" width="100%">
															
														<tr>
														<td> Status: </td> 
														<td>
															<select name="order_status" id="order_status"class="form-control">
																<option value="All">All </option>
																<option value="SUBMIT" >SUBMIT</option>
																<option value="PUBLISHED">PUBLISHED</option>
																<option value="REJECT">REJECT</option>
															</select>
														</td> 
														</tr>
														</table>
														</td>
														<td width="50%">
														<table cellpadding="5" cellspacing="5" width="100%">
															
														<tr>
														<td>From Date: </td>
														<td>
															<input type="text" name="fromdate" id="fromdate" value=""  class="form-control datepicker"/>
														</td> 
														</tr>
														<tr>
														<td>To Date: </td>
														<td>
															<input type="text" name="todate" id="todate" value="" class="form-control datepicker"/>
															
														 <div id="error_msg" style="color:#FF0000; display:none;"></div>
														</td> 
														</tr>
														
														</table>
														</td>
														</tr>
														
														<tr>
															<td colspan="2">
														
														<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" />
														
														</td>
														</tr>
													</table>
													</div>
													</form>
								
									
								
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
							<div>
							<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">
										    <div class="box">
												<div class="box-body table-responsive">
													<div style="float:right; margin:5px;"> 
												
												    </div>
													<div id="restaurant_list">							
														 <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>
                                                <th>Order No</th>
                                                <th>Order Type</th>
                                                <th>User name</th>
												<th>Restaurant Name</th>
                                                <th>Status</th>
                                                <th>Rejection Reason </th>
                                                <th>Rating </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; ?>
										 @foreach($review_detail as $list)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $list->order_id}}</td>
                                                <td>{{ $list->order_type}}</td>
                                                <td>{{ $list->order_fname }}</td>
                                                <td>{{ $list->rest_name }}</td>
                                                <td>{{ $list->re_status }}</td>
                                                <td>@if($list->re_status=='REJECT'){{ str_replace('_',' ',$list->re_rejectreson) }}@endif</td>
												
                                                <td>{{ $list->re_rating }}</td>
                                                <td>
												 	 <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#update_review-{{$list->re_id}}"><i class="fa fa-eye"></i></a>
													   <a title="Delete User" href="{{url('admin/review_delete')}}/{{$list->re_id}}"onclick="return delete_wal()"><i class="fa fa-trash-o"></i></a>  
												
												<div class="modal fade" id="update_review-{{$list->re_id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="addons_modal_data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-envelope-o"></i> User Review</h4>
			</div>
			<form action="{{url('/admin/review_update')}}" method="post">
			<input type="hidden" name="re_id" id="re_id" value="{{$list->re_id}}" />
			
								{!! csrf_field() !!}
				<div class="box-body">
					<div class="popup_divcall">
						<label>User Name:</label> {{ $list->order_fname }}
					</div>
					<div class="popup_divcall">
						<label>Order ID:</label> {{ $list->order_id}}
					</div>
					<div class="popup_divcall">
						<label>Review Date:</label> {{ $list->created_at}}
					</div>
					<div class="popup_divcall">
						<label>Review Message:</label> {{ $list->re_content}}
					</div>
					<div class="popup_divcall">
						<label>Review Rating:</label> {{ $list->re_rating }}
					</div>
					
					<div class="popup_divcall">
						<label>Review Status:</label> {{ $list->re_status }}
					</div>
					
					<div class="popup_divcall">
						<label>Food was good:</label> <?php if($list->re_food_good=='1'){ echo 'Yes';} else { echo 'No';}?>
					</div>
					
					<div class="popup_divcall">
					<label>Delivery was on time:</label> <?php if($list->re_delivery_ontime=='1'){ echo 'Yes';} else { echo 'No';}?>
					</div>
					
					<div class="popup_divcall">
					<label>Order was accurate:</label> <?php if($list->re_order_accurate=='1'){ echo 'Yes';} else { echo 'No';}?>
					</div>
					
					<div class="popup_divcall">
						<label>Review Status:</label> 
			<select name="revew_status" data-reid="{{$list->re_id}}" id="revew_status" class="form-control">
			<option value="SUBMIT" @if($list->re_status=='SUBMIT') selected="selected" @endif>SUBMIT</option>
			<option value="PUBLISHED" @if($list->re_status=='PUBLISHED') selected="selected" @endif>PUBLISHED</option>
			<option value="REJECT" @if($list->re_status=='REJECT') selected="selected" @endif>REJECT</option>
			
			</select>
					</div>
					
					<div id="Reason_reject{{$list->re_id}}" style="@if($list->re_status=='REJECT') display:block  @else display:none @endif" class="popup_divcall">
						<label>Reason:</label> 
							<select name="reason" id="reason" class="form-control">
							<option value="UNACCEPTABLE_LANGUAGE" @if($list->re_rejectreson=='UNACCEPTABLE_LANGUAGE') selected="selected" @endif>UNACCEPTABLE LANGUAGE</option>
							<option value="COMPARISON_TO_COMPETITION" @if($list->re_rejectreson=='COMPARISON_TO_COMPETITION') selected="selected" @endif>COMPARISON TO COMPETITION</option>
							<option value="INVALID_REASONING" @if($list->re_rejectreson=='INVALID_REASONING') selected="selected" @endif>INVALID REASONING</option>
							<option value="OTHER" @if($list->re_rejectreson=='re_rejectreson') selected="selected" @endif>OTHER</option>
							
							</select>
							
					</div>

				</div>
				<div class="modal-footer clearfix">


					<button type="submit" class="btn btn-primary pull-left">Update Status</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>

												</td>
                                            </tr> 										
									 <?php $i++; ?>
									@endforeach	                                      
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>	
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>					
						  </div>
                        </div>
					
                        
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop

@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
		   <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
		
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });				
            });
			  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
		 dateFormat: 'yy-mm-dd',
		 autoclose: true
		});
  });	
$(document).on('change', '#revew_status', function(){
 var value = $(this).find('option:selected').val();
 var aat_val = $(this).attr('data-reid');		
	
	if(value=='REJECT')	
	{		
		 $("#Reason_reject"+aat_val).show();	
	}
	else
	{	
		 $("#Reason_reject"+aat_val).hide();	
	}
 }); 			
		
		  
$(document).on('click', '#search_btn', function(){ 
	
	  
	 $('#error_msg').hide();
	 
var frmdate = $('#fromdate').val(); 
var todate = $('#todate').val(); 
	
	if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
	{//compare end <=, not >=
		//your code here
		//alert("From date will be big from to date!");
		
		
		$('#error_msg').html('From date will be big from to date!');
		$('#error_msg').show()
	}
	else
	{	
		
	 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/review_search_list')}}",
		data: frm_val,
		success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			//alert(msg)
				$('#restaurant_list').html(msg);
			}
		});
	}		
}); 		
			

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});			
        </script>
@stop
