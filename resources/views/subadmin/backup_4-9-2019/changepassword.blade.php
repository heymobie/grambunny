@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Change Password
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Change Password</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-8">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Change Password</h3>
                                </div><!-- /.box-header -->
								<p style="float: left;text-align: center;width: 100%;">
				
					 @if(Session::has('message'))
								{{Session::get('message')}}
							@endif </p>
                                <!-- form start -->
								
								<form  role="form" id="update_pwd" method="POST" action="{{ url('/admin/change_password') }}">
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Old Password</label>
                                           <input type="password" class="form-control" name="old_password" value="{{ old('old_password') }}" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">New  Password</label>
                                            <input type="password" class="form-control" name="password" id="new_password" required="required">
                                        </div>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" id="update_oldpwd" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
			<script>
		 $(document).on('click', '#update_oldpwd', function(){ 
		 	
			var form = $("#update_pwd");
				form.validate();
			var valid =	form.valid();
		 });
		</script>
@stop