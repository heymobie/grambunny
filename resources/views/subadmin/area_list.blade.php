@extends('layouts.subadmin')
@section("other_css")
<!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
@stop
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Area List
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Area List</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Area Listing</h3>
            <div style="float:right; margin-right:10px; margin-top:10px;">
              <a href="{{url('sub-admin/area-form')}}" class="btn btn-primary" style="color:#FFFFFF">Add New Area</a>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive">
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable">
              <i class="fa fa-check"></i>
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
              {{Session::get('message')}}
            </div>
            @endif
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>SrNo</th>
                  <th>Area Name</th>
                  <th>Country</th>
                  <th>State</th>
                  <th>City</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $i=1; ?>
                @foreach ($area_list as $list)
                <tr>
                  <td>{{ $i }}</td>
                  <td>{{ $list->area_name}}</td>
                  <td>{{ $list->area_country }}</td>
                  <td>{{ $list->area_state }}</td>
                  <td>{{ $list->area_city }}</td>
                <!-- <td>@if($list->status==1)
                  <span class="label label-success">Active</span>
                  @else
                  <span class="label label-danger">Inactive</span>@endif</td> -->
                  <td>
                    <a href="{{url('sub-admin/area-edit-form/')}}/{{ $list->area_id }}"><i class="fa fa-edit"></i></a>
                    <a title="View Detail" href="{{url('sub-admin/area-view')}}/{{ $list->area_id }}" ><i class="fa fa-eye"></i></a>
                    <a title="Delete" href="{{url('sub-admin/area_delete')}}/{{ $list->area_id }}"onclick="return confirm('Are You sure to delete this area?')"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                <?php $i++; ?>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <th>&nbsp;</th>
                  <!-- <th>&nbsp;</th> -->
                  <th>&nbsp;</th>
                              <!-- <th>&nbsp;</th>
                                <th>&nbsp;</th> -->
                              </tr>
                            </tfoot>
                          </table>
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>
                  </div>
                </section><!-- /.content -->
              </aside><!-- /.right-side -->
              @stop
              @section('js_bottom')
              <!-- jQuery 2.0.2 -->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
              <!-- Bootstrap -->
              <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
              <!-- DATA TABES SCRIPT -->
              <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
              <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
              <!-- AdminLTE App -->
              <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
              <!-- page script -->
              <script type="text/javascript">
                $(function() {
                  $("#example1").dataTable();
                  $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                  });
                });
              </script>
              @stop