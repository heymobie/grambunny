@extends('layouts.subadmin')
<style>
.ui-widget-content {
	max-height: 221px;
	overflow-y: scroll;
}
.ui-menu .ui-menu-item {
	padding: 5px;
}
.ui-menu-item:nth-child(2n) {
	background-color: #f1f1f1;
}

/*Error css start*/
.form-group.big-eror.cusn label.error {
 position: absolute;
 left: -48px;
 min-width: 300px;
 top:2px;
}

.form-group.big-eror.cusn {
 position: relative;
 margin-bottom: 18px;
}

.form-group.big-eror.serv {
 position: relative;
 margin-bottom: 18px;
}

.form-group.big-eror.serv label.error {
 position: absolute;
 left: -53px;
 min-width: 300px;
 top:2px;
}

.form-group.partil_pmt {
 position: relative;
 margin-bottom: 20px;
}



.form-group.partil_pmt label.error {
 position: absolute !important;
 left: -155px !important;
 min-width: 300px;
 top: 2px;
}


@media(min-width:992px) and (max-width:1420px)
{
  .form-group.big-eror.cusn label.error
  {
   top: 24px;
 }
}

/*End*/
</style>
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Restaurant Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Restaurant Form</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Restaurant Form</h3>
				</div><!-- /.box-header -->
				@if(Session::has('message_error'))
				<div class="alert alert-danger alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					{{Session::get('message_error')}}
				</div>
				@endif
				<!-- form start -->
				<form  role="form" method="POST" id="rest_frm" action="{{ url('/sub-admin/restaurant_action') }}" enctype="multipart/form-data">
					<input type="hidden" name="rest_id" value="{{$id}}" />
					<input type="hidden" name="rest_old_logo" id="rest_old_logo" value="@if($id>0){{$rest_detail[0]->rest_logo}} @endif" />

          <input type="hidden" name="rest_old_banner" id="rest_old_banner" value="@if($id>0){{$rest_detail[0]->rest_banner}} @endif" />
          <input type="hidden" name="vendor_id" id="vendor_id" value="@if($id>0){{$rest_detail[0]->vendor_id}}@else{{$vendor_id}} @endif" />
          {!! csrf_field() !!}
          <div class="box-body">
            <div class="form-group">
             <label for="exampleInputEmail1">Name</label>
             <input type="text" class="form-control" name="rest_name" id="rest_name" value="@if($id>0){{$rest_detail[0]->rest_name}}@endif" required="required">
           </div>
                                     <!--  <div class="form-group">
                                            <label for="exampleInputEmail1">Meta tag</label>
                                           <input type="text" class="form-control" name="rest_metatag" id="	rest_metatag" value="" required="required">
                                         </div>-->
                                         <div class="form-group">
                                          <label for="exampleInputEmail1">Description</label>
                                          <textarea  class="form-control" name="rest_desc" id="rest_desc" required="required">@if($id>0){{$rest_detail[0]->rest_desc}}@endif</textarea>
                                        </div>
										<!--<div class="form-group big-eror">
                                            <label for="exampleInputEmail1">Restaurant Type</label>
											 &nbsp; &nbsp;
                                            <input  type="checkbox"  class="form-control" name="rest_type[]" id="rest_type" value="1" @if(($id>0) && ( in_array('1',(explode(',',$rest_detail[0]->rest_type)) )))checked="checked"@endif > Vegan	 &nbsp;
                                            <input type="checkbox" class="form-control" name="rest_type[]" id="rest_type"  value="2" @if(($id>0) && ( in_array('2',(explode(',',$rest_detail[0]->rest_type)) )))checked="checked"@endif > Kosher	 &nbsp;
                                            <input type="checkbox"  class="form-control" name="rest_type[]" id="rest_type" value="3" @if(($id>0) && ( in_array('3',(explode(',',$rest_detail[0]->rest_type)) )))checked="checked"@endif > Halal	 &nbsp;
                                            <input type="checkbox"  class="form-control" name="rest_type[]" id="rest_type" value="4" @if(($id>0) && ( in_array('4',(explode(',',$rest_detail[0]->rest_type)) )))checked="checked"@endif > Vegetarian	 &nbsp;
                                          </div>-->
                                          <div class="form-group big-eror cusn">
                                           <label for="exampleInputEmail1">Cuisine</label>
                                           @if(!empty($cuisine_list))
                                           @foreach($cuisine_list as $Clist)
                                           <input type="checkbox"  required="required" class="form-control" name="rest_cuisine[]" id="rest_service" value="{{$Clist->cuisine_id}}" @if(($id>0) && ( in_array($Clist->cuisine_id,(explode(',',$rest_detail[0]->rest_cuisine)) )))checked="checked" @endif > {{$Clist->cuisine_name}}	 &nbsp;
                                           @endforeach
                                           @endif
                                         </div>
                                         <div class="form-group big-eror serv">
                                           <!--<label for="exampleInputEmail1">Services</label>-->
                                           <!-- &nbsp; &nbsp; -->
                                           <input  type="hidden" required="required" class="form-control" name="rest_service[]" id="rest_service" value="Delivery" @if(($id>0) && ( in_array('Delivery',(explode(',',$rest_detail[0]->rest_service)) )))checked="checked"@endif > 
                                           <!--<input type="checkbox" required="required" class="form-control" name="rest_service[]" id="rest_service"  value="Pickup" @if(($id>0) && ( in_array('Pickup',(explode(',',$rest_detail[0]->rest_service)) )))checked="checked"@endif > Pick-up -->
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Min Delivery Amount  in $</label>
                                           <input type="text" class="form-control" name="rest_mindelivery" id="	rest_mindelivery" value="@if($id>0){{$rest_detail[0]->rest_mindelivery}}@endif" required="required" number="number" >
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Contact Number </label>
                                           <input type="text" class="form-control" name="rest_landline" number="number"  id="rest_landline" value="@if($id>0){{$rest_detail[0]->rest_landline}}@endif" required="required" maxlength="12" >
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Contact Mobile Number </label>
                                           <input type="text" class="form-control" name="rest_contact" number="number" id="rest_contact" value="@if($id>0){{$rest_detail[0]->rest_contact}}@endif" required="required" maxlength="10" >
                                           <div id="error_msg" style="color:#FF0000; display:none;"></div>
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Commission % to Admin</label>
                                           <input type="text" class="form-control" name="rest_commission" id="rest_commission" value="@if($id>0){{$rest_detail[0]->rest_commission}}@endif" required="required" number="number" />
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Min order amt in $ (for delivery) </label>
                                           <input type="text" class="form-control" name="rest_min_orderamt" id="rest_min_orderamt" required="required" value="@if($id>0){{$rest_detail[0]->rest_min_orderamt}}@endif" number="number"  >
                                         </div>
                                         <div class="form-group">
                                           <label for="exampleInputEmail1">Email</label>
                                           <input type="email" class="form-control"  id="rest_email" name="rest_email"  value="@if($id>0){{$rest_detail[0]->rest_email}}@endif" required="required" >
                                         </div>
                                         <div class="form-group" id="locationField">
                                           <label for="exampleInputEmail1">Address Line1</label>
                                           <input type="text" class="form-control" name="rest_address" id="autocomplete" value="@if($id>0){{$rest_detail[0]->rest_address}}@endif" required="required" >
                                           <input type="hidden" class="form-control" name="r_lat" id="r_lat" >
                                           <input type="hidden" class="form-control" name="r_lang" id="r_lang" >
                                           <input type="hidden"  id="street_number" name="street_number"/>
                                           <input type="hidden" class="field" id="route" name="route" />
                                           <input type="hidden"  id="country" name="country" />
                                         </div>
                                      <!--  <div class="form-group">
                                            <label for="exampleInputEmail1">Address Line2</label>
                                           <input type="text" class="form-control" name="rest_address2" id="rest_address2" value="@if($id>0){{$rest_detail[0]->rest_address2}}@endif">
                                         </div>-->
                                         <div class="form-group">
                                          <label for="exampleInputEmail1">City</label>
                                          <input type="text" class="form-control" name="rest_suburb" id="locality" required="required" value="@if($id>0){{$rest_detail[0]->rest_suburb}}@endif" >
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleInputEmail1">Postcode</label>
                                          <input type="text" class="form-control" name="rest_zip_code" id="postal_code" required="required" value="@if($id>0){{$rest_detail[0]->rest_zip_code}}@endif"  >
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleInputEmail1">State</label>
                                          <input type="text" class="form-control" name="rest_state" id="administrative_area_level_1" required="required" value="@if($id>0){{$rest_detail[0]->rest_state}}@endif">
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleInputEmail1">Status</label>
                                          <select name="rest_status" id="rest_status" class="form-control">
                                           <option value="PUBLISHED" @if(($id>0)&& ($rest_detail[0]->rest_status=='PUBLISHED')) selected="selected" @endif>PUBLISHED</option>
                                           <!--<option value="ACTIVE" @if(($id>0)&& ($rest_detail[0]->rest_status=='ACTIVE')) selected="selected" @endif>ACTIVE</option>-->
                                           <option value="UNPUBLISHED" @if(($id>0)&& ($rest_detail[0]->rest_status=='UNPUBLISHED')) selected="selected" @endif>UNPUBLISHED</option>
                                         </select>
                                       </div>
                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Price Level</label>
                                       	<select name="rest_price_level" id="rest_price_level" class="form-control" required="required">
                                       		<option value="" >Please Select price level </option>
                                       		<option value="1" @if(($id>0)&& ($rest_detail[0]->rest_price_level=='1')) selected="selected" @endif>Normal ($)</option>
                                       		<option value="2" @if(($id>0)&& ($rest_detail[0]->rest_price_level=='2')) selected="selected" @endif>Inexpensive ($$)</option>
                                       		<option value="3" @if(($id>0)&& ($rest_detail[0]->rest_price_level=='3')) selected="selected" @endif>Moderate ($$$)</option>
                                       		<option value="4" @if(($id>0)&& ($rest_detail[0]->rest_price_level=='4')) selected="selected" @endif>Expensive ($$$$)</option>
                                       		<option value="5" @if(($id>0)&& ($rest_detail[0]->rest_price_level=='5')) selected="selected" @endif>Very Expensive ($$$$$)</option>
                                       	</select>
                                       </div>
                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Delivery Upto (in miles, "Enter only numeric value")  </label>
                                       	<input type="text" class="form-control" name="rest_delivery_upto" id="rest_delivery_upto" required="required" value="@if($id>0){{$rest_detail[0]->rest_delivery_upto}}@endif" number="number"  >
                                       </div>
                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Sales Tax in %</label>
                                       	<input type="text" class="form-control" name="rest_servicetax" id="rest_servicetax" value="@if($id>0){{$rest_detail[0]->rest_servicetax}}@else{{6.5}}@endif" required="required" number="number" />
                                       </div>
                                       <div class="form-group partil_pmt">
                                       	<label for="exampleInputEmail1">Partial payment support</label>
                                       	<input type="radio"  name="rest_partial_pay" class="partial_pay" value="1" required="required" @if(($id>0)&& ($rest_detail[0]->rest_partial_pay=='1')) checked="checked" @endif/> Yes
                                       	<input type="radio" name="rest_partial_pay" class="partial_pay" value="0" required="required" @if(($id>0)&& ($rest_detail[0]->rest_partial_pay=='0')) checked="checked" @endif/> No
                                       </div>
                                       <div class="form-group" id="payment_div" style=" <?php if(($id>0)&& ($rest_detail[0]->rest_partial_pay=='1')) {?>display:block <?php }else{?>display:none<?php }?>">
                                       	<label for="exampleInputEmail1">Partial payment percent(%) </label>
                                       	<input type="text" class="form-control" name="rest_partial_percent" id="rest_partial_percent" value="@if($id>0){{$rest_detail[0]->rest_partial_percent}}@endif" number="number" />
                                       </div>

                                       <!--<div class="form-group">
                                       	<label for="exampleInputEmail1">Cash on delivery support</label>
                                       	<input type="radio" name="rest_cash_deliver" value="1" required="required"  @if(($id>0)&& ($rest_detail[0]->rest_cash_deliver=='1')) checked="checked" @else if(($id==0)  checked="checked" @endif/> Yes
                                       	<input type="radio" name="rest_cash_deliver" value="0" required="required"  @if(($id>0)&& ($rest_detail[0]->rest_cash_deliver=='0')) checked="checked" @endif/> No
                                       </div>-->

                                        <input type="hidden" name="rest_cash_deliver" value="1">

                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Today's specials</label>
                                       	<input type="checkbox" name="rest_special" id="rest_special" value="1" @if(($id>0) && ($rest_detail[0]->rest_special==1))checked="checked"@endif />
                                       </div>

                                       <!--<div class="form-group">
                                       	<label for="exampleInputEmail1">Classification</label>
                                       	<select name="rest_classi" id="rest_classi" class="form-control">
                                       		<option value="1" @if(($id>0)&& ($rest_detail[0]->rest_classi=='1')) selected="selected" @endif>Sponsored</option>
                                       		<option value="2" @if(($id>0)&& ($rest_detail[0]->rest_classi=='2')) selected="selected" @endif>Popular</option>
                                       		<option value="3" @if(($id>0)&& ($rest_detail[0]->rest_classi=='3')) selected="selected" @endif>Special</option>
                                       		<option value="4" @if(($id>0)&& ($rest_detail[0]->rest_classi=='4')) selected="selected" @endif>New</option>
                                       		<option value="5" @if(($id>0)&& ($rest_detail[0]->rest_classi=='5')) selected="selected" @endif>Standard</option>
                                       	</select>
                                       </div>-->

                                       <input type="hidden" name="rest_classi" id="rest_classi" value="4">


                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Upload Logo (File Type: jpeg,gif,png)</label>
                                       	<input type="file" class="form-control" name="rest_logo" id="rest_logo">
                                       	@if(($id>0) && (!empty($rest_detail[0]->rest_logo)))
                                       	<img src="{{ url('/') }}/uploads/reataurant/{{ $rest_detail[0]->rest_logo }}" width="50px;" height="50px;">
                                       	@endif
                                       </div>
                                       <!--<div class="form-group">
                                       	<label for="exampleInputEmail1">Upload Banner (File Type: jpeg,gif,png)</label>
                                       	<input type="file" class="form-control" name="rest_banner" id="rest_banner">
                                       	@if(($id>0) && (!empty($rest_detail[0]->rest_banner)))
                                       	<img src="{{ url('/') }}/uploads/reataurant/banner/{{ $rest_detail[0]->rest_banner }}" width="150px;" height="50px;">
                                       	@endif
                                       </div>-->
                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Open Hours</label>
                                       	<div class="col-md-12" style="border:1px solid #0099FF; " >
                                       		<div  class="col-md-12" >
                                       			<div class="col-md-3"><label>Day Name</label></div>
                                       			<div class="col-md-3"><label>From</label></div>
                                       			<div class="col-md-3"><label>To</label></div>
                                       			<div class="col-md-3"><label>Closed</label></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Monday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="mon_from" name="mon_from" value="@if($id>0){{$mon_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="mon_to" name="mon_to" value="@if($id>0){{$mon_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="mon" @if(($id>0) && ( in_array('mon',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif /></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Tuesday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="tues_from" name="tues_from" value="@if($id>0){{$tues_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="tues_to" name="tues_to" value="@if($id>0){{$tues_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="tue" @if(($id>0) && ( in_array('tue',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif /></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Wednesday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="wed_from" name="wed_from" value="@if($id>0){{$wed_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="wed_to" name="wed_to" value="@if($id>0){{$wed_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="wed" @if(($id>0) && ( in_array('wed',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif />
                                       			</div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Thursday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="thu_from" name="thu_from" value="@if($id>0){{$thu_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="thu_to" name="thu_to" value="@if($id>0){{$thu_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="thu" @if(($id>0) && ( in_array('thu',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif /></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Friday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="fri_from" name="fri_from" value="@if($id>0){{$fri_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="fri_to" name="fri_to" value="@if($id>0){{$fri_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="fri" @if(($id>0) && ( in_array('fri',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif/></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Saturday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="sat_from" name="sat_from" value="@if($id>0){{$sat_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="sat_to" name="sat_to" value="@if($id>0){{$sat_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="sat" @if(($id>0) && ( in_array('sat',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif /></div>
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Sunday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="sun_from" name="sun_from" value="@if($id>0){{$sun_from}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="sun_to" name="sun_to" value="@if($id>0){{$sun_to}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="sun" @if(($id>0) && ( in_array('sun',(explode(',',$rest_detail[0]->rest_close)) )))checked="checked"@endif /></div>
                                       		</div>
                                       	</div>
                                       </div>
                                       <div class="form-group">
                                       	&nbsp;
                                       </div>
                                       <div class="form-group">
                                       	<label for="exampleInputEmail1">Delivery Hours</label>
                                       	<div class="col-md-12" style="border:1px solid #0099FF; " >
                                       		<div  class="col-md-12" >
                                       			<div class="col-md-3"><label>Day Name</label></div>
                                       			<div class="col-md-3"><label>From</label></div>
                                       			<div class="col-md-3"><label>To</label></div>
                                       			<!-- 													<div class="col-md-3"><label>Closed</label></div> -->
                                       		</div>
                                       		<div class="col-md-12">
                                       			<div class="col-md-3"><b>Monday</b></div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="mon_from_del" name="mon_from_del" value="@if($id>0){{$mon_from_del}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
                                       			<div class="col-md-3">
                                       				<div class="bootstrap-timepicker">
                                       					<div class="form-group">
                                       						<div class="input-group">
                                       							<input type="text" class="form-control timepicker" id="mon_to_del" name="mon_to_del" value="@if($id>0){{$mon_to_del}}@endif"/>
                                       							<div class="input-group-addon">
                                       								<i class="fa fa-clock-o"></i>
                                       							</div>
                                       						</div><!-- /.input group -->
                                       					</div><!-- /.form group -->
                                       				</div>
                                       			</div>
													<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="mon" @if(($id>0) && ( in_array('mon',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div>
													</div> -->
													<div class="col-md-12">
														<div class="col-md-3"><b>Tuesday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="tue_from_del" name="tue_from_del" value="@if($id>0){{$tue_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="tue_to_del" name="tue_to_del" value="@if($id>0){{$tue_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="tue" @if(($id>0) && ( in_array('tue',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div> -->
													</div>
													<div class="col-md-12">
														<div class="col-md-3"><b>Wednesday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="wed_from_del" name="wed_from_del" value="@if($id>0){{$wed_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="wed_to_del" name="wed_to_del" value="@if($id>0){{$wed_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="wed" @if(($id>0) && ( in_array('wed',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div> -->
													</div>
													<div class="col-md-12">
														<div class="col-md-3"><b>Thursday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="thu_from_del" name="thu_from_del" value="@if($id>0){{$thu_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="thu_to_del" name="thu_to_del" value="@if($id>0){{$thu_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="thu" @if(($id>0) && ( in_array('thu',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div> -->
													</div>
													<div class="col-md-12">
														<div class="col-md-3"><b>Friday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="fri_from_del" name="fri_from_del" value="@if($id>0){{$fri_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="fri_to_del" name="fri_to_del" value="@if($id>0){{$fri_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="fri" @if(($id>0) && ( in_array('fri',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif/></div> -->
													</div>
													<div class="col-md-12">
														<div class="col-md-3"><b>Saturday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="sat_from_del" name="sat_from_del" value="@if($id>0){{$sat_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="sat_to_del" name="sat_to_del" value="@if($id>0){{$sat_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- <div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="sat" @if(($id>0) && ( in_array('sat',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div> -->
													</div>
													<div class="col-md-12">
														<div class="col-md-3"><b>Sunday</b></div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="sun_from_del" name="sun_from_del" value="@if($id>0){{$sun_from_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-3">
															<div class="bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker" id="sun_to_del" name="sun_to_del" value="@if($id>0){{$sun_to_del}}@endif"/>
																		<div class="input-group-addon">
																			<i class="fa fa-clock-o"></i>
																		</div>
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<!-- 	<div class="col-md-3"><input type="checkbox" name="close_del[]" id="close_del" value="sun" @if(($id>0) && ( in_array('sun',(explode(',',$rest_detail[0]->rest_del_close)) )))checked="checked"@endif /></div> -->
													</div>
													<div class="col-md-12" id="time_error" style="display:none">
														<div class="col-md-4">&nbsp;</div>
														<div class="col-md-8">
															<span style="color:#FF0000">
																Delivery/Pickup to time always lessthan from time.
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												&nbsp;
											</div>
											<div class="form-group">
												<label for="exampleInputEmail1">Holiday Calender</label>
												<div class="col-md-12" style="border:1px solid #0099FF; " >
													<div  class="col-md-12" >
														<div class="col-md-4"><label>Date</label></div>
														<div class="col-md-4"><label>From</label></div>
														<div class="col-md-4"><label>To</label></div>
													</div>
													<div class="col-md-12">
														<div class="col-md-4"><b>Holiday Date</b></div>
														<div class="col-md-4">
															<div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" name="holiday_from" id="startdate"  value="@if($id>0){{$rest_detail[0]->rest_holiday_from}}@endif" />
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
														<div class="col-md-4">
															<div>
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control" name="holiday_to" id="enddate" value="@if($id>0){{$rest_detail[0]->rest_holiday_to}}@endif" />
																	</div><!-- /.input group -->
																</div><!-- /.form group -->
															</div>
														</div>
													</div>
													<div class="col-md-12" id="time_error" style="display:none">
														<div class="col-md-4">&nbsp;</div>
														<div class="col-md-8">
															<span style="color:#FF0000">
																Delivery/Pickup  time always lessthan from Delivery/Pickup to  time.
															</span>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												&nbsp;
											</div>
										</div>
										<div class="box-footer">
											<input type="button" class="btn btn-primary" value="Submit" onclick="check_frm()" />
											<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
										</div>
									</form>
								</div><!-- /.box -->
							</div>
						</section><!-- /.content -->
					</aside><!-- /.right-side -->
					@endsection
					@section('js_bottom')
					<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
					<!-- jQuery 2.0.2 -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
					<!-- jQuery UI 1.10.3 -->
					<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
					<!-- Bootstrap -->
					<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
					<!-- Bootstrap WYSIHTML5 -->
					<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
					<!-- bootstrap time picker -->
					<script src="{{ url('/') }}/design/admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
					<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
					<!-- AdminLTE App -->
					<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
					<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
					<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
					<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
					<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
					<script type="text/javascript">
						$(document).on('ifChanged', '.partial_pay', function(){
							if($('.partial_pay').prop("checked"))
							{
								$('#payment_div').show();
								$('#rest_partial_percent').prop("required","required");
							}
							else
							{
								$('#payment_div').hide();
								$('#rest_partial_percent').prop("required","");
							}
				//alert($('.partical_pay').prop("checked"));
				//$('.foodallclass').prop("checked",true);
			});
						$(".timepicker").timepicker({
							showInputs: false,
						});
						$(function() {
							var date = new Date();
							date.setDate(date.getDate());
							$("#startdate").datepicker({
								dateFormat: "yy-mm-dd",
								onSelect: function(selected) {
									var date1 = $('#startdate').datepicker('getDate', '+1d');
				 // date1.setDate(date1.getDate()+1);
				 date1.setDate(date1.getDate());
				 var date2 = $('#startdate').datepicker('getDate', '+1d');
				 date2.setDate(date2.getDate()+30);
				 $("#enddate").datepicker("option","minDate", date1);
				 $("#enddate").datepicker("option","maxDate", date2);
				},
				minDate: 0,
				autoclose: true,
			});
							$("#enddate").datepicker({
								dateFormat: "yy-mm-dd",
								onSelect: function(selected) {
									$("#startdate").datepicker("option","maxDate", selected)
								},
								minDate: 0 ,
								autoclose: true,
							});
						});
						$(document).on('keyup', '#rest_suburb', function(){
							$( "#rest_suburb" ).autocomplete({
								source: "{{url('/sub-admin/get_suburblist') }}",
								select: function(event, ui) {
									$("#rest_suburb").val(ui.item.value);
									$("#rest_zip_code").val(ui.item.pincode);
									$("#rest_city").val(ui.item.city);
									$("#rest_state").val(ui.item.state);
								}
							});
						});
						function check_frm()
						{
							$('#error_msg').hide();
							var start_date = $('#startdate').val();
							var end_date =  $('#enddate').val();
							if(start_date!='')
							{
								$("#enddate").prop('required',true);
							}
							var delivery=0;
			/*var start = $("#delivery_from").val();//"01:00 PM";
			var end = $("#delivery_to").val();*///"11:00 AM";
			/*var dtStart = new Date("1/1/2011 " + start);
			var dtEnd = new Date("1/1/2011 " + end);
			var difference_in_milliseconds = dtEnd - dtStart;
			//alert(difference_in_milliseconds);
			if (difference_in_milliseconds <= 0)
			{
				 delivery=0;
				$("#time_error").show();
				return false;
			}
			else
			{
				$("#time_error").hide();
				 delivery=1;
				}*/

        // $.validator.addMethod("regx", function(value, element, regexpr) {
        //   return regexpr.test(value);
        // }, "Please upload valid extension mp3 file.");


        jQuery.validator.addMethod("selectnic", function (value, element) {
          if (/^[a-zA-Z0-9]*$/.test(value)) {
            return true;
          } else {
            return false;
          };
        });


        var form = $("#rest_frm");
        form.validate({
         rules: {
          rest_name:{
           required:true,
           selectnic:true
         },
         rest_desc: {
           required: true,
           maxlength:300
           //rangelength:[10,20]
         },
         "rest_cuisine[]": {
           required: true,
         },
         "rest_service[]": {
           required: true,
         },
         rest_mindelivery: {
           required: true,
         },
         rest_landline: {
           required: true,
         },
         rest_contact: {
           required:true,
           maxlength:10,
           minlength:10
         },
         rest_commission: {
           required: true,
         },
         rest_min_orderamt: {
           required: true,
         },
         rest_email: {
           required: true,
         },
         rest_address: {
           required: true,
         },
         rest_suburb: {
           required: true,
         },
         rest_zip_code: {
           required: true,
         },
         rest_state: {
           required: true,
         },
         rest_price_level: {
           required: true,
         },
         rest_delivery_upto: {
           required: true,
         },
         rest_servicetax: {
           required: true,
         },
         rest_partial_pay: {
           required: true,
         },
         rest_partial_percent: {
           required: true,
         }
       },
       messages: {
        rest_name:{
         required:"Please enter restaurant name.",
         selectnic:"Please enter valid restaurant name."
       },
       rest_desc: {
         required: "Please enter restaurant description.",
         maxlength:"Please do not enter more then 300 charcters."
         //rangelength:"Please do not enter more then 10 charcters."
       },
       "rest_cuisine[]": {
         required: "Please select cuisine.",
       },
       "rest_service[]": {
         required: "Please select service.",
       },
       rest_mindelivery: {
         required: "Please enter minimum delivery amount.",
       },
       rest_landline: {
         required: "Please enter contact number.",
       },
       rest_contact: {
         required: "Please enter mobile number.",
         maxlength:"Please do not enter more then 10 digits.",
         minlength:"Please enter at least 10 digits."
       },
       rest_commission: {
         required: "Please enter commission % to Admin.",
       },
       rest_min_orderamt: {
         required: "Please enter minimum order amount.",
       },
       rest_email: {
         required: "Please enter contact email address.",
       },
       rest_address: {
         required: "Please enter restaurant postal address.",
       },
       rest_suburb: {
         required: "Please enter restaurant city.",
       },
       rest_zip_code: {
         required: "Please enter postcode.",
       },
       rest_state: {
         required: "Please enter restaurant state.",
       },
       rest_price_level: {
         required: "Please select price level.",
       },
       rest_delivery_upto: {
         required: "Please enter delivery range in miles.",
       },
       rest_servicetax: {
         required: "Please enter service tax.",
       },
       rest_partial_pay: {
         required: "Please select partial payment support.",
       },
       rest_partial_percent: {
         required: "Please enter partial payment percent %.",
       }
     }
   });
        var valid =	form.valid();
        var regExp = /^0[0-9].*$/;
        if(valid)
        {
         $(form).submit();
         return true;
         /*alert(regExp.test("0001"));*/
				/*if((regExp.test($("#rest_landline").val())) && (regExp.test($("#rest_contact").val())) && ( delivery==1))
				{
					$(form).submit();
					return true;
				}
				else
				{
					//alert('Land line and Mobile number start with 0.');
					$('#error_msg').html('Land line and Mobile number start with 0.');
					$('#error_msg').show();
					return false;
				}*/
			}
			else
			{
				return false;
			}
		}
	</script>
	<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };
    /*  function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }*/
      function initAutocomplete() {
       var input = document.getElementById('autocomplete');
       var autocomplete = new google.maps.places.Autocomplete(input);
       google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        document.getElementById('r_lat').value = place.geometry.location.lat();
        document.getElementById('r_lang').value = place.geometry.location.lng();
        for (var i = 0; i < place.address_components.length; i++) {
         for (var j = 0; j < place.address_components[i].types.length; j++) {
          if (place.address_components[i].types[j] == "postal_code") {
           $('#postal_code').val(place.address_components[i].long_name);
         }
         if (place.address_components[i].types[j] == "locality") {
           $('#locality').val(place.address_components[i].long_name);
         }
         if (place.address_components[i].types[j] == "administrative_area_level_1") {
           $('#administrative_area_level_1').val(place.address_components[i].long_name);
         }
       }
     }
   })
     }
      /*function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
			//alert(val);
          }
        }
      }*/
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
     /*function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }*/
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE&libraries=places&callback=initAutocomplete"></script>
    @stop