@extends('layouts.subadmin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Menu Template Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Menu Template Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Menu Template Form</h3>
				</div><!-- /.box-header -->

				@if(Session::has('error'))
				<div class="alert alert-danger alert-dismissable">
					{{Session::get('error')}}
				</div>
				@endif
				<!-- form start -->

				<form  role="form" enctype="multipart/form-data" method="POST" id="menu_temp_frm" action="{{ url('/sub-admin/menu_template_action') }}">
					<input type="hidden" name="template_id" value="{{$id}} " />


					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Template Name</label>
							<input type="text" class="form-control" name="template_name" id="template_name" value="@if($id>0){{$template_detail[0]->template_name}}@endif" required="required">
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Status</label>
							<select name="template_status" id="template_status"  class="form-control">
								<option value="1" @if(($id>0) && ($template_detail[0]->template_status==1)) selected="selected"@endif>Active </option>
								<option value="0" @if(($id>0) && ($template_detail[0]->template_status==0)) selected="selected"@endif>Inactive </option>
							</select>

						</div>




					</div> <!-- /.box-body -->

					<div class="box-footer">
						<input type="button" class="btn btn-primary" value="Submit" id="submit_temp_btn" onClick="check_email()" />


						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>

			</div><!-- /.box -->


		</div>


	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
	function check_email()
	{

		jQuery.validator.addMethod("vname", function (value, element) {
			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {
				return true;
			} else {
				return false;
			};
		});
		var form = $("#menu_temp_frm");
		form.validate({
			rules: {
				template_name:{
					required:true,
					vname:true,
				}
			},
			messages: {
				template_name:{
					required:'Please enter template name.',
					vname:'Please enter a valid template name.'
				}
			}
		});
		var valid =	form.valid();

		if(valid)
		{
			$('#submit_temp_btn').prop('disabled', true);
			$(form).submit();
			return true;
		}
		else
		{
			return false;
		}
	}
</script>
@stop