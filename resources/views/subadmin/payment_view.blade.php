@extends('layouts.subadmin')

@section("other_css")
<!-- DATA TABLES -->

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Payment View
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Payment View</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			@if(Session::has('message'))

			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('message')}}
			</div>
			@endif

			<div class="col-xs-12">
				<div class="box" style="display:block;float:left;padding-bottom:10px;">
					<div class="box-header">
						<h3 class="box-title">Payment View</h3>
						<div style="float:right; margin-right:10px; margin-top:10px;">
							<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
						</div>

					</div><!-- /.box-header -->
					<div class="box-body  table-responsive">
						<div class="col-md-12" style="border:1px solid #333333;">
							<div class="col-md-5">
								Status:

								<?php
								$payment_done_status='';
								$payment_not_done_status='';
								$payment_partial_done_status='';
								$payment_done_status='';
								if($order_detail[0]->pay_status=='1')
								{
									echo 'Payment Done';

									$payment_not_done_status='disabled="disabled"';
									$payment_partial_done_status='disabled="disabled"';

								}
								if($order_detail[0]->pay_status=='2'){ echo 'Partial Done';

							}
							if($order_detail[0]->pay_status=='3') { echo 'Payment Not Done';


						}
						if($order_detail[0]->pay_status=='4'){ echo 'Payment Refund';

						$payment_not_done_status='disabled="disabled"';
						$payment_partial_done_status='disabled="disabled"';
						$payment_done_status='disabled="disabled"';

					}
					?>




				</div>
				<form name="order_update" id="order_update" method="post">
					<div class="col-md-7">
						Change Status to:
						<input type="hidden" name="order_id" id="order_id" value="{{$order_detail[0]->order_id}}" />
						<input type="hidden" name="pay_id" id="pay_id" value="{{$order_detail[0]->pay_id}}" />
						<input type="hidden" name="old_status" id="old_status" value="{{$order_detail[0]->pay_status}}" />

						<select name="pay_status" id="pay_status">


							<option value="1" @if($order_detail[0]->pay_status=='1') selected="selected"@endif {{$payment_done_status}}> Payment Done</option>
							<option value="2" @if($order_detail[0]->pay_status=='2') selected="selected"@endif {{$payment_partial_done_status}}> Partial Done</option>
							<option value="3" @if($order_detail[0]->pay_status=='3') selected="selected"@endif {{$payment_not_done_status}}> Payment Not Done</option>
							<option value="4" @if($order_detail[0]->pay_status=='4') selected="selected"@endif >ToRefund</option>


						</select>

						<input type="button" value="Submit" id="update_status"  />

						<div class="col-md-7" id="capture_reference" style="display:none @if(($order_detail[0]->pay_status == "4" ) || ($order_detail[0]->pay_status == "5" )) display:block @endif">
							Outgoing Transaction Reference
							<input type="text" name="outgoing_transaction_reference" id="outgoing_transaction_reference"  required="required" value="{{$order_detail[0]->pay_outgoing_Ref}}"/>
						</div>

						<div id="error_msg" style="color:#FF0000; display:none;"></div>
					</div>
				</form>
			</div>

			<div class="col-md-12" style="border:1px solid #333333;">
				<div class="col-md-6">
					@if(!empty($order_detail[0]->rest_logo))
					<img src="{{ url('/') }}/uploads/reataurant/{{ $order_detail[0]->rest_logo }}" width="50px;" height="50px;">
					@else
					<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >
					@endif
				</div>
				<div class="col-md-6">

					<strong style="padding-top:5px;text-align:center">{{$order_detail[0]->order_type}} Order Details</strong>
				</div>
			</div>

			<div class="col-md-12" style="border:1px solid #333333;">
				<div class="col-md-2"> Venue:</div>
				<div class="col-md-9">{{$order_detail[0]->rest_name}}</div>
				<div class="col-md-2"> Address:</div>
				<div class="col-md-9">{{$order_detail[0]->rest_address.' '.$order_detail[0]->rest_address2}}
					{{$order_detail[0]->rest_suburb}}
					{{$order_detail[0]->rest_state}}
					{{$order_detail[0]->rest_zip_code}}
				</div>
				<div class="col-md-2"> Contact No:</div>
				<div class="col-md-9">{{$order_detail[0]->rest_contact}}</div>
			</div>


			<div class="col-md-12" style="border:1px solid #333333;">
				<div class="col-md-3">Order Number:</div>
				<div class="col-md-9">	{{$order_detail[0]->order_id}} ({{$order_detail[0]->order_uniqueid}})</div>

				<div class="col-md-3"> Pick-Up Date:</div>
				<div class="col-md-9">
					@if($order_detail[0]->order_pickdate!='0000-00-00')
					{{$order_detail[0]->order_pickdate}}&nbsp;
					{{substr($order_detail[0]->order_picktime, 0, 2) }}:{{ substr($order_detail[0]->order_picktime, 2, 3)}}
					@else
					{{$order_create_date}}
					@endif
				</div>
				<div class="col-md-3"> Delivery Address:</div>
				<div class="col-md-9">
					@if($order_detail[0]->order_type=='Delivery')
					{{$order_detail[0]->order_deliveryadd1}}
					{{$order_detail[0]->order_deliveryadd2}}
					{{$order_detail[0]->order_deliverysurbur}}
					{{$order_detail[0]->order_deliverypcode}}
					@else
					&nbsp;
					@endif

				</div>
										<!--<div class="col-md-3"> Amount:</div>
											<div class="col-md-9">&nbsp;</div>-->


											<div class="col-md-3">  ORDER STATUS:</div>
											<div class="col-md-9">
												@if($order_detail[0]->order_status=='1') Submit @endif
												@if($order_detail[0]->order_status=='2') Cancelled @endif
												@if($order_detail[0]->order_status=='4') Order Confirmed @endif
												@if($order_detail[0]->order_status=='5') Order Completed @endif
												@if($order_detail[0]->order_status=='6') Reject @endif
												@if($order_detail[0]->order_status=='7') Review Completed @endif
											</div>

											<div class="col-md-3">Order Submited:</div>
											<div class="col-md-9">{{$order_create_date}}</div>

											<div class="col-md-3">Order Complete/Cancel:</div>
											<div class="col-md-9">
												@if($order_detail[0]->order_status=='2' ||
												$order_detail[0]->order_status=='5' ||
												$order_detail[0]->order_status=='6'  )

												{{$order_can_comp_reg}}
												@endif


											</div>

										</div>


										<div class="col-md-12" style="border:1px solid #333333;">

											<div class="col-md-3"> Customer Name: </div>
											<div class="col-md-9">&nbsp;
												{{$order_detail[0]->order_fname.' '.$order_detail[0]->order_lname}}
											</div>

											<div class="col-md-3"> Mobile No:</div>
											<div class="col-md-9">&nbsp;{{$order_detail[0]->order_tel}} </div>
											<div class="col-md-3"> Email:</div>
											<div class="col-md-9">&nbsp;{{$order_detail[0]->order_email}} </div>
											<div class="col-md-3"> Customer Comment:</div>
											<div class="col-md-9">&nbsp;{{$order_detail[0]->order_instruction}}&nbsp; </div>



											<div class="col-md-3">Payment Method</div>
											<div class="col-md-9">&nbsp;{{$order_detail[0]->pay_method}}</div>

											<?php if(($order_detail[0]->pay_method)!='Cash'){?>
												<div class="col-md-3"> Payment Transection Id</div>
												<div class="col-md-9" >&nbsp;{{$order_detail[0]->pay_tx}} </div>
											</div>
										<?php }?>
										<div class="col-md-12" style="border:1px solid #333333;">
											<table width="100%" border="1" cellpadding="2" cellspacing="2">
												<tr>
													<td width="25%" align="center"><b>Item Details</b></td>
													<td width="15%" align="center"><b>Unit Size</b></td>
													<td width="10%" align="center"><b>Qty</b></td>
													<td width="10%" align="center"><b>Unit Price</b></td>
													<td width="10%" align="center"><b>Total</b></td>
												</tr>
												<?php
												$cart_price = 0;
												$order_carditem = json_decode($order_detail[0]->order_carditem, true);?>
												@foreach($order_carditem as $item)
												<?php
												$show_item_price = 0;
												$show_addon_price = 0;
												$show_sub_total = 0;

												$unit_name = '';

												if(!empty($item['options']['option_name']))
												{
													$category_item = DB::table('category_item')
													->select('*')
													->where('menu_cat_itm_id', '=' ,$item['options']['option_name'])
													->get();
													if($category_item )
													{
														$unit_name = $category_item[0]->menu_item_title;
													}

												}


												$cart_price = $cart_price+$item['price'];
												$addon_price = '';
												$addon_name = '';
												?>
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
												@foreach($item['options']['addon_data'] as $addon)

												<?php $cart_price = $cart_price+$addon['price'];
												$addon_price = $addon['price'];
												$addon_name = $addon['name'];

												?>

												@endforeach
												@endif




												<tr>
													<td width="25%" align="left">{{$item['name']}}

														@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
														@foreach($item['options']['addon_data'] as $addon)
														<div style="padding:5px;">{{$addon['name']}}</div>

														@endforeach
														@endif

													</td>
													<td width="15%" align="center">{{$unit_name}}</td>
													<td width="10%" align="center">{{$item['qty']}}</td>
													<td width="10%" align="right">
														<?php if(empty($order_detail[0]->order_restcartid)){?>
															${{number_format($item['price']/$item['qty'],2)}}


															@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
															@foreach($item['options']['addon_data'] as $addon)
															<div style="padding:5px;">${{number_format(($addon['price']/$item['qty']),2)}}</div>

															@endforeach
															@endif



														<?php }else{?>
															${{number_format($item['price'],2)}}
															@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
															@foreach($item['options']['addon_data'] as $addon)
															<div style="padding:5px;">${{number_format(($addon['price']),2)}}</div>

															@endforeach
															@endif


														<?php }?>

													</td>
													<td width="10%" align="right">
														<?php if(empty($order_detail[0]->order_restcartid)){?>
															${{number_format($item['price'],2)}}

															@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
															@foreach($item['options']['addon_data'] as $addon)
															<div style="padding:5px;">${{number_format(($addon['price']),2)}}</div>

															@endforeach
															@endif



														<?php }else{?>
															${{number_format($item['price']*$item['qty'],2)}}

															@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
															@foreach($item['options']['addon_data'] as $addon)
															<div style="padding:5px;">${{number_format(($addon['price']*$item['qty']),2)}}</div>

															@endforeach
															@endif


														<?php }?>



													</td>
												</tr>
												@endforeach
												<tr>
													<td width="25%" align="right"><b>Sub Total</b></td>
													<td colspan="4" align="right">
														<?php echo '$'.number_format($order_detail[0]->order_subtotal_amt,2);?>
														<?php /*?>${{number_format($cart_price,2)}}<?php */?></td>
													</tr>
													<tr>
														<td width="25%" align="right"><b>Delivery Fee</b></td>
														<td colspan="4" align="right">${{number_format($order_detail[0]->order_deliveryfee,2)}}</td>
													</tr>

													@if($order_detail[0]->order_remaning_delivery>0)
													<tr>
														<td width="25%" align="right">
															${{number_format($order_detail[0]->order_min_delivery,2)}} min Delivery amount<br />
															Remaining Amount :${{number_format($order_detail[0]->	order_remaning_delivery,2)}}
														</td>
														<td colspan="4" align="right">${{number_format($order_detail[0]->	order_remaning_delivery,2)}}
														</td>

													</tr>
													@endif



													@if($order_detail[0]->order_promo_cal>0)
													<tr>
														<td width="25%" align="right">
															<b> Discount</b>
														</td>
														<td colspan="3" align="right">{{$order_detail[0]->order_promo_applied}}
															<br />
															{{$order_detail[0]->order_food_promo}}
														</td>
														<td align="right">-${{number_format($order_detail[0]->	order_promo_cal,2)}}</td>

													</tr>
													@endif



													<tr>
														<td width="25%" align="right"><b>Sales Tax</b></td>
														<td colspan="4" align="right">
															${{number_format($order_detail[0]->order_service_tax,2)}}</td>
														</tr>



														<?php

														if($order_detail[0]->order_pmt_type!='1')
														{
															?>
															<tr>
																<td width="25%" align="right"><b>Partial Payment (Paid)</b></td>
																<td colspan="4" align="right">
																	${{number_format($order_detail[0]->order_partial_payment,2)}}</td>
																</tr>
																<tr>
																	<td width="25%" align="right"><b>Partial Remaning Payment</b></td>
																	<td colspan="4" align="right">
																		${{number_format($order_detail[0]->order_partial_remain,2)}}</td>
																	</tr>

																	<?php
																}

																?>



																<tr>
																	<td width="25%" align="right"><b>Amount Paid</b></td>
																	<td colspan="4" align="right">
																		${{number_format($order_detail[0]->order_paid_amt,2)}}</td>
																	</tr>
																	<tr>
																		<td width="25%" align="right"><b>Remaining Balance<?php
																		if(($order_detail[0]->order_remaing_amt>'0')
																			&& ($order_detail[0]->pay_status=='1'))
																		{
																			?>
																			(fully paid)
																			<?php
																		}
																		?> </b></td>
																		<td colspan="4" align="right">
																			${{number_format($order_detail[0]->order_remaing_amt,2)}}</td>
																		</tr>


																		<tr>
																			<td width="25%" align="right"><b>Total Charge</b></td>
																			<td colspan="4" align="right">
																				${{number_format($order_detail[0]->order_total,2)}}</td>
																			</tr>
																		</table>
																	</div>
																	<div style="float:right; margin:5px;">
																		<input class="btn btn-primary" type="button" value="Go Back"  onClick="history.go(-1);">
																	</div>

																</div><!-- /.box-body -->
															</div><!-- /.box -->
															<div>
															</div>
														</div>
													</div>

												</section><!-- /.content -->
											</aside><!-- /.right-side -->

											@stop


											@section('js_bottom')
											<style>

											#ajax_favorite_loddder {

												position: fixed;
												top: 0;
												left: 0;
												width: 100%;
												height: 100%;
												background:rgba(27, 26, 26, 0.48);
												z-index: 1001;
											}
											#ajax_favorite_loddder img {
												top: 50%;
												left: 46.5%;
												position: absolute;
											}

											.footer-wrapper {
												float: left;
												width: 100%;
												/*display: none;*/
											}
											#addons-modal.modal {
												z-index: 999;
											}
											.modal-backdrop {

												z-index: 998 !important;
											}
										</style>

										<div id="ajax_favorite_loddder" style="display:none;">
											<div align="center" style="vertical-align:middle;">
												<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
											</div>
										</div>

										<!-- jQuery 2.0.2 -->
										<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
										<!-- Bootstrap -->
										<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
										<!-- DATA TABES SCRIPT -->
										<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
										<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
										<!-- AdminLTE App -->
										<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
										<!-- page script -->

										<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>

										<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
										<script type="text/javascript">
											$(function() {
												$("#example1").dataTable();
												$('#example2').dataTable({
													"bPaginate": true,
													"bLengthChange": false,
													"bFilter": false,
													"bSort": true,
													"bInfo": true,
													"bAutoWidth": false
												});
											});

											$(document).on('change', '#pay_status', function(){
												$('#error_msg').hide();

												var select_val = $("#pay_status option:selected" ).val();

												if(select_val=='4' || select_val =='5')
												{
													$('#capture_reference').show();
												}
												else
												{
													$('#capture_reference').hide();
												}


											});

											$(document).on('click', '#update_status', function(){

												$('#error_msg').hide();

												var order_id = $('#order_id').val();
												var old_status = $('#old_status').val();
												var new_status = $("#pay_status option:selected" ).val();
 /*alert(order_id);
		alert(old_status);
		alert(new_status);
		return false;*/

		if((old_status=='1') && ((new_status=='2')||(new_status=='3')))
		{
			$('#error_msg').html('After Payment Done only select refund payment.');
			$('#error_msg').show();
		}
		else if((old_status=='2') && (new_status=='3'))
		{
			$('#error_msg').html('After Patrial Payment Done only select refund payment orpayment done.');
			$('#error_msg').show();
		}
		else
		{


			var form = $("#order_update");
			form.validate();
			var valid =	form.valid();

			if(valid)
			{

				$("#ajax_favorite_loddder").show();

				//var rest_id = $(this).attr('data-rest');
				//var promo_id = $(this).attr('data-promo');
				var frm_val = $('#order_update').serialize();
				$.ajax({
					type: "post",
					url : "{{url('/sub-admin/payment_status_update')}}",
					data: frm_val,
					success: function(msg) {

						$("#ajax_favorite_loddder").hide();
						//$('#Promotions').html(msg);
						//alert(msg);
						location.reload();
					}
				});
			}

		}



	});

											$.ajaxSetup({
												headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
											});
										</script>
										@stop
