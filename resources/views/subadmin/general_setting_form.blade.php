@extends('layouts.subadmin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			General Setting Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/sub-admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">General Setting Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">General Setting Form</h3>
				</div><!-- /.box-header -->

				@if(Session::has('error'))
				<div class="alert alert-danger alert-dismissable">
					{{Session::get('error')}}
				</div>
				@endif

				@if(Session::has('Succes'))
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					{{Session::get('Succes')}}
				</div>
				@endif

				<?php
				echo "<pre>";
				print_r($admin_detail);
				echo "</pre>";

				?>
				<!-- form start -->

				<form  role="form" enctype="multipart/form-data" method="POST" id="cuisine_frm" action="{{ url('/sub-admin/update_setting') }}">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Show Restaurant Data</label>
							<input type="radio" name="show_data" value="0"  @if($admin_detail[0]->show_data==0) checked="checked" @endif> All (Google + Registered)
							<input type="radio" name="show_data" value="1"  @if($admin_detail[0]->show_data==1) checked="checked" @endif> Only Registered Restaurant
						</div>

					</div> <!-- /.box-body -->

					<div class="box-footer">
						<input type="button" class="btn btn-primary" value="Submit" onClick="check_email()" />


						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>

			</div><!-- /.box -->


		</div>


	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
	function check_email()
	{

		var form = $("#cuisine_frm");
		form.validate();
		var valid =	form.valid();

		if(valid){
			$(form).submit();
			return true;
		}
		else
		{
			return false;
		}
	}
</script>
@stop