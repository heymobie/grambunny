@extends('layouts.admin')
@section('content')
<style>
.divition, .sub-drage
{
	border: 1px solid #666666;
	overflow:hidden;
}
.sub-drage
{
	margin-bottom: 6px;
	background: #cccccc;
	position:relative;
}
.sub-drage a.menu_edit_link {
	left: 4px;
	position: absolute;
	top: 6px;
	color:#000000;
}
.sub-drage p a {
	display: block;
	padding: 6px 0;
	color:#000;
}
.sub-drage.selected
{
	border:1px solid #3c8dbc;
	background: #666666;
	color: #fff !important;
}
.sub-drage.selected p a
{
	color: #fff !important;
}
.sub-drage p
{
	border-left:1px solid #666666;
	margin:0 0 0 20px;
	padding-left: 5px;
}
.drage
{
	overflow:hidden;
}
.col-sm-4.border
{
	border-right: 1px solid #ccc;
}
.col-sm-8.border {
	border-left: 1px solid #ccc;
}
.my-panel-data .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
{
	background: #3c8dbc;
	color: #fff;
}
.my-panel-data .nav-tabs>li
{
	margin: 0;
}
.my-panel-data .nav-tabs>li>a
{
	border-radius: 4px;
}
.my-panel-data .nav-tabs
{
	border:  none;
}
.my-panel-data .panel-heading
{
	padding: 12px;
	background-color: transparent !important;
	border: none;
}
.my-panel-data .panel-body {
	border-top: 2px solid #3c8dbc;
}
.my-panel-data .panel
{
	border: none;
	border-radius: 0;
	box-shadow: none;
}
.check_input_service .icheckbox_minimal.checked.disabled {
	background-position: -40px 0;
}
</style>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Area Detail Page
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Area Detail Page</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Area Detail Page</h3>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive">
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message')}}
					</div>
					@endif
					<table style="border:1px solid #666666; width:100%">
						<thead>
							<tr>
								<td style="vertical-align:top" >
									<table  width="100%" cellpadding="5" cellspacing="5">
										<tr>
											<td> Area Name:</td>
											<td><b> {{$Areadetail[0]->area_name}}</b></td>
										</tr>
										<tr>
											<td>Rout Name:</td>
											<td>
												@if($Areadetail[0]->route)
												{{$Areadetail[0]->route }}
												@else
												N/A
												@endif
											</td>
										</tr>
										<tr>
											<td>City:</td>
											<td> {{$Areadetail[0]->area_city }}</td>
										</tr>
									</table>
								</td>
								<td >
									<table  width="100%" cellpadding="5" cellspacing="5" >
										<tr>
											<td>State:</td>
											<td>{{$Areadetail[0]->area_state }} </td>
										</tr>
										<tr>
											<td>Country:</td>
											<td>{{$Areadetail[0]->area_country }} </td>
										</tr>
										<tr>
											<td>Zipcode:</td>
											<td>
												@if($Areadetail[0]->zipcode)
												{{$Areadetail[0]->zipcode }}
												@else
												N/A
												@endif
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<!-- <td>Status:

									<span style="padding-left:50px; font-weight:bold;">

										Active


									</span>

								</td> -->
								<td align="right">
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<div>
					</div>
				</div>
			</div><!-- /.box -->
			<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js_bottom')
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
@stop