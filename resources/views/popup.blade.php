@extends('layouts.app')



@section("other_css")



<meta name="_token" content="{!! csrf_token() !!}"/>

<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">

@stop

@section('content')

<div style="min-height: 600px;"></div>



<div class="modal fade" id="confirm_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal"></button>

		<h4 class="modal-title choice" id="show_popup_title_addon">Order Confirmation</h4>

      </div>

      <div class="modal-body popup-ctn">

	  		<p id="alert_confirm_msg">

				{{ $msg }}





			</p>

			<form name="confirm_order" >	  			

				<p>

					<button type="button" class="get_conf_val btn-default" id="ok">Ok</button>

				

				<?php if(!empty($rest_detail)){ ?>

				

					<button type="button" class="get_conf_val btn-default" id="print" onclick="myPrint();">Print</button>

				

			<?php }?>

				</p>

			</form>

      </div>

      <?php if(!empty($rest_detail)){ ?>

	      <div id="print_div">

		      <div style="display: none;" id="hidden_print_div">



					<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

					    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

					        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto 0;border:1px solid #e2e2e2;overflow:hidden;background: url(https://www.grambunny.com/design/front/img/resto.jpg);background-position:center;background-size:cover;text-align: center;color: #fff;">



					            <div style="padding:30px;border:1px solid #fff;background:rgba(31, 0, 0, 0.72);">

					                

					                <div style="border-bottom:2px solid #d2d2d2;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

					                    <h2 style="margin:0 0;">Hi <?php if(isset($rest_detail[0]->rest_name)){echo $rest_detail[0]->rest_name;}else{}?></h2>

					                </div>



					                <div style=" display:block; overflow:hidden">

					                    <p style=" margin:0 0 8px 0">&nbsp;</p>

					                    <p>Your have received order. Order no. is <strong>(<?php if(isset($order_detail[0]->order_id)){echo  $order_detail[0]->order_id;}else{}?>)(<?php if(isset($order_detail[0]->order_uniqueid)){echo $order_detail[0]->order_uniqueid;}else{}?>)</strong>. Please click on the confirm button to confirm this order.</p>

					                </div>

					                <div>

					                    <a href="https://www.grambunny.com/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">Visit site : - grambunny</a>

					                </div>

					            </div>

					        </div>        

							<div style="max-width:550px;width:85%;padding:30px;margin:0 auto 30px;border:1px solid #e81e2a;background: #e81e2a">

					        	<div style="background: #fff;padding: 15px;">

					        		<h2 style="text-align:center;"><?php if(isset($rest_detail[0]->rest_name)){echo $rest_detail[0]->rest_name;}else{}?></h2>

					        		    <p><strong>Name</strong><span style="float:right;"><?php echo $order_name;?></span></p>

									<p>

										<strong>Email Id</strong><span style="float:right;"><?php if(isset($order_email)){echo $order_email;}else{}?></span>

									</p>

									<p><strong>Contact Number</strong><span style="float:right;"><?php if(isset($order_tel)){echo $order_tel;}else{}?></span>

									</p>

									<p>

										<strong>Order Address</strong><span style="float:right;"><?php if(isset($order_address)){echo $order_address;}else{}?></span>

									</p>

					        		<p><strong>Order Instructions</strong> <span><?php echo $order_detail[0]->order_instruction;?></span></p>

					        		<p><strong>Payment Method <br> </strong> <span><?php echo $order_detail[0]->order_pmt_method;?></span></p>

									

									<?php $cart_price = 0;

									$cart = json_decode($order_detail[0]->order_carditem);

									?>

									<?php 

									if(isset($cart) && (count($cart)))

									{

										

										foreach($cart as $item)

										{

											 

											$cart_price = $cart_price+$item->price;

											?>

											<p>

												<strong><?php echo $item->qty; ?>x</strong> <?php echo $item->name;?> </strong>

												<span style="float:right;">$<?php echo number_format($item->price,2);?></span>

											</p>	

											

											<?php 

											if(!empty($item->options->addon_data)&&(count($item->options->addon_data)>0)){

											 	foreach($item->options->addon_data as $addon){

													

													$cart_price = $cart_price+$addon->price;

											?>

													<p>

														<strong><?php echo $addon->name; ?></strong>

														<span style="float:right;"><?php if($addon->price>0){

														echo "$".number_format($addon->price,2);}?></span>

													</p>

											<?php 

												} ?>

												

											<?php 

											}

										}

									}?>



									<p>

										<strong> Subtotal  </strong>

										<span style="float:right;"><?php echo $order_detail[0]->order_subtotal_amt;?></span>

					                </p>

									

									

									<?php if($order_detail[0]->order_promo_cal>0){?>

								

					               <p>

										<strong>Discount</strong>

										<span style="float:right;">-$<?php echo $order_detail[0]->order_promo_cal,2;?></span>

					                </p>

									

									<?php } ?>

									

									

							

									<?php if($order_detail[0]->order_service_tax>0){?>

								

					                <p>

										<strong> Sales Tax </strong>

										<span style="float:right;">$<?php echo $order_detail[0]->order_service_tax;?></span>

					                </p>

									

									<?php }?>



									<?php if($order_detail[0]->order_remaning_delivery>0){ ?>

									

										

									<p>  

										<strong> $<?php echo $order_detail[0]->order_min_delivery;?> min </strong>

										<span> Remaining<span  style="float:right;">$<?php echo $order_detail[0]->order_remaning_delivery;?></span>

										

					                </p>

									<?php  } ?>



							

									

									<p>

										<strong> TOTAL</strong>

										<span style="float:right;">$<?php echo $order_detail[0]->order_total;?></span>

									</p>



									<p>

										<strong> Tip Amount</strong>

										<span style="float:right;">$<?php echo $order_detail[0]->order_tip;?></span>

									</p>



									<p>

										<strong> GRAND TOTAL</strong>

										<span style="float:right;">$<?php echo $order_detail[0]->order_tip+ $order_detail[0]->order_total;?></span>

									</p>

									

					        	</div>

					        </div>

					    </div>

					</body>



		      </div>

		  </div>

	<?php } ?>

    </div>

  </div>

</div>





<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script> 

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script> 

<script src="{{ url('/') }}/design/front/js/functions.js"></script> 

<script src="{{ url('/') }}/design/front/js/validate.js"></script> 



<!-- SPECIFIC SCRIPTS --> 

<script src="{{ url('/') }}/design/front/js/custom.js"></script> 

<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> 

<!--<script src="{{ url('/') }}/design/front/js/bootstrap-select.js" /></script>-->

<script>



	$(document).ready(function(){

		$('#confirm_model').modal('show');

	});

    $('#ok').click(function(){

    	window.location.href='/home';

    });

    $('#confirm_model').modal({backdrop: 'static', keyboard: false})  

</script> 



<script>



	function myPrint() {

        var myPrintContent = document.getElementById('print_div');

        var windowUrl = 'https://www.grambunny.com/';

        var windowName = 'grambunny';

        var myPrintWindow = window.open(windowUrl, windowName, 'left=300,top=100,width=400,height=400');

        myPrintWindow.document.write(myPrintContent.innerHTML);

        myPrintWindow.document.getElementById('hidden_print_div').style.display='block';

        myPrintWindow.document.close();

        myPrintWindow.focus();

        myPrintWindow.print();

        myPrintWindow.close();    

        return false;

    } 

   </script> 

