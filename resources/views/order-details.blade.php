@extends("layouts.grambunny")
@section("styles")
{{-- styles goes here --}}
<style type="text/css">
   table {
      width: 100%;
   }

 td, th {
    text-align: left;
    padding: 4px 9px;
    font-size: 13px;
}

   .parallax-window#short {
      height: 230px;
      min-height: inherit;
      background: 0 0;
      position: relative;
      margin-top: 0px;
   }

   section.parallax-window {
      overflow: hidden;
      position: relative;
      width: 100%;
      background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
      background-attachment: fixed;
      background-repeat: no-repeat;
      background-position: top center;
      background-size: cover;
   }

   #sub_content {
      display: table-cell;
      padding: 50px 0 0;
      font-size: 16px;
   }

   #sub_content h1 {
      margin: 0 0 10px;
      font-size: 28px;
      font-weight: 300;
      color: #F5F0E3;
      text-transform: capitalize;
   }

   #short #subheader {
      height: 230px;
      color: #F5F0E3;
      text-align: center;
      display: table;
      width: 100%;
   }

   div#subheader {
      color: #F5F0E3;
      text-align: center;
      display: table;
      width: 100%;
      height: 380px;
   }

   ul.brad-home {
      padding: 0;
      margin: 0;
      text-align: center;
   }

   .brad-home li {
      display: inline-block;
      list-style: none;
      padding: 5px 10px;
      font-size: 12px;
   }

   #subheader a {
      color: #fff;
   }

   .myOrderActnBox {
      margin-top: 20px;
   }

   .myOrderDtelBtn a {
      border: 1px solid #231f20;
      background-color: #fff;
      color: #231f20;
      font-weight: 700;
      border-radius: 25px;
      padding: 8px 15px;
      min-width: 100px;
      cursor: pointer;
      text-decoration: none;
   }

   .myOrderDtelBtn a:hover {
      background-color: #231f20;
      color: #fff;
   }

   .deepmd {
      width: 100%;
   }

   .deepmd strong {
      float: right;
   }

   .deepon {
      font-size: 14px;
   }

   .table-responsive {
      border: unset;
      padding: 5px;
      background-color: #fff;
   }

  td.lcolrs {
    text-align-last: end;
   }

  td.lcolcen{ text-align: left; }

  .deeptbl label {
    font-weight: normal !important;
}

table.deeptbl th{
  border-bottom: 1px solid #ccc;
}
.right_side_order .myOrderDtel{
  border-bottom: none !important;
}

</style>
@endsection
@section("content")
{{-- content goes here --}}
<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">
   <div id="subheader">
      <div id="sub_content" class="animated zoomIn">
         <h1><span class="restaunt_countrt">
          <?php if(isset($ps_list[0]->type) == 3 ) {
               echo "Event Detail";
            }
            else {
               echo "Order Detail";
            }

            ?>
       </span></h1>
         <div id="position">
            <div class="container">
               <ul class="brad-home">
                  <li><a href="https://www.grambunny.com/home">Home</a></li>
                  <li>My Order Detail </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="myOrderSec section-full">
   <div class="container">
      <div class="row">
         @if(Session::has('message'))
         <div class="box-body table-responsive">
            <div class="alert alert-success alert-dismissable">
               <i class="fa fa-check"></i>
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
               {{Session::get('message')}}
            </div>
         </div>
         @endif
         <div class="col-md-12">
            <div class="myOdrersBox myOdrersDetailBox">
               <!-- <h2>Order detail</h2> -->
               <div class="row">
                  <div class="col-md-4">
                     <div class="myOrder order-detail-page">
                        <?php foreach ($ps_list as $key => $value) {
                           $pservice = DB::table('product_service')->where('id', '=', $value->ps_id)->first();
                           $order_ps = DB::table('orders_ps')->where('order_id','=',$value->order_id)->first();
                          // print_r($order_ps);
                        ?>
                           <?php //if (!empty($pservice)) { ?>
                              <div class="row">
                                 <div class="col-md-4 mb_wdt_left">
                                    <div class="myOrderImg">
                                       <?php if (!empty($pservice->image)) { ?>
                                          <img src="{{ url('/public/uploads/product/'.$pservice->image) }}">
                                       <?php }else{ ?>
                                         <img src="{{ url('/public/uploads/product/'.$order_ps->image) }}">

                                     <?php  } ?>
                                    </div>
                                 </div>
                                 <div class="col-md-8 mbwdt-right">
                                    <div class="order_d_extra">
                                       <div class="myOrderName deepon heading-prod">
                                          <?php if (!empty($pservice->name)) { ?>
                                             <strong>{{$pservice->name}}</strong>
                                          <?php }else{ ?>
                                 <strong>{{$order_ps->name}}</strong>
                              <?php }?>
                                       </div>
                                       <div class="myOrderDtel deepmd">
                                          <label>Price</label>
                                          <?php if (!empty($pservice->price)) { ?>
                                             <strong>${{$pservice->price}}</strong>
                                           <?php }else{ ?>
                                 <strong>{{$order_ps->price}}</strong>
                              <?php }?>
                                       </div>
                                       <div class="myOrderDtel deepmd">
                                          <label>Quantity</label>
                                          <strong>{{$value->ps_qty}}</strong>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                        <?php //}
                        } ?>
                     </div>


<?php
if ($pservice->type == 3 && count($multiple_order_detail) > 0) {
   ?>
      <div class="myOrder order-detail-page">
         <div class="row">
            <div class="col-md-6 mb_wdt_left">
               <div class="myOrderImg">
                  <b>Bar Code</b>
               </div>
            </div>
            <div class="col-md-6 mbwdt-right">
               <div class="order_d_extra">
                        <div class="myOrderDtel deepmd">
                           <label><b>Ticket Id</b></label>
                     </div>
               </div>
            </div>
         </div>
         <?php foreach ($multiple_order_detail as $value) {

         if(!empty($value)){    ?>
         <div class="row">
            <div class="col-md-6 mb_wdt_left">
               <div class="myOrderImg">
                  <?php if(!empty($value->ticket_qrcode)){ ?>
                     <img src="{{ $value->ticket_qrcode }}">
                  <?php } ?>
               </div>
            </div>
            <div class="col-md-6 mbwdt-right">
               <div class="order_d_extra">
                  <div class="myOrderDtel deepmd">
                     <label>{{ $value->ticket_id }}</label>
                  </div>
               </div>

               <div class="order_d_extra">
                  <div class="myOrderDtel deepmd">
                     <label>
                        @if($value->ticketuse_status==0) Un-used @endif
                        @if($value->ticketuse_status==1) Used @endif
                     </label>
                  </div>
               </div>

            </div>
         </div>
         <?php } } ?>
      </div>
      <?php
   }?>

    </div>
                  <div class="col-md-8">
                     <div class="myOrder right_side_order ">
                        <div class="myOrderDtelBox">
                           <div class="myOrderID">
                              <label>Order ID
                                 <strong>#{{$order_detail->order_id}}</strong>
                              </label>
                           </div>
                           <div class="row mt-4">
                             <div class="col-md-6">
                           <div class="myOrderDtel">
                              <label>Order By</label>
                              <strong>{{$order_detail->first_name}} {{$order_detail->last_name}}</strong>
                              <strong>{{$order_detail->mobile_no}}</strong>
                              <strong>{{$order_detail->email}} &nbsp;</strong>
                           </div></div>
                            <div class="col-md-6">
                            <?php if($pservice->type != 3) { ?>
                            <div class="myOrderDtel">
                              <label>Billing Address</label>
                              <strong>{{$order_detail->address}},{{$order_detail->city}},{{$order_detail->state}},{{$order_detail->zip}} </strong>
                            </div>
                            <?php } ?>

                              <div class="myOrderDtel">

                              <?php if($pservice->type == 3) { ?>
                              <label>Venue Address</label>

                              <strong>{{$pservice->venue_address}}

                              </strong>

                           <?php }else { ?>

                          <?php if(!empty($order_detail->addressd)){ ?>

                              <label>Delivery Address</label>
                              <strong>{{$order_detail->addressd}},{{$order_detail->cityd}},{{$order_detail->stated}},{{$order_detail->zipd}} </strong>

                        <?php }else{?>

                              <label>Pick Up Address</label>
                              <strong>{{$vendor_order->market_area}}</strong>


                         <?php }} ?>

                         </div>

                          </div>
                       </div>
                           <div class="myOrderDtel">
                              <label>Order Status
                                 <strong><span class="green">
                                    <?php if($pservice->type == 3) { ?>
                                       @if($order_detail->status==4) Complete @endif
                                       <?php }else{ ?>
                                       @if($order_detail->status==0) Pending @endif
                                       @if($order_detail->status==1) Accept @endif
                                       @if($order_detail->status==2) Cancelled @endif
                                       @if($order_detail->status==3) On the way @endif
                                       @if($order_detail->status==4) Delivered @endif
                                       @if($order_detail->status==5) Requested for return @endif
                                       @if($order_detail->status==6) Return request accepted @endif
                                       @if($order_detail->status==7) Return request declined @endif
                                       <?php } ?>
                                    </span></strong></label>
                           </div>
                           <?php if (!empty($order_detail->cancel_reason)) { ?>
                              <div class="myOrderDtel mb-4">
                                 <label>Cancel Reason</label>
                                 <strong>{{$order_detail->cancel_reason}} &nbsp;</strong>
                              </div>
                           <?php } ?>

                            <br>  <br>
                      <table class="deeptbl">
                        <tr>
                        <th colspan="5" style=" width: 40%;"><?php if($pservice->type == 3) { ?>Events Name<?php }else{?>Product Name<?php } ?></th>
                        <th style="text-align: center;">Unit Price</th>
                        <th style="text-align: right;">Qty</th>
                        <th style="text-align: right; width: 20%;">Total</th>
                        </tr>
                         <?php $subtotal = 0;?>

                                 <?php foreach ($ps_list as $key => $value) {
                                    $pservice = DB::table('product_service')->where('id', '=', $value->ps_id)->first();
                                   $order_ps = DB::table('orders_ps')->where('order_id','=',$value->order_id)->first();
                                    ?>

                                    <tr>

                                       <td class="lcolcen" colspan="5">
                                          <p><?php if (!empty($pservice->name)) { ?>
                                                {{$pservice->name}}
                                           <?php }else{ ?>
                                 {{$order_ps->name}}
                              <?php }?>
                                          </p>
                                       </td>
                                       <td class="lcolcen" style="text-align: center;">
                                          <p>
                                             <?php if (!empty($pservice->price)) { ?>
                                                ${{$pservice->price}}
                                            <?php }else{ ?>
                                 ${{$order_ps->price}}
                              <?php }?>
                                          </p>
                                           <p>
                         <?php if(!empty($pservice->price)){ $total_pp =$pservice->price*$value->ps_qty;}else{$total_pp =$order_ps->price*$value->ps_qty;} ?>
                           </p>
                                       </td>
                                        <td class="lcolcen" style="text-align: right;">
                                          <p>{{$value->ps_qty}}</p>
                                       </td>
                                    <td class="lcolrs">
                           <p>${{ number_format($total_pp, 2) }}</p>
                        </td>

                     </tr>

                     <?php $subtotal = $subtotal+$total_pp?>
                      <?php } ?>
                                 <!-- <tr>
                                    <td colspan="7" class="lcolrs"><b><?php if($pservice->type == 3) { ?>Ticket Total<?php }else{?>Product Total<?php } ?></b></td>
                                    <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->sub_total, 2) }}</span></td>
                                 </tr> -->

<?php if($order_detail->product_type == 3){  ?>

<tr>
<td colspan="7" class="lcolrs"> <label>Ticket Fee :</label>
<td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->ticket_fee * $value->ps_qty, 2) }}</span></td>
</tr>

<tr>
<td colspan="7" class="lcolrs"> <label>Ticket Service Fee :</label>
<td colspan="1" class="lcolrs"><span>${{number_format($order_detail->ticket_service_fee * $value->ps_qty, 2) }}</span></td>
</tr>

<?php } else { ?>

<tr>
<td colspan="7" class="lcolrs"> <label>Delivery Fee</label>
<td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->delivery_fee, 2) }}</span></td>
</tr>

<?php  } ?>



                                  <tr>
                                  <td colspan="7" class="lcolrs"> <label>Discount</label>
                                  </td>
                                   <td colspan="1" class="lcolrs"><span>-${{ number_format($order_detail->promo_amount, 2) }}</span></td>
                                 </tr>

                                 <tr>
                                 <td colspan="7" class="lcolrs"> <b>Subtotal</b></td>
                                    <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->sub_total, 2) }}</span></td>

                                 </tr>

                             <?php if($order_detail->product_type != 3){  ?>

                                 <tr>
                                     <td colspan="7" class="lcolrs" style="border-top:1px solid #ccc;"> <label>Excise Tax of Subtotal({{$vendor_order->excise_tax_percent}}%)</label>
                                    </td>
                                     <td colspan="1" class="lcolrs" style="border-top:1px solid #ccc;"><span>${{ number_format($order_detail->excise_tax, 2)}}</span></td>
                                 </tr>

                                 <tr>

                         <td colspan="7" class="lcolrs"> <label>City Tax of Subtotal({{$vendor_order->city_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->city_tax, 2)}}</span></td>
                     </tr>

                   <?php } ?>

                     <tr>
                         <td colspan="7" class="lcolrs"> <label>Sales Tax of Subtotal ({{$vendor_order->sales_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->service_tax, 2)}}</span></td>
                     </tr>
                           <tr>
                           <td colspan="7" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"><b>Total Paid Amount</b></td>
                           <td class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"><span>${{ number_format($order_detail->total, 2) }}</span></td>
                                 </tr>
                                 <tr>
                                    <td colspan="7" class="lcolrs"> <label>Payment Type</label>

                                    </td>
                                    <td class="lcolrs"><span>{{$order_detail->payment_method}}</span></td>
                                 </tr>
                                 <tr>

                                    <td colspan="7" class="lcolrs"> <label>TransactionID</label>
                                    </td>
                                    <td class="lcolrs"><span>{{$order_detail->txn_id}}</span></td>
                                 </tr>
                                 <tr>

                                    <td colspan="7" class="lcolrs"> <label>Payment Status</label>
                                    </td>
                                    <td class="lcolrs"><span>{{$order_detail->pay_status}}</span></td>
                                 </tr>


                                 <?php if (in_array($order_detail->status, array(5, 6, 7))) { ?>



                                 <?php } ?>


                                 <?php if (in_array($order_detail->status, array(6, 7)) && !empty($order_detail->return_reason_merchant)) { ?>


                                 <?php } ?>

                                 <?php if (!empty($vendor_order->commission_rate)) { ?>

                                 <?php } ?>


                              </table>

                           <?php if (in_array($order_detail->status, array(5, 6, 7))) { ?>
                              <div class="myOrderDtel">
                                 <label>Return reason</label>
                                 <strong>{{$order_detail->return_reason}}</strong>
                              </div>
                           <?php } ?>
                           <?php if (in_array($order_detail->status, array(6, 7)) && !empty($order_detail->return_reason_merchant)) { ?>
                              <div class="myOrderDtel">
                                 <label>Return reason merchant</label>
                                 <strong>{{$order_detail->return_reason_merchant}}</strong>
                              </div>
                           <?php } ?>
                           <div class="myOrderActnBox">
                              <div class="myOrderDtelBtn">

                              </div>
                           </div>
                           <div class="myOrderActnBox">
                              <div class="msg-box">
                                 <div id="message-box" style="display:none;">
                                    <i class="fa fa-check"></i>
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                    <span id="message"></span>
                                 </div>
                              </div>
                              <form accept-charset="UTF-8" id="return-order-form" action="javascript:void(0);" method="post" style="display: none;">
                                 {{csrf_field()}}
                                 <input id="order_id" name="order_id" type="hidden" value="{{$order_detail->id}}">
                                 <textarea style="width: 337px;" class="form-control animated" id="reason" name="reason" placeholder="Enter reason..." rows="3" required></textarea>
                                 <div class="text-right" style="margin-top:12px;float:left;">
                                    <button class="btn btn-success btn-lg" type="button" id="submit-request">Submit</button>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section("scripts")
{{-- scripts goes here --}}
<script type="text/javascript">
   $('#submit-request').on('click', function() {



      var order_id = $("#order_id").val();



      $.ajax({

         type: "post",

         headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
         },

         data: $("#return-order-form").serialize(),

         url: "{{url('/submit-return-request')}}",

         success: function(data) {



            if (data.ok) {

               location.reload();

            } else {

               var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message" style="float:left;">' + data.message + '</span></div>';

               $('.msg-box').html(html);

               $("#message-box").addClass("alert alert-danger alert-dismissable");

               $("#message").text(data.message);

            }

         }

      });

   });



   $('#return_order').on('click', function() {

      $("#return-order-form").css('display', 'block');

   });
</script>
@endsection