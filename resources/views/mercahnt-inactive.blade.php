@extends("layouts.grambunny")



@section("styles")

{{-- styles --}}

<style type="text/css">

  .parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

}

section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

}

#sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

}

#sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

}

#short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

}

div#subheader {

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

    height: 380px;

}

ul.brad-home {

    padding: 0;

    margin: 0;

    text-align: center;

}

.brad-home li {

    display: inline-block;

    list-style: none;

    padding: 5px 10px;

    font-size: 12px;

}

#subheader a {

    color: #fff;

}

</style>

@endsection



@section("content")





<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">

    <div id="subheader">

        <div id="sub_content" class="animated zoomIn">

          <h1><span class="restaunt_countrt">grambunny Merchant</h1>      

        </div>

    </div>

</section> 



<section class="support">

      <div class="container">



         <h1>Sorry, the merchant account is temporarily off line, please try back again soon.</h1>



      </div>

</section>









@endsection