@extends('layouts.app')



@section("other_css")



<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">

   	  <!-- Radio and check inputs -->

    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <link href="{{ url('/') }}/design/front/css/ion.rangeSlider.css" rel="stylesheet">

    <link href="{{ url('/') }}/design/front/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <!--[if lt IE 9]>

      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>

      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>

    <![endif]-->

	

	    

	<link href="{{ url('/') }}/design/css/darkbox.css" rel="stylesheet" type="text/css">	

 <style>

.thumb_strip img {

    width: 98px;

    height: 98px;

}

    </style>

	

	

  

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/ion.imageSlider.css" />

<!--<link rel="stylesheet" type="text/css" href="css/ion.imageSlider.metro.css" />-->

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/ion.imageSlider.minimal.css" />

<!--<link rel="stylesheet" type="text/css" href="css/ion.imageSlider.sexy.css" />-->

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/normalize.min.css" />



@stop

@section('content')





<!-- SubHeader =============================================== -->

<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_short.jpg" data-natural-width="1400" data-natural-height="350">

        <div id="subheader">

            <div id="sub_content">

			@if($total_vehicle)

                <h1 id="top_counter_msg">{{$total_vehicle}} results @if(!empty($surbur_name)) In {{$surbur_name}} @endif</h1>

			@endif		

                <!--<div><i class="icon_pin"></i></div>-->

            <!--<h6><a href="{{url('/')}}" class="btn_1" style="text-decoration:none;">Change Location</a></h6>-->

            </div><!-- End sub_content -->

        </div><!-- End subheader -->

    </section><!-- End section -->

<!-- End SubHeader ============================================ -->



     <div id="position">

        <div class="container">

            <ul>

                <li><a href="{{url('/')}}">Home</a></li>

                <li>View all Transport</li>

            </ul>

        </div>

    </div>

	<!-- Position -->





    <div class="collapse" id="collapseMap">

        <div id="map" class="map"></div>

    </div><!-- End Map -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">



        <div class="row">

            <div class="col-md-3"></div>

            <div class="col-md-9">

                <!--<div id="tools">-->

                <div>

                    <div class="row">



                        <div >

							<form name="home_search_data" id="home_search_data" method="get">

								<input type="hidden" name="surbur" id="surbur" value="{{$surbur_name}}" />

								<input type="hidden" name="pcode" id="pcode" value="{{$post_code}}" />

								<input type="hidden" name="loc_id" id="loc_id" value="{{$loc_id}}" />

								<input type="hidden" name="search_type" id="search_type1" value="{{$search_type}}" />

							</form>

							<?php //echo 'KKG'.$session_id = Session::getId();; ?>

							<div class="col-md-3 col-sm-3 col-xs-12">

	

								<p>

									<a class="btn_map"  href="{{url('/restaurant_listing')}}">Restaurants</a>

								</p>

							</div>

							

							<div class="col-md-3 col-sm-3 col-xs-12">

								<p>

									<a class="btn_map active" href="{{url('/transport_listing')}}">Vehicles</a>

								</p>

								

							</div>

            			</div>

                    </div>

                </div>

                <!--End tools -->

            </div>

        </div> 



        <div class="row" id="result_div">



            <div class="col-md-3 margin-top-minus-60">

				<div id="filters_col" class="listng_right_sidebr">

					<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters And Sorting<i id="collapseIcon" class="icon-minus-1 pull-right"></i></a>

					<form name="advance_search" id="advance_search" method="get">

					<input type="hidden" class="from_lable" name="from_lable" id="from_lable" value="{{$surbur_lable}}" />

					<input type="hidden" class="from_suburb" name="surbur" id="surbur" value="{{$surbur_name}}" />

					<input type="hidden" class="from_pincode" name="pcode" id="pcode" value="{{$post_code}}" />

					<input type="hidden" class="from_location_id" name="loc_id" id="loc_id" value="{{$loc_id}}" />

					<input type="hidden" class="from_search_type" name="search_type" id="search_type" value="{{$search_type}}" />

					

					

					

				<input type="hidden" class="to_suburb" name="to_suburb" id="to_surbur" value="{{$to_surbur_name}}" />

				<input type="hidden" class="to_pincode" name="to_pcode" id="to_pcode" value="{{$to_post_code}}" />

				<input type="hidden" class="to_location_id" name="to_loc_id" id="to_loc_id" value="{{$to_loc_id}}" />

				<input type="hidden" class="to_search_type" name="to_search_type" id="to_search_type" value="{{$to_search_type}}" />

					<input type="hidden" class="to_lable" name="to_lable" id="to_lable" value="{{$to_surbur_lable}}" />

					

					

					

<input type="hidden" name="on_date"  class="search_ondate" value="{{$ondate}}"  />

<input type="hidden" name="on_time"  class="search_ontime" value="{{$on_time}}" />

<input type="hidden" name="return_date" class="search_returndate" value="{{$return_date}}" />

<input type="hidden" name="return_time" class="search_returntime" value="{{$return_time}}" />

					

					<div class="collapse" id="collapseFilters">

					

					<div class="filter_type">

							<h6>Pick-up  location</h6>							

							<ul>

								<li>

							<input type="text" class="form-control  ui-autocomplete-input search_from" placeholder="Your Address or postal code" autocomplete="off" id="search_from" name="search_from" required="required" value="{{$surbur_lable}}">

								</li>

							</ul>						

							<ul>

								<li>Pick-up  Date & Time  </li>

							</ul>						

							<ul>

								<li>

								

							<input type="date" name="dt"  />

							<input type="text" name="on_date" id="datepicker_on" class="form-control datepicker" value="{{$ondate}}" required="required">

								

							<select class="form-control search_ontime" name="on_time" id="on_time"  required="required">

									<option value="">Select time</option>

									<?php

										$starttime = '7:00';  // your start time

							$endtime = '22:00';  // End time

							$duration = '15';  // split by 30 mins

							 

							

							$start_time    = strtotime ($starttime); //change to strtotime

							$end_time      = strtotime ($endtime); //change to strtotime

							 

							$add_mins  = $duration * 60;

							 

							while ($start_time <= $end_time) // loop between time

							{

								$dtime = date ("H:i", $start_time);

								$sdtval = str_replace(':','',$dtime);

								$select='';

								if($on_time==$dtime){$select='selected="selected"';}

							?>

							<option value="{{$sdtval}}"  <?php echo $select;?>>{{$dtime}}</option>

							<?php

							   $start_time += $add_mins; // to check endtie=me

							}

							?>

								</select>

								</li>

							</ul>

							<h6>To location</h6>							

							<ul>

								<li>

							<input type="text" class="form-control ui-autocomplete-input search_to" placeholder="Your Address or postal code" autocomplete="off" id="search_to" name="search_to" required="required" value="{{$to_surbur_lable}}">

								</li>

							</ul>						

							<ul>

								<li>To Date & Time </li>

							</ul>								

							<ul>

								<li>

							<input type="text" name="return_date" id="datepicker1" class="form-control datepicker1" value="{{$return_date}}" required="required">

								

								<span class="show_return_time">

								

								

								<?php 

								$starttime = '7:15';  // your start time

								if(trim($return_date)==trim($ondate))

								{

									$tt =  strtotime($on_time);

									$starttime = date ("H:i",($tt+(180*60)));

								}

								?>

							<select class="form-control search_returntime" name="return_time" id="return_time" required="required">

                    <option value="">Select time</option>

					<?php

						

			$endtime = '22:00';  // End time

			$duration = '15';  // split by 30 mins

			 

			

			$start_time    = strtotime ($starttime); //change to strtotime

			$end_time      = strtotime ($endtime); //change to strtotime

			 

			$add_mins  = $duration * 60;

			 

			while ($start_time <= $end_time) // loop between time

			{

				$dtime = date ("H:i", $start_time);

				$sdtval = str_replace(':','',$dtime);

				$select='';

				if($return_time==$dtime){$select='selected="selected"';}

			?>

			<option value="{{$sdtval}}" {{$select}}>{{$dtime}}</option>

			<?php

			   $start_time += $add_mins; // to check endtie=me

			}

			?>

				</select>

									</span>

								</li>

							</ul>	

							<ul>

								<li>

							 <input type="button" class="btn_full" id="add_filter_left" value="Filter" />

							 	</li>

							</ul>

					</div>

					

					

					

					

					@if(!empty($class_list))						

					

						<div class="filter_type">

							<h6>Vehicle Class</h6>

							<ul> 

								<li>

									 <div class="control-group islamic2">

										 <label>

											 <span class="control control--checkbox">

											

											 

											 <input type="checkbox" class="service_type_all" name="classType[]" id="vehicle_class-0" value="0" checked> 

											 

											 <div class="control__indicator"></div>

											 </span>

										 <!-- class="icheck"--> All Categories<!--<small>(49)</small>-->

										 </label>

									 </div>

								 </li>	

								@foreach($class_list as $C_list)	

								

								 <li>

									<div class="control-group islamic2">

										<label>

												 <span class="control control--checkbox"> 

									 <input type="checkbox" name="classType[]" class="service_type" id="vehicle_class-{{$C_list->vclass_id}}" value="{{$C_list->vclass_id}}" > 

											 <div class="control__indicator"></div>

												 </span>

										 <label>{{$C_list->vclass_name}}</label>

									 </label>

									</div>

								 </li>		

				  

								

								@endforeach

							</ul>

						</div>

					@endif 		

						

						<div class="filter_type">

							<!--<h6>Distance</h6>

							<input type="text" id="range" value="" name="range">-->

							<h6>Vehicle Category </h6>

							<ul>

							 <li>

							 <div class="control-group islamic2">

							 <label>

							 <span class="control control--checkbox">

							 <input type="checkbox" class="foodallclass" name="categoryType[]" id="categorytype-0" value="0" checked> 

							 

							 <div class="control__indicator"></div>

							 </span>

							 <!-- class="icheck"--> All Categories<!--<small>(49)</small>-->

							 </label>

							 </div>

							 </li>

							 

							@if(!empty($category_list))

											 @foreach($category_list as $Cat_list)	

				 <li>

				 <div class="control-group islamic2">

							 <label>

							 <span class="control control--checkbox"> 

				 <input type="checkbox" name="categoryType[]" class="foodclass" id="categorytype-{{$Cat_list->vcate_id}}" value="{{$Cat_list->vcate_id}}" > 

				 <div class="control__indicator"></div>

							 </span>

				 <label>{{$Cat_list->vcate_name}}</label>

				  </label>

							 </div>

				 </li>											 

																				

											 @endforeach

											@endif 	

							  

							</ul>

						</div>

						

						<div class="filter_type">

							<h6>Sort By:</h6>							

							<ul>

								<li>

									<select name="sort_by" id="sort_by"  class="form-control">

										<option value="1">Best match</option>

										<option value="2">Min Rates</option>

										<option value="3">Name A to Z</option>

										<option value="4">Name Z to A</option>

										<!--<option value="5">Review</option>-->

									</select>

								</li>

							   

							</ul>

						</div>

					</div><!--End collapse -->

					</form>

				</div><!--End filters col-->

			</div><!--End col-md -->



            <div class="col-md-9">

				<div class="lstng_resp" id="listing_transport">

				

			@if(count($vehicle_listing)>0)

				<div>

			@if(!empty($vehicle_listing))

				@foreach($vehicle_listing as $special_list)

				

				<div class="strip_list wow fadeIn" data-wow-delay="0.1s">

				@if($special_list->transport_classi!='5')

					<div class="ribbon_1">

					   <span class="left_head"> 

							@if($special_list->transport_classi=='1') Sponsored @endif

							@if($special_list->transport_classi=='2') Popular @endif

							@if($special_list->transport_classi=='3') Special @endif

							@if($special_list->transport_classi=='4') New @endif

							@if($special_list->transport_classi=='5') Standard @endif

						</span>

					</div>

				@endif	

					<div class="row">

						<div class="col-md-9 col-sm-9">

							<div class="desc">

								<div class="thumb_strip">

								<!--

								 <div id="owl-demo<?php //echo $special_list->vehicle_id;?>">

    <img src="{{ url('/') }}/img/zoom/zoom-small-01.jpg"

         data-darkbox="{{ url('/') }}/img/zoom/zoom-big-01.jpg"

         data-darkbox-group="one">

<img src="{{ url('/') }}/img/zoom/zoom-small-02.jpg"

         data-darkbox="{{ url('/') }}/img/zoom/zoom-big-02.jpg"

         data-darkbox-group="one">

<img src="{{ url('/') }}/img/zoom/zoom-small-01.jpg"

         data-darkbox="{{ url('/') }}img/zoom/zoom-big-01.jpg"

         data-darkbox-group="one">

<img src="{{ url('/') }}/img/zoom/zoom-small-02.jpg"

         data-darkbox="{{ url('/') }}/img/zoom/zoom-big-02.jpg"

         data-darkbox-group="one">

</div>-->



								<?php 

								$vehcile_img = DB::table('vehicle_img')

											->where('vehicle_id', '=' ,$special_list->vehicle_id)->get();	

								if(count($vehcile_img)>0)			

								{

									$counter = 1;

									?>	

										<div id="owl-demo-<?php echo $special_list->vehicle_id;?>">

									<?php foreach($vehcile_img as $img_list){

									

									$new_path = url('/') .'/uploads/vehicle/thumb/'.$img_list->imgPath;

									if(file_exists($new_path))

									{

										$new_path = url('/') .'/uploads/vehicle/'.$img_list->imgPath;

									}

									

									?>

											<img src="{{ $new_path }}" data-darkbox="{{ url('/') }}/uploads/vehicle/{{$img_list->imgPath}}" data-darkbox-group="one">	

									 

									 

										

								<?php }?>

											

							 

								</div>

									<?php

								}

								else

								{

								?>

									<img src="{{ url('/') }}/design/front/img/logo.png" data-darkbox="{{ url('/') }}/design/front/img/logo.png" data-darkbox-group="one">	

										

												

								<?php 

								}

								?>										

									

								</div>

								<div class="rating">

								   <?php 

								$avg_rating = '';				

								$count_review = 0 ;

								$count_avg_rating =0;

								$total_rating = DB::table('transport_review')	

											->select( DB::raw('SUM(re_rating) as rating'))	

											->where('re_transid', '=' ,$special_list->transport_id) 

											->where('re_vehicleid', '=' ,$special_list->vehicle_id) 

											->where('re_status', '=' ,'PUBLISHED')

											->orderBy('re_id', 'desc')						

											->get();	

											

								$count_review = DB::table('transport_review')	

											->select( DB::raw('SUM(re_rating) as rating'))	

											->where('re_transid', '=' ,$special_list->transport_id) 

											->where('re_vehicleid', '=' ,$special_list->vehicle_id) 

											->where('re_status', '=' ,'PUBLISHED')

											->orderBy('re_id', 'desc')						

											->count();	

								$count_avg_rating =	$total_rating[0]->rating;

									

								

			

								?>	

								@if(($count_review>0) && ($count_avg_rating>0))	

								<?php $avg_rating = round(($count_avg_rating/$count_review)/2); ?>

						   

							  @for ($x = 1; $x < 6; $x++)

								@if(($avg_rating>0) && ($avg_rating>=$x))

								<i class="icon_star voted"></i>

								@else

									<i class="icon_star"></i>

								@endif

							  @endfor  

							  (<small><a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->transport_suburb)).'/'.str_replace(' ','_',trim($special_list->vehicle_rego)).'/'.str_replace(' ','_',trim($special_list->vclass_name)).'/'.str_replace(' ','_',trim($special_list->model_name)).'/'.str_replace(' ','_',trim($special_list->vehicle_id))).'/review')}}">{{$count_review}} reviews</a></small>)

							@endif

							

							

							

								</div>

								<h3>{{$special_list->make_name.' - '.$special_list->model_name}}</h3>

								<div class="type">						

								{{$special_list->vclass_name}} 	- {{$special_list->vcate_name}}

								</div>

								<div class="location">

								

								<span class="opening">Maximum {{ $special_list->vehicle_pax}} Passengers</span>

								

								 @if(!empty($special_list->vehicle_minrate))

									  Minimum Rate: ${{$special_list->vehicle_minrate}}

								@endif

								</div>

								

							  

							<ul>	

								@if($special_list->vehicle_ac==1)

								<li>A/C<i class="icon_check_alt2 ok"></i></li>

								@endif	

								@if($special_list->vehicle_music==1)

								<li>Music<i class="icon_check_alt2 ok"></i></li>

								@endif	

								@if($special_list->vehicle_video==1)

								<li>Video<i class="icon_check_alt2 ok"></i></li>

								@endif			

								

							</ul>

							

							</div>

						</div>

						<div class="col-md-3 col-sm-3">

							<div class="go_to">

								<div>

								

					<form name="vehicle_info" id="vehicleinfo-{{$special_list->vehicle_id}}">

						<input type="hidden" name="vehicle_id" id="vehicle_id-{{$special_list->vehicle_id}}" value="{{$special_list->vehicle_id}}" />

						<input type="hidden" name="vehicle_from" id="vehicle_from-{{$special_list->vehicle_id}}" value="{{$surbur_name}}" />

						<input type="hidden" name="vehicle_to" id="vehicle_to-{{$special_list->vehicle_id}}" value="{{$to_surbur_name}}"  />

						<input type="hidden" name="vehicle_ontime" id="vehicle_ontime-{{$special_list->vehicle_id}}" value="{{$on_time}}"   />

						<input type="hidden" name="vehicle_ondate" id="vehicle_ondate-{{$special_list->vehicle_id}}" value="{{$ondate}}" />

						<input type="hidden" name="vehicle_returntime" id="vehicle_returntime-{{$special_list->vehicle_id}}" value="{{$return_time}}" />

						<input type="hidden" name="vehicle_returndate" id="vehicle_returndate-{{$special_list->vehicle_id}}" value="{{$return_date}}"  />

					</form>

								

									<?php /*?><a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->transport_suburb)).'/'.str_replace(' ','_',trim($special_list->vehicle_rego)).'/'.str_replace(' ','_',trim($special_list->vclass_name)).'/'.str_replace(' ','_',trim($special_list->model_name)).'/'.str_replace(' ','_',trim($special_list->vehicle_id))))}}" class="btn_1">View Rate</a><?php */?>

									<a href="javascript:void(0)" id="view_rate-{{$special_list->vehicle_id}}" data-vehicleid="{{$special_list->vehicle_id}}" class="btn_1">View Rate</a>

							

								</div>

							</div>

						</div>

					</div><!-- End row-->

				</div>

				

				<!-- End col-md-6-->

			

				@endforeach

			@else

				Restaurant Not Found!

			@endif

				

				</div>

				@else

				<center> Transport not found! </center>	

			@endif

					</div>

					

				@if($page_no>0)		

				<a href="javascript:void(0)" class="load_more_bt wow fadeIn" data-wow-delay="0.2s" id="trans_list_load-{{$page_no}}" data-nextpage="{{$page_no}}" >Load more...</a>

				@endif

			



				

			</div><!-- End col-md-9-->



        </div><!-- End row -->

    </div><!-- End container -->

    <!-- End Content =============================================== -->

@stop

@section('js_bottom')

 



<div class="modal fade" id="search_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" id="close_search" data-dismiss="modal">&times;</button>

	  		<h3>Search </h3>

      </div>

      <div class="modal-body popup-ctn">

	  		<form name="search_frm" id="search_frm" method="post">	

			<input type="hidden" name="select_vehicle" id="select_vehicle" value="" />

	

			

			  <div class="form-group">

				<label>From location</label>

				<input type="text" class="form-control  ui-autocomplete-input search_from" placeholder="Your Address or postal code" autocomplete="off" id="search_from" name="search_from" required="required" value="{{$surbur_lable}}">

			  </div>

			  <div class="form-group">

				<label>To location</label>

				

				<input type="text" class="form-control ui-autocomplete-input search_to" placeholder="Your Address or postal code" autocomplete="off" id="search_to" name="search_to" required="required" value="{{$to_surbur_lable}}">

			  </div>

			  <div class="form-group half-row">

				<label>On Date </label>

				<div class="col-md-12">

				<div class="date col-md-6" id="datetimepicker1">

                    <input type="text" name="on_date" id="datepicker_on" class="form-control datepicker" value="{{$ondate}}" required="required">

                   </div>

				   <div class="col-md-6">

				

				<select class="form-control search_ontime" name="on_time" id="on_time"  required="required">

                    <option value="">Select time</option>

					<?php

						$starttime = '7:00';  // your start time

			$endtime = '22:00';  // End time

			$duration = '15';  // split by 30 mins

			 

			

			$start_time    = strtotime ($starttime); //change to strtotime

			$end_time      = strtotime ($endtime); //change to strtotime

			 

			$add_mins  = $duration * 60;

			 

			while ($start_time <= $end_time) // loop between time

			{

				$dtime = date ("H:i", $start_time);

				$sdtval = str_replace(':','',$dtime);

			?>

			<option value="{{$sdtval}}" >{{$dtime}}</option>

			<?php

			   $start_time += $add_mins; // to check endtie=me

			}

			?>

				</select>

				

                    </div>

				<div id="del_error_msg" style="color:#FF0000; display:none"></div>

				</div>

			  </div>

			  <div class="form-group half-row">

				<label>Return Date </label>

				<div class="col-md-12">

				<div class="date col-md-6" id="datetimepicker1">

                    <input type="text" name="return_date" id="datepicker1" class="form-control datepicker1" value="{{$return_date}}" required="required">

                   </div>

				   <div class="col-md-6 show_return_time">

				

				<select class="form-control search_returntime" name="return_time" id="return_time" required="required">

                    <option value="">Select time</option>

					<?php

						$starttime = '7:15';  // your start time

			$endtime = '22:00';  // End time

			$duration = '15';  // split by 30 mins

			 

			

			$start_time    = strtotime ($starttime); //change to strtotime

			$end_time      = strtotime ($endtime); //change to strtotime

			 

			$add_mins  = $duration * 60;

			 

			while ($start_time <= $end_time) // loop between time

			{

				$dtime = date ("H:i", $start_time);

				$sdtval = str_replace(':','',$dtime);

			?>

			<option value="{{$sdtval}}" >{{$dtime}}</option>

			<?php

			   $start_time += $add_mins; // to check endtie=me

			}

			?>

				</select>

				

                    </div>

				<div id="del_error_msg" style="color:#FF0000; display:none"></div>

				</div>

			  </div>

			 

			   <div class="form-group">

			    <input type="button" class="btn_full" id="add_filter" value="Filter" /><!--disabled="disabled"-->

				

			  </div>

			</form>

			

		

      </div>

    </div>

  </div>

</div>

<style>



.ui-widget.ui-widget-content {

    border: 1px solid #c5c5c5;

    border-radius: 10px 0 0 10px;

   

    height: 250px;

    overflow-y: scroll;

    padding: 8px;

    z-index: 2147483647 !important;

}



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>





<div class="modal fade" id="confirm_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body popup-ctn">

	  		<p>

				<?php if(!empty($surbur_name)){?>

				Results are based on <?php echo $surbur_name;?> only.<?php }else{?> All Results listout. <?php } ?> Please enter information for more accurate results.				

			</p>	

			<p>

				<button type="button" class="get_conf_val btn-default" data-val="1">OK</button>

				<button type="button" class="get_conf_val btn-primary" data-val="0">Cancel</button>

			</p>

      </div>

    </div>

  </div>

</div>



<div class="modal fade" id="vehicle_notmeet_model" role="dialog">

  <div class="modal-dialog"> 

	<div class="modal-content">

      <div class="modal-header">

        <button class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body popup-ctn">

	  		<p>Vehicle you selected does not meet your search. Please select one from the filtered results.	</p>	

			<p>&nbsp;</p>

      </div>

    </div>

  </div>

</div>





<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- COMMON SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>

<script src="{{ url('/') }}/design/front/js/functions.js"></script>

<script src="{{ url('/') }}/design/front/assets/validate.js"></script>



<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/cat_nav_mobile.js"></script>

<script>$('#cat_nav').mobileMenu();</script>

<script src="https://maps.googleapis.com/maps/api/js"></script>

<script src="{{ url('/') }}/design/front/js/map.js"></script>

<script src="{{ url('/') }}/design/front/js/infobox.js"></script>

<script src="{{ url('/') }}/design/front/js/ion.rangeSlider.js"></script>

<!--<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script>--> 



 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 

<script>



 

//var d =  new Date();

   

//var dateToday =  d.getFullYear() + "-" + (d.getMonth()+1) + "-" +( d.getDate()+1) + " " + d.getHours() + ":" + d.getMinutes() + ":00";





//var dateToday =  ( d.getDate()+1) + "-" + (d.getMonth()+1) + "-" + d.getFullYear()+ " " + d.getHours() + ":" + d.getMinutes() + ":00";















//dateFormat: 'yy-mm-dd',





var today = new Date();

var dateToday = new Date(today.getTime() + 24 * 60 * 60 * 1000);





$( ".datepicker" ).datepicker({

 dateFormat: 'dd-mm-yy',

 minDate: dateToday,

 onSelect: function(selected) {		

 

 	

 $('.search_ondate').val($('.datepicker').val());

 $('.datepicker').val($('.datepicker').val());

 		 

	  var date1 = $('.datepicker').datepicker('getDate', '+1d'); 

		  //date1.setDate(date1.getDate()+1);  

	 

	  var date2 = $('.datepicker').datepicker('getDate', '+1d'); 

		  date2.setDate(date2.getDate()+30); 



	   $(".datepicker1").datepicker("option","minDate", date1);

	   $(".datepicker1").datepicker("option","maxDate", date2);

	 },

 

});

$( ".datepicker1" ).datepicker({

		 

 dateFormat: 'dd-mm-yy',

	   onSelect: function(selected) {

	   	   $('.search_returntime').val('');

 		$('.search_returndate').val($('.datepicker1').val());

		

 $('.datepicker1').val($('.datepicker1').val());

 

 

	   var date1 = $('.datepicker1').datepicker('getDate', '+1d'); 

	   

	   	if ($('.datepicker').val() == $('.datepicker1').val()) {

				    

		 		/* var frm_val ='type=same&ontime='+$('#on_time').val();	*/

			

			 var frm_val ='type=same&ontime='+$('.search_ontime').val();

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					$(".show_return_time").html(msg);

						

					}

				});

		

                

            }

			else

			{

				    

		 		 var frm_val ='type=diff&ontime=0700';	

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					

					$(".show_return_time").html(msg);

						

					}

				});

				

			}	 

	  // alert($('#datepicker').val());

	  // alert($('#datepicker1').val());

	   

	   $(".datepicker").datepicker("option","maxDate", selected);

	   

	},

 minDate: dateToday,

 

});



$(document).on('change', '#return_time', function(){



	value = $(this).find('option:selected').val();

	

	 $('.search_returntime').val(value);

 

});

//$(document).on('change', '#on_time', function(){

$(document).on('change', '.search_ontime', function(){





	$('.search_returntime').val();

	

	value = $(this).find('option:selected').val();

	

	$('.search_ontime').val(value);

	

	if( ($('.datepicker').val()!='')  && ($('.datepicker1').val()!=''))

	{

	 	if ($('.datepicker').val() == $('.datepicker1').val()) {

				    

		 		/* var frm_val ='type=same&ontime='+$('#on_time').val();	*/

			

			 var frm_val ='type=same&ontime='+$('.search_ontime').val();

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					$(".show_return_time").html(msg);

						

					}

				});

		

                

            }

			else

			{

				    

		 		 var frm_val ='type=diff&ontime=0700';	

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					

					$(".show_return_time").html(msg);

						

					}

				});

				

			}

 	}

});





	$(function () {

		'use strict';

		$("#range").ionRangeSlider({

			hide_min_max: true,

			keyboard: true,

			min: 0,

			max: 15,

			from: 0,

			to: 5,

			type: 'double',

			step: 1,

			prefix: "Km ",

			grid: true

		});

	});

	

	

	

	

</script>



<script type="text/javascript">

	$(document).ready(function () {

	

	

	

<?php if((empty($on_time) || empty($return_time) || empty($ondate) || empty($return_date))  && (!Session::has('login_message_error'))){?>

	

$('#search_model').modal('show');

<?php }?>

		//<i id="collapseIcon" class="icon-plus-1 pull-right"></i>



		$("#filters_col_bt").click(function () {

			//if (!$(this).hasClass("collapsed")) {

				if ($("#collapseIcon").hasClass("icon-plus-1")) {

					$("#collapseIcon").removeClass("icon-plus-1");

					$("#collapseIcon").addClass("icon-minus-1");

				}

				else {

					$("#collapseIcon").removeClass("icon-minus-1");

					$("#collapseIcon").addClass("icon-plus-1");

				}

			//}

			//else {

			//    $("#collapseIcon")

			//}

		});

	});

$(document).on('click', '[id^="trans_list_load-"]', function(){ 



$("#ajax_favorite_loddder").show();	





	

	

  

		var page = $(this).attr('data-nextpage');

		

			$("#trans_list_load-"+page).hide();

			

		var frm_val = $('#advance_search').serialize();		

		$.ajax({

	/*	//type: "post",

		//dataType: "json",

		//url:  '?page=' + page+'&'+'cartType='+type_service+'&'+cusin_val,*/

		

		dataType: "json",

		url:  '?page=' + page+'&'+frm_val,	

		success: function(msg) {

			$("#listing_transport").append(msg.listing_view);

			$("#ajax_favorite_loddder").hide();	

				

				/*$("#ajax_favorite_loddder").hide();	

				$('#BankDetail').html(msg);*/

			}

		});



		

		

});	



/* CLASS SEARCH */





$(document).on('click', '.service_type_all', function(){



	if(($(this).is(':checked')) && ($(this).val()==0) )

	{

		$('.service_type').prop("checked",false);

	}

	

	/*var  type_service='';

	if($('.service_type').is(':checked'))

	{

		type_service=$('.service_type:checked').val();

	}*/

	

	$("#ajax_favorite_loddder").show();	

  

  $(".load_more_bt").hide();

  

  

  

  var frm_val = $('#advance_search').serialize();	

  

		var page = 1

			

		$.ajax({

		//type: "post",

		//dataType: "json",

		url:  '?page=' + page+'&'+frm_val,

		success: function(msg) {

			$("#listing_transport").html(msg.listing_view);

			$("#ajax_favorite_loddder").hide();	

				

			}

		});

	

	

       

});



$(document).on('click', '.service_type', function(){ 



var check_cuisin = 0;

 var cusin_val = [];

 

   $('.service_type:checked').each(function(i){

		$('.service_type_all').prop("checked",false);

			check_cuisin = 1;

          cusin_val[i] ='foodType[]='+$(this).val();

  });

  

	if(check_cuisin==0)

	{

		cusin_val[0]  ='foodType[]=0';

		

		$('.service_type_all').prop("checked",true);

	}

	

	



	

	$("#ajax_favorite_loddder").show();	

	

  

  $(".load_more_bt").hide();

  

  var frm_val = $('#advance_search').serialize();		

		var page = 1

			

		$.ajax({

		//type: "post",

		dataType: "json",

		url:  '?page=' + page+'&'+frm_val,

		success: function(msg) {

			$("#listing_transport").html(msg.listing_view);

			$("#ajax_favorite_loddder").hide();	

				

			}

		});

		

});	



/* All CATEGORY CHECK BOX SEARCH */

$(document).on('click', '.foodallclass', function(){



	if(($(this).is(':checked')) && ($(this).val()==0) )

	{

		$('.foodclass').prop("checked",false);

	}

	

	/*var  type_service='';

	if($('.service_type').is(':checked'))

	{

		type_service=$('.service_type:checked').val();

	}*/

	

	$("#ajax_favorite_loddder").show();	

  

  $(".load_more_bt").hide();

  

  

  

  var frm_val = $('#advance_search').serialize();	

  

		var page = 1

			

		$.ajax({

		//type: "post",

		//dataType: "json",

		url:  '?page=' + page+'&'+frm_val,

		success: function(msg) {

			$("#listing_transport").html(msg.listing_view);

			$("#ajax_favorite_loddder").hide();	

				

			}

		});

	

	

       

});





/* CATEGORY SEARCH */

$(document).on('click', '.foodclass', function(){



var check_cuisin = 0;

 var cusin_val = [];

 

   $('.foodclass:checked').each(function(i){

		$('.foodallclass').prop("checked",false);

			check_cuisin = 1;

          cusin_val[i] ='foodType[]='+$(this).val();

  });

  

	if(check_cuisin==0)

	{

		cusin_val[0]  ='foodType[]=0';

		

		$('.foodallclass').prop("checked",true);

	}

	

	

	$("#ajax_favorite_loddder").show();	

  

  $(".load_more_bt").hide();

  

  

  var frm_val = $('#advance_search').serialize();	

		var page = 1

			

		$.ajax({

		//type: "post",

		dataType: "json",

		url:  '?page=' + page+'&'+frm_val,

		success: function(msg) {

			$("#listing_transport").html(msg.listing_view);

			$("#ajax_favorite_loddder").hide();	

				

			}

		});

	

	

	

});	



/* SORT BY */

$(document).on('change', '#sort_by', function(){



	$("#ajax_favorite_loddder").show();	

	$(".load_more_bt").hide();

	

	var frm_val = $('#advance_search').serialize();	

	var page = 1

		

	$.ajax({

	//type: "post",

	dataType: "json",

	url:  '?page=' + page+'&'+frm_val,

	success: function(msg) {

		$("#listing_transport").html(msg.listing_view);

		$("#ajax_favorite_loddder").hide();	

			

		}

	});

	



});



 /* CHANGE URL WITHOUT PAGE LOAD START */

	/*var  url_HREF = '';

	var city = $('#surbur').val();

	if(city != '')

	{

		

		var city1= city.split(' ').join('_');				 

			url_HREF =city1;

		

		newHREF = "{{url('/transport_listing/')}}";

		

		if(url_HREF != ''){ 

			newHREF = "{{url('/transport_listing/')}}"+'/'+url_HREF; 

			history.pushState('', 'New Page Title', newHREF);	

		}

	

	}*/



	/* CHANGE URL WITHOUT PAGE LOAD END */





	  $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

});





$(document).ready(function () {

	if ($('#back-to-top').length) {

		var scrollTrigger = 100, // px

		backToTop = function () {

			var scrollTop = $(window).scrollTop();

			if (scrollTop > scrollTrigger) {

				$('#back-to-top').addClass('show');

			} else {

				$('#back-to-top').removeClass('show');

			}

		};

		backToTop();

		$(window).on('scroll', function () {

			backToTop();

		});

		$('#back-to-top').on('click', function (e) {

			e.preventDefault();

			$('html,body').animate({

				scrollTop: 0

			}, 700);

		});

	}



	$("#close_in").click(function () {

		if ($(".cmn-toggle-switch__htx").hasClass("active")) {

			$(".cmn-toggle-switch__htx").removeClass("active")

		}

	});



});	





/* POPUP SEARCH FROM LOCATION */

			



$(document).on('keydown', '.search_from', function(){





var state_val =  $("#location_states option:selected").val();



 var availableTags = [

      "ActionScript",

      "AppleScript",

      "Asp",

      "BASIC",

      "C",

      "C++",

      "Clojure",

      "COBOL",

      "ColdFusion",

      "Erlang",

      "Fortran",

      "Groovy",

      "Haskell",

      "Java",

      "JavaScript",

      "Lisp",

      "Perl",

      "PHP",

      "Python",

      "Ruby",

      "Scala",

      "Scheme"

    ];

		//autoFocus: true,	 

	

	var NoResultsLabel = "No Results";	     	

    $( ".search_from" ).autocomplete({

      source: "{{url('/get_location_from') }}",

		minLength: 0,

	  select: function(event, ui) {

	  		  if (ui.item.label === NoResultsLabel) {

				 $('.search_from').val('');

                event.preventDefault();

            }

			else{

	 

				 $('.search_from').val(ui.item.label);

				 $('#from_lable').val(ui.item.label);

				 $(".from_location_id").val(ui.item.id); 

				 $(".from_suburb").val(ui.item.suburb); 

				 $(".from_pincode").val(ui.item.pincode);

			// $("#location_id").val(ui.item.id); 

				 $("#search_type").val(ui.item.search_type); 

			

			}

			 

		 },

	 focus: function (event, ui) {

	 		

	  		  if (ui.item.label === NoResultsLabel) {

				 $('.search_from').val('');

                event.preventDefault();

            }

			else{

			   if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {

				 $('.search_from').val(ui.item.label);

				 $('#from_lable').val(ui.item.label);

				 $(".from_location_id").val(ui.item.id); 

				 $(".from_suburb").val(ui.item.suburb); 

				 $(".from_pincode").val(ui.item.pincode);

				 

				 //$("#location_id").val(ui.item.id); 

				 $("#search_type").val(ui.item.search_type); 

			   } else{

				 $('.search_from').val(ui.item.label)

				 $('#from_lable').val(ui.item.label);

				 $(".from_location_id").val(ui.item.id); 

				 $(".from_suburb").val(ui.item.suburb); 

				 $(".from_pincode").val(ui.item.pincode);

				 $("#search_type").val(ui.item.search_type); 

			   }

			}

		}

    });





});



/* POPUP SEARCH TO LOCATION */



$(document).on('keydown', '.search_to', function(){





var state_val =  $("#location_states option:selected").val();



 var availableTags = [

      "ActionScript",

      "AppleScript",

      "Asp",

      "BASIC",

      "C",

      "C++",

      "Clojure",

      "COBOL",

      "ColdFusion",

      "Erlang",

      "Fortran",

      "Groovy",

      "Haskell",

      "Java",

      "JavaScript",

      "Lisp",

      "Perl",

      "PHP",

      "Python",

      "Ruby",

      "Scala",

      "Scheme"

    ];

	

	var NoResultsLabel = "No Results";

	//	autoFocus: true,	      	

    $( ".search_to" ).autocomplete({

      source: "{{url('/get_location') }}",

		minLength: 0,

	  select: function(event, ui) {

	  		 

	  		  if (ui.item.label === NoResultsLabel) {

           		  $('.search_to').val('');

                event.preventDefault();

            }

			else{

			 event.preventDefault();

             $('.search_to').val(ui.item.label);

			

				 $('#to_lable').val(ui.item.label);

				 $(".to_location_id").val(ui.item.id); 

				 $(".to_suburb").val(ui.item.suburb); 

				 $(".to_pincode").val(ui.item.pincode);

			

			// $("#location_id").val(ui.item.id); 

			 $("#to_search_type").val(ui.item.search_type); 

			} 

		 },

	 focus: function (event, ui) {

	 

	  		  if (ui.item.label === NoResultsLabel) {

           		  $('.search_to').val('');

                event.preventDefault();

            }

			else{

			   if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {

				 $('.search_to').val(ui.item.label);

			

				 $('#to_lable').val(ui.item.label);

				 $(".to_location_id").val(ui.item.id); 

				 $(".to_suburb").val(ui.item.suburb); 

				 $(".to_pincode").val(ui.item.pincode);

			 	 $("#to_search_type").val(ui.item.search_type); 

				 

				// $("#location_id").val(ui.item.id); 

				// $("#search_type").val(ui.item.search_type); 

			   } else{

				 $('.search_to').val(ui.item.label);

			

				 $(".to_location_id").val(ui.item.id); 

				 $(".to_suburb").val(ui.item.suburb); 

				 $(".to_pincode").val(ui.item.pincode);

				 $("#to_search_type").val(ui.item.search_type); 

				 $('#to_lable').val(ui.item.label);

			   }

			}

		}

    });





});



/* FILTER FORM WITH DATA  */





$(document).on('click', '#close_search', function(){





			$('#search_model').modal('hide');

			$('#confirm_model').modal('show');





			$(document).on('click', '.get_conf_val', function(){

			 confrim = $(this).attr('data-val');

			 	if(confrim==1)

				{

					 $('#confirm_model').modal('hide');

					 $('#search_model').modal('show');

				}						

				else if(confrim==0)

				{

					 $('#confirm_model').modal('hide');

				}

			 });





});







/* FORM FILTER START */

  



$(document).on('click', '#add_filter_left', function(){





if($('.datepicker').val() == $('.datepicker1').val())

{

	if(($('.search_ontime').val())> ($('.search_returntime').val()) )

	{

		

			$("#ajax_favorite_loddder").show();	

		$('.search_returntime').val();

		var frm_val ='type=same&ontime='+$('.search_ontime').val();

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					$(".show_return_time").html(msg);

					

					$("#ajax_favorite_loddder").hide();		

					}

				});

	}

}

 

 

 

var form = $("#advance_search");

	form.validate();

	var valid =	form.valid();

	

		if(valid)

		{			

			

			$('#search_model').modal('hide');

			$("#ajax_favorite_loddder").show();	

	

			  

			  $(".load_more_bt").hide();

			  

			 var select_vehicle = '';

			  

			  var frm_val = 'vehicle_id='+select_vehicle+'&'+$('#advance_search').serialize();		

					var page = 1

						

					$.ajax({

					//type: "post",

					dataType: "json",

					url:  '?page=' + page+'&'+frm_val,

					success: function(msg) {

					

							if(msg.redirect_on=='1'){

							

								$("#select_vehicle").val();

								$("#listing_transport").html(msg.listing_view);

								$("#top_counter_msg").html(msg.top_counter_msg);

								$("#ajax_favorite_loddder").hide();	

							}

							if(msg.redirect_on=='2'){

								//alert(msg.redirect_page);

								$("#ajax_favorite_loddder").hide();	

								

								window.location.href = msg.redirect_page;

							}

							if(msg.redirect_on=='3'){

							

								$("#select_vehicle").val();

								$("#listing_transport").html(msg.listing_view);

								$("#top_counter_msg").html(msg.top_counter_msg);

								$("#ajax_favorite_loddder").hide();

								$('#vehicle_notmeet_model').modal('show');	

								

							}

							

						}

					});

			

		}

});





/* LEFT SIDE FORM FILTER END */

$(document).on('click', '#add_filter', function(){









if(($('.search_ontime').val())> ($('.search_returntime').val()) )

{

	if($('.datepicker').val() == $('.datepicker1').val())

	{

		

			$("#ajax_favorite_loddder").show();	

		$('.search_returntime').val();

		var frm_val ='type=same&ontime='+$('.search_ontime').val();

			

				$.ajax({

			//	type: "POST",

				url: "{{url('/show_returntime')}}",

				data: frm_val,

				success: function(msg) {

					/*$("#show_return_time").html(msg);*/

					$(".show_return_time").html(msg);

					

			$("#ajax_favorite_loddder").hide();	

						

					}

				});

	}

}

 





var form = $("#search_frm");

	form.validate();

	var valid =	form.valid();

	

		if(valid)

		{			

			

			

			

			

			$('#search_model').modal('hide');

			$("#ajax_favorite_loddder").show();	

	

			  

			  $(".load_more_bt").hide();

			  

			 var select_vehicle = $('#select_vehicle').val();

			  

			  var frm_val = 'vehicle_id='+select_vehicle+'&'+$('#advance_search').serialize();		

					var page = 1

						

					$.ajax({

					//type: "post",

					dataType: "json",

					url:  '?page=' + page+'&'+frm_val,

					success: function(msg) {

					

							if(msg.redirect_on=='1'){

							

								$("#select_vehicle").val();

								$("#listing_transport").html(msg.listing_view);

								$("#top_counter_msg").html(msg.top_counter_msg);

								$("#ajax_favorite_loddder").hide();	

							}

							if(msg.redirect_on=='2'){

								//alert(msg.redirect_page);

								$("#ajax_favorite_loddder").hide();	

								

								window.location.href = msg.redirect_page;

							}

							if(msg.redirect_on=='3'){

							

								$("#select_vehicle").val();

								$("#listing_transport").html(msg.listing_view);

								$("#top_counter_msg").html(msg.top_counter_msg);

								$("#ajax_favorite_loddder").hide();

								$('#vehicle_notmeet_model').modal('show');	

								

							}

							

						}

					});

			

		}

});



$(document).on('click', '[id^="view_rate-"]', function(){



var vehicleid = $(this).attr('data-vehicleid');



var form = $("#vehicleinfo-"+vehicleid);

	

	

	

	var vehicle_id = $("#vehicle_id-"+vehicleid).val();

	var vehicle_from = $("#vehicle_from-"+vehicleid).val();

	var vehicle_to = $("#vehicle_to-"+vehicleid).val();

	var vehicle_ontime = $("#vehicle_ontime-"+vehicleid).val();

	var vehicle_ondate = $("#vehicle_ondate-"+vehicleid).val();

	var vehicle_returntime = $("#vehicle_returntime-"+vehicleid).val();

	var vehicle_returndate = $("#vehicle_returndate-"+vehicleid).val();

	 

	

	/*alert(vehicleid);

	alert(vehicle_id);*/

	

		if((vehicle_id!='') && (vehicle_from!='') && (vehicle_to!='') && 

			(vehicle_ontime!='') &&(vehicle_ondate!='') &&(vehicle_returntime!='') &&(vehicle_returndate!=''))

		{

				//alert('redirect on detail page');

				$("#ajax_favorite_loddder").show();	

	

			     var frm_val = form.serialize();

				$.ajax({

					//type: "post",

					dataType: "json",

					url:  "{{url('/setval_session') }}?select_vehicle="+vehicle_id+'&'+frm_val,

					success: function(msg) {

							if(msg.redirect_on=='1'){

								$("#ajax_favorite_loddder").hide();									

								window.location.href = msg.redirect_page;

							}

							if(msg.redirect_on=='2'){

								//alert(msg.redirect_page);

								$("#select_vehicle").val();

								$("#ajax_favorite_loddder").hide();									

								window.location.href = msg.redirect_page;

							}

							

						}

					});

		}

		else

		{

			$("#select_vehicle").val(vehicleid);

		

			$('#search_model').modal('show');

		}	

	

});



	</script>

	

	<script type="text/livescript" src="{{ url('/') }}/design/js/darkbox.js"></script>

<script src="{{ url('/') }}/design/front/js/jquery.prettyPhoto.js"></script> 

<script src="{{ url('/') }}/design/front/js/owl.carousel.min.js"></script> 





<script>

/* $(document).ready(function () {

     $("a[rel^='prettyPhoto']").prettyPhoto();



    $("[id^='picture']").owlCarousel({



         autoPlay: 3000, //Set AutoPlay to 3 seconds



         items: 1,

         itemsDesktop: [1199, 1],

         itemsDesktopSmall: [979, 1]



     });

	 

 });*/

</script>









<script>

$(document).ready(function() {



  $("[id^='owl-demo-']").owlCarousel({

 

      autoPlay: 3000, //Set AutoPlay to 3 seconds

 

      items : 1,

      itemsDesktop : [1199,1],

      itemsDesktopSmall : [979,1]

 

  });

 

});

</script>

@stop