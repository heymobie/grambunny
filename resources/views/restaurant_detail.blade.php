<?php //echo  Session::get('_token');?>
@extends('layouts.app')

@section("other_css")

<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">

   	  <!-- Radio and check inputs -->
	
	<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/elegant_font/elegant_font.min.css">
    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->
  <style>
    #addon-items-42 .popup-ctn ul.item-list li, .other-item-popup-ctn ul.item-list li {
        width: 50%;
        padding: 0 20px 0 0;
    }
    table tr td.td-details {
        width: 60%;
    }
    .td-details h5 {
        margin: 0 0 0;
        padding: 0 0 10px 0;
    }
    @media screen and (max-width: 1199px) {
        table tr td.td-details {
          width: 55%;
      } 
    }
  </style>

@stop
@section('content')

<?php $lang = Session::get('locale'); ?>

<!-- SubHeader =============================================== -->
<?php $style_banner ='/design/front/img/sub_header_2.jpg';
 if(!empty($rest_detail[0]->rest_banner)){
 //style="background-image: {{ url('/') }}/uploads/reataurant/banner/<?php echo $rest_detail[0]->rest_banner; !important;"
 $style_banner ='/uploads/reataurant/banner/'.$rest_detail[0]->rest_banner;
 }?> 
 
 <section style="background-image:{{ url('/').$style_banner }} !important" ; class="parallax-window">
 
  <div id="subheader">
  
  <div id="sub_content">
    	
        
      <div class="sub-hedr-left">
      <div id="thumb">	
	  				@if(!empty($rest_detail[0]->rest_logo))
					<img src="{{ url('/') }}/uploads/reataurant/{{$rest_detail[0]->rest_logo}}" alt="">									
					@else
					<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
					@endif
		</div>
     
     
     <div class="detail_cont_se">
     
       <div class="rtng-dtld">
	   @if($total_review>0)
      <div class="rating">
	  @for ($x = 1; $x < 6; $x++)
	  	@if(($avg_rating>0) && ($avg_rating>=$x))
			<i class="icon_star voted"></i>
		@else
			<i class="icon_star"></i>
		@endif
	  @endfor
	 (<small><a href="{{url('/'.strtolower(trim($rest_detail[0]->rest_suburb)).'/'.$rest_detail[0]->rest_metatag.'/review')}}">@lang('translation.read') {{$total_review}}, @lang('translation.reviews')</a></small>)</div>
	  @endif
	  <h1>
		  <?php if($lang=='ar'){ echo $rest_detail[0]->rest_name_ar; }else{ echo $rest_detail[0]->rest_name; } ?>
	      <!-- {{$rest_detail[0]->rest_name}} -->
  	  </h1>
      <div><em>{{$rest_detail[0]->cuisine_name}}</em></div>
      <div> <span><i class="icon_pin"></i> {{$rest_detail[0]->rest_address.' '.$rest_detail[0]->rest_zip_code}} </span><strong>@lang('translation.deliveryCharge'): ${{$rest_detail[0]->rest_mindelivery}}</strong></div>
      </div>
      
    </div>  
      
      </div>
      
      
      <div class="sub-hedr-right">
      	<div class="ratingsFacet-facetListContainer">
			<ul class="ratingsFacet-facetList"><!---->
			   @if($food_good>0)
				<li class="ratingsFacet-facetList-listItem text-xs-center">
					<span class="ratingsFacet-percent h5">{{$food_good}}</span>
					<span class="ratingsFacet-facetDesc u-text-secondary caption">@lang('translation.foodGood')</span>
				</li>
				@endif
				@if($delivery_ontime>0)
				<li class="ratingsFacet-facetList-listItem text-xs-center">
					<span class="ratingsFacet-percent h5">{{$delivery_ontime}}</span>
					<span class="ratingsFacet-facetDesc u-text-secondary caption">@lang('translation.deliveryOnTime')</span>
				</li>				
				@endif
				@if($order_accurate>0)
				<li class="ratingsFacet-facetList-listItem text-xs-center">
					<span class="ratingsFacet-percent h5">{{$order_accurate}}</span>
					<span class="ratingsFacet-facetDesc u-text-secondary caption">@lang('translation.orderAccurate')</span>
				</li>
				@endif
				
			</ul>
		</div>
      </div>
    </div>
    
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section>



<!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Position -->
    <!-- Content ================================================== -->
	
<div class="grey-back">
 <div class="container">
  <div id="container_pin">
    <div class="row">
      <div class="col-md-3" id="sidebar1">
        <div class="theiaStickySidebar">
          <p><a href="{{url('/restaurant_listing')}}" class="btn_side">@lang('translation.backToSearch')</a></p>
          <div class="box_style_1">
		  
		  @if(!empty($menu_array))	
            <ul id="cat_nav">
				@if(isset($popular_item['popular_item_count']))
              <li><a href="#menu_popular">Popular<span>({{$popular_item['popular_item_count']}})</span><i class="fa fa-caret-right"></i></a></li>
			  	@endif
			@foreach($menu_array as $marray)		
              <li><a href="#menu_{{$marray['menu_id']}}">
			  @if($lang=='ar')
			  	{{$marray['menu_name_ar']}}
			  @else
	          	{{$marray['menu_name']}}
	          @endif
	          <span>({{$marray['food_counter']}})</span><i class="fa fa-caret-right"></i></a></li><!-- class="active"-->
			 @endforeach 
            </ul>
			 @endif
			 
			 
           
          </div>
          <!-- End box_style_1 -->
				
				  <?php $promo_amt_cal = '0.00';			  
				  
				  //if(count($promo_list)>0)
				  if(!empty($promo_list))
				 	{	
				 ?>
				 
		  <div style="margin-bottom: 25px; background: rgb(255, 255, 255); border-radius: 3px;    border-width: 1px;
    border-style: solid;  border-color: rgb(237, 237, 237); border-image: initial;  padding: 25px;">
            <h3>@lang('translation.specOffer') </h3>
			
			<table class="table table_summary">
              <tbody>
			  <?php
				$p=0;
				$old_pro_amt =0;
				$old_remain_amt =0;
					 foreach($promo_list as $promo)
					 { 		
					 $p++;	
						 
						 ?>
						 <tr><td>
						 {{$promo['promo_desc']}}	
						 	</td></tr> 		
					<?php
					}
					?>   
              </tbody>
            </table>
			
            <hr>
			 </div>
					
					<?php 
				  }
				  
				
				?>	    
			 
          <div class="box_style_2 hidden-xs" id="help"> <i class="icon_lifesaver"></i>
            <h4> <span>@lang('translation.needHelp') </span></h4>
            <a href="#" class="phone"><?php echo $rest_detail[0]->rest_contact ?></a> <small>@lang('translation.openTime') 
				
				<?php 
				$tdoay_closed = 0;
				$open_time = '';
				$view_time  = '';
				$date_today = date('Y-m-d');
		$day_name =  strtolower(date("D",strtotime($date_today)));						
		switch ($day_name) {
			case 'sun':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_sun);
					$open_time = explode('_',$rest_detail[0]->rest_sun);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'mon':	
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_mon);			
					$open_time = explode('_',$rest_detail[0]->rest_mon);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'tue':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_tues);
					$open_time = explode('_',$rest_detail[0]->rest_tues);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'wed':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_wed);
					$open_time = explode('_',$rest_detail[0]->rest_wed);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'thu':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_thus);
					$open_time = explode('_',$rest_detail[0]->rest_thus);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'fri':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_fri);
					$open_time = explode('_',$rest_detail[0]->rest_fri);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
			case 'sat':
					$view_time = str_replace('_',' to ',$rest_detail[0]->rest_sat);
					$open_time = explode('_',$rest_detail[0]->rest_sat);
					$rest_start = $open_time[0];
					$rest_end = $open_time[1];
				break;
		}	
		
		if( strtotime(date("H:i", (strtotime($rest_end)))) <= strtotime(date("H:i")))
		{
			$tdoay_closed='1';
		}
			echo $view_time;
		?>
		
		</small> </div>
        </div>
      </div>
      <!-- End col-md-3 -->
      
      <div class="col-md-6">
        <div class="box_style_2" id="main_menu">
          <h2 class="inner">@lang('translation.menu')</h2>
		  
		@if(!empty($menu_array))
		
		  @if(isset($popular_item['popular_item_count'])&&($popular_item['popular_item_count']>0))
			
		 <h3 class="nomargin_top" id="menu_popular">@lang('translation.popular')</h3>
          <p>&nbsp; </p>

          <?php //print_r($popular_item['popular_food_detail']); ?>
		  
		  	<table class="table table-striped cart-list">
           				 <tbody>
							<?php $c=0;
							foreach($popular_item['popular_food_detail'] as $item){
								if($item['food_portion']=='yes'){?>
								
								<tr>
									<td colspan="3">
										 <table class="table cart-list table-bg-none">
											<?php 
											$fs = 0;
											foreach($item['food_size_detail'] as $fsize){	
											?>
											
											<tr>
											   <?php if($fs==0){?>
											   	<td class="deep-details deepimg"><img src="{{$item['food_image']}}" class="fooddimg"></td>
												<td class="td-details"><h5>
												<?php if($lang=='ar'){	echo $item['food_name_ar'] ; }else{ echo $item['food_name'] ; } ?>
															</h5>
													  <p><?php if($lang=='ar'){	echo $item['food_desc_ar'] ; }else{ echo $item['food_desc'] ; } ?></p>
												</td>		
											   <?php }else{?>
												<td class="td-details">&nbsp; </td>
												<td class="td-details">&nbsp; </td>
											   <?php }?>		  						  
											   <td class="mitemtl">{{$fsize->menu_item_title}}</td>
											  <td class="prc_detail"><strong>${{$fsize->menu_item_price}}</strong></td>
											  <td class="options">	
											  
											  <a href="javascript:void(0)" data-rest_id="{{$rest_detail[0]->rest_id}}"
														 data-menu_id="{{$item['food_menu_id']}}" 
														 data-menu_cat_id="{{$item['food_id']}}" id="show_modal" 
														 data-options="{{$fsize->menu_cat_itm_id}}"><i class="icon_plus_alt2"></i></a>
												</td>
										
											</tr>
											
											 <?php $fs++;}?>					 
										
										  </table>
									  </td>
								  </tr>
								<?php }else{?>
								<tr>
								<td class="deepimg"><img src="{{$item['food_image']}}" class="fooddimg"> </td>	
								<td><h5><?php if($lang=='ar'){	echo $item['food_name_ar'] ; }else{ echo $item['food_name'] ; } ?></h5>
								  <p><?php if($lang=='ar'){	echo $item['food_desc_ar'] ; }else{ echo $item['food_desc'] ; } ?></p></td>
								<td class="prc_detail"><strong>${{$item['food_price']}}</strong></td>
								<td class="options">
									<a href="javascript:void(0)" data-rest_id="{{$rest_detail[0]->rest_id}}"
									 data-menu_id="{{$item['food_menu_id']}}" 
									 data-menu_cat_id="{{$item['food_id']}}"
									 id="show_modal"><i class="icon_plus_alt2"></i></a>	
									</td>
							  </tr>
								
						 <?php }
							}
							?>
								<?php  $c++;?>
						</tbody>
         			 </table>
			
          
		  
          <hr>
			@endif  
			 
		  	
		<?php foreach($menu_array as $marray){ ?>
			<h3 class="nomargin_top" id="menu_{{$marray['menu_id']}}">
			<?php if($lang=='ar'){ echo $marray['menu_name_ar'] ;}else{ echo $marray['menu_name'] ; } ?>
		</h3>
			 <p><?php if($lang=='ar'){ echo $marray['menu_desc_ar'] ;}else{ echo $marray['menu_desc'] ; } ?></p>
			 	<?php if($marray['food_counter']>0){?>
					<table class="table table-striped cart-list">
           				 <tbody>
							<?php $c=0;
							foreach($marray['food_detail'] as $item){
								if($item['food_portion']=='yes'){?>
								
								<tr>
									<td colspan="3">
										 <table class="table cart-list table-bg-none">
											<?php 
											$fs = 0;
											foreach($item['food_size_detail'] as $fsize){	
											?>
											
											<tr>
												
											   <?php if($fs==0){?>
                                               <td class="deep-details deepimg"><img src="{{$item['food_image']}}" class="fooddimg"></td>
												<td class="td-details"><h5><?php if($lang=='ar'){	echo $item['food_name_ar'] ; }else{ echo $item['food_name'] ; } ?></h5>
													  <p><?php if($lang=='ar'){	echo $item['food_desc_ar'] ; }else{ echo $item['food_desc'] ; } ?></p>
													 
													</td>		
											   <?php }else{?>
												<td class="td-details">&nbsp; </td>
												<td class="td-details">&nbsp; </td>
											   <?php }?>		  						  
											   <td class="mitemtl">{{$fsize->menu_item_title}}</td>
											  <td class="prc_detail"><strong>${{$fsize->menu_item_price}}</strong></td>
											  <td class="options">	
											  
											  <a href="javascript:void(0)" data-rest_id="{{$rest_detail[0]->rest_id}}"
														 data-menu_id="{{$marray['menu_id']}}" 
														 data-menu_cat_id="{{$item['food_id']}}" id="show_modal" 
														 data-options="{{$fsize->menu_cat_itm_id}}"><i class="icon_plus_alt2"></i></a>
												</td>
										
											</tr>
											
											 <?php $fs++;}?>					 
										
										  </table>
									  </td>
								  </tr>
								<?php }else{?>
								<tr>
								<td class="deepimg deepimg"><img src="{{$item['food_image']}}" class="fooddimg"> </td>	
								<td><h5><?php if($lang=='ar'){	echo $item['food_name_ar'] ; }else{ echo $item['food_name'] ; } ?></h5>
								 <p><?php if($lang=='ar'){	echo $item['food_desc_ar'] ; }else{ echo $item['food_desc'] ; } ?></p></td>
								<td class="prc_detail"><strong>${{$item['food_price']}}</strong></td>
								<td class="options">
									<a href="javascript:void(0)" data-rest_id="{{$rest_detail[0]->rest_id}}"
									 data-menu_id="{{$marray['menu_id']}}" 
									 data-menu_cat_id="{{$item['food_id']}}"
									 id="show_modal"><i class="icon_plus_alt2"></i></a>	
									</td>
							  </tr>
								
						 <?php }
							}
							?>
								<?php  $c++;?>
						</tbody>
         			 </table>
				
				<?php }?>
		<?php }?>	
		
		@endif  	
        </div>
        <!-- End box_style_1 --> 
      </div>
      <!-- End col-md-6 -->
      
      <div class="col-md-3" id="sidebar">
        <div class="theiaStickySidebar">
		
		<form name="order_frm" id="order_frm" action="{{url('/post_checkout_rest')}}" method="post">
		<input type="hidden" name="rest_metatag" id="rest_metatag" value="{{url('/'.strtolower(trim($rest_detail[0]->rest_suburb)).'/'.$rest_detail[0]->rest_metatag)}}" />
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="hidden" name="temp_orderid" id="temp_orderid" value="" />
		  <input type="hidden" name="rest_id"  id="rest_id" value="{{$rest_detail[0]->rest_id}}">			  
		  <input type="hidden" name="rest_service_tax"  id="rest_service_tax" value="{{$rest_detail[0]->rest_servicetax}}">
		  <input type="hidden" name="promo_ids"  id="promo_ids_val" value="">
			  
		 
		  <input type="hidden" name="promo_list"  id="promo_list_cal" value="">
		  <input type="hidden" name="food_promo_list"  id="food_promo_list_cal" value="">
		  <input type="hidden" name="food_promo_applied"  id="food_promo_applied_cal" value="">
		  
		
				  <input type="hidden" name="total_promo_amt" id="total_promo_amt" value="0" />
		  
          <div id="cart_box">
            <h3 style="text-align: center;">@lang('translation.yourOrderSummary') <!--<i class="icon_cart_alt pull-right"></i>--></h3>
			<div id="order_list">
			
				<?php $cart_price = 0;?>
						@if(isset($cart) && (count($cart)))			
				<div class="table table_summary">
							
								
								 @foreach($cart as $item)
								<?php  $cart_price = $cart_price+$item->price;?>
								  <div class="spcl-ordr-div">
                                 <div class="crt_itm_cust1">
								  <p class="spcl-ordr-name">
								  <a href="javascript:void(0)" class="remove_item" style="margin-right: 5px;" id="add-qty-{{$item->id}}"  data-rowid="{{$item->rowId}}" data-total-qty="{{$item->qty}}" data-cartid="{{$item->id}}"><i class="icon_plus_alt2"></i></a><a href="javascript:void(0)" class="remove_item" id="subtract-qty-{{$item->id}}" data-total-qty="{{$item->qty}}" data-cartid="{{$item->id}}"  data-rowid="{{$item->rowId}}"><i class="icon_minus_alt"></i></a> <strong>{{$item->qty}}  x</strong> {{$item->name}} 
								  </p>
									<p class="spcl-ordr-priz"><strong class="pull-right">${{$item->price}}</strong></p>
								   <p class="spcl-ordr-think">{{@$item->options['instraction']}}</p>	
								   							</div>
								
								 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
									@foreach($item->options['addon_data'] as $addon)
										
									  <?php $cart_price = $cart_price+$addon['price'];?>
									 <div class="crt_itm_cust23">
									  <p class="spcl-ordr-name">{{$addon['name']}}</p>
									  <p class="spcl-ordr-priz"><strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong></p>
									</div>
									 @endforeach
								 @endif 
									</div>	
								 @endforeach
							  
				  </div>
					@else
						@lang('translation.cardEmpty')
				@endif
            
			</div>
            <hr>
			
			<?php
			//print_r(Session::get('cart_userinfo'));
			
			 $session_service = 'Delivery';
			
			 ?>			
				@if(!empty($rest_detail[0]->rest_service))
			<?php $service = explode(',',$rest_detail[0]->rest_service);
					if(!in_array('Delivery',$service ))
					{
						 $session_service = 'Pickup';
					}
				 
			if(Session::has('cart_userinfo')){
				
				if(!empty(Session::get('cart_userinfo')['service_type']))	{
				 	$session_service = @Session::get('cart_userinfo')['service_type'];
				 }
			}	
			 
			?> 
            <div class="row" id="options_2">
				@foreach($service as $rest_service)

				@if($rest_service == 'Delivery')
			  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="control-group islamic2">
				
					<label class="control control--radio">{{$rest_service}}
						<input type="radio"  value="{{$rest_service}}" class="icheck rest_option" name="option" required="required"  @if($session_service==$rest_service)  checked="checked" @endif>
						<div class="control__indicator"></div>
					</label>
				</div>  

              </div>
              @endif
			  	@endforeach
             
            </div>
			@endif
            
            <!-- Edn options 2 -->
            
            <hr>
            <!--<table class="table table_summary">
              <tbody>
                <tr>
                  <td> Subtotal <span class="pull-right">$56</span></td>
                </tr>
                <tr>
                  <td> Delivery fee <span class="pull-right">$10</span></td>
                </tr>
                <tr>
                  <td class="total"> TOTAL <span class="pull-right">$66</span></td>
                </tr>
              </tbody>
            </table>-->
			<table class="table table_summary">
              <tbody>
                <tr>
                  <td> @lang('translation.subtotal') <span class="pull-right" id="Subtotal_amt">${{$cart_price}}</span>
				  <input type="hidden" name="sub_total" id="sub_total" value="{{$cart_price}}" />
				  
				  </td>
                </tr>
                @foreach($rest_detail as $rest_detail_val)
                <tr>
                  <td> @lang('translation.salesTax') <span class="pull-right" id="service_tax_amt">
                  	
                  	 <?php echo'$0.00';?>
                  	</span>
				  <input type="hidden" name="service_tax" id="service_tax" value="{{ $rest_detail[0]->rest_servicetax }}" />
				  
				  </td>
                </tr>
					@endforeach
				
				  <?php $promo_amt_cal = '0.00';
				  
				  
				  //if(count($promo_list)>0)
				  if(!empty($promo_list))
				 {
				 
						
				?>
				<input type="hidden" name="total_promo" id="total_promo" value="<?php echo count($promo_list);?>" />
				
				<?php //echo"<pre>"; print_r($promo_desc_list); die;?>
                <tr>
                  <td>@lang('translation.discount')<div id="show_offer_text"> ({{ $promo_desc_list->promo_desc }})</div> 
				  <span class="pull-right" id="discount_amt">$0</span>
				  <input type="hidden" name="promo_discount_text" id="promo_discount_text" value="" />
				  
				  </td>
                </tr>
				
				<?php 
				  			  
				}
				?>	 
				
				
				@if(in_array("Delivery", $service))
				
				
                <tr id="del_fee" style=<?php if($session_service != 'Delivery'){ echo "display:none";  } ?> > 
                  <td>  
				  <input type="hidden" name="delivery_fee" id="delivery_fee" value="@if(($rest_detail_val->rest_mindelivery))${{$rest_detail_val->rest_mindelivery}}@else $0 @endif" />
				  @lang('translation.deliveryFee') 
				  <span class="pull-right delivery_fee">
				  	@if(($rest_detail_val->rest_mindelivery))${{$rest_detail_val->rest_mindelivery}}@else $0 @endif
				 <!--  @if((Session::has('delivery_fee')))${{Session::get('delivery_fee')}} @else $0 @endif --></span></td>
                </tr>
				
				 @if(!empty($rest_detail[0]->rest_mindelivery)) 
				 <?php
				
				 $remain = 0;
				 $display_style = 'block';
				 if(Session::has('delivery_fee_min_remaing'))
				 {
				 		 $remain = Session::get('delivery_fee_min_remaing');
				 }
				 elseif(($rest_detail[0]->rest_mindelivery)>($cart_price-$promo_amt_cal))
				 { 
				 	$remain =($rest_detail[0]->rest_mindelivery - $cart_price);
				 }
				 elseif(($rest_detail[0]->rest_mindelivery)<($cart_price-$promo_amt_cal))
				 { 
				 	$display_style = 'none';
				 	$remain =0;
				 }	
				else
				 { 		 
					 $remain =$rest_detail[0]->rest_mindelivery;
				 }
				 
				 
				  if($session_service != 'Delivery'){
				 	$display_style = 'none';
				 	$remain =$rest_detail[0]->rest_mindelivery;
				 
				 }
				 ?>
				 
				 
				
				 <tr id="del_fee_min" style="display:{{$display_style}}">
                  <td>  
				  <input type="hidden" name="delivery_fee_min" id="delivery_fee_min" value="{{$rest_detail[0]->rest_mindelivery}}" />
				  <input type="hidden" name="delivery_fee_min_remaing" id="delivery_fee_min_remaing" value="{{$remain}}" />
				  @lang('translation.minOrderAmount')
				  <span class="pull-right">${{$rest_detail[0]->rest_min_orderamt}}</span>
				  </td>
                </tr>
				
				 
				@endif
				
				@endif
				
				<?php if(($rest_detail[0]->rest_partial_pay==1) && (($rest_detail[0]->rest_partial_percent)>0)){?>
				
				
				<tr>
                	<td>
                     <hr />
                    <div  class="prsl-pay">
									 
						<label for="chkPassport">
							<input type="checkbox" name="partical_payment" id="chkPassport" value="<?php echo $rest_detail[0]->rest_partial_percent;?>" />
							@lang('translation.partialPayment')  (<?php echo $rest_detail[0]->rest_partial_percent;?>%)
						</label>
					   
						<div id="dvPassport" >
						   <p>@lang('translation.partialPayoutAmt') (<?php echo $rest_detail[0]->rest_partial_percent;?>%) <span class="pull-right"><span id="payout_partial">0.00</span>$</span></p>
						   <p>@lang('translation.partialRemainingPayment')  <span class="pull-right"><span id="payout_partial_remaining">0.00</span>$</span></p>
						</div>
          
                    </div>
                    </td>
                </tr>
				
				
				<?php /*?>
				<tr>
                  <td> <input type="checkbox" name="partical_payment" id="partical_payment" value="<?php echo $rest_detail[0]->rest_partial_percent;?>" /> Partial Payment (<?php echo $rest_detail[0]->rest_partial_percent;?>%)			  
				  </td>
                </tr>
				<tr>
                  <td>  Partial Payout Amt  <span class="pull-right">$<span id="payout_partial">0.00</span></span>
				  </td>
                </tr>
				<tr>
                  <td> Partial Remaining Payout Amt <span class="pull-right">$<span id="payout_partial_remaining">0.00</span> remaining</span>
				  </td>
                </tr><?php */?>
				<?php }?>
				
                <tr>
                  <td class="total"> @lang('translation.total') 
				  
				  	<?php
					
					
					$total_amt= 0;
					
				if($session_service != 'Delivery'){
					$total_amt= $cart_price-$promo_amt_cal; 
				}
				else
				{
					if(Session::has('delivery_fee'))
					{
						$total_amt= ($cart_price+(Session::get('delivery_fee'))-$promo_amt_cal);
						
						if((Session::has('delivery_fee_min')) && (Session::has('delivery_fee_min')>0) && (($cart_price-$promo_amt_cal)<$rest_detail[0]->rest_min_orderamt))
						{ 
							$total_amt= ($rest_detail[0]->rest_min_orderamt+Session::get('delivery_fee'));
						}
						elseif(($rest_detail[0]->rest_min_orderamt)>($cart_price-$promo_amt_cal))
						{
							$total_amt= ($rest_detail[0]->rest_min_orderamt+(Session::get('delivery_fee')));
						}
					
					
					}
					elseif(($rest_detail[0]->rest_mindelivery)>($cart_price-$promo_amt_cal))
					{ $total_amt= $rest_detail[0]->rest_min_orderamt;}
				    else{ $total_amt= $cart_price-$promo_amt_cal; } 
				}  
				 
					// $total_amt=($total_amt)+(Session::get('delivery_fee'));
				?>
				  
				    <input type="hidden" name="total_charge" id="total_charge" value="{{$total_amt}}" />
				    <input type="hidden" name="mini_order_amt" id="mini_order_amt" value="{{$rest_detail[0]->rest_min_orderamt}}" class="form-control"/>
				    <input type="hidden" name="selected_lang" id="selected_lang" value="<?php if($lang=='ar'){	echo "ar"; }else{ echo "en"; } ?>" class="form-control"/>
					
				  <span class="pull-right change_total" id="total_amt">${{$total_amt}} </span>
				  
				  
				  </td>
				</tr>
				  
                <!--<tr>
                	
                  <td class="total-tip"> 
                  	<div class="tip-div"> Tip Amount</div>
                  	<div class="tip-box">
                  		<div class="tips">
                  			<label class="control control--radio"><span id="ten">hello</span>
								<input type="radio" name="radio" class="ten" id="radio" value=""/>
								<div class="control__indicator"></div>
							</label>
							
						</div>
                  		<div class="tips">
                  			<label class="control control--radio"><span id="fifteen">test</span>
								<input type="radio" name="radio" class="fifteen" id="radio" value=""/>
								<div class="control__indicator"></div>
							</label>
                  			
                  		</div>
                  		<div class="tips">
                  			<label class="control control--radio"><span id="eighteen">demo</span>
							  <input type="radio" name="radio" class="eighteen" id="radio" value=""/>
							  <div class="control__indicator"></div>
							</label>
                  			
                  		</div>
                  	</div>
                  	<!-- (10% - {{$percentage_amt = ($total_amt / 100) * 10}}, 
                  	15% - {{$percentage_amt = ($total_amt / 100) * 15}}, 
                  	18% - {{$percentage_amt = ($total_amt / 100) * 13}}
                  	) -->
                  	<!--<input type="text" name="tip" id="tip" value="" placeholder="Enter Tip Amount" class="form-control" />
				  
				  </td>
				</tr>-->
                <tr>
                  <td class="total"> @lang('translation.grandTotal')
					<input type="hidden" value ="{{$total_amt}}" name="grand" class="grand">
				  <span class="pull-right" id="total_grand_amt">$<span class="grand_total_amt">{{$total_amt}} </span></span>

				  </td>
				  </tr>
              </tbody>
            </table>
			
            <hr>
			
			 <a class="btn_full" href="javascript:void(0)" id="plase_order">@lang('translation.btnOrderNow')</a>
			 </div>
			 	<input type="hidden" name="partical_percent" id="partical_percent" value="<?php echo $rest_detail[0]->rest_partial_percent;?>"/>
			 	<input type="hidden" name="partical_amt" id="partical_amt" />
			 	<input type="hidden" name="partical_remaning_amt" id="partical_remaning_amt" />
			</form> 
          <!-- End cart_box --> 
        </div>
      </div>
      <!-- End col-md-3 --> 
      
    </div>
    <!-- End row --> 
  </div>
  <!-- End container pin --> 
</div>
</div>
<!-- End container -->
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')


<div class="modal fade" id="other-items" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.makeChoice')</h4>
      </div>
      <div class="modal-body popup-ctn">
        <div class="other-item-popup-ctn rest_itm_pop" id="show_mod_addon">
		<form name="modal">
          <h2>Tokyo Original Burger</h2>
          <p class="dei-text">100% minced beef patty, cheese, pickles, fresh lettuce and tomato, special Japo mayonnaise base sauce, served on a sesame burger bun</p>
          <h3 class="small-title">Remove</h3>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <ul class="item-list">
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Cheese <small class="pull-right">€ 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"  class="icheck">
                    NO Pickles <small class="pull-right">€ 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Fresh Lettuce <small class="pull-right">€ 8,30</small></label>
                </li>
              </ul>
            </div>
            <div class="col-md-6 col-sm-6">
              <ul class="item-list">
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Cheese <small class="pull-right">€ 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"  class="icheck">
                    NO Pickles <small class="pull-right">€ 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Fresh Lettuce <small class="pull-right">€ 8,30</small></label>
                </li>
              </ul>
            </div>
          </div>
          <p>
            <button type="button" class="btn btn-primary">@lang('translation.btnContinue')</button>
          </p>
          <div class="cleafix"></div>
		  </form>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="devlivery_message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.message')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  	<p>
	  	The minimum order amount for this restaurant is $<span id="min_charge">15</span>*</p>
<p>
The total of your selected items is $<span id="subtotal_min_charge">5.25</span> 
		</p>
<p>
<a href="#" data-dismiss="modal"><strong>Choose more items </strong></a> OR
<a href="#" id="submit_with_min_charge"><strong>Just charge me an extra <span id="reman_min_charge">9.75</span></strong></a>
</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.message')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  		<p id="alert_model_msg"></p>	
      </div>
    </div>
  </div>
</div>



<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}


#add_sucess_msg {
    position: fixed;
    width: auto;
    height: auto;
    background: rgba(210, 71, 30, 0.89);
    z-index: 99999;
    color: #f5f0e3 !important;
    margin: 0 auto;
    padding: 21px;
    font-size: 20px;
    top: 50%;
    margin: 0 auto;
    text-align: center;
	left: 50%;
    transform: translate(-50%, 0);
}

#update_sucess_msg{
    position: fixed;
    width: auto;
    height: auto;
    background: rgb(232, 30, 42) !important;
    z-index: 99999;
    color: #f5f0e3 !important;
    margin: 0 auto;
    padding: 21px;
    font-size: 20px;
    top: 50%;
    margin: 0 auto;
    text-align: center;
    left: 50%;
    transform: translate(-50%, 0);
}

</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>


<div id="add_sucess_msg" style="display:none;">
		@lang('translation.added')
</div>


<div id="update_sucess_msg" style="display:none;">
		@lang('translation.updated')
</div>



<div class="modal fade" id="confirm_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.confirmationMessage')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  		<p>
				@lang('translation.removeItemMsg')
			</p>	
			<p>
				<button type="button" class="get_conf_val btn-default" data-val="1">@lang('translation.btnOk')</button>
				<button type="button" class="get_conf_val btn-primary" data-val="0">@lang('translation.btnCancel')</button>
			</p>
      </div>
    </div>
  </div>
</div>
<style>
div#my_order_confrim_model .modal-body.popup-ctn p button.get_orderconf_val {
    font-size: 14px;
    background: #e81e2a;
    padding: 10px 10px;
    display: block;
    border-radius: 3px;
    display: inline-block;
    border: none;
    color: #fff;
    padding: 6px 14px;
}
div#my_order_confrim_model .modal-body.popup-ctn p button.get_orderconf_val.btn-primary {
    background: #3c3c3c;
}
</style>

<div class="modal fade" id="my_order_confrim_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.confirmationMessage')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  		<p>
				@lang('translation.addRemoveOtherItem')
				
			</p>	
			<p>
				<button type="button" class="get_orderconf_val btn-default" data-val="1">@lang('translation.btnOk')</button>
				<button type="button" class="get_orderconf_val btn-primary" data-val="0">@lang('translation.btnCancel')</button>
			</p>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="refresh" value="no">
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>


<script>
	var $radios = $('input[name=radio]').change(function () {
	    var value = $radios.filter(':checked').val();
	    
	    $('#tip').val(value);
	    $("#tip").trigger('change');
	});

	function grandTotal(Total){
		var ten_percent = (Total / 100 * 10);
		$('#ten').html('10% - '+parseFloat(ten_percent).toFixed(2));
		$('.ten').val((ten_percent).toFixed(2));

		var fifteen_percent = (Total / 100 * 15);
		$('#fifteen').html('15% - '+parseFloat(fifteen_percent).toFixed(2));
		$('.fifteen').val((fifteen_percent).toFixed(2));

		var eighteen_percent = (Total / 100 * 18);
		$('#eighteen').html('18% - '+parseFloat(eighteen_percent).toFixed(2));
		$('.eighteen').val((eighteen_percent).toFixed(2));

		$('.grand_total_amt').html(Total);
		
		var tip = $('#tip').val();
		var amount=''; 
		var grand = parseFloat(Total);
		

		if(tip=='') {
			var tip1 =0;

		}else if(isNaN(tip)){
			var tip1 =0;
			
		}else{
			var tip1 = parseFloat($('#tip').val());
		}
		
		amount = tip1+grand;
			$(".grand_total_amt").html(parseFloat(amount).toFixed(2));
			$(".grand").val(amount);
	}
	$('#tip').keypress(function(e)
	   {
	   	//$("input:radio").removeAttr("checked");
	   	$('input[name=radio]').removeAttr("checked");			
	     var key_codes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 0, 8, 190, 46];

	     if (!($.inArray(e.which, key_codes) >= 0)) {
	       e.preventDefault();
	     }
	   });
	$('#tip').on("change",function(){
    
		
		var tip = parseFloat($('#tip').val());
		var grand = parseFloat($('#total_charge').val());
		if(tip=='') {
			var tip1 =0;

		}else if(isNaN(tip)){
			var tip1 =0;
			
		}else{
			var tip1 = parseFloat($('#tip').val());
		}
		var amount = tip1+grand;
		$(".grand_total_amt").html(parseFloat(amount).toFixed(2));
		$(".grand").val(amount);
		//alert(amount);

	});
	
$( document ).ready(function() {	


    

 
  	// $("#ajax_favorite_loddder").show();
	 
	 var dal_charg = 0;
	 var del_min_charg=0;
	 var remaing_min_del_charg = 0;
	 var tax_percent = $('#rest_service_tax').val();
	 
	 if($('.rest_option').is(':checked'))
	 {
		if($('.rest_option:checked').val()=='Delivery')
		{

			dal_charg = $("#delivery_fee").val();
			del_min_charg = $("#delivery_fee_min").val();
			}
	 }
	 
	
		var total_qty = $(this).attr('data-total-qty');
		var current_cartid = $(this).attr('data-cartid');
		var current_rowid = $(this).attr('data-rowid');
		var current_rowid = $(this).attr('data-rowid');
		
		//$("#show_offer_text").html('(10 % off total order of 100)');
		
		
		//var frm_val = 'menu_cat_id='+current_cartid+'&cart_row_id='+current_rowid+'&rest_tax='+tax_percent;;	
		
		var frm_val = 'rest_id=<?php echo $rest_detail[0]->rest_id;?>';	
			
		$.ajax({
		type: "post",
		url: "{{url('/cart/show_cart_data')}}",
		data: frm_val,
		dataType: 'json',
			success: function(msg) {
				
				console.log("1185"+msg.service_tax);
					$("#promo_list_cal").val(msg.promo_list);
					$("#food_promo_list_cal").val(msg.food_promo_list_cal);
					$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
				
				if(isNaN(dal_charg)){

					dal_charg = dal_charg.replace(/[^0-9.]/g, "");
					
				}	
				//dal_charg = dal_charg.replace(/[^0-9.]/g, "");	
			 //alert(dal_charg);
			 	var subtotal = msg.total_amt;
				
				var discount_val = 0;
				var total_rest_promo = $("#total_promo").val();
				var tax_calculation = msg.service_tax;
				//tax_calculation =102;
				var discount_val = msg.discount_amt;					
					$("#total_promo_amt").val(discount_val);
					$("#discount_amt").html('$'+discount_val);
					// $("#show_offer_text").html('('+msg.discount_txt+')');	
					
					
					  if(msg.discount_txt=='') {
					  	//$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }				
					  $("#promo_discount_text").val(msg.discount_txt);
					  	
					
					 $("#promo_ids_val").val(msg.order_offer_on);
				
				
				var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-discount_val).toFixed(2);
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				console.log(dal_charg);
				if(subtotal == 0){
					/*if((subtotal-discount_val)<del_min_charg)
					{*/
						/*$('#del_fee_min').show();
						
						var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);*/
						remaing_min_del_charg = parseFloat(del_min_charg).toFixed(2);/*-parseFloat(subtotal))*/
						
						if(remaing_min_del_charg<=0)
						{ 
							$('#del_fee_min').hide();
						}
					  
					//}
					/*else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}*/
				}
				if(subtotal > 0){
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();

						var dsubtotal = subtotal-discount_val;
						
						var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}
				}
			 	
				$('#delivery_fee_min_span').html(remaing_min_del_charg);
				$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				
					$('#service_tax_amt').html('$'+tax_calculation);					
					$('#service_tax').val(tax_calculation);
			 
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);	
					$('#sub_total').val((subtotal).toFixed(2));			
				
					$('#total_amt').html('$'+Total);
					grandTotal(Total);
					$('#total_charge').val(Total);
					$('input[name=radio]').trigger('change');
				
				
				
				
				
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}


				
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					
					
			 	//$("#ajax_favorite_loddder").hide();	
				
				//	$("#update_sucess_msg").show();					
			//	$("#update_sucess_msg").fadeOut(5000); 
			}
		});

		
		
				
});
</script>


	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="{{ url('/') }}/design/front/js/bootstrap.js"></script> 
    <script src="{{ url('/') }}/design/front/js/owl.carousel.js"></script> 
    <script src="{{ url('/') }}/design/front/js/wow.min.js"></script> 
	
	
	
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/js/validate.js"></script>
<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/custom.js"></script> 


<script>
    jQuery('#sidebar, #sidebar1').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script> 
<script>




$(document).on('click', '#show_modal', function(){ 


$("#ajax_favorite_loddder").show();

var rest_id = $(this).attr('data-rest_id');
var menu_id = $(this).attr('data-menu_id');
var menu_cat_id = $(this).attr('data-menu_cat_id');

var menu_option = '';

if($(this).attr('data-options')){

	var menu_option = $(this).attr('data-options');
}

	
frm_val = 'rest_id='+rest_id+'&menu_id='+menu_id+'&menu_cat_id='+menu_cat_id+'&menu_option='+menu_option;
		
	
//alert(frm_val);		
 
		$.ajax({
				type: "POST",
				url: "{{url('/show_other_data')}}",
				data: frm_val,
			   dataType: 'json',
					success: function(msg) { 
						//console.log(msg);
										
						if(msg.addon_count>0)				
						{
							if(msg.addon_lang == 'ar')				
							{
								$('#show_popup_title_addon').html('اصنع اختيارك');
							} else {
								$('#show_popup_title_addon').html('Make your choice');	
							}
							 
						}
						else
						{						
							$('#show_popup_title_addon').html(''); 
						}
										
						   
						$("#ajax_favorite_loddder").hide();		
									 
						$('#show_mod_addon').html(msg.addon_view); 
						$('#other-items').modal('show'); 
					}
				});
												
});

 $(function() {
	 'use strict';
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 70
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	
	
$(document).on('click', '[id^="show_model-"]', function(){ 
	
	
	$('.model_option').val('');	
	var restitem = $(this).attr('data-restitem');
	if($(this).attr('data-options')){
	
		var option = $(this).attr('data-options');
		$('.model_option').val(option);	
	}
	$('#addon-items-'+restitem).modal('show');
});
	
$(document).on('click', '[id^="add-qty-"]', function(){ 
  	 $("#ajax_favorite_loddder").show();
	 
	 var dal_charg = 0;
	 var del_min_charg=0;
	 var remaing_min_del_charg = 0;
	 var tax_percent = $('#rest_service_tax').val();
	 // var inc_dcont = $("#show_offer_text").text();
	 // alert(inc_dcont);
	 if($('.rest_option').is(':checked'))
	 {
		if($('.rest_option:checked').val()=='Delivery')
		{
			dal_charg = $("#delivery_fee").val();
			del_min_charg = $("#delivery_fee_min").val();
		}
	 }
	 	if(isNaN(dal_charg)){
			dal_charg = dal_charg.replace(/[^0-9.]/g, "");
		}
		var total_qty = $(this).attr('data-total-qty');
		var current_cartid = $(this).attr('data-cartid');
		var current_rowid = $(this).attr('data-rowid');
		var current_rowid = $(this).attr('data-rowid');
		
		var frm_val = 'menu_cat_id='+current_cartid+'&cart_row_id='+current_rowid+'&rest_tax='+tax_percent;;	

		//alert(frm_val);
			
		$.ajax({
		type: "POST",
		url: "{{url('/cart/increment')}}",
		data: frm_val,
		dataType: 'json',
			success: function(msg) { 
				console.log("1450"+msg);
			 	$("#promo_list_cal").val(msg.promo_list);
				$("#food_promo_list_cal").val(msg.food_promo_list_cal);
				$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
					
					
			 
			 	var subtotal = msg.total_amt;
				
				var discount_val = 0;
				var total_rest_promo = $("#total_promo").val();
				var tax_calculation = msg.service_tax;
				var discount_val = msg.discount_amt;					
					$("#total_promo_amt").val(discount_val);
					$("#discount_amt").html('$'+discount_val);
					//$("#show_offer_text").html('('+msg.discount_txt+')');	
					
					
					  if(msg.discount_txt=='') {
					  	//$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }				
					  $("#promo_discount_text").val(msg.discount_txt);
					  	
					
					 $("#promo_ids_val").val(msg.order_offer_on);
				
				
				var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-discount_val).toFixed(2);
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END **/
				
				if((subtotal-discount_val)<del_min_charg)
				{
					$('#del_fee_min').show();


					var dsubtotal = subtotal-discount_val;
						
					var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);
					
					//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);

					remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
					
					if(remaing_min_del_charg<=0)
					{
						$('#del_fee_min').hide();
					}
				  
				}
				else if((subtotal-discount_val)>del_min_charg)
				{
						$('#del_fee_min').hide();
				}
			 	
				$('#delivery_fee_min_span').html(remaing_min_del_charg);
				$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				
				$('#service_tax_amt').html('$'+tax_calculation);					
				$('#service_tax').val(tax_calculation);
			 
				$('#order_list').html(msg.cart_view);
				$('#Subtotal_amt').html('$'+msg.total_amt);	
				$('#sub_total').val((subtotal).toFixed(2));			
				
				$('#total_amt').html('$'+Total);
				grandTotal(Total);
				$('#total_charge').val(Total);
				$('input[name=radio]').trigger('change');
				
				
				
				
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}


				
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					
					
			 	$("#ajax_favorite_loddder").hide();	
				
					$("#update_sucess_msg").show();					
				$("#update_sucess_msg").fadeOut(5000); 
			}
		});

		
		
});
	
$(document).on('click', '[id^="subtract-qty-"]', function(){ 
  
		var total_qty = $(this).attr('data-total-qty');
		var current_cartid = $(this).attr('data-cartid');
		var current_rowid = $(this).attr('data-rowid');
		var confrim =1 ;
		
		var tax_percent = $('#rest_service_tax').val();
		 
	var dal_charg = 0;
	 var del_min_charg=0;
	 var remaing_min_del_charg = 0;
	 
	 if($('.rest_option').is(':checked'))
	 {
		if($('.rest_option:checked').val()=='Delivery')
		{
			dal_charg = $("#delivery_fee").val();
			del_min_charg = $("#delivery_fee_min").val();
		}
	 }
	 
		/*if(total_qty==1){
			
			var r = confirm("Are you sure you want to remove this Item?");
			if (r == true) {
				confrim = 1;
			} else {
				confrim = 0;
			}
		}*/
		
		
		
		if(total_qty==1){
		
			confrim=2;
		
			$('#confirm_model').modal('show');
			
			$(document).on('click', '.get_conf_val', function(){
			 confrim = $(this).attr('data-val');
			 	if(confrim==1)
				{
					 $('#confirm_model').modal('hide');
					$("#ajax_favorite_loddder").show();
					//var frm_val = 'menu_cat_id='+current_cartid;	
					var frm_val = 'menu_cat_id='+current_cartid+'&cart_row_id='+current_rowid+'&rest_tax='+tax_percent;				
						
					$.ajax({
					type: "POST",
					url: "{{url('/cart/decrease')}}",
					data: frm_val,
				   dataType: 'json',
					success: function(msg) {	
						
					$("#hcrtcount").text(msg.cart_count);
												
					console.log("1636"+msg);
					
					$("#promo_list_cal").val(msg.promo_list);
					$("#food_promo_list_cal").val(msg.food_promo_list_cal);
					$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
					
						
						
						var subtotal = msg.total_amt;
						
						
						var discount_val = 0;
					
						var tax_calculation = msg.service_tax;
				
					var total_rest_promo = $("#total_promo").val();
					
					
					var discount_val = msg.discount_amt;					
					 $("#total_promo_amt").val(discount_val);
					  $("#discount_amt").html('$'+discount_val);
					 // $("#show_offer_text").html('('+msg.discount_txt+')');		
					  
					  
					  if(msg.discount_txt=='') {
					  	//$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }
					  			  
					  $("#promo_discount_text").val(msg.discount_txt);
				 				
					 $("#promo_ids_val").val(msg.order_offer_on);	

					if(isNaN(dal_charg)){

						dal_charg = dal_charg.replace(/[^0-9.]/g, "");
						
					}
					var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
				
					
					
						
						
						//var Total1 = (parseFloat(subtotal)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);

						
						/*  MANAGE MINIMUM DELIVERY CHARGE END */
						if(subtotal == 0){
					/*if((subtotal-discount_val)<del_min_charg)
					{*/
						/*$('#del_fee_min').show();
						
						var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);*/
						remaing_min_del_charg = parseFloat(del_min_charg).toFixed(2);/*-parseFloat(subtotal))*/
						
						if(remaing_min_del_charg<=0)
						{ 
							$('#del_fee_min').hide();
						}
					  
					//}
					/*else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}*/
				}
				if(subtotal > 0){
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();
						

						var dsubtotal = subtotal-discount_val;
						
						var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);

						//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}
				}



						/*if((subtotal-discount_val)<del_min_charg)
						{
							$('#del_fee_min').show();
							
							var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
							remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
							
							if(remaing_min_del_charg<=0)
							{
								$('#del_fee_min').hide();
							}
						  
						}
						else if((subtotal-discount_val)>del_min_charg)
						{
								$('#del_fee_min').hide();
						}*/
						
						$('#delivery_fee_min_span').html(remaing_min_del_charg);
						$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
						
						/*  MANAGE MINIMUM DELIVERY CHARGE END */
					 
						
						$('#order_list').html(msg.cart_view);
						$('#Subtotal_amt').html('$'+msg.total_amt);
						$('#total_amt').html('$'+Total);
						grandTotal(Total);
						$('#total_charge').val(Total);
						$('input[name=radio]').trigger('change');
						
						
						
				
				
						$('#service_tax_amt').html('$'+tax_calculation);					
						$('#service_tax').val(tax_calculation);
					
						if(subtotal>0){
						$('#sub_total').val((subtotal).toFixed(2));
						}
						else
						{
						$('#sub_total').val('0.00');
							
						}
						
						
						
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}


				
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					
					
					
						 $("#ajax_favorite_loddder").hide();
						 
							$("#update_sucess_msg").show();					
							$("#update_sucess_msg").fadeOut(5000); 
						}
					});
		
				}	
				else if(confrim==0)
				{
					 $('#confirm_model').modal('hide');
				}
				
			 	
			
			 });
			/*var r = confirm("Are you sure you want to remove this Item?");
			if (r == true) {
				confrim = 1;
			} else {
				confrim = 0;
			}*/
		}
		
		
		if((confrim==1) && (total_qty>1))
		{
			$("#ajax_favorite_loddder").show();	
			//var frm_val = 'menu_cat_id='+current_cartid;	
			var frm_val = 'menu_cat_id='+current_cartid+'&cart_row_id='+current_rowid+'&rest_tax='+tax_percent;				
				
			$.ajax({
			type: "POST",
			url: "{{url('/cart/decrease')}}",
			data: frm_val,
		   dataType: 'json',
				success: function(msg) {			
					
					$("#hcrtcount").text(msg.cart_count);

					$("#promo_list_cal").val(msg.promo_list);
					$("#food_promo_list_cal").val(msg.food_promo_list_cal);
					$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
					
					
					
			 	var subtotal = msg.total_amt;
				
				
				
				var discount_val = 0;
				var tax_calculation = msg.service_tax;
				
					var total_rest_promo = $("#total_promo").val();
					
					
					var discount_val = msg.discount_amt;					
					 $("#total_promo_amt").val(discount_val);
					  $("#discount_amt").html('$'+discount_val);
					  //$("#show_offer_text").html('('+msg.discount_txt+')');		
					  
					  
					  if(msg.discount_txt=='') {
					  	//$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }
					  
					  		 		
					  $("#promo_discount_text").val(msg.discount_txt);
					  
					 $("#promo_ids_val").val(msg.order_offer_on);			
					if(isNaN(dal_charg)){

					dal_charg = dal_charg.replace(/[^0-9.]/g, "");
					
				}
					var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
				
				
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				if((subtotal-discount_val)<del_min_charg)
				{
					$('#del_fee_min').show();


					var dsubtotal = subtotal-discount_val;
						
					var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);
					
					//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
					remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
					
					if(remaing_min_del_charg<=0)
					{
						$('#del_fee_min').hide();
					}
				  
				}
				else if((subtotal-discount_val)>del_min_charg)
				{
					$('#del_fee_min').hide();
				}
			 	
				$('#delivery_fee_min_span').html(remaing_min_del_charg);
				$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
			 
				
				$('#order_list').html(msg.cart_view);
				$('#Subtotal_amt').html('$'+msg.total_amt);
				$('#total_amt').html('$'+Total);
				grandTotal(Total);
				$('#total_charge').val(Total);
				$('input[name=radio]').trigger('change');
				
				
				
					$('#service_tax_amt').html('$'+tax_calculation);					
					$('#service_tax').val(tax_calculation);
					
				//$('#sub_total').val((subtotal).toFixed(2));
				
				if(subtotal>0){
				$('#sub_total').val((subtotal).toFixed(2));
				}
				else
				{
				$('#sub_total').val('0.00');
					
				}
				
				
						
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}


				
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					
			
				 $("#ajax_favorite_loddder").hide();
				 	
					$("#update_sucess_msg").show();					
					$("#update_sucess_msg").fadeOut(10000); 
				}
			});

		}		
});



$(document).on('click', '#plase_order', function(){

	var s_total = parseFloat($('#sub_total').val());
	var m_order_total = parseFloat($('#mini_order_amt').val());
	var selected_lang = $('#selected_lang').val();
	
	var selected_option = $('.rest_option:checked').val();
	$('input[name=option]').trigger('change');
	
	if(($('#sub_total').val()==0))
	{	
		if(selected_lang == 'ar')              
		{
		    $('#alert_model_msg').html('يرجى اختيار البند للطلب');
		} else {
		    $('#alert_model_msg').html('Please Select Item for Order');  
		}
					
		$('#message_model').modal('show');
		return false;
	}
	else if((s_total<m_order_total) && (selected_option=='Delivery'))
	{
		if(selected_lang == 'ar')              
		{
		    $('#alert_model_msg').html('الحد الأدنى لمبلغ الطلب للتسليم هو $'+$('#mini_order_amt').val());
		} else {
		    $('#alert_model_msg').html('The minimum order amount for delivery is $'+$('#mini_order_amt').val());
		}			
			
		$('#message_model').modal('show');
		return false;
	}
	else{
	
	
	if($('.rest_option').is(':checked'))
	{
		
		if($('.rest_option:checked').val()=='Delivery')
		{
			
			 $("#del_fee").show();
			 $("#del_fee_min").show();		
			 
			 $("#div_service_area").show();	
			 
		}
		else
		{			
			 $("#div_service_area").hide();	
			 $("#del_fee").hide();	
			 $("#del_fee_min").hide();	
			 	
			 	
		}
	}
	var form = $("#order_frm");
	form.validate();
	var valid =	form.valid();
	
	
	
		if(valid)
		{
		
			$('#order_frm').submit();
			
				/*if($('.rest_option:checked').val()=='Delivery')
				{
				  
				
					if($('#service_area').val()>0){
					
						if($('#delivery_fee_min_remaing').val()>0)
						{
							 $('#min_charge').html($('#delivery_fee_min').val());
							 $('#subtotal_min_charge').html($('#Subtotal_amt').text());
							 $('#reman_min_charge').html($('#delivery_fee_min_remaing').val());
							 
							 $('#devlivery_message_model').modal('show');
							return false;
						}
						else
						{
						  $('#order_frm').submit();
						} 
					}
					else
					{
						$('#alert_model_msg').html('Please Select Delivery Location');			
						$('#message_model').modal('show');
						return false;
					}
				}
				else if($('.rest_option:checked').val()=='Dine-in')
				{
					
					if(($('#table_pic').val()!='') && ($('#table_time').val()!='') && 
						($('#table_date').val()!='') )
					{
						 $('#order_frm').submit();
					}
					else
					{		
						$('#dinein_model').modal('show');
						return false;
					}
				}
				else{
				$('#order_frm').submit();
			}
*/		}
		else
		{
			return false;
		}
			
	}
});
	
var order_type="";
//$(document).on('ifChanged', '.rest_option', function(){
$(document).on('change', '.rest_option', function(){

	if($('.rest_option').is(':checked'))
	{
		if($('.rest_option:checked').val()=='Delivery')
		{			
			var dal_charg = 0;
			var del_min_charg=0;
			var remaing_min_del_charg = 0;
			order_type="Delivery";
			var subrub = '{{ Request::segment(1) }}'; 
			var title = '{{ Request::segment(2) }}'; 
			 		
			$.ajax({
			type: "POST",
			url: "{{url('/get_delivery')}}",
			data: {'subrub':subrub,'title':title},
			async: false,
		   	dataType: 'json',
				success: function(data) {
					
					delivery_charge = data.rest_mindelivery;
					
					$("#delivery_fee").val(delivery_charge);
				 	$(".delivery_fee").html('$'+delivery_charge);
				}, 
				error: function(error){
					console.log(error);
					
				}
			});

				var newValue = $("#Subtotal_amt").text().replace('$','');
				var value = $("#delivery_fee").val();
				if(isNaN(value)){

					value = value.replace(/[^0-9.]/g, "");
					
				}	
				var discount_val = 0;
				var tax_calculation = $('#service_tax').val();
				var discount_val = $("#total_promo_amt").val();	
		
				var total_rest_promo = $("#total_promo").val();
					
				//var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
				
				var Total = (parseFloat(newValue)+parseFloat(tax_calculation)+parseFloat(value)-parseFloat(discount_val)).toFixed(2);
		
		
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				//var	dal_charg = $("#delivery_fee").val();
				var del_min_charg = $("#delivery_fee_min").val();
			
				if((newValue-discount_val)<del_min_charg)
				{
					$('#del_fee_min').show();
					
				
					var dsubtotal = newValue-discount_val;
						
					var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(del_min_charg)).toFixed(2);

					//var Total = (parseFloat(del_min_charg)).toFixed(2);

					remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(newValue)-parseFloat(discount_val)).toFixed(2);
					
					if(remaing_min_del_charg<=0)
					{
						$('#del_fee_min').hide();
					}
				  
				}
				else if((newValue-discount_val)>del_min_charg)
				{
						$('#del_fee_min').hide();
				}
			 	
				$('#delivery_fee_min_span').html(remaing_min_del_charg);
				$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				 //$("#delivery_fee").val(value);	
				 $(".delivery_fee").html('$'+value);				
				 $('#total_amt').html('$'+Total);
				grandTotal(Total);
				 $('#total_charge').val(Total);
				$('input[name=radio]').trigger('change');
			
				/*CALCULATE AMOUNT END */

				 $("#dinein_info_div").hide();
				 $("#div_service_area").show();	
				 $("#del_fee").show();		
				 $("#service_area").attr("required","required");
				
				
				 $("#del_fee_min").show();	
			 
			 
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
		}
		else
		{	
					
			$("#div_service_area").hide();			 
			$("#dinein_info_div").hide();
			$("#del_fee").hide();	
			$("#del_fee_min").hide();
			order_type="Pickup";	


			$('#service_area').prop('selectedIndex',0);	

			var newValue = $("#Subtotal_amt").text().replace('$','');
			 
			 
			 
			var tax_calculation = $('#service_tax').val();
			var discount_val = $("#total_promo_amt").val();	




			var total_rest_promo = $("#total_promo").val();


			var Total = (parseFloat(newValue)+parseFloat(tax_calculation)+parseFloat(0)-parseFloat(discount_val)).toFixed(2);

			$("#delivery_fee").val('0');	
			$(".delivery_fee").html('$0');				
			$('#total_amt').html('$'+Total);
			grandTotal(Total);
			$('#total_charge').val(Total);
			$('input[name=radio]').trigger('change');





			/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/

			var total_amt = Total;
			var amt_percent = $('#partical_percent').val();

			var get_payout_amt = 0; 	
			var get_payout_amt_remaining = 0; 
					
					
			if($('#chkPassport').is(':checked'))
			{
		
				get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
				
				get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
				
				$('#partical_amt').val(get_payout_amt);
				$('#partical_remaning_amt').val(get_payout_amt_remaining);
				
				$('#payout_partial').html(get_payout_amt);
				$('#payout_partial_remaining').html(get_payout_amt_remaining);
			}
			else
			{
				$('#partical_amt').val(get_payout_amt);
				$('#partical_remaning_amt').val(get_payout_amt_remaining);
				
				$('#payout_partial').html(get_payout_amt);
				$('#payout_partial_remaining').html(get_payout_amt_remaining);
			}
			/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
		}
	}

});

$(document).on('change', '#service_area1', function(){

 var value = '';
   value = $(this).find('option:selected').attr("langid");
  
	if($.isNumeric(value))
	{
		var newValue = $("#Subtotal_amt").text().replace('$','');
		
		
		 
		 var discount_val = 0;
		
		/*if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
		{
			if($("#promo_mode").val()=='$')
			{
				discount_val = $("#promo_value").val();
				$('#promo_amt').html('-$'+discount_val);
				$('#promo_amt_cal').val(discount_val);
			}
			if($("#promo_mode").val()=='%')
			{
				var promo_val = $("#promo_value").val();
				discount_val =((parseFloat(newValue)*parseFloat(promo_val))/100).toFixed(2);
				
				$('#promo_amt').html('-$'+discount_val);
				$('#promo_amt_cal').val(discount_val);
			}
		}*/
		
		
					var total_rest_promo = $("#total_promo").val();
					
					
					
					if(total_rest_promo>0)
					{	
						var counter_dicount = 0;	
						
						$(".cart_sub_total").val(newValue);
						
//						cart_sub_total_
								
						for(var p = 1; p<=total_rest_promo; p++)
						{
						
							var each_discount_val = 0;							
							
							var subtotal_promo = 0;
							
							
							
							
							if(($("#promo_mode_"+p).val()=='%') || ($("#promo_mode_"+p).val()=='$'))
							{
								var promo_buy = $("#promo_buy_"+p).val();
								if((promo_buy==0) || (promo_buy<=newValue))
								{
									
									subtotal_promo =$("#cart_sub_total_"+p).val()
									
									if($("#promo_mode_"+p).val()=='$')
									{
										each_discount_val = $("#promo_value_"+p).val();
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									if($("#promo_mode_"+p).val()=='%')
									{
										var promo_val = $("#promo_value_"+p).val();
										
										each_discount_val =((parseFloat(subtotal_promo)*parseFloat(promo_val))/100).toFixed(2);
										
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									
									
									discount_val = (parseFloat(discount_val)+parseFloat(each_discount_val)).toFixed(2);
								}	
							}	
						}				
					}
				
		
				
		var Total = (parseFloat(newValue)+parseFloat(value)-parseFloat(discount_val)).toFixed(2);
		
		
		/*  MANAGE MINIMUM DELIVERY CHARGE END */
		
		var del_min_charg = $("#delivery_fee_min").val();
		if(((parseFloat(newValue))-(parseFloat(discount_val)) ) < parseFloat(del_min_charg))
		{
			 Total = (parseFloat(del_min_charg)+parseFloat(value)).toFixed(2);			
		}	
		
		/*  MANAGE MINIMUM DELIVERY CHARGE END */



		
		

		 $("#delivery_fee").val(value);	
		 $(".delivery_fee").html('$'+value);				
		 $('#total_amt').html('$'+Total);
		// grandTotal(Total);
		 $('#total_charge').val(Total);
	}
	else
	{
		if($("#delivery_fee").val()>0)
		{
		
		
				
				var discount_val = 0;
				
				/*if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
				{
					if($("#promo_mode").val()=='$')
					{
						discount_val = $("#promo_value").val();
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
					if($("#promo_mode").val()=='%')
					{
						var promo_val = $("#promo_value").val();
						//alert(promo_val);
						//alert((parseFloat(subtotal)*parseFloat(promo_val))/100);
						
						discount_val =((parseFloat(subtotal)*parseFloat(promo_val))/100).toFixed(2);
						
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
				}*/
				
				
					var total_rest_promo = $("#total_promo").val();
					
					
					
					if(total_rest_promo>0)
					{	
						var counter_dicount = 0;	
						
						$(".cart_sub_total").val(newValue);
						
//						cart_sub_total_
								
						for(var p = 1; p<=total_rest_promo; p++)
						{
						
							var each_discount_val = 0;							
							
							var subtotal_promo = 0;
							
							
							
							
							if(($("#promo_mode_"+p).val()=='%') || ($("#promo_mode_"+p).val()=='$'))
							{
								var promo_buy = $("#promo_buy_"+p).val();
								if((promo_buy==0) || (promo_buy<=subtotal))
								{
									
									subtotal_promo =$("#cart_sub_total_"+p).val()
									
									if($("#promo_mode_"+p).val()=='$')
									{
										each_discount_val = $("#promo_value_"+p).val();
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									if($("#promo_mode_"+p).val()=='%')
									{
										var promo_val = $("#promo_value_"+p).val();
										
										each_discount_val =((parseFloat(subtotal_promo)*parseFloat(promo_val))/100).toFixed(2);
										
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									
									
									discount_val = (parseFloat(discount_val)+parseFloat(each_discount_val)).toFixed(2);
								}	
							}	
						}				
					}
				
				
				
				
			var del_new_val = $("#delivery_fee").val();	
			var total_new_val = $("#total_charge").val();
			
			
			
			var cal_total_val =  (parseFloat(total_new_val)-parseFloat(del_new_val)).toFixed(2);
			
			
			
			
			
			
			$("#delivery_fee").val('0');	
		 	$(".delivery_fee").html('$0');
							
			$('#total_amt').html('$'+cal_total_val);
			$('#total_charge').val(cal_total_val);
				
		}	
	}
		
});




		  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

/*RESET DINEIN MODEL VALUE */
$('#dinein_model').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select,hidden")
       .val('')
       .end()
});
$(document).on('click', '#submit_with_min_charge', function(){


 $('#devlivery_message_model').modal('hide');


$('#order_frm').submit();
});

function submit_continue()
{

	var form = $('#addon_frm');
	form.validate();
	var valid =	form.valid();
	if(valid)
	{
		var restitem = $(this).attr('data-restitem');
		 
		var tax_percent = $('#rest_service_tax').val();
		//var delivery_fee = $('#delivery_fee').val();	 
		 var dal_charg = 0;
		 var del_min_charg=0;
		 var remaing_min_del_charg = 0;
		 //var orderType="";
		 $('input[name=option]').trigger('change');
		 if($('.rest_option').is(':checked'))
		 {
			if($('.rest_option:checked').val()=='Delivery')
			{
				dal_charg = $("#delivery_fee").val();
				del_min_charg = $("#delivery_fee_min").val();
				//orderType = order_type;
			}
		 }
		// alert(order_type);
		 if(isNaN(dal_charg)){
		 	
			dal_charg = dal_charg.replace(/[^0-9.]/g, "");
					
		}
		$('#other-items').modal('hide');
		
		$("#ajax_favorite_loddder").show();
		
		var frm_val = $('#addon_frm').serialize()+'&rest_tax='+tax_percent+'&rest_order_type='+order_type;
		console.log(frm_val);

		//alert(frm_val);

			$.ajax({
			type: "POST",
			url: "{{url('/cart')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) { 
			
                    $("#hcrtcount").text(msg.cart_count);

				    console.log(msg);
					if(msg.cart_status==2)
					{
					
					$("#promo_list_cal").val(msg.promo_list);
					$("#food_promo_list_cal").val(msg.food_promo_list_cal);
					$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
										
				 $("#ajax_favorite_loddder").hide();	
				
					var subtotal = msg.total_amt;
					var offer_on = msg.order_offer_on;
					var discount_val = 0;
					var tax_calculation = msg.service_tax;					
					var total_rest_promo = $("#total_promo").val();					
					
					var discount_val = msg.discount_amt;					
					 $("#total_promo_amt").val(discount_val);
					  $("#discount_amt").html('$'+discount_val);
					 // $("#show_offer_text").html('('+msg.discount_txt+')');
					  
					  if(msg.discount_txt=='') {
					  	//$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }
					  $("#promo_discount_text").val(msg.discount_txt);
					  			 										
					var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
								
					 $("#promo_ids_val").val(msg.order_offer_on);
												
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();


						var dsubtotal = subtotal-discount_val;
						
						var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);
						
						//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
						$('#del_fee_min').hide();
					}
					
					$('#delivery_fee_min_span').html(remaing_min_del_charg);
					$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
					
					/*  MANAGE MINIMUM DELIVERY CHARGE END */			
					
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);
					
					 
					$('#service_tax_amt').html('$'+tax_calculation);					
					$('#service_tax').val(tax_calculation);
					
											
					$('#total_amt').html('$'+Total);
					//$('.grand_total_amt').html(Total);
					 grandTotal(Total);
					 $('#total_charge').val(Total);
					$('input[name=radio]').trigger('change');
					
					$('#sub_total').val((msg.total_amt).toFixed(2));
					
					
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}

					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					$("#add_sucess_msg").show();	
				/*setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);*/
				
				
				
				$("#add_sucess_msg").fadeOut(10000); 
					
				}
					if(msg.cart_status==1)
					{
					
					
					$("#ajax_favorite_loddder").hide();	
						$('#my_order_confrim_model').modal('show');
						
			$(document).on('click', '.get_orderconf_val', function(){
			
							confrim = $(this).attr('data-val');
							if(confrim==1)
							{
								
								 $('#my_order_confrim_model').modal('hide');
							
									$("#ajax_favorite_loddder").show();
									var frm_val = $('#addon_frm').serialize()+'&rest_tax='+tax_percent+'&delete_oldcart=1';
									$.ajax({
									type: "POST",
									url: "{{url('/cart')}}",
									data: frm_val,
									dataType: 'json',
										success: function(msg) {
											
					
					$("#promo_list_cal").val(msg.promo_list);
					$("#food_promo_list_cal").val(msg.food_promo_list_cal);
					$("#food_promo_applied_cal").val(msg.food_promo_applied_cal);
					
					
				 $("#ajax_favorite_loddder").hide();	
				
					var subtotal = msg.total_amt;
					var offer_on = msg.order_offer_on;
					var discount_val = 0;
					var tax_calculation = msg.service_tax;
					
					var total_rest_promo = $("#total_promo").val();
					
					
					var discount_val = msg.discount_amt;					
					 $("#total_promo_amt").val(discount_val);
					  $("#discount_amt").html('$'+discount_val);
					 // $("#show_offer_text").html('('+msg.discount_txt+')');
					  
					 
					  
					  if(msg.discount_txt=='') {
					  	$("#show_offer_text").html(''+msg.discount_txt+'');
					  }
					  else
					  {					 
					  	$("#show_offer_text").html('('+msg.discount_txt+')');
					  }
					  $("#promo_discount_text").val(msg.discount_txt);
					  
				 					
					
					var Total = (parseFloat(subtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
								
					 $("#promo_ids_val").val(msg.order_offer_on);
					
					
				
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();
						

						var dsubtotal = subtotal-discount_val;
						
						var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);

						//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
						$('#del_fee_min').hide();
					}
					
					$('#delivery_fee_min_span').html(remaing_min_del_charg);
					$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
					
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);
					
					 
					$('#service_tax_amt').html('$'+tax_calculation);					
					$('#service_tax').val(tax_calculation);
					
					
					
					
					$('#total_amt').html('$'+Total);
					//grandTotal(Total);
					//$('.grand_total_amt').html('$'+(Total).toFixed(2));
					$('#total_charge').val(Total);
					$('#sub_total').val((msg.total_amt).toFixed(2));
					
					
					/********************************  IF PARTICAL PAYEMNT IS SELECT START  ******************************/
				
					var total_amt = Total;
					var amt_percent = $('#partical_percent').val();
					
					var get_payout_amt = 0; 	
					var get_payout_amt_remaining = 0; 
					
					
						if($('#chkPassport').is(':checked'))
						{
					
							get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
							
							get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
							
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}
						else
						{
							$('#partical_amt').val(get_payout_amt);
							$('#partical_remaning_amt').val(get_payout_amt_remaining);
							
							$('#payout_partial').html(get_payout_amt);
							$('#payout_partial_remaining').html(get_payout_amt_remaining);
						}


				
					
					/*********************************** IF PARTICAL PAYEMNT IS SELECT END  ***************************/
					
					
					
					
					
					
					
					
					$("#add_sucess_msg").show();	
				/*setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);*/
				
				
				
				$("#add_sucess_msg").fadeOut(10000); 
					
				
										}
									});	
									
					
							}	
							else if(confrim==0)
							{
								 $('#my_order_confrim_model').modal('hide');
							}
						});	
						
					}
				
				}
			});
	}
}


function test()
{
	//alert('reena');
	
	var form = $('#addon_frm');
	form.validate();
	var valid =	form.valid();
	//alert(valid);
	
	
	
	if(valid)
	{
			 
	  
		 var restitem = $(this).attr('data-restitem');
			
			
			 
		 var dal_charg = 0;
		 var del_min_charg=0;
		 var remaing_min_del_charg = 0;
		 
		 if($('.rest_option').is(':checked'))
		 {
			if($('.rest_option:checked').val()=='Delivery')
			{
				dal_charg = $("#delivery_fee").val();
				del_min_charg = $("#delivery_fee_min").val();
			}
		 }
		 
						
			
			$('#other-items').modal('hide');
			
			$("#ajax_favorite_loddder").show();	
			
			
			var frm_val = $('#addon_frm').serialize();
			$.ajax({
			type: "POST",
			url: "{{url('/cart')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) {			
					
		  
		  
				 $("#ajax_favorite_loddder").hide();	
				
					var subtotal = msg.total_amt;
					var discount_val = 0;
					
					var total_rest_promo = $("#total_promo").val();
					
					
					
					if(total_rest_promo>0)
					{	
						var counter_dicount = 0;	
						
						$(".cart_sub_total").val(subtotal);
						
			//						cart_sub_total_
								
						for(var p = 1; p<=total_rest_promo; p++)
						{
						
							var each_discount_val = 0;							
							
							var subtotal_promo = 0;
							
							
							
							
							if(($("#promo_mode_"+p).val()=='%') || ($("#promo_mode_"+p).val()=='$'))
							{
								var promo_buy = $("#promo_buy_"+p).val();
								if((promo_buy==0) || (promo_buy<=subtotal))
								{
									
									subtotal_promo =$("#cart_sub_total_"+p).val()
									
									if($("#promo_mode_"+p).val()=='$')
									{
										each_discount_val = $("#promo_value_"+p).val();
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									if($("#promo_mode_"+p).val()=='%')
									{
										var promo_val = $("#promo_value_"+p).val();
										
										each_discount_val =((parseFloat(subtotal_promo)*parseFloat(promo_val))/100).toFixed(2);
										
										$('#promo_amt_'+p).html('-$'+each_discount_val);
										$('#promo_amt_cal_'+p).val(each_discount_val);
										
										if(p<total_rest_promo)
										{
											var pp = (parseFloat(p)+parseFloat(1));
											var aa = (parseFloat(subtotal_promo)-parseFloat(each_discount_val)).toFixed(2);
											$('#cart_sub_total_'+pp).val(aa);
										}
									}
									
									
									discount_val = (parseFloat(discount_val)+parseFloat(each_discount_val)).toFixed(2);
								}	
							}	
						}				
					}
					
					
					var Total = (parseFloat(subtotal)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
					
				
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();
						

						var dsubtotal = subtotal-discount_val;
						
						var Total = (parseFloat(dsubtotal)+parseFloat(tax_calculation)+parseFloat(dal_charg)).toFixed(2);

						//var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}
					
					$('#delivery_fee_min_span').html(remaing_min_del_charg);
					$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
					
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);
					
					
					$('#total_amt').html('$'+Total);
					//grandTotal(Total);
					$('#total_charge').val(Total);
					$('#sub_total').val((msg.total_amt).toFixed(2));
					
					
					$("#add_sucess_msg").show();	
				/*setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);*/
				
				
				
				$("#add_sucess_msg").fadeOut(10000); 
					
				}
			});
			
			
	}
}


			<?php /*?>
		$(document).on('click', '#addon_continue', function(){ 	
	
		var form = $('#addon_frm');
			form.validate();
			var valid =	form.valid();
			
			
			if(valid)
			{
			 
	  
			var restitem = $(this).attr('data-restitem');
			
			
			 
		 var dal_charg = 0;
		 var del_min_charg=0;
		 var remaing_min_del_charg = 0;
		 
		 if($('.rest_option').is(':checked'))
		 {
			if($('.rest_option:checked').val()=='Delivery')
			{
				dal_charg = $("#delivery_fee").val();
				del_min_charg = $("#delivery_fee_min").val();
			}
		 }
		 
						
			
			$('#other-items').modal('hide');
			
			$("#ajax_favorite_loddder").show();	
			
			
			var frm_val = $('#addon_frm').serialize();
			$.ajax({
			type: "POST",
			url: "{{url('/cart')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) {			
					
				 $("#ajax_favorite_loddder").hide();	
				
				
				
				
					var subtotal = msg.total_amt;
					var discount_val = 0;
					
					if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
					{
						if($("#promo_mode").val()=='$')
						{
							discount_val = $("#promo_value").val();
							$('#promo_amt').html('-$'+discount_val);
							$('#promo_amt_cal').val(discount_val);
						}
						if($("#promo_mode").val()=='%')
						{
							var promo_val = $("#promo_value").val();
							
							discount_val =((parseFloat(subtotal)*parseFloat(promo_val))/100).toFixed(2);
							
							$('#promo_amt').html('-$'+discount_val);
							$('#promo_amt_cal').val(discount_val);
						}
					}
					
					
					var Total = (parseFloat(subtotal)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
					
				
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					/*
					if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();
						
						var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}
					
					$('#delivery_fee_min_span').html(remaing_min_del_charg);
					$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
					
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					/*
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);
					
					
					$('#total_amt').html('$'+Total);
					$('#total_charge').val(Total);
					$('#sub_total').val((msg.total_amt).toFixed(2));
					
					
					$("#add_sucess_msg").show();	
				setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);
					
				}
			});
			
			
	}
	
});<?php */?>

$(document).on('change', '#chkPassport', function(){ 	

	
	
	var total_amt = $('#total_charge').val();
	var amt_percent = $('#chkPassport').val();
	
	var get_payout_amt = 0; 	
	var get_payout_amt_remaining = 0; 
	
	
		if($('#chkPassport').is(':checked'))
		{
	
			get_payout_amt =((parseFloat(total_amt)*parseFloat(amt_percent))/100).toFixed(2);
			
			get_payout_amt_remaining = (parseFloat(total_amt)-parseFloat(get_payout_amt)).toFixed(2);
			
			$('#partical_amt').val(get_payout_amt);
			$('#partical_remaning_amt').val(get_payout_amt_remaining);
			
			$('#payout_partial').html(get_payout_amt);
			$('#payout_partial_remaining').html(get_payout_amt_remaining);
		}
		else
		{
			$('#partical_amt').val(get_payout_amt);
			$('#partical_remaning_amt').val(get_payout_amt_remaining);
			
			$('#payout_partial').html(get_payout_amt);
			$('#payout_partial_remaining').html(get_payout_amt_remaining);
		}

	

});


$(document).ready(function(e) {
    var $input = $('#refresh');

    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
   $('input[name=option]').trigger('change');
});



</script>
    <script>
        wow = new WOW(
          {
            animateClass: 'animated',
            offset:       100
          }
        );
        wow.init();
    </script>

	
	
	
@stop