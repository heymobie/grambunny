@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Place your order</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section>
<!-- End section -->
<!-- End SubHeader ============================================ -->


 

<!-- Content ================================================== -->

<div class="container margin_60_35">
<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="box_style_2">
				<h2 class="inner">Order received!</h2>
				<div id="confirm">
					<i class="icon_check_alt2"></i>
					<h3>Thank you!</h3>
					<?php 
					
					$content_detail  = DB::table('pages')->where('page_id', '=' ,'9')->where('page_status', '=' ,'1')->get();	
					if($content_detail)
					{
						echo $content_detail[0]->page_content;
					}
					
					?>
				</div>
				<h4>Summary</h4><h4>Order Id ({{$order_detail[0]->order_id}})({{$order_detail[0]->order_uniqueid}})</h4>
				<table class="table table-striped nomargin">
				<tbody>
					<?php $cart_price = 0;?>
		@if(isset($cart) && (count($cart)))
				
				 @foreach($cart as $item)
				<?php  $cart_price = $cart_price+$item->price;?>
				<tr>
					<td>
						<strong>{{$item->qty}}x</strong> {{$item->name}} 
					</td>
					<td>
						<strong class="pull-right">${{number_format($item->price,2)}}</strong>
					</td>
				</tr>				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $cart_price = $cart_price+$addon['price'];?>
					  
				<tr>
					<td>
						 {{$addon['name']}}
					</td>
					<td>
						<strong class="pull-right">@if($addon['price']>0)${{number_format($addon['price'],2)}}@endif</strong>
					</td>
				</tr>
				
					 
					 @endforeach
				 @endif 
					
				 @endforeach
@endif


				<tr>
                  <td> Subtotal </td>
				  <td> <span class="pull-right">${{number_format($order_detail[0]->order_subtotal_amt,2)}}</span> </td>
                </tr>
				
				
				@if($order_detail[0]->order_promo_cal>0)
			
                <tr>
                  <td>Discount</td>
				  <td> <span class="pull-right"> -${{number_format($order_detail[0]->order_promo_cal,2)}}</span> </td>
                </tr>
				
				@endif
				
				
		
				@if($order_detail[0]->order_service_tax>0)
			
                <tr>
                  <td> Sales Tax </td>
				  <td> <span class="pull-right">${{number_format($order_detail[0]->order_service_tax,2)}}</span> </td>
                </tr>
				
				@endif
				@if($delivery_fee>0)
				<tr>
                  <td> Delivery Fee </td>
				  <td> <span class="pull-right">${{number_format($delivery_fee,2)}}</span> </td>
                </tr>
                @endif

				@if($order_detail[0]->order_remaning_delivery>0)
				
					
				<tr>
                  <td>   ${{number_format($order_detail[0]->order_min_delivery,2)}} min 
				  </td>
					<td>
						Remaining<span class="pull-right">${{number_format($order_detail[0]->	order_remaning_delivery,2)}}</span>
					</td>
                </tr>
				@endif
				
								
			<?php 
			
			if($order_detail[0]->order_pmt_type!='1')
			{
			?>
			<tr>
			<td>Partial Payment (Paid)</td>
			<td><span class="pull-right"> ${{number_format($order_detail[0]->order_partial_payment,2)}}</span> </td>
			</tr>
			<tr>
			<td>Partial Remaning Payment</td>
			<td><span class="pull-right">${{number_format($order_detail[0]->order_partial_remain,2)}}</span> </td>
			</tr>
			
			<?php 
			}
			
			?>

		
				
				<tr>
					<td class="total_confirm">
						 TOTAL
					</td>
					<td class="total_confirm">
					
					<?php
					$promo_amt_cal= '0.00';
					
					
					
					if($order_detail[0]->order_promo_cal>0)
					{
						 $promo_amt_cal= $order_detail[0]->order_promo_cal;
					}
					
					
					$total_amt= $cart_price+$delivery_fee-$promo_amt_cal+$order_detail[0]->order_service_tax;
					
					
						
					if(($order_detail[0]->order_remaning_delivery>0) && 
						($order_detail[0]->order_min_delivery>0) && 
						(($cart_price-$promo_amt_cal)<$order_detail[0]->order_min_delivery)
					  )
					{
						$total_amt= ($order_detail[0]->order_min_delivery+$delivery_fee);
					}
					elseif(($order_detail[0]->order_remaning_delivery>0) && 
						($order_detail[0]->order_min_delivery>0) && 
						(($cart_price-$promo_amt_cal)>$order_detail[0]->order_min_delivery)
					  )
					{
						$total_amt= $cart_price+$delivery_fee-$promo_amt_cal;
					}
				?>
				
						<span class="pull-right">${{number_format($total_amt,2)}}</span>
					</td>
				</tr>
				<tr>
                  <td> Tip Amount</td>
				  <td> <span class="pull-right">${{number_format($order_detail[0]->order_tip,2)}}</span> </td>
                </tr>
				<!-- <tr>
                  
                  <td class="total"> Tip 
                     <input type="text" name="tip" id="tip" value="${{number_format($order_detail[0]->order_tip,2)}}" class="form-control" readonly />
          
          
                  </td>
                </tr> -->
                <tr>
					<td class="total_confirm">
						GRAND TOTAL
					</td>
					<td class="total_confirm">
					
					<?php
						$tip = number_format($order_detail[0]->order_tip,2);			
					$grand_total_amt = $total_amt+ $tip;
					
				?>
				
						<span class="pull-right">${{number_format($grand_total_amt,2)}}</span>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>	
	<!-- End row -->
	
</div><!-- End container -->
<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	
@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>

@stop	