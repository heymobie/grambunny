@extends('layouts.app')

@section('content')




    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_short.jpg" data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
             <h1>{{$page_detail[0]->page_title}}</h1>
             <!--<p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>-->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End Header video -->
    <!-- End SubHeader ============================================ -->
	
<div id="position">
        <div class="container">
            <ul>
               <li><a href="{{ url('/') }}">Home</a></li>
                <li>contact us</li>
            </ul>
        </div>
    </div>
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
	<div class="row" id="contacts">
	
	<!--<div class="col-md-12">-->
		@if($page_detail[0]->page_status)
					{!! $page_detail[0]->page_content !!}
				@endif
	<!--</div>	-->		
				
   
    </div><!-- End row -->
</div>
	
	
@stop

@section('js_bottom')

<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>

<script type="text/javascript">/* <![CDATA[ */(function(d,s,a,i,j,r,l,m,t){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body>
</html>
@stop
