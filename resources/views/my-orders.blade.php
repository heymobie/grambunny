@extends("layouts.grambunny")
@section("styles")
{{-- styles goes here --}}
<style type="text/css">
	.parallax-window#short {
		height: 230px;
		min-height: inherit;
		background: 0 0;
		position: relative;
		margin-top: 0px;
	}

	section.parallax-window {
		overflow: hidden;
		position: relative;
		width: 100%;
		background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-position: top center;
		background-size: cover;
	}

	#sub_content {
		display: table-cell;
		padding: 50px 0 0;
		font-size: 16px;
	}

	#sub_content h1 {
		margin: 0 0 10px;
		font-size: 28px;
		font-weight: 300;
		color: #F5F0E3;
		text-transform: capitalize;
	}

	#short #subheader {
		height: 230px;
		color: #F5F0E3;
		text-align: center;
		display: table;
		width: 100%;
	}

	div#subheader {
		color: #F5F0E3;
		text-align: center;
		display: table;
		width: 100%;
		height: 380px;
	}

	ul.brad-home {
		padding: 0;
		margin: 0;
		text-align: center;
	}

	.brad-home li {
		display: inline-block;
		list-style: none;
		padding: 5px 10px;
		font-size: 12px;
	}

	#subheader a {
		color: #fff;
	}

	.myOrderActnBox {
		margin-top: 0px;
	}

	.myOrderDtelBtn a {
		border: 1px solid #231f20;
		background-color: #fff;
		color: #231f20;
		font-weight: 700;
		border-radius: 25px;
		padding: 8px 15px;
		min-width: 100px;
		cursor: pointer;
		text-decoration: none;
	}

	.myOrderDtelBtn a:hover {
		background-color: #231f20;
		color: #fff;
	}

	section#short #sub_content {
		/*padding-top: 12px !important;*/
	}

	span.green {
		border: unset !important;
		color: #ff000075;
		border-radius: 3px;
		padding: 2px 9px;
	}

	.view_order_extra a {
    color: #333 !important;
    border: 1PX solid #b7b7b7 !important;
    font-size: 14px;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-weight: bold !important;
    box-shadow: 4px 4px 7px #cdcdcd;
    padding: 11px;
}
	.myOrderDtel label {

    text-align: left;
}

</style>
@endsection
@section("content")
{{-- content goes here --}}

<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">
	<div id="subheader">
		<div id="sub_content" class="animated zoomIn">
			<h1><span class="restaunt_countrt">Order History</span></h1>
			<!-- <div class="" id="livechat"><p class="checklogin"><a style="cursor: pointer;"><i class="">Live Chat</i> </a></p></div> -->
			<div id="position">
				<div class="container">
					<ul class="brad-home">
						<li><a href="https://www.grambunny.com/home">Home</a></li>
						<li>My Order </li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="myOrderSec section-full">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="myOdrersBox">
					<h2>All Order Details</h2>

					<?php foreach ($order_detail as $key => $value) { ?>

					<?php $userinfo = DB::table('users')->where('id', '=', $value->user_id)->first(); ?>

						<?php //print_r($order_detail);?>

						<?php $merchantinfo = DB::table('vendor')->where('vendor_id', '=', $value->vendor_id)->first(); ?>

						<?php if (!empty($merchantinfo)) { ?>

							<div class="myOrder oder-detail_pages">

								<div class="myOrderDtelBox">
									<div class="myOrderID">
										<label>Order ID:</label>
										<strong>#{{$value->order_id}}</strong>
									</div>

									<div class="myOrderName">
										<label>Merchant ID:</label>
										<strong>#{{ $merchantinfo->vendor_id }}</strong>
									</div>

									<div class="myOrderDtel">
										<label>Merchant Name</label>
										<strong style="text-transform: capitalize;">{{ $merchantinfo->name }} {{ $merchantinfo->last_name }}</strong>
									</div>

									<div class="myOrderDtel">
										<label>User Name</label>
										<strong style="text-transform: capitalize;">{{ $merchantinfo->username }}</strong>
									</div>

									<!-- 	<div class="myOrderName">
								<label>{{$userinfo->name}} {{$userinfo->lname}}</label>

							</div> -->
									<div class="myOrderDtel">
										<label>Order Total</label>
										<strong>${{$value->total}}</strong>
									</div>
									<!--<div class="myOrderDtel">
								<label>Quantity</label>
								<strong>{{$value->ps_qty}}</strong>
							</div>-->
									<div class="myOrderDtel">
										<label>Placed</label>
										<strong>{{$value->created_at}}</strong>
									</div>
									<div class="myOrderDtel">

										<label>Order Status</label>
										<strong><span class="green">
												@if($value->status==0) Pending @endif
												@if($value->status==1) Accept @endif
												@if($value->status==2) Cancelled @endif
												@if($value->status==3) On the way @endif
												@if($value->status==4) Delivered @endif
												@if($value->status==5) Requested for return @endif
												@if($value->status==6) Return request accepted @endif
												@if($value->status==7) Return request declined @endif
											</span></strong>
									</div>
									<div class="myOrderDtel">
										<label>Delivery Address</label>
										<strong style="text-transform: capitalize;">{{$value->address}}, {{$value->city}}, {{$value->state}}, {{$value->zip}}</strong>
									</div>
									<div class="myOrderActnBox">
										<div class="myOrderDtelBtn view_order_extra">
											<a href="{{ url('/order-details').'/'.$value->id}}"><span><i class="fa fa-eye" aria-hidden="true"></i></span> Order</a>
										</div>
										<!-- <button>Show</button> -->
										<!-- <div class="myOrderDtelBtn view_order_extra">
											Show
										</div> -->

										<!-- <div class="myOrderDtelBtn view_order_extra">
											<span><i class="fa fa-eye" aria-hidden="true"></i></span> Invoice
										</div> -->

								    <?php /* if($value->status>0 && $value->status<4){ ?>

									<div class="myOrderDtelBtn view_order_extra">
										<a href="{{ url('/track-order').'/'.$value->id}}" target="_blank"><span><i class="fa fa-eye" aria-hidden="true"></i></span>Track Order</a>
									</div>

								    <?php } */ ?>

									</div>


								</div>
								<!-- <div class="myOrderDtelBox" style="margin-top: 10px;">
									<table class="" style="border-top: 1px dashed #ccc;">
										<tr>
											<th style="width:30px;">Subtotal</th>
											<th>Discount</th>
											<th>Delivery Fee</th>
											<th>Sales Tax</th>
											<th>Excise Tax</th>
											<th>City Tax</th>
											<th>Total Paid Amount</th>
										</tr>
										<tr>
											<td>{{$value->sub_total}}</td>
											<td>{{ number_format($value->promo_amount, 2) }}</td>
											<td>{{ number_format($value->delivery_fee, 2)}}</td>
											<td>{{ number_format($value->service_tax, 2)}}</td>
											<td>{{ number_format($value->excise_tax, 2)}}</td>
											<td>{{ number_format($value->city_tax, 2)}}</td>
											<td>{{$value->total}}</td>
										</tr>
									</table>
								</div> -->

								<div class="myOrderDtelBox" id="orderdetail" style="margin-top: 10px; display: none;">
									<table class="" style="border-top: 1px dashed #ccc;">
										<tr>
											<!-- <th style="width:30px;">Qty</th> -->
											<th style="width:230px;">Name</th>
											<th style="width:60px;">Price</th>
										</tr>
										<!-- <tr>
											<td>
												<p> &nbsp; &nbsp;
												<?php if (!empty($pservice->name)) { ?>
													{{$value->ps_qty}}
													<?php } ?>
													</p>
											</td>
											<td>
												<p><?php if (!empty($pservice->name)) { ?>
														{{$pservice->name}}
													<?php } ?>
												</p>
											</td>
											<td>
												<p>
													<?php if (!empty($pservice->price)) { ?>
														${{$pservice->price}}
													<?php } ?>
												</p>
											</td>
										</tr> -->


								 		<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Sub Total</label>
											</td>
											<td><span>${{ number_format($value->sub_total, 2) }}</span></td>
										</tr>
										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Discount</label>
											</td>
											<td><span>${{ number_format($value->promo_amount, 2) }}</span></td>
										</tr>

										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Delivery Fee</label>
											</td>
											<td><span>${{ number_format($value->delivery_fee, 2) }}</span></td>
										</tr>
										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Delivery Fee</label>
											</td>
											<td><span>${{ number_format($value->delivery_fee, 2) }}</span></td>
										</tr>

										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Sales Tax</label>
											</td>
											<td><span>${{ number_format($value->service_tax, 2)}}</span></td>
										</tr>

										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Excise Tax</label>
											</td>
											<td><span>${{ number_format($value->excise_tax, 2)}}</span></td>
										</tr>

										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>City Tax</label>
											</td>
											<td><span>${{ number_format($value->city_tax, 2)}}</span></td>
										</tr>

										<tr>
											<!-- <td colspan="1"></td> -->
											<td colspan="1"> <label>Total Paid Amount</label>
											</td>
											<td><span>${{ number_format($value->total, 2) }}</span></td>
										</tr>

									</table>
								</div>
							</div>

					<?php }
					} ?>

				</div>

				<div class="col-12 mt-5 text-center">
					<div class="custom-pagination">
						{{ $order_detail->links() }}
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

@endsection
@section("scripts")
<!-- <script type="text/javascript">
	$("button").click(function(){
		if ($(this).text() == "Show") {
			$("#orderdetail").css('display', 'block');
			$(this).text('Hide');
		}else{
			$("#orderdetail").css('display', 'none');
			$(this).text('Show');
		}
	});
</script> -->
{{-- scripts goes here --}}
@endsection