			<div class="col-md-12">

				<div class="myOdrersBox myOdrersDetailBox">

					<h2>Order detail</h2>



					<div class="row">

					<div class="col-md-4">



					<div class="myOrder">





					<?php foreach ($ps_list as $key => $value) {



                   $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); ?>



						<div class="row">	

                         <div class="col-md-6">

							<div class="myOrderImg">

							<img src="{{ url('/public/uploads/product/'.$pservice->image) }}">

							</div>

						</div>

					   <div class="col-md-6">

					   	<div class="myOrderName deepon">

					   	<strong>{{$pservice->name}}</strong>

					   </div>

                        </br>



                        <hr class="hrline">





											  <div class="myOrderDtel deepmd">

						<label>Price</label>

						<strong>${{$pservice->price}}</strong>

						</div>

						</br>



											   	<div class="myOrderDtel deepmd">

						<label>Quantity</label>

						<strong>{{$value->ps_qty}}</strong>

						</div>





					   </div>

					</div>



					<hr class="hrline">



                     <?php } ?>	



                      </div>



						</div>



						<div class="col-md-8">

							<div class="myOrder ">

								<div class="myOrderDtelBox">

									<div class="myOrderID">

										<label>Order ID</label>

										<strong>#{{$order_detail->order_id}}</strong>

									</div>

					

									<div class="myOrderDtel">

										<label>Order By</label>

										<strong>{{$order_detail->first_name}} {{$order_detail->last_name}}</strong>

									</div>

									<div class="myOrderDtel">

										<label>Contact Number</label>

										<strong>{{$order_detail->mobile_no}}</strong>

									</div>

									<div class="myOrderDtel">

										<label>Delivery Address</label>

										<strong>{{$order_detail->address}}</strong>

									</div>



						           <div class="myOrderDtel">

			                            <label>City</label>

			                            <strong>{{$order_detail->city}} &nbsp;</strong>

			                        </div>

			                         <div class="myOrderDtel">

			                            <label>State</label>

			                            <strong>{{$order_detail->state}} &nbsp;</strong>

			                        </div>



			                        <div class="myOrderDtel">

			                            <label>Zip Code</label>

			                            <strong>{{$order_detail->zip}} &nbsp;</strong>

			                        </div>



			                        <div class="myOrderDtel">

			                            <label>Email</label>

			                            <strong>{{$order_detail->email}} &nbsp;</strong>

			                        </div>



									<div class="myOrderDtel">

										<label>Order Status</label>

										<strong><span class="green">

								@if($order_detail->status==0) Pending @endif

								@if($order_detail->status==1) Accept @endif

								@if($order_detail->status==2) Cancelled @endif

								@if($order_detail->status==3) On the way @endif

								@if($order_detail->status==4) Delivered @endif

								@if($order_detail->status==5) Requested for return @endif

								@if($order_detail->status==6) Return request accepted @endif

								@if($order_detail->status==7) Return request declined @endif





									</span></strong>

									</div>



								<?php if(!empty($order_detail->cancel_reason)){ ?>

								<div class="myOrderDtel">

		                            <label>Cancel Reason</label>

		                            <strong>{{$order_detail->cancel_reason}} &nbsp;</strong>

		                        </div>

		                        <?php } ?>



								<div class="myOrderDtel">

		                            <label>Subtotal</label>

		                            <strong>${{ number_format($order_detail->sub_total, 2) }}</strong>

		                        </div>



		                        <div class="myOrderDtel">

		                            <label>Discount</label>

		                            <strong>${{ number_format($order_detail->promo_amount, 2) }}</strong>

		                        </div>



		                        <div class="myOrderDtel">

		                            <label>Service Tax</label>

		                            <strong>${{ number_format($order_detail->service_tax, 2)}}</strong>

		                        </div>



									<div class="myOrderDtel">

										<label>Order Price</label>

										<strong>${{ number_format($order_detail->total, 2) }}</strong>

									</div>

									<div class="myOrderDtel">

										<label>Payment Type</label>

										<strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong>

									</div>

									<div class="myOrderDtel">

										<label>Transaction ID</label>

										<strong>{{$order_detail->txn_id}}</strong>

									</div>

									<div class="myOrderDtel">

										<label>Payment Status</label>

										<strong>{{$order_detail->pay_status}}</strong>

									</div>



									<div class="myOrderDtel">

										<label>Placed at</label>

										<strong>{{$order_detail->created_at}}</strong>

									</div>



								</div>						

							</div>

						</div>
					</div>
				</div>
			</div>