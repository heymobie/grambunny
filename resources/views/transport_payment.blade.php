@extends('layouts.app')



@section("other_css")





<meta name="_token" content="{!! csrf_token() !!}"/>



<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">

<link rel="apple-touch-icon" type="image/x-icon" href="{{ url('/') }}/design/front/img/apple-touch-icon-57x57-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ url('/') }}/design/front/img/apple-touch-icon-72x72-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ url('/') }}/design/front/img/apple-touch-icon-114x114-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ url('/') }}/design/front/img/apple-touch-icon-144x144-precomposed.png">

<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">

   	  <!-- Radio and check inputs -->

    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>

      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>

      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>

    <![endif]-->

  



@stop

@section('content')





<!-- SubHeader =============================================== -->

<section class="parallax-window"  id="short"  data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">

    	<div id="sub_content">

    	 <h1>Place your order</h1>

            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step complete">

                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                 <a href="{{url('/transport_checkout')}}" class="bs-wizard-dot"></a>

				       <!-- <a href="#" class="bs-wizard-dot" id="back_checkout"></a>-->

                </div>

                               

                <div class="col-xs-4 bs-wizard-step active">

                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                  <a href="#0" class="bs-wizard-dot"></a>

                </div>

            

              <div class="col-xs-4 bs-wizard-step disabled">

                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                  <a href="cart_3.html" class="bs-wizard-dot"></a>

                </div>  

		</div><!-- End bs-wizard --> 

        </div><!-- End sub_content -->

	</div><!-- End subheader -->

</section><!-- End section -->

<!-- End SubHeader ============================================ -->

<div id="position">

        <div class="container">

            <ul>

                <li><a href="{{url('/')}}">Home</a></li>

                 <li><a href="#">DEtail</a></li>

                <li>Payment</li>

            </ul>

        </div>

    </div>

<!-- Position -->

    <!-- Content ================================================== -->

  <div class="container margin_60_35">

	<div id="container_pin">

		<div class="row">

			<div class="col-md-3">

				<div class="box_style_2 hidden-xs info">

					<h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>

					<p>

						Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.

					</p>

					<hr>

					<h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>

					<p>

						Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos.

					</p>

				</div><!-- End box_style_2 -->

                

				<div class="box_style_2 hidden-xs" id="help">

					<i class="icon_lifesaver"></i>

					<h4>Need <span>Help?</span></h4>

					<a href="tel://004542344599" class="phone">+1800123456</a>

					<!--<small>Monday to Friday 9.00am - 7.30pm</small>-->

				</div>

			</div><!-- End col-md-3 -->

            

			<div class="col-md-6">

				<div class="box_style_2">

					<h2 class="inner">Payment methods</h2>

		<?php /*?>			

					<div class="payment_select">

						<label><input type="radio" value="" checked name="payment_method" class="icheck">Credit card</label>

						<i class="icon_creditcard"></i>

					</div>

					<div class="form-group">

						<label>Name on card</label>

						<input type="text" class="form-control" id="name_card_order" name="name_card_order" placeholder="First and last name">

					</div>

					<div class="form-group">

						<label>Card number</label>

						<input type="text" id="card_number" name="card_number" class="form-control" placeholder="Card number">

					</div>

					<div class="row">

						<div class="col-md-6">

							<label>Expiration date</label>

							<div class="row">

								<div class="col-md-6 col-sm-6">

									<div class="form-group">

										<input type="text" id="expire_month" name="expire_month" class="form-control" placeholder="mm">

									</div>

								</div>

								<div class="col-md-6 col-sm-6">

									<div class="form-group">

										<input type="text" id="expire_year" name="expire_year" class="form-control" placeholder="yyyy">

									</div>

								</div>

							</div>

						</div>

						<div class="col-md-6 col-sm-12">

							<div class="form-group">

								<label>Security code</label>

								<div class="row">

									<div class="col-md-4 col-sm-6">

										<div class="form-group">

											<input type="text" id="ccv" name="ccv" class="form-control" placeholder="CCV">

										</div>

									</div>

									<div class="col-md-8 col-sm-6">

										<img src="img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Last 3 digits</small>

									</div>

								</div>

							</div>

						</div>

					</div>

					

					<?php */?>

					<!--End row -->

					<div  id="paypal" class="payment_select islamic2"> <!-- class="icheck"-->												

				  <label class="control control--radio">

				  <input type="radio" id="payment_option" value="" paypal name="payment_method">

				  

                  <div class="control__indicator"></div>

				  </label> Pay with paypal

						

					</div>

					<?php /*?><div class="payment_select nomargin">

						<label><input type="radio" value="" name="payment_method" class="icheck">Pay with cash</label>

						<i class="icon_wallet"></i>

					</div><?php */?>

				</div><!-- End box_style_1 -->

			</div><!-- End col-md-6 -->

            

			<div class="col-md-3" id="sidebar">

            <div class="theiaStickySidebar">

				<div id="cart_box">

					<h3>Your order <i class="icon_cart_alt pull-right"></i></h3>

					<?php $cart_price = 0; $count_number = '1'; $frm_field = '';$total_amt=0;?>

		@if(isset($cart) && (count($cart)))

			<div class="cart_scrllbr">

<div class="table table_summary">	

				

				 @foreach($cart as $item)

				<?php $total_amt= $cart_price = $cart_price+$item->price;?>



				  <div class="crt_itm_cust1">

                  <p> <strong>{{$item->qty}}  x</strong> {{str_replace('_',' ',$item->name)}} </p>

                  <p><strong class="pull-right">${{number_format($item->price,2)}}</strong></p>

                </div>

				

				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))

				 	@foreach($item->options['addon_data'] as $addon)

						

					  <?php $count_number ++; 

					  $total_amt = $cart_price = $cart_price+$addon['price'];

					  ?>

					  <div class="crt_itm_cust23">

					  <p>{{$addon['name']}}</p>

                  	<p><strong class="pull-right">@if($addon['price']>0)${{number_format($addon['price'],2)}}@endif</strong></p>

					</div>



					 @endforeach

				 @endif 

				<?php $count_number++; ?>

				 @endforeach

              </div> 

			  

@if(Session::has('cart_order_detail') && (!empty(Session::has('cart_order_detail'))))	

						

				<div class="table table_summary">			

					<div class="crt_itm_cust1" style=" border-top: 1px solid rgba(128, 128, 128, 0.11);    float: left;   margin-top: 15px;   padding-top: 10px;   width: 100%;">

						<p style="width:100%;float:left !important;">

						

						 Pickup Address

						 Line 1: <?php echo Session::get('picup_info')['pick_address1'];?> <br />

						 <?php if(!empty(Session::get('picup_info')['pick_address2'])){?>

						 Line 2: <?php echo Session::get('picup_info')['pick_address2'];?> <br />

						 <?php }?>

						 Surbur: <?php echo Session::get('picup_info')['pick_suburb'];?> <br />						 						 District: <?php echo Session::get('picup_info')['pick_district'];?> <br />

						 State: <?php echo Session::get('picup_info')['pick_state'];?> <br />

						 PostCode: <?php echo Session::get('picup_info')['pick_pcode'];?> 



						  </p>

					</div>

				</div>			

				<div class="table table_summary">			

					<div class="crt_itm_cust1" style=" border-top: 1px solid rgba(128, 128, 128, 0.11);    float: left;   margin-top: 15px;   padding-top: 10px;   width: 100%;">

						<p style="width:100%;float:left !important;">

						

						 Booking Type: <?php echo str_replace('_',' ',Session::get('picup_info')['pick_bookingtype']);?> <br />

						Booking From : <?php echo date('d-m-Y',strtotime(Session::get('cart_order_detail')['on_date'])).' '.Session::get('cart_order_detail')['on_time'] ?> <br />Booking To : <?php echo date('d-m-Y',strtotime(Session::get('cart_order_detail')['return_date'])).' '.Session::get('cart_order_detail')['return_time'] ?>  </p>

					</div>

				</div>

		@endif

  </div>

@endif

					<hr>

					<!-- Edn options 2 -->

					<table class="table table_summary">

              <tbody>

                <tr>

                  <td> Subtotal <span class="pull-right">${{number_format(Session::get('cart_order_detail')['subtotal'],2)}}</span></td>

                </tr>

				

                <tr>

                  <td> Service fee <span class="pull-right">${{number_format(Session::get('cart_order_detail')['delivery_fee'],2)}}</span></td>

                </tr>	

				

				@if((Session::get('cart_order_detail')['total_nignt']>0))

			

                <tr>

                  <td> Driver Allownce <span class="pull-right">${{number_format(Session::get('cart_order_detail')['driver_allownce'],2)}}</span></td>

                </tr>

				

				@endif

				

				

				@if((Session::get('cart_order_detail')['promo_amt_cal']>0))

			

                <tr>

                  <td> Discount <span class="pull-right">-${{number_format(Session::get('cart_order_detail')['promo_amt_cal'],2)}}</span></td>

                </tr>

				

				@endif

				

				

				

				

				

				

				

				

                <tr>

                  <td class="total"> TOTAL <span class="pull-right">${{number_format(Session::get('cart_order_detail')['total_charge'],2)}}</span></td>

                </tr>

              </tbody>

            </table>

					<hr>

					<a class="btn_full" href="javascript:void(0)" id="oder_confrim">Confirm your order</a>

				</div><!-- End cart_box -->

                </div>

			</div><!-- End col-md-3 -->

            

		</div><!-- End row -->

	</div><!-- End container pin-->

</div>

    <!-- End Content =============================================== -->

@stop

@section('js_bottom')

<?php

$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL

$paypal_id='votive.amit-business@gmail.com'; // Business email ID

//$paypal_id='votiveyogesh@gmail.com'; // Business email ID
 
//$paypal_id='info@shoutaparty.com';// Business email ID

?>

<!--https://www.paypal.com/cgi-bin/webscr-->

<form action="{{$paypal_url}}" method="post" id="payment_frm"><!-- target="_blank"-->



<input type="hidden" name="cmd" value="_cart">

<input type="hidden" name="upload" value="1">

    <input type="hidden" name="userid" value="1">

<input type="hidden" name="business" value="{{$paypal_id}}">

<input type="hidden" name="currency_code" value="US">



<?php $cart_price = 0; $count_number = '1'; ?>

		@if(isset($cart) && (count($cart)))				

				 @foreach($cart as $item)

				

		<input type="hidden" name="item_name_<?php echo $count_number;?>" value="{{$item->name}}">

    <input type="hidden" name="quantity_<?php echo $count_number;?>" value="{{$item->qty}}">

<input type="hidden" name="amount_<?php echo $count_number;?>" value="{{$item->price/$item->qty}}">



				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))

				 	@foreach($item->options['addon_data'] as $addon)

						

					  <?php $count_number ++; $cart_price = $cart_price+$addon['price']; ?>

	<input type="hidden" name="item_name_<?php echo $count_number;?>" value="{{$addon['name']}}">

    <input type="hidden" name="quantity_<?php echo $count_number;?>" value="{{$item->qty}}">

<input type="hidden" name="amount_<?php echo $count_number;?>" value="{{$addon['price']/$item->qty}}">

					 



					 @endforeach

				 @endif 

				<?php $count_number++; ?>

				 @endforeach

				 

						<input type="hidden" name="item_name_<?php echo $count_number;?>" value="Delivery fee">

<input type="hidden" name="amount_<?php echo $count_number;?>" value="{{Session::get('cart_order_detail')['delivery_fee']}}">

						

				 	@if(Session::get('cart_order_detail')['total_nignt']>0)

						<input type="hidden" name="item_name_<?php echo ($count_number+1);?>" value="Driver Allownce">

<input type="hidden" name="amount_<?php echo ($count_number+1);?>" value="{{Session::get('cart_order_detail')['driver_allownce']}}">

						

						@endif

						

						



						 

@endif

@if(Session::get('cart_order_detail')['promo_amt_cal']>0)

<input type="hidden" name="discount_amount_cart" value="{{Session::get('cart_order_detail')['promo_amt_cal']}}">

@endif

    <input type="hidden" name="cancel_return" value="{{url('/transport_order_cancel')}}">

    <input type="hidden" name="return" value="{{url('/transport_order_success')}}">

	

	<?php /*?><input type="hidden" name="cancel_return" value="http://localhost/shouta_party/order_cancel">

    <input type="hidden" name="return" value="http://localhost/shouta_party/order_success"><?php */?>

	

<!--<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">-->

</form>





	



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



<div class="modal fade" id="message_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body popup-ctn">

	  		<p id="alert_model_msg"></p>	

      </div>

    </div>

  </div>

</div>

<!-- COMMON SCRIPTS -->



<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>

<script src="{{ url('/') }}/design/front/js/functions.js"></script>

<script src="{{ url('/') }}/design/front/assets/validate.js"></script>



<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/custom.js"></script>

<script>

    jQuery('#sidebar').theiaStickySidebar({

      additionalMarginTop: 80

    });

	

$(document).on('click', '#oder_confrim', function(){	



if($('#payment_option').is(':checked'))

{

	$('#payment_frm').submit();

}

else

{

	//alert('Please Select payment method');

	$('#alert_model_msg').html("Please Select Payment Method");			

	$('#message_model').modal('show');

}



});





$(document).on('click', '#back_checkout', function(){

//alert('test');

	$('#user_back_checkout').submit();



});	

</script>





@stop