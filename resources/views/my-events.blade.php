@extends("layouts.grambunny")
@section("styles")
{{-- styles goes here --}}
<style type="text/css">
  .parallax-window#short {
    height: 230px;
    min-height: inherit;
    background: 0 0;
    position: relative;
    margin-top: 0px;
}
section.parallax-window {
    overflow: hidden;
    position: relative;
    width: 100%;
    background-image: url(https://grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
}
#sub_content {
    display: table-cell;
    padding: 50px 0 0;
    font-size: 16px;
}
#sub_content h1 {
    margin: 0 0 10px;
    font-size: 28px;
    font-weight: 300;
    color: #F5F0E3;
    text-transform: capitalize;
}
#short #subheader {
    height: 230px;
    color: #F5F0E3;
    text-align: center;
    display: table;
    width: 100%;
}
div#subheader {
    color: #F5F0E3;
    text-align: center;
    display: table;
    width: 100%;
    height: 380px;
}
ul.brad-home {
    padding: 0;
    margin: 0;
    text-align: center;
}
.brad-home li {
    display: inline-block;
    list-style: none;
    padding: 5px 10px;
    font-size: 12px;
}
#subheader a {
    color: #fff;
}
.myOrderActnBox{ margin-top: 0px; }
.myOrderDtelBtn a{
    border: 1px solid #231f20;
    background-color: #fff;
    color: #231f20;
    font-weight: 700;
    border-radius: 25px;
    padding: 8px 15px;
    min-width: 100px;
    cursor: pointer;
    text-decoration: none;
}

.myOrderDtelBtn a:hover {
    background-color: #231f20;
    color: #fff;
}

span.green {
    border: unset !important;
    color: #ff000075;
    border-radius: 3px;
    padding: 2px 9px;
}
.view_order_extra a {
    color: #333 !important;
    border: 1PX solid #333 !important;
    font-size: 14px;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-weight: bold !important;
}
</style>
@endsection
@section("content")
{{-- content goes here --}}

<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">
    <div id="subheader">
        <div id="sub_content" class="animated zoomIn">
          	<h1><span class="restaunt_countrt">Events History</span></h1>
          	 	<!-- <div class="" id="livechat"><p class="checklogin"><a style="cursor: pointer;"><i class="">Live Chat</i> </a></p></div> -->
	        <div id="position">
		        <div class="container">
		            <ul class="brad-home">
		                <li><a href="https://grambunny.com/">Home</a></li>
		                <li>My Events </li>
		            </ul>
		        </div>
		    </div>
        </div>
    </div>
</section>

<section class="myOrderSec section-full">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="myOdrersBox">
					<h2>All Events Details</h2>

            <?php foreach ($order_detail as $key => $value) { ?>
            <?php $userinfo = DB::table('users')->where('id','=',$value->user_id)->first();?>

       <?php $merchantinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first(); ?>




        <?php if(!empty($merchantinfo)){ ?>

			<div class="myOrder oder-detail_pages">
			<div class="myOrderImg">
             <?php if($userinfo->profile_image){ ?><img src="{{ url('/public/uploads/user/'.$userinfo->profile_image) }}">
			 <?php }else{ ?> <img src="{{ url('/public/uploads/user/user.jpg') }}"> <?php } ?>

						</div>
						<div class="myOrderDtelBox">
							<div class="myOrderID">
								<label>Event ID:</label>
								<strong>#{{$value->order_id}}</strong>
							</div>

							<div class="myOrderName">
							<label>Merchant ID:</label>
							<strong>#{{ $merchantinfo->vendor_id }}</strong>
							</div>

							<div class="myOrderDtel">
							<label>Merchant Name</label>
							<strong style="text-transform: capitalize;">{{ $merchantinfo->name }} {{ $merchantinfo->last_name }}</strong>
							</div>

							<div class="myOrderDtel">
							<label>User Name</label>
							<strong style="text-transform: capitalize;">{{ $merchantinfo->username }}</strong>
							</div>

						<!-- 	<div class="myOrderName">
								<label>{{$userinfo->name}} {{$userinfo->lname}}</label>

							</div> -->
							<div class="myOrderDtel">
								<label>Event Price</label>
								<strong>${{$value->total}}</strong>
							</div>
							<!--<div class="myOrderDtel">
								<label>Quantity</label>
								<strong>{{$value->ps_qty}}</strong>
							</div>-->
							<div class="myOrderDtel">
								<label>Placed</label>
								<strong>{{$value->created_at}}</strong>
							</div>
							<div class="myOrderDtel">

								<label>Event Status</label>
								<strong><span class="green">
								@if($value->status==0) Complete @endif
								@if($value->status==4) Complete @endif
								<!-- @if($value->status==1) Accept @endif -->
								@if($value->status==2) Cancelled @endif
								<!-- @if($value->status==3) On the way @endif -->

								<!-- @if($value->status==5) Requested for return @endif -->
								<!-- @if($value->status==6) Return request accepted @endif -->
								<!-- @if($value->status==7) Return request declined @endif -->
							 </span></strong>
							</div>
							<div class="myOrderDtel">
								<label>Venue Address</label>
								<strong style="text-transform: capitalize;">{{$value->venue_address}}</strong>
							</div>
							<div class="myOrderActnBox">
								<div class="myOrderDtelBtn view_order_extra">
									<a href="{{ url('/order-details').'/'.$value->id}}"><span><i class="fa fa-eye" aria-hidden="true"></i></span> Order</a>
								</div>

								<!-- <div class="myOrderDtelBtn view_order_extra">
									<a href="{{ url('/view-invoice').'/'.$value->id}}" target="_blank"><span><i class="fa fa-eye" aria-hidden="true"></i></span> Invoice</a>
								</div> -->

								<?php if(($value->status==1) || ($value->status==3)){ ?>

									<div class="myOrderDtelBtn view_order_extra">
										<a href="{{ url('/track-order').'/'.$value->id}}" target="_blank"><span><i class="fa fa-eye" aria-hidden="true"></i></span>Track Order</a>
									</div>

								<?php } ?>

							</div>
						</div>
					</div>

                 <?php } } ?>

				</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
	              	{{ $order_detail->links() }}
	              </div>
	            </div>

			</div>
		</div>
	</div>
</section>

@endsection
@section("scripts")
{{-- scripts goes here --}}
@endsection