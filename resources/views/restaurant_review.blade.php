@extends('layouts.app')

@section("other_css")


    <!-- SPECIFIC CSS -->
<meta name="_token" content="{!! csrf_token() !!}"/>
<link href="{{ url('/') }}/design/front/css/blog.css" rel="stylesheet">

  

@stop
@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window paralx_rev_top" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_2.jpg" data-natural-width="1400" data-natural-height="470">
  <div id="subheader">
    <div id="sub_content">
      <div id="thumb">	  
					@if(!empty($rest_detail[0]->rest_logo))
					<img src="{{ url('/') }}/uploads/reataurant/{{$rest_detail[0]->rest_logo}}" alt="">									
					@else
					<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
					@endif>
	  </div>
	
    <div class="detail_rev_se">
        @if($total_review>0)
      <div class="rating">
	  	  
	   @for ($x = 1; $x < 6; $x++)
		@if(($avg_rating>0) && ($avg_rating>=$x))
		<i class="icon_star voted"></i>
		@else
			<i class="icon_star"></i>
		@endif
	  @endfor 
	  
	  
	  (<small><a href="{{url('/'.strtolower(trim( str_replace(' ','_',$rest_detail[0]->rest_suburb))).'/'.$rest_detail[0]->rest_metatag.'/review')}}">Read 							  {{$total_review}}, reviews</a></small>)</div>
	  @endif
	  
      
      <h1>{{$rest_detail[0]->rest_name}}</h1>
      <div><em>{{$rest_detail[0]->cuisine_name}} - {{$rest_detail[0]->rest_suburb}} {{$rest_detail[0]->rest_state}}</em></div>
	  <div><i class="icon_pin"></i> {{$rest_detail[0]->rest_address}}, {{$rest_detail[0]->rest_suburb}} {{$rest_detail[0]->rest_state}} {{$rest_detail[0]->rest_zip_code}}.
	  
	  </div>
      <div>
	  @if(!empty($rest_detail[0]->rest_mindelivery))<strong>Delivery charge: ${{$rest_detail[0]->rest_mindelivery}} </strong>@endif
	   <!--, free over $15.-->
	  </div>
      
      </div>
    </div>
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section><!-- End section -->
<!-- End SubHeader ============================================ -->


<!-- Position -->
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
	<div class="row">
         
	 <aside class="col-md-3 col-xs-12" id="sidebar">
			<p><a href="{{url('/'.strtolower(trim(str_replace(' ','_',$rest_detail[0]->rest_suburb))).'/'.$rest_detail[0]->rest_metatag)}}" class="btn_side">Back</a></p>
				<div class="rev_main_wrap">
				<form role="form" id="update_frm" method="POST" action="{{ url('/order_review_action') }}">
				
                        {{ csrf_field() }}
						<input type="hidden" name="order_id" value="{{$refno}} " />
						<input type="hidden" name="user_id" value="{{$user_id}} " />
						<input type="hidden" name="rest_id" value="{{$rest_id}} " />
						<input type="hidden" name="ratting" id="ratting" value="" />
						
				<p><h4>Add Your Review</h4></p>
				@if($user_id==0)
				<p>
				To write a customer review you must have ordered from this restaurant before through our website so that we can obtain better quality, more objective reviews. Only one review per received order is allowed.
				</p>				
				@elseif(($refno==0) && ($user_id>0))
				<p>
					Our records show that you did not order from this restaurant or a review has already been submitted for your last completed order from this restaurant and only one review per received order is allowed.
				</p>				
				@elseif(($refno>0) && ($user_id>0) &&(!empty($order_detail)))
                  <div class="row">
				  <div class="col-md-12">
					<span class="food-main-avrg">Food</span>
				  </div>
				  <div class="col-md-12">
				  <div class="filter_type">
	                            <ul class="control-group">
	                                <li>
	                                    <label class="control control--radio">
	                                        <input type="radio" name="range"  value="5" required="required" ><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--radio">
	                                        <input type="radio" name="range"  value="4" required="required"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--radio">
	                                        <input type="radio" name="range" value="3" required="required"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--radio">
	                                        <input type="radio" name="range" value="2" required="required"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--radio">
	                                        <input type="radio" name="range"  value="1" required="required"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                            </ul>
	                        </div>
				  
					<!--<div class="review_rating">
						<div class="row">
							<div class="rev_row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<span>Poor</span>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center">
									<span>Average</span>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-right">
									<span>Excellent</span>
								</div>
							</div>
						</div>
						<div class="range-slider">
						  <input class="range-slider__range" name="range" type="range" value="5" min="0" max="10">
						  <span class="range-slider__value">5</span>
						</div>
					</div>-->
				  </div>
                  </div>
					<div class="exprnc">
					<div class="form-group">
					  <label for="comment">YOUR EXPERIENCE</label>
					  <textarea class="form-control" rows="9" id="comment" name="review_text" placeholder="To make your review more relevant consider adding restaurant name, cuisine and food item to your review"  required="required"></textarea>
					</div>
					
					
					<div class="form-group">
					  <label for="comment">Food was good</label>					
						<div> 
					  <input type="radio" name="re_food_good" id="re_food_good" value="1" required="required"/> YES
					  	<input type="radio" name="re_food_good" id="re_food_good" value="0" required="required"/> NO
						</div>
					</div>
					
					<div class="form-group">
					  <label for="comment">Delivery was on time</label>
						<div>
						<input type="radio" name="re_delivery_ontime" id="re_delivery_ontime" value="1" required="required"/> YES
						<input type="radio" name="re_delivery_ontime" id="re_delivery_ontime" value="0" required="required"/> NO
						</div>
					</div>
					<div class="form-group">
					  <label for="comment">Order was accurate</label>
						<div>
							<input type="radio" name="re_order_accurate" id="re_order_accurate" value="1" required="required"/> YES
							<input type="radio" name="re_order_accurate" id="re_order_accurate" value="0" required="required"/> NO
						</div>
					 </div>
					 
					 
					<button type="submit" class="btn btn-primary btn_1" id="update_infobtn">Add Review</button>
				</div>	
				@endif
				</form>
  				</div>                
     </aside><!-- End aside -->
	 
     <div class="col-md-9 col-xs-12">
	 	@if(count($review_list)>0)
			
		  @foreach($review_list as $rlist)
		
     		<div class="post clent-reviwsz">
					<!--<a href="blog_post.html" ><img src="{{ url('/') }}/design/front/img/blog-1.jpg" alt="" class="img-responsive"></a>-->
					<div class="post_info clearfix">
						<div class="post-left">
							<ul>
								<li><i class="fa fa-calendar"></i>{{date('jS F, Y',strtotime($rlist->created_at))}} <em>by {{$rlist->name}}</em></li>
                                <!--<li><i class="icon-inbox-alt"></i><a href="#">Category</a></li>
								<li><i class="icon-tags"></i><a href="#">Works</a>, <a href="#">Personal</a></li>-->
							</ul>
						</div>
						<!--<div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div>-->
					</div>
					<!--<h2>Duis aute irure dolor in reprehenderit</h2>-->
					<p>	{{$rlist->re_content}}</p>
                    
					<!--<a href="blog_post.html" class="btn_1" >Read more</a>-->
				</div><!-- end post -->
		  @endforeach		
        @endif             
              <?php /*?><div class="text-center">
                 <ul class="pager">
                    <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                    <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
                  </ul>
              </div><?php */?>
			  
     </div>
	 <!-- End col-md-9-->   
     
      
	</div>    
</div><!-- End container -->
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')




	


<!-- COMMON SCRIPTS -->
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>

<!-- review rating script -->
<script>
var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

	  
    value.each(function(){	  
      var value = $(this).prev().attr('value');
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
	  
    });
  });
};

rangeSlider();


$(document).ready(function () {
            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }

            $("#close_in").click(function () {
                if ($(".cmn-toggle-switch__htx").hasClass("active")) {
                    $(".cmn-toggle-switch__htx").removeClass("active")
                }
            });

        });	
</script>

<script>

 $(document).on('click', '#update_infobtn', function(){ 
		 	
			
			//alert($('.range-slider__value').text());
			
			var form = $("#update_frm");
				form.validate();
			var valid =	form.valid();
		 });

		  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
@stop