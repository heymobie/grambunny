@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Place your order</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Cancel!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section>
<!-- End section -->
<!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Order Cancel</li>
            </ul>
        </div>
    </div><!-- Position -->
 

<!-- Content ================================================== -->

<div class="container margin_60_35">
<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="box_style_2">
				<h2 class="inner">Booking Order Cancel!</h2>
				<div id="confirm">
					<!--<i class="icon_check_alt2"></i>
					<h3>Thank you!</h3>-->
					<p>
						We are sorry! Your last transaction was cancelled.
					</p>
				</div>
				<h4>Summary</h4>
				<table class="table table-striped nomargin">
				
				
				<tbody>
				<tr>
					<td>
						 Booking Type
					</td>
					<td>
						<span class="pull-right">{{str_replace('_',' ',$order_detail[0]->pick_bookingtype)
						}}</span>
					</td>
				</tr>
				
				
				<tr>
					<td>
						 Booking From
					</td>
					<td>
						<span class="pull-right">{{$order_detail[0]->order_ondate.' '.$order_detail[0]->order_ontime}}</span>
					</td>
				</tr>
				<tr>
					<td>
						Booking To
					</td>
					<td>
						<span class="pull-right">{{$order_detail[0]->order_returndate.' '.$order_detail[0]->order_returntime}}</span>
					</td>
				</tr>
				
				
					<?php $cart_price = 0;?>
		@if(isset($cart) && (count($cart)))
				
				 @foreach($cart as $item)
				<?php  $cart_price = $cart_price+$item->price;?>
				<tr>
					<td>
						<strong>{{$item->qty}}x</strong> {{str_replace('_',' ',$item->name)}} 
					</td>
					<td>
						<strong class="pull-right">${{number_format($item->price,2)}}</strong>
					</td>
				</tr>
				
				 
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $cart_price = $cart_price+$addon['price'];?>
					  
				<tr>
					<td>
						 {{$addon['name']}}
					</td>
					<td>
						<strong class="pull-right">@if($addon['price']>0)${{number_format($addon['price'],2)}}@endif</strong>
					</td>
				</tr>
				
					 
					 @endforeach
				 @endif 
					
				 @endforeach
@endif



				@if($order_detail[0]->order_deliveryfee>0)
				
					
				<tr>
					<td>
						 Service fee
					</td>
					<td>
						<span class="pull-right">${{number_format($order_detail[0]->order_deliveryfee,2)}}</span>
					</td>
				</tr>
				@endif

				@if($order_detail[0]->total_night>0)
				
					
				<tr>
                  <td> Driver Allownce   </td>
					<td><span class="pull-right">${{number_format($order_detail[0]->total_allownce,2)}}</span></td>
                </tr>
				@endif

		
				@if($order_detail[0]->order_promo_val>0)
			
                <tr>
                  <td> Discount </td>
				  <td><span class="pull-right"> -${{number_format($order_detail[0]->order_promo_cal,2)}} </span></td>
                </tr>
				
				@endif
				
				
				
				<tr>
					<td class="total_confirm">
						 TOTAL
					</td>
					<td class="total_confirm">
					
				
				
						<span class="pull-right">${{number_format($order_detail[0]->order_total_amt,2)}}</span>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>
	</div>	
	<!-- End row -->
	
</div><!-- End container -->
<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	
@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>

@stop	