@extends('layouts.app')


@section("other_css")
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/owl.theme.css">
@stop

@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>{{$page_detail[0]->page_title}}</h1>
         <!--<p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>-->
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

    <!-- Position -->
 

<!-- Content ================================================== -->
<div class="grey-back">
	<div class="container">
	<div class="row">
		<div class="col-md-12">
		
				@if($page_detail[0]->page_status)
					{!! $page_detail[0]->page_content !!}
				@endif
			
		</div>
	</div><!-- End row -->
	
	</div><!-- End container -->
</div><!-- End container -->
<!-- End Content =============================================== -->
	
@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/js/validate.js"></script>

@stop	