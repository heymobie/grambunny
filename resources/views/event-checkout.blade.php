@extends('layouts.grambunny')
@section('styles')
<style type="text/css">
   .text-muted {
   font-size: 14px;
   }
   .produt-total p {
    font-size: 14px;
}
   .parallax-window#short {
   height: 230px;
   min-height: inherit;
   background: 0 0;
   position: relative;
   margin-top: 0px;
   }
   section.parallax-window {
   overflow: hidden;
   position: relative;
   width: 100%;
   background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
   background-attachment: fixed;
   background-repeat: no-repeat;
   background-position: top center;
   background-size: cover;
   }
   #short #subheader {
   height: 230px;
   color: #F5F0E3;
   text-align: center;
   display: table;
   width: 100%;
   }
   section#short #sub_content {
   padding-top: 12px !important;
   }
   #sub_content {
   display: table-cell;
   font-size: 16px;
   }
   #sub_content h1 {
   margin: 0 0 10px;
   font-size: 28px;
   font-weight: 300;
   color: #F5F0E3;
   text-transform: capitalize;
   }
   .error{
   color: red;
   font-weight: normal;
   }
   .hide{
   display: none;
   }
   .dadrs{margin-top:5px !important; }
   .cpdaddrs{ 
   margin-right:10px; 
   cursor:pointer;
   } 
  span#dropid {
    float: right;
    font-size: 14px;
}
button, input, select[multiple], textarea {

    border: unset;
}
   span#pickid {
   float: right;
   font-size: 14px;
   }
   #cusadrs{ display:block; margin-top:10px;}
   #pickupadrs{ display:block; margin-top:10px;}
   #dropadrs{ display:block; margin-top:10px;}
   .addrstab{ 
   border:1px solid gray;
   padding: 3px;
   border-radius: 3px;
   }
   #dropid input{ margin-right:5px;}
   #pickid input{ margin-right:5px;}  
   @media screen and (max-width: 450px) {
   #pickid {
   float: none !important;
   padding-left: 5px !important;
   }
   #dropid {
   float: none !important;
   padding-left: 5px !important;
   }
   }
   .myOrderPay.cashcard {
   font-size: 14px;
   }
   button.btn.btn-primary.btn-lg.btn-block {
    width: 22%;
    height: 45px;
    float: right;
}
.cnt_shop a {
    padding: 6px;
    font-size: 13px;
    border-radius: 3px;
    color: #FFF;
    margin: auto;
    width: 100%;
    float: left;
}
.stic-s-btn {
    /* float: left; */
    width: 100%;
    padding: 8px;
    margin-top: -2px;
    margin-bottom: 17px;
}
.stic-s-btn .cnt_shop{
    float: left;
    text-align: center;
    margin: auto;
    width: 100%;
}

   .deep-tiket{
    margin-bottom: 0px !important;
    /*margin-top: 12px !important;*/
    font-size: 15px;
   }

 /* The Modal (background) */
.ebcf_modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    /*background-color: rgb(0,0,0); /* Fallback color */
    /* Black w/ opacity */*/
}

/* Modal Content */
.ebcf_modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 50px;
    border: 1px solid #888;
    width: 50%;
    font-size: 20px;
}

/* The Close Button */
.ebcf_close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    margin-top: -50px;
    margin-right: -30px;
}

.ebcf_close:hover,
.ebcf_close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.squarecard{ font-size: 14px; }  

</style>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="{{asset('resources/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('resources/js/additional-methods.js')}}"></script>

<!--squre payment script-->

<?php $payment_mode = DB::table('admin_setting')->where('id', '=' ,1)->value('payment_mode'); ?>

<?php if($payment_mode==1){ ?> 

 <script type="text/javascript" src="https://web.squarecdn.com/v1/square.js"></script> 

<?php }else{ ?> 

<script type="text/javascript" src="https://sandbox.web.squarecdn.com/v1/square.js"></script>

<?php  } ?>

<?php 

$app_id = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->value('app_id');

$location_id = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->value('location_id');

$paymentmethod = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->first();

$payment = DB::table('payment_setting')->where('vendor_id','=',$vendor_id)->get();
 
$paycount = count($payment); 

?>

    <script>
      const appId = '<?php echo $app_id;?>';
      const locationId = '<?php echo $location_id;?>';

      async function initializeCard(payments) {
        const card = await payments.card();
        await card.attach('#card-container');

        return card;
      }

      async function createPayment(token, verificationToken) {

         var firstName = $("#firstName").val(); 
         var lastName = $("#lastName").val();
         var email = $("#email").val();
         var user_mob = $("#user_mob").val(); 

         var address = $("#address").val();
         var city = $("#city").val();
         var zip = $("#zip").val();

         var addressds = '1';
         var cityds = '1';
         var zipds = '1';

         var delpickup = $('input[name="pickupdel"]:checked').val(); // delivery

         if(delpickup=='delivery'){

          var addressd = $("#addressd").val();
          var cityd = $("#cityd").val();
          var zipd = $("#zipd").val();

          if(addressd==''){ $("#addressd").css('border-color', '#ed1c24'); addressds = '';}
          if(cityd==''){ $("#cityd").css('border-color', '#ed1c24'); cityds = '';}
          if(zipd==''){ $("#zipd").css('border-color', '#ed1c24'); zipds = '';}

         }    

         if(address==''){ $("#address").css('border-color', '#ed1c24');}
         if(city==''){ $("#city").css('border-color', '#ed1c24');}
         if(zip==''){ $("#zip").css('border-color', '#ed1c24');}

         if(firstName==''){ $("#firstName").css('border-color', '#ed1c24');}
         if(lastName==''){ $("#lastName").css('border-color', '#ed1c24');}
         if(email==''){ $("#email").css('border-color', '#ed1c24');}
         if(user_mob==''){ $("#user_mob").css('border-color', '#ed1c24');}

        $("#card-button").removeAttr("disabled");

        if(firstName){
        $("#firstName").css('border-color', '#ced4da');
        if(lastName){
        $("#lastName").css('border-color', '#ced4da');
        if(email){
        $("#email").css('border-color', '#ced4da');
        if(user_mob){
        $("#user_mob").css('border-color', '#ced4da');
        if(address){
        $("#address").css('border-color', '#ced4da');
        if(city){
        $("#city").css('border-color', '#ced4da');
        if(zip){  
        $("#zip").css('border-color', '#ced4da');

        if(addressds){
        $("#addressd").css('border-color', '#ced4da');
        if(cityds){
        $("#cityd").css('border-color', '#ced4da');
        if(zipds){  
        $("#zipd").css('border-color', '#ced4da'); 


        $("#locationId").val(locationId);
        $("#sourceId").val(token);
        $("#verificationToken").val(verificationToken);
        $("#idempotencyKey").val(window.crypto.randomUUID());

              var post_data = new FormData($("#checkout_form")[0]);

               $("#card-button").attr('disabled', 'true');

               $.ajax({
   
                   type: 'POST',
   
                   url: "<?php echo url('/').'/submit_order'; ?>",
   
                   data: post_data,
   
                   dataType: "json",
   
                   processData: false,
   
                   contentType: false,
   
                   beforeSend: function() {
   
                    // $("#card-button").attr('disabled', 'true');
   
                    $("#card-button").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');
   
                   },
            
                       success: function(resultData) {  
   
                       if(resultData.status == 1){
   
                       $("#card-button").removeAttr("disabled");
   
                       $("#card-button").html('Submit Order');
   
                       var order_id = resultData.order_id;
   
                       window.location.href = "<?php echo url('/').'/thank_you_page/'; ?>"+order_id;
   
                       }else{

                         $("#card-button").removeAttr("disabled");
   
                         $("#card-button").html('Submit Order');
   
                         $('.error').removeClass('hide').find('.alert').text(resultData.msg);
   
                          setTimeout(function(){
   
                          $('.error').addClass('hide');
   
                         }, 5000);  
   
                     }
   
                   },
   
                   error: function(errorData) {
   
                     console.log(errorData);
   
                     $("#card-button").removeAttr("disabled");
   
                     $("#card-button").html('Submit Order');
   
                     $('.error').removeClass('hide').find('.alert').text('Please insert valid Information'); //Some internal issue occured!
   
                     setTimeout(function(){
   
                       $('.error').addClass('hide');
   
                     }, 5000);
   
                   }
   
               });

             }}}
           }}}}
         }}}

      }

      async function tokenize(paymentMethod) {
        const tokenResult = await paymentMethod.tokenize();
        if (tokenResult.status === 'OK') {
          return tokenResult.token;
        } else {
          let errorMessage = `Tokenization failed with status: ${tokenResult.status}`;
          if (tokenResult.errors) {
            errorMessage += ` and errors: ${JSON.stringify(
              tokenResult.errors
            )}`;
          }

          throw new Error(errorMessage);
        }
      }

      // Required in SCA Mandated Regions: Learn more at https://developer.squareup.com/docs/sca-overview
      async function verifyBuyer(payments, token) {

        var address = $("#address").val(); 
        var address2 = $("#address2").val();
        var city = $("#city").val();
        var zip = $("#zip").val();
   
        var firstName = $("#firstName").val();
        var email = $("#email").val();
        var user_mob = $("#user_mob").val();
        var final_amount = Math.round($("#final_amount").val());

        var cregion = $("#country").val(); 

        if(cregion=='USA'){ var ccountry = 'US'; }
        if(cregion=='JPN'){ var ccountry = 'JP'; }
        if(cregion=='AUS'){ var ccountry = 'AU'; }
        if(cregion=='HKG'){ var ccountry = 'HK'; }
        if(cregion=='CHN'){ var ccountry = 'CN'; }
        if(cregion=='FRA'){ var ccountry = 'FR'; }
        if(cregion=='ARE'){ var ccountry = 'AE'; }
        if(cregion=='GBR'){ var ccountry = 'GB'; }
        if(cregion=='IRL'){ var ccountry = 'IE'; }
        if(cregion=='CAN'){ var ccountry = 'CA'; }
     
        const verificationDetails = {
          amount: final_amount.toString(),
          billingContact: {
            addressLines: [address.toString(), address2.toString()],
            familyName: firstName.toString(),
            givenName: firstName.toString(),
            email: email.toString(),
            country: ccountry.toString(),
            phone: user_mob.toString(),
            region: cregion.toString(),
            city: city.toString(),
          },
          currencyCode: 'USD',
          intent: 'CHARGE',
        };

        //console.log(verificationDetails);

        const verificationResults = await payments.verifyBuyer(
          token,
          verificationDetails
        );
        return verificationResults.token;
      }

      // status is either SUCCESS or FAILURE;
      function displayPaymentResults(status) {
        const statusContainer = document.getElementById(
          'payment-status-container'
        );
        if (status === 'SUCCESS') {
          statusContainer.classList.remove('is-failure');
          statusContainer.classList.add('is-success');
        } else {
          statusContainer.classList.remove('is-success');
          statusContainer.classList.add('is-failure');
        }

        statusContainer.style.visibility = 'visible';
      }

      document.addEventListener('DOMContentLoaded', async function () {
        if (!window.Square) {
          throw new Error('Square.js failed to load properly');
        }

        let payments;
        try {
          payments = window.Square.payments(appId, locationId);
        } catch {
          const statusContainer = document.getElementById(
            'payment-status-container'
          );
          statusContainer.className = 'missing-credentials';
          statusContainer.style.visibility = 'visible';
          return;
        }

        let card;
        try {
          card = await initializeCard(payments);
        } catch (e) {
          console.error('Initializing Card failed', e);
          return;
        }

        async function handlePaymentMethodSubmission(event, card) {
          event.preventDefault();

          try {
            // disable the submit button as we await tokenization and make a payment request.
            cardButton.disabled = true;
            const token = await tokenize(card);
            const verificationToken = await verifyBuyer(payments, token);
            const paymentResults = await createPayment(
              token,
              verificationToken
            );
            displayPaymentResults('SUCCESS');

            console.debug('Payment Success', paymentResults);
          } catch (e) {
            cardButton.disabled = false;
            displayPaymentResults('FAILURE');
            console.error(e.message);
          }

        }

        const cardButton = document.getElementById('card-button');
        cardButton.addEventListener('click', async function (event) { 
          await handlePaymentMethodSubmission(event, card);
        });
      });

    </script>

<!--end payment script-->

<script type="text/javascript">

    // Get the modal
var ebModal = document.getElementById('btnCheckoutModal');

// Get the button that opens the modal
var ebBtn = document.getElementById("btnCheckout");

// Get the <span> element that closes the modal
var ebSpan = document.getElementsByClassName("ebcf_close")[0];

// When the user clicks the button, open the modal 
ebBtn.onclick = function() {
    //ebModal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
ebSpan.onclick = function() {
    ebModal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == ebModal) {
        ebModal.style.display = "none";
    }
}


   document.getElementById('user_mob').addEventListener('input', function (e) {
   
   var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
   
   e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
   
   });

   var xv = document.getElementById("user_mob").value;

   var xvs = xv.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
   
   xvsi = !xvs[2] ? xvs[1] : '(' + xvs[1] + ') ' + xvs[2] + (xvs[3] ? '-' + xvs[3] : '');

   var xins = document.getElementById("user_mob").value=xvsi;
   
   
   // $.validator.addMethod("zipCodeValidation", function() {
   // var zipCode = $('input#zip').val();
   // return (/(^\d{5}$)|(^\d{5}-\d{4}$)/).test(zipCode); // returns boolean
   // }, "Please enter a valid US zip code (use a hyphen if 9 digits).");
   
    $(document).ready(function() {
   
      $('#cashId').click(function() {
         
      var cashId = $('#cashId').val(); 
   
     $('#cardpaybox').hide(); 
   
      });
   
    $('#cardId').click(function() {
   
        var cardId = $('#cardId').val();
   
        $('#cardpaybox').show(); 
   
      }); 


      $('#pickupId').click(function() {
         
      var pickupId = $('#pickupId').val(); 

      $('#pickup_del').val(pickupId);

        $('#addressd').removeAttr('required');
        $('#addressd2').removeAttr('required');
        $('#cityd').removeAttr('required');
        $('#stated').removeAttr('required');
        $('#zipd').removeAttr('required');
   
       $('#dropadrs').hide();
       $('#dropid').hide(); 
   
      });
   
    $('#deliveryId').click(function() {
   
        var deliveryId = $('#deliveryId').val();

        $('#pickup_del').val(deliveryId); 

        $('#addressd').attr('required', 'required');
        //$('#addressd2').attr('required', 'required');
        $('#cityd').attr('required', 'required');
        $('#stated').attr('required', 'required');
        $('#zipd').attr('required', 'required');
   
        $('#dropadrs').show();
        $('#dropid').show(); 
   
      }); 

   
   });
   
</script>
<script type="text/javascript">
   $(document).ready(function () {
   
     $('#coupon_form').validate({
   
       rules: {
   
         promo_code: {
   
           required: true
   
         }
   
       },
   
       submitHandler: function(form) {
   
         //form.submit();
   
         return apply_coupon();
   
       }
   
     });
   
   
   });
   
   
   /* $(document).ready(function () {
   
     $('#checkout_form').validate({
   
       rules: {
   
         firstName: {
   
           required: true
   
         },
   
         lastName: {
   
           required: true
   
         },
   
         email: {
   
           required: true,
   
           email: true
   
         },
   
         user_mob: {
   
           required: true,
   
           minlength: 14,
   
         },
   
         address: {
   
           required: true
   
         },
   
         address2: {
   
           required: true
   
         },
   
         state: {
   
           required: true
   
         },
   
         zip: {
   
           required: true,
   
           //Zip: {zipCodeValidation: true},
   
           digits: true
   
         },
   
         cc_name: {
   
           required: true
   
         },
   
         cc_number: {
   
           required: true,
   
           digits: true
   
         },
   
         cc_expire_month: {
   
           required: true,
   
           digits: true
   
         },
   
         cc_expire_year: {
   
           required: true,
   
           digits: true
   
         },
   
         cc_cvv: {
   
           required: true,
   
           digits: true
   
         }
   
       },
   
       messages: {
       user_mob: {
         minlength: "Please enter valid Mobile Number.",
       }
     },
   
       submitHandler: function(form) {
   
         //form.submit();
   
       }
   
     });
   
   
   }); */
   
   
   
   function apply_coupon() {
   
     var promo_code = $("#promo_code").val();
   
     $.ajax({
   
         type: 'POST',
   
         url: "<?php echo url('/').'/apply_coupon'; ?>",
   
         data: {promo_code:promo_code,'_token':"<?php echo csrf_token(); ?>",item_price:"<?php echo $total_price; ?>",'vendor_id':'<?php echo $vendor_id; ?>','user_id':'<?php echo $user_id; ?>'},
   
         dataType: "json",
   
         beforeSend: function() {
   
             $("#btnRedeem").attr('disabled', 'true');
   
             $("#btnRedeem").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');
   
         },
   
         success: function(resultData) { 
   
           console.log(resultData);
   
           //var result = resultData.trim();
   
           //var json_res = JSON.stringify(result);
   
           if (resultData.status == 1) {
   
            // alert(resultData.msg);
   
             $("#btnRedeem").removeAttr("disabled");
   
             $("#btnRedeem").html('Redeem');
   
             location.reload();
   
           } else {
   
             alert(resultData.msg);
   
             $("#btnRedeem").removeAttr("disabled");
   
             $("#btnRedeem").html('Redeem');
   
           }
   
         },
   
         error: function(errorData) {
   
           console.log(errorData);
   
           //alert('Some internal issue occured!');
   
           $("#btnRedeem").removeAttr("disabled");
   
           $("#btnRedeem").html('Redeem');
   
         }
   
     });
   
     return false;
   
   }
   
</script>
<script>
   function initAutocomplete() {
   
      // var address = new google.maps.places.Autocomplete(
   
   
   
      //        (document.getElementById('address')),
   
   
   
      //        {types: ['geocode']}
   
   
   
      //  var address2 = new google.maps.places.Autocomplete(
   
   
   
      //        (document.getElementById('address2')),
   
   
   
      //        {types: ['geocode']}      
   
   
   
      // );
   
    // priyanka comment code 13112021 
   
       // var acInputs = document.getElementById("address");
   
       // var autocomplete = new google.maps.places.Autocomplete(acInputs);
   
       // autocomplete.inputId = acInputs.id;
   
       // google.maps.event.addListener(autocomplete, 'place_changed', function () {
   
       //     $("#latitude").val(autocomplete.getPlace().geometry.location.lat());
   
       //     $("#longitude").val(autocomplete.getPlace().geometry.location.lng());
   
       //     document.getElementById("log").innerHTML = 'You used input with id ' + this.inputId;
   
       // });
   
   
       //           var acInputsp = document.getElementById("addressp");
   
       // var autocompletep = new google.maps.places.Autocomplete(acInputsp);
   
       // autocompletep.inputId = acInputsp.id;
   
       // google.maps.event.addListener(autocompletep, 'place_changed', function () {
   
       //     $("#latitude").val(autocompletep.getPlace().geometry.location.lat());
   
       //     $("#longitude").val(autocompletep.getPlace().geometry.location.lng());
   
       //     document.getElementById("log").innerHTML = 'You used input with id ' + this.inputId;
   
       // });
   
       //           var acInputsd = document.getElementById("addressd");
   
       // var autocompleted = new google.maps.places.Autocomplete(acInputsd);
   
       // autocompleted.inputId = acInputsd.id;
   
       // google.maps.event.addListener(autocompleted, 'place_changed', function () {
   
       //     $("#latitude").val(autocompleted.getPlace().geometry.location.lat());
   
       //     $("#longitude").val(autocompleted.getPlace().geometry.location.lng());
   
       //     document.getElementById("log").innerHTML = 'You used input with id ' + this.inputId;
   
       // });
   
     // end priyanka comment code 13112021   
   
   
       /*var acInputs2 = document.getElementById("address2");
   
       var autocomplete = new google.maps.places.Autocomplete(acInputs2);
   
       autocomplete.inputId = acInputs2.id;
   
       google.maps.event.addListener(autocomplete, 'place_changed', function () {
   
           document.getElementById("log").innerHTML = 'You used input with id ' + this.inputId;
   
   
       });*/
   
   
   
   }
   
   
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSs4Tdf-8AKp9dz9_4bfsc_H9XtHrSwpY&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
   $(document).ready(function()
   {
   
       $("#checkout_btn").click(function()
       {
   
           alert('Info! You need to be login to perform this action.');
   
       });

   
      $("#checkout_minorder").click(function()
       {
   
           alert('Your order amount should be more than minimum amount');
   
       });

   
   });
   
   
   
</script>

<script type="text/javascript">
   $(function() {

    
      var $formg = $(".needs-g-validation");

      $('form.needs-g-validation').bind('submit', function(e) {

           var $formg     = $(".require-validation"),
   
           inputSelector = ['input[type=email]', 'input[type=text]', 'textarea'].join(', '),
   
           $inputs       = $form.find('.required').find(inputSelector),
   
           $errorMessage = $form.find('div.error'),
   
           valid         = true;
   
           $errorMessage.addClass('hide');
   
           $('.has-error').removeClass('has-error');
   
           $inputs.each(function(i, el) {
   
           var $input = $(el);
   
           if ($input.val() === '') {
   
           $input.parent().addClass('has-error');
   
           $errorMessage.removeClass('hide');
   
           e.preventDefault();
   
         }
   
       });
   
       if (!$form.data('cc-on-file')) {
   
         e.preventDefault();

         createorders();
   
       }
   
   
     });



      var $form = $(".needs-validation");
   
      $('form.needs-validation').bind('submit', function(e) {
   
      //$("#btnCheckout").attr('disabled', 'true');
   
           var $form     = $(".require-validation"),
   
           inputSelector = ['input[type=email]', 'input[type=text]', 'textarea'].join(', '),
   
           $inputs       = $form.find('.required').find(inputSelector),
   
           $errorMessage = $form.find('div.error'),
   
           valid         = true;
   
           $errorMessage.addClass('hide');
   
           $('.has-error').removeClass('has-error');
   
           $inputs.each(function(i, el) {
   
           var $input = $(el);
   
           if ($input.val() === '') {
   
           $input.parent().addClass('has-error');
   
           $errorMessage.removeClass('hide');
   
           e.preventDefault();
   
         }
   
       });
   
       if (!$form.data('cc-on-file')) {
   
        e.preventDefault();
           
        createorders();
   
       }
   
   
     });
   
   
   
     function createorders() {

         var firstName = $("#firstName").val(); 
         var lastName = $("#lastName").val();
         var email = $("#email").val();
         var user_mob = $("#user_mob").val(); 
         var user_mob_num = user_mob.length;   

         var address = $("#address").val();
         var city = $("#city").val();
         var zip = $("#zip").val();

         var addressds = '1';
         var cityds = '1';
         var zipds = '1';


         var delpickup = $('input[name="pickupdel"]:checked').val(); // delivery

         if(delpickup=='delivery'){

          var addressd = $("#addressd").val();
          var cityd = $("#cityd").val();
          var zipd = $("#zipd").val();

      if(addressd==''){ $("#addressd").css('border-color', '#ed1c24'); addressds = '';}
      if(cityd==''){ $("#cityd").css('border-color', '#ed1c24'); cityds = '';}
      if(zipd==''){ $("#zipd").css('border-color', '#ed1c24'); zipds = '';}

         }    

         if(address==''){ $("#address").css('border-color', '#ed1c24');}
         if(city==''){ $("#city").css('border-color', '#ed1c24');}
         if(zip==''){ $("#zip").css('border-color', '#ed1c24');}

         if(firstName==''){ $("#firstName").css('border-color', '#ed1c24');}
         if(lastName==''){ $("#lastName").css('border-color', '#ed1c24');}
         if(email==''){ $("#email").css('border-color', '#ed1c24');}
         if(user_mob==''){ $("#user_mob").css('border-color', '#ed1c24');}
         if(user_mob_num!='14'){ $("#user_mob").css('border-color', '#ed1c24');}

        $("#card-button").removeAttr("disabled");

        if(firstName){
        $("#firstName").css('border-color', '#ced4da');
        if(lastName){
        $("#lastName").css('border-color', '#ced4da');
        if(email){
        $("#email").css('border-color', '#ced4da');
        if(user_mob && user_mob_num=='14'){
        $("#user_mob").css('border-color', '#ced4da');
        if(address){
        $("#address").css('border-color', '#ced4da');
        if(city){
        $("#city").css('border-color', '#ced4da');
        if(zip){  
        $("#zip").css('border-color', '#ced4da');

        if(addressds){
        $("#addressd").css('border-color', '#ced4da');
        if(cityds){
        $("#cityd").css('border-color', '#ced4da');
        if(zipds){  
        $("#zipd").css('border-color', '#ced4da');

   
               var post_data = new FormData($("#checkout_form")[0]);

               $("#btnCheckout").attr('disabled', 'true');

               $.ajax({
   
                   type: 'POST',
   
                   url: "<?php echo url('/').'/submit_order'; ?>",
   
                   data: post_data,
   
                   dataType: "json",
   
                   processData: false,
   
                   contentType: false,
   
                   beforeSend: function() {
   
                       $("#btnCheckout").attr('disabled', 'true');
   
                       $("#btnCheckout").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');

                      var ebModal = document.getElementById('btnCheckoutModal');
                         ebModal.style.display = "block";
   
                   },
   
                   success: function(resultData) {  
   
                     //console.log(resultData);
   
                       if(resultData.status == 1){
   
                       $("#btnCheckout").removeAttr("disabled");
   
                       $("#btnCheckout").html('Submit Order');
   
                       var order_id = resultData.order_id;
   
                       window.location.href = "<?php echo url('/').'/thank_you_page/'; ?>"+order_id;

                       //window.location.reload();
   
                       }else if(resultData.status == 2){
   
                       $("#btnCheckout").removeAttr("disabled");
   
                       $("#btnCheckout").html('Submit Order');
   
                       window.location.href = resultData.redirect_url;
   
                     }else if(resultData.status == 3){
   
                       $("#btnCheckout").removeAttr("disabled");
   
                       $("#btnCheckout").html('Submit Order');
   
                       window.location.href = resultData.redirect_url+'&&order_id='+resultData.order_id;

                       /* var url = resultData.redirect_url+'&&order_id='+resultData.order_id;

                       window.open(url,'popUpWindow','height=300,width=700,left=50,top=50,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes'); */
   
                     }else{
   
                         $("#btnCheckout").removeAttr("disabled");
   
                         $("#btnCheckout").html('Submit Order');

                         $('.error').removeClass('hide').find('.alert').text(resultData.msg);
   
                          setTimeout(function(){
   
                          $('.error').addClass('hide');
   
                         }, 5000);  
   
                     }
   
                   },
   
                   error: function(errorData) {
   
                     console.log(errorData);
   
                     $("#btnCheckout").removeAttr("disabled");
   
                     $("#btnCheckout").html('Submit Order');

                     var ebModal = document.getElementById('btnCheckoutModal');
                         ebModal.style.display = "none";
   
                     $('.error').removeClass('hide').find('.alert').text('Please insert valid Information'); //Some internal issue occured!
   
                     setTimeout(function(){
   
                       $('.error').addClass('hide');
   
                     }, 5000);
   
                   }
   
               });



          }}}
            }}}}
         }}}
   
       }
     
   
   });
   

   
   function set_address(del_address,lat,long) {
   
     $("#address").val(del_address);
   
     $("#latitude").val(lat);
   
     $("#longitude").val(long);
   
   }
   
   
</script>
<script>
   /* function cAddress () {
   
   $('#cusadrs').show();
   $('#pickupadrs').hide(); 
   $('#dropadrs').hide();
   $('#customerid').addClass('addrstab'); 
   $('#pickid').removeClass('addrstab'); 
   $('#dropid').removeClass('addrstab'); 
   
   } */
   
   
   
   function pAddress() { 
   
     var address = $('#address').val();
     var city = $('#city').val();
     var zip = $('#zip').val();
     var state = $('#state').val();
   
     //alert(state);
   
   if($("#pickcheck").is(':checked')){ 
   
    $('#addressp').val('');
    $('#cityp').val('');
    $('#zipp').val('');
    $('#statep').val(''); 
   
    $('#pickcheck').prop( 'checked',false );
   
   } else {
   
    $('#addressp').val(address);
    $('#cityp').val(city);
    $('#zipp').val(zip);
    $('#statep').val(state); 
   
    $('#pickcheck').prop( 'checked',true );
   
   }
   
   
   }
   
   function dAddress() {
   
     var address = $('#address').val();
     var address2 = $('#address2').val();
     var city = $('#city').val();
     var zip = $('#zip').val();
     var state = $('#state').val(); 
     var country = $('#country').val();
   
   if($("#dropcheck").is(':checked')){
   
    $('#addressd').val('');
    $('#addressd2').val('');
    $('#cityd').val('');
    $('#zipd').val('');   
    $('#stated').val('');
    $('#countryd').val('');
   
    $('#dropcheck').prop( 'checked',false );
   
   } else {
   
    $('#addressd').val(address);
    $('#addressd2').val(address2);
    $('#cityd').val(city);
    $('#zipd').val(zip);
    $('#stated').val(state); 
    $('#countryd').val(country);   
   
    $('#dropcheck').prop( 'checked',true );
   
   } 
   
   
   }
   
</script>
@endsection
@section('content')
<!-- <section id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335" class="parallax-window Serv_Prod_Banner deepshort">
   <div id="subheader">
   
   <div id="sub_content" class="animated zoomIn">
   
     <h1><span class="restaunt_countrt">Checkout Process</span></h1> 
   
     <?php if(!Auth::guard("user")->check()){ ?>
   
      <p class="checklogin" ><a href="{{ route("signInPage",["redirect_url" => route("checkout")]) }}">Login</a></p>
   
    <?php } ?>
   
   
   </div>
   
   </div>
   
   
   
   </section> -->
<section class="section-full checkuot_Sec about-info">
   <div class="container">
      <div class="row">
         <div class="col-md-4 order-md-2 mb-4 orderSumery">
            <div id="sticky-anchor"></div>
            <div id="sticky">
               <ul class="list-group mb-3 orderSumeryBox">
               
                 <?php 
                  $subtotal =0 ;
                  $ticket_service_fee = 0;
                  $ticket_fee = 0;
                  ?>

                  <?php $producttotal = 0 ; //$excisetax = 0; ?>

                  <?php foreach($items as $item){
                  
                  $total = $item->quantity * $item->price ;              
                  $producttotal = $producttotal + $total;

                  $ticket_service_fee = $ticket_service_fee + $item->ticket_service_fee;

                  $ticket_fee = $ticket_fee + $item->ticket_fee;  
                     
                  ?>


                  <li class="list-group-item d-flex justify-content-between lh-condensed" style="border-bottom: 1px solid #ddd;">
                     <div class="produt-total">
                        <h3>{{$item->name}}</h3>
                        <small class="text-muted">{{$item->quantity}} x ${{$item->price}}</small>
                        
                     </div>
                     <div class="produt-total">

                        <!-- <h3>total</h3> -->
                        <span class="text-muted"><?php echo '$'.
                           number_format( $item->quantity*$item->price,2 ); 
                           
                           ?></span>
                     </div>
                  </li>
                  
                  <?php } ?>

              <?php 
             // print_r($item); die;

              $saletax = 0;
              $citytax = 0;
              $excisetax = 0;
              $deliveryfee = 0;

              $vendor = DB::table('vendor')->where('vendor_id','=',$vendor_id)->first();

            if(isset($vendor->delivery_fee)){ $deliveryfee = $vendor->delivery_fee; }

              //echo $deliveryfee; die;

              //$subtotal = $producttotal-$promo_amount+$deliveryfee+$ticket_service_fee;

              $subtotal = $producttotal-$promo_amount+($ticket_service_fee * $items[0]->quantity) + ($ticket_fee * $items[0]->quantity);

              //if(isset($vendor->excise_tax)){ $excisetax = ($subtotal * $vendor->excise_tax)/100 ; }
                     
              //if(isset($vendor->city_tax)){ $citytax = ($subtotal * $vendor->city_tax)/100 ; }
                     
              //$subtotal = $subtotal+$excisetax+$citytax;
                     
              if(isset($vendor->sales_tax)){ 

                $saletax = ($subtotal) * ($vendor->sales_tax)/100 ; 

              }
                     
                   
              $final_total = $subtotal+$saletax;

              $min_order_amount = $vendor->minimum_order_amount;
              
              ?> 
              

                  <li class="list-group-item d-flex justify-content-between lh-condensed">
                     <div>
                        <small class="text-muted">Events Total</small>
                     </div>
                     <span class="text-muted" style="">${{ number_format( $producttotal,2 ) }}</span>
                  </li>

                  <?php if($subtotal < $min_order_amount){ ?>

                  <li class="list-group-item d-flex justify-content-between lh-condensed">
                     <div>
                        <small class="text-muted" style="font-size: 12px;">Minimum order amount (${{ number_format( $min_order_amount,2 ) }})</small>
                     </div>
                     <span class="text-muted" style=""></span>
                  </li>

                 <?php } ?>


                  @auth("user")
                  <li class="list-group-item d-flex justify-content-between bg-light">
                     <div class="text-success">
                        <small style="color: #4d4d4d;">Promotion Applied</small>
                        <?php if(!empty($coupon_code)){ ?>
                         <p style="text-align: center; margin-bottom: 0; color: #4d4d4d;" id="promoccode">( {{$coupon_code}} )</p>
                        <?php } ?>
                     </div>
                     <span class="text-success" style="color: #4d4d4d !important;">${{ number_format( $promo_amount,2 ) }}<?php //echo $promo_amount; ?></span>
                  </li>
                  @endauth

                  <?php if($user_status==2){ ?>

                    <li class="list-group-item d-flex justify-content-between bg-light">
                     <div class="text-success">
                        <small>Promotion Applied</small>
                        <?php if(!empty($coupon_code)){ ?>
                         <p style="text-align: center; margin-bottom: 0;" id="promoccode">( {{$coupon_code}} )</p>
                        <?php } ?>
                     </div>
                     <span class="text-success">${{ number_format( $promo_amount,2 ) }}<?php //echo $promo_amount; ?></span>
                  </li> 

                  <?php } ?>  

  <li class="list-group-item d-flex justify-content-between lh-condensed" >
   <div>
      <small class="text-muted">Ticket Fee</small>
   </div>
   <span class="text-muted">${{ number_format( $ticket_fee * $items[0]->quantity ,2 ) }}</span>
  </li>
    <li class="list-group-item d-flex justify-content-between lh-condensed" style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">
   <div>
      <small class="text-muted">Ticket Service Fee</small>
   </div>
   <span class="text-muted">${{ number_format( $ticket_service_fee * $items[0]->quantity ,2 ) }}</span>
</li>

                  <li class="list-group-item d-flex justify-content-between lh-condensed">
                     <div>
                        <small class="text-muted">Sub Total</small>
                     </div>
                     <span class="text-muted" style="">${{ number_format( $subtotal,2 ) }}</span>
                  </li>


                  <li class="list-group-item d-flex justify-content-between lh-condensed" style="border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                     <div>
                        <small class="text-muted">Sales Tax<?php if($saletax!=0){?> ({{$vendor->sales_tax}}%)<?php } ?></small>
                     </div>
                     <span class="text-muted">${{ number_format( $saletax,2 ) }}</span>
                  </li>


                  <li class="list-group-item d-flex justify-content-between fulTotal">
                     <span>Total (USD)</span>
                     <strong>${{ number_format( $final_total,2 ) }}</strong>
                  </li>

               </ul>
               @if(Auth::guard("user")->check())
               <form class="card p-2" method="post" action="#" id="coupon_form">
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <div class="input-group">
                     <input type="hidden" name="vendor_id" value="{{$vendor_id}}">
                     <input type="text" class="form-control" id="promo_code" name="promo_code" placeholder="Promo code">
                     <div class="input-group-append">
                        <button type="submit" name="btnRedeem" id="btnRedeem" class="btn btn-secondary">Redeem</button>
                     </div>
                  </div>
                  <label for="promo_code" generated="true" class="error"></label>
               </form>
               <!--<div class="card p-2" style="overflow: auto;">
                  @if(!$delivery_addresss->isEmpty())
                  
                  
                  
                    @foreach($delivery_addresss as $arr)
                  
                  
                  
                      <div class="radio">
                  
                  
                  
                        <label><input onclick="set_address('<?php echo $arr->del_address; ?>','<?php echo $arr->del_lat; ?>','<?php echo $arr->del_long; ?>');" type="radio" value="{{$arr->del_address}}" name="delivery_adder">{{$arr->del_address}}</label>
                  
                  
                  
                      </div>
                  
                  
                  
                    @endforeach
                  
                  
                  
                  @endif
                  
                  
                  
                  </div>-->
               @endif

               <?php if($user_status==2){ ?>

               <form class="card p-2" method="post" action="#" id="coupon_form">
                  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                  <div class="input-group">
                     <input type="hidden" name="vendor_id" value="{{$vendor_id}}">
                     <input type="text" class="form-control" id="promo_code" name="promo_code" placeholder="Promo code">
                     <div class="input-group-append">
                        <button type="submit" name="btnRedeem" id="btnRedeem" class="btn btn-secondary">Redeem</button>
                     </div>
                  </div>
                  <label for="promo_code" generated="true" class="error"></label>
               </form>

               <?php } ?> 

                <?php $baseurl = url('/'); ?>

<?php 

 $vendorsDetials=DB::table('vendor')->where("vendor_id",$vendor_id)->where("login_status",1)->groupBy('vendor_id')->get();

?>
<div class="stic-s-btn"><span class="cnt_shop mt-3"><a href="<?php echo $baseurl;?>/cart">Continue Shopping</a></span>
 </div> 
 <!-- <div class="stic-s-btn"><span class="cnt_shop mt-3"><a href="<?php echo $baseurl;?>/{{$vendorsDetials[0]->username}}/store">Continue Shopping</a></span>
 </div> --> 
            </div>
         </div>
         <div class="col-md-8 order-md-1">
            <div class="py-5 text-center">
               {{-- 
               <h2>Products Summary </h2>
               --}}        
            </div>
            {{-- 
            <div class="yourOrder">
               <div class="row">
                  <div class="col-md-3 col-sm-3 col-xs-3">
                     <div class="sf-provider-des">
                        <div class="sf-thum-bx overlay-black-light">
                           <a href="{{ url('/').'/'.$item->slug.'/details' }}"><img src="{{ $item->imageURL }}" alt=""></a>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-9 col-sm-9 col-xs-9">
                     <div class="sf-about-box">
                        <a href="{{ url('/').'/'.$item->slug.'/details' }}" style="text-decoration: none;">
                           <h2 class="sf-title">  {{$item->name}}</h2>
                        </a>
                        <div class="sf-provider-cat">
                           <strong>By: </strong> <a>{{ $vendor->fullname }}</a> 
                           @if(request()->has("qty"))
                           <strong>Qty:</strong>  {{ request()->get("qty") }} 
                           @endif
                        </div>
                        <div class="proPrice">
                           <div class="cutPrice">${{$item->price}}</div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            --}}

           
            <?php if($user_status==2){ ?>
                
            <h4 class="mb-3 dadrs">Guest Information</h4>

            <form class="needs-g-validation" action="{{url('/submit_order')}}" id="checkout_form" novalidate="">
               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <input type="hidden" name="item_price" value="">
               <input type="hidden" name="item_qty" value="">
               <input type="hidden" name="sub_total" value="{{$subtotal}}">
               <input type="hidden" name="promo_amount" value="{{$promo_amount}}">
               <input type="hidden" name="final_amount" value="{{$final_total}}">
               <input type="hidden" name="ps_id" value="">
               <input type="hidden" name="vendor_id" value="{{$vendor_id}}">
               <input type="hidden" name="user_id" value="{{$user_id}}">
               <input type="hidden" name="saletax" value="{{$saletax}}">
               <input type="hidden" name="excisetax" value="0"> 
               <input type="hidden" name="citytax" value="0">

<input type="hidden" name="ps_type" value="3">
<input type="hidden" name="ticket_service_fee" value="{{$ticket_service_fee}}">
<input type="hidden" name="ticket_fee" value="{{$ticket_fee}}">
<input type="hidden" name="event_ticket_quantity" value="{{ $items[0]->quantity }}">

                <input type="hidden" name="deliveryfee" value="0">
               <input type="hidden" id="latitude" name="latitude" value="{{(!empty($user_info->user_lat) ? $user_info->user_lat : '')}}">

               <input type="hidden" id="longitude" name="longitude" value="{{(!empty($user_info->user_long) ? $user_info->user_long : '')}}">
               <input type="hidden" name="user_status" id="user_status" value="{{$user_status}}">
               <input type="hidden" name="cart_count" id="cart_count" value="{{$cart_count}}">

               <input type="hidden" name="locationId" id="locationId" value="">
               <input type="hidden" name="sourceId" id="sourceId" value="">
               <input type="hidden" name="verificationToken" id="verificationToken" value="">
               <input type="hidden" name="idempotencyKey" id="idempotencyKey" value="">

               <div class="row label-size">

                                <?php if($ticket_qty>1){ ?>

                 <?php for ($i=0; $i < $ticket_qty; $i++) { ?>
                           <div class="col-md-12 mb-3">
                    <h5 class="mb-3 dadrs deep-tiket" >Ticket-<?php echo $i+1;?></h5>
                   
                      </div>

                  <?php if($i==0){ ?> 
                     
                  <div class="col-md-6 mb-3">

                     <label for="firstName">First name</label>
                     <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="{{(!empty($user_info->name) ? $user_info->name : '')}}{{(!empty($last_data_get->name) ? $last_data_get->name : '')}}" required="">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="{{(!empty($user_info->lname) ? $user_info->lname : '')}}{{(!empty($last_data_get->lname) ? $last_data_get->lname : '')}}" required="">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>

                <?php }else{ ?>

                  <div class="col-md-6 mb-3">

                     <label for="firstName">First name</label>
                     <input type="text" class="form-control class_multiple_name" name="firstNameT[]" id="firstNameT" placeholder="" value="" required="">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control class_multiple_name" name="lastNameT[]" id="lastNameT" placeholder="" value="" required="">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>


                <?php } ?> 
               
                <?php } ?>

                <?php }else{ ?> 

                  <div class="col-md-6 mb-3">
                     <label for="firstName">First name</label>
                     <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="{{(!empty($user_info->name) ? $user_info->name : '')}}" required="">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="{{(!empty($user_info->lname) ? $user_info->lname : '')}}" required="">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>

                 <?php } ?>


               </div>
               <div class="row label-size">
                  <div class="col-md-6 mb-3">
                     <label for="email">Email  </label>
                     <input type="email" class="form-control" name="email" id="email" value="" placeholder="">
                     <div class="invalid-feedback">Please enter a valid email address for shipping updates.</div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="user_mob">Mobile Number</label>
                     <input type="text" class="form-control" name="user_mob" id="user_mob" value="" placeholder="" >
                  </div>
               </div>

               <h4 class="mb-3 dadrs">Customer Billing Address</h4>
               <span id="cusadrs">
                  <div class="row">

                  <?php if(isset($user_info->user_address)){ $address = explode(",",$user_info->user_address); $address1 = $address[0]; if(isset($address[1])){ $address2 = $address[1];}else{ $address2 = ''; } } else{ $address1 = ''; $address2 = ''; }  ?>

                     <div class="col-md-6 mb-3">
                        <label for="address">Address 1</label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="" required="" value="{{(!empty($user_info->user_address) ? $address1 : '')}}">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                     </div>

                      <div class="col-md-6 mb-3">
                        <label for="address">Address 2</label>
                        <input type="text" class="form-control" name="address2" id="address2" placeholder=""  value="{{(!empty($user_info->user_address) ? $address2 : '')}}">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                      </div>

                     <div class="col-md-6 mb-3">
                        <label for="address">City</label>
                        <input type="text" class="form-control" name="city" id="city" placeholder="" required="" value="{{(!empty($user_info->user_city) ? $user_info->user_city : '')}}">
                     </div>

                     <div class="col-md-6 mb-3">
                        <label for="state">State</label>
                        <select class="form-control custom-select d-block w-100" name="state" id="state" required="">
                           <!-- <option value="">Choose...</option> -->
                           @if(!$state_list->isEmpty())
                           @foreach($state_list as $arr)
                           <option value="{{$arr->name}}" <?php if(!empty($user_info->user_states)){ if($user_info->user_states == $arr->name){?> selected="selected" <?php } }?>>{{$arr->name}}</option>
                           @endforeach
                           @endif
                        </select>
                        <div class="invalid-feedback">Please provide a valid state.</div>
                     </div>
                     <div class="col-md-6 mb-3">
                        <label for="zip">Zip Code</label>
                        <input type="text" class="form-control" name="zip" id="zip" placeholder="" required="required" value="{{(!empty($user_info->user_zipcode) ? $user_info->user_zipcode : '')}}">
                        <div class="invalid-feedback"> Zip code required.</div>
                     </div>

                    <div class="col-md-6 mb-3">
                    <label for="zip">Country</label>
                    <select class="form-control custom-select d-block w-100" name="country" id="country" required="required"> 
                    <option value="USA">United States</option>
                    <option value="JPN">Japan</option>
                    <option value="AUS">Australia</option>
                    <option value="HKG">Hong Kong</option>
                    <option value="CHN">China</option>
                    <option value="FRA">France</option>
                    <option value="ARE">Dubai</option> 
                    <option value="GBR">United Kingdom</option>
                    <option value="IRL">Ireland</option>
                    <option value="CAN">Canada</option>      
                    </select>
                      <div class="invalid-feedback"> Country required.</div>
                    </div>

                  </div>
               </span>

                <div class="row">
                <div class="col-md-6 mb-3">
                  <h4 class="mb-3 dadrs"><span><input type="radio" name="pickupdel" id="pickupId" value="{{$vendor->pick_up_address}}" required="required" checked="checked"> Pick Up Address</span>
            </h4>
              <span id="pickupadrs">
              <div class="row">
                 <div class="col-md-12 mb-3">
                        <label for="address">{{$vendor->pick_up_address}}</label>
                        </div>
                      </div>
               </span>
             </div>
           </div>
            <div class="row">
            <div class="col-md-12">
             <!-- <h4 class="mb-3 dadrs"><span><input type="radio" name="pickupdel" id="deliveryId" value="delivery"> Delivered To Address</span></h4> -->

             <h4 class="mb-3 dadrs"><span><input type="radio" name="pickupdel" id="deliveryId" value="delivery"> Delivered To Address</span><span class="cpdaddrs" id="dropid" onclick="dAddress()" style="display: none;"><input type="checkbox" name="dropca" id="dropcheck" onclick="dAddress()">Copy Customer Address</span></h4>
             
               <span id="dropadrs" style="display: none;">
                  <div class="row label-size">

                     <?php if(isset($user_info->user_address)){ $address = explode(",",$user_info->user_address); $address1 = $address[0]; if(isset($address[1])){ $address2 = $address[1];}else{ $address2 = ''; } } else{ $address1 = ''; $address2 = ''; }  ?>

                    <div class="col-md-6 mb-3">
                        <label for="address">Address 1</label>
                       <input type="text" class="form-control" name="addressd" id="addressd"  placeholder="" value="">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                     </div>

                      <div class="col-md-6 mb-3">
                        <label for="address">Address 2</label>
                        <input type="text" class="form-control" name="addressd2" id="addressd2"  placeholder="" value="">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                      </div>

                       

                    <!--  <div class="col-md-6">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" name="addressd" id="addressd"  placeholder="" value="">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                     </div> -->

                     <div class="col-md-6 mb-3">
                        <label for="address">City</label>
                        <input type="text" class="form-control" name="cityd" id="cityd" placeholder="" value="">
                     </div>

                     <div class="col-md-6 mb-3">
                        <label for="state">State</label>
                        <select class="form-control custom-select d-block w-100 form-control" name="stated" id="stated">
                           <!-- <option value="">Choose...</option> -->
                           @if(!$state_list->isEmpty())
                           @foreach($state_list as $arr)
                           <option value="{{$arr->name}}" <?php if(!empty($user_info->user_states)){ if($user_info->user_states == $arr->name){?> selected="selected" <?php } }?>>{{$arr->name}}</option>
                           @endforeach
                           @endif
                        </select>
                        <div class="invalid-feedback">Please provide a valid state.</div>
                     </div>
                     <div class="col-md-6 mb-3">
                        <label for="zip">Zip Code</label>
                        <input type="text" class="form-control" name="zipd" id="zipd" placeholder="" value="">
                        <div class="invalid-feedback"> Zip code required.</div>
                     </div>

                     <div class="col-md-6 mb-3">
                     <label for="zip">Country</label>
                    <select class="form-control custom-select d-block w-100" name="countryd" id="countryd" required="required"> 
                    <option value="USA">United States</option>
                    <option value="JPN">Japan</option>
                    <option value="AUS">Australia</option>
                    <option value="HKG">Hong Kong</option>
                    <option value="CHN">China</option>
                    <option value="FRA">France</option>
                    <option value="ARE">Dubai</option> 
                    <option value="GBR">United Kingdom</option>
                    <option value="IRL">Ireland</option>
                    <option value="CAN">Canada</option>      
                    </select>
                      <div class="invalid-feedback"> Country required.</div>
                    </div>

                  </div>
               </span>   
           </div>
         </div>


               
             <!--  <input type="hidden" name="address" id="address" value="">
       
              <input type="hidden" name="address2" id="address2" value="">
                    
              <input type="hidden" name="city" id="city" value="">
                        
              <input type="hidden" name="state" id="state" value="">
                   
              <input type="hidden" name="zip" id="zip" value="">         
                   
             <input type="hidden" name="country" id="country" value=""> -->

               <input type="hidden" class="form-control" name="addressp" id="addressp" placeholder="" value="">

               <input type="hidden" class="form-control" name="cityp" id="cityp" placeholder="" value="">

               <input type="hidden" class="form-control" name="statep" id="statep" value="">

              <input type="hidden" class="form-control" name="zipp" id="zipp" value="">

              <input type="hidden" class="form-control" name="pick_up_address" id="pick_up_address" value="{{$vendor->pick_up_address}}"> 

              <input type="hidden" class="form-control" name="pickup_del" id="pickup_del" value="pickup"> 

            <h4 class="mb-3 dadrs"><span><input type="hidden" name="pickupdel" id="pickupId" value="pickup" required="required" checked="checked"></span>
            </h4>

             
                <input type="hidden" name="pmethodc" id="cashId" value="cash" required="required"> 
             

               <div class='form-row row'>
                  <div class='col-md-12 error form-group hide'>
                     <div class='alert-danger alert'>Please correct the errors and try again.</div>
                  </div>
               </div>
            <input type="hidden" name="comment" id="comment" value="">

               <?php if($subtotal < $min_order_amount){ ?>

              <button type="button" id="checkout_minorder" class="btn btn-primary btn-lg btn-block disabled">Submit Order</button>

               <?php }else{ ?> 

               <button class="btn btn-primary btn-lg btn-block" id="btnCheckout" name="btnCheckout" type="submit">Submit Order</button>

             <?php } ?>

            </form>

          <?php } else{ ?>

               <h4 class="mb-3 dadrs">Customer Information</h4>
            <form class="needs-validation" action="{{url('/submit_order')}}" id="checkout_form" novalidate="">
               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
               <input type="hidden" name="item_price" value="{{-- {{$item->price}} --}}">
               <input type="hidden" name="item_qty" value="{{-- {{$qty}} --}}">
               <input type="hidden" name="sub_total" value="{{$subtotal}}">
               <input type="hidden" name="promo_amount" value="{{$promo_amount}}">
               <input type="hidden" name="final_amount" value="{{$final_total}}">
               <input type="hidden" name="ps_id" value="{{-- {{$item->id}} --}}">
               <input type="hidden" name="vendor_id" value="{{$vendor_id}}">
               <input type="hidden" name="user_id" value="{{$user_id}}">
               <input type="hidden" name="saletax" value="{{$saletax}}">
               <input type="hidden" name="excisetax" value="0"> 
               <input type="hidden" name="citytax" value="0">

<input type="hidden" name="ps_type" value="3">
<input type="hidden" name="ticket_service_fee" value="{{$ticket_service_fee}}">
<input type="hidden" name="ticket_fee" value="{{$ticket_fee}}">
<input type="hidden" name="event_ticket_quantity" value="{{ $items[0]->quantity }}">

              <input type="hidden" name="deliveryfee" value="0">
               <input type="hidden" id="latitude" name="latitude" value="{{(!empty($user_info->user_lat) ? $user_info->user_lat : '')}}">
               <input type="hidden" id="longitude" name="longitude" value="{{(!empty($user_info->user_long) ? $user_info->user_long : '')}}">

            <input type="hidden" name="user_status" id="user_status" value="1">
            <input type="hidden" name="cart_count" id="cart_count" value="{{$cart_count}}">

              <input type="hidden" name="locationId" id="locationId" value="">
              <input type="hidden" name="sourceId" id="sourceId" value="">
              <input type="hidden" name="verificationToken" id="verificationToken" value="">
              <input type="hidden" name="idempotencyKey" id="idempotencyKey" value="">

               <div class="row">

                <?php if($ticket_qty>1){ ?>

                 <?php for ($i=0; $i < $ticket_qty; $i++) { ?>
                           <div class="col-md-12 mb-3">
                    <h5 class="mb-3 dadrs deep-tiket" >Ticket-<?php echo $i+1;?></h5>
                   
                      </div>

                  <?php if($i==0){ ?> 
                     
                  <div class="col-md-6 mb-3">

                     <label for="firstName">First name</label>
                     <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="{{(!empty($user_info->name) ? $user_info->name : '')}}{{(!empty($last_data_get->name) ? $last_data_get->name : '')}}" required="" readonly="readonly">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="{{(!empty($user_info->lname) ? $user_info->lname : '')}}{{(!empty($last_data_get->lname) ? $last_data_get->lname : '')}}" required="" readonly="readonly">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>

                <?php }else{ ?>

                  <div class="col-md-6 mb-3">

                     <label for="firstName">First name</label>
                     <input type="text" class="form-control class_multiple_name" name="firstNameT[]" id="firstNameT" placeholder="" value="" required="">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control class_multiple_name" name="lastNameT[]" id="lastNameT" placeholder="" value="" required="">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>


                <?php } ?> 
               
                <?php } ?>

                <?php }else{ ?> 

                  <div class="col-md-6 mb-3">
                     <label for="firstName">First name</label>
                     <input type="text" class="form-control" name="firstName" id="firstName" placeholder="" value="{{(!empty($user_info->name) ? $user_info->name : '')}}" required="" readonly="readonly">
                     <div class="invalid-feedback">
                        Valid first name is required.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="lastName">Last name</label>
                     <input type="text" class="form-control" name="lastName" id="lastName" placeholder="" value="{{(!empty($user_info->lname) ? $user_info->lname : '')}}" required="" readonly="readonly">
                     <div class="invalid-feedback">
                        Valid last name is required.
                     </div>
                  </div>

                 <?php } ?>

               </div>
               <div class="row">
                  <div class="col-md-6 mb-3">
                     <label for="email">Email  </label>
                     <input type="email" class="form-control" name="email" id="email" value="{{(!empty($user_info->email) ? $user_info->email : '')}}" placeholder="" readonly="readonly">
                     <div class="invalid-feedback">Please enter a valid email address for shipping updates.</div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <label for="user_mob">Mobile Number</label>
                     <input type="text" class="form-control" name="user_mob" id="user_mob" value="{{(!empty($user_info->user_mob) ? $user_info->user_mob : '')}}" placeholder="" readonly="readonly">
                  </div>
               </div>
               <h4 class="mb-3 dadrs">Customer Billing Address</h4>
               <span id="cusadrs">
                  <div class="row">

                  <?php if(isset($user_info->user_address)){ $address = explode(",",$user_info->user_address); $address1 = $address[0]; if(isset($address[1])){ $address2 = $address[1];}else{ $address2 = ''; } } else{ $address1 = ''; $address2 = ''; }  ?>

                     <div class="col-md-6 mb-3">
                        <label for="address">Address 1</label>
                        <input type="text" class="form-control" name="address" id="address" placeholder="" required="" value="{{(!empty($user_info->user_address) ? $address1 : '')}}">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                     </div>

                      <div class="col-md-6 mb-3">
                        <label for="address">Address 2</label>
                        <input type="text" class="form-control" name="address2" id="address2" placeholder=""  value="{{(!empty($user_info->user_address) ? $address2 : '')}}">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                      </div>

                     <div class="col-md-6 mb-3">
                        <label for="address">City</label>
                        <input type="text" class="form-control" name="city" id="city" placeholder="" required="" value="{{(!empty($user_info->user_city) ? $user_info->user_city : '')}}">
                     </div>

                     <div class="col-md-6 mb-3">
                        <label for="state">State</label>
                        <select class="form-control custom-select d-block w-100" name="state" id="state" required="">
                           <!-- <option value="">Choose...</option> -->
                           @if(!$state_list->isEmpty())
                           @foreach($state_list as $arr)
                           <option value="{{$arr->name}}" <?php if(!empty($user_info->user_states)){ if($user_info->user_states == $arr->name){?> selected="selected" <?php } }?>>{{$arr->name}}</option>
                           @endforeach
                           @endif
                        </select>
                        <div class="invalid-feedback">Please provide a valid state.</div>
                     </div>
                     <div class="col-md-6 mb-3">
                        <label for="zip">Zip Code</label>
                        <input type="text" class="form-control" name="zip" id="zip" placeholder="" required="required" value="{{(!empty($user_info->user_zipcode) ? $user_info->user_zipcode : '')}}">
                        <div class="invalid-feedback"> Zip code required.</div>
                     </div>

                    <div class="col-md-6 mb-3">
                    <label for="zip">Country</label>
                    <select class="form-control custom-select d-block w-100" name="country" id="country" required="required"> 
                    <option value="USA">United States</option>
                    <option value="JPN">Japan</option>
                    <option value="AUS">Australia</option>
                    <option value="HKG">Hong Kong</option>
                    <option value="CHN">China</option>
                    <option value="FRA">France</option>
                    <option value="ARE">Dubai</option> 
                    <option value="GBR">United Kingdom</option>
                    <option value="IRL">Ireland</option>
                    <option value="CAN">Canada</option>      
                    </select>
                      <div class="invalid-feedback"> Country required.</div>
                    </div>

                  </div>
               </span>

               <input type="hidden" class="form-control" name="addressp" id="addressp" placeholder="" value="0">

               <input type="hidden" class="form-control" name="cityp" id="cityp" placeholder="" value="0">

               <input type="hidden" class="form-control" name="statep" id="statep" value="0">

              <input type="hidden" class="form-control" name="zipp" id="zipp" value="0">

             <input type="hidden" class="form-control" name="pick_up_address" id="pick_up_address" value="{{$vendor->pick_up_address}}"> 

              <input type="hidden" class="form-control" name="pickup_del" id="pickup_del" value="pickup"> 

            <h4 class="mb-3 dadrs"><span><input type="radio" name="pickupdel" id="pickupId" value="pickup" required="required" checked="checked"> Pick Up Address</span>
            </h4>
              <span id="pickupadrs">
              <div class="row">
                 <div class="col-md-12 mb-3">
                        <label for="address">{{$vendor->pick_up_address}}</label>
                        </div>
                      </div>
               </span>

      
               <h4 class="mb-3 dadrs"><span><input type="radio" name="pickupdel" id="deliveryId" value="delivery"> Delivered To Address</span><span class="cpdaddrs" id="dropid" onclick="dAddress()" style="display: none;"><input type="checkbox" name="dropca" id="dropcheck" onclick="dAddress()">Copy Customer Address</span></h4>
               <span id="dropadrs" style="display: none;">
                  <div class="row">

                     <div class="col-md-6 mb-3">
                        <label for="address">Address 1</label>
                        <input type="text" class="form-control" name="addressd" id="addressd"  placeholder="" value="">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                     </div>

                      <div class="col-md-6 mb-3">
                        <label for="address">Address 2</label>
                        <input type="text" class="form-control" name="addressd2" id="addressd2"  placeholder="" value="">
                        <div class="invalid-feedback"> Please enter your shipping address.</div>
                      </div>

                     <div class="col-md-6 mb-3">
                        <label for="address">City</label>
                        <input type="text" class="form-control" name="cityd" id="cityd" placeholder="" value="">
                     </div>

                     <div class="col-md-6 mb-3">
                        <label for="state">State</label>
                        <select class="form-control form-control custom-select d-block w-100" name="stated" id="stated">
                           <!-- <option value="">Choose...</option> -->
                           @if(!$state_list->isEmpty())
                           @foreach($state_list as $arr)
                           <option value="{{$arr->name}}" <?php if(!empty($user_info->user_states)){ if($user_info->user_states == $arr->name){?> selected="selected" <?php } }?>>{{$arr->name}}</option>
                           @endforeach
                           @endif
                        </select>
                        <div class="invalid-feedback">Please provide a valid state.</div>
                     </div>
                     <div class="col-md-6 mb-3">
                        <label for="zip">Zip Code</label>
                        <input type="text" class="form-control" name="zipd" id="zipd" placeholder="" value="">
                        <div class="invalid-feedback"> Zip code required.</div>
                     </div>

                    <div class="col-md-6 mb-3">
                    <label for="zip">Country</label>
                    <select class="form-control custom-select d-block w-100" name="countryd" id="countryd" required="required"> 
                    <option value="USA">United States</option>
                    <option value="JPN">Japan</option>
                    <option value="AUS">Australia</option>
                    <option value="HKG">Hong Kong</option>
                    <option value="CHN">China</option>
                    <option value="FRA">France</option>
                    <option value="ARE">Dubai</option> 
                    <option value="GBR">United Kingdom</option>
                    <option value="IRL">Ireland</option>
                    <option value="CAN">Canada</option>      
                    </select>
                      <div class="invalid-feedback"> Country required.</div>
                    </div>

                  </div>
               </span>   

                
               <h4 class="mb-3">Payment</h4>

            
                <?php if($paycount==0){ ?>

                <div class="myOrderPay cashcard">
                <input type="radio" name="pmethodc" id="cashId" value="cash" required="required" checked="checked"> Cash
                </div>

                <?php }else{ ?>

              <?php if($paymentmethod->cash=='' && $paymentmethod->credit_card==''){ ?>  
                 
               <div class="myOrderPay cashcard">
                <input type="radio" name="pmethodc" id="cashId" value="cash" required="required" checked="checked"> Cash
                </div>

               <?php } ?> 

                <?php if($paymentmethod->cash==1){ ?>  
                 
               <div class="myOrderPay cashcard">
                <input type="radio" name="pmethodc" id="cashId" value="cash" required="required" checked="checked"> Cash
                </div>

               <?php } ?>

              <?php if($paymentmethod->credit_card==2){ ?>  
                 
               <!-- start credit card payment -->

            <div class="myOrderPay cashcard" style="border-bottom: 0;">
                  <input type="radio" name="pmethodc" id="cardId" value="card" required="required" checked> Credit/Debit Card
               </div>
               <div class="myOrderPay" id="cardpaybox" style="border-top: 0;">
                  <div class="row">
                         
                <input type="hidden" name="cc_name" class="form-control" id="cc-name" value="Grambunny">
                <input type="hidden" name="creditcardtype" class="form-control" id="creditcardtype" value="MasterCard">                        
                    
                     <div class="col-md-3 mb-3">
                        <label for="cc-number">Card Number</label>
                        <input type="text" name="cc_number" pattern="[0-9]+" title="Please insert valid number" maxlength="22" class="form-control" id="cc-number" value="" required="">
                        <div class="invalid-feedback">Credit card number is required</div>
                     </div>

                  <input type="hidden" name="cc_expiration" class="form-control" id="cc-expiration" value="1">

                   <div class="col-md-3 mb-3">
                        <label for="cc-expiration">Expire Month</label>
                        <select name="cc_expire_month" class="form-control" id="cc_expire_month" required="required">
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12">12</option>
                        </select>
                        <div class="invalid-feedback">
                           Expiration date required
                        </div>
                     </div>
                     <div class="col-md-3 mb-3">
                        <label for="cc-expiration">Expire Year</label>
                        <select name="cc_expire_year" class="form-control" id="cc_expire_year" required="required">
                        <?php for($i=0;$i<21;$i++){
                           echo "<option value='".(date('Y')+$i)."'>".(date('Y')+$i)."</option>\n";
                           
                           } ?>
                        </select>
                        <div class="invalid-feedback">
                           Expiration date required
                        </div>
                     </div>
                     <div class="col-md-3 mb-3">
                        <label for="cc-expiration">CVV</label>
                        <input type="text" name="cc_cvv" pattern="[0-9]+" title="Please insert valid number" maxlength="4" class="form-control" id="cc-cvv" value="" required="">
                        <div class="invalid-feedback">
                           Security code required
                        </div>
                     </div>
                  </div>
               </div> 

<!--end credit card payment -->

               <?php } ?>

               <?php /* if($paymentmethod->swipe==4){ ?>  
                 
                <div class="myOrderPay cashcard">
                <input type="radio" name="pmethodc" id="swipeId" value="swipe" required="required" checked="checked"> Card Present Swipe Terminal
                </div>

               <?php } */ ?>


              <?php } ?>

      <?php /* if($paycount>0){ ?>        

      <?php if($paymentmethod->square_payment==3){ ?>


<!-- Squre payment start -->

 <div class="myOrderPay squarecard">
                <input type="radio" name="pmethodc" id="squareId" value="square" required="required" checked="checked"> Credit/Debit Card
                </div>

<!--  <div class="myOrderPay" id="squarebox">

<div id="payment-form">
    <div id="card-container"></div>
    </div>
    <div id="payment-status-container"></div> 

  </div>  -->

<!-- Squre payment End --->

 <?php }else{ ?>



<?php } } */ ?>  

               <div class='form-row row'>
                  <div class='col-md-12 error form-group hide'>
                     <div class='alert-danger alert'>Please correct the errors and try again.</div>
                  </div>
               </div>
               <h4 class="mb-3">Comment</h4>
               <div class="myOrderPay">
                  <div class="row">
                     <div class="col-md-12 mb-6">
                        <textarea id="comment" name="comment" maxlength="250" rows="3" style="width:100%"> </textarea>
                     </div>
                  </div>
               </div>

             <?php if(!empty($user_id)){


               if($paycount==0){ ?>

               <button class="btn btn-primary btn-lg btn-block" id="btnCheckout" name="btnCheckout" type="submit">Submit Order</button>

                <?php }else{ ?>

               <?php //if($paymentmethod->cash==1 || $paymentmethod->credit_card==2 || $paymentmethod->swipe==4){ ?>

               <button class="btn btn-primary btn-lg btn-block" id="btnCheckout" name="btnCheckout" type="submit"><?php if($paymentmethod->square_payment==3){ ?>Goto Payments<?php }else{ ?>Submit Order<?php } ?></button>

               <?php /* } */ } ?> 

               <?php /* if($paycount>0){ ?>

                <?php if($paymentmethod->square_payment==3){ ?> 

                <button class="btn btn-primary btn-lg btn-block" id="card-button" type="button">Submit Order</button>

               <?php }} */ ?>          

              <?php }else{ ?> 

               <button type="button" class="btn btn-primary btn-lg btn-block disabled">Submit Order</button>
              
              <?php } ?>

            </form>

          <?php }?> 
         </div>
      </div>
   </div>
</section>
<!-- Modal -->
<div id="btnCheckoutModal" class="ebcf_modal">

  <div class="ebcf_modal-content">

    <span class="ebcf_close">&times;</span>
    <p>Ticket(s) are being generated, please do not close window.</p>

  </div>

</div>

@endsection
@section('scrips')
<script>
$(document).ready(function() {
// Showing hidden paragraphs
$("#show").click(function() {
$("h2").show();
});
// Hiding displayed paragraphs
$("#hide").click(function() {
$("h2").hide();
});
});
</script>
@endsection