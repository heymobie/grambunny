@if(count($cart))
<div class="table table_summary">				
				 @foreach($cart as $item)
				 <div class="crt_itm_cust1">
                  <p>
				 <?php /*?> <a href="javascript:void(0)" class="remove_item" style="margin-right: 5px;" id="add-qty-{{$item->id}}" data-total-qty="{{$item->qty}}" data-rowid="{{$item->rowId}}" data-cartid="{{$item->id}}"><i class="icon_plus_alt2"></i></a><a href="javascript:void(0)"  data-rowid="{{$item->rowId}}" class="remove_item" id="subtract-qty-{{$item->id}}" data-total-qty="{{$item->qty}}" data-cartid="{{$item->id}}"><i class="icon_minus_alt"></i></a><?php */?> 
				 <strong class="qunt_itm">{{$item->qty}} Day's </strong> {{str_replace('_',' ',$item->name)}}  
				 
				  </p>
                  <p><strong class="pull-right qnt_itm23">${{$item->price}}</strong></p>
                </div>
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
					 <div class="crt_itm_cust23">
					  <p>{{$addon['name']}}</p>
					  <p><strong class="pull-right qnt_itm23">@if($addon['price']>0)${{$addon['price']}}@endif</strong></p>
					</div>
					 @endforeach
				 @endif 
				 @endforeach
              
            </div>
@else
		No items in your cart		
@endif