 @extends('layouts.app')


	
@section("other_css")

<!-- Radio and check inputs -->
<!--[if lt IE 9]> 
<script src="{{ url('/') }}/public/design/front/js/html5shiv.min.js"></script>
<script src="{{ url('/') }}/public/design/front/js/respond.min.js"></script>
<![endif]-->

<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/elegant_font/elegant_font.min.css">

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/bootstrap-slider.css">

<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

<style>
.not_active_google {
  pointer-events: none;
  cursor: default;
}
.not_active_google_fav
{
	color: rgb(221, 221, 221);
    font-size: 24px;
    float: right;
	pointer-events: none;
	cursor: default;
}
</style>	

@stop

@section('content')


    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/public/design/front/img/sub_header_short.jpg" data-natural-width="1350" data-natural-height="335">
        <div id="subheader">
            <div id="sub_content">
              	 <?php 
				 if($total_restaurant){?>
                <h1><span class="restaunt_countrt"><?php echo $total_restaurant; ?></span> @lang('translation.results') </h1>
				<?php }?>
            	<h6>
            		<input type="submit" id="btnChangeLocation" name="btnChangeLocation" class="btn_1" value="@lang('translation.btnChangeLocation')" >
            	</h6>
            	<div id="position">
			        <div class="container">
			            <ul class="brad-home">
			                <li><a href="{{url('/home')}}">@lang('translation.home')</a></li>
			                <li>View all Merchants</li>
			            </ul>
			        </div>
			    </div>
            </div>
        </div>
    </section>
    <!-- End SubHeader ============================================ -->


    <div class="collapse" id="collapseMap">
        <div id="map" class="map"></div>
    </div><!-- End Map -->
    <!-- Content ================================================== -->

    <section style="background: #f5f5f5;">
	    <div class="container-fluid margin_20_35">	        

	        <div class="row">

	            <div class="col-md-3 col-sm-4 col-xs-12 margin-top-minus-60 ">
				
				     <div class="ftltr-head">
                    	<h3>@lang('translation.filters')</h3>
                        <a href="#" id="clear_all_search">@lang('translation.clearAll')</a>
                    </div>
					<div id="j_m_data" style="display:none"><?php print_r($j_map);?></div>
					
					<!--<textarea id="j_m_data" style="display:none" ><?php print_r($j_map);?></textarea>-->
					<form name="advance_search" id="advance_search" method="get">
					<input type="hidden" name="total_rec_counter" id="total_rec_counter" value="<?php echo $total_restaurant;?>" />
					<input type="hidden" name="surbur" id="surbur" value="" />
					<input type="hidden" name="pcode" id="pcode" value="" />
					<input type="hidden" name="loc_id" id="loc_id" value="" />
					<input type="hidden" name="search_type" id="search_type" value="" />
                    <div class="dist-range">
					
                    <h2>Distance Range (Mile)</h2>
<input id="ex4" type="text" name="range" data-slider-ticks="[5, 10, 20, 30, 40, 50]" data-slider-ticks-snap-bounds="30" data-slider-ticks-labels='["5m", "10m", "20m", "30m", "40m", "50m"]'/>
					</div>

                   <!-- <div class="dist-range">
					
                    <div class="box box-blue box-example-square">
              <h2>Price</h2>
              <div class="box-body">
                <select id="example-square" name="rating" autocomplete="off">
                  <option value=""></option>
                  <option value="1">$</option>
                  <option value="2">$$</option>
                  <option value="3">$$$</option>
                  <option value="4">$$$$</option>
                  <option value="5">$$$$$</option>
                </select>
              </div>
            </div>

					</div>-->
                    
	                <div id="filters_col" class="side_bar_filter">
	                	<div class="filter_type">
                        	
						<a data-toggle="collapse" href="#collapseFilters3" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">@lang('translation.price')<i class="fa fa-caret-right pull-right"></i></a>
	                    <div class="collapse in" id="collapseFilters3">
	                        <div class="filter_type">
	                            <ul class="control-group">
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_price_level[]" class="priceclass icheck" value="5" ><span class="rating">
	                                            <i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_price_level[]" class="priceclass icheck" value="4"><span class="rating">
	                                            <i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_price_level[]" class="priceclass icheck" value="3"><span class="rating">
	                                            <i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_price_level[]" class="priceclass icheck" value="2"><span class="rating">
	                                            <i class="fa fa-usd voted"></i><i class="fa fa-usd voted"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_price_level[]" class="priceclass icheck" value="1"><span class="rating">
	                                            <i class="fa fa-usd voted"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i><i class="fa fa-usd"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                            </ul>
	                        </div>
	                    </div> 
                        
                            <!--<a data-toggle="collapse" href="#collapseFilters0" aria-expanded="false" aria-controls="collapseFilters0" id="filters_col_bt">I want<i class="fa fa-caret-right pull-right"></i></a>
							
							<div class="collapse in" id="collapseFilters0">	                    	
	                        <div class="filter_type">
	                            <ul>
									 <li>	
										 <label class="control control--checkbox">Delivery
                                      <input type="checkbox" name="cartType[]" class="service_type icheck" id="Delivery" value="Delivery"/><div class="control__indicator"></div>
                                    </label>
									 </li>
									 <li>	
										 <label class="control control--checkbox">Pickup
                                      <input type="checkbox" name="cartType[]" class="service_type icheck" id="PickUp" value="Pickup"/><div class="control__indicator"></div>
                                    </label>
									 </li>
	                               
	                            </ul>
	                        </div>
	                    </div>-->
                     	 <?php /*?><div class="collapse in" id="collapseFilters0">
                             <ul class="control-group">
                                <li>
									<label class="control control--radio">Delivery
                                		<input type="radio"  name="cartType"  value="Delivery" id="Delivery" class="icheck service_type">
                                		<div class="control__indicator"></div>
                                	</label>
								
                                </li>
                                <li>
									
									<label class="control control--radio">Pickup
                                		<input type="radio"   name="cartType" value="Pick-up"  id="PickUp" class="icheck service_type">
                                        <div class="control__indicator"></div>                                			
                            		</label>
									
                            	</li>                                
                            </ul>
                        </div><?php */?>
                        </div>

						<?php if(count($cuisine_list)>0){?>
	                    <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">@lang('translation.cuisine')<i class="fa fa-caret-right pull-right"></i></a>
	                    <div class="collapse in" id="collapseFilters">	                    	
	                        <div class="filter_type">
	                            <ul>
	                             <li>
										 <label class="control control--checkbox">@lang('translation.all')
										 
										  <input type="checkbox" class="foodallclass" name="foodType[]" id="foodtype-0" value="0"> 
										<div class="control__indicator"></div>
										</label>
									</li>
									 @foreach($cuisine_list as $Clist)		
									 	<?php $check = '';
											if((!empty($search_cuisine)) && (in_array($Clist->cuisine_id, $search_cuisine)))
											{
												$check = 'checked="checked"';
											}
										$lang = Session::get('locale');	
										?>
									 <li>	
										 <label class="control control--checkbox">

					<?php if($lang=='ar'){	echo $Clist->cuisine_name_ar; }else{ echo $Clist->cuisine_name; } ?>

                                      <input type="checkbox" name="foodType[]" class="foodclass icheck" id="foodtype-{{$Clist->cuisine_id}}" value="{{$Clist->cuisine_id}}" {{$check}}/><div class="control__indicator"></div>
                                    </label>
									 </li>
									
									 
									 @endforeach
	                               
	                            </ul>
	                        </div>
	                    </div>
	                       
						<?php } ?>
						
						<a data-toggle="collapse" href="#collapseFilters2" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">@lang('translation.rating')<i class="fa fa-caret-right pull-right"></i></a>
	                    <div class="collapse in" id="collapseFilters2">
	                        <div class="filter_type">
	                            <ul class="control-group">
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_rating[]" class="ratingclass icheck" value="5" ><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_rating[]" class="ratingclass icheck" value="4"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_rating[]" class="ratingclass icheck" value="3"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_rating[]" class="ratingclass icheck" value="2"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                                <li>
	                                    <label class="control control--checkbox">
	                                        <input type="checkbox" name="rest_rating[]" class="ratingclass icheck" value="1"><span class="rating">
	                                            <i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>
	                                        </span>
                                            <div class="control__indicator"></div>
	                                    </label>
	                                </li>
	                            </ul>
	                        </div>
	                    </div> 
						
						
	                     
	                    
	                </div>
						</form>
	            </div>

	            <div class="col-md-9 col-sm-8 col-xs-12">

				
					<div class="row">
                        	 <div class="col-md-12 col-sm-12">
                             	<div class="selected-filter">
								
								 <?php echo $selected_cuisine;?>
								
                                	<!--<a href="#" class="slct-fltr" id="slct-fltr1">Hamburger<span><i class="fa fa-times"></i></span></a>-->
                                	
                                </div>
                                
                                <div class="searchSort">
                                <?php /*?><div class="rest-location">
                              <div class="type-location-container" ng-class="{'hide-locate-me':vm.hideLocateMe}">
              <input type="text" class="type-location ng-pristine ng-untouched ng-valid ng-empty" name="location" placeholder="Street address, city, state" autocomplete="">
              <span class="search-icon">
                <i class="fa fa-map-marker"></i>
              </span>
              <div class="clear-input-container">
                <span class="clear-input"></span>
              </div>
              
            </div>
                                </div>
                                <div class="rest-location">
                              <div class="type-location-container" ng-class="{'hide-locate-me':vm.hideLocateMe}">
              <input type="text" class="type-location ng-pristine ng-untouched ng-valid ng-empty" name="location" placeholder="Indian, Italian, Sushi" autocomplete="">
              <span class="search-icon">
                <i class="fa fa-search"></i>
              </span>
              <div class="clear-input-container">
                <span class="clear-input"></span>
              </div>
              
            </div>
                                </div><?php */?>
                                <!--<div class="searctt">
                                <label class="searchSort-label"> Sort by: </label>
                                <div class="s-select">
                                <select name="sort_by" class="searchSort-select" id="ghs-select-sort" >
                                    <option value="0"> Default </option>
                                    <option value="5"> Rating </option>    
                                    <option value="3"> Distance </option>                               
                                    <option value="2"> Delivery Min </option>
                                </select> 
								<?php /*?><option value=""> Distance </option>
                                    <option value=""> Delivery Estimate </option><?php */?>
                                </div>
                                </div>-->

                                <input type="hidden" id="ghs-select-sort" value="0" name="sort_by">
                                
                                </div>
                             </div>
	                        <div class="col-md-12 col-sm-12">
								<div class="resto-near">
	                            
                                <a href="#" class="show-map-mode">@lang('translation.showMap')</a>
                                <a href="#" class="show-list-mode">@lang('translation.showList')</a>
                                <p class="rst-lst-top">
                                    <span class="restaunt_countrt"><?php echo $total_restaurant; ?> </span> <span>&nbsp;Merchants</span>
	                            </p>
                                </div>
                                
	                        </div>
	                    </div>
				
				<div class="strip_list wow fadeIn" data-wow-delay="0.3s" id="listing_restaurant_div">
	                	
	                    <div class="row">
                        	<div class="col-md-12">

                	<div class="searchResults">


						<div class="row"> 
							<div class="col-md-12 col-sm-12">
								<div class="resto-near">	                            	
	                            	<h4>4 Results Found</h4>                                
                                </div>                                
	                        </div>
							<div class="col-md-3 col-sm-6 equal-col">
								<div class="sf-search-result-girds" id="proid-30">
					                            
					                <div class="sf-featured-top">
					                    <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_45198006-600x450.jpg)"></div>

					                    <div class="sf-overlay-box"></div>

					                    <span class="sf-categories-label"><a href="#">Event Organizer</a></span>

					                    <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

										<a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
					                    
					                    <div class="sf-featured-info">
					                        <div class="sf-featured-sign">Featured</div>
					                        <div class="sf-featured-provider">Robert M</div>
					                        <div class="sf-featured-address"><i class="fa fa-map-marker"></i>  Ballston Lake, United States </div>
					                         <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
					                    </div>

										<a href="" class="sf-profile-link"></a>

					                </div>
					                
					                <div class="sf-featured-bot">
					                    <div class="sf-featured-comapny">CLASSIC EVENT</div>
					                    <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
					                    <div class="btn-group sf-provider-tooltip" >
										  <a href="#">Read More</a>
										</div>
					                </div>
					                
					            </div>
							 </div>
							 <div class="col-md-3 col-sm-6 equal-col">
								<div class="sf-search-result-girds" id="proid-30">
					                            
					                <div class="sf-featured-top">
					                    <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_56063034-3-600x450.jpg)"></div>

					                    <div class="sf-overlay-box"></div>

					                    <span class="sf-categories-label"><a href="#">IT/WEB DEVELOPMENT</a></span>

					                    <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

										<a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
					                    
					                    <div class="sf-featured-info">
					                        
					                        <div class="sf-featured-provider">James Miller</div>
					                        <div class="sf-featured-address"><i class="fa fa-map-marker"></i>   Cambridge, United Kingdom </div>
					                         <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
					                    </div>

										<a href="" class="sf-profile-link"></a>

					                </div>
					                
					                <div class="sf-featured-bot">
					                    <div class="sf-featured-comapny">AONE THEME WEB</div>
					                    <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
					                    <div class="btn-group sf-provider-tooltip" >
										  <a href="#">Read More</a>
										</div>
					                </div>
					                
					            </div>
							 </div>
							 <div class="col-md-3 col-sm-6 equal-col">
								<div class="sf-search-result-girds" id="proid-30">
					                            
					                <div class="sf-featured-top">
					                    <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_38993507-600x450.jpg)"></div>

					                    <div class="sf-overlay-box"></div>

					                    <span class="sf-categories-label"><a href="#"> INTERIOR DECORATOR</a></span>

					                    <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

										<a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
					                    
					                    <div class="sf-featured-info">
					                        
					                        <div class="sf-featured-provider">Joshep Desoja</div>
					                        <div class="sf-featured-address"><i class="fa fa-map-marker"></i>  Brooklyn, United States </div>
					                         <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
					                    </div>

										<a href="" class="sf-profile-link"></a>

					                </div>
					                
					                <div class="sf-featured-bot">
					                    <div class="sf-featured-comapny">RED PLAINS PERSONAL</div>
					                    <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
					                    <div class="btn-group sf-provider-tooltip" >
										  <a href="#">Read More</a>
										</div>
					                </div>
					                
					            </div>
							 </div>
							 <div class="col-md-3 col-sm-6 equal-col">
								<div class="sf-search-result-girds" id="proid-30">
					                            
					                <div class="sf-featured-top">
					                    <div class="sf-featured-media" style="background-image:url(https://aonetheme.com/extend/wp-content/uploads/2016/08/dreamstime_l_56063034-600x450.jpg)"></div>

					                    <div class="sf-overlay-box"></div>

					                    <span class="sf-categories-label"><a href="#">IT/WEB DEVELOPMENT</a></span>

					                    <span class="sf-featured-approve"><i class="fa fa-check"></i><span>Verified</span></span>

										<a class="sf-featured-item" href="javascript:;"><i class="fa fa-heart-o"></i></a>
					                    
					                    <div class="sf-featured-info">
					                        
					                        <div class="sf-featured-provider">Dayle Stayn</div>
					                        <div class="sf-featured-address"><i class="fa fa-map-marker"></i> Brooklyn, United States </div>
					                         <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
					                    </div>

										<a href="" class="sf-profile-link"></a>

					                </div>
					                
					                <div class="sf-featured-bot">
					                    <div class="sf-featured-comapny">PIXEL PERFECT WEB</div>
					                    <div class="sf-featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus tortor [...]</div>
					                    <div class="btn-group sf-provider-tooltip" >
										  <a href="#">Read More</a>
										</div>
					                </div>
					                
					            </div>
							 </div>
						</div> 


						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="resto-near">	                            	
	                            	<h4>4 Results Found</h4>                                
                                </div>                                
	                        </div>
							<div class="col-md-3 col-sm-6 equal-col">
								<div class="sl-featuredProducts--post">
	                                <figure>
	                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-08.jpg" alt="Image Description">
	                                    <figcaption>
	                                        <div class="sl-slider__tags">
	                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
	                                        </div>
	                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
	                                    </figcaption>
	                                </figure>
	                                <div class="sl-featuredProducts--post__content">
	                                    <div class="sl-featuredProducts--post__title">
	                                        <h6>Kensington Contour 2.0 Backpack</h6>
	                                    </div>
	                                    <div class="sl-featuredProducts--post__price">
	                                        <h5>$12.19</h5>
	                                        <h6>$19.99</h6>
	                                    </div>
	                                    <div class="sl-featureRating">
	                                        <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
	                                        <em>(1887 Feedback)</em>
	                                    </div>
	                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
	                                    <button class="btn sl-btn">Add To Cart</button>
	                                </div>
	                            </div>
							</div>
							<div class="col-md-3 col-sm-6 equal-col">
								<div class="sl-featuredProducts--post">
                                <figure>
                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-06.jpg" alt="Image Description">
                                    <figcaption>
                                        <div class="sl-slider__tags">
                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                        </div>
                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                    </figcaption>
                                </figure>
                                <div class="sl-featuredProducts--post__content">
                                    <div class="sl-featuredProducts--post__title">
                                        <h6>Kensington Contour 2.0 Backpack</h6>
                                    </div>
                                    <div class="sl-featuredProducts--post__price">
                                        <h5>$12.19</h5>
                                        <h6>$19.99</h6>
                                    </div>
                                    <div class="sl-featureRating">
                                        <div class="star-rating">
									        <span class="fa fa-star" data-rating="1"></span>
									        <span class="fa fa-star" data-rating="2"></span>
									        <span class="fa fa-star" data-rating="3"></span>
									        <span class="fa fa-star" data-rating="4"></span>
									        <span class="fa fa-star" data-rating="5"></span>
									        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
									      </div>
                                        <em>(1887 Feedback)</em>
                                    </div>
                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                    <button class="btn sl-btn">Add To Cart</button>
	                               </div>
	                            </div>
	                        </div>
	                        <div class="col-md-3 col-sm-6 equal-col">
								<div class="sl-featuredProducts--post">
	                                <figure>
	                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-02.jpg" alt="Image Description">
	                                    <figcaption>
	                                        <div class="sl-slider__tags">
	                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
	                                        </div>
	                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
	                                    </figcaption>
	                                </figure>
	                                <div class="sl-featuredProducts--post__content">
	                                    <div class="sl-featuredProducts--post__title">
	                                        <h6>Kensington Contour 2.0 Backpack</h6>
	                                    </div>
	                                    <div class="sl-featuredProducts--post__price">
	                                        <h5>$12.19</h5>
	                                        <h6>$19.99</h6>
	                                    </div>
	                                    <div class="sl-featureRating">
	                                        <div class="star-rating">
										        <span class="fa fa-star" data-rating="1"></span>
										        <span class="fa fa-star" data-rating="2"></span>
										        <span class="fa fa-star" data-rating="3"></span>
										        <span class="fa fa-star" data-rating="4"></span>
										        <span class="fa fa-star" data-rating="5"></span>
										        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
										      </div>
	                                        <em>(1887 Feedback)</em>
	                                    </div>
	                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
	                                    <button class="btn sl-btn">Add To Cart</button>
	                                </div>
	                            </div>
							</div>
							<div class="col-md-3 col-sm-6 equal-col">
								<div class="sl-featuredProducts--post">
                                <figure>
                                    <img src="http://amentotech.com/htmls/servosell/images/vendor/products/img-03.jpg" alt="Image Description">
                                    <figcaption>
                                        <div class="sl-slider__tags">
                                            <a href="javascript:void(0);" class="sl-bg-red-orange">12% OFF</a>
                                        </div>
                                        <a href="javascript:void(0);"><i class="far fa-heart"></i></a>
                                    </figcaption>
                                </figure>
                                <div class="sl-featuredProducts--post__content">
                                    <div class="sl-featuredProducts--post__title">
                                        <h6>Kensington Contour 2.0 Backpack</h6>
                                    </div>
                                    <div class="sl-featuredProducts--post__price">
                                        <h5>$12.19</h5>
                                        <h6>$19.99</h6>
                                    </div>
                                    <div class="sl-featureRating">
                                        <div class="star-rating">
									        <span class="fa fa-star" data-rating="1"></span>
									        <span class="fa fa-star" data-rating="2"></span>
									        <span class="fa fa-star" data-rating="3"></span>
									        <span class="fa fa-star" data-rating="4"></span>
									        <span class="fa fa-star" data-rating="5"></span>
									        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
									      </div>
                                        <em>(1887 Feedback)</em>
                                    </div>
                                    <em>By: <a href="javascript:void(0);">Bags &amp; Bags Co.</a></em>
                                    <button class="btn sl-btn">Add To Cart</button>
	                               </div>
	                            </div>
	                        </div>
						</div>

	            	</div>






                            <div class="table-responsive resto-table">
                    <table id="listing_restaurant">
								
								@if(!empty($rest_listing))
									@foreach($rest_listing as $special_list)
				
				
					 
								<?php 
								$open_time = '';
								$day_name =  strtolower(date("D",strtotime(date('Y-m-d'))));
								$rest_end = '';
							 
								
								
								$old_day = 0;
								$current_time =$tt=  strtotime(date("H:i"));
								

															
									if($day_name=='sun') { 
							
													if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_sat)){	
																$rest_time2 = explode('_',$special_list->rest_sat);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_sat);
																}
															}
													}
													else
													{
								
														if(!empty($special_list->rest_sun)){														
														
														$rest_time1 = explode('_',$special_list->rest_sun);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_sat)){	
																$rest_time2 = explode('_',$special_list->rest_sat);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_sat);
																}
															}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_sun);
															
														}
													}
													}
												}
									if($day_name=='mon') { 
													/*if(!empty($special_list->rest_mon)){
													$open_time = str_replace('_',' to ',$special_list->rest_mon);
													$rest_time1 = explode('_',$special_list->rest_mon);
													$rest_end = $rest_time1[1];
													}*/
													
													if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_sun)){	
																$rest_time2 = explode('_',$special_list->rest_sun);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_sun);
																}
															}
													}
													else
													{
													
														if(!empty($special_list->rest_mon)){														
														
														$rest_time1 = explode('_',$special_list->rest_mon);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_sun)){	
																$rest_time2 = explode('_',$special_list->rest_sun);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_sun);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_sun);
														}
													}
													}
													
												}
									if($day_name=='tue') {
													/*if(!empty($special_list->rest_tues)){
													$open_time = str_replace('_',' to ',$special_list->rest_tues);
													$rest_time1 = explode('_',$special_list->rest_tues);
													$rest_end = $rest_time1[1];													
													$end = date("H:i", (strtotime($rest_end)));
													
								
												  }	*/
												  
												   
												  	  if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_mon)){	
																$rest_time2 = explode('_',$special_list->rest_mon);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_mon);
																}
															}
													}
													else
													{
													if(!empty($special_list->rest_tues)){														
														
														$rest_time1 = explode('_',$special_list->rest_tues);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_mon)){	
																$rest_time2 = explode('_',$special_list->rest_mon);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_mon);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_tues);
														}
													}	
													}					
												}
										if($day_name=='wed') { 
													/*if(!empty($special_list->rest_wed)){
													
													
													$rest_time1 = explode('_',$special_list->rest_tues);
												echo	$rest_end = $rest_time1[1];							
													
													
													$open_time = $rest_end.'-----'.str_replace('_',' to ',$special_list->rest_wed);
													//$open_time = str_replace('_',' to ',$special_list->rest_wed);
													$rest_time1 = explode('_',$special_list->rest_wed);
													$rest_end = $rest_time1[1];
													
													
													
												  }*/
												  
												  	  if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_tues)){	
																$rest_time2 = explode('_',$special_list->rest_tues);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_tues);
																}
															}
													}
													else
													{
												  
													if(!empty($special_list->rest_wed)){														
														
														$rest_time1 = explode('_',$special_list->rest_wed);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_tues)){	
																$rest_time2 = explode('_',$special_list->rest_tues);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_tues);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_wed);
														}
													}
													}
												}
							if($day_name=='thu') { 
													/*if(!empty($special_list->rest_thus)){
													$open_time = str_replace('_',' to ',$special_list->rest_thus);
													$rest_time1 = explode('_',$special_list->rest_thus);
													$rest_end = $rest_time1[1];
												  }*/
												  
												  if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_wed)){	
																$rest_time2 = explode('_',$special_list->rest_wed);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_wed);
																}
															}
													}
													else
													{
												  
													if(!empty($special_list->rest_thus)){														
														
														$rest_time1 = explode('_',$special_list->rest_thus);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_wed)){	
																$rest_time2 = explode('_',$special_list->rest_wed);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_wed);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_thus);
														}
													}
													
													}
												}
							if($day_name=='fri') {
													/*if(!empty($special_list->rest_fri)){
													$open_time = str_replace('_',' to ',$special_list->rest_fri);
													$rest_time1 = explode('_',$special_list->rest_fri);
													$rest_end = $rest_time1[1];
												  }*/
												  
												   
												  if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_thus)){	
																$rest_time2 = explode('_',$special_list->rest_thus);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_thus);
																}
															}
													}
													else
													{
												  
														if(!empty($special_list->rest_fri)){														
														
														$rest_time1 = explode('_',$special_list->rest_fri);
														
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_thus)){	
																$rest_time2 = explode('_',$special_list->rest_thus);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_thus);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_fri);
														}
													}
													}
												}
							if($day_name=='sat') { 
													/*if(!empty($special_list->rest_sat)){
													$open_time = str_replace('_',' to ',$special_list->rest_sat);
													$rest_time1 = explode('_',$special_list->rest_sat);
													$rest_end = $rest_time1[1];
												  }*/
												  
												  if((!empty($special_list->rest_close)) && 
														(in_array($day_name,(explode(',',$special_list->rest_close)) )))
													{	
														 $open_time = '';
														 
														 if(!empty($special_list->rest_fri)){	
																$rest_time2 = explode('_',$special_list->rest_fri);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_fri);
																}
															}
													}
													else
													{
														if(!empty($special_list->rest_sat)){														
														
														$rest_time1 = explode('_',$special_list->rest_sat);
														$start_time = date("H:i", (strtotime($rest_time1[0])));
														if((strtotime($start_time)>$tt))
														{
														
															if(!empty($special_list->rest_fri)){	
																$rest_time2 = explode('_',$special_list->rest_fri);
																if((strtotime($rest_time2[1])>$current_time))
																{
																	$open_time = str_replace('_',' to ',$special_list->rest_fri);
																}
																}
															
														}
														else
														{														
															$open_time = str_replace('_',' to ',$special_list->rest_sat);
														}
													}
													}
												}
								?>	

								<?php 
									$style="";
								if($special_list->google_type==1)
										{ 
											$style=  'border-bottom: 10px solid #fff; background: #f1f1f1;';
										 }else{
											$style = 'border-bottom: 10px solid #fff;';
										}
									?>
								<tr style=" <?php echo $style;?>">
                                    	<td width="110px">

										<?php 
										if($special_list->google_type!=1)
										{
										?>
                                        <div class="ribbon_1">
                                           @if($special_list->rest_classi!='5')
													@if($special_list->rest_classi=='1') Sponsored @endif
													@if($special_list->rest_classi=='2') Popular @endif
													@if($special_list->rest_classi=='3') Special @endif
													@if($special_list->rest_classi=='4') New @endif
													@if($special_list->rest_classi=='5') Standard @endif
										@endif
                                        </div>
										<?php }?>
                                        <div class="thumb_strip" style=" background: #f1f1f1;">
										
										<?php if($special_list->google_type==1)
										{
										?>										
											
										 <a href="#">
											@if(!empty($special_list->rest_logo))
											<img src="{{$special_list->rest_logo}}" alt="">									
											@else
											<img src="{{ url('/') }}/public/design/front/img/logo.png" alt="">									
											@endif						
											</a>
											
											
										 <?php	
										}
										else
										{
										?>
										
										 <a href="{{url('/'.strtolower(trim(str_replace(' ','_',$special_list->rest_suburb))).'/'.trim($special_list->rest_metatag))}}">
											@if(!empty($special_list->rest_logo))
											<img src="{{ url('/') }}/uploads/reataurant/{{$special_list->rest_logo}}" alt="">									
											@else
											<img src="{{ url('/') }}/public/design/front/img/logo.png" alt="">									
											@endif						
											</a>
										<?php }?>	
	                                	

	                                	</div>	
                                        </td>
                                        <td>
                                          <div class="desc">                               
                                              <h3>
                                              <?php if($lang=='ar'){ 

                                              	$rest_name_ar = DB::table('restaurant')		 
                                                ->where('rest_id', '=' , $special_list->rest_id)
												->value('rest_name_ar');

												echo $rest_name_ar;

                                              }
                                              else{ echo $special_list->rest_name; }	

                                              ?>
                                              </h3>	                                
                                              <div class="location">
                                                   {{$special_list->rest_address}} {{$special_list->rest_state}}  {{$special_list->rest_zip_code}}
                                              </div>
                                          </div>
                                           <div class="time">
                                                <span>
                                                <?php if($special_list->google_type!=1){?>
                                                @if(!empty($open_time))	Open {{ $open_time}} @else Now Closed @endif
                                                <?php }?>
                                                </span>
                                            </div>
                                        </td>
                                        
                                        <td> 

                                       <!-- Deep code --> 	
										
										<?php $avg_rating = $special_list->totalrating;  ?>
                                        <div class="rating">
                                        									   
									  @for ($x = 1; $x < 6; $x++)
										@if(($avg_rating>0) && ($avg_rating>=$x))
										<i class="icon_star voted"></i>
										@else
											<i class="icon_star"></i>
										@endif
									  @endfor 
									  <span><small>
										<a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->rest_suburb))).'/'.$special_list->rest_metatag.'/review')}}">{{$special_list->totalreview}} @lang('translation.reviews')</a></span>
	                                </div>
									
                                   <!-- <div class="dlvry-amt"><h4><span class="spn-actv">$</span><span  class="spn-actv">$</span><span class="spn-actv">$</span><span>$</span><span>$</span></h4></div>-->
								   
								   	<!--<div class="dlvry-amt"><h4><?php for($p=1;$p<=5;$p++){
										$class='';
										if($p<=$special_list->rest_price_level)
										{
											$class='class="spn-actv"';
										}
									?>
									<span <?php echo $class;?>>$</span>
									<?php }?></h4></div>-->
									
									
                                    <div class="min-dlvry-amt">
                                    <?php 
                                   if($special_list->google_type!=1)
                                   {

                                   ?>
                                   	<span class="value h5">${{$special_list->rest_mindelivery}}</span>
                                   	<span class="type caption u-text-secondary">@lang('translation.delivery')</span>
                                   <?php 
	                                }
	                                ?>
                                    </div>
                                    <div class="min-dlvry-amt">
                                    
                                     <?php if($special_list->google_type!=1){?>
                                   <span class="value h5">${{$special_list->rest_min_orderamt}} </span><span class="type caption u-text-secondary">@lang('translation.minimum')</span>
                                   <?php }?>
                                    </div>
                                        </td>
                                        <td><div class="go_to">
	                                <div>				
									
									<?php if($special_list->google_type==1)
										{
									?>
									
										 <a href="#" class="btn_1 not_active_google" >@lang('translation.btnViewMenu')</a>
									<?php
										}
										else
										{
										?>										
										 <a href="{{url('/'.strtolower(trim(str_replace(' ','_',$special_list->rest_suburb))).'/'.$special_list->rest_metatag)}}" class="btn_1">@lang('translation.btnViewMenu')</a>
										<?php 
										}
										?>
															
										 
										 <?php
										 $user_id = 0;
										  $fav_calss = '';
										 	if(!Auth::guest())
											{
												 $user_id = Auth::user()->id ;
												 
												 
										  		if(!empty($fav_list)){
													 
													 if(in_array($special_list->rest_id,$fav_list))
													 {
														 $fav_calss = 'item-like-selected';
													 }												 
												}
											}
										 ?>
								
								 	<?php if($special_list->google_type==1)
										{
									?>
									<a class="not_active_google_fav" href="javascript:void(0)"><i class="fa fa-heart"></i></a>
									
									<?php
										}
										else
										{
										?>	
										<a class="item_like {{ $fav_calss }}" href="javascript:void(0)" data-userid="<?php echo $user_id;?>" data-restid="<?php echo $special_list->rest_id;?>" id="rest_fav_id_<?php echo $special_list->rest_id;?>" data-googletype="<?php echo $special_list->google_type;?>"><i class="fa fa-heart"></i></a>
										<?php 
										}
										?>
									
										  
										  
	                                </div>
	                            </div></td>
                                    </tr>
				
				
				
					
				
				
				
								                <!-- End col-md-6-->
											
												@endforeach
											@else
												No search results near you
												<br /><br />
								We didn't find any restaurants in your area matching your search filters.
											@endif
									
			
				</table>
                                </div>



                            </div>
	                        
                            
	                        
	                    </div>
	                </div>
					
					
					<div class="map-div wow fadeIn" data-wow-delay="0.3s" id="map-restaurant"> </div>
				
				<div id="ajax_page_load">	
				<?php if($page_no>0) {?>	
					
                <a href="javascript:void(0)" class="load_more_bt wow fadeIn" data-wow-delay="0.2s" id="rest_list_load-{{$page_no}}" data-nextpage="{{$page_no}}" >Load more...</a>
			<?php }	?>
				</div>
	               

	            </div>

	        </div>
	    </div>
    </section>
@stop
@section('js_bottom')	


<div class="modal fade" id="google_view_detail" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  			This Restaurant is not registered with us. once it will registered with us, you will be able to see the restaurant detail.
	  	<p>
		
			
		</p>

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="google_fav" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  			This Restaurant is not registered with us. once it will registered with us, you will be able to favourite the restaurant.
	  	<p>
		
			
		</p>

      </div>
    </div>
  </div>
</div>


<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
 
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/public/design/admin/img/ajax-loader.gif" />
	</div>
</div>
    

 	<script src="{{ url('/') }}/public/design/front/js/jquery-1.9.1.min.js"></script>
    <script src="{{ url('/') }}/public/design/front/js/bootstrap.js"></script> 
    <script src="{{ url('/') }}/public/design/front/js/owl.carousel.js"></script> 
    <script src="{{ url('/') }}/public/design/front/js/wow.min.js"></script> 
    <script src="{{ url('/') }}/public/design/front/js/functions.js"></script> 
    <script src="{{ url('/') }}/public/design/front/js/bootstrap-slider.js"></script>    

	
	
<!--MAP WORK--> 	
	
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE&libraries=places" async defer></script>	
<style>
.mapchecked {
    color: orange;
}
</style>

<script>
var base_url = '';
var map;
var infowindow;
var markersArray = [];
var cent_lat;
var cent_long;
var globalMarker = [];

  function initialize() {
  
	var jsn_data =  $('#j_m_data').text();

	var map_rec_total = ($("#total_rec_counter").val());

	if(map_rec_total>0){
		var result = [];
		a = jsn_data.replace('"','') ;
		a = a.split(','); 
		
		while(a[0]) {
			/*result.push(a.splice(0,3));*/
			result.push(a.splice(0,6));
		}
	}
  
	var lat = <?php echo Session::get('home_lat')?>;
	var lng = <?php echo Session::get('home_lng')?>;
     //   var pyrmont = {lat: 22.714169, lng: 75.874362};
	var pyrmont = {lat: lat, lng: lng};
	map = new google.maps.Map(document.getElementById('map-restaurant'), {
	  center: pyrmont,
	  zoom: 12
	});
	infowindow = new google.maps.InfoWindow();
		
		
	var markers = new Array();

	//var PathData1 = <?php //echo $j_map ?>;

	var PathData1 = result;
		
	var locations = PathData1;
	
	if(map_rec_total>0){
			  
		for (var i = 0; i < locations.length; i++) { 
		
			var M_lat=locations[i][1] ;
			var M_lang=locations[i][2] ;
			
			if(i==(locations.length-1))
			{
				var M_lang=locations[i][2].replace('"', '') ;
			}
		
			  				
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(M_lat, M_lang),
				map: map,
				title: locations[i][0]
			  });
			  
			markers.push(marker);
					
				
			  
		   google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				//map.setCenter(new google.maps.LatLng(M_lat, M_lang));
				var map_star='';
				for(wm=1;wm<=5;wm++)
				{
					if(wm<=locations[i][4]){
						map_star+='<span class="fa fa-star mapchecked">';
					}
					else
					{
						map_star+='<span class="fa fa-star">';
					}
				}
				var url="";
				
				var latlng;
				latlng = new google.maps.LatLng(M_lat, M_lang);
				
				
				
			new google.maps.Geocoder().geocode({'latLng' : latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[1]) {
						var country = null, countryCode = null, city = null, cityAlt = null;
						var c, lc, component;
						for (var r = 0, rl = results.length; r < rl; r += 1) {
							var result = results[r];

							if (!city && result.types[0] === 'locality') {
								for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
									component = result.address_components[c];

									if (component.types[0] === 'locality') {
										city = component.long_name;
										break;
									}
								}
							}
							else if (!city && !cityAlt && result.types[0] === 'administrative_area_level_1') {
								for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
									component = result.address_components[c];

									if (component.types[0] === 'administrative_area_level_1') {
										cityAlt = component.long_name;
										break;
									}
								}
							} else if (!country && result.types[0] === 'country'){
								country = result.address_components[0].long_name;
								countryCode = result.address_components[0].short_name;
							}

							if (city && country) {
								break;
							}
						} 						
						var  content_data = '<div class="map_detail"><table border="0"><tbody><tr><td colspan="2" class="map_tit"></td></tr><tr><td valign="top"><img src="'+locations[i][3].replace(/\\\//g, "/")+'" width="50px" height="80px" ></td><td><b><a href="'+url+'/'+city.replace(/ /g, '_')+'/'+locations[i][0].replace(/ /g, '-')+'">'+locations[i][0]+'</a></b><p>'+map_star+'</p></td></tr></tbody></table></div>';
						infowindow.setContent(content_data);
					 }
				}
			});
			infowindow.open(map, marker); 
			}
		  })(marker, i));		  
			  
		}
			  
	}
	 
	 
     bounds = new google.maps.LatLngBounds();
	}
	
$("document").ready(function() {
 initialize();	
});
	
	
	
/*$( window ).on( "load",function(){
 alert('test');
});*/


$( window ).on( "load", initialize );

</script>
	
<!--  MAP WORK END -->

    <script>

$("#ex4").slider({

    ticks: [5, 10, 20, 30, 40, 50],
    ticks_labels: ['5m', '10m', '20m', '30m', '40m', '50m'],
    ticks_snap_bounds: 30,
	
	
	
});

var originalVal;

$('#ex4').slider().on('slideStart', function(ev){
   // originalVal = $('.span2').data('slider').getValue();
    originalVal = $('#ex4').data('slider').getValue();
});

$('#ex4').slider().on('slideStop', function(ev){
  //  var newVal = $('.span2').data('slider').getValue();
    var newVal = $('#ex4').data('slider').getValue();
    if(originalVal != newVal) {
        //alert('Value Changed!');
		
		$("#ajax_favorite_loddder").show();	
	
	   var sort_by = $('#ghs-select-sort').val();

	  
	  $(".load_more_bt").hide();
	  
	  var frm_val = $('#advance_search').serialize();

			var page = 1 //Deep
				
			$.ajax({
			//type: "post",
			dataType: "json",
			url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
			success: function(msg) { 
				$("#listing_restaurant").html(msg);
				
				$("msg.ajax_lsiting_view tr").css('border-bottom: 10px solid #fff; background: #f1f1f1;');
				$("#listing_restaurant").html(msg.ajax_lsiting_view);
				$(".restaunt_countrt").html(msg.total_record);
				$(".selected-filter").html(msg.selected_cuisine);				
				$("#ajax_page_load").html(msg.page_load);		
				$("#j_m_data").html(msg.j_map);		
				$("#total_rec_counter").val(msg.total_record);
				initialize();
				$("#ajax_favorite_loddder").hide();	
					
				}
			});
    }
});

</script>
	
<script type="text/javascript">	
	$(document).on('click', '[id^="rest_list_load-"]', function(){ 

$("#ajax_favorite_loddder").show();	


	
	
  
		var page = $(this).attr('data-nextpage');
		
			$("#rest_list_load-"+page).hide();
			
			
   var sort_by = $('#ghs-select-sort').val();
   
			
		var frm_val = $('#advance_search').serialize();		
		$.ajax({
		//type: "post",
		dataType: "json",
		/*//url:  '?page=' + page+'&'+'cartType='+type_service+'&'+cusin_val,*/
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,	
		success: function(msg) {
		
			$("#listing_restaurant").append(msg.ajax_lsiting_view);
			$("#ajax_page_load").html(msg.page_load);
			
			$("#ajax_favorite_loddder").hide();	
				
				/*$("#ajax_favorite_loddder").hide();	
				$('#BankDetail').html(msg);*/
			}
		});

		
		
});	


$(document).on('click', '.service_type', function(){ 

	
	$("#ajax_favorite_loddder").show();	
	
   var sort_by = $('#ghs-select-sort').val();
  
  $(".load_more_bt").hide();
  
  var frm_val = $('#advance_search').serialize();		
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			$("#listing_restaurant").html(msg);
			
			
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);	
			
				$("#total_rec_counter").val(msg.total_record);		
				$("#j_m_data").html(msg.j_map);		
				initialize();
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
		
});		




$(document).on('click', '.ratingclass', function(){ 
	
	$("#ajax_favorite_loddder").show();	
	
   var sort_by = $('#ghs-select-sort').val();
  
  $(".load_more_bt").hide();
  
  var frm_val = $('#advance_search').serialize();		
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);	
					
				$("#total_rec_counter").val(msg.total_record);
				$("#j_m_data").html(msg.j_map);		
				initialize();
				
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
		
});	

$(document).on('click', '.foodallclass', function(){


	//alert('test');
	//alert($(this).is(':checked'));
	
	if($(this).is(':checked') )
	{	
		$('.foodclass').prop("checked",true);
	}
	else
	{
		$('.foodclass').prop("checked",false);
	}
	
	 var sort_by = $('#ghs-select-sort').val();

	$("#ajax_favorite_loddder").show();	
  
   $(".load_more_bt").hide();
  
  
  var frm_val = $('#advance_search').serialize();	
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			//$("#listing_restaurant").html(msg);
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);	
				$("#total_rec_counter").val(msg.total_record);	
			$("#j_m_data").html(msg.j_map);		
			initialize();	
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
	
	
       
});
$(document).on('click', '.foodclass', function(){


   var sort_by = $('#ghs-select-sort').val();
   
var check_cuisin = 0;
 var cusin_val = [];
 
   $('.foodclass:checked').each(function(i){
		$('.foodallclass').prop("checked",false);
			check_cuisin = 1;
          cusin_val[i] ='foodType[]='+$(this).val();
  });
  
	/*if(check_cuisin==0)
	{
		cusin_val[0]  ='foodType[]=0';
		
		$('.foodallclass').prop("checked",true);
	}*/
	
	
	$("#ajax_favorite_loddder").show();	
  
  $(".load_more_bt").hide();
  
  
  var frm_val = $('#advance_search').serialize();	
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			//$("#listing_restaurant").html(msg);
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);
				$("#total_rec_counter").val(msg.total_record);		
				$("#j_m_data").html(msg.j_map);		
				initialize();		
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
	
	
	
});	


/*********** 16-oct-2017  ****************/


$(document).on('click', '.remove_top_cuisine', function(){


<?php if(request()->cuisine_name){?>
window.history.pushState("string", "Title", "<?php echo url('/restaurant_listing');?>"); 
<?php } ?>

	
		var cui_id = $(this).attr('data-cui_id');
		//alert(cui_id);
		
		
		$('#foodtype-'+cui_id).prop("checked",false);

var check_cuisin = 0;
 var cusin_val = [];
 
	
	$("#ajax_favorite_loddder").show();	
  
  $(".load_more_bt").hide();
  
  
  var frm_val = $('#advance_search').serialize();	
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&'+frm_val,
		success: function(msg) {
			//$("#listing_restaurant").html(msg);
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);
				$("#total_rec_counter").val(msg.total_record);			
				$("#j_m_data").html(msg.j_map);		
				initialize();	
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
	
});	


  
  $(document).on('click', '#clear_all_search', function(){
  
  <?php
  Session::forget('home_cuisine');
  ?>

  	window.history.pushState("string", "Title", "<?php echo url('/restaurant_listing');?>");
	
	
	$('.priceclass').prop("checked",false);
	  	$('.foodclass').prop("checked",false);
  	$('.ratingclass').prop("checked",false);
  	$('.service_type').prop("checked",false);
	$('#ghs-select-sort').val('0');
	$("#ex4").slider('setValue', 5, true);
	$("#ex4").slider('refresh');
	
	
		$("#ajax_favorite_loddder").show();	
  
  $(".load_more_bt").hide();
  
  
  var frm_val = $('#advance_search').serialize();	
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&'+frm_val,
		success: function(msg) {
			//$("#listing_restaurant").html(msg);
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);	
				$("#total_rec_counter").val(msg.total_record);		
				$("#j_m_data").html(msg.j_map);		
				initialize();	
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
	
	
  });
  
  $(document).on('change', '#ghs-select-sort', function(){
  
  var sort_by = $(this).val();

  $("#ajax_favorite_loddder").show();	
  
  $(".load_more_bt").hide();
  
  
  var frm_val = $('#advance_search').serialize();	
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			//$("#listing_restaurant").html(msg);
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);	
				$("#total_rec_counter").val(msg.total_record);	
			$("#j_m_data").html(msg.j_map);		
			initialize();		
			$("#ajax_favorite_loddder").hide();	
				
			}
		});
  
 });
 
 
 
 

 $(document).on('click', '.item_like', function(){
 
 	
		var userid = $(this).attr('data-userid');
		var restid = $(this).attr('data-restid');
		var googletype = $(this).attr('data-googletype');
		
	if(googletype==1)	
	{
		/*$("#google_fav").modal('show');*/		
	}
	else
	{
		
		if(userid==0)
		{ 
			$('#alert_info_fav').show();
			$('#login_2').modal('show');
				
				$("#alert_info_fav").fadeOut(10000); 
		}
		else
		{
			//alert(userid);
			
			$("#ajax_favorite_loddder").show();	
			
			var frm_val = 'userid=' + userid+'&restid=' +restid;
			
			
				$.ajax({
				//type: "post",
				dataType: "json",
				url:  '?userid=' + userid+'&restid=' +restid,				
				url: "{{url('/favourite')}}",
				data: frm_val,
				success: function(msg) {
					 if(msg.favoruite=='1'){
					 	 $("#rest_fav_id_"+restid).addClass('item-like-selected');
					 }else if(msg.favoruite=='0')
					 {
					 	 $("#rest_fav_id_"+restid).removeClass('item-like-selected');
					 }
					
					
					$("#ajax_favorite_loddder").hide();	
						
					}
				});
		}
	}
		
 
	 //$(this).toggleClass('item-like-selected')
  });


$(document).on('click', '.priceclass', function(){ 

	
	$("#ajax_favorite_loddder").show();	
	
	
	
   var sort_by = $('#ghs-select-sort').val();
  
  $(".load_more_bt").hide();
  
  var frm_val = $('#advance_search').serialize();		
		var page = 1
			
		$.ajax({
		//type: "post",
		dataType: "json",
		url:  '?page=' + page+'&sort_by=' + sort_by+'&'+frm_val,
		success: function(msg) {
			
			$("#listing_restaurant").html(msg.ajax_lsiting_view);
			$(".restaunt_countrt").html(msg.total_record);
			$(".selected-filter").html(msg.selected_cuisine);				
			$("#ajax_page_load").html(msg.page_load);
			$("#total_rec_counter").val(msg.total_record);	
			$("#j_m_data").html(msg.j_map);		
			
			initialize();
			
			$("#ajax_favorite_loddder").hide();		
			
			//alert($('div.contextualError.ckgcellphone').css('display')
				
			}
		});
		
});	

/*************16-oct-2017- end ************/

</script>

<script>

    $("a.show-map-mode").click(function(){
		$(this).hide();
		$(this).closest(".resto-near").find("a.show-list-mode").show();
		$("#listing_restaurant_div").hide();
		$("#ajax_page_load").hide();
		$("#map-restaurant").show();
			initialize();
	});
	
	$("a.show-list-mode").click(function(){
		$(this).hide();
		$(this).closest(".resto-near").find("a.show-map-mode").show();
		$("#listing_restaurant_div").show();
		$("#ajax_page_load").show();
		$("#map-restaurant").hide();
	});

	
	$(document).on('click', '#btnChangeLocation', function(){ 
	
			window.location.href='<?php echo url('/home')?>';
	});	
	
	/*$(document).on('click', '.fave_google', function(){ 
	
		$("#google_fav").modal('show');		
	});*/	
	$(document).on('click', '.view_google', function(){ 
	
		$("#google_view_detail").modal('show');		
	});	
	
	 
	
	
  $(document).on('click', '#send_coupon', function(){ 
  
  var form = $("#coupon_form");
    	form.validate();
    	var valid =	form.valid();
		if(valid)
		{
			$("#ajax_favorite_loddder").show();
					
		
		var frm_val = $('#coupon_form').serialize();
			$.ajax({
			type: "POST",
			url: "{{url('/send_coupon_request')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) {
					 $("#ajax_favorite_loddder").hide();			
					 
					 
					 $("#coupons_msg_div").html('Thanks, we will be in touch!');	
					 			
				}
			});

		}
		
  });
  
  $(document).on('click', '#show_owners_form', function(){ 

	
  	document.getElementById('become_partner').reset();
  
      $('#rest_owner').modal('show');
  
  });
	 
	 

	
	
		
    </script>

	
	 
	
@stop