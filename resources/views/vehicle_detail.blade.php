@extends('layouts.app')

@section("other_css")


<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="{{ url('/') }}/design/front/img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ url('/') }}/design/front/img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ url('/') }}/design/front/img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ url('/') }}/design/front/img/apple-touch-icon-144x144-precomposed.png">
<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">



<link href="{{ url('/') }}/design/front/css/owl.carousel.css" rel="stylesheet">
<link href="{{ url('/') }}/design/front/css/owl.theme.css" rel="stylesheet">
<link href="{{ url('/') }}/design/front/css/prettyPhoto.css" rel="stylesheet">


   	  <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->
  <style>
    #addon-items-42 .popup-ctn ul.item-list li, .other-item-popup-ctn ul.item-list li {
        width: 50%;
        padding: 0 20px 0 0;
    }
    table tr td.td-details {
        width: 60%;
    }
    .td-details h5 {
        margin: 0 0 0;
        padding: 0 0 10px 0;
    }
    @media screen and (max-width: 1199px) {
        table tr td.td-details {
          width: 55%;
      } 
    }
	
	
	
  </style>
  
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/ion.imageSlider.css" />
<!--<link rel="stylesheet" type="text/css" href="css/ion.imageSlider.metro.css" />-->
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/ion.imageSlider.minimal.css" />
<!--<link rel="stylesheet" type="text/css" href="css/ion.imageSlider.sexy.css" />-->
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/css/normalize.min.css" />
  

@stop
@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header-car.jpg" data-natural-width="1400" data-natural-height="470">
  <div id="subheader">
    <div id="sub_content">
      <div id="thumb"><?php if(count($vehicle_images)>0){
	  
	  $cove_img = DB::table('vehicle_img')	
						->where('vehicle_id', '=' ,$vehicle_detail[0]->vehicle_id)
						->where('isCover', '=' ,'1') 				
						->get();
	  ?>
	   <img src="{{ url('/') }}/uploads/vehicle/{{$cove_img[0]->imgPath}}" alt="">
	  <?php }else{?>
	  
	  <!--<img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">-->
	  <img src="{{ url('/') }}/design/front/img/logo.png" alt="">	
	  <?php }?></div>
   
	  
	  
	  @if($total_review>0)
      <div class="rating">
	  	  
	   @for ($x = 1; $x < 6; $x++)
		@if(($avg_rating>0) && ($avg_rating>=$x))
		<i class="icon_star voted"></i>
		@else
			<i class="icon_star"></i>
		@endif
	  @endfor 
	  
	  
	  (<small><a href="{{url('/'.strtolower(str_replace(' ','_',trim($vehicle_detail[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_id))).'/review')}}">Read 							  {{$total_review}}, reviews</a></small>)</div>
	  @endif
	  
	  
      <h1>{{$vehicle_detail[0]->make_name.' - '.$vehicle_detail[0]->model_name}}</h1>
      <div><em>{{$vehicle_detail[0]->transport_name}}</em></div>
      <div><i class="icon_pin"></i> {{$vehicle_detail[0]->transport_address}}, {{$vehicle_detail[0]->transport_suburb}} {{$vehicle_detail[0]->transport_pcode}}.</div>
      <div><strong>Minimum Rate:</strong> $ {{$vehicle_detail[0]->vehicle_minrate}}.</div>
    </div>
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<div id="position">
  <div class="container">
    <ul>
      <li><a href="{{url('/')}}">Home</a></li>
	  <li>{{$vehicle_detail[0]->transport_suburb}}</li>
      <li>{{$vehicle_detail[0]->make_name.' - '.$vehicle_detail[0]->model_name}}</li>
    </ul>
  </div>
</div>
<!-- Position -->
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
		
	<?php if(count($vehicle_images)>0){?>
	<div class="crusl-box">
  	<div class="row">
    	<div class="col-md-12">
        	
			
			<div class="ion-image-slider" id="mySlider">
			
			<?php foreach($vehicle_images as $img_list){
			
			$new_path = url('/') .'/uploads/vehicle/thumb/'.$img_list->imgPath;
			if(file_exists($new_path))
			{
				$new_path = url('/') .'/uploads/vehicle/'.$img_list->imgPath;
			}
			
			?>
						
			 <a href="{{ url('/') }}/uploads/vehicle/{{$img_list->imgPath}}"><img src="{{ $new_path }}"  /></a>
			 
			 
				
				<?php }?>
				
 
</div>
			
        </div>
    </div>
  </div>
  
  
	
	
    <?php }?>
  <div id="container_pin">
    <div class="row">
      <div class="col-md-3" id="sidebar1">
        <div class="theiaStickySidebar">
          <p><a href="{{url('/transport_listing')}}" class="btn_side">Back to search</a></p>
          <div class="box_style_1">	
            <ul id="cat_nav">
              <li><a href="#features" class="active">Features</a></li>
              <li><a href="#booking_rates">Booking Rates</a></li>
            </ul>
          </div>
          <!-- End box_style_1 -->
          
          <div class="box_style_2 hidden-xs" id="help"> <i class="icon_lifesaver"></i>
            <h4>Need <span>Help?</span></h4>
            <a href="#" class="phone">+1800123456</a> <small>Monday to Friday 9.00am - 7.30pm</small> </div>
        </div>
      </div>
      <!-- End col-md-3 -->
      
      <div class="col-md-6">
        <div class="box_style_2" id="features">
          <h2 class="inner">{{$vehicle_detail[0]->make_name.' - '.$vehicle_detail[0]->model_name}}</h2>
          <h3 class="nomargin_top" id="features">Features</h3>
          <p>{{$vehicle_detail[0]->vehcile_desc}} </p>
          <table class="table table-striped cart-list vehicle_features">
            <tbody>
              <tr>
                <td><h5 style="font-weight:normal;">Vehicle Make:</h5></td>
                <td class="options">{{$vehicle_detail[0]->make_name}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Vehicle Model":</h5></td>
                <td class="options">{{$vehicle_detail[0]->model_name}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Year Manufactured:</h5></td>
                <td class="options">{{$vehicle_detail[0]->vehicle_year}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Vehicle Category:</h5></td>
                <td class="options">{{$vehicle_detail[0]->vcate_name}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Vehicle Class:</h5></td>
                <td class="options">{{$vehicle_detail[0]->vclass_name}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Maximum number of passengers :</h5></td>
                <td class="options">{{$vehicle_detail[0]->vehicle_pax}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Space for large bags:</h5></td>
                <td class="options">{{$vehicle_detail[0]->vehicle_largebag}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Space for small bags:</h5></td>
                <td class="options">{{$vehicle_detail[0]->vehicle_smallbag}}</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Air Conditioning:</h5></td>
                <td class="options">
					<?php if($vehicle_detail[0]->vehicle_ac){echo 'YES';}else{ echo 'NO';} ?> 
				</td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Music System:</h5></td>
                <td class="options">
					<?php if($vehicle_detail[0]->vehicle_music){echo 'YES';}else{ echo 'NO';} ?>
				 </td>
              </tr>
              <tr>
                <td><h5 style="font-weight:normal;">Video System :</h5></td>
                <td class="options">
					<?php if($vehicle_detail[0]->vehicle_video){echo 'YES';}else{ echo 'NO';} ?> 
				</td>
              </tr>
            </tbody>
          </table>
          <hr>
          <h3 id="booking_rates">Booking Rates</h3>
          <p> Te ferri iisque aliquando pro, posse nonumes efficiantur in cum. Sensibus reprimique eu pro. Fuisset mentitum deleniti sit ea. </p>
		  <?php if((!empty($rate_array)) && ($rate_array['booking_rate_count']>0)){?>
          <table class="table table-striped cart-list ">
             <tbody>
			
			<?php $c=0;
			foreach($rate_array['booking_rate_detail'] as $rdetail)
			{
				if($rdetail->rate_diff=='yes')
				{
				?>
				
				<tr>
                <td colspan="3">
					 <table class="table cart-list table-bg-none">
						 @if(!empty($rdetail->rate_lname) && ($rdetail->rate_lstatus==1)  && ($rdetail->rate_lprice>0))	
						<tr>
						  <td class="td-details"><h5>{{str_replace('_',' ',$rdetail->rate_name)}}</h5>
							<p> {{$rdetail->rate_desc}} .</p></td>
						  <td>{{$rdetail->rate_lname}}</td>
						  <td class="prc_detail"><strong>${{$rdetail->rate_lprice}}</strong></td>
						  <td class="options">	
						  <?php if(!empty($rate_array['item_addons'][$c]['group_name'])){?>
							 <?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 	)
							 {?>
						 <a href="javascript:void(0)" data-trans_id="{{$vehicle_detail[0]->vehicle_transid}}"
						 data-vehicle_id="{{$vehicle_detail[0]->vehicle_id}}"  data-rate_id="{{$rdetail->rate_id}}" id="show_modal"  data-options="large"><i class="icon_plus_alt2"></i></a>
						 
							 <?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}L" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>

							<?php  }?>
						 
							<?php } else{?>					
							<form method="POST" id="crd_frm-{{$rdetail->rate_id}}"   action="{{url('/cart')}}">
							<input type="hidden" name="trans_id" value="{{$vehicle_detail[0]->vehicle_transid}}">
							<input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}">
							<input type="hidden" name="rate_id" value="{{$rdetail->rate_id}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
								 <?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>
							
						<a href="javascript:void(0)" data-restitem="{{$rdetail->rate_id}}" data-options="large" id="add_to_card-{{$rdetail->rate_id}}"><i class="icon_plus_alt2"></i></a>	
							<?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}L" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
							<?php  }?>
						</form>							
							<?php }?>
							</td>
						</tr>
						@endif	
						@if(!empty($rdetail->rate_mname) && ($rdetail->rate_mstatus==1)  && ($rdetail->rate_mprice>0))	
						<tr>
						  <td>&nbsp;</td>
						  <td>{{$rdetail->rate_mname}}</td>
						  <td class="prc_detail"><strong>${{$rdetail->rate_mprice}}</strong></td>
						  <td class="options">	
						   @if(!empty($rate_array['item_addons'][$c]['group_name']))
							 <?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>
								 
							<a href="javascript:void(0)" data-trans_id="{{$vehicle_detail[0]->vehicle_transid}}"
						 data-vehicle_id="{{$vehicle_detail[0]->vehicle_id}}" data-rate_id="{{$rdetail->rate_id}}" id="show_modal"  data-options="medium"><i class="icon_plus_alt2"></i></a>	
							<?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}M" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
							<?php  }?>
							@else						
							<form method="POST" id="crd_frm-{{$rdetail->rate_id}}" action="{{url('/cart')}}">
							
							<input type="hidden" name="trans_id" value="{{$vehicle_detail[0]->vehicle_transid}}">
							<input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}">
							<input type="hidden" name="rate_id" value="{{$rdetail->rate_id}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
							 <?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>
							
						<a href="javascript:void(0)" data-restitem="{{$rdetail->rate_id}}" id="add_to_card-{{$rdetail->rate_id}}"  data-options="medium"><i class="icon_plus_alt2"></i></a>	
						<?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
							<?php  }?>
						</form>							
						
							@endif</td>
						</tr>
						@endif	
						@if(!empty($rdetail->rate_sname)&& ($rdetail->rate_sstatus==1)  && ($rdetail->rate_sprice>0))	
						<tr>
						  <td>&nbsp;</td>
						  <td>{{$rdetail->rate_sname}}</td>
						  <td class="prc_detail"><strong>${{$rdetail->rate_sprice}}</strong></td>
						  <td class="options">	
						  @if(!empty($rate_array['item_addons'][$c]['group_name']))
							
							<?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>	
							
							<a href="javascript:void(0)" data-trans_id="{{$vehicle_detail[0]->vehicle_transid}}"
						 data-vehicle_id="{{$vehicle_detail[0]->vehicle_id}}" 
						 data-rate_id="{{$rdetail->rate_id}}"id="show_modal" data-options="small"><i class="icon_plus_alt2"></i></a>
						 	<?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}S" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
							<?php  }?>	
						 
							@else						
							<form method="POST" id="crd_frm-{{$rdetail->rate_id}}"   action="{{url('/cart')}}">
							
							<input type="hidden" name="trans_id" value="{{$vehicle_detail[0]->vehicle_transid}}">
							<input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}">
							<input type="hidden" name="rate_id" value="{{$rdetail->rate_id}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>
							
						<a href="javascript:void(0)" data-restitem="{{$rdetail->rate_id}}" id="add_to_card-{{$rdetail->rate_id}}" data-options="small"><i class="icon_plus_alt2"></i></a>	
							<?php }else{?>
							 	
								 <a href="javascript:void(0)"  id="tooltip-{{$rdetail->rate_id}}S" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
							<?php  }?>
						</form>							
							@endif</td>
						</tr>
						@endif	
					  </table>
				  </td>
              </tr>
			  
				<?php
				}
				else
				{
					if($rdetail->rate_price>0)
					{
				?>
				<tr>
					<td><h5>{{str_replace('_',' ',$rdetail->rate_name)}}</h5>
					  <p> {{$rdetail->rate_desc}} </p></td>
					<td class="prc_detail"><strong>${{$rdetail->rate_price}}</strong></td>
					<td class="options">
					
					
						<?php if(!empty($rate_array['item_addons'][$c]['group_name'])){?>
						 	<?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>
						<a href="javascript:void(0)" data-trans_id="{{$vehicle_detail[0]->vehicle_transid}}"
						 data-vehicle_id="{{$vehicle_detail[0]->vehicle_id}}" 
						 data-rate_id="{{$rdetail->rate_id}}"
						 id="show_modal"><i class="icon_plus_alt2"></i></a>
							<?php }else{?>
									
									 <a href="javascript:void(0)" id="tooltip-{{$rdetail->rate_id}}" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i></a>
								<?php  }?>	
							
						<?php }else		{?>
						<form method="POST" id="crd_frm-{{$rdetail->rate_id}}"   action="{{url('/cart')}}">
							<input type="hidden" name="trans_id" value="{{$vehicle_detail[0]->vehicle_transid}}">
							<input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}">
							<input type="hidden" name="rate_id" value="{{$rdetail->rate_id}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
							<?php if( (($count_no_days==0) && ($rdetail->rate_name=='Single_day_Hire')) ||
							 			(($count_no_days>0) && ($rdetail->rate_name=='Multi_day_Hire')) 
							 			)
								 {?>	
						<a href="javascript:void(0)" data-restitem="{{$rdetail->rate_id}}" id="add_to_card-{{$rdetail->rate_id}}"><i class="icon_plus_alt2"></i></a>
								<?php }else{?>
							 	
								 <a href="javascript:void(0)" id="tooltip-{{$rdetail->rate_id}}" title = "Not available on your search date criteria"><i class="icon_plus_alt2"></i>
</a>
							<?php  }?>
						</form>								
						<?php }?>
						
						
						</td>
				  </tr>
				<?php
					}
				}
				$c++;
			}
			?>
			
				
			
            </tbody>
          </table>
		  <?php } ?>
          <hr>
          
        </div>
        <!-- End box_style_1 --> 
      </div>
      <!-- End col-md-6 -->
      
      <div class="col-md-3" id="sidebar">
        <div class="theiaStickySidebar">
		<form name="order_frm" id="order_frm" action="{{url('/post_checkout')}}" method="post">
		<input type="hidden" name="rest_metatag" id="rest_metatag" value="{{url('/'.strtolower(str_replace(' ','_',trim($vehicle_detail[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_id))))}}" />
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="hidden" name="trans_id" value="{{$vehicle_detail[0]->vehicle_transid}}">
		  <input type="hidden" name="vehicle_id" value="{{$vehicle_detail[0]->vehicle_id}}">
		  <input type="hidden" name="total_night" value="{{$count_no_days}}" />
		  <input type="hidden" name="each_day_allownce" value="{{$vehicle_detail[0]->vehicle_allownce}}" />
		  
		  
		  	
				<input type="text" class="to_suburb" name="to_suburb" id="to_surbur" value="{{Session::get('to_location')['to_surbur_name']}}" />
				<input type="text" class="to_pincode" name="to_pcode" id="to_pcode" value="{{Session::get('to_location')['to_post_code']}}" />
				<input type="text" class="to_location_id" name="to_loc_id" id="to_loc_id" value="{{Session::get('to_location')['to_loc_id']}}" />
				<input type="text" class="to_search_type" name="to_search_type" id="to_search_type" value="{{Session::get('to_location')['to_search_type']}}" />
				<input type="text" class="to_lable" name="to_lable" id="to_lable" value="{{Session::get('to_location')['to_surbur_lable']}}" />
					
					
          <div id="cart_box">
            <h3>Your order summary <i class="icon_cart_alt pull-right"></i></h3>
			
			<div class="cart_scrllbr">
			
			<div id="order_list">
			
				<?php $cart_price = 0;?>
		@if(isset($cart) && (count($cart)))			
<div class="table table_summary">
            
				
				 @foreach($cart as $item)
				<?php  $cart_price = $cart_price+$item->price;?>
				 <div class="crt_itm_cust1">
                  <p>
				 		<strong>{{$item->qty}}  Day's </strong> {{str_replace('_',' ',$item->name)}} 
				  </p>
                  <p><strong class="pull-right">${{$item->price}}</strong></p>
                </div>
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $cart_price = $cart_price+$addon['price'];?>
					 <div class="crt_itm_cust23">
					  <p>{{$addon['name']}}</p>
					  <p><strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong></p>
					</div>
					 @endforeach
				 @endif 
					
				 @endforeach
              
  </div>
	@else
		No items in your cart		
@endif
			
			
						

				</div>
				<div id="dinein_info_div">
			
				@if(Session::has('cart_bookinginfo') && (!empty(Session::has('cart_bookinginfo'))))	
							
				<div class="table table_summary">			
					<div class="crt_itm_cust1" style=" border-top: 1px solid rgba(128, 128, 128, 0.11);    float: left;   margin-top: 15px;   padding-top: 10px;   width: 100%;">
						<p style="width:100%;float:left !important;">
						<a href="javascript:void(0)" class="remove_item" id="remove_ondate" title="Edit" ><i class="icon_minus-box"></i></a>
						
							Booking From: <?php echo date('d-m-Y',strtotime(Session::get('cart_bookinginfo')['vehicle_ondate'])).' '.Session::get('cart_bookinginfo')['vehicle_ontime'] ?> <br />Booking To: <?php echo date('d-m-Y',strtotime(Session::get('cart_bookinginfo')['vehicle_returndate'])).' '.Session::get('cart_bookinginfo')['vehicle_returntime'] ?>  </p>
					</div>
				</div>
					@endif
							
		   
			</div>
			
   
			</div>
            
            
			
			<?php
			$service_fee = '0.00';
			 if(count($service_area)>0){
				//print_r(Session::get('cart_bookinginfo'));
			?>
			
            <hr>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 <label>Pick-up Location</label>
                <div class="styled-select">
                  <select class="form-control" id="pickup_area" name="pickup_area" required='required'>
                    <option>Pickup Location</option>
					<?php foreach($service_area as $area_list) {
					$select = '';
							if(Session::get('cart_bookinginfo')['vehicle_from']==$area_list->tport_service_suburb)
						{
							 $select = 'selected="selected"';
							 $service_fee = $area_list->tport_service_charge;
						}
					?>
                    <option value="{{$area_list->tport_service_id}}" langid="{{$area_list->tport_service_charge}}" {{$select}}>{{$area_list->tport_service_suburb}}</option>
					<?php }?>
                  </select>
                </div>
              </div>
            </div>
			
			
            <hr>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
                  <label>To Location</label>
				 <input type="text" class="form-control ui-autocomplete-input search_to" placeholder="Your Address or postal code" autocomplete="off" id="search_to" name="search_to" required="required" value="{{Session::get('to_location')['to_surbur_lable']}}" required="required">
                </div>
              </div>
            </div>
			
			
			<?php }?>
            <hr>
            <table class="table table_summary">
              <tbody>
                <tr>
                  <td> Subtotal
				  	 <span class="pull-right" id="Subtotal_amt">${{$cart_price}}</span>
				 	 <input type="hidden" name="sub_total" id="sub_total" value="{{$cart_price}}" />
				  </td>
                </tr>
				
				 <tr id="del_fee">
                  <td>  
				  <input type="hidden" name="delivery_fee" id="delivery_fee" value="{{$service_fee}}" />
				  Service fee 
				  <span class="pull-right delivery_fee">${{$service_fee}}</span></td>
                </tr>
				
				<?php 
				$night_charge = '';
				if($count_no_days>0){
					$night_charge = $count_no_days*$vehicle_detail[0]->vehicle_allownce;
				?>
                <tr>
                  <td> Driver Overnight Charge <span class="pull-right">${{$night_charge}}</span>
				    <input type="hidden" name="driver_allownce" id="driver_allownce" value="{{$night_charge}}" />
				  </td>
                </tr>
				<?php }?>
				
				  <?php $promo_amt_cal = '0.00';
				  
				  
				  if(count($promo_list)>0)
				 {
				
				 if(Auth::guest()) 
				 {
				 ?>
					<tr>
						 <td> Register for Discount</td>
					</tr>
				<?php }
				else{	
					 foreach($promo_list as $promo)
					 { 					 	
						if(($promo->tport_promo_on==0) || ( $promo->tport_promo_on==($user_tot_order+1) ))
						{
							 if($cart_price>0) 
							 {
								 if($promo->tport_promo_mode=='%')
								 {	
									$promo_amt_cal =number_format((($cart_price*$promo->tport_promo_value)/100),2); 
								 }
								 elseif($promo->tport_promo_mode=='$')
								 { 
									 $promo_amt_cal =number_format($promo->tport_promo_value,2); 
								 }
							 }
						 
						 ?>
						 <td>
						  {{$promo->tport_promo_desc}}(End : {{$promo->tport_promo_end}})
						 <input type="hidden" name="promo_mode" id="promo_mode" value="{{$promo->tport_promo_mode}}" />
						 <input type="hidden" name="promo_value" id="promo_value" value="{{$promo->tport_promo_value}}" />
						 <input type="hidden" name="promo_amt_cal" id="promo_amt_cal" value="{{$promo_amt_cal}}" />
						 	<span class="pull-right" id="promo_amt">-${{$promo_amt_cal}}</span></td>
						 <?php
						 break;
						}
						elseif(($promo->tport_promo_on==0) || ( $promo->tport_promo_on>($user_tot_order+1) ))	
						{
						?>				 	
						 <td>												 
						 
						  {{$promo->tport_promo_desc}} (End : {{$promo->tport_promo_end}})
						</td>					 		
					<?php
							break;
					 }
					}
				  }
				  
				}
				?>	 
				
				
				
				
                <tr>
                  <td class="total"> TOTAL 
					  <input type="hidden" name="total_charge" id="total_charge" value="{{$cart_price+$night_charge+$service_fee}}" />
					  <span class="pull-right" id="total_amt">${{$cart_price+$service_fee+$night_charge-$promo_amt_cal}}</span>
					  
				  </td>
                </tr>
              </tbody>
            </table>
            <hr>
            <!--<a class="btn_full" href="cart.html">Order now</a> -->
			<a class="btn_full" href="javascript:void(0)" id="plase_order">Order now</a>
			</div>
          <!-- End cart_box --> 
		   </form> 
        </div>
      </div>
      <!-- End col-md-3 --> 
      
    </div>
    <!-- End row --> 
  </div>
  <!-- End container pin --> 
</div><!-- End container -->
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')


<div class="modal fade" id="other-items" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title choice">Make your choice</h4>
      </div>
      <div class="modal-body popup-ctn">
        <div class="other-item-popup-ctn rest_itm_pop" id="show_mod_addon">
		<form name="modal">
          <h2>Tokyo Original Burger</h2>
          <p class="dei-text">100% minced beef patty, cheese, pickles, fresh lettuce and tomato, special Japo mayonnaise base sauce, served on a sesame burger bun</p>
          <h3 class="small-title">Remove</h3>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <ul class="item-list">
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Cheese <small class="pull-right">� 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"  class="icheck">
                    NO Pickles <small class="pull-right">� 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Fresh Lettuce <small class="pull-right">� 8,30</small></label>
                </li>
              </ul>
            </div>
            <div class="col-md-6 col-sm-6">
              <ul class="item-list">
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Cheese <small class="pull-right">� 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox"  class="icheck">
                    NO Pickles <small class="pull-right">� 8,30</small></label>
                </li>
                <li>
                  <label>
                    <input type="checkbox" class="icheck">
                    NO Fresh Lettuce <small class="pull-right">� 8,30</small></label>
                </li>
              </ul>
            </div>
          </div>
          <p>
            <button type="button" class="btn btn-primary">Continue</button>
          </p>
          <div class="cleafix"></div>
		  </form>
        </div>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="devlivery_message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  	<p>
	  	The minimum order amount for this restaurant is $<span id="min_charge">15</span>*</p>
<p>
The total of your selected items is $<span id="subtotal_min_charge">5.25</span> 
		</p>
<p>
<a href="#" data-dismiss="modal"><strong>Choose more items </strong></a> OR
<a href="#" id="submit_with_min_charge"><strong>Just charge me an extra <span id="reman_min_charge">9.75</span></strong></a>
</p>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  		<p id="alert_model_msg"></p>	
      </div>
    </div>
  </div>
</div>



<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}


#add_sucess_msg {
    position: fixed;
    width: auto;
    height: auto;
    background: rgba(210, 71, 30, 0.89);
    z-index: 99999;
    color: #f5f0e3 !important;
    margin: 0 auto;
    padding: 21px;
    font-size: 20px;
    top: 50%;
    margin: 0 auto;
    text-align: center;
	left: 50%;
    transform: translate(-50%, 0);
}

</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>


<div id="add_sucess_msg" style="display:none;">
		Added
</div>


<div class="modal fade" id="confirm_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  		<p>
				Are you sure you want to remove this date Details?
				
			</p>	
			<p>
				<button type="button" class="get_conf_val btn-default" data-val="1">OK</button>
				<button type="button" class="get_conf_val btn-primary" data-val="0">Cancel</button>
			</p>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="refresh" value="no">
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>



<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> 
<!-- SPECIFIC SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/cat_nav_mobile.js"></script>
<script>$('#cat_nav').mobileMenu();</script>
<script src="{{ url('/') }}/design/front/js/custom.js"></script> 

<script src="{{ url('/') }}/design/front/js/jquery.prettyPhoto.js"></script> 
<script src="{{ url('/') }}/design/front/js/owl.carousel.min.js"></script> 


<script>
	 $(function() {
		//$("#tooltip-1").tooltip();
		$("[id^='tooltip']").tooltip();
	 });
</script>
	  
<script>


 $(document).ready(function () {
     $("a[rel^='prettyPhoto']").prettyPhoto();

     $(".owl-slider").owlCarousel({

         autoPlay: 3000, //Set AutoPlay to 3 seconds

         items: 4,
         itemsDesktop: [1199, 3],
         itemsDesktopSmall: [979, 3],
		 itemsTablet : [768, 2], 
		 itemsMobile : [479, 1],

     });
 });
</script>

<script>
    jQuery('#sidebar, #sidebar1').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script> 
<script>
$(document).on('click', '#show_modal', function(){ 

								 
								 
								 

$("#ajax_favorite_loddder").show();

var trans_id = $(this).attr('data-trans_id');
var vehicle_id = $(this).attr('data-vehicle_id');
var rate_id = $(this).attr('data-rate_id');

var menu_option = '';

if($(this).attr('data-options')){

	var menu_option = $(this).attr('data-options');
}

	
frm_val = 'trans_id='+trans_id+'&vehicle_id='+vehicle_id+'&rate_id='+rate_id+'&menu_option='+menu_option;
		
		
 
		$.ajax({
				type: "POST",
				url: "{{url('/show_rate_addons')}}",
				data: frm_val,
			   dataType: 'json',
					success: function(msg) {
						//alert(msg.addon_view)						
						   
						$("#ajax_favorite_loddder").hide();						 
						$('#show_mod_addon').html(msg.addon_view); 
						$('#other-items').modal('show'); 
					}
				});
				
				
				
});
 $(function() {
	 'use strict';
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 70
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	

	
$(document).on('click', '[id^="add_to_card-"]', function(){ 
  
		var restitem = $(this).attr('data-restitem');
		
		/**/
		
		
	 var driver_allownce = 0;	
	  
	 var dal_charg = 0;
	 var del_min_charg=0;
	 var remaing_min_del_charg = 0;
	 
	/* if($('.rest_option').is(':checked'))
	 {
		if($('.rest_option:checked').val()=='Delivery')
		{
			dal_charg = $("#delivery_fee").val();
			del_min_charg = $("#delivery_fee_min").val();
		}
	 }
	 */
		/**/
		
		var form = $('#crd_frm-'+restitem);
	form.validate();
	var valid =	form.valid();
	if(valid)
	{		
		
		
		$('#addon-items-'+restitem).modal('hide');
		
		$("#ajax_favorite_loddder").show();	
		
		
		if($(this).attr('data-options'))
		{
			var option = $(this).attr('data-options');
			var frm_val ='menu_option='+option+'&'+$('#crd_frm-'+restitem).serialize();		
			
		}
		else{
			var frm_val = $('#crd_frm-'+restitem).serialize();				
		}
		$.ajax({
		type: "POST",
		url: "{{url('/transport_cart')}}",
		data: frm_val,
		dataType: 'json',
			success: function(msg) {			
				
			 $("#ajax_favorite_loddder").hide();	
			
			
		//	alert( $("#promo_mode").val());
			
				
				dal_charg = $("#delivery_fee").val();
				var subtotal = msg.total_amt;
				var discount_val = 0;
				var driver_allownce = 0;	
				
				
				if(($("#driver_allownce")) && ($("#driver_allownce").val()>0))
				{
					driver_allownce = $("#driver_allownce").val();
				}
				
				//alert(driver_allownce);
				
				if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
				{
					if($("#promo_mode").val()=='$')
					{
						discount_val = $("#promo_value").val();
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
					if($("#promo_mode").val()=='%')
					{
						var promo_val = $("#promo_value").val();
						//alert(promo_val);
						//alert((parseFloat(subtotal)*parseFloat(promo_val))/100);
						
						discount_val =((parseFloat(subtotal)*parseFloat(promo_val))/100).toFixed(2);
						
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
				}
				
				
				var Total = (parseFloat(subtotal)+parseFloat(driver_allownce)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
				
			
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				/*if((subtotal-discount_val)<del_min_charg)
				{
					$('#del_fee_min').show();
					
					var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
					remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
					
					if(remaing_min_del_charg<=0)
					{
						$('#del_fee_min').hide();
					}
				  
				}
				else if((subtotal-discount_val)>del_min_charg)
				{
						$('#del_fee_min').hide();
				}
			 	
				$('#delivery_fee_min_span').html(remaing_min_del_charg);
				$('#delivery_fee_min_remaing').val(remaing_min_del_charg);*/
				
				/*  MANAGE MINIMUM DELIVERY CHARGE END */
				
				
				$('#order_list').html(msg.cart_view);
				$('#Subtotal_amt').html('$'+msg.total_amt);
				
				
				$('#total_amt').html('$'+Total);
				$('#total_charge').val(Total);
				$('#sub_total').val((msg.total_amt).toFixed(2));
				
				
				$("#add_sucess_msg").show();	
				setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);
				
			}
		});
		
		
	}//valid if end
		
		
});	


$(document).on('click', '#plase_order', function(){
//	alert('test');
	//alert($('#total_charge').val());
	if(($('#sub_total').val()==0))
	{
		$('#alert_model_msg').html('Please Select Bootking Rates for Order');			
		$('#message_model').modal('show');
		return false;
	}
	else{
	

	var form = $("#order_frm");
	form.validate();
	var valid =	form.valid();
	
	
	
		if(valid)
		{
		
					
				
					if(($('#pickup_area').val()>0) && ($('#to_loc_id').val()!='')){
						  $('#order_frm').submit();
					}
					else if($('#to_loc_id').val()==''){
						$('#alert_model_msg').html('Please Select To Location');			
						$('#message_model').modal('show');
						return false;
					}
					else
					{
						$('#alert_model_msg').html('Please Select Pickup Location');			
						$('#message_model').modal('show');
						return false;
					}
				
		}
		else
		{
			return false;
		}
			
	}
});
	


$(document).on('change', '#pickup_area', function(){

 var value = '';
   value = $(this).find('option:selected').attr("langid");
    //alert(value);
	if($.isNumeric(value))
	{
		var newValue = $("#Subtotal_amt").text().replace('$','');
		
		
		 
		 var discount_val = 0;
		 var driver_allownce = 0;	
		 
		 
		 if(($("#driver_allownce")) && ($("#driver_allownce").val()>0))
		{
			driver_allownce = $("#driver_allownce").val();
		}
		 
		if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
		{
			if($("#promo_mode").val()=='$')
			{
				discount_val = $("#promo_value").val();
				$('#promo_amt').html('-$'+discount_val);
				$('#promo_amt_cal').val(discount_val);
			}
			if($("#promo_mode").val()=='%')
			{
				var promo_val = $("#promo_value").val();
				discount_val =((parseFloat(newValue)*parseFloat(promo_val))/100).toFixed(2);
				
				$('#promo_amt').html('-$'+discount_val);
				$('#promo_amt_cal').val(discount_val);
			}
		}
				
		var Total = (parseFloat(newValue)+parseFloat(driver_allownce)+parseFloat(value)-parseFloat(discount_val)).toFixed(2);
		
		
		/*  MANAGE MINIMUM DELIVERY CHARGE END */
		
		/*var del_min_charg = $("#delivery_fee_min").val();
		if(((parseFloat(newValue))-(parseFloat(discount_val)) ) < parseFloat(del_min_charg))
		{
			 Total = (parseFloat(del_min_charg)+parseFloat(value)).toFixed(2);			
		}*/	
		
		/*  MANAGE MINIMUM DELIVERY CHARGE END */



		
		

		 $("#delivery_fee").val(value);	
		 $(".delivery_fee").html('$'+value);				
		 $('#total_amt').html('$'+Total);
		 $('#total_charge').val(Total);
	}
	else
	{
		if($("#delivery_fee").val()>0)
		{
		
		
				
				var discount_val = 0;
				
				if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
				{
					if($("#promo_mode").val()=='$')
					{
						discount_val = $("#promo_value").val();
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
					if($("#promo_mode").val()=='%')
					{
						var promo_val = $("#promo_value").val();
						//alert(promo_val);
						//alert((parseFloat(subtotal)*parseFloat(promo_val))/100);
						
						discount_val =((parseFloat(subtotal)*parseFloat(promo_val))/100).toFixed(2);
						
						$('#promo_amt').html('-$'+discount_val);
						$('#promo_amt_cal').val(discount_val);
					}
				}
				
				
				
			var del_new_val = $("#delivery_fee").val();	
			var total_new_val = $("#total_charge").val();
			
			
			
			var cal_total_val =  (parseFloat(total_new_val)-parseFloat(del_new_val)).toFixed(2);
			
			/*  MANAGE MINIMUM DELIVERY CHARGE END */
		
	/*		del_min_charg = $("#delivery_fee_min").val();
			if(newValue<del_min_charg)
			{
				$('#del_fee_min').show();
				
				var Total = (parseFloat(del_min_charg)+parseFloat(value)).toFixed(2);
			//	remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
				
				if(remaing_min_del_charg<=0)
				{
					$('#del_fee_min').hide();
				}
			  
			}
			else if(subtotal>del_min_charg)
			{
					$('#del_fee_min').hide();
			}*/
			
		//	$('#delivery_fee_min_span').html(remaing_min_del_charg);
			//$('#delivery_fee_min_remaing').val(remaing_min_del_charg);
			
			/*  MANAGE MINIMUM DELIVERY CHARGE END */

			
			
			
			
			
			
			
			$("#delivery_fee").val('0');	
		 	$(".delivery_fee").html('$0');
							
			$('#total_amt').html('$'+cal_total_val);
			$('#total_charge').val(cal_total_val);
				
		}	
	}
		
});

 	
$(document).on('click', '#remove_ondate', function(){ 
  
  
  	$('#confirm_model').modal('show');
	$(document).on('click', '.get_conf_val', function(){
			 confrim = $(this).attr('data-val');
	
			 	if(confrim==1)
				{
					 $('#confirm_model').modal('hide');
					 
					 $("#ajax_favorite_loddder").show();	
					$.ajax({
				/*	//type: "post",
					//dataType: "json",
					//url:  '?page=' + page+'&'+'cartType='+type_service+'&'+cusin_val,*/
					
					//dataType: "json",
					url:  '<?php echo url('/reset_transportinfo') ?>?remove=removefromdata',	
					success: function(msg) {		
					$("#ajax_favorite_loddder").hide();	
					window.location.href = '<?php echo url('/transport_listing') ?>';
						
						}
					});
					
				}	
				else if(confrim==0)
				{
					 $('#confirm_model').modal('hide');
				}
				
			 	
			// alert($(this).attr('data-val'));
			 });
		
});
 
 
var dateToday = new Date();
   

  


/**  DINEIN CALENDER INFO END  **/	


/* COLLECT DINEIN INFROMATION  */
$(document).on('click', '#Add_Dine_Info', function(){

//$('#del_error_msg').hide();
var closed =$('#todayclosed').val();
var holiday =$('#holiday').val();

//alert(closed);
//alert(holiday);


  if((closed==1)&&(holiday==0))
  {
  	$('#del_error_msg').html("Restaurant is closed on "+$('#datepicker').val());			
	$('#del_error_msg').show();
  }
  else if((closed==0)&&(holiday==1))
  {
  	$('#del_error_msg').html("Restaurant is closed until ");			
	$('#del_error_msg').show();
  }
  else if((closed==1)&&(holiday==1))
  {
  	$('#del_error_msg').html("Restaurant is closed until ");			
	$('#del_error_msg').show();
  }
  else if((closed==0)&&(holiday==0))
  {
  	var form = $("#dinein_frm");
	form.validate();
	var valid =	form.valid();
	
		if(valid)
		{			
			 $('#dinein_model').modal('hide');		
			 $("#ajax_favorite_loddder").show();	
		
			var frm_val = $('#dinein_frm').serialize();	
			$.ajax({
			type: "POST",
			url: "{{url('/restaurant_detail/dinein')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) {			
				//	alert(msg);
				 $("#ajax_favorite_loddder").hide();	
					
					$('#dinein_info_div').html(msg.dinein_view);
					$('#dinein_info_div').show();
					$('#table_pic').val(msg.dinein_pic);
					$('#table_date').val(msg.dinein_date);
					$('#table_time').val(msg.dinein_time);
				}
				  
			});
			
		}
  }

});

/* END */

		  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

/*RESET DINEIN MODEL VALUE */
$('#dinein_model').on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select,hidden")
       .val('')
       .end()
});
$(document).on('click', '#submit_with_min_charge', function(){


 $('#devlivery_message_model').modal('hide');


$('#order_frm').submit();
});




	
$(document).on('click', '#addon_continue', function(){ 	

var form = $('#addon_frm');
	form.validate();
	var valid =	form.valid();
	//alert(valid);
	
	if(valid)
	{
			 
	  
			var restitem = $(this).attr('data-restitem');
			
			/**/
			
			
			 
		 var dal_charg = 0;
		 var driver_allownce=0;
		 
	
				dal_charg = $("#delivery_fee").val();
				
		 
				if(($("#driver_allownce")) && ($("#driver_allownce").val()>0))
				{
					driver_allownce = $("#driver_allownce").val();
				}			
			
			$('#other-items').modal('hide');
			
			$("#ajax_favorite_loddder").show();	
			
			
			var frm_val = $('#addon_frm').serialize();
			$.ajax({
			type: "POST",
			url: "{{url('/transport_cart')}}",
			data: frm_val,
			dataType: 'json',
				success: function(msg) {			
					
				 $("#ajax_favorite_loddder").hide();	
				
				
			//	alert( $("#promo_mode").val());
				
				
					var subtotal = msg.total_amt;
					var discount_val = 0;
					
					if(($("#promo_mode").val()=='%') || ($("#promo_mode").val()=='$'))
					{
						if($("#promo_mode").val()=='$')
						{
							discount_val = $("#promo_value").val();
							$('#promo_amt').html('-$'+discount_val);
							$('#promo_amt_cal').val(discount_val);
						}
						if($("#promo_mode").val()=='%')
						{
							var promo_val = $("#promo_value").val();
							//alert(promo_val);
							//alert((parseFloat(subtotal)*parseFloat(promo_val))/100);
							
							discount_val =((parseFloat(subtotal)*parseFloat(promo_val))/100).toFixed(2);
							
							$('#promo_amt').html('-$'+discount_val);
							$('#promo_amt_cal').val(discount_val);
						}
					}
					
					
					var Total = (parseFloat(subtotal)+parseFloat(driver_allownce)+parseFloat(dal_charg)-parseFloat(discount_val)).toFixed(2);
					
				
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					/*if((subtotal-discount_val)<del_min_charg)
					{
						$('#del_fee_min').show();
						
						var Total = (parseFloat(del_min_charg)+parseFloat(dal_charg)).toFixed(2);
						remaing_min_del_charg = (parseFloat(del_min_charg)-parseFloat(subtotal)).toFixed(2);
						
						if(remaing_min_del_charg<=0)
						{
							$('#del_fee_min').hide();
						}
					  
					}
					else if((subtotal-discount_val)>del_min_charg)
					{
							$('#del_fee_min').hide();
					}
					
					$('#delivery_fee_min_span').html(remaing_min_del_charg);
					$('#delivery_fee_min_remaing').val(remaing_min_del_charg);*/
					
					/*  MANAGE MINIMUM DELIVERY CHARGE END */
					
					
					$('#order_list').html(msg.cart_view);
					$('#Subtotal_amt').html('$'+msg.total_amt);
					
					
					$('#total_amt').html('$'+Total);
					$('#total_charge').val(Total);
					$('#sub_total').val((msg.total_amt).toFixed(2));
					
					
					$("#add_sucess_msg").show();	
				setTimeout(function() {
					$("#add_sucess_msg").hide('blind', {}, 500)
				}, 1000);
					
				}
			});
			
			
	}
	
});


$(document).ready(function(e) {
    var $input = $('#refresh');

    $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
});

/* BAKC TO TOP AND CART */
$(document).ready(function () {
	if ($('#back-to-top').length) {
		var scrollTrigger = 100, // px
		backToTop = function () {
			var scrollTop = $(window).scrollTop();
			if (scrollTop > scrollTrigger) {
				$('#back-to-top').addClass('show');
			} else {
				$('#back-to-top').removeClass('show');
			}
		};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		$('#back-to-top').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
	}

	$("#close_in").click(function () {
		if ($(".cmn-toggle-switch__htx").hasClass("active")) {
			$(".cmn-toggle-switch__htx").removeClass("active")
		}
	});

});	
$(document).ready(function () {
	if ($('#back-to-cart').length) {
		var scrollTrigger = 100, // px
		backToTop = function () {
			var scrollTop = $(window).scrollTop();
			if (scrollTop > scrollTrigger) {
				$('#back-to-cart').addClass('show');
			} else {
				$('#back-to-cart').removeClass('show');
			}
		};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		<?php /*?>$('#back-to-cart').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});<?php */?>
	}

	$("#close_in").click(function () {
		if ($(".cmn-toggle-switch__htx").hasClass("active")) {
			$(".cmn-toggle-switch__htx").removeClass("active")
		}
	});

});	


$(document).on('keydown', '.search_to', function(){

$('#to_lable').val('');
$(".to_location_id").val(''); 
$(".to_suburb").val(''); 
$(".to_pincode").val(''); 
$("#to_search_type").val(''); 


var state_val =  $("#location_states option:selected").val();

 var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
	//	autoFocus: true,	  
	
	var NoResultsLabel = "No Results";
	    	
    $( ".search_to" ).autocomplete({
      source: "{{url('/get_location') }}",
		minLength: 0,
	  select: function(event, ui) {
	  
	  		 
	  		  if (ui.item.label === NoResultsLabel) {			  
				$("#search_to").val(''); 
                event.preventDefault();
            }
			else{
	  		 
			 event.preventDefault();
             $('.search_to').val(ui.item.label);
			
				 $('#to_lable').val(ui.item.label);
				 $(".to_location_id").val(ui.item.id); 
				 $(".to_suburb").val(ui.item.suburb); 
				 $(".to_pincode").val(ui.item.pincode);
			
			// $("#location_id").val(ui.item.id); 
			 $("#to_search_type").val(ui.item.search_type); 
			} 
		 },
	 focus: function (event, ui) {
	 
	  		 
	  		  if (ui.item.label === NoResultsLabel) {
				$("#search_to").val(''); 
                event.preventDefault();
            }
			else{
			   if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
				 $('.search_to').val(ui.item.label);
			
				 $('#to_lable').val(ui.item.label);
				 $(".to_location_id").val(ui.item.id); 
				 $(".to_suburb").val(ui.item.suburb); 
				 $(".to_pincode").val(ui.item.pincode);
			 	 $("#to_search_type").val(ui.item.search_type); 
				 
				// $("#location_id").val(ui.item.id); 
				// $("#search_type").val(ui.item.search_type); 
			   } else{
				 $('.search_to').val(ui.item.label);
			
				 $(".to_location_id").val(ui.item.id); 
				 $(".to_suburb").val(ui.item.suburb); 
				 $(".to_pincode").val(ui.item.pincode);
				 $("#to_search_type").val(ui.item.search_type); 
				 $('#to_lable').val(ui.item.label);
			   }
			}			
		}
    });


});
</script>



 
<script type="text/livescript" src="{{ url('/') }}/design/js/ion-imageSlider/ion.imageSlider.min.js" > </script>
<script type="text/livescript" src="{{ url('/') }}/design/js/ion-imageSlider/ion.imageSlider.js" > </script>
  


<script>
$("#mySlider").ionImageSlider({
    zoomText: "Zoom",                  // text near zoom icon; set "", if don't need
    startFrom: 0,                           // # of start picture
    slideShow: true,                        // enable slide show
    slideShowDelay: 7                       // pause between picture change (if slide show is on)
});
</script>
@stop