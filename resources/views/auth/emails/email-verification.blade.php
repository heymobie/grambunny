<!DOCTYPE html>
<html>
<head>
    <title>Email Verification</title>
</head>
<!-- <body>
    Hello {{ $name }},
    Welcome to our Website
    Please <a href="{{ route("EmailVerificationLink",["token" => encrypt($token)]) }}">Click here</a> to verify your email.  
</body> -->
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#ed1c24" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ed1c24" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                            <h1 style="font-size: 38px; font-weight: 400; margin: 2;">Welcome To!</h1> <img src=" https://www.626eats.com/public/assets/images/footlogo.jpg" width="200"  style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                             <p style="text-align: center; float: left;width: 100%; font-size: 15px; text-transform: capitalize;">Hello {{ $name }} {{$lname}}</p>
                            <p style="text-align: center; float: left;width: 100%; font-size: 15px; ">We're excited to have you get started. First, you need to confirm your account. Just click the link below.</p>
                            <p style="width: 100%; float: left; text-align: center;"> <a style="color: #ed1c24" href="{{ route("EmailVerificationLink",["token" => encrypt($token)]) }}">Click here</a></p>
                        </td>
                    </tr>
                    
                   
                </table>
            </td>
        </tr>
        

    </table>
</body>

</html>