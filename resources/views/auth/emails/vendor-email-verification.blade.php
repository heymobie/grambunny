<!DOCTYPE html>
<html>
<head>
	<title>Email Verification</title>
</head>
<body>
	Grambunny,
	<p>Thank you for signing up! Please <a href="{{ route("verifyVendorEmail",["data" => $data]) }}">click here</a> to verify your email address to continue. </p>
	<p>Thank you,</p><p>Grambunny Onboard Team </p>
</body>
</html>


