<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<!-- <body>
    <p>Hey,</p>
    <p>Did you forget your password? Create a new one by clicking the button below.</p>
    <a href="{{ route("ResetPasswordPage",encrypt($token)) }}">Reset Password</a>
    Thanks!
</body> -->
<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#ed1c24" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 0px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#ed1c24" align="center" style="padding: 0px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px; padding-top: 75px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px; padding: 40px 20px 20px 20px;">
                             <img src="https://www.grambunny.com/public/assets/images/footlogo.jpg" width="200"  style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                        	
                            <p style="text-align: center; float: left;width: 100%; font-size: 15px; ">Did you forget your password? Create a new one by clicking the button below.</p>
                            <p style="width: 100%; float: left; text-align: center;"> <a style="color: #ed1c24;" href="{{ route("ResetPasswordPage",encrypt($token)) }}">Reset Password</a></p>
                            <p style="width: 100%; float: left;text-align: center; font-weight: bold;">Thanks!</p>
                        </td>
                    </tr>
                    
                   
                </table>
            </td>
        </tr>
        

    </table>
</body>

</html>