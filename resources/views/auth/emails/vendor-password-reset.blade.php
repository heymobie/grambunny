<!DOCTYPE html>
<html>
<head>
	<title>Forgot Password</title>
</head>
<!-- <body>
	Hello {{ $firstname }}
	<p>Did you forget your password? Create a new one by clicking the button below.</p>
	<a href="{{ route("vendor.resetPassword",["token" => $token]) }}">Reset Password</a>
</body> -->
<body style=" margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->

    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top: 6px solid #EC7324;padding: 40px;border-bottom: 6px solid #EC7324;border-left: 6px solid #ec3e6c ;border-right: 6px solid #ec3e6c; ">
        <!-- LOGO -->
       
        <tr>
            <td bgcolor="" align="center" style="padding: 0px">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                             <img src=" https://www.grambunny.com/public/assets/images/footlogo.jpg" width="200"  style="display: block; border: 0px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="" align="center" style="padding: 0px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 0px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                        	<p style="text-align: center; float: left;width: 100%; font-size: 15px; ">Hello {{ $firstname }}</p>
                            <p style="text-align: center; float: left;width: 100%; font-size: 15px; margin-top: 0px; margin-bottom: 0px;">Did you forget your password? Create a new one by clicking the button below.</p>
                            <p style="width: 100%; float: left; text-align: center; color: #EC7324; font-size: 13px; "> 
                            	<a style="color: #EC7324; font-size: 13px;" href="{{ route("vendor.resetPassword",["token" => $token]) }}">Reset Password</a></p>
                            <p style="width: 100%; float: left;text-align: center; font-weight: bold; color: #EC7324; font-size: 15px; margin:0px !important; ">Thanks!</p>
                        </td>
                    </tr>
                    
                   
                </table>
            </td>
        </tr>
        

    </table>
</body>
</html>