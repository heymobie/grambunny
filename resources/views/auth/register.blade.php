@extends("layouts.grambunny")

@section("styles")

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}

.error{

    border-color: #f23a2e !important;

    color: #f23a2e ;

}

</style>

@endsection

@section("content")

<div class="site-section bg-light login_sec xcv">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-6 mb-5" data-aos="fade">

                <h2 class="mb-5 text-black">CUSTOMER REGISTRATION</h2>

                <form action="{{ route("webRegister") }}" method="POST" class="p-5 bg-white user-register" autocomplete="off" enctype="multipart/form-data" id="filter-form">

                    @if(session()->has("success"))

                    <div class="alert alert-success" role="alert">

                        <strong>{{ session()->get("success") }} </strong>

                    </div>

                    @endif

                    <input type="hidden" value="{{ csrf_token() }}" name="_token">

                    <div class="row form-group">

                        <div class="col-md-6">

                            <label class="text-black" for="email">First name</label>

                            <input required value="{{ old("firstname") }}" autocomplete="off" type="text"  name="firstname" class="form-control {{ $errors->has("firstname") ? "is-invalid" : "" }} ">

                            @if($errors->has("firstname"))

                            <span class="text-danger">{{ $errors->first("firstname") }}</span>

                            @endif

                        </div>

                        <div class="col-md-6">

                            <label class="text-black" for="lastname">Last name</label>

                            <input required value="{{ old("lastname") }}" autocomplete="off" type="text" id="lastname" name="lastname" class="form-control {{ $errors->has("lastname") ? "is-invalid" : "" }}">

                            @if($errors->has("lastname"))

                            <span class="text-danger">{{ $errors->first("lastname") }}</span>

                            @endif

                        </div>

                    </div>

                    <div class="row form-group">

                        <div class="col-md-6">

                            <label class="text-black" for="email">Email</label>

                            <input required value="{{ old("email") }}" type="email" id="email" name="email" class="form-control {{ $errors->has("email") ? "is-invalid" : "" }}">

                            @if($errors->has("email"))

                            <span class="text-danger">{{ $errors->first("email") }}</span>

                            @endif

                        </div>
                          <div class="col-md-6">

                            <label class="text-black" for="password">Password</label>

                            <input required type="password" name="password" id="password" class="form-control"  >

                            @if($errors->has("password"))

                            <span class="text-danger">{{ $errors->first("password") }}</span>

                            @endif

                        </div>
                    </div>

                    <div class="row form-group">

                           <div class="col-md-6">

                            <label class="text-black" for="phone">Phone</label>

                            <input required   value="{{ old("phone") }}" type="text" name="phone" id="phone" class="form-control {{ $errors->has("phone") ? "is-invalid" : "" }}">

                            @if($errors->has("phone"))

                            <span class="text-danger">{{ $errors->first("phone") }}</span>

                            @endif

                        </div>

                        <div class="col-md-6">

                            <!-- @php

                            $date=\Carbon\Carbon::now();

                            $minDate= $date->subYear(21)->format("Y-m-d");

                            @endphp -->

                            <label class="text-black" for="dob">Date of Birth (MM/DD/YYYY)</label>

                            <input placeholder="MM/DD/YYYY" name="dob" id="dob" onkeyup="date_reformat_mm(this);" onkeypress="date_reformat_mm(this);" onpaste="date_reformat_mm(this);" autocomplete="off" type="text" class="form-control {{ $errors->has("dob") ? "is-invalid" : "" }}" required>

                             @if($errors->has("dob"))

                            <span class="text-danger">{{ $errors->first("dob") }}</span>

                            @endif

                            <!-- <input type="text" id="date_s" name="dob" placeholder="mm/dd/yyyy" class="form-control"> -->

                            <!-- <input required required max="{{ $minDate }}" value="{{ old("dob") }}" type="text" id="dob" name="dob" class="form-control {{ $errors->has("dob") ? "is-invalid" : "" }}"> -->

                           <!--  @if($errors->has("dob"))

                            <span class="text-danger">{{ $errors->first("dob") }}</span>

                            @endif -->

                        </div>

                    </div>

                    <div class="row form-group">



                    </div>


                       <div class="row form-group">

                       <div class="col-md-6">

                            <label class="text-black" for="address">Address</label>

                            <input required value="{{ old("address") }}" type="text" id="address" name="address" class="form-control {{ $errors->has("address") ? "is-invalid" : "" }}">

                            @if($errors->has("address"))

                            <span class="text-danger">{{ $errors->first("address") }}</span>

                            @endif

                        </div>

                       <div class="col-md-6">

                            <label class="text-black" for="city">City</label>

                            <input required value="{{ old("city") }}" type="text" id="city" name="city" class="form-control {{ $errors->has("city") ? "is-invalid" : "" }}">

                            @if($errors->has("city"))

                            <span class="text-danger">{{ $errors->first("city") }}</span>

                            @endif

                        </div>

                    </div>

                    <div class="row form-group">

                       <div class="col-md-6">

                       <label class="text-black" for="state">State</label>

		                <select class="form-control" name="state" id="state" required="" style="border-radius: 0px;">

		                <option value="">Choose...</option>

		                  @if(!$state_list->isEmpty())

		                    @foreach($state_list as $arr)

		                      <option value="{{$arr->name}}" <?php if(!empty($user_detail[0]->user_states)){ if($user_detail[0]->user_states == $arr->name){ ?> selected="selected" <?php } }?>>{{$arr->name}}</option>

		                    @endforeach

		                  @endif

		                </select>


                        </div>

                       <div class="col-md-6">

                            <label class="text-black" for="zipcode">Zip Code</label>

                            <input value="{{ old("zipcode") }}" type="text" id="zipcode" name="zipcode" class="form-control {{ $errors->has("zipcode") ? "is-invalid" : "" }}" number="number" maxlength="6" required="required">

                            @if($errors->has("zipcode"))

                            <span class="text-danger">{{ $errors->first("zipcode") }}</span>

                            @endif

                        </div>

                    </div>


                    <div class="row form-group">

                       <div class="col-md-6">

                       <label class="text-black" for="state">License Front</label>

                      <input type="file" id="license_front" name="license_front" class="form-control" value="" required="required">


                        </div>

                       <div class="col-md-6">

                            <label class="text-black" for="zipcode">License Back</label>

                            <input type="file" id="license_back" name="license_back" class="form-control" value="" required="required">

                        </div>

                    </div> 


                    <div class="row form-group">

                       <div class="col-md-6">

                       <label class="text-black" for="state">Type Of Customer</label>

                      <select name="customer_type" id="customer_type" class="form-control" style="border-radius: 0px;">

                            <option value="Recreational Customer">Recreational Customer</option>

                            <option value="Medical Customer">Medical Customer</option>

                            </select>


                        </div>

                       <div class="col-md-6" style="display: block;" id="mmid">

                        <label class="text-black" for="zipcode">Upload Copy of Medical Marijuana ID Front Side  IF MEDICAL Customer</label>

                        <input type="file" id="marijuana_card" name="marijuana_card" class="form-control" value="">

                        </div>

                    </div>    
   

                    <div class="row form-group">

                        <div class="col-md-12">

                            <input type="submit" value="Create Account" class="btn btn-primary py-2 px-4 text-white">

                        </div>

                    </div>

                    <div class="row form-group loginfoot">

                        <div class="col-12">

                            <p>Alrady have an account? <a href="{{route("signInPage")}}">Login </a></p>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection

@section("scripts")

    <script src="{{ asset("public/assets/js/validate.js") }}"></script>

    <script src="{{ asset("public/assets/js/application.js") }}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

     <script>
       $(function() {
  $("#date_s").dates();
});

$("input[name='dob']:first").keyup(function(e) {
  var key = String.fromCharCode(e.keyCode);
  if (!(key >= 0 && key <= 9)) $(this).val($(this).val().substr(0, $(this).val().length - 1));
  var value = $(this).val();
  if (value.length == 2 || value.length == 5) $(this).val($(this).val() + '/');
});
    </script> -->

    <script>

    document.getElementById('phone').addEventListener('input', function (e) {

        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);

    e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');

    });



// $(function() {

//     $('input[name="dob"]').daterangepicker({

//         singleDatePicker: true,

//         showDropdowns: true,

//         minYear: 1920,

//         maxYear: parseInt(moment().subtract(0,'y').format('YYYY')),

//         maxDate: "+12m"

//     });

// });


$('#customer_type').change(function(){ 

    var value = $(this).val();

    if(value=='Medical Customer'){ 

    //$('#mmid').show(); 

    $("#marijuana_card").prop('required',true); 

    }else{

    $("#marijuana_card").prop('required',false); 
   // $('#mmid').hide();

    }

});

    </script>

      <script>
        
            function checkValue(str, max) {
                if (str.charAt(0) !== '0' || str == '00') {
                    var num = parseInt(str);
                    if (isNaN(num) || num <= 0 || num > max) num = 1;
                    str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
                };
                return str;
            };

  // reformat by date
       function date_reformat_mm(date) {
            date.addEventListener('input', function(e) {
                this.type = 'text';
                var input = this.value;
                if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                var values = input.split('/').map(function(v) {
                    return v.replace(/\D/g, '')
                });
                if (values[0]) values[0] = checkValue(values[0], 12);
                if (values[1]) values[1] = checkValue(values[1], 31);
                var output = values.map(function(v, i) {
                    return v.length == 2 && i < 2 ? v + '/' : v;
                });
                this.value = output.join('').substr(0, 14);
            });


        }
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
$(document).ready(function() {
    $("#filter-form").validate({
    
rules: {
         
email: "required",

// password: {
//    required: true,

//    minlength:6,

//    pass:true,
//  },

//  phone: {

//   required: true,

//   minlength:10,

//   maxlength:10

// },
        },
messages: {
email: "Please your Email",

password: {
required:'Please enter password.',
minlength:'Password must be at least 6 characters.',
pass:"at least one number, one lowercase and one uppercase letter.",

        },
phone: {
 required:'Please enter mobile number.',

 minlength:'Please enter a 10 digit mobile number.',

 maxlength:'Please enter a 10 digit mobile number.'

 },

        }
    })

$('#filter-form').change(function() {

 var form = $(this);

 var actionUrl = form.attr('action');

 var dateFormatRegex = /^(0[1-9]|1[0-2])\/(0[1-9]|[12][0-9]|3[01])\/\d{4}$/;

var dobString = $("#dob").val();

var dtDOB = new Date(dobString);

var dtCurrent = new Date();

var age = dtCurrent.getFullYear() - dtDOB.getFullYear();

if (age < 21 || (age === 21 && dtCurrent.getMonth() < dtDOB.getMonth()) || (age === 21 && dtCurrent.getMonth() === dtDOB.getMonth() && dtCurrent.getDate() < dtDOB.getDate())) {
    alert("Date must be greater then 21 Year.");
}

if (dobString.trim() === "") {
    //alert('null');
} else if (!dobString.match(dateFormatRegex)) {
    alert('Please enter a valid date in MM/DD/YYYY format.');
} else {
    //alert('123');
}

$.ajax({
          
url: "{{url('/custom-validation')}}",
type: "POST",
data: form.serialize(),
success: function( data ) {

if(data.emailcount>0 && data.emailcount!=2){

   alert("The email has already been taken");
            
        }
          }
      });  
      
    });
});


</script>

@endsection