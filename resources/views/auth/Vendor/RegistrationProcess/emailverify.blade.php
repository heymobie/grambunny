@extends("layouts.grambunny")

@section("styles")

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}

</style>

@endsection

@section("content")

<div class="site-section bg-light login_sec">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-7"  data-aos="fade" >

                <h2 class="mb-5 text-black">MERCHANT REGISTRATION</h2>

                <!-- partial:index.partial.html -->

                <!-- multistep form -->

                <form id="msform" >

                    <!-- progressbar -->

                    <ul id="progressbar">

                        <li class="active text-dark">Account Setup</li>

                        <li class="active text-dark">Personal Profile</li>

                        <li class="active text-dark">Other Details</li>

                    </ul>

                    <!-- fieldsets -->

                    

                    <fieldset>

                      <h2 class="fs-title">REGISTER TO BECOME A GRAMBUNNY </h2>      

                        <div class="alert alert-success" role="alert">

                            <h4 class="alert-heading">Well done!</h4>

                            <p>A verification link has been sent to your email account. Please verify your email to continue.</p>

                        </div> 

                    </fieldset>

                    

                </form>

                <!-- partial -->

            </div>

            

        </div>

    </div>

</div>

@endsection

@section("scripts")

@endsection