@extends("layouts.grambunny")

@section("styles")

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}

</style>

@endsection

@section("content")

<div class="site-section bg-light login_sec">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-7"  data-aos="fade" >

                <h2 class="mb-5 text-black">MERCHANT SIGN UP</h2>

                <!-- partial:index.partial.html -->

                <!-- multistep form -->

                <form id="msform" >

                    <!-- progressbar -->

                    <ul id="progressbar">

                        <li class="active text-dark">Account Setup</li>

                        <li class="active text-dark">Personal Profile</li>

                        <li class="active text-dark">Other Details</li>

                    </ul>

                    <!-- fieldsets -->

                    

                    <fieldset>

                        <div class="alert alert-success" role="alert">

                            <h4 class="alert-heading mb-3">Thank you for registering to become a Merchant with Gram Bunny.</h4>

                            <p class="mb-0">We will review your application and have one of our Merchant Assistants reach out to you to get your business started on Gram Bunny.</p>

                            <hr>

                            <p>You will recived a email when your account is approved by our team.</p>

                            <p>Thank You!</p>

                        </div>

                    </fieldset>

                    

                </form>

                <!-- partial -->

            </div>

            

        </div>

    </div>

</div>

@endsection

@section("scripts")

@endsection