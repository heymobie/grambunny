@extends("layouts.grambunny")

@section("styles")

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}



.error{

    border-color: #f23a2e !important;

    color: #f23a2e ;

}

.mandatory{
    color:red;
    font-size: x-large;
}

</style>

@endsection

@section("content")

<div class="site-section bg-light login_sec">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-7"  data-aos="fade" >

                <h2 class="mb-5 text-black">MERCHANT REGISTRATION</h2>

                <!-- partial:index.partial.html -->

                <!-- multistep form -->

                <form id="msform" class="vendor-firststep" autocomplete="off" action="{{ route("vendorRegistration.firststep") }}" method="POST">

                    <!-- progressbar -->

                    <ul id="progressbar">

                        <li class="active">Account Setup</li>

                        <li>Personal Profile</li>

                        <li>Other Details</li>

                    </ul>

                    <!-- fieldsets -->

                    <fieldset  >

                        <h2 class="fs-title">REGISTER TO BECOME A GRAMBUNNY</h2>

                        @if(session()->has("success"))

                        <div class="alert alert-success" role="alert">

                            <h4 class="alert-heading">Well done!</h4>

                            <p>{{session()->get("success")}}</p>

                        </div>

                        @endif

                        <!-- <h3 class="fs-subtitle">This is step 1</h3> -->

                        {{ csrf_field() }}

                        <div class="row form-group">

                            <div class="col-md-12">

                                <label class="text-black" for="email">Email<span class="mandatory">*</span></label>

                                <input required  type="email" value="{{ old("email") }}" id="email" name="email" class="form-control  {{ $errors->has("email") ? "is-invalid" : "" }}">

                                @if($errors->has("email"))

                                <span class="text-danger text-left">{{ $errors->first("email") }}</span>

                                @endif

                            </div>

                        </div>

                        <div class="row form-group">

                            <div class="col-md-12">

                                <label class="text-black" for="password">Password<span class="mandatory">*</span></label>

                                <input required type="password" id="password" name="password" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}">

                                @if($errors->has("password"))

                                <span class="text-danger text-left">{{ $errors->first("password") }}</span>

                                @endif

                            </div>

                        </div>

                        <div class="row form-group">

                            <div class="col-md-12">

                                <label class="text-black" for="password">Confirm Password<span class="mandatory">*</span></label>

                                <input required type="password" id="confimr_password" name="confimr_password" class="form-control">

                            </div>

                        </div>

                        <input type="submit"   class=" action-button" value="Continue" />

                        <div class="signupmsg">

                            <p>By clicking "CONTINUE", I represent that I have read, understand and agree to the grambunny <a href="{{url('terms')}}" target="_blank">Merchant Agreement</a> and <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy.</a> </p>

                        </div>

                    </fieldset>

                    

                    

                </form>

                <!-- partial -->

            </div>

            

        </div>

    </div>

</div>

@endsection

@section("scripts")

    <script src="{{ asset("public/assets/js/validate.js") }}"></script>

    <script src="{{ asset("public/assets/js/application.js") }}"></script>

@endsection