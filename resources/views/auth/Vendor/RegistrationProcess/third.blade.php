@extends("layouts.grambunny")
@section("styles")
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />
<style type="text/css">
  .is-invalid{
    border-color: #f23a2e !important;
  }
  .form-group{
    text-align: left;
  }
  .awesome-cropper .progress {
display: none;
  }
 .modal-body .col-md-9 img{
  width:  100% !important;
 }
 .modal-body .col-md-3 canvas {
    width: 100% !important;
}
.modal-body{
  display: flex;
 }
 .awesome-cropper img{
  width: 80px !important;
 }
 .mandatory{
    color:red;
    font-size: x-large;
}
</style>
@endsection
@section("content")
<div class="site-section bg-light login_sec">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7"  data-aos="fade" >

            <h2 class="mb-5 text-black">MOBIE SIGN UP</h2>

                <!-- partial:index.partial.html -->
                <!-- multistep form -->
                <form id="msform" autocomplete="off" enctype="multipart/form-data" action="{{ route("vendorRegistration.thirdStep") }}" method="POST">
                  @csrf 
                  <!-- progressbar -->
                  <ul id="progressbar">
                    <li class="active text-dark">Account Setup</li>
                    <li class="active text-dark">Personal Profile</li>
                    <li class="active text-dark">Other Details</li>
                  </ul>
                  <!-- fieldsets -->
            
                  <?php $cdate = date("Y-m-d"); ?>

                  <fieldset> 
                    <h2 class="fs-title">Other Details</h2>
                    <!-- <h3 class="fs-subtitle">We will never sell it</h3> -->
                    <div class="row">  
                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="dob">Date of Birth</label> 
                            <input type="date" min="1920-01-01" max="{{$cdate}}" id="dob" name="dob" value="{{old("dob") }}" class="form-control {{ $errors->has("dob") ? "is-invalid" : "" }}">
                            @if($errors->has("dob"))
                              <span class="text-danger">{{ $errors->first("dob") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="profile_img1">Account Profile Photo 1</label> 
                            <label for="profile_img1" class="custom-file-upload {{ $errors->has("profile_img1") ? "is-invalid" : "" }}">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input id="profile_img1" type="hidden" name="profile_img1" value="" />
                            <input id="profile_img1_old" type="hidden" name="profile_img1_old" value="" />
                            @if($errors->has("profile_img1"))
                              <span class="text-danger">{{ $errors->first("profile_img1") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="profile_img2">Account Profile Photo 2 (Optional)</label> 
                            <label for="profile_img2" class="custom-file-upload {{ $errors->has("profile_img2") ? "is-invalid" : "" }}">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input   id="profile_img2" type="hidden" name="profile_img2" value="" />
                            <input id="profile_img2_old" type="hidden" name="profile_img2_old" value="" />
                            @if($errors->has("profile_img2"))
                              <span class="text-danger">{{ $errors->first("profile_img2") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="profile_img3">Account Profile Photo 3  (Optional)</label> 
                            <label for="profile_img3" class="custom-file-upload {{ $errors->has("profile_img3") ? "is-invalid" : "" }}">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input   id="profile_img3" type="hidden" name="profile_img3" value="" />
                            <input id="profile_img3_old" type="hidden" name="profile_img3_old" value="" />
                            @if($errors->has("profile_img3"))
                              <span class="text-danger">{{ $errors->first("profile_img3") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="profile_img4">Account Profile Photo 4 (Optional)</label> 
                            <label for="profile_img4" class="custom-file-upload {{ $errors->has("profile_img4") ? "is-invalid" : "" }}">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input   id="profile_img4" type="hidden" name="profile_img4" value="" />
                            <input id="profile_img4_old" type="hidden" name="profile_img4_old" value="" />
                            @if($errors->has("profile_img4"))
                              <span class="text-danger">{{ $errors->first("profile_img4") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                       <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Video Upload</label>
                           <input type="file" accept="video/*" class="form-control" name="video" id="video" value="">
                           <input type="hidden" name="pvideo1_old" id="pvideo1_old" value="">
                          
                        </div>
                     </div>
                      
                    </div>

                    <h2 class="fs-title">Driver Licence & Permit</h2>

                    <div class="row">   
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="driver_license">Driver’s License #</label> 
                            <input type="text" id="driver_license" value="{{ old("driver_license") }}" name="driver_license" class="form-control {{ $errors->has("driver_license") ? "is-invalid" : "" }}">
                            @if($errors->has("driver_license"))
                              <span class="text-danger">{{ $errors->first("driver_license") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="license_expiry">Expiration Date</label> 
                            <input type="date" id="license_expiry" name="license_expiry" value="{{ old("license_expiry") }}" class="form-control {{ $errors->has("license_expiry") ? "is-invalid" : "" }}">
                            @if($errors->has("license_expiry"))
                                <span class="text-danger">{{ $errors->first("license_expiry") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">   
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black " for="email">Attach Driver’s License Front Copy<span class="mandatory">*</span></label> 
                            <label for="license_front " class="custom-file-upload d-none ">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input id="license_front" class="d-block {{ $errors->has("license_front") ? "is-invalid" : ""  }} " type="file" name="license_front" />
                            @if($errors->has("license_front"))
                              <span class="text-danger">{{ $errors->first("license_front") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black">Attach Driver's License Back Copy<span class="mandatory">*</span></label> 
                            <label for="license_back" class="custom-file-upload ">
                                <i class="fa fa-cloud-upload"></i> Upload Image
                            </label>
                            <input id="license_back " type="file" name="license_back" class="{{ $errors->has("license_back") ? "is-invalid" : ""  }}" />
                            @if($errors->has("license_back"))
                              <span class="text-danger">{{ $errors->first("license_back") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="permit_type">Permit/License Type</label> 
                            <input   type="text" id="permit_type" name="permit_type" value="{{ old("permit_type") }}"  class="form-control {{ $errors->has("permit_type") ? "is-invalid" : "" }}">
                            @if($errors->has("permit_type"))
                              <span class="text-danger">{{ $errors->first("permit_type") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="permit_number">Permit/License Number</label> 
                            <input   type="text" id="permit_number" name="permit_number" value="{{ old("permit_number") }}"  class="form-control {{ $errors->has("permit_number") ? "is-invalid" : "" }}">
                            @if($errors->has("permit_number"))
                              <span class="text-danger">{{ $errors->first("permit_number") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="permit_expiry">Permit/License Expiration Date</label> 
                            <input   type="date" id="permit_expiry" name="permit_expiry" value="{{ old("permit_expiry") }}"  class="form-control {{ $errors->has("permit_expiry") ? "is-invalid" : "" }}">
                            @if($errors->has("permit_expiry"))
                              <span class="text-danger">{{ $errors->first("permit_expiry") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="ssn">Social Security Number or EIN</label> 
                            <input type="text" id="ssn" name="ssn" value="{{ old("ssn") }}" class="form-control {{ $errors->has("ssn") ? "is-invalid" : "" }}">
                            @if($errors->has("ssn"))
                              <span class="text-danger">{{ $errors->first("ssn") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      
                    </div>


                    <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Instagram Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="insta" id="insta" value="" >
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Facebook Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="facebook" id="facebook" value="" >
                        </div>
                     </div>

                      <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Tik Tok Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="tiktok" id="tiktok" value="" >
                        </div>
                     </div>

                      <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Pinterest Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="pinterest" id="pinterest" value="" >
                        </div>
                     </div>
                  </div>
                  
                    <h2 class="fs-title">Service Details</h2>
                    <div class="row">
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="market_area">Merchant’s Google Map Address <span class="mandatory">*</span></label> 
                            <input required type="text" id="autocomplete" autocomplete="off" name="market_area" value="{{ old("market_area") }}"  class="form-control {{ $errors->has("market_area") ? "is-invalid" : "" }}">
                            @if($errors->has("market_area"))
                              <span class="text-danger">{{ $errors->first("market_area") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service_radius">Serviced Radius (Miles)</label> 
                            <input type="number" max="250" min="5" id="service_radius" name="service_radius" value="{{ old("service_radius") }}"  class="form-control {{ $errors->has("service_radius") ? "is-invalid" : "" }}">
                            @if($errors->has("service_radius"))
                              <span class="text-danger">{{ $errors->first("service_radius") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                   
                   <input type="hidden" name="type" value="0">
                   <input type="hidden" name="category" value="0">
                   <input type="hidden" name="subcategory" value="0">

                    <div class="row">   
                      <!--<div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Product or Service Offered</label> 
                            <select v-model="productType" name="type" class="form-control {{ $errors->has("type") ? "is-invalid" : "" }}" @change="getCategory()">
                              <option value="">Choose Type</option>
                              <option value="0">Products</option>
                              <option value="1">Services</option>
                            </select>  
                            @if($errors->has("type"))
                              <span class="text-danger">{{ $errors->first("type") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Category</label> 
                            <select v-model="selectedCategory"  class="form-control {{ $errors->has("category") ? "is-invalid" : "" }}" name="category" id="category">
                              <option value="">Choose Category</option>
                              <option :value="category.id" v-for="category in categories">@{{ category.category }}</option>
                            </select>
                            @if($errors->has("category"))
                              <span class="text-danger">{{ $errors->first("category") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Sub Category</label> 

                            <select class="form-control" name="subcategory[]" id="subcategory" style="border-radius: 10px;" multiple >
                              <option value="">Choose Sub Category</option>
                            </select>

                            @if($errors->has("subcategory"))
                              <span class="text-danger">{{ $errors->first("subcategory") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>-->

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Sales Tax (In %)</label> 
                            <input type="text" max="100" min="0" id="sales_tax" name="sales_tax" value="{{ old("sales_tax") }}"  class="form-control {{ $errors->has("sales_tax") ? "is-invalid" : "" }}">
                                               
                            @if($errors->has("sales_tax"))
                              <span class="text-danger">{{ $errors->first("sales_tax") }}</span>
                            @endif
                          </div>
                        </div>
                      </div> 

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Excise Tax (In %)</label> 
                            <input type="text" max="100" min="0" id="excise_tax" name="excise_tax" value="{{ old("excise_tax") }}"  class="form-control {{ $errors->has("excise_tax") ? "is-invalid" : "" }}">
                                               
                            @if($errors->has("sales_tax"))
                              <span class="text-danger">{{ $errors->first("sales_tax") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>  

                        <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">City Tax (In %)</label> 
                            <input type="text" max="100" min="0" id="city_tax" name="city_tax" value="{{ old("city_tax") }}"  class="form-control {{ $errors->has("city_tax") ? "is-invalid" : "" }}">
                                               
                            @if($errors->has("sales_tax"))
                              <span class="text-danger">{{ $errors->first("sales_tax") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>  


                        <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="service">Commission Rate (In %)</label> 
                            <input type="text" max="100" min="0" id="commission_rate" name="commission_rate" value="" class="form-control">     
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="description">Business Description</label> 
                            <textarea id="description" name="description" class="form-control {{ $errors->has("description") ? "is-invalid" : "" }}">{{ old("description") }}</textarea>
                            @if($errors->has("description"))
                              <span class="text-danger">{{ $errors->first("description") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <h2 class="fs-title">Vehicle Details</h2>
                    <div class="row">   
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="make">Make</label> 
                            <input type="text" id="make" name="make" value="{{ old("make") }}"  class="form-control {{ $errors->has("make") ? "is-invalid" : "" }}">
                            @if($errors->has("make"))
                              <span class="text-danger">{{ $errors->first("make") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="model">Model</label> 
                            <input type="text" id="model" name="model" value="{{ old("model") }}"  class="form-control {{ $errors->has("model") ? "is-invalid" : "" }}">
                            @if($errors->has("model"))
                              <span class="text-danger">{{ $errors->first("model") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="year">Year</label> 
                            <input type="number" max="2030" min="1985" id="year" name="year" value="{{ old("year") }}"  class="form-control {{ $errors->has("year") ? "is-invalid" : "" }}">
                            @if($errors->has("year"))
                              <span class="text-danger">{{ $errors->first("year") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                       <div class="col-md-4"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="color">Color</label> 
                            <input type="text" id="color" name="color" value="{{ old("color") }}"  class="form-control {{ $errors->has("color") ? "is-invalid" : "" }}">
                            @if($errors->has("color"))
                              <span class="text-danger">{{ $errors->first("color") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                       <div class="col-md-4"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="licenseplate">License Plate Number</label> 
                            <input type="text" id="licenseplate" name="licenseplate" value="{{ old("licenseplate") }}"  class="form-control {{ $errors->has("licenseplate") ? "is-invalid" : "" }}">
                            @if($errors->has("licenseplate"))
                              <span class="text-danger">{{ $errors->first("licenseplate") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      
                    </div>

                    <input  type="submit" name="submit" class="submit action-button" value="Submit" />
                  </fieldset>  
                </form>
                <!-- partial -->

          </div>
          
        </div>
      </div>
    </div>
@endsection
@section("scripts")
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
  
 $('#category').on('change', function() {
 
var catId = $(this).val();

$.ajax({
type: "post",
headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
data: {
        "_token": "{{ csrf_token() }}",
        "catid": catId
        },
url: "{{url('/merchant/subcategory')}}",       

success: function(msg) { 

$('#subcategory').html(msg);

}
});

}); 
 
</script>

<script>
$(function() {

  /* $('input[name="license_expiry"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: {{ date('Y') }},
    maxYear: parseInt(moment().add(35,"y").format('YYYY'),10)
  });
   $('input[name="dob"]').daterangepicker({
    autoUpdateInput: true,
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1920,
    maxYear: parseInt(moment().subtract(17,'y').format('YYYY')),
    locale: { format: 'MM/DD/YYYY' }
  }); 
    $('input[name="permit_expiry"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: {{ date('Y') }},
    maxYear: parseInt(moment().add(35,"y").format('YYYY'),10)
  }); */

});
console.log()
function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
 
    (document.getElementById('autocomplete')), {
      types: ['geocode']
    });

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
 // autocomplete.addListener('place_changed', fillInAddress);
} 
 
google.maps.event.addDomListener(window, "load", initAutocomplete);

$(document).ready(function(){

 //$('#id').val('');

});
 
</script>

<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script> 


<script type="text/javascript">

  /** Start Crop Image **/

   $(document).ready(function () {

   $('#profile_img1').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img2').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img3').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img4').awesomeCropper( { width: 400, height: 400, debug: true } );

    });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

    setInterval(function () {

            var vendor_id = $('#vendor_id').val();
            var profile1 = $('#profile_img1').val();
            var profile2 = $('#profile_img2').val();
            var profile3 = $('#profile_img3').val();
            var profile4 = $('#profile_img4').val();

 
            if(profile1 !=0){ var allprofile = profile1; var profile_field = 'profile_img1'; var fieldnum = '1'; }
            if(profile2 !=0){ var allprofile = profile2; var profile_field = 'profile_img2'; var fieldnum = '2'; }
            if(profile3 !=0){ var allprofile = profile3; var profile_field = 'profile_img3'; var fieldnum = '3'; }
            if(profile4 !=0){ var allprofile = profile4; var profile_field = 'profile_img4'; var fieldnum = '4'; }


            if(profile1 !=0 || profile2 !=0 || profile3 !=0 || profile4 !=0){

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                url: "https://www.grambunny.com/merchant/crop_imagesave1",
                data: {profile : allprofile, }, 
                cache: false,
                success: function (data)
                {
                   
                    $('#profile_img'+fieldnum).val('');
                    $('#profile_img'+fieldnum+'_old').val(data.status);

                }
            });

          }

    },5000); 
  
  /** Crop image */ 

  </script>

@endsection
