@extends("layouts.grambunny")
@section("styles")
<style type="text/css">
  .is-invalid{
    border-color: #f23a2e !important;
  }
  .form-group{
    text-align: left;
  }
  .mandatory{
    color:red;
    font-size: x-large;
}
</style>
@endsection
@section("content")
<div class="site-section bg-light login_sec">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7"  data-aos="fade" >

            <h2 class="mb-5 text-black">MOBIE SIGN UP</h2>

                <!-- partial:index.partial.html -->
                <!-- multistep form -->
                <form id="msform" autocomplete="off" action="{{ route("vendorRegistration.secondstep") }}" method="POST">
                  <!-- progressbar -->
                  <ul id="progressbar">
                    <li class="active text-dark">Account Setup</li>
                    <li class="active text-dark">Personal Profile</li>
                    <li >Other Details</li>
                  </ul>
                  <!-- fieldsets -->
                 <fieldset>
                    <h2 class="fs-title">Personal Profile</h2>
                    <!-- <h3 class="fs-subtitle">We will never sell it</h3> -->
                    {{ csrf_field() }}
                    <div class="row">   
                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="firstname">First Name<span class="mandatory">*</span></label> 
                            <input required type="text" name="firstname" value="{{ old("firstname") }}" id="firstname" class="form-control {{ $errors->has("firstname") ? "is-invalid" : "" }}" >
                            @if($errors->has("firstname"))
                              <span class="text-danger ">{{ $errors->first("firstname") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="lastname">Last Name<span class="mandatory">*</span></label> 
                            <input required type="text" id="lastname" name="lastname" value="{{ old("lastname") }}" class="form-control {{ $errors->has("lastname") ? "is-invalid" : "" }}">
                             @if($errors->has("lastname"))
                              <span class="text-danger ">{{ $errors->first("lastname") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="row">   
                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="username">Username<span class="mandatory">*</span></label> 
                            <input required type="text" value="{{ old("username") }}" id="username" name="username" class="form-control {{ $errors->has("username") ? "is-invalid" : "" }}">
                             @if($errors->has("username"))
                              <span class="text-danger">{{ $errors->first("username") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                          <label class="text-black" for="phone">Contact Phone Number<span class="mandatory">*</span></label> 
                          <input required type="text" value="{{ old("phone") }}" id="phone" name="phone" class="form-control {{ $errors->has("phone") ? "is-invalid" : "" }}">
                             @if($errors->has("phone"))
                              <span class="text-danger">{{ $errors->first("phone") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row">  

                      <div class="col-md-6">  
                        <div class="row form-group">                
                        <div class="col-md-12">
                        <label class="text-black" for="business">Full Company Name</label>
                        <input  type="text" value="{{ old("business") }}" id="business" name="business" class="form-control {{ $errors->has("business") ? "is-invalid" : "" }}">
                        @if($errors->has("business"))
                        <span class="text-danger ">{{ $errors->first("business") }}</span>
                        @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="zipcode">Zip Code</label> 
                            <input type="text" id="zipcode" name="zipcode" value="{{ old("zipcode") }}" class="form-control {{ $errors->has("zipcode") ? "is-invalid " : "" }}">
                            @if($errors->has("zipcode"))
                              <span class="text-danger">{{ $errors->first("zipcode") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row"> 

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="mail_address">Mailing Address</label> 
                            <input type="text" id="mail_address" value="{{ old("mail_address") }}" name="mail_address" class="form-control {{ $errors->has("mail_address") ? "is-invalid" : "" }}">
                            @if($errors->has("mail_address"))
                              <span class="text-danger">{{ $errors->first("mail_address") }}</span>
                            @endif
                          </div>
                        </div>
                      </div> 

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="address">Apt/Suite/Unit Number</label> 
                            <input type="text" id="address" name="address" value="{{ old("address") }}" class="form-control {{ $errors->has("address") ? "is-invalid" : "" }}">
                            @if($errors->has("address"))
                              <span class="text-danger">{{ $errors->first("address") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                    </div>


                    <div class="row"> 

                      <div class="col-md-6"> 
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="city">City</label> 
                            <input type="text" id="city" value="{{ old("city") }}" name="city" class="form-control {{ $errors->has("city") ? "is-invalid" : "" }}">
                              @if($errors->has("city"))
                              <span class="text-danger">{{ $errors->first("city") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">  
                        <div class="row form-group">                
                          <div class="col-md-12">
                            <label class="text-black" for="state">State<span class="mandatory">*</span></label> 
                            <app-select-states></app-select-states>
                            @if($errors->has("state"))
                              <span class="text-danger">{{ $errors->first("state") }}</span>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>


                    <input type="submit" name="next" class="next action-button" value="Next" />
                  </fieldset>

                
                </form>
                <!-- partial -->

          </div>
          
        </div>
      </div>
    </div>
@endsection
@section("scripts")
<script>
  document.getElementById('phone').addEventListener('input', function (e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
});
  </script>
@endsection
