@extends("layouts.grambunny")
 
@section("content")
 <div class="site-section bg-light login_sec">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-6 mb-5" data-aos="fade">
                            <h2 class="mb-5 text-black">CUSTOMER LOGIN</h2>
                            <form action="{{ route("webAuth") }}" method="POST" class="p-5 bg-white">
                                @if(session()->has("success"))
                                <div class="alert alert-success" role="alert">
                                    <strong>{{ session()->get("success") }} </strong>
                                </div>
                                @elseif(session()->has("warning"))
                                <div class="alert alert-warning" role="alert">
                                    <h4 class="alert-heading">Email Verification Required!</h4>
                                    <p>A verification link has been sent to your email account. Please verify your email to continue.</p>
                                </div>
                                @endif
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label class="text-black" for="email">Email</label>
                                        <input value="{{ (isset($_COOKIE["remember"])) ? json_decode($_COOKIE["remember"])->email : ""  }}" required type="email" id="email" name="email" class="form-control {{ $errors->has("email") ? "is-invalid" : "" }}">
                                        @if($errors->has("email"))
                                        <span class="text-danger">{{ $errors->first("email") }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <label class="text-black" for="subject">Password</label>
                                        <input value="{{ (isset($_COOKIE["remember"])) ? json_decode($_COOKIE["remember"])->password : ""  }}"  required type="password" name="password" id="subject" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}">
                                        @if($errors->has("password"))
                                        <span class="text-danger">{{ $errors->first("password") }}</span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="row form-group loginmid">
                                    <div class="col-6">
                                        <div class="form-check">
                                            <input {{ (isset($_COOKIE["remember"])) ? "checked" : ""  }} class="form-check-input" type="checkbox" name="remember_me" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="{{ url("forgot-password") }}" class="nav-link">Forgot Password?</a>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12">
                                    <input type="hidden" value="{{ request()->redirect_url }}" name="redirect_url">
                                        <input type="submit" value="Login" class="btn btn-primary py-2 px-4 text-white">
                                    </div>
                                </div>
                                <!--<div class="row social_med_log">
 <a class="loginBtn loginBtn--facebook" href="{{ route("social.auth",["provider" => "facebook"]) }}">
 <i class="fa fa-facebook" aria-hidden="true"></i> Login with Facebook
 </a>

<a class="loginBtn loginBtn--google" href="{{ route("social.auth",["provider" => "google"]) }}">
 <i class="fa fa-google" aria-hidden="true"></i> Login with Google
</a>
                                </div>-->
                                <div class="row form-group loginfoot">
                                    <div class="col-12">
                                        <p>No account yet? <a href="{{ route("signUpPage") }}"> </a></p>
                                    </div>
                                     <div class="col-12">
                                        <p> <a href="{{ route("signUpPage") }}" class="fot-btn-check">Register</a></p>
                                    </div>

                                   <!--  <div class="col-12">
                                        <p> <a href="{{ route("checkout") }}" class="fot-btn-check">Checkout As A Guest </a></p>
                                    </div> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection