@extends("layouts.grambunny")

@section("styles")

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}
#msform {
    min-height: auto;
}
</style>

@endsection

@section("content")

<div class="site-section bg-light login_sec">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-7"  data-aos="fade" >

               

                <!-- partial:index.partial.html -->

                <!-- multistep form -->

                <form id="msform" >

            

                    

                    <fieldset>

                        <div class="alert alert-success" role="alert">

                            <h4 class="alert-heading mb-3">Account registered successfully.</h4>

                         
                            <p>Thank You!</p>

                        </div>

                    </fieldset>

                    

                </form>

                <!-- partial -->

            </div>

            

        </div>

    </div>

</div>

@endsection

@section("scripts")

@endsection