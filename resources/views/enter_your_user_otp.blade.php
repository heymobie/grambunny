<!-- @extends('layouts.app') -->

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')
<div id="position">
	<div class="container">
		<ul>
			<li><a href="{{ url('/') }}">Home</a></li>
			<li>Update Account</li>
		</ul>
	</div>
</div> <!-- Position -->
 
<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message')}}
					</div>
					@endif
					@if(Session::has('message_error'))					
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message_error')}}
					</div>
					@endif
					<div>
						<form role="form" id="update_pwd" method="POST" action="{{ url('/user_password/user_enter_otp') }}">
							{{ csrf_field() }}
							<div class="box_style_2">
								<h2 class="inner">Enter OTP</h2>
								<input type="hidden" class="form-control" name="userID" value="{{ Session::get('user_id') }}">
						        @if(Session::has('Succes'))
						        <div class="alert alert-success alert-block">
						            <button type="button" class="close" data-dismiss="alert">×</button>
						            {{ Session::get('Succes') }}
						        </div>
						        @endif
						        @if (Session::has('Otp_Error'))
						        <div class="alert alert-danger alert-block">
						            <button type="button" class="close" data-dismiss="alert">×</button>
						            {{ Session::get('Otp_Error') }}
						        </div>
						        @endif
								<div class="form-group up-pas">
									<input type="text" class="form-control" name="user_otp" id="user_otp" placeholder="OTP">
								</div>
								
								<div class="row">
									<div class="">
										<div class="col-md-12">
											<button type="submit" class="btn btn-submit margin_30" id="submit_otp">Submit</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div>
					</div>					
				</div>
			</div>
		</div>
	</div><!-- End row -->
	<hr class="more_margin">
</div>

<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	

@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->
<!-- <script>					   
		 $(document).on('click', '#update_oldpwd', function(){ 
		 	
			var form = $("#update_pwd");
				form.validate();
			var valid =	form.valid();
		 });
</script>	

<script type="text/javascript">
 $(document).ready(function () {
 
 
 
 $("#showHide_old").click(function () {
	 if ($("#old_password").attr("type")=="password") {
	 $("#old_password").attr("type", "text");
	 }
	 else{
	 $("#old_password").attr("type", "password");
	 }
 
 });
 
 $("#showHide").click(function () {
 if ($("#new_password").attr("type")=="password") {
 $("#new_password").attr("type", "text");
 }
 else{
 $("#new_password").attr("type", "password");
 }
 
 });

  $("#cshowHide").click(function () {
 if ($("#cnew_password").attr("type")=="password") {
 $("#cnew_password").attr("type", "text");
 }
 else{
 $("#cnew_password").attr("type", "password");
 }
 
 });

 });
</script> -->
		 
@stop	