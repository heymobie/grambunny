@if(count($vehicle_listing)>0)
	<div class="lstng_resp" id="listing_transport">
	<div>
@if(!empty($vehicle_listing))
	@foreach($vehicle_listing as $special_list)
	
	<div class="strip_list wow fadeIn" data-wow-delay="0.1s">
	@if($special_list->transport_classi!='5')
		<div class="ribbon_1">
		   <span class="left_head"> 
				@if($special_list->transport_classi=='1') Sponsored @endif
				@if($special_list->transport_classi=='2') Popular @endif
				@if($special_list->transport_classi=='3') Special @endif
				@if($special_list->transport_classi=='4') New @endif
				@if($special_list->transport_classi=='5') Standard @endif
			</span>
		</div>
	@endif	
		<div class="row">
			<div class="col-md-9 col-sm-9">
				<div class="desc">
					<div class="thumb_strip">
						<?php 
								$vehcile_img = DB::table('vehicle_img')
											->where('vehicle_id', '=' ,$special_list->vehicle_id)->get();	
								if(count($vehcile_img)>0)			
								{
									?>
									<div id="owl-demo-<?php echo $special_list->vehicle_id;?>">
									<?php foreach($vehcile_img as $img_list){
									
									$new_path = url('/') .'/uploads/vehicle/thumb/'.$img_list->imgPath;
									if(file_exists($new_path))
									{
										$new_path = url('/') .'/uploads/vehicle/'.$img_list->imgPath;
									}
									
									?>
											<img src="{{ $new_path }}" data-darkbox="{{ url('/') }}/uploads/vehicle/{{$img_list->imgPath}}" data-darkbox-group="<?php echo $special_list->vehicle_id;?>">	
									 
									 
										
								<?php }?>
											
							 
								</div>
           
							<?php
								}
								else
								{
								?>
									<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
								<?php 
								}
								?>
						
					</div>
					<div class="rating">
					  <?php 
								$avg_rating = '';				
								$count_review = 0 ;
								$count_avg_rating =0;
								$total_rating = DB::table('transport_review')	
											->select( DB::raw('SUM(re_rating) as rating'))	
											->where('re_transid', '=' ,$special_list->transport_id) 
											->where('re_vehicleid', '=' ,$special_list->vehicle_id) 
											->where('re_status', '=' ,'PUBLISHED')
											->orderBy('re_id', 'desc')						
											->get();	
											
								$count_review = DB::table('transport_review')	
											->select( DB::raw('SUM(re_rating) as rating'))	
											->where('re_transid', '=' ,$special_list->transport_id) 
											->where('re_vehicleid', '=' ,$special_list->vehicle_id) 
											->where('re_status', '=' ,'PUBLISHED')
											->orderBy('re_id', 'desc')						
											->count();	
								$count_avg_rating =	$total_rating[0]->rating;
									
								
			
					?>	
					@if(($count_review>0) && ($count_avg_rating>0))	
					<?php $avg_rating = round(($count_avg_rating/$count_review)/2); ?>
			   
				  @for ($x = 1; $x < 6; $x++)
					@if(($avg_rating>0) && ($avg_rating>=$x))
					<i class="icon_star voted"></i>
					@else
						<i class="icon_star"></i>
					@endif
				  @endfor  
				 (<small><a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->transport_suburb)).'/'.str_replace(' ','_',trim($special_list->vehicle_rego)).'/'.str_replace(' ','_',trim($special_list->vclass_name)).'/'.str_replace(' ','_',trim($special_list->model_name)).'/'.str_replace(' ','_',trim($special_list->vehicle_id))).'/review')}}">{{$count_review}} reviews</a></small>)
				@endif
					</div>
					<h3>{{$special_list->make_name.' - '.$special_list->model_name}}</h3>
					<div class="type">						
					{{$special_list->vclass_name}} 	- {{$special_list->vcate_name}}
					</div>
					<div class="location">
					
					<span class="opening">Maximum {{ $special_list->vehicle_pax}} Passengers</span>
					
					 @if(!empty($special_list->vehicle_minrate))
						  Minimum Rate: ${{$special_list->vehicle_minrate}}
					@endif
					</div>
					
				  
				<ul>								
					
								@if($special_list->vehicle_ac==1)
								<li>A/C<i class="icon_check_alt2 ok"></i></li>
								@endif	
								@if($special_list->vehicle_music==1)
								<li>Music<i class="icon_check_alt2 ok"></i></li>
								@endif	
								@if($special_list->vehicle_video==1)
								<li>Video<i class="icon_check_alt2 ok"></i></li>
								@endif	
				</ul>
				
				</div>
			</div>
			<div class="col-md-3 col-sm-3">
				<div class="go_to">
					<div>
									
				<form name="vehicle_info" id="vehicleinfo-{{$special_list->vehicle_id}}">
						<input type="hidden" name="vehicle_id" id="vehicle_id-{{$special_list->vehicle_id}}" value="{{$special_list->vehicle_id}}" />
						<input type="hidden" name="vehicle_from" id="vehicle_from-{{$special_list->vehicle_id}}" value="{{$surbur_name}}" />
						<input type="hidden" name="vehicle_to" id="vehicle_to-{{$special_list->vehicle_id}}" value="{{$to_surbur_name}}"  />
						<input type="hidden" name="vehicle_ontime" id="vehicle_ontime-{{$special_list->vehicle_id}}" value="{{$on_time}}"   />
						<input type="hidden" name="vehicle_ondate" id="vehicle_ondate-{{$special_list->vehicle_id}}" value="{{$ondate}}" />
						<input type="hidden" name="vehicle_returntime" id="vehicle_returntime-{{$special_list->vehicle_id}}" value="{{$return_time}}" />
						<input type="hidden" name="vehicle_returndate" id="vehicle_returndate-{{$special_list->vehicle_id}}" value="{{$return_date}}"  />
					</form>
						<a href="javascript:void(0)" id="view_rate-{{$special_list->vehicle_id}}" data-vehicleid="{{$special_list->vehicle_id}}" class="btn_1">View Rate</a>
				
					</div>
				</div>
			</div>
		</div><!-- End row-->
	</div>
	
	<!-- End col-md-6-->

	@endforeach
@else
	Restaurant Not Found!
@endif
	
	</div>
		</div>
@else
	<center> Transport not found! </center>	
@endif

		
@if($page_no>0)		
	<a href="javascript:void(0)" class="load_more_bt wow fadeIn" data-wow-delay="0.2s" id="trans_list_load-{{$page_no}}" data-nextpage="{{$page_no}}" >Load more...</a>
@endif

<script type="text/livescript" src="{{ url('/') }}/design/js/darkbox.js"></script>

	
<script src="{{ url('/') }}/design/front/js/jquery.prettyPhoto.js"></script> 
<script src="{{ url('/') }}/design/front/js/owl.carousel.min.js"></script> 


<script>
 /*$(document).ready(function () {
   
     $("a[rel^='prettyPhoto']").prettyPhoto();

    $("[id^='picture']").owlCarousel({

         autoPlay: 3000, //Set AutoPlay to 3 seconds

         items: 1,
         itemsDesktop: [1199, 1],
         itemsDesktopSmall: [979, 1]

     });	 
 });*/
 $(document).ready(function() {

  $("[id^='owl-demo-']").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds 
      items : 1,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [979,1]
 
  });
 
});
</script>
