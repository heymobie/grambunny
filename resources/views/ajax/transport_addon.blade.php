<?php 
//echo '<pre>';
//print_r($group_name);/
//print_r($group_list);

?>

<form method="POST" id="addon_frm">
			<input type="hidden" name="trans_id" value="{{$trans_id}}">
			<input type="hidden" name="vehicle_id" value="{{$vehicle_id}}">
			<input type="hidden" name="rate_id" value="{{$rate_id}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="menu_option" class="model_option" value="{{$menu_option}}" />
			
          <h2>{{str_replace('_',' ',$menu_category_name)}}</h2>
          <p class="dei-text">{{$menu_category_desc}}</p>
		 <?php $g =0;?> 
		  @foreach($group_name as $grp_name)
		  
		  <?php $req = '';
		  if(substr($grp_name, -1)=='*') { $req = 'required=required'; }?>
		  
		  <input type="hidden" name="group_name[]" value="{{trim(str_replace(' ','',$grp_name))}}" />
		  
          <h3 class="small-title">{{$grp_name}}</h3>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <ul class="item-list">
			  
			  	@foreach($group_list[$g] as $grp_list)
					 
						<li>
					  <div class="control-group islamic2">
					  
					  <label>
					  	@if($grp_list->addon_option=='check')
						<span class="control control--checkbox">	
						<input type="checkbox" name="{{trim(str_replace(' ','',$grp_name))}}_option[]" value="{{$grp_list->addon_id}}" {{$req}}>
						<div class="control__indicator"></div>
						</span>
						<!--class="icheck"-->
							
						@elseif($grp_list->addon_option=='radio')
						<span class="control control--radio">
						<input type="radio" name="{{trim(str_replace(' ','',$grp_name))}}_option[]" value="{{$grp_list->addon_id}}" {{$req}}>
						<div class="control__indicator"></div>
						</span>
						<!-- class="icheck"-->
						@endif
						<span class="vali_txt">{{$grp_list->addon_name}} </span>
						
						@if($grp_list->addon_price>0)
						<small class="pull-right">$ {{$grp_list->addon_price}}</small>
						@endif
						</label>
						</div>
					</li>
					
				@endforeach
                
              </ul>
            </div>
          </div>
		  <?php $g++;?>
		  @endforeach
          <p>
		  	<a href="javascript:void(0)" class="btn btn-primary" id="addon_continue">Continue</a>
           <!-- <button type="submit" class="btn btn-primary">Continue</button>-->
          </p>
          <div class="cleafix"></div>
	</form>
