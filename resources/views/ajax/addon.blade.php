<?php $lang = Session::get('locale'); ?>
<form method="POST" id="addon_frm">
			<input type="hidden" name="rest_id" value="{{$rest_id}}">
			<input type="hidden" name="menu_id" value="{{$menu_id}}">
			<input type="hidden" name="menu_cat_id" value="{{$menu_category_id}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="menu_option" class="model_option" value="{{$menu_option}}" />
			
          <h2><?php if($lang=='ar'){ echo $menu_category_name_ar; }else{ echo $menu_category_name; }?></h2>

         <p class="dei-text"><?php if($lang=='ar'){ echo $menu_category_desc_ar; }else{ echo $menu_category_desc; }?></p>
		 <?php $g =0;?> 

		 @if($group_name)
		  @foreach($group_name as $grp_name)
		  
		  <?php $req = '';
		  if(substr($grp_name, -1)=='*') { $req = 'required=required'; }?>
		  
		  <input type="hidden" name="group_name[]" value="{{trim(str_replace(' ','',$grp_name))}}" />
		  
          <h3 class="small-title">{{$grp_name}}</h3>
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <ul class="item-list control-group">
			  
			  	@foreach($group_list[$g] as $grp_list)
					 
						<li>	
						 	@if($grp_list->addon_option=='check')	
						
						 <label class="control control--checkbox"><?php if($lang=='ar'){ echo $grp_list->addon_name_ar;}else{ echo $grp_list->addon_name; } ?>  
						 @if($grp_list->addon_price>0)
						<small class="pull-right"> ${{$grp_list->addon_price}}</small>
						@endif
                    <input type="checkbox"  name="{{trim(str_replace(' ','',$grp_name))}}_option[]" value="{{$grp_list->addon_id}}" {{$req}}   class="icheck">
                     <div class="control__indicator"></div>
                  </label>	
				  			@elseif($grp_list->addon_option=='radio')
				  
				 <label class="control control--radio"><?php if($lang=='ar'){ echo $grp_list->addon_name_ar;}else{ echo $grp_list->addon_name; } ?>  @if($grp_list->addon_price>0)
						<small class="pull-right"> ${{$grp_list->addon_price}}</small>
						@endif
                    <input type="radio" name="{{trim(str_replace(' ','',$grp_name))}}_option[]" value="{{$grp_list->addon_id}}" {{$req}}   class="icheck">
                     <div class="control__indicator"></div>
                  </label>		
				 	 @endif
					  
					  
					</li>
					
				@endforeach
                
              </ul>
            </div>
          </div>
		  <?php $g++;?>
		  @endforeach
		  @endif
		  
          <p>
			  <h3 class="small-title">@lang('translation.specialInstructions')</h3>
		  		<textarea name="special_text" id="special_text" class="form-control"></textarea>
          </p>
		  
          <p>
		  
		  	<a href="javascript:void(0)" class="btn btn-primary" id="addon_continue" onclick="submit_continue();">@lang('translation.btnContinue')</a>
		  	<!--<a href="javascript:void(0)" class="btn btn-primary" id="addon_continue" onclick="test();">Continue</a>-->
           <!-- <button type="submit" class="btn btn-primary">Continue</button>-->
          </p>
          <div class="cleafix"></div>
	</form>