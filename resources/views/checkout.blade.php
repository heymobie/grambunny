@extends('layouts.app')

@section("other_css")
<?php $lang = Session::get('locale'); ?>

<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">
   	  <!-- Radio and check inputs -->
    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->
  

<meta name="_token" content="{!! csrf_token() !!}"/>
@stop
@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window chckout_prllx_top" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
  <div id="subheader">
    <div id="sub_content">
      <h1>@lang('translation.placeYourOrder')</h1>
      <div class="bs-wizard">
        <div class="col-xs-4 bs-wizard-step active">
          <div class="text-center bs-wizard-stepnum"><strong>1.</strong> @lang('translation.yourDetails')</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="#0" class="bs-wizard-dot"></a> </div>
        <div class="col-xs-4 bs-wizard-step disabled">
          <div class="text-center bs-wizard-stepnum"><strong>2.</strong> @lang('translation.payment')</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="#" class="bs-wizard-dot"></a> </div>
        <div class="col-xs-4 bs-wizard-step disabled">
          <div class="text-center bs-wizard-stepnum"><strong>3.</strong> @lang('translation.finish')</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="cart_3.html" class="bs-wizard-dot"></a> </div>
      </div>
      <!-- End bs-wizard --> 
    </div>
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<!-- Position -->
    <!-- Content ================================================== -->
  <div class="container margin_60_35">
  <div id="container_pin">
    <div class="row">
      <div class="col-md-3">
        <div class="box_style_2 hidden-xs info">
          <h4 class="nomargin_top">
            @if(isset($delivery[0]->page_content))
              @lang('translation.deliveryTime') <i class="icon_clock_alt pull-right"></i>
            @else 
            @endif
          </h4>
          <div>
          @if(isset($delivery[0]->page_content))
          {{strip_tags($delivery[0]->page_content)}}
          @else
          @endif
          </div>
          <hr>
          <h4>
            @if(isset($secure[0]->page_content))
            @lang('translation.securePayment') <i class="icon_creditcard pull-right"></i>
            @else 
            @endif
          </h4>
            @if(isset($secure[0]->page_content))
            {{strip_tags($secure[0]->page_content)}} 
            @else
            @endif
        </div>
        <!-- End box_style_1 -->
        
        <!--<div class="box_style_2 hidden-xs" id="help"> <i class="icon_lifesaver"></i>
          <h4>Need <span>Help?</span></h4>
          <a href="#" class="phone">+1800123456</a> </div>-->
      </div>
      <!-- End col-md-3 -->
      
      <div class="col-md-6"> 
	 <!-- @if(Auth::guest()) 
        <div class="box_style_2">
          <div class="cart-lg-ctn"> <a href="#0" data-toggle="modal" data-target="#login_2" class="lg-btn">@lang('translation.login')</a> <span class="or-text">@lang('translation.or')</span> <span class="guest-text">@lang('translation.btnContinueAsGuest')</span>
            
          </div>
        </div>
	  @endif	-->
    <!-- {{url('/submit_checkoutdetail')}} -->
		<form name="checkout_user_info" id="checkout_user_info" method="post"><!--payment-->
		
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="hidden" name="rest_id" id="rest_id" value="{{$rest_id}}">
		  <input type="hidden" name="service_type" value="{{$option}}" />
		  <input type="hidden" name="service_area"  value="{{$service_area}}" />
		  <input type="hidden" name="rest_meta_url"  value="{{url($rest_meta_url)}}" />
		  <input type="hidden" name="user_id" id="user_id"  value="@if(Auth::guest()) 0 @else {{ Auth::user()->id }}@endif" />
		  <input type="hidden" name="todayclosed" id="todayclosed" value="{{$today_holiday}}" />
		  <input type="hidden" name="holiday" id="holiday" value="{{$holiday_cal}}" />
		  <input type="hidden" name="open_status" id="rest_open_status" value="{{$rest_open_status}}" />
		  <input type="hidden" name="back_time" id="back_time" value="" />
		  
		  
		
        <div class="box_style_2 delivery-info-ctn">
          <h2 class="inner">{{$option}} Info</h2>
		  @if(Session::get('service_option')!='Dine-in')
          <div class="delivery-time-ctn">
           <!-- <label>{{Session::get('service_option')}} Time</label>-->
            <div class="row">
              <div class="col-md-6 col-sm-6">
			  
			   @if($today_holiday=='0')
                <div class="form-group islamic2">
				  <label class="control control--radio">				  
				  
				  <input type="radio" value="asp" name="deliveri_time" class="info_time" @if((Session::get('cart_userinfo')['deliveri_time']=='asp') && (Session::get('cart_userinfo')['deliveri_time']!='later'))checked="checked" @elseif(!(Session::has('cart_userinfo')['deliveri_time']))  checked="checked" @endif>
				  
                  <div class="control__indicator"></div>
				  </label> <span class="pick_up_info">@lang('translation.asap')</span>
                    
                </div>
				
				@endif
              </div>
              <!--<div class="col-md-6 col-sm-6">
                <div class="form-group islamic2">
				  <label class="control control--radio">
				  <input type="radio" value="later" name="deliveri_time" @if(( (Session::has('cart_userinfo')) &&(Session::get('cart_userinfo')['deliveri_time']=='later')) || ($today_holiday=='1'))checked="checked" @endif class="info_time">
				  
                  <div class="control__indicator"></div>
				  </label> <span class="pick_up_info">@lang('translation.later')</span>
                    
                </div>
              </div>-->
            </div>
          </div> 
          <div class="time-date-ctn"   @if((Session::get('cart_userinfo')['deliveri_time']=='later')||($today_holiday=='1')) style="display:block" @else style="display:none" @endif >
            <div class="row">
              <div class="col-md-6 col-sm-6">
                <div class="form-group">
                  <label>@lang('translation.labelDate')</label>
                  <div class="date" id="datetimepicker1">
                    <input type="text" name="pickup_date" id="datepicker" class="form-control" required="required" value="@if(Session::get('cart_userinfo')['pickup_date']) {{Session::get('cart_userinfo')['pickup_date']}} @endif">
                   </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="form-group">
								@if(!empty($delivery_time))

                <label>@lang('translation.labelTime')</label> 
				<div  id="delivery_pick_time">					
				<select class="form-control" name="pickup_time" id="pickup_time" style="width:48%">
					@foreach($delivery_time as $dtime)
					<?php $sdtval = str_replace(':','',$dtime);
						$new_time = $dtime;
						if($new_time=='00:00'){$new_time = '12:00';}
						if($new_time=='00:30'){$new_time = '12:30';}
					
					?>
					 <option value="{{$sdtval}}" @if($rest_open_time==$sdtval) selected="selected" @endif>{{$new_time}}</option>
					@endforeach
				</select>
				
				<select class="form-control" name="pickup_time_format" id="pickup_time_format" style="width:40%">
					 <option value="AM"@if($rest_time_format=='AM')selected="selected" @endif>AM</option>
					 <option value="PM"@if($rest_time_format=='PM')selected="selected" @endif>PM</option>
				</select>
				
					<div id="time_error_msg" style="display:none; color:#FF0000;"></div>
				
				</div>
				
				@endif
                   </div>
              </div>
            </div>
          </div>
		  @endif
          @if(Session::get('cart_userinfo')['service_type']=='Delivery')

     <?php 

     $address_order = $user_detail['address_order'];
     $city_order = $user_detail['city_order'];
     $pcode_order = $user_detail['pcode_order'];

    if(empty($address_order)){

      $address_order = '';
      $city_order = '';
      $pcode_order = '';

    } 

   $user_id = ''; 

    if(!empty(Auth::user()->id)){

    $user_id = Auth::user()->id;

    $address_data = DB::table('delivery_address')->where('del_userid', '=' ,$user_id)->get(); 
    
    }

  ?>  

 <?php 
   if(!empty($address_data)){
  $i=1;
    foreach ($address_data as $key => $value) { ?>

     <p id="udeladd<?php echo $value->del_id; ?>"><label><input type="radio" value="oldaddress<?php echo $i; ?>" name="del_address_new" class="del_address_new"> @lang('translation.labelDeliveryAddress') - </label>

    <?php $del_id = $value->del_id; ?>
    <?php $del_userid = $value->del_userid; ?>
    <?php $del_contactno = $value->del_contactno; ?>
    <?php echo $del_address = $value->del_address; ?>
    <?php $del_city = $value->del_city; ?>
    <?php $del_zipcode = $value->del_zipcode; ?>
    <?php $del_state = $value->del_state; ?>
    <?php $del_country = $value->del_country; ?>
    <?php $del_lat = $value->del_lat; ?>
    <?php $del_long = $value->del_long; ?>

   <span class="checkaddel" id="checkaddel" onclick="return del_addressm('{{$del_id}}')"><i class="fa fa-trash-o"></i></span></p>  

  <input type="hidden" name="del_address<?php echo $i; ?>" id="del_address<?php echo $i; ?>" value="<?php echo $del_address; ?>">

   <input type="hidden" name="city<?php echo $i; ?>" id="city<?php echo $i; ?>" value="<?php echo $del_city; ?>">
   <input type="hidden" name="zipcode<?php echo $i; ?>" id="zipcode<?php echo $i; ?>" value="<?php echo $del_zipcode; ?>">

 <?php $i++; } } ?> 

<label><input type="radio" value="newaddress" name="del_address_new" class="del_address_new" checked="checked"> @lang('translation.labelDeliveryAddress')</label>

		  <div class="form-group">

           <?php if(Session::has('cart_userinfo')['delivery_add1']){ ?>

             <input type="text" name="delivery_add1" id="delivery_add1" value="{{Session::get('cart_userinfo')['delivery_add1']}}" class="form-control" required="required"> 

           <?php }else{ ?>
            <input type="text" name="delivery_add1" id="delivery_add1" value="{{$address_order}}" class="form-control" required="required">
            <?php } ?>


          </div>
		  <input type="hidden" name="delivery_add2" id="delivery_add2" value="" >
		  <input type="hidden" name="delivery_number" id="delivery_number" value="">
		 
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>@lang('translation.labelCity')</label>	

                <?php if(Session::has('cart_userinfo')['delivery_surbur']){ ?>

                  <input type="text" name="delivery_surbur" id="delivery_surbur" value="{{Session::get('cart_userinfo')['delivery_surbur']}}" class="form-control" readonly="readonly">

                <?php }else{ ?>   
           		 <input type="text" name="delivery_surbur" id="delivery_surbur" value="{{$city_order}}" class="form-control" readonly="readonly">
               <?php } ?>

              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>@lang('translation.labelPostcode')</label>	

               <?php if(Session::has('cart_userinfo')['delivery_postcode']){ ?>

           		 <input type="text" name="delivery_postcode" id="delivery_postcode" value="{{Session::get('cart_userinfo')['delivery_postcode']}}" class="form-control"  number="number"  maxlength="6" readonly="readonly">

               <?php }else{ ?> 

                <input type="text" name="delivery_postcode" id="delivery_postcode" value="{{$pcode_order}}" class="form-control"  number="number"  maxlength="6" readonly="readonly">

                <?php } ?>

              </div>

              <input type="hidden" name="order_lat" id="order_lat" value="">
              <input type="hidden" name="order_long" id="order_long" value="">

            </div>
			 
          </div>

		 
		  @endif
		  
		  
          <div class="form-group">
            <label>@lang('translation.labelInstructions')</label>
            <textarea class="form-control" name="order_instruction" id="order_instruction">@if(Session::get('cart_userinfo')['order_instruction']){{Session::get('cart_userinfo')['order_instruction']}} @endif</textarea>
          </div>
        </div>
        <div class="box_style_2" id="order_process">
          <h2 class="inner">@lang('translation.yourContactDetails')</h2>
          <div class="form-group">
            <label>@lang('translation.firstName')</label>
            <input type="text" class="form-control" id="firstname_order" name="firstname_order" placeholder="@lang('translation.firstName')"  value="{{$user_detail['firstname_order']}}">
          </div>
          
          <div class="form-group">
            <label>@lang('translation.lastName')</label>
            <input type="text" class="form-control" id="lastname_order" name="lastname_order" placeholder="@lang('translation.lastName')" value="{{$user_detail['lastname_order']}}">
          </div>
		  
          <div class="form-group">
            <label>@lang('translation.mobileNumber')</label>
				  <div class="row">

       <input type="hidden" name="user_mob_type" id="order_mob_type" value="1">  
          
			<div class="col-md-12 col-sm-12">
              <div class="form-group">		
           		 <input type="text" id="tel_order" name="tel_order" class="form-control" placeholder="@lang('translation.mobileNumber')" number="number" value="{{$user_detail['tel_order']}}" maxlength="10">
              </div>
            </div>
			</div>
           
          </div>
		
		  
		  
		  
		  
		  
          <div class="form-group">
            <label>@lang('translation.email')</label> 
            <input type="email" id="email_booking_2" name="email_order" class="form-control" placeholder="@lang('translation.email')"  value="{{$user_detail['email_order']}}" >
          </div>
         <!-- <div class="form-group">
            <label>Your full address</label>
            <input type="text" id="address_order" name="address_order" class="form-control" placeholder=" Your full address"  value="{{$user_detail['address_order']}}"  > onFocus="geolocate()"
			
 <input type="hidden"  id="street_number" name="street_number"/>	  
	 <input type="hidden" class="field" id="route" name="route" /> 
	  <input type="hidden"  id="country" name="country" />
	  
	  	 <input type="hidden"  id="administrative_area_level_1">	
	  
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>City</label>
                <input type="text" id="locality" name="city_order" class="form-control" placeholder="Your city"  value="{{$user_detail['city_order']}}" > 
              </div>
            </div> 
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Postal code</label>
                <input type="text" id="postal_code" name="pcode_order" class="form-control" placeholder=" Your postal code" number="number"  value="{{$user_detail['pcode_order']}}" maxlength="6"  >
              </div>
            </div>
          </div>-->
		  <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">&nbsp;</div>
            </div>
            <div class="col-md-6 col-sm-6">
			<!--<a class="btn_full " href="cart_3.html">Place Order</a>-->
      <!--deep-->
			<input type="button" id="submit_detail" name="submit_detail" value="@lang('translation.btnPlaceOrder')" class="btn_full"/>
            </div>
          </div>
        </div>
			
			<input type="hidden" id="otp_verify" name="otp_verify" value="0"  /> 
			<input type="hidden"  id="two_factor_auth" name="two_factor_auth"   value="@if(Auth::guest()) 0 @else {{ Auth::user()->checkout_verificaiton }}@endif" />
			
			
			<input type="hidden"  id="delivery_lat" name="delivery_lat" value="" />
			
			<input type="hidden"  id="delivery_lang" name="delivery_lang"  value="" />
		</form>
        <!-- End box_style_2 -->
        
        <!-- End box_style_3 --> 
      </div>
      <!-- End col-md-6 -->
      
      <div class="col-md-3" id="sidebar">
        <div class="theiaStickySidebar">
          <div id="cart_box">
            <h3 style="text-align: center;">@lang('translation.yourOrderSummary')<!--<i class="icon_cart_alt pull-right"></i>--></h3>
		
			<?php $cart_price = 0;$total_amt=0;?>
		@if(isset($cart) && (count($cart)))
			<div class="cart_scrllbr">
<div class="table table_summary">	
				
				 @foreach($cart as $item)
				<?php $total_amt= $cart_price = $cart_price+$item->price;?>
				  <div class="spcl-ordr-div">
				 <div class="crt_itm_cust1">
				  <p class="spcl-ordr-name"><strong>{{$item->qty}}  x</strong>
            <?php 
            if($lang=='ar'){
              $name_ar = DB::table('menu_category')
                        ->where('menu_category_id', '=' ,$item->id)
                        ->first();

              print_r($name_ar->menu_category_name_ar);
            }else{
               echo $item->name;
            }
            ?>

          </p>
                 <p class="spcl-ordr-priz"><strong class="pull-right">${{$item->price}}</strong></p>
                  <p class="spcl-ordr-think">{{@$item->options['instraction']}}</p>
                </div>
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $total_amt=$cart_price = $cart_price+$addon['price'];?>
					 <div class="crt_itm_cust23">
					  <p class="spcl-ordr-name">
              <?php 
              if($lang=='ar'){
                $addon_name_ar = DB::table('menu_category_addon')
                          ->where('addon_id', '=' ,$addon['id'])
                          ->first();

                print_r($addon_name_ar->addon_name_ar);
              }else{
                 echo $addon['name'];
              }
              ?>
            </p>
					  <p class="spcl-ordr-priz"><strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong></p>
					</div>
					 @endforeach
				 @endif 
					</div>	
				 @endforeach
				 
				 
 	 </div> 
	 
			
  </div>
@endif

			
		
   

            <hr>
			
			
			<div class="row" id="options_2">
			  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="control-group islamic2">
				
					<label class="control control--radio">{{$option}}
					<input type="radio" value="{{$option}}" class="rest_option" name="service_option" checked="checked">
						<div class="control__indicator"></div>
					</label>
				</div>  
				  	
				  
				  
				  
				  
              </div>
             
            </div>            <!-- Edn options 2 -->
            <hr>
            <table class="table table_summary">
              <tbody>
                <tr>
                  <td> @lang('translation.subtotal')
			
				  <span class="pull-right">${{$cart_price}}</span></td>
                </tr>
				
                <tr>
                  <td> @lang('translation.salesTax') <span class="pull-right">$<?php echo Session::get('cart_userinfo')['service_tax'];?></span></td>
                </tr>
				
                <tr>
                  <td> @lang('translation.discount')
				  <?php if(!empty(Session::get('cart_userinfo')['promo_discount_text'])){ echo '('.Session::get('cart_userinfo')['promo_discount_text'].')'; } ?>
				  
				   <span class="pull-right">$<?php echo Session::get('cart_userinfo')['promo_discount'];?></span></td>
                </tr>
				
                <tr>
                  <td> <?php //print_r(Session::get('cart_userinfo'));?>
				  </td>
                </tr>
               
				@if(!empty(Session::get('cart_userinfo')['delivery_fee']) && (Session::get('cart_userinfo')['delivery_fee']>0))
                <tr>
                  <td> @lang('translation.deliveryFee') <span class="pull-right">${{number_format(Session::get('cart_userinfo')['delivery_fee'],2)}}</span></td>
                </tr>
					@if((Session::get('cart_userinfo')['delivery_fee_min_remaing']>0))
					
					<tr id="del_fee_min">
					  <td> ${{number_format(Session::get('cart_userinfo')['delivery_fee_min'],2)}} min  <span class="pull-right">$<span id="delivery_fee_min_span">{{number_format(Session::get('cart_userinfo')['delivery_fee_min_remaing'],2)}}</span> remaining</span>
					  </td>
					</tr>				
					@endif
				
				@endif
        
				<?php	$total_amt = Session::get('cart_userinfo')['total_charge']; ?>
				
				
				
				<?php
				
				 if ((Session::get('cart_userinfo')['partical_payment_alow']>0))
				{
				?>
				
				<tr>
                	<td>
                     <hr />
                    <div  class="prsl-pay">
									 
						<label for="chkPassport">
							@lang('translation.partialPayment')  (<?php echo Session::get('cart_userinfo')['partical_percent'];?>%)
						</label>
					   
						<div id="dvPassport" >
						   <p>@lang('translation.partialPayoutAmt') (<?php echo Session::get('cart_userinfo')['partical_percent'];?>%) <span class="pull-right"><span id="payout_partial"><?php echo Session::get('cart_userinfo')['partical_amt'];?></span>$</span></p>
						   <p>@lang('translation.partialRemainingPayment')  <span class="pull-right"><span id="payout_partial_remaining"><?php echo Session::get('cart_userinfo')['partical_remaning_amt'];?></span>$</span></p>
						</div>
          
                    </div>
                    </td>
                </tr>
				
				<?php 
					}				
				?>
                <tr>
                  <td class="total"> @lang('translation.total') <span class="pull-right">${{number_format($total_amt,2)}}</span></td>
                </tr>
                
                  <?php
                   Session::get('cart_order_detail');
                  ?>
                  <!--<tr>
                  <td class="total"> Tip Amount
                    
                    <input type="text" name="tip" id="tip" readonly value="$<?php if(isset(Session::get('cart_order_detail') ['tip'])){echo Session::get('cart_order_detail') ['tip'];}else{ echo '';} ?>" class="form-control"/>    
          
                  </td>
                </tr>-->
                <tr>
                  
                  <td class="total"> @lang('translation.grandTotal')
                    <input type="hidden" value ="{{$total_amt}}" name="grand" class="grand">
                    <span class="pull-right grand_total_amt" id="total_amt">${{ number_format(Session::get('cart_order_detail') ['grand_total'],2)}}</span>

                  </td>
                </tr>
              </tbody>
            </table>
            <hr>
            <a class="btn_full_outline" href="{{url($rest_meta_url)}}"><i class="icon-right"></i> @lang('translation.btnAddOtherItems')</a> </div>
          <!-- End cart_box --> 
        </div>
      </div>
      <!-- End col-md-3 --> 
      
    </div>
    <!-- End row --> 
  </div>
  <!-- End container pin --> 
</div>
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')


<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

<div class="modal fade" id="message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.message')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  		<p id="alert_model_msg"></p>	
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirm_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.otpVerificationMessage')</h4>
      </div>
      <div class="modal-body popup-ctn">
	  		<p id="alert_confirm_msg">@lang('translation.otpMessage')</p>
			<form name="otp_verify_frm" id="otp_verify_frm" method="post">
	  		<p>	@lang('translation.enterOtp') </p>
			<p>	
				<input type="text" name="user_otp_number" id="user_otp_number" value="" required="required" number="number" maxlength="6" minlength="6"/>	
				<span id="otp_error_message" style="display:none"></span></p>	
							
			</p>	
			<p>
				<button type="button" class="get_conf_val btn-default" id="submit_otp">@lang('translation.btnSubmit')</button>
				 <button type="button" class="get_conf_val btn-default" id="resend_otp_checkout">@lang('translation.btnResendOtp')</button> 
			</p>
			</form>
				
			<!--<p>
				<button type="button" class="get_conf_val btn-default" data-val="1">OK</button>
			</p>-->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="otp_vefication_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body popup-ctn">
	  		<form name="otp_verify_frm" id="otp_verify_frm" method="post">
	  		<p>@lang('translation.enterOtp')</p>
			<p>	
				<input type="text" name="user_otp_number" id="user_otp_number" value="" required="required" number="number" maxlength="6" minlength="6"/>	
				<span id="otp_error_message" style="display:none"></span></p>	
							
			</p>	
			<p>
				<button type="button" class="btn-default" id="submit_otp">@lang('translation.btnSubmit')</button>
			</p>
			</form>
      </div>
    </div>
  </div>
</div>

<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script> 
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script> 
<script src="{{ url('/') }}/design/front/js/functions.js"></script> 
<script src="{{ url('/') }}/design/front/js/validate.js"></script> 
<!-- SPECIFIC SCRIPTS --> 
<script src="{{ url('/') }}/design/front/js/custom.js"></script> 
<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> 
<!--<script src="{{ url('/') }}/design/front/js/bootstrap-select.js" /></script>-->
<script>
    jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script> 
<script>

function getDates(start, end) {

    var datesArray = [];
    var startDate = new Date(start);
    while (startDate <= end) {
		var dd = startDate.getDate();
		var mm = parseInt(startDate.getMonth())+parseInt(1);
		var yy = startDate.getFullYear();
	
		var new_startDate = yy+'-'+mm+'-'+dd;
		datesArray.push(new_startDate);
        startDate.setDate(startDate.getDate() + 1);
    }
    return datesArray;
}

function findNextDisabledDateWithinMonth(date) {
    var currentDate = Number(date.split('-')[2]);
    var month = $('.ui-datepicker-title>.ui-datepicker-month').text();
    var year = $('.ui-datepicker-title>.ui-datepicker-year').text();
    var nextConsectiveDates = [];
    $.each($('.ui-state-disabled').find('.ui-state-default'), function (i, value) {
        var numericDate = +$(value).text();
        if (currentDate < numericDate) {
            nextConsectiveDates.push(numericDate);
        }
    });
    var nextDisabledDate = nextConsectiveDates[0] + "-" + month + "-" + year;
    return nextDisabledDate;
}

  $( function() {
    var start = '';
   var end = '';
  
<?php if(($rest_detail[0]->rest_holiday_from!='0000-00-00') && 
		 ($rest_detail[0]->rest_holiday_to!='0000-00-00')
		){?>
  var start = new Date('<?php echo $rest_detail[0]->rest_holiday_from;?>'),
    end = new Date('<?php echo $rest_detail[0]->rest_holiday_to;?>');
	
 <?php } ?>

   var dateToday = dc =  new Date();
   
   var Max_date =  new Date(dc.getFullYear() , (dc.getMonth() +1),(dc.getDate())); 
   
 <?php  if($today_holiday==1){?>
     var dt = new Date();
    dateToday = new Date(dt.getFullYear() , (dt.getMonth()),(dt.getDate()+1)); 
	
    Max_date = new Date(dt.getFullYear() , (dt.getMonth()+1),(dt.getDate()+1)); 
	
  <?php }?>
  
  /*function(event, ui) {
    var date = $(this).datepicker('getDate');
    var dayOfWeek = date.getUTCDay();
};*/
  
    $( "#datepicker" ).datepicker({
		 dateFormat: 'yy-mm-dd',
		 minDate: dateToday,
		 maxDate:Max_date,
		 onSelect: function(dateText, inst) {
		 
		 	$('#time_error_msg').hide();
				var date = $(this).val();
				var pickup_time = $("#pickup_time").val();
				var pickup_time_format = $("#pickup_time_format").val();
				var service_type = $(".rest_option").val();
			  var weekday = ["sun","mon","tue","wed","thu","fri","sat"];
        var a = new Date(dateText);
        var week_day = weekday[a.getDay()];
				frm_val = 'post_date='+date+'&pickup_time='+pickup_time+'&pickup_time_format='+pickup_time_format+'&service_type='+service_type+'&rest_id='+$('#rest_id').val();
				$.ajax({
						type: "POST",
						url: "{{url('/checkout/showTime')}}",
						data: frm_val,
					   dataType: 'json',
							success: function(msg) {
							//delivery_pick_time  
							
								$('#rest_open_status').val(msg.open_status);
							
							if((msg.today_holiday==0) && (msg.open_status==0) && (msg.holiday_cal==0))
							{
							
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==0) && ( (msg.today_holiday==1) || (msg.holiday_cal==1)))
							{
								
								$('#time_error_msg').html('Restaurant is closed on that date or day');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==1) && ( (msg.time_back==1)))
							{								
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
								$('#rest_open_status').val(0);
							}
								
								$('#delivery_pick_time').html(msg.select_time);
								$('#todayclosed').val(msg.today_holiday);
								$('#holiday').val(msg.holiday_cal);
								$('#back_time').val(msg.time_back);
								
								//alert(msg.select_time);
											
							}
						});

			}
		 
		});
	
	
  });
  
/*  ,
		 beforeShowDay: function(date){  			
			var between = getDates(start, end);
			var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
			return [ between.indexOf(string) == -1 ]
		},
		onSelect: function (date) {
				console.log(findNextDisabledDateWithinMonth(date));
		}*/
		
		
			
$(document).on('change', '#pickup_time', function(){
		$('#time_error_msg').hide();
				var date = $('#datepicker').val();
				var pickup_time = $("#pickup_time").val();
				var pickup_time_format = $("#pickup_time_format").val();
				var service_type = $(".rest_option").val();
				//alert(date);
				frm_val = 'post_date='+date+'&pickup_time='+pickup_time+'&pickup_time_format='+pickup_time_format+'&service_type='+service_type+'&rest_id='+$('#rest_id').val();
				$.ajax({
						type: "POST",
						url: "{{url('/checkout/showTime')}}",
						data: frm_val,
					   dataType: 'json',
							success: function(msg) {
							//delivery_pick_time  
							
							
							
							$('#rest_open_status').val(msg.open_status);
								
							
							if((msg.today_holiday==0) && (msg.open_status==0) && (msg.holiday_cal==0))
							{
							
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==0) && ( (msg.today_holiday==1) || (msg.holiday_cal==1)))
							{
								
								$('#time_error_msg').html('Restaurant is closed on that date or day');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==1) && ( (msg.time_back==1)))
							{								
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
								$('#rest_open_status').val(0);
							}
							
							
								$('#delivery_pick_time').html(msg.select_time);
								$('#todayclosed').val(msg.today_holiday);
								$('#holiday').val(msg.holiday_cal);
								$('#back_time').val(msg.time_back);
								
								//alert(msg.select_time);
											
							}
						});
});
		 
$(document).on('change', '#pickup_time_format', function(){
		$('#time_error_msg').hide();
				var date = $('#datepicker').val();
				var pickup_time = $("#pickup_time").val();
				var pickup_time_format = $("#pickup_time_format").val();
				var service_type = $(".rest_option").val();
				//alert(date);
				frm_val = 'post_date='+date+'&pickup_time='+pickup_time+'&pickup_time_format='+pickup_time_format+'&service_type='+service_type+'&rest_id='+$('#rest_id').val();
				$.ajax({
						type: "POST",
						url: "{{url('/checkout/showTime')}}",
						data: frm_val,
					   dataType: 'json',
							success: function(msg) {
							//delivery_pick_time  
							$('#rest_open_status').val(msg.open_status);
							
							if((msg.today_holiday==0) && (msg.open_status==0) && (msg.holiday_cal==0))
							{
							
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==0) && ( (msg.today_holiday==1) || (msg.holiday_cal==1)))
							{
								
								$('#time_error_msg').html('Restaurant is closed on that date or day');
								$('#time_error_msg').show();
							}
							else if((msg.open_status==1) && ( (msg.time_back==1)))
							{								
								$('#time_error_msg').html('Restaurant does not provide '+msg.service_type+' service this time.');
								$('#time_error_msg').show();
								$('#rest_open_status').val(0);
							}
							
							
								
								$('#delivery_pick_time').html(msg.select_time);
								$('#todayclosed').val(msg.today_holiday);
								$('#holiday').val(msg.holiday_cal);
								$('#back_time').val(msg.time_back);
								//alert(msg.select_time);
											
							}
						});
});

$(window).bind("load", function() {
    /* Get delivery time status */
    var service_type = $(".rest_option").val();
    var rest_id = $('#rest_id').val();

    var closed = $('#todayclosed').val();
    var holiday = $('#holiday').val();
    var open_status = $('#rest_open_status').val();
    
    if((closed !== 0 ) || (holiday !== 0) || (open_status !== 1) )
    {
        $.ajax({
          type: "POST",
          url: "{{url('/checkout/getDeliveryTimeStatus')}}",
          data: {service_type:service_type,rest_id:rest_id},
          dataType: 'json',
          async: false,
          success: function(msg) {
            
              console.log(msg);
              
              //msg.open_status
              //msg.today_holiday
              $('#todayclosed').val(msg.today_holiday);
              $('#holiday').val(msg.today_holiday);
              $('#rest_open_status').val(msg.open_status);
                    
          }
        });
    }
});
//window.onload = function () { alert("It's loaded!") }
		 
$(document).on('click', '#submit_detail', function(){

//var $target = $('html,body');

var elmnt = document.getElementById("checkout_user_info");

elmnt.scrollIntoView(true); // Top

/* $target.animate({scrollTop: $target.height()}, 1000);*/
var closed =$('#todayclosed').val();
var holiday =$('#holiday').val();
var open_status =$('#rest_open_status').val();
  
//alert($('#todayclosed').val());
//alert($('#holiday').val());

/*var select_option = '';

if($('.info_time').is(':checked'))
{

	select_option = $('.info_time:checked').val();
}

  alert(select_option);*/
  if((open_status==0))
  {
    
var detail_content = '<p> Now Restaurant is closed! open time is </p>';   
detail_content += '<div class="mang-timing-list"><div class="mang-timing">';   
detail_content += '<label>Monday </label>';   
detail_content += '<span><?php echo str_replace('_',' to ' , $rest_detail[0]->rest_mon)?></span>';   	
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Tuesday </label>';   
detail_content += '<span><?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_tues)?></span>';   
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Wednesday</label>';   
detail_content += '<span> <?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_wed)?></span>';   
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Thusday </label>';   
detail_content += '<span><?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_thus)?></span>';   
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Friday </label>';   
detail_content += '<span><?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_fri)?></span>';   
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Saturday</label>';   
detail_content += '<span> <?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_sat)?></span>';   
detail_content += '</div>';   
detail_content += '<div class="mang-timing">';   
detail_content += '<label>Sunday</label>';   
detail_content += '<span> <?php echo str_replace('_',' to ' ,$rest_detail[0]->rest_sun)?></span>';   
detail_content += '</div>';   
detail_content += '</div>';

  //	$('#alert_model_msg').html("Now Restaurant is closed!");			
  $('#alert_model_msg').html(detail_content);			
	$('#message_model').modal('show');
  }
  else if((open_status==1)&&(back_time==1))
  {
  	$('#alert_model_msg').html("Restaurant does not provide Pick-up or Delivery service this time.");			
	$('#message_model').modal('show');
  }
  else if((closed==1)&&(holiday==0))
  {
  	$('#alert_model_msg').html("Restaurant is closed on "+$('#datepicker').val());			
	$('#message_model').modal('show');
  }
  else if((closed==0)&&(holiday==1))
  {
  	$('#alert_model_msg').html("Restaurant is closed until <?php echo $rest_detail[0]->rest_holiday_to;?>");			
	$('#message_model').modal('show');
  }
  else if((closed==1)&&(holiday==1))
  {
  	$('#alert_model_msg').html("Restaurant is closed until <?php echo $rest_detail[0]->rest_holiday_to;?>");			
	$('#message_model').modal('show');
  }
  else if((closed==0)&&(holiday==0) && (open_status==1) )
  {
  	var checkout_contact =$('#tel_order').val();
	var checkout_email =$('#email_booking_2').val();
  
  	 
	if((checkout_contact=='') ||  (checkout_email=='') )
	{		
		$('#alert_model_msg').html("Email ID and  Contact Number is required");			
		$('#message_model').modal('show');
	}
	else
	{
		var form = $("#checkout_user_info");
		form.validate();
		var valid =	form.valid();
		
			if(valid)
			{
								
				var user_type = $("#user_id").val();
				var two_factor_auth = $("#two_factor_auth").val();
				var otp_verify = $("#otp_verify").val();
				var checkout_contact_type = $("#order_mob_type").val();
				
				
				if(
					((user_type>0) && (two_factor_auth==1) && (otp_verify==0)) || 
					((user_type==0) && (otp_verify==0))					
				)
				{
					frm_val = 'checkout_contact_type='+checkout_contact_type+'&checkout_contact='+checkout_contact+'&checkout_email='+checkout_email+'&user_type='+user_type;
					
					$.ajax({
							type: "POST",
							url: "{{url('/checkout/send_otp_verify')}}", 
							data: frm_val,
							dataType: 'json',
							success: function(msg) { 

              $("#checkout_user_info").submit(); 
							
							// $("#alert_confirm_msg").html(msg.sucess_message);
			//$('#confirm_model').modal('show');
							 
							/* $(document).on('click', '.get_conf_val', function(){
							 	confrim = $(this).attr('data-val');
								//alert(confrim);
								if(confrim)	
								{
							 		 $('#confirm_model').modal('hide');
							 		 $('#otp_vefication_model').modal('show');
								}
								
							 });	*/
								
							}
					
					});
				}
				else
				{
					$("#checkout_user_info").submit();
				}				
			}
	}	
  
  	/*var form = $("#checkout_user_info");
	form.validate();
	var valid =	form.valid();
	
		if(valid)
		{
			$("#checkout_user_info").submit();
		}*/
  }
});	

function del_addressm(delid){

          frm_val = 'delivery_add_del='+delid;   

          var delhid = '#udeladd'+delid;   
          
          $.ajax({
              type: "POST",
              url: "{{url('/checkout/delete_delivery_address')}}",
              data: frm_val,
              dataType: 'json',
              success: function(msg) { 
              
             $(delhid).hide();
             
              }
          
          });         

}

$(document).on('change', '.del_address_new', function(){

if($('.del_address_new').is(':checked'))
{
  if($('.del_address_new:checked').val()=='oldaddress1')
  {

    var daddress = $('#del_address1').val();
    $('#delivery_add1').val(daddress);

    var city = $('#city1').val();
    $('#delivery_surbur').val(city);

    var zipcode = $('#zipcode1').val();
    $('#delivery_postcode').val(zipcode);
    
  }
  else if($('.del_address_new:checked').val()=='oldaddress2')
  {
     var daddress = $('#del_address2').val();
     $('#delivery_add1').val(daddress);

     var city = $('#city2').val();
     $('#delivery_surbur').val(city);

     var zipcode = $('#zipcode2').val();
     $('#delivery_postcode').val(zipcode);
    
  }else if($('.del_address_new:checked').val()=='oldaddress3')
  {
     var daddress = $('#del_address3').val();
     $('#delivery_add3').val(daddress);

     var city = $('#city3').val();
     $('#delivery_surbur').val(city);

     var zipcode = $('#zipcode3').val();
     $('#delivery_postcode').val(zipcode);
    
  }else if($('.del_address_new:checked').val()=='oldaddress4')
  {
     var daddress = $('#del_address4').val();
     $('#delivery_add4').val(daddress);

     var city = $('#city4').val();
     $('#delivery_surbur').val(city);

     var zipcode = $('#zipcode4').val();
     $('#delivery_postcode').val(zipcode);
    
  }else{

   $('#delivery_add1').val('');
   $('#delivery_surbur').val('');
   $('#delivery_postcode').val('');

  }
}
}); 

$(document).on('change', '.info_time', function(){

if($('.info_time').is(':checked'))
{
	if($('.info_time:checked').val()=='later')
	{
	   $(".time-date-ctn").show();
	}
	else
	{
	   $(".time-date-ctn").hide();
	}
}
});	
 $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

$(document).on('click', '#submit_otp', function(){

$('#otp_error_message').hide();

	var form = $("#otp_verify_frm");
	form.validate();
	var valid =	form.valid();	
	if(valid)
	{				
	
		var user_type = $("#user_id").val();
		var two_factor_auth = $("#two_factor_auth").val();
		var otp_verify = $("#otp_verify").val();
		var checkout_contact_type = $("#order_mob_type").val();
		var user_otp_number = $("#user_otp_number").val();
		var checkout_contact =$('#tel_order').val();
		var checkout_email =$('#email_booking_2').val();
		
		frm_val = 'checkout_contact_type='+checkout_contact_type+'&checkout_contact='+checkout_contact+'&checkout_email='+checkout_email+'&user_type='+user_type+'&user_otp_number='+user_otp_number;
					
		$.ajax({
					type: "POST",
					url: "{{url('/checkout/check_submit_otp')}}",
					data: frm_val,
					dataType: 'json',
					success: function(msg) {
					
						if(msg.status==0)
						{
							
							$('#otp_error_message').html(msg.sucess_message);								
							$('#otp_error_message').show();
						}
						else
						{
							 $("#checkout_user_info").submit();
							 $('#otp_vefication_model').modal('hide');
						}
					}
			});	
	}
		

});


$(document).on('click', '#resend_otp_checkout', function(){

 $('#confirm_model').modal('hide');
 $("#ajax_favorite_loddder").show();

  var checkout_contact =$('#tel_order').val();
  var checkout_email =$('#email_booking_2').val();

  var user_type = $("#user_id").val();
        var two_factor_auth = $("#two_factor_auth").val();
        var otp_verify = $("#otp_verify").val();
        var checkout_contact_type = $("#order_mob_type").val();
        
        

          frm_val = 'checkout_contact_type='+checkout_contact_type+'&checkout_contact='+checkout_contact+'&checkout_email='+checkout_email+'&user_type='+user_type;         
          //alert(frm_val); die;
          $.ajax({
              type: "POST",
              url: "{{url('/checkout/send_otp_verify')}}",
              data: frm_val,
              dataType: 'json',
              success: function(msg) {
              
              $("#ajax_favorite_loddder").hide();
              // $("#alert_confirm_msg").html(msg.sucess_message);
               $('#confirm_model').modal('show');
               
                
              }
          
          });

});

</script>

		 

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDah_MDDXAO6_bLo7SMTgkdepS3gQw9jXE&libraries=places"></script>
  <script> 
  
function addressAndzipcode() { 

 var input = document.getElementById('delivery_add1');

 var options = {
   types: ['address']/*,
   componentRestrictions: {
     country: 'use'
   }*/
 };

 autocomplete = new google.maps.places.Autocomplete(input);
 google.maps.event.addListener(autocomplete, 'place_changed', function() {
   var place = autocomplete.getPlace();
   document.getElementById('delivery_lat').value = place.geometry.location.lat();
   document.getElementById('delivery_lang').value = place.geometry.location.lng();
   for (var i = 0; i < place.address_components.length; i++) {
     for (var j = 0; j < place.address_components[i].types.length; j++) {
      if (place.address_components[i].types[j] == "postal_code") {
         $('#delivery_postcode').val(place.address_components[i].long_name);
       }
        if (place.address_components[i].types[j] == "locality") {
         $('#delivery_surbur').val(place.address_components[i].long_name);
       }
     }
   }
   	
   var rest_id = $('#rest_id').val();
   var delivery_lat = place.geometry.location.lat();
   var delivery_lang =  place.geometry.location.lng();

   $('#order_lat').val(delivery_lat);
   $('#order_long').val(delivery_lang);
   
   frm_val = 'rest_id='+rest_id+'&delivery_lat='+delivery_lat+'&delivery_lang='+delivery_lang;
					
	$.ajax({
			type: "POST",
			url: "{{url('/checkout/check_delivery_address')}}",
			data: frm_val,
			dataType: 'json',
			success: function(msg) {
			
				 if(msg.status==0)
				 {
				 	$('#alert_model_msg').html(msg.sucess_message);			
					$('#message_model').modal('show');			
					$('#delivery_add1').val('');		
					$('#delivery_postcode').val('');		
					$('#delivery_surbur').val('');
				}			
			}	
	});
   
 }); 
 /**************************/ 
 var input_1 = document.getElementById('address_order');
 var options = {
   types: ['address']
 /* , componentRestrictions: {
     country: 'NJ'
   }*/
 };
 autocomplete_1 = new google.maps.places.Autocomplete(input_1);
 google.maps.event.addListener(autocomplete_1, 'place_changed', function() {
   var place_1 = autocomplete_1.getPlace();
   for (var i = 0; i < place_1.address_components.length; i++) {
     for (var j = 0; j < place_1.address_components[i].types.length; j++) {
       if (place_1.address_components[i].types[j] == "postal_code") {
         $('#postal_code').val(place_1.address_components[i].long_name);
       }
        if (place_1.address_components[i].types[j] == "locality") {
         $('#locality').val(place_1.address_components[i].long_name);
       }
     }
   }
 });
 
}

google.maps.event.addDomListener(window, "load", addressAndzipcode);

</script>

@stop