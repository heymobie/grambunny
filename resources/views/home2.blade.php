@extends('layouts.app')

@section("other_css")

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/owl.theme.css">

@stop

@section('content')

<?php $lang = Session::get('locale'); ?>

<?php if($lang=='ar'){ ?>

<style type="text/css">

.owl-wrapper{ width: 1366px !important; }

</style>

<?php } ?>

<section class="header-video fill">

  <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner" role="listbox">

      <div class="item active"> <img src="{{ url('/') }}/design/front/img/slider1.jpg" alt="Chania"></div>

      <div class="item"> <img src="{{ url('/') }}/design/front/img/slider2.jpg" alt="Chania"></div>

      <div class="item"> <img src="{{ url('/') }}/design/front/img/slider3.jpg" alt="Flower"></div>

      <div class="item"> <img src="{{ url('/') }}/design/front/img/slider4.jpg" alt="Flower"></div>

      <div class="item"> <img src="{{ url('/') }}/design/front/img/slider5.jpg" alt="Flower"></div>

      <div class="item"> <img src="{{ url('/') }}/design/front/img/slider6.jpg" alt="Flower"></div>

    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="fa fa-angle-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="fa fa-angle-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>

    <ol class="carousel-indicators">

      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

      <li data-target="#myCarousel" data-slide-to="1"></li>

      <li data-target="#myCarousel" data-slide-to="2"></li>

    </ol>

  </div>

  <!--<video autoplay loop id="video-background" muted plays-inline>

    <source src="video/intro.mp4" type="video/mp4">

  </video>-->

  <div id="count" class="" style="display:none;">

    <ul>

      <li><span class="number">{{$total_restaurant}} </span> Restaurant</li>

      <li><span class="number">{{$total_served}}</span> People Served</li>

      <li><span class="number">{{$total_users}}</span> Registered Users</li>

    </ul>

  </div>

</section>

<section class="banner-sec wow ">

  <form method="post" action="{{url('/restaurantslisting')}}" id="search_form" name="search_form">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <?php //echo "<pre>"; print_r($ip_location);die;?>

                <input type="hidden" id="ip_street_number" name="ip_street_number" value="" />

      <input type="hidden" id="ip_city" name="ip_city" value="<?php if(isset($ip_location->city)){echo $ip_location->city;}else{}?>" />

      <input type="hidden" id="ip_postal_code"  name="ip_postal_code" value="<?php if(isset($ip_location->zip)){echo $ip_location->zip;}else{}?>" />

      <input type="hidden" id="ip_country" name="ip_country" value="<?php if(isset($ip_location->country_code)){echo $ip_location->country_code;}else{}?>"/>

      <input type="hidden" name="ip_lat"  id="ip_lat" value="<?php if(isset($ip_location->latitude)){echo $ip_location->latitude;}else{}?>"/>

      <input type="hidden" name="ip_lng" id="ip_lng"  value="<?php if(isset($ip_location->longitude)){echo $ip_location->longitude;}else{}?>">

                <input class="hidden" id="street_number" name="street_number"></input>

                <input type="hidden" class="field" id="route" name="route" />

                <input class="hidden" id="locality" name="locality" />

                <input class="hidden" id="administrative_area_level_1" name="level" />

                <input class="hidden" id="postal_code"  name="postal_code" />

                <input class="hidden" id="country" name="country" />

                <input name="lat" class="MapLat" id="lat" value="" type="hidden" />

                <input name="lng" class="MapLon" id="lng" value="" type="hidden">

                <div class="home-content container ">

        <h1 class="hero-title"><?php /*Order Food from Local Restaurants*/?>

        <?php

                    $lang = Session::get('locale');

                    if($lang == 'ar'){

                      echo $home_content[4]->home_content_ar;

                    } else {

                      echo $home_content[4]->home_content;

                    }            

        ?>

                  </h1>

                  <span class="white-divider"><img src="{{ url('/') }}/design/front/img/white-line.png"></span>

                  <home wow fadeInUp><div class="home-search">

                      <div class="home-filter wow fadeInUp">

                          <div class="type-location-container" ng-class="{'hide-locate-me': vm.hideLocateMe}" id="locationField">

              <?php /*?><input  type="text" class="type-location ng-pristine ng-untouched ng-valid ng-empty" name="location" placeholder="Type  zip code / address"  id="search_surbur" autocomplete="" ><?php */?>

                              <input class="type-location ng-pristine ng-untouched ng-valid ng-empty" id="autocomplete" placeholder="@lang('translation.enterAddress')" onFocus="geolocate()" type="text" name="location" />

                              <span class="search-icon">

                                  <i class="fa fa-map-marker"></i>

                              </span>

                              <div class="clear-input-container">

                                  <span class="clear-input"></span>

                              </div>

                              <div class="show-restaurants">

                                  <span ng-switch-default=""><a href="#" id="search_home">@lang('translation.showrestaurants')</a></span>

                              </div>

                              <div class="locate-me">

                                  <span class="locate-me-divider"></span>

                                  <span class="locate-me-icon">

                                     <i class="fa fa-crosshairs"></i>

                                 </span>

                                 <span class="locate-me-text"><a href="#" id="search_nearme">@lang('translation.locatenearme')</a></span>

                             </div>

                         </div>

                     </div>

                 </div>

               </home>

               <div class="container">

                  <div class=" select-ctgry"style="display:none;">

            <?php foreach($cuisine_list as $clist){ ?>

            <label for="default<?php echo $clist->cuisine_id;?>" class="btn">

              <?php if($lang=='ar'){ echo $clist->cuisine_name_ar; }else{ echo $clist->cuisine_name; } ?>

              <input type="checkbox" id="default<?php echo $clist->cuisine_id;?>" class="badgebox food_type" name="cuisine[]" value="<?php echo $clist->cuisine_id;?>" ><span class="badge">&check;</span></label>

              <?php } ?>

                        <div class=" select-ctgry-hed">

                            <div class=" select-ctgry-fltr">

                               <label for="default-13" class="btn">Select All <input type="checkbox" id="default-13" class="badgebox check_all" ><span class="badge">&check;</span></label>

                               <label for="default-14" class="btn">Deselect All <input type="checkbox" id="default-14" class="badgebox check_none" checked="checked" ><span class="badge">&check;</span></label>

                           </div>

                           <div class=" select-ctgry-fltr">

                              <label for="default104" class="btn ">Vegan <input type="checkbox" name="rest_type[]" value="1" id="default104" class="badgebox" ><span class="badge">&check;</span></label>

                              <label for="default100" class="btn ">Kosher <input type="checkbox" name="rest_type[]" value="2" id="default100" class="badgebox" ><span class="badge">&check;</span></label>

                              <label for="default200" class="btn ">Halal <input type="checkbox" name="rest_type[]" value="3"  id="default200" class="badgebox" ><span class="badge">&check;</span></label>

                              <label for="default300" class="btn">Vegetarian <input type="checkbox" name="rest_type[]" value="4"  id="default300" class="badgebox" ><span class="badge">&check;</span></label>

                          </div>

                      </div>

                  </div>

              </div>

          </div>

      </form>

    </section>

    <section class="dark-section wow fadeInUp" >

        <div class="container">

            <div class="row">

                <div class="col-md-6 col-sm-12">

                   <aside class="col_r wow fadeInRight">

                      <articl class="con_tren">

                          <h2 class="c_heading">@lang('translation.trending') </h2>

                <?php

                //print_r($trending_list);

                          if(!empty($trending_list))

                          {

                ?>

                            <div class="for_article_home">

                  <?php

                                foreach($trending_list as $tlist)

                                {

                                 $rest_detail =  DB::table('restaurant')->selectRaw('*')->where('rest_id','=',$tlist->rest_id)->get();

                  ?>

                                 <article class="trend">

                                    <aside class="trend_thumb">

                                       @if(!empty($rest_detail[0]->rest_logo))

                                       <img src="{{ url('/') }}/uploads/reataurant/{{$rest_detail[0]->rest_logo}}" alt="">

                                       @else

                                       <img src="{{ url('/') }}/design/front/img/logo.png" alt="">

                                       @endif

                                   </aside>

                                   <table class="trend_info" cellpadding="0" cellspacing="0" border="0">

                                       <tbody>

                                           <tr>

                        <td height="72" valign="middle"><article class="trend_rate"><?php  echo $tlist->total_orders; ?></article>

                                              <h2>

                        <a href="{{url('/'.strtolower(trim(str_replace(' ','_',@$rest_detail[0]->rest_suburb))).'/'.@$rest_detail[0]->rest_metatag)}}" ><?php  echo @$rest_detail[0]->rest_name; ?></a>

                                              </h2>

                        <!-- <span>ET Food Voyage</span>-->

                                          </td>

                                      </tr>

                                  </tbody>

                              </table>

                          </article>

              <?php

                        }

              ?>

                    </div>

            <?php

                  }

            ?>

              </articl>

          </aside>

      </div>

      <div class="col-md-6 col-sm-12">

          <div class="dwnld-app-sec">

              <div class="column-form wow fadeInRight">

            <h2><?php

                  if(($home_content_api) && (!empty($home_content_api[0]->home_title)))

                  {

                    echo $home_content_api[0]->home_title;

                  }

            ?></h2>

              </div>

          <?php

              if(($home_content_api) && (!empty($home_content_api[0]->home_content)))

              {

                echo "<div class='grey-hed '>".$home_content_api[0]->home_content."</div>";

              }

          ?>

              <div class="left-mbl-img wow fadeInLeft">

                  <img src="{{ url('/') }}/design/front/img/feed-app.jpg">

              </div>

              <div class="right-mbl-img wow fadeInLeft">

            <!-- <div class="app-verify"  id="app-verify_mob">

                      <form name="send_app" id="send_app_mob">

                         <input type="hidden" name="app_mob_type" value="1">

                          <input type="text" name="app_mob" class="phone_nu" placeholder="@lang('translation.phoneNumber')" required="required" number="number">

                          <input type="button" id="submit_mob" value="@lang('translation.btnSendMeLink')" name="submit_mob" class="ap-submit">

                      </form>

            </div> -->

            <!-- <span class="center-divi-line">@lang('translation.or')</span> -->

                  <div class="app-verify" id="app-verify_email">

                      <form name="send_app" id="send_app_email">

                          <input type="email" class="email_app" name="app_email" placeholder="@lang('translation.emailAddress')" required="required">

                          <input type="button" id="submit_email" value="@lang('translation.btnSendMeLink')" name="submit_email" class="ap-submit">

                      </form>

                  </div>

                  <div class="store-links col-l-10">

                      <ul>

                          <li>

                              <a class="pr20" target="_blank" href="https://i.diawi.com/pFcDYN">

                                  <img src="{{ url('/') }}/design/front/img/i-phone.png" height="40">

                              </a>

                          </li>

                          <li>

                              <a target="_blank" href="https://drive.google.com/open?id=1uGENUOW3goo0CAxbHGbnkHI0oZhdZSzh">

                                  <img src="{{ url('/') }}/design/front/img/android-ap.png" height="40">

                              </a>

                          </li>

                      </ul>

                  </div>

              </div>

          </div>

      </div>

    </div>

  </div>

</section>

<section>

    <div class="icon-sec">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="ful-pg-heading">

                        <div class="ful-pg-hed">

                            <h2>@lang('translation.quickSearche')</h2>

                            <span class="white-divider"><img src="{{ url('/') }}/design/front/img/grey-line.png"></span>

                        </div>

                    </div>

                </div>

                <div class="col-md-12">

                   <div class="icon-menu">

                      <div class="customNavigation outer-hndler">

                          <a class="btn prev"><i class="fa fa-angle-left"></i></a>

                          <a class="btn next"><i class="fa fa-angle-right"></i></a>

                      </div>

                      <div id="owl-demo" class="owl-carousel">

              <?php //print_r($cuisine_list) ;

                         if($cuisine_list){

              foreach($cuisine_list as $clist){ ?>

              <?php  if($lang=='ar'){ ?>

                            <div class="item">

                <a href="<?php echo url('/restaurant_listing/cuisine/'.$clist->cuisine_name) ;?>">

                  <?php if(!empty($clist->cuisine_image)){?>

                  <img src="{{ url('/') }}/uploads/cuisine/<?php echo $clist->cuisine_image;?>">

                  <?php }else{?>

                                   <img src="{{ url('/') }}/design/front/img/item-1.png">

                  <?php }?>

                  <span><?php  echo $clist->cuisine_name_ar; ?></span></a>

                             </div>

                <?php }else{ ?>

                              <div class="item">

                  <a href="<?php echo url('/restaurant_listing/cuisine/'.$clist->cuisine_name) ;?>">

                    <?php if(!empty($clist->cuisine_image)){?>

                    <img src="{{ url('/') }}/uploads/cuisine/<?php echo $clist->cuisine_image;?>">

                    <?php }else{?>

                                     <img src="{{ url('/') }}/design/front/img/item-1.png">

                    <?php }?>

                    <span><?php  echo $clist->cuisine_name; ?></span></a>

                               </div>

                  <?php } }

                  } ?>

                         </div>

                     </div>

                 </div>

            <?php /*?><div class="col-md-12">

                          <div class="icon-menu">

                <?php foreach($cuisine_list as $clist){ ?>

                <a href="<?php echo url('/restaurant_listing/cuisine/'.$clist->cuisine_name) ;?>">

                  <?php if(!empty($clist->cuisine_image)){?>

                  <img src="{{ url('/') }}/uploads/cuisine/<?php echo $clist->cuisine_image;?>">

                  <?php }else{?>

                   <img src="{{ url('/') }}/design/front/img/item-1.png">

                  <?php }?>

                  <span><?php echo $clist->cuisine_name;?></span></a>

                  <?php } ?>

                              </div>

              </div><?php */?>

                        </div>

                    </div>

                </div>

            </section>

      <!--<section class="we-carr-sec wow fadeInUp">

                <div class="container">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="ful-pg-heading">

                                <div class="ful-pg-hed">

                                    <h2>@lang('translation.howTo')</h2>

                                    <span class="white-divider"><img src="{{ url('/') }}/design/front/img/grey-line.png"></span>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

      </section>-->

      <!--<section id="welcome-section-mover" class="wow fadeInUp">

                <div class="container">

                    <div class="row">

            <?php

                        if($home_content)

                        {

                         foreach($home_content as $hlist)

                         {

                          if($hlist->home_id==1)

                          {

            ?>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                <div class="single-welcome-mover thm-image-hover hvr-float-shadow wow fadeInLeft" data-wow-delay="400ms">

                                  <figure class="img-holder hovereffect"> <img height="73px" width="73px" src="{{ url('/') }}/design/front/img/food-rdr1.png" alt=""> </figure>

              <div class="content"> <a  id="view_more_<?php echo $hlist->home_id;?>" data-homeid="<?php echo $hlist->home_id;?>">

                <h2><?php echo $hlist->home_title;?> </h2>

                                 </a>

                                 <p>

                <?php

                                     $string = $hlist->home_content;

                                     if (strlen($string) > 100) {

                // truncate string

                                       $stringCut = substr($string, 0, 100);

                // make sure it ends in a word so assassinate doesn't become ass...

                                       $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a  id="view_more_'.$hlist->home_id.'" data-homeid="'.$hlist->home_id.'">Read More</a>';

                                     }

              echo $string;?></p>

                               </div>

                           </div>

                       </div>

        <?php

                     }

                     if($hlist->home_id==2)

                     {

        ?>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                          <div class="single-welcome-mover thm-image-hover hvr-float-shadow wow fadeInUp" >

                            <figure class="img-holder hovereffect"> <img height="73px" width="73px" src="{{ url('/') }}/design/front/img/food-rdr2.png" alt=""> </figure>

          <div class="content"> <a id="view_more_<?php echo $hlist->home_id;?>" data-homeid="<?php echo $hlist->home_id;?>">

            <h2><?php echo $hlist->home_title;?></h2>

                           </a>

                           <p>

            <?php

                               $string = $hlist->home_content;

                               if (strlen($string) > 100) {

            // truncate string

                                 $stringCut = substr($string, 0, 100);

            // make sure it ends in a word so assassinate doesn't become ass...

                                 $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a  id="view_more_'.$hlist->home_id.'" data-homeid="'.$hlist->home_id.'">Read More</a>';

                               }

          echo $string;?></p>

                         </div>

                     </div>

                 </div>

    <?php

               }

               if($hlist->home_id==3)

               {

    ?>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                    <div class="single-welcome-mover thm-image-hover hvr-float-shadow wow fadeInRight " data-wow-delay="800ms">

                      <figure class="img-holder"> <img height="73px" width="73px" src="{{ url('/') }}/design/front/img/food-rdr3.png" alt=""> </figure>

      <div class="content"> <a  id="view_more_<?php echo $hlist->home_id;?>" data-homeid="<?php echo $hlist->home_id;?>">

        <h2><?php echo $hlist->home_title;?> </h2>

                     </a>

                     <p>

        <?php

                         $string = $hlist->home_content;

                         if (strlen($string) > 100) {

        // truncate string

                           $stringCut = substr($string, 0, 100);

        // make sure it ends in a word so assassinate doesn't become ass...

                           $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a id="view_more_'.$hlist->home_id.'" data-homeid="'.$hlist->home_id.'">Read More</a>';

                         }

        echo $string;?>

                     </p>

                 </div>

             </div>

         </div>

<?php

       }

     }

   }

?>

 </div>

</div>

</section>-->

<section id="testimonials">

 <div class="container">

  <div class="row">

    <div class="col-md-12">

        <div class="ful-pg-heading wow fadeInUp">

            <div class="ful-pg-hed">

                <h2>@lang('translation.peopleSays')</h2>

                <span class="white-divider"><img src="{{ url('/') }}/design/front/img/white-line.png"></span>

            </div>

        </div>

    </div>

    <div>

        <div id="demo">

            <div id="testimonils-slide" class="owl-carousel">

      <?php

                if($testimonial_list)

                {

                 foreach($testimonial_list as $tlist)

                 {

      ?>

                  <div class="item testo-slid">

                      <div class="row">

                         <div class="col-md-12 col-xs-12 col-sm-12">

                             <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">

              <?php if(!empty($tlist->image)){?>

              <img class="img-responsive media-object" src="{{ url('/') }}/uploads/testimonial/<?php echo $tlist->image;?>" alt="testimonials_img1" title="testimonials_img1">

              <?php }else{?>

                                  <img class="img-responsive media-object" src="{{ url('/') }}/uploads/testimonial/avtar.png" alt="testimonials_img1" title="testimonials_img1">

              <?php }

              ?>

                            </div>

                            <div class="col-md-8 col-xs-12 col-sm-8 col-lg-8 ">

                                <figcaption class="caption">

                                  <div>

                <h6><?php echo $tlist->name;?></h6>

                <!--<h5>Manager</h5>-->

                <?php echo $tlist->content;?>

                                 </div>

                               </figcaption>

                           </div>

                       </div>

                   </div>

               </div>

      <?php

             }

           }

      ?>

      <?php /*?><div class="item testo-slid">

                          <div class="row">

                              <div class="col-md-12 col-xs-12 col-sm-12">

                                  <div class="col-md-8 col-xs-12 col-sm-8 col-lg-8">

                                      <figcaption class="caption">

                                      <div>

                                          <h6>Lorem Ipsum</h6>

                                          <h5>Manager</h5>

                                          Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor

                                          in subascidi dunt ut

                                      </div>

                                      </figcaption>

                                  </div>

                                  <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">

                                      <img class="img-responsive media-object" src="{{ url('/') }}/design/front/img/testimonial1.jpg" alt="testimonials_img2" title="testimonials_img2">

                                  </div>

                              </div>

                          </div>

                      </div>

                      <div class="item testo-slid">

                          <div class="row">

                              <div class="col-md-12 col-xs-12 col-sm-12">

                                  <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">

                                        <img class="img-responsive media-object" src="{{ url('/') }}/design/front/img/testimonial1.jpg" alt="testimonials_img1" title="testimonials_img1">

                                  </div>

                                  <div class="col-md-8 col-xs-12 col-sm-8 col-lg-8 ">

                                      <figcaption class="caption">

                                        <div>

                                            <h6>Lorem Ipsum</h6>

                                            <h5>Manager</h5>

                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor

                                            in subascidi dunt ut

                                        </div>

                                      </figcaption>

                                  </div>

                              </div>

                          </div>

                      </div>

                      <div class="item testo-slid">

                          <div class="row">

                              <div class="col-md-12 col-xs-12 col-sm-12">

                                  <div class="col-md-8 col-xs-12 col-sm-8 col-lg-8">

                                      <figcaption class="caption">

                                      <div>

                                          <h6>Lorem Ipsum</h6>

                                          <h5>Manager</h5>

                                          Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor

                                          in subascidi dunt ut

                                      </div>

                                      </figcaption>

                                  </div>

                                  <div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">

                                      <img class="img-responsive media-object" src="{{ url('/') }}/design/front/img/testimonial1.jpg" alt="testimonials_img2" title="testimonials_img2">

                                  </div>

                              </div>

                          </div>

      </div>                <?php */?>

                    </div>

                </div>

            </div>

          </div>

        </div>

      </section>

      <section class="spcl-offer wow fadeInUp">

        <div class="container">

          <div class="row">

            <div class="col-md-4 col-sm-12">

                <div class="grb-points"><h2>@lang('translation.suggestRestaurant')</h2>

                    <div class="grb-points-dtl">

                        <span>@lang('translation.earnReward')</span>

      <!-- <span>@lang('translation.useFavorite')</span> -->

                        <button class="spcl-btn"  id="show_suggest_restaurant">@lang('translation.suggestFavoriteRestaurant')</button>

                    </div>

                </div>

            </div>

            <div class="col-md-4 col-sm-12">

                <div class="grb-points"><h2>@lang('translation.restaurantsOwners')</h2>

                    <div class="grb-points-dtl">

                        <span>@lang('translation.loyalCustomers')</span>

      <!-- <span>@lang('translation.useatyourfavoriterestaurants')</span> -->

      <?php

                        if(!Auth::guest())

                        {

      ?>

      <!-- <button class="spcl-btn" id="show_owners_form">Sign Up my Restaurant</button>-->

      <?php

                        }

                        else

                        {

      ?>

                          <button class="spcl-btn" id="show_owners_form">@lang('translation.SignUpmyRestaurant')</button>

      <?php

                        }

      ?>

                    </div>

                </div>

            </div>

            <div class="col-md-4 col-sm-12">

                <div class="grb-points"><h2>@lang('translation.specialOffers')</h2>

                   <div class="grb-points-dtl" id="coupons_msg_div">

                      <form name="coupon_form" id="coupon_form" method="post">

                          {{ csrf_field() }}

                          <span><input type="email" name="coupon_email" id="coupon_email" placeholder="@lang('translation.emailAddress')" required="required"></span>

        <!--<span class="col-sm-6"><input type="text" name="coupon_pincode" id="coupon_pincode" placeholder="@lang('translation.zipCode')" required="required" number="number" maxlength="5"></span>-->

                          <span class="col-sm-6"><button class="spcl-btn" id="send_coupon" type="button"> @lang('translation.btnSendMe')</button></span>

                      </form>

                  </div>

              </div>

          </div>

        </div>

      </div>

    </section>

    <div id="map"></div>

<?php

    if($home_content)

    {

     foreach($home_content as $hlist)

     {

?>

<div class="modal fade" id="view_detail_home_<?php echo $hlist->home_id;?>" role="dialog">

         <div class="modal-dialog">

<!-- Modal content-->

          <div class="modal-content">

            <div class="modal-header">

               <button type="button" class="close" data-dismiss="modal">&times;</button>

  <h4><?php echo $hlist->home_title;?> </h4>

           </div>

           <div class="modal-body popup-ctn">

  <?php echo $hlist->home_content;?>

               <p>

               </p>

           </div>

         </div>

       </div>

     </div>

<?php

   }

 }

?>

<style>

 #ajax_favorite_loddder {

  position: fixed;

  top: 0;

  left: 0;

  width: 100%;

  height: 100%;

  background:rgba(27, 26, 26, 0.48);

  z-index: 1001;

}

#ajax_favorite_loddder img {

  top: 50%;

  left: 46.5%;

  position: absolute;

}

.footer-wrapper {

  float: left;

  width: 100%;

  /*display: none;*/

}

#addons-modal.modal {

z-index: 999;

}

.modal-backdrop {

z-index: 998 !important;

}

#add_sucess_msg {

  position: fixed;

  width: auto;

  height: auto;

  background: rgba(210, 71, 30, 0.89);

  z-index: 99999;

  color: #f5f0e3 !important;

  margin: 0 auto;

  padding: 21px;

  font-size: 20px;

  top: 50%;

  margin: 0 auto;

  text-align: center;

  left: 50%;

  transform: translate(-50%, 0);

}

</style>

<div id="ajax_favorite_loddder" style="display:none;">

<div align="center" style="vertical-align:middle;">

<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

</div>

</div>

@stop

@section('js_bottom')

<script type="text/javascript" src="{{ url('/') }}/design/front/js/jquery-1.9.1.min.js"></script>

<script src="{{ url('/') }}/design/front/js/bootstrap.js"></script>

<script src="{{ url('/') }}/design/front/js/owl.carousel.js"></script>

<script src="{{ url('/') }}/design/front/js/wow.min.js"></script>

<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script>

<script>

  $('#myCarousel').carousel();

  var winWidth = $(window).innerWidth();

  $(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {

      $('.carousel-inner>.item>img').css({

        'min-width': winWidth, 'width': winWidth

      });

    }

    else {

      winWidth = $(window).innerWidth();

      $('.carousel-inner>.item>img').css({

        'min-width': '', 'width': ''

      });

    }

  });

</script>

<script>

  wow = new WOW(

  {

    animateClass: 'animated',

    offset:       100

  }

  );

  wow.init();

</script>

<script>

  $(window).scroll(function() {

    if ($(document).scrollTop() > 50) {

      $('nav').addClass('shrink');

    } else {

      $('nav').removeClass('shrink');

    }

  });

</script>

<script>

  $(document).ready(function(){

          $(window).scroll(function() { // check if scroll event happened

            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window

              $(".navbar-fixed-top").css("background-color", "#e81e2a"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)

            } else {

              $(".navbar-fixed-top").css("background-color", "#e81e2a"); // if not, change it back to transparent

            }

          });

        });

</script>

<script>

        $(function () {

          function closeSearch() {

            var $form = $('.navbar-collapse form[role="search"].active')

            $form.find('input').val('');

            $form.removeClass('active');

          }

  // Show Search if form is not active // event.preventDefault() is important, this prevents the form from submitting

  $(document).on('click', '.navbar-collapse form[role="search"]:not(.active) button[type="submit"]', function(event) {

  event.preventDefault();

  var $form = $(this).closest('form'),

        $input = $form.find('input');

        $form.addClass('active');

        $input.focus();

      });

  // ONLY FOR DEMO // Please use $('form').submit(function(event)) to track from submission

  // if your form is ajax remember to call `closeSearch()` to close the search container

  $(document).on('click', '.navbar-collapse form[role="search"].active button[type="submit"]', function(event) {

  event.preventDefault();

  var $form = $(this).closest('form'),

        $input = $form.find('input');

        $('#showSearchTerm').text($input.val());

        closeSearch()

      });

    });

</script>

<script>

    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({

        autoPlay : 3000,

        stopOnHover : true,

        items : 8, //10 items above 1000px browser width

        itemsDesktop : [1000,8], //5 items between 1000px and 901px

        itemsDesktopSmall : [900,5], // 3 items betweem 900px and 601px

        itemsTablet: [600,3], //2 items between 600 and 0;

        itemsMobile : [400,1] // itemsMobile disabled - inherit from itemsTablet option

      });

        // Custom Navigation Events

        $(".next").click(function(){

          owl.trigger('owl.next');

        })

        $(".prev").click(function(){

          owl.trigger('owl.prev');

        })

      });

</script>

<script>

      $(document).ready(function() {

        var owl = $("#testimonils-slide");

        owl.owlCarousel({

          autoPlay : 3000,

          stopOnHover : true,

          items : 2,

          itemsDesktop : [1000,2],

          itemsDesktopSmall : [900,1],

          itemsTablet: [600,1],

          itemsMobile : [400,1],

          pagination : true

        });

      });

</script>

<script>

      $(document).ready(function() {

        var owl = $("#owl-demo2");

        owl.owlCarousel({

          autoPlay : 3000,

          stopOnHover : true,

        items : 3, //10 items above 1000px browser width

        itemsDesktop : [1000,3], //5 items between 1000px and 901px

        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px

        itemsTablet: [600,1], //2 items between 600 and 0;

        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option

      });

        // Custom Navigation Events

        $(".next2").click(function(){

          owl.trigger('owl.next');

        })

        $(".prev2").click(function(){

          owl.trigger('owl.prev');

        })

      });

</script>

<script>

      $(document).on('click', '.check_all', function(){

        if($(this).is(':checked') )

        {

          $('.food_type').prop("checked",true);

          $('.check_none').prop("checked",false);

        }

        else

        {

          $('.food_type').prop("checked",false);

          $('.check_none').prop("checked",true);

        }

      });

      $(document).on('click', '.check_none', function(){

        if($(this).is(':checked') )

        {

          $('.food_type').prop("checked",false);

          $('.check_all').prop("checked",false);

        }

      });

      $(document).on('click', '.food_type', function(){

        var total_len= $('.food_type').length;

        var checked_len= $('.food_type:checked').length;

        var chekced_counter = 0;

        $('.food_type:checked').each(function(i){

         chekced_counter++

         $('.check_none').prop("checked",false);

       });

        if( chekced_counter==0)

        {

          $('.check_none').prop("checked",true);

        }

        if(total_len==checked_len)

        {

          $('.check_all').prop("checked",true);

        }

        else

        {

          $('.check_all').prop("checked",false);

        }

      });

      $(document).on('click', '#search_home', function(){

       var search_box = $('#autocomplete').val();

       var counter_cuisine = 0;

       $('.food_type:checked').each(function(i){

        counter_cuisine = 1;

      });

       if( (search_box!='') )

       {

        $("#ajax_favorite_loddder").show();

        if(($('#lat').val()!='') && ($('#lat').val()!='')){

         var frm_val = $('#search_form').serialize();

      //alert("hello");

      console.log(frm_val);

      $.ajax({

        type: "POST",

        url: "{{url('/restaurantslisting_map')}}",

        data: frm_val,

        dataType: 'json',

        success: function(msg) {

          console.log("success"+msg);

window.location.href='<?php echo url('/restaurant_listing')?>';

        },

        error: function(error) {

          console.log("error"+error);

        }

      });

    }

    else

    {

     if(search_box!='')

     {

      var geocoder = new google.maps.Geocoder();

      geocoder.geocode( { 'address': search_box}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

         var latitude = results[0].geometry.location.lat();

         var longitude = results[0].geometry.location.lng();

         $('.MapLat').val(latitude);

         $('.MapLon').val(longitude);

         var frm_val = $('#search_form').serialize();

         $.ajax({

          type: "POST",

          url: "{{url('/restaurantslisting_map')}}",

          data: frm_val,

          dataType: 'json',

          success: function(msg) {

            console.log(msg);

window.location.href='<?php echo url('/restaurant_listing')?>';

          },

          error: function(error) {

            console.log(error);

          }

        });

       }

     });

    }

    else if(counter_cuisine == 1)

    {

      var frm_val = $('#search_form').serialize();

      $.ajax({

        type: "POST",

        url: "{{url('/restaurantslisting_map')}}",

        data: frm_val,

        dataType: 'json',

        success: function(msg) {

window.location.href='<?php echo url('/restaurant_listing')?>';

       }

     });

    }

  }

}

else

{

  alert('Please enter location');

}

});

      function getLocation(address)

      {

        var geocoder = new google.maps.Geocoder();

        geocoder.geocode( { 'address': address}, function(results, status) {

          if (status == google.maps.GeocoderStatus.OK) {

           var latitude = results[0].geometry.location.lat();

           var longitude = results[0].geometry.location.lng();

           $('.MapLat').val(latitude);

           $('.MapLon').val(longitude);

           console.log(latitude, longitude);

         }

       });

      }

      var map;

      var infowindow;

      var radius_meters = 30*1000;

      function initMap(lat_c,long_c) {

//   var pyrmont = {lat: 22.714169, lng: 75.874362};

var pyrmont = {lat: parseFloat(lat_c), lng: parseFloat(long_c)};

 //var service ;

 console.log(pyrmont);

 map = new google.maps.Map(document.getElementById('map'), {

  center: pyrmont,

  zoom: 13

});

 infowindow = new google.maps.InfoWindow();

 var service = new google.maps.places.PlacesService(map);

 service.nearbySearch({

  location: pyrmont,

  radius: radius_meters,

  type: ['restaurant']

}, callback);

}

console.log(service);

function callback(results, status) {

if (status === google.maps.places.PlacesServiceStatus.OK) {

   for (var i = 0; i < results.length; i++) {

     var imageUrl ;

     var photos = results[i].photos;

     console.log(photos);

  /*if (!photos) {

return;

     }*/

     var imageUrl ;

     if(photos){

      if(photos[0].getUrl({'maxWidth': 135, 'maxHeight': 150}))

      {

        imageUrl = photos[0].getUrl({'maxWidth': 135, 'maxHeight': 150});

      }

    }

    var rest_name = results[i].name;

    var rest_address = results[i].vicinity;

    var rest_image = imageUrl;

    var rest_latlng = results[i].geometry.location;

    var rest_range = results[i].rating;

    var frm_val='rest_name='+rest_name+'&rest_address='+rest_address+'&rest_logo='+rest_image+'&rest_latlng='+rest_latlng+'&rest_range='+rest_range;

    $.ajax({

     type: "POST",

     url: "{{url('/google_rest_insert')}}",

     data: frm_val,

     dataType: 'json',

     success: function(msg) {

       console.log(msg.query);

     }

   });

    if(i==(results.length-1))

    {

window.location.href='<?php echo url('/restaurant_listing')?>';

    }

//createTable(results[i]);

   // createMarker(results[i]);

  }

}

}

</script>

<script>

      // This example displays an address form, using the autocomplete feature

      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places

      // parameter when you first load the API. For example:

// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;

      var componentForm = {

        street_number: 'short_name',

        route: 'long_name',

        locality: 'long_name',

        administrative_area_level_1: 'short_name',

        country: 'long_name',

        postal_code: 'short_name'

      };

      function initAutocomplete() {

        // Create the autocomplete object, restricting the search to geographical

        // location types.

        autocomplete = new google.maps.places.Autocomplete(

         /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),

         {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address

        // fields in the form.

        autocomplete.addListener('place_changed', fillInAddress);

      }

      function fillInAddress() {

        // Get the place details from the autocomplete object.

        var place = autocomplete.getPlace();

        $('.MapLat').val(place.geometry.location.lat());

        $('.MapLon').val(place.geometry.location.lng());

        for (var component in componentForm) {

          document.getElementById(component).value = '';

          document.getElementById(component).disabled = false;

        }

        // Get each component of the address from the place details

        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {

          var addressType = place.address_components[i].types[0];

          if (componentForm[addressType]) {

            var val = place.address_components[i][componentForm[addressType]];

            document.getElementById(addressType).value = val;

//alert(val);

    }

  }

}

      // Bias the autocomplete object to the user's geographical location,

      // as supplied by the browser's 'navigator.geolocation' object.

      function geolocate() {

        if (navigator.geolocation) {

         $('.MapLat').val('');

         $('.MapLon').val('');

         navigator.geolocation.getCurrentPosition(function(position) {

          var geolocation = {

            lat: position.coords.latitude,

            lng: position.coords.longitude

          };

          var circle = new google.maps.Circle({

            center: geolocation,

            radius: position.coords.accuracy

          });

          autocomplete.setBounds(circle.getBounds());

        });

       }

     }

</script>

<!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU&libraries=places&callback=initAutocomplete"></script>-->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkGCDT06dfMUewInZ5ScKw4R9i6qMVhCo&libraries=places&callback=initAutocomplete"></script>

<script>

     $(document).on('click', '#search_nearme', function(){

       $("#ajax_favorite_loddder").show();

       var frm_val = $('#search_form').serialize();

       //alert(frm_val);

       $.ajax({

        type: "POST",

        url: "{{url('/get_current_locationdata')}}",

        data: frm_val,

        dataType: 'json',

        success: function(msg) {

         $("#ajax_favorite_loddder").hide();

         if(msg.status==1)

         {

window.location.href='<?php echo url('/restaurant_listing')?>';

         }

         else

         {

window.location.href='<?php echo url('/restaurant_listing')?>';

         }

//alert(msg.status);

       }

     });

     });

     $(document).on('click', '[id^="view_more_"]', function(){

//alert($(this).attr('data-homeid'));

var home_id = $(this).attr('data-homeid');

$("#view_detail_home_"+home_id).modal('show');

});

     $(document).on('click', '#submit_mob', function(){

      var form = $("#send_app_mob");

      form.validate();

      var valid =form.valid();

      if(valid)

      {

       $("#ajax_favorite_loddder").show();

       var frm_val = $('#send_app_mob').serialize();

       $.ajax({

         type: "POST",

         url: "{{url('/send_app_link')}}",

         data: frm_val,

         dataType: 'json',

         success: function(msg) {

           $("#ajax_favorite_loddder").hide();

           $("#send_app_mob").html('Success! Check your phone for the link.');

         }

       });

     }

   });

$(document).on('click', '#submit_email', function(){

  //alert('test email') ;

  var form = $("#send_app_email");

  form.validate();

  var valid =form.valid();

  if(valid)

  {

    $("#ajax_favorite_loddder").show();

    var frm_val = $('#send_app_email').serialize();

    $.ajax({

      type: "POST",

      url: "{{url('/send_app_link')}}",

      data: frm_val,

      dataType: 'json',

      success: function(msg) {

        $("#ajax_favorite_loddder").hide();

        $("#send_app_email").html('Success! Check your email for the link.');

      }

    });

  }

});

     $(document).on('click', '#send_coupon', function(){

      var form = $("#coupon_form");

      form.validate();

      var valid =form.valid();

      if(valid)

      {

       $("#ajax_favorite_loddder").show();

       var frm_val = $('#coupon_form').serialize();

       $.ajax({

         type: "POST",

         url: "{{url('/send_coupon_request')}}",

         data: frm_val,

         dataType: 'json',

         success: function(msg) {

          $("#ajax_favorite_loddder").hide();

          $("#coupons_msg_div").html('Thanks, we will be in touch.');

        }

      });

     }

   });

     $(document).on('click', '#show_owners_form', function(){

     // $('#partner_modal').modal('show');

  /* $('#rest_owner').on('hidden.bs.modal', function (e) {

      $(this)

        .find("input,textarea,select,hidden")

        .val('')

        .end()

      });*/

      document.getElementById('become_partner').reset();

      $('#rest_owner').modal('show');

    });

</script>

  @stop