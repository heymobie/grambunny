@extends("layouts.grambunny")

@section("styles")

<style>

#map {
    height: 740px; 
    width: 100%; 
}
.calTacsn.aos-init {
    margin: 0 auto;
    display: table;
    float: none;
    }
.welcome-area button {
    display: none;
    }
.resto-near-home a.show-map-mode {
    float: left;
    padding-right: 15px;
    border: none;
    background: #444648;
    padding:12px 20px;
    transition: all .3s;
    font-weight: 600;
    color: #fff;
    display: none;
    border-radius: 50px;
    margin-left: 15px;
    }
a.show-list-mode {
    float: left;
    padding-right: 15px;
    border: none;
    background: #444648;   
    transition: all .3s;
    font-weight: 600;
    color: #fff;    
    padding: 12px 20px;
    border-radius: 50px;
    margin-left: 15px;
    }
.resto-near-home a:hover{
    color:#fff;
    }
.resto-near-home{ 
    float:right;        
    margin-top: -65px;
    z-index: 0;
    position: relative;
    right: 20px;
    }
form#search_form {
    width: 90%;
        margin-bottom: 0px;
    }
.btn-group.sf-provider-tooltip a:hover {
    color: #fff !important;
    text-decoration: none;
    }
    

@media only screen and (max-width: 991px) {

.resto-near-home{
    float:right;
    margin-top:-79px;
    right:10px;
    }
}


@media only screen and (max-width: 767px) {
    form#search_form {
        width: 100%;
        margin-bottom: 0px;

    }

.form-search-wrap.mb-3.aos-init.aos-animate {
    width: 100%;
    margin: 0 auto;
    padding-bottom: 75px;
}


    .resto-near-home a {
        width: 100%;
        margin-left: 0px !important;
        border-radius: 4px !important;
    }
    .resto-near-home {
        

        float: left;
    margin-top: -60px;
    right: inherit;
    width: calc(100% - 40px);
    margin-left: 20px;
    }

}


</style>

@endsection

@section("content")


<div class="site-blocks-cover top_search_sec overlay" data-aos="fade-up" data-aos-delay="250" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-12">
                <app-search-field appcategories="{{ $categories }}"></app-search-field>
<!--                 <div class="resto-near-home">
                    <a class="show-map-mode" id="dmapview" style="display: none; cursor:pointer">Map View</a>
                    <?php if (Session::has('nearlat')){ ?>
                    <a class="show-list-mode" id="dlistview" style="display: block; cursor:pointer">List View</a>
                    <?php }else{ ?>
                    <a class="show-list-mode" href="javascript:window.location.reload(true)" style="display: block; cursor:pointer">List View</a>
                    <?php } ?>
                </div> -->
            </div>
        </div>
    </div>
</div>



<!--<div class="site-blocks-cover overlay" style="background-image: url({{asset("public/assets/images/bnr1.jpg")}});"  data-aos="fade-up" data-aos-delay="250" data-stellar-background-ratio="0.5">

                <div class="container">

                    <div class="row align-items-center justify-content-center text-center">

                        <div class="col-md-12">

                            

                            

                            <div class="row justify-content-center mb-4">

                                <div class="col-md-8 text-center">

                                    <h1 class="" data-aos="fade-up">Welcome To grambunny</h1>

                                    <p data-aos="fade-up" data-aos-delay="100">The On Demand E-commerce</p>

                                </div>

                            </div>

                            <app-search-field appcategories="{{ $categories }}"></app-search-field>

                        </div>

                    </div>

                </div>

            </div>-->
<div class="welcome-area wow fadeInUp" style="margin-top:0 !important;" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">

    <app-nearby-map></app-nearby-map>

        <div class="container">
            <div class="row">

            <?php foreach ($verdorslist as $key => $value) { ?>
		        <div class="col-md-3 col-sm-6 equal-col dltv pilist" id="listview" style="display:none;">
			        <div class="sf-search-result-girds" id="proid-30">
				        <div class="sf-featured-top">
					        <div class="sf-featured-media" style="background-image:url(https://www.grambunny.com/public/uploads/vendor/profile/{{$value->profile_img1}})"></div>
					        <div class="sf-overlay-box"></div>
					        <div class="sf-featured-info">
						        <div class="sf-featured-provider">{{$value->business_name}}<!--{{$value->name}} {{$value->last_name}}--></div>
                                <div class="sf-featured-address">{{$value->description}}</div>
						        <div class="sf-featured-address"><i class="fa fa-map-marker"></i>{{$value->city}}, {{$value->state}}, {{$value->zipcode}}</div>
						        <?php $ratings = $value->avg_rating; ?>
						        <div class="star-rating"> 
							    <?php if($ratings>0){                            
                                    for ($i=0; $i < $ratings ; $i++) { ?>
                            	       <span class="fa fa-star" data-rating="<?php echo $i;?>"></span>
                                <?php }
							 }else{?>
							<span class="fa fa-star" style="color: gray;" data-rating="1"></span>
							<span class="fa fa-star" style="color: gray;" data-rating="2"></span>
							<span class="fa fa-star" style="color: gray;" data-rating="3"></span>
							<span class="fa fa-star" style="color: gray;" data-rating="4"></span>
							<span class="fa fa-star" style="color: gray;" data-rating="5"></span>
							<?php } ?>
							<input type="hidden" name="whatever1" class="rating-value" value="2.56">
						</div>
					</div>
				</div>
				<div class="sf-featured-bot">
					<!--<div class="sf-featured-comapny">{{$value->business_name}}</div>-->
					<div class="sf-featured-text"></div>
					<div class="btn-group sf-provider-tooltip">
						<a href="https://www.grambunny.com/{{$value->username}}/services" class="text-dark">View Store</a>
					</div>
				</div>
			</div>
		</div>

     <?php } ?>

            </div>

            </div>

            </div>

            <?php //print_r($listview); ?>

            <div class="main_add_sec">

			<div class="container">

            <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-12">

            <div class="advertBox">

                            @if($advertisement2)

                            <img src="{{asset("public/uploads/advertisement/".$advertisement2->image)}}"> 

                            @endif

                        </div>

                        </div>

            </div></div>

            

            </div>

            

            <div class="site-section tpProduct">

            <div class="container">

     

                <div class="row justify-content-center mb-5"  data-aos="fade-up" data-aos-delay="200">

                    <div class="col-md-7 text-center border-primary">

                        <h2 class="font-weight-light text-primary"><span>Top</span> Products </h2>

                       <!--  <p class="color-black-opacity-5">Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.</p> -->

                    </div>

                </div>


                <div class="row"  data-aos="fade-up" data-aos-delay="300">

                    <div class="col-12  block-13">

                        <div class="owl-carousel nonloop-block-13">

                <?php 


                  if(!empty($products[0]->vendor_id)){

                foreach ($products as $key => $product) {  ?>
                                          
                            <div class="d-block d-md-flex listing vertical">
                                <a href="{{$product->slug}}/details" class="img d-block" style="background-image: url('{{asset("public/uploads/product/".$product->image)}}')"></a>
                                <!--<div class="offers">20% OFF</div>-->
                                <div class="lh-content">
                                    <h3><a href="listings-single.html">{{$product->name}}</a></h3>
                                    <div class="prdctPric">${{$product->price}} <!--<span class="cutPrice"></span>--></div>
            <?php $vendor = DB::table('vendor')->where('vendor_id', '=' ,$product->vendor_id)->first(); ?> 
                                    <address><span>By: <span> 
                                    <?php if(!empty($vendor->username)){ ?> 
                                        <a href="{{$vendor->username}}/services">{{$vendor->name}} {{$vendor->last_name}}</a><?php } ?> </address> 
                                    <a href="{{$product->slug}}/details"><button class="adToCart">View Details</button></a>
                                </div>
                            </div>

                 <?php } }else{

                
                        foreach ($productsdft as $key => $product) {  ?>
                                          
                            <div class="d-block d-md-flex listing vertical">
                                <a href="{{$product->slug}}/details" class="img d-block" style="background-image: url('{{asset("public/uploads/product/".$product->image)}}')"></a>
                                <!--<div class="offers">20% OFF</div>-->
                                <div class="lh-content">
                                    <h3><a href="listings-single.html">{{$product->name}}</a></h3>
                                    <div class="prdctPric">${{$product->price}} <!--<span class="cutPrice"></span>--></div>
            <?php $vendor = DB::table('vendor')->where('vendor_id', '=' ,$product->vendor_id)->first(); ?> 
                                    <address><span>By: <span> 
                                   <?php if(!empty($vendor->username)){ ?> 
                                     <a href="{{$vendor->username}}/services">{{$vendor->name}} {{$vendor->last_name}}</a><?php } ?></address>
                                 
                                    <a href="{{$product->slug}}/details"><button class="adToCart">View Details</button></a>
                                </div>
                            </div>

                 <?php } } ?>   

                        </div>

                    </div>

                </div>

                

            </div>

        </div>

        

            <?php /* <div class="site-section tpProduct">

            <div class="container">



                    <div class="row justify-content-center mb-5">

                        <div class="col-md-7 text-center border-primary">

                            <h2 class="font-weight-light text-primary"><span>Top</span> Services </h2>

                            <!-- <p class="color-black-opacity-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.</p> -->

                        </div>

                    </div>



                    <div class="row"  data-aos="fade-up" data-aos-delay="300">

                    <div class="col-12  block-13">

                        <div class="owl-carousel nonloop-block-13">

                            
                <?php 

                if(!empty($services[0]->vendor_id)){

                foreach ($services as $key => $product) { ?>
                                          
                    <div class="d-block d-md-flex listing vertical">
                        <a href="{{$product->slug}}/details" class="img d-block" style="background-image: url('{{asset("public/uploads/product/".$product->image)}}')"></a>
                        <!--<div class="offers">20% OFF</div>-->
                        <div class="lh-content">
                            <h3><a href="listings-single.html">{{$product->name}}</a></h3>
                            <div class="prdctPric">${{$product->price}} <!--<span class="cutPrice"></span>--></div>
    <?php $vendor = DB::table('vendor')->where('vendor_id', '=' ,$product->vendor_id)->first(); ?> 
                            <address><span>By: <span> 
                            <?php if(!empty($vendor->username)){ ?> 
                            <a href="{{$vendor->username}}/services">{{$vendor->name}} {{$vendor->last_name}}</a><?php } ?></address>
                            <a href="{{$product->slug}}/details"><button class="adToCart">View Details</button></a>
                        </div>
                    </div>

                 <?php }}else{ 

                foreach ($servicesdft as $key => $product) { ?>
                                          
                <div class="d-block d-md-flex listing vertical">
                    <a href="{{$product->slug}}/details" class="img d-block" style="background-image: url('{{asset("public/uploads/product/".$product->image)}}')"></a>
                    <!--<div class="offers">20% OFF</div>-->
                    <div class="lh-content">
                        <h3><a href="listings-single.html">{{$product->name}}</a></h3>
                        <div class="prdctPric">${{$product->price}} <!--<span class="cutPrice"></span>--></div>

                                 
                    <?php $vendor = DB::table('vendor')->where('vendor_id', '=' ,$product->vendor_id)->first(); ?>

                    <address><span>By: <span> 
                    <?php if(!empty($vendor->username)){ ?>                         
                    <a href="{{!empty($vendor->username)}}/services">{{$vendor->name}} {{$vendor->last_name}}</a><?php } ?></address>
                                    
                     <a href="{{$product->slug}}/details"><button class="adToCart">View Details</button></a>
                                </div>
                            </div>

                 <?php } } ?>  



                        </div>

                    </div>

                </div>



                </div>

            </div>  */ ?>

            

  

        <?php /* ?><div class="site-section tpProduct">

            <div class="container-fluid">

                <div class="row justify-content-center mb-5"  data-aos="fade-up" data-aos-delay="200">

                    <div class="col-md-7 text-center border-primary">

                        <h2 class="font-weight-light text-primary"><span>Top</span> Service Provider </h2>

                        <!-- <p class="color-black-opacity-5">Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.</p> -->

                    </div>

                </div>

                

                <div class="col-md-12"  data-aos="fade-up" data-aos-delay="300">

                    <div class="col-12  block-13">

                        <div class="owl-carousel nonloop-block-14">

      

                            <?php foreach ($service_provider as $key => $provider) { ?>



                             <?php $merchant = DB::table('product_service_category')->where('id', '=' ,$provider->category_id)->first(); ?>    

                            

                            <div class="d-block d-md-flex listing vertical">

                                <a href="{{$provider->username}}/services" class="img d-block" style="background-image: url('{{asset("public/uploads/category/".$merchant->cat_image)}}')"></a>

                                <div class="lh-content">

                                    <div class="ownr_img"><img src="{{asset("public/uploads/vendor/profile/".$provider->profile_img1)}}"></div>

                                    <div class="assrNfrtur"><!--<a class="featured" href="#">Featured</a>--><a class="Verified">Verified</a></div>

                                  

                                    <span class="category">{{$provider->business_name}}</span>

                                    <!--<a href="#" class="bookmark"><span class="icon-heart"></span></a>-->

                                    <h3><a href="{{$provider->username}}/services">{{$merchant->category}}</a></h3>

                                    <address><span>By: </span> <a href="{{$provider->username}}/merchant">{{$provider->name}} {{$provider->last_name}}</a></address>

                                </div>

                            </div>



                        <?php } ?>



     



                        </div>

                    </div>

                </div>

            </div>

        </div> <?php */ ?>

        

        <section class="rehome-call-to-action-area bg-overlay bg-img jarallax section-padding-100"  data-aos="fade-up" data-aos-delay="200" style="background-image: url({{asset("public/assets/images/banner.jpg")}});" >

                <div class="container">

                    <div class="row">

                        <div class="col-12 col-lg-12">

                            <div class="call-to-action-content " style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">

                                <div class="calTacsn"  data-aos="fade-up" data-aos-delay="300">

                                    <p>Join Our Friendly Community</p>

                                    <h2>Start Your Own Business, become a Mobie today!</h2>

                                    <div class="download-btn float-left"  data-aos="fade-up" data-aos-delay="350">

                                        <a href="{{ url('/sign-in') }}" class="btn btn-primary rounded py-2 px-4 text-white">Register Now</a>

                                    </div>

                                </div>

                             

                            </div>

                        </div>

                    </div>

                </div>

                <!-- <div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; z-index: -100;"><div style="background-position: 50% 50%; background-size: cover; background-repeat: no-repeat; background-image: url(&quot;https://colorlib.com/preview/theme/rehomes/img/bg-img/11.jpg&quot;); position: fixed; top: 0px; left: 0px; width: 1211px; height: 605.344px; overflow: hidden; pointer-events: none; margin-top: 91.8281px; transform: translate3d(0px, 71.7031px, 0px);"></div>

            </div> -->

        </section>

        

        <div class="main_add_sec">

			<div class="container">

            <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-12">

            <div class="advertBox">

                            @if($advertisement1)

                            <img src="{{asset("public/uploads/advertisement/".$advertisement1->image)}}"> 

                            @endif

                        </div>

                        </div>

            </div></div>

            

            </div>

        

        <div class="site-section  bg-light" data-aos="fade">

            <div class="container">

                <div class="row justify-content-center mb-5"  data-aos="fade-up" data-aos-delay="200">

                    <div class="col-md-7 text-center border-primary">

                        <h2 class="font-weight-light text-primary"><span>Fastest Way </span> to Get Product and Services</h2>

                        <!-- <p class="color-black-opacity-5">Lorem Ipsum Dolor Sit Amet</p> -->

                    </div>

                </div>

                <div class="overlap-category mb-5">

                    <div class="row align-items-stretch">

                        <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-3"  data-aos="fade-up" data-aos-delay="250">



                            @if($ps_block1)

                            <span class="popular-category h-100">

                                <span class="icon"><img src="{{asset("public/uploads/advertisement/".$ps_block1->image)}}"></span>

                                <span class="caption mb-2 d-block">{{ $ps_block1->name }}</span>

                                <p>{{ $ps_block1->content }}</p>

                            </span>

                              @endif



                        </div>

                        <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-3"  data-aos="fade-up" data-aos-delay="300">

                            @if($ps_block2)

                            <span class="popular-category h-100">

                                <span class="icon"><img src="{{asset("public/uploads/advertisement/".$ps_block2->image)}}"></span>

                                <span class="caption mb-2 d-block">{{ $ps_block2->name }}</span>

                                <p>{{ $ps_block2->content }}</p>

                            </span>

                              @endif

                        </div>

                        <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-3"  data-aos="fade-up" data-aos-delay="350">

                            @if($ps_block3)

                            <span class="popular-category h-100">

                                <span class="icon"><img src="{{asset("public/uploads/advertisement/".$ps_block3->image)}}"></span>

                                <span class="caption mb-2 d-block">{{ $ps_block3->name }}</span>

                                <p>{{ $ps_block3->content }}</p>

                            </span>

                              @endif

                        </div>

                        <div class="col-sm-6 col-md-4 mb-4 mb-lg-0 col-lg-3"  data-aos="fade-up" data-aos-delay="400">

                            @if($ps_block4)

                            <span class="popular-category h-100">

                                <span class="icon"><img src="{{asset("public/uploads/advertisement/".$ps_block4->image)}}"></span>

                                <span class="caption mb-2 d-block">{{ $ps_block4->name }}</span>

                                <p>{{ $ps_block4->content }}</p>

                            </span>

                              @endif

                        </div>

                        

                    </div>

                </div>

                

                

            </div>

        </div>

        

      

      

        <?php /*?><div class="site-section bg-white testimonialSection"  data-aos="fade-up" data-aos-delay="200">

            <div class="container">

                <div class="row justify-content-center mb-5">

                    <div class="col-md-7 text-center border-primary">

                        <h2 class="font-weight-light text-primary">Testimonials</h2>

                    </div>

                </div>

                <div class="slide-one-item home-slider owl-carousel">

                        @foreach($testimonials as $testimonial)

                    <div>

                        <div class="testimonial">

                            <figure class="mb-4">

                                <img src="{{asset("uploads/testimonial/".$testimonial->image)}}" alt="Image" class="img-fluid mb-3">

                                <p>{{ $testimonial->name }}</p>

                            </figure>

                            <blockquote>

                                <p>{{ $testimonial->content }}</p>

                            </blockquote>

                        </div>

                    </div>

                        @endforeach

                   

                </div>

            </div>

        </div><?php */?>

@endsection

@section("scripts")


    <!--  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU&libraries=places&callback=initAutocomplete"></script>-->


@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>	

<script type="text/javascript">

$(document).ready(function(){

  $("#dmapview").click(function(){

    $("#dlistview").show();
    $("#dmapview").hide();
    $(".dltv").hide();
    $("#map").show();


  });

    $("#dlistview").click(function(){

    $("#dmapview").show();
    $("#dlistview").hide();
    $(".dltv").show();
    $("#map").hide();


  });


});


 </script>

