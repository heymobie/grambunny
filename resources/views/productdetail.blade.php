@extends('layouts.grambunny')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@section('styles')
<style type="text/css">
   .view-group {
   display: -ms-flexbox;
   display: flex;
   -ms-flex-direction: row;
   flex-direction: row;
   padding-left: 0;
   margin-bottom: 0;
   }
   .thumbnail
   {
   margin-bottom: 30px;
   padding: 0px;
   -webkit-border-radius: 0px;
   -moz-border-radius: 0px;
   border-radius: 0px;
   }
   .item.list-group-item
   {
   float: none;
   width: 100%;
   background-color: #fff;
   margin-bottom: 30px;
   -ms-flex: 0 0 100%;
   flex: 0 0 100%;
   max-width: 100%;
   padding: 0 1rem;
   border: 0;
   }
   .item.list-group-item .img-event {
   float: left;
   width: 20%;
   display: flex;
   justify-content: center;
   align-items: center;
   padding: 15px;
   }
   .item.list-group-item .list-group-image
   {
   margin-right: 10px;
   border: 1px solid #ddd;
   padding: 10px;
   background-color: #f5f5f5;
   }
   .item.list-group-item .thumbnail
   {
   margin-bottom: 0px;
   display: inline-block;
   padding: 10px;
   box-shadow: rgb(0 0 0 / 10%) 0px 4px 10px;
   background-color: #f7f7f7;
   }
   .item.list-group-item .caption
   {
   float: left;
   width: 61%;
   margin: 0;
   }
   .item.list-group-item:before, .item.list-group-item:after
   {
   display: table;
   content: " ";
   }
   .item.list-group-item:after
   {
   clear: both;
   }
   .caption {
   font-size: 15px;
   letter-spacing: 0;
   text-transform: capitalize;
   font-weight: bold;
   }
   .card {
   border-top: 1px solid #EC7324;
   border-bottom: 1px solid #EC7324;
   border-left: 1px solid #ec3e6c;
   border-right: 1px solid #ec3e6c;
   border-radius: 2px !important;
   }
   p.group.inner.list-group-item-text {
   float: left;
   width: 100%;
   font-size: 13px;
   font-weight: normal !important;
   color: #222;
   }
   h4.group.card-title.inner.list-group-item-heading {
   font-size: 17px;
   font-weight: bold;
   }
   .add-cart-list {
   float: left;
   width: 18%;
   margin-top: 6%;
   display: flex;
   align-self: flex-end;
   justify-content: flex-end;
   align-items: flex-end;
   margin-right: 3px;
   margin-bottom: 20px;
   }
   .add-cart-list .btn {
   background-image: linear-gradient(to right, #EC7324 ,#ec3e6c ) !important;
   border: 1px solid #ddd;
   border-radius: 40pc;
   padding: 6px 24px;
   }
   .add-cart-list .btn:hover {
   background-image: linear-gradient(to right,#ec3e6c, #EC7324 ) !important;
   }
   .lead {
   font-size: 1.25rem;
   font-weight: bold;
   float: left;
   width: 100%;
   color: #EC7324;
   padding-bottom: 0px;
   margin-bottom: 0px;
   }
   img.group.list-group-image.img-fluid {
   display: flex;
   justify-content: center;
   align-items: center;
   margin: 0 auto;
   }
   .btn-info {
   color: #fff;
   background-color: #EC7324;
   border-color: #EC7324;
   }
   .btn-danger {
   color: #fff;
   background-color: #ec3e6c;
   border-color: #ec3e6c;
   }
   .btn-info:hover {
   color: #fff;
   background-color: #ec3e6c;
   border-color: #ec3e6c;
   }
   .btn-danger:hover {
   color: #fff;
   background-color: #EC7324;
   border-color: #EC7324;
   }
   .filter-left-side h6 {
   font-size: 17px;
   font-weight: bold;
   padding-bottom: 0px;
   margin-bottom: 0px;
   color: #fff;
   }
   header.card-header {
   background-image: linear-gradient(to right, #EC7324 , #F7FF0A ) !important;
   }
   .filter-left-side {
   margin-top: 5%;
   }
   .form-check-label {
   margin-bottom: 0;
   padding-left: 10px;
   }
   a.wg-g:hover {
    border: 1px solid #EC7324;
    color: #EC7324;
    text-decoration: unset;
}

   .thumbnail.card {
   box-shadow: rgb(0 0 0 / 10%) 0px 4px 10px;
   background-color: #f7f7f7;
   }
   .sort-by {
   float: left;
   }
   select.sordt-by-product {
   border-top: 1px solid #EC7324;
   border-bottom: 1px solid #EC7324;
   border-left: 1px solid #ec3e6c;
   border-right: 1px solid #ec3e6c;
   padding: 6px 10px;
   border-radius: 40px;
   color: EC7324;
   width: 125px;
   }
   section#ordr-grab-sec {
   margin-bottom: 30px;
   }
   a.wg-g {
   float: left;
   width: 45%;
   border: 1px solid #ddd;
   margin-top: -8px;
   margin-bottom: 17px;
   margin-left: 7px;
   padding: 6px;
   text-align: center;
   font-size: 14px;
   color: #222;
   }
@media only screen and (max-width: 640px) {
.item.list-group-item .caption {
    width: 70% !important;
}
.item.list-group-item .img-event {
    float: left;
    width: 30% !important;
    padding: 0px !important;
}
.add-cart-list {
    float: left;
    width: 100% !important;
    margin-top: 0 !important;
  
    justify-content: center !important;
    align-items: center !important;
    margin-right: 3px;
    margin-bottom: 5px !important;
}
.item.list-group-item .list-group-image {
    padding: 0px !important;
}
.caption {
    padding-bottom: 0px;
}
}
</style>
@endsection
@section('content')
<section id="ordr-grab-sec">
   <div class="container">
      <div class="row  wow fadeInUp" data-wow-delay="0.1s">
         <div class="col-lg-12">
            <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
               <!-- <span>Grab it features //</span> -->
               <h2>{{@$page_data->page_title}}</h2>
               <p>{{@$page_data->sub_title}}</p>
            </div>
         </div>
      </div>
   </div>
</section>
<!------ Include the above in your HEAD tag ---------->
<div class="container">
   <div class="container">
      <div class="row">
         <div class="col-md-3 filter-left-side">
            <div class="card">
               <article class="card-group-item">
                  <header class="card-header">
                     <h6 class="title">Brands </h6>
                  </header>
                  <div class="filter-content">
                     <div class="card-body">
                        <form>
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" value="">
                           <span class="form-check-label">
                           Mersedes Benz
                           </span>
                           </label> <!-- form-check.// -->
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" value="">
                           <span class="form-check-label">
                           Nissan Altima
                           </span>
                           </label>  <!-- form-check.// -->
                           <label class="form-check">
                           <input class="form-check-input" type="checkbox" value="">
                           <span class="form-check-label">
                           Another Brand
                           </span>
                           </label>  <!-- form-check.// -->
                        </form>
                     </div>
                     <!-- card-body.// -->
                  </div>
               </article>
               <!-- card-group-item.// -->
               <article class="card-group-item">
                  <header class="card-header">
                     <h6 class="title">Types </h6>
                  </header>
                  <div class="filter-content">
                     <div class="card-body">
                        <label class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadio" value="">
                        <span class="form-check-label">
                        First hand items
                        </span>
                        </label>
                        <label class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadio" value="">
                        <span class="form-check-label">
                        Brand new items
                        </span>
                        </label>
                        <label class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadio" value="">
                        <span class="form-check-label">
                        Some other option
                        </span>
                        </label>
                     </div>
                     <!-- card-body.// -->
                  </div>
               </article>
               <!-- card-group-item.// -->
               <article class="card-group-item">
                  <header class="card-header">
                     <h6 class="title">Weights </h6>
                  </header>
                  <div class="filter-content">
                     <div class="card-body weight-content">
                        <a href="#" class="wg-g">1g</a>
                        <a href="#" class="wg-g">4g</a>
                        <a href="#" class="wg-g">6g</a>
                        <a href="#" class="wg-g">8g</a>
                     </div>
                     <!-- card-body.// -->
                  </div>
               </article>
               <!-- card-group-item.// -->
               <article class="card-group-item">
                  <header class="card-header">
                     <h6 class="title">Potency </h6>
                  </header>
                  <div class="filter-content">
                     <div class="card-body range-potency">
                        <form method="post" action="/action_page_post.php">
                           <div data-role="rangeslider">
                              <label for="price-min">Price:</label>
                              <input type="range" name="price-min" id="price-min" value="200" min="0" max="1000">
                              <label for="price-max">Price:</label>
                              <input type="range" name="price-max" id="price-max" value="800" min="0" max="1000">
                           </div>
                        </form>
                     </div>
                     <!-- card-body.// -->
                  </div>
               </article>
               <!-- card-group-item.// -->
            </div>
         </div>
         <div class="col-md-9">
            <div class="row">
               <div class="col-lg-12 my-3">
                  <div class="sort-by">
                     <select class="sordt-by-product">
                        <option>Sort By</option>
                        <option>Popular</option>
                        <option>Brand</option>
                        <option>Price: Low to High</option>
                        <option>Price: High to Low</option>
                        <option>Potency: Low to High</option>
                        <option>Potency: High to Low</option>
                     </select>
                  </div>
                  <div class="pull-right">
                     <div class="btn-group">
                        <button class="btn btn-info" id="list">
                        <i class="fa fa-list" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-danger" id="grid">
                        <i class="fa fa-th" aria-hidden="true"></i>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <div id="products" class="row view-group">
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy .
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy.
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/AiJi1r1627556299.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy.
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/AiJi1r1627556299.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy.
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/AiJi1r1627556299.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy.
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
               <div class="item col-xs-4 col-lg-4">
                  <div class="thumbnail card">
                     <div class="img-event">
                        <img class="group list-group-image img-fluid" src="https://www.grambunny.com/public/uploads/product/AiJi1r1627556299.png" alt="" />
                     </div>
                     <div class="caption card-body">
                        <h4 class="group card-title inner list-group-item-heading">
                           Product title
                        </h4>
                        <p class="group inner list-group-item-text">
                           Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                           sed diam nonummy .
                        </p>
                        <div class="row">
                           <div class="col-xs-12 col-md-6">
                              <p class="lead">
                                 $21.000
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="add-cart-list">
                        <a class="btn btn-success" href="#">Add to cart</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).ready(function() {
          $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
          $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
      });
</script>
@endsection