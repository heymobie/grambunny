@extends('layouts.app')

@section("other_css")


    <!-- SPECIFIC CSS -->
<meta name="_token" content="{!! csrf_token() !!}"/>
<link href="{{ url('/') }}/design/front/css/blog.css" rel="stylesheet">

  

@stop
@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window paralx_rev_top" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header-car.jpg" data-natural-width="1400" data-natural-height="470">
  <div id="subheader">
    <div id="sub_content">
      <div id="thumb">	  
					<?php 
	  
	  $cove_img = DB::table('vehicle_img')	
						->where('vehicle_id', '=' ,$vehicle_detail[0]->vehicle_id)
						->where('isCover', '=' ,'1') 				
						->get();
		if(count($cove_img)>0){				
	  ?>
	   <img src="{{ url('/') }}/uploads/vehicle/{{$cove_img[0]->imgPath}}" alt="">
	  <?php }else{?>
	  
	  <!--<img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">-->
	  <img src="{{ url('/') }}/design/front/img/logo.png" alt="">	
	  <?php }?>
	  </div>
	    @if($total_review>0)
      <div class="rating">
	  	  
	   @for ($x = 1; $x < 6; $x++)
		@if(($avg_rating>0) && ($avg_rating>=$x))
		<i class="icon_star voted"></i>
		@else
			<i class="icon_star"></i>
		@endif
	  @endfor 
	  
	  
	  (<small><a href="{{url('/'.strtolower(str_replace(' ','_',trim($vehicle_detail[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_id))).'/review')}}">Read 							  {{$total_review}}, reviews</a></small>)</div>
	  @endif
	  
     
      <h1>{{$vehicle_detail[0]->make_name.' - '.$vehicle_detail[0]->model_name}}</h1>
      <div><em>{{$vehicle_detail[0]->transport_name}}</em></div>
      <div><i class="icon_pin"></i> {{$vehicle_detail[0]->transport_address}}, {{$vehicle_detail[0]->transport_suburb}} {{$vehicle_detail[0]->transport_pcode}}.</div>
      <div><strong>Minimum Rate:</strong> $ {{$vehicle_detail[0]->vehicle_minrate}}.</div>
	  
	  
    </div>
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section><!-- End section -->
<!-- End SubHeader ============================================ -->

<div id="position">
  <div class="container">
    <ul>
      <li><a href="{{url('/')}}">Home</a></li>
	  <li>{{$vehicle_detail[0]->transport_suburb}}</li>
      <li>{{$vehicle_detail[0]->make_name.' - '.$vehicle_detail[0]->model_name}}</li>
    </ul>
  </div>
</div>
<!-- Position -->
    <!-- Content ================================================== -->
    <div class="container margin_60_35">
	<div class="row">
         
	 <aside class="col-md-3 col-xs-12" id="sidebar">
			<p><a href="{{url('/'.strtolower(str_replace(' ','_',trim($vehicle_detail[0]->transport_suburb)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_rego)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vclass_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->model_name)).'/'.str_replace(' ','_',trim($vehicle_detail[0]->vehicle_id))))}}" class="btn_side">Back</a></p>
				<div class="rev_main_wrap">
				<form role="form" id="update_frm" method="POST" action="{{ url('/transport_order_review_action') }}">
				
                        {{ csrf_field() }}
						<input type="hidden" name="order_id" value="{{$refno}} " />
						<input type="hidden" name="user_id" value="{{$user_id}} " />
						<input type="hidden" name="trans_id" value="{{$trans_id}} " />
						<input type="hidden" name="vehicle_id" value="{{$vehicle_id}} " />
						<input type="hidden" name="ratting" id="ratting" value="" />
						
				<p><h4>Add Your Review</h4></p>
				@if($user_id==0)
				<p>
				To write a customer review you must have ordered from this restaurant before through our website so that we can obtain better quality, more objective reviews. Only one review per received order is allowed.
				</p>				
				@elseif(($refno==0) && ($user_id>0))
				<p>
					Our records show that you did not order from this restaurant or a review has already been submitted for your last completed order from this restaurant and only one review per received order is allowed.
				</p>				
				@elseif(($refno>0) && ($user_id>0) &&(!empty($order_detail)))
				  <div class="col-md-2 col-sm-3 col-xs-2">
					<span>Food</span>
				  </div>
				  <div class="col-md-10 col-sm-9 col-xs-10">
				  
					<div class="review_rating">
						<div class="row">
							<div class="rev_row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<span>Poor</span>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center">
									<span>Average</span>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-right">
									<span>Excellent</span>
								</div>
							</div>
						</div>
						<div class="range-slider">
						  <input class="range-slider__range" name="range" type="range" value="5" min="0" max="10">
						  <span class="range-slider__value">5</span>
						</div>
					</div>
				  </div>
					<div class="exprnc">
					<div class="form-group">
					  <label for="comment">YOUR EXPERIENCE</label>
					  <textarea class="form-control" rows="9" id="comment" name="review_text" placeholder="To make your review more relevant consider adding restaurant name, cuisine and food item to your review"  required="required"></textarea>
					</div>
					<button type="submit" class="btn btn-primary btn_1" id="update_infobtn">Add Review</button>
				</div>	
				@endif
				</form>
  				</div>                
     </aside><!-- End aside -->
	 
     <div class="col-md-9 col-xs-12">
	 	@if(count($review_list)>0)
			
		  @foreach($review_list as $rlist)
		
     		<div class="post">
					<!--<a href="blog_post.html" ><img src="{{ url('/') }}/design/front/img/blog-1.jpg" alt="" class="img-responsive"></a>-->
					<div class="post_info clearfix">
						<div class="post-left">
							<ul>
								<li><i class="icon-calendar-empty"></i>{{date('jS F, Y',strtotime($rlist->created_at))}} <em>by {{$rlist->name}}</em></li>
                                <!--<li><i class="icon-inbox-alt"></i><a href="#">Category</a></li>
								<li><i class="icon-tags"></i><a href="#">Works</a>, <a href="#">Personal</a></li>-->
							</ul>
						</div>
						<!--<div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div>-->
					</div>
					<!--<h2>Duis aute irure dolor in reprehenderit</h2>-->
					<p>	{{$rlist->re_content}}</p>
                    
					<!--<a href="blog_post.html" class="btn_1" >Read more</a>-->
				</div><!-- end post -->
		  @endforeach		
        @endif             
              <?php /*?><div class="text-center">
                 <ul class="pager">
                    <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                    <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
                  </ul>
              </div><?php */?>
			  
     </div>
	 <!-- End col-md-9-->   
     
      
	</div>    
</div><!-- End container -->
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')




	


<!-- COMMON SCRIPTS -->
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>

<!-- review rating script -->
<script>
var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

	  
    value.each(function(){	  
      var value = $(this).prev().attr('value');
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
	  
    });
  });
};

rangeSlider();


$(document).ready(function () {
            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('#back-to-top').addClass('show');
                    } else {
                        $('#back-to-top').removeClass('show');
                    }
                };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }

            $("#close_in").click(function () {
                if ($(".cmn-toggle-switch__htx").hasClass("active")) {
                    $(".cmn-toggle-switch__htx").removeClass("active")
                }
            });

        });	
</script>

<script>

 $(document).on('click', '#update_infobtn', function(){ 
		 	
			
			//alert($('.range-slider__value').text());
			
			var form = $("#update_frm");
				form.validate();
			var valid =	form.valid();
		 });

		  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
@stop