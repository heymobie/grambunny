<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<div style="display: table;margin: 0 auto;width: 100%;padding: 20px;background-color: #f9f9f9; color: #272727; font-family: 'Open Sans', sans-serif;border: 1px solid #cecece;">
	<div>
		<h1 style="font-size: 20px;margin-bottom: 10px;font-weight: 900;">
<?php
echo (!empty($order_detail[0]->rest_name) ? $order_detail[0]->rest_name : 'N/A');

echo "</h1>
		<p style='margin: 0;padding: 0;
     text-decoration: none;'><a style='color: #272727;text-decoration: none;' href='https://maps.google.com/?q=152+Easton+Avenue,+New+Brunswick,+NJ,+USA+New+Brunswick,+NJ+08901&amp;entry=gmail&amp;source=g'>";
echo (!empty($order_detail[0]->rest_address) ? $order_detail[0]->rest_address : 'N/A');
echo "</a></p>
		<p style='margin: 0;padding: 0;
     text-decoration: none;'><a  style='color: #272727; text-decoration: none;' href='https://maps.google.com/?q=152+Easton+Avenue,+New+Brunswick,+NJ,+USA+New+Brunswick,+NJ+08901&amp;entry=gmail&amp;source=g'>";
echo @$order_detail[0]->rest_city.', '.@$order_detail[0]->rest_state.' '.@$order_detail[0]->rest_zip_code;
echo "</a></p><br><h1 style='font-size: 20px;margin-bottom: 10px;font-weight: 900;'>Sales Summary as of</h1>
	</div>		
	<div>";

				$sub_total = 0;
				$delivery = 0;
				$tax = 0;
				$tip = 0;
				$all_total = 0;

				$pre_sub_total = 0;
				$pre_delivery = 0;
				$pre_tax = 0;
				$pre_tip = 0;
				$pre_total = 0;

				$cash_sub_total = 0;
				$cash_delivery = 0;
				$cash_tax = 0;
				$cash_tip = 0;
				$cash_total = 0;

				$prepaidOrders = 0;
				$cashOrders = 0;
				foreach ($order_detail as $key2 => $val) 
				{
					$total = 0;
					$sub_total = $sub_total+$val->order_subtotal_amt;
					$delivery = $delivery+$val->order_min_delivery;
					$tax = $tax+$val->order_service_tax;
					$tip = $tip+$val->order_tip;

					$total = $total+$val->order_subtotal_amt;
					$total = $total+$val->order_min_delivery;
					$total = $total+$val->order_service_tax;
					$total = $total+$val->order_tip;

					
					if($val->order_pmt_method == 'Cash')
					{
						$cashOrders++;
						$cash_sub_total = $cash_sub_total+$val->order_subtotal_amt;
						$cash_delivery = $cash_delivery+$val->order_min_delivery;
						$cash_tax = $cash_tax+$val->order_service_tax;
						$cash_tip = $cash_tip+$val->order_tip;
						$cash_total = $cash_total+$total;
					}
					else
					{
						$prepaidOrders++;
						$pre_sub_total = $pre_sub_total+$val->order_subtotal_amt;
						$pre_delivery = $pre_delivery+$val->order_min_delivery;
						$pre_tax = $pre_tax+$val->order_service_tax;
						$pre_tip = $pre_tip+$val->order_tip;
						$pre_total = $pre_total+$total;
					}

					
					$all_total = $all_total+$total;

				}
				
				
				?>
				<table border="1" width="100%" cellpadding="5" cellspacing="0" style="border-color: #e4e4e4;color: #272727;font-size: 14px;">
					<thead>
						<tr style="background-color: #ddd;">
							<th align="left">Type</th>
							<th align="right">#</th>
							<th align="right">Subtotal</th>
							<th align="right">Delivery</th>
							<th align="right">Tax</th>
							<th align="right">Tip</th>
							<th align="right">Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="left">Prepaid Order</td>
							<td align="right"><?php echo '$'.$prepaidOrders; ?></td>
							<td align="right"><?php echo '$'.$pre_sub_total; ?></td>
							<td align="right"><?php echo '$'.$pre_delivery; ?></td>
							<td align="right"><?php echo '$'.$pre_tax; ?></td>
							<td align="right"><?php echo '$'.$pre_tip; ?></td>
							<td align="right"><?php echo '$'.$pre_total; ?></td>
						</tr>
						<tr>
							<td align="left">Cash Order</td>
							<td align="right"><?php echo '$'.$cashOrders; ?></td>
							<td align="right"><?php echo '$'.$cash_sub_total; ?></td>
							<td align="right"><?php echo '$'.$cash_delivery; ?></td>
							<td align="right"><?php echo '$'.$cash_tax; ?></td>
							<td align="right"><?php echo '$'.$cash_tip; ?></td>
							<td align="right"><?php echo '$'.$cash_total; ?></td>
						</tr>
						<tr style="background-color: #ddd;">

							<td colspan="2"></td>
							<td align="right"><?php echo '$'.$sub_total; ?></td>
							<td align="right"><?php echo '$'.$delivery; ?></td>
							<td align="right"><?php echo '$'.$tax; ?></td>
							<td align="right"><?php echo '$'.$tip; ?></td>
							<td align="right"><?php echo '$'.$all_total; ?></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div>
				<br>
			<div>
				<h1 style="font-size: 20px;margin-bottom: 10px;font-weight: 900;">Order Detail</h1>	
			</div>
			<div>
				<table border="1" width="100%" cellpadding="5" cellspacing="0" style="border-color: #e4e4e4;color: #272727;font-size: 14px;    font-size: 14px;">
					<thead>
						<tr style="background-color: #ddd;">
							<th align="left">ID</th>
							<th align="left">Type</th>
							<th align="left">Date</th>
							<th align="left">Time</th>
							<th align="right">Subtotal</th>
							<th align="right">Delivery</th>
							<th align="right">Tax</th>
							<th align="right">Tip</th>
							<th align="right">Total</th>
						</tr>
					</thead>
					<tbody>
				
						<?php
						foreach ($order_detail as $key2 => $val) 
						{
							$total = 0;
							?>
							<tr>
								<td align="left"><?php echo $val->order_id; ?></td>
								<td align="left"><?php echo ($val->order_pmt_method == 'Cash' ? 'Cash' : 'Prepaid'); ?></td>
								<td align="left"><span class="aBn" data-term="goog_988920882" tabindex="0"><span class="aQJ"><?php echo (!empty($val->order_create) ? date('d-m-Y',strtotime($val->order_create)) : 'N/A'); ?></span></span></td>
								<td align="left"><?php echo (!empty($val->order_picktime) ? $val->order_picktime : '').' '.(!empty($val->order_picktime_formate) ? $val->order_picktime_formate : ''); ?></td>
								<td align="right">
									<?php 
									$total = $total+$val->order_subtotal_amt;
									echo '$'.$val->order_subtotal_amt; 
									?>
								</td>
								<td align="right" align="right">
									<?php 
										$total = $total+$val->order_min_delivery;
										echo '$'.$val->order_min_delivery; 
									?>
								</td>
								<td align="right">
									<?php 
										$total = $total+$val->order_service_tax;
										echo '$'.$val->order_service_tax; 
									?>
								</td>
								<td align="right">
									<?php 
										$total = $total+$val->order_tip;
										echo '$'.$val->order_tip; 
									?>
								</td>
								<td align="right"><?php echo '$'.$total; ?></td>
							</tr>
							<?php
						}
						?>
						<tr style="background-color: #ddd;">
							<td colspan="4"></td>
							<td align="right"><?php echo '$'.$sub_total; ?></td>
							<td align="right"><?php echo '$'.$delivery; ?></td>
							<td align="right"><?php echo '$'.$tax; ?></td>
							<td align="right"><?php echo '$'.$tip; ?></td>
							<td align="right"><?php echo '$'.$all_total; ?></td>
						</tr>
			</tbody>
		</table>
	</div>

	<div class="yj6qo"></div><div class="adL"></div>

</div>