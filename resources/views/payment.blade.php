@extends('layouts.app')

@section("other_css")

<?php $lang = Session::get('locale'); ?>

<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">

<link rel="apple-touch-icon" type="image/x-icon" href="{{ url('/') }}/design/front/img/apple-touch-icon-57x57-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ url('/') }}/design/front/img/apple-touch-icon-72x72-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ url('/') }}/design/front/img/apple-touch-icon-114x114-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ url('/') }}/design/front/img/apple-touch-icon-144x144-precomposed.png">

<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">

   	  <!-- Radio and check inputs -->

    <!--[if lt IE 9]>

      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>

      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>

    <![endif]-->

 <style type="text/css">
 .braintree-option.braintree-option__paypal {
    display: none;
}	
 </style>   

@stop

@section('content')

<!-- SubHeader =============================================== -->

<section class="parallax-window"  id="short"  data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">

    <div id="subheader">

    	<div id="sub_content">

    	 <h1>@lang('translation.placeYourOrder')</h1>

            <div class="bs-wizard">

                <div class="col-xs-4 bs-wizard-step complete">

                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> @lang('translation.yourDetails')</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                  <a href="{{url('/checkout')}}" class="bs-wizard-dot"></a>

				       <!-- <a href="#" class="bs-wizard-dot" id="back_checkout"></a>-->

                </div>                             

                <div class="col-xs-4 bs-wizard-step active">

                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> @lang('translation.payment')</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                  <a href="#0" class="bs-wizard-dot"></a>

                </div>         

              <div class="col-xs-4 bs-wizard-step disabled">

                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> @lang('translation.finish')</div>

                  <div class="progress"><div class="progress-bar"></div></div>

                  <a href="cart_3.html" class="bs-wizard-dot"></a>

                </div>  

		</div><!-- End bs-wizard --> 

        </div><!-- End sub_content -->

	</div><!-- End subheader -->

</section><!-- End section -->

<!-- End SubHeader ============================================ -->


<!-- Position -->

    <!-- Content ================================================== -->

  <div class="container margin_60_35">

	<div id="container_pin">

		<div class="row">

			<div class="col-md-3">

				<div class="box_style_2 hidden-xs info">
					<h4 class="nomargin_top">	
					@if(isset($delivery[0]->page_content))
			          @lang('translation.deliveryTime') <i class="icon_clock_alt pull-right"></i>
			          @else 
			          @endif
			          </h4>
			          <div>
			          @if(isset($delivery[0]->page_content))
			          {{strip_tags($delivery[0]->page_content)}}
			          @else
			          @endif
			          </div>
			          <hr>
			          <h4>
			            @if(isset($secure[0]->page_content))
			            @lang('translation.securePayment') <i class="icon_creditcard pull-right"></i>
			            @else 
			            @endif
			          </h4>
			            @if(isset($secure[0]->page_content))
			            {{strip_tags($secure[0]->page_content)}} 
			            @else
			            @endif
				</div><!-- End box_style_2 -->

                

				<!--<div class="box_style_2 hidden-xs" id="help">

					<i class="icon_lifesaver"></i>

					<h4>Need <span>Help?</span></h4>

					<a href="tel://004542344599" class="phone">+1800123456</a>

					

				</div>-->

			</div><!-- End col-md-3 -->


 <?php 

 $grand_total = number_format(Session::get('cart_order_detail')['grand_total'],2); 

 if($user_wallet>$grand_total){

 $checkcondition = "1";
 $user_wallets = "0";

 }elseif(($user_wallet>0) && ($user_wallet<$grand_total)){

 $checkcondition = "2";

 $grand_total = $grand_total-$user_wallet;
 $user_wallets = $user_wallet;

 }else{

 $checkcondition = "3";
 $user_wallets = "0";

 }


 ?>				

            
<div class="col-md-6">
				<div class="box_style_2">
					<h2 class="inner">@lang('translation.paymentMethods')</h2>	

		<?php if($checkcondition!=3){ ?> 			

		<div class="payment_select">
		<label class="control control--radio">
		<input type="<?php if($checkcondition==2){ echo "checkbox";}else{ echo "radio";}?>" class="rest_option payment_option rest_payment_option rest_payment_wallet" id="payment_option" value="wallet" name="payment_method" <?php if($checkcondition==1){ echo "checked";} if($checkcondition==3){ echo "disabled";} ?>>@lang('translation.payWithWallet') ( $<?php if($user_wallet){ print_r($user_wallet); }else{ echo "0"; } ?>	)
		<div class="control__indicator"></div>
		<i class="icon_wallet"></i>
		</label>

		<div id="wshowmsg" style="color: red;"></div>
	    </div>	

	    <?php } ?>			
										
					<div class="payment_select">
						<label class="control control--radio">
						<input type="radio" id="payment_option" value="credit_card" class="rest_option rest_payment_option payment_option rest_payment_cart"  name="payment_method" >@lang('translation.creditCard')
							<div class="control__indicator"></div>
							<i class="icon_creditcard"></i>
						</label>
						
					</div>
				<form name="cart_detail_frm" id="cart_detail_frm" method="post">	
				<input type="hidden" name="sub_total" value="{{Session::get('cart_userinfo')['sub_total']}}"/>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf">
				<input type="hidden" name="total_amt" value="{{Session::get('cart_userinfo')['total_charge']}}"/>
				<input type="hidden" name="service_tax" value="{{Session::get('cart_userinfo')['service_tax']}}"/>
					<!--<div class="form-group">
						<label>Name on card</label>
						<select class="form-control" id="name_card_order" name="name_card_order" >
							<option value="Visa">Visa</option>
							<option value="MasterCard">MasterCard</option>
							<option value="Discover">Discover</option>
							<option value="Amex">American Express</option>
						</select>
					</div>-->
					<!-- <div class="form-group">
						<label>Card number</label>

						<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf">

						<input type="hidden" name="nonce" value="" id="nonce">	
						<input type="text" id="card_number" name="card_number" class="form-control cart_field" placeholder="Card number" value="4242424242424242" maxlength="16">
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>Expiration date</label>
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
									
										<select id="expire_month" name="expire_month" class="form-control cart_field">
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
										</select>
										
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group">
										
										<select id="expire_year" name="expire_year" class="form-control cart_field">
											<?php for($y=date('Y');$y<=(date('Y')+5);$y++)
											{
											?>
												<option value="<?php echo $y;?>"><?php echo $y;?></option>
											<?php 
											}
											?>
										</select>
										
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label>Security code</label>
								<div class="row">
									<div class="col-md-4 col-sm-6">
										<div class="form-group">
											<input type="text" id="ccv" name="ccv" class="form-control cart_field" placeholder="CCV"  maxlength="3">
										</div>
									</div>
									<div class="col-md-8 col-sm-6">
										<img src="{{ url('/') }}/design/front/img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Last 3 digits</small>
									</div>
								</div>
							</div>	
						</div>
					</div> -->

					<div id="dropin-container">					
					</div>
					</form>
					<!--End row -->
					<!--div class="payment_select" id="paypal">

						<form id="checkout" method="post" action="/checkout">
						  <div id="payment-form"></div>
						  <input type="submit" value="Pay $10">
						</form>
						  <label class="control control--radio"><input class="rest_option rest_payment_option payment_option"  type="radio"  id="payment_option" value="paypal" name="payment_method" >Pay with paypal
						  <div class="control__indicator"></div>
						  </label>
					</div-->
					<?php
					
					 if ((Session::get('cart_userinfo')['partical_payment_alow']==0))
					{
					?>
				
					<div class="payment_select">
						<label class="control control--radio"><input type="radio" class="rest_option
						payment_option rest_payment_option rest_payment_cash" id="payment_option" value="cash" name="payment_method" >@lang('translation.payAtDelivery')
						<div class="control__indicator"></div>
						<i class="icon_wallet"></i>
						</label>
					</div>

					<?php }?>

					<div class="payment_select">
                    <label class="couponcodes">
					<span class="dcc">@lang('translation.couponCode')</span>
					<input type="text" class="apply_coupon" id="coupon_code" value="" name="coupon_code" > 
					<input type="button" class="apply" name="apply" id="apply" value="@lang('translation.btnApply')">
				    </label>
				    <div id="ccmsg" class="ccmsg" style="color: green;text-align: center;"></div>
					</div>
								
				</div><!-- End box_style_1 -->
			</div>
			<!-- End col-md-6 -->
       
			<div class="col-md-3 side_cart_sec" id="sidebar">

            <div class="theiaStickySidebar">

				<div id="cart_box">

					<h3 style="text-align: center;">@lang('translation.yourOrder')<!--<i class="icon_cart_alt pull-right"></i>--></h3>

					<?php $cart_price = 0; $count_number = '1'; $frm_field = '';$total_amt=0;?>

		@if(isset($cart) && (count($cart)))

			<div class="cart_scrllbr">

<div class="table table_summary">	
			
				 @foreach($cart as $item)

				<?php $total_amt= $cart_price = $cart_price+$item->price;?>


 				<div class="spcl-ordr-div">
				 <div class="crt_itm_cust1">
				  <p class="spcl-ordr-name"><strong>{{$item->qty}}  x</strong>
					<?php 
		            if($lang=='ar'){
		              $name_ar = DB::table('menu_category')
		                        ->where('menu_category_id', '=' ,$item->id)
		                        ->first();

		              print_r($name_ar->menu_category_name_ar);
		            }else{
		               echo $item->name;
		            }
		            ?>
		          </p>

                 <p class="spcl-ordr-priz"><strong class="pull-right">${{$item->price}}</strong></p>
                  <p class="spcl-ordr-think">{{@$item->options['instraction']}}</p>
                </div>

				

				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))

				 	@foreach($item->options['addon_data'] as $addon)

					  <?php $count_number ++; 

					  $total_amt = $cart_price = $cart_price+$addon['price'];

					  ?>
					  <div class="crt_itm_cust23">
					  <p class="spcl-ordr-name">
					  	  <?php 
			              if($lang=='ar'){
			                $addon_name_ar = DB::table('menu_category_addon')
			                          ->where('addon_id', '=' ,$addon['id'])
			                          ->first();

			                print_r($addon_name_ar->addon_name_ar);
			              }else{
			                 echo $addon['name'];
			              }
			              ?>
					  </p>
					  <p class="spcl-ordr-priz"><strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong></p>
					</div>
					 @endforeach

				 @endif 
</div>	
				<?php $count_number++; ?>

				 @endforeach

              </div> 

  </div>

@endif

					<hr>

					<div class="row" id="options_2">

						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">

				  

						<div class="control-group islamic2">

						  <label class="control control--radio">

						  <input type="radio" class="rest_option"  checked name="service_option" value="{{Session::get('cart_userinfo')['service_type']}}">

						   

						  <div class="control__indicator"></div>

						  </label> {{Session::get('cart_userinfo')['service_type']}}

						</div>

						</div>

					</div><!-- Edn options 2 -->

					<hr>

					<table class="table table_summary">

					<tbody>
					<tr>
						<td>
						 @lang('translation.subtotal') <span class="pull-right">${{$cart_price}}</span>

						 <input type="hidden" name="cartprice" id="cartprice" value="{{$cart_price}}">
						</td>
					</tr>					
					<tr>
					  <td>@lang('translation.salesTax') <span class="pull-right">$<?php echo Session::get('cart_userinfo')['service_tax'];?></span></td>
					</tr>

					@if((Session::get('cart_userinfo')['promo_discount_text']) && (Session::get('cart_userinfo')['promo_discount_text']>0))
					
					<tr>
					  <td> @lang('translation.discount')
					  <?php  echo '('.Session::get('cart_userinfo')['promo_discount_text'].')';?>
					  
					   <span class="pull-right">$<?php echo Session::get('cart_userinfo')['promo_discount'];?></span></td>
					</tr>

					@endif

				@if(!empty(Session::get('cart_userinfo')['delivery_fee']) && (Session::get('cart_userinfo')['delivery_fee']>0))
                <tr>
                  <td> @lang('translation.deliveryFee') <span class="pull-right">${{number_format(Session::get('cart_userinfo')['delivery_fee'],2)}}</span></td>
                </tr>
					@if((Session::get('cart_userinfo')['delivery_fee_min_remaing']>0))
					
					<tr id="del_fee_min">
					  <td> ${{number_format(Session::get('cart_userinfo')['delivery_fee_min'],2)}} min  <span class="pull-right">$<span id="delivery_fee_min_span">{{number_format(Session::get('cart_userinfo')['delivery_fee_min_remaing'],2)}}</span> remaining</span>
					  </td>
					</tr>				
					@endif
				
				@endif
				
				
				<?php
				
				/* if ((Session::get('cart_userinfo')['partical_payment_alow']>0))
				{
				?>
				
				<tr>
                	<td>
                     <hr />
                    <div  class="prsl-pay">
									 
						<label for="chkPassport">
							Partial Payment  (<?php echo Session::get('cart_userinfo')['partical_percent'];?>%)
						</label>
					   
						<div id="dvPassport" >
						   <p>Partial Payout Amt (<?php echo Session::get('cart_userinfo')['partical_percent'];?>%) <span class="pull-right">$<span id="payout_partial"><?php echo Session::get('cart_userinfo')['partical_amt'];?></span></span></p>
						   <p>Partial Remaining Payment  <span class="pull-right">$<span id="payout_partial_remaining"><?php echo Session::get('cart_userinfo')['partical_remaning_amt'];?></span></span></p>
						</div>
          
                    </div>
                    </td>
                </tr>
				
				<?php }	*/ ?>
				
				<?php	$total_amt = Session::get('cart_userinfo')['total_charge']; ?>
                <tr>
                  <td class="total"> @lang('translation.total') <span class="pull-right">${{number_format($total_amt,2)}}</span></td>
                </tr>
                
                  <?php Session::get('cart_order_detail'); ?>

                  <?php 
                  $drest_id = Session::get('cart_userinfo')['rest_id'];
                  $duser_id = Session::get('cart_userinfo')['user_id'];

                  $getapplycpn = DB::table('rest_cart')		
                  ->where('user_id', '=' ,$duser_id)
                  ->where('rest_id', '=' ,$drest_id)
                  ->where('coupon_status', '=' ,1)
                  ->value('coupon_amount');


                  if($getapplycpn){

                   $coupon_amount = $getapplycpn;

                  }else{

                   $coupon_amount = 0;

                  }

                  ?> 

                  <tr>

                  <td class="couponapl" id="couponapl" style="<?php if($coupon_amount!=0){ ?> display: block<?php }else{ ?> display: none; <?php } ?>">@lang('translation.couponDiscount')
                     <span class="pull-right">$<span id="cpnapply">
                     <?php if($coupon_amount!=0){ echo $coupon_amount; }else{ echo "0"; } ?>
                 </span></span>       
                  </td>


                </tr>
            <tr>
              
              <td class="total"> @lang('translation.grandTotal')
              	<?php 
              	$dgrand_total = number_format(Session::get('cart_order_detail') ['grand_total'],2); 
              	$grandtotald = $dgrand_total-$coupon_amount;
              	?>

                <input type="hidden" value ="{{$total_amt}}" name="grand" class="grand">
                <span class="pull-right grand_total_amt" id="total_amt">$<span id="dgtotal">{{ $grandtotald }}</span></span>
                <input type="hidden" value ="{{ $grandtotald }}" name="grand_total" id="grand_total" class="grand_total">
                <input type="hidden" value ="{{ $user_wallets}}" name="user_wallets" class="user_wallets" id="user_wallets">

               <input type="hidden" value ="{{ $grand_total }}" name="grand_total1" class="grand_total1">
               <input type="hidden" value ="{{ $user_wallets}}" name="user_wallets1" class="user_wallets1" id="user_wallets1">
               <input type="hidden" value ="{{ $grand_total }}" name="coupon_total" id="coupon_total">
                
              </td>
            </tr>
				

					</tbody>

					</table>

					<hr>

					<a class="btn_full" href="javascript:void(0)" id="oder_confrim">@lang('translation.btnConfirmYourOrder')</a>
					<!--button type="button" style="display: none;" class="btn_full" id="submit_button">Confirm your order</button-->
				</div><!-- End cart_box -->

                </div>

			</div><!-- End col-md-3 -->

            

		</div><!-- End row -->

	</div><!-- End container pin-->

</div>

    <!-- End Content =============================================== -->

@stop

@section('js_bottom')

<?php

//$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_url="";
//$paypal_id='votive.amit-business@gmail.com'; // Business email ID

//$paypal_id='votiveyogesh@gmail.com'; // Business email ID
  
//$paypal_id='info@shoutaparty.com';// Business email ID
$paypal_id ="";
//$paypal_id='toshikbuyer@gmail.com';// Business email ID S=>U: toshikparihar23@gmail.com  P: toshik71855

$notify_url = url('/ipn_notify');

?>

<!--https://www.paypal.com/cgi-bin/webscr-->

<!-- <form action="{{$paypal_url}}" method="post" id="payment_frm" >



<input type="hidden" name="cmd" value="_cart">

<input type="hidden" name="upload" value="1">

<input type="hidden" name="userid" value="1">

<input type="hidden" name="business" value="{{$paypal_id}}">

<input type="hidden" name="currency_code" value="USD">

<input type="hidden" name="custom" id="custom_order_payid" value="">

<input type="hidden" name="item_name_1" id="paypal_order_uniqe" value="">
 <input type="hidden" name="quantity_1" value="1">
<input type="hidden" name="amount_1" id="papal_total_amt" value=""-->

<input type="hidden" name="cancel_return" value="{{url('/order_cancel')}}">

<input type="hidden" name="return" value="{{url('/order_success')}}">

<input type="hidden" name="notify_url" value="<?php echo $notify_url;?>" />

	 

	<?php /*?><input type="hidden" name="cancel_return" value="http://localhost/shouta_party/order_cancel">

    <input type="hidden" name="return" value="http://localhost/shouta_party/order_success"><?php */?>

	

<!--<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">-->

</form>



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



<div class="modal fade" id="message_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">@lang('translation.informationMessage')</h4>

      </div>

      <div class="modal-body popup-ctn">

	  		<p id="alert_model_msg"></p>	

      </div>

    </div>

  </div>

</div>

<!-- COMMON SCRIPTS -->



<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>

<script src="{{ url('/') }}/design/front/js/functions.js"></script>

<script src="{{ url('/') }}/design/front/js/validate.js"></script>

<script src="https://js.braintreegateway.com/web/dropin/1.11.0/js/dropin.min.js"></script>

<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/custom.js"></script>

<script>
/*$(document).ready(function(){
	//braintreeUI();
});*/


$("#apply").click(function(){

 var coupon = $('#coupon_code').val();
 //var totalamt = $('#coupon_total').val();
 var totalamt = $('#grand_total').val(); // cartprice

 if(coupon==''){

 $("#coupon_code").css({"border-color":"red"});	

 return false;

 }else{ $("#coupon_code").css({"border-color":""});	

	$("#ajax_favorite_loddder").show();
		
		$.ajax({
			type: "POST",
			url: "{{url('/checkout/coupon_code_apply')}}",
			//data: frm_val,
			data: {'coupon_code':coupon,'total_amt':totalamt}, 
		    dataType: 'json',
				success: function(response) { console.log(response.data[0]);
									
					if(response.status==0)
					{
                         $("#ccmsg").css({"color":"red"});	
                         $("#ccmsg").html("Invalid Coupon Code");
                         $("#ajax_favorite_loddder").hide();

					}else if(response.status==1){

						 var camount = response.data[0].amount; 
  					     var cctotal = totalamt-camount;

						 $(".grand_total").val(cctotal.toFixed(2));
						 $("#dgtotal").text(cctotal.toFixed(2));

						// }

						 $("#cpnapply").text(camount);
						 $("#couponapl").show();                
                         $("#ccmsg").css({"color":"green"});
						 $("#ccmsg").html("Coupon Apply Successfully");
						 $("#ajax_favorite_loddder").hide();

					}else if(response.status==2){

                         $("#ccmsg").css({"color":"red"});	
                         $("#ccmsg").html("Minimum order amount"+" $"+response.data[0].apply_min_amount+" for apply coupon");
                         $("#ajax_favorite_loddder").hide();

					}else{

						 //$("#couponapl").show();                
                         $("#ccmsg").css({"color":"green"});
						 $("#ccmsg").html("Coupon code already applied");
						 $("#ajax_favorite_loddder").hide();

					}			
								
				}
			});

 }


});

    jQuery('#sidebar').theiaStickySidebar({

      additionalMarginTop: 80

    });

$(document).on('change', '.payment_option', function(){
		
	var selected_val = $(this).val();

	//alert(selected_val);
	
	if(selected_val=='credit_card')
	{		
		$(".cart_field").prop('required',true);
		$('#oder_confrim').attr('id', 'submit_button');

		$("#wshowmsg").html("");

	    if($('.rest_payment_wallet').is(':checked'))
        {  }else{

        var total =	$(".grand_total").val(); 
	    var wallets =	$(".user_wallets").val();

        total = parseInt(total)+parseInt(wallets);
        wallets = 0;
        
        $(".grand_total").val(total);
        $(".user_wallets").val(wallets); 

        }

		braintreeUI();
	}
	else if(selected_val=='cash')
	{

		$(".rest_payment_wallet").prop("checked", false);

		$(".cart_field").prop('required',false);
		$('#submit_button').attr('id', 'oder_confrim');
		//var button ="";
	}
	else if(selected_val=='wallet')
	{

	  if($('.rest_payment_cart').is(':checked'))
        { 

        var total =	$(".grand_total1").val(); 
	    var wallets =	$(".user_wallets1").val();
        
        $(".grand_total").val(total);
        $(".user_wallets").val(wallets); 

        }

       if($('.rest_payment_wallet').is(':checked'))
        {}else{ 

        var total =	$(".grand_total").val(); 
	    var wallets =	$(".user_wallets").val();

        total = parseInt(total)+parseInt(wallets);
        wallets = 0;
        
        $(".grand_total").val(total);
        $(".user_wallets").val(wallets); 

        }

        $(".rest_payment_cash").prop("checked", false);

		$(".cart_field").prop('required',false);
		$('#submit_button').attr('id', 'oder_confrim');
		//var button ="";
	}
});	


$(document).on('click', '#oder_confrim', function(){

 //alert('Test');	


if($('.rest_payment_option').is(':checked'))
{
	
 if($('.rest_option:checked').val()=='cash')
	{			
	
		$("#ajax_favorite_loddder").show();
		var frm_val = 'method_type=frm_val';
		$.ajax({
			type: "POST",
			url: "{{url('/checkout/cash_payment_process')}}",
			//data: frm_val,
			data: {'_token':"<?php echo csrf_token(); ?>"}, 
		   dataType: 'json',
				success: function(msg) { 
				//delivery_pick_time  					
					if(msg.order_id)
					{
							window.location.href='<?php echo url('/order_detail')?>';
					}			
								
				}
			});

	}else if($('.rest_option:checked').val()=='wallet')
	   {
 
	    var wtxn = $("#user_wallets").val();

	    if(wtxn!=0){

	    $("#wshowmsg").html("Your wallet ballance is insufficient");
	    	
	    return false; 

	    }

		$("#ajax_favorite_loddder").show();
		var frm_val = 'method_type=frm_val';
		$.ajax({
			type: "POST",
			url: "{{url('/checkout/wallet_payment_process')}}",
			//data: frm_val,
			data: {'_token':"<?php echo csrf_token(); ?>"}, 
		   dataType: 'json',
				success: function(msg) {
				//delivery_pick_time  					
					if(msg.order_id)
					{
							window.location.href='<?php echo url('/order_detail')?>';
					}			
								
				}
			});


	}
	

}
else
{

	$('#alert_model_msg').html("Please Select Payment Method");			

	$('#message_model').modal('show');

}



});

function braintreeUI(){ 
	var button = document.querySelector('#submit_button');

	var total =	$(".grand_total").val(); 
	var wallets =	$(".user_wallets").val();
	
	if(button!= "" || button!= null){
	braintree.dropin.create({
      authorization: "{{ Braintree_ClientToken::generate() }}",
      container: '#dropin-container',
      paypal: {
	    flow: 'checkout',
	    amount: total,
	    currency: 'USD'
	  }
    }, function (createErr, instance) {
      button.addEventListener('click', function () {

      	instance.requestPaymentMethod(function (err, payload) {
	        $.get('{{ route('payment.process') }}', {payload:payload,amt:total,uwallet:wallets}, function (response) {
	        	console.log(response);
	        	$("#ajax_favorite_loddder").show();
	        	//alert(response.transaction.paymentInstrumentType);
	            if (response.success){
	            	if(response.transaction.paymentInstrumentType=="credit_card"){
		            	$.ajax({
						type: "POST",
						url: "{{url('/checkout/cart_payment_process')}}",
						//data: frm_val,
						data: {'_token':"<?php echo csrf_token(); ?>", 'values':response},
					   	dataType: 'json',
							success: function(msg) {  
							console.log(msg); 

								if(msg.status=='1')
								{
										window.location.href='<?php echo url('/order_detail')?>';
								}		
								else if(msg.status=='0')
								{	console.log(msg);
									var transactionid = msg.trans_id;
									var amt = msg.amt;
									 $.get('{{ route('payment.cancel') }}', {transactionid:transactionid,amt:amt}, function (response) {
									 	console.log(response);
									 });
									$("#alert_model_msg").html("Order didn't processed!");
									$("#ajax_favorite_loddder").hide();
									//$("#message_model").modal('show');
								}else{
									$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
									$("#ajax_favorite_loddder").hide();
									$("#message_model").modal('show');	
								}		
											
							}
						});
			        }else if(response.transaction.paymentInstrumentType=="paypal_account"){

			        	
						$.ajax({
							type: "POST",
							url: "{{url('/checkout/paypal_payment_process')}}",
							//data: frm_val,
							data: {'_token':"<?php echo csrf_token(); ?>"},
						   dataType: 'json',
								success: function(msg) {
								//delivery_pick_time  
									console.log(msg);				
									if(msg.status==1){	
										window.location.href='<?php echo url('/order_detail')?>';		
									}else if(msg.status=='0')
									{	console.log(msg);
										var transactionid = msg.trans_id;
										var amt = msg.amt;
										 $.get('{{ route('payment.cancel') }}', {transactionid:transactionid,amt:amt}, function (response) {
										 	console.log(response);
										 });
										$("#alert_model_msg").html("Order didn't processed!");
										$("#ajax_favorite_loddder").hide();
										//$("#message_model").modal('show');
									}else{
										$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
										$("#ajax_favorite_loddder").hide();
										$("#message_model").modal('show');	
									}				
								}
							});
			        }	


	              console.log(response);
	            } else {
	              	$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
					$("#ajax_favorite_loddder").hide();
					$('#message_model').modal('show');
	              console.log(response);
	              
	            }
	        }, 'json');
        });
      });
    });
}

 /*braintree.setup(
    "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIwNzUxYzZlYzBkMzAyYjYzMjM3NDFkZDRiYzMxNDc2MDQ1NTM0ZTE2YTZkOTVhMTMyZGRhY2E4ZDExYWNlOGYwfGNyZWF0ZWRfYXQ9MjAxOC0wNi0wN1QxMTo1NjoxMy4xNzg4NDEyOTYrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=",
    "dropin", {
      container: "payment-form"
    });
*/
  }



</script>





@stop