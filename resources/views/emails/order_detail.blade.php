<style type="text/css">
  p{
    padding: 5px;
  }
  label {
    display: inline-block;
    margin-bottom: 5px;
    font-weight: bold;
}

td.lcolrs {
    text-align-last: end;
}
.billing td {
 border: 1px solid #ccc;
 border-collapse: collapse;
}
td, th {
 text-align: left;
 padding: 4px !important;
 width: 10%;
}
.billing th {
 border: 1px solid #ccc;
  border-collapse: collapse;
  text-align: center;
}
.billing table {
 border: 1px solid #ccc;
 border-collapse: collapse;
 margin-top: 10px;
}
</style>


<div class="prduct_detail billing">
    <table style="width: 100%;">
      <tr>
        <td></td>
        <td bgcolor="#FFFFFF ">
          <div style="padding: 15px; max-width: 600px;margin: 0 auto;display: block; border-radius: 0px;padding: 0px;">

          <header>
          <div class="row">
          <div class="col">
                       
          <h2 style="text-transform:uppercase;color:#ec7324!important;text-align:center;font-size:30px;margin:2px; margin-bottom: 10px; margin-top: 10px;">{{$business_name}}</h2>
                           
          <div class="col company-details" style="text-align:center;line-height: 21px;">
          <p style="font-family:Arial,Helvetica,sans-serif;margin:0px;text-align:center">{{$vendor_maddress}} <br> {{$vendor_address}} <br> {{$vendor_city}}, {{$vendor_state}} {{$vendor_zipcode}} <br>{{$mob_no}}</p><!-- <p style="font-family: Arial, Helvetica, sans-serif; margin:0px; text-align: center;">  </p> --><!-- <p style="font-family: Arial, Helvetica, sans-serif; margin:0px; text-align: center;">Lic# {{$driver_license}}</p> -->
                            <div class="address" style="font-family: Arial, Helvetica, sans-serif; font-style: 14px; padding-bottom: 10px;">Order Date/Time: {{$date_time}}  <br><b style="margin-top: 7px;float: left;text-align: center;width: 100%;">Permit/License # : {{$driver_license}} </b></div>
  
                           <div class="address" style="font-family: Arial, Helvetica, sans-serif; font-style: 14px; border-bottom: 1px dashed #ddd; padding-bottom: 25px; margin-bottom: 0px;"> </div>
                    </div>
                    </div>
                   
                </div>
            </header>
            
            <table style="padding: 10px;font-size:14px; width:100%;">
              <tr>
                <td style="font-size:24px; width:100%;">
                    <p style="text-align: center; font-weight: bold; padding: 0px; margin:0px; "> ORDER DETAILS/INVOICE</p>
                    <p style="font-size: 16px; font-weight: 600;">Order# {{$order_id}}</p>
                  
                    <!-- <p><a href="#" style="color:blue;font-size:12px;">Click Here </a></p> -->
                    
       <table style="padding: 5px;font-size:14px; width:100%;">

                 <tr> 
                  <td style="font-size: 15px; text-transform: uppercase;"><b>CUSTOMER ID<b> </td>
                   <td style="font-size: 15px; text-transform: uppercase;"><b>MERCHANT/DRIVER ID</b></td>

                   </tr>

                  <tr> 
                  <td style="font-size: 14px; line-height: 20px;"> {{$user_id}} <br>{{$user_name}}<br>
                    
                    {{$address}} <br>{{$city}}, {{$state}}, {{$zip}}, {{$country}}<br>{{$user_mob}}<br><a href="#">{{$user_email}} </a> </td>

                  <td style="font-size: 14px; line-height: 20px;"> {{$vendor_id}} <br>{{$vendor_name}}<br>{{$vendor_maddress}} <br> {{$vendor_address}} <br> {{$vendor_city}}, {{$vendor_state}} {{$vendor_zipcode}}<br>{{$mob_no}}<br><a href="#">{{$vendor_email}} </a> </td>
                  </tr>
                    
                <tr> 
                <td style="font-size: 16px; text-transform: uppercase; padding-top: 20px;">
                <?php if($pickupdel=='delivery'){ ?><b>DELIVERY ADDRESS<b> <?php }else{ ?><b>Pick Up Address</b> <?php } ?></td>
                <td style="font-size: 16px; text-transform: uppercase; padding-top: 20px;"><b>PAYMENT TYPE: {{$payment_method}} {{$accountNumber}}<b></td> 
                </tr>

                  <tr> 
                  <td style="font-size: 14px; line-height: 20px;">
                  <?php if($pickupdel=='delivery'){ ?>  
                    {{$addressd}}<br> {{$cityd}}, {{$stated}}, {{$zipd}}, {{$countryd}}<br>{{$mob_no}}<br>
                  <?php }else{ ?>{{$pickup_add}}<?php } ?>
                  </td>

                  <td style="font-size: 15px; line-height: 20px;"> <p style="width: 200px;">The cannabis excise taxes are included in the total amount of this invoice. </p></td>
                  </tr>

                <?php if($pickupdel=='delivery'){ ?>

                <tr> 

                <td style="font-size: 16px; text-transform: uppercase; padding-top: 20px;">

                <b>Pick Up Address</b></td>

                <td style="font-size: 16px; text-transform: uppercase; padding-top: 20px;"><b>&nbsp;<b></td> 

                </tr>

                  <tr> 

                  <td style="font-size: 14px; line-height: 20px;">

                  {{$pickup_add}}

                  </td>

                  <td style="font-size: 15px; line-height: 20px;"> <p style="width: 200px;">&nbsp;</p></td>

                  </tr>

                  <?php } ?>

                  </table>

                   <table style="padding: 5px;font-size:14px;width:100%;border-top: 2px dashed #505050;padding-top: 26px;margin-top: 19px; border-spacing: 0px;">

                 <tr> 
                
                  <td colspan="2" style="font-size: 15px; text-transform: uppercase; border-bottom: 1px solid #ccc;"><b>Product Name  </b></td>
                  <td style="font-size: 15px; text-transform: uppercase; border-bottom: 1px solid #ccc;"><b>Unit Price </b></td>
                  <td style="font-size: 15px; text-transform: uppercase; border-bottom: 1px solid #ccc;"><b>QTY<b> </td>
                  <td style="font-size: 15px; text-transform: uppercase; border-bottom: 1px solid #ccc;"><b>Total </b></td>
                  
                 </tr>

                 <?php $subtotal = 0;?>
                <?php foreach( $item_info as $item){ ?>

                    <tr>
                   
                   
                        <td class="lcolrs" colspan="2">
                          <p>
                            {{$item['name']}}
                          </p>
                        </td>

                           <td class="lcolrs">
                             <p>
                             <?php if(!empty($item['price'])){ ?> 
                               ${{number_format($item['price'],2)}}
                             <?php } ?>
                             </p>

                             <p><?php $total_pp = $item['price']*$item['quantity'];?></p>
                           </td>

                           <td class="lcolrs">
                             <p>
                               {{$item['quantity']}}
                             </p>
                           </td>
                           <td class="lcolrs">
                             <p>
                               ${{ number_format($total_pp, 2) }}
                             </p>
                           </td>
                    </tr>

                     <?php $subtotal = $subtotal+$total_pp?>
                      <?php  } ?>

                       <tr>
                      
                      <td colspan="4"><b>Product Total</b></td>
                      <td colspan="1" class="lcolrs">${{ number_format($subtotal, 2) }}</td>
                      </tr>

                       <tr>
                         
                         <td colspan="4"> <label>Delivery Fee</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_delivery_fee, 2) }}</span></td>
                     </tr>

                     <tr>
                        
                         <td colspan="4"> <label>Discount</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>-${{ number_format($order_promo_cal, 2) }}</span></td>

                     </tr>

                     <tr>
                         
                         <td colspan="4" style="border-top:1px solid #ccc;"> <b>Subtotal</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ccc;"><span>${{ number_format($order_subtotal_amt, 2) }}</span></td>
                     </tr>

                      <tr>
                         
                         <td colspan="4" style="border-top:1px solid #ccc;"> <label>Excise Tax of Subtotal ({{$excise_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ccc;"><span>${{ number_format($order_excise_tax, 2)}}</span></td>
                     </tr>

                      <tr>
                        
                         <td colspan="4"> <label>City Tax of Subtotal ({{$city_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_city_tax, 2)}}</span></td>
                     </tr>

                     <tr>
                         
                         <td colspan="4"> <label>Sales Tax of Subtotal  ({{$sales_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_service_tax, 2)}}</span></td>
                     </tr>

                     <tr>
                         
                         <td colspan="4" style="border-top: 1px solid #ccc;"> <b>Total Paid Amount</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top: 1px solid #ccc;"><span>${{ number_format($order_total, 2) }}</span></td>
                     </tr>

                 </table>

            </table>
          </div>
