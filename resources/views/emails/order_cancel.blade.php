<html>

<head>

<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

</head>

<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

        <div style="max-width:550px;width:100%;padding:20px 20px 0px;margin:30px auto 0;overflow:hidden;background:#e2e2e2; text-align:center;color: #fff;">

            <div style="padding:30px;background:#231f20;">

                <div style="text-align:center">

                    <a href="https://www.grambunny.com/" style="display: block;overflow:hidden;padding: 0 0 0 0;">

                        <img style="max-width:160px;width: 100%;" src="https://www.grambunny.com/public/assets/images/footer_logo.png">

                    </a>

                </div>

                <div style="border-bottom:2px solid #d2d2d2;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

                    <h2 style="margin:0px 0px;">Hi,{{$vendor_name}}</h2>

                </div>

                <div style=" display:block; overflow:hidden">

                    <p style=" margin:0 0 8px 0">&nbsp;</p>

                    <p>{{$order_msg}}</p>

                </div> 

                <div>

                    <a href="https://www.grambunny.com/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">Visit site : - grambunny</a>

                </div>

            </div>

        </div>

        <div style="max-width:550px;width:100%;padding:15px 20px 20px;margin:0 auto 30px;background:#e2e2e2;">

            <div style="background: #fff;padding: 15px;">

                <p style="border-bottom:1px solid #ccc; padding:0px 5px 5px 5px; margin-bottom:0px !important; margin-top: 5px;">

                    <strong>Return Reason: </strong>

                    <span style="margin-left:10px;">{{$return_reason}}</span>

                </p>

            </div>

        </div>

    </div>

</body>

</html>