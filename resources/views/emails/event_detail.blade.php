<html>

<head>

<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

</head>

<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

 <?php  
  $pdf_file = $generated_filename.'.pdf';
  $pdf_file_path = asset("/public/fpdf1/gen_file")."/".$pdf_file; ?>   

    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

    <div style="max-width:550px;width:85%;padding:30px;margin:30px auto 0;border:1px solid #212121;overflow:hidden;background-position:center;background-size:cover;text-align: center;color:#181818f5;">

            <div style="padding:30px;border:1px solid #fff;background:#FFF;">

                <div style="text-align:center">

                    <a href="https://grambunny.com/" style="display: block;overflow:hidden;padding: 0 0 0 0;">

                        <img style="max-width:160px;width: 100%;" src="https://grambunny.com/public/assets/images/footlogo.jpg">

                    </a>

                </div>

                <div style="border-bottom:2px solid #212121;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

                    <h2 style="margin:0 0;">Your booking is confirmed!</h2>

                </div>

                <div style=" display:block; overflow:hidden">

                <p> Order ID# &nbsp; {{$order_id}}</p>

                <p> Ticket ID# {{$ticket_id}}</p>

      <p><a style="color: #e5313c;font-size: 14px;font-weight: 600;padding:8px 15px;border:2px solid #e5313c;display:inline-block;" href="<?php echo $pdf_file_path; ?>">Download Ticket</a></p>

                </div>

                <div>

                    <a href="https://grambunny.com/" style="text-decoration:none;color:#212121;padding-bottom:2px;display:inline-block;border-bottom:1px solid #212121;">Visit site : - Grambunny</a>

                </div>

            </div>

        </div>        

    </div>

</body>

</html>
