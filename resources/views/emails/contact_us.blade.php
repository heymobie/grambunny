<html lang="en-US">
    <head>
        <meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

</head>

<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

        <div style="max-width:550px;width:100%;padding:20px 20px 0px;margin:30px auto 0;overflow:hidden;background:#e2e2e2; text-align:center;color: #fff;">

            <div style="padding:30px;background:#ffffff;">

                <div style="text-align:center">

                    <a href="https://www.grambunny.com/" style="display: block;overflow:hidden;padding: 0 0 0 0;">

        <img style="max-width:160px;width: 100%;" src="https://www.grambunny.com/public/assets/images/gramlogo.jpg">

                    </a>

                </div>


                <div style="border-bottom:2px solid #d2d2d2;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

                    <h2 style="margin:0px 0px;color:#222;">Hi, Admin</h2>

                </div>

                <div style=" display:block; overflow:hidden">

                    <p style=" margin:0 0 8px 0">&nbsp;</p>

                    <p style="color:#222;">Customer Contact Message</p>

                </div> 

                <div>

                    <a href="https://www.grambunny.com/" style="text-decoration:none;color:#222;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">Visit site : - grambunny</a>

                </div>

            </div>

        </div> 


        <div style="max-width:550px;width:100%;padding:15px 20px 20px;margin:0 auto 30px;background:#e2e2e2;">

            <div style="background: #fff;padding: 15px;">

                <p style="border-bottom:1px solid #ccc; padding:0px 5px 5px 5px; margin-bottom:0px !important; margin-top: 5px;">

                    <strong>Name</strong>

                    <span style="float:right;">{{$name}}</span>

                </p>

                <p style="border-bottom:1px solid #ccc; padding:0px 5px 5px 5px; margin-bottom:0px !important; margin-top: 5px;">

                    <strong>Email</strong>

                    <span style="float:right;">{{$email}}</span>
                </p>


                <p style="border-bottom:1px solid #ccc; padding:0px 5px 5px 5px; margin-bottom:0px !important; margin-top: 5px;">

                    <strong>Subject</strong>

                    <span style="float:right;">{{$subject}}</span>

                </p>

             
                <p style="border-bottom:1px solid #ccc; padding:0px 5px 5px 5px; margin-bottom:0px !important; margin-top: 5px;">

                    <strong>Message</strong>

                    <span style="float:right;">{{$messagess}}</span>

                </p>

            </div>

        </div>


    </div>

</body>

</html>