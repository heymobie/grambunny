

<html>

<head>

<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

</head>

<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto 0;border:1px solid #e2e2e2;overflow:hidden;background: url(https://www.grambunny.com/design/front/img/resto.jpg);background-position:center;background-size:cover;text-align: center;color: #fff;">



            <div style="padding:30px;border:1px solid #fff;background:rgba(31, 0, 0, 0.72);">

                <div style="text-align:center">

                    <a href="https://www.grambunny.com/" style="display: block;overflow:hidden;padding: 0 0 0 0;">

                        <img style="max-width:160px;width: 100%;" src="https://www.grambunny.com/design/front/img/logo.png">

                    </a>

                </div>

                <div style="border-bottom:2px solid #d2d2d2;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

                    <h2 style="margin:0 0;">Hi <?php echo $rest_detail[0]->rest_name;?></h2>

                </div>



                <div style=" display:block; overflow:hidden">

                    <p style=" margin:0 0 8px 0">&nbsp;</p>

                    <p>

						Your have received order. Order no. is <strong>(<?php echo  $order_detail[0]->order_id;?>)(<?php echo $order_detail[0]->order_uniqueid;?>)</strong>. Please click on the confirm button to confirm this order.</br>

						



					

					</p>

					<center><button type="button" class="btn btn-danger" style="width: 28%; height: 40px; background-color: #e81e2a; padding: 12px; color: #fff; margin-bottom:10px; text-align: center; font-weight: 700; font-size: 13px; border-radius: 3px; border: 0; outline: 0; cursor: pointer; "><a  href="https://www.grambunny.com/vendor_confirm?id=<?php echo  $order_detail[0]->order_id;?>" style="color: #fff !important" >Confirm Order</a></button></center>

                </div>

                <div>

                    <a href="https://www.grambunny.com/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">Visit site : - grambunny</a>

                </div>

            </div>

        </div>        

		<div style="max-width:550px;width:85%;padding:30px;margin:0 auto 30px;border:1px solid #e81e2a;background: #e81e2a">

        	<div style="background: #fff;padding: 15px;">

        		<!--<p style="text-align:center;">Your food should arrive around 7:05 - 8:00</p>-->

        		<h2 style="text-align:center;"><?php echo $rest_detail[0]->rest_name;?></h2>

        		<p>

        			<strong>Order Id</strong><span style="float:right;"><?php echo  $order_detail[0]->order_id;?></span>

        		</p>

        		<p>

        			<strong>Order Unique Id</strong><span style="float:right;"><?php echo $order_detail[0]->order_uniqueid;?></span>

        		<p>

        		<p>

        			<strong>Name</strong><span style="float:right;"><?php echo $order_name;?></span>

        		</p>

				<p>

					<strong>Email Id</strong><span style="float:right;"><?php if(isset($order_email)){echo $order_email;}else{}?></span>

				</p>

				<p><strong>Contact Number</strong><span style="float:right;"><?php if(isset($order_tel)){echo $order_tel;}else{}?></span>

				</p>

				<p>

					<strong>Order Address</strong><span style="float:right;"><?php if(isset($order_address)){echo $order_address;}else{}?></span>

				</p>



				

        		<p><strong>Order Instructions</strong> <span><?php echo $order_detail[0]->order_instruction;?></span></p>

        		<p><strong>Payment Method <br> </strong> <span><?php echo $order_detail[0]->order_pmt_method;?></span></p>

				

				

				

				<?php $cart_price = 0;

				$cart = json_decode($order_detail[0]->order_carditem);

				

				

				?>

				

				

				

				

				<?php 

				if(isset($cart) && (count($cart)))

				{

					

				foreach($cart as $item)

				{

					 

				$cart_price = $cart_price+$item->price;

				?>

				<p>

						<strong><?php echo $item->qty; ?>x</strong> <?php echo $item->name;?> </strong>

						<span style="float:right;">$<?php echo number_format($item->price,2);?></span>

				</p>	

				

				<?php 

				if(!empty($item->options->addon_data)&&(count($item->options->addon_data)>0)){

				 	foreach($item->options->addon_data as $addon){

						

							$cart_price = $cart_price+$addon->price;

						?>

						<p>

							<strong><?php echo $addon->name; ?></strong>

							<span style="float:right;"><?php if($addon->price>0){

							echo "$".number_format($addon->price,2);}?></span>

						</p>

				<?php } ?>

					

				<?php }

				}

				}?>

				 			

				 

					

				







				<p>

					<strong> Subtotal  </strong>

					<span style="float:right;"><?php echo $order_detail[0]->order_subtotal_amt;?></span>

                </p>

				

				

				<?php if($order_detail[0]->order_promo_cal>0){?>

			

               <p>

					<strong>Discount</strong>

					<span style="float:right;">-$<?php echo $order_detail[0]->order_promo_cal,2;?></span>

                </p>

				

				<?php } ?>

				

				

		

				<?php if($order_detail[0]->order_service_tax>0){?>

			

                <p>

					<strong> Sales Tax </strong>

					<span style="float:right;">$<?php echo $order_detail[0]->order_service_tax;?></span>

                </p>

				

				<?php }?>



				<!-- <?php if($order_detail[0]->order_remaning_delivery>0){ ?>

				

					

				<p>  

					<strong> $<?php echo $order_detail[0]->order_min_delivery;?> min </strong>

					<span> Remaining<span  style="float:right;">$<?php echo $order_detail[0]->order_remaning_delivery;?></span>

					

                </p>

				<?php  } ?> -->

				<?php if($order_detail[0]->order_min_delivery>0){?>

				

					

				<p>  

					<strong> Delivery Fee </strong>

					<span  style="float:right;"><?php echo "$".$order_detail[0]->order_min_delivery;?></span>

					

                </p>

				<?php }?>



		

				

				<p>

					<strong> TOTAL</strong>

					<span style="float:right;">$<?php echo $order_detail[0]->order_total;?></span>

				</p>



				<p>

					<strong> Tip Amount</strong>

					<span style="float:right;">$<?php echo $order_detail[0]->order_tip;?></span>

				</p>



				<p>

					<strong> GRAND TOTAL</strong>

					<span style="float:right;">$<?php echo $order_detail[0]->order_tip+ $order_detail[0]->order_total;?></span>

				</p>

				<p style="display: inline-block; width: 100%;">

					<span style="float:right;">

						<button type="button" class="btn btn-danger" style="width: 100%; height: 40px; background-color: #e81e2a; padding: 12px;  color: #fff; text-align: center; font-weight: 700; font-size: 13px; border-radius: 3px; border: 0; outline: 0; cursor: pointer; display: inline-block;"><a href="https://www.grambunny.com/vendor_confirm?id=<?php echo  $order_detail[0]->order_id;?>" style="color: #fff !important">Confirm Order</a></button>

					</span>

				</p>

        	</div>

        </div>

    </div>

</body>

</html>