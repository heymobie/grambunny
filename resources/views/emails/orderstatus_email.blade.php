<html>

<head>

<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

</head>

<body style="font-family:'open Sans';font-size: 14px; line-height:20px;">

    <div style="padding: 0 10px;max-width: 600px;margin: 0 auto;">

        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto 0;border:1px solid #e2e2e2;overflow:hidden;background: url(https://www.grambunny.com/design/front/img/resto.jpg);background-position:center;background-size:cover;text-align: center;color: #fff;">



            <div style="padding:30px;border:1px solid #fff;background:rgba(31, 0, 0, 0.72);">

                <div style="text-align:center">

                    <a href="https://www.grambunny.com/" style="display: block;overflow:hidden;padding: 0 0 0 0;">

                        <img style="max-width:160px;width: 100%;" src="https://www.grambunny.com/design/front/img/logo.png">

                    </a>

                </div>

                <div style="border-bottom:2px solid #d2d2d2;margin:15px auto 15px;padding:10px;display:block;overflow:hidden;max-width: 400px;">

                    <h2 style="margin:0 0;">Hi,{{ $user_name}}</h2>

                </div>



                <div style=" display:block; overflow:hidden">

                    <p style=" margin:0 0 8px 0">&nbsp;</p>

                    <p>

						{{ $email_content }}

					

					</p>

                </div>

                <div>

                    <a href="https://www.grambunny.com/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">Visit site : - grambunny</a>

                </div>

            </div>

        </div>   

		<div style="max-width:550px;width:85%;padding:30px;margin:0 auto 30px;border:1px solid #e81e2a;background: #e81e2a">

        	<div style="background: #fff;padding: 15px;">

        		<!--<p style="text-align:center;">Your food should arrive around 7:05 - 8:00</p>-->

        		<h2 style="text-align:center;">{{$rest_detail[0]->rest_name}}</h2>

				

        		<p><strong>Order Instructions</strong> <span>{{$order_detail[0]->order_instruction}}</span></p>

        		<p><strong>Payment Method <br> </strong> <span>{{$order_detail[0]->order_pmt_method}}</span></p>

				

				

				

				<?php $cart_price = 0;

				$cart = json_decode($order_detail[0]->order_carditem);

				

				?>

@if(isset($cart) && (count($cart)))

				

				 @foreach($cart as $item)

				<?php  $cart_price = $cart_price+$item->price;?>

				<p>

						<strong>{{$item->qty}}x</strong> {{$item->name}} </strong>

						<span style="float:right;">${{number_format($item->price,2)}}</span>

				</p>	

				

				@if(!empty($item->options->addon_data)&&(count($item->options->addon_data)>0))

				 	@foreach($item->options->addon_data as $addon)

						<?php 

							$cart_price = $cart_price+$addon->price;

						?>

						<p>

							<strong>{{$addon->name}}</strong>

							<span style="float:right;">@if($addon->price>0)${{number_format($addon->price,2)}}@endif</span>

						</p>

				

					 @endforeach

				 @endif 

				 			

				 

					

				 @endforeach

@endif







				<p>

					<strong> Subtotal  </strong>

					<span style="float:right;">{{$order_detail[0]->order_subtotal_amt}}</span>

                </p>

				

				

				@if($order_detail[0]->order_promo_cal>0)

			

               <p>

					<strong>Discount</strong>

					<span style="float:right;">-${{$order_detail[0]->order_promo_cal,2}}</span>

                </p>

				

				@endif

				

				

		

				@if($order_detail[0]->order_service_tax>0)

			

                <p>

					<strong> Service Tax </strong>

					<span style="float:right;">${{$order_detail[0]->order_service_tax}}</span>

                </p>

				

				@endif



				@if($order_detail[0]->order_remaning_delivery>0)

				

					

				<p>  

					<strong> ${{$order_detail[0]->order_min_delivery}} min </strong>

					<span> Remaining <span  style="float:right;">${{$order_detail[0]->order_remaning_delivery}}</span></span>

					

                </p>

				@endif



		

				

				<p>

					<strong> TOTAL</strong>

					<span style="float:right;">${{$order_detail[0]->order_total}}</span>

				</p>

				<p>

					<strong> Tip Amount</strong>

					<span style="float:right;">${{$order_detail[0]->order_tip}}</span>

				</p>

				<p>

					<strong> GRAND TOTAL</strong>

					<span style="float:right;">${{$order_detail[0]->order_tip+ $order_detail[0]->order_total}}</span>

				</p>





        	</div>

        </div>     

    </div>

</body>

</html>