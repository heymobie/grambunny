@extends("layouts.grambunny")

@section("styles")

{{-- styles --}}

<link rel="stylesheet" type="text/css" href="css/xzoom.css" media="all" /> 

<link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">

<style type="text/css">
  .sf-about-box .qtycss .buycss {

display: block;
margin-bottom: 0px;
margin-top: 1px;
}

  input#promo_code {
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
}
.myOrderDtelBtn{
  height: 46px !important;
}
.input-group-append button#btnRedeem {
    background: #ed1c24 !important;
    border: 1px solid #ed1c24 !important;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
}
.input-group {
   border: 2px solid #ed1c24;
    border-radius: 6px;
    padding: 4px;
}
input#quantity {
    height: 29px;
}
button.quantity-left-minus.btn-number {
    border: unset;
    background-color: unset;
}

input#quantity {
    border: unset;
    border-left: 1px solid #ed1c24;
    border-right: 1px solid #ed1c24;
    text-align: center;
    color: #ed1c24;
}

span.input-group-btn {
    border: unset !important;
}

button.quantity-right-plus.btn-number {
    border: unset;
    background-color: unset;
    color: #ed1c24;
}

span.glyphicon.glyphicon-plus {
    font-size: 16px;
    font-weight: normal !important;
    padding-top: 2px;
}

span.glyphicon.glyphicon-minus {
    font-size: 16px;
    padding-top: 2px;
    color: #ed1c24;
}

.input-group {}
  @media only screen and (max-width: 640px) {
.myOrderDtelBtn {
    width: auto !important;
}
  }
  .review-block {
    background-color: #f5f5f5;
    border: 1px solid #ddd;
    padding: 15px;
    border-radius: 3px;
    margin-bottom: 15px;
    margin-top: 10px;
}
/*.myOrderDtelBtn {
    width: 22% ;
}*/
button.btn.btn-sm.btn-primary.sf-review-btn {
    float: left;
   /* width: 20%;*/
}
.myOrderDtelBtn a{

    border: 1px solid #231f20;

    background-color: #231f20;

    color: #fff;

    font-weight: 700;

    border-radius: 25px;

    padding: 8px 25px;

    min-width: 100px;

    cursor: pointer;

    text-decoration: none;

}

.myOrderDtelBtn a:hover {

    background-color: #231f20;
    color: #fff;

}

.myOrderDtelBtn {
    float: none;
    text-align: center;
    vertical-align: -8px;
    margin-left: 30px;
}

#qtyidmsg p{ margin-top: 5px; margin-bottom: -10px !important; }

</style>



@endsection

@section("content")

  {{-- content goes here --}}

  <?php //$date = date('Y-m-d H:i:s'); echo $date; ?>

<section class="section-full bg-gray about-info" id="sf-provider-info">

    <div class="container">

      <div class="row">

        <div class="col-md-4 product-left">

          <div class="xzoom-container">
            <div class="img_wdt_product">
              <?php if(!empty($item->images[0]->path)){ ?>
          <img class="xzoom" id="xzoom-default" src="{{ $item->images[0]->path }}" xoriginal="{{ $item->images[0]->path }}" />
        <?php }?>
        </div>
            <div class="xzoom-thumbs">   

                @foreach($item->images as $image)

            <a href="{{ $image->path }}"><img class="xzoom-gallery" width="80" src="{{ $image->path }}"  xpreview="{{ $image->path }}" title=""></a>

                @endforeach

            </div>



          </div> 

          <!-- <div class="sf-provider-des">

            <div class="sf-thum-bx overlay-black-light">

              <img src="https://images-na.ssl-images-amazon.com/images/I/71Xp-K4MMBL._SX569_.jpg" alt="">

            </div>

            <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button>

          </div> -->

        </div>

        <div class="col-md-8 product-rigt">

          <div class="sf-about-box product-width">

            <h2 class="sf-title">{{ $item->name }}</h2>

            <h6 class="product-title-btype"> UPC# {{ $item->product_code }}</h6>

            <div class="star-rating">

              <div class=" text-warning">

                <?php 

                    //$userRatingInitial = 0;

                    //if($totalUserRating != '0')

                    //$userRatingInitial = $totalUserRating / count($ratingReviewData);

                    //$userRating = round($userRatingInitial);

                  $userRatingInitial = $userRating = $item->avg_rating;

                  if($userRating == 1){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }

                  else if($userRating == 2){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($userRating == 3){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($userRating == 4){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($userRating == 5){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                  }else if($userRating == '0'){

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                  }

                ?>

              </div>

              <span> {{$userRatingInitial}} out of 5 stars | {{ $item->rating_count }} ratings </span>
              
            </div>

           <h6 class="product-title-btype"> BRAND: {{ $item->brands }} | TYPE: {{ $item->types }} @if(!empty($item->potency_thc))| THC: {{ $item->potency_thc }}% @endif @if(!empty($item->potency_cbd))| CBD: {{ $item->potency_cbd }}% @endif</h6>   

            <div class="proPrice">

              <div class="cutPrice">${{ $item->price }} <span class="unitrate" style="font-size: 18px;">( {{ $item->unit }} )</span></div>

              {{-- <div class="mainPrice">$110.00</div> --}}

            </div>


          <?php if($item->quantity >0){ ?>
          <div class="avilblStock">In Stock</div>
          <?php }else{ ?>  
          <div class="avilblStock">Out of stock</div>
          <?php } ?>

          <h6 class="product-title">product information</h6>
              <p class="descrip-tex-p">

                {{ $item->description }}

              </p>

         <!--{!! csrf_field() !!}-->  

               
         <div class="quantity-new-plus">
           <div class="col-lg-2" style="padding-left: 0px; float: left; margin-left: 0px;">
                                        <div class="input-group">
                                    <span class="input-group-btn">

                                    <?php if($item->quantity >0){ ?>

                                        <button type="button" class="quantity-left-minus  btn-number"  data-type="minus" data-field="">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>

                                    <?php }else{ ?> 

                                    <button type="button" class="quantity-left-minus  btn-number"  data-type="minus" data-field="" disabled>
                                    <span class="glyphicon glyphicon-minus"></span>
                                    </button>

                                    <?php } ?> 

                                    </span>
                  
                                    <input type="hidden" name="remainingqty" id="remainingqty" value="<?php  echo $item->quantity; ?>">

                                <input type="hidden" name="cartqtyold" id="cartqtyold" value="<?php  echo $cartqtyold; ?>">


                                  <?php if($item->quantity >0){ ?>

                                    <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="<?php  echo $item->quantity; ?>">

                                  <?php }else{ ?> 

                                    <input type="text" id="quantity" name="quantity" class="form-control input-number" value="0" min="1" max="<?php  echo $item->quantity; ?>"> 
                                  
                                  <?php } ?> 

                                    <span class="input-group-btn">

                                      <?php if($item->quantity >0){ ?>

                                        <button type="button" class="quantity-right-plus  btn-number" data-type="plus" data-field="">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>

                                      <?php }else{ ?> 

                                      <button type="button" class="quantity-right-plus  btn-number" data-type="plus" data-field="" disabled>
                                       <span class="glyphicon glyphicon-plus"></span>
                                      </button>

                                      
                                      <?php } ?> 

                                    </span>
                                </div>
                        </div>
         </div>

    <span id="qtyidmsg" style="display: none; color: #ed1c24;"><p>Quantity not available at this store</p></span>

         <div class="qtycss"> 

          <!-- <h6 class="product-title">Quantity</h6> -->
         
         <!--  <select name="quantity" autocomplete="off" id="quantity" tabindex="0" class="a-native-dropdown">

           <option value="1" selected="selected">1</option>

           <option value="2">2</option>

           <option value="3">3</option>

           <option value="4">4</option>

           <option value="5">5</option>

           <option value="6">6</option>

           <option value="7">7</option>

           <option value="8">8</option>

           <option value="9">9</option>

           <option value="10">10</option>

           <option value="11">11</option>

           <option value="12">12</option>

           <option value="13">13</option>

           <option value="14">14</option>

           <option value="15">15</option>

           <option value="16">16</option>

           <option value="17">17</option>

           <option value="18">18</option>

           <option value="19">19</option>

           <option value="20">20</option>

           </select> -->

            <div class="buycss"> 

               <!-- <a class="btn btn-sm btn-primary sf-review-btn" href="{ route("checkout",["slug" => $item->slug]) }}" >Add to Cart</a> -->

            <input type="hidden" name="product_id" id="product_id" value="{{ $item->id }}"> 

            <input type="hidden" name="vendor_id"  id="vendor_id"  value="{{ $vendor_id }}">    

            <input type="hidden" name="purl" id="purl" value="{{ url('/').'/cart' }}"> 

              <?php if($item->stock==1){ ?>

               <button onclick="return apply_qty();" class="btn btn-sm btn-primary sf-review-btn btn-x"><span><i class="fa fa-shopping-cart" aria-hidden="true"></i>
      </span>Add to Cart</button>

              <?php }else{ ?>

            <button class="btn btn-sm btn-primary sf-review-btn disablebtn" disabled="disabled">Add to Cart</button>

              <?php } ?> 

              <span class="myOrderDtelBtn"><a href="javascript:history.back()"><span><i class="fa fa-arrow-left" aria-hidden="true"></i>
</span>Back to Store</a></span>

            </div>


    <div class="alert alert-success" id="addcartalt" style="color: green;background: lightyellow;float: left;width: 301px !important; display: none;">

    <button type="button" class="close" id="closeaddtoc">&times;</button>

    <strong id="msgaddtocart">Product Added Successfully!</strong> 

    </div>        


          </div>

              {{-- <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button> --}}


          </div>

      </div>

    </div>

    

  <div class="row">
  <div class="col-md-12 similar-pro">

        <div id="demo">

           <h2>RELATED PRODUCTS</h2>

          <div class="">

            <div class="row">

              <div class="col-md-12  ">

                <div class="block-13">

            <div class="owl-carousel similer_prod">
        

     <?php foreach($finalSimilarItems as $similarItem){ ?>

     
                <div class="item">

                    <div class="ctimg_show">

                      <?php $similarurl = $similarItem['vendor_id'].'/'.$similarItem['slug'].'/detail/' ; ?>

                      <a href="<?php echo url($similarurl); ?>"><img src="<?php echo $similarItem['imageURL'];?>" alt=""></a>

                    </div>


                    <div class="smilrProdct prerolls_product">

                    <p class="f_left"><?php if(isset($category)){ echo $category; } ?></p>  

                    <a href="<?php echo url($similarurl); ?>"><?php echo $similarItem['name']; ?></a>

                    <p class="raleted-rating"><a href="#"><span class="rgt_chk"><i aria-hidden="true" class="fa fa-check-circle"></i></span><span class="rgt_cont"></span></a>
                    <a href="{{url('/')}}/{{$storename}}/store"> {{$storename}}</a></p><br>  
                    <h6 class="product-title-btype" style="font-size:12px;">TYPE: {{ str_limit($item->types, 10) }} <br> @if(!empty($item->potency_thc))THC: {{ $item->potency_thc }}% @endif<br> @if(!empty($item->potency_cbd)) CBD: {{ $item->potency_cbd }}% @endif</h6>
                    <!-- <div class=" text-warning">

                    <?php

                    if($similarItem['avgRating'] == 1){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                   }else if($similarItem['avgRating'] == 2){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                   }else if($similarItem['avgRating'] == 3){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                    echo '<i class="fa fa-star-o"></i>'; 

                   }else if($similarItem['avgRating'] == 4){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                   }else if($similarItem['avgRating'] == 5){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                   }else if($similarItem['avgRating'] == '0'){

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>'; 

                   }

                    ?>

                    </div> -->

                    <p class="similar_price-s">${{ $similarItem['price'] }}</p>

                    </div>

                  </div>

     <?php } ?>

            </div>

          </div>

              </div>

            </div>

          </div>            

        </div> 

      </div>

      </div>

      
      <div class="row">
        <div class="col-md-12 rating-product">
     <div class="row for-Ratingsec">

          <div class="col-sm-12">
            <h2 style="border-bottom: 1px solid #dddddd; ">product rating</h2>
            <div class="rating-block deeprating">

              <h4>Average  rating</h4>

              <h2 class="bold padding-bottom-7">{{$userRatingInitial}} <small>/ 5</small></h2>

              <?php

                  if($userRating == 1){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

            }else if($userRating == 2){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                  }else if($userRating == 3){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';



                  }else if($userRating == 4){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                  }else if($userRating == 5){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';

                  }else if($userRating == '0'){

                    echo '<i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

            }

              ?>



            </div>

          </div>     

        </div>      

        

        <div class="row">

          <div class="col-sm-12">

            @if(count($ratingReviewData))

            <div class="review-block">

              

                <?php $i = 1; ?>

                @foreach ($ratingReviewData as $data)

                    <div class="row">

                      <div class="col-sm-12">

                        <div class="bcx_lft">

                          @if(!empty($data->profile_image))

                            <img src="{{url('public/uploads/user/'.$data->profile_image)}}" class="img-rounded" height="60" width="60">

                          @else

                            <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">

                          @endif

                        </div>

                        <div class="bcx_rg">

                          <div class="review-block-name">{{$data->name.' '.$data->lname}}</div>

                          <div class="review-block-date">{{date('l d, Y', strtotime ( $data->created_at ))}} <br> {{Helper::calculate_time_span($data->created_at)}}</div>

                        </div>

                      </div>

                      <div class="col-sm-12 review-blockOne">

                        <div class="review-block-rate deeprevblock">



                 <?php

                  if($data->rating == 1){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                  }else if($data->rating== 2){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                  }else if($data->rating == 3){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';



                  }else if($data->rating == 4){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                  }else if($data->rating == 5){

                    echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';

                  }

              ?>



                        </div>

                        <div class="review-block-title">Customer Review</div>

                        <div class="review-block-description">{{$data->review}}</div>

                      </div>

                    </div>

                    <?php $i++; ?>

                    @if($i != count($ratingReviewData))

                      <hr/>

                    @endif

                @endforeach

            </div>

            @endif

            @if(Auth::guard("user")->check())

              <div class="post-review-box" id="post-review-box">

                <div class="col-md-12">

                  <h2>Write your review</h2>

                  <div class="msg-box">

                      <div id="message-box" style="display:none;">

                        <i class="fa fa-check"></i>

                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                        <span id="message"></span>

                      </div>

                    </div>

                    <form accept-charset="UTF-8" id="review-rating-form" action="javascript:void(0);" method="post">

                        {{csrf_field()}}

                        <input id="ratings-hidden" name="rating" type="hidden"> 

                        <input id="ps-id" name="ps_id" type="hidden" value="{{$item->id}}"> 

                        <input id="type" name="type" type="hidden" value="{{$item->type}}"> 

                        <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5" required></textarea>    

                        <div class="text-right">

                            <div class="stars starrr" data-rating="0"></div>

                          <!--  <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="">Cancel</a> -->

                          <button class="btn btn-success btn-lg" type="button" id="submit-review">Submit Review</button>

                        </div>

                    </form>

                </div>

              </div>

            @endif

          </div>

        </div>
</div>
      </div>
    </div>



</section>


@endsection

@section("scripts")

{{-- styles --}}


<script type="text/javascript">

   function apply_qty(){

      var qty = $('#quantity').val(); // 

      var remainqty = parseInt($('#remainingqty').val());

      var cartqtyold = $('#cartqtyold').val(); 

      var totalqty = parseInt(qty)+parseInt(cartqtyold);

      if(remainqty < totalqty){

      $('#qtyidmsg').show(); 
       //alert('Selected quantity not available at this store');
       
       return false; 

      }

       if(remainqty == 0){
        $('#qtyidmsg').show(); 
         //alert('Selected quantity not available at this store');
         
         return false; 
      }

      $('#qtyidmsg').hide(); 

      var product_id = $('#product_id').val();

      var vendor_id  = $('#vendor_id').val();

      var purl = $('#purl').val();

      $.ajax({

         type: "post",

         headers: {

            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

         },

         data: {

            "_token": "{{ csrf_token() }}",

            "qty": qty,

            "product_id": product_id,

            "vendor_id": vendor_id

         },

         url: "{{url('/check_same_merchant')}}",       

         success: function(msg) { 

         if(msg.mstatus==1){

          apply_qty_add();

         }else{

        var ans = "Your cart contains product from "+msg.store1+". Do you want to discard the selection and add product from "+msg.store2+"?";

        $('#msgmodal').html(ans);

        $('#myModal').modal('show');

         }

        }

      });

   }

$(document).ready(function() {
    $('#modalyes').click(function(e) {  

      $('#myModal').modal('hide');
      apply_qty_add();
    });
});

   function apply_qty_add(){

      var qty = $('#quantity').val(); // 

      var remainqty = parseInt($('#remainingqty').val());

      var cartqtyold = $('#cartqtyold').val(); 

      var totalqty = parseInt(qty)+parseInt(cartqtyold);

      if(remainqty < totalqty){

      $('#qtyidmsg').show(); 
       //alert('Selected quantity not available at this store');
       
       return false; 

      }

       if(remainqty == 0){
        $('#qtyidmsg').show(); 
         //alert('Selected quantity not available at this store');
         
         return false; 
      }

      $('#qtyidmsg').hide(); 

      var product_id = $('#product_id').val();

      var vendor_id  = $('#vendor_id').val();

      var purl = $('#purl').val();

      $.ajax({

         type: "post",

         headers: {

            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

         },

         data: {

            "_token": "{{ csrf_token() }}",

            "qty": qty,

            "product_id": product_id,

            "vendor_id": vendor_id

         },

      url: "{{url('/quantity')}}",       

      success: function(msg) { 

      $('#session_qty').text(msg.cartcount); 

      $(".dcart a").prop("href", "{{url('/cart')}}");

      $("#addcartalt").show();

      $("#msgaddtocart").text(msg.msg);

      setTimeout(explode, 2000);   

         //window.location.replace(purl);

        }

      });

   }

  function explode(){

  $("#addcartalt").hide();

  }

   $('#submit-review').on('click', function() {

  $.ajax({

    type: "post",

    headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

    data: $("#review-rating-form").serialize(),

    url: "{{url('/save-user-ps-review')}}",       

    success: function(data) {

      $("#message-box").show();

      $("#message-box").removeClass();

      if(data.ok){

        $("#message-box").addClass("alert alert-success alert-dismissable");

        $("#message").text(data.message);

      }else{

        var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message">'+data.message+'</span></div>';

        $('.msg-box').html(html);

        $("#message-box").addClass("alert alert-danger alert-dismissable");

        $("#message").text(data.message);

      }

    }

  });

});



</script>

<!-- quantity script  -->
<script type="text/javascript">
  $(document).ready(function(){

var quantitiy=0;
   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined

        var remainqty = $('#remainingqty').val();

         if(quantity < remainqty){
            
            $('#quantity').val(quantity + 1);

             $('#qtyidmsg').hide(); 

          }else{

            $('#qtyidmsg').show(); 

          }  
        
            // Increment
        
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined

        $('#qtyidmsg').hide(); 
      
            // Increment
            if(quantity>1){
            $('#quantity').val(quantity - 1);
            }
    });
    
});
</script>



<!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModal">

    <div class="modal-dialog">
      <div class="modal-content bord-cann m-auto w-75 p-3">
      
        <!-- Modal Header -->
        <div class="modal-header border-0 pb-0 modal-box-cann">
          <h4 class="modal-title">Replace cart item?</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body cann-modal pt-1">
       <p id="msgmodal">Your cart contains product from other store. Do you want to discard the selection and add product from New store?</p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer border-0 cann-btn">
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-danger" id="modalyes">Yes</button>
        
        </div>
        
      </div>
    </div>
  </div>

@endsection