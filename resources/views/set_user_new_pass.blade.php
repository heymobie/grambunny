@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')
<div id="position">
	<div class="container">
		<ul>
			<li><a href="{{ url('/') }}">Home</a></li>
			<li>Update Account</li>
		</ul>
	</div>
</div><!-- Position -->
 
<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					@if(Session::has('message'))
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message')}}
					</div>
					@endif
					@if(Session::has('message_error'))					
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message_error')}}
					</div>
					@endif
					<div>
						<form role="form" id="setUserNewPass" method="POST" action="{{ url('/user_password/reset_your_user_password') }}">
							{{ csrf_field() }}
							<div class="box_style_2">
								<h2 class="inner">Reset Your Password</h2>
								<input type="hidden" class="form-control" name="userID" value="{{ Session::get('userID') }}">
						        @if(Session::has('Succes'))
						        <div class="alert alert-success alert-block">
						            <button type="button" class="close" data-dismiss="alert">×</button>
						            {{ Session::get('Succes') }}
						        </div>
						        @endif
						        @if (Session::has('Change_pass'))
						        <div class="alert alert-danger alert-block">
						            <button type="button" class="close" data-dismiss="alert">×</button>
						            {{ Session::get('Change_pass') }}
						        </div>
						        @endif
								<div class="form-group up-pas">
									<label for="exampleInputEmail1">@lang('translation.newPassword')</label>
									<input type="password" class="form-control" name="new_pass" id="new_pass" required="required">
									
								</div>

								<div class="form-group up-pas">
									<label for="exampleInputEmail1">@lang('translation.confirmNewPassword') </label>
									<input type="password" class="form-control" name="confirm_pass" id="confirm_pass" required="required">
									
								</div>
								
								<div class="row">
									<div class="">
										<div class="col-md-12">
											<button type="submit" class="btn btn-submit margin_30" id="update_oldpwd">   
												<i class="fa fa-btn fa-user"></i> @lang('translation.btnUpdatePassword')
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div>
					</div>					
				</div>
			</div>
		</div>
	</div><!-- End row -->
	<hr class="more_margin">
</div>

<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	

@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->
<!-- form validation start -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
   		jQuery.validator.addMethod("pass", function (value, element) {
			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {
				return true;
			} else {
				return false;
			};
		});
       $("#setUserNewPass").validate({
        rules: {
            new_pass:{
               required:true,
               minlength:8,
               pass:true,
           },
           confirm_pass: {
            required: true,
            minlength: 8,
            equalTo : "#new_pass",
        }
    },
    messages: {
        new_pass:{
           required:'Please enter a new password.',
           minlength:'Password must be at least 8 characters.',
           pass:"At least one number, one lowercase and one uppercase letter."
       },
       confirm_pass:{
        required:'Please enter confirm password.',
        minlength:'Password must be at least 8 characters.',
        equalTo:'confirm password not match, please enter correct.'
    },
}
});
   });
</script>
<!-- form validation end -->
		 
@stop	