@extends('layouts.grambunny')

@section('styles')

<style type="text/css">
.table-bordered td, .table-bordered th {
    border: 1px solid #dee2e6;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-size: 14px;
}
.MnThnxBox .btn-primary {
    font-size: 14px;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
}
.parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

}



section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

}

#short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

}

#sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

}

#sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

}

.ordrSumry tr {

    font-size: 13px;

}

section.section-full.myOrderThnk {
    padding: 6px 0px;
    background-color: #FFF;
}

.parallax-window#short {
   background: #FFF; 
   border-top: 1px solid #f1f1f1;
}
section.parallax-window{
  background-image: inherit !important;
}
span.restaunt_countrt {
    text-transform: uppercase;
    color: #000;
}
.UserNames{
  color: #000;
}

.adre_confirm {
    display: flex;
    align-items: flex-start;
}

.adre_confirm h4 {
    width: 162px;
}
 .pic_del_adre{
    line-height: 19px;
    margin-top: 8px;
}

.ordrSumry h4 {
    margin-top: 4px;
    }

</style>

@endsection

@section('content')



<section id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335" class="parallax-window Serv_Prod_Banner">

  <div id="subheader">

    <div id="sub_content" class="animated zoomIn">

      <h1><span class="restaunt_countrt">Thank You</span></h1> 

      <div class="UserNames">

        <strong>{{(!empty($user_info->name) ? $user_info->name : '')}} {{(!empty($user_info->lname) ? $user_info->lname : '')}}</strong>

      </div>

    </div>

  </div>

</section>



<section class="section-full myOrderThnk">

  <div class="container">

    <div class="row">
      <div class="col-md-12  ">
        <div class="MnThnxBox">
          <div class="">

          <div class="thnxMsgSec text-center">

         <!--    <span><i class="fa fa-check"></i></span> -->

            <!-- <h2>Thank you for your order.</h2>  -->

            <?php if($pstatus==1){ ?>

            <p>The order confirmation and email with detail of your order and a link to track its progress has been sent to your email address </p> 

            <?php }else{ ?>

            <p style="color: #ed1c24">Your payment has been failed</p>  
            <p style="margin-top: -20px;">{{$pmsg}}</p>  

            <?php } ?>          

          </div> 



          <div class="row">

            <div class="col-md-6">

              <div class="ordrSumry orderadrs">

                <!-- <span class="thnx_head"><i class="fa fa-home"></i></span><h4>
                  <?php if(empty($order_info->addressd)){ ?> Pick Up Address <?php }else{ ?> Delivery Address <?php } ?></h4> -->

                  <div class="addresss">
                    <div class="adre_confirm">
                      <h4>
                  <?php if(empty($order_info->addressd)){ ?> Pick Up Address: <?php }else{ ?> Delivery Address: <?php } ?></h4>

                <?php if(empty($order_info->addressd)){ ?> 

              <span>{{$market_area}}</span>

                <?php }else{ ?> 

              <span class="pic_del_adre">
                <span>{{ (!empty($order_info->addressd) ? $order_info->addressd : 'N/A') }}</span>

                <span>{{ $order_info->cityd }}, {{ $order_info->stated }}, {{ $order_info->zipd }}, {{ $order_info->countryd }}</span>
               </span>
                 <?php } ?>

             </div>

 <div class="adre_confirm">
               <h4>Name:</h4><span style="text-transform: capitalize;">{{ (!empty($order_info->first_name) ? $order_info->first_name : 'N/A') }} {{ (!empty($order_info->last_name) ? $order_info->last_name : 'N/A') }}</span> 
                </div>

                 <div class="adre_confirm">

                 <h4>Mobile No.:</h4><span>{{ $order_info->mobile_no }}</span></div>
                 
 <div class="adre_confirm">
                  <h4>Email:</h4><span>{{ $order_info->email }}</span></div>

              </div>



              </div>

              <!-- <button type="button" class="btn-primary" onclick="location.href='index.html'">Back to home</button> -->

              <a href="{{url('/')}}" class="btn-primary"><span><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
</span>Back to home</a> 

          <!-- <a href="{{url('/track-order/')}}/{{ $order_info->id }}" class="btn-primary"><span><i class="fa fa-truck" aria-hidden="true"></i></span>Track order</a> -->

            </div>

      <div class="col-md-6">

            <div class="ordrSumry">

              <span class="thnx_head"><i class="fa fa-check-square-o"></i></span><h4>Order Summary</h4>

              <table class="table table-bordered">

                <tbody>

                @if(!empty($item_info))

                @foreach($item_info as $item) 

                <tr>

                  <td>

                    {{ $item['quantity']}} x ${{ $item['price']}}

                    <p>({{ $item['name']}})</p>

                  </td>                

                  <td style="text-align: right;"><?php echo '$'.

                  number_format( $item['quantity']*$item['price'],2 ); 

                  ?></td>

                </tr>

               @endforeach

                @else

                @foreach($pslist as $items) 

                <tr>

                  <td>

                    {{ $items->ps_qty}} x ${{ $items->price}}

                    <p>({{ $items->name}})</p>

                  </td>                

                  <td style="text-align: right;"><?php echo '$'.

                  number_format( $items->ps_qty*$items->price,2 ); 

                  ?></td>

                </tr>
                @endforeach

               @endif


                @if($order_info->promo_amount)

                <tr>

                  <td>Promotion Applied</td>                

                  <td style="text-align: right;">

 

                  $<?php echo number_format($order_info->promo_amount,2); ?>



                  </td>

                </tr>

                @endif

<?php $ps_type = $order_info->product_type; ?>  
        
<?php if ($ps_type == 3 ){ ?>  

<tr>
  <td>Ticket Fee</td>                
  <td style="text-align: right;">${{ number_format( $pslist[0]->ps_qty * $order_info->ticket_fee,2)}}</td>
</tr>
<tr>
  <td>Ticket Service Fee</td>                
  <td style="text-align: right;">${{ number_format($pslist[0]->ps_qty * $order_info->ticket_service_fee,2)}}</td>
</tr>

<?php } else { ?>

<tr>
  <td>Delivery Fee</td>                
  <td style="text-align: right;">${{ number_format($order_info->delivery_fee,2)}}</td>
</tr>

<?php  } ?>


                <tr>

                  <td>Sub Total</td>                

                  <td style="text-align: right;">${{(!empty($order_info->sub_total) ? number_format($order_info->sub_total,2) : 0)}}</td>

                </tr>
                
            <?php if ($ps_type != 3 ){ ?>  

                  <tr>

                  <td>Excise Tax of Subtotal  ({{$excise_tax_percent}}%)</td>                

                  <td style="text-align: right;">${{ number_format($order_info->excise_tax,2)}}</td>

                </tr>

                <tr>

                  <td>City Tax of Subtotal  ({{$city_tax_percent}}%)</td>                

                  <td style="text-align: right;">${{ number_format($order_info->city_tax,2)}}</td>

                </tr>

         <?php } ?>       

                <tr>

                  <td>Sales Tax of Subtotal  ({{$sales_tax_percent}}%)</td>                

                  <td style="text-align: right;">${{ number_format($order_info->service_tax,2)}}</td>

                </tr>



                <tr>

                  <td><strong>Total (USD)</strong></td>

                  <td style="text-align: right;"><strong>${{(!empty($order_info->total) ? number_format($order_info->total,2) : 0)}}</strong></td>

                </tr>

              </tbody></table>

            </div>

         </div>

      </div>



          

          </div> 

      </div>    

    </div>

    </div>

  </div>

</section>

    

@endsection

@section('scrips')

    

@endsection