@extends('layouts.grambunny')
@section('styles')
<style type="text/css">
   .MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
   .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
   .MultiCarousel .MultiCarousel-inner .item { float: left;}
   .MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:#f1f1f1; color:#666;}
   .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
   .MultiCarousel .leftLst { left:0; }
   .MultiCarousel .rightLst { right:0; }
   .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background-image:linear-gradient(to right, #EC7324  , #F7FF0A ) !important; }
   .pad15 {
   text-align: center;
   float: left;
   width: 100%;
   }
   .item {
   padding: 10px;
   margin-left: 26px;
   width: 27% !;
   width: 164px !important;
   border-top: 1px solid #EC7324; 
   border-bottom: 1px solid #EC7324; 
   border-left: 1px solid #ec3e6c ; 
   border-right: 1px solid #ec3e6c ; 
   background-color: #fff;
   box-shadow: rgb(0 0 0 / 10%) 0px 4px 10px;
   border-radius: 15px;
   }
   .pad15 img {
   width: 100%;
   }
   p.lead {
   font-size: 19px;
   color: #222;
   font-weight: bold;
   }
   .shop-at{
   font-weight: bold;
   text-transform: uppercase;
   line-height: 12px;
   color: rgb(175, 177, 178);
   margin: 0px;
   font-size: 11px;
   }
   .btn-primary {
   color: #fff;
   background-image:linear-gradient(to right, #EC7324  , #F7FF0A ) !important;
   border-color: #EC7324;
   }
   .cat-heading {
   float: left;
    width: 94%;
    background-color: #f5f5f5;
    margin-top: 10px;
    padding: 10px;
    border: 1px solid #ddd;
    border-radius: 5px;
    margin-left: 4%;
   }
   .cat-heading h3 {
   font-weight: bold;
   margin-bottom: 0px;
   }
   .pad15 h4 {
    float: left;
    width: 100%;
    color: #222;
    font-weight: bold;
}

.pad15 p {
    color: #000;
    padding: 0px;
    margin: 0px;
}

.pad15 span {
    color: rgb(150, 158, 165);
    font-weight: bold;
    font-size: 9px;
}

p.price {
    color: #EC7324;
    font-size: 20px;
}
@media only screen and (max-width: 640px) {
 .item {
    width: 345px !important;
    margin-left: 0px !important;
}
}
</style>
@endsection
@section('content')
<section id="ordr-grab-sec">
   <div class="container">
      <div class="row  wow fadeInUp" data-wow-delay="0.1s">
         <div class="col-lg-12">
            <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
               <!-- <span>Grab it features //</span> -->
               <h2>{{@$page_data->page_title}}</h2>
               <p>{{@$page_data->sub_title}}</p>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="" style="background:#f0f4f6">
   <div class="container">
      <!-- <div class="row align-items-center">
         <div class="row" >
         
             <div class="col-12  block-13">
         
         
         <?php 
            if(!empty($product_category[0]->id)){
            
            foreach ($product_category as $key => $category) {  ?>
                                   
                  
         <a href="category/{{$category->id}}" class="img d-block"><img src="{{asset("public/uploads/category/".$category->cat_image)}}"></a>
         
         <h3>{{$category->category}}</h3>
                    
          <?php } } ?>   
         
             </div>
         
         </div>
         
         </div> -->
   </div>
</section>
<div class="container">
   <div class="row">
      <div class="cat-heading">
         <h3>Categories</h3>
      </div>
      <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
         <div class="MultiCarousel-inner">
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <p class="lead">Flower</p>
                     <p class="shop-at">Shop</p>
                  </div>
               </a>
            </div>
         </div>
         <button class="btn btn-primary leftLst"><</button>
         <button class="btn btn-primary rightLst">></button>
      </div>
   </div>
</div>
<div class="container">
   <div class="row">
      <div class="cat-heading">
         <h3>Popular product</h3>
      </div>
      <div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
         <div class="MultiCarousel-inner">
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
              <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
              <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
                <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
                <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
             <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
            <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
             <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
             <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
               <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
                <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
               <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
                <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
                <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
               <div class="item">
               <a href="">
                  <div class="pad15">
                     <img src="https://www.grambunny.com/public/uploads/product/kKRMXh1627558065.png" alt="" >
                     <h4>Mendo Breath</h4>
                     <p>West Coast Treez</p>
                     <span>Indica</span>
                     <span>THC: 20.81%</span>
                     <span>CBD: 17.4%</span>
                     <p class="price">$29</p>

                  </div>
               </a>
            </div>
         </div>
         <button class="btn btn-primary leftLst"><</button>
         <button class="btn btn-primary rightLst">></button>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).ready(function () {
   var itemsMainDiv = ('.MultiCarousel');
   var itemsDiv = ('.MultiCarousel-inner');
   var itemWidth = "";
   
   $('.leftLst, .rightLst').click(function () {
      var condition = $(this).hasClass("leftLst");
      if (condition)
          click(0, this);
      else
          click(1, this)
   });
   
   ResCarouselSize();
   
   
   
   
   $(window).resize(function () {
      ResCarouselSize();
   });
   
   //this function define the size of the items
   function ResCarouselSize() {
      var incno = 0;
      var dataItems = ("data-items");
      var itemClass = ('.item');
      var id = 0;
      var btnParentSb = '';
      var itemsSplit = '';
      var sampwidth = $(itemsMainDiv).width();
      var bodyWidth = $('body').width();
      $(itemsDiv).each(function () {
          id = id + 1;
          var itemNumbers = $(this).find(itemClass).length;
          btnParentSb = $(this).parent().attr(dataItems);
          itemsSplit = btnParentSb.split(',');
          $(this).parent().attr("id", "MultiCarousel" + id);
   
   
          if (bodyWidth >= 1200) {
              incno = itemsSplit[3];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 992) {
              incno = itemsSplit[2];
              itemWidth = sampwidth / incno;
          }
          else if (bodyWidth >= 768) {
              incno = itemsSplit[1];
              itemWidth = sampwidth / incno;
          }
          else {
              incno = itemsSplit[0];
              itemWidth = sampwidth / incno;
          }
          $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
          $(this).find(itemClass).each(function () {
              $(this).outerWidth(itemWidth);
          });
   
          $(".leftLst").addClass("over");
          $(".rightLst").removeClass("over");
   
      });
   }
   
   
   //this function used to move the items
   function ResCarousel(e, el, s) {
      var leftBtn = ('.leftLst');
      var rightBtn = ('.rightLst');
      var translateXval = '';
      var divStyle = $(el + ' ' + itemsDiv).css('transform');
      var values = divStyle.match(/-?[\d\.]+/g);
      var xds = Math.abs(values[4]);
      if (e == 0) {
          translateXval = parseInt(xds) - parseInt(itemWidth * s);
          $(el + ' ' + rightBtn).removeClass("over");
   
          if (translateXval <= itemWidth / 2) {
              translateXval = 0;
              $(el + ' ' + leftBtn).addClass("over");
          }
      }
      else if (e == 1) {
          var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
          translateXval = parseInt(xds) + parseInt(itemWidth * s);
          $(el + ' ' + leftBtn).removeClass("over");
   
          if (translateXval >= itemsCondition - itemWidth / 2) {
              translateXval = itemsCondition;
              $(el + ' ' + rightBtn).addClass("over");
          }
      }
      $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
   }
   
   //It is used to get some elements from btn
   function click(ell, ee) {
      var Parent = "#" + $(ee).parent().attr("id");
      var slide = $(Parent).attr("data-slide");
      ResCarousel(ell, Parent, slide);
   }
   
   });
</script>
@endsection