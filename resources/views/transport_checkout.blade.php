@extends('layouts.app')

@section("other_css")


<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" type="image/x-icon" href="{{ url('/') }}/design/front/img/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ url('/') }}/design/front/img/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ url('/') }}/design/front/img/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ url('/') }}/design/front/img/apple-touch-icon-144x144-precomposed.png">
<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">
   	  <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->
  

<meta name="_token" content="{!! csrf_token() !!}"/>
@stop
@section('content')


<!-- SubHeader =============================================== -->
<section class="parallax-window chckout_prllx_top" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
  <div id="subheader">
    <div id="sub_content">
      <h1>Place your order</h1>
      <div class="bs-wizard">
        <div class="col-xs-4 bs-wizard-step active">
          <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="#0" class="bs-wizard-dot"></a> </div>
        <div class="col-xs-4 bs-wizard-step disabled">
          <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="#" class="bs-wizard-dot"></a> </div>
        <div class="col-xs-4 bs-wizard-step disabled">
          <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
          <div class="progress">
            <div class="progress-bar"></div>
          </div>
          <a href="cart_3.html" class="bs-wizard-dot"></a> </div>
      </div>
      <!-- End bs-wizard --> 
    </div>
    <!-- End sub_content --> 
  </div>
  <!-- End subheader --> 
</section><!-- End section -->
<!-- End SubHeader ============================================ -->
<div id="position">
  <div class="container">
    <ul>
      <li><a href="{{url('/')}}">Home</a></li>
      <li>Cart</li>
    </ul>
  </div>
</div>
<!-- Position -->
    <!-- Content ================================================== -->
  <div class="container margin_60_35">
  <div id="container_pin">
    <div class="row">
      <div class="col-md-3">
        <div class="box_style_2 hidden-xs info">
          <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
          <p> Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos. </p>
          <hr>
          <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
          <p> Lorem ipsum dolor sit amet, in pri partem essent. Qui debitis meliore ex, tollit debitis conclusionemque te eos. </p>
        </div>
        <!-- End box_style_1 -->
        
        <div class="box_style_2 hidden-xs" id="help"> <i class="icon_lifesaver"></i>
          <h4>Need <span>Help?</span></h4>
          <a href="#" class="phone">+1800123456</a> <!--<small>Monday to Friday 9.00am - 7.30pm</small>--> </div>
      </div>
      <!-- End col-md-3 -->
      
      <div class="col-md-6"> 
	  @if(Auth::guest()) 
        <div class="box_style_2">
          <div class="cart-lg-ctn"> <a href="#0" data-toggle="modal" data-target="#login_2" class="lg-btn">Login</a> <span class="or-text">OR</span> <a href="#" class="guest-text">Continue as a guest</a>
            <p class="btm-text">Logging in will save time and money. Blah blah</p>
          </div>
        </div>
	  @endif	
		<form name="checkout_user_info" id="checkout_user_info" method="post" action="{{url('/submit_personaldetail')}}">
		
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  
		  <input type="hidden" name="user_id"  value="@if(Auth::guest()) 0 @else {{ Auth::user()->id }}@endif" />
		
        <div class="box_style_2 delivery-info-ctn">
          <h2 class="inner">Trip Details </h2>
		  <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Contact Name</label>		   
           		 <input type="text" name="pick_contact_name" id="pick_contact_name" value="{{Session::get('picup_info')['pick_contact_name']}}" class="form-control" required="required" >
              </div>
            </div>
            
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Contact Mobile Number</label>
           		 <input type="text" name="pick_contact_no" id="pick_contact_no" value="{{Session::get('picup_info')['pick_contact_no']}}" class="form-control" required="required" number="number"  maxlength="10">
				 	 <div id="error_number_msg" style="color:#FF0000; display:none;"></div>
              </div>
            </div>
          </div>
		  
		  <div class="form-group">
            <label>Addressline1</label>
            <input type="text" name="pick_address1" id="pick_address1" value="{{Session::get('picup_info')['pick_address1']}}" class="form-control" required="required">
          </div>
		  <div class="form-group">
            <label>Addressline2</label>
            <input type="text" name="pick_address2" id="pick_address2" value="{{Session::get('picup_info')['pick_address2']}}" class="form-control">
          </div>
		  <div class="form-group">
            <label>Booking Type<br /></label>
			 <div>
			  <div class="control-group islamic2">
				 <label>
					 <span class="control control--radio">
					<input type="radio" name="pick_bookingtype" id="pick_bookingtype" value="One_Way_Trip" <?php if(Session::get('picup_info')['pick_bookingtype']=='One_Way_Trip'){ echo 'checked="checked"';} ?>>
					 
					 
					 <div class="control__indicator"></div>
					 </span>
						One Way Trip
				 </label>
				 <label>
					 <span class="control control--radio">
					
					 
						<input type="radio" name="pick_bookingtype" id="pick_bookingtype" value="Return_Trip" <?php if(Session::get('picup_info')['pick_bookingtype']=='Return_Trip'){ echo 'checked="checked"'; }?>>
					 
					 <div class="control__indicator"></div>
					 </span>
					 Return Trip
				 </label>
				</div>	
				
			<?php /*?>						 
				<input type="radio" name="pick_bookingtype" id="pick_bookingtype" value="One_Way_Trip" <?php if(Session::get('picup_info')['pick_bookingtype']=='One_Way_Trip'){ echo 'checked="checked"';} ?>>
				  One Way Trip
				<input type="radio" name="pick_bookingtype" id="pick_bookingtype" value="Return_Trip" <?php if(Session::get('picup_info')['pick_bookingtype']=='Return_Trip'){ echo 'checked="checked"'; }?>>
				Return Trip<?php */?>
			</div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Suburb</label>		   
           		 <input type="text" name="pick_suburb" id="pick_suburb" value="{{Session::get('picup_info')['pick_suburb']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Postcode</label>			
           		 <input type="text" name="pick_pcode" id="pick_pcode" class="form-control" readonly="readonly"  value="{{Session::get('picup_info')['pick_pcode']}}" required="required">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>District</label>
           		 <input type="text" name="pick_district" id="pick_district" value="{{Session::get('picup_info')['pick_district']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>State</label>
           		 <input type="text" name="pick_state" id="pick_state" value="{{Session::get('picup_info')['pick_state']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
          </div>
		  
		  
          <h4 class="inner">ToLocation Details </h4>
		  
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>ToSuburb</label>		   
           		 <input type="text" name="pick_to_suburb" id="pick_to_suburb" value="{{Session::get('picup_info')['pick_to_suburb']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>ToPostcode</label>			
           		 <input type="text" name="pick_to_pcode" id="pick_to_pcode" value="{{Session::get('picup_info')['pick_to_pcode']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
			
			
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>District</label>
           		 <input type="text" name="pick_to_district" id="pick_to_district" value="{{Session::get('picup_info')['pick_to_district']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>State</label>
           		 <input type="text" name="pick_to_state" id="pick_to_state" value="{{Session::get('picup_info')['pick_to_state']}}" class="form-control" required="required" readonly="readonly">
              </div>
            </div>
			
          </div>
        </div>
		
		
		
		
        <div class="box_style_2" id="order_process">
          <h2 class="inner">Your Contact details</h2>
          <div class="form-group">
            <label>First name</label>
            <input type="text" class="form-control" id="firstname_order" name="firstname_order" placeholder="First name" required="required" value="{{$user_detail['firstname_order']}}">
          </div>
          <div class="form-group">
            <label>Last name</label>
            <input type="text" class="form-control" id="lastname_order" name="lastname_order" placeholder="Last name" required="required" value="{{$user_detail['lastname_order']}}">
          </div>
          <div class="form-group">
            <label>Telephone/mobile</label>
            <input type="text" id="tel_order" name="tel_order" class="form-control" placeholder="Telephone/mobile" required="required" number="number" value="{{$user_detail['tel_order']}}">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" id="email_booking_2" name="email_order" class="form-control" placeholder="Your email" required="required" value="{{$user_detail['email_order']}}">
          </div>
          <div class="form-group">
            <label>Your full address</label>
            <input type="text" id="address_order" name="address_order" class="form-control" placeholder=" Your full address" required="required" value="{{$user_detail['address_order']}}">
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>City</label>
                <input type="text" id="city_order" name="city_order" class="form-control" placeholder="Your city" required="required" value="{{$user_detail['city_order']}}"> 
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <div class="form-group">
                <label>Postal code</label>
                <input type="text" id="pcode_order" name="pcode_order" class="form-control" placeholder=" Your postal code" number="number" required="required" value="{{$user_detail['pcode_order']}}">
              </div>
            </div>
          </div>
		  <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="form-group">&nbsp;</div>
            </div>
            <div class="col-md-6 col-sm-6">
			<!--<a class="btn_full " href="cart_3.html">Place Order</a>-->
			<input type="button" id="submit_detail" name="submit_detail" value="Place Order" class="btn_full" /> 
            </div>
          </div>
        </div>
		</form>
        <!-- End box_style_2 -->
        
        <!-- End box_style_3 --> 
      </div>
      <!-- End col-md-6 -->
      
      <div class="col-md-3" id="sidebar">
        <div class="theiaStickySidebar">
          <div id="cart_box">
            <h3>Your order summary <i class="icon_cart_alt pull-right"></i></h3>
		
			<?php $cart_price = 0;$total_amt=0;?>
		@if(isset($cart) && (count($cart)))
			<div class="cart_scrllbr">
<div class="table table_summary">	
				
				 @foreach($cart as $item)
				<?php $total_amt= $cart_price = $cart_price+$item->price;?>
				  <div class="crt_itm_cust1">
                  <p><strong>{{$item->qty}}  Day's</strong> {{str_replace('_',' ',$item->name)}}   </p>
                  <p><strong class="pull-right">${{number_format($item->price,2)}}</strong></p>
                </div>
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $total_amt=$cart_price = $cart_price+$addon['price'];?>
					  <div class="crt_itm_cust23">
					  <p>{{$addon['name']}}</p>
                  	<p><strong class="pull-right">@if($addon['price']>0)${{number_format($addon['price'],2)}}@endif</strong></p>
					</div>
					 @endforeach
				 @endif 
					
				 @endforeach
				 
				 
 	 </div> 
	 
			@if(Session::has('cart_order_detail') && (!empty(Session::has('cart_order_detail'))))	
							
				<div class="table table_summary">			
					<div class="crt_itm_cust1" style=" border-top: 1px solid rgba(128, 128, 128, 0.11);    float: left;   margin-top: 15px;   padding-top: 10px;   width: 100%;">
						<p style="width:100%;float:left !important;">
						From : <?php echo Session::get('cart_order_detail')['from_location']; ?>
						<br />
						To : <?php echo Session::get('cart_order_detail')['to_location']; ?>
						<br />
						
						Booking From: <?php echo date('d-m-Y',strtotime(Session::get('cart_order_detail')['on_date'])).' '.Session::get('cart_order_detail')['on_time'] ?> <br />Booking To: <?php echo date('d-m-Y',strtotime(Session::get('cart_order_detail')['return_date'])).' '.Session::get('cart_order_detail')['return_time'] ?>  </p>
					</div>
				</div>
		@endif
			
  </div>
@endif

			
		
   

            <hr>
            
            <!-- Edn options 2 -->
            <table class="table table_summary">
              <tbody>
                <tr>
                  <td> Subtotal <span class="pull-right">${{number_format(Session::get('cart_order_detail')['subtotal'],2)}}</span></td>
                </tr>
				
                <tr>
                  <td> Service fee <span class="pull-right">${{number_format(Session::get('cart_order_detail')['delivery_fee'],2)}}</span></td>
                </tr>	
				
				@if((Session::get('cart_order_detail')['total_nignt']>0))
			
                <tr>
                  <td> Driver Allownce <span class="pull-right">${{number_format(Session::get('cart_order_detail')['driver_allownce'],2)}}</span></td>
                </tr>
				
				@endif
				
				
				@if((Session::get('cart_order_detail')['promo_amt_cal']>0))
			
                <tr>
                  <td> Discount <span class="pull-right">-${{number_format(Session::get('cart_order_detail')['promo_amt_cal'],2)}}</span></td>
                </tr>
				
				@endif
				
				
				
				
				
				
				
				
                <tr>
                  <td class="total"> TOTAL <span class="pull-right">${{number_format(Session::get('cart_order_detail')['total_charge'],2)}}</span></td>
                </tr>
              </tbody>
            </table>
            <hr>
            <a class="btn_full_outline" href="{{url(Session::get('cart_order_detail')['back_url'])}}"><i class="icon-right"></i> Change Booking Rate</a> </div>
          <!-- End cart_box --> 
        </div>
      </div>
      <!-- End col-md-3 --> 
      
    </div>
    <!-- End row --> 
  </div>
  <!-- End container pin --> 
</div>
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')


<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

<div class="modal fade" id="message_model" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body popup-ctn">
	  		<p id="alert_model_msg"></p>	
      </div>
    </div>
  </div>
</div>

<!-- COMMON SCRIPTS -->




<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script> 
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script> 
<script src="{{ url('/') }}/design/front/js/functions.js"></script> 
<script src="{{ url('/') }}/design/front/assets/validate.js"></script> 

<!-- SPECIFIC SCRIPTS --> 
<script src="{{ url('/') }}/design/front/js/custom.js"></script> 
<script src="{{ url('/') }}/design/front/js/jquery-ui.js"></script> 
<script src="{{ url('/') }}/design/front/js/bootstrap-select.js" /></script>
<script>
    jQuery('#sidebar').theiaStickySidebar({
      additionalMarginTop: 80
    });
</script> 
<script>

		 
		 
$(document).on('click', '#submit_detail', function(){


  	var form = $("#checkout_user_info");
	form.validate();
	var valid =	form.valid();
	
		if(valid)
		{
		
			$('#error_number_msg').hide();
			var regExp = /^0[0-9].*$/;
			if(regExp.test($("#pick_contact_no").val()))
			{
				$("#checkout_user_info").submit();
				return true;					
			}
			else
			{
				//alert('Contact number start with 0.');
				$('#error_number_msg').html('Contact number start with 0.');
				$('#error_number_msg').show();
				return false;
			}
			
		}
});	



 $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

@stop