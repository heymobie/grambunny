@extends('layouts.grambunnyrefresh')
@section('styles')
<style type="text/css">
span.catactive {
color: #ed1c24;
font-weight: bold;
}

.prduc_img {
float: left;
width: 100%;
display: flex;
justify-content: center;
align-items: center;
height: 172px !important;
margin-top: 10px;
overflow: hidden;
}

.bannerimg {
width: 100%;
height: 480px;
overflow: hidden;
}

.bannerimg img {
width: 100%;
object-fit: cover;

}

@media only screen and (max-width: 767px) and (min-width: 320px) {
.bannerimg {
width: 100%;
height: auto;
overflow: hidden;
object-fit: cover;
}

}

@media screen and (orientation:portrait) {

.bannerimg {
width: 100%;
height: auto;
overflow: hidden;
object-fit: cover;
}

.bannerimg {
width: 100%;
height: auto !important;
overflow: hidden;
}
}

@media screen and (orientation:landscape) {

.bannerimg {
width: 100%;
height: auto !important;
overflow: hidden;
}

.addtocart {
width: 30px !important;
float: right;
position: relative;
top: 5px;
}
}


.product-bann .carousel-inner {
position: relative;
width: 100%;
overflow: hidden;

}

.product-bann .carousel-item img {
width: 100%;
height: 480px;
object-fit: fill;
}

@media only screen and (max-width: 1000px) and (min-width: 769px) {
.product-bann .carousel-inner {
position: relative;
width: 100%;
overflow: hidden;

}

.product-bann .carousel-inner img {
height: 100% !important;
}

.product-bann .carousel-item {
height: 300px;
}

}

@media only screen and (max-width: 768px) and (min-width: 320px) {
.product-bann .carousel-inner {
position: relative;
width: 100%;
overflow: hidden;
height: 300px !important;
}

.product-bann .carousel-inner img {
height: 100% !important;
}

.product-bann .carousel-item {
height: 224px;
/* display: flex; */
margin: auto;
}
a#mapview {
color: #fff !important;
border: 1px solid #fff;
line-height: 23px;
}
a#listview {
color: #fff !important;
border: 1px solid #fff;
line-height: 23px;
}
.file-form .pb-4{
padding-bottom: 0px !important;
}

}

@media screen and (min-width:320px) and (max-width: 767px) and (orientation: landscape) {

.product-bann .carousel-item img {
width: 100%;
height: 100%;
object-fit: fill;
}

.product-bann .carousel-inner {
position: relative;
width: 100%;
overflow: hidden;
height: 300px;

}
}

.list_view_bg_sect .sf-featured-top .prduc_img {
margin-top: 24px;
height: 187px;
margin-bottom: 1px !important;
background: #fff;
padding: 41px;
overflow: hidden;
}

.sf-featured-top {
position: relative;
overflow: hidden;
padding: 13px;
box-shadow: 1px 2px 17px #cccccc99;
border-radius: 11px;
}
</style>
@endsection
@section('content')
<main id="main">
<?php $baseurl = url('/'); ?>

<section class="map-top">
<div class="container-fluid">

<div class="row">
<div class="col-md-12">

<div id="carouselExampleControls" class="carousel product-bann slide" data-ride="carousel">
<div class="carousel-inner">
<div class="carousel-item active">
<img src="<?php echo $banner_image; ?>">
</div>
@if (!empty($banner_image_multiple))
@foreach ($banner_image_multiple as $value1)
<div class="carousel-item">
<img class="d-block w-100"
src="{{ url('/') }}/public/uploads/advertisement/{{ $value1->banner_image_multiple }}"
height="500" alt="Second slide">
</div>
@endforeach
@endif

</div>

</div>
</div>
</div>

<div class="row">
<div class="col-md-6 m-auto filter-map">
<div class="">
<form class="file-form ">
<div class="row pb-4">
<div class="col-md-6">
<div class="form-group">
<div class="icon-addon addon-lg">
<label class="filt-frm"> <i class='bx bx-search'></i> <input type="text"
placeholder="Search by Merchant Name" class="form-control"
id="searchpbr" name="searchpbr" onkeyup="searchKeypbr()">
</label>
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group">
<div class="icon-addon addon-lg">
<label class="filt-frm"><i class='bx bx-map'></i> <input type="text"
placeholder="Search by City or Zip Code" class="form-control"
name="searchcityz" id="searchcityz" onkeyup="searchKeypbr()">
</label>
</div>
</div>
</div>

<div class="col-md-12 mt-4 curent-locat">
<a class="filter-se" onClick="clFunction()"><i class='bx bxs-bullseye'></i> Current
Location</a>
<a class="filter-se" id="mapview" style="display: block; cursor:pointer"><i
class='bx bxs-map-alt'></i>
Map View</a>
<a class="filter-se" id="listview" style="display: block; cursor:pointer"><i
class='bx bx-list-ul'></i>
List View</a>
</div>
</div>
</form>

</div>
</div>


</div>
{{-- new code here start --}}
<div id="ajaxdatacall">

<div class="container-fuild">
<div class="row map-full-bg">
<div class="col-md-12">
<div class="map">

<div class="welcome-area " style="margin-top:0 !important;">

<div id="map"></div>

<section id="" class="merchnt">
<div class="" data-aos="fade-up">


<div class="row d-flex flex-wrap">
@php
$storedata = [];
$vdistance = '';
@endphp
                                    
<?php foreach ($verdorslist as $key => $value) { 

            if($value->map_icon){
                
                 $mapicon = $value->map_icon;

                }else{

                  $mapicon = 'map.png';

                }  

            ?>

<?php $storedata[] = ['name' => "'" . $value->username . "'", 'location' => ['lat' => $value->lat, 'lng' => $value->lng], 'icon' => "'" . $mapicon . "'"]; ?>

@foreach ($results as $revalue)
@if ($revalue->vendor_id == $value->vendor_id)
<div class="col-md-3 d-md-flex align-items-md-stretch"
style="display:none!important;">
<a
href="<?php echo $baseurl; ?>/{{ $value->username }}/store">
<div class="count-box">

<div class="count-img">
@if (!empty($value->profile_img1))
<img src="{{ $baseurl }}/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
alt="Profile Image">
@else
<img src="https://www.grambunny.com/public/uploads/user.jpg"
alt="Profile Image">
@endif
</div>
<p>{{ $value->business_name }}</p>
<small>{{ $value->description }}</small>
<div class="conpmy-name">
<h6><i class='bx bx-check-circle'></i>
{{ $value->username }}</h6>
<h6><i class="bx bx-map"></i> Distance :
{{ round($revalue->distance, 1) }} Miles
</h6>
</div>
<div class="rating-box">
@php
$ratings = $value->avg_rating;
@endphp
@for ($i = 0; $i < 5; $i++)
@if ($ratings > $i)
<i class='bx bxs-star'></i>
@else
<i class='bx bx-star'></i>
@endif
@endfor
</div>
</div>
</a>
</div>
@endif
@endforeach
<?php } ?>
</div>
</div>
</section>

</div>

</div>

</div>
</div>
</div>
</div>

<?php

$drivermap = json_encode($storedata);

$drivermaps = str_replace('"', '', $drivermap);

?>

<div class="container-fuild">

<div class="row">
<div class="product-listing-section">
<div class="row row_listing">
<div class="col-md-12">

<div class="list_view_bg_sect" id="list_view_bg_sect">
<section id="" class="merchnt">
<div class="" data-aos="fade-up">

<div class="row d-flex flex-wrap">
@php
$storedata = [];
$vdistance = '';
@endphp
{{-- <div class="section-title">
<p>Merchants</p>
</div> --}}

@foreach ($verdorslist as $value)
@foreach ($results as $revalue)
@if ($revalue->vendor_id == $value->vendor_id)
<div class="col-md-3 d-md-flex align-items-md-stretch"
id="listviewscroll">
<a
href="<?php echo $baseurl; ?>/{{ $value->username }}/store">
<div class="count-box">

<div class="count-img">
@if (!empty($value->profile_img1))
<img src="{{ $baseurl }}/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
alt="Profile Image">
@else
<img src="https://www.grambunny.com/public/uploads/user.jpg"
alt="Profile Image">
@endif
</div>

<p>{{ $value->business_name }}</p>
<small>{{ $value->description }}</small>
<div class="conpmy-name">
<h6><i class='bx bx-check-circle'></i>
{{ $value->username }}</h6>
<h6><i class="bx bx-map"></i> Distance :
{{ round($revalue->distance, 1) }}
Miles
</h6>
</div>
<div class="rating-box">
@php
$ratings = $value->avg_rating;
@endphp
@for ($i = 0; $i < 5; $i++)
@if ($ratings > $i)
<i class='bx bxs-star'></i>
@else
<i class='bx bx-star'></i>
@endif
@endfor
</div>
</div>
</a>
</div>
@endif
@endforeach
@endforeach
</div>
</div>
</section>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
{{-- new code here end --}}






<input type="hidden" name="pagenum" id="pagenum" value="1">
<input type="hidden" name="totalvhp" id="totalvhp" value="{{ $total }}">

<div class="col-md-12 load-btn"><button id="loadshow" class="round load-mor-btn"
style="display: none;">Load
More</button></div>


</div>
</section>

</main><!-- End #main -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

var totalvrshp = $('#totalvhp').val();

if (totalvrshp == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

$('#loadshow').on('click', function() {

$("#loadshow").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');

var pagenum = $('#pagenum').val();

var newpage = parseInt(pagenum) + 1;

// code of searchajax function

var x = document.getElementById("searchpbr");

var xsearch = x.value;

var citzi = document.getElementById("searchcityz");

var xcitzi = citzi.value;

var xcategory = '';

if ($("input[name='category']:checked").val()) {

xcategory = $("input[name='category']:checked").val();

}

if (xsearch == '' && xcitzi == '' && xcategory == '') {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: newpage
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(newpage);

$('#list_view_bg_sect').append(msg);

$("#loadshow").html('Load More');

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}


}

});


} else {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: newpage
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(newpage);

$('#list_view_bg_sect').append(msg);

$("#loadshow").html('Load More');

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});

}

});

});

$(document).ready(function() {
$('#closeaddtoc').click(function(e) {
$("#addcartalt").hide();
});
});

function apply_qty(productid) {

var qty = 1;

var product_id = productid;

var vendor_id = $('#vendor_id').val();

var purl = $('#purl').val();

$.ajax({

type: "post",

headers: {

'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

},

data: {

"_token": "{{ csrf_token() }}",

"qty": qty,

"product_id": product_id,

"vendor_id": vendor_id

},

url: "{{ url('/addtocart') }}",

success: function(msg) {

$('#session_qty').text("(" + msg.cartcount + ")");

$(".dcart a").prop("href", "https://www.grambunny.com/cart");

$("#addcartalt").show();

$("#msgaddtocart").text(msg.msg);

//window.location.replace(purl);

}

});

}
</script>

<script type="text/javascript">
$(document).ready(function() {

$('#catfilter input').on('change', function() {

var cateId = $("input[name='category']:checked").val();

$('.astro').removeClass("catactive");

$("input[name='category']:checked").addClass("catactive");

searchKeypbr();

});

$('#catfiltermo input').on('change', function() {

var cateId = $("input[name='categorymo']:checked").val();

$('.astromo').removeClass("catactive");

$("input[name='categorymo']:checked").addClass("catactive");

searchKeypbrmo();

});


});
</script>

<?php // if(empty(Session::get('latitudegeo'))){
?>

<script>
function clFunction() {

if (navigator.geolocation) {

navigator.geolocation.getCurrentPosition(showPosition);

}

function showPosition(position) {

var lat = position.coords.latitude;

var long = position.coords.longitude;


$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
latitude: lat,
longitude: long
},

},

url: "{{ url('/searchproductonload_new') }}",

success: function(msg) {

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});

}

}


$(window).on('load', function() {

if (navigator.geolocation) {

navigator.geolocation.getCurrentPosition(showPosition);

}

function showPosition(position) {

var lat = position.coords.latitude;

var long = position.coords.longitude;

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
latitude: lat,
longitude: long
},

},

url: "{{ url('/searchproductonload_new') }}",

success: function(msg) {

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});

}

}) 
</script>

<?php // }
?>

<script>
setInterval(searchKeypbr, 300000);

setInterval(searchKeypbrmo, 300000);

function searchKeypbr() {

var page = 1;

var x = document.getElementById("searchpbr");

var xsearch = x.value;

var citzi = document.getElementById("searchcityz");

var xcitzi = citzi.value;

var xcategory = '';

if ($("input[name='category']:checked").val()) {

xcategory = $("input[name='category']:checked").val();

}

/*if($("input[name='categorymo']:checked").val()){

xcategory = $("input[name='categorymo']:checked").val();

}*/

if (xsearch == '' && xcitzi == '' && xcategory == '') {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: page
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(page);

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});


} else {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: page
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(page);

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}


}

});

}

}

function searchKeypbrmo() {

var page = 1;
$('#pagenum').val(page);

var x = document.getElementById("searchpbr");

var xsearch = x.value;

var citzi = document.getElementById("searchcityz");

var xcitzi = citzi.value;

var xcategory = '';

if ($("input[name='categorymo']:checked").val()) {

xcategory = $("input[name='categorymo']:checked").val();

}

if (xsearch == '' && xcitzi == '' && xcategory == '') {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: page
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(page);

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});

} else {

$.ajax({

type: "post",

headers: {
'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
},

data: {

"_token": "{{ csrf_token() }}",

"data": {
searchpbr: xsearch,
cityzip: xcitzi,
category: xcategory,
pages: page
},

},

url: "{{ url('/searchproduct_new') }}",

success: function(msg) {

$('#pagenum').val(page);

$('#ajaxdatacall').html(msg);

var totalvendor = $('#totalvendor').val();

if (totalvendor == 0) {

$('#loadshow').hide();

} else {

$('#loadshow').show();

}

}

});

}

}
</script>


<!-- < baaner ands & product secttion end> -->
@endsection
@section('scripts')
<script>
function initMap() {
var myMapCenter = {
lat: <?php echo $latitude; ?>,
lng: <?php echo $longitude; ?>
};

// Create a map object and specify the DOM element for display.
var map = new google.maps.Map(document.getElementById('map'), {
center: myMapCenter,
zoom: 11
});


function markStore(storeInfo) {

var appUrl = "<?php echo url('/'); ?>";

// Create a marker and set its position.
var marker = new google.maps.Marker({
map: map,
position: storeInfo.location,
title: storeInfo.name,
icon: appUrl + "/public/uploads/" + storeInfo.icon
});

// show store info when marker is clicked
marker.addListener('click', function() {
showStoreInfo(storeInfo);
});
}

function markStoreuser() {

var marker = new google.maps.Marker({
position: {
lat: <?php echo $latitude; ?>,
lng: <?php echo $longitude; ?>
},
map: map,
draggable: true,

});

// show store info when marker is clicked
marker.addListener('click', function() {
showStoreInfo(storeInfo);
});
}

// show store info in text box
function showStoreInfo(storeInfo) {
var info_div = document.getElementById('info_div');


window.location.href = "<?php echo $baseurl; ?>/" + storeInfo.name + "/store";

//info_div.innerHTML = 'Store name: '+ storeInfo.name;

}

var stores = <?php echo $drivermaps; ?>

stores.forEach(function(store) {
markStore(store);
});

markStoreuser();

}
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

$("#mapview").click(function() {

$('html, body').animate({
scrollTop: parseInt($("#ajaxdatacall").offset().top)
}, 2000);

});

$("#listview").click(function() {

$('html, body').animate({
scrollTop: parseInt($("#list_view_bg_sect").offset().top)
}, 2000);

});

});


</script>
@endsection
