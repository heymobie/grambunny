<?php if($pages==1){ ?>
<!-- <category secttion end> -->
<!-- <map secttion start> -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="map">

                <div class="welcome-area wow fadeInUp" style="margin-top:0 !important;" data-wow-delay="200ms"
                    style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">

                    <div id="map"></div>


                    <div class="container">
                        <div class="list_view_bg_sect">

                            <?php $storedata = [];
                            $vdistance = ''; ?>

                            <?php foreach ($verdorslist as $key => $value) { 

            if($value->map_icon){
                
                 $mapicon = $value->map_icon;

                }else{

                  $mapicon = 'map.png';

                } 

            ?>

                            <?php $storedata[] = ['name' => "'" . $value->username . "'", 'location' => ['lat' => $value->lat, 'lng' => $value->lng], 'icon' => "'" . $mapicon . "'"]; ?>

                            <?php foreach ($results as $key => $revalue) {
                                if ($revalue->vendor_id == $value->vendor_id) {
                                    $vdistance = $revalue->distance;
                                }
                            } ?>

                            <div class="col-md-2 listing_width_eq equal-col dltv pilist" id="listview"
                                style="display:none;">
                                <div class="sf-search-result-girds" id="proid-30">
                                    <div class="sf-featured-top">

                                        <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">
                                            <div class="prduc_img">

                                                <?php if(!empty($value->profile_img1)){ ?>

                                                <img
                                                    src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                                                <?php }else{ ?>

                                                <img src="<?php echo url('/'); ?>/public/uploads/user/user.jpg">

                                                <?php } ?>

                                            </div>


                                            <div class="descrption_set">
                                                <div class="sf-featured-provider">
                                                    {{ $value->business_name }}<!--{{ $value->name }} {{ $value->last_name }}-->
                                                </div>
                                                <div class="sf-featured-address">{{ $value->description }}</div>

                                                <div class="sf-featured-provider"><i aria-hidden="true"
                                                        class="fa fa-check-circle"></i> {{ $value->username }} </div>

                                                <div class="sf-featured-address"><i
                                                        class="fa fa-map-marker"></i>Distance : <?php echo round($vdistance, 1); ?>
                                                    Miles</div>

                                                <?php $ratings = $value->avg_rating; ?>
                                                <div class="star-rating">

                                                    <div class="rating">

                                                        <?php
                                                        
                                                        if ($ratings == 1) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        } elseif ($ratings == 2) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        } elseif ($ratings == 3) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        } elseif ($ratings == 4) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        } elseif ($ratings == 5) {
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        
                                                            echo '<i class="fa fa-star"></i>';
                                                        } elseif ($ratings == '0') {
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        
                                                            echo '<i class="fa fa-star-o"></i>';
                                                        }
                                                        
                                                        ?>

                                                        <!--<span>5.0(2)</span>-->
                                                    </div>

                                                    <input type="hidden" name="whatever1" class="rating-value"
                                                        value="2.56">
                                                </div>

                                            </div>
                                        </a>
                                    </div>
                                    <!--             <div class="sf-featured-bot">
               <div class="sf-featured-text"></div>
               <div class="btn-group sf-provider-tooltip">
                  <a href="<?php echo url('/'); ?>/{{ $value->username }}/store" class="text-dark">View Store</a>
               </div>
            </div> -->
                                </div>
                            </div>

                            <?php } ?>



                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php $drivermap = json_encode($storedata);
$drivermaps = str_replace('"', '', $drivermap); ?>

<!-- <baaner ands & product secttion start> -->

<div class="container">
    <div class="row">

        <div class="b_add">
            <div class="col-md-6">
                <div class="adds_banner">
                    @if ($advertisement1)
                        <img src="{{ asset('public/uploads/advertisement/' . $advertisement1->image) }}">
                    @endif
                </div>
            </div>
            <div class="col-md-6 ads_rgt">
                <div class="adds_banner">
                    @if ($advertisement2)
                        <img src="{{ asset('public/uploads/advertisement/' . $advertisement2->image) }}">
                    @endif
                </div>
            </div>
        </div>

        <div class="alert alert-success" id="addcartalt" style="display: none; color: green;background: lightyellow;">
            <button type="button" class="close" id="closeaddtoc">&times;</button>
            <strong id="msgaddtocart">Product Added Successfully!</strong>
        </div>


        <div class="product-listing-section" data-aos="fade-up" data-aos-delay="200">
            <div class="row row_listing">
                <div class="col-md-12">

                    <div class="list_view_bg_sect" id="list_view_bg_sect">

                        <?php $storedata = [];
                        $vdistance = ''; ?>

                        <?php foreach ($verdorslist as $key => $value) { 

            foreach ($results as $key => $revalue) {

            if($revalue->vendor_id==$value->vendor_id){

              $vdistance = $revalue->distance;

             }

            } ?>

                        <div class="col-md-2 listing_width_eq equal-col pilist" id="listviewscroll">
                            <div class="sf-search-result-girds" id="proid-30">
                                <div class="sf-featured-top">

                                    <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">
                                        <div class="prduc_img">

                                            <?php if(!empty($value->profile_img1)){ ?>

                                            <img
                                                src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                                            <?php }else{ ?>

                                            <img src="<?php echo url('/'); ?>/public/uploads/user/user.jpg">

                                            <?php } ?>

                                        </div>


                                        <div class="descrption_set">
                                            <div class="sf-featured-provider">
                                                {{ $value->business_name }}<!--{{ $value->name }} {{ $value->last_name }}-->
                                            </div>
                                            <div class="sf-featured-address">{{ $value->description }}</div>

                                            <div class="sf-featured-provider"><i aria-hidden="true"
                                                    class="fa fa-check-circle"></i> {{ $value->username }} </div>

                                            <div class="sf-featured-address"><i class="fa fa-map-marker"></i>Distance :
                                                <?php echo round($vdistance, 1); ?> Miles</div>

                                            <?php $ratings = $value->avg_rating; ?>
                                            <div class="star-rating">

                                                <div class="rating">

                                                    <?php
                                                    
                                                    if ($ratings == 1) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    } elseif ($ratings == 2) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    } elseif ($ratings == 3) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    } elseif ($ratings == 4) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    } elseif ($ratings == 5) {
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    
                                                        echo '<i class="fa fa-star"></i>';
                                                    } elseif ($ratings == '0') {
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    
                                                        echo '<i class="fa fa-star-o"></i>';
                                                    }
                                                    
                                                    ?>

                                                    <!--<span>5.0(2)</span>-->
                                                </div>

                                                <input type="hidden" name="whatever1" class="rating-value"
                                                    value="2.56">
                                            </div>

                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <?php } ?>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<input type="hidden" name="totalvendor" id="totalvendor" value="{{ $total }}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#closeaddtoc').click(function(e) {
            $("#addcartalt").hide();
        });
    });

    function apply_qty(productid) {

        var qty = 1;

        var product_id = productid;

        var vendor_id = $('#vendor_id').val();

        var purl = $('#purl').val();

        $.ajax({

            type: "post",

            headers: {

                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

            },

            data: {

                "_token": "{{ csrf_token() }}",

                "qty": qty,

                "product_id": product_id,

                "vendor_id": vendor_id

            },

            url: "{{ url('/addtocart') }}",

            success: function(msg) {

                $('#session_qty').text("(" + msg.cartcount + ")");

                $(".dcart a").prop("href", "https://www.grambunny.com/cart");

                $("#addcartalt").show();

                $("#msgaddtocart").text(msg.msg);

                //window.location.replace(purl);

            }

        });

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#catfilter input').on('change', function() {

            var cateId = $("input[name='category']:checked").val();

            $('.astro').removeClass("catactive");

            $("input[name='category']:checked").addClass("catactive");

            searchKeypbr();

        });

        $('#catfiltermo input').on('change', function() {

            var cateId = $("input[name='categorymo']:checked").val();

            $('.astromo').removeClass("catactive");

            $("input[name='categorymo']:checked").addClass("catactive");

            searchKeypbrmo();

        });


    });
</script>

<script>
    function searchKeypbr() {

        var page = 1;

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='category']:checked").val()) {

            xcategory = $("input[name='category']:checked").val();

        }

        /* if($("input[name='categorymo']:checked").val()){

         xcategory = $("input[name='categorymo']:checked").val();

         } */


        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory,
                    pages: page
                },

            },

            url: "{{ url('/searchproduct') }}",

            success: function(msg) {

                $('#pagenum').val(page);

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }


            }

        });

    }

    function searchKeypbrmo() {

        var page = 1;

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='categorymo']:checked").val()) {

            xcategory = $("input[name='categorymo']:checked").val();

        }

        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory,
                    pages: page
                },

            },

            url: "{{ url('/searchproduct') }}",

            success: function(msg) {

                $('#pagenum').val(page);

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }

            }

        });

    }
</script>


<script>
    function initMap() {
        var myMapCenter = {
            lat: <?php echo $latitude; ?>,
            lng: <?php echo $longitude; ?>
        };

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myMapCenter,
            zoom: 11
        });


        function markStore(storeInfo) {

            var appUrl = "<?php echo url('/'); ?>";

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
                map: map,
                position: storeInfo.location,
                title: storeInfo.name,
                icon: appUrl + "/public/uploads/" + storeInfo.icon
            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        function markStoreuser() {

            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $latitude; ?>,
                    lng: <?php echo $longitude; ?>
                },
                map: map,
                draggable: true,

            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        // show store info in text box
        function showStoreInfo(storeInfo) {
            var info_div = document.getElementById('info_div');


            window.location.href = "<?php echo url('/'); ?>/" + storeInfo.name + "/store";

            //info_div.innerHTML = 'Store name: '+ storeInfo.name;

        }

        var stores = <?php echo $drivermaps; ?>

        stores.forEach(function(store) {
            markStore(store);
        });

        markStoreuser();

    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
    defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#mapview").click(function() {

            //$("#listview").show();
            $("#mapview").css("color", "#FFA41C");
            $("#listview").css("color", "#ffffff");
            //$(".dltv").hide();
            //$("#map").show();

            $('html, body').animate({
                scrollTop: parseInt($("#ajaxdatacall").offset().top)
            }, 1000);


        });

        $("#listview").click(function() {

            //$("#mapview").show();
            $("#listview").css("color", "#FFA41C");
            $("#mapview").css("color", "#ffffff");
            //$(".dltv").show();
            // $("#map").hide();

            $('html, body').animate({
                scrollTop: parseInt($("#listviewscroll").offset().top)
            }, 1000);

        });


    });
</script>

<?php }else{ ?>

<?php $storedata = [];
$vdistance = ''; ?>

<?php foreach ($verdorslist as $key => $value) { 

            if($value->map_icon){
                
                 $mapicon = $value->map_icon;

                }else{

                  $mapicon = 'map.png';

                } 

            ?>

<?php $storedata[] = ['name' => "'" . $value->username . "'", 'location' => ['lat' => $value->lat, 'lng' => $value->lng], 'icon' => "'" . $mapicon . "'"]; ?>

<?php foreach ($results as $key => $revalue) {
    if ($revalue->vendor_id == $value->vendor_id) {
        $vdistance = $revalue->distance;
    }
} ?>

<div class="col-md-2 listing_width_eq equal-col dltv pilist" id="listview" style="display:none;">
    <div class="sf-search-result-girds" id="proid-30">
        <div class="sf-featured-top">

            <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">
                <div class="prduc_img">

                    <?php if(!empty($value->profile_img1)){ ?>

                    <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                    <?php }else{ ?>

                    <img src="<?php echo url('/'); ?>/public/uploads/user/user.jpg">

                    <?php } ?>

                </div>


                <div class="descrption_set">
                    <div class="sf-featured-provider">
                        {{ $value->business_name }}<!--{{ $value->name }} {{ $value->last_name }}--></div>
                    <div class="sf-featured-address">{{ $value->description }}</div>

                    <div class="sf-featured-provider"><i aria-hidden="true" class="fa fa-check-circle"></i>
                        {{ $value->username }} </div>

                    <div class="sf-featured-address"><i class="fa fa-map-marker"></i>Distance : <?php echo round($vdistance, 1); ?>
                        Miles</div>

                    <?php $ratings = $value->avg_rating; ?>
                    <div class="star-rating">

                        <div class="rating">

                            <?php
                            
                            if ($ratings == 1) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 2) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 3) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 4) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 5) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            } elseif ($ratings == '0') {
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            }
                            
                            ?>

                            <!--<span>5.0(2)</span>-->
                        </div>

                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                    </div>

                </div>
            </a>
        </div>

    </div>
</div>

<?php } ?>


<?php $drivermap = json_encode($storedata);
$drivermaps = str_replace('"', '', $drivermap); ?>


<?php $storedata = [];
$vdistance = ''; ?>

<?php foreach ($verdorslist as $key => $value) { 

            foreach ($results as $key => $revalue) {

            if($revalue->vendor_id==$value->vendor_id){

              $vdistance = $revalue->distance;

             }

            } ?>

<div class="col-md-2 listing_width_eq equal-col pilist" id="listviewscroll">
    <div class="sf-search-result-girds" id="proid-30">
        <div class="sf-featured-top">

            <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">
                <div class="prduc_img">

                    <?php if(!empty($value->profile_img1)){ ?>

                    <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                    <?php }else{ ?>

                    <img src="<?php echo url('/'); ?>/public/uploads/user/user.jpg">

                    <?php } ?>

                </div>


                <div class="descrption_set">
                    <div class="sf-featured-provider">
                        {{ $value->business_name }}<!--{{ $value->name }} {{ $value->last_name }}--></div>
                    <div class="sf-featured-address">{{ $value->description }}</div>

                    <div class="sf-featured-provider"><i aria-hidden="true" class="fa fa-check-circle"></i>
                        {{ $value->username }} </div>

                    <div class="sf-featured-address"><i class="fa fa-map-marker"></i>Distance : <?php echo round($vdistance, 1); ?>
                        Miles</div>

                    <?php $ratings = $value->avg_rating; ?>
                    <div class="star-rating">

                        <div class="rating">

                            <?php
                            
                            if ($ratings == 1) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 2) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 3) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 4) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            } elseif ($ratings == 5) {
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            
                                echo '<i class="fa fa-star"></i>';
                            } elseif ($ratings == '0') {
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            
                                echo '<i class="fa fa-star-o"></i>';
                            }
                            
                            ?>

                            <!--<span>5.0(2)</span>-->
                        </div>

                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                    </div>

                </div>
            </a>
        </div>

    </div>
</div>

<?php } ?>

<input type="hidden" name="totalvendor" id="totalvendor" value="{{ $total }}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#closeaddtoc').click(function(e) {
            $("#addcartalt").hide();
        });
    });

    function apply_qty(productid) {

        var qty = 1;

        var product_id = productid;

        var vendor_id = $('#vendor_id').val();

        var purl = $('#purl').val();

        $.ajax({

            type: "post",

            headers: {

                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

            },

            data: {

                "_token": "{{ csrf_token() }}",

                "qty": qty,

                "product_id": product_id,

                "vendor_id": vendor_id

            },

            url: "{{ url('/addtocart') }}",

            success: function(msg) {

                $('#session_qty').text("(" + msg.cartcount + ")");

                $(".dcart a").prop("href", "https://www.grambunny.com/cart");

                $("#addcartalt").show();

                $("#msgaddtocart").text(msg.msg);

                //window.location.replace(purl);

            }

        });

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#catfilter input').on('change', function() {

            var cateId = $("input[name='category']:checked").val();

            $('.astro').removeClass("catactive");

            $("input[name='category']:checked").addClass("catactive");

            searchKeypbr();

        });

        $('#catfiltermo input').on('change', function() {

            var cateId = $("input[name='categorymo']:checked").val();

            $('.astromo').removeClass("catactive");

            $("input[name='categorymo']:checked").addClass("catactive");

            searchKeypbrmo();

        });


    });
</script>

<script>
    function searchKeypbr() {

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='category']:checked").val()) {

            xcategory = $("input[name='category']:checked").val();

        }

        /* if($("input[name='categorymo']:checked").val()){

         xcategory = $("input[name='categorymo']:checked").val();

         } */


        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory
                },

            },

            url: "{{ url('/searchproduct') }}",

            success: function(msg) {

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }

            }

        });

    }

    function searchKeypbrmo() {

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='categorymo']:checked").val()) {

            xcategory = $("input[name='categorymo']:checked").val();

        }

        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory
                },

            },

            url: "{{ url('/searchproduct') }}",

            success: function(msg) {

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }


            }

        });

    }
</script>


<script>
    function initMap() {
        var myMapCenter = {
            lat: <?php echo $latitude; ?>,
            lng: <?php echo $longitude; ?>
        };

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myMapCenter,
            zoom: 11
        });


        function markStore(storeInfo) {

            var appUrl = "<?php echo url('/'); ?>";

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
                map: map,
                position: storeInfo.location,
                title: storeInfo.name,
                icon: appUrl + "/public/uploads/" + storeInfo.icon
            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        function markStoreuser() {

            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $latitude; ?>,
                    lng: <?php echo $longitude; ?>
                },
                map: map,
                draggable: true,

            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        // show store info in text box
        function showStoreInfo(storeInfo) {
            var info_div = document.getElementById('info_div');


            window.location.href = "<?php echo url('/'); ?>/" + storeInfo.name + "/store";

            //info_div.innerHTML = 'Store name: '+ storeInfo.name;

        }

        var stores = <?php echo $drivermaps; ?>

        stores.forEach(function(store) {
            markStore(store);
        });

        markStoreuser();

    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
    defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#mapview").click(function() {

            //$("#listview").show();
            $("#mapview").css("color", "#FFA41C");
            $("#listview").css("color", "#ffffff");
            //$(".dltv").hide();
            //$("#map").show();

            $('html, body').animate({
                scrollTop: parseInt($("#ajaxdatacall").offset().top)
            }, 1000);


        });

        $("#listview").click(function() {

            //$("#mapview").show();
            $("#listview").css("color", "#FFA41C");
            $("#mapview").css("color", "#ffffff");
            //$(".dltv").show();
            // $("#map").hide();

            $('html, body').animate({
                scrollTop: parseInt($("#listviewscroll").offset().top)
            }, 1000);

        });


    });
</script>

<?php } ?>
