@extends('layouts.app')



@section("other_css")

   	  <!-- Radio and check inputs -->

    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <link href="{{ url('/') }}/design/front/css/ion.rangeSlider.css" rel="stylesheet">

    <link href="{{ url('/') }}/design/front/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <!--[if lt IE 9]>

      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>

      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>

    <![endif]-->

<meta name="_token" content="{!! csrf_token() !!}"/>

 <style>

.thumb_strip img {

    width: 98px;

    height: 98px;

}

    </style>

@stop

@section('content')





<!-- SubHeader =============================================== -->

<!-- End section -->

<!-- End SubHeader ============================================ -->



     <div id="position">

        <div class="container">

            <ul>

                <li><a href="{{ url('/')}}">Home</a></li>

                <li>View all Post Order</li>

            </ul>

        </div>

    </div>

	<!-- Position -->

<!-- End Map -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">



        <div class="row">

            <div class="col-md-12">

                <!--<div id="tools">-->

                <div>

                    <div class="row">

                      

                        <div class="col-md-3 col-sm-3 col-xs-12">



                            <p class="top-ordr-btn">

                                <a  href="{{ url('/myorder') }}" class="btn_map ">Food Orders</a>

                               

                            </p>

                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12">



                            <p class="top-ordr-btn">

                                <a href="{{ url('/mytransportorder') }}" class="btn_map active">Vehicle Bookings</a>

                            </p>

                        </div>



                        <!--<div class="col-md-9 col-sm-6 hidden-xs">

            </div>-->

                    </div>

                </div>

                <!--End tools -->

            </div>

        </div>



        <div class="row">



            <!--End col-md -->



            <div class="col-md-12">

			

			

					 @if(Session::has('message'))

					 

					 <div class="alert alert-success alert-dismissable">

                          <i class="fa fa-check"></i>

                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                                       {{Session::get('message')}}

                     </div>

					@endif

					

				<div id="listing_restaurant">

			@if(!empty($order_detail))

				@foreach($order_detail as $special_list)

				

				<div class="strip_list wow fadeIn" data-wow-delay="0.1s">

                    <div class="row">

                        <div class="col-md-9 col-sm-9">

                            <div class="desc">

                                <div class="thumb_strip">

								

								<?php 

	  

	  $cove_img = DB::table('vehicle_img')	

						->where('vehicle_id', '=' ,$special_list->vehicle_id)

						->where('isCover', '=' ,'1') 				

						->get();

	if(count($cove_img)>0){					

	  ?>

	   <img src="{{ url('/') }}/uploads/vehicle/{{$cove_img[0]->imgPath}}" alt="">

	  <?php }else{?>

	  

	  <!--<img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">-->

	  <img src="{{ url('/') }}/design/front/img/logo.png" alt="">	

	  <?php }?>

								

									

                                </div>

                                

                                <h3>{{$special_list->make_name.' - '.$special_list->model_name}}({{$special_list->vehicle_rego}})  </h3>

								

								 <div class="type">

									{{$special_list->transport_name}}

                                </div>

								 <div class="type">

									Ordered on {{date('d M Y',strtotime($special_list->order_create)) }}                                 </div>

                          

                                <div class="type">

								

									<p> From Location : {{$special_list->order_from_location}}, On Date & Time: {{date('d M Y',strtotime($special_list->order_ondate)) }} {{$special_list->order_ontime}}</p>

								

									<p> To Location : {{$special_list->order_to_location}}, Return Date & Time: {{date('d M Y',strtotime($special_list->order_returndate)) }} {{$special_list->order_returntime}}</p>

									

                                </div>

								

									  

									 

									

								

                                <div class="location">

								 <?php $cart_price = 0; ?>									

@if(isset($special_list->order_carditem) && (count($special_list->order_carditem)))

		 <?php	$order_carditem = json_decode($special_list->order_carditem, true);?>			 

	 @foreach($order_carditem as $item)

	<?php  $cart_price = $cart_price+$item['price'];?>

		{{$item['qty']}} Days {{ str_replace('_',' ',$item['name'])}}, 

	 @if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))

		@foreach($item['options']['addon_data'] as $addon)			

		  <?php $cart_price = $cart_price+$addon['price'];?>

		 {{$addon['name']}} 

		 @endforeach

	 @endif 

		

	 @endforeach

@endif



								

                                </div>

							

							

                            <ul>					

                                <li><strong>Booking Type: <?php echo str_replace('_',' ',$special_list->pick_bookingtype);?></strong></li>

                            </ul>

							

							

                            <ul>					

                                <li><strong>SubTotal: ${{number_format($special_list->order_subtotal,2)}}</strong></li>

                            </ul>

                            <ul>					

                                <li><strong>Service Charge: ${{number_format($special_list->order_deliveryfee,2)}}</strong></li>

                            </ul>

							

							@if($special_list->total_night>0)

							

                            <ul>					

                                <li><strong>Driver overnight allownce: ${{number_format($special_list->total_allownce,2)}}</strong></li>

                            </ul>

							@endif

							

							

							@if($special_list->order_promo_cal>0)

							

                            <ul>					

                                <li><strong>Discount: -${{number_format($special_list->order_promo_cal,2)}}</strong></li>

                            </ul>

							@endif

							

							

                            <ul>					

                                <li><strong>Total: ${{number_format(($special_list->order_total_amt),2)}}</strong></li>

                            </ul>

							

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-3">

                            <div class="go_to">                                

                                <div class="ordr-lstng">									

									<div style="margin:5px;">

										<?php /*?><a href="{{url('/orderreview/'.$special_list->order_id)}}" class="btn_1">Add Review</a><?php */?>

										

										

										@if((empty($special_list->re_id)) && ($special_list->order_status=='5'))

										

										<a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->transport_suburb)).'/'.str_replace(' ','_',trim($special_list->vehicle_rego)).'/'.str_replace(' ','_',trim($special_list->vclass_name)).'/'.str_replace(' ','_',trim($special_list->model_name)).'/'.str_replace(' ','_',trim($special_list->vehicle_id))).'/review?refno='.$special_list->order_id)}}" class="btn_1">Add Review</a>

										@endif

									</div>

                                </div>

                            </div>

                        </div>

                    </div><!-- End row-->

                </div>

				

                <!-- End col-md-6-->

			

				@endforeach

			@else

				<center><strong>You not have any Order!</strong></center>

			@endif

					</div>

					

				

            </div><!-- End col-md-9-->



        </div><!-- End row -->

    </div><!-- End container -->

    <!-- End Content =============================================== -->

@stop

@section('js_bottom')



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>





<!-- COMMON SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>

<script src="{{ url('/') }}/design/front/js/functions.js"></script>

<script src="{{ url('/') }}/design/front/assets/validate.js"></script>



<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/cat_nav_mobile.js"></script>

<script>$('#cat_nav').mobileMenu();</script>

<script src="{{ url('/') }}/design/front/js/ion.rangeSlider.js"></script>

<script>

	$(function () {

		'use strict';

		$("#range").ionRangeSlider({

			hide_min_max: true,

			keyboard: true,

			min: 0,

			max: 15,

			from: 0,

			to: 5,

			type: 'double',

			step: 1,

			prefix: "Km ",

			grid: true

		});

	});

</script>

<script type="text/javascript">

	$(document).ready(function () {

		//<i id="collapseIcon" class="icon-plus-1 pull-right"></i>



		$("#filters_col_bt").click(function () {

			//if (!$(this).hasClass("collapsed")) {

				if ($("#collapseIcon").hasClass("icon-plus-1")) {

					$("#collapseIcon").removeClass("icon-plus-1");

					$("#collapseIcon").addClass("icon-minus-1");

				}

				else {

					$("#collapseIcon").removeClass("icon-minus-1");

					$("#collapseIcon").addClass("icon-plus-1");

				}

			//}

			//else {

			//    $("#collapseIcon")

			//}

		});

	});

$(document).on('click', '[id^="rest_list_load-"]', function(){ 



$("#ajax_favorite_loddder").show();	

  

		var page = $(this).attr('data-nextpage');

		

			$("#rest_list_load-"+page).hide();

			

		$.ajax({

		//type: "post",

		//dataType: "json",

		url:  '?page=' + page,

		success: function(msg) {

		$("#ajax_favorite_loddder").hide();	

			$("#listing_restaurant").append(msg);

				

				/*$("#ajax_favorite_loddder").hide();	

				$('#BankDetail').html(msg);*/

			}

		});



		

		

});	

	  $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

});

</script>

@stop