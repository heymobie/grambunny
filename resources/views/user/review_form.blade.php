@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Review</li>
            </ul>
        </div>
    </div><!-- Position -->
 
<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
		
					<div>
					<form role="form" id="update_frm" method="POST" action="{{ url('/order_review_action') }}">
                        {{ csrf_field() }}
						<input type="hidden" name="order_id" value="{{$order_id}} " />
						<input type="hidden" name="user_id" value="{{$user_id}} " />
						<input type="hidden" name="rest_id" value="{{$order_detail[0]->rest_id}} " />
						
						
					<div class="box_style_2">
          <h2 class="inner">Your Review</h2>
          
		   <div class="row">
          
          
          <div class="form-group">
            <label></label>
			<textarea id="review_text" name="review_text" placeholder="To make your review more relevant consider adding restaurant name, cuisine and food item to your review" class="form-control" required="required"></textarea>
          </div>
          
          
		  <div class="form-group">
		  <div class="col-md-12 ">
              <button type="submit" class="btn btn-submit margin_30" id="update_infobtn">                           <i class="fa fa-btn fa-user"></i> Update
               </button>
			  
             </div>
          </div>
		  </div>
		 
		 </div>
		  </form>
        </div>
            
        </div>
    </div>
</div>

</div><!-- End row -->
	<hr class="more_margin">
    
	<!-- End row -->
	<!-- End row -->
	<!-- End row -->
</div><!-- End container -->

<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	

@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->
<script>
	 $(document).on('click', '#update_infobtn', function(){ 
		 	
			var form = $("#update_frm");
				form.validate();
			var valid =	form.valid();
		 });
</script>

@stop	