@extends('layouts.app')



@section("other_css")





<meta name="_token" content="{!! csrf_token() !!}"/>



<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">

<link rel="apple-touch-icon" type="image/x-icon" href="{{ url('/') }}/design/front/img/apple-touch-icon-57x57-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{ url('/') }}/design/front/img/apple-touch-icon-72x72-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{ url('/') }}/design/front/img/apple-touch-icon-114x114-precomposed.png">

<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{ url('/') }}/design/front/img/apple-touch-icon-144x144-precomposed.png">

<link rel="stylesheet" href="{{ url('/') }}/design/front/css/jquery-ui.css">



@stop

@section('content')



  <div class="container margin_60_35" style="margin-top: 50px;">

	<div id="container_pin">

		<div class="row">
          
<div class="col-md-8">
				<div class="box_style_2">
					<h2 class="inner">Payment methods</h2>					
						
						
					<div class="payment_select">
						<label class="control control--radio">
						<input type="radio" id="payment_option" value="credit_card" class="rest_option rest_payment_option payment_option"  name="payment_method" >Credit card or Paypal
							<div class="control__indicator"></div>
							<i class="icon_creditcard"></i>
						</label>
						
					</div>
					<form name="cart_detail_frm" id="cart_detail_frm" method="post">
					
					<input type="hidden" name="sub_total" value="{{Session::get('cart_userinfo')['sub_total']}}"/>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf">
					<input type="hidden" name="total_amt" value="{{Session::get('cart_userinfo')['total_charge']}}"/>
					<input type="hidden" name="service_tax" value="{{Session::get('cart_userinfo')['service_tax']}}"/>

					<div id="dropin-container">					
					</div>
					</form>

					<?php
					
					if ((Session::get('cart_userinfo')['partical_payment_alow']==0))
					{
					?>

					<?php }?>
				</div><!-- End box_style_1 -->
			</div>
			<!-- End col-md-6 -->

            

			<div class="col-md-4" id="sidebar">

            <div class="theiaStickySidebar">

				<div id="cart_box">

					<h3>Wallet Amount<i class="icon_cart_alt pull-right"></i></h3>

					<?php $cart_price = 0; $count_number = '1'; $frm_field = '';$total_amt=0;?>


					<table class="table table_summary">

					<tbody>

                  
                  <td class="total"> Total Amount : 30
                    <input type="hidden" value ="25" name="grand" class="grand">

                    <input type="hidden" value ="30" name="grand_total" class="grand_total">
                  </td>
                </tr>
				

					

					</tbody>

					</table>

					<hr>

					<a class="btn_full" href="javascript:void(0)" id="oder_confrim">Confirm Add Money To Wallet</a>
					<!--button type="button" style="display: none;" class="btn_full" id="submit_button">Confirm your order</button-->
				</div><!-- End cart_box -->

                </div>

			</div><!-- End col-md-3 -->

            

		</div><!-- End row -->

	</div><!-- End container pin-->

</div>

    <!-- End Content =============================================== -->

@stop

@section('js_bottom')

<?php

//$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_url="";
//$paypal_id='votive.amit-business@gmail.com'; // Business email ID

//$paypal_id='votiveyogesh@gmail.com'; // Business email ID
  
//$paypal_id='info@shoutaparty.com';// Business email ID
$paypal_id ="";
//$paypal_id='toshikbuyer@gmail.com';// Business email ID S=>U: toshikparihar23@gmail.com  P: toshik71855

$notify_url = url('/ipn_notify');

?>

<!--https://www.paypal.com/cgi-bin/webscr-->

<!-- <form action="{{$paypal_url}}" method="post" id="payment_frm" >



<input type="hidden" name="cmd" value="_cart">

<input type="hidden" name="upload" value="1">

<input type="hidden" name="userid" value="1">

<input type="hidden" name="business" value="{{$paypal_id}}">

<input type="hidden" name="currency_code" value="USD">

<input type="hidden" name="custom" id="custom_order_payid" value="">

<input type="hidden" name="item_name_1" id="paypal_order_uniqe" value="">
 <input type="hidden" name="quantity_1" value="1">
<input type="hidden" name="amount_1" id="papal_total_amt" value=""-->

<input type="hidden" name="cancel_return" value="{{url('/order_cancel')}}">

<input type="hidden" name="return" value="{{url('/order_success')}}">

<input type="hidden" name="notify_url" value="<?php echo $notify_url;?>" />

	 

	<?php /*?><input type="hidden" name="cancel_return" value="http://localhost/shouta_party/order_cancel">

    <input type="hidden" name="return" value="http://localhost/shouta_party/order_success"><?php */?>

	

<!--<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">-->

</form>



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



<div class="modal fade" id="message_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title choice" id="show_popup_title_addon">Information Message</h4>

      </div>

      <div class="modal-body popup-ctn">

	  		<p id="alert_model_msg"></p>	

      </div>

    </div>

  </div>

</div>

<!-- COMMON SCRIPTS -->



<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>

<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>

<script src="{{ url('/') }}/design/front/js/functions.js"></script>

<script src="{{ url('/') }}/design/front/js/validate.js"></script>

<script src="https://js.braintreegateway.com/web/dropin/1.11.0/js/dropin.min.js"></script>

<!-- SPECIFIC SCRIPTS -->

<script src="{{ url('/') }}/design/front/js/custom.js"></script>

<script>
/*$(document).ready(function(){
	//braintreeUI();
});*/



    jQuery('#sidebar').theiaStickySidebar({

      additionalMarginTop: 80

    });

$(document).on('change', '.payment_option', function(){
		
	var selected_val = $(this).val();
	
	if(selected_val=='credit_card')
	{		
		$(".cart_field").prop('required',true);
		$('#oder_confrim').attr('id', 'submit_button');
		braintreeUI();
	}
	/*else if(selected_val=='paypal')
	{
		$(".cart_field").prop('required',false);
		$('#submit_button').attr('id', 'oder_confrim');
		//var button ="";
	}*/
	else if(selected_val=='cash')
	{
		$(".cart_field").prop('required',false);
		$('#submit_button').attr('id', 'oder_confrim');
		//var button ="";
	}
});	




$(document).on('click', '#oder_confrim', function(){


if($('.rest_payment_option').is(':checked'))
{
	
	/*if($('.rest_option:checked').val()=='credit_card')
	{
		
		//braintreeUI();
		

			var form = $('#cart_detail_frm');
			form.validate();
			var valid =	form.valid();
			if(valid)
			{
				$("#ajax_favorite_loddder").show();
				var frm_val = $('#cart_detail_frm').serialize();
			
				console.log(frm_val);
				 frm_val =frm_val+'&method_type=cart';
				$.ajax({
					type: "POST",
					url: "{{url('/checkout/cart_payment_process')}}",
					data: frm_val,
				   	dataType: 'json',
						success: function(msg) {
						console.log(msg); 					
							if(msg.status=='1')
							{
									window.location.href='<?php echo url('/order_detail')?>';
							}		
							else if(msg.status=='0')
							{
								$("#alert_model_msg").html(msg.message);
								$("#ajax_favorite_loddder").hide();
								$("#message_model").modal('show');
							}	
										
						}
					});
			}
		
	}
	else*/ /*if($('.rest_option:checked').val()=='paypal')
	{
		
	$("#ajax_favorite_loddder").show();
		$('#payment_frm').submit();
		
		
		$("#ajax_favorite_loddder").show();
		var frm_val = 'method_type=frm_val';
		$.ajax({
			type: "POST",
			url: "{{url('/checkout/paypal_payment_process')}}",
			//data: frm_val,
			data: {'_token':"<?php echo csrf_token(); ?>"},
		   dataType: 'json',
				success: function(msg) {
				//delivery_pick_time  
				console.log(msg);					
					if(msg.order_payid)
					{							
						$("#custom_order_payid").val(msg.order_payid);
						$("#paypal_order_uniqe").val('Order number :'+msg.order_uniqueid);
						$("#papal_total_amt").val(msg.order_payment);
						$('#payment_frm').submit();
					}			
								
				}
			});
			
			
	}
	else*/ if($('.rest_option:checked').val()=='cash')
	{			
	
	
		$("#ajax_favorite_loddder").show();
		var frm_val = 'method_type=frm_val';
		$.ajax({
			type: "POST",
			url: "{{url('/checkout/cash_payment_process')}}",
			//data: frm_val,
			data: {'_token':"<?php echo csrf_token(); ?>"}, 
		   dataType: 'json',
				success: function(msg) {
				//delivery_pick_time  					
					if(msg.order_id)
					{
							window.location.href='<?php echo url('/order_detail')?>';
					}			
								
				}
			});
	}
	
	
	
	//$('#payment_frm').submit();
}

else
{



	$('#alert_model_msg').html("Please Select Payment Method");			

	$('#message_model').modal('show');

}



});

function braintreeUI(){
	var button = document.querySelector('#submit_button');
	
	var total =	$(".grand_total").val();
	
	if(button!= "" || button!= null){
	braintree.dropin.create({
      authorization: "{{ Braintree_ClientToken::generate() }}",
      container: '#dropin-container',
      paypal: {
	    flow: 'checkout',
	    amount: total,
	    currency: 'USD'
	  }
    }, function (createErr, instance) {
      button.addEventListener('click', function () {

      	instance.requestPaymentMethod(function (err, payload) {
	        $.get('{{ route('payment.process') }}', {payload:payload,amt:total}, function (response) {
	        	console.log(response);
	        	$("#ajax_favorite_loddder").show();
	        	//alert(response.transaction.paymentInstrumentType);
	            if (response.success){
	            	if(response.transaction.paymentInstrumentType=="credit_card"){
		            	$.ajax({
						type: "POST",
						url: "{{url('/checkout/cart_payment_process')}}",
						//data: frm_val,
						data: {'_token':"<?php echo csrf_token(); ?>", 'values':response},
					   	dataType: 'json',
							success: function(msg) {
							console.log(msg); 

								if(msg.status=='1')
								{
										window.location.href='<?php echo url('/order_detail')?>';
								}		
								else if(msg.status=='0')
								{	console.log(msg);
									var transactionid = msg.trans_id;
									var amt = msg.amt;
									 $.get('{{ route('payment.cancel') }}', {transactionid:transactionid,amt:amt}, function (response) {
									 	console.log(response);
									 });
									$("#alert_model_msg").html("Order didn't processed!");
									$("#ajax_favorite_loddder").hide();
									//$("#message_model").modal('show');
								}else{
									$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
									$("#ajax_favorite_loddder").hide();
									$("#message_model").modal('show');	
								}		
											
							}
						});
			        }else if(response.transaction.paymentInstrumentType=="paypal_account"){

			        	
						$.ajax({
							type: "POST",
							url: "{{url('/checkout/paypal_payment_process')}}",
							//data: frm_val,
							data: {'_token':"<?php echo csrf_token(); ?>"},
						   dataType: 'json',
								success: function(msg) {
								//delivery_pick_time  
									console.log(msg);				
									if(msg.status==1){	
										window.location.href='<?php echo url('/order_detail')?>';		
									}else if(msg.status=='0')
									{	console.log(msg);
										var transactionid = msg.trans_id;
										var amt = msg.amt;
										 $.get('{{ route('payment.cancel') }}', {transactionid:transactionid,amt:amt}, function (response) {
										 	console.log(response);
										 });
										$("#alert_model_msg").html("Order didn't processed!");
										$("#ajax_favorite_loddder").hide();
										//$("#message_model").modal('show');
									}else{
										$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
										$("#ajax_favorite_loddder").hide();
										$("#message_model").modal('show');	
									}				
								}
							});
			        }	


	              console.log(response);
	            } else {
	              	$("#alert_model_msg").html("Something went wrong! Order didn't processed!");
					$("#ajax_favorite_loddder").hide();
					$('#message_model').modal('show');
	              console.log(response);
	              
	            }
	        }, 'json');
        });
      });
    });
}

 /*braintree.setup(
    "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIwNzUxYzZlYzBkMzAyYjYzMjM3NDFkZDRiYzMxNDc2MDQ1NTM0ZTE2YTZkOTVhMTMyZGRhY2E4ZDExYWNlOGYwfGNyZWF0ZWRfYXQ9MjAxOC0wNi0wN1QxMTo1NjoxMy4xNzg4NDEyOTYrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=",
    "dropin", {
      container: "payment-form"
    });
*/
  }



</script>





@stop