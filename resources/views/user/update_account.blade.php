@extends('layouts.app')



@section("other_css")



     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>



    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>



    <!-- Radio and check inputs -->



    <link href="{{asset("public/design/front/css/skins/square/grey.css")}}" rel="stylesheet">



@stop



@section('content')



   <!--  <div id="position">



        <div class="container">



            <ul>



                <li><a href="{{ url('/') }}">Home</a></li>



                <li>Update Account</li>



            </ul>



        </div>



    </div> --><!-- Position -->



 



<!-- Content ================================================== -->



<section class="myAccountSec">



<div class="container">



    <div class="row">



        <div class="col-md-6 col-md-offset-3">



					@if(Session::has('message'))

	 

					<div class="alert alert-success alert-dismissable">

          <i class="fa fa-check"></i>

          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

          {{Session::get('message')}}

          </div>



					@endif



					<div>



					<form role="form" id="update_frm" method="POST" action="{{ url('/update_account_action') }}">



                        {{ csrf_field() }}



						<input type="hidden" name="user_id" value="{{$id}} " />		



					<div class="box_style_2">



          <h2 class="inner">Your Account Details</h2>



          <div class="form-group">



            <label>First Name</label>



			<input id="name" type="text" class="form-control" name="name" value="{{$user_detail[0]->name}}" required="required" >



          </div>



          <div class="form-group">



            <label>Last Name</label>



              <input id="lname" type="text" class="form-control" name="lname" value="{{$user_detail[0]->lname}}" required="required" >



          </div>



          <div class="form-group">



            <label>Mobile Number</label>



            <input type="text" id="user_mob" name="user_mob" class="form-control" value="{{$user_detail[0]->user_mob}}" number="number"  required="required" <?php if(!empty($user_detail[0]->user_mob)){echo "readonly";}else{echo "";}?>>



          </div>



          <div class="form-group">



            <label>Email</label>



            <input id="email" type="email" class="form-control" name="email" value="{{$user_detail[0]->email}} " readonly="readonly">



          </div>



          <div class="form-group">



            <label>Your full address (Optional)</label>



           <input type="text" id="user_address" name="myaccount" class="form-control" value="{{$user_detail[0]->user_address}}" onFocus="geolocate()">



										   



	  <input type="hidden"  id="street_number" name="street_number"/>



	 <input type="hidden" class="field" id="route" name="route" /> 



	  <input type="hidden"  id="country" name="country" />



		 <input type="hidden" class="form-control" id="administrative_area_level_1" >	



          </div>



          <div class="row">



            <div class="col-md-6 col-sm-6">



              <div class="form-group">



                <label>City (Optional)</label>



                <input type="text" id="locality" name="user_city" class="form-control" value="{{$user_detail[0]->user_city}}" >



              </div>



            </div>



            <div class="col-md-6 col-sm-6">



              <div class="form-group">



                <label>Postcode (Optional)</label>



                <input type="text" id="postal_code" name="user_zipcode" class="form-control" value="{{$user_detail[0]->user_zipcode}}" number="number" maxlength="6">



              </div>



            </div>



          </div>





		   <div class="row">



		  <div class="form-group">



           <div class="col-md-12 ">



              <button type="submit" class="btn btn-submit margin_30" id="update_infobtn">



                <i class="fa fa-btn fa-user"></i> Update



              </button>



             </div>



          </div>



		  </div> 



		 



		 </div>



		  </form>



        </div>



            



        </div>



    </div>



</div><!-- End row -->



<!-- 	<hr class="more_margin"> -->



    



	<!-- End row -->



	<!-- End row -->



	<!-- End row -->



</section><!-- End container -->







<!-- End container-fluid  -->



<!-- End Content =============================================== -->



	







@stop







@section('js_bottom')



<!-- COMMON SCRIPTS -->



<script src="{{ asset('public/design/front/js/jquery-1.11.2.min.js') }}"></script>



<script src="{{ asset('public/design/front/js/common_scripts_min.js') }}"></script>



<script src="{{ asset('public/design/front/js/functions.js') }}"></script>



<script src="{{ asset('public/design/front/assets/validate.js') }}"></script>



 



<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->



<script>



	 $(document).on('click', '#update_infobtn', function(){







      jQuery.validator.addMethod("vname", function (value, element) {



        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



          return true;



        } else {



          return false;



        };



      });







      jQuery.validator.addMethod("lname", function (value, element) {



        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



          return true;



        } else {



          return false;



        };



      });







      jQuery.validator.addMethod("user_city", function (value, element) {



        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



          return true;



        } else {



          return false;



        };



      });



		 	



			var form = $("#update_frm");



				form.validate({



          rules: {



            name:{



              required:true,



              vname:true



            },



            lname: {



              required: true,



              lname:true



            }



          },



            messages: {



              name:{



                required:'Please enter name.',



                vname:"Please enter only letters."



              },



              lname: {



                required:'Please enter last name.',



                lname:'Please enter only letters.'



              }



            }



        });



			var valid =	form.valid();



		 });



</script>







<script>



      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.







      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">







      var placeSearch, autocomplete;



      var componentForm = {



        street_number: 'short_name',



        route: 'long_name',



        locality: 'long_name',



        administrative_area_level_1: 'short_name',



        country: 'long_name',



        postal_code: 'short_name'



      };







      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



            /** @type {!HTMLInputElement} */(document.getElementById('user_address11')),



            {types: ['geocode']});







        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);



      }







      function fillInAddress() {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();







        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }







        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



			



			//alert(val);



          }



        }



      }







      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() {



        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {



              lat: position.coords.latitude,



              lng: position.coords.longitude



            };



            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }



    </script>



  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>	



@stop	