@extends('layouts.app')

@section("other_css")
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
<!-- Radio and check inputs -->
<link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
<script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
<![endif]-->

@stop
@section('content')
{{-- <!-- <div id="position">
	<div class="container">
		<ul>
			<li><a href="{{ url('/') }}">Home</a></li>
			<li>Update Account</li>
		</ul>
	</div>
</div> --><!-- Position --> --}}
 
<!-- Content ================================================== -->
<section class="myAccountSec">

		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					@if(Session::has('success'))
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
					 
						{{Session::get('success')}}
					</div>
					@endif
					@if(Session::has('message_error'))					
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-check"></i>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						{{Session::get('message_error')}}
					</div>
					@endif
					<div>
						<form role="form" id="update_pwd" method="POST" action="{{ url('/password_action') }}">
							{{ csrf_field() }}
							<div class="box_style_2">
								<h2 class="inner">Update Password</h2>
								<div class="form-group up-pas">
									<label for="exampleInputEmail1">Old Password</label>
									<input type="password" class="form-control {{ $errors->has("old_password") ? "is-invalid" : "" }}" name="old_password" id="old_password" value="{{ old('old_password') }}" required="required">
									<a href="javascript:void(0)" class="show_pas" id="showHide_old"><i id="openeye_old_password"class="fa fa-eye" style="display: none;"></i><i class="fa fa-eye-slash" id="closeeye_old_password"></i></a>
									@if($errors->has("old_password"))
										<span class="text-danger">{{ $errors->first("old_password") }}</span>
									@endif
								</div>

								<div class="form-group up-pas">
									<label for="exampleInputEmail1">New Password</label>
									<input value="{{ old("password") }}" type="password" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}" name="password" id="new_password" required="required">
									<a href="javascript:void(0)" class="show_pas" id="showHide"><i id="openeye_new_password" class="fa fa-eye" style="display: none;"></i><i class="fa fa-eye-slash" id="closeeye_new_password"></i></a>
									@if($errors->has("password"))
										<span class="text-danger">{{ $errors->first("password") }}</span>
									@endif
								</div>

								<div class="form-group up-pas">
									<label for="exampleInputEmail1">Confirm New Password</label>
									<input type="password" class="form-control {{ $errors->has("password_confirmation") ? "is-invalid" : "" }}" name="password_confirmation" id="cnew_password" required="required">
									<a href="javascript:void(0)" class="show_pas" id="cshowHide"><i id="openeye_cnew_password" class="fa fa-eye" style="display: none;"></i><i class="fa fa-eye-slash" id="closeeye_cnew_password"></i></a>
									@if($errors->has("password_confirmation"))
										<span class="text-danger">{{ $errors->first("password_confirmation") }}</span>
									@endif
								</div>
								
								<div class="row">
									<div class="">
										<div class="col-md-12">
											<button type="submit" class="btn btn-submit  margin_30" id="update_oldpwd">   
												<i class="fa fa-btn fa-user"></i> Update Password
											</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div>
					</div>					
				</div>
			</div>

	</div><!-- End row -->
</section>

<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	

@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->
<script>					   
$(document).on('click', '#update_oldpwd', function(){ 
	
	jQuery.validator.addMethod("pass", function (value, element) {
        if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {
          return true;
        } else {
          return false;
        };
      });

	var form = $("#update_pwd");
		form.validate({
	        rules: {
	          password: {
	            required: true,
	            minlength:8,
	            pass:true,
	          },
	          cnew_password: {
	            required: true,
	            minlength:8,
	            equalTo : "#new_password",
	          }
	        },
	          messages: {
	            password: {
	              required:'Please enter password.',
	              minlength:'Password must be at least 8 characters.',
	              pass:"at least one number, one lowercase and one uppercase letter.",
	            },
	            cnew_password: {
	              required:'Please enter confirm password.',
	              minlength:'Password must be at least 8 characters.',
	              equalTo:'confirm password and password should be same, please enter correct.'
	            }
	          }
	      });
	var valid =	form.valid();
});
</script>	

<script type="text/javascript">
$(document).ready(function () {  
	$("#showHide_old").click(function () {
		if ($("#old_password").attr("type")=="password") {
			$("#old_password").attr("type", "text");
			$("#closeeye_old_password").hide();
			$("#openeye_old_password").show();
		}
		else{
			$("#old_password").attr("type", "password");
			$("#closeeye_old_password").show();
			$("#openeye_old_password").hide();
		}
	});

	$("#showHide").click(function () {
		if ($("#new_password").attr("type")=="password") {
			$("#new_password").attr("type", "text");
			$("#closeeye_new_password").hide();
			$("#openeye_new_password").show();
		}else{
			$("#new_password").attr("type", "password");
			$("#closeeye_new_password").show();
			$("#openeye_new_password").hide();
		} 
	});

	$("#cshowHide").click(function () {
		if ($("#cnew_password").attr("type")=="password") {
			$("#cnew_password").attr("type", "text");
			// closeeye_cnew_password
			$("#closeeye_cnew_password").hide();
			$("#openeye_cnew_password").show();
		}else{
			$("#cnew_password").attr("type", "password");
			$("#closeeye_cnew_password").show();
			$("#openeye_cnew_password").hide();
		}
	});
 });
</script>
		 
@stop	