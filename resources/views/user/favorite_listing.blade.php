@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')
    <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/') }}">Home</a></li>
                <li>Favorite Restaurent Listing </li>
            </ul>
        </div>
    </div><!-- Position -->
 
<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">


<div class="row">
            <div class="col-md-12">
                <div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <p class="top-ordr-btn">
							
                                <h2 class="user_title">@lang('translation.favoriteRestaurant')</h2>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
<div class="container">
    <div class="row">
        <div class="strip_list favorite_restaurant_list_sec fav_lst_s wow fadeIn" data-wow-delay="0.3s" id="listing_restaurant_div" style="display:block">	                	
	                    <div class="row">
                        	<div class="col-md-12">
                            	<div class="table-responsive resto-table">
                                <table id="listing_restaurant">
								@if(!empty($restaurant_list))
				@foreach($restaurant_list as $special_list)
				<?php 
				 $user_id = Auth::user()->id ;
				?>
								<tr>
                                    	<td width="110px">
                                        <div class="ribbon_1">
                                           @if($special_list->rest_classi!='5')
													@if($special_list->rest_classi=='1') Sponsored @endif
													@if($special_list->rest_classi=='2') Popular @endif
													@if($special_list->rest_classi=='3') Special @endif
													@if($special_list->rest_classi=='4') New @endif
													@if($special_list->rest_classi=='5') Standard @endif
										@endif
                                        </div>
                                        <div class="thumb_strip">
										<?php if($special_list->google_type==1)
										{
										?>										
										 <a href="#">
											@if(!empty($special_list->rest_logo))
											<img src="{{$special_list->rest_logo}}" alt="">									
											@else
											<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
											@endif						
											</a>
										 <?php	
										}
										else
										{
										?>
										 <a href="{{url('/'.strtolower(trim(str_replace(' ','_',$special_list->rest_suburb))).'/'.trim($special_list->rest_metatag))}}">
											@if(!empty($special_list->rest_logo))
											<img src="{{ url('/') }}/uploads/reataurant/{{$special_list->rest_logo}}" alt="">									
											@else
											<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
											@endif						
											</a>
										<?php }?>	
	                                	</div>	
                                        </td>
                                        <td>
                                          <div class="desc">                               
                                              <h3>{{$special_list->rest_name}}</h3>	                                             <div class="location">
                                                   {{$special_list->rest_address}} {{$special_list->rest_suburb}} {{$special_list->rest_state}} {{$special_list->rest_zip_code}}
                                              </div>
                                          </div>
                                        </td>
                                        <td> 
										<?php $avg_rating =  $special_list->totalrating;  ?>
                                        <div class="rating">										   
									  @for ($x = 1; $x < 6; $x++)
										@if(($avg_rating>0) && ($avg_rating>=$x))
										<i class="icon_star voted"></i>
										@else
											<i class="icon_star"></i>
										@endif
									  @endfor 
									  <span>(<small>
										<a href="{{url('/'.strtolower(str_replace(' ','_',trim($special_list->rest_suburb))).'/'.$special_list->rest_metatag.'/review')}}">{{$special_list->totalreview}} @lang('translation.reviews')</a></span>
	                                </div>
                                    <div class="dlvry-amt"><h4><?php for($p=1;$p<=5;$p++){
										$class='';
										if($p<=$special_list->rest_price_level)
										{
											$class='class="spn-actv"';
										}
									?>
									<span <?php echo $class;?>>$</span>
									<?php }?></h4></div>
									<div class="min-dlvry-amt"><span class="value h5">${{$special_list->rest_mindelivery}} </span><span class="type caption u-text-secondary">@lang('translation.minimum')</span></div>
                                    <div class="min-dlvry-amt"><span class="value h5">@lang('translation.free')</span><span class="type caption u-text-secondary">@lang('translation.delivery')</span></div>
                                        </td>
                                        <td><div class="go_to">
	                                <div>									
										<a href="{{url('/'.strtolower(trim(str_replace(' ','_',$special_list->rest_suburb))).'/'.$special_list->rest_metatag)}}" class="btn_1">@lang('translation.btnViewMenu')</a>
										 <a class="item_like item-like-selected" href="javascript:void(0)" data-userid="<?php echo $user_id;?>" data-restid="<?php echo $special_list->rest_id;?>" id="rest_fav_id_<?php echo $special_list->rest_id;?>" data-googletype="<?php echo $special_list->google_type;?>"><i class="fa fa-heart"></i></a>
										
	                                </div>
	                            </div></td>
                                    </tr>
			@endforeach <span class="fev_sec_main">@else @lang('translation.emptyFavoriteRestaurant')            </span> @endif
            </table></div>
                            </div>
	                        
                            
	                        
	                    </div>
	                </div>
    </div>
</div>

</div><!-- End row -->
	<hr class="more_margin">
    
	<!-- End row -->
	<!-- End row -->
	<!-- End row -->
</div><!-- End container -->

<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	

@stop

@section('js_bottom')


<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
 
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>-->
<script>

 $(document).on('click', '.item_like', function(){
 
 	
		var userid = $(this).attr('data-userid');
		var restid = $(this).attr('data-restid');
		var googletype = $(this).attr('data-googletype');
		
	if(googletype==1)	
	{
		$("#google_fav").modal('show');		
	}
	else
	{
		
		if(userid==0)
		{ 
			$('#alert_info_fav').show();
			$('#login_2').modal('show');
				
				$("#alert_info_fav").fadeOut(10000); 
		}
		else
		{
			//alert(userid);
			
			$("#ajax_favorite_loddder").show();	
			
			var frm_val = 'userid=' + userid+'&restid=' +restid;
			
			
				$.ajax({
				//type: "post",
				dataType: "json",
				url:  '?userid=' + userid+'&restid=' +restid,				
				url: "{{url('/favourite')}}",
				data: frm_val,
				success: function(msg) {					
					
					
					 location.reload();
						$("#ajax_favorite_loddder").hide();	
						
					}
				});
		}
	}
		
 
	 //$(this).toggleClass('item-like-selected')
  });


</script>

@stop	