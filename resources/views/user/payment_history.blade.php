@extends('layouts.app')

@section("other_css")
   	  <!-- Radio and check inputs -->
   	<link href="{{ url('/') }}/design/css/style.css" rel="stylesheet">  
    <link href="{{ url('/') }}/design/css/skins/square/grey.css" rel="stylesheet">
    <link href="{{ url('/') }}/design/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="{{ url('/') }}/design/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->
<meta name="_token" content="{!! csrf_token() !!}"/>

     <div id="position">
        <div class="container">
            <ul>
                <li><a href="{{ url('/')}}">Home</a></li>
                <li>View all Post Order</li>
            </ul>
        </div>
    </div>
	<!-- Position -->
<!-- End Map -->
    <!-- Content ================================================== -->
    <div class="container margin_60_35">

        <div class="row">
            <div class="col-md-12">
                <!--<div id="tools">-->
                <div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <p class="top-ordr-btn">
                                <h2 class="user_title">@lang('translation.paymentHistory')</h2>
                               
                            </p>
                        </div>

                    </div>
                </div>
                <!--End tools -->
            </div>
        </div>

        <div class="row">

            <!--End col-md -->

            <div class="col-md-12">
			
			
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					
				<div id="listing_restaurant" class="no_order">

        <div class="strip_list wow fadeIn" data-wow-delay="0.1s" style="display:block">  

				<?php //print_r($order_detail); ?>

                        <div class="box">
                        <div class="box-body table-responsive">
                          <div style="float:right; margin:5px;"> 
                  <!--<input class="btn btn-primary" type="button" id="generate_file"  value="Export">-->
                            </div>
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>@lang('translation.srNo')</th>
                                  <th>@lang('translation.orderId')</th>
                                  <th>@lang('translation.mode')</th>
                                  <th>@lang('translation.restName')</th>
                                  <th>@lang('translation.transactionNo')</th>
                                  <th>@lang('translation.transactionAmount')</th>
                                  <th>@lang('translation.status')</th>
                                  <th>@lang('translation.orderTime')</th>
                                  <th>@lang('translation.paymentTime')</th>

                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($order_detail)) 
                               @foreach ($order_detail as $list)
                               <?php 

                              $ordertbl = DB::table('order')
                               ->where('order_id', '=', trim($list->pay_orderid))
                               ->get();

                               $rest_name = DB::table('restaurant')
                               ->where('rest_id', '=', trim($ordertbl[0]->rest_id))
                               ->value('rest_name'); 


                               ?>
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ $ordertbl[0]->order_uniqueid}}</td>
                                  <td>{{ $list->pay_method}}</td>
                                  <td>{{ $rest_name }}</td>
                                  <td>{{ $list->pay_tx}}</td>
                                  <td>{{ $list->pay_amt}} {{ $list->pay_cc}}</td>
                                  <td>  

                   @if($ordertbl[0]->order_status=='11') Payment Done @else                 
                   @if($list->pay_status=='1') Payment Done @endif
                   @if($list->pay_status=='2') Partial Done @endif
                   @if($list->pay_status=='3') Payment Not Done @endif
                   @if($list->pay_status=='4') Payment Refund @endif

                   @endif
                   
                                  </td>
                                  <td>{{ $ordertbl[0]->order_create }}</td>
                                  <td>{{ $list->pay_update }}</td>

                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>

</div><!-- End col-md-9-->

</div><!-- End row -->
</div><!-- End container -->
    <!-- End Content =============================================== -->
@stop
@section('js_bottom')

<div class="modal fade for-othr-itm" id="review_modal" role="dialog">
  <div class="modal-dialog"> 
  
  <form role="form" id="msform" method="POST" action="{{ url('/order_review_action') }}">				
                {{ csrf_field() }}
				<input type="hidden" name="order_id" id="review_orderid" value="" />
				<input type="hidden" name="user_id" id="userid" value="{{Auth::user()->id}}" />
				<input type="hidden" name="rest_id" id="review_restid" value="" />
				<input type="hidden" name="ratting"  id="ratting" value="" />
  <!-- progressbar -->
  <ul id="progressbar">
    <li class="active">Driver</li>
    <li>Restaurant</li>
    <li>Food</li>
  </ul>
  <!-- fieldsets -->
   
  <fieldset>
  <h2 class="modal-title choice fs-title" id="show_restaurants_title">Review And Rating </h2>
  
   <!-- <h3 class="fs-subtitle">This is step 1</h3>-->
    		<div class="food-main-avrg text-center">How was <span id="dboyname">devid</span>'s delivery?</div>

			<div class="deliverymex text-center">Your feedback helps improve the delivery service.</div>
            
            <section class='rating-widget'>
				  <!-- Rating Stars Box -->
				  <div class='rating-stars text-center'>
				    <ul id='sdelivery'>
				      <li class='star' title='Poor' data-value='1'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Fair' data-value='2'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Good' data-value='3'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Excellent' data-value='4'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='WOW!!!' data-value='5'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				    </ul>
				  </div>
				 
				 <input type="hidden" name="re_delivery_ontime" id="re_delivery_ontime" value="0"> 
				 </section>
            
<!--    <input type="text" name="email" placeholder="Email" />
    <input type="password" name="pass" placeholder="Password" />
    <input type="password" name="cpass" placeholder="Confirm Password" />--> 
    
    <input type="hidden" name="re_delivery_ontime" id="re_delivery_ontime" value="0"> 
   <input type="button" name="next" class="next action-button" value="Next" />
  </fieldset>
  
  <fieldset>
  
    <div class="food-main-avrg text-center"> 
    <h2 class="fs-title">How was <span id="drestname"></span>?</h2></div>	
    <h3 class="fs-subtitle">We'll share your rating with the restaurant.</h3>
    
    <section class='rating-widget'>
				 
				  <div class='rating-stars text-center'>
				    <ul id='srestaurant'>
				      <li class='star' title='Poor' data-value='1'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Fair' data-value='2'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Good' data-value='3'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Excellent' data-value='4'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='WOW!!!' data-value='5'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				    </ul>
				  </div>
				 
				 <input type="hidden" name="range" id="range" value="0"> 
				 </section>
	<input type="button" name="previous" class="previous action-button" value="Previous" />			 
    <input type="button" name="next" class="next action-button" value="Next" />
  </fieldset>
  
  <fieldset>
  
  	
    <h2 class="fs-title">How did you like the food?</h2>
    <h2 class="fs-title">Please give your rate these items</h2>
    <h3 class="fs-subtitle" id="dfoodnmitem">Dal Bafla Thali</h3>
    
      <section class='rating-widget'>
				
				  <div class='rating-stars text-center'>
				    <ul id='sfood'>
				      <li class='star' title='Poor' data-value='1'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Fair' data-value='2'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Good' data-value='3'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='Excellent' data-value='4'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				      <li class='star' title='WOW!!!' data-value='5'>
				        <i class='fa fa-star fa-fw'></i>
				      </li>
				    </ul>
				  </div>
				 
				 <input type="hidden" name="re_food_good" id="re_food_good" value="0"> 
				 </section>
                 
       <input type="hidden" name="re_order_accurate" id="re_order_accurate" value="1"> 
       
       <div class="exprnc dexprnc">
					
					<div class="form-group">
					  <label for="comment">Leave feedback about this item</label>
					  <textarea class="form-control" rows="5" id="comment" name="review_text" placeholder="" ></textarea>
					</div>
					</div>         
    
    <input type="button" name="previous" class="previous action-button" value="Previous" />
   <!-- <input type="submit" name="submit" class="submit action-button" value="Submit" />-->
    
   <!-- <button type="submit" class="submit action-button" id="update_infobtn">Add Review</button>	-->

    <button type="submit" class="btn btn-primary btn_1" id="update_infobtn">Add Review</button>	
    
  </fieldset>
</form>
    
    </div>
  </div>
</div>


<div class="modal fade for-othr-itm" id="other-items" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title choice" id="show_popup_title_addon">View Order Details </h4>
      </div>
      <div class="modal-body popup-ctn">
        <div class="other-item-popup-ctn rest_itm_pop" id="show_mod_addon">
		
        </div>
      </div>
    </div>
  </div>
</div>


<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>


<!-- COMMON SCRIPTS -->
 <script type="text/javascript" src="{{ url('/') }}/design/front/js/jquery-1.9.1.min.js"></script> 
    <script src="{{ url('/') }}/design/front/js/bootstrap.js"></script> 
    <script src="{{ url('/') }}/design/front/js/owl.carousel.js"></script> 
    <script src="{{ url('/') }}/design/front/js/wow.min.js"></script> 
<script>


$(document).on('click', '#show_review', function(){ 

var review_restid = $(this).attr('data-rest_id');
var review_orderid = $(this).attr('data-order_id');
var review_rest_name = $(this).attr('data-rest_name');
var review_rest_delivery = $(this).attr('data-rest_delivery');

var fooditemname = $(this).attr('data-rest_food');

$("#review_restid").val(review_restid);		
$("#review_orderid").val(review_orderid);
$("#show_restaurants_title").html(review_rest_name);
$("#dboyname").html(review_rest_delivery);	
$("#drestname").html(review_rest_name);	
$("#dfoodnmitem").html(fooditemname);	

	$('#review_modal').modal('show'); 

});
 $(document).on('click', '#msform', function(){ 
		 	
			
			//alert($('.range-slider__value').text());
			
			var form = $("#update_frm");
				form.validate();
			var valid =	form.valid();
		 });

		  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

$(document).on('click', '#show_modal', function(){ 


var rest_id = $(this).attr('data-rest_id');
var order_id = $(this).attr('data-order_id');
 //$('#other-items').modal('show'); 


$("#ajax_favorite_loddder").show();
	
frm_val = 'rest_id='+rest_id+'&order_id='+order_id;
		
		$.ajax({
				type: "POST",
				url: "{{url('/show_order_detail')}}",
				data: frm_val,
			   dataType: 'json',
					success: function(msg) {
						//alert(msg.addon_view)	
						   
						$("#ajax_favorite_loddder").hide();						 
						$('#show_mod_addon').html(msg.cart_view); 
						$('#other-items').modal('show'); 
					}
				});
		
				
});


$(document).ready(function(){

 /* Restuarant star start */	
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#srestaurant li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#srestaurant li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#srestaurant li.selected').last().data('value'), 10);

    $('#range').val(ratingValue);
    
  });

   /* Food star start */	
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#sfood li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#sfood li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#sfood li.selected').last().data('value'), 10);

    $('#re_food_good').val(ratingValue);
    
  });

   /* Delivery star start */	
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#sdelivery li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#sdelivery li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#sdelivery li.selected').last().data('value'), 10);

    $('#re_delivery_ontime').val(ratingValue);
    
  });

  /* delivery star end */
  
  
});



</script>
 <script src="{{ url('/') }}/design/front/js/jquery.easing.min.js"></script>
<script>
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})
</script>



@stop