<?php $cart = json_decode($detail_data[0]->order_carditem); ?>

<table cellpadding="5" class="nomargin table-striped frm-rcpt-table">
				<tbody>
				
				<!-- @if((empty($detail_data[0]->re_id)) && ($detail_data[0]->order_status=='11'))
				<tr>
					<td colspan="2">			
										
										<a href="{{url('/'.strtolower(trim( str_replace(' ','_',$detail_data[0]->rest_suburb))).'/'.$detail_data[0]->rest_metatag.'/review?refno='.$detail_data[0]->order_id)}}" class="btn_1">Add Review</a>									
						</td>
				</tr>
					@endif -->					
				<tr>
					<td colspan="2" class="other-rspts">
						<p>
						@if(!empty($detail_data[0]->rest_logo))
									<img src="{{ url('/') }}/uploads/reataurant/{{$detail_data[0]->rest_logo}}" alt="">									
									@else
									<img src="{{ url('/') }}/design/front/img/logo.png" alt="">									
									@endif
						</p>
						<p>
							<span>{{$detail_data[0]->rest_name}}</span>
							<span>{{$detail_data[0]->rest_address}}</span>							
						</p>
					</td>
				</tr>	
				
				<tr>
					<td colspan="2">
						@lang('translation.orderId'): {{$detail_data[0]->order_id}} ({{$detail_data[0]->order_uniqueid}})
					</td>
				</tr>
				<tr>
					<td colspan="2">
						@lang('translation.orderType'):{{$detail_data[0]->order_type}}
					</td>
				</tr>
				<tr>
					<td colspan="2">
						@lang('translation.orderDate'):{{date('d-m-Y',strtotime($detail_data[0]->order_pickdate))}}
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						@lang('translation.orderInformation') :
					</td>
				</tr>

					<?php $cart_price = 0;?>
		@if(count($cart))
				
				 @foreach($cart as $item)
				<?php  $cart_price = $cart_price+$item->price;?>
				<tr>
					<td>
						<strong>{{$item->qty}}x</strong> {{$item->name}} 
					</td>
					<td>
						<strong class="pull-right">${{number_format($item->price,2)}}</strong>
					</td>
				</tr>				
				 @if(!empty($item->options->addon_data)&&(count($item->options->addon_data)>0))
				 	@foreach($item->options->addon_data as $addon)
						
					  <?php $cart_price = $cart_price+$addon->price;?>
					  
				<tr>
					<td>
						<?php 
							echo $addon->name.',';
							/*$exp_addon_name = explode(":",$addon->name);
							if(isset($exp_addon_name[1])){
								echo $exp_addon_name[1].',';
							}*/
						?>
					</td>
					<td>
						<strong class="pull-right">@if($addon->price>0)${{number_format($addon->price,2)}}@endif</strong>
					</td>
				</tr>
				
					 
					 @endforeach
				 @endif 
					
				 @endforeach
@endif


				<tr>
                  <td> @lang('translation.subtotalTax') </td>
				  <td> <span class="pull-right">${{number_format($detail_data[0]->order_subtotal_amt,2)}}</span> </td>
                </tr>
				
				
				@if($detail_data[0]->order_promo_cal>0)
			
                <tr>
                  <td>@lang('translation.discount')</td>
				  <td> <span class="pull-right"> -${{number_format($detail_data[0]->order_promo_cal,2)}}</span> </td>
                </tr>
				
				@endif
				
				
		
				@if($detail_data[0]->order_service_tax>0)
			
                <tr>
                  <td> @lang('translation.salesTax') </td>
				  <td> <span class="pull-right">${{number_format($detail_data[0]->order_service_tax,2)}}</span> </td>
                </tr>
				
				@endif

				@if($detail_data[0]->order_remaning_delivery>0)
				
					
				<tr>
                  <td>   ${{number_format($detail_data[0]->order_min_delivery,2)}} min 
				  </td>
					<td>
						@lang('translation.remaining')<span class="pull-right">${{number_format($detail_data[0]->	order_remaning_delivery,2)}}</span>
					</td>
                </tr>
				@endif

				
				<tr>
					<td>
						 <strong>@lang('translation.total')</strong>
					</td>
					<td>
					
					<?php
					$promo_amt_cal= '0.00';
					
					
					
					if($detail_data[0]->order_promo_cal>0)
					{
						 $promo_amt_cal= $detail_data[0]->order_promo_cal;
					}
					
					
					$total_amt= $cart_price+$detail_data[0]->order_deliveryfee-$promo_amt_cal+$detail_data[0]->order_service_tax;
					
					
						
					if(($detail_data[0]->order_remaning_delivery>0) && 
						($detail_data[0]->order_min_delivery>0) && 
						(($cart_price-$promo_amt_cal)<$detail_data[0]->order_min_delivery)
					  )
					{
						//$total_amt= ($detail_data[0]->order_min_delivery+$delivery_fee);
					}
					elseif(($detail_data[0]->order_remaning_delivery>0) && 
						($detail_data[0]->order_min_delivery>0) && 
						(($cart_price-$promo_amt_cal)>$detail_data[0]->order_min_delivery)
					  )
					{
						//$total_amt= $cart_price+$delivery_fee-$promo_amt_cal;
					}
				?>
				
						<span class="pull-right"><strong>${{number_format($detail_data[0]->order_total,2)}}</strong></span>
					</td>
				</tr>

				<?php 
				$coupon_code = $detail_data[0]->coupon_code; 

				if($coupon_code){

				$camount  =  DB::table('coupon_code')		
								->where('coupon', '=' ,$coupon_code)
								->value('amount');	
				}else{ $camount = 0;}
				?>

				<tr>
					<td>@lang('translation.instantDiscount')</td>
					<td><span class="pull-right">${{number_format($camount,2)}}</span></td>
				</tr>
				<tr>
					<td>
						 <strong>@lang('translation.grandTotal')</strong>
					</td>
					<td>
			<span class="pull-right"><strong>${{number_format(($detail_data[0]->order_total - $camount),2) }}</strong></span>
					</td>
				</tr>
				
				
				<tr>
					<td colspan="2" class="other-rspts">
						@lang('translation.paymentType') :<span class="pull-right"><?php if($detail_data[0]->order_pmt_type=='2'){ echo 'Partial Payment';}
									else{echo 'Full Payment';}?></span>
					</td>
				</tr>
				
				<?php if($detail_data[0]->order_pmt_type=='2'){?>
				<tr>
					<td colspan="2" class="other-rspts">
						@lang('translation.percentage') : <span class="pull-right">{{$detail_data[0]->order_partial_percent}} </span>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="other-rspts">
						@lang('translation.paidAmount') : {{$detail_data[0]->order_partial_payment}} 
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="other-rspts">
						@lang('translation.remaingAmount') : <span class="pull-right">{{$detail_data[0]->order_partial_remain}} </span>
					</td>
				</tr>
				<?php 
					}
				?>
				
				
				<tr>
					<td colspan="2" class="other-rspts">
						@lang('translation.paymentMethods') : <span class="pull-right">{{$detail_data[0]->order_pmt_method}}</span>
					</td>
				</tr>
				
				
				</tbody>
				</table>