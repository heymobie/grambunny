@extends('layouts.grambunny')

@section("styles")



<style type="text/css">

  .parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

}

section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

}

#sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

}

#sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

}

#short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

}

div#subheader {

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

    height: 380px;

}

ul.brad-home {

    padding: 0;

    margin: 0;

    text-align: center;

}

.brad-home li {

    display: inline-block;

    list-style: none;

    padding: 5px 10px;

    font-size: 12px;

}

#subheader a {

    color: #fff;

}

.sf-featured-provider {
    text-transform: capitalize;
}

.resto-near a.show-map-mode {
    float: left;
    padding-right: 15px;
    border: none;
    background: #444648;
    padding: 5px 8px;
    transition: all .3s;
    font-weight: 600;
    color: #fff;
    display: none;
    border-radius: 4px;
    margin-left: 15px;
}

a.show-list-mode {
    float: left;
    padding-right: 15px;
    border: none;
    background: #444648;
    padding: 5px 8px;
    transition: all .3s;
    font-weight: 600;
    color: #fff;
    border-radius: 4px;
    margin-left: 15px;
}

a#dlistview:hover {
    color: #ffffff !important;
    text-decoration: none;
}

</style>

 <style>

    #map {

        height: 740px; /* The height is 400 pixels */

        width: 100%; /* The width is the width of the web page */

    }

   .btn-group.sf-provider-tooltip a:hover {
    color: #fff !important;
    text-decoration: none;
} 

</style>

@endsection

 

@section('content')



<!-- SubHeader =============================================== -->

<!-- <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/public/design/front/img/sub_header_short.jpg" data-natural-width="1350" data-natural-height="335">

	<div id="subheader">

		<div id="sub_content">			

			<h6>

			<input type="submit" id="btnChangeLocation" name="btnChangeLocation" class="btn_1" value="Change Location" >

			</h6>

			<div id="position">

				<div class="container">

					<ul class="brad-home">

						<li><a href="{{url('/home')}}">Home</a></li>

						<li>View all Merchants</li>

					</ul>

				</div>

			</div>

		</div>

	</div>

</section> -->



<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">

    <div id="subheader">

        <div id="sub_content" class="animated zoomIn">

          	<h1><span class="restaunt_countrt">Near by Products</span></h1>

	        <div id="position">

		        <div class="container">

		            <ul class="brad-home">

		                <li><a href="https://www.grambunny.com">Home</a></li>

		                <li>Search listing </li>

		            </ul>

		        </div>

		    </div>

        </div>

    </div>

</section>



<!-- End SubHeader ============================================ -->


<div class="collapse" id="collapseMap">

	<div id="map" class="map"></div>

	</div><!-- End Map -->

	<!-- Content ================================================== -->

	<section style="background: #f5f5f5;">

		<div class="container-fluid margin_20_35">

			<div class="row">



				{{-- filter --}}

			<app-products-filter selected-category="{{ request()->category }}" query-address="{{ request()->address }}" search="{{ request()->search }}" min-price="{{ request()->min }}" max-price="{{ request()->max }}"></app-products-filter>

				<div class="col-md-9 col-sm-8 col-xs-12">

						

					<div class="row">

						<div class="col-md-12 col-sm-12">

							<div class="selected-filter">

								
	

							</div>

							

							<div class="searchSort">

								

								<input type="hidden" id="ghs-select-sort" value="0" name="sort_by">

								

							</div>

						</div>

						<!-- <div class="col-md-12 col-sm-12">

							<div class="resto-near">

								

								<a href="#" class="show-map-mode">Show Map</a>

								<a href="#" class="show-list-mode">Show List</a>

								<p class="rst-lst-top">

									<span class="restaunt_countrt"> </span> <span>&nbsp;Merchants</span>

								</p>

							</div>

							

						</div> -->

					</div>

					

					<div class="strip_list wow fadeIn" data-wow-delay="0.3s" id="listing_restaurant_div">

						

						<div class="row">

							<div class="col-md-12">

								<div class="searchResults">

									 <div class="row">


                                     	<div class="resto-near">
	                            
									    <a class="show-map-mode" id="dmapview" style="display: none; cursor:pointer">Map View</a>
									    <a class="show-list-mode" id="dlistview" style="display: block; cursor:pointer">List View</a>

									    </div>

								 <!--{{$verdorslist}}-->	    
   
                                <?php foreach ($verdorslist as $key => $value) { ?>
                                

										<div class="col-md-3 col-sm-6 equal-col dltv" id="listview" style="display:none">

											<div class="sf-search-result-girds" id="proid-30">
	
												<div class="sf-featured-top">

													<div class="sf-featured-media" style="background-image:url(https://www.grambunny.com/public/uploads/vendor/profile/{{$value->profile_img1}})"></div>

													<div class="sf-overlay-box"></div>


													<div class="sf-featured-info">

														<div class="sf-featured-provider">{{$value->business_name}}</div>

														<div class="sf-featured-address">{{$value->description}}</div>
														
														<div class="sf-featured-address"><i class="fa fa-map-marker"></i>{{$value->city}}, {{$value->state}}, {{$value->zipcode}}</div>

                        <?php $ratings = $value->avg_rating; ?>

                        <div class="star-rating">
 
                            <?php if($ratings>0){ 
                            
                            for ($i=0; $i < $ratings ; $i++) { ?>

                                <span class="fa fa-star" data-rating="<?php echo $i;?>"></span>

                           <?php }

                             }else{?>

                            <span class="fa fa-star" style="color: gray;" data-rating="1"></span>

                            <span class="fa fa-star" style="color: gray;" data-rating="2"></span>

                            <span class="fa fa-star" style="color: gray;" data-rating="3"></span>

                            <span class="fa fa-star" style="color: gray;" data-rating="4"></span>

                            <span class="fa fa-star" style="color: gray;" data-rating="5"></span>

                            <?php } ?>

                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">

                        </div>

													</div>

													

												</div>

												<div class="sf-featured-bot">

													<div class="sf-featured-text"></div>

													<div class="btn-group sf-provider-tooltip" >

												<a href="https://www.grambunny.com/{{$value->username}}/services" class="text-dark">View Store</a>

													</div>

												</div>

											</div>

										</div>

                                     <?php } ?>



								</div> 
								 
								<app-products auth="{{ auth()->guard()->check() }}" lat="{{ $lat }}" lng="{{ $lng }}" category="{{ $category }}"></app-products>

									 
								</div>

							</div>


						</div>

					</div>


					<div class="map-div wow fadeIn" data-wow-delay="0.3s" id="map-restaurant"> </div>

					<div id="ajax_page_load"> </div>				

				</div>


			</div>

		</div>

	</section>

	@endsection

	@section("scripts")

	@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>	

<script type="text/javascript">

$(document).ready(function(){

  $("#dmapview").click(function(){

    $("#dlistview").show();
    $("#dmapview").hide();
    $(".dltv").hide();
    $("#mapss").show();


  });

    $("#dlistview").click(function(){

    $("#dmapview").show();
    $("#dlistview").hide();
    $(".dltv").show();
    $("#mapss").hide();


  });


});


 </script>