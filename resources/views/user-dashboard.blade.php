@extends("layouts.grambunny")

@section("styles")

{{-- on page style --}}



<style type="text/css">

.verify-info .form-group input.form-control{  
background-color: #f1f1f1;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-size: 15px;

} 
.prsn-info.my-profl p label {
    margin-bottom: 0;
    font-size: 14px;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-size: 15px;
}
.prsn-info.my-profl p strong {
    float: right;
    font-size: 14px;
    font-family: 'PT Sans Narrow';
    letter-spacing: 1px;
    font-size: 15px;
}
.verify-info.my-prof-verify p {

    background-color: #ffffff;

    padding-left: 0px;

  }



 .verify-info .form-group select.form-control {

 background-color: #f1f1f1;
    border-radius: 25px;
    font-family: 'PT Sans Narrow';
    font-size: 15px;
    letter-spacing: 1px;

} 
.modal-dialog11 .col-md-9 {
    max-width: 100% !important;
    
}
.awesome-cropper img{
    width: 150px;
/*    height: 150px;*/
    margin-top: 5px;
}
.modal-dialog11 img{
width: 100% !important;
}
.awesome-cropper{ width: 100px; }

.modal-content canvas{
  width: 100%;
}

.modal-dialog11{
width: 100% !important;
margin: auto !important;
    }

</style>
<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />



@endsection

@section("content")

{{--  Content goes here --}}


<section class="setting-section"> 

      <div class="container">

        <div class="row wow fadeInDown">


        <div class="col-lg-12">

            <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">

              <!-- <span>Grab it features //</span> -->

              <h2>My Profile</h2>

              <!--<p>It is a long established fact that a reader will be distracted</p>-->

            </div>

        </div>


          <div class="col-lg-12" role="tabpanel">

            <div class="row manag-tabs">

              <div class="col-sm-3">

              	<div id="sticky-anchor"></div>

                <ul class="nav nav-pills brand-pills nav-stacked" role="tablist" id="sticky">

                    <li class="brand-nav"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" class="@if(!Session::has('upassword')) active show @endif">User Profile </a></li>                    

                    <!--<li class="brand-nav"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Basic Info</a></li>-->

                    <!--<li class="brand-nav"><a href="#tab5" aria-controls="tab5" role="tab" data-toggle="tab">Payment</a></li>

                    <li class="brand-nav"><a href="#tab6" aria-controls="tab6" role="tab" data-toggle="tab">Payout method</a></li>-->



                    <li class="brand-nav"><a href="#tab7" aria-controls="tab7" role="tab" data-toggle="tab" class="@if(Session::has('upassword')) active show @endif">Change Password</a></li>


                    <li><a href="#"></a></li>

                </ul>

              </div>

              <div class="col-sm-9">

                  <div class="tab-content">


                  @if(Session::has('upmessage'))

             

                    <div class="alert alert-success alert-dismissable">

                    <i class="fa fa-check"></i>

                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                    {{Session::get('upmessage')}}

                    </div>



                    @endif



                    @if(Session::has('success'))

                    <div class="alert alert-success alert-dismissable">

                    <i class="fa fa-check"></i>{{Session::get('success')}}</div>

                    @endif



                    @if(Session::has('message_error'))          

                    <div class="alert alert-danger alert-dismissable">

                    <i class="fa fa-check"></i>

                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                    {{Session::get('message_error')}}

                    </div>

                    @endif



                      <div role="tabpanel" class="tab-pane @if(!Session::has('upassword')) active @endif" id="tab1">

                        <div class="profl-setng">

                          <div class="row">



                            <div class="col-lg-12">

                              <div class="profile-basic background-white p20">

                                <h3 class="page-title">

                                    User Information

                                </h3>

                                <form>



                                <div class="prof-img">

                                  

                                  <div class="user-photo">

                                      <a>

                                          <img src="{{ url('/public/uploads/user/'.$user_detail[0]->profile_image) }}" alt="User Photo">

                                      </a>

                                  </div>



                                </div>

                                <div class="prsn-info my-profl">

                                  <div class="row">

                                    <div class="form-group col-lg-12">

                                        <p><label>User Name </label> <strong style="text-transform: capitalize;">{{$user_detail[0]->name}} {{$user_detail[0]->lname}}</strong></p>

                                    </div><!-- /.form-group -->



                                    <div class="form-group col-lg-12">

                                        <p><label>Average Rating From Merchant </label> 

			                            <div class="tvlr-rating">



			                            <p>


                               <?php if(@$user_detail[0]->avg_rating == 0){ ?> 
 
                              <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                              <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                              <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                              <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                              <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                            

                               <?php }else{ ?>

                               <?php for ($i=1; $i < 6 ; $i++) { ?>


                              <?php if(@$user_detail[0]->avg_rating >= $i){ ?>

                             	<span><i class="text-warning fa fa-star"></i></span>

                              <?php }else{ ?>

                             <span><i class="text-warning fa fa-star-o"></i></span>             

			                       <?php } } } ?>


			                               </p>

			                               <strong></strong>

			                            </div>

                        				</p>

                                    </div>



                                  </div><!-- /.row -->

                                </div>



                                </form>

                              </div>

                            </div>

                              

                          </div>

                        </div>

<!--       <div class="profl-setng">
      <div class="row">
      <div class="col-lg-12">
      <div class="profile-basic background-white p20">
      <h3 class="page-title">Review & Rating</h3>
      <form class="ureviewrat">

       <div class="prsn-info my-profl">

       <?php foreach ($userRating as $key => $value) { 
       $mname = DB::table('vendor')->where('vendor_id', '=', $value->merchant_id)->value('name');
       $mlname = DB::table('vendor')->where('vendor_id', '=', $value->merchant_id)->value('last_name'); 
      ?>
        
       <div class="row">

        <div class="col-lg-8">

         <div class="form-group"> 
        
        <span style="text-transform: capitalize;">{{ $mname }} {{ $mlname }}  :  </span>{{ $value->review }}

        </div> 
      </div>


       <div class="col-lg-4">
        <div class="form-group">
      
       <?php if($value->rating == 0){ ?> 
 
        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>
                            
        <?php }else{ ?>
        <?php for ($i=1; $i < 6 ; $i++) { ?>
        <?php if($value->rating >= $i){ ?>
        <span><i class="text-warning fa fa-star"></i></span>
        <?php }else{ ?>
        <span><i class="text-warning fa fa-star-o"></i></span>             
       <?php } } } ?>
      
       </div>
       </div>
        </div>

        <?php } ?>

      </div>

    </form>
    
        <div class="col-12 mt-5 text-center">

                <div class="custom-pagination">

                  {{ $userRating->links() }}

                </div>

              </div>
      
     </div>
   </div>     
 </div>
</div>    -->

           
                  <div class="profile-basic background-white p20">
                  <h3 class="page-title">Basic Information</h3> 
                  <form role="form" id="update_frm" method="POST" action="{{ url('/update_account_action') }}" enctype="multipart/form-data">

                        {{ csrf_field() }}

                       <input type="hidden" name="user_id" value="{{$user_detail[0]->id}}" />  

                            <div class="verify-info my-prof-verify">

                              <div class="row">

                                <div class="form-group col-lg-6">

                                    <label>First Name</label>

                                    <p><input id="name" type="text" class="form-control" name="name" value="{{$user_detail[0]->name}}" required="required"> </p>

                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-6">

                                    <label>Last Name</label>

                                    <p> <input id="lname" type="text" class="form-control" name="lname" value="{{$user_detail[0]->lname}}" required="required"></p>

                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-6">

                                    <label>Email</label>

                                    <p> <input id="email" type="email" class="form-control" name="email" value="{{$user_detail[0]->email}} " readonly="readonly" ></p>

                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-6">

                                    <label>Phone Number</label>

                                    <p><input type="text" id="user_mob" name="user_mob" class="form-control" value="{{$user_detail[0]->user_mob}}" number="number"  required="required"></p>

                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-6">

                                    <label>Date of Birth (MM/DD/YYYY)</label>

                                    <?php //$user_dobs = date("m/d/Y", strtotime($user_detail[0]->dob));

                                    if($user_detail[0]->dob!='0000-00-00'){
                 
                                        $user_dobs = date("m/d/Y", strtotime($user_detail[0]->dob));}else{$user_dobs = '';}
                                        ?>

                                    <p><input placeholder="MM/DD/YYYY" name="dob" onkeyup="date_reformat_mm(this);" value="{{$user_dobs}}" onkeypress="date_reformat_mm(this);" onpaste="date_reformat_mm(this);" autocomplete="off" type="text" class="form-control {{ $errors->has("dob") ? "is-invalid" : "" }}"></p>

                             @if($errors->has("dob"))

                            <span class="text-danger">{{ $errors->first("dob") }}</span>

                            @endif

                                    <!-- <p><input type="text" name="user_dob" placeholder="mm/dd/yyyy" value="{{$user_dobs}}" class="form-control"></p> -->

                                    <!-- <p><input type="text" id="user_dob" name="user_dob" class="form-control" value="{{$user_detail[0]->dob}}" required="required"></p> -->

                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-6">

                                    <label>Address</label>

                                    <p><input type="text" id="user_address" name="user_address" class="form-control" value="{{$user_detail[0]->user_address}}" onFocus="geolocate()" required="required"></p>

                                </div><!-- /.form-group -->



                                <div class="form-group col-lg-6">

                                    <label>City</label>

                                    <p><input type="text" id="locality" name="user_city" class="form-control" value="{{$user_detail[0]->user_city}}" required="required"></p>

                                </div><!-- /.form-group -->



                                 <div class="form-group col-lg-6">

                    

              <label>State</label>

              <p><select class="form-control" name="user_states" id="user_states" required="">

              <option value="">Choose...</option>

                  @if(!$state_list->isEmpty())

                    @foreach($state_list as $arr)

                      <option value="{{$arr->name}}" <?php if(!empty($user_detail[0]->user_states)){ if($user_detail[0]->user_states == $arr->name){ ?> selected="selected" <?php } }?>>{{$arr->name}}</option>

                    @endforeach

                  @endif

                </select></p>



                                 </div><!-- /.form-group -->



                                <div class="form-group col-lg-6">

                                  <label>Zip Code</label>

                                  <p> <input type="number" id="postal_code" name="user_zipcode" class="form-control" value="{{$user_detail[0]->user_zipcode}}" required="required"></p>

                                </div><!-- /.form-group -->



                                <div class="form-group col-lg-6">

                                  <label>Profile</label>

                                  <p> <input type="hidden" id="profile_image" name="profile_image" class="form-control" value="" accept="image/png, image/gif, image/jpeg"></p>


                                  <input type="hidden" name="profile_old" id="profile_old" value="{{$user_detail[0]->profile_image}}">


               <?php if(!empty($user_detail[0]->profile_image)){ ?> 

               <span id="proimgthumb">

               <img width="150px" height="150px" style="margin-left: 247px;" src="<?php if($id>0){ echo url('/public/uploads/user/').'/'.$user_detail[0]->profile_image; } ?>">
               </span>

                <?php } ?>
                                    
                                </div><!-- /.form-group -->

                                  <div class="form-group col-lg-6">

                                  <label>Upload Copy of Driver's License Front Side</label>

                               <p> <input type="file" id="license_front" name="license_front" class="form-control" value=""></p>

                                  <input type="hidden" name="license_front_old" value="{{$user_detail[0]->license_front}}">

                                  <?php if(!empty($user_detail[0]->license_front)){ ?>  

                                  <!--<img width="100px" height="100px" src="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_front; ?>">-->

                                   <object width="150px" height="150px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_front; ?>"></object>  

                                   <?php } ?>

                                </div><!-- /.form-group -->

                              <div class="form-group col-lg-6">

                                  <label>Upload Copy of Driver's License Back Side</label>

                                  <p> <input type="file" id="license_back" name="license_back" class="form-control" value=""></p>

                                  <input type="hidden" name="license_back_old" value="{{$user_detail[0]->license_back}}">

                                <?php if(!empty($user_detail[0]->license_back)){ ?>  
                                   <object width="150px" height="150px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_back; ?>"></object>  
                                 <?php } ?>

                                </div><!-- /.form-group -->



                             
              <div class="form-group col-lg-6">

              <label>Type Of Customer</label>

              <p><select name="customer_type" id="customer_type"  class="form-control">

            <option value="Recreational Customer" <?php if($user_detail[0]->customer_type=='Recreational Customer') {echo 'selected="selected"';}?>>Recreational Customer</option>

              <option value="Medical Customer" <?php if($user_detail[0]->customer_type=='Medical Customer') {echo 'selected="selected"';}?>>Medical Customer</option>

              </select></p>


              </div>



            <div class="form-group col-lg-6">

                  <label>Upload Copy of Medical Marijuana ID Card Front Side</label>

                   <p><input type="file" id="marijuana_card" name="marijuana_card" class="form-control" value=""></p>

                  <input type="hidden" name="marijuana_card_old" value="{{$user_detail[0]->marijuana_card}}">

                  <?php if(!empty($user_detail[0]->marijuana_card)){ ?>  
                                  
                                    <object width="150px" height="150px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->marijuana_card; ?>"></object>  
                                    
                                 <?php } ?>



              </div>


                              </div><!-- /.row -->

                              <div class="full-width">

                                    <div class="">

                                        <button type="submit" class="btn btn-primary color-btn update_infobtn" id="psubid">Update Profile</button>



                                    </div>

                                  </div>

                            </div>

                          </form>

                        </div>

                      </div>



                      <!--<div role="tabpanel" class="tab-pane" id="tab5">

                        <form>

                          <div class="profile-basic background-white p20 ">

                            <h3 class="page-title">

                              Payment Method

                            </h3>

                            <div class="add-new-method">

                              <a class="color-btn" data-toggle="modal" data-target="#add-pay-method"><i class="fa fa-plus"></i> Add new method</a>

                            </div>



                            <div class="method-list method-box">

                              <label class="control control--radio"> 

                                <input type="radio" name="radio" />

                                <div class="control__indicator"></div>

                              </label>

                              <span><img src="{{ url('/') }}/public/assets/images/pay-7.png"></span>

                             <div class="form-group updat-method">

                                <div class="method-btn">

                                    <button type="reset" class="color-btn"><i class="fa fa-edit"></i> </button>

                                </div>

                                <div class="method-btn">

                                    <button type="submit" class=" color-btn"><i class="fa fa-trash"></i></button>

                                </div>

                            </div>

                            <div class="updat-method-text">

                            	<p>lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam. lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam.</p>

                            </div>



                            </div>



                            <div class="method-list method-box">

                              <label class="control control--radio">

                                <input type="radio" name="radio" />

                                <div class="control__indicator"></div>

                              </label>

                              

                              <span><img src="{{ url('/') }}/public/assets/images/pay-1.png"></span>

                              <span><img src="{{ url('/') }}/public/assets/images/pay-2.png"></span>

                              <span><img src="{{ url('/') }}/public/assets/images/pay-3.png"></span>

                              <span><img src="{{ url('/') }}/public/assets/images/pay-4.png"></span>



                              <div class="form-group updat-method">

                                <div class="method-btn">

                                    <button type="reset" class="color-btn"><i class="fa fa-edit"></i> </button>

                                </div>

                                <div class="method-btn">

                                    <button type="submit" class=" color-btn"><i class="fa fa-trash"></i></button>

                                </div>

                            </div>

                            <div class="updat-method-text">

                            	<p>lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam. lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam.</p>

                            </div>

                            </div>



                          </div>

                        </form>

                      </div>



                      <div role="tabpanel" class="tab-pane" id="tab6">

                        <form>

                          <div class="profile-basic background-white p20">

                                <h3 class="page-title">

                                    Payout method 

                                </h3> 

                                <div class="method-list method-box">

                              <label class="control control--radio"> 

                                <input type="radio" name="radio" />

                                <div class="control__indicator"></div>

                              </label>

                              <span><img src="{{ url('/') }}/public/assets/images/pay-7.png"></span>

                             

                            <div class="updat-method-text">

                            	<p>lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam. lorem ipsum dolor sit , consectet adipisii elit, sed do eiusmod tempor for enim ens adesg ens minim veniam.</p>

                            </div>



                            </div>

                                

                          </div>

                        </form>

                      </div>-->



                <div role="tabpanel" class="tab-pane @if(Session::has('upassword')) active @endif" id="tab7">

                <form role="form" id="update_pwd" method="POST" action="{{ url('/password_action') }}">

                  {{ csrf_field() }}



                        <div class="profile-basic background-white p20">

                          <h3 class="page-title">Change Password</h3> 

                                <form>

                                <div class="verify-info">

                                  <div class="row">



                                    <div class="form-group col-lg-12">

                                        <label>Old Password</label>

                                          <input type="password" class="form-control {{ $errors->has("old_password") ? "is-invalid" : "" }}" name="old_password" id="old_password" value="{{ old('old_password') }}" required="required">



                          <!--<span class="pass-view fa fa-fw fa-eye field-icon toggle-password"></span>-->


                          @if($errors->has("old_password"))<span class="text-danger">{{ $errors->first("old_password") }}</span>@endif


                                </div><!-- /.form-group -->

                                <div class="form-group col-lg-12">

                                <label>New Password</label>


                                <input value="{{ old("password") }}" type="password" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}" name="password" id="new_password" required="required">


                          <!--<span class="pass-view fa fa-fw fa-eye field-icon toggle-password"></span>-->

                                                         
                                    </div><!-- /.form-group -->


                            <div class="form-group col-lg-12">

                            <label>Confirm Password</label>


                          <input type="password" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}" name="password_confirmation" id="cnew_password" required="required">


                              <!--<span class="pass-view fa fa-fw fa-eye field-icon toggle-password"></span>-->


                          @if($errors->has("password"))<span class="text-danger">{{ $errors->first("password") }}</span>@endif


                          @if($errors->has("password_confirmation"))<span class="text-danger">{{ $errors->first("password_confirmation") }}</span>@endif  


                          </div><!-- /.form-group -->

                                  </div><!-- /.row -->

                                </div>

                                <div class="full-width">

                                  <div class="">

                                  <button type="submit" class="btn btn-primary color-btn" id="update_oldpwd">Change Password</button>

                                  </div>

                                  </div> 

                          </div>

                        </form>

                      </div>



                  </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </section>



@endsection

@section("scripts")

{{-- on page scripts --}}
<!--  <script>
        
       $(function() {
  $("#date_s").date();
});

$("input[name='dob']:first").keyup(function(e) {
  var key = String.fromCharCode(e.keyCode);
  if (!(key >= 0 && key <= 9)) $(this).val($(this).val().substr(0, $(this).val().length - 1));
  var value = $(this).val();
  if (value.length == 2 || value.length == 5) $(this).val($(this).val() + '/');
});
    </script> -->


<script>
  document.getElementById('user_mob').addEventListener('input', function (e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
});
</script>





<script>

  $('#customer_type').change(function(){ 

    var value = $(this).val();

    if(value=='Medical Customer'){ 

    $("#marijuana_card").prop('required',true); 

    }else{

    $("#marijuana_card").prop('required',false); 

    }

});

   $(document).on('click', '.update_infobtn', function(){

      jQuery.validator.addMethod("vname", function (value, element) {

        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

          return true;

        } else {

          return false;

        };

      });


      jQuery.validator.addMethod("lname", function (value, element) {



        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



          return true;



        } else {



          return false;



        };



      });





      jQuery.validator.addMethod("user_city", function (value, element) {



        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



          return true;



        } else {



          return false;



        };



      });



      var form = $("#update_frm");



        form.validate({



          rules: {



            name:{



              required:true,



              vname:true



            },



            lname: {



              required: true,



              lname:true



            }



          },



            messages: {



              name:{



                required:'Please enter name.',



                vname:"Please enter only letters."



              },



              lname: {



                required:'Please enter last name.',



                lname:'Please enter only letters.'



              }



            }



        });



      var valid = form.valid();



     });



</script>



<script>



      var placeSearch, autocomplete;



      var componentForm = {



        street_number: 'short_name',



        route: 'long_name',



        locality: 'long_name',



        administrative_area_level_1: 'short_name',



        country: 'long_name',



        postal_code: 'short_name'



      };







      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



            /** @type {!HTMLInputElement} */(document.getElementById('user_address11')),



            {types: ['geocode']});







        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);



      }







      function fillInAddress() {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();







        for (var component in componentForm) {



          document.getElementById(component).value = '';



          document.getElementById(component).disabled = false;



        }







        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



          var addressType = place.address_components[i].types[0];



          if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



      



      //alert(val);



          }



        }



      }







      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      function geolocate() {



        if (navigator.geolocation) {



          navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {



              lat: position.coords.latitude,



              lng: position.coords.longitude



            };



            var circle = new google.maps.Circle({



              center: geolocation,



              radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



          });



        }



      }



    </script>



  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>  


<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script> 

<script type="text/javascript">

   $(document).ready(function () {

   $('#profile_image').awesomeCropper( { width: 600, height: 600, debug: true } );

    });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

setInterval(function () {

var profile_image = $('#profile_image').val();

if(profile_image!=0){

$('#psubid').prop('disabled', true);

}     

},2000); 

    setInterval(function () {

            var user_id = $('#user_id').val(); 
            var profile_image = $('#profile_image').val();

           // alert(profile_image);
          
            if(profile_image!=0){

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "https://www.grambunny.com/crop_profile_image",
                data: {productimg : profile_image, user_id : user_id}, 
                cache: false,
                success: function (data)
                {          
                    $('#profile_image').val('');
                    $('#profile_old').val(data.status);
                    $('#proimgthumb').hide();
                    $('#psubid').prop('disabled', false);
                }
            });

           }

          },6000); 
  
  /** Crop image */ 

</script>

 <script>
        
            function checkValue(str, max) {
                if (str.charAt(0) !== '0' || str == '00') {
                    var num = parseInt(str);
                    if (isNaN(num) || num <= 0 || num > max) num = 1;
                    str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
                };
                return str;
            };

  // reformat by date
       function date_reformat_mm(date) {
            date.addEventListener('input', function(e) {
                this.type = 'text';
                var input = this.value;
                if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                var values = input.split('/').map(function(v) {
                    return v.replace(/\D/g, '')
                });
                if (values[0]) values[0] = checkValue(values[0], 12);
                if (values[1]) values[1] = checkValue(values[1], 31);
                var output = values.map(function(v, i) {
                    return v.length == 2 && i < 2 ? v + '/' : v;
                });
                this.value = output.join('').substr(0, 14);
            });


        }
    </script>
@endsection