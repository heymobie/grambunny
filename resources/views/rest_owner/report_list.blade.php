@extends('layouts.owner')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

  <style type="text/css">

  #myInput{ float: right; width: 30%;margin-bottom: 10px; }
      
  </style>  

@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Product Order List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
              

				<div>
<div class="box-body table-responsive">
					@if(Session::has('message'))
		 
					<div class="alert alert-success alert-dismissable">
			            <i class="fa fa-check"></i>
			               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
			                    {{Session::get('message')}}
			        </div>
					@endif
			
                        
                    </div><!-- /.box-body -->

<div class="d-flex flex-wrap">
<div class="col-md-2" style="padding-left: 0px;">Filter by Order id : &nbsp;   	
<input type="text" name="order_id" id="order_id" style="width: 100%;">
</div>

<div class="col-md-2" style="padding-left: 0px;">Filter by Delivery Status: &nbsp;   	
<select name="order_status_serach" id="order_status_serach" style="width: 100%;">
<option value="5">All</option>	
<option value="0">Pending</option>
<option value="1">Accept</option>
<option value="2">Cancelled</option>
<option value="3">On the way</option>
<option value="4">Complete</option>
</select>
</div>

<div class="col-md-2" style="padding-left: 0px;">Filter By Customer Name:   	
<input type="text" name="customer_name" id="customer_name" style="width: 100%;">
</div>
</div>

<form action="#" method="post" id="datefilter">
<div class="col-md-2" style="padding-left: 0px;">Filter By Date: <br>   	
<input type="date" name="order_date" id="order_date" style="width: 100%;">
</div>
<div class="col-md-2 mt-5" style="padding-left: 0px; margin-top: 19px;">
<input type="date" name="from_order_date" id="from_order_date" style="width: 100%;">
</div>
<div class="col-md-2 mt-5" style="padding-left: 0px; margin-top: 19px;"> 	
<input type="submit" name="filter" id="filter" value="filter" style="width: 100%;background: #231f20;border: 1px solid #231f20;color: #FFF;padding: 7px !important;">
</div>
</form>

<!-- <div class="col-md-3" style="padding-left: 0px;">Filter &nbsp;   	
<input type="button" name="filter" id="filter" value="filter">
</div> -->

</br></br>

		
        <br>	

				<section id="mainsection" >
					
				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">
									<div style="float:right"><button><a href="/merchant/export_in_excel?var=<?php echo rand();?>">Export</a></button></div>
										<div >							
										<table id="example2" class="table table-bordered table-hover">
											<thead>
												<tr>
												<th>IDs#</th>
												<th>Order By</th>
												<th>Order Price</th>
												<th>Order Status</th>
												<th>Placed at</th>
												<th>Coupon Code</th>
												<th>Action</th>
												</tr>
											</thead>
											<tbody id="myTable">										
											<?php foreach ($order_detail as $key => $value) { 
											$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();
												$timestamp = strtotime($value->created_at);
												$dataPoints1 = array(
													array("label"=>date("d", $timestamp) , "y"=> $value->total),
													
												);
												//print_r($dataPoints1);
											?>

												<tr>
												<td>{{$value->order_id}}</td>
												
												<td>{{$userinfo->name}} {{$userinfo->lname}}</td>
												<td>${{ number_format($value->total, 2) }}</td>
												<td>							
												@if($value->status==0) Pending @endif
												@if($value->status==1) Accept @endif
												@if($value->status==2) Cancelled @endif
												@if($value->status==3) On the way @endif
												@if($value->status==4) Complete @endif
												@if($value->status==5) Requested for return @endif
												@if($value->status==6) Return request accepted @endif
												@if($value->status==7) Return request declined @endif
												</td>

											<?php $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); ?>
											
												<td>{{$createdat}}</td>

											<td>
											<?php $coupon_code = DB::table('coupon_code')->where('id', '=', $value->coupon_id)->first();
											echo @$coupon_code->coupon; ?>

												</td>
												
												<td>
												    <a href="{{ url('merchant/order-details').'/'.$value->id}}">View</a>
												</td>

												</tr> 

											<?php } ?>
											                                   
											</tbody>
											<tfoot>
												<tr>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												</tr>
											</tfoot>
											</table>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
	              	<?php 
	                if(!empty($customer_name)){ ?>
	              	{{ $order_detail->appends(['customer_name' => $searchid])->links() }}
	              	
	              	<?php }else if(!empty($merchant)){ ?>
	              	{{ $order_detail->appends(['merchant' => $merchant])->links() }}
	              	
	              	<?php }else if(!empty($order_status_serach)){ ?>
	              	{{ $order_detail->appends(['order_status_serach' => $order_status_serach])->links() }}
	              
	              	<?php }else if(!empty($order_date)){ ?>
	              	{{ $order_detail->appends(['order_date' => $order_date])->links() }}
	              
	              	<?php }else if(!empty($coupon)){ ?>
	              	{{ $order_detail->appends(['coupon' => $coupon])->links() }}
	              	<?php }else{ ?>

                    {{ $order_detail->links() }}

	              	<?php } ?>

	              </div>
	            </div>
	            
	            
				</section>	

				<section id="vender_search_list" >
				</section>

			
				
			  </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <!-- jQuery 2.0.2 -->
       <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

        <!-- page script -->
        <script type="text/javascript">
			

$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});


$('#order_search').on('change', function() {
  //alert( this.value );
	  if(this.value=='today_sale_report'){
	  		$('#today_sale').show();
	  		$('#week_sale').hide();
	  		$('#month_sale').hide();
	  		
	  }
	  else if(this.value=='this_week_report'){
	  		$('#today_sale').hide();
	  		$('#week_sale').show();
	  		$('#month_sale').hide();
	  		
	  }
	  else if(this.value=='this_month_report'){

	  		$('#today_sale').hide();
	  		$('#week_sale').hide();
	  		$('#month_sale').show();
	  		

	  }
});
        </script>

        <script type="text/javascript">
        	
        	$('#order_id').keyup(function() {
        		var order_id = $('#order_id').val();
        		//var order_status_serach = $('#order_status_serach').val();
        		// alert(order_status_serach);
        		// if(){

        		// }else{

        		// }
        		//var data = 'order_id='+ order_id  & 'order_status_serach='+ order_status_serach;
				$.ajaxSetup({
					  headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  }
				});
				$.ajax({
				type: "POST",
				url: "{{url('/merchant/filter_order')}}",
				data: ({order_id: order_id}),
					success: function(msg) {

					 	$("#mainsection").hide();	
						$('#vender_search_list').html(msg);
					}
				});
        		
        	});

		$('#order_status_serach').on('change', function() {
				var order_status_serach = $('#order_status_serach').val();
				$.ajaxSetup({
					  headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  }
				});
				$.ajax({
				type: "POST",
				url: "{{url('/merchant/filter_order_status')}}",
				data: ({order_status_serach: order_status_serach}),
					success: function(msg) {

					 	$("#mainsection").hide();	
						$('#vender_search_list').html(msg);
					}
				});
		});

		$('#customer_name').keyup(function() {
        		var customer_name = $('#customer_name').val();
				$.ajaxSetup({
					  headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  }
				});
				$.ajax({
				type: "POST",
				url: "{{url('/merchant/filter_order_customer')}}",
				data: ({customer_name: customer_name}),
					success: function(msg) {

					 	$("#mainsection").hide();	
						$('#vender_search_list').html(msg);
					}
				});
        		
        	});

		$( '#datefilter' ).on( 'submit', function(e) {
        		e.preventDefault();
        		var to_order_date = $('#order_date').val();
        		var from_order_date = $('#from_order_date').val();

        		var order_date = to_order_date+'_'+from_order_date;

        		//alert(order_date);

				$.ajaxSetup({
					  headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  }
				});
				$.ajax({
				type: "POST",
				url: "{{url('/merchant/filter_order_date')}}",
				data: ({order_date: order_date}),
					success: function(msg) {

					 	$("#mainsection").hide();	
						$('#vender_search_list').html(msg);
					}
				});
        		
        	});
		

				$('#coupon').keyup(function() {
        		var coupon = $('#coupon').val();
				$.ajaxSetup({
					  headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					  }
				});
				$.ajax({
				type: "POST",
				url: "{{url('/admin/filter_order_coupon')}}",
				data: ({coupon: coupon}),
					success: function(msg) {

					 	$("#mainsection").hide();	
						$('#vender_search_list').html(msg);
					}
				});
        		
        	});

        </script>




@stop
