@extends('layouts.owner')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

  <style type="text/css">

  #myInput{ float: right; width: 30%;margin-bottom: 10px; }
      
  </style>  

@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Return Order Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Return Order List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
              
              
				<div>
    <div class="box-body table-responsive">
					@if(Session::has('message'))
		 
					<div class="alert alert-success alert-dismissable">
			            <i class="fa fa-check"></i>
			               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
			                    {{Session::get('message')}}
			        </div>
					@endif
			
                        
                    </div><!-- /.box-body -->
		<div class="col-md-5">Order Status : &nbsp;   	
<select name="order_search" id="order_search" onchange="location = this.value;">
<option value="{{url('/merchant/return-list/')}}"@if($searchid=='') selected @endif>All</option>	
<option value="{{url('/merchant/return-list/5')}}"@if($searchid=='5') selected @endif>Requested for return </option>
<option value="{{url('/merchant/return-list/6')}}"@if($searchid=='6') selected @endif>Return request accepted</option>
<option value="{{url('/merchant/return-list/7')}}"@if($searchid=='7') selected @endif>Return request declined</option>
</select>

			</div>

		</br></br>

		<input id="myInput" type="text" placeholder="Search..">
        <br>	

				<section>
				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">
										<div id="vender_search_list">							
										<table id="example2" class="table table-bordered table-hover">
<thead>
<tr>
<th>IDs#</th>
<th>#</th>
<th>Order By</th>
<th>Order Price</th>
<th>Order Status</th>
<th>Placed at</th>
<!--<th>Payment Type</th>-->
<th>Action</th>
</tr>
</thead>
<tbody id="myTable">										
<?php foreach ($order_detail as $key => $value) { 
$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();?>

<tr>
<td>{{$value->order_id}}</td>
<td><?php if($userinfo->profile_image){ ?><img src="{{ url('/public/uploads/user/'.$userinfo->profile_image) }}" width="50px;" height="50px;"><?php }else{ ?> <img src="{{ url('/public/uploads/user/user.jpg') }}" width="50px;" height="50px;"> <?php } ?>	
</td>
<td>{{$userinfo->name}} {{$userinfo->lname}}</td>
<td>${{$value->total}}</td>
<td>							
@if($value->status==0) Pending @endif
@if($value->status==1) Accept @endif
@if($value->status==2) Cancelled @endif
@if($value->status==3) On the way @endif
@if($value->status==4) Delivered @endif
@if($value->status==5) Requested for return @endif
@if($value->status==6) Return request accepted @endif
@if($value->status==7) Return request declined @endif

</td>
<td>{{$value->created_at}}</td>
<!--<td>{{$value->payment_method}}</td>-->

<td>
  <!--<a title="Edit Category" href="{{url('admin/product_service_category_form')}}/{{ $value->id }}"><i class="fa fa-edit"></i></a>
    <a title="Delete Category" href="{{url('admin/product_service_category_delete')}}/{{ $value->id }}"><i class="fa fa-trash-o"></i></a>-->
    <a href="{{ url('merchant/order-details').'/'.$value->id}}">View</a>
</td>

</tr> 

<?php } ?>
                                   
</tbody>
<tfoot>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</tfoot>
</table>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
	              	{{ $order_detail->links() }}
	              </div>
	            </div>
	            
				</section>					
			  </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

        <!-- page script -->
        <script type="text/javascript">
function check_frm()
{


$('#error_msg').hide();

var form = $("#search_frm");
		form.validate();
	var valid =	form.valid();
	if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/vendor_search')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#vender_search_list').html(msg);
			}
		});
	}
	else
	{
		//alert('Please insert any one value');
		
		$('#error_msg').html('Please insert any one value');
		$('#error_msg').show();
		return false;
	}		
}			

$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
        </script>
@stop
