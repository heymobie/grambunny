@extends('layouts.owner')
@section("other_css")
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
   .deepmd{ width: 100%; }
   .deepmd strong{ float: right; }
   .deepon{ font-size: 14px; } 
   .top-detail_invoice {
   float: left;
   width: 100%;
   text-align: center;
   border-bottom: 1px dashed #ddd;
   margin-bottom: 10px;
   text-transform: capitalize;
   }
   .top-detail_invoice h3 {
   text-transform: uppercase;
   font-weight: bold;
   }
   table {
    border-collapse: collapse;
    width: 100%;
}

.rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    z-index: -99;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}

#submit-merchant-review{ margin-top: 10px; }

.msg-box span#message {
padding-left: 20px;
}
.msg-box .alert-dismissable .close {
top: -7px;
}
.msg-box div#message-box i {
position: absolute;
left: 7px;
top: 7px;
}


.billing table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin-top: 10px;
}
.billing th {
  border: 1px solid #ccc;
  border-collapse: collapse;
  text-align: center;
  padding: 8px;
}
.billing td {
  border: 1px solid #ccc;
  border-collapse: collapse;
  padding: 8px;
}

div#gettime {
    font-size: 20px;
    text-align: left;
    font-weight: bold;
/*   // border: 1px solid;*/
    width: 80px;
    padding: 5px;
}

.ridetime{

font-size: 20px;
    text-align: left;
    font-weight: bold;
    width: 150px;
    padding: 5px;
    float: left;
  
}

</style>
@stop
@section('content')
<aside class="right-side">
   <section class="content-header">
      <h1>Event Order Detail</h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li class="active">Event Order Detail</li>
      </ol>
   </section>
   <div class="myOdrersDetailBox">
      <div class="row">
      
         <div class="col-md-2">
         </div>
<?php $vendor_info = DB::table('vendor')->where('vendor_id', '=' ,$order_detail->vendor_id)->first();?>

         <div class="col-md-8">
            <div class="myOrder">
               <div class="myOrderDtelBox">
                  <div class="top-detail_invoice">
                     <h3>{{$vendor_info->business_name}}</h3>
                     <div class="area_market"> <p><strong>{{$vendor_info->address}}, {{$vendor_info->mailing_address}}, {{$vendor_info->city}}, {{$vendor_info->state}}, {{$vendor_info->zipcode}}</strong></p>
                     </div>
                     <div class="order-pace-b">
                      <?php $order_date = date("Y-m-d g:iA", strtotime($order_detail->created_at)); ?>
                     <p> Order Placed At
                        {{$order_date}}
                     </p>

                      <p>Permit/License Number: {{$vendor_info->permit_number}}</p>
                     <br>
                     <p>Order ID: {{$order_detail->order_id}}</p>
                   </div>
                  </div>

                  <h3 class="ev-detail">TICKET ORDER DETAILS</h3>

                  <div class="col-md-6 cust-bold">
                  <div class="customer_detail">
                    <strong>Customer</strong>
                     <p> {{$order_detail->first_name}} {{$order_detail->last_name}}</p>
                     <p>{{$order_detail->mobile_no}}</p>
                     <p class="mail-d">{{$order_detail->email}}</p><br>
                    <!-- <p>Order Status: Delivered </p> -->                                          
                  </div>
                  </div>

                  <div class="col-md-6 cust-bold">
                  <div class="customer_detail">
                    <strong>Merchant</strong>
                     <p>{{$vendor_info->name}} {{$vendor_info->last_name}}</p>
                     <p>{{$vendor_info->mob_no}}</p>
                     <p class="mail-d">{{$vendor_info->email}}</p>
                     <br>
                     <p>Instructions : <?php echo $ps_list[0]->sinstruction; ?> </p>

                  </div>
                  </div>

                  <div class="customer_detail_border">&nbsp;</div>
   <div class="desc-box">

<table class="table table-borderless">
                        <thead>
                            <tr>
                                <th scope="col">Qty</th>
                                <th colspan="3">Description</th>
                                <th scope="col">Price</th>
                                
                            </tr>
                        </thead>
                        <tbody>
<?php 

//$orderstatus = DB::table('orders')->where('id','=',$ps_list[0]->order_id)->value('status');

?>

<?php foreach ($ps_list as $key => $value) {

  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); ?>

   @if(!empty($pservice->vendor_id))

   @if( $pservice->vendor_id!=0)

  <tr class="br-tn">
      <td>{{$value->ps_qty}}</td>
      <td colspan="3">
      <b><?php if(!empty($pservice->name)){ ?> {{$pservice->name}} <?php } ?></b>
      </td>
      <td><?php if(!empty($pservice->price)){ ?> ${{$pservice->price}} <?php } ?></td>
    
  </tr>

  @endif
  @endif
  <?php } ?>        
                          
<br><br>
<tr class="p-gb mt-3">
<td colspan="3"></td>
<td style="text-align: right;"><b>Sub Total :</b></td>
<td><b>${{ number_format($order_detail->sub_total, 2) }}</b></td>
</tr>

<?php $cc = DB::table('coupon_code')->where('id','=',$order_detail->coupon_id)->first();?>

<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Discount :</b></td>
<td><b>${{ number_format($order_detail->promo_amount, 2) }}</b></td>
</tr>

<?php if($order_detail->product_type == 3){  ?>

<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Ticket Fee :</b></td>
<td><b>${{ number_format($order_detail->ticket_fee * $value->ps_qty, 2) }}</b></td>
</tr>

<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Ticket Service Fee :</b></td>
<td><b>${{ number_format($order_detail->ticket_service_fee * $value->ps_qty, 2) }}</b></td>
</tr>

<?php } else { ?>

<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Delivery Fee :</b></td>
<td><b>${{ number_format($order_detail->delivery_fee, 2) }}</b></td>
</tr>  

<?php  } ?>


<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Sales Tax :</b></td>
<td><b>${{ number_format($order_detail->service_tax, 2)}}</b></td>
</tr> 


 <tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Total Paid Amount :</b></td>
<td><b>${{ number_format($order_detail->total, 2) }}</b></td>
</tr> 



<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Payment Type :</b></td>
<td><b>{{$order_detail->payment_method}}</b></td>
</tr> 
     

<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>TransactionID :</b></td>
<td><b>{{$order_detail->txn_id}}</b></td>
</tr>      


<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Payment Status :</b></td>
<td><b>{{$order_detail->pay_status}}</b></td>
</tr>        
                     
<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Comment :</b></td>
<td><b>{{$order_detail->instruction}}</b></td>
</tr> 

<?php if(!empty($cc->coupon)){ ?>
<tr class="br-tn p-gb">
<td colspan="3"></td>
<td style="text-align: right;"><b>Coupon Code Applied :</b></td>
<td><b><?php echo  @$cc->coupon; ?></b></td>
</tr>
<?php } ?>


</tbody>
</table>

</div>

</div>

            </div>
         </div>


      </div>
   </div>
   </div>
</aside>
@stop
@section('js_bottom')
<style>
   #ajax_favorite_loddder {
   position: fixed;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background:rgba(27, 26, 26, 0.48);
   z-index: 1001;
   }
   #ajax_favorite_loddder img {
   top:50%;
   left:46.5%;
   position:absolute;
   }
   .footer-wrapper {
   float:left;
   width:100%;
   }
   #addons-modal.modal {
   z-index:999;
   }
   .modal-backdrop {    
   z-index:998 !important;
   }
</style>
<div id="ajax_favorite_loddder" style="display:none;">
   <div align="center" style="vertical-align:middle;">
      <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>       
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>        
<script type="text/javascript">
   $('#order_status').on('change', function(){
   
       var val = $('#order_status').val();
   
       if( val == 2){
   
       $('#cancel_area').html('<textarea style="width: 337px;" class="form-control animated"  id="cancel_reason" name="cancel_reason" placeholder="Enter cancel reason..." rows="3"></textarea>');
   
       }
   
   });
   
   
   
   function check_frm(){
   
   $('#error_msg').hide();
   
   var form = $("#search_frm");
   
   form.validate();
   
   var valid =  form.valid();
   
   if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
   
   {        
   
   $("#ajax_favorite_loddder").show();  
   
   var frm_val = $('#search_frm').serialize();              
   
   $.ajax({
   
   type: "POST",
   
   url: "{{url('/admin/vendor_search')}}",
   
   data: frm_val,
   
   success: function(msg) {
   
    $("#ajax_favorite_loddder").hide(); 
   
   
   
    $('#vender_search_list').html(msg);
   
   }
   
   });
   
   }
   
   else
   
   {
   
   //alert('Please insert any one value');
   
   
   
   $('#error_msg').html('Please insert any one value');
   
   $('#error_msg').show();
   
   return false;
   
   }        
   
   }
   
   $(function() {
   
   // $("#example1").dataTable();
   
   $('#example2').dataTable({
   
   "bPaginate": true,
   
   "bLengthChange": false,
   
   "bFilter": true,
   
   "bSort": true,
   
   "bInfo": true,
   
   "bAutoWidth": false
   
   });
   
   });


        $('#submit-merchant-review').on('click', function() {
   
        $.ajax({
   
          type: "post",
   
          headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
   
          data: $("#merchant-rating-form").serialize(),
   
          url: "{{url('/save-customer-review')}}",       
   
          success: function(data) { 
   
            $("#message-box").show();
   
            $("#message-box").removeClass();
   
            if(data.ok){
   
              $("#message-box").addClass("alert alert-success alert-dismissable");
   
              $("#message").text(data.message);
   
            }else{
   
              var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message">'+data.message+'</span></div>';
   
              $('.msg-box').html(html);
   
              $("#message-box").addClass("alert alert-danger alert-dismissable");
   
              $("#message").text(data.message);
   
            }
   
              }
   
          });
   
      });
   
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript">

 //$(document).ready(function(){   

    function myTime(){

   var order_id = $("#myField").val();

    $.ajax({

      type: "POST",

      dataType: "json",

      headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

      url: "<?php echo url('/merchant/order_get_timer'); ?>",

      data: {'order_id': order_id},

       success: function(data){
      
     $("#gettime").text(data);
  },

    });
}

   var myTimes = setInterval(myTime, 1000);

     // });
  
</script>

@stop