@extends('layouts.owner')

@section("other_css")
        <!-- DATA TABLES -->
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Order View
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/vendor/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Order Listing</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					
                        <div class="col-xs-12">
                            <div class="box" style="display:block;float:left;padding-bottom:10px;">
                                <div class="box-header">
                                    <h3 class="box-title">Order View</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
										</div>
									
                                </div><!-- /.box-header -->
                                <div class="box-body  table-responsive">
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-5">
										
										<!--1=> Order received (Delivery Pending)
2=> Order canceled by customer 
3=> Oder canceled by Restaurant owner
4=> Order Completed (Either Deivered or Pickup)
5=> Order rating Done by Customer -->


									  ORDER STATUS: 
									 @if($order_detail[0]->order_status=='1') Submit  @endif
									 @if($order_detail[0]->order_status=='2') Cancelled @endif
									 @if($order_detail[0]->order_status=='3') Sent to Partner @endif
									 @if($order_detail[0]->order_status=='4') Order Confirmed @endif
									 @if($order_detail[0]->order_status=='5') Order Completed @endif
									 @if($order_detail[0]->order_status=='6') Reject @endif
									 @if($order_detail[0]->order_status=='7') Review Completed @endif
									  
									  
									  </div>
									  <form name="order_update" id="order_update" method="post" >
									  <div class="col-md-7">
									  Change Status to:  
					 <input type="hidden" name="order_id" id="order_id" value="{{$order_detail[0]->order_id}}" />
					 <input type="hidden" name="old_status" id="old_status" value="{{$order_detail[0]->order_status}}" />
					
					<select name="order_status" id="order_status">
						<option value="1"@if($order_detail[0]->order_status=='1') selected="selected"@endif>Submit</option>
						<option value="2"@if($order_detail[0]->order_status=='2') selected="selected"@endif>Cancelled</option>
						<!--<option value="3"@if($order_detail[0]->order_status=='3') selected="selected"@endif>Sent to Owner</option>-->
						<option value="4"@if($order_detail[0]->order_status=='4') selected="selected"@endif>Order Confirmed</option>
						<option value="5"@if($order_detail[0]->order_status=='5') selected="selected"@endif>Order Completed</option>
						<option value="6"@if($order_detail[0]->order_status=='6') selected="selected"@endif>Order Reject</option>
						<option value="7"@if($order_detail[0]->order_status=='7') selected="selected"@endif>Review Completed </option>
				</select>
									
										<input type="button" value="Submit" id="update_status" 
											@if(($order_detail[0]->order_status == "2" ) ||
											($order_detail[0]->order_status == "6" ) ||
											($order_detail[0]->order_status == "5" )||
											($order_detail[0]->order_status == "7" )
										   )
										disabled="disabled" @endif/>
									 <div id="capture_resone" style="display:none @if(($order_detail[0]->order_status == '2' ) || ($order_detail[0]->order_status == '6' )) display:block @endif
										   ">  
									 <span>	 Enter Reason </span> 
									 <textarea name="order_cancel_reason" id="order_cancel_reason" required="required">{{trim($order_detail[0]->order_cancel_reason)}}</textarea>
									 </div>	
										
								<div id="error_msg" style="color:#FF0000; display:none;"></div>
									   </div>
									   
								{!! csrf_field() !!}
										</form>
									</div>
									
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-6">
									  	@if(!empty($order_detail[0]->rest_logo))	
											<img src="{{ url('/') }}/uploads/reataurant/{{ $order_detail[0]->rest_logo }}" width="50px;" height="50px;">
										@else
											<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >
										@endif		
									  </div>
									  <div class="col-md-6">
									  
									   <strong style="padding-top:5px;text-align:center">{{$order_detail[0]->order_type}} Order Details</strong>
									   </div>
									</div>
									
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-2"> Venue:</div> 
											<div class="col-md-9">{{$order_detail[0]->rest_name}}</div>
										<div class="col-md-2"> Address:</div>
											<div class="col-md-9">{{$order_detail[0]->rest_address.' '.$order_detail[0]->rest_address2}}
											{{$order_detail[0]->rest_suburb}}
											{{$order_detail[0]->rest_state}}
											{{$order_detail[0]->rest_zip_code}}
											</div>
										<div class="col-md-2"> Contact No:</div> 
											<div class="col-md-9">{{$order_detail[0]->rest_contact}}</div>
									</div>
								
									
									<div class="col-md-12" style="border:1px solid #333333;">
										<div class="col-md-6">&nbsp;</div> 
										<div class="col-md-6">
											Request Submeted Date: {{$order_create_date}}
										</div>	
										
										<div class="col-md-3">Order Number:</div> 
											<div class="col-md-9">{{$order_detail[0]->order_uniqueid}}</div>
										
										<div class="col-md-3"> Pick-Up Date:</div> 
											<div class="col-md-9">
												@if(($order_detail[0]->order_typetime=='later') || 
												($order_detail[0]->order_pickdate!='0000-00-00')
												)
												

												{{$order_detail[0]->order_pickdate}}&nbsp;
												{{substr($order_detail[0]->order_picktime, 0, 2) }}:{{ substr($order_detail[0]->order_picktime, 2, 3)}}
&nbsp;
												@else
												{{$order_create_date}}
												@endif
											</div>
										
											@if($order_detail[0]->order_type=='Delivery')
										<div class="col-md-3"> Delivery Address:</div> 
											<div class="col-md-9">
											{{$order_detail[0]->order_deliveryadd1}} 
											{{$order_detail[0]->order_deliveryadd2}} 
											{{$order_detail[0]->order_deliverysurbur}} 
											{{$order_detail[0]->order_deliverypcode}}
											
											
											</div>
											@endif
											
										<!--<div class="col-md-3"> Amount:</div>
											 <div class="col-md-9">&nbsp;</div>-->
									</div>
								
									
									<div class="col-md-12" style="border:1px solid #333333;">
										
										<div class="col-md-3"> &nbsp;</div>
											 <div class="col-md-9" style="text-align:center"> 
												 <strong> Payment Method : <?php echo  $order_detail[0]->order_pmt_method;?></strong>
											 </div>
											 
										<div class="col-md-3"> Customer Name: </div>
											<div class="col-md-9">
										{{$order_detail[0]->order_fname}} 
											{{$order_detail[0]->order_lname}} 
										</div> 
											
										<div class="col-md-3"> Mobile No:</div>
											 <div class="col-md-9">{{$order_detail[0]->order_tel}} </div>
										<!--<div class="col-md-3"> Landline:</div>
											<div class="col-md-9">&nbsp;</div>-->
										<div class="col-md-3"> Customer Comment:</div>
											 <div class="col-md-9">{{$order_detail[0]->order_instruction}}&nbsp; </div>
										<div class="col-md-3"> Paypal ref number:</div>
											 <div class="col-md-9">{{$order_detail[0]->pay_tx}} &nbsp;</div>
											 
									</div>
									
									<div class="col-md-12" style="border:1px solid #333333;">
									
									
									
										<table width="100%" border="1" cellpadding="2" cellspacing="2">
											<tr>
												<td width="25%" align="center"><b>Item Details</b></td>
												<td width="15%" align="center"><b>Unit Size</b></td>
												<td width="10%" align="center"><b>Qty</b></td>
												<td width="10%" align="center"><b>Unit Price</b></td>
												<td width="10%" align="center"><b>Total</b></td>
											</tr>
											<?php
											 $cart_price = 0;
										$order_carditem = json_decode($order_detail[0]->order_carditem, true);?>	
											 @foreach($order_carditem as $item)
											<?php  
											$show_item_price = 0;
											$show_addon_price = 0;
											$show_sub_total = 0;
											
											$unit_name = '';
											
											if(!empty($item['options']['option_name']))
											{
												$category_item = DB::table('category_item')
																 ->select('*')
																 ->where('menu_cat_itm_id', '=' ,$item['options']['option_name'])
																 ->get();
												if($category_item )
												{
													$unit_name = $category_item[0]->menu_item_title; 
												}
												
											}
											
											
												$cart_price = $cart_price+$item['price'];
												$addon_price = '';
												$addon_name = '';											
											?>
											 @if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
							@foreach($item['options']['addon_data'] as $addon)
								
							  <?php $cart_price = $cart_price+$addon['price'];
							  $addon_price = $addon['price'];
							  $addon_name = $addon['name'];
							  
							  ?>
							
							 @endforeach
						 @endif
						 
						 
												
						 
											<tr>
												<td width="25%" align="left">{{$item['name']}} 
												
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
													@foreach($item['options']['addon_data'] as $addon)
														 <div style="padding:5px;">{{$addon['name']}}</div>
													
													 @endforeach
												 @endif
						 
												</td>
												<td width="15%" align="center">{{$unit_name}}</td>
												<td width="10%" align="center">{{$item['qty']}}</td>
												<td width="10%" align="right">
														<?php if(empty($order_detail[0]->order_restcartid)){?>
												${{number_format($item['price']/$item['qty'],2)}}
												
												
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
													@foreach($item['options']['addon_data'] as $addon)
														 <div style="padding:5px;">${{number_format(($addon['price']/$item['qty']),2)}}</div>
													
													 @endforeach
												 @endif
												
												
												
												<?php }else{?>
												${{number_format($item['price'],2)}}
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
													@foreach($item['options']['addon_data'] as $addon)
														 <div style="padding:5px;">${{number_format(($addon['price']),2)}}</div>
													
													 @endforeach
												 @endif
												
												
												<?php }?>
												
												</td>
												<td width="10%" align="right">
												<?php if(empty($order_detail[0]->order_restcartid)){?>
												${{number_format($item['price'],2)}}
												
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
													@foreach($item['options']['addon_data'] as $addon)
														 <div style="padding:5px;">${{number_format(($addon['price']),2)}}</div>
													
													 @endforeach
												 @endif
												
												
												
												<?php }else{?>
												${{number_format($item['price']*$item['qty'],2)}}		
												
												@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
													@foreach($item['options']['addon_data'] as $addon)
														 <div style="padding:5px;">${{number_format(($addon['price']*$item['qty']),2)}}</div>
													
													 @endforeach
												 @endif
												
																						
												<?php }?>
												 
												
												
												</td>
											</tr>
											@endforeach
											<tr>
												<td width="25%" align="right"><b>Sub Total</b></td>
												<td colspan="4" align="right">
												<?php echo '$'.number_format($order_detail[0]->order_subtotal_amt,2);?>
												<?php /*?>${{number_format($cart_price,2)}}<?php */?></td>
											</tr>
											<tr>
												<td width="25%" align="right"><b>Delivery Fee</b></td>
												<td colspan="4" align="right">${{number_format($order_detail[0]->order_deliveryfee,2)}}</td>
											</tr>
											
											@if($order_detail[0]->order_remaning_delivery>0)
											<tr>
											  <td width="25%" align="right"> 
											   ${{number_format($order_detail[0]->order_min_delivery,2)}} min Delivery amount<br /> 
											   Remaining Amount :${{number_format($order_detail[0]->	order_remaning_delivery,2)}}
											  </td>
												<td colspan="4" align="right">${{number_format($order_detail[0]->	order_remaning_delivery,2)}}
													</td>
											 
											</tr>
											@endif
											
											
											
											@if($order_detail[0]->order_promo_cal>0)
											<tr>
											  <td width="25%" align="right"> 
											  <b> Discount</b>
											  </td>
											  <td colspan="3" align="right">{{$order_detail[0]->order_promo_applied}}
											  	<br />
												{{$order_detail[0]->order_food_promo}}
											  </td>
											 <td align="right">-${{number_format($order_detail[0]->	order_promo_cal,2)}}</td>
											 
											</tr>
											@endif
											
											
											<?php 
												
												if($order_detail[0]->order_pmt_type!='1')
												{
												?>
											<tr>
												<td width="25%" align="right"><b>Partial Payment (Paid)</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_partial_payment,2)}}</td>
											</tr>
											<tr>
												<td width="25%" align="right"><b>Partial Remaning Payment</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_partial_remain,2)}}</td>
											</tr>
												
												<?php 
												}
											
											?>
											
											
											
											<tr>
												<td width="25%" align="right"><b>Sales Tax</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_service_tax,2)}}</td>
											</tr>


											<tr>
												<td width="25%" align="right"><b>Amount Paid</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_paid_amt,2)}}</td>
											</tr>
											<tr>
												<td width="25%" align="right"><b>Remaining Balance</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_remaing_amt,2)}}</td>
											</tr>


											<tr>
												<td width="25%" align="right"><b>Total Charge</b></td>
												<td colspan="4" align="right">
												${{number_format($order_detail[0]->order_total,2)}}</td>
											</tr>
										</table>
									</div>
								
									<div style="float:right; margin:5px;"> 
										<input class="btn btn-primary" type="button" value="Go Back"  onClick="history.go(-1);">
									 </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
							<div>				
						  </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
			



$(document).on('click', '#update_status', function(){ 

 
 $('#error_msg').hide();
 var order_id = $('#order_id').val();	
 var old_status = $('#old_status').val();	
 var new_status = $("#order_status option:selected" ).val();
 /*alert(order_id);
		alert(old_status);
		alert(new_status);*/
		
  if(new_status=='1')
  { 
  	$('#error_msg').html('Please change status.');
	$('#error_msg').show();
  //	alert('Please change status');
	return false; 
  }
  else if( (new_status=='7') && (old_status!='5'))
  { 
  	$('#error_msg').html('Without order completed, you can not set review status');
	$('#error_msg').show();
  //	alert('Please change status');
	return false; 
  }
  else if(new_status>1)
  {
  
  
  	$('#error_msg').hide();
  
  var form = $("#order_update");
	form.validate();
	var valid =	form.valid();
	
	
	if(valid){	
		
		//$('#order_update').submit();
	 $("#ajax_favorite_loddder").show();
	 
	 	//var rest_id = $(this).attr('data-rest');	
	 	//var promo_id = $(this).attr('data-promo');	
		var frm_val = $('#order_update').serialize();	
		$.ajax({
		type: "post",
		url: "{{url('/vendor/order_status_update')}}",
		data: frm_val,
			success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				//$('#Promotions').html(msg);
			//	alert(msg);
				 location.reload();
			}
		});
	}
	else
	{
		return false;
	}	
  }		
 
});

$(document).on('change', '#order_status', function(){ 
//alert('test'); 
  
		var select_val = $(this).find('option:selected').val();
	//	alert(select_val); 
	if(select_val=='2' || select_val =='6')
	{
		$('#capture_resone').show();
	}
	else
	{
		$('#capture_resone').hide();
	}
});


  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>
@stop
