
 
    <input type="hidden" value="@if(!empty($today_date[0]->y)){{ $today_date[0]->y }}@endif" id="order8">

      <input type="hidden" value="@if(!empty($today_date[0]->x)){{ $today_date[0]->x }}@endif" id="order108">

      <input type="hidden" value="@if(!empty($today_sell[0]->total)){{ $today_sell[0]->total }}@endif" id="order109">

       <input type="hidden" value="@if(!empty($today_user[0]->user_id)){{ $today_user[0]->user_id }}@endif" id="order107">

       <input type="hidden" value="@if(!empty($yesterday)){{ $yesterday }}@endif" id="orderid_001">

        <input type="hidden" value="@if(!empty($yesterday)){{ $yesterday }}@endif" id="orderid_002">
         
        <input type="hidden" value="@if(!empty($one_month)){{ $one_month }}@endif" id="orderid_003">
    
<div>
    <canvas id="line-chart" width="500" height="150"></canvas>
 </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@4.0.1/dist/chart.umd.min.js"></script>
<script>
var orderid_001 = $( "#orderid_001" ).val();
var orderid_002 = $( "#orderid_002" ).val();
var orderid_003 = $( "#orderid_003" ).val();

  if(orderid_001=="1"){

  var order8 = $( "#order8" ).val();
   var order107 = $( "#order107" ).val();
   var order108 = $( "#order108" ).val();

   var order109 = $( "#order109" ).val();
    new Chart(document.getElementById("line-chart"), {

      type : 'line',
      data : {
        labels : [ order108],
        datasets : [
            {
              data : [ order8],
              label : "orders",
              borderColor : "#3cba9f",
              fill : false
            },
            {
              data : [ order109],
              label : "Total Sell",
              borderColor : "#e43202",
              fill : false
            },
            // {
            //   data : [ order107],
            //   label : "Total Customer",
            //   borderColor : "#545454",
            //   fill : false
            // }
           
            ]
      },
   
      options : {
        title : {
          display : true,
          text : ''
        }
      }
   
    });
  }else if(orderid_002=="2"){

  var order8 = $( "#order8" ).val();
  var order107 = $( "#order107" ).val();
   var order108 = $( "#order108" ).val();
    new Chart(document.getElementById("line-chart"), {

      type : 'line',
      data : {
        labels : [ order108],
        datasets : [
            {
              data : [ order8],
              label : "orders",
              borderColor : "#3cba9f",
              fill : false
            },

            {
              data : [ order109],
              label : "Total Sell",
              borderColor : "#e43202",
              fill : false
            },

            // {
            //   data : [ order107],
            //   label : "Total Customer",
            //   borderColor : "#545454",
            //   fill : false
            // }
           
            ]
      },
   
      options : {
        title : {
          display : true,
          text : ''
        }
      }
   
    });

  }else if(orderid_003=="3"){

   new Chart(document.getElementById("line-chart"), {

      type : 'line',
      
      data : {
        
        labels : [ 

            @foreach ($today_date as $res)

            "{{$res->x}}", 
           
            @endforeach
         ],
     
        datasets : [
            {
            
            data : [  @foreach ($today_date as $res1)

            {{$res1->y}},

            @endforeach

            ],
           
              label : "orders",
              borderColor : "#3cba9f",
              fill : false
            },

            {
            
            data : [  @foreach ($today_sell as $res1)

            {{$res1->total}},

            @endforeach

            ],
           
              label : "Total Sell",
              borderColor : "#e43202",
              fill : false
            },

          //   {
          // data : [ @foreach ($today_user as $res1)

          //  {{($res1->user_id)}},

          //   @endforeach

          //    ],
          //     label : "Total Customer",
          //     borderColor : "#545454",
          //     fill : false
          //   }
           
            ]
      },

      options : {
        title : {
          display : true,
          text : ''
        }
      }
    });
   
  // });
  }else{

    new Chart(document.getElementById("line-chart"), {

      type : 'line',
      
      data : {
        
        labels : [ 

            @foreach ($today_date as $res)

            "{{$res->x}}", 
           
            @endforeach
         ],
     
        datasets : [
            {
            
            data : [  @foreach ($today_date as $res1)

            {{$res1->y}},

            @endforeach

            ],
           
              label : "orders",
              borderColor : "#3cba9f",
              fill : false
            },

            {
            
            data : [  @foreach ($today_sell as $res1)

            {{$res1->total}},

            @endforeach

            ],
           
              label : "Total sell",
              borderColor : "#e43202",
              fill : false
            },

          //    {
          // data : [ @foreach ($today_user as $res1)

          //  {{($res1->user_id)}},

          //   @endforeach

          //    ],
          //     label : "Total Customer",
          //     borderColor : "#545454",
          //     fill : false
          //   }
           
            ]
      },

      options : {
        title : {
          display : true,
          text : ''
        }
      }
    });

  }
  $(".chart").hide();
  $(".filter").show();
</script>