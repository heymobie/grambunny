@extends('layouts.owner')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Change Password <small>Control Panel</small></h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('merchant.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Change Password</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				
				
				@if(Session::has('message'))
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					{{Session::get('message')}}
				</div>
				@endif
				<!-- form start -->
				<form  role="form" method="POST" action="{{ route('merchant.changePassword') }}">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Old Password</label>
							<input type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" value="{{ old('old_password') }}" required >
							@error('old_password')
    							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">New  Password</label>
							<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="new_password" required>
							@error('password')
    							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Confirm New  Password</label>
							<input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="new_cpassword" required>
							@error('password_confirmation')
    							<span class="text-danger">{{ $message }}</span>
							@enderror
						</div>
					</div><!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" id="update_oldpwd" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
@section('js_bottom')
 
<script>
$(document).on('click', '#update_oldpwd', function(){

	jQuery.validator.addMethod("pass", function (value, element) {
		if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {
			return true;
		} else {
			return false;
		};
	});
	
	var form = $("#update_pwd");
	form.validate({
		rules: {
			password: {
				required: true,
				minlength:8,
				pass:true,
			},
			Cpassword: {
				required: true,
				minlength:8,
				equalTo : "#new_password",
			}
		},
		messages: {
			password: {
				required:'Please enter password.',
				minlength:'Password must be at least 8 characters.',
				pass:"at least one number, one lowercase and one uppercase letter.",
			},
			Cpassword: {
				required:'Please enter confirm password.',
				minlength:'Password must be at least 8 characters.',
				equalTo:'confirm password and password should be same, please enter correct.'
			}
		}
	});
	var valid =	form.valid();
});
</script>
@stop