@extends('layouts.owner')

@section("other_css")

<!-- DATA TABLES -->

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<meta name="_token" content="{!! csrf_token() !!}" />

@stop


@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>
			Event Management List
		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Event Management List</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

		<div class="row">

			<div class="col-xs-12">

				<div class="box">

					<div class="box-header">

						<div style="float:right; margin-right:10px; margin-top:10px;">

							<a href="{{url('merchant/event-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New </a>

						</div>

					</div><!-- /.box-header -->

				</div><!-- /.box -->

				<div>

					<section>

						<div class="row">

							<div class="my-panel-data">

								<div class="col-xs-12">

									<div class="box">

										<div class="box-body table-responsive">

											@if(Session::has('message'))

											<div class="alert alert-success alert-dismissable">

												<i class="fa fa-check"></i>

												<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

												{{Session::get('message')}}

											</div>

											@endif

											@if(Session::has('message_error'))

											<div class="alert alert-danger alert-dismissable">

												<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button> {{Session::get('message_error')}}

											</div>

											@endif

											<div id="restaurant_list">

												<table id="myTable11" class="table table-bordered table-hover" data-order='[[ 0, "desc" ]]'>

													<thead>

														<tr>

															<th>ID </th>

															<th>Event Name</th>

															<th>Event Date</th>

															<th>Event Start Time</th>

															<th>Event End Time</th>

															<th>Inv Qty</th>

															<th>Ticket Price</th>

															<th>Status</th>

															<!-- <th>Create Date</th> -->

															<th>Action</th>

														</tr>

													</thead>

													<tbody>

														<?php $i = 1; ?>

														@if(!empty($rest_list))

														@foreach ($rest_list as $list)

														<tr>

															<td>{{ $list->id }}</td>

															<td>{{ $list->name}}</td>

															<td>{{ date('Y-m-d',strtotime($list->event_date))}}</td>

															<td>{{ $list->event_start_time }}</td>

															<td>{{ $list->event_end_time }}</td>

															<td>{{ $list->quantity}}</td>

															<td>{{ $list->price}}</td>

															<td>

																@if($list->status=='1')

																<span class="label label-primary">PUBLISHED</span>

																@else

																<span class="label label-danger">UNPUBLISHED</span>

																@endif

															</td>

															<!-- <td>{{ $list->created_at}}</td> -->

															<td>

																<a title="Edit" href="{{url('merchant/event-edit/')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a>

																&nbsp;&nbsp;

																<!-- <a title="View" href="#"><i class="fa fa-eye"></i></a> -->

																&nbsp;&nbsp;

																<a title="Delete Event" href="{{url('merchant/event-service-delete/')}}/{{ $list->id }}"><i class="fa fa-trash-o"></i></a>

															</td>

														</tr>

														<?php $i++; ?>

														@endforeach

														@endif

													</tbody>

												</table>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="col-12 mt-5 text-center">
							<div class="custom-pagination">
								
							</div>
						</div>

					</section>

				</div>

			</div>

		</div>

	</section><!-- /.content -->

</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>
	#ajax_favorite_loddder {


		position: fixed;

		top: 0;

		left: 0;

		width: 100%;

		height: 100%;

		background: rgba(27, 26, 26, 0.48);

		z-index: 1001;

	}

	#ajax_favorite_loddder img {

		top: 50%;

		left: 46.5%;

		position: absolute;

	}


	.footer-wrapper {

		float: left;

		width: 100%;

		/*display: none;*/

	}

	#addons-modal.modal {

		z-index: 999;

	}

	.modal-backdrop {
		z-index: 998 !important;

	}
</style>


<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>


<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script type="text/javascript">
	// $(function() {

	// 	$("#example1").dataTable();

	// 	$('#example2').dataTable({

	// 		"bPaginate": true,

	// 		"bLengthChange": false,

	// 		"bFilter": true,

	// 		"bSort": true,

	// 		"bInfo": true,

	// 		"bAutoWidth": false

	// 	});

	// });


	function check_frm()

	{

		$('#error_msg').hide();

		var form = $("#search_frm");

		if (($("#rest_cont").val() != '') || ($("#rest_name").val() != ''))

		{

			$("#ajax_favorite_loddder").show();

			var frm_val = $('#search_frm').serialize();

			$.ajax({

				type: "POST",

				url: "{{url('/vendor/restaurant_search')}}",

				data: frm_val,

				success: function(msg) {

					$("#ajax_favorite_loddder").hide();

					$('#restaurant_list').html(msg);

				}

			});

		} else

		{
		
			$('#error_msg').html('Please insert any one value.');

			$('#error_msg').show();

			return false;

		}

	}





	$.ajaxSetup({

		headers: {
			'X-CSRF-Token': $('meta[name=_token]').attr('content')
		}

	});
</script>


 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script

@stop