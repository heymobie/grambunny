@extends('layouts.owner')

@section("other_css")
        <!-- DATA TABLES -->
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Service Management
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ route('merchant.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Service List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Service Management</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">

										<a href="{{route('merchant.serviceCreate')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Service</a>

									
										</div>
									
                                </div><!-- /.box-header -->

                            </div><!-- /.box -->
							<div>
							<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">								 
										 
										    <div class="box">
												<div class="box-body table-responsive">
												
												 @if(Session::has('message'))
												 
												 <div class="alert alert-success alert-dismissable">
													  <i class="fa fa-check"></i>
													   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
																   {{Session::get('message')}}
												 </div>
												@endif
												
												 @if(Session::has('message_error'))
													 
													 <div class="alert alert-danger alert-dismissable">
														   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>		   {{Session::get('message_error')}}
													 </div>
												@endif
					
					
					
													<div id="restaurant_list">							
														 <table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>Sr.No.</th>
			<th>Image</th>
			<th>Title</th>
			<th>Code</th>
			<th>Category</th>
			<th>Staus</th>
			<th>Create Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	@if(!empty($rest_list)) 
	 @foreach ($rest_list as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>

		@if(!empty($list->image))	
	   <img src="{{asset("public/uploads/product/".$list->image)}}" width="50px;" height="50px;">
		@else
			<img src="{{ url('/') }}/public/uploads/product/product.jpg" width="50px;" height="50px;">
		@endif	
		
		</td>

		   <?php  $cat_name = DB::table('product_service_category')		
						->where('id', '=' ,$list->category_id)
						->value('category'); ?>	

			<td>{{ $list->name}}</td>
			<td>{{ $list->product_code}}</td>
			<td>{{ $cat_name}}</td>
			<td>
			@if($list->status=='1') 
			<span class="label label-primary">PUBLISHED</span>
			 @else
			
			<span class="label label-danger">UNPUBLISHED</span>
			@endif
			</td>

			<td>{{ $list->created_at}}</td>
			 <td>

		<a title="Edit" href="{{url('merchant/service-form/')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a>  
		<!--<a title="View" href="{{url('merchant/product_view/')}}/{{ $list->id }}"><i class="fa fa-eye"></i></a>-->
		&nbsp;&nbsp;
		<a title="Delete Product" href="{{url('merchant/service_delete/')}}/{{ $list->id }}"><i class="fa fa-trash-o"></i></a>
					 
			</td>
		</tr> 										
	<?php $i++; ?>
	@endforeach	   
	@endif                                     
	</tbody>
	<tfoot>
		<tr>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>					
						  </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
			
function check_frm()
{
$('#error_msg').hide();

var form = $("#search_frm");
	
	if(($("#rest_cont").val()!='') || ($("#rest_name").val()!=''))
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/vendor/restaurant_search')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#restaurant_list').html(msg);
			}
		});
	}
	else
	{
		//alert('Please insert any one value');
		$('#error_msg').html('Please insert any one value.');
		$('#error_msg').show();
		return false;
	}		
}


  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>
@stop
