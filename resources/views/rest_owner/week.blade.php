<!DOCTYPE HTML>
<html>
<head>  

</head>
<body>
<div id="chartContainer" style="height: 300px; width: 100%;" id="content">
</div>
<!-- <button id="cmd">Generate PDF</button> -->
</body>
<script type="text/javascript">
window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Week Sale Report"
    },
    axisX:{
        title: "Date",
        gridThickness: 2
    },
    axisY: {
        title: "Sale"
    },
    data: [
    {        
        type: "area",
        dataPoints: [
            <?php foreach ($month  as $key => $value) { 
                $timestamp = strtotime($value->created_at);
                   $m = date("m", $timestamp);
                   $d = date("d", $timestamp);
                   $y = date("Y", $timestamp);
                ?>
                { x: new Date(<?php echo $y;?>, <?php echo $m-1;?>, <?php echo $d;?>), y: <?php echo $value->total; ?>},
            <?php }?>    
           

        ]
    }
    ]
});

    chart.render();
}
</script>
<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script type="text/javascript">
  
var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {   
    doc.fromHTML($('#content').html(), 15, 15, {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});

</script>
</html>