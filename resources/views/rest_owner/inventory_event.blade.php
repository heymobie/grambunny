@extends('layouts.owner')

@section("other_css")

        <!-- DATA TABLES -->

  <style type="text/css">
  
  input#product_qty {
    border: none;
    width: 20%;
}	

  </style>      

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

   <meta name="_token" content="{!! csrf_token() !!}"/>

@stop


@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->

            <aside class="right-side">

                <!-- Content Header (Page header) -->

                <section class="content-header">

                    <h1>

                        Event Inventory 

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="{{ url('/merchant/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li class="active">Event Inventory</li>

                    </ol>

                </section>



                <!-- Main content -->

                <section class="content">

                    <div class="row">

                        <div class="col-xs-12">

                            <!--<div class="box">

                                <div class="box-header">

                                    <h3 class="box-title">{{$cat_name}}</h3>

									<div style="float:right; margin-right:10px; margin-top:10px;">


										<a href="{{url('merchant/product-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New </a>

								
										</div>
									
                                </div>

                            </div>-->

							<div>

							<section>

							 	<div class="row">

									<div class="my-panel-data">

										 <div class="col-xs-12">								 
										    <div class="box">

												<div class="box-body table-responsive">

												

									  <div class="alert alert-success alert-dismissable" id="inventoryid" style="display: none;">

										<i class="fa fa-check"></i>

										<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

										Inventory Update Sucessfully!

										</div>


												

												 @if(Session::has('message_error'))

													 

													 <div class="alert alert-danger alert-dismissable">

														   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>		   {{Session::get('message_error')}}

													 </div>

												@endif

					
				<div id="restaurant_list">							

				<table id="myTable11" class="table table-bordered table-hover">

	<thead>

		<tr>

			<th>IDs</th>

			<th>Image</th>

			<th>Product Name</th>

			<th>Code</th>

			<th>Available</th>

			<th>Sold</th>

			<th>Total</th>

			<th>Action</th> 

		</tr>

	</thead>

	<tbody>										

	<?php $i=1; ?>

	@if(!empty($rest_list)) 

	 @foreach ($rest_list as $list)

		<tr>

			<td>{{ $i }}</td>

			<td>



		@if(!empty($list->image))	

	   <img src="{{ asset("public/uploads/product/$list->image")}}" width="50px;" height="50px;">

		@else

			<img src="{{ asset('public/uploads/product/product.jpg') }}" width="50px;" height="50px;">

		@endif	

		

		</td>

<?php $cat_name = DB::table('product_service_category')->where('id', '=' ,$list->category_id)->value('category'); ?>

		   <?php  $sub_cat_name = DB::table('product_service_sub_category')		

						->where('id', '=' ,$list->sub_category_id)

						->value('sub_category'); ?>	


<?php 

if($list->vendor_id!=0){ $username = DB::table('vendor')->where('vendor_id', '=' ,$list->vendor_id)->value('username'); }else{ $username = "Admin";	}

// $soldqty = DB::table('orders_ps')->where('ps_id', '=' ,$list->id)->sum('ps_qty');


$soldqty1=  DB::table('orders_ps')->where('ps_id', '=' ,$list->id)->sum('ps_qty');

$soldqty2 = DB::table('orders_ps')->where('ps_id', '=' ,$list->id)->where('cancel_status', 1)->sum('ps_qty');

$soldqty = $soldqty1-$soldqty2;

$totalqty = ($list->quantity)+$soldqty;

?>

			<td>{{ $list->name}}</td>

			<td>{{ $list->product_code}}</td>			

			<td><input type="text" value="<?php echo $list->quantity; ?>" name="product_qty" id="product_qty"></td>		

			<td><span id="soldqty">{{$soldqty}}</span></td>

            <td><span id="totalqty">{{$totalqty}}</span></td>

<td><a href="#" onclick="Inventory(<?php echo $list->id; ?>); return false;" class="btn btn-primary">Update</a></td>

		</tr> 										

	<?php $i++; ?>

	@endforeach	   

	@endif  


	@if(!empty($rest_list1)) 

	 @foreach ($rest_list1 as $list)

		<tr>

			<td>{{ $i }}</td>

			<td>

     <?php $getprodtls = DB::table('product_service')->where('id', '=' ,$list->product_id)->first(); ?>


		@if(!empty($getprodtls->image))	

	   <img src="{{ asset("public/uploads/product/$getprodtls->image")}}" width="50px;" height="50px;">

		@else

			<img src="{{ asset('public/uploads/product/product.jpg') }}" width="50px;" height="50px;">

		@endif	

		

		</td>



<?php 

if($list->vendor_id!=0){ $username = DB::table('vendor')->where('vendor_id', '=' ,$list->vendor_id)->value('username'); }else{ $username = "Admin";	}

$soldqty = DB::table('orders_ps')->where('ps_id', '=' ,$list->product_id)->sum('ps_qty');

$totalqty = ($list->product_quantity)+$soldqty;

?>

			<td>{{ $getprodtls->name}}</td>

			<td>{{ $getprodtls->product_code}}</td>			

			<td><input type="text" value="<?php echo $list->product_quantity; ?>" name="product_qty" id="product_qty"></td>		

			<td><span id="soldqty">{{$soldqty}}</span></td>

            <td><span id="totalqty">{{$totalqty}}</span></td>

			<td> 
           

	 <a href="#" onclick="Inventory(<?php echo $list->product_id; ?>); return false;" class="btn btn-primary">Update</a>  

					 
			</td>

		</tr> 										

	<?php $i++; ?>

	@endforeach	   

	@endif  

	</tbody>



</table>

													</div>

												</div>	

											</div>

										</div>

									</div>

								</div>

							</section>					

						  </div>

                        </div>

                    </div>



                </section><!-- /.content -->

            </aside><!-- /.right-side -->



@stop





@section('js_bottom')

<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>
		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

        <script type="text/javascript">

            // $(function() {

            //     $("#example1").dataTable();

            //     $('#example2').dataTable({

            //         "bPaginate": true,

            //         "bLengthChange": false,

            //         "bFilter": true,

            //         "bSort": true,

            //         "bInfo": true,

            //         "bAutoWidth": false

            //     });

            // });


function Inventory(proId){

var proqty = $('#product_qty').val();	

      $.ajax({
      
      type: "post",
      
      headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
      
      data: {
      
              "_token": "{{ csrf_token() }}",
      
              "data": {productid:proId, productqty:proqty},
      
            },
      
      url: "{{url('/merchant/inventory-product-action')}}",       
           
      success: function(msg) { 

      $('#inventoryid').show();

      var soldqty = $('#soldqty').text();

      var totalqtynew = parseInt(proqty)+parseInt(soldqty);

      $('#totalqty').text(totalqtynew);
         
      }
      
   });

}			

function check_frm()

{

$('#error_msg').hide();



var form = $("#search_frm");

	

	if(($("#rest_cont").val()!='') || ($("#rest_name").val()!=''))

	{		

		 $("#ajax_favorite_loddder").show();	

		var frm_val = $('#search_frm').serialize();				

		$.ajax({

		type: "POST",

		url: "{{url('/vendor/restaurant_search')}}",

		data: frm_val,

			success: function(msg) {

			 $("#ajax_favorite_loddder").hide();	

			

				$('#restaurant_list').html(msg);

			}

		});

	}

	else

	{

		//alert('Please insert any one value');

		$('#error_msg').html('Please insert any one value.');

		$('#error_msg').show();

		return false;

	}		

}





  $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

});

 </script>

 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>

@stop

