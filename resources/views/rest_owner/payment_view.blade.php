@extends('layouts.owner')

@section("other_css")

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<style type="text/css">

.deepmd{ width: 100%; }

.deepmd strong{ float: right; }

.deepon{ font-size: 14px; } 

</style>

@stop

@section('content')

<aside class="right-side">

    <section class="content-header">

        <h1>Customer Payment Detail</h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/merchant/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Customer Payment Detail</li>

        </ol>

    </section>



    <div class="myOdrersDetailBox">

    	<div class="row">

            <div class="col-md-12">

                <div class="myOrder">

                    <div class="myOrderDtelBox">

                        <div class="myOrderID" style="float: none;">

                            <label>Order ID:</label>

                            <strong><?php if(isset($order_detail->order_id)){ ?>{{$order_detail->order_id}}<?php } ?></strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Order By</label>

                            <strong><?php if(isset($order_detail->first_name)){ ?>{{$order_detail->first_name}} {{$order_detail->last_name}}<?php } ?></strong>

                        </div>

                        <div class="myOrderDtel">

                            <label>Contact Number</label>

                            <strong><?php if(isset($order_detail->mobile_no)){ ?>{{$order_detail->mobile_no}}<?php } ?></strong>

                        </div>

                        <div class="myOrderDtel">

                            <label>Delivery Address</label>

                            <strong><?php if(isset($order_detail->address)){ ?>{{$order_detail->address}}<?php } ?></strong>

                        </div>


                         <div class="myOrderDtel">

                            <label>City</label>

                            <strong><?php if(isset($order_detail->city)){ ?>{{$order_detail->city}} &nbsp;<?php } ?></strong>

                        </div>

                         <div class="myOrderDtel">

                            <label>State</label>

                            <strong><?php if(isset($order_detail->state)){ ?>{{$order_detail->state}} &nbsp;<?php } ?></strong>

                        </div>


                        <div class="myOrderDtel">

                            <label>Zip Code</label>

                            <strong><?php if(isset($order_detail->zip)){ ?>{{$order_detail->zip}} &nbsp;<?php } ?></strong>

                        </div>


                        <div class="myOrderDtel">

                            <label>Email</label>

                            <strong><?php if(isset($order_detail->email)){ ?>{{$order_detail->email}} &nbsp;<?php } ?></strong>

                        </div>


                        <div class="myOrderDtel">

                            <label>Order Status</label>

                            <strong><span class="green">

                            @if($order_detail->status==0) Pending @endif

                            @if($order_detail->status==1) Accept @endif

                            @if($order_detail->status==2) Cancelled @endif

                            @if($order_detail->status==3) On the way @endif

                            @if($order_detail->status==4) Complete @endif

                            </span></strong>

                        </div>





                        <div class="myOrderDtel">

                            <label>Subtotal</label>

                            <strong>${{ number_format($order_detail->sub_total, 2) }}</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Discount</label>

                            <strong>${{ number_format($order_detail->promo_amount, 2) }}</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Service Tax</label>

                            <strong>${{ number_format($order_detail->service_tax, 2)}}</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Total Paid Amount</label>

                            <strong>${{ number_format($order_detail->total, 2) }}</strong>

                        </div>

                        <div class="myOrderDtel">

                            <label>Payment Type</label>

                            <strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong>

                        </div>

                        <div class="myOrderDtel">

                            <label>Transaction ID</label>

                            <strong>{{$order_detail->txn_id}}</strong>

                        </div>

                        <?php $cardstatus = DB::table('users_card')->where('order_id',$order_detail->order_id)->where('user_id',$order_detail->user_id)->value('transaction_type'); ?>

                        <div class="myOrderDtel">

                            <label>Payment Status</label>

                            <strong><?php if($cardstatus=='05'){ echo "Pre-Authorization"; }else{ ?> {{$order_detail->pay_status}} <?php } ?></strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Placed at</label>

                            <strong>{{$order_detail->created_at}}</strong>

                        </div>



                        <!--<div class="myOrderDtel">

                        <label>Comment</label>

                        <strong>{{$order_detail->instruction}}</strong>

                        </div>-->



                        <!--<div class="myOrderActnBox">

                            <div class="myOrderDtelBtn">

                                <a href="{{$order_detail->receipt_url}}" target="_blank">View Reciept</a>

                            </div>

                        </div>-->

                    </div>

                </div>

            </div>



    	</div>

    </div>

</aside>

@stop

@section('js_bottom')

<style>

	#ajax_favorite_loddder {

		position: fixed;

		top: 0;

		left: 0;

		width: 100%;

		height: 100%;

		background:rgba(27, 26, 26, 0.48);

		z-index: 1001;

	}

	#ajax_favorite_loddder img {

		top:50%;

		left:46.5%;

		position:absolute;

	}

	.footer-wrapper {

	    float:left;

	    width:100%;

	}

	#addons-modal.modal {

		z-index:999;

	}

	.modal-backdrop {	

		z-index:998 !important;

	}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>        

<script type="text/javascript">

	function check_frm(){

		$('#error_msg').hide();

		var form = $("#search_frm");

		form.validate();

		var valid =	form.valid();

		if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))

		{		

			 $("#ajax_favorite_loddder").show();	

			var frm_val = $('#search_frm').serialize();				

			$.ajax({

			type: "POST",

			url: "{{url('/admin/vendor_search')}}",

			data: frm_val,

				success: function(msg) {

				 $("#ajax_favorite_loddder").hide();	

				

					$('#vender_search_list').html(msg);

				}

			});

		}

		else

		{

			//alert('Please insert any one value');

			

			$('#error_msg').html('Please insert any one value');

			$('#error_msg').show();

			return false;

		}		

	}

	$(function() {

		$("#example1").dataTable();

		$('#example2').dataTable({

			"bPaginate": true,

			"bLengthChange": false,

			"bFilter": true,

			"bSort": true,

			"bInfo": true,

			"bAutoWidth": false

		});

	});

</script>

@stop





