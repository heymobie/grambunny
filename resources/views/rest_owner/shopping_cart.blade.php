@extends('layouts.owner')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>Shopping Cart<small>Control Panel</small>

	</h1>
  
		<ol class="breadcrumb">

			<li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Dashboard</li>

		</ol>

	</section>

	<!-- Main content -->

	@if(Session::has('message'))

	<div class="alert alert-success alert-dismissable">
	<i class="fa fa-check"></i>
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
	{{Session::get('message')}}
	</div>
	
	@endif


<section class="content">

<div id="container" class="inner-box-de">

<?php  if(!empty($cartstatus)){ $cstatus = $cartstatus->status ;}else{ $cstatus = '1'; } ?>

<div class="col-md-12">

<form  role="form" method="POST" id="shopping_frm" action="{{ url('/merchant/shop_action') }}">    

	{!! csrf_field() !!}

        <div class="box-body">
            <div class="form-group">

            <label for="exampleInputEmail1">Status</label>

			<select name="cart_status" id="cart_status"  class="form-control">

			<option value="1" @if($cstatus==1) selected="selected" @endif>ON</option>

			<option value="0" @if($cstatus==0) selected="selected"@endif>OFF</option>

			</select>	

            </div>

        </div> <!-- /.box-body -->
        <div class="box-footer">		
		<input type="submit" class="btn btn-primary" value="Submit" />

		<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

        </div>

</form>

</div>

</div>

<!--<div id="container_transport" style="min-width: 310px; height: 400px; margin: 0 auto"></div>-->

	</section><!-- /.content -->

</aside><!-- /.right-side -->

@endsection

@section('js_bottom')

<!-- jQuery 2.0.2 -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<!-- jQuery UI 1.10.3 -->

<script src="{{ url('/') }}/public/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- Bootstrap -->

<!-- <script src="{{ url('/') }}/public/design/admin/js/bootstrap.min.js" type="text/javascript"></script>   -->     

<!-- Bootstrap WYSIHTML5 -->

<script src="{{ url('/') }}/public/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>


<!-- AdminLTE App -->

<script src="{{ url('/') }}/public/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="{{ url('/') }}/public/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="/css/result-light.css">

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

$(document).ready(function(){

  $("#myInput").on("keyup", function() {

    var value = $(this).val().toLowerCase();

    $("#myTable tr").filter(function() {

      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

    });

  });

});

</script>


@stop