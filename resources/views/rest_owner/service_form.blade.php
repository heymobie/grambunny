@extends('layouts.owner')
<style>
.ui-widget-content {
    max-height: 221px;
    overflow-y: scroll;
}
.ui-menu .ui-menu-item {
    padding: 5px;
}
.ui-menu-item:nth-child(2n) {
    background-color: #f1f1f1;
}
</style>
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Add Service  
			<small>Control Panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ route('merchant.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Add Service </li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary"> 
                                <div class="box-header">
                                    <h3 class="box-title">Add Service</h3>
                                </div><!-- /.box-header -->

                    @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
						  <i class="fa fa-check"></i>
						  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						  {{Session::get('message')}}
					 </div>
					@endif               
											
					 @if(Session::has('message_error'))
					 
					 <div class="alert alert-danger alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message_error')}}
                     </div>
					@endif
                                <!-- form start -->

                                <?php if($id>0){ $catid = $rest_detail[0]->category_id;}else{ $catid = 0; } ?>
                                 <?php if($id>0){ $subcatid = $rest_detail[0]->sub_category_id;}else{ $subcatid = 0; } ?>
								
								<form  role="form" method="POST" id="rest_frm" action="{{ url('/merchant/service-action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="product_id" value="{{$id}}" />
								<input type="hidden" name="ps_type" value="1" />
								<input type="hidden" name="product_old_img" id="product_old_img" value="@if($id>0){{$rest_detail[0]->image}} @endif" />
							
								
								{!! csrf_field() !!}
                                    <div class="box-body">

                                    <div class="form-group">

                                    <label for="exampleInputEmail1">Category</label>
                                    <select class="form-control" name="category_id" id="category_id">
                                    <?php  foreach ($proservice_cat as $key => $value) { ?>

                                    <option value="{{$value->id}}"<?php if($value->id == $catid ){ echo "selected"; }?>>{{$value->category}}</option>

                                    <?php } ?>	
                                   	
                                    </select>
                                   
                                    </div>	

                                  <div class="form-group">

                                    <label for="exampleInputEmail1">Sub Category</label>
                                    <select class="form-control" name="sub_category_id" id="sub_category_id">
                                    <?php  foreach ($proservice_sub_cat as $key => $value) { ?>

                                    <option value="{{$value->id}}"<?php if($value->id == $subcatid ){ echo "selected"; }?>>{{$value->sub_category}}</option>

                                    <?php } ?>	
                                   	
                                    </select>
                                   
                                    </div>	


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                           <input type="text" class="form-control" name="product_name" id="product_name" value="@if($id>0){{$rest_detail[0]->name}}@endif" required="required">
                                        </div>

										
										<div class="form-group">
                                        <label for="exampleInputEmail1">Description</label>
										<textarea  class="form-control" name="product_desc" id="product_desc" required="required" style="height: 200px;">@if($id>0){{$rest_detail[0]->description}}@endif</textarea>																
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Price</label>
                                           <input type="text" class="form-control" name="product_price" id="product_price" value="@if($id>0){{$rest_detail[0]->price}}@endif" required="required">
                                        </div>

                                    <div class="form-group">
                                    <label for="exampleInputEmail1">Unit</label>															
									<select name="unit" id="unit" class="form-control">

                                    <?php  foreach ($product_unit as $key => $value) { ?>

                                   <option value="{{$value->unit_symbol}}" @if(($id>0)&& ($rest_detail[0]->unit==$value->unit_symbol)) selected="selected" @endif>{{$value->unit_name}}</option>

                                    <?php } ?>		

											</select>
                                        </div>

              
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Stock</label>															
											<select name="stock" id="stock" class="form-control">
											<option value="1" @if(($id>0)&& ($rest_detail[0]->stock=='1')) selected="selected" @endif>In stock</option>

											<option value="0" @if(($id>0)&& ($rest_detail[0]->stock=='0')) selected="selected" @endif>Out of stock</option>
											</select>
                                        </div>
                                       
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>															
											<select name="status" id="status" class="form-control">
											<option value="1" @if(($id>0)&& ($rest_detail[0]->status=='1')) selected="selected" @endif>PUBLISHED</option>

											<option value="0" @if(($id>0)&& ($rest_detail[0]->status=='0')) selected="selected" @endif>UNPUBLISHED</option>
											</select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Thumb Image (File Type: jpeg,gif,png)</label>
                                            <input type="file" class="form-control" name="product_image" id="product_image">		
											
											@if(($id>0) && (!empty($rest_detail[0]->image)))
											
											<img src="{{asset("public/uploads/product/".$rest_detail[0]->image)}}" width="50px;" height="50px;">
											@endif	

										@if($errors->has("product_image"))
                                       <span class="text-danger">{{ $errors->first("product_image") }}</span>
                                       @endif

                                        </div>



                                       <div class="form-group">
                                            <label for="exampleInputEmail1">Gallery Image (File Type: jpeg,gif,png)</label>
                                            <input type="file" class="form-control" name="product_image_gallery[]" id="product_image_gallery" multiple>

											@if(($id>0) && (!empty($glimage)))

											<?php  foreach ($glimage as $key => $value) { ?>

											<span class="img-gl"><img src="{{ url('/') }}/public/uploads/product/{{ $value->name }}" width="50px;" height="50px;"></span>

										 <a href="{{ url('/') }}/merchant/serviceimage/{{ $value->id }}"><i class="fa fa-trash-o" style="color:red"></i></a>	
											
											<?php } ?>

											@endif	

										@if($errors->has("product_image_gallery"))
                                       <span class="text-danger">{{ $errors->first("product_image_gallery") }}</span>
                                       @endif

                                        </div> 
										
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
										<input type="submit" class="btn btn-primary" value="Submit" />
									<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->

                        </div> 
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->

@endsection

@section('js_bottom')

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">

$('#category_id').on('change', function() {
 
var cateId = $(this).val();

//$("#ajax_favorite_loddder").show();	

var frm_val = "cateid="+cateId;

$.ajax({
type: "post",
headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
url: "{{url('/merchant/sub-category')}}",
data: frm_val,
success: function(msg) {

//$("#ajax_favorite_loddder").hide();	

$('#sub_category_id').html(msg);

//$("#addons-modal").modal('show');

}
});

});	

 
</script>
	
@stop