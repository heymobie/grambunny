<style type="text/css">
  .methodbw{
    text-transform: capitalize;
  }
</style>
@extends('layouts.owner')

@section('content')

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Right side column. Contains the navbar and content of the page -->
<link href="{{ url('/') }}/design/css/easy-responsive-tabs.css" rel="stylesheet">
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Bank Account
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/merchant/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Bank Account</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary dobbel-box">
    
								
								 @if(Session::has('message'))
					 
								 <div class="alert alert-success alert-dismissable">
									  <i class="fa fa-check"></i>
									   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
												   {{Session::get('message')}}
								 </div>
								@endif
                                <!-- form start -->
				
				      <div class="col-md-12">
      
      <div class="in_wal">
        <div id="horizontalTab">
  

      <div class="dwallet">

  <div class="box box-primary" style="box-shadow: unset !important;">
     

      <p style="float: left;text-align: center;width: 100%;">
         </p>
      <!-- form start -->
        <form name="banksetup" id="banksetup" method="post" action="{{ url('/') }}/merchant/bank-setup">

          {!! csrf_field() !!}
        
          @if($vid!=0)
          <div class="box-body">
            <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInputEmail1">Account Holder Name * </label>
              <input type="text" class="form-control" name="holder_name" value="{{$vendor_bank_detail[0]->holder_name}}" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Bank Name * </label>
             <input type="text" class="form-control" name="bank_name" value="{{$vendor_bank_detail[0]->bank_name}}" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Branch * </label>
             <input type="text" class="form-control" name="branch" value="{{$vendor_bank_detail[0]->branch}}" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Account Number * </label>
             <input type="text" class="form-control" name="account_number" value="{{$vendor_bank_detail[0]->account_number}}" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">IFSC Code * </label>
             <input type="text" class="form-control" name="ifsc_code" value="{{$vendor_bank_detail[0]->ifsc}}" required="required" placeholder="">
            </div>
          </div>
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Update">
              <input type="button" class="btn btn-primary" value="Go Back" onclick="history.go(-1);">
            </div>
          </div>
          @else
          <div class="box-body">
            <div class="col-md-4">
            <div class="form-group">
              <label for="exampleInputEmail1">Account Holder Name * </label>
              <input type="text" class="form-control" name="holder_name" value="" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Bank Name * </label>
             <input type="text" class="form-control" name="bank_name" value="" required="required" placeholder="">
            </div>
          </div>
           <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Branch * </label>
             <input type="text" class="form-control" name="branch" value="" required="required" placeholder="">
            </div>
          </div>
           <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">Account Number * </label>
             <input type="text" class="form-control" name="account_number" value="" required="required" placeholder="">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
             <label for="exampleInputEmail1">IFSC Code * </label>
             <input type="text" class="form-control" name="ifsc_code" value="" required="required" placeholder="">
            </div>
          </div>
            <div class="box-footer">
              <input type="submit" class="btn btn-primary" value="Submit">
              <input type="button" class="btn btn-primary" value="Go Back" onclick="history.go(-1);">
            </div>
          </div>
          @endif

        </form>
    </div>
    
            </div>
          </div>

          <div>
 
          </div>
            
          </div>
        </div>
      </div>
      </div>				

								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ui-widget-content {
    max-height: 221px;
    overflow-y: scroll;
}
.ui-menu .ui-menu-item {
    padding: 5px;
}
.ui-menu-item:nth-child(2n) {
    background-color: #f1f1f1;
}
</style>
        <script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script>
        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
<script>
		/*$(document).on('keyup', '#suburb', function(){
	 $( "#suburb" ).autocomplete({
    	 source: "{{url('/vendor/get_suburblist') }}",
		 select: function(event, ui) {
		 
		 	$("#suburb").val(ui.item.value);	
			$("#zipcode").val(ui.item.pincode);	
			$("#state").val(ui.item.state);	
		 }
    });

});*/
$(function() {
  $('#payment_received').dataTable({
      "bPaginate": true,
      "bLengthChange": false,
      "bFilter": true,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false
  });
)};
		function check_email()
		{
		
					$('#error_msg').hide();
		var form = $("#user_frm");
		form.validate();
			var valid =	form.valid();
			if(valid){	
			
				/*var regExp = /^0[0-9].*$/;
				if(regExp.test($("#mob_no").val()))
				{
					$(form).submit();
					return true;*/
					
					var frm_val = $('#user_frm').serialize();				
					$.ajax({
					type: "POST",
					url: "./check_vendor_duplicateemail",
					data: frm_val,
						success: function(msg) {
							if(msg=='1')
							{
								$('#email_msg').show();
								return false;
							}
							else if(msg=='2')
							{
								$(form).submit();
								return true;					
							}
						}
					});
				
				/*}
				else
				{
					//alert('Contact number start with 0.');
					$('#error_msg').html('Contact number start with 0.');
					$('#error_msg').show();
					return false;
				}*/
				
			
				
					
				
				
				
			}
			else
			{
				return false;
			}		
		}
		function check_number()
		{
		
					$('#error_msg').hide();
			var form = $("#user_frm");
			form.validate();
			var valid =	form.valid();
			if(valid){	
			
				/*var regExp = /^0[0-9].*$/;
				if(regExp.test($("#mob_no").val()))
				{*/
					$(form).submit();
					return true;
				/*}
				else
				{
					//alert('Contact number start with 0.');
					$('#error_msg').html('Contact number start with 0.');
					$('#error_msg').show();
					return false;
				}*/
				
			}
			else
			{
				return false;
			}		
		}
		</script>
		
		<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
			
			//alert(val);
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
		
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>

<script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script> 

@stop