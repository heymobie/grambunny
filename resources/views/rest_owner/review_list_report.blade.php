@extends('layouts.owner')

@section("other_css")
        <!-- DATA TABLES -->
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style>
  .popup_divcall{
	  margin: 10px;  
  }
  .checked {
  color: orange;
}
  </style>
   <meta name="_token" content="{!! csrf_token() !!}"/>

@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Review & Rating Reports
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/vendor/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Review & Rating Reports</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
					 @if(Session::has('message'))
					 
					 <div class="alert alert-success alert-dismissable">
                          <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message')}}
                     </div>
					@endif
					<div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Review & Rating Reports Management</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
										</div>
									
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
									
									<form name="search_frm" id="search_frm" method="post" >
									
													{!! csrf_field() !!}
													<div>
													<table cellpadding="5" cellspacing="5" width="100%" style="border:1px solid #999999" class="table">
													<tr>
														<td width="50%">
														<table cellpadding="5" cellspacing="5" width="100%">
												
														<tr>
														<td class="marginRight"> Status: </td> 
														<td>
															<select name="order_status" id="order_status"class="form-control">
																<option value="All">All Time </option>
																<option value="daily" >Daily</option>
																<option value="weekly">Weekly</option>
																<option value="monthly">Monthly</option>
															</select>
														</td> 
														</tr>
                                                        <tr>
                                                        <!--<td>Items :</td> 
                                                        <td>
                                                            <select name="order_item" id="order_item" class="form-control">

                                                            <option value="">Select Item</option>   

                                                            <?php  foreach ($item_details as $key => $value) { ?>

                                                            <option value="<?php echo $value->menu_category_name ;?>"><?php echo $value->menu_category_name ;?></option>

                                                              <?php } ?>
                                                                                 
                                                            </select>

                                                        </td>-->
                                                        </tr>
														</table>
														</td>
														<td width="50%">
														<table cellpadding="5" cellspacing="5" width="100%">
															
														<tr>
														<td class="marginRight">From Date: </td>
														<td>
															<input type="text" name="fromdate" id="fromdate" value=""  class="form-control datepicker"/>
														</td> 
														</tr>
														<tr>
														<td class="marginRight">To Date: </td>
														<td>
															<input type="text" name="todate" id="todate" value="" class="form-control datepicker"/>
															
														 <div id="error_msg" style="color:#FF0000; display:none;"></div>
														</td> 
														</tr>
														
														</table>
														</td>
														</tr>
														
														<tr>
															<td colspan="2">
														
														<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" />
														
														</td>
														</tr>
													</table>
													</div>
													</form>
								
									
								
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

							<div>

							<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">
										    <div class="box">
												<div class="box-body table-responsive">
												
												<h3 class="box-title">Customers Review &amp; Rating</h3>
												   
													<div id="restaurant_list">							
														 <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>    
												<th>Restaurant Name</th>
												<th>User Name</th>
                                                <th>Restaurant Rating </th>
                                                <th>Delivery Rating </th>
                                                <th>Food Rating </th>
                                                <th>Feedback</th>
                                                <!--<th>Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; 
									 	if($review_detail){ ?>

										<?php foreach($review_detail as $list){ ?>

                                            <tr>
                                                <td>{{ $i }}</td>
                                                
                                                <td>{{ $list->rest_name }}</td>	
                                                 <td>{{ $list->order_fname }}</td>	
                                               									
                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_rating){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_delivery_ontime){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_food_good){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td> {{ $list->re_content }} </td>
                               
                                            </tr> 	

									 <?php $i++; }  ?>
								  
										<?php }?>                                    
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
                                                <th>&nbsp;</th> 
                                                

                                            </tr>
                                        </tfoot>
                                    </table>	
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>									


							<!--<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">
										    <div class="box">
												<div class="box-body table-responsive">
												
												<h3 class="box-title">Average Rating</h3>
												   
													<div id="restaurant_list">							
														 <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
												<th>Restaurant Name</th>
                                                <th>Restaurant Rating </th>
                                                <th>Delivery Rating </th>
                                                <th>Food Rating </th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; $restnewid='';

                                     //print_r($review_detail);

									 if($review_detail){ ?>

									 <?php foreach($review_detail as $list){ ?>

                                      <?php 

										 $restaid = $list->rest_id; 
										
										 if($restaid != $restnewid){
										  
										  $restnewid = $restaid;

						$re_rating  = DB::table('review')->where('re_restid', '=' ,$restaid)->where('re_status', '=' , 'PUBLISHED')->avg('re_rating');	
						$re_ratings = round($re_rating);

					   $re_food_good  = DB::table('review')->where('re_restid', '=' ,$restaid)->where('re_status', '=' , 'PUBLISHED')->avg('re_food_good');	
						$re_food_goods = round($re_food_good);

						$re_delivery_ontime  = DB::table('review')->where('re_restid', '=' ,$restaid)->where('re_status', '=' , 'PUBLISHED')->avg('re_delivery_ontime');	
						$re_delivery_ontimes = round($re_delivery_ontime);

										?>

                                            <tr>
                                                
                                                <td>{{ $list->rest_name }}</td>	
                                               									
                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$re_ratings){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$re_delivery_ontimes){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$re_food_goods){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>
                               
                                            </tr> 

                                            <?php  } ?>	

									 <?php $i++; }  ?>
								  
										<?php }?>                                    
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>

                                            </tr>
                                        </tfoot>
                                    </table>	
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>-->


						  </div>


                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop

@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
		   <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
		
		
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });				
            });
			  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
		 dateFormat: 'yy-mm-dd',
		 autoclose: true
		});
  });	
$(document).on('change', '#revew_status', function(){
 var value = $(this).find('option:selected').val();
 var aat_val = $(this).attr('data-reid');		
	
	if(value=='REJECT')	
	{		
		 $("#Reason_reject"+aat_val).show();	
	}
	else
	{	
		 $("#Reason_reject"+aat_val).hide();	
	}
 }); 			
		
		  
$(document).on('click', '#search_btn', function(){ 
	
	  
	 $('#error_msg').hide();
	 
var frmdate = $('#fromdate').val(); 
var todate = $('#todate').val(); 
	
	if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
	{//compare end <=, not >=
		//your code here
		//alert("From date will be big from to date!");
		
		
		$('#error_msg').html('From date will be big from to date!');
		$('#error_msg').show()
	}
	else
	{	
		
	 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/vendor/review_search_list_report')}}",
		data: frm_val,
		success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			//alert(msg)
				$('#restaurant_list').html(msg);
			}
		});
	}		
}); 		
			

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});			
        </script>
@stop
