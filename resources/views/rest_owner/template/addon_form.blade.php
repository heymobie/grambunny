<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{$template_name}}->{{$menu_name}}->{{$item_name}}->Addon Form</h4>
</div>
	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"></h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
									
								<form  role="form" method="POST" id="addon_frm"  enctype="multipart/form-data">    
								<input type="hidden" name="addon_id" id="addon_id" value="{{$id}}" />
								<input type="hidden" name="addon_templateid" value="{{$template_id}}" />
								<input type="hidden" name="addon_menuid" value="{{$menu_id}}" />
								<input type="hidden" name="addon_menucatid" value="{{$menu_item}}" />
								{!! csrf_field() !!}
                                    <div class="box-body">										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">GroupName</label>
                                           <input type="text" class="form-control" name="addon_groupname" id="addon_groupname" value="@if($id>0){{$addon_detail[0]->addon_groupname}} @endif" required="required">
                                        </div>		
												
																		
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Selection Option</label>
											
											
											<select name="addon_option" id="addon_option" class="form-control">
												<option value="radio" @if(($id>0) && ($addon_detail[0]->addon_option=='radio')) selected="selected" @endif>Radio Button </option>			
												<option value="check" @if(($id>0) && ($addon_detail[0]->addon_option=='check')) selected="selected" @endif>Tick Box</option>						
											</select>
                                        </div> 
																		
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                           <input type="text" class="form-control" name="addon_name" id="addon_name" value="@if($id>0){{$addon_detail[0]->addon_name}}@endif" required="required">
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Price</label>
											 <input type="text" class="form-control" name="addon_price" id="addon_price" value="@if($id>0){{$addon_detail[0]->addon_price}}@endif" required="required" number="number">													
                                        </div>
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>	
											<select name="addon_status" id="addon_status" class="form-control">
												<option value="1" @if(($id>0) && ($addon_detail[0]->addon_status==1)) selected="selected" @endif>Active</option>			
												<option value="0" @if(($id>0) && ($addon_detail[0]->addon_status==0)) selected="selected" @endif>Inactive</option>										
											</select>
                                        </div>
										
															
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Show as default Addon</label>
											   
                                           <input type="checkbox" name="addon_default" id="addon_default" value="1"  @if(($id>0) && ($addon_detail[0]->addon_default==1)) checked="checked" @endif  >
                                        </div>
										
										
                                       
                                      
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									@if($id>0)
									<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									@else
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									@endif
									
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>	
	</section><!-- /.content -->

		<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>


function check_frm(tpy)
{

var form = $("#addon_frm");
		form.validate();
	var valid =	form.valid();
	if(valid)
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = 'from='+tpy+'&'+$('#addon_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/template_addon_action')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#addons_modal_data').html(msg);
			}
		});
	}
	else
	{
		return false;
	}		
}
</script>

