<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{$template_name}}->{{$menu_name}}->{{$item_name}}->Addon Listing</h4>
</div>

<div class="box">
<div class="box-header">
	
</div><!-- /.box-header -->
<div class="box-body table-responsive">
	<table id="example2" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>SrNo</th>
				<th>Group Name</th>
				<th>Selection Option</th>
				<th>Name</th>
				<th>Price</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		@if($addon_list)<?php $c=1;?>
			@foreach($addon_list as $list )
			<tr>
				<td>{{$c++}}</td>
				<td>{{$list->addon_groupname}}</td>
				<td>@if($list->addon_option=='check') 
						Tick Box
					@else 
						Radio Button
					@endif
				</td>
				<td>{{$list->addon_name}}</td>
				<td>{{$list->addon_price}}</td>
				<td>
					@if($list->addon_status==1) 
						<span class="label label-success">Active</span>
					@else 
						<span class="label label-danger">Inactive</span>
					@endif
				</td>
			</tr>
		  @endforeach
		@endif  		                                     
		</tbody>
		<tfoot>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</tfoot>
	</table>
</div><!-- /.box-body -->
</div><!-- /.box -->
