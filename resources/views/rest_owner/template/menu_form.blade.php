
	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Template Menu Form</h3>
                                </div><!-- /.box-header -->
							
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_frm" action="#" enctype="multipart/form-data">    
								<input type="hidden" name="template_id" value="{{$template_id}}" />
								<input type="hidden" name="menu_id" value="{{$id}}" />
							<input type="hidden" name="menu_image_old" value="@if($id>0){{$menu_detail[0]->menu_image}}@endif"/>
							
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Menu name</label>
                                           <input type="text" class="form-control" name="menu_name" id="menu_name" value="@if($id>0){{$menu_detail[0]->menu_name}}@endif" required="required">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="menu_desc" id="menu_desc">@if($id>0){{$menu_detail[0]->menu_desc}}@endif</textarea>																
                                        </div>
										
										 
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Upload Image (File Type: jpeg,gif,png)</label>
                                            <input type="file" class="form-control" name="menu_image" id="menu_image">		
											
											@if(($id>0) && (!empty($menu_detail[0]->menu_image)))
											
											<img src="{{ url('/') }}/uploads/menu/{{ $menu_detail[0]->menu_image}}" width="50px;" height="50px;">
											@endif														
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
																								
											<select name="menu_status" id="menu_status" class="form-control">
											<option value="1" @if(($id>0)&& ($menu_detail[0]->menu_status=='1')) selected="selected" @endif>Active</option>
											<option value="0" @if(($id>0)&& ($menu_detail[0]->menu_status=='0')) selected="selected" @endif>Inactive</option>
											</select>															
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									
									@if($id>0)
									<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@else
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@endif
									
										
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->


		
		<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script>
		function check_frm(tpy)
		{
		alert('menu_form');
		
			if(tpy=='back')
			{
				var	valid = true;
			}
			else
			{		
					
				var form = $("#rest_frm");
					form.validate();
				var valid =	form.valid();
			}		
			
			if(valid)
			{		
				 $("#ajax_favorite_loddder").show();	
				/*var frm_val = 'from='+tpy+'&'+$("#rest_frm")[0];	
				//var frm_val = new FormData($("#rest_frm")[0])+'&from='+tpy;	
				
				var frm_val1 = new FormData($("#rest_frm")[0]);	*/	
				
				var frm_val1 =$('#rest_frm').serialize();
				var frm_val = 'from='+tpy+'&'+frm_val1;	
				$.ajax({
				type: "POST",
				url: "{{url('/admin/template_menu_action')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#ajax_div').html(msg);
					}
				});
				
				
				
				/*var formData = new FormData($("#rest_frm")[0]);
		
				$.ajax({
					url: "page.php",
					type: "POST",
					data: formData,
					async: false,
					success: function (msg) {
						alert(msg)
					},
					cache: false,
					contentType: false,
					processData: false
				});*/
		
				//e.preventDefault();
			
	
			}
			else
			{
				return false;
			}		
		}
		</script>
