<div class="col-sm-12">
	<p style="float: left;text-align: center;width: 100%;">				
	@if(Session::has('menu_message'))
		{{Session::get('menu_message')}}
	@endif 
	</p>
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Menu Category</h4>
			<div class="drage" id="sortable" data-rest="{{$template_id}}">
			
		
			<div class="sub-drage">
				<p>
				<a title="Edit" href="javascript:void(0)" data-rest="{{$template_id}}" id="show_popular_item">Popular</a>
				</p>
			</div>
																			
			@if(!empty($menu_list))
			  @foreach($menu_list as $mlist)
				<div class="sub-drage @if($mlist->menu_id==$menu_id)selected @endif " id="listItem_{{$mlist->menu_id}}">
			
				
					<p><a href="javascript:void(0)" id="menu_cat_list-{{$mlist->menu_id}}" data-menu="{{$mlist->menu_id}}"  data-rest="{{$template_id}}">{{$mlist->menu_name}}</a></p>
				</div>
			  @endforeach
			@endif
				
			</div>	
													
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">
											
					@if(!empty($menu_list))
			
						<h4>{{$menu_detail[0]->menu_name}}@if($menu_detail[0]->menu_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif </h4>
					
					<p>{{$menu_detail[0]->menu_desc}}</p>
					
						@if(!empty($menu_cate_detail))
						<div style="border:1px solid #666666;">
							<?php $c = 1;?>
							<table width="100%" cellpadding="5" cellspacing="5"  border="1" bordercolor="#ddd" >

							
							@foreach($menu_cate_detail as $cat_list)
							
							<tr>
							<td width="5%" valign="top">{{$c++}}</td>
								<td width="25%" valign="top">{{$cat_list->menu_category_name}}: <br />{{$cat_list->menu_category_desc}}</td>
								<td  width="25%">
									@if($cat_list->menu_category_portion=='no')
								${{$cat_list->menu_category_price}}
								@elseif($cat_list->menu_category_portion=='yes')
								
								<?php 
											  	$menu_sub_itme = DB::table('template_category_item')	
														->where('template_id', '=' ,$template_id)
														->where('menu_id', '=' ,$menu_id)
														->where('menu_category', '=' ,$cat_list->menu_category_id)
														->orderBy('menu_cat_itm_id', 'asc')
														->get();
														
														
											if($menu_sub_itme)	
												{	
											foreach($menu_sub_itme as  $msi){
											echo $msi->menu_item_title.' : $'.$msi->menu_item_price.'<br />';
											}
										 }
														
								?>
								
								
								 
								@endif
								</td>
						<td width="20%" valign="middle">
						
							
							
							@if($cat_list->menu_cat_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif
							<br />
							<br />
							
							@if($cat_list->menu_cat_diet==1) 
								<span class="label label-success">&nbsp;</span>
							@elseif($cat_list->menu_cat_diet==2)
								<span class="label label-danger">&nbsp;</span>
							@endif
							<br />
							<br />
<a class="btn btn-primary" href="javascript:void(0)" data-menu="{{$menu_id}}" data-rest="{{$template_id}}" data-item="{{$cat_list->menu_category_id}}" id="addon_list_data-{{$cat_list->menu_category_id}}" data-toggle="modal" >View Addons</a> 
							</td>
							</tr>
						@endforeach
						<tr>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
							</table>
						</div>
						@endif
					
					@endif
				</div>												
			</div>
		</div>
	</div>
</div>	