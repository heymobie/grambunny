@extends('layouts.owner')



@section("other_css")


        <!-- DATA TABLES -->

 <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

 <link href="{{ url('/') }}/public/assets/css/custom.css" rel="stylesheet" type="text/css" />

 <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

 <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.min.css"> -->

<style type="text/css">

 .fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}   

#myInput{ float: right; width: 30%;margin-bottom: 10px; }

#livechat {

    border: 2px solid #333333;
    padding: 2px 15px 2px;
    display: inline-block;
    margin-bottom: 15px;
    border-radius: 4px;
    margin-left: 10px;
    font-weight: bold;

    background-color: deepskyblue;
    color: #ffffff;

}     

.badge-btn .label-primary {
    background-color: #ed1c24 !important;
    font-size: 17px;
    font-weight: 600;
    padding: 9px 7px;
    margin-right: 5px;
    margin-bottom: 6px;
    width: 174px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    /* border: 1px solid #000; */
    border-radius: 0;
    box-shadow: 4px 5px 10px #afafaf;
}

</style>  

   

@stop



@section('content')



<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header badge-btn">

        <h1 style="margin-bottom: 15px;">

            Dashboard

            <small>Control Panel</small>

        </h1>

      
 <div class="t-heding"> <h3>Total Statics </h3></div>
<div class="top-btn-bar">
    <span class="label label-primary "> <label>Orders</label>  &nbsp; <span class="badge">  {{ $ordercount }}</span></span>

    <!-- <span class="label label-primary "> <label>Users </label> &nbsp; <span class="badge">  {{ $usercount }}</span></span> -->
    

     <span class="label label-primary "> <label>Pending Orders </label> &nbsp; <span class="badge">  {{ $orderpending }}</span></span>

      <span class="label label-primary "><label>This Week Orders </label> &nbsp; <span class="badge">  {{ $currentweek }}</span></span>

       <span class="label label-primary "> <label>Products </label> &nbsp; <span class="badge">  {{ $totalproducts }}</span></span>

      </div>

      <div class="t-heding"><h3>Today Statics &nbsp; <?php echo date("d/m/Y");?></h3></div>
      <div class="top-btn-bar">

      <span class="label label-primary "> <label>Orders</label>&nbsp; <span class="badge">  {{ $todaydate }}</span></span>

     <span class="label label-primary "><label>Pending Orders </label> &nbsp; <span class="badge">  {{ $todaypending }}</span></span>

      <span class="label label-primary "><label>Accepted Orders </label> &nbsp; <span class="badge">  {{ $todayAccept }}</span></span>

       <span class="label label-primary "><label>Cancelled Orders </label> &nbsp; <span class="badge">  {{ $todaycancel }}</span></span>

        <span class="label label-primary "><label>On The Way</label> &nbsp; <span class="badge">  {{ $today_ontheway }}</span></span>


    <!-- <span class="label label-primary ">Pending Orders &nbsp; <span class="badge">  {{ $orderpending }}</span></span> -->

     <span class="label label-primary "><label>Complete Orders</label> &nbsp; <span class="badge">  {{ $todaydeliverd }}</span></span>

      <!-- <span class="label label-primary ">Pending This Week Orders &nbsp; <span class="badge">  </span></span> -->

      </div>


    <!-- <span class="label label-primary">Total Orders &nbsp; <span class="badge">  {{ $ordercount }}</span></span> -->

      <!--  <span id="livechat" style="cursor: pointer;" onclick="mlivechat()"><i>Live Chat</i></span>  -->

        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Dashboard</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

     <div class="col-md-2">
  <p>Orders</p>
    <select name="orders_get_id" id="orders_get_id" onchange="orders_show()" style=" width: 100%;">
    <!-- <option value="all" id="all" >All Order</option> -->
     <option value="all" id="all">One Week</option>
    <option value="1" id="1">Today</option>
   <option value="2" id="2">Yesterday</option>
    <option value="3" id="3">One Month</option>
     <option value="custom" id="custom">custom Date</option>
</select>
</div>
<form action="#" method="post" id="datefilter" class="show_date" style="display :none;">
  <div class="col-md-3">
<p>Start Date</p>   
<input type="date" name="order_date" id="order_date" class="" required>
</div>
<div class="col-md-3">
<p>From Date</p> 
<input type="date" name="from_order_date" id="from_order_date" class="" required>
</div>
<div class="col-md-2 w-100">
  <p> Search</p>
<input type="submit" name="" id="" class="" value="filter">
</div>
</form>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">

<div id="ajaxcontent" class="filter ajaxcontent"></div>


<div>
    <canvas id="line-chart" class="chart Active" width="500" height="150"></canvas>
 </div>

<h4>Recent Orders</h4>  

    

            <!--<input id="myInput" type="text" placeholder="Search..">-->

                <section>

                    <div class="row">

                        <div class="my-panel-data">

                             <div class="col-xs-12">

                                <div class="box">

                                    <div class="box-body table-responsive">

                                        <div id="vender_search_list">   



                                    <table id="myTable11" data-order='[[ 0, "desc" ]]' class="table table-bordered table-hover table-striped">

<thead>

<tr>

<th>IDs</th>

<!-- <th></th> -->

<th>Order By</th>

<th>Customer Email</th>

<th>Contact No</th>

<th>Order Price</th>

<th>Order Status</th>

<th>Placed at</th>

<th>Action</th>

</tr>

</thead>

<tbody id="myTable">                                        

<?php foreach ($order_detail as $key => $value) { 

$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();

?>



<tr>

<td>{{$value->order_id}}</td>

<!-- <td><?php if(@$userinfo->profile_image){ ?><img src="{{ url('/public/uploads/user/'.$userinfo->profile_image) }}" width="50px;" height="50px;"><?php }else{ ?> <img src="{{ url('/public/uploads/user/user.jpg') }}" width="50px;" height="50px;"> <?php } ?>    

</td> -->

<td>{{@$userinfo->name}} {{@$userinfo->lname}}</td>

<td>{{@$userinfo->email}}</td>
<td>{{@$userinfo->user_mob}}</td>

<td>${{ number_format($value->total, 2) }}</td>

<td>                            

@if($value->status==0) Pending @endif

@if($value->status==1) Accept @endif

@if($value->status==2) Cancelled @endif

@if($value->status==3) On the way @endif

@if($value->status==4) Complete @endif

@if($value->status==5) Requested for return @endif

@if($value->status==6) Return request accepted @endif

@if($value->status==7) Return request declined @endif

</td>

<?php $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); ?>

<td>{{$createdat}}</td>



<td>

  <!--<a title="Edit Category" href="{{url('admin/product_service_category_form')}}/{{ $value->id }}"><i class="fa fa-edit"></i></a>

    <a title="Delete Category" href="{{url('admin/product_service_category_delete')}}/{{ $value->id }}"><i class="fa fa-trash-o"></i></a>-->

    <a href="{{ url('merchant/order-details').'/'.$value->id}}">View</a>

</td>



</tr> 



<?php } ?>

                                   

</tbody>

<!-- <tfoot>

<tr>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

</tr>

</tfoot> -->

</table>

                                        </div>

                                    </div>  

                                </div>

                            </div>

                        </div>

                    </div>



                <div class="col-12 mt-5 text-center">

                  <div class="custom-pagination">

                  

                  </div>

                </div>

                

                </section>  





</div>

<br /><br /><br />



<!--<div id="container_transport" style="min-width: 310px; height: 400px; margin: 0 auto"></div>-->

    </section><!-- /.content -->

</aside><!-- /.right-side -->

@endsection


<!--<div id="frame">

    <div id="sidepanel">

        <div id="profile">

            <div class="wrap">

                <p>Online Customer</p>

            </div>

        </div>

        <div id="contacts"></div>

    </div>

    <div class="content">

        <div class="contact-profile">

            <img src="" alt="" id="mmerchant" style="display:none;" />

            <p id="nmerchant"></p>

        </div>

        <div class="messages" id="messages"></div>

        <div class="message-input">

        <div class="wrap">

        <input type="text" name="cmessage" id="cmessage" value="" placeholder="Write your message..." />

        <input type="hidden" name="cuserid" id="cuserid" value="431">

        <input type="hidden" name="cmerchantid" id="cmerchantid" value="{{Auth::guard('vendor')->user()->vendor_id}}">

        <button class="submit" onclick="myChat()" ><i class="fa fa-paper-plane" aria-hidden="true"></i></button>

        </div>

        </div>

    </div>

</div>-->


@section('js_bottom')



<!-- jQuery 2.0.2 -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->

<!-- jQuery UI 1.10.3 -->

<script src="{{ url('/') }}/public/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- Bootstrap -->

<!--<script src="{{ url('/') }}/public/design/admin/js/bootstrap.min.js" type="text/javascript"></script>-->        

<!-- Bootstrap WYSIHTML5 -->

<script src="{{ url('/') }}/public/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>



<!-- AdminLTE App -->

<script src="{{ url('/') }}/public/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>



<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="{{ url('/') }}/public/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

        

<link rel="stylesheet" type="text/css" href="/css/result-light.css">

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>

<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://code.highcharts.com/modules/exporting.js"></script>

<!-- 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script src="//cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
<script>
    let table = new DataTable('#myTable11');
    </script> -->
    
<script>

$(document).ready(function(){

  $("#myInput").on("keyup", function() {

    var value = $(this).val().toLowerCase();

    $("#myTable tr").filter(function() {

      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

    });

  });

});

</script>

 
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@4.0.1/dist/chart.umd.min.js"></script>
    <script>

    new Chart(document.getElementById("line-chart"), {
      type : 'line',
      data : {
        labels : [  @foreach ($orderss as $res)

            "{{$res->x}}", 
           
            @endforeach

            ],
        datasets : [
            {
              data : [ @foreach ($orderss as $res1)

            {{$res1->y}},

            @endforeach

             ],

              label : "orders",
              borderColor : "#3cba9f",
              fill : false
            },

            {
              data : [ @foreach ($one_week as $res1)

            {{$res1->total}},

            @endforeach

             ],
              label : "Total Sell",
              borderColor : "#e43202",
              fill : false
            },

            // {
            //   data : [ @foreach ($userss as $res1)

            // {{$res1->user_id}},

            // @endforeach

            //  ],
            //   label : "Total Customer",
            //   borderColor : "#545454",
            //   fill : false
            // } 
             
            ]
      },
      options : {
        title : {
          display : true,
          text : ''
        }
      }
    });

 
  </script>

  <script type="text/javascript">
  function orders_show() {
    
     var id = $('option:selected').attr("id");

  if(id=="all"){

    $(".chart").show();
  $(".filter").hide();

}else{

  var today = $('option:selected').attr("id");
  var yesterday = $('option:selected').attr("id");
  var one_month = $('option:selected').attr("id");

  //alert(one_month);

  $(".show_date").hide();

    $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/merchant/orders_show'); ?>",

      data: {'today': today, 'yesterday': yesterday, 'one_month': one_month},

       success: function(data){
      
     $("#ajaxcontent").html(data);
  },

    });
  }

  if(id=="custom"){

    $(".show_date").show();
 
}
}
  </script>

  <script>
    
    $( '#datefilter' ).on( 'submit', function(e) {

            e.preventDefault();
            var to_order_date = $('#order_date').val();

            var from_order_date = $('#from_order_date').val();

            var order_date = to_order_date+'_'+from_order_date;

        $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

        type: "POST",
        url: "{{url('/merchant/chart_filter_order_date')}}",
        data: ({order_date: order_date}),
          success: function(data) {
            $(".ajaxcontent").html(data);

          },
        });
            
          });
  </script>

@stop