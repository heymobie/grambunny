				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">

										@if(isset($order_id_filter))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$order_id_filter}}/order_id_export">Export</a></button></div>
						              	@endif
										@if(isset($customer_name))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$customer_name}}/order_customer_export">Export</a></button></div>
						              	@endif
						              	@if(isset($merchant))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$merchant}}/order_merchant_export">Export</a></button></div>
						              	@endif
						              	@if(isset($order_status_serach))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$order_status_serach}}/order_status_export">Export</a></button></div>
						              	@endif
						              	@if(isset($order_date))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$order_date}}/order_date_export">Export</a></button></div>
						              	@endif
						              	@if(isset($coupon))
						              		<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/export_in_excel_filter/{{$coupon}}/order_coupon_export">Export</a></button></div>
						              	@endif


										<div id="">							
										<table id="example2" class="table table-bordered table-hover">
											<thead>
												<tr>
												<th>IDs#</th>
												<th>Order By</th>
												<th>Order Price</th>
												<th>Order Status</th>
												<th>Placed at</th>
												<th>Coupon Code</th>
												<th>Action</th>
												</tr>
											</thead>
											<tbody id="myTable">										
											<?php foreach ($order_detail as $key => $value) { 
											$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();
												$timestamp = strtotime($value->created_at);
												$dataPoints1 = array(
													array("label"=>date("d", $timestamp) , "y"=> $value->total),
													
												);
												//print_r($dataPoints1);
											?>

												<tr>
												<td>{{$value->order_id}}</td>
												<!-- <td><?php if($userinfo->profile_image){ ?><img src="{{ url('/public/uploads/user/'.$userinfo->profile_image) }}" width="50px;" height="50px;"><?php }else{ ?> <img src="{{ url('/public/uploads/user/user.jpg') }}" width="50px;" height="50px;"> <?php } ?>	
												</td> -->
												<td>{{$userinfo->name}} {{$userinfo->lname}}</td>
												<td>${{$value->total}}</td>
												<td>							
												@if($value->status==0) Pending @endif
												@if($value->status==1) Accept @endif
												@if($value->status==2) Cancelled @endif
												@if($value->status==3) On the way @endif
												@if($value->status==4) Complete @endif
												@if($value->status==5) Requested for return @endif
												@if($value->status==6) Return request accepted @endif
												@if($value->status==7) Return request declined @endif
												</td>

											    <?php $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); ?>
												<td>{{$createdat}}</td>
												<td></td>
												
												<td>
												    <a href="{{ url('merchant/order-details').'/'.$value->id}}">View</a>
												</td>

												</tr> 

											<?php } ?>
											                                   
											</tbody>
											<tfoot>
												<tr>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												</tr>
											</tfoot>
											</table>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
	               @if(!empty($customer_name))
	              	{{ $order_detail->appends(['customer_name' => $customer_name])->links() }}
	              	@endif
	              	@if(!empty($merchant))
	              	{{ $order_detail->appends(['merchant' => $merchant])->links() }}
	              	@endif
	              	@if(!empty($order_status_serach))
	              	{{ $order_detail->appends(['order_status_serach' => $order_status_serach])->links() }}
	              	@endif
	              	@if(!empty($order_date))
	              	{{ $order_detail->appends(['order_date' => $order_date])->links() }}
	              	@endif
	              	@if(!empty($coupon))
	              	{{ $order_detail->appends(['coupon' => $coupon])->links() }}
	              	@endif
	              </div>
	            </div>
	            
	            