<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Order No</th>
			<th>Order Type</th>
			<th>User name</th>
			<th>Restaurant Name</th>
			<th>Status</th>
			<th>Rejection Reason </th>
			<th>Rating </th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	 @foreach($review_detail as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>{{ $list->order_id}}</td>
			<td>{{ $list->order_type}}</td>
			<td>{{ $list->order_fname }}</td>
			<td>{{ $list->rest_name }}</td>
			<td>{{ $list->re_status }}</td>
			<td>@if($list->re_status=='REJECT'){{ str_replace('_',' ',$list->re_rejectreson) }}@endif</td>
			
			<td>{{ $list->re_rating }}</td>
			<td>
				 <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#update_review-{{$list->re_id}}"><i class="fa fa-eye"></i></a>  
				 
				  <a title="Delete User" href="{{url('vendor/review_delete')}}/{{$list->re_id}}"onclick="return delete_wal()"><i class="fa fa-trash-o"></i></a>
			
			<div class="modal fade" id="update_review-{{$list->re_id}}" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content" id="addons_modal_data">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title"><i class="fa fa-envelope-o"></i> User Review</h4>
	</div>
	<form action="{{url('/vendor/review_update')}}" method="post">
	<input type="hidden" name="re_id" id="re_id" value="{{$list->re_id}}" />
	
	{!! csrf_field() !!}
	<div class="box-body">
	<div class="popup_divcall">
	<label>User Name:</label> {{ $list->order_fname }}
	</div>
	<div class="popup_divcall">
	<label>Order ID:</label> {{ $list->order_id}}
	</div>
	<div class="popup_divcall">
	<label>Review Date:</label> {{ $list->created_at}}
	</div>
	<div class="popup_divcall">
	<label>Review Message:</label> {{ $list->re_content}}
	</div>
	<div class="popup_divcall">
	<label>Review Rating:</label> {{ $list->re_rating }}
	</div>
	<div class="popup_divcall">
	<label>Review Status:</label> 
	<select name="revew_status" data-reid="{{$list->re_id}}" id="revew_status" class="form-control">
	<option value="SUBMIT" @if($list->re_status=='SUBMIT') selected="selected" @endif>SUBMIT</option>
	<option value="PUBLISHED" @if($list->re_status=='PUBLISHED') selected="selected" @endif>PUBLISH</option>
	<option value="REJECT" @if($list->re_status=='REJECT') selected="selected" @endif>REJECT</option>
	
	</select>
	</div>
	
	<div id="Reason_reject{{$list->re_id}}" style="@if($list->re_status=='REJECT') display:block  @else display:none @endif" class="popup_divcall">
	<label>Reason:</label> 
	<select name="reason" id="reason" class="form-control">
	<option value="UNACCEPTABLE_LANGUAGE" @if($list->re_rejectreson=='UNACCEPTABLE_LANGUAGE') selected="selected" @endif>UNACCEPTABLE LANGUAGE</option>
	<option value="COMPARISON_TO_COMPETITION" @if($list->re_rejectreson=='COMPARISON_TO_COMPETITION') selected="selected" @endif>COMPARISON TO COMPETITION</option>
	<option value="INVALID_REASONING" @if($list->re_rejectreson=='INVALID_REASONING') selected="selected" @endif>INVALID REASONING</option>
	<option value="OTHER" @if($list->re_rejectreson=='re_rejectreson') selected="selected" @endif>OTHER</option>
	
	</select>
	
	</div>
	
	</div>
	<div class="modal-footer clearfix">
	
	
	<button type="submit" class="btn btn-primary pull-left">Update Status</button>
	</div>
	</form>
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
	</div>
	
			</td>
		</tr> 										
	<?php $i++; ?>
	@endforeach	                                      
	</tbody>
	<tfoot>
		<tr>
			<th>&nbsp;</th>
			<th>&nbsp;</th> 
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th> 
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th> 
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>	

<script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>