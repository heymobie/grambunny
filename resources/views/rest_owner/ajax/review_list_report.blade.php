									<table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>    
												<th>Restaurant Name</th>
												<th>User Name</th>
                                                <th>Restaurant Rating </th>
                                                <th>Delivery Rating </th>
                                                <th>Food Rating </th>
                                                <th>Feedback</th>
                                                <!--<th>Action</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>										
									 <?php $i=1; 
									 	if($review_detail){ ?>

										<?php foreach($review_detail as $list){ ?>

                                            <tr>
                                                <td>{{ $i }}</td>
                                                
                                                <td>{{ $list->rest_name }}</td>	
                                                 <td>{{ $list->order_fname }}</td>	
                                               									
                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_rating){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_delivery_ontime){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td>
                                                	
                                                <?php for ($j=1; $j <=5 ; $j++) { ?>
                                                	
                                                <?php if($j<=$list->re_food_good){ ?>	
                                                 <span class="fa fa-star checked"></span>
                                                <?php }else{ ?> <span class="fa fa-star"></span> <?php } ?>
                                                	
                                               <?php } ?>		

                                                </td>

                                                <td> {{ $list->re_content }} </td>
                               
                                            </tr> 	

									 <?php $i++; }  ?>
								  
										<?php }?>                                    
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th> 
                                                <th>&nbsp;</th> 
                                                

                                            </tr>
                                        </tfoot>
                                    </table>	

<script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>