<table id="example2" class="table table-bordered table-hover">
<thead>
	<tr>
		<th>Sr. No.</th>
		<th>Order No.</th>
		<th>Order ID</th>
		<th>OrderReq Date</th>
		<th>Restaurant</th>
		<th>Status</th>
		<th>Cust Submit Date</th>
		<!--<th>Last_Staus_Date</th>-->
		<th>Action</th>
	</tr>
</thead>
<tbody>										
<?php $i=1; ?>
@if(!empty($order_detail)) 
 @foreach ($order_detail as $list)
	<tr>
		<td>{{ $i }}</td>
		<td>{{ $list->order_id}}</td>
		<td>{{ $list->order_uniqueid}}</td>
		<td>{{ $list->order_create}}</td>
		<td>{{ $list->rest_name}}</td>
		<td>
		
@if($list->order_status=='1') New Order  @endif
@if($list->order_status=='4') Confirmed Order @endif
@if($list->order_status=='5') Ready Order @endif
@if($list->order_status=='6') Cancelled Order @endif
@if($list->order_status=='7') Assigned Order @endif
@if($list->order_status=='8') Accepted Order @endif
@if($list->order_status=='9') Picked Order @endif
@if($list->order_status=='10') On The Way Order @endif
@if($list->order_status=='11') Delivered Order @endif
		
		</td>
		<td>{{ $list->order_create }}</td>
		<!--<td></td>-->
		 <td> 
		  <a title="Update" href="{{url('vendor/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
		 
		 
		 <!--<a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>-->
		 
			 
		</td>
	</tr> 
<div class="modal fade" id="order-{{$list->order_id}}" role="dialog">
<div class="modal-dialog"> 

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title choice">Order Details</h4>
</div>
<div class="modal-body popup-ctn">

<h5><strong>Reataurant  summary</strong></h5>
<div>
<div>Name: {{$list->rest_name}}</div>
<div>Address: {{$list->rest_address}} {{$list->rest_suburb}} {{$list->rest_state}} {{$list->rest_zip_code}}</div>
<div>Contact No: {{$list->rest_contact}}</div>
</div>


<h5><strong>User summary</strong></h5>

<div>
<div>Name: {{$list->order_fname}} {{$list->order_lname}}</div>
<div>Address: {{$list->order_address}} {{$list->order_city}} {{$list->order_pcode}}</div>
<div>Contact No: {{$list->order_tel}}</div>
<div>Email: {{$list->order_email}}</div>
</div>	
<h5><strong>Your order summary</strong></h5>
<div>	
<?php $cart_price = 0;?>
@if(isset($list->order_carditem) && (count($list->order_carditem)))
<?php	$order_carditem = json_decode($list->order_carditem, true);?>	

<div>


@foreach($order_carditem as $item)
<?php  $cart_price = $cart_price+$item['price'];?>
<div>
<strong>{{$item['qty']}}  x</strong> {{$item['name']}} 

<strong class="pull-right">${{$item['price']}}</strong>
</div>

@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
@foreach($item['options']['addon_data'] as $addon)

<?php $cart_price = $cart_price+$addon['price'];?>
<div>
{{$addon['name']}}
<strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong>
</div>
@endforeach
@endif 

@endforeach
<div>
TotalAmount:
<strong class="pull-right">${{$cart_price}}</strong>
</div>
</div>
@endif
</div>
</div>
</div>
</div>
</div>										
<?php $i++; ?>
@endforeach	 
@else
	No record Found!  
@endif                                     
</tbody>
</table>	

<script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>