<div class="row">
<div class="my-panel-data">
<div class="col-xs-12">
<div class="box">
<div class="box-body table-responsive">

@if(isset($order_id_filter))
<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/product_export_filter/{{$order_id_filter}}/order_id_export">Export</a></button></div>
@endif
@if(isset($customer_name))
<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/product_export_filter/{{$customer_name}}/order_customer_export">Export</a></button></div>
@endif
@if(isset($merchant))
<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/product_export_filter/{{$merchant}}/order_merchant_export">Export</a></button></div>
@endif
@if(isset($order_status_serach))
<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/product_export_filter/{{$order_status_serach}}/order_status_export">Export</a></button></div>
@endif


@if(isset($order_date))
<div style="float:right;margin-bottom: 10px;"><button><a href="/merchant/product_export_filter/{{$order_date}}/order_date_export">Export</a></button></div>
@endif



<div id="">							
<table id="example2" class="table table-bordered table-hover">
<thead>
<tr>
<th>Order ID</th>
<th>Merchant</th>
<th>Customer</th>
<th>Status</th>
<th>Product Name</th>
<th>Qty</th>
<th>Unit Price</th>
<th>Total</th>
<th>Order Date</th>
</tr>
</thead>
<tbody id="myTable">										
<?php foreach ($order_detail as $key => $value) { 
$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();
$timestamp = strtotime($value->created_at);
$dataPoints1 = array(
array("label"=>date("d", $timestamp) , "y"=> $value->total),

);

$vendors = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
   
$merchant_name =$vendors->name.' '.$vendors->last_name;

$orderid = DB::table('orders')->where('order_id','=',$value->order_id)->value('id');

$productinfo = DB::table('orders_ps')->where('order_id','=',$orderid)->get();

foreach ($productinfo as $key => $pdata) {

?>

<tr>
<td>{{$value->order_id}}</td>
<td>{{$merchant_name}}</td>
<td>{{$userinfo->name}} {{$userinfo->lname}}</td>
<td>							
@if($value->status==0) Pending @endif
@if($value->status==1) Accept @endif
@if($value->status==2) Cancelled @endif
@if($value->status==3) On the way @endif
@if($value->status==4) Delivered @endif
@if($value->status==5) Requested for return @endif
@if($value->status==6) Return request accepted @endif
@if($value->status==7) Return request declined @endif
</td>
<td>{{$pdata->name}}</td>
<td>{{ $pdata->ps_qty }}</td>
<td>${{ $pdata->price }}</td>
<td>$<?php echo $pdata->ps_qty*$pdata->price; ?></td>

<?php $createdat = date("Y-m-d g:iA", strtotime($value->created_at)); ?>

<td>{{$createdat}}</td>

</tr> 

	
<?php } ?>

<?php } ?>

</tbody>
<tfoot>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</div>	
</div>
</div>
</div>
</div>

<div class="col-12 mt-5 text-center">
<div class="custom-pagination">
@if(!empty($customer_name))
{{ $order_detail->appends(['customer_name' => $customer_name])->links() }}
@endif
@if(!empty($merchant))
{{ $order_detail->appends(['merchant' => $merchant])->links() }}
@endif
@if(!empty($order_status_serach))
{{ $order_detail->appends(['order_status_serach' => $order_status_serach])->links() }}
@endif
@if(!empty($order_date))
{{ $order_detail->appends(['order_date' => $order_date])->links() }}
@endif
@if(!empty($coupon))
{{ $order_detail->appends(['coupon' => $coupon])->links() }}
@endif
</div>
</div>

