	<!-- Main content -->
	<section class="content">
	<div class="col-xd-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Menu Item Form -> {{$menu_name}}</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_cate_frm" action="{{ url('/vendor/category_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="menu_category_id" value="{{$id}}" />
								<input type="hidden" name="menu_id" value="{{$menu_id}}" />
								<input type="hidden" name="restaurant_id" value="{{$rest_id}}" />
								{!! csrf_field() !!}
                                    <div class="box-body">										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                           <input type="text" class="form-control" name="menu_name" id="	menu_name" value="" required="required">
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="menu_desc" id="menu_desc" required="required"></textarea>																
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Popular</label>
											<select name="menu_cat_popular" id="menu_cat_popular">
												<option value="0">No</option>			
												<option value="1">Yes</option>										
											</select>												
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Diet</label>
											<select name="menu_cat_diet" id="menu_cat_diet">	
												<option value="0">N/A</option>	
												<option value="1">Veg</option>			
												<option value="2">Non-Veg</option>										
											</select>												
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Available in diffrent portion sizes</label>	
											<select name="diff_size" id="diff_size">
												<option value="no">No</option>			
												<option value="yes">Yes</option>										
											</select>
                                        </div>
										
										
										<div id="diff_size_div" style="display:none" >
											 <div class="form-group">
											 	<table width="100%" cellpadding="2" cellspacing="2" id="portion_size_table"> 
													<tr>
														<th>Portion Size</th>
														<th>Display Order</th>
														<th>Price</th>
														<th>Status</th>
													</tr>
													<tr><td>
													<input type="hidden" name="item_id[]" value="0" />
													
													<input class="form-control" type="text" name="item_title[]" value="" required="required"/></td>
														<td>1</td>
														<td><input class="form-control" type="text" name="item_price[]" value="" required="required"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td>
													
													<input type="hidden" name="item_id[]" value="0" />													
													<input class="form-control" type="text" name="item_title[]" value=""/></td>
														<td>2</td>
														<td><input class="form-control" type="text" name="item_price[]" value=""/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td>
													
													<input type="hidden" name="item_id[]" value="0" />
													<input class="form-control" type="text" name="item_title[]" value=""/></td>
														<td>3</td>
														<td><input class="form-control" type="text" name="item_price[]" value=""/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>
													
													
													<tr>
														<td>
													
													<input type="hidden" name="item_id[]" value="0" />
													<input class="form-control" type="text" name="item_title[]" value=""/></td>
														<td>4</td>
														<td><input class="form-control" type="text" name="item_price[]" value=""/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>	
													<tr>
													<td>
													
													<input type="hidden" name="item_id[]" value="0" />
													<input class="form-control" type="text" name="item_title[]" value=""/></td>
														<td>5</td>
														<td><input class="form-control" type="text" name="item_price[]" value=""/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>													
													<tr>
													<td>
													
													<input type="hidden" name="item_id[]" value="0" />
													<input class="form-control" type="text" name="item_title[]" value=""/></td>
														<td>6</td>
														<td><input class="form-control" type="text" name="item_price[]" value=""/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1">Active</option>
															<option value="0">Inactive</option>
														</select>
														</td>
													</tr>
													
												</table>
											 </div>
										</div>
										
										
										
                                        <div class="form-group" id="main_price_div">
                                            <label for="exampleInputEmail1">Item Price</label>
                                           <input type="text" class="form-control" name="menu_price" id="	menu_price" value="" required="required" number="number">
                                        </div>
										
									   <div class="form-group" id="main_price_div">
                                       <label for="exampleInputEmail1">Item Image</label>
                                       <input type="file" class="form-control" name="menu_image" id="menu_image" value="">
                                       </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
																								
											<select name="menu_cat_status" id="menu_cat_status" class="form-control">
											<option value="1">Active</option>
											<option value="0">Inactive</option>
											</select>															
                                        </div>
										
                                       
                                      
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<!--<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />-->
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>	
	</section><!-- /.content -->

<script>
  $(document).on('change', '#diff_size', function(){ 
  	if($(this).find(":selected").val()=='yes'){
  		$('#main_price_div').hide();
  		$('#diff_size_div').show();
	}
	else if($(this).find(":selected").val()=='no')
	{
  		$('#diff_size_div').hide();
  		$('#main_price_div').show();
		
	}
  
 });
 
 
$(document).on('change', '#add_new_row', function(){ 
	
});
 
function check_frm(tpy)
{

	if(tpy=='back')
	{
		var	valid = true;
	}
	else
	{		
			
		var form = $("#rest_cate_frm");
		form.validate();
		var valid =	form.valid();
	}
	
	
	if(valid)
	{		
		 $("#ajax_favorite_loddder").show();	
		//var frm_val = 'from='+tpy+'&'+$('#rest_cate_frm').serialize();
		var formdata = new FormData($("#rest_cate_frm")[0]);

		$.ajax({
		type: "POST",
		url: "{{url('/vendor/category_action')}}",
		data: formdata,
		cache:false,
        contentType: false,
        processData: false,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#ajax_div').html(msg);
			}
		});
	}
	else
	{
		return false;
	}		
}
</script>

