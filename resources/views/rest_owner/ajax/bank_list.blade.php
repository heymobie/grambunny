<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">BankDetail Listing</h3><br />
			<div style="margin-top:10px;">
			<a  href="javascript:void(0)" data-rest="{{$rest_id}}" id="bank_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Details</a>
				</div>
			
		</div><!-- /.box-header -->
		<div class="box-body table-responsive">

@if(Session::has('bank_message'))

<div class="alert alert-success alert-dismissable">
  <i class="fa fa-check"></i>
   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
			   {{Session::get('bank_message')}}
</div>
@endif 
	
			<table id="example2" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>SrNo</th>
						<th>Name</th>
						<th>Bank</th>
						<th>Branch</th>
						<th>BSB</th>
						<th>Account</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@if($bank_list)<?php $c=1;?>
				  @foreach($bank_list as $blist )
					<tr>
						<td>{{$c++}}</td>
						<td>{{$blist->bank_uname}}</td>
						<td>{{$blist->bank_name}}</td>
						<td>{{$blist->bank_branch}}</td>
						<td>{{$blist->bank_bsb}}</td>
						<td>{{$blist->bank_acc}}</td>
						<td>
							@if($blist->bank_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif
						</td>
						<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$rest_id}}" data-bank="{{$blist->bank_id}}" id="update_bank-{{$blist->bank_id}}">Edit</a></td>
					</tr>	
				  @endforeach
				@endif  		                                     
				</tbody>
				<tfoot>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
				</tfoot>
			</table>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
