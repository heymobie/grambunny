@extends("layouts.grambunny")

@section("content")

  <div class="site-section bg-light login_sec">

                <div class="container">

                    <div class="row justify-content-center">

                        <div class="col-md-6 mb-5" data-aos="fade">

                            <h2 class="mb-5 text-black">FORGOT PASSWORD?</h2>

                            <form action="{{ route("vendor.sendPasswordResetLink") }}" method="POST" class="p-5 bg-white">

                                 @if(session()->has("success"))

                                <div class="alert alert-success" role="alert">

                                    <strong>{{ session()->get("success") }} </strong>

                                </div>

                                @endif

                                <input type="hidden" value="{{ csrf_token() }}" name="_token">

                                <div class="row form-group">

                                    <div class="col-md-12  ">

                                        <p class="text-center">Enter Your Email to reset password</p>

                                        <input  required type="email" id="email" name="email" class="form-control mt-3 {{ $errors->has("email") ? "is-invalid" : "" }}">

                                        @if($errors->has("email"))

                                        <span class="text-danger">{{ $errors->first("email") }}</span>

                                        @endif

                                    </div>

                                </div>

                                

                                <div class="row form-group">

                                    <div class="col-md-12">

                                        <input type="submit" value="Get Reset Link" class="btn btn-primary py-2 px-4 text-white">

                                    </div>

                                </div>

                                

                            </form>

                        </div>

                    </div>

                </div>

            </div>

@endsection

 