@extends("layouts.grambunny")

@section("content")

<div class="site-section bg-light login_sec">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-6 mb-5" data-aos="fade">

                <h2 class="mb-5 text-black">MERCHANT LOGIN</h2>

                <form action="{{ route('merchant.auth') }}" method="POST" class="p-5 bg-white">

                    @if(session()->has("success"))

                    <div class="alert alert-success" role="alert">

                        <strong>{{ session()->get("success") }} </strong>

                    </div>

                    @elseif(session()->has("warning"))

                    <div class="alert alert-warning" role="alert">

                        <h4 class="alert-heading">Account In Review!</h4>

                        <p>{{  session()->get("warning")}} </p>

                    </div>

                    @endif

                    @if(Session::has('login_message_error'))

                            <div class="alert alert-danger alert-block">

                                <button type="button" class="close" data-dismiss="alert">×</button>

                                {{ Session::get('login_message_error') }}

                            </div>

                            @endif

                            @if(Session::has('Change_pass'))

                            <div class="alert alert-danger alert-block">

                                <button type="button" class="close" data-dismiss="alert">×</button>

                                {{ Session::get('Change_pass') }}

                            </div>

                            @endif

                            @if(Session::has('PassSucChange'))

                            <div class="alert alert-success alert-block">

                                <button type="button" class="close" data-dismiss="alert">×</button>

                                {{ Session::get('PassSucChange') }}

                            </div>

                            @endif

                    <input type="hidden" value="{{ csrf_token() }}" name="_token">

                    <div class="row form-group">

                        <div class="col-md-12">

                        <label class="text-black" for="email">Email</label>

                        <input value="<?php if(isset($_COOKIE["vemail"])) { echo $_COOKIE["vemail"]; }else{ echo old("email") ; } ?>" required type="email" id="email" name="email" class="form-control {{ $errors->has("email") ? "is-invalid" : "" }}">

                            @if($errors->has("email"))

                            <span class="text-danger">{{ $errors->first("email") }}</span>

                            @endif

                        </div>

                    </div>

                    <div class="row form-group">

                        <div class="col-md-12">

                            <label class="text-black" for="subject">Password</label>

                            <input value="<?php if(isset($_COOKIE["vpassword"])) { echo $_COOKIE["vpassword"]; }else{ echo ""; } ?>"    type="password" name="password" id="subject" class="form-control {{ $errors->has("password") ? "is-invalid" : "" }}">

                            @if($errors->has("password"))

                            <span class="text-danger">{{ $errors->first("password") }}</span>

                            @endif

                        </div>

                    </div>

                    <div class="row form-group loginmid">

                        <div class="col-6">

                            <div class="form-check">

                                <input {{ (isset($_COOKIE["vremember"])) ? "checked" : ""  }} class="form-check-input" type="checkbox" name="remember" id="defaultCheck1">

                                <label class="form-check-label" for="defaultCheck1">

                                    Remember me

                                </label>

                            </div>

                        </div>

                        <div class="col-6 text-right">

                            <a href="{{ url('/merchant/forgot_password') }}" class="nav-link">Forgot Password?</a>

                        </div>

                    </div>

                    <div class="row form-group">

                        <div class="col-md-12">

                            <input type="submit" value="Login" class="btn btn-primary py-2 px-4 text-white">

                        </div>

                    </div>

                  <div class="row form-group loginfoot">

                        <div class="col-12">

                            <p>No account yet? <a href="{{ route("vendorRegistration",["step" => "get-started"]) }}">Register </a></p>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection