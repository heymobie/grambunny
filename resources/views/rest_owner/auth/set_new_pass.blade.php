@extends("layouts.grambunny")

@section("content")

  <div class="site-section bg-light login_sec">

                <div class="container">

                    <div class="row justify-content-center">

                        <div class="col-md-6 mb-5" data-aos="fade">

                            <h2 class="mb-5 text-black">RESET PASSWORD</h2>

                            



                            @if(isset($invalid))

                                <form class="p-5 bg-white">

                                    <div class="alert alert-danger">

                                        <strong>{{ $invalid }}</strong>

                                    </div>  

                                </form>

                            @else



                            <form action="{{ route("vendor.updatePassword",["token" => encrypt($token)]) }}" method="POST" class="p-5 bg-white merchant-reset-password-form">

                                 @if(session()->has("success"))

                                <div class="alert alert-success" role="alert">

                                    <strong>{{ session()->get("success") }} </strong>

                                </div>

                                @endif

                                <input type="hidden" value="{{ csrf_token() }}" name="_token">

                                        <p class="text-center">Reset Your Password</p>

                                <div class="row form-group">

                                    <div class="col-md-12  ">

                                        <label>Password</label>

                                        <input required type="password" id="password" name="password" class="form-control   {{ $errors->has("password") ? "is-invalid" : "" }}">

                                        @if($errors->has("password"))

                                        <span class="text-danger">{{ $errors->first("password") }}</span>

                                        @endif

                                    </div>

                                </div>

                                <div class="row form-group">

                                    <div class="col-md-12  ">

                                        <label>Confirm Password</label>

                                        <input required type="password" id="password_confirmation" name="password_confirmation" class="form-control   {{ $errors->has("password_confirmation") ? "is-invalid" : "" }}">

                                        @if($errors->has("password_confirmation"))

                                        <span class="text-danger">{{ $errors->first("password_confirmation") }}</span>

                                        @endif

                                    </div>

                                </div>

                                

                                <div class="row form-group">

                                    <div class="col-md-12">

                                        <input type="submit" value="Update Password" class="btn btn-primary py-2 px-4 text-white">

                                    </div>

                                </div>

                                

                            </form>

                            @endif

                        </div>

                    </div>

                </div>

            </div>

@endsection

@section("styles")

<style type="text/css">

.is-invalid{

border-color: #f23a2e !important;

}

.form-group{

text-align: left;

}



.error{

    border-color: #f23a2e !important;

    color: #f23a2e ;

}

</style>

@endsection

@section("scripts")

    <script src="{{ asset("public/assets/js/validate.js") }}"></script>

    <script src="{{ asset("public/assets/js/application.js") }}"></script>

@endsection