@extends("layouts.grambunny")

@section("content")

<div class="site-section bg-light login_sec">

      <div class="container">

        <div class="row justify-content-center">

          <div class="col-md-7"  data-aos="fade" >



            <h2 class="mb-5 text-black">MOBIE SIGN UP</h2>



                <!-- partial:index.partial.html -->

                <!-- multistep form -->

                <form id="msform">

                  <!-- progressbar -->

                  <ul id="progressbar">

                    <li class="active">Account Setup</li>

                    <li>Personal Profile</li>

                    <li>Other Details</li>

                  </ul>

                  <!-- fieldsets -->

                  <fieldset>

                    <h2 class="fs-title">SIGN UP TO BECOME A MOBIE</h2>

                    <!-- <h3 class="fs-subtitle">This is step 1</h3> -->



                    <div class="row form-group">                

                      <div class="col-md-12">

                        <label class="text-black" for="email">Email</label> 

                        <input type="email" id="email" class="form-control">

                      </div>

                    </div>



                    <div class="row form-group">                

                      <div class="col-md-12">

                        <label class="text-black" for="subject">Password</label> 

                        <input type="password" id="subject" class="form-control">

                      </div>

                    </div>



                    <input type="button" name="next" class="next action-button" value="Continue" />



                    <div class="signupmsg">

                      <p>By clicking "CONTINUE", I represent that I have read, understand and agree to the grambunny Merchant  <a href="#">Agreement</a> and <a href="#">Privacy Policy.</a> </p>

                    </div>

                  </fieldset>



                  <fieldset>

                    <h2 class="fs-title">Personal Profile</h2>

                    <!-- <h3 class="fs-subtitle">We will never sell it</h3> -->



                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">First Name</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Last Name</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Full Company Name (Optional)</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Phone Number</label> 

                            <input type="Number" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Zip Code</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Mailing Address</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Apt/Suite/Unit Number</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">City</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">State</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Referral Code (Optional)</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <input type="button" name="previous" class="previous action-button" value="Previous" />

                    <input type="button" name="next" class="next action-button" value="Next" />

                  </fieldset>



                  <fieldset>

                    <h2 class="fs-title">Other Details</h2>

                    <!-- <h3 class="fs-subtitle">We will never sell it</h3> -->

                    

                    

                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Market Area (City)</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Serviced Radius (Miles)</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Driver’s License #</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Expiration Date</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                    </div>





                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Attach Driver’s License Front Copy</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Attach Driver's License Back Copy</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>

                    </div>



                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">SSN</label> 

                            <input type="text" id="email" class="form-control">

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">DOB</label> 

                            <input type="Date" id="email" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>



                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Account Profile Photo 1</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Account Profile Photo 2 (Optional)</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>

                    </div>



                    <div class="row">   

                      <div class="col-md-6">  

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Account Profile Photo 3 (Optional)</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>



                      <div class="col-md-6"> 

                        <div class="row form-group">                

                          <div class="col-md-12">

                            <label class="text-black" for="email">Account Profile Photo 4 (Optional)</label> 

                            <label for="file-upload" class="custom-file-upload">

                                <i class="fa fa-cloud-upload"></i> Upload Image

                            </label>

                            <input id="file-upload" type="file"/>

                          </div>

                        </div>

                      </div>

                    </div>



                    <input type="button" name="previous" class="previous action-button" value="Previous" />

                    <input type="submit" name="submit" class="submit action-button" value="Submit" />

                  </fieldset>

                </form>

                <!-- partial -->



          </div>

          

        </div>

      </div>

    </div>

@endsection

@section("scripts")

  <script type="text/javascript">

    //jQuery time

var current_fs, next_fs, previous_fs; //fieldsets

var left, opacity, scale; //fieldset properties which we will animate

var animating; //flag to prevent quick multi-click glitches



$(".next").click(function(){

  if(animating) return false;

  animating = true;

  

  current_fs = $(this).parent();

  next_fs = $(this).parent().next();

  

  //activate next step on progressbar using the index of next_fs

  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

  

  //show the next fieldset

  next_fs.show(); 

  //hide the current fieldset with style

  current_fs.animate({opacity: 0}, {

    step: function(now, mx) {

      //as the opacity of current_fs reduces to 0 - stored in "now"

      //1. scale current_fs down to 80%

      scale = 1 - (1 - now) * 0.2;

      //2. bring next_fs from the right(50%)

      left = (now * 50)+"%";

      //3. increase opacity of next_fs to 1 as it moves in

      opacity = 1 - now;

      current_fs.css({

        'transform': 'scale('+scale+')',

        'position': 'absolute'

      });

      next_fs.css({'left': left, 'opacity': opacity});

    }, 

    duration: 100, 

    complete: function(){

      current_fs.hide();

      animating = false;

    }, 

    //this comes from the custom easing plugin

    easing: 'easeInOutBack'

  });

});



$(".previous").click(function(){

  if(animating) return false;

  animating = true;

  

  current_fs = $(this).parent();

  previous_fs = $(this).parent().prev();

  

  //de-activate current step on progressbar

  $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

  

  //show the previous fieldset

  previous_fs.show(); 

  //hide the current fieldset with style

  current_fs.animate({opacity: 0}, {

    step: function(now, mx) {

      //as the opacity of current_fs reduces to 0 - stored in "now"

      //1. scale previous_fs from 80% to 100%

      scale = 0.8 + (1 - now) * 0.2;

      //2. take current_fs to the right(50%) - from 0%

      left = ((1-now) * 50)+"%";

      //3. increase opacity of previous_fs to 1 as it moves in

      opacity = 1 - now;

      current_fs.css({'left': left});

      previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});

    }, 

    duration: 100, 

    complete: function(){

      current_fs.hide();

      animating = false;

    }, 

    //this comes from the custom easing plugin

    easing: 'easeInOutBack'

  });

});

  

$(".submit").click(function(){

  return false;

})



</script>

@endsection