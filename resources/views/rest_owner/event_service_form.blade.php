@extends('layouts.owner')
<style>
   .ui-widget-content {
      max-height: 221px;
      overflow-y: scroll;
   }

   .ui-menu .ui-menu-item {
      padding: 5px;
   }

   .ui-menu-item:nth-child(2n) {
      background-color: #f1f1f1;
   }
.awesome-cropper{ width: 100px; }

 .modal-content canvas{
  width: 100%;
 }

.modal-dialog11{
 width: 50% !important;
 margin: auto !important;
 } 

 a.galdelsec{ padding-right:15px; margin-left:-15px; }   
   
</style>

<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />
@section('content')
<aside class="right-side">
   <section class="content-header">
      <h1>
         Event Management
         <small>Control Panel</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ route('merchant.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Event Management Service</li>
      </ol>
   </section>
   <section class="content">
      <div class="col-md-12">
         <div class="box box-primary">
            <div class="box-header">
               <h3 class="box-title">Event Management Service</h3>
            </div>
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable">
               <i class="fa fa-check"></i>
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
               {{Session::get('message')}}
            </div>
            @endif
            @if(Session::has('message_error'))
            <div class="alert alert-danger alert-dismissable">
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
               {{Session::get('message_error')}}
            </div>
            @endif
            <!-- form start -->           
            <!-- {{ Request::segment(2) }} -->
            <!-- {{ $preurl = url()->previous() }} -->   

            <?php if ($id > 0) {
               $catid = $ticket_detail[0]->category_id;
            }else{
               $catid = 0;
            } ?>

            <?php if ($id > 0) {
               $subcatid = $ticket_detail[0]->sub_category_id;
            } else {
               $subcatid = 0;
            } ?>

            <form role="form" method="POST" id="rest_frm" action="{{ url('/merchant/event-management-action') }}" enctype="multipart/form-data">
               <input type="hidden" name="event_id" id="event_id" value="{{$id}}" />
               <input type="hidden" name="vendor_id" id="vendor_id" value="{{$vendor_id}}" /> 
               
               
               <input type="hidden" name="addon_type" value="0" />
               <input type="hidden" name="ps_type" value="3" />

               {!! csrf_field() !!}
               <div class="box-body">

                  <div class="row">

                     <div class="col-md-6">

                        <div class="form-group">

                           <label for="exampleInputEmail1">Category</label>

                           <select class="form-control" name="category_id" id="category_id" >

                           <?php foreach ($category_list as $key => $value) { ?>

                                    <option value="{{$value->id}}" <?php if ($value->id == $catid) { echo "selected"; } ?>>{{$value->category}}</option>

                              <?php } ?>

                           </select>

                        </div>

                     </div>

                     <div class="col-md-6">

                        <div class="form-group">

                           <label for="exampleInputEmail1">Sub Category</label>

                           <input type="hidden" name="sub_category_id" id="sub_cat_id" value="{{$subcatid}}">

                           <select class="form-control" name="sub_category_id" id="sub_category_id" >

                              <?php foreach ($sub_category_list as $key => $value) { ?>

                                 <option value="{{$value->id}}" <?php 
                                 if ($value->id == $subcatid) {   echo "selected";} ?>>{{$value->sub_category}}</option>

                              <?php } ?>

                           </select>

                        </div>

                     </div>
                     </div>   





                  <div class="form-group">
                     <label for="exampleInputEmail1">Event Name</label>
                     <input type="text" class="form-control" name="event_name" id="event_name" value="@if($id>0){{$ticket_detail[0]->name}}@endif" required="required">
                  </div>

                  <div class="form-group">
                     <label for="exampleInputEmail1">Notes & Remarks</label>
                     <textarea class="form-control" required="required" name="notes_remarks" id="notes_remarks" min="20" style="height: 100px;">@if($id>0){{$ticket_detail[0]->notes_remarks}}@endif</textarea>
                  </div>

                  <div class="form-group">
                     <label for="exampleInputEmail1">Event Description</label>
                     <textarea class="form-control" required="required" name="event_desc" id="event_desc" min="20"  style="height: 100px;">@if($id>0){{$ticket_detail[0]->description}}@endif</textarea>
                  </div>

                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Venue Name</label>
                           <input type="text" class="form-control" name="venue_name" id="venue_name" value="@if($id>0){{$ticket_detail[0]->venue_name}}@endif" required="required">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Venue Address</label>
                           <input type="text" class="form-control" name="venue_address" id="venue_address" value="@if($id>0){{$ticket_detail[0]->venue_address}}@endif" maxlength="60" required="required">
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <?php

                     function get_browser_name($user_agent)
                     {
                        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
                        elseif (strpos($user_agent, 'Edge')) return 'Edge';
                        elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
                        elseif (strpos($user_agent, 'Safari')) return 'Safari';
                        elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
                        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

                        return 'Other';
                     }

                     $browsername = get_browser_name($_SERVER['HTTP_USER_AGENT']);
                     $event_date = ''; 
                     if (isset($ticket_detail[0]->event_date)) {
                        $event_date = $ticket_detail[0]->event_date;                         
                        $event_date =  date("Y-m-d", strtotime($event_date));
                     } else {
                        $event_date = '';
                     }

                     if ($browsername == 'Safari') {
                        if ($event_date) {
                           //$event_date = date("m/d/Y", strtotime($event_date));
                           $event_date = date("Y-m-d", strtotime($event_date));
                        }
                     }else{
                        $event_date = $event_date;
                        
                     }


                     ?>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Event Date</label>

                           <?php if ($browsername == 'Safari') { ?>
                              <input type="text" required="required" placeholder="yyyy-mm-dd" class="form-control" name="event_date" id="datepicker2" value="@if($id>0){{$event_date}}@endif">
                           <?php } else { ?>
                           <!--  <input type="date" required="required" placeholder="yyyy-mm-dd" class="form-control" name="event_date" id="permit_expiry" value="@if($id>0){{$event_date}}@endif">
                           -->   
                           <input readonly type="text" required="required" placeholder="yyyy-mm-dd" data-date-format='yyyy-mm-dd' class="form-control" name="event_date" id="permit_expiry" value="@if($id>0){{$event_date}}@endif">

                            @if($errors->has("event_date"))

                            <span class="text-danger">{{ $errors->first("event_date") }}</span>

                            @endif
                            
                           <?php } ?>
                        </div>

                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-6'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Event Start Time</label>
                              <div class='input-group date' id='mondatetimepicker31'>
                                 <input type='text' class="form-control" id="event_start_time" name="event_start_time" value="@if($id>0){{$ticket_detail[0]->event_start_time}}@endif" required="required"/>
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <div class='col-sm-6'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Event End Time</label>
                              <div class='input-group date' id='mondatetimepicker32'>
                                 <input type='text' class="form-control" id="event_end_time" name="event_end_time" value="@if($id>0){{$ticket_detail[0]->event_end_time}}@endif" required="required"/>
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6"></div>
                  </div>

                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Available Ticket Inventory</label>
                           <input type="number"  class="form-control" name="ticket_inventory" id="ticket_inventory" value="@if($id>0){{$ticket_detail[0]->quantity}}@endif" required="required" >
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Seating Area</label>
                           <input type="text" class="form-control" name="seating_area" id="seating_area" value="@if($id>0){{$ticket_detail[0]->seating_area}}@endif" required="required">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Ticket Price</label>
                           <input type="text" class="form-control" name="ticket_price" id="ticket_price" value="@if($id>0){{$ticket_detail[0]->price}}@endif" required="required">
                              @if ($errors->has('ticket_price'))
                                 <span class="text-danger text-left">{{ $errors->first('ticket_price') }}</span>
                              @endif
                        </div>
                     </div>
                      <div class="col-md-4">

                  <div class="form-group">

                    <label for="exampleInputEmail1">Unit of Sale</label>

                           <select name="unit" id="unit" class="form-control">

                              <?php  foreach ($product_unit as $key => $value) { ?>

                                 <option value="{{$value->unit_name}}" @if(($id>0)&& ($ticket_detail[0]->unit==$value->unit_name)) selected="selected" @endif>{{$value->unit_name}}</option>

                              <?php } ?>

                           </select>

                     </div>

                </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Ticket Commission (%)</label>
                           <input type="text" class="form-control" name="ticket_commission" id="ticket_commission" value="@if($id>0){{$ticket_detail[0]->ticket_commission}}@endif" required="required">
                           @if ($errors->has('ticket_commission'))
                                 <span class="text-danger text-left">{{ $errors->first('ticket_commission') }}</span>
                           @endif
                        </div>
                     </div>


                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Ticket Fee</label>
                           <input type="text" class="form-control" name="ticket_fee" id="ticket_fee" value="@if($id>0){{$ticket_detail[0]->ticket_fee}}@endif" required="required">
                           @if ($errors->has('ticket_fee'))
                                 <span class="text-danger text-left">{{ $errors->first('ticket_fee') }}</span>
                           @endif
                        </div>
                     </div>


                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Ticket Service Fee</label>
                           <input type="text" class="form-control" name="ticket_service_fee" id="ticket_service_fee" value="@if($id>0){{$ticket_detail[0]->ticket_service_fee}}@endif" required="required">
                           @if ($errors->has('ticket_service_fee'))
                                 <span class="text-danger text-left">{{ $errors->first('ticket_service_fee') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Status</label>
                           <select name="status" id="status" class="form-control">
                              <option value="1" @if(($id>0)&& ($ticket_detail[0]->status=='1')) selected="selected" @endif>Active</option>
                              <option value="0" @if(($id>0)&& ($ticket_detail[0]->status=='0')) selected="selected" @endif>Inactive</option>
                           </select>
                        </div>
                     </div>

                  </div>


                  <div class="form-group">

                     <label for="exampleInputEmail1">Thumb Image (File Type: jpeg,gif,png)</label>

                     <input type="hidden" class="form-control" name="product_image" id="product_image" accept="image/png, image/gif, image/jpeg">


                      <input type="hidden" class="form-control" name="product_old_img" id="product_old_img" value="@if(!empty($product_detail[0]->image)) @if($id>0){{$product_detail[0]->image}}@endif @endif">
                      <br>
                       
                     @if($id>0 && $product_detail[0]->image!='')
                          <span id="profile1a">
                     <img src="{{ url('/') }}/public/uploads/product/{{ $product_detail[0]->image }}" width="50px;" height="50px;">
                  </span>

                     @endif

                     @if($errors->has("product_image"))

                  <span class="text-danger">{{ $errors->first("product_image") }}</span>
                     @endif
                  </div>
                  <div class="form-group">

                     <label for="exampleInputEmail1">Gallery Image (File Type: jpeg,gif,png)</label>

                     <input type="hidden" class="form-control" name="product_image_gallery" id="product_image_gallery" multiple>

                           <input type="hidden" name="product_old_glimg" id="product_old_glimg" value="" />

                     @if(($id>0) && (!empty($glimage)))

                     <?php foreach ($glimage as $key => $value) { ?>

                        <div class="mgmOne">

                           <span class="img-gl"><img src="{{ url('/') }}/public/uploads/product/{{ $value->name }}" width="50px;" height="50px;"></span>

                           <a href="{{ url('/admin/event-management-gallery-delete/'.$value->id) }}"><i class="fa fa-times" aria-hidden="true"></i></a>

                        </div>
                     <?php } ?>

                     @endif

                     @if($errors->has("product_image_gallery"))

                     <span class="text-danger">{{ $errors->first("product_image_gallery") }}</span>

                     @endif

                  </div>

                  <div class="row">
                  </div>

               </div><!-- /.box-body -->

               <div class="box-footer">
                  <input type="submit" class="btn btn-primary" value="Submit" id=
                     "psubid"/>
                  <input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);" />


               </div>
            </form>

         </div><!-- /.box -->


      </div>


   </section><!-- /.content -->
</aside><!-- /.right-side -->

@endsection

@section('js_bottom')

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

 
<script type="text/javascript">

   $('#permit_expiry').focusin( function()
     {
       $("#permit_expiry").datepicker({ dateFormat: 'yyyy/mm/dd',autoclose: true }).val();          
     });


   $('#category_id').on('change', function() {
      var catId = $(this).val();
      var sub_cat_id = $('#sub_cat_id').val();
      $.ajax({
         type: "post",
         headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
         },
         data: {
            catid: catId,
            sub_cat_id: sub_cat_id
         },
         url: "{{url('/merchant/sub-category')}}",
         success: function(msg) {
            $('#sub_category_id').html(msg);
         }
      });
   });
   $(document).ready(function() {
      var catId = $('#category_id').val();
      var sub_cat_id = $('#sub_cat_id').val();
      $.ajax({
         type: "post",
         headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
         },
         data: {
            catid: catId,
            sub_cat_id: sub_cat_id
         },
         url: "{{url('/merchant/sub-category')}}",
         success: function(msg) {
            $('#sub_category_id').html(msg);
         }
      });
   });
</script>
<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script> 
<script type="text/javascript">

  /** Start Crop Image **/

   $(document).ready(function () {

   $('#product_image').awesomeCropper( { width: 500, height: 500, debug: true, imageSmoothingEnabled: true, imageSmoothingQuality: 'high' } );

    });

   $(document).ready(function () {

    $('#product_image_gallery').awesomeCropper( { width: 500, height: 500, debug: true, imageSmoothingEnabled: true, imageSmoothingQuality: 'high' } );

   });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

// setInterval(function () {

// var product_image = $('#product_image').val();
// var product_image_gallery = $('#product_image_gallery').val(); 

// if(product_image!=0){

// $('#psubid').prop('disabled', true);

// }

// if(product_image_gallery!=0){

// $('#psubid').prop('disabled', true);   

// }     

// },2000); 

    setInterval(function () {

            var vendor_id = $('#vendor_id').val(); 
            var event_id = $('#event_id').val(); 
            var product_image = $('#product_image').val();
            var product_image_gallery = $('#product_image_gallery').val();

            $('#product_image').val('');
            $('#product_image_gallery').val(''); 

            if(product_image!=0){

                $("#psubid").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');   
                $('#psubid').prop('disabled', true);
                $('#product_image').val('');
                $('#proimgthumb').hide();

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "{{url('/merchant/crop_event_image_save')}}",
                data: {productimg : product_image, vendor_id : vendor_id, event_id : event_id}, 
                cache: false,
                success: function (data)
                {  

                   if(data.status != 0) { 

                    $('#product_image').val('');
                    $('#product_old_img').val(data.status);
                    $('#proimgthumb').hide();
                    $('#psubid').prop('disabled', false);

                  }else{
                  
                  $('#psubid').prop('disabled', false);
                  
                  }

                }
            });

           }

            if(product_image_gallery!=0){

           $("#psubid").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');  
           $('#psubid').prop('disabled', true);
           $('#product_image_gallery').val('');

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "{{url('/merchant/gallery_event_crop_image_save')}}",
                data: {productimg : product_image_gallery, vendor_id : vendor_id, event_id : event_id}, 
                cache: false,
                success: function (data)
                {    
                    if(data.status != 0) { 

                    $('#product_image_gallery').val(''); 
                    $('#product_old_glimg').val(data.status);
                    $('#psubid').prop('disabled', false);
                    //window.location.reload();

                  }else{
                  
                  $('#psubid').prop('disabled', false);
                  
                  }

                }
            });

           }
       timerId = setTimeout(mpimg, 2000); 

          },2000); 
  
  /** Crop image */ 

</script>
@stop