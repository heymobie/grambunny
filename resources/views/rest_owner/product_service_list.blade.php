@extends('layouts.owner')

@section("other_css")

        <!-- DATA TABLES -->

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

   <meta name="_token" content="{!! csrf_token() !!}"/>

@stop


@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->

            <aside class="right-side">

                <!-- Content Header (Page header) -->

                <section class="content-header">

                    <h1>

                        Product Management

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="{{ url('/merchant/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li class="active">Product List</li>

                    </ol>

                </section>



                <!-- Main content -->

                <section class="content">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="box">

                                <div class="box-header">

                                    <h3 class="box-title">{{$cat_name}}</h3>

									<div style="float:right; margin-right:10px; margin-top:10px;">



										<a href="{{url('merchant/product-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New </a>



									

										</div>

									

                                </div><!-- /.box-header -->



                            </div><!-- /.box -->

							<div>

							<section>

							 	<div class="row">

									<div class="my-panel-data">

										 <div class="col-xs-12">								 

										 

										    <div class="box">

												<div class="box-body table-responsive">

												

												 @if(Session::has('message'))

												 

												 <div class="alert alert-success alert-dismissable">

													  <i class="fa fa-check"></i>

													   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

																   {{Session::get('message')}}

												 </div>

												@endif

												

												 @if(Session::has('message_error'))

													 

													 <div class="alert alert-danger alert-dismissable">

														   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>		   {{Session::get('message_error')}}

													 </div>

												@endif

					

					

					

													<div id="restaurant_list">							

														 <table id="example2" class="table table-bordered table-hover">

	<thead>

		<tr>

			<th>IDs</th>

			<th>Image</th>

			<th>Title</th>

			<th>Code</th>

			<th>Sub Category</th>

			<th>Status</th>

			<th>Create Date</th>

			<th>Action</th>

		</tr>

	</thead>

	<tbody>										

	<?php $i=1; ?>

	@if(!empty($rest_list)) 

	 @foreach ($rest_list as $list)

		<tr>

			<td>{{ $list->id }}</td>

			<td>



		@if(!empty($list->image))	

	   <img src="{{ asset("public/uploads/product/$list->image")}}" width="50px;" height="50px;">

		@else

			<img src="{{ asset('public/uploads/product/product.jpg') }}" width="50px;" height="50px;">

		@endif	

		

		</td>



		   <?php  $sub_cat_name = DB::table('product_service_sub_category')		

						->where('id', '=' ,$list->sub_category_id)

						->value('sub_category'); ?>	



			<td>{{ $list->name}}</td>

			<td>{{ $list->product_code}}</td>

			<td>{{ $sub_cat_name}}</td>

			<td>

			@if($list->status=='1') 

			<span class="label label-primary">PUBLISHED</span>

			 @else

			

			<span class="label label-danger">UNPUBLISHED</span>

			@endif

			</td>

			<td>{{ $list->created_at}}</td>

			 <td>

	

		<a title="Edit" href="{{url('merchant/product-form/')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a>  

		<!--<a title="View" href="{{url('merchant/product_view/')}}/{{ $list->id }}"><i class="fa fa-eye"></i></a>-->

		&nbsp;&nbsp;

		<a title="Delete Product" href="{{url('merchant/product-delete/')}}/{{ $list->id }}"><i class="fa fa-trash-o"></i></a>

					 

			</td>

		</tr> 										

	<?php $i++; ?>

	@endforeach	   

	@endif                                     

	</tbody>



</table>

													</div>

												</div>	

											</div>

										</div>

									</div>

								</div>

							</section>					

						  </div>

                        </div>

                    </div>



                </section><!-- /.content -->

            </aside><!-- /.right-side -->



@stop





@section('js_bottom')

<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



    

		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

        <script type="text/javascript">

            $(function() {

                $("#example1").dataTable();

                $('#example2').dataTable({

                    "bPaginate": true,

                    "bLengthChange": false,

                    "bFilter": true,

                    "bSort": true,

                    "bInfo": true,

                    "bAutoWidth": false

                });

            });

			

function check_frm()

{

$('#error_msg').hide();



var form = $("#search_frm");

	

	if(($("#rest_cont").val()!='') || ($("#rest_name").val()!=''))

	{		

		 $("#ajax_favorite_loddder").show();	

		var frm_val = $('#search_frm').serialize();				

		$.ajax({

		type: "POST",

		url: "{{url('/vendor/restaurant_search')}}",

		data: frm_val,

			success: function(msg) {

			 $("#ajax_favorite_loddder").hide();	

			

				$('#restaurant_list').html(msg);

			}

		});

	}

	else

	{

		//alert('Please insert any one value');

		$('#error_msg').html('Please insert any one value.');

		$('#error_msg').show();

		return false;

	}		

}





  $.ajaxSetup({

   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

});

 </script>

@stop

