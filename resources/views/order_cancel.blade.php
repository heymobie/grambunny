@extends('layouts.app')

@section("other_css")
     <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,300,300italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>

    
    <!-- Radio and check inputs -->
    <link href="{{ url('/') }}/design/front/css/skins/square/grey.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="{{ url('/') }}/design/front/js/html5shiv.min.js"></script>
      <script src="{{ url('/') }}/design/front/js/respond.min.js"></script>
    <![endif]-->

@stop
@section('content')

<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Place your order</h1>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Cancel!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section>
<!-- End section -->
<!-- End SubHeader ============================================ -->

 

<!-- Content ================================================== -->

<div class="container margin_60_35">
<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="box_style_2">
				<h2 class="inner">Order Canceled!</h2>
				<div id="confirm">
					<!--<i class="icon_check_alt2"></i>
					<h3>Thank you!</h3>-->
					<?php 
					
					$content_detail  = DB::table('pages')->where('page_id', '=' ,'8')->where('page_status', '=' ,'10')->get();	
					if($content_detail)
					{
						echo $content_detail[0]->page_content;
					}
					
					?>
				</div>
				<h4>Summary</h4>
				<table class="table table-striped nomargin">
				<tbody>
					<?php $cart_price = 0;?>
		@if(isset($cart) && (count($cart)))
				
				 @foreach($cart as $item)
				<?php  $cart_price = $cart_price+$item->price;?>
				<tr>
					<td>
						<strong>{{$item->qty}}x</strong> {{$item->name}} 
					</td>
					<td>
						<strong class="pull-right">${{number_format($item->price,2)}}</strong>
					</td>
				</tr>
				
				 
				
				 @if(!empty($item->options['addon_data'])&&(count($item->options['addon_data'])>0))
				 	@foreach($item->options['addon_data'] as $addon)
						
					  <?php $cart_price = $cart_price+$addon['price'];?>
					  
				<tr>
					<td>
						 {{$addon['name']}}
					</td>
					<td>
						<strong class="pull-right">@if($addon['price']>0)${{number_format($addon['price'],2)}}@endif</strong>
					</td>
				</tr>
				
					 
					 @endforeach
				 @endif 
					
				 @endforeach
@endif




				
				<tr>
                  <td> Subtotal Tax </td>
				  <td> <span class="pull-right">${{number_format($cart_price,2)}}</span> </td>
                </tr>


				@if((Session::has('cart_userinfo')['delivery_fee']) && (Session::get('cart_userinfo')['delivery_fee']>0))
                <tr>
                  <td> Delivery fee</td>
				  <td>  <span class="pull-right">${{number_format(Session::get('cart_userinfo')['delivery_fee'],2)}}</span></td>
                </tr>
					@if((Session::has('cart_userinfo')['delivery_fee_min_remaing']) && (Session::get('cart_userinfo')['delivery_fee_min_remaing']>0))
					
					<tr id="del_fee_min">
					  <td> ${{number_format(Session::get('cart_userinfo')['delivery_fee_min'],2)}} min  <span class="pull-right">$<span id="delivery_fee_min_span">{{number_format(Session::get('cart_userinfo')['delivery_fee_min_remaing'],2)}}</span> remaining</span>
					  </td>
					</tr>				
					@endif
				
				@endif
		
			
                <tr>
                  <td> Discount <?php  echo '('.Session::get('cart_userinfo')['promo_discount_text'].')';?></td>
				  <td><span class="pull-right"> -$<?php echo Session::get('cart_userinfo')['promo_discount'];?> </span></td>
                </tr>
				
			
                <tr>
                  <td> Service Tax </td>
				  <td><span class="pull-right"> <?php echo Session::get('cart_userinfo')['service_tax'];?></span> </td>
                </tr>
					
				<?php
				
				 if ((Session::get('cart_userinfo')['partical_payment_alow']>0))
				{
				?>
				
				<tr>
					<td colspan="2">Partial Payment(<?php echo Session::get('cart_userinfo')['partical_percent'];?>%)</td>
				</tr>
				<tr>
				
					<td>Partial Payout Amt (<?php echo Session::get('cart_userinfo')['partical_percent'];?>%) </td>
                	<td><span class="pull-right"> <?php echo Session::get('cart_userinfo')['partical_amt'];?></span></td>
				</tr>
				<tr>
				
					<td>Partial Remaining Payment </td>
                	<td><span class="pull-right"> <?php echo Session::get('cart_userinfo')['partical_remaning_amt'];?></span></td>
				</tr>
				
				<?php 
					}				
				?>
				
				
				<tr>
					<td class="total_confirm">
						 TOTAL
					</td>
					<td class="total_confirm">
					
					<?php	$total_amt = Session::get('cart_userinfo')['total_charge']; ?>
						<span class="pull-right">${{number_format($total_amt,2)}}</span>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td><span style="width:30%"><input type="button" id="re_order" name="re_order" value="Place this order again" class="btn_full" /></span> </td>
				</tr>
				
				
				
				</tbody>
				</table>
				
				
				
			</div>
		</div>
	</div>	
	<!-- End row -->
	
</div><!-- End container -->
<!-- End container-fluid  -->
<!-- End Content =============================================== -->
	
@stop

@section('js_bottom')
<!-- COMMON SCRIPTS -->
<script src="{{ url('/') }}/design/front/js/jquery-1.11.2.min.js"></script>
<script src="{{ url('/') }}/design/front/js/common_scripts_min.js"></script>
<script src="{{ url('/') }}/design/front/js/functions.js"></script>
<script src="{{ url('/') }}/design/front/assets/validate.js"></script>
<script>
$(document).on('click', '#re_order', function(){
	
	window.location.href='<?php echo url('/payment')?>';
});

</script>

@stop	