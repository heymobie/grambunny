@extends('layouts.app')



@section('content')











    <!-- SubHeader =============================================== -->

    <section class="header-video">

        <div id="hero_video">

            <div id="sub_content">

                <h1>Take Away Food &amp; Home Delivery</h1>

                <p>

                    Find food in:

                </p>

                <form method="post" action="list_page.html">

                    <div id="custom-search-input">

                        <div class="input-group">

                            

                                <select class="form-control select" name="states" id="states">

                                    <option value="VIC" selected="selected">VIC</option>

                                    <option value="NSW">NSW</option>

                                    <option value="SA">SA</option>

                                    <option value="WA">WA</option>

                                    <option value="NT">NT</option>

                                </select>

                            

                            <input type="text" class=" search-query" placeholder="Your Address or postal code">

                            <span class="input-group-btn">

                                <input type="submit" class="btn_search" value="submit">

                            </span>

                        </div>

                    </div>

                </form>

            </div><!-- End sub_content -->

        </div>

        <img src="{{ url('/') }}/design/front/img/video_fix.png" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="Vimeo" data-video-width="1920" data-video-height="960">

        <div id="count" class="hidden-xs">

            <ul>

                <li><span class="number">1100</span> Restaurant</li>

                <li><span class="number">3120</span> People Served</li>

                <li><span class="number">4520</span> Registered Users</li>

            </ul>

        </div>

    </section><!-- End Header video -->

    <!-- End SubHeader ============================================ -->

    <!-- Content ================================================== -->

    <div class="container margin_60">



        <div class="main_title">

            <h2 class="nomargin_top" style="padding-top:0">How it works</h2>

            <!--<p>

                Cum doctus civibus efficiantur in imperdiet deterruisset.

            </p>-->

        </div>

        <div class="row">

            <div class="col-md-3">

                <div class="box_home" id="one">

                    <span>1</span>

                    <h3>Search by address</h3>

                    <p>

                        Find all restaurants available in your zone.

                    </p>

                </div>

            </div>

            <div class="col-md-3">

                <div class="box_home" id="two">

                    <span>2</span>

                    <h3>Choose a restaurant</h3>

                    <p>

                        We have more than 1000s of menus online.

                    </p>

                </div>

            </div>

            <div class="col-md-3">

                <div class="box_home" id="three">

                    <span>3</span>

                    <h3>Pay by card or cash</h3>

                    <p>

                        It's quick, easy and totally secure.

                    </p>

                </div>

            </div>

            <div class="col-md-3">

                <div class="box_home" id="four">

                    <span>4</span>

                    <h3>Delivery or takeaway</h3>

                    <p>

                        You are lazy? Are you backing home?

                    </p>

                </div>

            </div>

        </div><!-- End row -->



        <div id="delivery_time" class="hidden-xs">

            <strong><span>2</span><span>5</span></strong>

            <h4>The minutes that usually takes to deliver!</h4>

        </div>

    </div><!-- End container -->





    <div class="white_bg">

        <div class="container margin_60">



            <div class="main_title">

                <h2 class="nomargin_top">Explore today's specials...</h2>

            </div>



            <div class="row">

                <div class="col-md-6">

                    <a href="detail_page.html" class="strip_list">

                        <div class="ribbon_1">Popular</div>

                        <div class="desc">

                            <div class="thumb_strip">

                                <img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">

                            </div>

                            <div class="rating">

                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>

                            </div>

                            <h3>Antonio's Pizzeria</h3>

                            <div class="type">

                                Antonio's Pizzeria - Caringbah NSW

                            </div>

                            <div class="location">

                                360 The Kingsway, Caringbah NSW 2229. <span class="opening">Opens at 17:00</span>

                            </div>

                            <ul>

                                <li>Take away<i class="icon_check_alt2 ok"></i></li>

                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>

                            </ul>

                        </div><!-- End desc-->

                    </a><!-- End strip_list-->

                    <a href="detail_page.html" class="strip_list">

                        <div class="ribbon_1">Popular</div>

                        <div class="desc">

                            <div class="thumb_strip">

                                <img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">

                            </div>

                            <div class="rating">

                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>

                            </div>

                            <h3>Antonio's Pizzeria</h3>

                            <div class="type">

                                Antonio's Pizzeria - Caringbah NSW

                            </div>

                            <div class="location">

                                360 The Kingsway, Caringbah NSW 2229. <span class="opening">Opens at 17:00</span>

                            </div>

                            <ul>

                                <li>Take away<i class="icon_check_alt2 ok"></i></li>

                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>

                            </ul>

                        </div><!-- End desc-->

                    </a><!-- End strip_list-->

                </div><!-- End col-md-6-->

                <div class="col-md-6">

                    <a href="detail_page.html" class="strip_list">

                        <div class="ribbon_1">Popular</div>

                        <div class="desc">

                            <div class="thumb_strip">

                                <img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">

                            </div>

                            <div class="rating">

                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>

                            </div>

                            <h3>Antonio's Pizzeria</h3>

                            <div class="type">

                                Antonio's Pizzeria - Caringbah NSW

                            </div>

                            <div class="location">

                                360 The Kingsway, Caringbah NSW 2229. <span class="opening">Opens at 17:00</span>

                            </div>

                            <ul>

                                <li>Take away<i class="icon_check_alt2 ok"></i></li>

                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>

                            </ul>

                        </div><!-- End desc-->

                    </a><!-- End strip_list-->

                    <a href="detail_page.html" class="strip_list">

                        <div class="ribbon_1">Popular</div>

                        <div class="desc">

                            <div class="thumb_strip">

                                <img src="{{ url('/') }}/design/front/img/Antonios-Pizzeria.png" alt="">

                            </div>

                            <div class="rating">

                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>

                            </div>

                            <h3>Antonio's Pizzeria</h3>

                            <div class="type">

                                Antonio's Pizzeria - Caringbah NSW

                            </div>

                            <div class="location">

                                360 The Kingsway, Caringbah NSW 2229. <span class="opening">Opens at 17:00</span>

                            </div>

                            <ul>

                                <li>Take away<i class="icon_check_alt2 ok"></i></li>

                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>

                            </ul>

                        </div><!-- End desc-->

                    </a><!-- End strip_list-->

                </div>

            </div><!-- End row -->



        </div><!-- End container -->

    </div><!-- End white_bg -->



    <div class="high_light">

        <div class="container">

            <h3>Choose from over 1100 Restaurants</h3>

            <p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>

            <a href="list_page.html">View all Restaurants</a>

        </div><!-- End container -->

    </div><!-- End hight_light -->



    <section class="parallax-window" data-parallax="scroll" data-image-src="{{ url('/') }}/design/front/img/bg_office.jpg" data-natural-width="1200" data-natural-height="600">

        <div class="parallax-content">

            <div class="sub_content">

                <i class="icon_mug"></i>

                <h3>We also deliver to your office</h3>

                <p>

                    Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.

                </p>

            </div><!-- End sub_content -->

        </div><!-- End subheader -->

    </section><!-- End section -->

	

	

@stop