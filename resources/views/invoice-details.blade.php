@extends("layouts.grambunny")
@section("styles")
{{-- styles goes here --}}
<style type="text/css">
   table {
   width: 100%;
   }
   td, th {
   border: 1px solid #dddddd;
   text-align: left;
   padding: 8px 17px;
   text-align: center;
   }
   .table-responsive {
   border: unset;
   padding: 5px;
   background-color: #fff;
   }
   .parallax-window#short {
   height: 230px;
   min-height: inherit;
   background: 0 0;
   position: relative;
   margin-top: 0px;
   }
   section.parallax-window {
   overflow: hidden;
   position: relative;
   width: 100%;
   background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
   background-attachment: fixed;
   background-repeat: no-repeat;
   background-position: top center;
   background-size: cover;
   }
   #sub_content {
   display: table-cell;
   padding: 50px 0 0;
   font-size: 16px;
   }
   #sub_content h1 {
   margin: 0 0 10px;
   font-size: 28px;
   font-weight: 300;
   color: #F5F0E3;
   text-transform: capitalize;
   }
   #short #subheader {
   height: 230px;
   color: #F5F0E3;
   text-align: center;
   display: table;
   width: 100%;
   }
   div#subheader {
   color: #F5F0E3;
   text-align: center;
   display: table;
   width: 100%;
   height: 380px;
   }
   ul.brad-home {
   padding: 0;
   margin: 0;
   text-align: center;
   }
   .brad-home li {
   display: inline-block;
   list-style: none;
   padding: 5px 10px;
   font-size: 12px;
   }
   #subheader a {
   color: #fff;
   }
   .myOrderActnBox{ margin-top: 20px; }
   .myOrderDtelBtn a{
   border: 1px solid #231f20;
   background-color: #fff;
   color: #231f20;
   font-weight: 700;
   border-radius: 25px;
   padding: 8px 15px;
   min-width: 100px;
   cursor: pointer;
   text-decoration: none;
   }
   .myOrderDtelBtn a:hover {
   background-color: #231f20;
   color: #fff;
   }
   .deepmd{ width: 100%; }
   .deepmd strong{ float: right; }
   .deepon{ font-size: 14px; }
</style>
@endsection
@section("content")
{{-- content goes here --}}
<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">
   <div id="subheader">
      <div id="sub_content" class="animated zoomIn">
         <h1><span class="restaunt_countrt">Order Invoice</span></h1>
         <div id="position">
            <div class="container">
               <ul class="brad-home">
                  <li><a href="https://www.grambunny.com/home">Home</a></li>
                  <li>My Order Invoice</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section class="myOrderSec section-full">
   <div class="container">
      <div class="row">
         @if(Session::has('message'))
         <div class="box-body table-responsive">
            <div class="alert alert-success alert-dismissable">
               <i class="fa fa-check"></i>
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
               {{Session::get('message')}}
            </div>
         </div>
         @endif
         <div class="col-md-2">
         </div>
         <div class="col-md-8">
            <div class="myOdrersBox myOdrersDetailBox">
               <div class="row">
                  <div class="col-md-12">
                     <div class="myOrder invoice_order">
                        <div class="myOrderDtelBox">
                           <div class="myOrderID">
                              <label>Order ID</label>
                              <strong>#{{$order_detail->order_id}}</strong>
                           </div>
                           <div class="myOrderDtel">
                              <label>Quantity</label>
                              <strong>{{ $order_detail->ps_qty}}</strong>
                           </div>
                           <div class="myOrderDtel">
                              <label>Order By</label>
                              <strong>{{$order_detail->first_name}} {{$order_detail->last_name}}</strong>
                              <strong>{{$order_detail->mobile_no}}</strong>
                              <strong>{{$order_detail->email}}</strong>
                           </div>
                           <div class="myOrderDtel">
                              <label>Delivery Address</label>
                              <strong>{{$order_detail->address}},{{$order_detail->city}},{{$order_detail->state}},{{$order_detail->zip}}</strong>
                           </div>
                           <table class="table-responsive order_dt">
                              <tr>
                                 <th>Amount paid</th>
                                 <th>Payment Type</th>
                                 <th>Transaction ID</th>
                                 <th>Payment Status</th>
                                 <th>DATE PAID</th>
                              </tr>
                              <tr>
                                 <td><strong>${{ number_format($order_detail->total, 2) }}</strong></td>
                                 <td><strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong></td>
                                 <td><strong>{{$order_detail->txn_id}}</strong></td>
                                 <td><strong class="complete_status">{{$order_detail->pay_status}}</strong></td>
                                 <td><strong>{{$order_detail->created_at}}</strong></td>
                              </tr>
                           </table>
                           <!-- 	<div class="myOrderDtel">
                              <label>Amount paid</label>
                              <strong>${{ number_format($order_detail->total, 2) }}</strong>
                              </div> -->
                           <!-- <div class="myOrderDtel">
                              <label>Payment Type</label>
                              <strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong>
                              </div> -->
                           <!-- <div class="myOrderDtel">
                              <label>Transaction ID</label>
                              <strong>{{$order_detail->txn_id}}</strong>
                              </div> -->
                           <!-- <div class="myOrderDtel">
                              <label>Payment Status</label>
                              <strong class="complete_status">{{$order_detail->pay_status}}</strong>
                              </div> -->
                           <!-- <div class="myOrderDtel">
                              <label>DATE PAID</label>
                              <strong>{{$order_detail->created_at}}</strong>
                              </div> -->
                           <div class="myOrderActnBox">
                              <div class="msg-box">
                                 <div id="message-box" style="display:none;">
                                    <i class="fa fa-check"></i>
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                    <span id="message"></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section("scripts")
{{-- scripts goes here --}}
<script type="text/javascript">
   $('#submit-request').on('click', function() {
   
   	var order_id = $("#order_id").val();
   
   	$.ajax({
       type: "post",
       headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
       data: $("#return-order-form").serialize(),
       url: "{{url('/submit-return-request')}}",       
       success: function(data) {
   
         if(data.ok){
         	location.reload();
         }else{
         	var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message" style="float:left;">'+data.message+'</span></div>';
               $('.msg-box').html(html);
   	        $("#message-box").addClass("alert alert-danger alert-dismissable");
   	        $("#message").text(data.message);
         }
       }
     });
   });
   
   $('#return_order').on('click', function() {
      $("#return-order-form").css('display', 'block');
   });
</script>
@endsection