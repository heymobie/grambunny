<!DOCTYPE html>

<html>

<head>

  <meta charset="UTF-8">

  <title>Admin | Dashboard</title>

  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @section("style")


  <!-- bootstrap 3.0.2 -->

  <link href="{{ url('/') }}/public/design/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- font Awesome -->

  <link href="{{ url('/') }}/public/design/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <!-- bootstrap wysihtml5 - text editor -->

  <link href="{{ url('/') }}/public/design/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

  <link href="{{ url('/') }}/public/design/admin/css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" />

  <link href="{{ url('/') }}/public/design/admin/css/datepicker/datepicker3.css" rel="stylesheet" />

  <!-- Theme style -->

  <link href="{{ url('/') }}/public/design/admin/css/AdminLTE.css?var=<?php echo rand(); ?>" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->

  @show

  @yield("other_css")

</head>

<body class="skin-blue">

  <!-- header logo: style can be found in header.less -->

  <header class="header">

    <a href="{{ url('/') }}" class="logo" target="_blank">

      <!-- Add the class icon to your logo image or logo icon to add the margining -->

      <img src="{{ url('/public/assets/images/logo.png') }}">

    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top" role="navigation">

      <!-- Sidebar toggle button-->

      <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">

        <span class="sr-only">Toggle navigation</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

      </a>

      <div class="navbar-right">

        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->

          <li class="dropdown user user-menu">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

              <span>Admin<i class="caret"></i></span>

            </a>

            <ul class="dropdown-menu">

              <!-- Menu Footer-->

              <!--<li class="user-footer">

<div class="pull-right">

<a href="{{ url('/admin/general_setting')}}" class="btn btn-default btn-flat">Setting</a>

</div>

</li>-->

              <li class="user-footer">

                <div class="pull-right">

                  <a href="{{ url('/admin/change_password') }}" class="btn btn-default btn-flat">Manage Account</a>

                </div>

              </li>

              <li class="user-footer">

                <!--<div class="pull-left">

                <a href="#" class="btn btn-default btn-flat">Profile</a>

              </div>-->

                <div class="pull-right">

                  <a href="{{ url('/admin/logout') }}" class="btn btn-default btn-flat">Sign out</a>

                </div>

              </li>

            </ul>

          </li>

        </ul>

      </div>

    </nav>

  </header>

  <div class="wrapper row-offcanvas row-offcanvas-left">

    <!-- Left side column. contains the logo and sidebar -->

    <aside class="left-side sidebar-offcanvas">

      <!-- sidebar: style can be found in sidebar.less -->

      <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu" data-widget="tree">

          <li class="@if((Request::segment(2) == 'dashboard'))active @endif">

            <a href="{{ url('/admin/dashboard') }}">

              <i class="fa fa-dashboard"></i> <span>Dashboard</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'allorder')||(Request::segment(2) == 'filter_order_data')||(Request::segment(2) == 'allorder'))active @endif">

            <a href="{{ url('/admin/allorder') }}">

              <i class="fa fa-exchange" aria-hidden="true"></i>

              <span>All Order</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'merchant-logged-status'))active @endif">
            <a href="{{ url('/admin/merchant-logged-status') }}">
              <i class="fa fa-user"></i> <span>Merchants Logged In Status</span>
            </a>
          </li>


          <!--<li class="@if((Request::segment(2) == 'my_wallet'))active @endif">

    <a href="{{ url('/admin/my_wallet') }}">

        <i class="fa fa-dashboard"></i> <span>Wallet Account</span>

    </a>

  </li>-->



          <!-- <li class="@if((Request::segment(2) == 'product-service-merchant-list') || (Request::segment(2) == 'merchant-product-service-list') || (Request::segment(2) == 'merchant-product-service-form') || (Request::segment(2) == 'merchant-product-service-edit'))active @endif">



          <a href="{{ url('/admin/product-service-merchant-list') }}">

            <i class="fa fa-building" aria-hidden="true"></i>

          Product Management</a>

        </li> -->

          <li class="treeview @if((Request::segment(2) == 'merchant-product-service-list') || (Request::segment(2) == 'all-product-list') || (Request::segment(2) == 'product-form') || (Request::segment(2) == 'product-edit') || (Request::segment(2) == 'my-product-list') || (Request::segment(2) == 'product-merchant-list') || (Request::segment(2) == 'product_service_unit') || (Request::segment(2) == 'ps_unit_form') || (Request::segment(2) == 'product_service_category') || (Request::segment(2) == 'product_service_category_form') || (Request::segment(2) == 'product_service_sub_category') || (Request::segment(2) == 'product_service_sub_category_form') || (Request::segment(2) == 'product_service_sub_category_edit') ||(Request::segment(2) == 'product_brands') || (Request::segment(2) == 'product_brand_form') ||(Request::segment(2) == 'product_types') || (Request::segment(2) == 'product_type_form'))active @endif">
            <a href="{{ url('/admin/product-service-merchant-list') }}"><i class="fa fa-th-large"></i> <span>Product Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ url('/admin/all-product-list') }}">All Products</a></li>
              <li><a href="{{ url('/admin/my-product-list') }}">Admin Products</a></li>
              <li><a href="{{ url('/admin/product-merchant-list') }}">Merchant Products</a></li>

               <li>
                <a class="accordion-heading rt-b collapsed" data-toggle="collapse" data-target="#submenu111">Product Categories <span class="pull-right"><b class="caret"></b></span> </a>
                <ul class="nav nav-list ri-vx collapse" id="submenu111" style="height: 0px;">
                  <li><a href="{{ url('/admin/product_service_category') }}" title="Title">Product Category List</a></li>
                 <li><a href="{{ url('/admin/product_service_unit') }}">Product Unit</a></li>
                 <li><a href="{{ url('/admin/product_brands') }}">Product Brands</a></li>
                 <li><a href="{{ url('/admin/product_types') }}">Product Types</a></li>
                </ul>
            </li>
            </ul>
          </li>


          <li class="treeview @if((Request::segment(2) == 'event-management-list') || (Request::segment(2) == 'event-merchant-list') || (Request::segment(2) == 'product_event_category') ||(Request::segment(2) == 'event_service_unit') || (Request::segment(2) == 'event_unit_form'))active @endif">
            <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span>Event Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              
              <li><a href="{{ url('/admin/event-management-list') }}">Admin Event List</a></li>
              <li><a href="{{ url('/admin/event-merchant-list') }}">Merchant Event List</a></li>

             <li>
                <a class="accordion-heading rt-b collapsed" data-toggle="collapse" data-target="#submenu11">Event Categories <span class="pull-right"><b class="caret"></b></span> </a>
                <ul class="nav nav-list ri-vx collapse" id="submenu11" style="height: 0px;">
                  <li><a href="{{ url('/admin/product_event_category') }}" title="Title">Event Category List</a></li>
                  <li><a href="{{ url('/admin/event_service_unit') }}">Event Unit</a></li>
                </ul>
            </li>

            </ul>
          </li>

           <?php 
             $admin_ids  = Auth::guard("admin")->user();

             if($admin_ids->id=='1'){
        ?>

            <li class="@if((Request::segment(2) == 'adminlist') || (Request::segment(2) == 'admin-form'))active @endif">

            <a href="{{ url('/admin/adminlist') }}">

              <i class="fa fa-users" aria-hidden="true"></i>

              <span>Admin Management</span>

            </a>

          </li>

          <?php }?>
              
          <li class="@if((Request::segment(2) == 'merchant-list') || (Request::segment(2) == 'merchant-form') || (Request::segment(2) == 'merchant-product-inventory-list') || (Request::segment(2) == 'merchant-profile') || (Request::segment(2) == 'merchant-inventory-copy') ||(Request::segment(2) == 'order-list')||(Request::segment(2) == 'order_view')||(Request::segment(2) == 'event-list'))active @endif">

            <a href="{{ url('/admin/merchant-list') }}">

              <i class="fa fa-user" aria-hidden="true"></i>

              <span>Merchant Management</span>

            </a>

          </li>

          <li class="@if((Request::segment(2) == 'userlist') || (Request::segment(2) == 'user-form'))active @endif">

            <a href="{{ url('/admin/userlist') }}">

              <i class="fa fa-users" aria-hidden="true"></i>

              <span>Customer Management</span>

            </a>

          </li>

          <li class="@if((Request::segment(2) == 'abandoned-cart') || (Request::segment(2) == 'abandoned-cart-view'))active @endif">

            <a href="{{ url('/admin/abandoned-cart') }}">

              <i class="fa fa-users" aria-hidden="true"></i>

              <span>Report of abandoned carts</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'order_view')||(Request::segment(2) == 'merchant-order'))active @endif">

            <a href="{{ url('/admin/merchant-order') }}">

              <i class="fa fa-exchange" aria-hidden="true"></i>

              <span>Order Management</span>

            </a>

          </li>


         <!--  <li class="@if((Request::segment(2) == 'product_service_category') || (Request::segment(2) == 'product_service_category_form') || (Request::segment(2) == 'product_service_sub_category') || (Request::segment(2) == 'product_service_sub_category_form') || (Request::segment(2) == 'product_service_sub_category_edit'))active @endif">

            <a href="{{ url('/admin/product_service_category') }}">

              <i class="fa fa-building" aria-hidden="true"></i>

              <span>Product Category</span>

            </a>

          </li> -->



         <!--  <li class="@if((Request::segment(2) == 'product_service_unit') || (Request::segment(2) == 'ps_unit_form'))active @endif">

            <a href="{{ url('/admin/product_service_unit') }}">

              <i class="fa fa-building" aria-hidden="true"></i>

              <span>Product Unit</span>

            </a>

          </li> -->

          <!-- <li class="@if((Request::segment(2) == 'product_brands') || (Request::segment(2) == 'product_brand_form'))active @endif">

            <a href="{{ url('/admin/product_brands') }}">

              <i class="fa fa-building" aria-hidden="true"></i>

              <span>Product Brands</span>

            </a>

          </li> -->

         <!--  <li class="@if((Request::segment(2) == 'product_types') || (Request::segment(2) == 'product_type_form'))active @endif">

            <a href="{{ url('/admin/product_types') }}">

              <i class="fa fa-building" aria-hidden="true"></i>

              <span>Product Types</span>

            </a>

          </li> -->

         <!--  <li class="@if((Request::segment(2) == 'event_service_unit') || (Request::segment(2) == 'event_unit_form'))active @endif">

            <a href="{{ url('/admin/event_service_unit') }}">

              <i class="fa fa-building" aria-hidden="true"></i>

              <span>Event Unit</span>

            </a>

          </li> -->


           <li class="treeview @if((Request::segment(2) == 'merchant-product-service-list') || (Request::segment(2) == 'product-form') || (Request::segment(2) == 'product-edit') || (Request::segment(2) == 'my-product-list') || (Request::segment(2) == 'product-merchant-list'))active @endif">
            <a href="{{ url('/admin/product-service-merchant-list') }}"><i class="fa fa-th-large"></i> <span>Coupan code Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <!-- <li><a href="{{ url('/admin/all-product-list') }}">All Products</a></li> -->
              <li><a href="{{ url('/admin/coupan-list') }}">Admin Coupan Code</a></li>
              <li><a href="{{ url('/admin/coupan-merchant-list') }}">Merchant Coupan Code</a></li>
            </ul>
          </li>



          <!--<li class="@if((Request::segment(2) == 'coupon_code') || (Request::segment(2) == 'coupon-form')  || (Request::segment(2) == 'coupon-edit-form'))active @endif">

          <a href="{{ url('/admin/coupon_code') }}">

            <i class="fa fa-tag fa-lg" aria-hidden="true"></i>

            <span>Coupon Code</span>

          </a>

        </li>-->



          <!--<li class="@if((Request::segment(2) == 'subadmin_list') || (Request::segment(2) == 'subadmin-form') || (Request::segment(2) == 'subadmin-edit-form')|| (Request::segment(2) == 'subadmin-view'))active @endif">

          <a href="{{ url('/admin/subadmin_list') }}">

            <i class="fa fa-user-circle" aria-hidden="true"></i>

            <span>Sub Admin Management</span>

          </a>

        </li>-->





          <!--<li class="@if((Request::segment(2) == 'permissions_list') || (Request::segment(2) == 'permission-form') || (Request::segment(2) == 'permission-edit-form')|| (Request::segment(2) == 'permission-view'))active @endif">

          <a href="{{ url('/admin/permissions_list') }}">

            <i class="fa fa-key" aria-hidden="true"></i>

            <span>Sub Admin Permissions</span>

          </a>

        </li>-->





          <!--<li class="@if((Request::segment(2) == 'delivery_manlist') || (Request::segment(2) == 'delivery_man-form') || (Request::segment(2) == 'deliveryman-edit-form')|| (Request::segment(2) == 'deliveryman-view'))active @endif">

          <a href="{{ url('/admin/delivery_manlist') }}">

            <i class="fa fa-gift" aria-hidden="true"></i>

            <span>Delivery Man Management</span>

          </a>

        </li>





  <li class="@if((Request::segment(2) == 'dm_wallet'))active @endif">

    <a href="{{ url('/admin/dm_wallet') }}">

        <i class="fa fa-dashboard"></i> <span>Delivery Man Summary</span>

    </a>

  </li>



        <li class="@if((Request::segment(2) == 'area_list') || (Request::segment(2) == 'area-form') || (Request::segment(2) == 'area-view'))active @endif">

          <a href="{{ url('/admin/area_list') }}">

            <i class="fa fa-map-marker" aria-hidden="true"></i>

            <span>Area Management</span>

          </a>

        </li>-->





          <!--<li class="@if((Request::segment(2) == 'vendor_fgt_pass_req_list'))active @endif">

          <a href="{{ url('/admin/vendor_fgt_pass_req_list') }}">

            <i class="fa fa-user" aria-hidden="true"></i>

            <span>Vendor Password Request</span>

          </a>

        </li>-->







          <!--  <li class="@if((Request::segment(2) == 'cuisinelist') || (Request::segment(2) == 'cuisine-form'))active @endif">

          <a href="{{ url('/admin/cuisinelist') }}">

            <i class="fa fa-cutlery" aria-hidden="true"></i>

            <span>Cuisine Management</span>

          </a>

        </li>



      <li class="@if((Request::segment(2) == 'view_google_form') || (Request::segment(2) == 'get_google_data'))active @endif">

          <a href="{{ url('/admin/view_google_form') }}">

            <i class="fa fa-dashboard"></i> <span>Copy Restaurants by Cuisine data from Google</span>

          </a>

        </li> -->





          <!--  <li class="@if((Request::segment(2) == 'menu_template')||(Request::segment(2) == 'get_templatename') ||(Request::segment(2) == 'template_menu_list'))active @endif">

          <a href="{{ url('/admin/menu_template') }}">

            <i class="fa fa-list-alt" aria-hidden="true"></i>

            <span>Menu Template  </span>

          </a>

        </li> 







        <li class="@if((Request::segment(2) == 'review_list') )active @endif">

          <a href="{{ url('/admin/review_list') }}">

            <i class="fa fa-star" aria-hidden="true"></i>

            <span>Review & rating </span>

          </a>

        </li>-->


          <li class="@if((Request::segment(2) == 'order_report')||(Request::segment(2) == 'order_report')||(Request::segment(2) == 'order_report'))active @endif">

            <a href="{{ url('/admin/order_report') }}">

              <i class="fa fa-exchange" aria-hidden="true"></i>

              <span>Reporting Management</span>

            </a>

          </li>


      <li class="@if((Request::segment(2) == 'product_report'))active @endif">

            <a href="{{ url('/admin/product_report') }}">

              <i class="fa fa-exchange" aria-hidden="true"></i>

              <span>Product sale Reports</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'payment-list') || (Request::segment(2) == 'payment-details'))active @endif">

            <a href="{{ url('/admin/payment-list') }}">

              <i class="fa fa-usd" aria-hidden="true"></i>

              <span>Payment History</span>

            </a>

          </li>

          <li class="@if((Request::segment(2) == 'payment-setting'))active @endif">

            <a href="{{ url('/admin/payment-setting') }}">

              <i class="fa fa-usd" aria-hidden="true"></i>

              <span>Payment Setting</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'banner-image'))active @endif">
            <a href="{{ url('/admin/banner-image') }}">
              <i class="fa fa-upload"></i> <span>Banner Image</span>
            </a>
          </li>


          <li class="@if((Request::segment(2) == 'content_pagelist') || (Request::segment(2) == 'page-form'))active @endif">

            <a href="{{ url('/admin/content_pagelist') }}">



              <i class="fa fa-pencil-square" aria-hidden="true"></i>

              <span>Content & Seo Management</span>

            </a>

          </li>

          <li class="@if((Request::segment(2) == 'faq') || (Request::segment(2) == 'faq-form'))active @endif">

            <a href="{{ url('/admin/faq') }}">

              <i class="fa fa-pencil-square" aria-hidden="true"></i>

              <span>Faq Management </span>

            </a>

          </li>



          <!--<li class="@if((Request::segment(2) == 'socail_pagelist') || (Request::segment(2) == 'social-form'))active @endif">

          <a href="{{ url('/admin/socail_pagelist') }}">

       

            <i class="fa fa-users" aria-hidden="true"></i>

            <span>Social Management </span>

          </a>

        </li>-->



          <li class="@if((Request::segment(2) == 'testimonial_list') || (Request::segment(2) == 'testimonial-form'))active @endif">

            <a href="{{ url('/admin/testimonial_list') }}">

              <i class="fa fa-commenting" aria-hidden="true"></i>

              <span> Testimonial </span>

            </a>

          </li>



          <li class="@if((Request::segment(2) == 'advertisement-list') || (Request::segment(2) == 'advertisement-form'))active @endif">

            <a href="{{ url('/admin/advertisement-list') }}">

              <i class="fa  fa-adn" aria-hidden="true"></i>

              <span> Advertisement & Static Block</span></a>

          </li>



          <!--<li class="@if((Request::segment(2) == 'homepage_content') || (Request::segment(2) == 'homepage-form') || (Request::segment(2) == 'content-form') )active @endif">

          <a href="{{ url('/admin/homepage_content') }}">

          

            <i class="fa fa-align-center" aria-hidden="true"></i>

            <span>Home page content  </span>

          </a>

        </li>



        <li class="@if((Request::segment(2) == 'coupon_request'))active @endif">

          <a href="{{ url('/admin/coupon_request') }}">

            <i class="fa fa-dashboard"></i> <span>special offer requests  </span>

          </a>

        </li> 

         

        <li class="@if((Request::segment(2) == 'suggestion_list'))active @endif">

          <a href="{{ url('/admin/suggestion_list') }}">

            <i class="fa fa-dashboard"></i> <span>Suggest Restaurant Request </span>

          </a>

        </li>-->



          <!--<li class="@if((Request::segment(2) == 'email_content'))active @endif">

          <a href="{{ url('/admin/email_content') }}">

            <i class="fa fa-dashboard"></i> <span>Notification Management</span>

          </a>

        </li>-->



          <!--   <li class="@if((Request::segment(2) == 'reporting-list')||(Request::segment(2) == 'reporting-view')||(Request::segment(2) == 'reporting-details'))active @endif">

          <a href="{{ url('/admin/reporting-list') }}">

            <i class="fa fa-exchange" aria-hidden="true"></i>

            <span>Reporting Management</span>

          </a>

        </li>  -->




          <!--<li class="@if((Request::segment(2) == 'merchant-bank-list') || (Request::segment(2) == 'merchant-bank-details'))active @endif">

          <a href="{{ url('/admin/merchant-bank-list') }}">

            <i class="fa fa-usd" aria-hidden="true"></i>

            <span>Merchant Bank Account</span>

          </a>

        </li>-->



          <!-- <li class="@if((Request::segment(2) == 'membership-management') || (Request::segment(2) == 'membership-management'))active @endif">

          <a href="{{ url('/admin/membership-management') }}">

            <i class="fa fa-user" aria-hidden="true"></i>

            <span>Membership Plan</span>

          </a>

        </li>-->



          <li class="@if((Request::segment(2) == 'review-list') || (Request::segment(2) == 'review-list'))active @endif">

            <a href="{{ url('/admin/review-list') }}">

              <i class="fa fa-star" aria-hidden="true"></i>

              <span>Review Product</span>

            </a>

          </li>



          <li class="@if((Request::segment(2) == 'review-merchant-list') || (Request::segment(2) == 'review-merchant-list'))active @endif">

            <a href="{{ url('/admin/review-merchant-list') }}">

              <i class="fa fa-star" aria-hidden="true"></i>

              <span>Review Merchant</span>

            </a>

          </li>


          <li class="@if((Request::segment(2) == 'review-customer-list') || (Request::segment(2) == 'review-customer-list'))active @endif">

            <a href="{{ url('/admin/review-customer-list') }}">

              <i class="fa fa-star" aria-hidden="true"></i>

              <span>Review Customer </span>

            </a>

          </li>



          <!--<li class="@if((Request::segment(2) == 'active_log'))active @endif">

          <a href="{{ url('/admin/notification_list') }}">

            <i class="fa fa-dashboard"></i> <span>Notification List</span>

          </a>

        </li>



        <li class="@if((Request::segment(2) == 'send_notification'))active @endif">

          <a href="{{ url('/admin/send_notification') }}">

            <i class="fa fa-dashboard"></i> <span>Send Notification to user </span>

          </a>

        </li>-->



          <!--<li class="@if((Request::segment(2) == 'active_log'))active @endif">

          <a href="{{ url('/admin/active_log') }}">

            <i class="fa fa-dashboard"></i> <span>Active Log</span>

          </a>

        </li>

        <li class="@if((Request::segment(2) == 'viewmenu_log'))active @endif">

          <a href="{{ url('/admin/viewmenu_log') }}">

            <i class="fa fa-dashboard"></i> <span>View Menu Log</span>

          </a>

        </li> -->

        </ul>

      </section>

      <!-- /.sidebar -->

    </aside>

    @yield("content")

  </div><!-- ./wrapper -->

  <!-- add new calendar event modal -->

  <script type="text/javascript">
    function delete_wal()

    {

      var conf = confirm("Are you sure to delete this record?");

      if (conf)

      {

        return true;

      } else

      {

        return false;

      }

    }

    function delete_parent(child_val)

    {

      if (child_val > 0)

      {

        alert("Some data connect with that so, please delete 1st that.");

        return false;

      } else

      {

        var conf = confirm("Are you sure to delete this record?");

        if (conf)

        {

          return true;

        } else

        {

          return false;

        }

      }

    }



    /*Confirm to delete a deliveryman */

    function delete_deliverman()

    {

      var conf = confirm("Are You sure to delete this delivery man?");

      if (conf)

      {

        return true;

      } else

      {

        return false;

      }

    }



    /*Confirm to delete a sub-admin*/

    function delete_subAdmin()

    {

      var conf = confirm("Are you sure to delete this sub-admin?");

      if (conf)

      {

        return true;

      } else

      {

        return false;

      }

    }

    function delete_cuisine(child_val)

    {

      if (child_val > 0)

      {

        alert("Some data connect with that so, please delete 1st that.");

        return false;

      } else

      {

        var conf = confirm("Are you sure to delete this record?");

        if (conf)

        {

          return true;

        } else

        {

          return false;

        }

      }

    }

    function delete_template(child_val)

    {

      if (child_val > 0)

      {

        alert("Some data connect with that so, please delete 1st that.");

        return false;

      } else

      {

        var conf = confirm("Are you sure to delete this record?");

        if (conf)

        {

          return true;

        } else

        {

          return false;

        }

      }

    }



    function delete_template_menus(tamplate_id)

    {

      var conf = confirm("Are You sure to delete this menu template and its all menus ?");

      if (conf)

      {

        return true;

      } else {

        return false;

      }

    }
  </script>

  @yield("js_bottom")

<?php if(Request::segment(2) != 'merchant-form' && Request::segment(2) != 'merchant-product-service-form' && Request::segment(2) != 'merchant-product-service-edit' && Request::segment(2) != 'product-form' && Request::segment(2) != 'product-edit' && Request::segment(2) != 'user-form' && Request::segment(2) != 'event-management-edit' && Request::segment(2) != 'event-management-add' && Request::segment(2) != 'event-product-service-form' && Request::segment(2) != 'event-product-service-edit' ) { ?> 
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> 
<?php } ?>

  <script src="{{ url('/') }}/public/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

  <script src="{{ asset('public/design/admin/js/AdminLTE/app.js') }}" type="text/javascript"></script><!-- Input checkbox -->

  <script src="{{ url('/') }}/public/design/admin/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript">
    $(function() {
      $("#datepicker").datepicker({
        // autoclose: true,
        todayHighlight: true
      });
    });
  </script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

  <script type="text/javascript">
    // $('#mondatetimepicker31').datetimepicker(function(){
    //   alert('datetime picker clicked');
    // });

    $(function() {
      $('#mondatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#mondatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#mondatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#mondatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });



    $(function() {
      $('#tuedatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#tuedatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#tuedatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#tuedatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });


    $(function() {
      $('#weddatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#weddatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#weddatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#weddatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });


    $(function() {
      $('#thudatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#thudatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#thudatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#thudatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#fridatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#fridatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#fridatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#fridatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#satdatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#satdatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#satdatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#satdatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#sundatetimepicker31').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#sundatetimepicker32').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#sundatetimepicker33').datetimepicker({
        format: 'LT'
      });
    });

    $(function() {
      $('#sundatetimepicker34').datetimepicker({
        format: 'LT'
      });
    });
  </script>

</body>

</html>