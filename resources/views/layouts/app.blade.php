<!doctype html>

  @if(session()->has('locale'))

    <html class="dynamic-lang" lang="{{session('locale')}}">

  @else

  <?php $app_locale = 'en'; 

        Session::put('locale', $app_locale);

  ?>

    <html class="static-lang" lang="{{session('locale')}}">

  @endif  

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>@lang('translation.onlineFoodOrdering')</title>  

    @section("style")

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/bootstrap.css">

    @if(session()->has('locale') == 'ar')

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/master-ar.css">

    @endif

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/master.css">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/font-awesome.css">

    <!-- GOOGLE WEB FONT -->

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/design/front/css/owl.carousel.css">

    <link rel="stylesheet" href="{{ url('/') }}/public/design/front/css/animate.css">

    <link rel="shortcut icon" href="{{ url('/') }}/public/design/front/img/favicon.ico" type="image/x-icon">



    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/style.css">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/fonts/icomoon/style.css">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/jquery-ui.css">



    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/custom.css">

    <!--<link rel="stylesheet" type="text/css" href="{{ url('/') }}/assets/css/bootstrap.min.css">-->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAK0KfvOdF7mN08ohLo7rPiLeX31SRW1IU&libraries=places"></script>

    <style>

      /* iCheck plugin Minimal skin, black



----------------------------------- */



section.navigation_main {

    position: relative;

    z-index: 999;

    display: none;

}



section.footer {

    float: left;

    width: 100%;

    background-color: #363535;

    padding: 10px 0px;

    display: none;

}



.icheckbox_minimal,



.iradio_minimal {



  display: inline-block;



  *display: inline;



  vertical-align: middle;



  margin: 0;



  padding: 0;



  width: 18px;



  height: 18px;



  background: rgba(255, 255, 255, 0.7) url(iCheck/minimal/minimal.png) no-repeat;



  border: none;



  cursor: pointer;



}



.icheckbox_minimal {



  background-position: 0 0;



}



.icheckbox_minimal.hover {



  background-position: -20px 0;



}



.icheckbox_minimal.checked {



  background-position: -40px 0;



}



.icheckbox_minimal.disabled {



  background-position: -60px 0;



  cursor: default;



}



.icheckbox_minimal.checked.disabled {



  background-position: -80px 0;



}



.iradio_minimal {



  background-position: -100px 0;



}



.iradio_minimal.hover {



  background-position: -120px 0;



}



.iradio_minimal.checked {



  background-position: -140px 0;



}



.iradio_minimal.disabled {



  background-position: -160px 0;



  cursor: default;



}



.iradio_minimal.checked.disabled {



  background-position: -180px 0;



}



/* Retina support */

      html,body{height:100%;}

      .carousel,.item,.active{height:100%;}

      .carousel-inner{height:100%;}

      .fill{ 

        width:100%;height:100%;background-position:center;background-size:cover;    overflow: hidden;

      }

      .carousel-inner > .item {

          -webkit-transition: 0.3s ease-in-out left;

          -moz-transition: 0.3s ease-in-out left;

          -o-transition: 0.3s ease-in-out left;

          transition: 0.3s ease-in-out left;

      }

      .badgebox

      {

          opacity: 0;

      }

      .badgebox + .badge

      {   text-indent: -999999px;

          width: 27px;

      }

      .badgebox:focus + .badge

      {

          box-shadow: inset 0px 0px 5px;

      }

      .badgebox:checked + .badge {

          text-indent: -2px;

      }

    .dropdown-menu > li.kopie > a {

    padding-left:5px;

}

 

.dropdown-submenu {

    position:relative;

}

.dropdown-submenu>.dropdown-menu {

   top:0;left:100%;

   margin-top:-6px;margin-left:-1px;

   -webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;

 }

  

.dropdown-submenu > a:after {

  border-color: transparent transparent transparent #333;

  border-style: solid;

  border-width: 5px 0 5px 5px;

  content: " ";

  display: block;

  float: right;  

  height: 0;     

  margin-right: -10px;

  margin-top: 5px;

  width: 0;

}

 

.dropdown-submenu:hover>a:after {

    border-left-color:#555;

 }



.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {

  text-decoration: none;

}  

section.navigation_main .navbar-nav > li > .dropdown-menu ul.dropdown-menu {

    background-color: #e81e2a;

}



section.navigation_main .navbar-nav > li > .dropdown-menu .dropdown-submenu > a:after {

    border-color: transparent transparent transparent #fff;

}



@media (max-width: 767px) {



  .navbar-nav  {

     display: inline;

  }

  .navbar-default .navbar-brand {

    display: inline;

  }

  .navbar-default .navbar-toggle .icon-bar {

    background-color: #fff;

  }

  .navbar-default .navbar-nav .dropdown-menu > li > a {

    color: red;

    background-color: #ccc;

    border-radius: 4px;

    margin-top: 2px;   

  }

   .navbar-default .navbar-nav .open .dropdown-menu > li > a {

     color: #333;

   }

   .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,

   .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {

     background-color: #ccc;

   }



   .navbar-nav .open .dropdown-menu {

     border-bottom: 1px solid white; 

     border-radius: 0;

   }

  .dropdown-menu {

      padding-left: 10px;

  }

  .dropdown-menu .dropdown-menu {

      padding-left: 20px;

   }

   .dropdown-menu .dropdown-menu .dropdown-menu {

      padding-left: 30px;

   }

   li.dropdown.open {

    border: 0px solid red;

   }



}

 

@media (min-width: 768px) {

  ul.nav li:hover > ul.dropdown-menu {

    display: block;

  }

  #navbar {

    text-align: center;

  }

}  



      @media (max-width: 767px) { 

        body {

          padding-left: 0;

          padding-right: 0;

        }

    section.navigation_main .navbar-nav > li > .dropdown-menu ul.dropdown-menu {

    background-color: #d01c27;

    display: block;

    margin-left: 25px;

    padding-left: 0px;

}

    .dropdown-submenu{position:relative;}

.dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}

.dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}

.dropdown-submenu:hover>a:after{border-left-color:#555;}

.dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}



      }

    </style>



    @show

    @yield("other_css")

  </head>

  <body class="<?php if(Session::get('locale') == 'ar'){ echo "right-to-left";}else{ echo "";}?>">



<?php 

      $restids = '';

      $ccartitem = '0';

      $cartrestname = '';

      $cartsuburb = '';



      if(!Auth::guest()) 

      {

        $user_ids = Auth::user()->id;

        $userkey = 'user_id';

      }

      else

      {

        $user_ids = Session::get('_token');

        $userkey = 'guest_id';

      } 



    $restidds = DB::table('rest_cart')                        

    ->where($userkey, '=' ,trim($user_ids))                  

    ->get();



    if(count($restidds) > 0){



    foreach ($restidds as $key => $value) {



      $restids = $value->rest_id;



     }



    }



  if($restids){ 



  $checkatcd = DB::table('rest_cart')           

  ->select('*')               

  ->where($userkey, '=' ,trim($user_ids))         

  ->where('rest_id', '=' ,trim($restids))          

  ->get();



  $ccartitem = count($checkatcd);



  $getcartsurl = DB::table('restaurant')                               

  ->where('rest_id', '=' ,trim($restids))          

  ->first();



  $cartrestname = $getcartsurl->rest_metatag;

  $cartsuburb = $getcartsurl->rest_suburb;



  } 



?> 

  

  <div class="site-mobile-menu">

      <div class="site-mobile-menu-header">

          <div class="site-mobile-menu-close mt-3">

              <span class="icon-close2 js-menu-toggle"></span>

          </div>

      </div>

      <div class="site-mobile-menu-body"></div>

  </div>



    <div class="top-header-area"  data-aos="fade-up" data-aos-delay="150">

    <div class="container h-100">

    <div class="row h-100 align-items-center">

      <!-- <div class="col-6">

        <div class="top-header-content">

          <a href="#"><i class="fa fa-envelope"></i> <span>info.colorlib@gmail.com</span></a>

          <a href="#"><i class="fa fa-phone"></i> <span>(12) 345 6789</span></a>

        </div>

      </div> -->

      <div class="col-12">

        <div class="top-header-content">

          <div class="top-social-area ml-auto">  

          <div class="tpOpsn"><i class="fa fa-mobile"></i> <span>Download our mobile app - </span><a href="#">NOW</a></div>

          <!-- <div class="tpOpsn"><i class="fa fa-comments"></i> <span></span><a href="#">Live Chat</a></div> -->

          <div class="pull-right">

              <li>

                  <a href="{{ route('merchant.login') }}" class="cta"><span class="text-white round">Merchant Login</span></a>

              </li>



        

              <?php if(is_null(Auth::guard("user")->user())){ ?>

                  <li>

                  <a href="{{ route("signInPage",["redirect_url" => url()->current()]) }}" class="cta"><span class="text-white round">Customer Login</span></a> 



                 </li>

               <?php } ?>



               <?php 

                //$ps_slug = session('ps_slug'); 

                $ps_qty = session('ps_qty');

                if(empty($ps_qty)){ $ps_qty = 0; } ?>



                <li class="cart_icon_sec dcart">

                <a <?php if(!empty($ps_qty)){ ?>href="{{ url('/').'/checkout' }}" <?php } ?> ><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>

                <b>(</b><b id="session_qty">{{$ps_qty}}</b><b>)</b>

                </li>



          </div>

          <!--           

            <div class="tpOpsn"><i class="fa fa-headphones"></i>

              <select>

                <option>Help</option>

                <option>Option 1</option>

                <option>Option 2</option>

                <option>Option 3</option>

              </select>

            </div> -->

            

          </div>

        </div>

      </div>

    </div>

    </div>

  </div>



    <header class="site-navbar py-0 " role="banner"  data-aos="fade-up" data-aos-delay="200">



       <div class="container">

        <div class="row align-items-center">

          

          <div class="col-md-3">

            <h1 class="mb-0 site-logo"><a href="{{ url('/') }}" class="text-white mb-0"><img class="logoMail" src="{{asset("public/assets/images/footlogo.png")}}"></a></h1>

          </div>

          <div class="col-md-9 d-none d-xl-block d-md-block d-sm-block d-lg-block ">

            <nav class="site-navigation position-relative text-right" role="navigation">



              <ul class="site-menu js-clone-nav mr-auto d-none d-md-block d-sm-block d-lg-block">

                <li class="active"><a href="{{ url('/') }}">Home</a></li> 

                <li><a href="{{ route("page.aboutUs") }}">About us</a></li>



                  <li><a href="{{ route("page.help") }}">Help</a></li>

                            <li><a href="{{ route("page.faq") }}">FAQ</a></li>

                                <li><a href="{{route("page.contactUs")}}">Contact</a></li>

                <!-- <li><a href="login.html">Log In</a></li> -->

                @if(Auth::guard("user")->check())

                                <li class="has-children"><a href="#" class="cta"><span class="text-white round">{{ Auth::guard("user")->user()->name.' '

                                .Auth::guard("user")->user()->lname }}</span></a>

                                <ul class="dropdown">

                                    <li><a href="{{ route("userAccount") }}">My Account</a></li>

                                    <li><a href="{{ route("myorders") }}">My Orders</a></li>

                                    <li><a href="{{ route("userLogout") }}">Logout</a></li>

                               

                                </ul></li>

                                @else

                                <!-- <li><a href="{{ route("signInPage") }}" class="cta"><span class="text-white round">Log in/Register</span></a></li> -->

                                @endif

              </ul>

            </nav>

          </div>





         <div class="d-md-none d-sm-block ml-auto py-3 col-3 text-black " style="position: relative; top: 3px; display: none;">

                        <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>

                    </div>



        </div>

      </div> 

  </header>





    <!-- End Header =============================================== --> 

    @yield("content")  

<div class="tpOpsn for-FixedChat"><a href="#"><i class="fa fa-comment fa-flip-horizontal"></i> <span>Live Chat</span></a></div>

        <footer class="site-footer">

                <div class="footFirst"  data-aos="fade-up" data-aos-delay="200">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-3">

                                        <div class="footlogo"><img src="{{asset("public/assets/images/footer_logo.png")}}"></div>

                                        <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>

                                    </div>



                                    <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Company</h2>

                                        <ul class="list-unstyled">

                                            <li><a href="{{ route("page.aboutUs") }}">About Us</a></li>  

                                            <li><a href="{{route("page.contactUs")}}">Contact Us</a></li>

                                        </ul>

                                    </div>

                                     <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Other Links</h2>

                                        <ul class="list-unstyled">

                                            <li><a href="{{ route("page.help") }}">Help</a></li>

                                            <li><a href="{{ route("page.faq") }}">Faq</a></li>

                                        </ul>

                                    </div>



                                    <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Legal</h2>

                                        <ul class="list-unstyled">

                                            <!--<li><a href="#">Refund Policy</a></li>-->

                                            <li><a href="{{ route("page.terms") }}">Terms</a></li>

                                            <li><a href="{{ route("page.privacy") }}">Privacy Policy</a></li>

                                        </ul>

                                    </div>

                                    

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="footSecond"  data-aos="fade-up" data-aos-delay="250">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="foot_msg"><p>© 2019 grambunny. All Rights Reserved.</p></div>

                            </div>

                            <div class="col-md-6">

                                <div class="foot_Img "><a href="#"><img src="{{ asset("public/assets/images/android.png")}}"></a><a href="#"><img src="{{ asset("public/assets/images/ios.png")}}"></a></div>

                            </div>

                        </div>

                    </div>

                </div>

            </footer> 



<?php die; ?>

<!-- Start old footer section -->



   <section class="footer wow fadeInUp" style="display: none;">

      <div class="container">

        <div class="row">

          <div class="col-md-4 col-sm-4">

            <div class="footr-menu">

             

        <?php

        $lang = Session::get('locale');

        

        if($lang == 'ar'){

          $about = DB::table('pages')->where('page_status','=','1')->where('page_id','=','13')->where('page_lang', '=' ,'ar')->get();

          

          

        } else {

          $about = DB::table('pages')->where('page_status','=','1')->where('page_id','=','1')->where('page_lang', '=' ,'en')->get();

          

        } 

        // $about = DB::table('pages')->where('page_status','=','1')->where('page_id','=','1')->get();

          

        if($about)

        {

        ?>

        

        

              <h2><?php echo $about[0]->page_title;?></h2>

              <p><?php 

        

                 $string = $about[0]->page_content;

                  if (strlen($string) > 200) {

                  // truncate string

                  $stringCut = substr($string, 0, 200);

                

                  // make sure it ends in a word so assassinate doesn't become ass...

                  $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... <a href="#0" data-toggle="modal" data-target="#view_aboutpopup" style="color:#FFFFFF">Read More</a>   '; 

                }

              

               echo $string;

        ?></p>

        

        <?php }?>

            </div>

          </div>

          <div class="col-md-4 col-sm-4">

            <div class="footr-menu">

              <h2>@lang('translation.onlineRestaurant')</h2>

             <ul>

        <?php if($about)

        {

          if ($lang == 'ar') {

            echo '<li><a href="'.url('/content/about-us').'">'.$about[0]->page_title.'</a></li>';

          } else {

           echo '<li><a href="'.url('/content/'.$about[0]->page_url).'">'.$about[0]->page_title.'</a></li>';

          }            

        }

        ?>

               <!-- <li><a href="#">Restaurants Near Me</a></li>-->

          <?php

          $blog = DB::table('pages')->where('page_status','=','1')->where('page_id','=','7')->get();

        // if($blog)

        // {

        

    //            echo '<li><a href="'.url('/content/'.$blog[0]->page_url).'">'.$blog[0]->page_title.'</a></li>';

        

        // }

        ?>

        <li><a href="<?php echo url('/vendor'); ?>" target="_blank">@lang('translation.owners')</a></li>

        

         <?php

        if($lang == 'ar'){

          $career = DB::table('pages')->where('page_status','=','1')->where('page_id','=','34')->where('page_lang', '=' ,'ar')->get();

        } else {

          $career = DB::table('pages')->where('page_status','=','1')->where('page_id','=','6')->where('page_lang', '=' ,'en')->get();

        }

        if($career)

        {

          if($lang == 'ar'){

            echo '<li><a href="'.url('/content/careers').'">'.$career[0]->page_title.'</a></li>';

          } else {

            echo '<li><a href="'.url('/content/'.$career[0]->page_url).'">'.$career[0]->page_title.'</a></li>';

          }     

                  

        }



        if($lang == 'ar'){

          $touch = DB::table('pages')->where('page_status','=','1')->where('page_id','=','38')->where('page_lang', '=' ,'ar')->get();

        } else {

          $touch = DB::table('pages')->where('page_status','=','1')->where('page_id','=','2')->where('page_lang', '=' ,'en')->get();

        }     

          

        if($touch)

        {

          if($lang == 'ar'){

            echo '<li><a href="'.url('/content/get-in-touch').'">'.$career[0]->page_title.'</a></li>';

          } else {

            echo '<li><a href="'.url('/content/'.$touch[0]->page_url).'">'.$touch[0]->page_title.'</a></li>';

          }           

        }

        ?>

              </ul>

            </div>

          </div>

          <div class="col-md-4 col-sm-4" >

            <div class="footr-menu">

      

      <?php 

    

    if($lang == 'ar'){

      $cuisine = DB::table('cuisine')   

          ->select('*') 

          ->where('cuisine_status', '=' , '1')

          ->where('cuisine_lang', '=' ,'ar')

          ->orderBy('cuisine_id', 'desc')

          ->get();

      

    } else {

      $cuisine = DB::table('cuisine')   

          ->select('*') 

          ->where('cuisine_status', '=' , '1')

          ->where('cuisine_lang', '=' ,'en')

          ->orderBy('cuisine_id', 'desc')

          ->get();

      

    } 

                

     

          if($cuisine){ 

      ?>

              <h2>@lang('translation.cuisine')</h2>

              <ul>

        

                <!--<li><a href="#">Pizza</a></li>

                <li><a href="#">Chinese</a></li>

                <li><a href="#">Indian</a></li>

                <li><a href="#">Thai</a></li>

                <li><a href="#">Sushi</a></li>

                <li><a href="#">More...</a></li>-->

        

        <?php 

        $city_c=1;

           $count = count($cuisine);

          foreach($cuisine as $citylist){ 

          

          ?>

           <li>

            <a href="<?php echo url('/restaurant_listing/cuisine/'.str_replace(' ','_',$citylist->cuisine_name)) ;?>"><?php  echo $citylist->cuisine_name;?></a></li>

              <?php   

                        

            $city_c++;  

            if($city_c>5)         

            {

              break;

            }

          }     

          if($count>5)      

          {

          ?>

          

          <li><a href="#0" data-toggle="modal" data-target="#cuisine_list">@lang('translation.more')...</a></li>

          <?php 

          }

        

        ?>

        

        

        </ul>

        <?php }?>

            </div>

          </div>

          <!-- <div class="col-md-2 col-sm-2">

            <div class="footr-menu">

      

      <?php 

      

                

     $restaurant_city = DB::table('restaurant')   

          ->select('rest_suburb') 

          ->where('rest_status', '=' , 'PUBLISHED') 

          ->where('rest_suburb', '!=' , '')         

          ->orderBy('rest_id', 'desc')

          ->groupBy('rest_suburb')

          ->get();



      //print_r($restaurant_city) ;

          

      ?>

      

      

      

              <h2>@lang('translation.cities')</h2>

              <ul>

          <?php 

        

        if($restaurant_city){

        $city_c=1;

           $count = count($restaurant_city);

          foreach($restaurant_city as $citylist){ 

          

          ?>

           <li>

            <a href="<?php echo url('/restaurant_listing/location/'.str_replace(' ','_',$citylist->rest_suburb)) ;?>"><?php  echo $citylist->rest_suburb;?></a></li>

          <?php   

                        

            $city_c++;  

            if($city_c>5)         

            {

              break;

            }

          }     

          if($count>5)      

          {

          ?>

          

          <li><a href="#0" data-toggle="modal" data-target="#cities_list">More...</a></li>

          <?php 

          }

        }

        

        ?>

              </ul>

            </div>

          </div> -->

        </div>

        <div class="row">

          <div class="col-md-12">

            <div class="ftr-text">

              <div class="col-md-4 col-sm-4">

                <div class="ftr-menu">

                  <ul>

            <?php

                      

            

            if($lang == 'ar'){

              $ploci = DB::table('pages')->where('page_status','=','1')->where('page_id','=','35')->where('page_lang', '=' ,'ar')->get();

            } else {

              $ploci = DB::table('pages')->where('page_status','=','1')->where('page_id','=','5')->where('page_lang', '=' ,'en')->get();

            }

            if($ploci)

            {

              

              if($lang == 'ar'){

                echo '<li><a href="'.url('/content/privacy-policy').'">'.$ploci[0]->page_title.'</a></li>';

              } else {

                echo '<li><a href="'.url('/content/'.$ploci[0]->page_url).'">'.$ploci[0]->page_title.'</a></li>';

              }

            }



            if($lang == 'ar'){

              $term = DB::table('pages')->where('page_status','=','1')->where('page_id','=','36')->where('page_lang', '=' ,'ar')->get();

            } else {

              $term = DB::table('pages')->where('page_status','=','1')->where('page_id','=','4')->where('page_lang', '=' ,'en')->get();

            }



            if($term)

            {

              if($lang == 'ar'){

                echo '<li><a href="'.url('/content/terms-condition').'">'.$term[0]->page_title.'</a></li>';

              } else {

                echo '<li><a href="'.url('/content/'.$term[0]->page_url).'">'.$term[0]->page_title.'</a></li>';

              }             

            }



            

    

            ?>

                  </ul>

                </div>

              </div>

              <div class="col-md-4 col-sm-4">

                <div class="top-bar-left">

                    <?php

        $socila_link = DB::table('social_link')->where('social_status','=','1')->get();

        if($socila_link)

        {

        ?>

           <ul>

        <?php

          foreach($socila_link as $slink)

          {

            if($slink->social_id==1)

            {

              echo '<li><a href="'.$slink->social_link.'" target="_blank"><i class="fa fa-facebook" style="margin-top:5px;"></i></a></li>';

            }

            if($slink->social_id==2)

            {

              echo '<li><a href="'.$slink->social_link.'" target="_blank"><i class="fa fa-twitter" style="margin-top:5px;"></i></a></li>';

            }

            if($slink->social_id==3)

            {

              echo '<li><a href="'.$slink->social_link.'" target="_blank"><i class="fa fa-linkedin" style="margin-top:5px;"></i></a></li>';

            }

          

          }       

        ?>

             </ul>

        <?php 

        }

        ?>

                </div>

              </div>

              <div class="col-md-4 col-sm-4">

                <div class="ftr-text-bot">

                  <p>@lang('translation.copyRights')</a></p>

                </div>

              </div>

            </div>

          </div>

          

        </div>

      </div>

    </section> 





<!-- end old footer section -->

  

    <?php 

  if($about)

  {

  ?>

    <div class="modal fade" id="view_aboutpopup" role="dialog">

        <div class="modal-dialog"> 

        

        <!-- Modal content-->

        <div class="modal-content">

          <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

            <h4><?php echo $about[0]->page_title;?> </h4>

          </div>

          <div class="modal-body popup-ctn">

              <?php echo $about[0]->page_content;?> 

          <p>

          

            

          </p>

      

          </div>

        </div>

        </div>

      </div>

  <?php 

  }

  ?>  

  

  

    @yield("js_bottom") 

    <style>

      #ajax_parner_loddder {

        position: fixed;

        top: 0;

        left: 0;

        width: 100%;

        height: 100%;

        background:rgba(27, 26, 26, 0.48);

        z-index: 1001;

      }

      #ajax_parner_loddder img {

        top: 50%;

        left: 46.5%;

        position: absolute;

      }

      .footer-wrapper {

          float: left;

          width: 100%;

      }

      #addons-modal.modal {

        z-index: 999;

      }

    

        button.submit_partnern {

    margin-top: 8px;

    margin-bottom: 10px;

    background-color: #e81e2a;

    color: #fff !important;

}



#register label.error {

bottom: -5px !important;;

}

    </style>



    <div id="ajax_parner_loddder" style="display:none;">

      <div align="center" style="vertical-align:middle;">

        <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

      </div>

    </div>

  

  

<!-- Forgot Password modal -->

<div class="login modal fade" id="forgot_2" tabindex="-1" role="dialog" aria-labelledby="myForgot" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content modal-popup">

          <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

           <form class="forgt_form" name="fwd_frm" id="fwd_frm" role="form" method="POST" action="{{ url('/user_password/user_forgot_password') }}">

            

            {{ csrf_field() }}

      

      <!-- @if (session('status'))

                <div class="alert alert-success">

                    {{ session('status') }}

                </div>

            @endif -->



            @if (session('status'))

          <div class="alert alert-success alert-block">

              <button type="button" class="close" data-dismiss="alert">×</button>

              {{ session('status') }}

          </div>

          @endif

         <div class="login_icon"><i class="icon_lock_alt"></i></div>      

               <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control form-white" placeholder="@lang('translation.emailAddress')" required>

               <button type="submit" id="fwd_btn" class="btn btn-submit"> Send OTP</button>

            </form>

        </div>

    </div>

</div>

<!-- End modal -->



    <div class="login modal fade" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">

      <div class="modal-dialog">

        <div class="modal-content modal-popup">

          <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

          <form class="popup-form" id="myLogin" action="{{ url('auth/login') }}" method="post">

            {{ csrf_field() }}

            <div class="login_icon"><i class="icon_lock_alt"></i></div>

            <div class="alert alert-info fade in alert-dismissable" id="alert_info_fav" style="display:none">

              <a href="#" class="close" data-dismiss="alert">&times;</a>

              <strong>Info!</strong> Please login for favourite any restaurant.

            </div>

            

            <div class="alert alert-danger fade in alert-dismissable" id="alert_logerror" style="display:none">

              <a href="#" class="close" data-dismiss="alert">&times;</a>

              Invalid email address/mobile number or password.

            </div>

            

            @if(Session::has('login_message_error'))

            

            <div class="form-group has-error">

              <span class="help-block">

                <strong style="color:#FFFFFF;">  {{Session::get('login_message_error')}}</strong>                

              </span>

            </div>

            @endif

            

            <div class="input-filed">

              <input id="user_login" type="text" name="email" value="<?php if(isset($_COOKIE["useremail"])) { echo $_COOKIE["useremail"]; }else{ echo "" ; } ?>" class="form-control form-white" placeholder="@lang('translation.emailMobileNumber')" required>

            </div>



            <div class="input-filed">

              <input  id="password" type="password"  name="password" class="form-control form-white" placeholder="@lang('translation.password')" value="<?php if(isset($_COOKIE["upassword"])) { echo $_COOKIE["upassword"]; }else{ echo ""; } ?>"  required="required">

            </div>



            <div class="text-left dfpleft">

              <a href="#0" data-toggle="modal" data-target="#forgot_2" id="pws_forgot">@lang('translation.forgotPassword')</a>

            </div>



          <div class="dtext-right">

            <input type="checkbox" id="logremem" name="remember" <?php if(isset($_COOKIE["uremember"])) { echo 'checked'; } ?>/> @lang('translation.rememberMe')

         </div>

            

            <!-- <div style="line-height: 20px;">

              <hr class="social-media-login-border-left"><span class="or-dividr" style="text-align: center;width: 60px;display: inline-block;"> Or </span><hr class="social-media-login-border-right">

            </div>

           <div style="  text-align: center;">Login With</div>

            <div class="cleafix"></div> -->

            <div class="login-with-social-icons">

              

              

              <a href="#"><img src="{{ url('/') }}/design/front/img/facebook.png" height="35" width="35" /></a>

        

        

       <a href="#"><img src="{{ url('/') }}/design/front/img/googleplus.png" height="35" width="35" /></a>

        

      <!--a href="redirect/facebook">Login in with Facebook</a> 

      <a href="/redirect/google">Login in with Google +</a-->       

              

            </div>

            <button type="submit" id="signin" class="btn btn-submit">@lang('translation.login')</button>

            

          </form>

        </div>

      </div>

    </div>

   



    <!-- Register modal -->

    <div class="ragister modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">

      <div class="modal-dialog">

        <div class="modal-content modal-popup">

          <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

          <form action="{{ url('/user_register') }}" class="popup-form" id="myRegister" method="post" >

            <div class="login_icon"><i class="icon_lock_alt"></i></div>

            

            <div class="input-filed">

              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.firstName')" name="name" id="name" required>

            </div>



            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.lastName')" name="lname" id="lname" required>

            </div>



            <input type="hidden" name="user_mob_type" id="user_mob_type" value="1">



            <input type="hidden" name="user_status" id="user_status" value="1">



      <!--<div class="input-filed">

      

      <select name="user_mob_type" id="user_mob_type"  class="form-control">            

            <option value="1">Alltel</option>

            <option value="2">AT&T</option>

            <option value="3">Boost Mobile</option>

            <option value="4">Sprint</option>

            <option value="5">T-Mobile</option>

            <option value="6">U.S. Cellular</option>

            <option value="7">Verizon</option>

            <option value="8">Virgin Mobile</option>

            <option value="9">Republic Wireless</option>

          </select>

         </div>-->



            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.mobileNumber')" name="user_mob" id="user_mob" required number="number" maxlength="10">

              <div id="number_msg" class="eror_msg" style="display:none; color:#FF0000">An account with this phone number is already registered. Use a different phone number. If you forgot your user ID or password, go to Forgot Password.</div>

        

            </div>

            

            <div class="input-filed">

              <input type="email" class="form-control form-white" placeholder="@lang('translation.emailAddress')" name="email" id="email" required>

              <div id="email_msg"  class="eror_msg" style="display:none; color:#FF0000">An account with this email ID is already registered. Use a different email ID. If you forgot your user ID or password, go to Forgot Password.</div>

            </div>



            <div class="input-filed">

              <input type="password" class="form-control form-white" placeholder="@lang('translation.password')" id="password1" name="password" required minlength="8">

            </div>



            <div class="input-filed">

              <input type="password" class="form-control form-white" placeholder="@lang('translation.confirmPassword')" id="password-confirm" name="password_confirmation" required minlength="8">

            </div>

      

    

              <input type="hidden" class="form-control form-white" value="House" id="dummy1" name="dummy1" readonly>

      

            

              <input type="hidden" class="form-control form-white" value="House" id="dummy2" name="dummy2" readonly>

            

            

              <input type="hidden" class="form-control form-white" value="House" id="dummy3" name="dummy3"  readonly="readonly">

           

            <div id="pass-info" class="clearfix"></div>



            <!-- <div class="checkbox-holder text-left">

              <div class="checkbox">

              <input type="checkbox" value="accept_2" id="check_2" name="check_2" required/>

                  <div class="control__indicator"></div>

                <label class="control control--checkbox" for="check_2">

                  <span>I Agree to the <strong>Terms &amp; Conditions</strong></span>                 

                </label>

              </div>

            </div> -->

  

            <input type="hidden" value="1" id="two_factor" name="two_factor" />

             



            <div class="fieldset">

              <div class="chckbox">

                <input type="checkbox" value="accept_2" id="check_2" name="check_2" required/>

                <label for="check_2" class="check-in"></label> <span class="che-condition">@lang('translation.iAgree') <strong>

        <?php       

        if($term)

        {

          if($lang == 'ar'){

            echo '<a href="'.url('/content/terms-condition').'"  style="color:#545454" target="_blank">' ?> @lang('translation.termsConditions')<?php echo '</a>';

          } else {

            echo '<a href="'.url('/content/'.$term[0]->page_url).'"  style="color:#545454" target="_blank">' ?> @lang('translation.termsConditions')<?php echo '</a>';

          } 

          

        }

        else{   ?>

          @lang('translation.termsConditions')

        <?php }

        ?></strong></span>

              </div>

            </div>

      

      

            <!-- <div class="text-left">

              <a href="#0" data-toggle="modal" data-target="#forgot_2" id="pws_forgot" style="color:#545454">Forgot Password?</a>

            </div> -->

      

            <button type="button" class="btn btn-submit margin_30" id="btn_register">@lang('translation.register')</button>

            <!--<div style="line-height: 20px;">

              <hr class="social-media-login-border-left">

              <span class="or-dividr" style="text-align: center;width: 60px;display: inline-block;">

               Or 

              </span>

              <hr class="social-media-login-border-right">

            </div>

            <div style=" text-align:center;">Register With</div>

            <div class="cleafix"></div>

            <div class="login-with-social-icons">

      

       <a href=" "><img src="{{ url('/') }}/design/front/img/facebook.png" height="35" width="35" /></a> 



       <a href=" "><img src="{{ url('/') }}/design/front/img/googleplus.png" height="35" width="35" /></a>



            </div>-->

          </form>

        </div>

      </div>

    </div>

  <!-- End Register modal -->

  

  

    

  

    <!-- RESTAURANT OWNER  modal -->

  <div class="ragister modal fade rest_owner" id="rest_owner" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">

      <div class="modal-dialog">

        <div class="modal-content modal-popup">

          <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

      

      

           <form class="popup-form" id="become_partner" method="post">

              <div style="font-weight:bold">

              @lang('translation.vendorRegistration')

          <!--Just add a few short details and we'll be in touch-->

          </div>



            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.firstName')" name="name" id="vendor_name" required>

            </div>

      

            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.lastName')" name="lname" id="vendor_lname" required>

            </div>



      

            <div class="input-filed">         

          

               <input id="vendor_number" type="text" name="contact_number"  class="form-control form-white" placeholder="@lang('translation.mobileNumber')" required number="number" maxlength="10">

            </div>



            <div class="input-filed">

             <input id="vendor_emailId" type="email" name="email"  class="form-control form-white" placeholder="@lang('translation.emailAddress')" required>

              <div id="vendor_email_msg"  class="eror_msg" style="display:none; color:#FF0000 !important;">An account with this email  is already registered. Use a different email.</div>

            </div>          



            <div class="input-filed">

              <input type="password" class="form-control form-white" placeholder="@lang('translation.password')" id="vendor_password1" name="password" required>

            </div>

      

      

            <div class="input-filed">

              <input type="password" class="form-control form-white" placeholder="@lang('translation.confirmPassword')" id="vendor_cpassword1" name="verndo_cpassword" required><!--equalTo="#vendor_password1"-->

        <span id="error_msg_vendor" style="display:none; color:#e81e2a; bottom: -7px; font-size: 12px; float: left;">The confirm password and the password do not match.</span>

            </div>



        <?php $cate_detail = DB::table('vendor_category')->where('status', '=' ,1)->get(); ?>  



          <div class="input-filed">

           

              <select class="form-control" id="vendor_category" name="vendor_category" required>

              <option value="">--@lang('translation.selectCategory')--</option>

              <?php foreach ($cate_detail as $key => $value) { ?>

              <option value="<?php echo $value->id; ?>"><?php echo $value->category; ?></option>

              <?php } ?>

              </select>



            </div>  



      

            <div id="pass-info" class="clearfix"></div>

           <button type="button" id="submit_partner" class="btn btn-submit submit_partner">@lang('translation.btnSubmit')</button>

          </form>

        </div>

      </div>

    </div>

  

  <div class="modal fade" id="partner_modalsucess" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content modal-popup">

                <a href="#" class="close-link" style="color:#000000;"><i class="icon_close_alt2"></i></a>

                    <!--<div class="login_icon"><i class="icon_lock_alt"></i></div>-->

          

            <div style="font-weight:bold">

          

              <br><br>

          Thanks for submitting your details, we'll be in touch with you soon.

            </br></br>

          </div>

                    

          

                    <div class="cleafix"></div>

                    

            </div>

        </div>

    </div>

  <!-- End RESTAURANT OWNER modal -->

  

  

    <style>

.pac-container.pac-logo {

z-index: 999999999;

}

</style>

     

    <!--   SUGGEST RESTAUANRT  modal -->

  <div class="ragister modal fade rest_owner" id="suggest_restaurant" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">

      <div class="modal-dialog">

        <div class="modal-content modal-popup">

          <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

      

      

           <form class="popup-form" id="user_suggest_restaurant" method="post">

              <div style="font-weight:bold"> @lang('translation.tellUsWhichRest') </div>

            

            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.restName')" name="suggest_rest_name" id="rest_suggest_address"  onFocus="RestaddressAndzipcode()" required>

            </div>

      

            <div class="input-filed">

              <input type="text" class="form-control form-white" placeholder="@lang('translation.restZipCode')" name="suggest_rest_zipcode"  number="number" id="rest_suggest_zipcode" readonly required>

            </div>



      

            <div class="input-filed">   

               <input  type="text" name="suggest_user_name"  class="form-control form-white" placeholder="@lang('translation.yourName')" value="<?php if(!Auth::guest()){ echo Auth::user()->name;}?>" required>

            </div>



            <div class="input-filed">

             <input  type="email" name="suggest_user_email"  class="form-control form-white" placeholder="@lang('translation.emailAddress')" value="<?php if(!Auth::guest()){ echo Auth::user()->email;}?>" required>

            </div>          

      

            <div id="pass-info" class="clearfix"></div>

           <button type="button" id="submit_suggetion" class="btn btn-submit submit_partnern">@lang('translation.btnSubmit')</button>

          </form>

        </div>

      </div>

    </div>

  

  <div class="modal fade" id="suggest_modalsucess" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content modal-popup">

             <a href="#" class="close-link" style="color:#000000;"><i class="icon_close_alt2"></i></a>

            <div style="font-weight:bold">

          

              <br><br>

                Thanks! We'll reach out to you once we add the restaurant.

              </br></br>

          </div>

                    

          

                    <div class="cleafix"></div>

                    

            </div>

        </div>

    </div>

  <!-- End SUGGEST RESTAUANRT OWNER modal -->

  

  

    <!--   CITIES  MODAL START -->

  <div class="modal fade" id="cities_list" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content modal-popup">

             

         <div class="modal-header">

                 <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

         <div style="font-weight:bold">

          All Cities          

         </div>

                 </div>

                 <div class="modal-body all-city-name">            

          <div style="font-weight:bold">

          

             <ul>

            <?php 

            

            if($restaurant_city){

              foreach($restaurant_city as $citylist){ 

              

              ?>

               <li>

                <a style="color:#000000;" href="<?php echo url('/restaurant_listing/location/'.str_replace(' ','_',$citylist->rest_suburb)) ;?>"><?php  echo $citylist->rest_suburb;?></a></li>

              <?php   

              }   

            }

            

            ?>

            </ul>

          </div>

                 </div>                    

                 <div class="cleafix"></div>                    

            </div>

        </div>

    </div>

  

    <!--   CUISINE  MODAL END-->

    

    <!--   CITIES  MODAL START -->

  <div class="modal fade" id="cuisine_list" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">

        <div class="modal-dialog">

            <div class="modal-content modal-popup">

             <div class="modal-header">

             <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>

            

         <div style="font-weight:bold">

          All Cuisine

          

          </div>

               </div>

            

      <div class="modal-body all-city-name"> 

      

            <div style="font-weight:bold">

          

            <?php 

      

                

     $cuisine = DB::table('cuisine')    

          ->select('*') 

          ->where('cuisine_status', '=' , '1')

          ->orderBy('cuisine_id', 'desc')

          ->get();

          if($cuisine){ 

      ?>

             

              <ul>

        

        <?php 

        $city_c=1;

           $count = count($cuisine);

          foreach($cuisine as $citylist){ 

          ?>

           <li>

            <a style="color:#000000" href="<?php echo url('/restaurant_listing/cuisine/'.str_replace(' ','_',$citylist->cuisine_name)) ;?>"><?php  echo $citylist->cuisine_name;?></a></li>

        <?php               

          }             

        ?>

        

        

              </ul>

        <?php }?>

          </div>

               </div>     

                    

          

                    <div class="cleafix"></div>

                    

            </div>

        </div>

    </div>

  

  

  

    

    <div class="modal fade" id="login_confirm_model" role="dialog">

  <div class="modal-dialog"> 

    

    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

      </div>

      <div class="modal-body popup-ctn">



        <p id="alert_confirm_msg">Thank You! <p>



        <p> Your registration is successful processed. </p>



       <p id="alert_confirm_msg">

        OTP has been sent on your Email. Please enter in below text box and verify it 

      </p>

      <form name="login_otp_verify_frm" id="login_otp_verify_frm" method="post">

      <input type="hidden" name="otp_user_id" id="otp_user_id" value="">

        <p> 

        Please enter OTP.       

      </p>

      <p> 

        <input type="text" name="user_otp_number" id="login_user_otp_number" value="" required number="number" maxlength="6" minlength="6"/>  

        <span id="login_otp_error_message" style="display:none"></span></p> 

              

      </p>  

      <p>

        <button type="button" class="get_conf_val btn-default" id="submit_login_otp">Submit</button>

        <!--<button type="button" class="get_conf_val btn-default" id="resend_otp">Resend OTP</button>--> 

      </p>

      </form>

        

      <!--<p>

        <button type="button" class="login_otp_conf_val btn-default" data-val="1">OK</button>

      </p>-->

      </div>

    </div>

  </div>

</div>

     

    <!--   CITIES  MODAL END-->

   

  <script src="{{ url('/') }}/public/design/jquery.validate.min.js"></script>

  <script src="{{ url('/') }}/public/design/front/js/functions.js"></script> 

  <script src="{{ url('/') }}/public/design/front/js/jquery-1.9.1.min.js"></script> 

  <script src="{{ url('/') }}/public/design/front/js/owl.carousel.js"></script>





<script>

 (function($){

  $(document).ready(function(){

    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {

      event.preventDefault(); 

      event.stopPropagation(); 

      $(this).parent().siblings().removeClass('open');

      $(this).parent().toggleClass('open');

    });

  });

})(jQuery);

  </script>

  <script>

    /*PARTNER MODEL VALUE */

    $('#partner_modal').on('hidden.bs.modal', function (e) {

      $(this)

        .find("input,textarea,select,hidden")

        .val('')

        .end()

    });



    $(document).on('click', '#submit_partner_shouta_party', function(){

      var form = $("#become_partner");

      form.validate();

      var valid = form.valid();

        if(valid){      

        $('#partner_modal').modal('hide');

        $("#ajax_parner_loddder").show(); 



        var frm_val = $('#become_partner').serialize();       

          $.ajax({

          type: "POST",

          url: "{{url('/become_partner')}}",

          data: frm_val,

          success: function(msg) {

          //  alert(msg);

          if(msg=='1')

          {

            $("#ajax_parner_loddder").hide(); 

            $('#partner_modalsucess').modal('show');

              return false;

          }

          else if(msg=='2')

          {

          //$(form).submit();

            return true;          

          }

          }

        });

      }

      else

      {

        return false;

      } 

    });



     





$(document).on('click', '#resend_otp', function(){ 

      

  $('#login_confirm_model').modal('hide');

  $("#ajax_parner_loddder").show();   

    ///var frm_val = $('#myRegister').serialize();    

  $.ajax({

    type: "POST",

    url: "{{url('/resendotp')}}",

    data: {},

    dataType: 'json',

    success: function(msg) {

      $("#ajax_parner_loddder").hide(); 

      if(msg.status=='3')

      {

        $('#register').modal('show');

        $('#email_msg').show();

        return false;

      }

      else if(msg.status=='4')

      { 

        $('#otp_user_id').val(msg.user_id);         

        $('#login_confirm_model').modal('show');             

        return false;   

            

      }

    }

  });

}); 



  

  

  

  



    $(document).on('click', '#submit_partner', function(){

      $("#error_msg_vendor").hide();

  

      $('#vendor_email_msg').hide();

      

      jQuery.validator.addMethod("vname", function (value, element) {

        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

          return true;

        } else {

          return false;

        };

      });



      jQuery.validator.addMethod("lname", function (value, element) {

        if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

          return true;

        } else {

          return false;

        };

      });



      jQuery.validator.addMethod("pass", function (value, element) {

        if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {

          return true;

        } else {

          return false;

        };

      });

            

      var form = $("#become_partner");

      form.validate({

        rules: {

          name:{

            required:true,

            vname:true

          },

          lname: {

            required: true,

            lname:true

          },

          password: {

            required: true,

            minlength:8,

            pass:true,

          },

          password_confirmation: {

            required: true,

            minlength:8,

            equalTo : "#password",

          },

          verndo_cpassword: {

            required: true,

            minlength:8,

            equalTo : "#vendor_password1",

          }

        },

          messages: {

            name:{

              required:'Please enter name.',

              vname:"Please enter only letters."

            },

            lname: {

              required:'Please enter last name.',

              lname:'Please enter only letters.'

            },

            password: {

              required:'Please enter password.',

              minlength:'Password must be at least 8 characters.',

              pass:"at least one number, one lowercase and one uppercase letter.",

            },

            password_confirmation: {

              required:'Please enter confirm password.',

              minlength:'Password must be at least 8 characters.',

              equalTo:'confirm password and password should be same, please enter correct.'

            },

            verndo_cpassword: {

              required:'Please enter confirm password.',

              minlength:'Password must be at least 8 characters.',

              equalTo:'confirm password and password should be same, please enter correct.'

            }

          }

      });

      var valid = form.valid();

        if(valid){  

    

    if(($("#vendor_password1").val())==($("#vendor_cpassword1").val()))   

    {

        

      $('#rest_owner').modal('hide');

      $("#ajax_parner_loddder").show(); 



          var frm_val = $('#become_partner').serialize(); 

        

      $.ajax({

          type: "POST",

          url: "{{url('/check_vendor_duplicateemail')}}",

          data: frm_val,

          success: function(msg) {

        

              if(msg=='1')

            {

          

          

              $("#ajax_parner_loddder").hide(); 

              $('#rest_owner').modal('show');

              $('#vendor_email_msg').show();

                return false;

            }

            else if(msg=='2')

            {

                $.ajax({

              type: "POST",

              url: "{{url('/become_partner')}}",

              data: frm_val,

              success: function(msg) {

              //  alert(msg);

              if(msg=='1')

              {

                $("#ajax_parner_loddder").hide(); 

                $('#partner_modalsucess').modal('show');

                  return false;

              }

              else if(msg=='2')

              {

              //$(form).submit();

                return true;          

              }

              }

            }); 

              return false;

            }

          }

        }); 

    }

    else

    {

      $("#error_msg_vendor").show();

      

        return false;

    } 

        

      

      }

      else

      {

        return false;

      } 

    });  

  

  



    $(document).on('click', '#btn_register', function(){ 



      jQuery.validator.addMethod("pass", function (value, element) {

        if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {

          return true;

        } else {

          return false;

        };

      });



      var form = $("#myRegister");

      form.validate({

        rules: {

          password: {

            required: true,

            minlength:8,

            pass:true,

          },

          password_confirmation: {

            required: true,

            minlength:8,

            equalTo : "#password1",

          }

        },

        messages: {

          password: {

            required:'Please enter password.',

            minlength:'Password must be at least 8 characters.',

            pass:"at least one number, one lowercase and one uppercase letter.",

          },

          password_confirmation: {

            required:'Please enter confirm password.',

            minlength:'Password must be at least 8 characters.',

            equalTo:'confirm password and password should be same, please enter correct.'

          }

        }

      });

      var valid = form.valid();

        if(valid){    

        $('#register').modal('hide');

        $("#ajax_parner_loddder").show();   

          var frm_val = $('#myRegister').serialize();   

          //console.log(frm_val);

          $.ajax({

          type: "POST",

          url: "{{url('/check_user_duplicateemail')}}",

          data: frm_val,

        dataType: 'json',

          success: function(msg) {

            console.log(msg);

            console.log(msg.status);

          $("#ajax_parner_loddder").hide(); 

            if(msg.status=='1')

            {

              $('#register').modal('show');

              $('#email_msg').show();

              $('#number_msg').show();

              return false;

            }

            else if(msg.status=='2')

            {

              $('#register').modal('show');

              $('#email_msg').show();

              $('#number_msg').hide();

              return false;

            }

            else if(msg.status=='3')

            {

              $('#register').modal('show');

              $('#number_msg').show();

              $('#email_msg').hide();

              return false;

            }

            else if(msg.status=='4')

            { 

             /*$('#otp_user_id').val(msg.user_id);          

             $('#login_confirm_model').modal('show'); 

              return false;*/   

            

            $('#otp_user_id').val(msg.user_id);         

             $('#login_confirm_model').modal('show');            

              return false;   

                  

            }

          }

        });

      }

      else

      {

        return false;

      } 

    }); 



    $(document).on('click', '#myLogin', function(){ 

      var form = $("#signin");

      form.validate();

      var valid = form.valid();



    }); 



     $('#myLogin').on('submit', function() { 



      var user_login = $('#user_login').val();

      var password = $('#password').val();

      var remember = '';



      if ($('#logremem').is(":checked"))

      {



        remember = $('#logremem').val();



      }



      $.ajax({

      type: "POST",

      url: "{{url('/check_user_login')}}",

      data: { user_login : user_login, password : password, remember : remember},

      dataType: 'json',

        success: function(msg) { 

        

        if(msg=='0'){



        $('#alert_logerror').show();

       /* setTimeout(function(){

           $("#alert_logerror").remove();

        }, 5000 ); */



        }else{ 



         var respns = 'yes';



         $('#alert_logerror').hide();



         //$( "#myLogin" ).submit();



        window.location.href = "{{url('/restaurant_listing')}}"; //'https://otlubli.com/demo/restaurant_listing';



        }

                       

        }

      });



   

      return false;

   



    }); 



    $(document).on('click', '#pws_forgot', function(){

      $('#login_2').modal('hide');

      $('#register').modal('hide');

      $('#forgot_2').modal('show');

    });



    $(document).on('click', '#fwd_btn', function(){

      var form = $("#fwd_frm");

      form.validate();

      var valid = form.valid();

    });



    $(document).on('click', '#target_register', function(){

      $('#login_2').modal('hide');

      $('#forgot_2').modal('hide');

      $('#register').modal('show');

    });



    $(document).on('click', '#target_signin', function(){

      $('#login_2').modal('show');

      $('#forgot_2').modal('hide');

    });



    </script>

      @if ($errors->has('email'))

      <script>

        //$(document).ready(function(){

          $('#login_2').modal('show');

        //});

      </script>

      @endif

      @if(Session::has('login_message_error'))

      <script>

        //$(document).ready(function(){

          //setTimeout(function(){

            $('#login_2').modal('show');

          //},5000);

          //$('#login_2').modal('show');

        //});

    </script>

    @endif

    @if(session('status'))

    <script>

    

        $('#forgot_2').modal('show');

    /*  $(document).ready(function(){

    

     });*/

    </script>

    @endif

    <script>

      $(document).on('keyup', '#user_login', function(){

        //alert($(this).val());

        var val = $(this).val();

        if(isNaN(val.charAt(0)))    {

          $('#user_login').prop("type","email");

          $('#user_login').removeAttr("maxlength");

          $('#user_login').removeAttr("number");

        }

        else

        {      

          $('#user_login').attr("number","number");

          $('#user_login').attr("maxlength","10");

          $('#user_login').prop("type","text");      

        }

      });

    

    

        

 $(document).on('click', '#show_suggest_restaurant', function(){

     

    document.getElementById('user_suggest_restaurant').reset();

      $('#suggest_restaurant').modal('show');

        

    var input_1 = document.getElementById('rest_suggest_address');



   var options = {

     types: ['address'] 

   /* , componentRestrictions: {

     country: 'NJ'

     }*/

   };

   autocomplete_1 = new google.maps.places.Autocomplete(input_1);

   google.maps.event.addListener(autocomplete_1, 'place_changed', function() {



     var place_1 = autocomplete_1.getPlace();

     for (var i = 0; i < place_1.address_components.length; i++) {

     for (var j = 0; j < place_1.address_components[i].types.length; j++) {

       if (place_1.address_components[i].types[j] == "postal_code") {

       $('#rest_suggest_zipcode').val(place_1.address_components[i].long_name);

       }

     }

     }

   });

 });

 

  

  

  

  $(document).on('click', '#submit_suggetion', function(){ 



    jQuery.validator.addMethod("validname", function (value, element) {

      if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

        return true;

      } else {

        return false;

      };

    });

  

  var form = $("#user_suggest_restaurant");

      form.validate({

        rules: {

          suggest_user_name:{

            required:true,

            validname:true

          }

        },

          messages: {

            suggest_user_name:{

              required:'Please enter name.',

              validname:"Please enter only letters."

            }

          }

      });

      var valid = form.valid();

    if(valid)

    {

        $('#suggest_restaurant').modal('hide');

      $("#ajax_favorite_loddder").show();

          

    

    var frm_val = $('#user_suggest_restaurant').serialize();

      $.ajax({

      type: "POST",

      url: "{{url('/send_restaurant_request')}}",

      data: frm_val,

      dataType: 'json',

        success: function(msg) {

           $("#ajax_favorite_loddder").hide();           

           $("#suggest_modalsucess").modal('show');               

        }

      });



    }

    

  });

  

  

    

function RestaddressAndzipcode() {

 

 var input_1 = document.getElementById('rest_suggest_address');



 var options = {

   types: ['address'] 

 /* , componentRestrictions: {

     country: 'NJ'

   }*/

 };

 autocomplete_1 = new google.maps.places.Autocomplete(input_1);

 google.maps.event.addListener(autocomplete_1, 'place_changed', function() {



   var place_1 = autocomplete_1.getPlace();

   for (var i = 0; i < place_1.address_components.length; i++) {

     for (var j = 0; j < place_1.address_components[i].types.length; j++) {

       if (place_1.address_components[i].types[j] == "postal_code") {

         $('#rest_suggest_zipcode').val(place_1.address_components[i].long_name);

       }

     }

   }

 });



 

}



$(document).on('click', '#submit_login_otp', function(){



$('#otp_error_message').hide();



  var form = $("#login_otp_verify_frm");

  form.validate();

  var valid = form.valid(); 

  if(valid)

  {       

  

  

    

    var frm_val = $('#login_otp_verify_frm').serialize()+'&'+$('#myRegister').serialize();    

          

    $.ajax({

          type: "POST",

          url: "{{url('/user_registration_otp')}}",

          data: frm_val,

          dataType: 'json',

          success: function(msg) {

          

               var red_url = "{{url('/')}}"+msg.return_url;

          //alert(red_url);

            if(msg.status==1)

            {

              

              $('#login_otp_error_message').html(msg.sucess_message);               

              $('#login_otp_error_message').show();

            }

            else if(msg.status==2)

            {

               $('#login_confirm_model').modal('hide');

               window.location.href = red_url;

            }

          }

      });

  }

    



});



    </script>

    <script type="text/javascript">

  $.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

  });

  </script>

  <script type="text/javascript">

     $(document).ready(function(){

            //-- Click on detail

            $("ul.menu-items > li").on("click",function(){

                $("ul.menu-items > li").removeClass("active");

                $(this).addClass("active");

            })



            $(".attr,.attr2").on("click",function(){

                var clase = $(this).attr("class");



                $("." + clase).removeClass("active");

                $(this).addClass("active");

            })



            //-- Click on QUANTITY

            $(".btn-minus").on("click",function(){

                var now = $(".section > div > input").val();

                if ($.isNumeric(now)){

                    if (parseInt(now) -1 > 0){ now--;}

                    $(".section > div > input").val(now);

                }else{

                    $(".section > div > input").val("1");

                }

            })            

            $(".btn-plus").on("click",function(){

                var now = $(".section > div > input").val();

                if ($.isNumeric(now)){

                    $(".section > div > input").val(parseInt(now)+1);

                }else{

                    $(".section > div > input").val("1");

                }

            })                        

        }) 

</script>

  </body>

</html>

