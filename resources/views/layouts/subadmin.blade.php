<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Sub-Admin | Dashboards</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  @section("style")
  <!-- bootstrap 3.0.2 -->
  <link href="{{ url('/') }}/design/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- font Awesome -->
  <link href="{{ url('/') }}/design/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- bootstrap wysihtml5 - text editor -->
  <link href="{{ url('/') }}/design/admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
  <link href="{{ url('/') }}/design/admin/css/timepicker/bootstrap-timepicker.min.css" rel="stylesheet"/>
  <link href="{{ url('/') }}/design/admin/css/datepicker/datepicker3.css" rel="stylesheet"/>
  <!-- Theme style -->
  <link href="{{ url('/') }}/design/admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        @show
        @yield("other_css")
      </head>
      <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
          <a href="{{ url('/admin/dashboard') }}" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
            Sub-Admin
          </a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
              <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span>Sub-Admin<i class="caret"></i></span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- Menu Footer-->
                    <!-- <li class="user-footer">
                      <div class="pull-right">
                        <a href="{{ url('/sub-admin/general_setting')}}" class="btn btn-default btn-flat">Setting</a>
                      </div>
                    </li> -->
                    <!-- <li class="user-footer">
                      <div class="pull-right">
                        <a href="{{ url('/sub-admin/change_password') }}" class="btn btn-default btn-flat">Manage Account</a>
                      </div>
                    </li> -->
                    <li class="user-footer">
                                    <!--<div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                      </div>-->
                                      <div class="pull-right">
                                        <a href="{{ url('/sub-admin/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                      </div>
                                    </li>
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </nav>
                        </header>
                        <div class="wrapper row-offcanvas row-offcanvas-left">
                          <!-- Left side column. contains the logo and sidebar -->
                          <aside class="left-side sidebar-offcanvas">
                            <!-- sidebar: style can be found in sidebar.less -->
                            <section class="sidebar">
                              <!-- sidebar menu: : style can be found in sidebar.less -->
                              <ul class="sidebar-menu">

                             <?php $subpermision  =  DB::table('sub_admin_dashboard_menus')->get(); ?>

                             <?php $surl = Request::segment(2); ?>

                              <?php foreach ($subpermision as $key => $value) { ?>
                          
                               <?php if(($value->menu_id==1) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'dashboard'))active @endif">
                                  <a href="{{ url('/sub-admin/dashboard') }}">
                                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                  </a>
                                </li>

                              <?php } ?>

                             <?php if(($value->menu_id==2) && ($value->access_permissions==1)){ ?>
                                <li class="@if((Request::segment(2) == 'userlist') || (Request::segment(2) == 'user-form'))active @endif">
                                  <a href="{{ url('/sub-admin/userlist') }}">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    <span>User Management</span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==3) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'delivery_manlist') || (Request::segment(2) == 'delivery_man-form') || (Request::segment(2) == 'deliveryman-edit-form')|| (Request::segment(2) == 'deliveryman-view'))active @endif">
                                  <a href="{{ url('/sub-admin/delivery_manlist') }}">
                                    <i class="fa fa-gift" aria-hidden="true"></i>
                                    <span>Delivery man Management</span>
                                  </a>
                                </li>

                              
                              <?php } ?>

                             <?php if(($value->menu_id==4) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'area_list') || (Request::segment(2) == 'area-form') || (Request::segment(2) == 'area-view'))active @endif">
                                  <a href="{{ url('/sub-admin/area_list') }}">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span>Area Management</span>
                                  </a>
                                </li>


                              <?php } ?>

                            <?php if(($value->menu_id==5) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'vendorlist') || (Request::segment(2) == 'vendor-form') || (Request::segment(2) == 'vendor_profile'))active @endif">
                                  <a href="{{ url('/sub-admin/vendorlist') }}">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                    <span>Vendor Management</span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==7) && ($value->access_permissions==1)){ ?>


                                <li class="@if((Request::segment(2) == 'restaurant-form') || (Request::segment(2) == 'restaurant_list') || (Request::segment(2) == 'menu_list') || (Request::segment(2) == 'menu-form')|| (Request::segment(2) == 'menu_categorylist') || (Request::segment(2) == 'category-form') || (Request::segment(2) == 'menu_cate_item_list') || (Request::segment(2) == 'category-item-form') || (Request::segment(2) == 'restaurant_view'))active @endif">



                                  <a href="{{ url('/sub-admin/restaurant_list') }}">
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                  Restaurant Management</a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==8) && ($value->access_permissions==1)){ ?>


                                <li class="@if((Request::segment(2) == 'menu_template')||(Request::segment(2) == 'get_templatename'))active @endif">
                                  <a href="{{ url('/sub-admin/menu_template') }}">
                                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <span>Menu Template  </span>
                                  </a>
                                </li>


                              <?php } ?>

                            <?php if(($value->menu_id==9) && ($value->access_permissions==1)){ ?>

                                <li>
                                  <a href="{{ url('/sub-admin/orderlisting') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-exchange" aria-hidden="true"></i>
                                    <span>Order Management</span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==10) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'review_list') || (Request::segment(2) == 'page-form'))active @endif">
                                  <a href="{{ url('/sub-admin/review_list') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <span>Review & rating </span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==11) && ($value->access_permissions==1)){ ?>

                                <li>
                                  <a href="{{ url('/sub-admin/paymentlisting') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                    <span>Payment Management </span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==12) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'content_pagelist') || (Request::segment(2) == 'page-form'))active @endif">
                                  <a href="{{ url('/sub-admin/content_pagelist') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                    <span>Content Management </span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==13) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'socail_pagelist') || (Request::segment(2) == 'socail-form'))active @endif">
                                  <a href="{{ url('/sub-admin/socail_pagelist') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    <span>Social Management </span>
                                  </a>
                                </li>


                              <?php } ?>

                            <?php if(($value->menu_id==14) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'testimonial_list') || (Request::segment(2) == 'testimonial-form'))active @endif">
                                  <a href="{{ url('/sub-admin/testimonial_list') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-commenting" aria-hidden="true"></i>
                                    <span> Testimonial </span>
                                  </a>
                                </li>


                              <?php } ?>

                             <?php if(($value->menu_id==15) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'homepage_content') || (Request::segment(2) == 'homepage-form'))active @endif">
                                  <a href="{{ url('/sub-admin/homepage_content') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-align-center" aria-hidden="true"></i>
                                    <span>Home page content  </span>
                                  </a>
                                </li>

                              
                              <?php } ?>

                             <?php if(($value->menu_id==16) && ($value->access_permissions==1)){ ?>

                               <li class="@if((Request::segment(2) == 'send_notification'))active @endif">
                                  <a href="{{ url('/sub-admin/send_notification') }}">

                                    <i class="fa fa-envelope-square" aria-hidden="true"></i>
                                    <span>Send Notification to user </span>
                                  </a>
                                </li>
                             
                               
                              <?php } ?>

                             <?php /* if(($value->menu_id==17) && ($value->access_permissions==1)){ ?>

                                <li class="@if((Request::segment(2) == 'email_content'))active @endif">
                                  <a href="{{ url('/sub-admin/email_content') }}">
                                    <!-- <i class="fa fa-dashboard"></i>  -->
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <span>Email Template Content </span>
                                  </a>
                                </li>

                              <?php } */ ?>

                                 <?php } ?>

                              </ul>
                            </section>
                            <!-- /.sidebar -->
                          </aside>
                          @yield("content")
                        </div><!-- ./wrapper -->
                        <!-- add new calendar event modal -->
                        <script type="text/javascript">
                          function delete_wal()
                          {
                           var conf=confirm("Are you sure to delete this record?");
                           if(conf)
                           {
                            return true;
                          }
                          else
                          {
                            return false;
                          }
                        }
                        function delete_parent(child_val)
                        {
                         if(child_val>0)
                         {
                          alert("Some data connect with that so, please delete 1st that.");
                          return false;
                        }
                        else
                        {
                          var conf=confirm("Are you sure to delete this record?");
                          if(conf)
                          {
                           return true;
                         }
                         else
                         {
                           return false;
                         }
                       }
                     }
                     /*Confirm to delete a sub-admin*/
                     function delete_subAdmin()
                     {
                       var conf=confirm("Are you sure to delete this sub-admin?");
                       if(conf)
                       {
                        return true;
                      }
                      else
                      {
                        return false;
                      }
                    }
                    function delete_cuisine(child_val)
                    {
                     if(child_val>0)
                     {
                      alert("Some data connect with that so, please delete 1st that.");
                      return false;
                    }
                    else
                    {
                      var conf=confirm("Are you sure to delete this record?");
                      if(conf)
                      {
                       return true;
                     }
                     else
                     {
                       return false;
                     }
                   }
                 }
                 function delete_template(child_val)
                 {
                   if(child_val>0)
                   {
                    alert("Some data connect with that so, please delete 1st that.");
                    return false;
                  }
                  else
                  {
                    var conf=confirm("Are you sure to delete this record?");
                    if(conf)
                    {
                     return true;
                   }
                   else
                   {
                     return false;
                   }
                 }
               }

               function delete_template_menus(tamplate_id)
               {
                var conf=confirm("Are You sure to delete this menu template and its all menus ?");
                if(conf)
                {
                 return true;
               }else{
                 return false;
               }
             }
           </script>
           @yield("js_bottom")
         </body>
         </html>