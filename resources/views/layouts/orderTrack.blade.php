<!DOCTYPE html>

<html lang="en">

    <head>

        <title>grambunny | @yield("title")</title>

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="app-url" content="{{ asset('/') }}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{asset("public/assets/fonts/icomoon/style.css")}}" type="text/css">

        <link href="{{asset("public/assets/fonts/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css">

        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/magnific-popup.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/jquery-ui.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/owl.carousel.min.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/owl.theme.default.min.css")}}">

        <link rel="stylesheet"  type="text/css"href="{{asset("public/assets/css/bootstrap-datepicker.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/fonts/flaticon/font/flaticon.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/aos.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/rangeslider.css")}}">

        <!--  <link href="css/animate.min.css" rel="stylesheet">   -->

        <link rel="shortcut icon" href="{{ asset('public/design/front/img/favicon.ico') }}" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/style.css")}}">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/custom.css")}}">

        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset("public/assets/css/xzoom.css")}}" media="all" /> 

          <script>

              window.mapState = {

                  initMap: false

                }

                

                function initMap () {

                    window.mapState.initMap = true

                }

                </script>

                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkGCDT06dfMUewInZ5ScKw4R9i6qMVhCo&libraries=places&callback=initMap"></script>


  {{-- <script type="text/javascript">

    (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);



var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})



$(function(){


  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('.post-review-box');

  var newReview = $('#new-review');

  var openReviewBtn = $('#open-review-box');

  var closeReviewBtn = $('#close-review-box');

  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)

  {

    reviewBox.slideDown(400, function()

      {

        $('#new-review').trigger('autosize.resize');

        newReview.focus();

      });

    // openReviewBtn.fadeOut(100);

    closeReviewBtn.show();

  });



  closeReviewBtn.click(function(e)

  {

    e.preventDefault();

    reviewBox.slideUp(300, function()

      {

        newReview.focus();

        openReviewBtn.fadeIn(200);

      });

    closeReviewBtn.hide();

    

  });



  $('.starrr').on('starrr:change', function(e, value){

    ratingsField.val(value);

  });

});

  </script> --}}





      <!--   CITIES  MODAL END-->

{{-- <script>

  $(document).ready(function() {



    var owl = $("#owl-demo");



    owl.owlCarousel({

  autoPlay : 3000,

      stopOnHover : true,

    items : 3, //10 items above 1000px browser width

    itemsDesktop : [1000,3], //5 items between 1000px and 901px

    itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px

    itemsTablet: [600,1], //2 items between 600 and 0;

    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option

    

    });



    // Custom Navigation Events

    $(".next").click(function(){

      owl.trigger('owl.next');

    })

    $(".prev").click(function(){

      owl.trigger('owl.prev');

    })

  });

</script> --}}

        @yield("styles")

        

    </head>

    <body>

        <div class="site-wrap" id="app">

            <div class="site-mobile-menu ">

                <div class="site-mobile-menu-header">

                    <div class="site-mobile-menu-close mt-3">

                        <span class="icon-close2 js-menu-toggle"></span>

                    </div>

                </div>

                <div class="site-mobile-menu-body"></div>

            </div>

            <div class="top-header-area"  data-aos="fade-up" data-aos-delay="150">

            <div class="container h-100">

            <div class="row h-100 align-items-center">

                <!-- <div class="col-6">

                    <div class="top-header-content">

                        <a href="#"><i class="fa fa-envelope"></i> <span>info.colorlib@gmail.com</span></a>

                        <a href="#"><i class="fa fa-phone"></i> <span>(12) 345 6789</span></a>

                    </div>

                </div> -->

                <div class="col-12">

                    <div class="top-header-content">

                        <div class="top-social-area ml-auto">

                            <div class="tpOpsn"><i class="fa fa-mobile"></i> <span>Download our mobile app - </span><a href="#">NOW</a></div>

                            <!-- <div class="tpOpsn"><i class="fa fa-comments"></i> <span></span><a href="#">Live Chat</a></div> -->

                            <div class="pull-right">

                            <li>

                                <a href="{{ route('merchant.login') }}" class="cta"><span class="text-white round">Merchant Login</span></a>

                            </li>

                            <li>

                                <a href="{{ route("signInPage") }}" class="cta"><span class="text-white round">Customer Login</span></a>

                            </li>

                            

                            <?php 

                            $ps_slug = session('ps_slug'); $ps_qty = session('ps_qty');

                            if(empty($ps_slug)){ $ps_qty = 0; } ?>



                            <li class="cart_icon_sec dcart">

                            <a <?php if(!empty($ps_slug)){ ?>href="{{ url('/').'/'.$ps_slug.'/checkout' }}" <?php } ?> ><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>

                            <b>({{$ps_qty}})</b>

                            </li>

                                

                                

                            </div>

                            <!--

                            <div class="tpOpsn"><i class="fa fa-headphones"></i>

                                <select>

                                    <option>Help</option>

                                    <option>Option 1</option>

                                    <option>Option 2</option>

                                    <option>Option 3</option>

                                </select>

                            </div> -->

                            

                        </div>

                    </div>

                        </div>

                    </div>

                </div>

            </div>

            <header class="site-navbar container py-0 " role="banner"  data-aos="fade-up" data-aos-delay="200">

                <!-- <div class="container"> -->

                <div class="row align-items-center">

                    

                    <div class="col-md-3 col-sm-9 col-9">

                        <h1 class="mb-0 site-logo"><a href="{{ url('/') }}" class="text-white mb-0"><img class="logoMail" src="{{asset("public/assets/images/footlogo.png")}}"></a></h1>

                    </div>

                    <div class="col-md-9 d-md-block d-sm-none d-none">

                        <nav class="site-navigation position-relative text-right" role="navigation">

                            <ul class="site-menu js-clone-nav mr-auto d-md-block d-sm-none">

                                <li class="active"><a href="{{ url('/') }}">Home</a></li>

                            <li><a href="{{ route("page.aboutUs") }}">About us</a></li>

                                

                                {{-- <li class="has-children">

                                    <a href="#"  >About</a>

                                    <ul class="dropdown">

                                        <li><a href="#">The Company</a></li>

                                        <li><a href="#">The Leadership</a></li>

                                        <li><a href="#">Philosophy</a></li>

                                        <li><a href="#">Careers</a></li>

                                    </ul>

                                </li> --}}

                                 

                                

                            <li><a href="{{ route("page.help") }}">Help</a></li>

                            <li><a href="{{ route("page.faq") }}">FAQ</a></li>

                                <li><a href="{{route("page.contactUs")}}">Contact</a></li>

                                <!-- <li><a href="login.html">Log In</a></li> -->

                                @if(Auth::guard("user")->check())

                                <li class="has-children"><a href="#" class="cta"><span class="text-white round">{{ Auth::guard("user")->user()->name.' '

                                .Auth::guard("user")->user()->lname }}</span></a>

                                <ul class="dropdown">

                                    <li><a href="{{ route("userAccount") }}">My Account</a></li>

                                    <li><a href="{{ route("userUpdatePassword") }}">Update Password</a></li>

                                    <li><a href="#">My Orders</a></li>

                                    <li><a href="{{ route("userLogout") }}">Logout</a></li>

                                    

                                </ul></li>

                                @else

                                <!-- <li style="display: none;"><a href="{{ route("signInPage") }}" class="cta"><span class="text-white round">Log in/Register</span></a></li> -->

                                @endif

                            </ul>

                        </nav>

                    </div>

                    <div class="d-md-none d-sm-block ml-auto py-3 col-3 text-black" style="position: relative; top: 3px;">

                        <a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>

                    </div>

                </div>

                <!-- </div> -->

                

            </header>

            

            @yield("content")

            <div class="tpOpsn for-FixedChat"><a href="#"><i class="fa fa-comment fa-flip-horizontal"></i> <span>Live Chat</span></a></div>

            <footer class="site-footer">

                <div class="footFirst"  data-aos="fade-up" data-aos-delay="200">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-3">

                                        <div class="footlogo"><img src="{{asset("public/assets/images/footer_logo.png")}}"></div>

                                        <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>

                                        <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>

                                    </div>



                                    <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Company</h2>

                                        <ul class="list-unstyled">

                                            <li><a href="{{ route("page.aboutUs") }}">About Us</a></li>  

                                            <li><a href="{{route("page.contactUs")}}">Contact Us</a></li>

                                        </ul>

                                    </div>

                                     <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Other Links</h2>

                                        <ul class="list-unstyled">

                                            <li><a href="{{ route("page.help") }}">Help</a></li>

                                            <li><a href="{{ route("page.faq") }}">Faq</a></li>

                                        </ul>

                                    </div>



                                    <div class="col-md-3">

                                        <h2 class="footer-heading mb-4">Legal</h2>

                                        <ul class="list-unstyled">

                                            <!--<li><a href="#">Refund Policy</a></li>-->

                                            <li><a href="{{ route("page.terms") }}">Terms</a></li>

                                            <li><a href="{{ route("page.privacy") }}">Privacy Policy</a></li>

                                        </ul>

                                    </div>

                                    

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="footSecond"  data-aos="fade-up" data-aos-delay="250">

                    <div class="container">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="foot_msg"><p>© 2019 grambunny. All Rights Reserved.</p></div>

                            </div>

                            <div class="col-md-6">

                                <div class="foot_Img "><a href="#"><img src="{{ asset("public/assets/images/android.png")}}"></a><a href="#"><img src="{{ asset("public/assets/images/ios.png")}}"></a></div>

                            </div>

                        </div>

                    </div>

                </div>

            </footer>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                <div class="modal-dialog" role="document">

                  <div class="modal-content">

                    <div class="modal-header">

                      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>

                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                        <span aria-hidden="true">&times;</span>

                      </button>

                    </div>

                    <div class="modal-body">

                        <form action="">

                            <div class="form-group">

                                <label for="email">Email</label>

                                <input type="text" name="email" id="email" class="form-control">

                            </div>

                            <div class="form-group">

                                <label for="password">Password</label>

                                <input type="text" name="password" id="password" class="form-control">

                            </div>

                        </form>    

                    </div>

                    <div class="modal-footer">

                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                      <button type="button" class="btn btn-primary">Login</button>

                    </div>

                  </div>

                </div>

              </div>

        </div>

        <script src="{{asset("public/js/app.js")}}?var=<?php echo rand();?>"></script>

        <script src="{{asset("public/assets/js/jquery-migrate-3.0.1.min.js")}}"></script>

        <script src="{{asset("public/assets/js/jquery-ui.js")}}"></script>

        <script src="{{asset("public/assets/js/popper.min.js")}}"></script>

        <script src="{{asset("public/assets/js/bootstrap.min.js")}}"></script>

        <script src="{{asset("public/assets/js/owl.carousel.min.js")}}"></script>

        <script src="{{asset("public/assets/js/jquery.stellar.min.js")}}"></script>

        <script src="{{asset("public/assets/js/jquery.countdown.min.js")}}"></script>

        <script src="{{asset("public/assets/js/jquery.magnific-popup.min.js")}}"></script>

        <script src="{{asset("public/assets/js/bootstrap-datepicker.min.js")}}"></script>

        <script src="{{asset("public/assets/js/aos.js")}}"></script>

        <script src="{{asset("public/assets/js/rangeslider.min.js")}}"></script>

        <script src="{{asset("public/assets/js/main.js")}}"></script>

        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>

   



   

  <!-- xzoom plugin here -->

  <script src="{{asset("public/assets/js/xzoom.min.js")}}"></script>



  <script type="text/javascript">

      (function ($) {

    $(document).ready(function() {

        $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', Xoffset: 15});

        $('.xzoom2, .xzoom-gallery2').xzoom({position: '#xzoom2-id', tint: '#ffa200'});

        $('.xzoom3, .xzoom-gallery3').xzoom({position: 'lens', lensShape: 'circle', sourceClass: 'xzoom-hidden'});

        $('.xzoom4, .xzoom-gallery4').xzoom({tint: '#006699', Xoffset: 15});

        $('.xzoom5, .xzoom-gallery5').xzoom({tint: '#006699', Xoffset: 15});



        //Integration with hammer.js

        var isTouchSupported = 'ontouchstart' in window;



        if (isTouchSupported) {

            //If touch device

            $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function(){

                var xzoom = $(this).data('xzoom');

                xzoom.eventunbind();

            });

            

            $('.xzoom, .xzoom2, .xzoom3').each(function() {

                var xzoom = $(this).data('xzoom');

                $(this).hammer().on("tap", function(event) {

                    event.pageX = event.gesture.center.pageX;

                    event.pageY = event.gesture.center.pageY;

                    var s = 1, ls;

    

                    xzoom.eventmove = function(element) {

                        element.hammer().on('drag', function(event) {

                            event.pageX = event.gesture.center.pageX;

                            event.pageY = event.gesture.center.pageY;

                            xzoom.movezoom(event);

                            event.gesture.preventDefault();

                        });

                    }

    

                    xzoom.eventleave = function(element) {

                        element.hammer().on('tap', function(event) {

                            xzoom.closezoom();

                        });

                    }

                    xzoom.openzoom(event);

                });

            });



        $('.xzoom4').each(function() {

            var xzoom = $(this).data('xzoom');

            $(this).hammer().on("tap", function(event) {

                event.pageX = event.gesture.center.pageX;

                event.pageY = event.gesture.center.pageY;

                var s = 1, ls;



                xzoom.eventmove = function(element) {

                    element.hammer().on('drag', function(event) {

                        event.pageX = event.gesture.center.pageX;

                        event.pageY = event.gesture.center.pageY;

                        xzoom.movezoom(event);

                        event.gesture.preventDefault();

                    });

                }



                var counter = 0;

                xzoom.eventclick = function(element) {

                    element.hammer().on('tap', function() {

                        counter++;

                        if (counter == 1) setTimeout(openfancy,300);

                        event.gesture.preventDefault();

                    });

                }



                function openfancy() {

                    if (counter == 2) {

                        xzoom.closezoom();

                        $.fancybox.open(xzoom.gallery().cgallery);

                    } else {

                        xzoom.closezoom();

                    }

                    counter = 0;

                }

            xzoom.openzoom(event);

            });

        });

        

        $('.xzoom5').each(function() {

            var xzoom = $(this).data('xzoom');

            $(this).hammer().on("tap", function(event) {

                event.pageX = event.gesture.center.pageX;

                event.pageY = event.gesture.center.pageY;

                var s = 1, ls;



                xzoom.eventmove = function(element) {

                    element.hammer().on('drag', function(event) {

                        event.pageX = event.gesture.center.pageX;

                        event.pageY = event.gesture.center.pageY;

                        xzoom.movezoom(event);

                        event.gesture.preventDefault();

                    });

                }



                var counter = 0;

                xzoom.eventclick = function(element) {

                    element.hammer().on('tap', function() {

                        counter++;

                        if (counter == 1) setTimeout(openmagnific,300);

                        event.gesture.preventDefault();

                    });

                }



                function openmagnific() {

                    if (counter == 2) {

                        xzoom.closezoom();

                        var gallery = xzoom.gallery().cgallery;

                        var i, images = new Array();

                        for (i in gallery) {

                            images[i] = {src: gallery[i]};

                        }

                        $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});

                    } else {

                        xzoom.closezoom();

                    }

                    counter = 0;

                }

                xzoom.openzoom(event);

            });

        });



        } else {

            //If not touch device



            //Integration with fancybox plugin

            $('#xzoom-fancy').bind('click', function(event) {

                var xzoom = $(this).data('xzoom');

                xzoom.closezoom();

                $.fancybox.open(xzoom.gallery().cgallery, {padding: 0, helpers: {overlay: {locked: false}}});

                event.preventDefault();

            });

           

            //Integration with magnific popup plugin

            $('#xzoom-magnific').bind('click', function(event) {

                var xzoom = $(this).data('xzoom');

                xzoom.closezoom();

                var gallery = xzoom.gallery().cgallery;

                var i, images = new Array();

                for (i in gallery) {

                    images[i] = {src: gallery[i]};

                }

                $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});

                event.preventDefault();

            });

        }

    });

})(jQuery);

  </script>



              <!--   CITIES  MODAL END-->

<script>

  $(document).ready(function() {



    var owl = $("#owl-demo");



    owl.owlCarousel({

  autoPlay : 3000,

      stopOnHover : true,

    items : 3, //10 items above 1000px browser width

    itemsDesktop : [1000,3], //5 items between 1000px and 901px

    itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px

    itemsTablet: [600,1], //2 items between 600 and 0;

    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option

    

    });



    // Custom Navigation Events

    $(".next").click(function(){

      owl.trigger('owl.next');

    })

    $(".prev").click(function(){

      owl.trigger('owl.prev');

    })

  });

</script>



          <script type="text/javascript">

    (function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);



var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})



$(function(){



  $('#new-review').autosize({append: "\n"});



  var reviewBox = $('.post-review-box');

  var newReview = $('#new-review');

  var openReviewBtn = $('#open-review-box');

  var closeReviewBtn = $('#close-review-box');

  var ratingsField = $('#ratings-hidden');



  openReviewBtn.click(function(e)

  {

    reviewBox.slideDown(400, function()

      {

        $('#new-review').trigger('autosize.resize');

        newReview.focus();

      });

    // openReviewBtn.fadeOut(100);

    closeReviewBtn.show();

  });



  closeReviewBtn.click(function(e)

  {

    e.preventDefault();

    reviewBox.slideUp(300, function()

      {

        newReview.focus();

        openReviewBtn.fadeIn(200);

      });

    closeReviewBtn.hide();

    

  });



  $('.starrr').on('starrr:change', function(e, value){

    ratingsField.val(value);

  });

});

  </script>



        @yield("scripts")

    </body>

</html>