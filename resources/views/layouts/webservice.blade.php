<!doctype html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online Food Ordering</title>
	
	@section("style")
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/master.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/font-awesome.css">
    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/design/front/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/') }}/design/front/css/animate.css">


<link rel="shortcut icon" href="{{ url('/') }}/design/front/img/favicon.ico" type="image/x-icon">
    <style>      
	
	html,body{height:100%;}
      .carousel,.item,.active{height:100%;}
      .carousel-inner{height:100%;}
      .fill{
        width:100%;height:100%;background-position:center;background-size:cover;    overflow: hidden;
      }
      .carousel-inner > .item {
          -webkit-transition: 0.3s ease-in-out left;
          -moz-transition: 0.3s ease-in-out left;
          -o-transition: 0.3s ease-in-out left;
          transition: 0.3s ease-in-out left;
      }
      .badgebox
      {
          opacity: 0;
      }
      .badgebox + .badge
      {   text-indent: -999999px;
      	  width: 27px;
      }
      .badgebox:focus + .badge
      {
          box-shadow: inset 0px 0px 5px;
      }
      .badgebox:checked + .badge {
          text-indent: -2px;
      }
      @media (max-width: 767px) { 
      	body {
      		padding-left: 0;
      		padding-right: 0;
      	}
      }
    </style>
	@show

	@yield("other_css")
  </head>
  <body>

<?php /*?><section class="navigation_main">
  <nav class="navbar navbar-fixed-top">
    <div class="container">
      <div ><!--class="navbar-header"-->
        <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
        </button>-->
      <!--  <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('/') }}/design/front/img/logo.png"></a>-->
      </div>
      <div class="collapse navbar-collapse">
        &nbsp;
      </div>
    </div>
  </nav>
</section><?php */?>

    <!-- End Header =============================================== -->
	
	 @yield("content")
   

    




@yield("js_bottom")	

  </body>
</html>
