<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-RQ8B04R992"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-RQ8B04R992');
    </script>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Grambunny</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('public/design/front/img/favicon.png') }}" rel="icon">

    <link href="{{ asset('public/assetss/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,200;0,400;0,500;0,700;0,800;1,700&display=swap"
        rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('public/assetss/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assetss/vendor/swiper/swiper-bundle.min.css') }}" rel='stylesheet'>
    <!-- Template Main CSS File -->
    <link href="{{ asset('public/assetss/css/style.css') }}" rel='stylesheet'>
    <?php if((Request::segment(3) == 'bussiness-details')){ ?>

    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/customthird.css') }}">

    <?php }else{ ?>

    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/custom.css') }}">

    <?php } ?>

    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <script>
        window.mapState = {

            initMap: false

        }

        function initMap() {

            window.mapState.initMap = true

        }
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&libraries=places&callback=initAutocomplete">
    </script>
    @yield('styles')
</head>

<body>
    <style type="text/css">
        span#session_qty li.cart_icon_sec.dcart {
            margin-right: 10px;
        }

        span#session_qty {
            position: absolute;
          /*  right: -25px;*/
        }

        .position-relative {
            margin-right: 20px;
        }
        .navbar {
    padding: 0;
    float: right;
}
    </style>
    <!-- ======= Header ======= -->

   <?php 
 
  $latitude = Session::get('mylatitude'); 
  $longitude = Session::get('mylongitude'); 

  if(empty($latitude)){

  $latitude = '34.03026980';
  $longitude = '-118.23087030'; 

  }

  $apiKey = 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU';
  $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$latitude.','.$longitude.'&sensor=false&key='.$apiKey);

    $output= json_decode($geocode);

    for($j=0;$j<count($output->results[0]->address_components);$j++){
               $cn=array($output->results[0]->address_components[$j]->types[0]);
           if(in_array("locality", $cn))
           {
            $city= $output->results[0]->address_components[$j]->long_name;
           }

           if(in_array("administrative_area_level_1", $cn))
           {
            $state= $output->results[0]->address_components[$j]->long_name;
           }

           if(in_array("country", $cn))
           {
            $country= $output->results[0]->address_components[$j]->long_name;
           }

          }

          $ucaddress =  $city.', '.$state.', '.$country;

 ?>    

    <header id="header" class="fixed-top">
        <div class="container-fluid">
<div class="row">
    <div class="col-md-4 col-sm-7 pv-x">
        <div class="log-locaton">
            <h1 class="mb-0 site-logo">
  <a href="{{ url('/') }}" class="logo"><img src="{{ url('/') }}/public/assetss/img/logo.png"></a></h1>
            <a class="hed-map"><img src="{{ url('/public/uploads/pin-location.png') }}"><span id="catoph" >{{$ucaddress}}</span></a>

        </div>
    </div>

    <div class="col-md-7 h-pro-nav">
 <nav id="navbar" class="navbar">
            <ul>
            <li><a class="nav-link scrollto active" href="{{ url('/') }}">Home</a></li>
            <li><a class="nav-link scrollto" href="{{ route('page.aboutUs') }}">About</a></li>   
            <li><a class="nav-link scrollto " href="{{ route('page.help') }}">Help</a></li>
            <li><a href="{{ route('page.faq') }}">FAQ</a></li>
            <li><a class="nav-link scrollto" href="{{ route('page.contactUs') }}">Contact</a></li>


                  <!--   <li><a class="nav-link scrollto" href="#"></a></li> -->

                    <?php if(is_null(Auth::guard("user")->user())){ 

                      $cart_uid = session('cart_uid');
                      
                      ?>
                    <?php if(is_null(Auth::guard("vendor")->user())){ ?>

                    <li style="border-bottom: 0;">
                        <a href="{{ route('signInPage', ['redirect_url' => url()->current()]) }}"
                            class="getstarted scrollto" id="cloginurl">Customer Login</a>
                    </li>

                    <?php }else{ ?> 

           <!--  <a href="{{ route('signInPage', ['redirect_url' => url()->current()]) }}" id="cloginurl"></a>  --><?php } ?>

                    <?php }else{ $cart_uid = auth()->guard('user')->user()->id; } ?>

                    <?php
                    $ps_qty = DB::table('cart')->where('user_id', $cart_uid)->sum('cart.quantity');
                    
                    if(empty($ps_qty)) { $ps_qty = 0; } ?>

                    <?php if(is_null(Auth::guard("user")->user())){ ?>
                    <?php if(is_null(Auth::guard("vendor")->user())){ ?>

                    <li style="border-bottom: 0;">
                        <a class="getstarted scrollto" href="{{ route('merchant.login') }}">Merchant Login</a>
                    </li>
                   <!--  <li style="border-bottom: 0;"> <a href="{{ url('/').'/cart' }}" style="padding: 10px 0 10px 7px;">
                        <img
                                src="{{ asset('public/assetss/img/cart.png') }}" style="width: 30px;"> </a></li> -->
                    <?php }else{ ?>

                    <li class="arch-remove"><a href="{{ route('merchant.dashboard') }}" class=" ropd-nav"><span
                                class=" round customer_btn">Merchant Dashboard</span></a></li>

                    </a></li>

                    <?php } }?>

                                            <!-- <li><a href="login.html">Log In</a></li> -->
                        @if(Auth::guard("user")->check())
 

                          <li class="dropdown login-aft">
            <a href="#" class="ropd-nav"><span>{{ Auth::guard("user")->user()->name.' '
                           .Auth::guard("user")->user()->lname }}</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
            <li><a href="{{ route("userAccount") }}">My Account</a></li>
                              <li><a href="{{ route("myevents") }}">My Events</a></li>
                              <li><a href="{{ route("myorders") }}">My Orders</a></li>
                              <li><a href="{{ route("userLogout") }}">Logout</a></li>
            </ul>
          </li>
                        @else
                        <!-- <li style="display: none;"><a href="{{ route("signInPage") }}" class="cta"><span class="text-white round">Log in/Register</span></a></li> -->
                        @endif

                </ul>
             
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->
    </div>

<div class="col-md-1 col-sm-1 col-1 ando-p">
    <li class="cart_icon_sec dcart" style="list-style: none;">
        <a href="https://www.grambunny.com/cart"><img src="https://www.grambunny.com/public/assets/images/cart.png" class="cart-vx"> <span id="session_qty">{{ $ps_qty }}</span></a> </li>    </div>
    </div>
     

        </div>
    </header><!-- End Header -->
    @yield('content')

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="row d-flex justify-content-center">

                            <div class="col-lg-5 col-md-4 footer-links">
                                <div class="logo-centr-fot">
                                    <a href="#"><img src="{{ url('/') }}/public/assetss/img/logo.png"
                                            class="mb-4"></a>
                                    <?php $career = DB::table('advertisement')->where('status', '=', '1')->where('id', 7)->first();
                                    
                                    $career1 = DB::table('advertisement')->where('status', '=', '1')->where('id', 8)->first();
                                    
                                    $career2 = DB::table('advertisement')->where('status', '=', '1')->where('id', 9)->first();
                                    
                                    $career3 = DB::table('advertisement')->where('status', '=', '1')->where('id', 10)->first();
                                    
                                    $career4 = DB::table('advertisement')->where('status', '=', '1')->where('id', 11)->first();
                                    
                                    $career5 = DB::table('advertisement')->where('status', '=', '1')->where('id', 12)->first();
                                    
                                    $career6 = DB::table('advertisement')->where('status', '=', '1')->where('id', 13)->first();
                                    
                                    $career7 = DB::table('advertisement')->where('status', '=', '1')->where('id', 14)->first();
                                    
                                    ?>



                                    <div class="down-app">
                                        @if (!empty($career->image))
                                            <a href="{{ $career->content }}" class="sote-b" target="_blank"><img
                                                    src="{{ url('/') }}/public/uploads/advertisement/{{ $career->image }}"></a>
                                        @endif
                                        @if (!empty($career1->image))
                                            <a href="{{ $career1->content }}" class="sote-b" target="_blank"><img
                                                    src="{{ url('/') }}/public/uploads/advertisement/{{ $career1->image }}"></a>
                                        @endif
                                    </div>




                                    <center>
                                        <div class="social-links mt-3">
                                            @if (!empty($career2->image))
                                                <a href="{{ $career2->content }}" target="_blank"
                                                    class="facebook"><i class="bx bxl-facebook"></i></a>
                                            @endif
                                            @if (!empty($career3->image))
                                                <a href="{{ $career3->content }}" target="_blank"
                                                    class="instagram"><i class='bx bxl-instagram-alt'></i></a>
                                            @endif

                                            @if (!empty($career4->image))
                                                <a href="{{ $career4->content }}" target="_blank"
                                                    class="linkedin"><i class="bx bxl-linkedin"></i></a>
                                            @endif
                                            @if (!empty($career5->image))
                                                <a href="{{ $career5->content }}" target="_blank"
                                                    class="facebook"><i class='bx bxl-twitter'></i></a>
                                            @endif


                                            @if (!empty($career6->image))
                                                <a href="{{ $career6->content }}" target="_blank"
                                                    class="instagram"><i class='bx bxl-youtube'></i></a>
                                            @endif

                                            @if (!empty($career7->image))
                                                <a href="{{ $career7->content }}" target="_blank"
                                                    class="linkedin"><i class='bx bxl-gmail'></i></a>
                                            @endif


                                        </div>
                                    </center>
                                </div>
                            </div>

                            <div class="col-lg-2 col-md-2 footer-links">
                                <h3>ABOUT </h3>
                                <ul>
                                    <li><a href="javascript:void(0)">Company</a></li>
                                    <li><a href="javascript:void(0)">Careers</a></li>
                                    <li><a href="javascript:void(0)">Advocacy</a></li>
                                    <li><a href="javascript:void(0)">Download The App</a></li>

                                </ul>

                            </div>

                            <div class="col-lg-3 col-md-3 footer-links">
                                <h3>GET TO KNOW US </h3>
                                <ul>
                                    <li><a href="{{ route('page.aboutUs') }}">About us</a></li>
                                    <li><a href="{{ route('page.help') }}">Help</a></li>
                                    <li><a href="{{ route('page.faq') }}">FAQ</a></li>
                                    <li><a href="{{ route('page.contactUs') }}">Contact</a></li>
                                </ul>

                            </div>

                            <div class="col-lg-2 col-md-3 footer-links">
                                <h3>LEGAL </h3>
                                <ul>
                                    <li><a href="{{ route('page.terms') }}">Terms Of use</a></li>
                                    <li><a href="{{ route('page.privacy') }}">Privacy Policy</a></li>
                                    <li><a href="javascript:void(0)">Cookie Policy</a></li>
                                    <li><a href="javascript:void(0)">Referral program</a></li>

                                </ul>

                            </div>




                        </div>
                    </div>






                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>Grambunny</span></strong>. All Rights Reserved
            </div>

        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" id="password" class="form-control">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Login</button>
                </div>
            </div>
        </div>
    </div>



    <!-- Vendor JS Files -->
    <script src="{{ url('/') }}/public/assetss/vendor/aos/aos.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
    <script src="{{ url('/') }}/public/assetss/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{ url('/') }}/public/assetss/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="{{ url('/') }}/public/assetss/vendor/swiper/swiper-bundle.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>
    <!-- Template Main JS File -->
    <script src="{{ url('/') }}/public/assetss/js/main.js"></script>

    <!--     <div class="mylivechat">
         <script type="text/javascript">
             function add_chatinline() {
                 var hccid = 90602094;
                 var nt = document.createElement("script");
                 nt.async = true;
                 nt.src = "https://mylivechat.com/chatinline.aspx?hccid=" + hccid;
                 var ct = document.getElementsByTagName("script")[0];
                 ct.parentNode.insertBefore(nt, ct);
             }
             add_chatinline();
         </script>
      </div> -->

    <script src="{{ asset('public/js/app.js') }}?var=<?php echo rand(); ?>"></script>
    <script src="{{ asset('public/assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('public/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery.stellar.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/aos.js') }}"></script>
    <script src="{{ asset('public/assets/js/rangeslider.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/main.js') }}"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src="{{ asset('public/assets/js/xzoom.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {


            $('#mapauto').click(function() {

                $('#merchant').val('');

            });

            $('#merchant').click(function() {

                $('#mapauto').val('');

            });


            $('#livechat').on('click', function() {



                var cuserid = $('#cuserid').val();



                if (cuserid != '') {



                    $.ajax({

                        type: "post",

                        headers: {
                            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                        },

                        data: {

                            "_token": "{{ csrf_token() }}",

                            "chat_data": {
                                cuserid: cuserid
                            },

                        },

                        url: "{{ url('/chatmlist') }}",



                        success: function(msg) {



                            $('#contacts').html(msg);



                        }

                    });





                    $('#frame').toggle();



                }



            });



            $('body').on('click', '.contact', function() {



                mimags = $(this).attr('id');

                mnames = $(this).text();

                merchid = $(this).attr("value");



                $('#nmerchant').html(mnames);

                $('#mmerchant').attr("src", mimags);

                $('#cmerchantid').val(merchid);



            });



        });



        function myChat() {



            var cmessage = $('#cmessage').val();

            var cuserid = $('#cuserid').val();

            var cmerchantid = $('#cmerchantid').val();



            if (cmessage == '') {
                return false;
            }



            $.ajax({

                type: "post",

                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                },

                data: {

                    "_token": "{{ csrf_token() }}",

                    "chat_data": {
                        cmessage: cmessage,
                        cuserid: cuserid,
                        cmerchantid: cmerchantid
                    },

                },

                url: "{{ url('/chatsystem') }}",



                success: function(msg) {



                    $('#cmessage').val('');

                    $('#messages').html(msg);



                }

            });



        };



        function timeChat() {



            var cuserid = $('#cuserid').val();

            var cmerchantid = $('#cmerchantid').val();


            if (cmessage != '' && cmerchantid != '') {


                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "chat_data": {
                            cuserid: cuserid,
                            cmerchantid: cmerchantid
                        },

                    },

                    url: "{{ url('/chathistory') }}",



                    success: function(msg) {



                        $('#messages').html(msg);



                    }

                });



            } else {
                return false;
            }



        };



        var myVar = setInterval(timeChat, 2000);





        (function($) {

            $(document).ready(function() {

                $('.xzoom, .xzoom-gallery').xzoom({
                    zoomWidth: 400,
                    title: true,
                    tint: '#333',
                    Xoffset: 15
                });

                $('.xzoom2, .xzoom-gallery2').xzoom({
                    position: '#xzoom2-id',
                    tint: '#ffa200'
                });

                $('.xzoom3, .xzoom-gallery3').xzoom({
                    position: 'lens',
                    lensShape: 'circle',
                    sourceClass: 'xzoom-hidden'
                });

                $('.xzoom4, .xzoom-gallery4').xzoom({
                    tint: '#006699',
                    Xoffset: 15
                });

                $('.xzoom5, .xzoom-gallery5').xzoom({
                    tint: '#006699',
                    Xoffset: 15
                });



                //Integration with hammer.js

                var isTouchSupported = 'ontouchstart' in window;



                if (isTouchSupported) {

                    //If touch device

                    $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function() {

                        var xzoom = $(this).data('xzoom');

                        xzoom.eventunbind();

                    });



                    $('.xzoom, .xzoom2, .xzoom3').each(function() {

                        var xzoom = $(this).data('xzoom');

                        $(this).hammer().on("tap", function(event) {

                            event.pageX = event.gesture.center.pageX;

                            event.pageY = event.gesture.center.pageY;

                            var s = 1,
                                ls;



                            xzoom.eventmove = function(element) {

                                element.hammer().on('drag', function(event) {

                                    event.pageX = event.gesture.center.pageX;

                                    event.pageY = event.gesture.center.pageY;

                                    xzoom.movezoom(event);

                                    event.gesture.preventDefault();

                                });

                            }



                            xzoom.eventleave = function(element) {

                                element.hammer().on('tap', function(event) {

                                    xzoom.closezoom();

                                });

                            }

                            xzoom.openzoom(event);

                        });

                    });



                    $('.xzoom4').each(function() {

                        var xzoom = $(this).data('xzoom');

                        $(this).hammer().on("tap", function(event) {

                            event.pageX = event.gesture.center.pageX;

                            event.pageY = event.gesture.center.pageY;

                            var s = 1,
                                ls;



                            xzoom.eventmove = function(element) {

                                element.hammer().on('drag', function(event) {

                                    event.pageX = event.gesture.center.pageX;

                                    event.pageY = event.gesture.center.pageY;

                                    xzoom.movezoom(event);

                                    event.gesture.preventDefault();

                                });

                            }



                            var counter = 0;

                            xzoom.eventclick = function(element) {

                                element.hammer().on('tap', function() {

                                    counter++;

                                    if (counter == 1) setTimeout(openfancy, 300);

                                    event.gesture.preventDefault();

                                });

                            }



                            function openfancy() {

                                if (counter == 2) {

                                    xzoom.closezoom();

                                    $.fancybox.open(xzoom.gallery().cgallery);

                                } else {

                                    xzoom.closezoom();

                                }

                                counter = 0;

                            }

                            xzoom.openzoom(event);

                        });

                    });



                    $('.xzoom5').each(function() {

                        var xzoom = $(this).data('xzoom');

                        $(this).hammer().on("tap", function(event) {

                            event.pageX = event.gesture.center.pageX;

                            event.pageY = event.gesture.center.pageY;

                            var s = 1,
                                ls;



                            xzoom.eventmove = function(element) {

                                element.hammer().on('drag', function(event) {

                                    event.pageX = event.gesture.center.pageX;

                                    event.pageY = event.gesture.center.pageY;

                                    xzoom.movezoom(event);

                                    event.gesture.preventDefault();

                                });

                            }



                            var counter = 0;

                            xzoom.eventclick = function(element) {

                                element.hammer().on('tap', function() {

                                    counter++;

                                    if (counter == 1) setTimeout(openmagnific, 300);

                                    event.gesture.preventDefault();

                                });

                            }



                            function openmagnific() {

                                if (counter == 2) {

                                    xzoom.closezoom();

                                    var gallery = xzoom.gallery().cgallery;

                                    var i, images = new Array();

                                    for (i in gallery) {

                                        images[i] = {
                                            src: gallery[i]
                                        };

                                    }

                                    $.magnificPopup.open({
                                        items: images,
                                        type: 'image',
                                        gallery: {
                                            enabled: true
                                        }
                                    });

                                } else {

                                    xzoom.closezoom();

                                }

                                counter = 0;

                            }

                            xzoom.openzoom(event);

                        });

                    });



                } else {

                    //If not touch device



                    //Integration with fancybox plugin

                    $('#xzoom-fancy').bind('click', function(event) {

                        var xzoom = $(this).data('xzoom');

                        xzoom.closezoom();

                        $.fancybox.open(xzoom.gallery().cgallery, {
                            padding: 0,
                            helpers: {
                                overlay: {
                                    locked: false
                                }
                            }
                        });

                        event.preventDefault();

                    });



                    //Integration with magnific popup plugin

                    $('#xzoom-magnific').bind('click', function(event) {

                        var xzoom = $(this).data('xzoom');

                        xzoom.closezoom();

                        var gallery = xzoom.gallery().cgallery;

                        var i, images = new Array();

                        for (i in gallery) {

                            images[i] = {
                                src: gallery[i]
                            };

                        }

                        $.magnificPopup.open({
                            items: images,
                            type: 'image',
                            gallery: {
                                enabled: true
                            }
                        });

                        event.preventDefault();

                    });

                }

            });

        })(jQuery);
    </script>
    <!--   CITIES  MODAL END-->
    <script>
        $(document).ready(function() {



            var owl = $("#owl-demo");



            owl.owlCarousel({

                autoPlay: 3000,

                stopOnHover: true,

                items: 3, //10 items above 1000px browser width

                itemsDesktop: [1000, 3], //5 items between 1000px and 901px

                itemsDesktopSmall: [900, 2], // 3 items betweem 900px and 601px

                itemsTablet: [600, 1], //2 items between 600 and 0;

                itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option



            });



            // Custom Navigation Events

            $(".next").click(function() {

                owl.trigger('owl.next');

            })

            $(".prev").click(function() {

                owl.trigger('owl.prev');

            })

        });
    </script>
    <script type="text/javascript">
        (function(e) {
            var t, o = {
                    className: "autosizejs",
                    append: "",
                    callback: !1,
                    resizeDelay: 10
                },
                i =
                '<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',
                n = ["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform",
                    "wordSpacing", "textIndent"
                ],
                s = e(i).data("autosize", !0)[0];
            s.style.lineHeight = "99px", "99px" === e(s).css("lineHeight") && n.push("lineHeight"), s.style.lineHeight =
                "", e.fn.autosize = function(i) {
                    return this.length ? (i = e.extend({}, o, i || {}), s.parentNode !== document.body && e(document
                        .body).append(s), this.each(function() {
                        function o() {
                            var t, o;
                            "getComputedStyle" in window ? (t = window.getComputedStyle(u, null), o = u
                                    .getBoundingClientRect().width, e.each(["paddingLeft", "paddingRight",
                                        "borderLeftWidth", "borderRightWidth"
                                    ], function(e, i) {
                                        o -= parseInt(t[i], 10)
                                    }), s.style.width = o + "px") : s.style.width = Math.max(p.width(), 0) +
                                "px"
                        }

                        function a() {
                            var a = {};
                            if (t = u, s.className = i.className, d = parseInt(p.css("maxHeight"), 10), e
                                .each(n, function(e, t) {
                                    a[t] = p.css(t)
                                }), e(s).css(a), o(), window.chrome) {
                                var r = u.style.width;
                                u.style.width = "0px", u.offsetWidth, u.style.width = r
                            }
                        }

                        function r() {
                            var e, n;
                            t !== u ? a() : o(), s.value = u.value + i.append, s.style.overflowY = u.style
                                .overflowY, n = parseInt(u.style.height, 10), s.scrollTop = 0, s.scrollTop =
                                9e4, e = s.scrollTop, d && e > d ? (u.style.overflowY = "scroll", e = d) : (
                                    u.style.overflowY = "hidden", c > e && (e = c)), e += w, n !== e && (u
                                    .style.height = e + "px", f && i.callback.call(u, u))
                        }

                        function l() {
                            clearTimeout(h), h = setTimeout(function() {
                                var e = p.width();
                                e !== g && (g = e, r())
                            }, parseInt(i.resizeDelay, 10))
                        }
                        var d, c, h, u = this,
                            p = e(u),
                            w = 0,
                            f = e.isFunction(i.callback),
                            z = {
                                height: u.style.height,
                                overflow: u.style.overflow,
                                overflowY: u.style.overflowY,
                                wordWrap: u.style.wordWrap,
                                resize: u.style.resize
                            },
                            g = p.width();
                        p.data("autosize") || (p.data("autosize", !0), ("border-box" === p.css(
                                "box-sizing") || "border-box" === p.css("-moz-box-sizing") ||
                            "border-box" === p.css("-webkit-box-sizing")) && (w = p.outerHeight() -
                            p.height()), c = Math.max(parseInt(p.css("minHeight"), 10) - w || 0, p
                            .height()), p.css({
                            overflow: "hidden",
                            overflowY: "hidden",
                            wordWrap: "break-word",
                            resize: "none" === p.css("resize") || "vertical" === p.css(
                                "resize") ? "none" : "horizontal"
                        }), "onpropertychange" in u ? "oninput" in u ? p.on(
                            "input.autosize keyup.autosize", r) : p.on("propertychange.autosize",
                            function() {
                                "value" === event.propertyName && r()
                            }) : p.on("input.autosize", r), i.resizeDelay !== !1 && e(window).on(
                            "resize.autosize", l), p.on("autosize.resize", r), p.on(
                            "autosize.resizeIncludeStyle",
                            function() {
                                t = null, r()
                            }), p.on("autosize.destroy", function() {
                            t = null, clearTimeout(h), e(window).off("resize", l), p.off(
                                "autosize").off(".autosize").css(z).removeData("autosize")
                        }), r())
                    })) : this
                }
        })(window.jQuery || window.$);



        var __slice = [].slice;
        (function(e, t) {
            var n;
            n = function() {
                function t(t, n) {
                    var r, i, s, o = this;
                    this.options = e.extend({}, this.defaults, n);
                    this.$el = t;
                    s = this.defaults;
                    for (r in s) {
                        i = s[r];
                        if (this.$el.data(r) != null) {
                            this.options[r] = this.$el.data(r)
                        }
                    }
                    this.createStars();
                    this.syncRating();
                    this.$el.on("mouseover.starrr", "span", function(e) {
                        return o.syncRating(o.$el.find("span").index(e.currentTarget) + 1)
                    });
                    this.$el.on("mouseout.starrr", function() {
                        return o.syncRating()
                    });
                    this.$el.on("click.starrr", "span", function(e) {
                        return o.setRating(o.$el.find("span").index(e.currentTarget) + 1)
                    });
                    this.$el.on("starrr:change", this.options.change)
                }
                t.prototype.defaults = {
                    rating: void 0,
                    numStars: 5,
                    change: function(e, t) {}
                };
                t.prototype.createStars = function() {
                    var e, t, n;
                    n = [];
                    for (e = 1, t = this.options.numStars; 1 <= t ? e <= t : e >= t; 1 <= t ? e++ : e--) {
                        n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))
                    }
                    return n
                };
                t.prototype.setRating = function(e) {
                    if (this.options.rating === e) {
                        e = void 0
                    }
                    this.options.rating = e;
                    this.syncRating();
                    return this.$el.trigger("starrr:change", e)
                };
                t.prototype.syncRating = function(e) {
                    var t, n, r, i;
                    e || (e = this.options.rating);
                    if (e) {
                        for (t = n = 0, i = e - 1; 0 <= i ? n <= i : n >= i; t = 0 <= i ? ++n : --n) {
                            this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass(
                                "glyphicon-star")
                        }
                    }
                    if (e && e < 5) {
                        for (t = r = e; e <= 4 ? r <= 4 : r >= 4; t = e <= 4 ? ++r : --r) {
                            this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass(
                                "glyphicon-star-empty")
                        }
                    }
                    if (!e) {
                        return this.$el.find("span").removeClass("glyphicon-star").addClass(
                            "glyphicon-star-empty")
                    }
                };
                return t
            }();
            return e.fn.extend({
                starrr: function() {
                    var t, r;
                    r = arguments[0], t = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                    return this.each(function() {
                        var i;
                        i = e(this).data("star-rating");
                        if (!i) {
                            e(this).data("star-rating", i = new n(e(this), r))
                        }
                        if (typeof r === "string") {
                            return i[r].apply(i, t)
                        }
                    })
                }
            })
        })(window.jQuery, window);
        $(function() {
            return $(".starrr").starrr()
        })



        $(function() {



            $('#new-review').autosize({
                append: "\n"
            });



            var reviewBox = $('.post-review-box');

            var newReview = $('#new-review');

            var openReviewBtn = $('#open-review-box');

            var closeReviewBtn = $('#close-review-box');

            var ratingsField = $('#ratings-hidden');



            openReviewBtn.click(function(e)

                {

                    reviewBox.slideDown(400, function()

                        {

                            $('#new-review').trigger('autosize.resize');

                            newReview.focus();

                        });

                    // openReviewBtn.fadeOut(100);

                    closeReviewBtn.show();

                });



            closeReviewBtn.click(function(e)

                {

                    e.preventDefault();

                    reviewBox.slideUp(300, function()

                        {

                            newReview.focus();

                            openReviewBtn.fadeIn(200);

                        });

                    closeReviewBtn.hide();



                });



            $('.starrr').on('starrr:change', function(e, value) {

                ratingsField.val(value);

            });

        });
    </script>

<script>

$(window).on('load', function() {
   
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
             var lat = position.coords.latitude;
             var long = position.coords.longitude;

            // Make a request to Google Maps Geocoding API to get the address
            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json',
                type: 'GET',
                dataType: 'json',
                data: {
                    latlng: lat + ',' + long,
                    key: 'AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU'
                },
                success: function(response) {
                    // Handle successful response
                    

                    if (response.status === 'OK' && response.results.length > 0) {

                       var addressComponents = response.results[0].address_components;
                        var city = '';
                        var state = '';
                        var postalCode = '';

                       
                        for (var i = 0; i < addressComponents.length; i++) {
                            var component = addressComponents[i];
                            if (component.types.includes('locality')) {
                                city = component.short_name;
                            } else if (component.types.includes('administrative_area_level_1')) {
                                state = component.short_name;
                            }else if(component.types.includes('country')){
                                country = component.short_name;
                            }
                        }

                     var address = city + ', ' + state + ', ' + country; 

                     /* if(response.results.length>9){

                      var addlength = response.results.length-4; 

                      }else{

                      var addlength = response.results.length-2;

                      }

                      var address = response.results[addlength].formatted_address; */

                      document.getElementById('catoph').innerHTML = address;

                    }
                },
                error: function(xhr, status, error) {
                    // Handle errors
                    console.error(error);
                }
            });
        }, function(error) {
            // Handle geolocation error
            console.error('Error getting geolocation:', error);
        });
    } else {
        // Handle case where geolocation is not supported
        console.error('Geolocation is not supported by this browser.');
    }
});

    function clFunctionTop() {

        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(showPosition);

        }

        function showPosition(position) {

            //var lat = position.coords.latitude;

            //var long = position.coords.longitude;
            
            var lat = 34.052235;

            var long = -118.243683;
            
            $.ajax({

                type: "post",

                headers: {
                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                },

                data: {

                    "_token": "{{ csrf_token() }}",

                    "data": {
                        latitude: lat,
                        longitude: long
                    },

                },

                url: "{{ url('/searchproductonload_new') }}",

                success: function(msg) {

                    $('#ajaxdatacall').html(msg);

                    var totalvendor = $('#totalvendor').val();

                    if (totalvendor == 0) {

                        $('#loadshow').hide();

                    } else {

                        $('#loadshow').show();

                    }

                }

            });

        }

    }


   

</script>
    @yield('scripts')
</body>
<style type="text/css">
    @media only screen and (max-width: 640px) {
        span.round.merchnat_btn {
            background-color: unset !important;
            color: #fff !important;
            border: unset !important;
            margin-bottom: 0px !important;
        }

        span.round.customer_btn {
            background-color: unset !important;
            color: #fff !important;
        }
    }
</style>



</body>

</html>
