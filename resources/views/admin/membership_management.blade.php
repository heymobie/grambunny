@extends('layouts.admin')



@section("other_css")

        <!-- DATA TABLES -->

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />



<style type="text/css">

  	

#myInput{ float: right; width: 30%;margin-bottom: 10px; }

      

</style>  

   

@stop



@section('content')

<aside class="right-side">    

    <section class="content-header">

        <h1>Membership Plan List</h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Membership Plan List</li>

        </ol>

    </section>



    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header">

                        <h3 class="box-title">Membership Plan List</h3>

                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive">

						@if(Session::has('message'))		 

						<div class="alert alert-success alert-dismissable">

				            <i class="fa fa-check"></i>

				            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

				            {{Session::get('message')}}

				        </div>

						@endif

                    </div>

                </div>

				<div>



<!--<div class="col-md-5">Order History / Order Status : &nbsp;   	

<select name="order_search" id="order_search" onchange="location = this.value;">

<option value="{{url('/admin/payment-list/')}}"@if($searchid=='') selected @endif>All</option>	

<option value="{{url('/admin/payment-list/0')}}"@if($searchid=='0') selected @endif>pending</option>

<option value="{{url('/admin/payment-list/1')}}"@if($searchid=='1') selected @endif>Accept</option>

<option value="{{url('/admin/payment-list/2')}}"@if($searchid=='2') selected @endif>Cancelled</option>

<option value="{{url('/admin/payment-list/3')}}"@if($searchid=='3') selected @endif>On the way</option>

<option value="{{url('/admin/payment-list/4')}}"@if($searchid=='4') selected @endif>Delivered</option>

</select>

</div>-->





			<input id="myInput" type="text" placeholder="Search..">

            <br>



				<section>

				 	<div class="row">

						<div class="my-panel-data">

							 <div class="col-xs-12">

							    <div class="box">

									<div class="box-body table-responsive">

										<div id="vender_search_list">	

							

										<table id="example2" class="table table-bordered table-hover">

<thead>

<tr>

<th>Merchant Name</th>

<th>Username</th>

<th>Plan Status</th>

<th>Plan Purchased</th>

<th>Plan Expiry</th>

<th>Action</th>

</tr>

</thead>

<tbody id="myTable">										

<?php foreach ($vendor_detail as $key => $value) { 

//$userinfo = DB::table('users')->where('id','=',$value->user_id)->first();?>



<tr>

<td>{{$value->name}} {{$value->last_name}}</td>

<td>{{$value->username}}</td>

<td>

	<?php if($value->plan_id == 1){?>

		Active

	<?php }else{ ?>

		Expired

	<?php } ?>

</td>

<td>{{$value->plan_purchased}}</td>

<td>{{ $value->plan_expiry	 }}</td>



<td>

	<a title="Edit Category" href="{{ url('admin/membership-update').'/'.$value->vendor_id}}"><i class="fa fa-edit"></i></a>

    <a href="{{ url('admin/membership-details').'/'.$value->vendor_id}}">View</a>

</td>



</tr> 



<?php } ?>

                                   

</tbody>

<tfoot>

<tr>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

</tr>

</tfoot>

</table>

										</div>

									</div>	

								</div>

							</div>

						</div>

					</div>



				<div class="col-12 mt-5 text-center">

	              <div class="custom-pagination">

	              	{{ $vendor_detail->links() }}

	              </div>

	            </div>

	            

				</section>					

			  </div>

            </div>

        </div>

    </section><!-- /.content -->

</aside><!-- /.right-side -->



@stop



@section('js_bottom')



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



        <!-- jQuery 2.0.2 -->

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->

        <!-- Bootstrap -->

        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

        <!-- AdminLTE App -->

        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

$(document).ready(function(){

  $("#myInput").on("keyup", function() {

    var value = $(this).val().toLowerCase();

    $("#myTable tr").filter(function() {

      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

    });

  });

});

</script>



        <!-- page script -->

        <script type="text/javascript">

function check_frm()

{





$('#error_msg').hide();



var form = $("#search_frm");

		form.validate();

	var valid =	form.valid();

	if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))

	{		

		 $("#ajax_favorite_loddder").show();	

		var frm_val = $('#search_frm').serialize();				

		$.ajax({

		type: "POST",

		url: "{{url('/admin/vendor_search')}}",

		data: frm_val,

			success: function(msg) {

			 $("#ajax_favorite_loddder").hide();	

			

				$('#vender_search_list').html(msg);

			}

		});

	}

	else

	{

		//alert('Please insert any one value');

		

		$('#error_msg').html('Please insert any one value');

		$('#error_msg').show();

		return false;

	}		

}			



$(function() {

	$("#example1").dataTable();

	$('#example2').dataTable({

		"bPaginate": true,

		"bLengthChange": false,

		"bFilter": true,

		"bSort": true,

		"bInfo": true,

		"bAutoWidth": false

	});

});

        </script>

@stop

