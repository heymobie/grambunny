	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Menu Item Update -> {{$menu_name}}</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_cate_frm" action="{{ url('/admin/category_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="menu_category_id" value="{{$id}}" />
								<input type="hidden" name="menu_id" value="{{$menu_id}}" />
								<input type="hidden" name="template_id" value="{{$template_id}}" />
								{!! csrf_field() !!}
                                    <div class="box-body">										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                           <input type="text" class="form-control" name="menu_name" id="	menu_name" value="{{$cate_detail[0]->menu_category_name}}" required="required">
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Description</label>
											<textarea  class="form-control" name="menu_desc" id="menu_desc" required="required">{{$cate_detail[0]->menu_category_desc}}</textarea>																
                                        </div>
										
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Popular</label>
											<select name="menu_cat_popular" id="menu_cat_popular">
												<option value="0" @if($cate_detail[0]->menu_cat_popular=='0')selected="selected"@endif>No</option>			
												<option value="1" @if($cate_detail[0]->menu_cat_popular=='1')selected="selected"@endif>Yes</option>										
											</select>												
                                        </div>
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Diet</label>
											<select name="menu_cat_diet" id="menu_cat_diet">	
												<option value="0" @if($cate_detail[0]->menu_cat_diet=='0')selected="selected"@endif>N/A</option>	
												<option value="1" @if($cate_detail[0]->menu_cat_diet=='1')selected="selected"@endif>Veg</option>			
												<option value="2" @if($cate_detail[0]->menu_cat_diet=='2')selected="selected"@endif>Non-Veg</option>										
											</select>												
                                        </div>
										
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Available in diffrent portion sizes</label>	
											<select name="diff_size" id="diff_size">
												<option value="no" @if($cate_detail[0]->menu_category_portion=='no')selected="selected"@endif>No</option>			
												<option value="yes" @if($cate_detail[0]->menu_category_portion=='yes')selected="selected"@endif>Yes</option>										
											</select>
                                        </div>
										
										
										<div id="diff_size_div" style="display:@if($cate_detail[0]->menu_category_portion=='yes')block @else none @endif" >
											 <div class="form-group">
											 <?php 
											 
											 $item1_id='';
											 $item1_title = '';
											 $item1_price = '';
											 $item1_status = '';
											 
											 
											 $item2_id='';
											 $item2_title = '';
											 $item2_price = '';
											 $item2_status = '';
											 
											 $item3_id='';
											 $item3_title = '';
											 $item3_price = '';
											 $item3_status = '';
											 
											  $item4_id='';
											 $item4_title = '';
											 $item4_price = '';
											 $item4_status = '';
											 
											 $item5_id='';
											 $item5_title = '';
											 $item5_price = '';
											 $item5_status = '';
											 
											 $item6_id='';
											 $item6_title = '';
											 $item6_price = '';
											 $item6_status = '';
											 
											 
											  if($cate_detail[0]->menu_category_portion=='yes'){
											  	$menu_sub_itme = DB::table('template_category_item')	
														->where('template_id', '=' ,$template_id)
														->where('menu_id', '=' ,$menu_id)
														->where('menu_category', '=' ,$id)
														->orderBy('menu_cat_itm_id', 'asc')
														->get();
														
														$i=0;
												foreach($menu_sub_itme as  $msi){	
													 if($i==0){
													 $item1_id=$menu_sub_itme[0]->menu_cat_itm_id;
													 $item1_title = $menu_sub_itme[0]->menu_item_title;
													 $item1_price = $menu_sub_itme[0]->menu_item_price;
													 $item1_status = $menu_sub_itme[0]->menu_item_status;
													 }
													  if($i==1){
													 $item2_id=$menu_sub_itme[1]->menu_cat_itm_id;
													 $item2_title =$menu_sub_itme[1]->menu_item_title;
													 $item2_price =  $menu_sub_itme[1]->menu_item_price;
													 $item2_status = '';
													  }
													   if($i==2){
													 $item3_id=$menu_sub_itme[2]->menu_cat_itm_id;
													 $item3_title = $menu_sub_itme[2]->menu_item_title;
													 $item3_price =  $menu_sub_itme[2]->menu_item_price;
													 $item3_status =  $menu_sub_itme[2]->menu_item_status;
													  }
													  if($i==3){
													 $item4_id=$menu_sub_itme[3]->menu_cat_itm_id;
													 $item4_title = $menu_sub_itme[3]->menu_item_title;
													 $item4_price = $menu_sub_itme[3]->menu_item_price;
													 $item4_status = $menu_sub_itme[3]->menu_item_status;
													 }
													  if($i==4){
													 $item5_id=$menu_sub_itme[4]->menu_cat_itm_id;
													 $item5_title =$menu_sub_itme[4]->menu_item_title;
													 $item5_price =  $menu_sub_itme[4]->menu_item_price;
													 $item5_status = $menu_sub_itme[4]->menu_item_status;
													  }
													   if($i==5){
													 $item6_id=$menu_sub_itme[5]->menu_cat_itm_id;
													 $item6_title = $menu_sub_itme[5]->menu_item_title;
													 $item6_price =  $menu_sub_itme[5]->menu_item_price;
													 $item6_status =  $menu_sub_itme[5]->menu_item_status;
													  }
													 	$i++;
													}	
												}		
											 ?>
											 
											 <table width="100%" cellpadding="2" cellspacing="2" id="portion_size_table"> 
													<tr>
														<th>Portion Size</th>
														<th>Display Order</th>
														<th>Price</th>
														<th>Status</th>
													</tr>
													
					
													<tr>
														<td>
													<input type="hidden" name="item_id[]" value="<?php echo $item1_id;?>" />
													
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item1_title;?>" required="required"/></td>
														<td>1</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item1_price;?>" required="required"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1" <?php if($item1_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0" <?php if($item1_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td>
													
													<input type="hidden" name="item_id[]" value="<?php echo $item2_id;?>" />													
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item2_title;?>"/></td>
														<td>2</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item2_price;?>"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1"<?php if($item2_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0"<?php if($item2_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													<tr><td>
													
													<input type="hidden" name="item_id[]" value="<?php echo $item3_id;?>" />
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item3_title;?>"/></td>
														<td>3</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item3_price;?>"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1" <?php if($item3_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0" <?php if($item3_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													
													
													<tr>													
														<td>													
													<input type="hidden" name="item_id[]" value="<?php echo $item4_id;?>" />
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item4_title;?>"/></td>
														<td>4</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item4_price;?>"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1" <?php if($item4_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0" <?php if($item4_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													
													<tr>													
														<td>													
													<input type="hidden" name="item_id[]" value="<?php echo $item5_id;?>" />
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item5_title;?>"/></td>
														<td>5</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item5_price;?>"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1" <?php if($item5_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0" <?php if($item5_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													
													<tr>													
														<td>													
													<input type="hidden" name="item_id[]" value="<?php echo $item6_id;?>" />
													<input class="form-control" type="text" name="item_title[]" value="<?php echo $item6_title;?>"/></td>
														<td>6</td>
														<td><input class="form-control" type="text" name="item_price[]" value="<?php echo $item6_price;?>"/></td>
														<td>
														<select class="form-control" name="item_status[]">
															<option value="1" <?php if($item6_status =='1'){echo 'selected="selected"';}?>>Active</option>
															<option value="0" <?php if($item6_status =='0'){echo 'selected="selected"';}?>>Inactive</option>
														</select>
														</td>
													</tr>
													
												</table>
												
											 	
											 </div>
										</div>
										
										
                                        <div class="form-group"  id="main_price_div" style="display:@if($cate_detail[0]->menu_category_portion=='no')block @else none @endif">
                                            <label for="exampleInputEmail1">Item Price</label>
                                           <input type="text" class="form-control" name="menu_price" id="	menu_price" value="{{$cate_detail[0]->menu_category_price}}" required="required" number="number">
                                        </div>
										
										
						   
											<div class="form-group">
												<label for="exampleInputEmail1">Status</label>
																									
												<select name="menu_cat_status" id="menu_cat_status" class="form-control">
												<option value="1" @if(($id>0) && ($cate_detail[0]->menu_cat_status=='1')) selected="selected" @endif>Active</option>
												<option value="0" @if(($id>0) && ($cate_detail[0]->menu_cat_status=='0')) selected="selected" @endif>Inactive</option>
												</select>															
											</div>
													  
                                      
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									<input type="button" class="btn btn-primary"  value="Update" onclick="check_update_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_update_frm('back')" />
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>	
	</section><!-- /.content -->

		<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
  $(document).on('load', '#diff_size', function(){ 
  	if($(this).find(":selected").val()=='yes'){
  		$('#diff_size_div').show();
	}
	else if($(this).find(":selected").val()=='no')
	{
  		$('#diff_size_div').hide();
	}
  
 });

  $(document).on('change', '#diff_size', function(){ 
  	if($(this).find(":selected").val()=='yes'){
  		$('#main_price_div').hide();
  		$('#diff_size_div').show();
	}
	else if($(this).find(":selected").val()=='no')
	{
  		$('#diff_size_div').hide();
  		$('#main_price_div').show();
	}
  
 });
function check_update_frm(tpy)
{

var form = $("#rest_cate_frm");
		form.validate();
	var valid =	form.valid();
	if(valid)
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = 'from='+tpy+'&'+$('#rest_cate_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/template_category_action')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#ajax_div').html(msg);
			}
		});
	}
	else
	{
		return false;
	}		
}
</script>

