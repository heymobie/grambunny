@extends('layouts.admin')

<style>

	.ui-widget-content {

	    max-height:221px;

	    overflow-y:scroll;

	}

	.ui-menu .ui-menu-item {

	    padding:5px;

	}

	.ui-menu-item:nth-child(2n) {

	    background-color:#f1f1f1;

	}

.awesome-cropper{ width: 100px; }

.modal-content canvas{
  width: 100%;
}

.modal-dialog11{
width: 50% !important;
margin: auto !important;
    }

</style>
<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />

@section('content')

<aside class="right-side">	

	<section class="content-header">

		<h1>

			Add Product 

			<small>Control Panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ route('merchant.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Add Product</li>

		</ol>

	</section>

	<section class="content">

	<div class="col-md-12">              

        <div class="box box-primary"> 

            <div class="box-header">

        		<h3 class="box-title">Add Product</h3>

            </div>

			@if(Session::has('message'))

			<div class="alert alert-success alert-dismissable">

				<i class="fa fa-check"></i>

				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

				{{Session::get('message')}}

			</div>

			@endif            

			@if(Session::has('message_error'))

			<div class="alert alert-danger alert-dismissable">

                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                {{Session::get('message_error')}}

            </div>

					@endif

                                <!-- form start -->

                        <?php if($id>0){ $catid = $product_detail[0]->category_id;}else{ $catid = 0; } ?>

                        <?php if($id>0){ $subcatid = $product_detail[0]->sub_category_id;}else{ $subcatid = 0; } ?>

					
							<form  role="form" method="POST" id="rest_frm" action="{{ url('/admin/product-action') }}" enctype="multipart/form-data">  

								<input type="hidden" name="product_id" id="product_id" value="{{$id}}" />

                                <input type="hidden" name="vendor_id" id="vendor_id" value="0" />

                                <input type="hidden" name="category_id" value="0" />

								<input type="hidden" name="ps_type" value="0" />

								<input type="hidden" name="product_old_img" id="product_old_img" value="@if($id>0){{$product_detail[0]->image}} @endif" />

								<input type="hidden" name="product_old_glimg" id="product_old_glimg" value="" />

								{!! csrf_field() !!}

                                    <div class="box-body">

                                    	<div class="row">

                                    		<div class="col-md-6">

                                    			<div class="form-group">

					                                <label for="exampleInputEmail1">Category</label>

					                                <select class="form-control" name="category_id" id="category_id" required="required">

					                                    <?php  foreach ($category_list as $key => $value) { ?>

					                                    <option value="{{$value->id}}"<?php if($value->id == $catid ){ echo "selected"; }?>>{{$value->category}}</option>

					                                    <?php } ?>                         	

					                                </select>                          

					                            </div>	

                                    		</div>

                                    		    <div class="col-md-6">

                                    			<div class="form-group">

					                                <label for="exampleInputEmail1">Sub Category</label>

					                                <select class="form-control" name="sub_category_id" id="sub_category_id" required="required">

					                                  <input type="hidden" id="subcategoryid" value="{{$subcatid}}">                       	

					                                </select>                          

					                            </div>	

                                    		</div>

                                    		<div class="col-md-6">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">Product Name</label>

		                                           <input type="text" class="form-control" name="product_name" id="product_name" value="@if($id>0){{$product_detail[0]->name}}@endif" required="required">

		                                        </div>

                                    		</div>

                                    		<div class="col-md-6">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">UPC/UID Code</label>

		                                           <input type="text" class="form-control" name="product_code" id="product_code" value="@if($id>0){{$product_detail[0]->product_code}}@endif" maxlength="40">

		                                        </div>

                                    		</div>

                                    	</div>


                                        <div class="row">

                                    		<div class="col-md-6">

		                                        <div class="form-group">

		                                            <label for="exampleInputEmail1">Price</label>

		                                           <input type="text" class="form-control" name="product_price" id="product_price" value="@if($id>0){{$product_detail[0]->price}}@endif" required="required">

		                                        </div>

                                    		</div>

                                    		<input type="hidden" name="excise_tax" id="excise_tax" value="0">

                                    		<!--<div class="col-md-4">

		                                        <div class="form-group">

		                                            <label for="exampleInputEmail1">Excise Tax</label>

		                                           <input type="text" class="form-control" name="excise_tax" id="excise_tax" value="@if($id>0){{$product_detail[0]->excise_tax}}@endif" required="required">

		                                        </div>

                                    		</div>-->

                                    		<div class="col-md-6">

		                                        <div class="form-group">

		                                            <!-- <label for="exampleInputEmail1">Inv Qty</label> -->

		                                           <!-- <input type="text" class="form-control" name="product_quantity" id="product_quantity" value="@if($id>0){{$product_detail[0]->quantity}}@endif" required="required"> -->

		                                           <input type="hidden" name="product_quantity" id="product_quantity" value="0">

		                                        </div>

                                    		</div>

                                    		</div>

                                        <div class="form-group">

                                        <label for="exampleInputEmail1">Description</label>

										<textarea  class="form-control" name="product_desc" id="product_desc" min="20" required="required" style="height: 200px;">@if($id>0){{$product_detail[0]->description}}@endif</textarea>																
                                        </div>

                                    		<div class="row">

                                    		<div class="col-md-4">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">Unit of Sale</label>

													<select name="unit" id="unit" class="form-control">

			                                    		<?php  foreach ($product_unit as $key => $value) { ?>

			                                   			<option value="{{$value->unit_name}}" @if(($id>0)&& ($product_detail[0]->unit==$value->unit_name)) selected="selected" @endif>{{$value->unit_name}}</option>

			                                    		<?php } ?>

													</select>

		                                        </div>

                                    		</div>

                                    	
                                    		<div class="col-md-4">

		                                        <div class="form-group">

		                                            <label for="exampleInputEmail1">Brands</label>

		                                        <select name="brands" id="brands" class="form-control">

			                                    <?php  foreach ($product_brands as $key => $value) { ?>

			                                   	<option value="{{$value->brand_name}}" @if(($id>0)&& ($product_detail[0]->brands==$value->brand_name)) selected="selected" @endif>{{$value->brand_name}}</option>

			                                    <?php } ?>

												</select>

		                                        </div>

                                    		</div>

                                    		<div class="col-md-4">

		                                    <div class="form-group">

		                                    <label for="exampleInputEmail1">Types</label>

                                			<select name="types" id="types" class="form-control">

                                    		<?php  foreach ($product_types as $key => $value) { ?>

                                   			<option value="{{$value->types_name}}" @if(($id>0)&& ($product_detail[0]->types==$value->types_name)) selected="selected" @endif>{{$value->types_name}}</option>

                                    		<?php } ?>

											</select>

		                                    </div>

                                    		</div>

                                    	</div>


                                    	<div class="row">

                                    	    <div class="col-md-4">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">Potency THC</label>

													  <input type="text" class="form-control" name="potency_thc" id="potency_thc" value="@if($id>0){{$product_detail[0]->potency_thc}}@endif" >

		                                        </div>

                                    		</div>	


                                    		<div class="col-md-4">

                                    			<div class="form-group">

		                                        <label for="exampleInputEmail1">Potency CBD</label>

                                                <input type="text" class="form-control" name="potency_cbd" id="potency_cbd" value="@if($id>0){{$product_detail[0]->potency_cbd}}@endif" >

		                                        </div>

                                    		</div>		

                                    		<div class="col-md-4">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">Stock</label>

													<select name="stock" id="stock" class="form-control">

													<option value="1" @if(($id>0)&& ($product_detail[0]->stock=='1')) selected="selected" @endif>In stock</option>

													<option value="0" @if(($id>0)&& ($product_detail[0]->stock=='0')) selected="selected" @endif>Out of stock</option>

													</select>

		                                        </div>

                                    		</div>

                                    		</div>

                                    		<div class="row">

                                    		<div class="col-md-4">

                                    			<div class="form-group">

		                                            <label for="exampleInputEmail1">Status</label>

													<select name="status" id="status" class="form-control">

														<option value="1" @if(($id>0)&& ($product_detail[0]->status=='1')) selected="selected" @endif>PUBLISHED</option>

														<option value="0" @if(($id>0)&& ($product_detail[0]->status=='0')) selected="selected" @endif>UNPUBLISHED</option>

													</select>

		                                        </div>	

                                    		</div>
 	

                                        <div class="col-md-4">

                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Thumb Image (File Type: jpeg,gif,png)</label>

                                            <input type="hidden" class="form-control" name="product_image" id="product_image">		

											

											@if(($id>0) && (!empty($product_detail[0]->image)))

											<div class="mgmOne" id="proimgthumb">

											<span class="img-gl"><img src="{{ url('/') }}/public/uploads/product/{{ $product_detail[0]->image }}" width="50px;" height="50px;"></span>

										   </div>

											@endif	

										@if($errors->has("product_image"))

                                        <span class="text-danger">{{ $errors->first("product_image") }}</span>

                                       @endif



                                        </div>
                                    </div>


                                    <div class="col-md-4">
                                       <div class="form-group">

                                            <label for="exampleInputEmail1">Gallery Image (File Type: jpeg,gif,png)</label>

                                            <input type="hidden" class="form-control" name="product_image_gallery" id="product_image_gallery" multiple>



											@if(($id>0) && (!empty($glimage)))



											<?php  foreach ($glimage as $key => $value) { ?>

											<div class="mgmOne">

												<span class="img-gl"><img src="{{ url('/') }}/public/uploads/product/{{ $value->name }}" width="50px;" height="50px;"></span>

											 	<a href="{{ url('/admin/product-gallery-delete/'.$value->id) }}"><i class="fa fa-times" aria-hidden="true"></i></a>	

											</div>

																						

											<?php } ?>



											@endif	



										@if($errors->has("product_image_gallery"))

                                       <span class="text-danger">{{ $errors->first("product_image_gallery") }}</span>

                                       @endif



                                        </div>
                                    </div>

									</div>	

										

                                    </div><!-- /.box-body -->



                                    <div class="box-footer">

										<input type="submit" class="btn btn-primary" value="Submit" id="psubid"/>

									<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

									 

									 	

                                    </div>

                                </form>

								

                            </div><!-- /.box -->





                        </div> 

	

	

	</section><!-- /.content -->

</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

 

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>



<script type="text/javascript">



$('#category_id').on('change', function() {

var catId = $(this).val();

$.ajax({

type: "post",

headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

data: {

        "_token": "{{ csrf_token() }}",

        "catid": catId

        },

url: "{{url('/admin/subcategory')}}",       

success: function(msg) { 

$('#sub_category_id').html(msg);

}

});

});	

$( document ).ready(function() {

var catId = $('#category_id').val();

var sub_category_id = $('#subcategoryid').val();

$.ajax({

type: "post",

headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

data: {

        "_token": "{{ csrf_token() }}",

        "catid": catId,
        
         "sub_category_id": sub_category_id

        },

url: "{{url('/admin/subcategory')}}",       

success: function(msg) { 

$('#sub_category_id').html(msg);

}

});

});  

</script>

<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script> 

<script type="text/javascript">

  /** Start Crop Image **/

   $(document).ready(function () {

   $('#product_image').awesomeCropper( { width: 600, height: 600, debug: true } );

    });

   $(document).ready(function () {

    $('#product_image_gallery').awesomeCropper( { width: 600, height: 600, debug: true } );

   });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

setInterval(function () {

var product_image = $('#product_image').val();
var product_image_gallery = $('#product_image_gallery').val(); 

if(product_image!=0){

$('#psubid').prop('disabled', true);

}

if(product_image_gallery!=0){

$('#psubid').prop('disabled', true);   

}     

},2000); 

    setInterval(function () {

            var vendor_id = $('#vendor_id').val(); 
            var product_id = $('#product_id').val(); 
            var product_image = $('#product_image').val();
            var product_image_gallery = $('#product_image_gallery').val();

            if(product_image!=0){

              //alert(product_id);

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "https://www.grambunny.com/admin/crop_product_image_save",
                data: {productimg : product_image, vendor_id : vendor_id, product_id : product_id}, 
                cache: false,
                success: function (data)
                {          
                    $('#product_image').val('');
                    $('#product_old_img').val(data.status);
                    $('#proimgthumb').hide();
                    $('#psubid').prop('disabled', false);
                }
            });

           }

            if(product_image_gallery!=0){

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "https://www.grambunny.com/admin/gallery_crop_image_save",
                data: {productimg : product_image_gallery, vendor_id : vendor_id, product_id : product_id}, 
                cache: false,
                success: function (data)
                {          
                    $('#product_image_gallery').val('');
                    $('#product_old_glimg').val(data.status);
                    $('#psubid').prop('disabled', false);
                    //window.location.reload();
                }
            });

           }

          },6000); 
  
  /** Crop image */ 

</script>
	

@stop