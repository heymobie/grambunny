@extends('layouts.admin')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Access Permissions
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Access Permissions</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Access Permissions</h3>
				</div><!-- /.box-header -->
				<p style="float: left;text-align: center;width: 100%;">
					@if(Session::has('message'))
					{{Session::get('message')}}
				@endif </p>
				<!-- form start -->
				<form  role="form" method="POST" id="user_frm" action="{{ url('/admin/permission-form') }}">
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Sub Admin</label><br>
							{{$user_detail[0]->name.' '.$user_detail[0]->lname}}
						</div>
						<input type="hidden" name="user_id" value="{{$user_id}}">
						<?php

						foreach ($permissionDetail as $key => $value) {
							if ($value->menu_id == 1) {
								$menu_name = 'dashboard';
							} elseif($value->menu_id == 2) {
								$menu_name = 'user_manegement';
								
							}

							?>
							<label for="exampleInputEmail1">{{$menu_name}}</label><br>

								<input type="checkbox" class="form-control" name="<?php echo $menu_name; ?>[<?php echo $value->menu_id; ?>][]" value="1" <?php if($value->add_permission==1){ echo "checked"; } ?>> Add &nbsp;&nbsp;


								<input type="checkbox" class="form-control" name="<?php echo $menu_name; ?>[<?php echo $value->menu_id; ?>][]" value="1" <?php if($value->edit_permission==1){ echo "checked"; } ?>> Edit &nbsp;&nbsp;

								<input type="checkbox" class="form-control" name="<?php echo $menu_name; ?>[<?php echo $value->menu_id; ?>][]" value="1" <?php if($value->view_permission==1){ echo "checked"; } ?>> View &nbsp;&nbsp;

								<input type="checkbox" class="form-control" name="<?php echo $menu_name; ?>[<?php echo $value->menu_id; ?>][]" value="1" <?php if($value->delete_permission==1){ echo "checked"; } ?>> Delete &nbsp;&nbsp;
							<?php
						}


						foreach ($menu_list as $list){?>
							<div class="form-group">




								<label for="exampleInputEmail1">{{$list->menu_name}}</label><br>

								<?php foreach ($permissionDetail as $value){?>



									<?php if($list->menu_id==$value->menu_id){ ?>




										<input type="checkbox" class="form-control" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="1" <?php if($value->add_permission==1){ echo "checked=checked";} ?>> Add &nbsp;&nbsp;



										<input type="checkbox" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="2" <?php if($value->edit_permission==1){ echo "checked=checked";} ?>> Edit &nbsp;&nbsp;




										<input type="checkbox" class="form-control" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="3" <?php if($value->view_permission==1){ echo "checked=checked";} ?>> View &nbsp;&nbsp;




										<input type="checkbox" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="4" <?php if($value->delete_permission==1){ echo "checked=checked";} ?>> Delete



									<?php }else{ ?>


										<?php if($value->add_permission!=1){?>
											<input type="checkbox" class="form-control" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="1"> Add &nbsp;&nbsp;
										<?php } ?>


										<?php if($value->edit_permission!=1){?>
											<input type="checkbox" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="2"> Edit &nbsp;&nbsp;
										<?php } ?>


										<?php if($value->view_permission!=1){?>
											<input type="checkbox" class="form-control" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="3"> View &nbsp;&nbsp;
										<?php } ?>


										<?php if($value->delete_permission!=1){?>
											<input type="checkbox" name="<?php echo $list->menu_name; ?>[<?php echo $list->menu_id; ?>][]" value="4"> Delete
										<?php } ?>

									<?php } ?>



								<?php } ?>






							</div>
						<?php } ?>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<input type="submit" class="btn btn-primary" value="Save"/>
						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
<style>
#ajax_parner_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_parner_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
}
#addons-modal.modal {
	z-index: 999;
}
</style>
<div id="ajax_parner_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
@section('js_bottom')
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };
      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;
			//alert(val);
		}
	}
}
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
  @stop