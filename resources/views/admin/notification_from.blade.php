@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop 

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Send Notification
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Send Notification</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Notification</h3>
						<div style="float:right; margin-right:10px; margin-top:10px;">
						<!--<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>-->
							</div>					
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
        @if(Session::has('message'))
		 
		 <div class="alert alert-success alert-dismissable">                          
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            {{Session::get('message')}}
         </div>
		@endif

	<form  role="form" enctype="multipart/form-data" method="POST" id="cuisine_frm" action="{{ url('/admin/send_notification_action') }}">					
					{!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                               <input type="text" class="form-control" name="title" id="title" value="" required="required">
                            </div>							
							<div class="form-group">
                                <label for="exampleInputEmail1">Content</label>
                                <textarea class="form-control"  name="noti_content" id="noti_content" required="required"></textarea>			
                            </div>							
							<div class="form-group">							
								<table id="example2" class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>Sr. No.
											<input type="checkbox" class="user_check_box check_all_none"  /></th>
											<th>Name</th>
											<th>Contact Number</th>
										</tr>
									</thead>
								<tbody>
									@if(!empty($user_info)) 
									<?php $i=1; ?>
									 @foreach ($user_info as $list)

										<tr>
										<td>
											<input type="checkbox" class="user_check_box user_check-frnt" name="user_id[]" value="<?php echo $list->id;?>" required="required" /><span class="user_check-listng"><?php echo $i ;?></span>
										</td>
										<td><?php echo $list->name.' '.$list->lname;?></td>
										<td><?php echo $list->user_mob;?></td>
										</tr>
										<?php $i++; ?>
										@endforeach	   
									@endif									
								</tbody>
									<tfoot>
										<tr>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</tfoot>
								</table>								
							</div>							
                        </div> <!-- /.box-body -->
                        <div class="box-footer">		
							<input type="button" class="btn btn-primary" value="Submit" onClick="check_email()" />
						
                        </div>
                    </form>					
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
			
function check_email()
{
	
	/*var form = $("#cuisine_frm");
	form.validate();
	var valid =	form.valid();
	if(valid)
	{
		$(form).submit();
		return true;	
	}*/	
	
	var user_list = $('.user_check_box').length;
	if(user_list>0)
	{
		var form = $("#cuisine_frm");
		form.validate();
		var valid =	form.valid();
		if(valid)
		{
		
			//alert($('.user_check_box').length);
			$(form).submit();
			return true;	
		}	
	}
	else
	{
		alert("You don't have any user.");
	}
}

$(document).on('ifChanged', '.check_all_none', function(){
 
 		if($('.check_all_none').prop('checked')){
            $('.user_check-frnt').prop('checked',true);
        } else {
		  $('.user_check-frnt').prop('checked',false)
        }
        $('.user_check-frnt').iCheck('update');
		


});

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>
@stop
