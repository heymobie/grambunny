@extends('layouts.admin')



@section("other_css")

        <!-- DATA TABLES -->

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

  <style type="text/css">

 td.actioncls {
    width: 15%;
}
 	
 .actioncls a{ 
 	padding-left: 15px; 
 } 	

  </style> 

@stop



@section('content')


<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Event Category Management

        </h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Event Category</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <div class="box">

                    <div class="box-header">

                        <h3 class="box-title">Event Category</h3>

                        <div style="float:right; margin-right:10px; margin-top:10px;">

							<a href="{{url('admin/event_category_form')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New Category</a>

							</div>

                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive">

					@if(Session::has('message'))

		 

					<div class="alert alert-success alert-dismissable">

			            <i class="fa fa-check"></i>

			               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

			                    {{Session::get('message')}}

			        </div>

					@endif

		

					<form name="search_frm" id="search_frm" method="post" action="administrator/property-list" onsubmit="return check_search();">

					{!! csrf_field() !!}

                    <div>

					<table cellpadding="5" cellspacing="5" width="100%" style="border:0px solid #999999" class="table">

						<tr>

						
							<td width="100%">

								
							</td>

						</tr>

					</table>

                    </div>

					</form>			

                        

                    </div><!-- /.box-body -->

                </div><!-- /.box -->

				<div>

				<section>

				 	<div class="row">

						<div class="my-panel-data">

							 <div class="col-xs-12">

							    <div class="box">

									<div class="box-body table-responsive">

										<div id="vender_search_list">							

											 <table id="myTable11" class="table table-bordered table-hover" data-order='[[ 0, "desc" ]]'>

<thead>

<tr>

<th>ID</th>

<th>Category Name</th>

<!--<th>Category Type</th>-->

<!-- <th>Image</th> -->

<!--<th>Status</th>-->

<!-- <th>Create Date</th> -->

<th>Action</th>

</tr>

</thead>

<tbody>										

<?php $i=1; ?>

@foreach ($vendor_cate_list as $list)

@if($list->type==3)

<tr>

<td>{{ $list->id}}</td>

<td>{{ $list->category}}</td>

<!--<td>@if($list->type==0) Product @else Service @endif</td>-->

<!-- <td>@if(!empty($list->cat_image))

<img src="{{ url('/') }}/public/uploads/category/{{ $list->cat_image }}" width="50px;" height="50px;">

@endif	

</td> -->

<!--<td>@if($list->status==1) 

<span class="label label-success">Active</span>

@else 								

<span class="label label-danger">Inactive</span>@endif

</td>-->

<!-- <td>{{ $list->created_at}}</td> -->

<td class="actioncls">

<a title="Edit Category" href="{{url('admin/event_category_form')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a>

<?php if($i!=1){ ?>

<a title="Delete Category" href="{{url('admin/event_category_delete')}}/{{ $list->id }}"><i class="fa fa-trash-o"></i></a>

<?php } ?>

<a href="{{ route("event_subcategory.view",["category" => $list->id]) }}"><i class="fa fa-eye" style=""></i></a>

</td>



</tr> 

@endif										

<?php $i++; ?>

@endforeach	                                      

</tbody>

<!-- <tfoot>

<tr>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

<th>&nbsp;</th>

</tr>

</tfoot> -->

</table>

										</div>

									</div>	

								</div>

							</div>

						</div>

					</div>

				</section>					

			  </div>

            </div>

        </div>

    </section><!-- /.content -->

</aside><!-- /.right-side -->



@stop



@section('js_bottom')



<style>



#ajax_favorite_loddder {



position: fixed;

top: 0;

left: 0;

width: 100%;

height: 100%;

background:rgba(27, 26, 26, 0.48);

z-index: 1001;

}

#ajax_favorite_loddder img {

top: 50%;

left: 46.5%;

position: absolute;

}



.footer-wrapper {

    float: left;

    width: 100%;

    /*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	

	z-index: 998 !important;

}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



        <!-- jQuery 2.0.2 -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- Bootstrap -->

        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

        <!-- AdminLTE App -->

        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



        <!-- page script -->

        <script type="text/javascript">

function check_frm()

{





$('#error_msg').hide();



var form = $("#search_frm");

		form.validate();

	var valid =	form.valid();

	if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))

	{		

		 $("#ajax_favorite_loddder").show();	

		var frm_val = $('#search_frm').serialize();				

		$.ajax({

		type: "POST",

		url: "{{url('/admin/vendor_search')}}",

		data: frm_val,

			success: function(msg) {

			 $("#ajax_favorite_loddder").hide();	

			

				$('#vender_search_list').html(msg);

			}

		});

	}

	else

	{

		//alert('Please insert any one value');

		

		$('#error_msg').html('Please insert any one value');

		$('#error_msg').show();

		return false;

	}		

}			



// $(function() {

// 	$("#example1").dataTable();

// 	$('#example2').dataTable({

// 		"bPaginate": true,

// 		"bLengthChange": false,

// 		"bFilter": true,

// 		"bSort": true,

// 		"bInfo": true,

// 		"bAutoWidth": false

// 	});

// });

        </script>


        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>

@stop

