@extends('layouts.admin')

@section("other_css")

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<style type="text/css">

.deepmd{ width: 100%; }

.deepmd strong{ float: right; }

.deepon{ font-size: 14px; } 

</style>

@stop

@section('content')

<aside class="right-side">

    <section class="content-header">

        <h1>Membership Detail</h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>

            <li class="active">Membership Detail</li>

        </ol>

    </section>



    <div class="myOdrersDetailBox">

    	<div class="row">

            <div class="col-md-12">

                <div class="myOrder">



                    <?php if(isset($vendor_detail)){ ?>

                    <div class="myOrderDtelBox">

                        <div class="myOrderID" style="float: none;">

                            <label>Transaction ID:</label>

                            <strong>{{$vendor_detail->txn_id}}</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Merchant Name</label>

                            <strong>{{$vendor_detail->name}} {{$vendor_detail->last_name}}</strong>

                        </div>

                        <div class="myOrderDtel">

                            <label>Contact Number</label>

                            <strong>{{$vendor_detail->mob_no}}</strong>

                        </div>



                        <?php if(!empty($vendor_detail->address)){ ?>

                            <div class="myOrderDtel">

                                <label>Delivery Address</label>

                                 <strong>{{$vendor_detail->address}}</strong>

                            </div>

                        <?php } ?>



                         <div class="myOrderDtel">

                            <label>City</label>

                            <strong>{{$vendor_detail->city}} &nbsp;</strong>

                        </div>

                         <div class="myOrderDtel">

                            <label>State</label>

                            <strong>{{$vendor_detail->state}} &nbsp;</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Zip Code</label>

                            <strong>{{$vendor_detail->zipcode}} &nbsp;</strong>

                        </div>





                        <div class="myOrderDtel">

                            <label>Email</label>

                            <strong>{{$vendor_detail->email}} &nbsp;</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Plan Status</label>

                            <strong><span class="green">

                            @if($vendor_detail->plan_id==0) Expired @endif

                            @if($vendor_detail->plan_id==1) Active @endif

                            </span></strong>

                        </div>





                        <div class="myOrderDtel">

                            <label>Plan Amount</label>

                            <strong>${{$vendor_detail->amount}} &nbsp;</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Plan Purchased</label>

                            <strong>{{$vendor_detail->plan_purchased}} &nbsp;</strong>

                        </div>



                        <div class="myOrderDtel">

                            <label>Plan Expired</label>

                            <strong>{{$vendor_detail->plan_expiry}} &nbsp;</strong>

                        </div>



                    </div>

                </div>



                <?php }else{ ?>

                    No details found

                <?php } ?>

            </div>



    	</div>

    </div>

</aside>

@stop

@section('js_bottom')

<style>

	#ajax_favorite_loddder {

		position: fixed;

		top: 0;

		left: 0;

		width: 100%;

		height: 100%;

		background:rgba(27, 26, 26, 0.48);

		z-index: 1001;

	}

	#ajax_favorite_loddder img {

		top:50%;

		left:46.5%;

		position:absolute;

	}

	.footer-wrapper {

	    float:left;

	    width:100%;

	}

	#addons-modal.modal {

		z-index:999;

	}

	.modal-backdrop {	

		z-index:998 !important;

	}

</style>	

	

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>		

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>        

<script type="text/javascript">

	function check_frm(){

		$('#error_msg').hide();

		var form = $("#search_frm");

		form.validate();

		var valid =	form.valid();

		if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))

		{		

			 $("#ajax_favorite_loddder").show();	

			var frm_val = $('#search_frm').serialize();				

			$.ajax({

			type: "POST",

			url: "{{url('/admin/vendor_search')}}",

			data: frm_val,

				success: function(msg) {

				 $("#ajax_favorite_loddder").hide();	

				

					$('#vender_search_list').html(msg);

				}

			});

		}

		else

		{

			//alert('Please insert any one value');

			

			$('#error_msg').html('Please insert any one value');

			$('#error_msg').show();

			return false;

		}		

	}

	$(function() {

		$("#example1").dataTable();

		$('#example2').dataTable({

			"bPaginate": true,

			"bLengthChange": false,

			"bFilter": true,

			"bSort": true,

			"bInfo": true,

			"bAutoWidth": false

		});

	});

</script>

@stop





