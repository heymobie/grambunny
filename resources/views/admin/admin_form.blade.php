@extends('layouts.admin')

@section('content')

<aside class="right-side">

	<style type="text/css">

	.field-icon {

		float: right;

		margin-left: -25px;

		margin-top: -25px;

		position: relative;

		z-index: 2;

	}

.downimg{ padding: 4px 19px; }

.mandatory{
    color:red;
    font-size: large;
}

.awesome-cropper{ width: 100px; }

.modal-content canvas{
  width: 100%;
}

.modal-dialog11{
width: 50% !important;
margin: auto !important;
    }

</style>

<section class="content-header">

	<h1>

		Admin Form

		<small>Control Panel</small>

	</h1>

	<ol class="breadcrumb">

		<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

		<li class="active">Admin Form</li>

	</ol>

</section>

<section class="content">

	<div class="col-md-12">

		<!-- general form elements -->

		<div class="box box-primary">

			<div class="box-header">

				<h3 class="box-title">Admin Form</h3>

			</div><!-- /.box-header -->


			<p style="float: left;text-align: center;width: 100%;">

				@if(Session::has('message'))

				{{Session::get('message')}}


			@endif </p>


			<!-- form start -->


			<form  role="form" method="POST" id="user_frm" action="{{ url('/admin/admin_action') }}"  enctype="multipart/form-data">


			<input type="hidden" name="admin_id" value="{{$id}}" />

				{!! csrf_field() !!}

				<div class="box-body">

					<div class="row">

						<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Name<span class="mandatory">*</span></label>

								<input type="text" class="form-control" name="name" id="name" value="@if($id>0){{trim($user_detail[0]->name)}}@endif" required="required">

							</div>

						</div>

						<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Last Name<span class="mandatory">*</span></label>

								<input type="text" class="form-control" name="lname" id="lname" value="@if($id>0){{trim($user_detail[0]->lname)}}@endif" required="required">

							</div>							

						</div>
							<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Phone Number<span class="mandatory">*</span></label>

								<input type="text" class="form-control form-white"  name="mobile_no" id="mobile_no" required="required" value="<?php if($id>0) { echo trim($user_detail[0]->mobile_no);}?>"  @if(($id>0) && (!empty(trim($user_detail[0]->mobile_no))) )  @endif >

								<div id="number_msg" class="eror_msg" style="display:none; color:#FF0000">An account with this phone number is already registered</div>

							</div>							

						</div>
					</div>

					<div class="row">

						<div class="col-md-4">

							<div class="form-group">

							<label for="exampleInputEmail1">Email<span class="mandatory">*</span></label>

							<input type="email" class="form-control" name="email" id="email" required="required" value="@if($id>0) {{$user_detail[0]->email}} @endif" @if($id>0) @endif>

							 @if($errors->has('email'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('email') }}</div>
                @endif

							<div id="email_msg" style="display:none; color:#FF0000">An account with this email is already registered</div>

						</div>

						</div>

							<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">Password<span class="mandatory">*</span></label>

									<input type="password" class="form-control" name="password" id="password" <?php if($id==0){?>required="required"<?php } ?>>

									 @if($errors->has('password'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('password') }}</div>
                        @endif

									<span toggle="#password" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>

								</div>

							</div>
								<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">Confirm Password<span class="mandatory">*</span></label>

									<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" <?php if($id==0){?>required="required"<?php } ?>>

									<span toggle="#confirmpassword" class="fa fa-fw fa-eye-slash field-icon toggle-confirmpassword"></span>

								</div>

							</div>
						
					</div>

					
						<div class="row">

							<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">User status</label>

									<select name="status" id="status"  class="form-control">

										<option value="1" <?php if(($id>0) && ($user_detail[0]->status=='1')) {echo 'selected="selected"';}?>>Active</option>

										<option value="0" <?php if(($id>0) && ($user_detail[0]->status=='0')) {echo 'selected="selected"';}?>>Inactive</option>

									</select>

								</div>	

							</div>
						
						</div>


					</div>

					<div class="box-footer">

						@if($id==0)

						<input type="submit" class="btn btn-primary" value="Submit"/>

						@else

						<input type="submit" class="btn btn-primary" value="Save"/>

						@endif

						<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

					</div>
				</form>

			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->

</aside><!-- /.right-side -->



@endsection



<style>



#ajax_parner_loddder {



	position: fixed;



	top: 0;



	left: 0;



	width: 100%;



	height: 100%;



	background:rgba(27, 26, 26, 0.48);



	z-index: 1001;



}



#ajax_parner_loddder img {



	top: 50%;



	left: 46.5%;



	position: absolute;



}



.footer-wrapper {



	float: left;



	width: 100%;



}



#addons-modal.modal {



	z-index: 999;



}



</style>



<div id="ajax_parner_loddder" style="display:none;">



	<div align="center" style="vertical-align:middle;">



		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />



	</div>



</div>



@section('js_bottom')



<!-- jQuery 2.0.2 -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>



<!-- jQuery UI 1.10.3 -->



<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>



<!-- Bootstrap -->



<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>



<!-- Bootstrap WYSIHTML5 -->



<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>



<!-- AdminLTE App -->



<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>



<!-- AdminLTE dashboard demo (This is only for demo purposes) -->



<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>



<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>


<script>
  document.getElementById('mobile_no').addEventListener('input', function (e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
});
</script>

<script>


	$(".toggle-password").click(function() {



		$(this).toggleClass("fa-eye-slash fa-eye");



		var input = $($(this).attr("toggle"));



		if (input.attr("type") == "password") {



			input.attr("type", "text");



		} else {



			input.attr("type", "password");



		}



	});

	$(".toggle-confirmpassword").click(function(){



		$(this).toggleClass("fa-eye-slash fa-eye");



		var input = $($(this).attr("toggle"));



		if (input.attr("type") == "password") {



			input.attr("type", "text");



		} else {



			input.attr("type", "password");



		}



	});



	function check_email()
	{



		jQuery.validator.addMethod("pass", function (value, element) {



			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



				return true;



			} else {



				return false;



			};



		});


		jQuery.validator.addMethod("vname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});

		jQuery.validator.addMethod("lname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});



		var form = $("#user_frm");

		form.validate({

			rules: {

				name:{

					required:true,

					vname:true

				},

				lname: {

					required: true,



					lname:true


				},


				user_mob: {


					//required: true,

					//minlength:10,

					//maxlength:10

				},


				email: {


					required: true,

					email:true

				},

				password: {

					required: true,

					minlength:8,

					pass:true,

				},


				confirmpassword: {

					required: true,

					minlength:8,

					equalTo : "#password",

				},



				user_address: {



					required: true,



				},



				state: {



					required: false,



				},



				city: {



					required: false,



				},



				zipcode: {



					required: false,



				}



			},



			messages: {



				name:{



					required:'Please enter name.',



					vname:"Please enter only letters."



				},



				lname: {



					required:'Please enter last name.',



					lname:'Please enter only letters.'



				},



				user_mob: {



					required:'Please enter mobile number.',



					minlength:'Please enter valid mobile number.',



					maxlength:'Please enter valid mobile number.'



				},



				email: {



					required:'Please enter email address.',



					email:'Please enter an valid email address.',



				},



				password: {



					required:'Please enter password.',



					minlength:'Password must be at least 8 characters.',



					pass:"at least one number, one lowercase and one uppercase letter.",



				},



				confirmpassword: {



					required:'Please enter confirm password.',



					minlength:'Password must be at least 8 characters.',



					equalTo:'confirm password and password should be same, please enter correct.'



				},



				user_address: {



					required:'Please enter postal address.',



				},



				state: {



					required:'Please enter state.',



				},



				city: {



					required:'Please enter city name.',



				},



				zipcode: {



					required:'Please enter zipcoad.',



				}



			}



		});



		var valid =	form.valid();



		if(valid){





				$("#ajax_parner_loddder").show();



				var frm_val = $('#user_frm').serialize();



				$.ajax({



					type: "POST",



					url: "./check_user_duplicateemail",



					data: frm_val,



					success: function(msg) {



						$("#ajax_parner_loddder").hide();



						if(msg=='1')



						{



							$('#email_msg').show();



							$('#number_msg').show();



							return false;



						}



						else if(msg=='2')



						{



							$('#email_msg').show();



							$('#number_msg').hide();



							return false;



						}



						else if(msg=='3')



						{



							$('#number_msg').show();



							$('#email_msg').hide();



							return false;



						}



						else if(msg=='4')



						{



							$(form).submit();



							return true;



						}



					}



				});





		}else{



			return false;



		}



	}



	function check_valid()



	{



		jQuery.validator.addMethod("pass", function (value, element) {



			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



				return true;



			} else {



				return false;



			};



		});





		jQuery.validator.addMethod("vname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});


		jQuery.validator.addMethod("lname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});



		var form = $("#user_frm");

		form.validate({

			rules: {

				name:{

					required:true,

					vname:true

				},

				lname: {

					required: true,

					lname:true

				},

				user_mob: {


					//required: true,



					//minlength:10,



					//maxlength:10



				},



				email: {



					required: true,



					email:true



				},



				password: {



					required: false,



					minlength:8,



					pass:false,



				},



				confirmpassword: {



					required: false,



					minlength:8,



					equalTo : "#password",



				},



				user_address: {



					required: true,



				},



				state: {



					required: false,



				},



				city: {



					required: false,



				},



				zipcode: {



					required: false,



				}



			},



			messages: {



				name:{



					required:'Please enter name.',



					vname:"Please enter only letters."



				},



				lname: {



					required:'Please enter last name.',



					lname:'Please enter only letters.'



				},



				user_mob: {



					required:'Please enter mobile number.',



					minlength:'Please enter valid mobile number.',



					maxlength:'Please enter valid mobile number.'



				},



				email: {



					required:'Please enter email address.',



					email:'Please enter an valid email address.',



				},



				password: {



					required:'Please enter password.',



					minlength:'Password must be at least 8 characters.',



					pass:"at least one number, one lowercase and one uppercase letter.",



				},



				confirmpassword: {



					required:'Please enter confirm password.',



					minlength:'Password must be at least 8 characters.',



					equalTo:'confirm password and password should be same, please enter correct.'



				},



				user_address: {



					required:'Please enter postal address.',



				},



				state: {



					required:'Please enter state.',



				},



				city: {



					required:'Please enter city name.',



				},



				zipcode: {



					required:'Please enter zipcoad.',



				}



			}



		});



		var valid =	form.valid();



		if(valid){

				var frm_val = $('#user_frm').serialize();



				$(form).submit();



			}



			else



			{



				return false;



			}



		}



	</script>



	<script>



      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



      	street_number: 'short_name',



      	route: 'long_name',



      	locality: 'long_name',



      	administrative_area_level_1: 'short_name',



      	country: 'long_name',



      	postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



        	{types: ['geocode']});



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);



    }



    function fillInAddress() {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



        	document.getElementById(component).value = '';



        	document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



        	var addressType = place.address_components[i].types[0];



        	if (componentForm[addressType]) {



        		var val = place.address_components[i][componentForm[addressType]];



        		document.getElementById(addressType).value = val;



			//alert(val);



		}



	}



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      // function geolocate() {



      // 	if (navigator.geolocation) {



      // 		navigator.geolocation.getCurrentPosition(function(position) {



      // 			var geolocation = {



      // 				lat: position.coords.latitude,



      // 				lng: position.coords.longitude



      // 			};



      // 			var circle = new google.maps.Circle({



      // 				center: geolocation,



      // 				radius: position.coords.accuracy



      // 			});



      // 			autocomplete.setBounds(circle.getBounds());



      // 		});



      // 	}



      // }



  </script>



  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&libraries=places&callback=initAutocomplete"></script>


<script>
        
            function checkValue(str, max) {
                if (str.charAt(0) !== '0' || str == '00') {
                    var num = parseInt(str);
                    if (isNaN(num) || num <= 0 || num > max) num = 1;
                    str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
                };
                return str;
            };

  // reformat by date
       function date_reformat_mm(date) {
            date.addEventListener('input', function(e) {
                this.type = 'text';
                var input = this.value;
                if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                var values = input.split('/').map(function(v) {
                    return v.replace(/\D/g, '')
                });
                if (values[0]) values[0] = checkValue(values[0], 12);
                if (values[1]) values[1] = checkValue(values[1], 31);
                var output = values.map(function(v, i) {
                    return v.length == 2 && i < 2 ? v + '/' : v;
                });
                this.value = output.join('').substr(0, 14);
            });


        }
    </script>
  @stop