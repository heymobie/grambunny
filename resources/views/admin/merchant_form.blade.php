@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<style type="text/css">
   i.fa.fa-times {
      font-size: 20px;
      vertical-align: top;
      padding: 5px;
   }

   input[type="date"] {
      display: block;
      -webkit-appearance: textfield;
      -moz-appearance: textfield;
      min-height: 1.2em;
   }
   .mandatory{
    color:red;
    font-size: large;
}
 .awesome-cropper{ width: 100px; }

.modal-content canvas{
  width: 100%;
}

.modal-dialog11{
width: 50% !important;
margin: auto !important;
    }

</style>
<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />

<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>Merchant Form
         <small>Control Panel</small>
      </h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Merchant Form</li>
      </ol>
   </section>


   <!-- Main content -->
   <section class="content">
      <div class="col-md-12">
         <!-- general form elements -->
         <div class="box box-primary">
            <div class="box-header">
               <h3 class="box-title">Merchant Form</h3>
            </div>
            <!-- /.box-header -->
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissable">
               <i class="fa fa-check"></i>
               {{Session::get('message')}}
            </div>
            @endif
            <!-- form start -->

            <form role="form" method="POST" id="user_frm" action="{{ url('/admin/merchant_action') }}" enctype="multipart/form-data">
               <input type="hidden" name="vendor_id" id="vendor_id" value="{{$id}}" />
               <input type="hidden" name="type" id="pstype" value="0" />
               <input type="hidden" name="category" id="category" value="0">
               <input type="hidden" name="subcategory[]" id="subcategory" value="0">
               {!! csrf_field() !!}
               <div class="box-body">

                  <!-- <div class="row">
                     <div class="col-md-6">
                     
                        <div class="form-group">
                     
                     <label for="exampleInputEmail1">Product Offered</label>
                     
                     <select name="type" class="form-control" id="pstype">
                     
                     <option value="">Choose Type</option> 
                     
                     <option value="0" @if(($id>0)&& ($user_detail[0]->type=='0')) selected="selected" @endif>Products</option> 
                     
                     <option value="1" @if(($id>0)&& ($user_detail[0]->type=='1')) selected="selected" @endif>Services</option>
                     
                     </select>
                     
                     
                     
                     </div>   
                     
                     </div>
                     
                     <div class="col-md-6">
                     
                        <div class="form-group">
                     
                     <label for="exampleInputEmail1">Category</label>
                     
                     <select name="category" id="category" class="form-control" required="required">
                     
                     
                                  @if(($id>0))
                     
                                 
                     
                     <?php foreach ($catagory as $key => $value) { ?>
                     
                     
                     
                     <option value="{{$value->id}}" @if($user_detail[0]->category_id==$value->id) selected="selected" @endif>{{$value->category}}</option>
                     
                     
                     
                     <?php } ?>
                     
                     
                     
                                  @endif
                     
                     </select>
                     
                     </div>
                     
                     </div>
                     
                     </div>-->
                  <?php
                  $subcatsde = '';

                  if (!empty($user_detail[0]->sub_category_id)) {

                     $subcats = $user_detail[0]->sub_category_id;

                     $subcatsde = json_decode($subcats);
                  }

                  ?>
                  <div class="row">
                     <!--<div class="col-md-6">
                        <div class="form-group">
                        
                        <label for="exampleInputEmail1">Sub Category</label>
                        
                        <select name="subcategory[]" id="subcategory" class="form-control" required="required" multiple>
                        
                        
                        
                                   @if(($id>0))
                        
                                  
                        
                        <?php foreach ($subcatagory as $key => $value) { ?>
                        
                        
                        
                        <option value="{{$value->id}}"<?php if ($subcatsde != '') {
                                                         foreach ($subcatsde as $subcat) {
                                                            if ($subcat == $value->id) { ?> selected="selected" <?php }
                                                                     }
                                                                  } ?> >{{$value->sub_category}}</option>
                        
                        
                        
                        <?php } ?>
                        
                        
                        
                                   @endif
                        
                        </select>
                        
                        </div>
                        
                        
                        
                        </div>-->
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">User Name <span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="username" id="username" value="@if($id>0){{$user_detail[0]->username}}@endif" required="required">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">First Name <span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="name" id="name" value="@if($id>0){{$user_detail[0]->name}}@endif" required="required">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Last Name <span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="last_name" id="last_name" value="@if($id>0){{$user_detail[0]->last_name}}@endif" required="required">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Mobile Location Number</label>
                           <input type="text" class="form-control" name="mob_no" id="mob_no" value="@if($id>0){{$user_detail[0]->mob_no}}@endif">
                           <div id="error_msg" style="color:#FF0000; display:none;"></div>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Contact Phone Number</label>
                           <input type="text" class="form-control" name="contact_no" id="contact_no" value="@if($id>0){{$user_detail[0]->contact_no}}@endif">
                           <div id="error_msg" style="color:#FF0000; display:none;"></div>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Full Company Name (Optional)</label>
                           <input type="text" class="form-control" name="business" id="business" value="@if($id>0){{$user_detail[0]->business_name}}@endif">
                           <div id="error_msg" style="color:#FF0000; display:none;"></div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Email <span class="mandatory">*</span></label>
                           <input type="email" class="form-control" name="email" id="email" required="required" value="@if($id>0){{$user_detail[0]->email}}@endif">
                         @if($errors->has('email'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('email') }}</div>
                        @endif
                           <div id="email_msg" style="display:none; color:#FF0000">Account with this email is already registered</div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Mailing Address <span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="mailing_address" id="mailing_address" value="@if($id>0){{$user_detail[0]->mailing_address}}@endif" required>
                           <!--  <input type="text" class="form-control" name="market_area" id="autocomplete" value="@if($id>0){{$user_detail[0]->market_area}}@endif"  required="required" onFocus="geolocate()"> -->
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Apt/Suite/Unit Number</label>
                           <input type="text" class="form-control" name="address" id="address" value="@if($id>0){{$user_detail[0]->address}}@endif">
                           <input type="hidden" id="street_number" name="street_number" />
                           <input type="hidden" class="field" id="route" name="route" />
                           <input type="hidden" id="country" name="country" />
                           <input type="hidden" class="form-control" name="state" id="state" value="" />
                        </div>
                     </div>

                  </div>
                  <div class="row">

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">City<span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="city" id="locality" value="@if($id>0){{$user_detail[0]->city}}@endif" required="required">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">State<span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="state" id="administrative_area_level_1" value="@if($id>0){{$user_detail[0]->state}}@endif" required="required" />
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Post code<span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="zipcode" id="postal_code" value="@if($id>0){{$user_detail[0]->zipcode}}@endif" number="number" required="required">
                        </div>
                     </div>

                  </div>
                  <div class="row">

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Merchant’s Google Map Address <span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="market_area" id="autocomplete" value="@if($id>0){{$user_detail[0]->market_area}}@endif" required="required" onFocus="geolocate()">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Serviced Radius <span class="mandatory">*</span>(Miles)</label>
                           <input type="number" class="form-control" name="service_radius" id="service_radius" value="@if($id>0){{$user_detail[0]->service_radius}}@endif" min="1" max="25000" required="required">
                        </div>
                     </div>

                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">PicK Up Address(Full Address)<span class="mandatory">*</span></label>
                           <input type="text" class="form-control" name="pick_up_address" id="pick_up_address" value="@if($id>0){{$user_detail[0]->pick_up_address}}@endif" required="required">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Driver’s License #</label>
                           <input type="text" class="form-control" name="driver_license" id="driver_license" value="@if($id>0){{$user_detail[0]->driver_license}}@endif">
                        </div>
                     </div>
                     <div class="col-md-4">

                        <?php

                        function get_browser_name($user_agent)
                        {
                           if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
                           elseif (strpos($user_agent, 'Edge')) return 'Edge';
                           elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
                           elseif (strpos($user_agent, 'Safari')) return 'Safari';
                           elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
                           elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

                           return 'Other';
                        }

                        $browsername = get_browser_name($_SERVER['HTTP_USER_AGENT']);

                        if (isset($user_detail[0]->license_expiry)) {
                           $license_expiry = $user_detail[0]->license_expiry;
                        } else {
                           $license_expiry = '';
                        }
                        if (isset($user_detail[0]->dob)) {
                           $dob = $user_detail[0]->dob;
                        } else {
                           $dob = '';
                        }
                        if (isset($user_detail[0]->permit_expiry)) {
                           $permit_expiry = $user_detail[0]->permit_expiry;
                        } else {
                           $permit_expiry = '';
                        }

                        $licenseexpiry = '';
                        $dobnew = '';
                        $permitexpiry = '';

                        if ($browsername == 'Safari') {

                           if ($license_expiry != '0000-00-00') {
                              $licenseexpiry = date("m/d/Y", strtotime($license_expiry));
                           }
                           if ($dob != '0000-00-00') {
                              $dobnew = date("m/d/Y", strtotime($dob));
                           }
                           if ($permit_expiry != '0000-00-00') {
                              $permitexpiry = date("m/d/Y", strtotime($permit_expiry));
                           }
                        } else {

                           $licenseexpiry = $license_expiry;
                           $dobnew = $dob;
                           $permitexpiry = $permit_expiry;
                        }


                        ?>

                        <div class="form-group">
                           <label for="exampleInputEmail1">Expiration Date</label>
                           <?php if ($browsername == 'Safari') { ?>
                              <input type="text" placeholder="yyyy-mm-dd" class="form-control" name="license_expiry" id="datepicker" value="@if($id>0){{$licenseexpiry}}@endif">
                           <?php } else { ?>
                              <input type="date" placeholder="yyyy-mm-dd" class="form-control" name="license_expiry" id="license_expiry" value="@if($id>0){{$licenseexpiry}}@endif">
                           <?php } ?>
                        </div>


                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Attach Driver’s License Front Copy</label>
                           <input type="file" class="form-control" name="license_front" id="license_front" value="">
                           <input type="hidden" name="license_front_old" value="@if($id>0){{$user_detail[0]->license_front}}@endif">
                           @if($id>0 && $user_detail[0]->license_front!='')

                         <?php $ext = pathinfo(storage_path().'/public/uploads/vendor/license/'.'/'.$user_detail[0]->license_front, PATHINFO_EXTENSION); ?>

                          <?php if($ext=='pdf'){ ?>
    
                            <object width="250px" height="260px" data="<?php echo url('/public/uploads/vendor/license/').'/'.$user_detail[0]->license_front; ?>"></object> 

                           <?php }else{ ?> 
                           
                           <a href="{{ url('/public/uploads/vendor/license/'.$user_detail[0]->license_front) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/license/'.$user_detail[0]->license_front) }}" height="100" width="100"></a>

                           <?php } ?>  

                           <!--<div class="docdown"><a href="{{ url('/public/uploads/vendor/license/'.$user_detail[0]->license_front) }}" download="download">Download</a></div>-->
                           @endif
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Attach Driver's License Back Copy</label>
                           <input type="file" class="form-control" name="license_back" id="license_back" value="">
                           <input type="hidden" name="license_back_old" value="@if($id>0){{$user_detail[0]->license_back}}@endif">
                           @if($id>0 && $user_detail[0]->license_back!='')
                         <?php $ext = pathinfo(storage_path().'/public/uploads/vendor/license/'.'/'.$user_detail[0]->license_back, PATHINFO_EXTENSION); ?>

                          <?php if($ext=='pdf'){ ?>
                           
                           <object width="250px" height="260px" data="<?php echo url('/public/uploads/vendor/license/').'/'.$user_detail[0]->license_back; ?>"></object> 

                           <?php }else{ ?> 
                           
                            <a href="{{ url('/public/uploads/vendor/license/'.$user_detail[0]->license_back) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/license/'.$user_detail[0]->license_back) }}" height="100" width="100"></a>

                           <?php } ?> 

                           @endif
                        </div>
                     </div>

                  </div>

                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Instagram Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="insta" id="insta" value="@if($id>0){{$user_detail[0]->insta}}@endif" >
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Facebook Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="facebook" id="facebook" value="@if($id>0){{$user_detail[0]->facebook}}@endif" >
                        </div>
                     </div>

                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Tik Tok Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="tiktok" id="tiktok" value="@if($id>0){{$user_detail[0]->tiktok}}@endif" >
                        </div>
                     </div>

                      <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Pinterest Link<span class="mandatory"></span></label>
                           <input type="url" class="form-control" name="pinterest" id="pinterest" value="@if($id>0){{$user_detail[0]->pinterest}}@endif" >
                        </div>
                     </div>
                  </div>
                  <div class="row">

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Social Security Number or EIN</label>
                           <input type="text" class="form-control" name="ssn" id="ssn" value="@if($id>0){{$user_detail[0]->ssn}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Date of Birth</label>

                           <?php if ($browsername == 'Safari') { ?>
                              <input type="text" placeholder="yyyy-mm-dd" class="form-control" name="dob" id="datepicker1" value="@if($id>0){{$dobnew}}@endif">
                           <?php } else { ?>
                              <input type="date" placeholder="yyyy-mm-dd" class="form-control" name="dob" id="dob" value="@if($id>0){{$dobnew}}@endif">
                           <?php } ?>

                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Account Profile Photo 1</label>
                           <input type="hidden" class="form-control" name="profile_img1" id="profile_img1" value="" accept="image/png, image/gif, image/jpeg">

                           <input type="hidden" name="profile_img1_old" id="profile_img1_old" value="@if($id>0){{$user_detail[0]->profile_img1}}@endif">

                           @if($id>0 && $user_detail[0]->profile_img1!='')

                           <span id="profile1a">
                           <a href="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img1) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img1) }}" height="70" width="70"></a>
                           </span>

                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="row">

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Account Profile Photo 2 (Optional)</label>
                           <input type="hidden" class="form-control" name="profile_img2" id="profile_img2" value="" accept="image/png, image/gif, image/jpeg">

                           <input type="hidden" name="profile_img2_old" id="profile_img2_old" value="@if($id>0){{$user_detail[0]->profile_img2}}@endif">

                           @if($id>0 && $user_detail[0]->profile_img2!='')
                           <span id="profile2a">
                           <a href="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img2) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img2) }}" height="70" width="70"></a>
                           </span>
                           <a href="{{ url('/admin/merchant_pic_delete/2/'.$id) }}"><i class="fa fa-times" aria-hidden="true"></i></a>
                           @endif
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Account Profile Photo 3 (Optional)</label>
                           <input type="hidden" class="form-control" name="profile_img3" id="profile_img3" value="" accept="image/png, image/gif, image/jpeg">

                           <input type="hidden" name="profile_img3_old" id="profile_img3_old" value="@if($id>0){{$user_detail[0]->profile_img3}}@endif">

                           @if($id>0 && $user_detail[0]->profile_img3!='')
                           
                           <span id="profile3a">
                           <a href="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img3) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img3) }}" height="70" width="70"></a>
                           </span>
                           <a href="{{ url('/admin/merchant_pic_delete/3/'.$id) }}"><i class="fa fa-times" aria-hidden="true"></i></a>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Account Profile Photo 4 (Optional)</label>
                           <input type="hidden" class="form-control" name="profile_img4" id="profile_img4" value="" accept="image/png, image/gif, image/jpeg">

                           <input type="hidden" name="profile_img4_old" id="profile_img4_old" value="@if($id>0){{$user_detail[0]->profile_img4}}@endif">

                           @if($id>0 && $user_detail[0]->profile_img4!='')
                           <span id="profile4a">
                           <a href="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img4) }}" target="_blank"><img src="{{ url('/public/uploads/vendor/profile/'.$user_detail[0]->profile_img4) }}" height="70" width="70"></a>
                           </span>
                           <a href="{{ url('/admin/merchant_pic_delete/4/'.$id) }}"><i class="fa fa-times" aria-hidden="true"></i></a>
                           @endif
                        </div>
                     </div>

                  </div>
                  <div class="row">

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Permit/License Type</label>
                           <input type="text" class="form-control" name="permit_type" id="permit_type" value="@if($id>0){{$user_detail[0]->permit_type}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Permit/License Number</label>
                           <input type="text" class="form-control" name="permit_number" id="permit_number" value="@if($id>0){{$user_detail[0]->permit_number}}@endif">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Permit/License Expiration Date</label>

                           <?php if ($browsername == 'Safari') { ?>
                              <input type="text" placeholder="yyyy-mm-dd" class="form-control" name="permit_expiry" id="datepicker2" value="@if($id>0){{$permitexpiry}}@endif">
                           <?php } else { ?>
                              <input type="date" placeholder="yyyy-mm-dd" class="form-control" name="permit_expiry" id="permit_expiry" value="@if($id>0){{$permitexpiry}}@endif">
                           <?php } ?>
                        </div>

                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Business Description</label>
                           <input type="text" class="form-control" name="description" id="description" value="@if($id>0){{$user_detail[0]->description}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Video Upload</label>
                           <input type="file" accept="video/*" class="form-control" name="video" id="video" value="">
                           <input type="hidden" name="pvideo1_old" id="pvideo1_old" value="@if($id>0){{$user_detail[0]->video}}@endif">
                           @if($id>0 && $user_detail[0]->video!='')
                           <span id="pvideo1a">
                              <video width="250" height="200" controls>
                                 <source src="{{ url('/public/'.$user_detail[0]->video) }}" type="video/mp4">
                              </video>
                              <!-- <a href="{{ url('/admin/merchant_pic_delete/11/'.$id) }}"><i class="fa fa-times" aria-hidden="true"></i></a> -->
                           </span>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-6"></div>
                  </div>
                  <h4 class="box-title" style="background-color: #f5f5f5;padding: 7px; border:1px solid #ddd;">Vehicle</h4>
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Make</label>
                           <input type="text" class="form-control" name="make" id="make" value="@if($id>0){{$user_detail[0]->make}}@endif" >
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Model</label>
                           <input type="text" class="form-control" name="model" id="model" value="@if($id>0){{$user_detail[0]->model}}@endif" >
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Year</label>
                           <input type="number" class="form-control" name="year" id="year" value="@if($id>0){{$user_detail[0]->year}}@endif">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Color</label>
                           <input type="text" class="form-control" name="color" id="color" value="@if($id>0){{$user_detail[0]->color}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">License Plate Number</label>
                           <input type="text" class="form-control" name="license_plate" id="license_plate" value="@if($id>0){{$user_detail[0]->license_plate}}@endif">
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Status</label>
                           <select name="vendor_status" id="vendor_status" class="form-control">
                              <option value="1" @if(($id>0)&& ($user_detail[0]->vendor_status=='1')) selected="selected" @endif>Active</option>
                              <option value="0" @if(($id>0)&& ($user_detail[0]->vendor_status=='0')) selected="selected" @endif>Inactive</option>
                           </select>
                        </div>
                     </div>
                     @if($id==0)
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Password <span class="mandatory">*</span></label>
                           <input type="password" class="form-control" name="password" id="password" required="required">

                              @if($errors->has('password'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('password') }}</div>
                        @endif


                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Confirm Password <span class="mandatory">*</span></label>
                           <input type="password" class="form-control" name="confirm_password" id="confirm_password" required="required">
                        </div>
                     </div>
                     @else
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Password<span class="mandatory">*</span></label>
                           <input type="password" class="form-control" name="password" id="password">
                           <p style="font-size:12px;color:red">Note: you can't see the password only can change.</p>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Confirm Password <span class="mandatory">*</span></label>
                           <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                        </div>
                     </div>
                     @endif

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Sales Tax (In %)</label>
                           <input type="text" max="100" min="0" id="sales_tax" name="sales_tax" value="@if($id>0){{$user_detail[0]->sales_tax}}@endif" class="form-control ">
                        </div>
                     </div>

                     <p style="margin: 0 0 -2px;">&nbsp;</p>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Excise Tax (In %)</label>
                           <input type="text" max="100" min="0" id="excise_tax" name="excise_tax" value="@if($id>0){{$user_detail[0]->excise_tax}}@endif" class="form-control ">
                        </div>
                     </div>


                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">City Tax (In %)</label>
                           <input type="text" max="100" min="0" id="city_tax" name="city_tax" value="@if($id>0){{$user_detail[0]->city_tax}}@endif" class="form-control ">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Commission Rate (in %)</label>
                           <input type="text" class="form-control" name="commission_rate" id="commission_rate" value="@if($id>0){{$user_detail[0]->commission_rate}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Delivery Fee (in $)</label>
                           <input type="text" class="form-control" name="delivery_fee" id="delivery_fee" value="@if($id>0){{$user_detail[0]->delivery_fee}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Type Of Merchant</label>
                           <select name="type_of_merchant" id="type_of_merchant" class="form-control">
                              <option value="Driver" @if(($id>0)&& ($user_detail[0]->type_of_merchant=='Driver')) selected="selected" @endif>Driver</option>
                              <option value="Non Mobile Storefront" @if(($id>0)&& ($user_detail[0]->type_of_merchant=='Non Mobile Storefront')) selected="selected" @endif>Non Mobile Storefront</option>
                              <option value="Drive Thru" @if(($id>0)&& ($user_detail[0]->type_of_merchant=='Drive Thru')) selected="selected" @endif>Drive Thru</option>
                           </select>
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Map Icon(Optional)</label>
                           <input type="file" class="form-control" name="map_icon" id="map_icon" value="">
                           <input type="hidden" name="map_icon_old" value="@if($id>0){{$user_detail[0]->map_icon}}@endif">
                           @if($id>0 && $user_detail[0]->map_icon!='')
                           <a href="{{ url('/public/uploads/'.$user_detail[0]->map_icon) }}" target="_blank"><img src="{{ url('/public/uploads/'.$user_detail[0]->map_icon) }}" height="70" width="70"></a>
                           @endif
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                           <label for="exampleInputEmail1">Minimum Amount Per Order (in $)</label>
                           <input type="text" class="form-control" name="minimum_order_amount" id="minimum_order_amount" value="@if($id>0){{$user_detail[0]->minimum_order_amount}}@endif">
                        </div>
                     </div>

                     <div class="col-md-4">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Payment Method Cash/Card</label>
                        <select name="cash_card" id="cash_card" class="form-control">
                        <option value="0" @if(($id>0)&& ($user_detail[0]->cash_card=='0')) selected="selected" @endif>Cash or Credit/Debit Card Swipe on Delivery</option>
                        <option value="1" @if(($id>0)&& ($user_detail[0]->cash_card=='1')) selected="selected" @endif>Credit Card/Debit Card</option>
                        </select>
                        </div>
                     </div> 

                       <!--<div class="col-md-4">
                        <div class="form-group">
                        <label for="exampleInputEmail1">Delivery or Pick Up</label>
                        <select name="cash_card" id="cash_card" class="form-control">
                        <option value="0" @if(($id>0)&& ($user_detail[0]->cash_card=='0')) selected="selected" @endif>Delivery</option>
                        <option value="1" @if(($id>0)&& ($user_detail[0]->cash_card=='1')) selected="selected" @endif>Pick Up</option>
                        </select>
                        </div>
                     </div>-->        

                  </div>

                  <h4 class="box-title" style="background-color: #f5f5f5;padding: 7px; border:1px solid #ddd;"> Hours Of Operation</h4>

                  <?php

                  $bustime = DB::table('vendor_business_time')->where('vendor_id', '=', $id)->get();

                  if (count($bustime) > 0) {

                     $mon = explode('-', $bustime[0]->monday);

                     $tue = explode('-', $bustime[0]->tuesday);

                     $wed = explode('-', $bustime[0]->wednesday);

                     $thu = explode('-', $bustime[0]->thursday);

                     $fri = explode('-', $bustime[0]->friday);

                     $sat = explode('-', $bustime[0]->saturday);

                     $sun = explode('-', $bustime[0]->sunday);

                     $open_close_mon = $bustime[0]->open_close_mon;
                     $open_close_tue = $bustime[0]->open_close_tue;
                     $open_close_wed = $bustime[0]->open_close_wed;
                     $open_close_thu = $bustime[0]->open_close_thu;
                     $open_close_fri = $bustime[0]->open_close_fri;
                     $open_close_sat = $bustime[0]->open_close_sat;
                     $open_close_sun = $bustime[0]->open_close_sun;
                  } else {

                     $mon = array('', '');
                     $tue = array('', '');
                     $wed = array('', '');
                     $thu = array('', '');
                     $fri = array('', '');
                     $sat = array('', '');
                     $sun = array('', '');
                     $open_close_mon = '0';
                     $open_close_tue = '0';
                     $open_close_wed = '0';
                     $open_close_thu = '0';
                     $open_close_fri = '0';
                     $open_close_sat = '0';
                     $open_close_sun = '0';
                  }

                  ?>

                  <div class="row" style="padding-top: 10px;">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group"><label for="exampleInputEmail1">Monday</label></div>
                        </div>
                     </div>

                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="ocmon" name="open_close_mon" value="1" class="" <?php if ($open_close_mon == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>



                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='mondatetimepicker31'>
                                 <input type='text' class="form-control" id="mon1" name="mon1" value="{{ $mon[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='mondatetimepicker32'>
                                 <input type='text' class="form-control" id="mon2" name="mon2" value="{{ $mon[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Tuesday</label>
                           </div>
                        </div>
                     </div>

                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="octue" name="open_close_tue" value="1" class="" <?php if ($open_close_tue == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>

                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='tuedatetimepicker31'>
                                 <input type='text' class="form-control" id="tue1" name="tue1" value="{{ $tue[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='tuedatetimepicker32'>
                                 <input type='text' class="form-control" id="tue2" name="tue2" value="{{ $tue[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>

                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Wednesday</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="ocwed" name="open_close_wed" value="1" class="" <?php if ($open_close_wed == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='weddatetimepicker31'>
                                 <input type='text' class="form-control" id="wed1" name="wed1" value="{{ $wed[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='weddatetimepicker32'>
                                 <input type='text' class="form-control" id="wed2" name="wed2" value="{{ $wed[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Thursday</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="octhu" name="open_close_thu" value="1" class="" <?php if ($open_close_thu == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='thudatetimepicker31'>
                                 <input type='text' class="form-control" id="thu1" name="thu1" value="{{ $thu[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='thudatetimepicker32'>
                                 <input type='text' class="form-control" id="thu2" name="thu2" value="{{ $thu[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Friday</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="ocfri" name="open_close_fri" value="1" class="" <?php if ($open_close_fri == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='fridatetimepicker31'>
                                 <input type='text' class="form-control" id="fri1" name="fri1" value="{{ $fri[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='fridatetimepicker32'>
                                 <input type='text' class="form-control" id="fri2" name="fri2" value="{{ $fri[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>



                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Saturday</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="ocsat" name="open_close_sat" value="1" class="" <?php if ($open_close_sat == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='satdatetimepicker31'>
                                 <input type='text' class="form-control" id="sat1" name="sat1" value="{{ $sat[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='satdatetimepicker32'>
                                 <input type='text' class="form-control" id="sat2" name="sat2" value="{{ $sat[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>


                  <div class="row">
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <label for="exampleInputEmail1">Sunday</label>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-2">
                        <div class='col-sm-12'>
                           <div class="form-group">
                              <input type='checkbox' id="ocsun" name="open_close_sun" value="1" class="" <?php if ($open_close_sun == 1) {
                                                                                                            echo 'checked';
                                                                                                         } ?> /> Open
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='sundatetimepicker31'>
                                 <input type='text' class="form-control" id="sun1" name="sun1" value="{{ $sun[0] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                        <div class='col-sm-4'>
                           <div class="form-group">
                              <div class='input-group date' id='sundatetimepicker32'>
                                 <input type='text' class="form-control" id="sun2" name="sun2" value="{{ $sun[1] }}" />
                                 <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                 </span>
                              </div>
                           </div>
                        </div>

                     </div>
                  </div>

               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  @if($id==0)
                  <input type="submit" class="btn btn-primary" id="psubid" onclick="return check_email_duplicate();" value="Submit" />
                  <!-- <input type="button" class="btn btn-primary" onclick="check_email_duplicate();" value="Submit"/> -->
                  @else
                  <input type="submit" class="btn btn-primary" id="psubid" value="Save" />
                  <!-- <input type="button" class="btn btn-primary" value="Save"  onclick="check_number();"/> -->
                  @endif
                  <input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);" />
               </div>
            </form>
         </div>
         <!-- /.box -->
      </div>


      <script>
         var datefield = document.createElement("input")
         datefield.setAttribute("type", "date")
         if (datefield.type != "date") { // if browser doesn't support input type="date", load files for jQuery UI Date Picker
            document.write('<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />\n')
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"><\/script>\n')
            document.write('<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"><\/script>\n')
         }
      </script>

      <script>
         if (datefield.type != "date") { // if browser doesn't support input type="date", initialize date picker widget:
            jQuery(function($) { // on document.ready

               $('#license_expiry').datepicker({
                  dateFormat: 'mm-dd-yy'
               });

               $('#dob').datepicker({
                  dateFormat: 'mm-dd-yy'
               });

               $('#permit_expiry').datepicker({
                  dateFormat: 'mm-dd-yy'
               });

            })
         }
      </script>


   </section>
   <!-- /.content -->
</aside>
<!-- /.right-side -->
@endsection
@section('js_bottom')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
   .docdown {
      font-size: 16px;
      color: gray;
   }

   .docdown a {
      color: gray;
   }

   .field-icon {
      float: right;
      margin-left: -25px;
      margin-top: -25px;
      position: relative;
      z-index: 2;
   }

   .container {
      padding-top: 50px;
      margin: auto;
   }

   .ui-widget-content {
      max-height: 221px;
      overflow-y: scroll;
   }

   .ui-menu .ui-menu-item {
      padding: 5px;
   }

   .ui-menu-item:nth-child(2n) {
      background-color: #f1f1f1;
   }
</style>
<style>
   #ajax_favorite_loddder {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background: rgba(27, 26, 26, 0.48);
      z-index: 1001;
   }

   #ajax_favorite_loddder img {
      top: 50%;
      left: 46.5%;
      position: absolute;
   }

   .footer-wrapper {
      float: left;
      width: 100%;
      /*display: none;*/
   }

   #addons-modal.modal {
      z-index: 999;
   }

   .modal-backdrop {
      z-index: 998 !important;
   }
</style>
<div id="ajax_favorite_loddder" style="display:none;">
   <div align="center" style="vertical-align:middle;">
      <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
   </div>
</div>
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- start date picker --->

<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<?php /* if ($browsername == 'Safari') { ?>
   <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<?php } */ ?>
<!--<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>-->

<script>
   $(function() {
      $("#datepicker").datepicker();
   });
   $(function() {
      $("#datepicker1").datepicker();
   });
   $(function() {
      $("#datepicker2").datepicker();
   });
</script>

<!-- End date picker -->

<script type="text/javascript">
   $(document).ready(function() {

      //$('#octue').val(this.checked);

      $('#ocmon').change(function() {

         if (this.checked) {

            $("#mon1").prop('required', true);
            $("#mon2").prop('required', true);
            $("#ocmon").val('1');

         } else {

            $("#mon1").prop('required', false);
            $("#mon2").prop('required', false);
            $("#mon1").val('');
            $("#mon2").val('');
            $("#ocmon").val('');

         }

      });

      $('#octue').change(function() {

         if (this.checked) {

            $("#tue1").prop('required', true);
            $("#tue2").prop('required', true);
            $("#octue").val('1');

         } else {

            $("#tue1").prop('required', false);
            $("#tue2").prop('required', false);
            $("#tue1").val('');
            $("#tue2").val('');
            $("#octue").val('');

         }

      });

      $('#ocwed').change(function() {

         if (this.checked) {

            $("#wed1").prop('required', true);
            $("#wed2").prop('required', true);
            $("#ocwed").val('1');

         } else {

            $("#wed1").prop('required', false);
            $("#wed2").prop('required', false);
            $("#wed1").val('');
            $("#wed2").val('');
            $("#ocwed").val('');

         }

      });

      $('#octhu').change(function() {

         if (this.checked) {

            $("#thu1").prop('required', true);
            $("#thu2").prop('required', true);
            $("#octhu").val('1');

         } else {

            $("#thu1").prop('required', false);
            $("#thu2").prop('required', false);
            $("#thu1").val('');
            $("#thu2").val('');
            $("#octhu").val('');

         }

      });

      $('#ocfri').change(function() {

         if (this.checked) {

            $("#fri1").prop('required', true);
            $("#fri2").prop('required', true);
            $("#ocfri").val('1');

         } else {

            $("#fri1").prop('required', false);
            $("#fri2").prop('required', false);
            $("#fri1").val('');
            $("#fri2").val('');
            $("#ocfri").val('');

         }

      });

      $('#ocsat').change(function() {

         if (this.checked) {

            $("#sat1").prop('required', true);
            $("#sat2").prop('required', true);
            $("#ocsat").val('1');

         } else {

            $("#sat1").prop('required', false);
            $("#sat2").prop('required', false);
            $("#sat1").val('');
            $("#sat2").val('');
            $("#ocsat").val('');

         }

      });

      $('#ocsun').change(function() {

         if (this.checked) {

            $("#sun1").prop('required', true);
            $("#sun2").prop('required', true);
            $("#ocsun").val('1');

         } else {

            $("#sun1").prop('required', false);
            $("#sun2").prop('required', false);
            $("#sun1").val('');
            $("#sun2").val('');
            $("#ocsun").val('');

         }

      });


   });
</script>

<script>
   document.getElementById('mob_no').addEventListener('input', function(e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
      e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
   });

   document.getElementById('contact_no').addEventListener('input', function(e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
      e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
   });
</script>

<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script>

<script type="text/javascript">

  /** Start Crop Image **/

   $(document).ready(function () {

   $('#profile_img1').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img2').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img3').awesomeCropper( { width: 400, height: 400, debug: true } );
   $('#profile_img4').awesomeCropper( { width: 400, height: 400, debug: true } );
  //  $('#profile_img5').awesomeCropper( { width: 400, height: 400, debug: true } );
  //  $('#profile_img6').awesomeCropper( { width: 400, height: 400, debug: true } );
  //  $('#profile_img7').awesomeCropper( { width: 400, height: 400, debug: true } );
  //  $('#profile_img8').awesomeCropper( { width: 400, height: 400, debug: true } );
  //  $('#profile_img9').awesomeCropper( { width: 400, height: 400, debug: true } );
  // $('#profile_img10').awesomeCropper( { width: 400, height: 400, debug: true } );

    });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

   setInterval(function () {

            var vendor_id = $('#vendor_id').val();
            var profile1 = $('#profile_img1').val();
             var profile2 = $('#profile_img2').val();
             var profile3 = $('#profile_img3').val();
             var profile4 = $('#profile_img4').val();
 
            if(profile1 !=0){ var allprofile = profile1; var profile_field = 'profile_img1'; var fieldnum = '1'; }
             if(profile2 !=0){ var allprofile = profile2; var profile_field = 'profile_img2'; var fieldnum = '2'; }

             if(profile3 !=0){ var allprofile = profile3; var profile_field = 'profile_img3'; var fieldnum = '3'; }
             if(profile4 !=0){ var allprofile = profile4; var profile_field = 'profile_img4'; var fieldnum = '4'; }


         if(profile1 !=0 || profile2 !=0 || profile3 !=0 || profile4 !=0){ 

            $('#psubid').prop('disabled', true);

            $.ajax({
               headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                type: "POST",
                 url: "https://www.grambunny.com/admin/crop_image_save",
                 data: {profile : allprofile, profile_field : profile_field, vendor_id : vendor_id}, 
                cache: false,
                success: function (data)
                {      
                    //console.log(data.status);
                    $('#profile_img'+fieldnum).val('');
                    $('#profile_img'+fieldnum+'_old').val(data.status);
                    $('#profile'+fieldnum+'a').hide();
                    $('#psubid').prop('disabled', false);
                }
            });

           }


    },3000); 
  
  /** Crop image */ 

</script> 

<script>
   $(".toggle-password").click(function() {



      $(this).toggleClass("fa-eye fa-eye-slash");



      var input = $($(this).attr("toggle"));



      if (input.attr("type") == "password") {



         input.attr("type", "text");



      } else {



         input.attr("type", "password");



      }



   });







   // $(".toggle-conpassword").click(function() {



   //    $(this).toggleClass("fa-eye fa-eye-slash");



   //    var input = $($(this).attr("toggle"));



   //    if (input.attr("type") == "password") {



   //       input.attr("type", "text");



   //    } else {



   //       input.attr("type", "password");



   //    }



   // });

   function check_email_duplicate() {

      var frm_val = $('#user_frm').serialize();

      $.ajax({

         type: "POST",

         url: "./check_vendor_duplicateemail",

         data: frm_val,

         success: function(msg) {

            if (msg == '1') {

               $("#ajax_parner_loddder").hide();

               $('#email_msg').show();

               return false;

            } else if (msg == '2') {

               $('#email_msg').hide();

               //$( "#user_frm" ).submit();

               return true;

            }

         }

      });

   }


   function check_email() {


      $('#error_msg').hide();



      jQuery.validator.addMethod("pass", function(value, element) {



         if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



            return true;



         } else {



            return false;



         };



      });



      jQuery.validator.addMethod("vname", function(value, element) {



         if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



            return true;



         } else {



            return false;



         };



      });







      jQuery.validator.addMethod("lname", function(value, element) {



         if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



            return true;



         } else {



            return false;



         };



      });





      var form = $("#user_frm");



      form.validate({



         rules: {



            name: {



               required: true,



               vname: true



            },



            last_name: {



               required: true,



               lname: true



            },



            mob_no: {



               //required: true,



            },



            email: {



               required: true,



               email: true



            },



            password: {



               required: true,



               pass: true,



               minlength: 8



            },



            confirm_password: {



               required: true,



               minlength: 8,



               equalTo: "#password"



            },



            state: {



               required: true,



            },



            suburb: {



               required: true,



            },



            zipcode: {



               required: true,



            }



         },



         messages: {



            name: {



               required: 'Please enter name.',



               vname: "Please enter only letters."



            },



            last_name: {



               required: 'Please enter last name.',



               lname: 'Please enter only letters.'



            },



            mob_no: {



               required: 'Please enter mobile number.',



               minlength: "Please enter valid mobile no. with country code."



            },



            email: {



               required: 'Please enter email address.',



               email: 'Please enter an valid email address.',



            },



            password: {



               required: 'Please enter password.',



               pass: "at least one number, one lowercase and one uppercase letter.",



               minlength: "Please enter at least 8 characters."



            },



            confirm_password: {



               required: 'Please enter confirm password.',



               minlength: 'Password must be at least 8 characters.',



               equalTo: 'confirm password not match, please enter correct.'



            },





            state: {



               required: 'Please enter state.',



            },



            suburb: {



               required: 'Please enter city name.',



            },



            zipcode: {



               required: 'Please enter zip code.',



            }



         }



      });



      var valid = form.valid();



      if (valid) {



         $("#ajax_parner_loddder").show();



         /*var regExp = /^0[0-9].*$/;
   
   
   
            if(regExp.test($("#mob_no").val()))
   
   
   
               {*/



         /*$(form).submit();
   
   
   
               return true;*/



         var frm_val = $('#user_frm').serialize();



         $.ajax({



            type: "POST",



            url: "./check_vendor_duplicateemail",



            data: frm_val,



            success: function(msg) {



               if (msg == '1')



               {



                  $("#ajax_parner_loddder").hide();



                  $('#email_msg').show();



                  return false;



               } else if (msg == '2')



               {



                  $(form).submit();



                  return true;



               }



            }



         });



         /*}
   
   
   
            else
   
   
   
            {
   
   
   
               //alert('Contact number start with 0.');
   
   
   
               $('#error_msg').html('Contact number start with 0.');
   
   
   
               $('#error_msg').show();
   
   
   
               return false;
   
   
   
            }*/



      } else



      {



         return false;



      }



   }



   function check_number()



   {



      var check_password = $('#password').val();



      // alert(check_password);



      var confirm_password = $('#confirm_password').val();







      jQuery.validator.addMethod("pass", function(value, element) {



         if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



            return true;



         } else {



            return false;



         };



      });











      jQuery.validator.addMethod("vname", function(value, element) {



         if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



            return true;



         } else {



            return false;



         };



      });







      jQuery.validator.addMethod("lname", function(value, element) {



         if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



            return true;



         } else {



            return false;



         };



      });







      $('#error_msg').hide();



      var form = $("#user_frm");



      form.validate({



         rules: {



            name: {



               required: true,



               vname: true



            },



            last_name: {



               required: true,



               lname: true



            },



            mob_no: {



               //required: true,



            },



            email: {



               required: true,



               email: true



            },



            password: {



               required: false,



               pass: false,



               minlength: 8



            },



            confirm_password: {



               required: false,



               minlength: 8,



               equalTo: "#password"



            },



            state: {



               required: true,



            },



            suburb: {



               required: true,



            },



            zipcode: {



               required: true,



            }



         },



         messages: {



            name: {



               required: 'Please enter name.',



               vname: "Please enter only letters."



            },



            last_name: {



               required: 'Please enter last name.',



               lname: 'Please enter only letters.'



            },



            mob_no: {



               required: 'Please enter mobile number.',



               minlength: "Please enter valid mobile no. with country code."



            },



            email: {



               required: 'Please enter email address.',



               email: 'Please enter an valid email address.',



            },



            password: {



               required: 'Please enter password.',



               pass: "at least one number, one lowercase and one uppercase letter.",



               minlength: "Please enter at least 8 characters."



            },



            confirm_password: {



               required: 'Please enter confirm password.',



               minlength: 'Password must be at least 8 characters.',



               equalTo: 'confirm password not match, please enter correct.'



            },



            state: {



               required: 'Please enter state.',



            },



            suburb: {



               required: 'Please enter city name.',



            },



            zipcode: {



               required: 'Please enter zip code.',



            }



         }



      });



      var valid = form.valid();



      if (valid) {



         /*var regExp = /^0[0-9].*$/;
   
   
   
            if(regExp.test($("#mob_no").val()))
   
   
   
               {*/



         $(form).submit();



         return true;



         /*}
   
   
   
            else
   
   
   
            {
   
   
   
               //alert('Contact number start with 0.');
   
   
   
               $('#error_msg').html('Contact number start with 0.');
   
   
   
               $('#error_msg').show();
   
   
   
               return false;
   
   
   
            }*/



      } else



      {



         return false;



      }



   }
</script>
<script>
   // This example displays an address form, using the autocomplete feature



   // of the Google Places API to help users fill in the information.



   // This example requires the Places library. Include the libraries=places



   // parameter when you first load the API. For example:



   // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



   var placeSearch, autocomplete;



   var componentForm = {



      //street_number: 'short_name',



      //route: 'long_name',



      //locality: 'long_name',



      //administrative_area_level_1: 'short_name',



      //country: 'long_name',



      //postal_code: 'short_name'



   };



   function initAutocomplete() {



      // Create the autocomplete object, restricting the search to geographical



      // location types.



      autocomplete = new google.maps.places.Autocomplete(



         /** @type {!HTMLInputElement} */
         (document.getElementById('autocomplete')),



         {
            types: ['geocode']
         });



      // When the user selects an address from the dropdown, populate the address



      // fields in the form.



      autocomplete.addListener('place_changed', fillInAddress);



   }



   function fillInAddress() {



      // Get the place details from the autocomplete object.



      var place = autocomplete.getPlace();



      for (var component in componentForm) {



         document.getElementById(component).value = '';



         document.getElementById(component).disabled = false;



      }



      // Get each component of the address from the place details



      // and fill the corresponding field on the form.



      for (var i = 0; i < place.address_components.length; i++) {



         var addressType = place.address_components[i].types[0];



         if (componentForm[addressType]) {



            var val = place.address_components[i][componentForm[addressType]];



            document.getElementById(addressType).value = val;



            //alert(val);



         }



      }



   }



   // Bias the autocomplete object to the user's geographical location,



   // as supplied by the browser's 'navigator.geolocation' object.



   function geolocate() {



      if (navigator.geolocation) {



         navigator.geolocation.getCurrentPosition(function(position) {



            var geolocation = {



               lat: position.coords.latitude,



               lng: position.coords.longitude



            };



            var circle = new google.maps.Circle({



               center: geolocation,



               radius: position.coords.accuracy



            });



            autocomplete.setBounds(circle.getBounds());



         });



      }



   }
</script>
<script type="text/javascript">
   $('#pstype').on('change', function() {



      var psId = $(this).val();



      //$("#ajax_favorite_loddder").show();  



      var frm_val = "psid=" + psId;



      $.ajax({

         type: "post",

         headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
         },

         data: {

            "_token": "{{ csrf_token() }}",

            "psid": psId

         },

         url: "{{url('/admin/category')}}",



         success: function(msg) {



            //$("#ajax_favorite_loddder").hide();  



            $('#category').html(msg);



            //$("#addons-modal").modal('show');



         }

      });



   });





   $('#category').on('change', function() {



      var catId = $(this).val();



      $.ajax({

         type: "post",

         headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
         },

         data: {

            "_token": "{{ csrf_token() }}",

            "catid": catId

         },

         url: "{{url('/admin/subcategory')}}",



         success: function(msg) {



            $('#subcategory').html(msg);



         }

      });



   });
</script>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&libraries=places&callback=initAutocomplete"></script>


@stop