@extends('layouts.admin')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Product Brand Form

			<small>Control panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Product Brand</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

		<div class="col-md-12">

			<!-- general form elements -->

			<div class="box box-primary">

				<div class="box-header">

					<h3 class="box-title">Product Brand</h3>

				</div><!-- /.box-header -->

				<p style="float: left;text-align: center;width: 100%;">

					@if(Session::has('message'))

					{{Session::get('message')}}

				@endif </p>

				<!-- form start -->

				<form  role="form" method="POST" id="brand_frm" action="{{ url('/admin/product_brand_action') }}">
	
	  @csrf
					<div class="box-body">

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Brand</label>
									<input type="text" class="form-control" name="brand_name" id="brand_name" value="" required="required">
								</div>	
							</div>
							<div class="col-md-6">&nbsp;</div>
						</div>	

						</div> <!-- /.box-body -->

						<div class="box-footer">

						<input type="submit" class="btn btn-primary" value="Submit"  />

						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

						</div>

					</form>

				</div><!-- /.box -->

			</div>

		</section><!-- /.content -->

	</aside><!-- /.right-side -->

	@endsection

