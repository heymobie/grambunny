@extends('layouts.admin')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<style type="text/css">

	.field-icon {

		float: right;

		margin-left: -25px;

		margin-top: -25px;

		position: relative;

		z-index: 2;

	}

.downimg{ padding: 4px 19px; }

.mandatory{
    color:red;
    font-size: large;
}

.awesome-cropper{ width: 100px; }

.modal-content canvas{
  width: 100%;
}

.modal-dialog11{
width: 50% !important;
margin: auto !important;
    }

</style>
<link href="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/css/imgareaselect-default.css" rel="stylesheet" type="text/css" />
<link href="{{ url('/') }}/public/design/admin/imagecrop/css/jquery.awesome-cropper.css" rel="stylesheet" type="text/css" />

<!-- Content Header (Page header) -->

<section class="content-header">

	<h1>



		Customer Form



		<small>Control Panel</small>



	</h1>



	<ol class="breadcrumb">



		<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>



		<li class="active">Customer Form</li>



	</ol>



</section>



<!-- Main content -->

<section class="content">

	<div class="col-md-12">

		<!-- general form elements -->

		<div class="box box-primary">

			<div class="box-header">

				<h3 class="box-title">Customer Form</h3>

			</div><!-- /.box-header -->


			<p style="float: left;text-align: center;width: 100%;">

				@if(Session::has('message'))

				{{Session::get('message')}}


			@endif </p>


			<!-- form start -->


			<form  role="form" method="POST" id="user_frm" action="{{ url('/admin/user_action') }}"  enctype="multipart/form-data">


			<input type="hidden" name="user_id" value="{{$id}}" />

				{!! csrf_field() !!}

				<div class="box-body">

					<div class="row">

						<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Name<span class="mandatory">*</span></label>

								<input type="text" class="form-control" name="name" id="name" value="@if($id>0){{trim($user_detail[0]->name)}}@endif" required="required">

							</div>

						</div>

						<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Last Name<span class="mandatory">*</span></label>

								<input type="text" class="form-control" name="lname" id="lname" value="@if($id>0){{trim($user_detail[0]->lname)}}@endif" required="required">

							</div>							

						</div>
							<div class="col-md-4">

							<div class="form-group">

								<label for="exampleInputEmail1">Phone Number<span class="mandatory">*</span></label>

								<input type="text" class="form-control form-white"  name="user_mob" id="user_mob" required="required" value="<?php if($id>0) { echo trim($user_detail[0]->user_mob);}?>"  @if(($id>0) && (!empty(trim($user_detail[0]->user_mob))) )  @endif >

								<div id="number_msg" class="eror_msg" style="display:none; color:#FF0000">An account with this phone number is already registered</div>

							</div>							

						</div>
					</div>



					<div class="row">

					

						<div class="col-md-4">

							<div class="form-group">

							<label for="exampleInputEmail1">Email<span class="mandatory">*</span></label>

							<input type="email" class="form-control" name="email" id="email" required="required" value="@if($id>0) {{$user_detail[0]->email}} @endif" @if($id>0) @endif>

							 @if($errors->has('email'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('email') }}</div>
                        @endif

							<div id="email_msg" style="display:none; color:#FF0000">An account with this email is already registered</div>

						</div>

						</div>
							<div class="col-md-4">

								<div class="form-group">

									<label>Date of Birth (MM/DD/YYYY)<span class="mandatory">*</span></label>
                   
									 <?php if($id>0){

                     if($user_detail[0]->dob!='0000-00-00'){
									 	$user_dobs = date("m/d/Y", strtotime($user_detail[0]->dob));}else{$user_dobs = '';}}?>

									 <input placeholder="MM/DD/YYYY" name="dob" value="@if(!empty($user_dobs)){{$user_dobs}}@endif" onkeyup="date_reformat_mm(this);" onkeypress="date_reformat_mm(this);" onpaste="date_reformat_mm(this);" autocomplete="off" type="text" class="form-control {{ $errors->has("dob") ? "is-invalid" : "" }}" >

                             @if($errors->has("dob"))

                            <span class="text-danger">{{ $errors->first("dob") }}</span>

                            @endif

									<!-- <input type="text" id="user_dob" name="user_dob" class="form-control" value="@if($id>0){{$user_dobs}}@endif" placeholder="mm/dd/yyyy"> -->

								</div>

							</div>
								<div class="col-md-4">

								<div class="form-group">

									<label>Address<span class="mandatory">*</span></label>

									<input type="text" class="form-control" name="user_address" id="autocomplete85" value="@if($id>0){{$user_detail[0]->user_address}}@endif" required="required">

							

								</div>

							</div>
					</div>

				

						<div class="row">

					    <div class="col-md-4">

								<div class="form-group">

									<label>City<span class="mandatory">*</span></label>

									<input type="text" id="locality" name="user_city" class="form-control" value="@if($id>0){{$user_detail[0]->user_city}}@endif" required="required">

								</div>

							</div>


							<div class="col-md-4">

								<div class="form-group">

									<label>State<span class="mandatory">*</span></label>

			<p><select class="form-control" name="user_states" id="user_states" required="">

              <option value="">Choose...</option>

                  @if(!$state_list->isEmpty())

                @foreach($state_list as $arr)

                <option value="{{$arr->name}}" <?php if(!empty($user_detail[0]->user_states)){ if($user_detail[0]->user_states == $arr->name){ ?> selected="selected" <?php } }?>>{{$arr->name}}</option>

                    @endforeach

                  @endif

                </select></p>

								</div>

							</div>	
							<div class="col-md-4">

								<div class="form-group">

									<label>Zip Code<span class="mandatory">*</span></label>

									<input type="text" id="postal_code" name="user_zipcode" class="form-control" value="@if($id>0){{$user_detail[0]->user_zipcode}}@endif" number="number" maxlength="6" required="required">

								</div>

							</div>

						</div>



						<div class="row">

							

							<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">User status</label>

									<select name="user_status" id="user_status"  class="form-control">

										<option value="1" <?php if(($id>0) && ($user_detail[0]->user_status=='1')) {echo 'selected="selected"';}?>>Active</option>

										<option value="0" <?php if(($id>0) && ($user_detail[0]->user_status=='0')) {echo 'selected="selected"';}?>>Inactive</option>

									</select>

								</div>	

							</div>
							<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">Password<span class="mandatory">*</span></label>

									<input type="password" class="form-control" name="password" id="password" <?php if($id==0){?>required="required"<?php } ?>>

									 @if($errors->has('password'))
                           <div class="error text-red-500 text-xs">{{ $errors->first('password') }}</div>
                        @endif

									<span toggle="#password" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>

								</div>



							</div>
								<div class="col-md-4">

								<div class="form-group">

									<label for="exampleInputEmail1">Confirm Password<span class="mandatory">*</span></label>

									<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" <?php if($id==0){?>required="required"<?php } ?>>

									<span toggle="#confirmpassword" class="fa fa-fw fa-eye-slash field-icon toggle-confirmpassword"></span>

								</div>

							</div>
						</div>


						<div class="row">

							<div class="col-md-4">

							<div class="form-group">

							<label>Profile</label>

							<input type="hidden" id="profile_image" name="profile_image" class="form-control" value="" accept="image/png, image/gif, image/jpeg">

              <input type="hidden" name="profile_old" id="profile_old" value="@if($id>0){{$user_detail[0]->profile_image}} @endif">

              <input type="hidden" value="1" name="profile1">

               <?php if(!empty($user_detail[0]->profile_image)){ ?> 

               <span class="img-gl"	id="proimgthumb">

               <img width="100px" height="100px" src="<?php if($id>0){ echo url('/public/uploads/user/').'/'.$user_detail[0]->profile_image; } ?>">
               </span>

                <?php } ?>

						    </div>

							</div>


							<!--- licence --->
							<div class="col-md-4">

								<div class="form-group">

									<label>Upload Copy of Driver's License Front Side<span class="mandatory">*</span></label>

									<input type="file" id="license_front" name="license_front" class="form-control" value="" <?php if(empty($user_detail[0]->license_front)){ ?> required="required" <?php } ?> >

									<input type="hidden" name="license_front_old" value="@if($id>0){{$user_detail[0]->license_front}} @endif">

								  <?php if(!empty($user_detail[0]->license_front)){ ?> 

                                   <object width="100px" height="100px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_front; ?>"></object>  

                                    <div class=""><a href="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_front; ?>" class="btn btn-primary downimg" download>Download</a></div>

                                 <?php } ?>

								</div>

							</div>

							<div class="col-md-4">

								<div class="form-group">

									<label>Upload Copy of Driver's License Back Side<span class="mandatory">*</span></label>

									<input type="file" id="license_back" name="license_back" class="form-control" value="" <?php if(empty($user_detail[0]->license_back)){ ?> required="required" <?php } ?>>

									<input type="hidden" name="license_back_old" value="@if($id>0){{$user_detail[0]->license_back}} @endif">

									<?php if(!empty($user_detail[0]->license_back)){ ?>  
                                 
                                   <object width="100px" height="100px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_back; ?>"></object> 

                                    <div class=""><a href="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->license_back; ?>" class="btn btn-primary downimg" download>Download</a></div>
                                    
                                 <?php } ?>

								</div>

							</div>
							<!-- end licence -->

							<div class="col-md-4">

							<div class="form-group">

							<label for="exampleInputEmail1">Type Of Customer</label>

							<select name="customer_type" id="customer_type"  class="form-control">

							<option value="Recreational Customer" <?php if(($id>0) && ($user_detail[0]->customer_type=='Recreational Customer')) {echo 'selected="selected"';}?>>Recreational Customer</option>

							<option value="Medical Customer" <?php if(($id>0) && ($user_detail[0]->customer_type=='Medical Customer')) {echo 'selected="selected"';}?>>Medical Customer</option>

							</select>

							</div>	

							</div>



							<div class="col-md-4">

								<div class="form-group">

								<label>Upload Copy of Medical Marijuana ID Card Front Side</label>

								<input type="file" id="marijuana_card" name="marijuana_card" class="form-control" value="">

								<input type="hidden" name="marijuana_card_old" value="@if($id>0){{$user_detail[0]->marijuana_card}} @endif">

									<?php if(!empty($user_detail[0]->marijuana_card)){ ?>  
                        
                                   <object width="100px" height="100px" data="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->marijuana_card; ?>"></object> 

                                    <div class=""><a href="<?php echo url('/public/uploads/user/').'/'.$user_detail[0]->marijuana_card; ?>" class="btn btn-primary downimg" download>Download</a></div>
                                    
                                 <?php } ?>


								</div>

							</div>




						</div>


					</div>

					

					<div class="box-footer">



						@if($id==0)



						<input type="submit" class="btn btn-primary" value="Submit" id="psubid"/>



						@else



						<input type="submit" class="btn btn-primary" value="Save" id="psubid"/>



						@endif



						<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />



					</div>



				</form>



			</div><!-- /.box -->



		</div>



	</section><!-- /.content -->



</aside><!-- /.right-side -->



@endsection



<style>



#ajax_parner_loddder {



	position: fixed;



	top: 0;



	left: 0;



	width: 100%;



	height: 100%;



	background:rgba(27, 26, 26, 0.48);



	z-index: 1001;



}



#ajax_parner_loddder img {



	top: 50%;



	left: 46.5%;



	position: absolute;



}



.footer-wrapper {



	float: left;



	width: 100%;



}



#addons-modal.modal {



	z-index: 999;



}



</style>



<div id="ajax_parner_loddder" style="display:none;">



	<div align="center" style="vertical-align:middle;">



		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />



	</div>



</div>



@section('js_bottom')



<!-- jQuery 2.0.2 -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>



<!-- jQuery UI 1.10.3 -->



<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>



<!-- Bootstrap -->



<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>



<!-- Bootstrap WYSIHTML5 -->



<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>



<!-- AdminLTE App -->



<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>



<!-- AdminLTE dashboard demo (This is only for demo purposes) -->



<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>



<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>


<script>
  document.getElementById('user_mob').addEventListener('input', function (e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
});
</script>

<script>


	$(".toggle-password").click(function() {



		$(this).toggleClass("fa-eye-slash fa-eye");



		var input = $($(this).attr("toggle"));



		if (input.attr("type") == "password") {



			input.attr("type", "text");



		} else {



			input.attr("type", "password");



		}



	});















	$(".toggle-confirmpassword").click(function(){



		$(this).toggleClass("fa-eye-slash fa-eye");



		var input = $($(this).attr("toggle"));



		if (input.attr("type") == "password") {



			input.attr("type", "text");



		} else {



			input.attr("type", "password");



		}



	});



	function check_email()
	{



		jQuery.validator.addMethod("pass", function (value, element) {



			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



				return true;



			} else {



				return false;



			};



		});











		jQuery.validator.addMethod("vname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});











		// jQuery.validator.addMethod("add", function (value, element) {



		// 	if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



		// 		return true;



		// 	} else {



		// 		return false;



		// 	};



		// });











		jQuery.validator.addMethod("lname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});



		var form = $("#user_frm");

		form.validate({

			rules: {

				name:{

					required:true,

					vname:true

				},

				lname: {

					required: true,



					lname:true


				},


				user_mob: {


					//required: true,

					//minlength:10,

					//maxlength:10

				},


				email: {


					required: true,

					email:true

				},

				password: {

					required: true,

					minlength:8,

					pass:true,

				},


				confirmpassword: {

					required: true,

					minlength:8,

					equalTo : "#password",

				},



				user_address: {



					required: true,



				},



				state: {



					required: false,



				},



				city: {



					required: false,



				},



				zipcode: {



					required: false,



				}



			},



			messages: {



				name:{



					required:'Please enter name.',



					vname:"Please enter only letters."



				},



				lname: {



					required:'Please enter last name.',



					lname:'Please enter only letters.'



				},



				user_mob: {



					required:'Please enter mobile number.',



					minlength:'Please enter valid mobile number.',



					maxlength:'Please enter valid mobile number.'



				},



				email: {



					required:'Please enter email address.',



					email:'Please enter an valid email address.',



				},



				password: {



					required:'Please enter password.',



					minlength:'Password must be at least 8 characters.',



					pass:"at least one number, one lowercase and one uppercase letter.",



				},



				confirmpassword: {



					required:'Please enter confirm password.',



					minlength:'Password must be at least 8 characters.',



					equalTo:'confirm password and password should be same, please enter correct.'



				},



				user_address: {



					required:'Please enter postal address.',



				},



				state: {



					required:'Please enter state.',



				},



				city: {



					required:'Please enter city name.',



				},



				zipcode: {



					required:'Please enter zipcoad.',



				}



			}



		});



		var valid =	form.valid();



		if(valid){





				$("#ajax_parner_loddder").show();



				var frm_val = $('#user_frm').serialize();



				$.ajax({



					type: "POST",



					url: "./check_user_duplicateemail",



					data: frm_val,



					success: function(msg) {



						$("#ajax_parner_loddder").hide();



						if(msg=='1')



						{



							$('#email_msg').show();



							$('#number_msg').show();



							return false;



						}



						else if(msg=='2')



						{



							$('#email_msg').show();



							$('#number_msg').hide();



							return false;



						}



						else if(msg=='3')



						{



							$('#number_msg').show();



							$('#email_msg').hide();



							return false;



						}



						else if(msg=='4')



						{



							$(form).submit();



							return true;



						}



					}



				});





		}else{



			return false;



		}



	}



	function check_valid()



	{



		jQuery.validator.addMethod("pass", function (value, element) {



			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {



				return true;



			} else {



				return false;



			};



		});





		jQuery.validator.addMethod("vname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});



		// jQuery.validator.addMethod("add", function (value, element) {



		// 	if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



		// 		return true;



		// 	} else {



		// 		return false;



		// 	};



		// });




		jQuery.validator.addMethod("lname", function (value, element) {



			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {



				return true;



			} else {



				return false;



			};



		});



		var form = $("#user_frm");

		form.validate({

			rules: {

				name:{

					required:true,

					vname:true

				},

				lname: {

					required: true,

					lname:true

				},

				user_mob: {


					//required: true,



					//minlength:10,



					//maxlength:10



				},



				email: {



					required: true,



					email:true



				},



				password: {



					required: false,



					minlength:8,



					pass:false,



				},



				confirmpassword: {



					required: false,



					minlength:8,



					equalTo : "#password",



				},



				user_address: {



					required: true,



				},



				state: {



					required: false,



				},



				city: {



					required: false,



				},



				zipcode: {



					required: false,



				}



			},



			messages: {



				name:{



					required:'Please enter name.',



					vname:"Please enter only letters."



				},



				lname: {



					required:'Please enter last name.',



					lname:'Please enter only letters.'



				},



				user_mob: {



					required:'Please enter mobile number.',



					minlength:'Please enter valid mobile number.',



					maxlength:'Please enter valid mobile number.'



				},



				email: {



					required:'Please enter email address.',



					email:'Please enter an valid email address.',



				},



				password: {



					required:'Please enter password.',



					minlength:'Password must be at least 8 characters.',



					pass:"at least one number, one lowercase and one uppercase letter.",



				},



				confirmpassword: {



					required:'Please enter confirm password.',



					minlength:'Password must be at least 8 characters.',



					equalTo:'confirm password and password should be same, please enter correct.'



				},



				user_address: {



					required:'Please enter postal address.',



				},



				state: {



					required:'Please enter state.',



				},



				city: {



					required:'Please enter city name.',



				},



				zipcode: {



					required:'Please enter zipcoad.',



				}



			}



		});



		var valid =	form.valid();



		if(valid){



			// var dummy1 = $('#dummy1').val();



			// var dummy2 = $('#dummy2').val();



			// var dummy3 = $('#dummy3').val();



			// var dummy_counter = 0;



			// if((dummy1==0) && (dummy2==0) && (dummy3==0))



			// {



			// 	dummy_counter = 1;



			// }



			// else



			// {



			// 	if( (dummy1==0) || (dummy2==0) || (dummy3==0) )



			// 	{



			// 		alert('Default user not match with other User , So Please select only default user or  different user!');



			// 		dummy_counter = 0;



			// 	}



			// 	else if( (dummy1==dummy2) || (dummy2==dummy3) || (dummy1==dummy3) )



			// 	{



			// 		alert('User can not be same, So Please select different user!');



			// 		dummy_counter = 0;



			// 	}



			// 	else



			// 	{



			// 		dummy_counter = 1;



			// 	}



			// }



			// if(dummy_counter==1)



			// {



				var frm_val = $('#user_frm').serialize();



				$(form).submit();



			// }



			// else



			// {



			// 	return false;



			// }



				/*var frm_val = $('#user_frm').serialize();



				$(form).submit();*/



			}



			else



			{



				return false;



			}



		}



	</script>



	<script>



      // This example displays an address form, using the autocomplete feature



      // of the Google Places API to help users fill in the information.



      // This example requires the Places library. Include the libraries=places



      // parameter when you first load the API. For example:



      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">



      var placeSearch, autocomplete;



      var componentForm = {



      	street_number: 'short_name',



      	route: 'long_name',



      	locality: 'long_name',



      	administrative_area_level_1: 'short_name',



      	country: 'long_name',



      	postal_code: 'short_name'



      };



      function initAutocomplete() {



        // Create the autocomplete object, restricting the search to geographical



        // location types.



        autocomplete = new google.maps.places.Autocomplete(



        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),



        	{types: ['geocode']});



        // When the user selects an address from the dropdown, populate the address



        // fields in the form.



        autocomplete.addListener('place_changed', fillInAddress);



    }



    function fillInAddress() {



        // Get the place details from the autocomplete object.



        var place = autocomplete.getPlace();



        for (var component in componentForm) {



        	document.getElementById(component).value = '';



        	document.getElementById(component).disabled = false;



        }



        // Get each component of the address from the place details



        // and fill the corresponding field on the form.



        for (var i = 0; i < place.address_components.length; i++) {



        	var addressType = place.address_components[i].types[0];



        	if (componentForm[addressType]) {



        		var val = place.address_components[i][componentForm[addressType]];



        		document.getElementById(addressType).value = val;



			//alert(val);



		}



	}



}



      // Bias the autocomplete object to the user's geographical location,



      // as supplied by the browser's 'navigator.geolocation' object.



      // function geolocate() {



      // 	if (navigator.geolocation) {



      // 		navigator.geolocation.getCurrentPosition(function(position) {



      // 			var geolocation = {



      // 				lat: position.coords.latitude,



      // 				lng: position.coords.longitude



      // 			};



      // 			var circle = new google.maps.Circle({



      // 				center: geolocation,



      // 				radius: position.coords.accuracy



      // 			});



      // 			autocomplete.setBounds(circle.getBounds());



      // 		});



      // 	}



      // }



  </script>



  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&libraries=places&callback=initAutocomplete"></script>


<script src="{{ url('/') }}/public/design/admin/imagecrop/components/imgareaselect/scripts/jquery.imgareaselect.js"></script> 
<script src="{{ url('/') }}/public/design/admin/imagecrop/build/jquery.awesome-cropper.js"></script> 

<script type="text/javascript">

   $(document).ready(function () {

   $('#profile_image').awesomeCropper( { width: 600, height: 600, debug: true } );

    });

   var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();

setInterval(function () {

var profile_image = $('#profile_image').val();

if(profile_image!=0){

$('#psubid').prop('disabled', true);

}     

},2000); 

    setInterval(function () {

            var user_id = $('#user_id').val(); 
            var profile_image = $('#profile_image').val();

           // alert(profile_image);
          
            if(profile_image!=0){

            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }, 
                type: "POST",
                url: "https://www.grambunny.com/admin/crop_profile_image_save",
                data: {productimg : profile_image, user_id : user_id}, 
                cache: false,
                success: function (data)
                {          
                    $('#profile_image').val('');
                    $('#profile_old').val(data.status);
                    $('#proimgthumb').hide();
                    $('#psubid').prop('disabled', false);
                }
            });

           }

          },6000); 
  
  /** Crop image */ 

</script>

<script>
        
            function checkValue(str, max) {
                if (str.charAt(0) !== '0' || str == '00') {
                    var num = parseInt(str);
                    if (isNaN(num) || num <= 0 || num > max) num = 1;
                    str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
                };
                return str;
            };

  // reformat by date
       function date_reformat_mm(date) {
            date.addEventListener('input', function(e) {
                this.type = 'text';
                var input = this.value;
                if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
                var values = input.split('/').map(function(v) {
                    return v.replace(/\D/g, '')
                });
                if (values[0]) values[0] = checkValue(values[0], 12);
                if (values[1]) values[1] = checkValue(values[1], 31);
                var output = values.map(function(v, i) {
                    return v.length == 2 && i < 2 ? v + '/' : v;
                });
                this.value = output.join('').substr(0, 14);
            });


        }
    </script>
  @stop