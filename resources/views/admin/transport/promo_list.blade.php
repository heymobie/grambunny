<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Promotions Listing</h3><br />
			<div style="margin-top:10px;">
			<a  href="javascript:void(0)" data-rest="{{$transport_id}}" id="promo_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Promo</a>
				</div>
			
		</div><!-- /.box-header -->
		<div class="box-body table-responsive">

@if(Session::has('promo_message'))

<div class="alert alert-success alert-dismissable">
  <i class="fa fa-check"></i>
   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
			   {{Session::get('promo_message')}}
</div>
@endif 
	
			<table id="example2" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>SrNo</th>
						<th>On Order</th>
						<th>Description</th>
						<th>Mode</th>
						<th>Value</th>
						<th>Start</th>
						<th>End</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@if($promo_list)<?php $c=1;?>
				  @foreach($promo_list as $plist )
					<tr>
						<td>{{$c++}}</td>
						<td>{{$plist->tport_promo_on}}</td>
						<td>{{$plist->tport_promo_desc}}</td>
						<td>{{$plist->tport_promo_mode}}</td>
						<td>{{$plist->tport_promo_value}}</td>
						<td>{{$plist->tport_promo_start}}</td>
						<td>{{$plist->tport_promo_end}}</td>
						<td>
							@if($plist->tport_promo_status==1) 
								<span class="label label-success">Active</span>
							@else 
								<span class="label label-danger">Inactive</span>
							@endif
						</td>
						<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$transport_id}}" data-promo="{{$plist->tport_promo_id}}" id="update_promo-{{$plist->tport_promo_id}}">Edit</a></td>
					</tr>	
				  @endforeach
				@endif  		                                     
				</tbody>
				<tfoot>
					<tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
					</tr>
				</tfoot>
			</table>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</div>
