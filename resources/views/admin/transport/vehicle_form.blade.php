
	<!-- Main content -->
	<section class="content">
	<div class="col-xd-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Transport Vehicle Form</h3>
                                </div><!-- /.box-header -->
							
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_frm" action="{{ url('/admin/menu_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="vehicle_transid" value="{{$transport_id}}" />
								<input type="hidden" name="vehicle_id" value="{{$id}}" />
							
								{!! csrf_field() !!}
                                    <div class="box-body">
									<table border="0" width="100%" cellpadding="2" cellspacing="2">
										<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Add Image</td>
													<td>  
													 <input type="file" id="other_images"  name="other_images[]" multiple/>
                        <output id="selectedFiles" class="dropzone"/>   
						<?php if(($id>0) && (!empty($vehicle_images))){?>
						
						<div class="tab_five_cont"  id="all_images">
						 @foreach ($vehicle_images as $img_list)
						 	<?php $checked = '';
								if($img_list->isCover){$checked ='checked="checked"';}
							?>
                       	<div class="col-md-3">  
                        <div class="tab_five_img">
                        <img src="{{ url('/') }}/uploads/vehicle/{{ $img_list->imgPath }}" />
						 
						<img title="Delete"  alt="{{ $img_list->imgPath }}" vimageid="{{$img_list->id}}"  src="{{ url('/') }}/design/front/img/delete.png" style="margin:3px 3px 3px 3px" onclick="delete_image('{{$id}}','{{$img_list->id}}');">
						
						<input  type="radio" name="isCover" id="isCover" value="{{$img_list->id}}" {{$checked}}/> Is Cover
						
                        </div></div>
						@endforeach
                        </div>
						<?php }?>
														</td>
													</tr>
													
													
													
						
						
												</table>
											</td>	
										</tr>
										<tr>
											<td>
											<table width="100%" cellpadding="2" cellspacing="2">
											 <tr>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Rego</td>
															<td><input type="text" class="form-control" name="vehicle_rego" id="vehicle_rego" value="@if($id>0){{$vehicle_detail[0]->vehicle_rego}}@endif" required="required"></td>
														</tr>
														<tr>
															<td>Vehicle Make</td>
															<td>
									<select class="form-control" name="vehicle_make" id="vehicle_make" required="required">
									<option value="">Select Make</option>			
									
										<?php if(count($make_list)>0)
										{
											foreach($make_list as $mlist)
											{
												
												$select = '';
												if(($id>0)&& ($vehicle_detail[0]->vehicle_make==$mlist->make_id))
												{
												 	$select = 'selected="selected"';
												}
											?>
											<option value="{{$mlist->make_id}}" {{$select}}>{{$mlist->make_name}}</option>				
											<?php
											}
										}
										?>
									</select>
																
															</td>
														</tr>
														<tr>
															<td>Vehicle Model</td>
															<td id="model_selection">
															
									<select class="form-control" name="vehicle_model" id="vehicle_model" required="required">
									<option value="">Select Model</option>			
									
										<?php if(count($model_list)>0)
										{
											foreach($model_list as $mlist)
											{
												$select = '';
												if(($id>0)&& ($vehicle_detail[0]->vehicle_model==$mlist->model_id))
												{
												 	$select = 'selected="selected"';
												}
											?>
											<option value="{{$mlist->model_id}}" {{$select}}>{{$mlist->model_name}}</option>				
											<?php
											}
										}
										?>
									</select>
															</td>
														</tr>
														<tr>
															<td>Year</td>
															<td><select class="form-control" name="vehicle_year" id="vehicle_year" required="required">
									<option value="">Select Year</option>			
									
										<?php 
											for($y=date('Y');$y>=1900;$y--)
											{											
												$select = '';
												if(($id>0)&& ($vehicle_detail[0]->vehicle_year==$y))
												{
												 	$select = 'selected="selected"';
												} 
											?>
											<option value="{{$y}}" {{$select}}>{{$y}}</option>				
											<?php
											}
										?>
									</select></td>
														</tr>
														<tr>
															<td>Vehicle Category</td>
															<td>
								<select class="form-control" name="vehicle_catid" id="vehicle_catid" required="required">
									<option value="">Select Category</option>			
									
										<?php if(count($cate_list)>0)
										{
											foreach($cate_list as $clist)
											{
												
												$select = '';
												if(($id>0)&& ($vehicle_detail[0]->vehicle_catid==$clist->vcate_id))
												{
												 	$select = 'selected="selected"';
												} 
											?>
											<option value="{{$clist->vcate_id}}" {{$select}}>{{$clist->vcate_name}}</option>				
											<?php
											}
										}
										?>
									</select></td>
														</tr>
													</table>
												</td>
												<td style="border:1px solid #000000;">
													<table width="100%" cellpadding="2" cellspacing="2">
														<tr>
															<td>Vehicle Class</td>
															<td>
								<select class="form-control" name="vehicle_classid" id="vehicle_classid" required="required">
									<option value="">Select Class</option>			
									
										<?php if(count($class_list)>0)
										{
											foreach($class_list as $clist)
											{
												$select = '';
												if(($id>0)&& ($vehicle_detail[0]->vehicle_classid==$clist->vclass_id))
												{
												 	$select = 'selected="selected"';
												} 
											?>
											<option value="{{$clist->vclass_id}}" {{$select}}>{{$clist->vclass_name}}</option>				
											<?php
											}
										}
										?>
									</select></td>
														</tr>
														<tr>
															<td>Max Pax</td>
															<td><input type="number" class="form-control" name="vehicle_pax" id="vehicle_pax" value="@if($id>0){{$vehicle_detail[0]->vehicle_pax}}@endif" required="required" number="number" min="0" ></td>
														</tr>
														<tr>
															<td>Max Large Bags</td>
															<td><input type="number" class="form-control" name="vehicle_largebag" id="vehicle_largebag" value="@if($id>0){{$vehicle_detail[0]->vehicle_largebag}}@endif" required="required" min="0"></td>
														</tr>
														<tr>
															<td>Max Small Bags</td>
															<td><input type="number" class="form-control" name="vehicle_smallbag" id="vehicle_smallbag" value="@if($id>0){{$vehicle_detail[0]->vehicle_smallbag}}@endif" required="required" min="0"></td>
														</tr>
														<tr>
															<td>Minimum Rates $</td>
															<td><input type="number" class="form-control" name="vehicle_minrate" id="vehicle_minrate" value="@if($id>0){{$vehicle_detail[0]->vehicle_minrate}}@endif" required="required" min="0"></td>
														</tr>
													</table>
												</td>
											</tr>
											</table>	
											</td>	
										</tr>
										<tr>
											<td  style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>A/C</td>
														<td>
															<select name="vehicle_ac" id="vehicle_ac" class="form-control">
																<option value="1" @if(($id>0)&& ($vehicle_detail[0]->vehicle_ac=='1')) selected="selected" @endif>Yes</option>
																<option value="0" @if(($id>0)&& ($vehicle_detail[0]->vehicle_ac=='0')) selected="selected" @endif>No</option>
															</select>
														</td>
														<td>Music System</td>
														<td>
															<select name="vehicle_music" id="vehicle_music" class="form-control">
																<option value="1" @if(($id>0)&& ($vehicle_detail[0]->vehicle_music=='1')) selected="selected" @endif>Yes</option>
																<option value="0" @if(($id>0)&& ($vehicle_detail[0]->vehicle_music=='0')) selected="selected" @endif>No</option>
															</select>
														</td>
														<td>Video System</td>
														<td>
															<select name="vehicle_video" id="vehicle_video" class="form-control">
																<option value="1" @if(($id>0)&& ($vehicle_detail[0]->vehicle_video=='1')) selected="selected" @endif>Yes</option>
																<option value="0" @if(($id>0)&& ($vehicle_detail[0]->vehicle_video=='0')) selected="selected" @endif>No</option>
															</select>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Description</td>
														<td>
															<textarea name="vehcile_desc" id="vehcile_desc" class="form-control">@if($id>0){{$vehicle_detail[0]->vehcile_desc}}@endif</textarea>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										
										<tr>
											<td style="border:1px solid #000000;">
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
														<td>Driver first Name</td>
														<td><input type="text" class="form-control" name="vehicle_drvierfname" id="vehicle_drvierfname" value="@if($id>0){{$vehicle_detail[0]->vehicle_drvierfname}}@endif" required="required">
														</td>
														</tr>
													<tr>
														<td>Driver Last Name</td>
														<td><input type="text"  id="vehicle_driverlanme" class="form-control" name="vehicle_driverlanme" value="@if($id>0){{$vehicle_detail[0]->vehicle_driverlanme}}@endif" required="required">
														</td>
													</tr>
													<tr>
														<td>Driver Mobile No</td>
														<td><input type="text" class="form-control" name="vehicle_mobno" id="vehicle_mobno" value="@if($id>0){{$vehicle_detail[0]->vehicle_mobno}}@endif" required="required" maxlength="10">
														</td>
													</tr>
													<tr>
														<td>Driver Overnight Allowance $</td>
														<td><input type="text" class="form-control" name="vehicle_allownce" id="vehicle_allownce" value="@if($id>0){{$vehicle_detail[0]->vehicle_allownce}}@endif" required="required" number="number">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										<tr>
											<td>
												<table width="100%" cellpadding="2" cellspacing="2">
													<tr>
													<td>Status</td>
													<td> 
													<select name="vehicle_status" id="vehicle_status" class="form-control">
													<option value="1" @if(($id>0)&& ($vehicle_detail[0]->vehicle_status=='1')) selected="selected" @endif>Active</option>
													<option value="0" @if(($id>0)&& ($vehicle_detail[0]->vehicle_status=='0')) selected="selected" @endif>Inactive</option>
											</select></td>
													</tr>
												</table>
											</td>	
										</tr>
										
										
									</table>
									
                                        
                                        
                                       
                                        
										
                                        
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									
									@if($id>0)
									<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@else
									<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
									<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
									<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@endif
									
										
									 	
                                    </div>
									
               	 <input type="hidden" name="del_file" id="del_file" value="" />
               	 <input type="hidden" name="from" id="from" value="" />
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->







		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
		function check_frm(tpy)
		{
		
			
			 $("#from").val(tpy);	

			if(tpy=='back')
			{
				var	valid = true;
			}
			else
			{		
					
				var form = $("#rest_frm");
					form.validate();
				var valid =	form.valid();
			}
			
			
			if(valid)
			{		
				 $("#ajax_favorite_loddder").show();	
				 
				 var formData = new FormData($('#rest_frm')[0]);
				 
				var frm_val = 'from='+tpy+'&'+formData; //$('#rest_frm').serialize();				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/vehicle_action')}}",
           		 async: false,
				data: formData,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#ajax_div').html(msg);
					},
					cache: false,
					contentType: false,
					processData: false
				});
				return false;
			}
			else
			{
				return false;
			}		
		}

 $(document).on('change', '#vehicle_make', function(){ 	
 var make_id = 	 $(this).find('option:selected').val();
 
 
 		var frm_val = 'make_id='+make_id;				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/findmodel')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#model_selection').html(msg);
					}
				});
				
 });	
		</script>



<script>
var selDiv = "";
var storedFiles = [];
	
$(document).ready(function() {
		$("#other_images").on("change", handleFileSelect);		
		selDiv = $("#selectedFiles"); 		
		$("body").on("click", ".selFile", removeFile);
});

		
	function handleFileSelect(e) {
		var files = e.target.files;
		var filesArr = Array.prototype.slice.call(files);
		filesArr.forEach(function(f) {			

			if(!f.type.match("image.*")) {
				return;
			}
			storedFiles.push(f);
			
			var reader = new FileReader();
			reader.onload = function (e) {
				//var html = "<div class='col-md-3'><div class='tab_five_img'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='selFile' title='Click to remove'><span style='cursor:pointer;' data-file='"+f.name+"' class='selFile'>X</span></div></div><span style='cursor:pointer;' data-file='"+f.name+"' class='selFile'>X</span><br clear=\"left\"/>";
				
				
				
				var html = "<div style='height:100px; width:100px; float:left; margin: 10px;'><img src=\"" + e.target.result + "\" data-file='"+f.name+"' class='selFile' height='100px' width='100px' title='Click to remove'><span style='cursor:pointer;' data-file='"+f.name+"' class='selFile'>X</span></div>";
				selDiv.append(html);
				
			}
			reader.readAsDataURL(f); 
		});
	}
		
	function handleForm(e) {
		e.preventDefault();
		var data = new FormData();
		
		for(var i=0, len=storedFiles.length; i<len; i++) {
			data.append('files', storedFiles[i]);	
		}
		
		var xhr = new XMLHttpRequest();
		xhr.open('POST', 'uploads.php', true);
		
		xhr.onload = function(e) {
			if(this.status == 200) {
				console.log(e.currentTarget.responseText);	
				alert(e.currentTarget.responseText + ' items uploaded.');
			}
		}
		
		xhr.send(data);
	}
		
	function removeFile(e) {
		//var file = $(this).data("file");
		var file = $(this).attr("data-file");
		
		
		//$('#del_file').val()=nfile;
		var nfile = document.getElementById("del_file").value;
		if(nfile!=''){nfile = nfile+','+file;}else{nfile = file;}
		
		document.getElementById("del_file").value=nfile;
		for(var i=0;i<storedFiles.length;i++) {
			if(storedFiles[i].name === file) {
				storedFiles.splice(i,1);
				break;
			}
		}
		$(this).parent().remove();
	}
	
	function delete_image(pid,vimageid)
	{
		//alert('test');
		//alert(pid);
		//alert(vimageid);
			
				var r 				= confirm('Are you sure you want to delete?');
				if( r==true ){
				 $("#ajax_favorite_loddder").show();
					var vId 		= pid //$(this).attr('pid');
					var vimageid 	= vimageid;
					
					var url 		= "{{url('/admin/update_vehicle_images')}}";
					var formdata 	= 'status=del_images&vid='+vId+'&vimageid='+vimageid; 
					$.post( url, formdata, function( data ){
					 $("#ajax_favorite_loddder").hide();
						$('#all_images').html(data);
					});
				}
	}
</script>