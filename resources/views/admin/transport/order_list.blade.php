<table id="example2" class="table table-bordered table-hover">
<thead>
	<tr>
		<th>SrNo</th>
		<th>Order_no</th>
		<th>OrderReq_date</th>
		<th>Transport</th>
		<th>Vehicle</th>
		<th>Status</th>
		<th>Cust_Submit_date</th>
		<!--<th>Last_Staus_Date</th>-->
		<th>Action</th>
	</tr>
</thead>
<tbody>										
<?php $i=1; ?>
@if(!empty($order_detail)) 
 @foreach ($order_detail as $list)
	<tr>
		<td>{{ $i }}</td>
		<td>{{ $list->order_id}}</td>
		<td>{{ $list->order_create}}</td>
		<td><a href="{{url('admin/transport_view/')}}/{{$list->transport_id}}">{{ $list->transport_name}}</a></td>
		<td>{{ $list->vehicle_rego}}</td>
		<td>
		
@if($list->order_status=='1') Submit From Website @endif
@if($list->order_status=='2') Cancelled @endif
@if($list->order_status=='3') Sent to Partner @endif
@if($list->order_status=='4') Partner Confirmed @endif
@if($list->order_status=='5') Partner Completed @endif
@if($list->order_status=='6') Reject @endif
@if($list->order_status=='7') Review Completed @endif
		
		</td>
		<td>{{ $list->order_create }}</td>
<!--<td><?php echo date('Y-m-d',strtotime($list->created_at))?></td>-->
		 <td> 
		  <a title="Update" href="{{url('admin/transport_order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
		 
		 
		 <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>
		 
			 
		</td>
	</tr> 
<div class="modal fade" id="order-{{$list->order_id}}" role="dialog">
<div class="modal-dialog"> 

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title choice">Order Details</h4>
</div>
<div class="modal-body popup-ctn">

<h5><strong>Transport   summary</strong></h5>
<div>
<div>Name: {{$list->transport_name}}</div>
<div>Address: {{$list->transport_address}} {{$list->transport_suburb}} {{$list->transport_state}} {{$list->transport_pcode}}</div>
<div>Contact No: {{$list->transport_contact}}</div>

<div>Vehicle Name : {{$list->vehicle_rego}}</div>
</div>


<h5><strong>User summary</strong></h5>

<div>
<div>Name: {{$list->order_fname}} {{$list->order_lname}}</div>
<div>Address: {{$list->order_address}} {{$list->order_city}} {{$list->order_pcode}}</div>
<div>Contact No: {{$list->order_tel}}</div>
<div>Email: {{$list->order_email}}</div>
</div>	
<h5><strong>Your order summary</strong></h5>
<div>	
<?php $cart_price = 0;?>
@if(isset($list->order_carditem) && (count($list->order_carditem)))
<?php	$order_carditem = json_decode($list->order_carditem, true);?>	

<div>


@foreach($order_carditem as $item)
<?php  $cart_price = $cart_price+$item['price'];?>
<div>
<strong>{{$item['qty']}}  x</strong> {{$item['name']}} 

<strong class="pull-right">${{$item['price']}}</strong>
</div>

@if(!empty($item['options']['addon_data'])&&(count($item['options']['addon_data'])>0))
@foreach($item['options']['addon_data'] as $addon)

<?php $cart_price = $cart_price+$addon['price'];?>
<div>
{{$addon['name']}}
<strong class="pull-right">@if($addon['price']>0)${{$addon['price']}}@endif</strong>
</div>
@endforeach
@endif 

@endforeach


<div>
TotalAmount:
<strong class="pull-right">${{$cart_price}}</strong>
</div>
</div>
@endif
</div>
</div>
</div>
</div>
</div>										
<?php $i++; ?>
@endforeach	   
@endif                                     
</tbody>
</table>	
													