<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Logo</th>
			<th>Transport Name</th>
			<th>Vehicle Rego</th>
			<th>Vendor Name</th>
			<th>Staus</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	@if(!empty($trans_list)) 
	 @foreach ($trans_list as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>
		@if(!empty($list->transport_logo))	
	<img src="{{ url('/') }}/uploads/transport/{{ $list->transport_logo }}" width="50px;" height="50px;">
		@else
			<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >
		@endif				
		</td>
			<td>{{ $list->transport_name }}</td>
			<td>{{ $list->vehicle_rego }}</td>
			<td>{{ $list->name }}</td>
			<td>
			@if($list->transport_status=='PUBLISHED') 
			<span class="label label-primary">PUBLISHED</span>
			 @elseif($list->transport_status=='ACTIVE') 
			
			<span class="label label-danger">{{$list->rest_status}}</span>
			 @elseif($list->transport_status=='INACTIVE') 
			
			<span class="label label-danger">{{$list->rest_status}}</span>
			@endif
			</td>
			 <td>
			 <a title="Edit" href="{{url('admin/transport-update/')}}/{{ $list->transport_id }}"><i class="fa fa-edit"></i></a>  
			 <a title="View" href="{{url('admin/transport_view/')}}/{{ $list->transport_id }}"><i class="fa fa-eye"></i></a>
			 
				 
			</td>
		</tr> 										
	<?php $i++; ?>
	@endforeach	   
	@endif                                     
	</tbody>
	<tfoot>
		<tr>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>

 <script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>