
	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Service Area Form->{{$transport_detail[0]->transport_name}}</h3>
                                </div><!-- /.box-header -->
							
                                <!-- form start -->
								
								<form  role="form" method="POST" id="service_frm" action="#" enctype="multipart/form-data">    
								<input type="hidden" name="service_id" value="{{$id}}" />
								<input type="hidden" name="service_restid" value="{{$transport_id}}" />
							
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Suburb</label>
                                           <input type="text" class="form-control ui-autocomplete-input" name="service_suburb" id="service_suburb" value="@if($id>0){{$service_detail[0]->tport_service_suburb}}@endif" required="required"  autocomplete="off">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">PostCode</label>
											<input type="text" name="service_postcode" id="service_postcode" value="@if($id>0){{$service_detail[0]->tport_service_postcode}}@endif" class="form-control" required="required" number="number">												
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Delivery Charge</label>
											<input type="text" class="form-control" name="service_charge" id="	service_charge" value="@if($id>0){{$service_detail[0]->tport_service_charge}}@endif" required="required" number="number">												
                                        </div>
										<div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
												
										<select name="service_status" id="service_status"  class="form-control">
										 <option value="1" @if(($id>0) && ($service_detail[0]->tport_service_status==1)) selected="selected"@endif>Active </option>
										 <option value="0" @if(($id>0) && ($service_detail[0]->tport_service_status==0)) selected="selected"@endif>Inactive </option>
										</select>		
												
                                        </div>
                                        
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
									@if($id>0)
					<input type="button" class="btn btn-primary"  value="Update" onclick="check_frm('update')" />
					<input type="button"  class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									 
									@else
				<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
				<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
				<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
									@endif
										
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->






<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.ui-widget-content {
    max-height: 221px;
    overflow-y: scroll;
}
.ui-menu .ui-menu-item {
    padding: 5px;
}
.ui-menu-item:nth-child(2n) {
    background-color: #f1f1f1;
}
</style>

		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>

$(document).on('keyup', '#service_suburb', function(){
	 $( "#service_suburb" ).autocomplete({
    	 source: "{{url('/admin/get_suburblist') }}",
		 select: function(event, ui) {
		 
		 	$("#service_suburb").val(ui.item.value);	
			$("#service_postcode").val(ui.item.pincode);	
		 }
    });

});
		function check_frm(tpy)
		{
			
			if(tpy=='back')
			{
				var	valid = true;
			}
			else
			{		
				var form = $("#service_frm");
				form.validate();
				var valid =	form.valid();
			}
			
			
			if(valid)
			{		
				 $("#ajax_favorite_loddder").show();	
				var frm_val = 'from='+tpy+'&'+$('#service_frm').serialize();				
				$.ajax({
				type: "POST",
				url: "{{url('/admin/transport_service_action')}}",
				data: frm_val,
					success: function(msg) {
					 $("#ajax_favorite_loddder").hide();	
					
						$('#Servicearea').html(msg);
					}
				});
			}
			else
			{
				return false;
			}		
		}
		</script>
