<div class="col-sm-12">
	<p style="float: left;text-align: center;width: 100%;">				
	@if(Session::has('menu_message'))
		{{Session::get('menu_message')}}
	@endif 
	</p>
	<div class="divition">
		<div class="col-sm-4 border">
			<h4>Vehicle List</h4>
			<div class="drage" id="sortable" data-rest="{{$trans_id}}">
			
																			
			@if(!empty($vehicle_list))
			  @foreach($vehicle_list as $list)
				<div class="sub-drage @if($list->vehicle_id==$vehicle_id)selected @endif " id="listItem_{{$list->vehicle_id}}">
				<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$trans_id}}" data-menu="{{$list->vehicle_id}}" id="update_vehicle-{{$list->vehicle_id}}"><i class="fa fa-edit"></i></a>
				
					<p><a href="javascript:void(0)" id="menu_cat_list-{{$list->vehicle_id}}" data-menu="{{$list->vehicle_id}}"  data-rest="{{$trans_id}}">{{$list->vehicle_rego}}</a></p>
				</div>
			  @endforeach
			@endif
				
			</div>	
			<form id="res_btnfrm" method="post">
			<input type="hidden" name="transport_id" value="{{$trans_id}}" />
			{!! csrf_field() !!}
			<a class="btn btn-primary" href="javascript:void(0)" id="add_vehicle" data-restid="{{$trans_id}}">Add Vehicle</a> 	
			</form>											
		</div>
		<div class="col-sm-8">
			<div class="divtion-data" id="show_allview">
				<div class="containt-box">	
						<!-- Main content -->
						<section class="content">
						<div class="col-xd-12">
												<!-- general form elements -->
												<div class="box box-primary">
													<div class="box-header">
														<h3 class="box-title">Vehicle Rates Form -> {{$vehicle_detail[0]->vehicle_rego}}</h3>
													</div><!-- /.box-header -->
													<!-- form start -->
													
													<form  role="form" method="POST" id="rest_cate_frm" action="{{ url('/admin/vehicle_charge_action') }}" enctype="multipart/form-data">    
													<input type="hidden" name="rate_id" value="{{$rate_id}}" />
													<input type="hidden" name="rate_vehicleid" value="{{$vehicle_id}}" />
													<input type="hidden" name="rate_transid" value="{{$trans_id}}" />
													{!! csrf_field() !!}
														<div class="box-body">										
															<div class="form-group">
																<label for="exampleInputEmail1">Name</label>
															  <select name="rate_name" id="rate_name">
																	<option value="Single_day_Hire">Single-day Hire</option>			
																	<option value="Multi_day_Hire">Multi-day Hire</option>										
																</select>
															</div>
															
															<div class="form-group">
																<label for="exampleInputEmail1">Description</label>
																<textarea  class="form-control" name="rate_desc" id="rate_desc" required="required"></textarea>																
															</div>
															
															
															<div class="form-group">
																<label for="exampleInputEmail1">Available in diffrent ways</label>	
																<select name="rate_diff" id="diff_size">
																	<option value="no">No</option>			
																	<option value="yes">Yes</option>										
																</select>
															</div>
															
															
															<div id="diff_size_div" style="display:none" >
																 <div class="form-group">
																	<table width="100%" cellpadding="2" cellspacing="2" id="portion_size_table"> 
																		<tr>
																			<th>Name</th>
																			<th>Display Order</th>
																			<th>Price</th>
																			<th>Status</th>
																		</tr>
																		<tr><td><input type="text" name="rate_lname" class="form-control" value="" required="required"/></td>
																			<td>1</td>
																			<td><input type="text" name="rate_lprice" class="form-control"  value="" required="required"/></td>
																			<td>
																			<select class="form-control" name="rate_lstatus">
																				<option value="1">Active</option>
																				<option value="0">Inactive</option>
																			</select>
																			</td>
																		</tr>
																		<tr><td><input type="text" name="rate_mname" class="form-control" value=""/></td>
																			<td>2</td>
																			<td><input type="text" name="rate_mprice" class="form-control" value=""/></td>
																			<td>
																			<select class="form-control" name="rate_mstatus">
																				<option value="1">Active</option>
																				<option value="0">Inactive</option>
																			</select>
																			</td>
																		</tr>
																		<tr><td><input type="text" name="rate_sname" class="form-control" value=""/></td>
																			<td>3</td>
																			<td><input type="text" name="rate_sprice" class="form-control" value=""/></td>
																			<td>
																			<select class="form-control" name="rate_sstatus">
																				<option value="1">Active</option>
																				<option value="0">Inactive</option>
																			</select>
																			</td>
																		</tr>
																	</table>
																 </div>
															</div>
															
															
															
															<div class="form-group" id="main_price_div">
																<label for="exampleInputEmail1">Item Price</label>
															   <input type="text" name="rate_price" id="rate_price" class="form-control" value="" required="required" number="number">
															</div>
															
															
															
															<div class="form-group">
																<label for="exampleInputEmail1">Status</label>
																													
																<select name="rate_status" id="rate_status" class="form-control">
																<option value="1">Active</option>
																<option value="0">Inactive</option>
																</select>															
															</div>
															
														   
														  
															
														</div><!-- /.box-body -->
					
														<div class="box-footer">
														<input type="button" class="btn btn-primary"  value="Submit" onclick="check_frm('submit')" />
														<input type="button"   class="btn btn-primary"  value="Add Next" onclick="check_frm('addnext')" />
														<input type="button"   class="btn btn-primary"  value="Back" onclick="check_frm('back')" />
														 
															
														</div>
													</form>
													
												</div><!-- /.box -->
					
					
											</div>	
						</section><!-- /.content -->
																
			</div>
		</div>
	</div>
</div>
<script>

$( function() {
    $( "#sortable" ).sortable({
		
  	update:  function (event, ui) {
	        var sort_data = $("#sortable").sortable("serialize");
	        var trans_id = $("#sortable").attr("data-rest");
			
			var data = 'trans_id='+trans_id+'&'+sort_data;
			
				$("#ajax_favorite_loddder").show();	
            $.ajax({
                data: data,
                type: 'POST',
                url: "{{url('/admin/update_vehicle_sortorder')}}",
				success: function(msg) {
				
				$("#ajax_favorite_loddder").hide();	
				 //alert(msg);
				}
            });
	}
	 
     // revert: true
    });
    $( "#draggable" ).draggable({
      connectToSortable: "#sortable",
      helper: "clone",
      revert: "invalid"
    });
    $( "ul, li" ).disableSelection();
} );

  $(document).on('change', '#diff_size', function(){ 
  	if($(this).find(":selected").val()=='yes'){
  		$('#main_price_div').hide();
  		$('#diff_size_div').show();
	}
	else if($(this).find(":selected").val()=='no')
	{
  		$('#diff_size_div').hide();
  		$('#main_price_div').show();
		
	}
  
 });
 

 
function check_frm(tpy)
{

	if(tpy=='back')
	{
		var	valid = true;
	}
	else
	{		
			
		var form = $("#rest_cate_frm");
		form.validate();
		var valid =	form.valid();
	}
	
	
	if(valid)
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = 'from='+tpy+'&'+$('#rest_cate_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/vehicle_charge_action')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#ajax_div').html(msg);
			}
		});
	}
	else
	{
		return false;
	}		
}
</script>

