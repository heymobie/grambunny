@extends('layouts.admin')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Faq Form

			<small>Control panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Faq Form</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

	<div class="col-md-12">

                            <!-- general form elements -->

                            <div class="box box-primary">

                                <div class="box-header">

                                    <h3 class="box-title">Faq Form</h3>

                                </div><!-- /.box-header -->

								

					 @if(Session::has('error'))

					 <div class="alert alert-danger alert-dismissable">				

								{{Session::get('error')}}

								</div>

							@endif 

                                <!-- form start -->
								<form  role="form" method="POST" id="cuisine_frm" action="{{ url('/admin/faq_action') }}">    

								<input type="hidden" name="page_id" value="{{$id}} " /> 
								<input type="hidden" name="page_old_title" value="@if($id>0){{$page_detail[0]->title}}@endif" /> 
								{!! csrf_field() !!}

                                    <div class="box-body">
                                    	<div class="row">
											<div class="col-md-12">
												<div class="form-group">
		                                            <label for="exampleInputEmail1">Title</label>
		                                           <input type="text" class="form-control" id="page_title" name="page_title"  value="@if($id>0){{$page_detail[0]->title}}@endif" required="required">
		                                        </div>
											</div>
										
										</div>
                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Content</label>

											<textarea name="page_content" id="page_content" class="form-control textarea"> @if($id>0){{$page_detail[0]->content}}@endif</textarea>

                                        </div>
                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Status</label>

												
										<select name="page_status" id="page_status"  class="form-control">

										 <option value="1" @if(($id>0) && ($page_detail[0]->status==1)) selected="selected"@endif>Active </option>

										 <option value="0" @if(($id>0) && ($page_detail[0]->status==0)) selected="selected"@endif>Inactive </option>

										</select>		

                                        </div>


                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		

										<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />

									 	<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

                                    </div>

                                </form>

							
                            </div><!-- /.box -->

                        </div>


	</section><!-- /.content -->

</aside><!-- /.right-side -->

@endsection

@section('js_bottom')


<!-- jQuery 2.0.2 -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- jQuery UI 1.10.3 -->

        <!--<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>-->

        <!-- Bootstrap -->

        <!--<script src="{{ url('/') }}/public/design/admin/js/bootstrap.min.js" type="text/javascript"></script>-->		

        <!-- Bootstrap WYSIHTML5 -->

        <!--<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->

        <!-- AdminLTE App -->

        <!--<script src="{{ url('/') }}/public/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>-->

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

      <!--<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>-->

        <!-- CK Editor -->

        <!--<script src="{{ url('/') }}/public/design/admin/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>-->

		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script type="text/javascript">

$(function() {

	// Replace the <textarea id="editor1"> with a CKEditor

	// instance, using default configuration.

	CKEDITOR.replace('page_content');

	CKEDITOR.config.allowedContent = true; 

	CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);

	//bootstrap WYSIHTML5 - text editor

	$(".textarea").wysihtml5();

});

function check_email()

{

	var form = $("#cuisine_frm");

	form.validate();

	var valid =	form.valid();

	if(valid){			

					$(form).submit();

					return true;					

	}

	else

	{

		return false;

	}		

}

		</script>

@stop