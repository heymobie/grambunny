<style type="text/css">
  .methodbw{
    text-transform: capitalize;
  }
</style>
@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
    
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <link href="{{ url('/') }}/design/css/easy-responsive-tabs.css" rel="stylesheet">
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Notification List<small>Control Panel</small></h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Notification List</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="col-md-12">
      <div id="vendor_success_message" class="alert alert-success alert-dismissable" style="display: none;">
          <i class="fa fa-check"></i>
           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 Notification deleted sucessfully!
       </div>
       <div id="vendor_error_message" class="alert alert-danger alert-dismissable" style="display: none;">
          <i class="fa fa-check"></i>
           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 Something went wrong!
       </div>
      <!-- general form elements -->
      <div class="box box-primary">
        @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
              {{Session::get('message')}}
          </div>
        @endif
          <div class="col-xs-12">

          <div class="in_wal">
          <div id="horizontalTab">
          <ul class="resp-tabs-list">
            <li>User Notification</li>
            <li id="dmanNoti">Delivery Man Notification</li>
            <li id="vendorNoti">Vendor Notification</li>
          </ul>

          <div class="resp-tabs-container">

            <div>
              <div class="dwallet">
               
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">       
                        <div class="box">
                        <div class="box-body table-responsive">
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Title</th>
                                  <th>Description</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($user_notif_detail)) 
                               @foreach ($user_notif_detail as $list)
                               <?php $ufname  =  DB::table('users')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('name');

                                    $ulname  =  DB::table('users')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('lname'); 
                                                
                                  $username =  $ufname.' '.$ulname;

                                  $uemail  =  DB::table('users')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('email');                     
                                ?>
                                <tr id="{{$list->noti_id}}">
                                  <td>{{ $i }}</td>
                                  <td>{{ $username }}</td>
                                  <td>{{ $uemail }}</td>
                                  <td>{{ $list->noti_title }}</td>
                                  <td>{{ $list->noti_desc }}</td>
                                  <td>
                                    <input type="button" name="deleteUnotify" class="btn btn-primary" id="deleteUnotify" onclick="deleteUnotify({{$list->noti_id}})" value="Delete">
                                  </td>
                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

            <div>
              <div class="transactions_hist">
                
            <div>
            <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="payment_distribution_list">              
                             <table id="payment_distribution" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Title</th>
                                  <th>Description</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($delivery_notif_detail)) 
                               @foreach ($delivery_notif_detail as $list)
                               <?php $dfname  =  DB::table('deliveryman')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('name');

                                    $dlname  =  DB::table('deliveryman')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('lname'); 
                                                
                                  $deliverymanname =  $dfname.' '.$dlname;

                                  $deliverymanemail  =  DB::table('deliveryman')   
                                                ->where('id', '=', $list->noti_userid)
                                                ->value('email');                     
                                ?>
                                <tr id="{{$list->noti_id}}">
                                  <td>{{ $i }}</td>
                                  <td>{{ $deliverymanname }}</td>
                                  <td>{{ $deliverymanemail }}</td>
                                  <td>{{$list->noti_title}}</td>
                                  <td>{{ $list->noti_desc}}</td>
                                  <td>
                                    <input type="button" name="deleteDnotify" class="btn btn-primary" id="deleteDnotify" onclick="deleteDnotify({{$list->noti_id}})" value="Delete">
                                  </td>
                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

              </div>
            </div>
                        <div>
              <div class="transactions_hist">
                
            <div>
            <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="restaurant_list3">              
                             <table id="example3" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Title</th>
                                  <th>Description</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($vendor_notif_detail)) 
                               @foreach ($vendor_notif_detail as $list)
                                <?php $vfname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->noti_userid)
                                                ->value('name');

                                    $vlname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->noti_userid)
                                                ->value('last_name'); 
                                                
                                  $vendorname =  $vfname.' '.$vlname;

                                  $vendoremail  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->noti_userid)
                                                ->value('email');                     
                                ?>              
                                <tr id="{{$list->noti_id}}">
                                  <td>{{ $i }}</td>
                                  <td>{{ $vendorname }}</td>
                                  <td>{{ $vendoremail }}</td>
                                  <td>{{ $list->noti_title }}</td>
                                  <td>{{ $list->noti_desc }}</td>
                                  <td>
                                    <input type="button" name="deleteVnotify" class="btn btn-primary" id="deleteVnotify" onclick="deleteVnotify({{$list->noti_id}})" value="Delete">
                                  </td>
                                </tr> 
                   
                              <?php $i++; ?>
                              @endforeach    
                              @endif
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
            </div>

              </div>
            </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
  z-index: 999;
}
.modal-backdrop {
  
  z-index: 998 !important;
}
</style>  
  
<div id="ajax_favorite_loddder" style="display:none;">
  <div align="center" style="vertical-align:middle;">
    <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
  </div>
</div>

<script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>    
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
  $(function() {
      $("#example1").dataTable();
      $('#example2').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
      });
      $('#example3').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
      });
      $('#payment_distribution').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
      });
      $('#vendor_payment_request').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
      });
  });
  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
     dateFormat: 'yy-mm-dd',
     autoclose: true,
    });
  });  
  $(document).on('click', '#search_btn', function(){     
    $('#error_msg').hide();
    var frmdate = $('#fromdate').val(); 
    var todate = $('#todate').val();   
  if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
  {//compare end <=, not >=
    //your code here
    //alert("From date will be big from to date!");
    
    $('#error_msg').html('From date will be big from to date!');
    $('#error_msg').show();
  }
  else
  { 
    $("#ajax_favorite_loddder").show(); 
    var frm_val = $('#search_frm').serialize();       
    $.ajax({
    type: "POST",
    url: "{{url('/admin/payment_search_list')}}",
    data: frm_val,
    success: function(msg) {
       $("#ajax_favorite_loddder").hide();  
      //alert(msg)
        $('#restaurant_list').html(msg);
      }
    });
  }   
});     
      

  
$(document).on('click', '#generate_file', function(){     



 $('#search_frm').attr('action', "{{url('/admin/payment_report')}}");
 
 $('#search_frm').attr('target', '_blank').submit();

    
});   

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

<script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});

function deleteVnotify(id){
  var delurl = "delete_vendor_notification/" + id;
  var vid = "#"+id;  
  $.ajax({
    type: "GET",
    url: delurl,
    success: function(response) {
      if(response == 1){
        $(vid).hide();
        $("#vendor_error_message").hide();
        $("#vendor_success_message").show();
      } else {
        $("#vendor_error_message").show();
        $("#vendor_success_message").hide();
      }        
    }
  });
}

function deleteDnotify(id){
  var delurl = "delete_deliveryman_notification/" + id;  
  var did = "#"+id;
  $.ajax({
    type: "GET",
    url: delurl,
    success: function(response) {
      if(response == 1){
        $(did).hide();
        $("#vendor_error_message").hide();
        $("#vendor_success_message").show();
      } else {
        $("#vendor_error_message").show();
        $("#vendor_success_message").hide();
      }        
    }
  });
}

function deleteUnotify(id){
  var delurl = "delete_user_notification/" + id;
  var uid = "#"+id;
  $.ajax({
    type: "GET",
    url: delurl,
    success: function(response) {
      if(response == 1){
        $(uid).hide();
        $("#vendor_error_message").hide();
        $("#vendor_success_message").show();
      } else {
        $("#vendor_error_message").show();
        $("#vendor_success_message").hide();
      }        
    }
  });
}

$(document).on('click', '#dmanNoti', function(){
  $.ajax({
    type: "GET",
    url: "{{url('/admin/read_dman_notification')}}",
    success: function(response) {
             
    }
  });
});

$(document).on('click', '#vendorNoti', function(){
  $.ajax({
    type: "GET",
    url: "{{url('/admin/read_vendor_notification')}}",
    success: function(response) {
             
    }
  });
});

</script> 

@stop
