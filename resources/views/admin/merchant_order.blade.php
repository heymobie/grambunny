@extends('layouts.admin')

@section("other_css")

<!-- DATA TABLES -->

<!-- <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" /> -->

<style type="text/css">
	#myInput {
		float: right;
		width: 30%;
		margin-bottom: 10px;
	}
</style>

@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Merchant Order Management

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Merchant Order Management</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

		<div class="row">

			<div class="col-xs-12">

				<div class="box">

					<div class="box-header">

						<h3 class="box-title">Merchant Order Management</h3>

					</div><!-- /.box-header -->

					<div class="box-body table-responsive">

						@if(Session::has('message'))

						<div class="alert alert-success alert-dismissable">

							<i class="fa fa-check"></i>{{Session::get('message')}}

						</div>

						@endif
					</div><!-- /.box-body -->
				</div><!-- /.box -->
				<div>

					<!-- <div class="col-md-5"> Products : &nbsp;   	
<select name="order_search" id="order_search" onchange="location = this.value;">
<option value="{{url('/admin/merchant-order/')}}"@if($searchid=='') selected @endif>All</option>		
<option value="{{url('/admin/merchant-order/0')}}"@if($searchid=='0') selected @endif>Products</option>	
<option value="{{url('/admin/merchant-order/1')}}"@if($searchid=='1') selected @endif>Services</option>
</select>
</div> -->
					</br></br>


					<!-- <input id="myInput" type="text" placeholder="Search.."> -->
					<br>

					<section>

						<div class="row">

							<div class="my-panel-data">

								<div class="col-xs-12">

									<div class="box">

										<div class="box-body table-responsive">

											<div id="vender_search_list">

												<table id="myTable11" class="table table-bordered table-hover">

													<thead>

														<tr>

															<th>Merchant ID#</th>

															<!-- <th>Merchant</th> -->

															<th>User Name</th>

															<th>Name</th>

															<th>Email</th>

															<th>Contact No.</th>

															<!--<th>Type</th>-->

															<th>Category</th>

															<th>Status</th>

															<th>Action</th>

														</tr>

													</thead>

													<tbody id="myTable">

														<?php $i = 1; ?>

														@foreach ($vendor_list as $list)

														<?php

														$rest_count = DB::table('restaurant')->where('vendor_id', '=', $list->vendor_id)->count();

														$category = DB::table('product_service_category')->where('id', '=', $list->category_id)->value('category');

														?>

														<tr>

															<td>{{$list->vendor_id}}</td>

															<!-- <td><?php if (!empty($list->profile_img1)) { ?> <img src="{{ url('/public/uploads/vendor/profile/'.$list->profile_img1) }}" height="70" width="70"> <?php } else { ?>

																	<img src="{{ asset('public/uploads/user.jpg') }}" width="70px;" height="70px;">

																<?php } ?>
															</td> -->

															<td>{{ $list->username}}</td>

															<td>{{ $list->name}} {{ $list->last_name}}</td>

															<td>{{ $list->email }}</td>

															<td>{{ $list->contact_no }}</td>

															<!--<td>@if($list->type=='0') Products @else Services @endif</td>-->

															<td>{{$category}}</td>

															<td>@if($list->vendor_status==1)

																<span class="label label-success">Active</span>

																@else

																<span class="label label-danger">Inactive</span>@endif
															</td>

															<td>

																<a title="View" href="{{url('admin/order-list/')}}/{{ $list->vendor_id }}"><span class="label label-primary">View Orders</span></a>

																<a title="View" href="{{url('admin/event-list/')}}/{{ $list->vendor_id }}"><span class="label label-primary">View Events</span></a>

																<!-- <a title="View" href="{{url('admin/order-list/')}}/{{ $list->vendor_id }}"><i class="fa fa-eye"></i></a> -->

															</td>

														</tr>

														<?php $i++; ?>

														@endforeach

													</tbody>

													<!-- <tfoot>

														<tr>

															<th>&nbsp;</th>

															<th>&nbsp;</th>

															<th>&nbsp;</th>

															<th>&nbsp;</th>

															<th>&nbsp;</th>

															<th>&nbsp;</th>

														</tr>

													</tfoot> -->

												</table>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="col-12 mt-5 text-center">
							<div class="custom-pagination">
								
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@stop

@section('js_bottom')

<style>
	#ajax_favorite_loddder {

		position: fixed;

		top: 0;

		left: 0;

		width: 100%;

		height: 100%;

		background: rgba(27, 26, 26, 0.48);

		z-index: 1001;

	}

	#ajax_favorite_loddder img {

		top: 50%;

		left: 46.5%;

		position: absolute;

	}



	.footer-wrapper {

		float: left;

		width: 100%;

		/*display: none;*/

	}

	#addons-modal.modal {

		z-index: 999;

	}

	.modal-backdrop {



		z-index: 998 !important;

	}
</style>



<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>



<!-- jQuery 2.0.2 -->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->

<!-- Bootstrap -->

<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

<!-- DATA TABES SCRIPT -->

<!-- <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script> -->

<!-- <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script> -->

<!-- AdminLTE App -->

<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>



<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});
</script>

<!-- page script -->

<script type="text/javascript">
	function check_frm()

	{





		$('#error_msg').hide();



		var form = $("#search_frm");

		form.validate();

		var valid = form.valid();

		if (($("#vendor_cont").val() != '') || ($("#vendor_email").val() != ''))

		{

			$("#ajax_favorite_loddder").show();

			var frm_val = $('#search_frm').serialize();

			$.ajax({

				type: "POST",

				url: "{{url('/admin/vendor_search')}}",

				data: frm_val,

				success: function(msg) {

					$("#ajax_favorite_loddder").hide();



					$('#vender_search_list').html(msg);

				}

			});

		} else

		{

			//alert('Please insert any one value');



			$('#error_msg').html('Please insert any one value');

			$('#error_msg').show();

			return false;

		}

	}
</script>

	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>




@stop