@extends('layouts.admin')
@section("other_css")
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
   .deepmd{ width: 100%; }
   .deepmd strong{ float: right; }
   .deepon{ font-size: 14px; } 
   .top-detail_invoice {
    float: left;
    width: 100%;
    text-align: center;
    border-bottom: 1px dashed #ddd;
    margin-bottom: 10px;
    text-transform: capitalize;
}

.top-detail_invoice h3 {
    text-transform: uppercase;
    font-weight: bold;
    color: #ec7324!important;
    font-size: 30px;
}
.prduct_detail table td p:nth-child(1n+5) {
    text-align: left;
    padding: 0px !important;
    margin: 0px;
}
.prduct_detail table td{
    text-align: left;
    /*padding: 1px !important;*/
    
}
.customer_detail {
    float: left;
    width: 100%;
    text-transform: capitalize;
   /* border-bottom: 1px dashed #ddd;*/
}

.top-detail_invoice p{
margin: 0px !important;
}
.customer_detail p{
 margin: 0px !important; 
}
.customer_detail strong {
    padding-top: 5px;
    padding-bottom: 0px;
    margin-top: 2px;
    float: left;
    width: 100%;
}
.prduct_detail label{
  font-weight: normal;
}
.billing td {
   /* border: 1px solid #ccc;*/
    border-collapse: collapse;
}
td, th {
    text-align: left;
    padding: 8px;
   /* width: 10%;*/
}
.billing th {
    border-bottom:  1px solid #ccc;
    border-collapse: collapse;
    text-align: left;
}
.billing table {
   /* border: 1px solid #ccc;*/
    border-collapse: collapse;
    margin-top: 10px;
}
label {
    display: inline-block;
    margin-bottom: 5px;
    font-weight: bold;
}

td.lcolrs {
    text-align-last: end;
}

td.lcolcen{ text-align: left; }

@media print {
    .printhide {
        display:none;
    }
 }

 @media print {
    #order_status {
        display:none;
    }
 }

</style>
@stop
@section('content')
<aside class="right-side">
   <?php if($order_detail->product_type!=3){ ?>
   <section class="content-header">
      <h1>Product Order Detail</h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li class="active">Product Order Detail</li>
      </ol>
   </section>
   <?php }else{?>
      <section class="content-header">
      <h1>Event Order Detail</h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li class="active">Event Order Detail</li>
      </ol>
   </section>

  <?php } ?>
   <div class="myOdrersDetailBox">
      <div class="row">
         <?php if($order_detail->product_type!=3){ ?>
         <?php if( !in_array($order_detail->status, array(5,6,7) )){ ?>
         <form id="supdate" role="form" method="POST" action="{{ url('/admin/status-update') }}" >
            {!! csrf_field() !!}
            <div class="col-md-12">
               <span class="printhide">Order Status :</span> &nbsp;   
               <input type="hidden" name="order_id" id="order_id" value="{{$order_detail->order_id}}">
               <input type="hidden" name="return_flag" id="return_flag" value="0">
               <input type="hidden" name="id" id="id" value="{{$order_detail->id}}">
               <select name="order_status" class="printhide" id="order_status">
               <option value="0" @if($order_detail->status == 0) selected @endif >pending</option>
               <option value="1" @if($order_detail->status == 1) selected @endif >Accept</option>
               <option value="2" @if($order_detail->status == 2) selected @endif >Cancelled</option>
               <option value="3" @if($order_detail->status == 3) selected @endif >On the way</option>
               <option value="4" @if($order_detail->status == 4) selected @endif >Complete</option>
               </select>
               <div id="cancel_area"><textarea style="width: 337px;" class="form-control animated"  id="cancel_reason" name="cancel_reason" placeholder="Order Notes" rows="3"></textarea></div>
               <span class="printhide">
               <input type="submit" class="btn"  value="Submit" />
               </span>

         </form>
         <div class="w-100 printhide">
         <button onclick="window.print()" style="color: #FFF;float: right;margin-top: 6px;margin-bottom: 6px;position: relative;right: -67px;padding: 5px 17px !important;background: #f20239;border: 1px solid #f20239;">Print</button>
     </div>
         </br></br>

         <?php }} ?>

          <?php //print_r($ps_list); die; ?> 
         
       <!--   <div class="col-md-4">
            <div class="myOrder">
               <?php foreach ($ps_list as $key => $value) {
                  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); ?>
               <div class="row">
                  <div class="col-md-6">
                     <div class="myOrderImg">
                        <?php if(!empty($pservice->image)){ ?>      
                        <img src="{{ url('/public/uploads/product/'.$pservice->image) }}">
                        <?php } ?>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="myOrderName deepon">
                        <?php if(!empty($pservice->name)){ ?>       
                        <strong>{{$pservice->name}}</strong>
                        <?php } ?>
                     </div>
                     </br>
                     <hr class="hrline">
                     <div class="deepmd">
                        <label>Price</label>
                        <?php if(!empty($pservice->price)){ ?>  
                        <strong>${{$pservice->price}}</strong>
                        <?php } ?>
                     </div>
                     </br>
                     <div class="deepmd">
                        <label>Quantity</label>
                        <strong>{{$value->ps_qty}}</strong>
                     </div>
                  </div>
               </div>
               <hr class="hrline">
               <?php } ?> 
            </div>
         </div> -->
         <div class="col-md-2">
         </div>
          @if($order_detail->product_type!=3)
<?php $vendor_info = DB::table('vendor')->where('vendor_id', '=' ,$order_detail->vendor_id)->first();?>
         <div class="col-md-8">
            <div class="myOrder">
               <div class="myOrderDtelBox">
                  <div class="top-detail_invoice">
                     <h3>{{$vendor_info->business_name}}</h3>
                      <p><strong style="font-size: 18px;">{{$vendor_info->mailing_address}},  {{$vendor_info->address}} <br> {{$vendor_info->city}}, {{$vendor_info->state}} {{$vendor_info->zipcode}}</strong></p>
                      <?php $order_date = date("m-d-Y H:i:s", strtotime($order_detail->created_at)); ?>

                    <!-- <p>{{$vendor_info->mob_no}}</p> -->

                     <p> Order Date/Time:
                        {{$order_date}}
                     </p>
                     
                    <p>Permit/License #: {{$vendor_info->permit_number}}</p>
                    <p>&nbsp;</p>
                     
                  </div>
                  <p style="text-align:center;font-weight:bold; padding:10px;font-size:24px;width:100%">ORDER DETAILS</p>
                    <p style="margin: 0; padding: 0px;"><b>Order ID: {{$order_detail->order_id}}</b></p>
                  <div class="col-md-6" style="padding: 9px;">
                  <div class="customer_detail">
                  
                    <strong>Customer</strong>
                    <p>{{$order_detail->user_id}}</p>
                     <p> {{$order_detail->first_name}} {{$order_detail->last_name}}</p>
                     <p>{{$order_detail->mobile_no}}</p>
                     <p>{{$order_detail->email}}</p>
                     <strong>Billing Address</strong>
                     <p>{{$order_detail->address}} {{$order_detail->city}} {{$order_detail->state}} {{$order_detail->zip}}</p>

                     <?php if(!empty($order_detail->addressd)){ ?>
                     <strong>Drop Off Address</strong> 
                     <p>{{$order_detail->addressd}}, {{$order_detail->cityd}}, {{$order_detail->stated}}, {{$order_detail->zipd}}</p>
                     <?php }else{?>

                     <strong>Pick Up Address</strong>
                     <p>{{$vendor_order->merchnt_pickup_address}}</p> 

                     <?php } ?>
                    
                  </div>
                  </div>

                  <div class="col-md-6">
                  <div class="customer_detail">
                    <strong>Merchant</strong>
                     <p>{{$vendor_info->vendor_id}}</p>
                     <?php if(!empty($vendor_order)){?>
                     <p> {{$vendor_order->merchant_name}} {{$vendor_order->merchant_last_name}}</p>
                     <p>{{$vendor_order->merchant_mob}}</p>
                     <p>{{$vendor_order->merchnt_email}}</p>
                 <?php }?>
                       <p><strong>Order Status</strong> 
                        <span class="green">
                        @if($order_detail->status==0) Pending @endif
                        @if($order_detail->status==1) Accept @endif
                        @if($order_detail->status==2) Cancelled @endif
                        @if($order_detail->status==3) On the way @endif
                        @if($order_detail->status==4) Complete @endif
                        @if($order_detail->status==5) Requested for return @endif
                        @if($order_detail->status==6) Return request accepted @endif
                        @if($order_detail->status==7) Return request declined @endif
                        </span>
                     </p>
                  </div>
                  </div>

                  <div class="prduct_detail billing">
                     <table>
                     <tr>        
                        <th colspan="5" style=" width: 40%;">Product Name</th>   
                        <th style="text-align: center;">Unit Price</th>
                        <th style="text-align: right;">Qty</th>
                        <th style="text-align: right; width: 20%;">Total</th>
                     </tr>

                     <?php $subtotal = 0; $total_pp = 0; ?>
                     
                  <?php foreach ($ps_list as $key => $value) {
                  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); 
                  $order_ps = DB::table('orders_ps')->where('order_id','=',$value->order_id)->first();

               //print_r($order_detail);die;
                  ?>
                    <tr>
                        <td class="lcolcen" colspan="5">
                           <p><?php if(!empty($value->name)){ ?>     
                              {{$value->name}}
                              
                              <?php } ?>
                           </p>
                           
                        </td>

                             

                        <td class="lcolcen" style="text-align: center;">
                           <p>
                            <?php //print_r($pservice);die; ?>
                          <?php if(!empty($value->price)){ ?>   
                          ${{$value->price}}
                       
                              <?php } ?>
                           </p>
                           <p>
                          <?php if(!empty($value->price)){ $total_pp =$value->price*$value->ps_qty;} ?>
                           </p>
                        </td>
                          <td class="lcolcen" style="text-align: right;">
                           <p>{{$value->ps_qty}}</p>
                        </td>

                          <td class="lcolrs" >
                           <p>${{ number_format($total_pp, 2) }}</p>
                        </td>

                     </tr>

                     <?php $subtotal = $subtotal+$total_pp; ?>

                      <?php } ?>


                      <tr>
                      
                      <td colspan="7" class="lcolrs"><b>Product Total</b></td>
                      <td colspan="1" class="lcolrs">${{ number_format($subtotal, 2) }}</td>
                         </tr>
                        <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Delivery Fee</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->delivery_fee, 2) }}</span></td>
                     </tr>

                     <tr>
                        
                         <td colspan="7" class="lcolrs"> <label>Discount</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>-${{ number_format($order_detail->promo_amount, 2) }}</span></td>
                     </tr>

                     <tr >
                         
                         <td colspan="7" class="lcolrs" style="border-top:0px solid #ddd; border-bottom: 0; "> <b>Subtotal</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:0px solid #ddd; border-bottom: 0;"><span>${{ number_format($order_detail->sub_total, 2) }}</span></td>
                     </tr>
                      
                     
                      <tr>
                         
                         <td colspan="7" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"> <label>Excise Tax of Subtotal ({{$vendor_order->excise_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"><span>${{ number_format($order_detail->excise_tax, 2)}}</span></td>
                     </tr>

                      <tr>
                        
                         <td colspan="7" class="lcolrs"> <label>City Tax of Subtotal ({{$vendor_order->city_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->city_tax, 2)}}</span></td>
                     </tr>

                     <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Sales Tax of Subtotal  ({{$vendor_order->sales_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>${{ number_format($order_detail->service_tax, 2)}}</span></td>
                     </tr>


                     <tr>
                         
                         <td colspan="7" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0; "> <b>Total Paid Amount</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0; "><span>${{ number_format($order_detail->total, 2) }}</span></td>
                     </tr>
                       <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Payment Type</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->payment_method}}</span></td>
                     </tr>
                     <tr>
                       
                         <td colspan="7" class="lcolrs"> <label>TransactionID</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->txn_id}}</span></td>
                     </tr>
                        <tr>
                        
                         <td colspan="7" class="lcolrs"> <label>Payment Status</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->pay_status}}</span></td>
                     </tr>
                       <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Comment</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->instruction}}</span></td>
                     </tr>
                
                    <?php $cc = DB::table('coupon_code')->where('id','=',$order_detail->coupon_id)->first(); ?>
                       <?php if(!empty($cc->coupon)){ ?>
                        <tr>
                           
                           <td colspan="7" class="lcolrs"> <label>Coupon Code </label>
                            </td>
                           <td colspan="1" class="lcolrs"><span><?php echo  @$cc->coupon; ?></span></td>
                        </tr>
                      <?php } ?>

              <?php if( in_array($order_detail->status, array(5,6,7))) { ?>

                <tr>
                 
                 <td colspan="7" class="lcolrs"> <label>Return reason:</label>
                </td>
                 <td colspan="1" class="lcolrs"><span>{{$order_detail->return_reason}}</span></td>
               </tr>

               <?php } ?>


              <?php if(in_array($order_detail->status, array(6,7)) && !empty($order_detail->return_reason_merchant)){ ?>

               <tr>
               
               <td colspan="7" class="lcolrs"> <label>Return reason merchant:</label>
               </td>
               <td colspan="1" class="lcolrs"><span>{{$order_detail->return_reason_merchant}}</span></td>
               </tr>

               <?php } ?>

               <?php //if($vendor_order->commission_rates) { ?>

                <tr>
                
                 <td colspan="7" class="lcolrs"> <label>Commission rate (in %)</label>
                </td>
                 <td colspan="1" class="lcolrs"><span>{{$vendor_order->commission_rates}}</span></td>
               </tr>


                 <tr>
                
                 <td colspan="7" class="lcolrs"> <label>Commission amount</label>
                 </td>
                 <td colspan="1" class="lcolrs"><span><?php $com_amount = $order_detail->total*$vendor_order->commission_rates/100;
                     echo '$'.number_format($com_amount, 2); ?> </span></td>
                 </tr>


              <tr>
               
               <td colspan="7" class="lcolrs"> <label>Merchant amount</label>
               </td>
               <td colspan="1" class="lcolrs"><span><?php $merchant_amount = $order_detail->total - $com_amount; 
                     echo '$'.number_format($merchant_amount, 2); ?></span></td>
              </tr>

               <?php //} ?>


                     </table>
                  </div>
               </div>
               <!-- <div class="myOrderDtel">
                  <label>Subtotal</label>
                  <strong>${{ number_format($order_detail->sub_total, 2) }}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Discount</label>
                  <strong>${{ number_format($order_detail->promo_amount, 2) }}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Service Tax</label>
                  <strong>${{ number_format($order_detail->service_tax, 2)}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Total Paid Amount</label>
                  <strong>${{ number_format($order_detail->total, 2) }}</strong>
               </div> -->
               <!-- <div class="myOrderDtel">
                  <label>Payment Type</label>
                  <strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>TransactionID</label>
                  <strong>{{$order_detail->txn_id}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Payment Status</label>
                  <strong>{{$order_detail->pay_status}}</strong>
               </div> -->
               <!-- <div class="myOrderDtel">
                  <label>Comment</label>
                  <strong>{{$order_detail->instruction}}</strong>
               </div> -->

            
            </div>
         </div>
         @else
         <?php $vendor_info = DB::table('vendor')->where('vendor_id', '=' ,$order_detail->vendor_id)->first();?>
         <div class="col-md-8">
            <div class="myOrder">
               <div class="myOrderDtelBox">
                  <div class="top-detail_invoice">
                     <h3>{{$vendor_info->business_name}}</h3>
                      <p><strong style="font-size: 18px;">{{$vendor_info->mailing_address}},  {{$vendor_info->address}} <br> {{$vendor_info->city}}, {{$vendor_info->state}} {{$vendor_info->zipcode}}</strong></p>
                      <?php $order_date = date("m-d-Y H:i:s", strtotime($order_detail->created_at)); ?>

                    <!-- <p>{{$vendor_info->mob_no}}</p> -->

                     <p> Order Date/Time:
                        {{$order_date}}
                     </p>
                     
                    <p>Permit/License #: {{$vendor_info->permit_number}}</p>
                    <p>&nbsp;</p>
                     
                  </div>
                  <p style="text-align:center;font-weight:bold; padding:10px;font-size:24px;width:100%">ORDER DETAILS</p>
                    <p style="margin: 0; padding: 0px;"><b>Order ID: {{$order_detail->order_id}}</b></p>
                  <div class="col-md-6" style="padding: 9px;">
                  <div class="customer_detail">
                  
                    <strong>Customer</strong>
                    <p>{{$order_detail->user_id}}</p>
                     <p> {{$order_detail->first_name}} {{$order_detail->last_name}}</p>
                     <p>{{$order_detail->mobile_no}}</p>
                     <p>{{$order_detail->email}}</p>
                     <strong>Billing Address</strong>
                     <p>{{$order_detail->address}} {{$order_detail->city}} {{$order_detail->state}} {{$order_detail->zip}}</p>

                     <?php if(!empty($order_detail->addressd)){ ?>
                     <strong>Drop Off Address</strong> 
                     <p>{{$order_detail->addressd}}, {{$order_detail->cityd}}, {{$order_detail->stated}}, {{$order_detail->zipd}}</p>
                     <?php }else{?>

                     <strong>Pick Up Address</strong>
                     <p>{{$vendor_order->merchnt_pickup_address}}</p> 

                     <?php } ?>
                    
                  </div>
                  </div>

                  <div class="col-md-6">
                  <div class="customer_detail">
                    <strong>Merchant</strong>
                     <p>{{$vendor_info->vendor_id}}</p>
                     <?php if(!empty($vendor_order)){?>
                     <p> {{$vendor_order->merchant_name}} {{$vendor_order->merchant_last_name}}</p>
                     <p>{{$vendor_order->merchant_mob}}</p>
                     <p>{{$vendor_order->merchnt_email}}</p>
                 <?php }?>
                       <p><strong>Order Status</strong> 
                        <span class="green">
                        @if($order_detail->status==0) Pending @endif
                        @if($order_detail->status==1) Accept @endif
                        @if($order_detail->status==2) Cancelled @endif
                        @if($order_detail->status==3) On the way @endif
                        @if($order_detail->status==4) Complete @endif
                        @if($order_detail->status==5) Requested for return @endif
                        @if($order_detail->status==6) Return request accepted @endif
                        @if($order_detail->status==7) Return request declined @endif
                        </span>
                     </p>
                  </div>
                  </div>

                  <div class="prduct_detail billing">
                     <table>
                     <tr>        
                        <th colspan="5" style=" width: 40%;">Event Name</th>   
                        <th style="text-align: center;">Unit Price</th>
                        <th style="text-align: right;">Qty</th>
                        <th style="text-align: right; width: 20%;">Total</th>
                     </tr>

                     <?php $subtotal = 0; $total_pp = 0; ?>


                     
                  <?php foreach ($ps_list as $key => $value) {
                  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); 
                  $order_ps = DB::table('orders_ps')->where('order_id','=',$value->order_id)->first();

               //print_r($order_detail);die;
                  ?>
                    <tr>
                        <td class="lcolcen" colspan="5">
                           <p><?php if(!empty($value->name)){ ?>     
                              {{$value->name}}
                              
                              <?php } ?>
                           </p>
                           
                        </td>

                             

                        <td class="lcolcen" style="text-align: center;">
                           <p>
                            <?php //print_r($pservice);die; ?>
                          <?php if(!empty($value->price)){ ?>   
                          ${{$value->price}}
                       
                              <?php } ?>
                           </p>
                           <p>
                          <?php if(!empty($value->price)){ $total_pp =$value->price*$value->ps_qty;} ?>
                           </p>
                        </td>
                          <td class="lcolcen" style="text-align: right;">
                           <p>{{$value->ps_qty}}</p>
                        </td>

                          <td class="lcolrs" >
                           <p>${{ number_format($total_pp, 2) }}</p>
                        </td>

                     </tr>

                     <?php $subtotal = $subtotal+$total_pp; ?>

                      <?php } ?>


                      <tr>
                      
                      <td colspan="7" class="lcolrs"><b>Event Total</b></td>
                      <td colspan="1" class="lcolrs">${{ number_format($subtotal, 2) }}</td>
                         </tr>
                       <tr class="br-tn p-gb">

<td colspan="7" class="lcolrs">Ticket Fee :</td>
<td colspan="1" class="lcolrs">${{ number_format($order_detail->ticket_fee * $value->ps_qty, 2) }}</td>
</tr>

<tr class="br-tn p-gb">

<td colspan="7" class="lcolrs">Ticket Service Fee :</td>
<td colspan="1" class="lcolrs">${{ number_format($order_detail->ticket_service_fee * $value->ps_qty, 2) }}</td>
</tr>

                     <tr>
                        
                         <td colspan="7" class="lcolrs"> <label>Discount</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>-${{ number_format($order_detail->promo_amount, 2) }}</span></td>
                     </tr>

                     <tr >
                         
                         <td colspan="7" class="lcolrs" style="border-top:0px solid #ddd; border-bottom: 0; "> <b>Subtotal</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:0px solid #ddd; border-bottom: 0;"><span>${{ number_format($order_detail->sub_total, 2) }}</span></td>
                     </tr>

                     <tr>
                         
                         <td colspan="7" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"> <label>Sales Tax of Subtotal  ({{$vendor_order->sales_tax_percent}}%)</label>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0;"><span>${{ number_format($order_detail->service_tax, 2)}}</span></td>
                     </tr>
                      
                     <tr>
                         
                         <td colspan="7" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0; "> <b>Total Paid Amount</b>
                        </td>
                         <td colspan="1" class="lcolrs" style="border-top:1px solid #ddd; border-bottom: 0; "><span>${{ number_format($order_detail->total, 2) }}</span></td>
                     </tr>
                       <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Payment Type</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->payment_method}}</span></td>
                     </tr>
                     <tr>
                       
                         <td colspan="7" class="lcolrs"> <label>TransactionID</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->txn_id}}</span></td>
                     </tr>
                        <tr>
                        
                         <td colspan="7" class="lcolrs"> <label>Payment Status</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->pay_status}}</span></td>
                     </tr>
                       <tr>
                         
                         <td colspan="7" class="lcolrs"> <label>Comment</label>
                        </td>
                         <td colspan="1" class="lcolrs"><span>{{$order_detail->instruction}}</span></td>
                     </tr>
                
                    <?php $cc = DB::table('coupon_code')->where('id','=',$order_detail->coupon_id)->first(); ?>
                       <?php if(!empty($cc->coupon)){ ?>
                        <tr>
                           
                           <td colspan="7" class="lcolrs"> <label>Coupon Code </label>
                            </td>
                           <td colspan="1" class="lcolrs"><span><?php echo  @$cc->coupon; ?></span></td>
                        </tr>
                      <?php } ?>

              <?php if( in_array($order_detail->status, array(5,6,7))) { ?>

                <tr>
                 
                 <td colspan="7" class="lcolrs"> <label>Return reason:</label>
                </td>
                 <td colspan="1" class="lcolrs"><span>{{$order_detail->return_reason}}</span></td>
               </tr>

               <?php } ?>


              <?php if(in_array($order_detail->status, array(6,7)) && !empty($order_detail->return_reason_merchant)){ ?>

               <tr>
               
               <td colspan="7" class="lcolrs"> <label>Return reason merchant:</label>
               </td>
               <td colspan="1" class="lcolrs"><span>{{$order_detail->return_reason_merchant}}</span></td>
               </tr>

               <?php } ?>

               <?php //if($vendor_order->commission_rates) { ?>

                <tr>
                
                 <td colspan="7" class="lcolrs"> <label>Commission rate (in %)</label>
                </td>
                 <td colspan="1" class="lcolrs"><span>{{$vendor_order->commission_rates}}</span></td>
               </tr>


                 <tr>
                
                 <td colspan="7" class="lcolrs"> <label>Commission amount</label>
                 </td>
                 <td colspan="1" class="lcolrs"><span><?php $com_amount = $order_detail->total*$vendor_order->commission_rates/100;
                     echo '$'.number_format($com_amount, 2); ?> </span></td>
                 </tr>


              <tr>
               
               <td colspan="7" class="lcolrs"> <label>Merchant amount</label>
               </td>
               <td colspan="1" class="lcolrs"><span><?php $merchant_amount = $order_detail->total - $com_amount; 
                     echo '$'.number_format($merchant_amount, 2); ?></span></td>
              </tr>

               <?php //} ?>


                     </table>
                  </div>
               </div>
              
            </div>
         </div>
         @endif
      </div>
   </div>
   </div>
</aside>
@stop
@section('js_bottom')
<style>
   #ajax_favorite_loddder {
   position: fixed;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background:rgba(27, 26, 26, 0.48);
   z-index: 1001;
   }
   #ajax_favorite_loddder img {
   top:50%;
   left:46.5%;
   position:absolute;
   }
   .footer-wrapper {
   float:left;
   width:100%;
   }
   #addons-modal.modal {
   z-index:999;
   }
   .modal-backdrop {    
   z-index:998 !important;
   }
   table {
 
  border-collapse: collapse;
  width: 100%;
}

td, th {

  text-align: left;
  padding: 4px;
 /* width: 10%;*/
}

</style>

</style>
<div id="ajax_favorite_loddder" style="display:none;">
   <div align="center" style="vertical-align:middle;">
      <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>       
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>        
<script type="text/javascript">
   function check_frm(){
   
    $('#error_msg').hide();
   
    var form = $("#search_frm");
   
    form.validate();
   
    var valid = form.valid();
   
    if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
   
    {       
   
         $("#ajax_favorite_loddder").show();    
   
        var frm_val = $('#search_frm').serialize();             
   
        $.ajax({
   
        type: "POST",
   
        url: "{{url('/admin/vendor_search')}}",
   
        data: frm_val,
   
            success: function(msg) {
   
             $("#ajax_favorite_loddder").hide();    
   
            
   
                $('#vender_search_list').html(msg);
   
            }
   
        });
   
    }
   
    else
   
    {
   
        //alert('Please insert any one value');
   
        
   
        $('#error_msg').html('Please insert any one value');
   
        $('#error_msg').show();
   
        return false;
   
    }       
   
   }
   
   $(function() {
   
    $("#example1").dataTable();
   
    $('#example2').dataTable({
   
        "bPaginate": true,
   
        "bLengthChange": false,
   
        "bFilter": true,
   
        "bSort": true,
   
        "bInfo": true,
   
        "bAutoWidth": false
   
    });
   
   });
   
</script>
@stop