<style type="text/css">
  .methodbw{
    text-transform: capitalize;
  }
</style>
@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
    
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <link href="{{ url('/') }}/design/css/easy-responsive-tabs.css" rel="stylesheet">
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Wallet Account
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Wallet Account</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Received Wallet Account Balance : ${{$wallet_amount}} </h3>
                                </div><!-- /.box-header -->
                                <div class="box-header">
                                    <h3 class="box-title">Total Payed: ${{$payed_amount}} </h3>
                                </div><!-- /.box-header -->

                
                 @if(Session::has('message'))
           
                 <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                           {{Session::get('message')}}
                 </div>
                @endif
          
            <div class="col-xs-12">

          <div class="in_wal">
          <div id="horizontalTab">
          <ul class="resp-tabs-list">
            <li>Payment Orders Summary</li>
            <li>Payment Send In Vendor Wallet</li>
            <li>Payment Distribution Summary</li>
            <li>Payment Request By Vendor</li>
            </ul>

          <div class="resp-tabs-container">

            <div>
               <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Order Id</th>
                                  <th>Mode</th>
                                  <th>Restaurant Name</th>
                                  <th>Txn. No.</th>
                                  <th>Txn. Amount</th>
                                  <th>Status</th>
                                  <!--<th>Cust_Submit_date</th>-->
                                  <th>Last Status Date</th>

                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($payment_detail)) 
                               @foreach ($payment_detail as $list)
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ 
                                  $list->order_id}}
                                  ({{$list->order_uniqueid}})</td>
                                  <td>{{ $list->pay_method}}</td>
                                  <td>{{ $list->rest_name}}</td>
                                  <td>{{ $list->pay_tx}}</td>
                                  <td>{{ $list->pay_amt}}</td>
                                  <td>    
                   @if($list->pay_status=='1') Payment Done @endif
                   @if($list->pay_status=='2') Partial Done @endif
                   @if($list->pay_status=='3') Payment Not Done @endif
                   @if($list->pay_status=='4') Payment Refund @endif
                                  </td>
                                  <td>{{ $list->pay_update }}</td>
                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

             <div>
              <div class="dwallet">

 

          <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Payment Send In Vendor Wallet</h3>
      </div><!-- /.box-header -->
      <p style="float: left;text-align: center;width: 100%;">
         </p>
      <!-- form start -->
         <form name="tranvenwallet" id="tranvenwallet" method="post" action="{{ url('/') }}/admin/transfer-vendor">  

          <div class="box-body">
          <div class="form-group">

          <label for="exampleInputEmail1">Select Vendor : </label>  

          <select name="vendorid" id="vendorid" class="form-control" required="required">
  
          <?php foreach ($vendor_list as $key => $value) { ?>

          <option value="<?php echo $value->vendor_id ?>"><?php echo $value->name.''.$value->last_name ;?></option>
               
          <?php } ?>

           </select>

         </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Vendor Amount : </label>

          <input type="text" class="form-control" name="vamount" value="" required="required" placeholder="Amount">

        </div>

        <div class="form-group">
         <label for="exampleInputEmail1">Admin Commission : </label>

        <input type="text" class="form-control" pattern="\d*" maxlength="2" name="acommition" value="" required="required" placeholder="Commission in %"> 

        </div>

        <div class="box-footer">

          <input type="submit" class="btn btn-primary" value="Submit">

          <input type="button" class="btn btn-primary" value="Go Back" onclick="history.go(-1);">
        </div>

      </div>

          </form>
    </div>

            </div>
          </div>

 
            <div>
              <div class="transactions_hist">
                
            <div>
            <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="payment_distribution_list">              
                             <table id="payment_distribution" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Vendor Name</th>
                                  <th>Vendor Amount</th>
                                  <th>Admin Commission(%)</th>
                                  <th>Commission Amount</th>
                                  <th>Total Pay Amount</th>
                                  <th>Payment Method</th>
                                  <th>Date</th>

                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($vendor_payment_detail)) 
                               @foreach ($vendor_payment_detail as $list)

                                              
                              <?php $vname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->vendor_id)
                                                ->value('name');

                                    $vlname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->vendor_id)
                                                ->value('last_name');   
                                                
                                  $vendorname =  $vname.' '.$vlname;                      


                                                 ?>
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{$vendorname}}</td>
                                  <td>${{$list->vendor_amount}}</td>
                                  <td>{{ $list->admin_commission_per}}</td>
                                  <td>${{ $list->commission_amount}}</td>
                                  <td>${{ $list->total_pay_amount}}</td>
                                  <td class="methodbw">{{ $list->txn_method}}</td>
                                  <td>{{ $list->created_at}}</td>

                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

              </div>
            </div>



                        <div>
              <div class="transactions_hist">
                
            <div>
            <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="">              
                             <table id="" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Vendor Name</th>
                                  <th>Account Holder</th>
                                  <th>Branch</th>
                                  <th>Account Number</th>
                                  <th>IFSC</th>
                                  <th>Request Amount</th>
                                  <th>Change Status</th>

                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($vendor_request_detail)) 
                               @foreach ($vendor_request_detail as $list)
                                              
                              <?php $vname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->vendor_id)
                                                ->value('name');

                                    $vlname  =  DB::table('vendor')   
                                                ->where('vendor_id', '=', $list->vendor_id)
                                                ->value('last_name');   
                                                
                                  $vendorname =  $vname.' '.$vlname;                      


                                                 ?>
                                <form name="requestAmountForm" id="requestAmountForm" method="post" action="{{ url('/') }}/admin/request-amount-form">                                  
                                
                                <tr>
                                  <td>{{ $i }}
                                    <input type="hidden" name="vendorrequestid" value="{{$list->id}}">
                                    <input type="hidden" name="vendorwalletamount" value="{{$list->wallet_amount}}">
                                    <input type="hidden" name="vendorvendorid" value="{{$list->vendor_id}}">
                                  </td>
                                  <td>{{ $vendorname }}</td>
                                  <td>{{ $list->holder_name }}</td>
                                  <td>{{ $list->branch }}</td>

                                  <td>{{ $list->account_number }}</td>
                                  <td>{{ $list->ifsc }}</td>
                                  <td>${{ $list->vendor_amount }}<input type="hidden" name="request_amount" value="{{$list->vendor_amount}}"></td>
                                  <td>
                                    <select name="chnageStatus" id="chnageStatus" class="form-control">
                                      <option value="0" @if($list->transfer_status==0) selected="selected" @endif>Pending</option>
                                      <option value="1" @if($list->transfer_status==1) selected="selected" @endif>Reject</option>
                                      <option value="2" @if($list->transfer_status==2) selected="selected" @endif>Completed</option>
                                    </select>
                                    <input type="submit" name="updateStaus" class="btn btn-primary" value="Update">
                                  </td>

                                </tr> 

                                </form>
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

              </div>
            </div>


           </div>


                      </div>
                      </div>
                    </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
  z-index: 999;
}
.modal-backdrop {
  
  z-index: 998 !important;
}
</style>  
  
<div id="ajax_favorite_loddder" style="display:none;">
  <div align="center" style="vertical-align:middle;">
    <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
  </div>
</div>

        <script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
    
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
                $('#payment_distribution').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
                $('#vendor_payment_request').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
     dateFormat: 'yy-mm-dd',
     autoclose: true,
    });
  }); 
  
  
  
$(document).on('click', '#search_btn', function(){ 
  
   
    
   $('#error_msg').hide();
var frmdate = $('#fromdate').val(); 
var todate = $('#todate').val(); 
  
  if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
  {//compare end <=, not >=
    //your code here
    //alert("From date will be big from to date!");
    
    $('#error_msg').html('From date will be big from to date!');
    $('#error_msg').show();
  }
  else
  { 
    $("#ajax_favorite_loddder").show(); 
    var frm_val = $('#search_frm').serialize();       
    $.ajax({
    type: "POST",
    url: "{{url('/admin/payment_search_list')}}",
    data: frm_val,
    success: function(msg) {
       $("#ajax_favorite_loddder").hide();  
      //alert(msg)
        $('#restaurant_list').html(msg);
      }
    });
  }   
});     
      

  
$(document).on('click', '#generate_file', function(){     



 $('#search_frm').attr('action', "{{url('/admin/payment_report')}}");
 
 $('#search_frm').attr('target', '_blank').submit();

    
});   

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>

<script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script> 

@stop
