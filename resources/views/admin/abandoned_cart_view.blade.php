@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->

<style type="text/css">
  
  .product_qty{
    border: none;
    width: 30%;
}	

  </style>        
		
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                         Abandoned Cart Products
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Abandoned Cart Product List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
							<div>
							<section>
							 	<div class="row">
									<div class="my-panel-data">
										 <div class="col-xs-12">								 
										 
										    <div class="box">
												<div class="box-body table-responsive">
												
					
						<div id="restaurant_list">				
						<table id="myTable11" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>Image</th>
			<th>Product Name</th>
			<th>UPC Code</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total</th>
			<!-- <th>Action</th>  -->
		</tr>
	</thead>
	<tbody>	

	<?php if(!empty($items)){ ?>									
	<?php  foreach($items as $item){ ?> 

		<tr>
            <td class="product-thumbnail">

            <img width="90" height="90" src="{{ $item->imageURL }}" class="img_thumb" alt="">

            </td>

            <td>{{$item->name}}</td>
            <td>{{$item->product_code}}</td>
            <td>$</span>{{$item->price}}</span></td>
            <td>{{$item->quantity}}</td>
            <td>$</span><span id="totalprice_{{ $item->id }}">{{ number_format( $item->quantity * $item->price, 2) }} </span></td>
         <!--    <td>
            <a  href="{{ url('/').'/'.$item->vendor_id.'/'.$item->slug.'/detail' }}" class="btn btn-primary" target="_blank">View Product</a>
         </td> -->

		</tr>

    <?php } ?>
	<?php } ?>		

	</tbody>

</table>
													</div>
												</div>	
											</div>
										</div>
									</div>
								</div>
							</section>					
						  </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

    
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            // $(function() {
            //     $("#example1").dataTable();
            //     $('#example2').dataTable({
            //         "bPaginate": true,
            //         "bLengthChange": false,
            //         "bFilter": true,
            //         "bSort": true,
            //         "bInfo": true,
            //         "bAutoWidth": false
            //     });
            // });


function Inventory(proId){

var prodidun = '#product_qty'+proId;

var totalqtyun = '#totalqty'+proId;	

var soldqtyun = '#soldqty'+proId;		

var proqty = $(prodidun).val();

var vendorids = $('#vendorid').val();	


      $.ajax({
      
      type: "post",
      
      headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },
      
      data: {
      
              "_token": "{{ csrf_token() }}",
      
              "data": {productid:proId, productqty:proqty, vendorid:vendorids},
      
            },
      
      url: "{{url('/admin/inventory-product-action')}}",       
           
      success: function(msg) { 

      $('#inventoryid').show();

      var soldqty = $(soldqtyun).text();

      var totalqtynew = parseInt(proqty)+parseInt(soldqty);

      $(totalqtyun).text(totalqtynew);
         
      }
      
   });

}	

			
function check_frm()
{
$('#error_msg').hide();

var form = $("#search_frm");
	
	if(($("#rest_cont").val()!='') || ($("#rest_name").val()!=''))
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/vendor/restaurant_search')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#restaurant_list').html(msg);
			}
		});
	}
	else
	{
		//alert('Please insert any one value');
		$('#error_msg').html('Please insert any one value.');
		$('#error_msg').show();
		return false;
	}		
}


  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>


    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>

@stop
