@extends('layouts.admin')
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<style type="text/css">
	.field-icon {
		float: right;
		margin-left: -25px;
		margin-top: -25px;
		position: relative;
		z-index: 2;
	}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Edit sub-admin Form
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Edit sub-admin Form</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">Edit sub-admin Form</h3>
			</div><!-- /.box-header -->
			<p style="float: left;text-align: center;width: 100%;">
				@if(Session::has('message'))
				{{Session::get('message')}}
			@endif </p>
			<!-- form start -->
			@if(Session::has('errmessage'))
			<div class="alert alert-danger alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('errmessage')}}
			</div>
			@endif

			<?php
				// echo "<pre>";
				// print_r($user_detail);
				// echo "</pre>";
			?>

			<form  id="subAdmin" role="form" method="POST"  action="{{ url('/admin/subadmin-edit-form') }}">
				<input type="hidden" name="subadmin_id" value="{{$id}} " />
				{!! csrf_field() !!}
				<div class="box-body">
					<div class="form-group">
						<label for="exampleInputEmail1">Name</label>
						<input type="text" class="form-control" name="name" id="name" value="{{$user_detail[0]->name}}">
						<?php echo $errors->first('name'); ?>
					</div>
					<div class="form-group">
						<label for="exampleInputEmail1">Last Name</label>
						<input type="text" class="form-control" name="lname" id="lname"  value="{{$user_detail[0]->lname}}">
						<?php echo $errors->first('lname'); ?>
					</div>
						<!-- <div class="form-group">
							<label for="exampleInputEmail1">Mobile Number form</label>
							<select name="user_mob_type" id="user_mob_type"  class="form-control">
								<option value="1">Alltel</option>
								<option value="2" >AT&T</option>
								<option value="3" >Boost Mobile</option>
								<option value="4" >Sprint</option>
								<option value="5" >T-Mobile</option>
								<option value="6" >U.S. Cellular</option>
								<option value="7" >Verizon</option>
								<option value="8" >Virgin Mobile</option>
								<option value="9" >Republic Wireless</option>
							</select>
						</div> -->
						<div class="form-group">
							<label for="exampleInputEmail1">Mobile Number</label>
							<input type="text" class="form-control form-white"  name="mob" id="user_mob"  number="number" maxlength="10" value="{{$user_detail[0]->mobile_no}}">
							<?php echo $errors->first('mob'); ?>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							<input type="email" class="form-control" name="email" id="email" value="{{$user_detail[0]->email}}">
							<?php echo $errors->first('email'); ?>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">status</label>
							<select name="status" id="status"  class="form-control">
								<option value="1" <?php if($user_detail[0]->status=='1'){echo 'selected="selected"';}?>>Active</option>
								<option value="0" <?php if($user_detail[0]->status=='0'){echo 'selected="selected"';}?>>Deactive</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Password</label>


							<input type="password" class="form-control" name="password" id="password" required="required">
							<span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>



							<!-- <input type="password" class="form-control" name="password" id="password"> -->
							<p style="font-size:11px;color:red">Note: you can't see the password only can change.</p>
							<?php //echo $errors->first('password'); ?>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Confirm Password</label>

							<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" required="required">
							<span toggle="#confirmpassword" class="fa fa-fw fa-eye field-icon toggle-confirmpassword"></span>


							<!-- <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" > -->
							<?php //echo $errors->first('password'); ?>
						</div>

						<div class="form-group">
							<label>Your full address</label>
							<input type="text" class="form-control" name="address" id="autocomplete"   onFocus="geolocate()" value="{{$user_detail[0]->postal_address}}">
							<?php echo $errors->first('address'); ?>
							<input type="hidden"  id="street_number" name="street_number"/>
							<input type="hidden" class="field" id="route" name="route" />
							<input type="hidden"  id="country" name="country" />
							<input type="hidden" class="form-control" name="address1" id="address1" value="" />
							<input type="hidden" name="state" id="administrative_area_level_1" value="" />
						</div>
						<div class="form-group">
							<label>City</label>
							<input type="text" id="locality" name="city" class="form-control" value="{{$user_detail[0]->city}}">
							<?php echo $errors->first('city'); ?>
						</div>
						<div class="form-group">
							<label>Zip code</label>
							<input type="text" id="postal_code" name="zipcode" class="form-control"  number="number" maxlength="6" value="{{$user_detail[0]->zipcoad}}"/>
							<?php echo $errors->first('zipcode'); ?>
						</div>
					</div><!-- /.box-body -->
					<div class="box-footer">
						<input type="submit" name="subEdit" class="btn btn-primary"  value="Submit"  />
						<!-- <input type="button" class="btn btn-primary" value="Submit"   /> -->
						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>
			</div><!-- /.box -->
		</div>
	</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
<style>
#ajax_parner_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_parner_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
}
#addons-modal.modal {
	z-index: 999;
}
</style>
<div id="ajax_parner_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
@section('js_bottom')
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<!-- form validation start -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script> -->
	<script type="text/javascript">

		$(".toggle-password").click(function(){
			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});



		$(".toggle-confirmpassword").click(function(){
			$(this).toggleClass("fa-eye fa-eye-slash");
			var input = $($(this).attr("toggle"));
			if (input.attr("type") == "password") {
				input.attr("type", "text");
			} else {
				input.attr("type", "password");
			}
		});





		$(document).ready(function() {


			jQuery.validator.addMethod("pass", function (value, element) {
				if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {
					return true;
				} else {
					return false;
				};
			});


			jQuery.validator.addMethod("vname", function (value, element) {
				if (/^[a-zA-Z][a-z\s]*$/.test(value)) {
					return true;
				} else {
					return false;
				};
			});


		// jQuery.validator.addMethod("add", function (value, element) {
		// 	if (/^[a-zA-Z][a-z\s]*$/.test(value)) {
		// 		return true;
		// 	} else {
		// 		return false;
		// 	};
		// });


		jQuery.validator.addMethod("lname", function (value, element) {
			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {
				return true;
			} else {
				return false;
			};
		});


		$("#subAdmin").validate({
			rules: {
				name:{
					required:true,
					vname:true
				},
				lname: {
					required: true,
					lname:true
				},
				mob: {
					required: true,
					minlength:10,
					maxlength:10
				},
				email: {
					required: true,
					email:true
				},
				password: {
					required: false,
					minlength:8,
					pass:false
				},
				confirmpassword: {
					required: false,
					minlength:8,
					equalTo : "#password",
				},
				address: {
					required: true,
				},
				city: {
					required: false,
				},
				zipcode: {
					required: false,
				}
			},
			messages: {
				name:{
					required:'Please enter name.',
					vname:"Please enter only letters."
				},
				lname: {
					required:'Please enter last name.',
					lname:'Please enter only letters.'
				},
				mob: {
					required:'Please enter mobile number.',
					minlength:'Please enter valid mobile number.',
					maxlength:'Please enter valid mobile number.'
				},
				email: {
					required:'Please enter email address.',
					email:'Please enter an valid email address.',
				},
				password: {
					required:'Please enter password.',
					minlength:'Password must be at least 8 characters.',
					pass:"at least one number, one lowercase and one uppercase letter."

				},
				confirmpassword: {
					required:'Please enter password.',
					minlength:'Password must be at least 8 characters.',
					equalTo:'confirm password and password should be same, please enter correct.'
				},
				address: {
					required:'Please enter postal address.',
				},
				city: {
					required:'Please enter city name.',
				},
				zipcode: {
					required:'Please enter zipcoad.',
				}
			}
		});
	});
</script>
<!-- form validation end -->
<script>
	function check_email()
	{
		var dummy1 = $('#dummy1').val();
		var dummy2 = $('#dummy2').val();
		var dummy3 = $('#dummy3').val();
		var form = $("#user_frm");
		form.validate();
		var valid =	form.valid();
		if(valid){
			var dummy_counter = 0;
			if((dummy1==0) && (dummy2==0) && (dummy3==0))
			{
				dummy_counter = 1;
			}
			else
			{
				if( (dummy1==0) || (dummy2==0) || (dummy3==0) )
				{
					alert('Default user not match with other User , So Please select only default user or  different user!');
					dummy_counter = 0;
				}
				else if( (dummy1==dummy2) || (dummy2==dummy3) || (dummy1==dummy3) )
				{
					alert('User can not be same, So Please select different user!');
					dummy_counter = 0;
				}
				else
				{
					dummy_counter = 1;
				}
			}
			if(dummy_counter==1)
			{
				$("#ajax_parner_loddder").show();
				var frm_val = $('#user_frm').serialize();
				$.ajax({
					type: "POST",
					url: "./check_user_duplicateemail",
					data: frm_val,
					success: function(msg) {
						$("#ajax_parner_loddder").hide();
						if(msg=='1')
						{
							$('#email_msg').show();
							$('#number_msg').show();
							return false;
						}
						else if(msg=='2')
						{
							$('#email_msg').show();
							$('#number_msg').hide();
							return false;
						}
						else if(msg=='3')
						{
							$('#number_msg').show();
							$('#email_msg').hide();
							return false;
						}
						else if(msg=='4')
						{
							$(form).submit();
							return true;
						}
					}
				});
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function check_valid()
	{
		var form = $("#user_frm");
		form.validate();
		var valid =	form.valid();
		if(valid){
			var dummy1 = $('#dummy1').val();
			var dummy2 = $('#dummy2').val();
			var dummy3 = $('#dummy3').val();
			var dummy_counter = 0;
			if((dummy1==0) && (dummy2==0) && (dummy3==0))
			{
				dummy_counter = 1;
			}
			else
			{
				if( (dummy1==0) || (dummy2==0) || (dummy3==0) )
				{
					alert('Default user not match with other User , So Please select only default user or  different user!');
					dummy_counter = 0;
				}
				else if( (dummy1==dummy2) || (dummy2==dummy3) || (dummy1==dummy3) )
				{
					alert('User can not be same, So Please select different user!');
					dummy_counter = 0;
				}
				else
				{
					dummy_counter = 1;
				}
			}
			if(dummy_counter==1)
			{
				var frm_val = $('#user_frm').serialize();
				$(form).submit();
			}
			else
			{
				return false;
			}
				/*var frm_val = $('#user_frm').serialize();
				$(form).submit();*/
			}
			else
			{
				return false;
			}
		}
	</script>
	<script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      var placeSearch, autocomplete;
      var componentForm = {
      	street_number: 'short_name',
      	route: 'long_name',
      	locality: 'long_name',
      	administrative_area_level_1: 'short_name',
      	country: 'long_name',
      	postal_code: 'short_name'
      };
      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        	{types: ['geocode']});
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
        	document.getElementById(component).value = '';
        	document.getElementById(component).disabled = false;
        }
        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
        	var addressType = place.address_components[i].types[0];
        	if (componentForm[addressType]) {
        		var val = place.address_components[i][componentForm[addressType]];
        		document.getElementById(addressType).value = val;
			//alert(val);
		}
	}
}
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
      	if (navigator.geolocation) {
      		navigator.geolocation.getCurrentPosition(function(position) {
      			var geolocation = {
      				lat: position.coords.latitude,
      				lng: position.coords.longitude
      			};
      			var circle = new google.maps.Circle({
      				center: geolocation,
      				radius: position.coords.accuracy
      			});
      			autocomplete.setBounds(circle.getBounds());
      		});
      	}
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>
  @stop