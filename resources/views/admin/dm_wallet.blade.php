<style type="text/css">
  .methodbw{
    text-transform: capitalize;
  }
</style>
@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
    
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
   <link href="{{ url('/') }}/design/css/easy-responsive-tabs.css" rel="stylesheet">
   <meta name="_token" content="{!! csrf_token() !!}"/>
@stop

@section('content')

<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Delivery Man Account
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Delivery Man Account</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
  <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
           
                 @if(Session::has('message'))
           
                 <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                           {{Session::get('message')}}
                 </div>
                @endif
          
            <div class="col-xs-12">

          <div class="in_wal">
          <div id="horizontalTab">
          <ul class="resp-tabs-list">
            <li>Bank Setting</li>
            <li>Remuneration Summary</li>
            <li>Remuneration transfer to Bank</li>
            <!--<li>Payment Distribution Summary</li>-->
            </ul>

      <div class="resp-tabs-container">
                
      <div>
      <div class="dwallet">

      <?php if($dm_id){ ?>  

      <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Delivery Man Bank Account Setup</h3>
      </div><!-- /.box-header -->

      <!-- form start -->
        <form name="banksetup" id="banksetup" method="post" action="{{ url('/') }}/dm/bank-setup">
        
          @if($dm_bank_detail)
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Account Holder Name * </label>
              <input type="text" class="form-control" name="holder_name" value="{{$dm_bank_detail->holder_name}}" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Bank Name * </label>
             <input type="text" class="form-control" name="bank_name" value="{{$dm_bank_detail->bank_name}}" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Branch * </label>
             <input type="text" class="form-control" name="branch" value="{{$dm_bank_detail->branch}}" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Account Number * </label>
             <input type="text" class="form-control" name="account_number" value="{{$dm_bank_detail->account_number}}" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">IFSC Code * </label>
             <input type="text" class="form-control" name="ifsc_code" value="{{$dm_bank_detail->ifsc}}" required="required" placeholder="">
            </div>

            <div class="box-footer">
              <input type="hidden" name="dm_id" value="{{$dm_bank_detail->dm_id}}">
              <input type="submit" class="btn btn-primary" value="Update">
              <input type="button" class="btn btn-primary" value="Go Back" onclick="history.go(-1);">
            </div>
          </div>
          @else
          <div class="box-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Account Holder Name * </label>
              <input type="text" class="form-control" name="holder_name" value="" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Bank Name * </label>
             <input type="text" class="form-control" name="bank_name" value="" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Branch * </label>
             <input type="text" class="form-control" name="branch" value="" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">Account Number * </label>
             <input type="text" class="form-control" name="account_number" value="" required="required" placeholder="">
            </div>

            <div class="form-group">
             <label for="exampleInputEmail1">IFSC Code * </label>
             <input type="text" class="form-control" name="ifsc_code" value="" required="required" placeholder="">
            </div>

            <div class="box-footer">
              <input type="hidden" name="dm_id" value="{{$dm_id}}">
              <input type="submit" class="btn btn-primary" value="Submit">
              <input type="button" class="btn btn-primary" value="Go Back" onclick="history.go(-1);">
            </div>
          </div>
          @endif

        </form>
    </div>

  <?php }else{ ?>

      <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Delivery Man List</h3>
      </div><!-- /.box-header -->

              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">

                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr.No.</th>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <!--<th>Address</th>-->
                                  <th>City</th>
                                  <th>Zip Code</th>
                                  <th>Add / View Bank Details</th>
                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($deliveryman_list)) 
                               @foreach ($deliveryman_list as $list)
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ $list->name}} {{ $list->lname}}</td>
                                  <td>{{ $list->email}}</td>
                                  <td>{{ $list->user_mob}}</td>
                                  <!--<td>{{ $list->user_address}}</td>-->
                                  <td>{{ $list->user_city }}</td>
                                  <td>{{ $list->user_zipcode }}</td>

          <td><a href="{{url('admin/dm_wallet')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a></td>

                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          


         </div> 


         <?php  } ?>


              </div>
           
            </div> 

            <div>
               <div class="dwallet">
              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Order Id</th>
                                  <th>Restaurant</th>
                                  <th>Delivery Man</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th>Remuneration</th>
                                  <th>Delivery Time</th>
                                  


                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($payment_detail)) 
                               @foreach ($payment_detail as $list)
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ 
                                  $list->order_id}}
                                  ({{$list->order_uniqueid}})</td>
                                  <td>{{ $list->rest_name}}</td>
                              
                                  <td>{{ $list->name}} {{ $list->lname}}</td>
                                  <td>{{ $list->email}}</td>
                                  <td>{{ $list->user_mob}}</td>
                                  <td>${{ $list->order_income}}</td>
                                  <td>{{ $list->deliver_order_time}}</td>

                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>          
              </div>
                        </div>

      <div>
      <div class="dwallet">

              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                          <!--<div style="float:right; margin:5px;"> 
                        <input class="btn btn-primary" type="button" id="generate_file"  value="Export">
                            </div>-->
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Order Id</th>
                                  <th>Restaurant</th>
                                  <th>Delivery Man</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th>Amount</th>
                                  <th>Bank Status</th>
                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($transfer_bank)) 
                               @foreach ($transfer_bank as $list)
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ 
                                  $list->order_id}}
                                  ({{$list->order_uniqueid}})</td>
                                  <td>{{ $list->rest_name}}</td>
                                  <td>{{ $list->name}} {{ $list->lname}}</td>
                                  <td>{{ $list->email}}</td>
                                  <td>{{ $list->user_mob}}</td>
                                  <td>${{ $list->order_income}}</td> 
                          <td><?php if($list->pay_status == 1){ ?>

                          <button name="dmcmp" class="btn label-success" id="dmcmp" style="color: #ffffff;">Complete</button>

                          <?php } else{ ?>

                      <a href="{{url('admin/dm_status')}}/{{ $list->order_id }}"><button name="dmcmp" class="btn btn-primary" id="dmcmp" title="Change Status">Pending</button></a>

                          <?php } ?> </td>

                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section> 

            </div>
          </div>

 
           <!--<div>
              <div class="transactions_hist">
                
            <div>
            <div class="dwallet">

              <section>
                <div class="row">
                  <div class="my-panel-data">
                     <div class="col-xs-12">
                        <div class="box">
                        <div class="box-body table-responsive">
                       
                          <div id="restaurant_list">              
                             <table id="example2" class="table table-bordered table-hover">
                              <thead>
                                <tr>
                                  <th>Sr. No.</th>
                                  <th>Order Id</th>
                                  <th>Delivery Man</th>
                                  <th>Email</th>
                                  <th>Mobile</th>
                                  <th>Remuneration</th>
                                  <th>Restaurant Name</th>


                                </tr>
                              </thead>
                              <tbody>                   
                              <?php $i=1; ?>
                              @if(!empty($distribution)) 
                               @foreach ($distribution as $list)
                                <tr>
                                  <td>{{ $i }}</td>
                                  <td>{{ 
                                  $list->order_id}}
                                  ({{$list->order_uniqueid}})</td>
                              
                                  <td>{{ $list->name}} {{ $list->lname}}</td>
                                  <td>{{ $list->email}}</td>
                                  <td>{{ $list->user_mob}}</td>
                                  <td>${{ $list->order_income}}</td>
                                  <td>{{ $list->rest_name}}</td>


                                </tr> 
                    
                              <?php $i++; ?>
                              @endforeach    
                              @endif                                     
                              </tbody>
                            </table>  
                          </div>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </section>  

              </div>
                        </div>

              </div>
            </div>-->


           </div>


                      </div>
                      </div>
                    </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop


@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
  z-index: 999;
}
.modal-backdrop {
  
  z-index: 998 !important;
}
</style>  
  
<div id="ajax_favorite_loddder" style="display:none;">
  <div align="center" style="vertical-align:middle;">
    <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
  </div>
</div>

        <script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
    
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
                $('#payment_distribution').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
                $('#vendor_payment_request').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
  $( function() {
  var dateToday = new Date();
    $( ".datepicker" ).datepicker({
     dateFormat: 'yy-mm-dd',
     autoclose: true,
    });
  }); 
  
  
  
$(document).on('click', '#search_btn', function(){ 
  
   
    
   $('#error_msg').hide();
var frmdate = $('#fromdate').val(); 
var todate = $('#todate').val(); 
  
  if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
  {//compare end <=, not >=
    //your code here
    //alert("From date will be big from to date!");
    
    $('#error_msg').html('From date will be big from to date!');
    $('#error_msg').show();
  }
  else
  { 
    $("#ajax_favorite_loddder").show(); 
    var frm_val = $('#search_frm').serialize();       
    $.ajax({
    type: "POST",
    url: "{{url('/admin/payment_search_list')}}",
    data: frm_val,
    success: function(msg) {
       $("#ajax_favorite_loddder").hide();  
      //alert(msg)
        $('#restaurant_list').html(msg);
      }
    });
  }   
});     
      

  
$(document).on('click', '#generate_file', function(){     



 $('#search_frm').attr('action', "{{url('/admin/payment_report')}}");
 
 $('#search_frm').attr('target', '_blank').submit();

    
});   

  $.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
 </script>

<script src="{{ url('/') }}/design/front/js/easy-responsive-tabs.js"></script> 
<script>
$(document).ready(function () {
$('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
$('#verticalTab').easyResponsiveTabs({
type: 'vertical',
width: 'auto',
fit: true
});
});
</script> 

@stop
