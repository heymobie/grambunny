@extends('layouts.admin')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Coupon Form

			<small>Control Panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Coupon Form</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

		<div class="col-md-12">

			<!-- general form elements -->

			<div class="box box-primary">

				
				

				<p style="float: left;text-align: center;width: 100%;">

					@if(Session::has('message'))

					{{Session::get('message')}}

				@endif </p>

				<!-- form start -->

				@if(Session::has('errmessage'))

				<div class="alert alert-danger alert-dismissable">

					<i class="fa fa-check"></i>

					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

					{{Session::get('errmessage')}}

				</div>

				@endif

				<form  id="subAdmin" role="form" method="POST"  action="{{ url('/admin/coupon-add') }}" autocomplete="off">

					{!! csrf_field() !!}

					<div class="box-body">
						<div class="col-md-4">
						<div class="form-group">

							<label>Name</label>

							<input type="text" class="form-control" name="coupon_name" id="coupon_name" required="required">

						</div>
							</div>
							<div class="col-md-4">
						<div class="form-group">

							<label>Coupon code</label>

							<input type="text" class="form-control" name="coupon" id="coupon" required="required">

						</div>
							</div>
								<div class="col-md-4">
						<div class="form-group">

                            <label>Discount</label>

							<select name="discount" id="discount" class="form-control">

							<option value="0" selected="selected">Flat</option>

							<option value="1" >Percentage</option>

							</select>

                        </div>
                        	</div>
                        		<div class="col-md-4">
						<div class="form-group">

							<label id="couponprsnt">Coupon amount</label>

							<input type="number" id="amount" name="amount" class="form-control" required="required">

						</div>
							</div>
							<div class="col-md-4">
						<div class="form-group" id="appminamt">

						<label>Apply on minimum amount</label>

						<input type="number" id="mamount" name="mamount" class="form-control" required="required">

						</div>
							</div>
							<div class="col-md-4">
						<div class="form-group">

							<label>Expiry Date</label>

							<input type="date" id="valid_till" name="valid_till" class="form-control" required="required"/>

						</div>
</div>
						<!--<div class="form-group">

							<label>Usage Limit</label>

					<input type="number" id="usage_limit" name="usage_limit" class="form-control"/>

						</div>-->

						<input type="hidden" id="usage_limit" name="usage_limit" value="1" />

						<!--<div class="form-group">

							<label>Valid Per User</label>

							<input type="number" id="per_user" name="per_user" class="form-control"/>

						</div>-->

						<input type="hidden" id="per_user" name="per_user" value="1"/>

						<!-- <div class="form-group">

							<label>Description</label>

							<input type="text" class="form-control" name="description" id="description">

						</div> -->

						<input type="hidden" class="form-control" name="description" id="description" value="">
					<div class="col-md-4">
                	<div class="form-group">

                 <label>Status</label>

							<select name="coupon_status" id="coupon_status" class="form-control">

							<option value="1" selected="selected">ACTIVE</option>

							<option value="0" >INACTIVE</option>

							</select>

                        </div>
					</div>
					</div><!-- /.box-body -->

					<div class="box-footer">

						<input type="submit" class="btn btn-primary"  value="Submit"  />

						<!-- <input type="button" class="btn btn-primary" value="Submit"   /> -->

						<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

					</div>

				</form>

			</div><!-- /.box -->

		</div>

	</section><!-- /.content -->

</aside><!-- /.right-side -->

@endsection

<style>

#ajax_parner_loddder {

	position: fixed;

	top: 0;

	left: 0;

	width: 100%;

	height: 100%;

	background:rgba(27, 26, 26, 0.48);

	z-index: 1001;

}

#ajax_parner_loddder img {

	top: 50%;

	left: 46.5%;

	position: absolute;

}

.footer-wrapper {

	float: left;

	width: 100%;

}

#addons-modal.modal {

	z-index: 999;

}

</style>

<div id="ajax_parner_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>

@section('js_bottom')

<!-- jQuery 2.0.2 -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<!-- jQuery UI 1.10.3 -->

<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- Bootstrap -->

<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Bootstrap WYSIHTML5 -->

<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->

<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<!-- form validation start -->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

	<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script> -->

	<script type="text/javascript">

		$(document).ready(function() {





			jQuery.validator.addMethod("aname", function (value, element) {

				if (/^[a-zA-Z0-9-,. ]*$/.test(value)) {

					return true;

				} else {

					return false;

				};

			});





			$("#subAdmin").validate({

				rules: {

					address: {

						required: true,

						aname:true

					},

					city: {

						required: false,

					},

					zipcode: {

						required: false,

					}

				},

				messages: {

					address: {

						required:'Please enter area name.',

						aname:'Please enter valid area name'

					},

					city: {

						required:'Please enter city name.',

					},

					zipcode: {

						required:'Please enter zipcoad.',

					}

				}

			});

		});

	</script>

	<!-- form validation end -->

	<script>

      // This example displays an address form, using the autocomplete feature

      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places

      // parameter when you first load the API. For example:

      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;

      var componentForm = {

      	street_number: 'short_name',

      	route: 'long_name',

      	locality: 'long_name',

      	administrative_area_level_1: 'short_name',

      	country: 'long_name',

      	postal_code: 'short_name'

      };

      function initAutocomplete() {

        // Create the autocomplete object, restricting the search to geographical

        // location types.

        //console.log('initAutocomplete');

        autocomplete = new google.maps.places.Autocomplete(

        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),

        	{types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address

        // fields in the form.

        autocomplete.addListener('place_changed', fillInAddress);





        // var place = autocomplete.getPlace();

        // document.getElementById('r_lat').value = place.geometry.location.lat();

        // document.getElementById('r_lang').value = place.geometry.location.lng();



    }

    function fillInAddress() {

        // Get the place details from the autocomplete object.

        //console.log('fillInAddress');

        var place = autocomplete.getPlace();

        for (var component in componentForm) {

        	document.getElementById(component).value = '';

        	document.getElementById(component).disabled = false;

        }

        // Get each component of the address from the place details

        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {

        	var addressType = place.address_components[i].types[0];

        	if (componentForm[addressType]) {

        		var placeID = place.place_id;

        		$('#place_ID').val(placeID);



        		var place = autocomplete.getPlace();

        		var lat = place.geometry.location.lat();

        		var long = place.geometry.location.lng();

        		$('#r_lat').val(lat);

        		$('#r_lang').val(long);



        		//console.log(placeID);

        		var val = place.address_components[i][componentForm[addressType]];

        		document.getElementById(addressType).value = val;

        		//console.log(place.address_components);

			//alert(val);

		}

	}

}

      // Bias the autocomplete object to the user's geographical location,

      // as supplied by the browser's 'navigator.geolocation' object.

      function geolocate(){

      	//alert('test');

      	//console.log('testsfdsdf');

      	if (navigator.geolocation) {

      		navigator.geolocation.getCurrentPosition(function(position) {

      			var geolocation = {

      				lat: position.coords.latitude,

      				lng: position.coords.longitude

      			};

      			var circle = new google.maps.Circle({

      				center: geolocation,

      				radius: position.coords.accuracy

      			});

      			autocomplete.setBounds(circle.getBounds());

      		});

      	}

      }


 $("#discount").on("change",function(){
  
    var selValue = $("#discount").val();

    if(selValue==1){ 
    	
    	//$("#amount").value('1');
    	$("#mamount").prop('disabled', true);
    	$("#couponprsnt").text('Coupon %');

    }else{

      //$("#amount").value(''); 
      $("#mamount").prop('disabled', false);
      $("#couponprsnt").text('Coupon amount');	
      

  }

});     

  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>

  @stop