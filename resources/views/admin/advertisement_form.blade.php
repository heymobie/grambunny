@extends('layouts.admin')
@section('content')
<aside class="right-side">
	<section class="content-header">
		<h1>Advertisement & Static Block Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Advertisement & Static Block Form</li>
		</ol>
	</section>
	<section class="content">
		<div class="col-md-12">
			<div class="box box-primary">
				
				@if(Session::has('error'))
				<div class="alert alert-danger alert-dismissable">			
					{{Session::get('error')}}
				</div>
				@endif
				<form  role="form" enctype="multipart/form-data" method="POST" id="cuisine_frm" action="{{ url('/admin/advertisement-action') }}">    
					<input type="hidden" name="id" value="{{$id}} " />
					<input type="hidden" name="old_image" value="@if($id>0){{ $advertisement_detail[0]->image }}@endif" />
					{!! csrf_field() !!}
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Name</label>
									<input type="text" class="form-control" name="name" id="name" value="@if($id>0){{$advertisement_detail[0]->name}}@endif" required="required">
								</div>		
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Content</label>
									<textarea  class="form-control" name="content" id="content">@if($id>0){{$advertisement_detail[0]->content}}@endif</textarea>
								</div>	
							</div>
						</div>						
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Image (File Type: jpeg,gif,png)</label>
									<input type="file" class="form-control" name="testi_image" id="testi_image" @if($id==0) required @endif>
									@if(($id>0) && (!empty($advertisement_detail[0]->image)))
									<img src="{{ url('/') }}/public/uploads/advertisement/{{ $advertisement_detail[0]->image }}" width="50px;" height="50px;">
									@endif	
									<div id="image_error" style="display:none; color:#FF0000">
									Upload Images only   jpeg,gif,png
									</div>
								</div>		
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">Status</label>
									<select name="status" id="status"  class="form-control">
										<option value="1" @if(($id>0) && ($advertisement_detail[0]->status==1)) selected="selected"@endif>Active </option>
										<option value="0" @if(($id>0) && ($advertisement_detail[0]->status==0)) selected="selected"@endif>Inactive </option>
									</select>
								</div>								
							</div>
						</div>
					</div>
					<div class="box-footer">	
					    <input type="submit" class="btn btn-primary" value="Submit"/>	
						<!-- <input type="button" class="btn btn-primary" value="Submit" onClick="check_email()" /> -->
						<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
					</div>
				</form>
			</div>
		</div>
	</section>
</aside>
@endsection
@section('js_bottom')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script>
		function check_email(){
		var form = $("#cuisine_frm");
		form.validate();
		var valid =	form.valid();
		var fup = document.getElementById('testi_image');
        var fileName = fup.value;
	//alert(fileName);
    	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
			if(valid){	
//			alert(fileName);
			if(fileName!=''){
				if(ext =="GIF" || ext=="gif" || ext=="png"|| ext=="PNG" || ext=="jpeg" || ext=="JPEG" || ext=="jpg" || ext=="JPG"){
					$('#image_error').hide();
					$(form).submit();
							return true;	
						}
						else{
							$('#image_error').show();
							return false;
						}
					}
					else
					{
							$(form).submit();
							return true;	
					}				
			}
			else
			{
				return false;
			}		
		}
	</script>
@stop