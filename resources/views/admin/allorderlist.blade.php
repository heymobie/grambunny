@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->

   
  <style type="text/css">

        #myInput{ float: right; width: 30%;margin-bottom: 10px; }
      
  </style>  

@stop

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Report
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Product Order List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div>
                <div class="box-body table-responsive">
                    @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                {{Session::get('message')}}
                    </div>
                    @endif      
                    </div><!-- /.box-body -->                 


<!-- <div class="col-md-3" style="padding-left: 0px;">Filter &nbsp;     
<input type="button" name="filter" id="filter" value="filter">
</div> -->
                      <!-- <select name="order_data" id="order_data">
                                        <option value="all" <?php if(@$order_data=='all'){echo 'selected';} ?>>All</option>  
                                        <option value="10" <?php if(@$order_data=='10'){echo 'selected';}else{echo 'selected';} ?>>10</option>
                                        <option value="25" <?php if(@$order_data=='25'){echo 'selected';} ?>>25</option>
                                        <option value="50" <?php if(@$order_data=='50'){echo 'selected';} ?>>50</option>
                                        <!-- <option value="100">100</option> -->
                                        </select> 

                <section id="mainsection" >
                    
                    <div class="row">
                        <div class="my-panel-data">
                             <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body table-responsive">
                                      
                                        <br/>
                                        <div >                          
                                        <table id="myTable11" data-order='[[ 0, "desc" ]]' class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                <th>ID#</th>
                                                <th>Merchant</th>
                                                
                                                <!-- <th>#</th> -->
                                                <th>Order By</th>
                                                <th>Customer Email</th>
                                                <th>ContactNumber</th>
                                                <th>Price</th>
                                                <th>Status</th>
                                                <th>Placed at</th>
                                                <th>Order Type</th>
                                                <th>Coupon Code</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="myTable">                                        
                                            <?php foreach ($order_detail as $key => $value) { 
                                            $userinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
                                            $userinfo1 = DB::table('users')->where('id','=',$value->user_id)->first();

                                                $timestamp = strtotime($value->orderdate);
                                                $dataPoints1 = array(
                                                    array("label"=>date("d", $timestamp) , "y"=> $value->total),
                                                    
                                                );
                                                //print_r($dataPoints1);
                                             ?>

                                                <tr>
                                                <td>{{$value->order_id}}</td>
                                                <td>
                                                    <?php if(!empty($userinfo)){?>
                                                    {{$userinfo->name}} {{$userinfo->last_name}}
                                                <?php }?>
                                                </td>
                                                
                                                 <?php if(!empty($userinfo1)){?>
                                                <td>{{$userinfo1->name}} {{$userinfo1->lname}}
                                                   <?php }?>
                                                </td>
                                                <td>{{$userinfo1->email}}</td>
                                                <td>{{$userinfo1->user_mob}}</td>
                                                <td>${{ number_format($value->total, 2) }}</td>
                                                <td>                            
                                                @if($value->status==0) Pending @endif
                                                @if($value->status==1) Accept @endif
                                                @if($value->status==2) Cancelled @endif
                                                @if($value->status==3) On the way @endif
                                                @if($value->status==4) Complete @endif
                                                @if($value->status==5) Requested for return @endif
                                                @if($value->status==6) Return request accepted @endif
                                                @if($value->status==7) Return request declined @endif
                                                </td>

                                            <?php $createdate = date("Y-m-d g:iA", strtotime($value->orderdate)); ?>

                                                <td>{{$createdate}}</td>
                                                <td> @if($value->product_type!=3) Product & Service @endif
                                                   @if($value->product_type==3) Event @endif</td>
                                                
                                                <td>
                                                    <?php $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->first(); 
                                                    echo @$coupon_code->coupon;
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="{{ url('admin/order-details').'/'.$value->id}}">View</a>
                                                </td>

                                                </tr> 

                                            <?php } ?>
                                                                               
                                            </tbody>
                                          <!--   <tfoot>
                                                <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                </tr>
                                            </tfoot> -->
                                            </table>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>

               
                
                
                </section>  

                <section id="vender_search_list" >
                </section>

            
                
              </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
    z-index: 999;
}
.modal-backdrop {
    
    z-index: 998 !important;
}
</style>    
    
<div id="ajax_favorite_loddder" style="display:none;">
    <div align="center" style="vertical-align:middle;">
        <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
    </div>
</div>

        <!-- jQuery 2.0.2 -->
      
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
      <!--   <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script> -->
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

        <!-- page script -->
        <script type="text/javascript">
        


$('#order_search').on('change', function() {
  //alert( this.value );
      if(this.value=='today_sale_report'){
            $('#today_sale').show();
            $('#week_sale').hide();
            $('#month_sale').hide();
            
      }
      else if(this.value=='this_week_report'){
            $('#today_sale').hide();
            $('#week_sale').show();
            $('#month_sale').hide();
            
      }
      else if(this.value=='this_month_report'){

            $('#today_sale').hide();
            $('#week_sale').hide();
            $('#month_sale').show();
            

      }
});
        </script>

        <script type="text/javascript">
            
            $('#order_id').keyup(function() {
                var order_id = $('#order_id').val();
                //var order_status_serach = $('#order_status_serach').val();
                // alert(order_status_serach);
                // if(){

                // }else{

                // }
                //var data = 'order_id='+ order_id  & 'order_status_serach='+ order_status_serach;
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order')}}",
                data: ({order_id: order_id}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });
        

        $(document).on('change','#order_data', function() {
                var order_data = $('#order_data').val();
                
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_data')}}",
                data: ({order_data: order_data}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
        });

        $('#customer_name').keyup(function() {
                var customer_name = $('#customer_name').val();
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_customer')}}",
                data: ({customer_name: customer_name}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });

            $('#customer_id').keyup(function() {
                var customer_id = $('#customer_id').val();

                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_id')}}",
                data: ({customer_id: customer_id}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });


        $('#merchant').keyup(function() {
                var merchant = $('#merchant').val();
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_merchant')}}",
                data: ({merchant: merchant}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });

        $( '#datefilter' ).on( 'submit', function(e) {
                e.preventDefault();
                var to_order_date = $('#order_date').val();
                var from_order_date = $('#from_order_date').val();

                var order_date = to_order_date+'_'+from_order_date;

                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_date')}}",
                data: ({order_date: order_date}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });


        $('#coupon').keyup(function() {
                var coupon = $('#coupon').val();
                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                type: "POST",
                url: "{{url('/admin/filter_order_coupon')}}",
                data: ({coupon: coupon}),
                    success: function(msg) {

                        $("#mainsection").hide();   
                        $('#vender_search_list').html(msg);
                    }
                });
                
            });

        </script>

   <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>

@stop
