@extends('layouts.admin')



@section("other_css")

        <!-- DATA TABLES -->

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

@stop


<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Minimum Order Amount

        </h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Minimum Order Amount</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <div class="box">
         
                    <div class="box-body table-responsive">

		 @if(Session::has('message'))

		 <div class="alert alert-success alert-dismissable">

              <i class="fa fa-check"></i>

               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                           {{Session::get('message')}}

         </div>

		     @endif

                    <table id="example2" class="table table-bordered table-hover">

                            <thead>

                                <tr>

                                    <th>Minimum Order Amount($)</th>

                                </tr>

                            </thead>

                            <tbody>										


                <tr>

                 <td>

                  <form  role="form" method="POST" id="user_frm" action="{{ url('/admin/minimum-order-amount-change') }}"  enctype="multipart/form-data">                  

                  <div class="col-md-4">

                  <div class="form-group">

                   {!! csrf_field() !!} 

                  <input type="text" id="order_min" name="order_min" class="form-control" value="{{ $min_order }}">

            <!-- <input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  /> -->

          </div>
          <input type="submit" class="btn btn-primary" value="Save"/>
           </div>

        </form>  

        </td> 

              </tr> 										
                 
							</tbody>

                            <tfoot>

                                <tr>

                                    <th>&nbsp;</th>

                                    <th>&nbsp;</th>                     

                                </tr>

                            </tfoot>

                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div>

        </div>



    </section><!-- /.content -->

</aside><!-- /.right-side -->



@stop



@section('js_bottom')



        <!-- jQuery 2.0.2 -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- Bootstrap -->

        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

        <!-- AdminLTE App -->

        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- page script -->

        <script type="text/javascript">

            $(function() {

                $("#example1").dataTable();

                $('#example2').dataTable({

                    "bPaginate": true,

                    "bLengthChange": false,

                    "bFilter": true,

                    "bSort": true,

                    "bInfo": true,

                    "bAutoWidth": false

                });

            });

        </script>

@stop

