@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Cuisine Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Cuisine Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Cuisine Form</h3>
                                </div><!-- /.box-header -->
								
					 @if(Session::has('error'))
					 <div class="alert alert-danger alert-dismissable">				
								{{Session::get('error')}}
								</div>
							@endif 

					
                                <!-- form start -->
								
								<form  role="form" enctype="multipart/form-data" method="POST" id="cuisine_frm" action="{{ url('/admin/cuisine_action') }}">    
								<input type="hidden" name="cuisine_id" value="{{$id}} " />
								<input type="hidden" name="cuisine_old_logo" value="@if($id>0) $cuisine_detail[0]->cuisine_image @endif" />
								
								
								{!! csrf_field() !!}
                                    <div class="box-body">

                                    	<!--<div class="form-group">
                                            <label for="exampleInputEmail1">Select Language</label>
                                            <?php //echo "<pre>"; print_r($cuisine_detail); ?>
                                            <select name="cuisine_lang" id="cuisine_lang"  class="form-control">
                                            	
												<option value="en" @if(($id>0) && ($cuisine_detail[0]->cuisine_lang=="en")) selected="selected"@endif > English </option>
												<option value="ar" @if(($id>0) && ($cuisine_detail[0]->cuisine_lang=="ar")) selected="selected"@endif > Arabic </option>
											</select>
                                        </div>-->

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name (English)</label>
                                            <?php //echo "<pre>"; print_r($cuisine_detail); ?>
                                           <input type="text" class="form-control" name="cuisine_name" id="cuisine_name" value="@if($id>0){{$cuisine_detail[0]->cuisine_name}}@endif" required="required">
                                        </div>

                                       <div class="form-group">
                                            <label for="exampleInputEmail1">Name (Arabic)</label>
                                            <?php //echo "<pre>"; print_r($cuisine_detail); ?>
                                           <input type="text" class="form-control" name="cuisine_name_ar" id="cuisine_name_ar" value="@if($id>0){{$cuisine_detail[0]->cuisine_name_ar}}@endif" required="required">
                                        </div>

										<input type="hidden" class="form-control" name="cuisine_image" id="cuisine_image" value="1">
										
										<!-- <div class="form-group">
                                            <label for="exampleInputEmail1">Image (File Type: jpeg,gif,png)</label>
                                            <input type="file" class="form-control" name="cuisine_image" id="cuisine_image">		
											
											@if(($id>0) && (!empty($cuisine_detail[0]->cuisine_image)))
											
											<img src="{{ url('/') }}/uploads/cuisine/{{ $cuisine_detail[0]->cuisine_image }}" width="150px;" height="50px;">
											@endif	
											
											<div id="image_error" style="display:none; color:#FF0000">
												Upload Images only   jpeg,gif,png
											</div>													
                                        </div> -->
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>
												
										<select name="cuisine_status" id="cuisine_status"  class="form-control">
										 <option value="1" @if(($id>0) && ($cuisine_detail[0]->cuisine_status==1)) selected="selected"@endif>Active </option>
										 <option value="0" @if(($id>0) && ($cuisine_detail[0]->cuisine_status==0)) selected="selected"@endif>Inactive </option>
										</select>		
												
                                        </div>
										
										
										
										
                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		
										<input type="button" class="btn btn-primary" value="Submit" onClick="check_email()" />
									
									 
									 	<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
		<script>
		function check_email()
		{
		
		var form = $("#cuisine_frm");
		form.validate();
			var valid =	form.valid();
			
			var fup = document.getElementById('cuisine_image');
        var fileName = fup.value;
	//alert(fileName);
		
      	  var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
			
			
			if(valid){	
			
//			alert(fileName);
					// if(fileName!='')
					// {
					// 	if(ext =="GIF" || ext=="gif" || ext=="png"|| ext=="PNG" || ext=="jpeg" || ext=="JPEG" || ext=="jpg" || ext=="JPG")
					// 	{
					// 		$('#image_error').hide();
							
						
					// 		$(form).submit();
					// 		return true;	
					// 	}
					// 	else
					// 	{
					// 		$('#image_error').show();
					// 		return false;
					// 	}
					// }
					// else
					// {
				
						
					// 		$(form).submit();
					// 		return true;	
							
					// }
					$(form).submit();
							return true;				
			}
			else
			{
				return false;
			}		
		}
		</script>
@stop