@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  	
#myInput{ float: right; width: 30%;margin-bottom: 10px; }
      
</style>  
   
@stop

@section('content')
<aside class="right-side">    
    <section class="content-header">
        <h1>Merchant Bank Account List</h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Merchant Bank Account List</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Merchant Bank Account List</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
						@if(Session::has('message'))		 
						<div class="alert alert-success alert-dismissable">
				            <i class="fa fa-check"></i>
				            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				            {{Session::get('message')}}
				        </div>
						@endif
                    </div>
                </div>
				<div>


			<input id="myInput" type="text" placeholder="Search..">
            <br>

				<section>
				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">
										<div id="vender_search_list">	
							
										<table id="example2" class="table table-bordered table-hover">
<thead>
<tr>
<th>IDs</th>
<th>Merchant</th>
<th>Bank Name</th>
<th>Branch</th>
<th>Account Number</th>
<th>Holder Name</th>
<th>Ifsc</th>
<!--<th>Date</th>-->

</tr>
</thead>
<tbody id="myTable">										
<?php foreach ($vendor_bank_detail as $key => $value) { 

$vendorinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->value('username');
//$vendid = count($vendorinfo);	



?>



<tr>
<td>{{$value->id}}</td>
<td>{{$vendorinfo}}</td>

<td>{{$value->bank_name}}</td>
<td>{{$value->branch}}</td>
<td>{{$value->account_number}}</td>
<td>{{$value->holder_name}}</td>
<td>{{$value->ifsc}}</td>

<!--<td>{{$value->created_at}}</td>-->

</tr> 

<?php } ?>
                                   
</tbody>
<tfoot>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</tfoot>
</table>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
	              	{{ $vendor_bank_detail->links() }}
	              </div>
	            </div>
	            
				</section>					
			  </div>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')

<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

        <!-- jQuery 2.0.2 -->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

        <!-- page script -->
        <script type="text/javascript">
function check_frm()
{


$('#error_msg').hide();

var form = $("#search_frm");
		form.validate();
	var valid =	form.valid();
	if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
	{		
		 $("#ajax_favorite_loddder").show();	
		var frm_val = $('#search_frm').serialize();				
		$.ajax({
		type: "POST",
		url: "{{url('/admin/vendor_search')}}",
		data: frm_val,
			success: function(msg) {
			 $("#ajax_favorite_loddder").hide();	
			
				$('#vender_search_list').html(msg);
			}
		});
	}
	else
	{
		//alert('Please insert any one value');
		
		$('#error_msg').html('Please insert any one value');
		$('#error_msg').show();
		return false;
	}		
}			

$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
        </script>
@stop
