@extends('layouts.admin')
<style>
.ui-widget-content {
    max-height: 221px;
    overflow-y: scroll;
}
.ui-menu .ui-menu-item {
    padding: 5px;
}
.ui-menu-item:nth-child(2n) {
    background-color: #f1f1f1;
}
</style>
@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Transport Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Transport Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                             
											
					 @if(Session::has('message_error'))
					 
					 <div class="alert alert-danger alert-dismissable">
                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                       {{Session::get('message_error')}}
                     </div>
					@endif
                                <!-- form start -->
								
								<form  role="form" method="POST" id="rest_frm" action="{{ url('/admin/transport_action') }}" enctype="multipart/form-data">    
								<input type="hidden" name="transport_id" value="{{$id}}" />
								<input type="hidden" name="transport_old_logo" id="transport_old_logo" value="@if($id>0){{$trans_detail[0]->transport_logo}} @endif" />
								<input type="hidden" name="transport_vendor" id="transport_vendor" value="@if($id>0){{$trans_detail[0]->transport_vendor}}@else{{$vendor_id}} @endif" />
								
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                 					 <input type="text" class="form-control" name="transport_name" id="transport_name" value="@if($id>0){{$trans_detail[0]->transport_name}}@endif" required="required">
                                        </div>
										
										<div class="form-group">	
                                            <label for="exampleInputEmail1">Description</label>
									<textarea  class="form-control" name="transport_desc" id="transport_desc" required="required">@if($id>0){{$trans_detail[0]->transport_desc}}@endif</textarea>																
                                        </div>
										
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Contact Landline</label>
											
											  <input type="text" class="form-control" name="transport_landline" number="number"  id="transport_landline" value="@if($id>0){{$trans_detail[0]->transport_landline}}@endif" required="required" >															
                                        </div>
										
                                       <div class="form-group">
                                            <label for="exampleInputEmail1">Contact Mob</label>
                                           <input type="text" class="form-control" name="transport_contact" number="number" id="transport_contact" value="@if($id>0){{$trans_detail[0]->transport_contact}}@endif" required="required" maxlength="10" >
										    <div id="error_msg" style="color:#FF0000; display:none;"></div>
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Commission in %</label>
                                          <input type="text" class="form-control" number="number" value="@if($id>0){{$trans_detail[0]->transport_commission}}@endif"  name="transport_commission" id="transport_commission"  required="required"/> 
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                         <input type="email" class="form-control"  id="transport_email" name="transport_email"  value="@if($id>0){{$trans_detail[0]->transport_email}}@endif" required="required" >
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address Line1</label>
                                           <input type="text" class="form-control" name="transport_address" id="transport_address" value="@if($id>0){{$trans_detail[0]->transport_address}}@endif" required="required">
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address Line2</label>
                                           <input type="text" class="form-control" name="transport_address2" id="transport_address2" value="@if($id>0){{$trans_detail[0]->transport_address2}}@endif">
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Suburb</label>
                                            <input type="text" class="form-control ui-autocomplete-input" name="transport_suburb" id="transport_suburb" required="required" value="@if($id>0){{$trans_detail[0]->transport_suburb}}@endif"  autocomplete="off">																
                                        </div>
					
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Postcode</label>
                                            <input type="text" class="form-control" name="transport_pcode" id="transport_pcode" required="required" value="@if($id>0){{$trans_detail[0]->transport_pcode}}@endif" >																
                                        </div>
										
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">City</label>
                                            <input type="text" class="form-control" name="transport_city" id="transport_city" required="required" value="@if($id>0){{$trans_detail[0]->transport_city}}@endif">																
                                        </div>
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">State</label>
                                            <input type="text" name="transport_state" id="transport_state"  class="form-control" required="required" value="@if($id>0){{$trans_detail[0]->transport_state}}@endif">																
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Status</label>															
											<select name="transport_status" id="transport_status" class="form-control">
											<option value="PUBLISHED" @if(($id>0)&& ($trans_detail[0]->transport_status=='PUBLISHED')) selected="selected" @endif>PUBLISHED</option>
											<option value="ACTIVE" @if(($id>0)&& ($trans_detail[0]->transport_status=='ACTIVE')) selected="selected" @endif>ACTIVE</option>
											<option value="INACTIVE" @if(($id>0)&& ($trans_detail[0]->transport_status=='INACTIVE')) selected="selected" @endif>INACTIVE</option>
											</select>
                                        </div>
										
										
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Classification</label>
											
																			
											<select name="transport_classi" id="transport_classi" class="form-control">
											
											<option value="1" @if(($id>0)&& ($trans_detail[0]->transport_classi=='1')) selected="selected" @endif>Sponsored</option>
											<option value="2" @if(($id>0)&& ($trans_detail[0]->transport_classi=='2')) selected="selected" @endif>Popular</option>
											<option value="3" @if(($id>0)&& ($trans_detail[0]->transport_classi=='3')) selected="selected" @endif>Special</option>
											<option value="4" @if(($id>0)&& ($trans_detail[0]->transport_classi=='4')) selected="selected" @endif>New</option>
											<option value="5" @if(($id>0)&& ($trans_detail[0]->transport_classi=='5')) selected="selected" @endif>Standard</option>
											
											</select>
											
																										
                                        </div>
										
										
                                        
										
                                        
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Upload Logo (File Type: jpeg,gif,png)</label>
                                            <input type="file" class="form-control" name="transport_logo"                      id="transport_logo">		
											
											@if(($id>0) && (!empty($trans_detail[0]->transport_logo)))
											
											<img src="{{ url('/') }}/uploads/transport/{{ $trans_detail[0]->	transport_logo }}" width="50px;" height="50px;">
											@endif														
                                        </div>
										
										
										<div class="form-group">
                                            <label for="exampleInputEmail1">Open Hours</label>
											<div class="col-md-12" style="border:1px solid #0099FF; " >
												<div  class="col-md-12" >
													<div class="col-md-3"><label>Day Name</label></div>
													<div class="col-md-3"><label>From</label></div>
													<div class="col-md-3"><label>To</label></div>
													<div class="col-md-3"><label>Closed</label></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Monday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
											
                                                <input type="text" class="form-control timepicker" id="mon_from" name="mon_from" value="@if($id>0){{$mon_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="mon_to" name="mon_to" value="@if($id>0){{$mon_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="mon" @if(($id>0) && ( in_array('mon',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Tuesday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="tues_from" name="tues_from" value="@if($id>0){{$tues_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="tues_to" name="tues_to" value="@if($id>0){{$tues_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>													
													</div>
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="tue" @if(($id>0) && ( in_array('tue',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Wednesday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="wed_from" name="wed_from" value="@if($id>0){{$wed_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>												
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="wed_to" name="wed_to" value="@if($id>0){{$wed_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>												
													</div>
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="wed" @if(($id>0) && ( in_array('wed',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Thusday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="thu_from" name="thu_from" value="@if($id>0){{$thu_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="thu_to" name="thu_to" value="@if($id>0){{$thu_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="thu" @if(($id>0) && ( in_array('thu',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Friday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="fri_from" name="fri_from" value="@if($id>0){{$fri_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="fri_to" name="fri_to" value="@if($id>0){{$fri_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>													
													</div>													
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="fri" @if(($id>0) && ( in_array('fri',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif/></div>
												</div>
												<div class="col-md-12">
													<div class="col-md-3"><b>Saturday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="sat_from" name="sat_from" value="@if($id>0){{$sat_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="sat_to" name="sat_to" value="@if($id>0){{$sat_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>													
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="sat" @if(($id>0) && ( in_array('sat',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
												<div class="col-md-12">
												
													<div class="col-md-3"><b>Sunday</b></div>
													<div class="col-md-3">
									<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="sun_from" name="sun_from" value="@if($id>0){{$sun_from}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3">
													<div class="bootstrap-timepicker">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control timepicker" id="sun_to" name="sun_to" value="@if($id>0){{$sun_to}}@endif"/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                    </div>
													
													</div>
													<div class="col-md-3"><input type="checkbox" name="close_day[]" id="close_day" value="sun" @if(($id>0) && ( in_array('sun',(explode(',',$trans_detail[0]->transport_close)) )))checked="checked"@endif /></div>
												</div>
											</div>												
                                        </div>
										
										
										<div class="form-group">
										&nbsp;
										</div>
										
										
										
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
										<input type="button" class="btn btn-primary" value="Submit" onclick="check_frm()" />
									<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
									 
									 	
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="{{ url('/') }}/design/admin/js/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
		
		
        <script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		 <script type="text/javascript">
		 
		  $(".timepicker").timepicker({
                    showInputs: false,
           });
		  
		
$(function() {
 
	 var date = new Date();
	date.setDate(date.getDate()); 
	$("#startdate").datepicker({
			 dateFormat: "yy-mm-dd",
			 onSelect: function(selected) {					 
			  var date1 = $('#startdate').datepicker('getDate', '+1d'); 
				  date1.setDate(date1.getDate()+1);  
			 
			  var date2 = $('#startdate').datepicker('getDate', '+1d'); 
				  date2.setDate(date2.getDate()+30); 
		
			   $("#enddate").datepicker("option","minDate", date1);
			   $("#enddate").datepicker("option","maxDate", date2);
			 },
			minDate: 0,
			autoclose: true,
	
	});
	$("#enddate").datepicker({
				 
		   dateFormat: "yy-mm-dd",
		   onSelect: function(selected) {
		   $("#startdate").datepicker("option","maxDate", selected)
		},
		 minDate: 0 ,
		 autoclose: true, 
	});  
	 
 

});
	
$(document).on('keyup', '#transport_suburb', function(){
	 $( "#transport_suburb" ).autocomplete({
    	 source: "{{url('/admin/get_suburblist') }}",
		 select: function(event, ui) {
		 
		 	$("#transport_suburb").val(ui.item.value);	
			$("#transport_pcode").val(ui.item.pincode);	
			$("#transport_city").val(ui.item.city);	
			$("#transport_state").val(ui.item.state);	
		 }
    });

});		


		
		function check_frm()
		{
			
					$('#error_msg').hide();

			var start_date = $('#startdate').val();
			var end_date =  $('#enddate').val();

			if(start_date!='')			
			{
				$("#enddate").prop('required',true);
			}
		
			var delivery=0;
			var start = $("#delivery_from").val();//"01:00 PM";
			var end = $("#delivery_to").val();//"11:00 AM";
			var dtStart = new Date("1/1/2011 " + start);
			var dtEnd = new Date("1/1/2011 " + end);
			var difference_in_milliseconds = dtEnd - dtStart;
			//alert(difference_in_milliseconds);
			if (difference_in_milliseconds <= 0)
			{
				 delivery=0;
				$("#time_error").show();
				return false;
			}
			else
			{
				
				$("#time_error").hide();
				 delivery=1;
				 
			}
			

			
			var form = $("#rest_frm");
				form.validate();
				
			var valid =	form.valid();
			
			var regExp = /^0[0-9].*$/;
			
			
			if(valid)
			{	
				/*alert(regExp.test("0001"));*/
				if((regExp.test($("#transport_landline").val())) && (regExp.test($("#transport_contact").val())) && ( delivery==1))
				{
					$(form).submit();
					return true;
				}
				else
				{
					//alert('Land line and Mobile number start with 0.');
					
					$('#error_msg').html('Land line and Mobile number start with 0.');
					$('#error_msg').show();
					return false;
				}
					
			}
			else
			{
				return false;
			}		
		}
		
		</script>
@stop