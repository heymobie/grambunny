@extends('layouts.admin')

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Content Form
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Content Form</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Content Form</h3>
                                </div><!-- /.box-header -->
								
					 @if(Session::has('error'))
					 <div class="alert alert-danger alert-dismissable">				
								{{Session::get('error')}}
								</div>
							@endif 
                                <!-- form start -->
								
								<form  role="form" method="POST" id="cuisine_frm" action="{{ url('/admin/homepage_action') }}">    
								<input type="hidden" name="home_id" value="{{$id}}" />
								
								{!! csrf_field() !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Title</label>
                                           <input type="text" class="form-control" id="home_title" name="home_title"  value="@if($id>0){{$page_detail[0]->home_title}}@endif" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">English Content</label>
											<textarea name="home_content" id="home_content" class="form-control"> @if($id>0){{$page_detail[0]->home_content}}@endif</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="home_content_ar">Arabic Content</label>
											<textarea name="home_content_ar" id="home_content_ar" class="form-control"> @if($id>0){{$page_detail[0]->home_content_ar}}@endif</textarea>
                                        </div>
                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		
										<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />
									
									 
									 	<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />
                                    </div>
                                </form>
								
                            </div><!-- /.box -->


                        </div>
	
	
	</section><!-- /.content -->
</aside><!-- /.right-side -->



@endsection



@section('js_bottom')

<!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>		
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
		     
        <!-- CK Editor -->
		
		
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>



@stop