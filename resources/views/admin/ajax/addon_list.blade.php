<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{{$rest_name}}->{{$menu_name}}->{{$item_name}}->Addon Listing</h4>
</div>

<div class="box">
<div class="box-header">
	<div class="col-sm-12" >
		<a  href="javascript:void(0)" data-rest="{{$rest_id}}" data-menu="{{$menu_id}}" data-item="{{$menu_item}}"  class="btn btn-primary" style="color:#FFFFFF" id="add_addons"> Add Addons</a>
	</div>
	
</div><!-- /.box-header -->
<div class="box-body table-responsive">
	@if(Session::has('addon_message'))
	
	<div class="alert alert-success alert-dismissable">
	  <i class="fa fa-check"></i>
	   <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				   {{Session::get('addon_message')}}
	</div>
	@endif 
	
	<table id="example2" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>SrNo</th>
				<th>Group Name</th>
				<th>Selection Option</th>
				<th>Name</th>
				<th>Price</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		@if($addon_list)<?php $c=1;?>
			@foreach($addon_list as $list )
			<tr>
				<td>{{$c++}}</td>
				<td>{{$list->addon_groupname}}</td>
				<td>@if($list->addon_option=='check') 
						Tick Box
					@else 
						Radio Button
					@endif
				</td>
				<td>{{$list->addon_name}}</td>
				<td>{{$list->addon_price}}</td>
				<td>
					@if($list->addon_status==1) 
						<span class="label label-success">Active</span>
					@else 
						<span class="label label-danger">Inactive</span>
					@endif
				</td>
				<td><a href="javascript:void(0)" data-rest="{{$rest_id}}" data-menu="{{$menu_id}}" data-item="{{$menu_item}}" data-addon="{{$list->addon_id}}" id="addon_update_data-{{$list->addon_id}}"><i class="fa fa-edit"></i></a></td>
			</tr>
		  @endforeach
		@endif  		                                     
		</tbody>
		<tfoot>
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>
		</tfoot>
	</table>
</div><!-- /.box-body -->
</div><!-- /.box -->
