<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Mode</th>
			<th>Restaurant Name</th>
			<!--<th>Restaurant</th>-->
			<th>Status</th>
			<!--<th>Cust_Submit_date</th>-->
			<th>Last_Status_Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	@if(!empty($order_detail)) 
	 @foreach ($order_detail as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>Paypal</td>
			<td><a href="{{url('admin/restaurant_view/')}}/{{$list->rest_id}}">{{ $list->rest_name}}</a>
			</td>
			<td>		
				@if($list->pay_status=='1') Payment Done @endif
				@if($list->pay_status=='2') Partial Done @endif
				@if($list->pay_status=='3') Payment Not Done @endif
				@if($list->pay_status=='4') Payment Refund @endif
						
			</td>
			<td>{{ $list->pay_update }}</td>
		 <td> 
			  <a title="Update" href="{{url('admin/payment_view?order='.$list->pay_id)}}" ><i class="fa fa-edit"></i></a>
			 
				 
			</td>
		</tr> 
	
	<?php $i++; ?>
	@endforeach	   
	@endif                                     
	</tbody>
</table>	
 <script type="text/javascript">			
	  $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });												
	</script>		