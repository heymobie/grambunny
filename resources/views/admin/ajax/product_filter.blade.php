<div class="row">
<div class="my-panel-data">
<div class="col-xs-12">
<div class="box">
<div class="box-body table-responsive">

@if(isset($order_id_filter))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$order_id_filter}}/order_id_export">Export</a></button></div>
@endif
@if(isset($customer_name))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$customer_name}}/order_customer_export">Export</a></button></div>
@endif
@if(isset($merchant))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$merchant}}/order_merchant_export">Export</a></button></div>
@endif
@if(isset($order_status_serach))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$order_status_serach}}/order_status_export">Export</a></button></div>
@endif
@if(isset($order_date))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$order_date}}/order_date_export">Export</a></button></div>
@endif
@if(isset($coupon))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/product_export_in_excel_filter/{{$coupon}}/order_coupon_export">Export</a></button></div>
@endif

<div id="">							
<table id="example2" class="table table-bordered table-hover">
<thead>
<tr>
<th>Order ID</th>
<th>UPC/UID</th>
<th>Merchant</th>
<th>First Name</th>
<th>Last Name</th>
<th>Status</th>
<th>Product Name</th>
<th>Qty</th>
<th>List Price</th>
<th>Discount</th>
<th>Price</th>
<!-- <th>Total</th> -->
<th>Order Date</th>
</tr>
</thead>
<tbody id="myTable">										
<?php 

//print_r($order_detail); die;

foreach ($order_detail as $key => $value) { 
$userinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
$userinfo1 = DB::table('users')->where('id','=',$value->user_id)->first();
$timestamp = strtotime($value->orderdate);
$dataPoints1 = array(
array("label"=>date("d", $timestamp) , "y"=> $value->total),
);

 $orderid = DB::table('orders')->where('order_id','=',$value->order_id)->value('id');

 $productinfo = DB::table('orders_ps')->where('order_id','=',$orderid)->get();

 $dps_id = DB::table('orders_ps')->where('order_id','=',$orderid)->value('ps_id');

$product_code = DB::table('product_service')->where('id','=',$dps_id)->value('product_code'); 

      if(isset($value->coupon_id)){

      $coupon_id = $value->coupon_id;

      $dtype = DB::table('coupon_code')->where('id','=',$coupon_id)->value('discount');

      $camount = DB::table('coupon_code')->where('id','=',$coupon_id)->value('amount');

      $totalitem = DB::table('orders_ps')->where('order_id','=',$value->id)->sum('ps_qty');

      $capply = 1;   

      }else{ $capply = 0; } 

      //print_r($productinfo);
      

foreach ($productinfo as $key => $pdata) {

       $totalprice = 0;	

       if($capply==1){

      $itemprice = $pdata->ps_qty*$pdata->price;

	if($dtype==1){ //Percentage

	$itemdiscount = ($itemprice*$camount)/100;	

       $totalprice = $itemprice-$itemdiscount; 

       }else{ 

        $per_item = $camount/$totalitem;

        $itemdiscount = $pdata->ps_qty*$per_item;
 
        $totalprice = $itemprice-$itemdiscount;

       }

      }else{

       $itemdiscount = 0;     

       $totalprice = $pdata->ps_qty*$pdata->price;

      } 		

?>

<tr>
<td>{{$value->order_id}}</td>
<td>{{$product_code}}</td>
<!-- <td>{{$value->user_id}}</td> -->
<td>{{$userinfo->name}} <!-- {{$userinfo->last_name}} --></td>
<td>{{$userinfo1->name}}</td>
<td>{{$userinfo1->lname}}</td>
<td>							
@if($value->status==0) Pending @endif
@if($value->status==1) Accept @endif
@if($value->status==2) Cancelled @endif
@if($value->status==3) On the way @endif
@if($value->status==4) Delivered @endif
@if($value->status==5) Requested for return @endif
@if($value->status==6) Return request accepted @endif
@if($value->status==7) Return request declined @endif
</td>

<td>{{$pdata->name}}</td>
<td>{{ $pdata->ps_qty }}</td>
<td>{{ $pdata->price }}</td>
<td>${{ number_format($itemdiscount, 2) }}</td>
<td>${{ number_format($totalprice, 2) }}</td>
<!-- <td>$<?php echo $pdata->ps_qty*$pdata->price; ?></td> -->

<?php $order_date = date("Y-m-d g:iA", strtotime($value->orderdate)); ?>
<td>{{$order_date}}</td>

</tr> 

<?php }} ?>

</tbody>
<tfoot>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</div>	
</div>
</div>
</div>
</div>

<div class="col-12 mt-5 text-center">
<div class="custom-pagination">

</div>
</div>

