<table id="example2" class="table table-bordered table-hover">
<thead>
	<tr>
		<th>Sr. No. </th>
		<th>Order No.</th>
		<th>Order Date  </th>
		<th>Scheduled Order  </th>
		<th>User Name</th>
		<th>Restaurant Name</th>
		<th>Status</th>
		<!-- <th>Device Type</th>
		<th>Device Name</th>
		<th>Device OS</th> -->
		<th>Action</th>
	</tr>
</thead>
<tbody>	

<?php if($orderstatus!=11 && $orderstatus!=12 && $orderstatus!=13 && $orderstatus!=14 && $orderstatus!=15 && $orderstatus!=16 ){ ?>

<?php $i=1; ?>
@if(!empty($order_detail)) 
 @foreach ($order_detail as $list)
	<tr>
		<td>{{ $i }}</td>
		<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
		<!-- <td>{{ $list->created_at}}</td> -->
		<td>{{ $list->order_create}}</td>
		<td>{{ $list->order_pickdate}} {{ $list->order_picktime}}</td>		
		<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
		<td>{{ $list->rest_name}}</td>
		<td>	
		<?php 
		
		$orderid = $list->order_id;
		$dmorder_status = '';
		$dmar_status = '';

		$dmstatus = DB::table('delivery_man_orders')
        ->where('delivery_man_orders.order_id', '=', $orderid)
        ->select('*')
        ->get();

        if(!empty($dmstatus)){

        $dmorder_status = $dmstatus[0]->order_status;
        $dmar_status = $dmstatus[0]->accept_reject_status;

        }

		?>														
														
			     <?php if($list->order_status=='1'){ ?>

				 <span style="display: block;" class="label label-info deepinfo">New Order</span>


				 <?php } ?>
				

				 <?php if($list->order_status=='4'){ ?>

					<span style="display: block;" class="label label-primary deepinfo">Confirmed Order</span>

				 <?php } ?>


				 <?php 

				 if($dmorder_status=='0' && $dmar_status=='0'){ ?>

				 	<span style="display: block;" class="label label-info deepinfo">Order Pending</span>

				<?php }else if($dmorder_status=='0' && $dmar_status=='1'){ ?>

				 	<span style="display: block;" class="label label-primary deepinfo">Order Accepted</span>

				<?php }else if($dmorder_status=='0' && $dmar_status=='2'){ ?>

				 	<span style="display: block;" class="label label-danger deepinfo">Order Rejected</span>

				<?php }else if($dmorder_status=='1'){ ?>

				 	<span style="display: block;" class="label label-info deepinfo">Order Picked</span>

				<?php }else if($dmorder_status=='2'){ ?>

				 	<span style="display: block;" class="label label-primary deepinfo">Order On The Way</span>

				<?php }else if($dmorder_status=='3'){ ?>

				 	<span style="display: block;" class="label label-success deepinfo">Order Delivered</span>

				<?php }else if($list->order_status=='5'){ ?>

				 <span style="display: block;" class="label label-success deepinfo">Order Ready</span>

				 <?php } ?>

				 <?php if($list->order_status=='6'){ ?>

					<span style="display: block;" class="label label-danger deepinfo">Cancelled</span>

                 <?php } ?>		
		</td>

<!-- <td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td> -->

<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
			 
		<!-- <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#order-{{$list->order_id}}"><i class="fa fa-eye"></i></a>-->
		 
			 
		</td>
	</tr> 
										
<?php $i++; ?>
@endforeach	 
@else
	No record Found!  
@endif 

<?php }else{ ?>

<?php $i=1; ?>
@if(!empty($order_detail)) 
@foreach ($order_detail as $list)

<?php 

$orderid = $list->order_id;
$dmorder_status = '';
$dmar_status = '';

$dmstatus = DB::table('delivery_man_orders')
->where('delivery_man_orders.order_id', '=', $orderid)
->select('*')
->get();

if(!empty($dmstatus)){

$dmorder_status = $dmstatus[0]->order_status;
$dmar_status = $dmstatus[0]->accept_reject_status;

}

?>	

<?php if($dmorder_status=='0' && $dmar_status=='0' && $orderstatus=='11'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td>	
<span style="display: block;" class="label label-info deepinfo">Order Pending</span>
</td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
</td>
</tr> 

<?php }else if($dmorder_status=='0' && $dmar_status=='1' && $orderstatus=='12'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td>
<span style="display: block;" class="label label-primary deepinfo">Order Accepted</span>
</td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
</td>
</tr> 

<?php }else if($dmorder_status=='0' && $dmar_status=='2' && $orderstatus=='13'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td>
<span style="display: block;" class="label label-danger deepinfo">Order Rejected</span>
</td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>	</td>
</tr> 

<?php }else if($dmorder_status=='1' && $orderstatus=='14'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td>
<span style="display: block;" class="label label-info deepinfo">Order Picked</span>
</td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>	</td>
</tr> 

<?php }else if($dmorder_status=='2' && $orderstatus=='15'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td>
<span style="display: block;" class="label label-primary deepinfo">Order On The Way</span>
</td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>	</td>
</tr> 

<?php }else if($dmorder_status=='3' && $orderstatus=='16'){ ?>

<tr>
<td>{{ $i }}</td>
<td>{{ $list->order_id}} ({{$list->order_uniqueid}})</td>
<td>{{ $list->created_at}}</td>																
<td><?php if(!empty($list->name)){echo $list->name.' '.$list->lname;}else{ echo 'Guest'; }?></td>
<td>{{ $list->rest_name}}</td>
<td><span style="display: block;" class="label label-success deepinfo">Order Delivered</span></td>
<td>{{ $list->order_device}}</td>
<td>{{ $list->order_devicename}}</td>
<td>{{$list->order_device_os}}</td>
<td> 
<a title="Update" href="{{url('admin/order_view?order='.$list->order_id)}}" ><i class="fa fa-edit"></i></a>
</td>
</tr> 

<?php } ?>
		

<?php $i++; ?>
@endforeach	 
@else
	No record Found!  
@endif 

<?php } ?>

</tbody>
</table>	

<script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>