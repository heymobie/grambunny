<div class="row">
<div class="my-panel-data">
<div class="col-xs-12">
<div class="box">
<div class="box-body table-responsive">

@if(isset($order_id_filter))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$order_id_filter}}/order_id_export">Export</a></button></div>

<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$order_id_filter}}/order_id_export">METRC Flat File</a></button></div>

@endif
@if(isset($customer_name))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$customer_name}}/order_customer_export">Export</a></button></div>

<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$customer_name}}/order_customer_export">METRC Flat File</a></button></div>
@endif
@if(isset($merchant))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$merchant}}/order_merchant_export">Export</a></button></div>

<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$merchant}}/order_merchant_export">METRC Flat File</a></button></div>
@endif
@if(isset($order_status_serach))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$order_status_serach}}/order_status_export">Export</a></button></div>

<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$order_status_serach}}/order_status_export">METRC Flat File</a></button></div>
@endif
@if(isset($order_date))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$order_date}}/order_date_export">Export</a></button></div>
@endif
@if(isset($coupon))
<div style="float:right;margin-bottom: 10px;"><button><a href="/admin/export_in_excel_filter/{{$coupon}}/order_coupon_export">Export</a></button></div>

<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$coupon}}/order_coupon_export">METRC Flat File</a></button></div>
@endif

@if(isset($order_date))
<div style="float:right;margin-bottom: 10px; margin-right: 10px;"><button> <a href="{{ url('/admin/METRC_in_excel_filter')}}/{{$order_date}}/order_date_export">METRC Flat File</a></button></div>
@endif

<div id="">							
<table id="example2" class="table table-bordered table-hover">
<thead>
<tr>
<th>Order ID#</th>
<!-- <th>Customer ID#</th> -->
<th>Merchant</th>
<th>Order By</th>
<th>Order Price</th>
<th>Order Status</th>
<th>Placed at</th>
<th>Coupon Code</th>
<th>Action</th>
</tr>
</thead>
<tbody id="myTable">										
<?php 

foreach ($order_detail as $key => $value) { 
$userinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
$userinfo1 = DB::table('users')->where('id','=',$value->user_id)->first();
$timestamp = strtotime($value->orderdate);
$dataPoints1 = array(array("label"=>date("d", $timestamp) , "y"=> $value->total),);

$orderid = DB::table('orders')->where('order_id','=',$value->order_id)->value('id');
?>

<tr>
<td>{{$value->order_id}}</td>

<!-- <td>{{$value->user_id}}</td> -->
<td>@if((!empty($userinfo->name)) && (!empty($userinfo->last_name))){{$userinfo->name}} {{$userinfo->last_name}}@endif</td>
<td>@if((!empty($userinfo1->name)) && (!empty($userinfo1->lname))){{$userinfo1->name}} {{$userinfo1->lname}}@endif</td>

<td>${{$value->total}}</td>
<td>							
@if($value->status==0) Pending @endif
@if($value->status==1) Accept @endif
@if($value->status==2) Cancelled @endif
@if($value->status==3) On the way @endif
@if($value->status==4) Delivered @endif
@if($value->status==5) Requested for return @endif
@if($value->status==6) Return request accepted @endif
@if($value->status==7) Return request declined @endif
</td>
<?php $order_date = date("Y-m-d g:iA", strtotime($value->orderdate)); ?>
<td>{{$order_date}}</td>
<td><?php $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->first(); 
echo @$coupon_code->coupon;
?></td>
<td>
<button><a href="{{ url('admin/order-details').'/'.$orderid}}">View</a></button>

<!-- <button> <a href="{{ url('/admin/mertc_flat_file').'/'.$value->vendor_id.'/'.$value->user_id}}/{{$value->id}}?var=<?php echo rand();?>">METRC Flat File</a></button> -->

</td>

</tr> 

<?php } ?>

</tbody>
<tfoot>
<tr>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</div>	
</div>
</div>
</div>
</div>

<div class="col-12 mt-5 text-center">
<div class="custom-pagination">

</div>
</div>

