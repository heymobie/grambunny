				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">
										
                                        <br/>
										<div id="">							
										<table id="example2" class="table table-bordered table-hover">
											<thead>
												<tr>
												<th>Order ID#</th>
												<!-- <th>Customer ID#</th> -->
												<th>Merchant</th>
												<th>Order By</th>
												<th>Order Price</th>
												<th>Order Status</th>
												<th>Placed at</th>
												<th>Coupon Code</th>
												<th>Action</th>
												</tr>
											</thead>
											<tbody id="myTable">										
											<?php 
											
											foreach ($order_detail as $key => $value) { 
											$userinfo = DB::table('vendor')->where('vendor_id','=',$value->vendor_id)->first();
											$userinfo1 = DB::table('users')->where('id','=',$value->user_id)->first();
												$timestamp = strtotime($value->orderdate);
												$dataPoints1 = array(
													array("label"=>date("d", $timestamp) , "y"=> $value->total),
												);
												//print_r($dataPoints1);
											?>

												<tr>
												<td>{{$value->order_id}}</td>
												
												<!-- <td>{{$value->user_id}}</td> -->
												<td>{{$userinfo->name}} {{$userinfo->last_name}}</td>
												<td>{{$userinfo1->name}} {{$userinfo1->lname}}</td>
												
												<td>${{$value->total}}</td>
												<td>							
												@if($value->status==0) Pending @endif
												@if($value->status==1) Accept @endif
												@if($value->status==2) Cancelled @endif
												@if($value->status==3) On the way @endif
												@if($value->status==4) Delivered @endif
												@if($value->status==5) Requested for return @endif
												@if($value->status==6) Return request accepted @endif
												@if($value->status==7) Return request declined @endif
												</td>

											<?php $createdate = date("Y-m-d g:iA", strtotime($value->orderdate)); ?>

												<td>{{$createdate}}</td>

												<td><?php $coupon_code = DB::table('coupon_code')->where('id','=',$value->coupon_id)->first(); 
													echo @$coupon_code->coupon;
													?></td>
												<td>
												    <a href="{{ url('admin/order-details').'/'.$value->id}}">View</a>
												</td>

												</tr> 

											<?php } ?>
											                                   
											</tbody>
											<tfoot>
												<tr>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												</tr>
											</tfoot>
											</table>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>

				<div class="col-12 mt-5 text-center">
	              <div class="custom-pagination">
		            @if($order_data!='all')  
		            @if(!empty($order_data))
                    {{ $order_detail->appends(['order_data' => $order_data])->links() }}
 					@else
                    {{ $order_detail->links() }}
                    @endif
                    @endif
	              </div>
	            </div>
	            
     