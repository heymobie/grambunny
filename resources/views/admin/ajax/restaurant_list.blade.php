<table id="example2" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>SrNo</th>
			<th>Logo</th>
			<th>Name</th>
			<th>Vendor Name</th>
			<th>Address</th>
			<th>Staus</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>										
	<?php $i=1; ?>
	@if(!empty($rest_list)) 
	 @foreach ($rest_list as $list)
		<tr>
			<td>{{ $i }}</td>
			<td>
		@if(!empty($list->rest_logo))	
	<img src="{{ url('/') }}/uploads/reataurant/{{ $list->rest_logo }}" width="50px;" height="50px;">
		@else
			<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >
		@endif				
		</td>
			<td>{{ $list->rest_name}}</td>
			<td>{{ $list->name }}</td>
			<td>{{ $list->rest_address }}</td>
			<td>
			@if($list->rest_status=='PUBLISHED') 
			<span class="label label-primary">PUBLISHED</span>
			 @elseif($list->rest_status=='ACTIVE') 
			
			<span class="label label-danger">{{$list->rest_status}}</span>
			 @elseif($list->rest_status=='INACTIVE') 
			
			<span class="label label-danger">{{$list->rest_status}}</span>
			@endif
			</td>
			 <td>
			   <?php if($list->google_type==0){?>
			 <a title="Edit" href="{{url('admin/restaurant-form/')}}/{{ $list->rest_id }}"><i class="fa fa-edit"></i></a>  
			 <a title="View" href="{{url('admin/restaurant_view/')}}/{{ $list->rest_id }}"><i class="fa fa-eye"></i></a>
			 	 <?php }?>
				 
			</td>
		</tr> 										
	<?php $i++; ?>
	@endforeach	   
	@endif                                     
	</tbody>
	<tfoot>
		<tr>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</tfoot>
</table>

 <script type="text/javascript">
$(function() {
	$("#example1").dataTable();
	$('#example2').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false
	});
});
</script>