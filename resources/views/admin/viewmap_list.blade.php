<?php 
use App\Restaurant; 
use App\Review;
$lat= $latitude;
$long = $longitude;
?>
@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Google Data List
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Google Data List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Google Data which store from google</h3>
									<div style="float:right; margin-right:10px; margin-top:10px;">
									<a href="{{url('admin/view_google_form')}}" class="btn btn-primary" style="color:#FFFFFF" > Back</a>
									</div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
			
							
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
												<td>Sr</td>
												<td>Name</td>
												<td>Address</td>
												<td>location</td>
												<td>postal code</td>
												<td>lat</td>
												<td>lang</td>
												<td>ratting</td>
											</tr>
                                        </thead>
                                        <tbody>			
								
									<?php 
			 $c=0;		
foreach($cuisine as $clist)
{
	$keyword = $clist->cuisine_name;
	$key_id = $clist->cuisine_id;
	$radius = $distance_range;
echo $url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$long&radius=$radius&type=restaurant&keyword=$keyword&key=AIzaSyD9jNDXA5_KJPANLMNi99ADymC8bfsVjGg";



      $json=file_get_contents($url);
      $data1=json_decode($json,true);
	/*  echo '<pre>';
	  print_r($data1);*/
	    if($data1['status']=='OK'){
		  
		  		if(!empty($data1['results']))
				{	
		   
				   foreach($data1['results'] as $result)
				   {
				   $c++;
				   	$rest_logo='';	
				  // print_r($result );
				  $rest_rating='';
						
					   $rest_lat=$result['geometry']['location']['lat'];
					   $rest_lang=$result['geometry']['location']['lng'];
					   $rest_name=$result['name'];
					   
					   if(isset($result['rating'])){
					   	$rest_rating=$result['rating'];
					  	}
					   if(isset($result['photos'])){					   
					   		//$rest_logo=$result['photos'][0]['photo_reference'];
							
							$photo_reference = $result['photos'][0]['photo_reference'];
						 $url_image = 		   
"https://maps.googleapis.com/maps/api/place/photo?maxwidth=135&photoreference=$photo_reference&key=AIzaSyD9jNDXA5_KJPANLMNi99ADymC8bfsVjGg";

						$rest_logo = $url_image ;
						
						
					   }
					   $rest_vicinity = $result['vicinity'];
					   
		$price_level = 0;
		if(isset($result['price_level'])){
			$price_level = @$result['price_level'];
		}
		
					    $url_place = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$result['place_id'].'&key=AIzaSyD9jNDXA5_KJPANLMNi99ADymC8bfsVjGg';
					 // echo '<br>';
$json_place=file_get_contents($url_place);
      $data1_place=json_decode($json_place,true);
	  
	
	 // echo '<pre>';
	//  print_r($data1);
	$loca = '';
	  	$postcode = '';
	if($data1_place['result'])	
	{	
	 foreach($data1_place['result']['address_components'] as $add)
	  {
	  	
	  		if($add['types'][0] == 'locality')
			{				
	  			 $loca = $add['long_name'];
			}
	  		if($add['types'][0] == 'postal_code')
			{
				 $postcode = $add['long_name'];
			}
	  	//echo $add['types'][0];
		
	  }
	  
	 } 
	  
					    $total_rec  = DB::table('restaurant')->where('rest_name', '=' ,$rest_name)->count();	
		// echo $rest_name.'---'.$total_rec.'<br>' ;
						if($total_rec==0)
						{
								$rest = new Restaurant;
				
								$rest->rest_name = $rest_name;
								$rest->rest_address = $rest_vicinity;
								$rest->rest_city = $loca;
								$rest->rest_suburb = $loca;
								$rest->rest_zip_code = $postcode;
								$rest->rest_cuisine = $key_id;
								$rest->rest_logo = $rest_logo;
								$rest->rest_status ='PUBLISHED';
								$rest->google_type ='1';	
								$rest->rest_price_level = $price_level;		
								$rest->rest_lat =  $rest_lat;
								$rest->rest_long = $rest_lang;				
				
								$rest->save();
								
						
						 $rest_id = $rest->rest_id;	
							
							if(!empty($rest_id)){
							
							DB::enableQueryLog(); 	
							  $range =$rest_rating;
							  if($range>0)
							  {
									$range=$rest_rating;
								if($range<=5){
									$range=($rest_rating*2);
								}
							  }
							  else
							  {
								$range = 0;
							  }
								$order_re = new Review;
								$order_re->re_orderid = 0;
								$order_re->re_userid = 0;
								$order_re->re_content = '';
								$order_re->re_restid = $rest_id;
								$order_re->re_rating = $range;			
								$order_re->re_status = 'PUBLISHED';
								$order_re->save();
								$re_id = $order_re->re_id;
							//	print_r( DB::getQueryLog());
								
							}
							
							
						}
						
					   
					   ?>
					   					   
<tr>
	<td><?php  echo $c;?></td>
	<td><?php  echo $rest_name;?></td>
	<td><?php  echo $rest_vicinity;?></td>
	<td><?php  echo $loca;?></td>
	<td><?php  echo $postcode;?></td>
	<td><?php  echo $rest_lat;?></td>
	<td><?php  echo $rest_lang;?></td>
	<td><?php  echo $rest_rating;?></td>
</tr>
		
					   
					   <?php
					   
			   
		
					} 
				    
				}
			}
			
	}
?>                                   
										</tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>       
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>       
												<th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

@stop

@section('js_bottom')

        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
@stop
