@extends('layouts.admin')

@section("other_css")

        <!-- DATA TABLES -->

<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

  <style type="text/css">

  #myInput{ float: right; width: 30%;margin-bottom: 10px; }    

  </style> 

@stop

@section('content')

            <!-- Right side column. Contains the navbar and content of the page -->

            <aside class="right-side">

                <!-- Content Header (Page header) -->

                <section class="content-header">

                    <h1>

                        Admin List

                    </h1>

                    <ol class="breadcrumb">

                        <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

                        <li class="active">Admin List</li>

                    </ol>

                </section>

                <!-- Main content -->

                <section class="content">

                    <div class="row">

                        <div class="col-xs-12">

                            <div class="box">

                                <div class="box-header">

                                    <h3 class="box-title">Admin Listing</h3>

                                    <div style="float:right; margin-right:10px; margin-top:10px;">

                                    <a href="{{url('admin/admin-form')}}" class="btn btn-primary" style="color:#FFFFFF" > Add New Admin</a>

                                    </div>

                                </div><!-- /.box-header -->

                                <div class="box-body table-responsive">

                     @if(Session::has('message'))

                     <div class="alert alert-success alert-dismissable">

                          <i class="fa fa-check"></i>

                           <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                                       {{Session::get('message')}}

                     </div>

                    @endif

                            <br>

                                    <table id="myTable11" class="table table-bordered table-hover" data-order='[[ 0, "asc" ]]'>

                                        <thead>

                                            <tr>

                                               <th>ID#</th>

                                                <th>Name</th>

                                                <th>Email</th>

                                                <th>Contact Number</th>

                                                <th>Status</th>

                                                <th>Action</th>

                                            </tr>



                                        </thead>



                                        <tbody id="myTable" >                                        



                                     <?php $i=1; ?>



                                         @foreach ($admin_list as $list)



                                            <tr>



                                                <td>{{$list->id}}</td>


                                                <td>{{ $list->name.' '.$list->lname}}</td>


                                                <td>{{ $list->email }}</td>


                                                <td>{{ $list->mobile_no }}</td>
                                               
                                                
                                                <td>@if($list->status==1)

                                                  <span class="label label-success">Active</span>

                                                  @else

                                                  <span class="label label-danger">Inactive</span>@endif</td>

                                                 
                                                <td>

                                           <a href="{{url('admin/admin-form/')}}/{{ $list->id }}"><i class="fa fa-edit"></i></a>  



                                            <a title="Delete User" href="{{url('admin/admin_delete')}}/{{ $list->id }}"onclick="return delete_wal()" style="padding :5px;"><i class="fa fa-trash-o"></i></a>


                                                </td>





                                            </tr>                                       



                                     <?php $i++; ?>



                                    @endforeach                                       



                                        </tbody>



                                       <!--  <tfoot>



                                            <tr>



                                                <th>&nbsp;</th>



                                                <th>&nbsp;</th>



                                                <th>&nbsp;</th>



                                                <th>&nbsp;</th>



                                                <th>&nbsp;</th>



                                                <th>&nbsp;</th>



                                            </tr>



                                        </tfoot> -->



                                    </table>



                                </div><!-- /.box-body -->



                            </div><!-- /.box -->



                        </div>



                    </div>







                </section><!-- /.content -->



            </aside><!-- /.right-side -->







@stop







@section('js_bottom')







        <!-- jQuery 2.0.2 -->



        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->



        <!-- Bootstrap -->



        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>



        <!-- DATA TABES SCRIPT -->



        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>



        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>



        <!-- AdminLTE App -->



        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>



        <!-- page script -->



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

$(document).ready(function(){

  $("#myInput").on("keyup", function() {

    var value = $(this).val().toLowerCase();

    $("#myTable tr").filter(function() {

      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

    });

  });

});

</script>


<!-- 
        <script type="text/javascript">



            $(function() {



                $("#example1").dataTable();



                $('#example2').dataTable({



                    "bPaginate": true,



                    "bLengthChange": false,



                    "bFilter": true,



                    "bSort": true,



                    "bInfo": true,



                    "bAutoWidth": false



                });



            });



        </script> -->

 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>

@stop



