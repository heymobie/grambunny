@extends('layouts.admin')
@section('content')
<meta name="_token" content="{!! csrf_token() !!}"/>
<!-- Right side column. Contains the navbar and content of the page -->
<style>
.divition, .sub-drage
{
	border: 1px solid #666666;
	overflow:hidden;
}
.sub-drage
{
	margin-bottom: 6px;
	background: #cccccc;
	position:relative;
}
.sub-drage a.menu_edit_link {
	left: 4px;
	position: absolute;
	top: 6px;
	color:#000000;
}
.sub-drage p a {
	display: block;
	padding: 6px 0;
	color:#000;
}
.sub-drage.selected
{
	border:1px solid #3c8dbc;
	background: #666666;
	color: #fff !important;
}
.sub-drage.selected p a
{
	color: #fff !important;
}
.sub-drage p
{
	border-left:1px solid #666666;
	margin:0 0 0 20px;
	padding-left: 5px;
}
.drage
{
	overflow:hidden;
}
.col-sm-4.border
{
	border-right: 1px solid #ccc;
}
.col-sm-8.border {
	border-left: 1px solid #ccc;
}
.my-panel-data .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
{
	background: #3c8dbc;
	color: #fff;
}
.my-panel-data .nav-tabs>li
{
	margin: 0;
}
.my-panel-data .nav-tabs>li>a
{
	border-radius: 4px;
}
.my-panel-data .nav-tabs
{
	border:  none;
}
.my-panel-data .panel-heading
{
	padding: 12px;
	background-color: transparent !important;
	border: none;
}
.my-panel-data .panel-body {
	border-top: 2px solid #3c8dbc;
}
.my-panel-data .panel
{
	border: none;
	border-radius: 0;
	box-shadow: none;
}
.check_input_service .icheckbox_minimal.checked.disabled {
	background-position: -40px 0;
}

.deepmenu {
    padding: 20px;
    border: 1px solid #666666;
    margin-bottom: 10px;
}

</style>

<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Restaurant Profile Page
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Restaurant Profile Page</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="col-md-12">
			<!-- general form elements -->
			@if(Session::has('message'))
			<div class="alert alert-success alert-dismissable">
				<i class="fa fa-check"></i>
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				{{Session::get('message')}}
			</div>
			@endif
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Restaurant {{$rest_detail[0]->rest_name }} Profile Page</h3>
				</div><!-- /.box-header -->
				<div class="box-body table-responsive rstrnt-profl">
					<table  width="100%" cellpadding="5" cellspacing="5" style="border:1px solid #666666; width:100%">
		<!--<tr>
			<td align="center"> RATING : 4.5 </td>
		</tr>-->
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="5" border="0">
					<tr>
						<td valign="top" width="25%">
							<table width="100%" cellpadding="2" cellspacing="2">
								<tr><td><strong>Restauant Logo</strong></td></tr>
								<tr><td>
									@if(!empty($rest_detail[0]->rest_logo))
									<img src="{{ url('/') }}/uploads/reataurant/{{ $rest_detail[0]->rest_logo }}" >
									@else
									<img src="{{ url('/') }}/design/front/img/logo.png" width="100px;">
									@endif
								</td></tr>
								<!--<tr><td><strong>Restauant Banner</strong></td></tr>
								<tr><td>
									@if(!empty($rest_detail[0]->rest_banner))
									<img src="{{ url('/') }}/uploads/reataurant/banner/{{ $rest_detail[0]->rest_banner }}" >
									@else
									<img src="{{ url('/') }}/design/front/img/logo.png" width="100px;">
									@endif
								</td></tr>-->
							</tr>
						</table>
					</td>
					<td valign="top">
						<table class="innr-tbl-view"  width="100%" cellpadding="5" cellspacing="5">
							<tr>
								<td><strong>Restaurant Name:</strong></td>
								<td> {{$rest_detail[0]->rest_name}}</td>
							</tr>
							<tr>
								<td><strong>Contact Number:</strong></td>
								<td> {{$rest_detail[0]->rest_landline}}</td>
							</tr>
							<tr>
								<td><strong>Contact Mobile Number:</strong></td>
								<td> {{$rest_detail[0]->rest_contact}}</td>
							</tr>
							<tr>
								<td><strong>Email:</strong></td>
								<td> {{$rest_detail[0]->rest_email}}</td>
							</tr>
							<tr>
								<td><strong>Cuisine:</strong></td>
								<td> {{$rest_detail[0]->cuisine_name}}</td>
							</tr>
					<!-- <tr>
					 	<td><strong>Type:</strong></td>
						<td>
							<?php
							/*	$r_type_name='';
							if(!empty($rest_detail[0]->rest_type))
							{
								$rtype = explode(',',$rest_detail[0]->rest_type);
								foreach($rtype as $t)
								{
									if($t=='1')
									{
										$r_type_name .='Vegan, ';
									}
									if($t=='2')
									{
										$r_type_name .='Kosher, ';
									}
									if($t=='3')
									{
										$r_type_name .='Halal, ';
									}
									if($t=='4')
									{
										$r_type_name .='Vegetarian, ';
									}
								}
							}
							echo rtrim($r_type_name,', ');*/
							?>
						</td>
					</tr>-->
					<tr><td  valign="top">
						<strong>Description:	</strong>
					</td>
					<td>
						{{str_limit($rest_detail[0]->rest_desc,200)}}
					</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<table  class="innr-tbl-view" width="100%" cellpadding="5" cellspacing="5">
				<tr>
					<td><strong>Status:</strong></td>
					<td>{{$rest_detail[0]->rest_status}}</td>
				</tr>
				<tr>
					<td><strong>Classification:</strong></td>
					<td>
						@if($rest_detail[0]->rest_classi=='1') Sponsored @endif
						@if($rest_detail[0]->rest_classi=='2') Popular @endif
						@if($rest_detail[0]->rest_classi=='3') Special @endif
						@if($rest_detail[0]->rest_classi=='4') New @endif
						@if($rest_detail[0]->rest_classi=='5') Standard @endif
					</td>
				</tr>
				<tr>
					<td><strong>Commission % to Admin:</strong></td>
					<td>{{$rest_detail[0]->rest_commission}}%</td>
				</tr>
				<tr>
					<td><strong>Today's specials:</strong></td>
					<td>@if($rest_detail[0]->rest_special==1)Yes @else No @endif</td>
				</tr>
				<tr>
					<td><strong>Delivery upto location(Miles):</strong></td>
					<td>{{ $rest_detail[0]->rest_delivery_upto }} Miles</td>
				</tr>
				<tr>
					<td><strong>Min order amt in $ (for delivery):</strong></td>
					<td>${{ $rest_detail[0]->rest_min_orderamt }} </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
<tr>
	<td >
		<table  width="100%" cellpadding="5" cellspacing="5" border="0">
			<tr>
				<td valign="top" width="25%">
					<table  width="100%" cellpadding="5" cellspacing="5" style="border:1px solid #CCCCCC">
						<tr>
							<td style="background:#CCCCCC;"><B>Open Hours</B></td>
						</tr>
						@if(!empty($rest_detail[0]->rest_mon))
						<tr>
							<td>Monday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_mon)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('mon',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_tues))
						<tr>
							<td>Tuesday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_tues)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('tue',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_wed))
						<tr>
							<td>Wednesday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_wed)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('wed',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_thus))
						<tr>
							<td>Thusday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_thus)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('thu',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_fri))
						<tr>
							<td>Friday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_fri)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('fri',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_sat))
						<tr>
							<td>Saturday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_sat)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('sat',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_sun))
						<tr>
							<td>Sunday - {{ str_replace('_', ' to ',$rest_detail[0]->rest_sun)}}  @if((!empty($rest_detail[0]->rest_close)) && ( in_array('sun',(explode(',',$rest_detail[0]->rest_close)) ))) <span style="color:#FF0000">Closed</span> @endif</td>
						</tr>
						@endif
					</table>
				</td>
				<td valign="top">
					<table class="innr-tbl-view" width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td><strong>Address Line1:</strong></td>
							<td> {{$rest_detail[0]->rest_address }}</td>
						</tr>
						<tr>
							<td><strong>City:</strong></td>
							<td> {{$rest_detail[0]->rest_suburb }}</td>
						</tr>
						<tr>
							<td><strong>State:</strong></td>
							<td> {{$rest_detail[0]->rest_state }}</td>
						</tr>
						<tr>
							<td><strong>PostCode:</strong></td>
							<td> {{$rest_detail[0]->rest_zip_code}}</td>
						</tr>
					</table>
					<table  width="100%" cellpadding="5" cellspacing="5" style="border:1px solid #CCCCCC; margin-top:20px;">
						<tr>
							<td colspan="2" style="background:#CCCCCC"><b>Services</b></td>
						</tr>
						<tr>
							<td>Delivery</td>
							<td class="check_input_service"><input  type="checkbox" disabled="disabled" name="rest_service[]" id="rest_service" @if(($id>0) && ( in_array('Delivery',(explode(',',$rest_detail[0]->rest_service)) )))checked="checked"@endif ></td>
						</tr>
						<!-- <tr>
							<td>Pick-up</td>
							<td class="check_input_service"> <input disabled="disabled" type="checkbox"  name="rest_service[]" id="rest_service" @if(($id>0) && ( in_array('Pickup',(explode(',',$rest_detail[0]->rest_service)) )))checked="checked"@endif ></td>
						</tr> -->
					</table>
				</td>
				<td valign="top">
					<table class="innr-tbl-view" width="100%" cellpadding="5" cellspacing="5">
						<tr>
							<td><strong>Sales Tax:</strong> </td>
							<td>{{$rest_detail[0]->rest_servicetax}} %</td>
						</tr>
						<tr>
							<td><strong>Partial Payment allow:</strong></td>
							<td>@if($rest_detail[0]->rest_partial_pay==1)Yes @else No @endif</td>
						</tr>
						@if($rest_detail[0]->rest_partial_pay==1)
						<tr>
							<td><strong>Partial Payment percent :</strong></td>
							<td>{{$rest_detail[0]->rest_partial_percent}} %</td>
						</tr>
						@endif
						<tr>
							<td ><strong>Cash on deliver</strong></td>
							<td> @if($rest_detail[0]->rest_cash_deliver==1)Yes @else No @endif</td>
						</tr>
						<tr>
							<td><strong>Min $ for Delivery :</strong> </td>
							<td>${{$rest_detail[0]->rest_mindelivery}}	</td>
						</tr>
						@if(!empty($rest_detail[0]->rest_delivery_from))
						<tr>
							<td><strong>Delivery Time:</strong></td>
							<td>{{$rest_detail[0]->rest_delivery_from}} To {{$rest_detail[0]->rest_delivery_to}} </td>
						</tr>
						@endif
						@if(!empty($rest_detail[0]->rest_holiday_from) && ($rest_detail[0]->rest_holiday_from!='0000-00-00'))
						<tr>
							<td><strong>Holiday Date:</strong></td>
							<td>{{$rest_detail[0]->rest_holiday_from}} To {{$rest_detail[0]->rest_holiday_to}}</td>
						</tr>
						@endif
						<tr>
							<td><strong>Vender:</strong></td>
							<td><a href="{{url('admin/vendor_profile/')}}/{{ $rest_detail[0]->vendor_id}}">{{$rest_detail[0]->name}}</a></td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<a class="btn btn-primary" href="{{url('admin/restaurant-form/')}}/{{ $rest_detail[0]->rest_id }}" style="float:right;">Edit</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="getdrestid" id="getdrestid" value="{{ $rest_detail[0]->rest_id }}">
		<input type="button" class="btn btn-primary" value="Go Back" onClick="history.go(-1);" />
	</td>
</tr>
</table>
<div>
</div>
</div>
</div><!-- /.box -->
<div>

	
	<section>
		<div class="row">
			<div class="my-panel-data">
				<!-- Menu management start -->
				<div class="col-md-12">
					<div class="panel with-nav-tabs panel-default">
						<div class="panel-heading">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1default" data-toggle="tab">Menu</a></li>
								<li>
									<a href="#tab3default" data-toggle="tab">Promotions
									</a>
								</li>
								<!--<li><a href="#tab5default" data-toggle="tab">Menu Templates</a></li>-->
							</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1default">
                               
										<div class="deepmenu">		

										<h4>Select Area *</h4>	

										<?php if(!empty($area_list)){ ?>

                                                  <select name="menuarea" id="menuarea" required="required">
 
                                                  <option value="0">Select Area</option>

                                                  <?php foreach ($area_list as $key => $value) { ?>
    	
                                                   <option value="<?php echo $value->area_city; ?>"><?php echo $value->area_name;?></option>

                                          <?php } ?>

                                          </select>  

                                          <?php } ?>

                                      </div>
                                 
									<div class="row" id="ajax_div">
										<div class="col-sm-12">
     
											<div class="divition" id="deepdivi" style="display: none;">
												<div class="col-sm-4 border">
													<h4>Menu Category</h4>
													<div class="drage" id="sortable"  data-rest="{{$rest_detail[0]->rest_id}}">
														<div class="sub-drage selected">
															<p>
																<a title="Edit" href="javascript:void(0)" data-rest="{{$rest_detail[0]->rest_id}}" id="show_popular_item">Popular</a>
															</p>
														</div>


														@if(!empty($menu_list))
														<?php $c = 1;?>
														@foreach($menu_list as $mlist)
														<div class="sub-drage" id="listItem_{{$mlist->menu_id}}">
															<a title="Edit" class="menu_edit_link" href="javascript:void(0)" data-rest="{{$rest_detail[0]->rest_id}}" data-menu="{{$mlist->menu_id}}" id="update_menu-{{$mlist->menu_id}}"><i class="fa fa-edit"></i></a>
															<p><a href="javascript:void(0)" id="menu_cat_list-{{$mlist->menu_id}}" data-menu="{{$mlist->menu_id}}"  data-rest="{{$rest_detail[0]->rest_id}}">{{$mlist->menu_name}}</a></p>
														</div>
														<?php $c++;?>
														@endforeach
														@endif
													</div>
													<form id="res_btnfrm" method="post">
														<input type="hidden" name="rest_id" value="{{$rest_detail[0]->rest_id}}" />
														<input type="hidden" name="aria_id" id="aria_id" value="">
														{!! csrf_field() !!}
														<a class="btn btn-primary" href="javascript:void(0)" id="add_menu" data-restid="{{$rest_detail[0]->rest_id}}">Add Menu Category</a>
													</form>
												</div>
													

                                                   <div class="col-sm-8 border">

                                                   <div class="divtion-data" id="show_allview">

														<div class="containt-box">
															@if(!empty($menu_list))
															<h4>Popular
																<span class="label label-success">Active</span>
															</h4>
															<p>&nbsp;</p>
															@if(count($menu_cate_detail)>0)
															<div style="border:1px solid #666666;">
																<?php $c = 1;?>
																<table width="100%" cellpadding="5" cellspacing="5" border="1" bordercolor="#ddd" >
																	@foreach($menu_cate_detail as $cat_list)
																	<tr>
																		<td width="5%" valign="top">{{$c++}}</td>
																		<td width="25%" valign="top">{{$cat_list->menu_category_name}}: <br />{{$cat_list->menu_category_desc}}</td>
																		<td  width="25%" align="right">
																			@if($cat_list->menu_category_portion=='no')
																			${{$cat_list->menu_category_price}}
																			@elseif($cat_list->menu_category_portion=='yes')
																			<?php
																			$menu_sub_itme = DB::table('category_item')
																			->where('rest_id', '=' ,$rest_detail[0]->rest_id)
																			->where('menu_category', '=' ,$cat_list->menu_category_id)
																			->orderBy('menu_cat_itm_id', 'asc')
																			->get();
																			if($menu_sub_itme)
																			{
																				foreach($menu_sub_itme as  $msi){
																					echo $msi->menu_item_title.' : $'.$msi->menu_item_price.'<br />';
																				}
																			}
																			?>
																			@endif
																		</td>
																		<td width="20%" valign="middle" align="center">

																			@if($cat_list->menu_cat_status==1)
																			<span class="label label-success">Active</span>
																			@else
																			<span class="label label-danger">Inactive</span>
																			@endif

																			<!-- @if($cat_list->menu_cat_diet==1)
																			<span class="label label-success">&nbsp;</span>
																			@elseif($cat_list->menu_cat_diet==2)
																			<span class="label label-danger">&nbsp;</span>
																			@endif *-->

																		</td>
																	</tr>
																	@endforeach
																</table>
															</div>
															@endif
															@endif
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab2default">
									<div id="Servicearea">
										<div class="col-xs-12">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title">Service Areas Listing</h3><br />
													<div style="margin-top:10px;">
														<a  href="javascript:void(0)" data-rest="{{$rest_detail[0]->rest_id}}" id="service_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Suburb</a>
													</div>
												</div>
												<div class="box-body table-responsive">
													@if(Session::has('serivce_message'))
													<div class="alert alert-success alert-dismissable">
														<i class="fa fa-check"></i>
														<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
														{{Session::get('serivce_message')}}
													</div>
													@endif
													<table id="example2" class="table table-bordered table-hover">
														<thead>
															<tr>
																<th>SrNo</th>
																<th>Suburb</th>
																<th>PostCode</th>
																<th>Delivery Charge</th>
																<th>Status</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody>
															@if($service_list)<?php $c=1;?>
															@foreach($service_list as $slist )
															<tr>
																<td>{{$c++}}</td>
																<td>{{$slist->service_suburb}}</td>
																<td>{{$slist->service_postcode}}</td>
																<td>{{$slist->service_charge}}</td>
																<td>
																	@if($slist->service_status==1)
																	<span class="label label-success">Active</span>
																	@else
																	<span class="label label-danger">Inactive</span>
																	@endif
																</td>
																<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$rest_detail[0]->rest_id}}" data-service="{{$slist->service_id}}" id="update_service-{{$slist->service_id}}">Edit</a></td>
															</tr>
															@endforeach
															@endif
														</tbody>
														<tfoot>
															<tr>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab3default">
									<div id="Promotions">
										<div class="col-xs-12">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title">Promotions Listing</h3><br />
													<div style="margin-top:10px;">
														<a  href="javascript:void(0)" data-rest="{{$rest_detail[0]->rest_id}}" id="promo_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Promo</a>
													</div>
												</div>
												<div class="box-body table-responsive">
													@if(Session::has('promo_message'))
													<div class="alert alert-success alert-dismissable">
														<i class="fa fa-check"></i>
														<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
														{{Session::get('promo_message')}}
													</div>
													@endif
													<table id="example2" class="table table-bordered table-hover">
														<thead>
															<tr>
																<th>Sr.No.</th>
																<th>Promotion For </th>
																<th>Promotion On</th>
																<th>Description</th>
																<th>Mode</th>
																<th>Discount</th>
																<th>Status</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody>
															@if($promo_list)<?php $c=1;?>
															@foreach($promo_list as $plist )
															<tr>
																<td>{{$c++}}</td>
																<td>{{str_replace('_',' ',$plist->promo_for)}}</td>
																<td>{{$plist->promo_on}}</td>
																<td>{{$plist->promo_desc}}</td>
																<td>{{$plist->promo_mode}}</td>
																<td>{{$plist->promo_value}}</td>
																<td>
																	@if($plist->promo_status==1)
																	<span class="label label-success">Active</span>
																	@else
																	<span class="label label-danger">Inactive</span>
																	@endif
																</td>
																<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$rest_detail[0]->rest_id}}" data-promo="{{$plist->promo_id}}" id="update_promo-{{$plist->promo_id}}">Edit</a></td>
															</tr>
															@endforeach
															@endif
														</tbody>
														<tfoot>
															<tr>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="tab4default">
									<div id="BankDetail">
										<div class="col-xs-12">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title">Bank Detail Listing</h3><br />
													<div style="margin-top:10px;">
														<a  href="javascript:void(0)" data-rest="{{$rest_detail[0]->rest_id}}" id="bank_btn" class="btn btn-primary" style="color:#FFFFFF"> Add Details</a>
													</div>
												</div>
												<div class="box-body table-responsive">
													@if(Session::has('bank_message'))
													<div class="alert alert-success alert-dismissable">
														<i class="fa fa-check"></i>
														<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
														{{Session::get('bank_message')}}
													</div>
													@endif
													<table id="example2" class="table table-bordered table-hover">
														<thead>
															<tr>
																<th>Sr.No.</th>
																<th>Name</th>
																<th>Bank</th>
																<th>Branch</th>
																<th>BSB</th>
																<th>Account</th>
																<th>Status</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody>
															@if($bank_list)<?php $c=1;?>
															@foreach($bank_list as $blist )
															<tr>
																<td>{{$c++}}</td>
																<td>{{$blist->bank_uname}}</td>
																<td>{{$blist->bank_name}}</td>
																<td>{{$blist->bank_branch}}</td>
																<td>{{$blist->bank_bsb}}</td>
																<td>{{$blist->bank_acc}}</td>
																<td>
																	@if($blist->bank_status==1)
																	<span class="label label-success">Active</span>
																	@else
																	<span class="label label-danger">Inactive</span>
																	@endif
																</td>
																<td><a class="btn btn-primary" href="javascript:void(0)"  data-rest="{{$rest_detail[0]->rest_id}}" data-bank="{{$blist->bank_id}}" id="update_bank-{{$blist->bank_id}}">Edit</a></td>
															</tr>
															@endforeach
															@endif
														</tbody>
														<tfoot>
															<tr>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
																<th>&nbsp;</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div></div>
									<div class="tab-pane fade" id="tab5default">
										<div id="MenuTemplates">
											<div class="col-md-12">

												<div class="box box-primary">
													<div class="box-header">
														<h3 class="box-title">Menu Template list</h3>
													</div>
													<form  role="form" method="POST" id="template_frm" action="{{ url('/admin/menutemplate_action') }}" enctype="multipart/form-data">
														<input type="hidden" name="restaurant_id" value="{{$id}}" />
														{!! csrf_field() !!}
														<div class="box-body">
															<div class="form-group">
																<label for="exampleInputEmail1">Add Menu Templates in Your Restaurant</label>
																<div>
																	<?php if(!empty($template_list)){ ?>
																		<?php $rest_template = explode(',',$rest_detail[0]->template_id);?>
																		<?php foreach ($template_list as $Tlist) { ?>


																			<input type="checkbox" class="template_menu" name="tempalte_data[]" value="{{$Tlist->template_id}}" required="required" <?php if(in_array($Tlist->template_id,$rest_template)){ ?> <?php echo 'disabled="disabled"';} ?>> <?php echo $Tlist->template_name; ?>

																		<?php } ?>


																	<?php } ?>


																</div>
															</div>
														</div>
														<div class="box-footer">
															<input type="button" class="btn btn-primary"  value="Submit" id="add_template" />
														</div>
													</form>
												</div>
											</div>
										</div></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Menu management End -->
			</section>
		</div>
	</div>
</section><!-- /.content -->
</aside><!-- /.right-side -->
@endsection
<!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="addons-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" id="addons_modal_data">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
			</div>
			<form action="#" method="post">
				<div class="modal-body">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">TO:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email TO">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">CC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email CC">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">BCC:</span>
							<input name="email_to" type="email" class="form-control" placeholder="Email BCC">
						</div>
					</div>
					<div class="form-group">
						<textarea name="message" id="email_message" class="form-control" placeholder="Message" style="height: 120px;"></textarea>
					</div>
					<div class="form-group">
						<div class="btn btn-success btn-file">
							<i class="fa fa-paperclip"></i> Attachment
							<input type="file" name="attachment"/>
						</div>
						<p class="help-block">Max. 32MB</p>
					</div>
				</div>
				<div class="modal-footer clearfix">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
					<button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('js_bottom')
<!-- Start of the property info details div -->
<style>
#ajax_favorite_loddder {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background:rgba(27, 26, 26, 0.48);
	z-index: 1001;
}
#ajax_favorite_loddder img {
	top: 50%;
	left: 46.5%;
	position: absolute;
}
.footer-wrapper {
	float: left;
	width: 100%;
	/*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	z-index: 998 !important;
}
</style>
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>
<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- AdminLTE App
	<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>-->
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>
	<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
	<script>


    $('#menuarea').on('change', function() {

        var menuaria = $('#menuarea').val();

        if(menuaria != 0){ 

        $('#deepdivi').show(); 

        	$("#ajax_favorite_loddder").show();
			var rest_id = $('#getdrestid').val(); 
			var aria = $('#menuarea').val(); 
			var frm_val = "rest_id="+rest_id+"&aria="+aria;
			$.ajax({
				type: "post",
				url: "{{url('/admin/show_popular')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#ajax_div').html(msg);
				}
			});

        }else{ $('#deepdivi').hide(); }

        $('#aria_id').val(menuaria); 
        $('#caria_id').val(menuaria);
        $('#menu_aria_id').val(menuaria); 
        $('#mitem_aria_id').val(menuaria);    

    });


		function check_frm()
		{
			var form = $("#rest_frm");
			form.validate();
			var valid =	form.valid();
			if(valid)
			{
				$(form).submit();
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
	<script>
		/* MENU TABE START */
		$( function() {
			$( "#sortable" ).sortable({
				update:  function (event, ui) {
					var sort_data = $("#sortable").sortable("serialize");
					var rest_id = $("#sortable").attr("data-rest");
					var data = 'rest_id='+rest_id+'&'+sort_data;
					$("#ajax_favorite_loddder").show();
					$.ajax({
						data: data,
						type: 'POST',
						url: "{{url('/admin/update_sortorder')}}",
						success: function(msg) {
							$("#ajax_favorite_loddder").hide();
//alert(msg);
}
});
				}
//revert: true
});
			$( "#draggable" ).draggable({
				connectToSortable: "#sortable",
				helper: "clone",
				revert: "invalid"
			});
			$( "ul, li" ).disableSelection();
		} );
	$(document).on('click', '#add_menu', function(){

		var aria = $('#menuarea').val(); 

		var rest_id = $(this).attr('data-restid');
//alert(rest_id);
if(rest_id>0)
{
	$("#ajax_favorite_loddder").show();
	var frm_val = $('#res_btnfrm').serialize();
	$.ajax({
		type: "post",
		url: "{{url('/admin/menu_ajax_form')}}",
		data: frm_val,
		success: function(msg) {
			$("#ajax_favorite_loddder").hide();
			$('#show_allview').html(msg);
			$('#caria_id').val(aria);
		}
	});
}
});
		$(document).on('click', '[id^="update_menu-"]', function() {
			var menu =$(this).attr('data-menu');
			var rest = $(this).attr('data-rest') ;
			$("#ajax_favorite_loddder").show();
//var frm_val = $('#res_btnfrm').serialize();

var aria = $('#menuarea').val(); 

var frm_val = "rest="+rest+"&menu="+menu+"&aria_id="+aria;
$.ajax({
	type: "post",
	url: "{{url('/admin/menu_ajax_form')}}",
	data: frm_val,
	success: function(msg) {
		$("#ajax_favorite_loddder").hide();
		$('#show_allview').html(msg);
	}
});
});
		/*  MENU ITEM */
		$(document).on('click', '#add_menu_item', function(){
			$("#ajax_favorite_loddder").show();
			var frm_val = $('#itm_btnfrm').serialize();
			$.ajax({
				type: "post",
				url: "{{url('/admin/menu_ajax_itemform')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#show_allview').html(msg);
				}
			});
		});
		$(document).on('click', '[id^="menu_cat_list-"]', function() {
			var menu =$(this).attr('data-menu');
			var rest = $(this).attr('data-rest') ;
			var aria = $('#menuarea').val(); 

			$("#ajax_favorite_loddder").show();
			var frm_val = "menu="+menu+"&rest="+rest+"&aria="+aria;
			$.ajax({
				type: "post",
				url: "{{url('/admin/show_menudetail')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#ajax_div').html(msg);
				}
			});
		});
		/*UPDATE MENU ITEMS */
		$(document).on('click', '[id^="update_item_data-"]', function() {
			var menu =$(this).attr('data-menu');
			var rest = $(this).attr('data-rest') ;
			var item_id = $(this).attr('data-item') ;
			$("#ajax_favorite_loddder").show();
var aria = $('#menuarea').val(); 
var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id+"&aria_id="+aria;
$.ajax({
	type: "post",
	url: "{{url('/admin/update_menu_item')}}",
	data: frm_val,
	success: function(msg) {
		$("#ajax_favorite_loddder").hide();
		$('#show_allview').html(msg);
	}
});
});
		/* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */
		$(document).on('click', '[id^="addon_list_data-"]', function() {
//	$("#ajax_favorite_loddder").show();
//$("#addons-modal").modal('show');
var menu =$(this).attr('data-menu');
var rest = $(this).attr('data-rest') ;
var item_id = $(this).attr('data-item') ;
$("#ajax_favorite_loddder").show();
var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id;
$.ajax({
	type: "post",
	url: "{{url('/admin/addons_list')}}",
	data: frm_val,
	success: function(msg) {
		$("#ajax_favorite_loddder").hide();
		$('#addons_modal_data').html(msg);
		$("#addons-modal").modal('show');
	}
});
});
		$(document).on('click', '#add_addons', function() {
//	$("#ajax_favorite_loddder").show();
//$("#addons-modal").modal('show');
// $("#addons-modal").modal('hide');
var menu =$(this).attr('data-menu');
var rest = $(this).attr('data-rest') ;
var item_id = $(this).attr('data-item') ;
$("#ajax_favorite_loddder").show();
var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id;
$.ajax({
	type: "post",
	url: "{{url('/admin/addons_form')}}",
	data: frm_val,
	success: function(msg) {
		$("#ajax_favorite_loddder").hide();
		$('#addons_modal_data').html(msg);
		$("#addons-modal").modal('show');
	}
});
});
		$(document).on('click', '[id^="addon_update_data-"]', function() {
			var menu =$(this).attr('data-menu');
			var rest = $(this).attr('data-rest') ;
			var item_id = $(this).attr('data-item') ;
			var addon_id = $(this).attr('data-addon') ;
			$("#ajax_favorite_loddder").show();
			var frm_val = "rest="+rest+"&menu="+menu+"&menu_itme="+item_id+"&addon_id="+addon_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/addons_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#addons_modal_data').html(msg);
					$("#addons-modal").modal('show');
				}
			});
		});
		/* ADDONS LIST /ADD /EDIT FUNCTIONALITY WORK  START */
		/* MENU TAB END */
		/* SERVICE TAB START */
		$(document).on('click', '#service_btn', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var frm_val = "rest_id="+rest_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/service_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#Servicearea').html(msg);
				}
			});
		});
		$(document).on('click', '[id^="update_service-"]', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var service_id = $(this).attr('data-service');
			var frm_val = "rest_id="+rest_id+"&service_id="+service_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/service_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#Servicearea').html(msg);
				}
			});
		});
		/* SERVICE TAB EBD */
		/* PROMOSTIONS TAE START */
		$(document).on('click', '#promo_btn', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var frm_val = "rest_id="+rest_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/promo_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#Promotions').html(msg);
				}
			});
		});
		$(document).on('click', '[id^="update_promo-"]', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var promo_id = $(this).attr('data-promo');
			var frm_val = "rest_id="+rest_id+"&promo_id="+promo_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/promo_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#Promotions').html(msg);
				}
			});
		});
		/* PROMOTIONS TAB END*/
		/* BANKDETAIL TAE START */
		$(document).on('click', '#bank_btn', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var frm_val = "rest_id="+rest_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/bank_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#BankDetail').html(msg);
				}
			});
		});
		$(document).on('click', '[id^="update_bank-"]', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var bank_id = $(this).attr('data-bank');
			var frm_val = "rest_id="+rest_id+"&bank_id="+bank_id;
			$.ajax({
				type: "post",
				url: "{{url('/admin/bank_form')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#BankDetail').html(msg);
				}
			});
		});
		/* BANKDETAIL TAE END */
		$(document).on('click', '[id^="show_popular_item"]', function(){
			$("#ajax_favorite_loddder").show();
			var rest_id = $(this).attr('data-rest');
			var aria = $('#menuarea').val(); 
			var frm_val = "rest_id="+rest_id+"&aria="+aria;
			$.ajax({
				type: "post",
				url: "{{url('/admin/show_popular')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();
					$('#ajax_div').html(msg);
				}
			});
		});
		$(document).on('click', '#add_template', function(){
			var total_tempalte = $('.template_menu').length;
			if(total_tempalte>0)
			{
				var form = $("#template_frm");
				form.validate();
				var valid =	form.valid();
				if(valid)
				{
					$(form).submit();
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				alert('Template not available!');
			}
		});
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
	</script>
	@stop