@extends('layouts.admin')
@section("other_css")
<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
   .deepmd{ width: 100%; }
   .deepmd strong{ float: right; }
   .deepon{ font-size: 14px; } 
   .top-detail_invoice {
    float: left;
    width: 100%;
    text-align: center;
    border-bottom: 1px dashed #ddd;
    margin-bottom: 10px;
    text-transform: capitalize;
}

.top-detail_invoice h3 {
    text-transform: uppercase;
    font-weight: bold;
}

.customer_detail {
    float: left;
    width: 100%;
    text-transform: capitalize;
    border-bottom: 1px dashed #ddd;
}
</style>
@stop
@section('content')
<aside class="right-side">
   <section class="content-header">
      <h1>Product Order Detail</h1>
      <ol class="breadcrumb">
         <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
         <li class="active">Product Order Detail</li>
      </ol>
   </section>
   <div class="myOdrersDetailBox">
      <div class="row">
       <!--   <div class="col-md-4">
            <div class="myOrder">
               <?php foreach ($ps_list as $key => $value) {
                  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); ?>
               <div class="row">
                  <div class="col-md-6">
                     <div class="myOrderImg">
                        <?php if(!empty($pservice->image)){ ?>      
                        <img src="{{ url('/public/uploads/product/'.$pservice->image) }}">
                        <?php } ?>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="myOrderName deepon">
                        <?php if(!empty($pservice->name)){ ?>       
                        <strong>{{$pservice->name}}</strong>
                        <?php } ?>
                     </div>
                     </br>
                     <hr class="hrline">
                     <div class="deepmd">
                        <label>Price</label>
                        <?php if(!empty($pservice->price)){ ?>  
                        <strong>${{$pservice->price}}</strong>
                        <?php } ?>
                     </div>
                     </br>
                     <div class="deepmd">
                        <label>Quantity</label>
                        <strong>{{$value->ps_qty}}</strong>
                     </div>
                  </div>
               </div>
               <hr class="hrline">
               <?php } ?> 
            </div>
         </div> -->
         <div class="col-md-2">
         </div>
         <div class="col-md-8">
            <div class="myOrder">
               <div class="myOrderDtelBox">
                  <div class="top-detail_invoice">
                     <h3>herbarium</h3>
                     <p>  <strong>{{$order_detail->address}}</strong> <strong>{{$order_detail->city}} </strong> <strong>{{$order_detail->state}} </strong><!--  <strong>{{$order_detail->zip}} </strong> --></p>
                    
                    <?php $order_date = date("Y-m-d g:iA", strtotime($order_detail->created_at)); ?>
                     <p> Placed at
                        {{$order_date}}
                     </p>
                     <p>Order ID:{{$order_detail->order_id}}</p>
                  </div>
                  <div class="customer_detail">
                     <p> {{$order_detail->first_name}} {{$order_detail->last_name}}</p>
                     <p>{{$order_detail->mobile_no}}</p>
                     <p>{{$order_detail->email}}</p>
                     <p><strong>Pick Up Address </strong> {{$order_detail->addressp}}</p>
                     <p><strong>Drop Off Address</strong> {{$order_detail->addressd}}</p>
                     <p><strong>Order Status</strong> 
                        <span class="green">
                        @if($order_detail->status==0) Pending @endif
                        @if($order_detail->status==1) Accept @endif
                        @if($order_detail->status==2) Cancelled @endif
                        @if($order_detail->status==3) On the way @endif
                        @if($order_detail->status==4) Delivered @endif
                        @if($order_detail->status==5) Requested for return @endif
                        @if($order_detail->status==6) Return request accepted @endif
                        @if($order_detail->status==7) Return request declined @endif
                        </span>
                     </p>
                  </div>
                  <div class="prduct_detail">
                     <table>
                     <tr>
                        <th style="width:30px;">Qty</th>
                        <th>Name</th>
                        <th>Price</th>
                     </tr>
                     
                         <?php foreach ($ps_list as $key => $value) {
                  $pservice = DB::table('product_service')->where('id','=',$value->ps_id)->first(); ?>
                    <tr>
                        <td>
                           <p>{{$value->ps_qty}}</p>
                        </td>
                        <td>
                           <p> <?php if(!empty($pservice->name)){ ?>     
                              {{$pservice->name}}
                              <?php } ?>
                           </p>
                        </td>
                        <td>
                           <p>
                              <?php if(!empty($pservice->price)){ ?>   
                              ${{$pservice->price}}
                              <?php } ?>
                           </p>
                        </td></tr>
                      <?php } ?>
                     <tr >
                         <td colspan="1"></td>
                         <td colspan="1" style="border-top:1px dashed #ddd;"> <label>Subtotal</label>
                        </td>
                         <td style="border-top:1px dashed #ddd;"><span>${{ number_format($order_detail->sub_total, 2) }}</span></td>
                     </tr>
                     <?php $cc = DB::table('coupon_code')->where('id','=',$order_detail->coupon_id)->first(); ?>
                           <?php if(!empty($cc->coupon)){ ?>
                            <tr>
                               <td colspan="1"></td>
                               <td colspan="1"> <label>Coupon Code</label>
                                </td>
                               <td><span><?php echo  @$cc->coupon; ?></span></td>
                            </tr>
                          <?php } ?>
                     <tr>
                         <td colspan="1"></td>
                         <td colspan="1"> <label>Discount</label>
                        </td>
                         <td><span>${{ number_format($order_detail->promo_amount, 2) }}</span></td>
                     </tr>
                     <tr>
                         <td colspan="1"></td>
                         <td colspan="1"> <label>Service Tax</label>
                        </td>
                         <td><span>${{ number_format($order_detail->service_tax, 2)}}</span></td>
                     </tr>
                     <tr>
                         <td colspan="1"></td>
                         <td colspan="1"> <label>Total Paid Amount</label>
                        </td>
                         <td><span>${{ number_format($order_detail->total, 2) }}</span></td>
                     </tr>
                       <tr>
                         <td colspan="1"></td>
                         <td colspan="1" style="border-top:1px dashed #ddd;"> <label>Payment Type</label>
                        </td>
                         <td style="border-top:1px dashed #ddd;"><span>{{$order_detail->payment_method}}</span></td>
                     </tr>
                     <tr>
                         <td colspan="1"></td>
                         <td colspan="1"> <label>TransactionID</label>
                        </td>
                         <td><span>{{$order_detail->txn_id}}</span></td>
                     </tr>
                        <tr>
                         <td colspan="1"></td>
                         <td colspan="1"> <label>Payment Status</label>
                        </td>
                         <td><span>{{$order_detail->pay_status}}</span></td>
                     </tr>
                       <tr>
                         <td colspan="1"></td>
                         <td colspan="1" style="border-top:1px dashed #ddd;"> <label>Comment</label>
                        </td>
                         <td style="border-top:1px dashed #ddd;"><span>{{$order_detail->instruction}}</span></td>
                     </tr>


              <?php if( in_array($order_detail->status, array(5,6,7))) { ?>

                <tr>
                 <td colspan="1"></td>
                 <td colspan="1" style="border-top:1px dashed #ddd;"> <label>Return reason:</label>
                </td>
                 <td style="border-top:1px dashed #ddd;"><span>{{$order_detail->return_reason}}</span></td>
               </tr>

               <?php } ?>


              <?php if(in_array($order_detail->status, array(6,7)) && !empty($order_detail->return_reason_merchant)){ ?>

               <tr>
               <td colspan="1"></td>
               <td colspan="1" style="border-top:1px dashed #ddd;"> <label>Return reason merchant:</label>
               </td>
               <td style="border-top:1px dashed #ddd;"><span>{{$order_detail->return_reason_merchant}}</span></td>
               </tr>

               <?php } ?>

               <?php if( !empty($vendor_order->commission_rate) ) { ?>

                <tr>
                 <td colspan="1"></td>
                 <td colspan="1" style="border-top: 1px dashed #ddd;"> <label>Commission rate (in %)</label>
                </td>
                 <td  style="border-top: 1px dashed #ddd;"><span>{{$vendor_order->commission_rate}}</span></td>
               </tr>


                 <tr>
                 <td colspan="1"></td>
                 <td colspan="1" > <label>Commission amount</label>
                 </td>
                 <td ><span><?php $com_amount = $order_detail->total*$vendor_order->commission_rate/100;
                     echo '$'.number_format($com_amount, 2); ?> </span></td>
                 </tr>


              <tr>
               <td colspan="1"></td>
               <td colspan="1" > <label>Merchant amount</label>
               </td>
               <td ><span><?php $merchant_amount = $order_detail->total - $com_amount; 
                     echo '$'.number_format($merchant_amount, 2); ?></span></td>
              </tr>

               <?php } ?>


                     </table>
                  </div>
               </div>
               <!-- <div class="myOrderDtel">
                  <label>Subtotal</label>
                  <strong>${{ number_format($order_detail->sub_total, 2) }}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Discount</label>
                  <strong>${{ number_format($order_detail->promo_amount, 2) }}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Service Tax</label>
                  <strong>${{ number_format($order_detail->service_tax, 2)}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Total Paid Amount</label>
                  <strong>${{ number_format($order_detail->total, 2) }}</strong>
               </div> -->
               <!-- <div class="myOrderDtel">
                  <label>Payment Type</label>
                  <strong style="text-transform: capitalize;">{{$order_detail->payment_method}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>TransactionID</label>
                  <strong>{{$order_detail->txn_id}}</strong>
               </div> -->
              <!--  <div class="myOrderDtel">
                  <label>Payment Status</label>
                  <strong>{{$order_detail->pay_status}}</strong>
               </div> -->
               <!-- <div class="myOrderDtel">
                  <label>Comment</label>
                  <strong>{{$order_detail->instruction}}</strong>
               </div> -->

            
            </div>
         </div>
      </div>
   </div>
   </div>
</aside>
@stop
@section('js_bottom')
<style>
   #ajax_favorite_loddder {
   position: fixed;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background:rgba(27, 26, 26, 0.48);
   z-index: 1001;
   }
   #ajax_favorite_loddder img {
   top:50%;
   left:46.5%;
   position:absolute;
   }
   .footer-wrapper {
   float:left;
   width:100%;
   }
   #addons-modal.modal {
   z-index:999;
   }
   .modal-backdrop {    
   z-index:998 !important;
   }
   table {
 
  border-collapse: collapse;
  width: 100%;
}

td, th {

  text-align: left;
  padding: 8px;
  width: 10%;
}

</style>

</style>
<div id="ajax_favorite_loddder" style="display:none;">
   <div align="center" style="vertical-align:middle;">
      <img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>       
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>        
<script type="text/javascript">
   function check_frm(){
   
    $('#error_msg').hide();
   
    var form = $("#search_frm");
   
    form.validate();
   
    var valid = form.valid();
   
    if(($("#vendor_cont").val()!='') || ($("#vendor_email").val()!=''))
   
    {       
   
         $("#ajax_favorite_loddder").show();    
   
        var frm_val = $('#search_frm').serialize();             
   
        $.ajax({
   
        type: "POST",
   
        url: "{{url('/admin/vendor_search')}}",
   
        data: frm_val,
   
            success: function(msg) {
   
             $("#ajax_favorite_loddder").hide();    
   
            
   
                $('#vender_search_list').html(msg);
   
            }
   
        });
   
    }
   
    else
   
    {
   
        //alert('Please insert any one value');
   
        
   
        $('#error_msg').html('Please insert any one value');
   
        $('#error_msg').show();
   
        return false;
   
    }       
   
   }
   
   $(function() {
   
    $("#example1").dataTable();
   
    $('#example2').dataTable({
   
        "bPaginate": true,
   
        "bLengthChange": false,
   
        "bFilter": true,
   
        "bSort": true,
   
        "bInfo": true,
   
        "bAutoWidth": false
   
    });
   
   });
   
</script>
@stop