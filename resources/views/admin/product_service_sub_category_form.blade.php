@extends('layouts.admin')

@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Product Sub Category Form

			<small>Control panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Product Sub Category Form</li>

		</ol>

	</section>

	<!-- Main content -->

	<section class="content">

		<div class="col-md-12">

			<!-- general form elements -->

			<div class="box box-primary">

				<div class="box-header">

					<h3 class="box-title">Product Sub Category Form</h3>

				</div><!-- /.box-header -->

				<p style="float: left;text-align: center;width: 100%;">

					@if(Session::has('message'))

					{{Session::get('message')}}

				@endif </p>

				<!-- form start -->

		<form  role="form" method="POST" id="proservice_frm" action="{{ url('/admin/product_service_sub_cate_action') }}" enctype="multipart/form-data">



	  @csrf



		<input type="hidden" name="category_id" value="{{$id}}" />



					<div class="box-body">



						<div class="form-group">

							<label for="exampleInputEmail1">Sub Category</label>

							<input type="text" class="form-control" name="category_name" id="category_name" value="" required="required">

						</div>



							<div class="form-group">

							<label for="exampleInputEmail1">Image (File Type: jpeg,gif,png)</label>

							<input type="file" class="form-control" name="cat_image" id="cat_image">		



							@if($errors->has("cat_image"))

                                       <span class="text-danger">{{ $errors->first("cat_image") }}</span>

                                       @endif



							<div id="image_error" style="display:none; color:#FF0000">

							Upload Images only   jpeg,gif,png

							</div>											

						</div>



						<!--<div class="form-group">

							<label for="exampleInputEmail1">Status</label>

							<select name="status" id="status"  class="form-control">

							<option value="1">Active </option>

							<option value="0">Inactive </option>

							</select>		

						</div>-->

						<input type="hidden" name="status" id="status" value="1">



						</div> <!-- /.box-body -->

						<div class="box-footer">

						

							<input type="submit" class="btn btn-primary" value="Submit"  />

						

							<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

						</div>

					</form>

				</div><!-- /.box -->

			</div>

		</section><!-- /.content -->

	</aside><!-- /.right-side -->

	@endsection

	@section('js_bottom')

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<style>



	.field-icon {

		float: right;

		margin-left: -25px;

		margin-top: -25px;

		position: relative;

		z-index: 2;

	}



	.container{

		padding-top:50px;

		margin: auto;

	}





	.ui-widget-content {

		max-height: 221px;

		overflow-y: scroll;

	}

	.ui-menu .ui-menu-item {

		padding: 5px;

	}

	.ui-menu-item:nth-child(2n) {

		background-color: #f1f1f1;

	}

</style>

<style>

#ajax_favorite_loddder {

	position: fixed;

	top: 0;

	left: 0;

	width: 100%;

	height: 100%;

	background:rgba(27, 26, 26, 0.48);

	z-index: 1001;

}

#ajax_favorite_loddder img {

	top: 50%;

	left: 46.5%;

	position: absolute;

}

.footer-wrapper {

	float: left;

	width: 100%;

	/*display: none;*/

}

#addons-modal.modal {

	z-index: 999;

}

.modal-backdrop {

	z-index: 998 !important;

}

</style>

<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

</div>

<!-- jQuery 2.0.2 -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

<!-- jQuery UI 1.10.3 -->

<script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- Bootstrap -->

<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Bootstrap WYSIHTML5 -->

<script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->

<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>







<script>



	$(".toggle-password").click(function() {

		$(this).toggleClass("fa-eye fa-eye-slash");

		var input = $($(this).attr("toggle"));

		if (input.attr("type") == "password") {

			input.attr("type", "text");

		} else {

			input.attr("type", "password");

		}

	});



	// $(".toggle-conpassword").click(function() {

	// 	$(this).toggleClass("fa-eye fa-eye-slash");

	// 	var input = $($(this).attr("toggle"));

	// 	if (input.attr("type") == "password") {

	// 		input.attr("type", "text");

	// 	} else {

	// 		input.attr("type", "password");

	// 	}

	// });







	function check_email()

	{

		$('#error_msg').hide();





		jQuery.validator.addMethod("pass", function (value, element) {

			if (/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(value)) {

				return true;

			} else {

				return false;

			};

		});





		jQuery.validator.addMethod("vname", function (value, element) {

			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

				return true;

			} else {

				return false;

			};

		});



		jQuery.validator.addMethod("lname", function (value, element) {

			if (/^[a-zA-Z][a-z\s]*$/.test(value)) {

				return true;

			} else {

				return false;

			};

		});





		var form = $("#user_frm");

		form.validate({

			rules: {

				name:{

					required:true,

					vname:true

				},

				last_name: {

					required: true,

					lname:true

				},

				mob_no: {

					required: true,

					minlength:14,

				},

				email: {

					required: true,

					email:true

				},

				password: {

					required: true,

					pass:true,

					minlength:8

				},

				confirm_password: {

					required: true,

					minlength: 8,

					equalTo : "#password"

				},

				address: {

					required: true,

				},

				state: {

					required: true,

				},

				suburb: {

					required: true,

				},

				zipcode: {

					required: true,

				}

			},

			messages: {

				name:{

					required:'Please enter name.',

					vname:"Please enter only letters."

				},

				last_name: {

					required:'Please enter last name.',

					lname:'Please enter only letters.'

				},

				mob_no: {

					required:'Please enter mobile number.',

					minlength:"Please enter valid mobile no. with country code."

				},

				email: {

					required:'Please enter email address.',

					email:'Please enter an valid email address.',

				},

				password: {

					required:'Please enter password.',

					pass:"at least one number, one lowercase and one uppercase letter.",

					minlength:"Please enter at least 8 characters."

				},

				confirm_password: {

					required:'Please enter confirm password.',

					minlength:'Password must be at least 8 characters.',

					equalTo:'confirm password not match, please enter correct.'

				},

				address: {

					required:'Please enter postal address.',

				},

				state: {

					required:'Please enter state.',

				},

				suburb: {

					required:'Please enter city name.',

				},

				zipcode: {

					required:'Please enter zip code.',

				}

			}

		});

		var valid =	form.valid();

		if(valid){

			$("#ajax_parner_loddder").show();

				/*var regExp = /^0[0-9].*$/;

				if(regExp.test($("#mob_no").val()))

					{*/

					/*$(form).submit();

					return true;*/

					var frm_val = $('#user_frm').serialize();

					$.ajax({

						type: "POST",

						url: "./check_vendor_duplicateemail",

						data: frm_val,

						success: function(msg) {

							if(msg=='1')

							{

								$("#ajax_parner_loddder").hide();

								$('#email_msg').show();

								return false;

							}

							else if(msg=='2')

							{

								$(form).submit();

								return true;

							}

						}

					});

				/*}

				else

				{

					//alert('Contact number start with 0.');

					$('#error_msg').html('Contact number start with 0.');

					$('#error_msg').show();

					return false;

				}*/

			}

			else

			{

				return false;

			}

		}

		function check_number()

		{

			$('#error_msg').hide();

			var form = $("#user_frm");

			form.validate({

				rules: {

					name:{

						required:true,

					},

					last_name: {

						required: true,

					},

					mob_no: {

						required: true,

						minlength:14

					},

					email: {

						required: true,

						email:true

					},

					password: {

						required: true,

					},

					address: {

						required: true,

					},

					state: {

						required: true,

					},

					suburb: {

						required: true,

					},

					zipcode: {

						required: true,

					}

				},

				messages: {

					name:{

						required:'Please enter name.',

					},

					last_name: {

						required:'Please enter last name.',

					},

					mob_no: {

						required:'Please enter mobile number.',

						minlength:"Please enter valid mobile no. with country code."

					},

					email: {

						required:'Please enter email address.',

						email:'Please enter an valid email address.',

					},

					password: {

						required:'Please enter password.',

					},

					address: {

						required:'Please enter postal address.',

					},

					state: {

						required:'Please enter state.',

					},

					suburb: {

						required:'Please enter city name.',

					},

					zipcode: {

						required:'Please enter zip code.',

					}

				}

			});

			var valid =	form.valid();

			if(valid){

				/*var regExp = /^0[0-9].*$/;

				if(regExp.test($("#mob_no").val()))

					{*/

						$(form).submit();

						return true;

				/*}

				else

				{

					//alert('Contact number start with 0.');

					$('#error_msg').html('Contact number start with 0.');

					$('#error_msg').show();

					return false;

				}*/

			}

			else

			{

				return false;

			}

		}

	</script>

	<script>

      // This example displays an address form, using the autocomplete feature

      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places

      // parameter when you first load the API. For example:

      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;

      var componentForm = {

      	street_number: 'short_name',

      	route: 'long_name',

      	locality: 'long_name',

      	administrative_area_level_1: 'short_name',

      	country: 'long_name',

      	postal_code: 'short_name'

      };

      function initAutocomplete() {

        // Create the autocomplete object, restricting the search to geographical

        // location types.

        autocomplete = new google.maps.places.Autocomplete(

        	/** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),

        	{types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address

        // fields in the form.

        autocomplete.addListener('place_changed', fillInAddress);

    }

    function fillInAddress() {

        // Get the place details from the autocomplete object.

        var place = autocomplete.getPlace();

        for (var component in componentForm) {

        	document.getElementById(component).value = '';

        	document.getElementById(component).disabled = false;

        }

        // Get each component of the address from the place details

        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {

        	var addressType = place.address_components[i].types[0];

        	if (componentForm[addressType]) {

        		var val = place.address_components[i][componentForm[addressType]];

        		document.getElementById(addressType).value = val;

			//alert(val);

		}

	}

}

      // Bias the autocomplete object to the user's geographical location,

      // as supplied by the browser's 'navigator.geolocation' object.

      function geolocate() {

      	if (navigator.geolocation) {

      		navigator.geolocation.getCurrentPosition(function(position) {

      			var geolocation = {

      				lat: position.coords.latitude,

      				lng: position.coords.longitude

      			};

      			var circle = new google.maps.Circle({

      				center: geolocation,

      				radius: position.coords.accuracy

      			});

      			autocomplete.setBounds(circle.getBounds());

      		});

      	}

      }

  </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&libraries=places&callback=initAutocomplete"></script>

  @stop