@extends('layouts.admin')



@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

	<!-- Content Header (Page header) -->

	<section class="content-header">

		<h1>

			Email/Notification Template

			<small>Control Panel</small>

		</h1>

		<ol class="breadcrumb">

			<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

			<li class="active">Email/Notification Template</li>

		</ol>

	</section>



	<!-- Main content -->

	<section class="content">

	<div class="col-md-12">

                            <!-- general form elements -->

                            <div class="box box-primary">

                                <div class="box-header">

                                    <h3 class="box-title">Email/Notification Template</h3>

                                </div><!-- /.box-header -->

								

					 @if(Session::has('error'))

					 <div class="alert alert-danger alert-dismissable">				

								{{Session::get('error')}}

								</div>

							@endif 

                                <!-- form start -->

								

								<form  role="form" method="POST" id="cuisine_frm" action="{{ url('/admin/email_content_action') }}">    

								<input type="hidden" name="email_id" value="{{$id}}" />

								

								{!! csrf_field() !!}

                                    <div class="box-body">

                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Title</label>

                                           <input type="text" class="form-control" id="email_title" name="email_title"  value="@if($id>0){{$page_detail[0]->email_title}}@endif" required="required">

                                        </div>

                                        <div class="form-group">

                                            <label for="exampleInputEmail1">Content</label>

											<textarea name="email_content" id="email_content" class="form-control textarea"> @if($id>0){{$page_detail[0]->email_content}}@endif</textarea>

                                        </div>

                                        <div class="form-group">

                                            <label>Content Type</label>

											<select name="content_type" id="content_type"  class="form-control">

											 <option value="Email" @if(($id>0) && ($page_detail[0]->content_type=="Email")) selected="selected"@endif> Email </option>

											 <option value="Notification" @if(($id>0) && ($page_detail[0]->content_type=="Notification")) selected="selected"@endif> Notification </option>

											</select>		
	
                                        </div>

                                    </div> <!-- /.box-body -->

                                    <div class="box-footer">		

										<input type="submit" class="btn btn-primary" value="Submit" onClick="check_email()" />


									 	<input type="button"   class="btn btn-primary" value="Go Back" onClick="history.go(-1);"  />

                                    </div>

                                </form>

								

                            </div><!-- /.box -->





                        </div>

	

	

	</section><!-- /.content -->

</aside><!-- /.right-side -->







@endsection







@section('js_bottom')



<!-- jQuery 2.0.2 -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- jQuery UI 1.10.3 -->

        <script src="{{ url('/') }}/design/admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->		

        <!-- Bootstrap WYSIHTML5 -->

        <script src="{{ url('/') }}/design/admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->

        <script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

        <script src="{{ url('/') }}/design/admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>

		     

        <!-- CK Editor -->	

<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>


<script type="text/javascript">
	
$(function() {

	// Replace the <textarea id="editor1"> with a CKEditor

	// instance, using default configuration.

	CKEDITOR.replace('email_content');

	CKEDITOR.config.allowedContent = true; 

	CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);

	//bootstrap WYSIHTML5 - text editor

	$(".textarea").wysihtml5();

});

</script>




@stop