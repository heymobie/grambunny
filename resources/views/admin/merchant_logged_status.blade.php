@extends('layouts.admin')

@section("other_css")

        <!-- DATA TABLES -->

   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

@stop

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>


@section('content')

<!-- Right side column. Contains the navbar and content of the page -->

<aside class="right-side">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <h1>

            Merchants Logged In Status

        </h1>

        <ol class="breadcrumb">

            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

            <li class="active">Merchants Logged In Status</li>

        </ol>

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <div class="box">

                  

                    <div class="box-body table-responsive">

					

		 @if(Session::has('message'))

		 

		 <div class="alert alert-success alert-dismissable">

              <i class="fa fa-check"></i>

               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>

                           {{Session::get('message')}}

         </div>

		@endif


				
                    <table id="myTable11" class="table table-bordered table-hover">

                            <thead>

                                <tr>

                                    <th>ID#</th>

                                    <th>Name</th>
                                    <th>Email</th>

                                    <th>ContactNumber</th>
                                    <th>City</th>

                                    <th>Image</th>

                                    <th>Status</th>

                                    <th>Action</th>

                                </tr>

                            </thead>

                            <tbody>										

						 <?php $i=1; ?>

							 @foreach ($vendor_list as $list)

                                <tr>

                                    <td>{{ $list->vendor_id }}</td>

                                    <td>{{ $list->name }} {{ $list->last_name }}</td>
                                    <td>{{ $list->email }}</td>
                                    <td>{{ $list->contact_no }}</td>
                                    <td>{{ $list->city }}</td>
                                    <td>

									@if(!empty($list->profile_img1))

								
								<img src="{{ url('/') }}/public/uploads/vendor/profile/{{ $list->profile_img1 }}" width="50px;" height="50px;">

								@endif	

									

    							</td>

                                <td>

    							@if($list->login_status==1) 

    							<span class="label label-success">Logged In</span>

    							 @else 

    							<span class="label label-danger">Logged Out</span>@endif

    							</td>

                  <td>

								<a href="{{url('admin/merchant-logged-status-change/')}}/{{ $list->vendor_id }}" class="btn btn-primary">

								@if($list->login_status==1) OFF  @else ON @endif

                </a>  
																	 
								</td>

                </tr> 										

						 <?php $i++; ?>

						@endforeach	                                      

							</tbody>

                            <!-- <tfoot>

                                <tr>

                                    <th>&nbsp;</th>

                                    <th>&nbsp;</th>                     

									<th>&nbsp;</th>                            

									<th>&nbsp;</th>                           

									<th>&nbsp;</th>

                                </tr>

                            </tfoot> -->

                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->

            </div>

        </div>



    </section><!-- /.content -->

</aside><!-- /.right-side -->



@stop



@section('js_bottom')



        <!-- jQuery 2.0.2 -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

        <!-- Bootstrap -->

        <script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->

        <!-- <script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

        <script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script> -->
        <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
        <!-- AdminLTE App -->
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
        <!-- page script -->

        <script type="text/javascript">

            // $(function() {

            //     $("#example1").dataTable();

            //     $('#example2').dataTable({

            //         "bPaginate": true,

            //         "bLengthChange": false,

            //         "bFilter": true,

            //         "bSort": true,

            //         "bInfo": true,

            //         "bAutoWidth": false

            //     });

            // });

            $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });

        </script>

@stop

