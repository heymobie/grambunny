<style type="text/css">

.fogpass{

    display: inline-block;

    /* border: 1px solid red; */

    margin: -10px;

    float: left;

    margin-left: 32px;

}

</style>



@extends('layouts.adminapp')

@section('content')
<div class="form-box">
<div class="inner-l" id="login-box">

    <div class="header">Sign in</div>

    <form  role="form" id="signin" method="POST" action="{{ url('/admin/login') }}">
       <!-- @csrf -->

       {{ csrf_field() }}

       <div class="body bg-gray">



        @if(Session::has('PassSucChange'))

        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button>

            {{ Session::get('PassSucChange') }}

        </div>

        @endif

        @if(session()->has("success"))

                    <div class="alert alert-success" role="alert">

                        <strong>{{ session()->get("success") }} </strong>

                    </div>

                    @elseif(session()->has("warning"))

                    <div class="alert alert-warning" role="alert">

                        <h4 class="alert-heading">Account In Review!</h4>

                        <p>{{  session()->get("warning")}} </p>

                    </div>

                    @endif



        @if(Session::has('login_message_error'))

        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button>

            {{ Session::get('login_message_error') }}

        </div>

        @endif


        @if (Session::has('message'))

         <span class="help-block" style="text-align: center;">
         <strong style="color:#FF0000">{{ Session::get('message') }}</strong>
         </span>

        @endif


        @if ($errors->has('email'))

        <span class="help-block">

            <strong style="color:#FF0000">{{ $errors->first('email') }}</strong>

        </span>

        @endif

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

         <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>" required="required">

     </div>

     <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">

        <input id="password" type="password" placeholder="Password" class="form-control" name="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>" required="required">

        @if ($errors->has('password'))

        <span class="help-block">

            <strong>{{ $errors->first('password') }}</strong>

        </span>

        @endif

    </div>

    <div>

     <div class="form-group fogpass" style="width: 50%">

    <input type="checkbox" id="remember" name="remember" <?php if(isset($_COOKIE["remember"])) { echo 'checked'; } ?>/> Remember me

        </div>

        <div class="form-group fogpass" style="width: 50%">

            <a href="{{url('/admin/password/reset_password')}}">

                Forgot Password ?

            </a>

        </div>

    </div>



                </div>

                <div class="footer">

                <input type="submit" id="sign_in" class="btn bg-olive btn-block" value="Sign in">

                    <!-- <p><a href="{{ url('/admin/password/reset') }}">I forgot my password</a></p>-->

                </div>

            </form>

        </div>

        </div>
        @endsection

