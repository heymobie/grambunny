<style type="text/css">
.fogpass{
    display: inline-block;
    /* border: 1px solid red; */
    margin: -10px;
    float: left;
    margin-left: 32px;
}
</style>
@extends('layouts.adminapp')
@section('content')
<div class="form-box" id="login-box">
    <div class="header">Reset your password</div>
    <form  role="form" autocomplete="off" id="setNewPass" method="POST" action="{{ url('/admin/password/reset_your_password') }}">
       {{ csrf_field() }}
       <div class="body bg-gray">
        <input type="hidden" class="form-control" name="AdminID" value="{{ Session::get('adminID') }}">
        @if(Session::has('Succes'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Succes') }}
        </div>
        @endif
        @if (Session::has('Change_pass'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Change_pass') }}
        </div>
        @endif
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <input type="password" class="form-control" placeholder="New Password" id="new_pass" name="new_pass"  required="required" >
        </div>
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" placeholder="Confirm Password" class="form-control" name="confirm_pass" required="required" >
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    <!-- <div>
        <div class="form-group fogpass">
            <input type="checkbox" name="remember"/> Remember me
        </div>
        <div class="form-group fogpass">
            <a href="{{url('/admin/password/reset_password')}}">
                Forgot Password ?
            </a>
        </div>
    </div> -->
</div>
<div class="footer">
    <button type="submit" id="sign_in" class="btn bg-olive btn-block">Update password</button>
    <!-- <p><a href="{{ url('/admin/password/reset') }}">I forgot my password</a></p>-->
</div>
</form>
</div>
@endsection

<!-- form validation start -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
       $("#setNewPass").validate({
        rules: {
            new_pass:{
               required:true,
               minlength:6,
           },
           confirm_pass: {
            required: true,
            minlength: 6,
            equalTo : "#new_pass",
        }
    },
    messages: {
        new_pass:{
           required:'Please enter a new passwordsdfsd.',
           minlength:'Password must be at least 6 characters.'
       },
       confirm_pass:{
        required:'Please enter confirm password.',
        minlength:'Password must be at least 6 characters.',
        equalTo:'confirm password not match, please enter correct.'
    },
}
});
   });
</script>
<!-- form validation end -->