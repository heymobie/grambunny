<style type="text/css">
.fogpass{
    display: inline-block;
    /* border: 1px solid red; */
    margin: -10px;
    float: left;
    margin-left: 32px;
}
</style>

@extends('layouts.adminapp')
@section('content')
<div class="form-box">
<div class="" id="login-box">
    <div class="header">Forgot your password?</div>
    <form  role="form" id="forgetPass_Email" method="POST" action="{{ url('/admin/password/reset_password') }}">
     {{ csrf_field() }}
     <div class="body bg-gray">

        @if(Session::has('Succes'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Succes') }}
        </div>
        @endif

        @if (Session::has('Error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{ Session::get('Error') }}
        </div>
        @endif


        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
         <input id="email" type="email" class="form-control" placeholder="Email" name="email">
     </div>
     <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        <!-- <input id="password" type="password" placeholder="Password" class="form-control" name="password" required="required"> -->
        @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <!-- <div>
        <div class="form-group fogpass">
            <input type="checkbox" name="remember"/> Remember me
        </div>
        <div class="form-group fogpass">
            <a href="{{url('/admin/password/reset_password')}}">
                Forgot Password ?
            </a>
        </div>
    </div> -->
</div>
<div class="footer">
    <button type="submit" id="resetEmail" class="btn bg-olive btn-block">Send OTP</button>
    <!-- <p><a href="{{ url('/admin/password/reset') }}">I forgot my password</a></p>-->
</div>
</form>
</div>
</div>
@endsection


<!-- form validation start -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
       $("#forgetPass_Email").validate({
        rules: {
            email:{
               required:true,
               email:true,
           }
       },
       messages: {
        email:{
           required:'Please enter email address.',
           email:'Please enter valid email address.',
       }
   }
});
   });
</script>
<!-- form validation end -->