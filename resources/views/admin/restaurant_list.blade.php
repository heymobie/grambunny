@extends('layouts.admin')

@section("other_css")

	<!-- DATA TABLES -->

	<link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

	<meta name="_token" content="{!! csrf_token() !!}"/>

@stop

@section('content')

	<!-- Right side column. Contains the navbar and content of the page -->

	<aside class="right-side">

		<!-- Content Header (Page header) -->

		<section class="content-header">

			<h1>Product Service Management</h1>

			<ol class="breadcrumb">

				<li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>

				<li class="active">Product Service Management</li>

			</ol>

		</section>

		<!-- Main content -->

		<section class="content">

			<div class="row">

				<div class="col-xs-12">

					<div class="box">

						<div class="box-header">

							<h3 class="box-title">Product Service Management</h3>

							<!--<div style="float:right; margin-right:10px; margin-top:10px;">

								<a href="{{url('admin/restaurant-form')}}" class="btn btn-primary" style="color:#FFFFFF"> Add New Restaurant</a>

							</div>-->

						</div><!-- /.box-header -->

						<!--<div class="box-body table-responsive">

						<form name="search_frm" id="search_frm" method="post" action="administrator/property-list" onsubmit="return check_search();">

						<div >

						<table cellpadding="5" cellspacing="5" width="100%" style="border:1px solid #999999" class="table">

						<tr><td width="50%">

						<table cellpadding="5" cellspacing="5" width="50%">

						<tr>

						<td>Restaurant Cont No: </td>

						<td ><input type="text" name="rest_cont" id="rest_cont" class="form-control" value="" />

						</td>

						</tr>

						<tr>

						<td>Restaurant Name</td>

						<td ><input type="text" name="rest_name" id="rest_name" class="form-control" value="" />

						</td>

						</tr>

						<tr>

						<td>&nbsp;</td>

						<td>

						<div id="error_msg" style="color:#FF0000; display:none;"></div>

						<input class="btn btn-primary" type="button" name="search_btn" id="search_btn" value="SEARCH" onclick="check_frm()" /></td>

						</tr>

						</table>

						</td>

						</tr>

						</table>

						</div>

						</form>

						</div>-->

						<!-- /.box-body -->

					</div><!-- /.box -->

					<div>

						<section>

							<div class="row">

								<div class="my-panel-data">

									<div class="col-xs-12">

										<div class="box">

											<!--<div class="box-body table-responsive">

												<div id="restaurant_list">

													<table id="example2" class="table table-bordered table-hover">

													<thead>

													<tr>

													<th>Sr. No.</th>

													<th>Logo</th>

													<th>Name (English)</th>

													<th>Name (Arabic)</th>

													<th>Vendor Name</th>

													<th>Address</th>

													<th>Status</th>

													<th>Action</th>

													</tr>

													</thead>

													<tbody>

													<?php $i=1; ?>

													@if(!empty($rest_list))

													@foreach ($rest_list as $list)

													<tr>

													<td>{{ $i }}</td>

													<td>

													<?php if($list->google_type==0){?>

													@if(!empty($list->rest_logo))

													<img src="{{ url('/') }}/uploads/reataurant/{{ $list->rest_logo }}" width="50px;" height="50px;">

													@else

													<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >

													@endif

													<?php }elseif($list->google_type==1){?>

													@if(!empty($list->rest_logo))

													<img src="{{ $list->rest_logo }}" width="50px;" height="50px;">

													@else

													<img src="{{ url('/') }}/design/front/img/logo.png" width="50px;" >

													@endif

													<?php }?>

													</td>

													<td>{{ $list->rest_name}}</td>

													<td>{{ $list->rest_name_ar}}</td>

													<td>{{ $list->name }} </td>

													<td>{{ $list->rest_address }}</td>

													<td>

													@if($list->rest_status=='PUBLISHED')

													<span class="label label-primary">PUBLISHED</span>

													@elseif($list->rest_status=='ACTIVE')

													<span class="label label-danger">{{$list->rest_status}}</span>

													@elseif($list->rest_status=='INACTIVE')

													<span class="label label-danger">{{$list->rest_status}}</span>

													@endif

													</td>

													<td>

													<?php if($list->google_type==0){?>

													<a title="Edit" href="{{url('admin/restaurant-form/')}}/{{ $list->rest_id }}"><i class="fa fa-edit"></i></a>

													<a title="View" href="{{url('admin/restaurant_view/')}}/{{ $list->rest_id }}"><i class="fa fa-eye"></i></a>

													<?php }?>



													<a title="Delete Restaurant"

													href="{{url('admin/restaurant_delete')}}/{{ $list->rest_id }}"

													onclick="return delete_parent()">

													<i class="fa fa-trash-o"></i>

													</a>



													</td>

													</tr>

													<?php $i++; ?>

													@endforeach

													@endif

													</tbody>

													<tfoot>

													<tr>

													<th>&nbsp;</th>

													<th>&nbsp;</th>

													<th>&nbsp;</th>

													<th>&nbsp;</th>

													<th>&nbsp;</th>

													<th>&nbsp;</th>

													</tr>

													</tfoot>

													</table>

												</div>

											</div>-->

										</div>

									</div>

								</div>

							</div>

						</section>

					</div>

				</div>

			</div>

		</section><!-- /.content -->

	</aside><!-- /.right-side -->

	@stop

	@section('js_bottom')

	<style>

	#ajax_favorite_loddder {

	position: fixed;

	top: 0;

	left: 0;

	width: 100%;

	height: 100%;

	background:rgba(27, 26, 26, 0.48);

	z-index: 1001;

	}

	#ajax_favorite_loddder img {

	top: 50%;

	left: 46.5%;

	position: absolute;

	}

	.footer-wrapper {

	float: left;

	width: 100%;

	/*display: none;*/

	}

	#addons-modal.modal {

	z-index: 999;

	}

	.modal-backdrop {

	z-index: 998 !important;

	}

	</style>

	<div id="ajax_favorite_loddder" style="display:none;">

	<div align="center" style="vertical-align:middle;">

	<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />

	</div>

	</div>

	<!-- jQuery 2.0.2 -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

	<!-- Bootstrap -->

	<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>

	<!-- DATA TABES SCRIPT -->

	<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

	<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

	<!-- AdminLTE App -->

	<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- page script -->

	<script src="https://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>

	<script type="text/javascript">

	$(function() {

	$("#example1").dataTable();

	$('#example2').dataTable({

	"bPaginate": true,

	"bLengthChange": false,

	"bFilter": true,

	"bSort": true,

	"bInfo": true,

	"bAutoWidth": false

	});

	});

	function check_frm()

	{

	$('#error_msg').hide();

	var form = $("#search_frm");

	if(($("#rest_cont").val()!='') || ($("#rest_name").val()!=''))

	{

	$("#ajax_favorite_loddder").show();

	var frm_val = $('#search_frm').serialize();

	$.ajax({

	type: "POST",

	url: "{{url('/admin/restaurant_search')}}",

	data: frm_val,

	success: function(msg) {

	$("#ajax_favorite_loddder").hide();

	$('#restaurant_list').html(msg);

	}

	});

	}

	else

	{

	//alert('Please insert any one value');

	$('#error_msg').html('Please insert any one value.');

	$('#error_msg').show();

	return false;

	}

	}

	$.ajaxSetup({

	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }

	});

	</script>

@stop