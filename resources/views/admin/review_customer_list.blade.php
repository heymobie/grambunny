@extends('layouts.admin')

@section("other_css")
        <!-- DATA TABLES -->
   <link href="{{ url('/') }}/design/admin/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <style>
  .popup_divcall{
	  margin: 10px;  
  }

    div#addons_modal_data {
    height: 160px;
    padding: 13px;
    margin-top: 125px;
}

  </style>
   <meta name="_token" content="{!! csrf_token() !!}"/>

@stop

@section('content')
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Customer Review & Rating  List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Review List</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
		 @if(Session::has('message'))
		 
		 <div class="alert alert-success alert-dismissable">
              <i class="fa fa-check"></i>
               <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                           {{Session::get('message')}}
         </div>
		@endif
		<div class="col-xs-12">
          
				<div>
				<section>
				 	<div class="row">
						<div class="my-panel-data">
							 <div class="col-xs-12">
							    <div class="box">
									<div class="box-body table-responsive">
										<div style="float:right; margin:5px;"> 
									
									    </div>
										<div id="restaurant_list">							
											 <table id="myTable11" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>User name</th>
									<th>Merchant Name</th>
                                    <th>Status</th>
                                    <th>Comment</th>
                                    <th>Rating</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>										
						 <?php $i=1; ?>
							 @foreach($review_detail as $list)

	<?php 

		$uname = DB::table('users')->where('id', '=', $list->user_id)->value('name');
		$ulname = DB::table('users')->where('id', '=', $list->user_id)->value('lname');
		$mname = DB::table('vendor')->where('vendor_id', '=', $list->merchant_id)->value('name');
		$mlname = DB::table('vendor')->where('vendor_id', '=', $list->merchant_id)->value('last_name'); 
	?>

                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $uname }} {{ $ulname }}</td>
                                    <td>{{ $mname }} {{ $mlname }}</td>
                                    <td>@if($list->status==0)Unpublish @endif @if($list->status==1)Publish @endif </td>
                                    <td>{{ $list->review }}</td>
                                    <td>{{ $list->rating }}</td>
                                    <td>
									 	 <a title="View" href="javascript:void(0)" data-toggle="modal" data-target="#update_review-{{$list->id}}"><i class="fa fa-eye"></i></a>
										   <a title="Delete User" href="{{url('admin/review_customer_delete')}}/{{$list->id}}" onclick="return delete_wal()"><i class="fa fa-trash-o"></i></a>  
									
									<div class="modal fade" id="update_review-{{$list->id}}" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog">
									<div class="modal-content" id="addons_modal_data">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title"><i class="fa fa-envelope-o"></i> User Review</h4>
									</div>

									<form action="{{url('admin/review_customer_update/')}}/{{$list->id}}" method="post">
										<input type="hidden" name="re_id" id="re_id" value="{{$list->id}}" />

										{!! csrf_field() !!}

										<select name="revew_status" data-reid="{{$list->id}}" id="revew_status" class="form-control">
											@if($list->status==0)
											<option value="1">Publish</option>
											 @endif
                                            @if($list->status==1)
											<option value="0">Unpublish</option>
											@endif
										</select>
										<button type="submit" class="btn btn-primary pull-left">Update Status</button>
									</form>

									

									</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
									</div>

									</td>
                                </tr> 										
						 <?php $i++; ?>
						@endforeach	                                      
							</tbody>
                           <!--  <tfoot>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th> 
									<th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th> 
									<th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th> 
									<th>&nbsp;</th>
                                </tr>
                            </tfoot> -->
                        </table>	
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</section>					
			  </div>
            </div>
		
            
        </div>

    </section><!-- /.content -->
</aside><!-- /.right-side -->

@stop

@section('js_bottom')
<style>

#ajax_favorite_loddder {

position: fixed;
top: 0;
left: 0;
width: 100%;
height: 100%;
background:rgba(27, 26, 26, 0.48);
z-index: 1001;
}
#ajax_favorite_loddder img {
top: 50%;
left: 46.5%;
position: absolute;
}

.footer-wrapper {
    float: left;
    width: 100%;
    /*display: none;*/
}
#addons-modal.modal {
	z-index: 999;
}
.modal-backdrop {
	
	z-index: 998 !important;
}
</style>	
	
<div id="ajax_favorite_loddder" style="display:none;">
	<div align="center" style="vertical-align:middle;">
		<img src="{{ url('/') }}/design/admin/img/ajax-loader.gif" />
	</div>
</div>

<!-- jQuery 2.0.2 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{ url('/') }}/design/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/design/admin/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ url('/') }}/design/admin/js/AdminLTE/app.js" type="text/javascript"></script>
<!-- page script -->
<script src="{{ url('/') }}/design/admin/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>		
		
<script type="text/javascript">
    // $(function() {
    //     $("#example1").dataTable();
    //     $('#example2').dataTable({
    //         "bPaginate": true,
    //         "bLengthChange": false,
    //         "bFilter": false,
    //         "bSort": true,
    //         "bInfo": true,
    //         "bAutoWidth": false
    //     });				
    // });
	$( function() {
	  	var dateToday = new Date();
	    $( ".datepicker" ).datepicker({
			 dateFormat: 'yy-mm-dd',
			 autoclose: true
		});
	});	
	$(document).on('change', '#revew_status', function(){
		var value = $(this).find('option:selected').val();
		var aat_val = $(this).attr('data-reid');		
		
		if(value=='REJECT')	
		{		
			$("#Reason_reject"+aat_val).show();	
		}
		else
		{	
			$("#Reason_reject"+aat_val).hide();	
		}
	}); 		
		  
	$(document).on('click', '#search_btn', function(){ 
		  
		$('#error_msg').hide();
	 
		var frmdate = $('#fromdate').val(); 
		var todate = $('#todate').val(); 
	
		if(((frmdate!='') && (todate!='')) && (new Date(frmdate) >= new Date(todate)))
		{//compare end <=, not >=
			//your code here
			//alert("From date will be big from to date!");	
			
			$('#error_msg').html('From date will be big from to date.');
			$('#error_msg').show()
		}
		else
		{				
		 	$("#ajax_favorite_loddder").show();	
			var frm_val = $('#search_frm').serialize();				
			$.ajax({
				type: "POST",
				url: "{{url('/admin/review_search_list')}}",
				data: frm_val,
				success: function(msg) {
					$("#ajax_favorite_loddder").hide();	
					//alert(msg)
					$('#restaurant_list').html(msg);
				}
			});
		}		
	}); 		
			

	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});			
</script>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <!--Data Table-->
    <script type="text/javascript"  src=" https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript"  src=" https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>

    <!--Export table buttons-->
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js" ></script>
    <script type="text/javascript"  src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>

<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">

<script type="text/javascript">
 $('#myTable11').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    });
</script>
@stop
