
    <input type="hidden" value="@if(!empty($today_date1[0]->y)){{ $today_date1[0]->y }}@endif" id="order8">

      <input type="hidden" value="@if(!empty($today_date1[0]->x)){{ $today_date1[0]->x }}@endif" id="order108">

      <input type="hidden" value="@if(!empty($today_user[0]->user_id)){{ $today_user[0]->user_id }}@endif" id="order107">
      
       <input type="hidden" value="@if(!empty($today_merchant[0]->vendor_id)){{ $today_merchant[0]->vendor_id }}@endif" id="order106">

       <input type="hidden" value="@if(!empty($user_id)){{ $user_id }}@endif" id="user_id_001">
        <input type="hidden" value="@if(!empty($yesterday)){{ $yesterday }}@endif" id="orderid_002">
         
        <input type="hidden" value="@if(!empty($one_month)){{ $one_month }}@endif" id="orderid_003">

       <input type="hidden" value="@if(!empty($today)){{ $today }}@endif" id="orderid_001">
<?php //print_r($today);die;?>
      
<div>
    <canvas id="line-chart-user" width="500" height="150"></canvas>
 </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@4.0.1/dist/chart.umd.min.js"></script>
<script>
var orderid_001 = $( "#orderid_001" ).val();
var orderid_002 = $( "#orderid_002" ).val();
var orderid_003 = $( "#orderid_003" ).val();

  if(orderid_001=="1_user"){

  var order8 = $( "#order8" ).val();
  var order109 = $( "#order109" ).val();
  var order107 = $( "#order107" ).val();
  var order108 = $( "#order108" ).val();

    var order106 = $( "#order106" ).val();
    new Chart(document.getElementById("line-chart-user"), {

      type : 'line',
      data : {
        labels : [order108],
        datasets : [
           
            {
              data : [ order8],
              label : "New Customer",
              borderColor : "#545454",
              fill : false
            },
            {
              data : [ order106],
              label : "New Merchant",
              borderColor : "#6495ED",
              fill : false
            },
            ]
      },
   
      options : {
        title : {
          display : true,
          text : ''
        }
      }
   
    });
  }else if(orderid_002=="2_user"){

  var order8 = $( "#order8" ).val();
  var order106 = $( "#order106" ).val();
  var order109 = $( "#order109" ).val();
  var order107 = $( "#order107" ).val();
  var order108 = $( "#order108" ).val();
    new Chart(document.getElementById("line-chart-user"), {

      type : 'line',
      data : {
        labels : [ order108],
        datasets : [

            {
              data : [ order107],
              label : "New Customer",
              borderColor : "#545454",
              fill : false
            },
            {
              data : [ order106],
              label : "New Merchant",
              borderColor : "#6495ED",
              fill : false
            }
           
           
            ]
      },
   
      options : {
        title : {
          display : true,
          text : ''
        }
      }
   
    });

  }else if(orderid_003=="3_user"){

    var user_id_001 = $( "#user_id_001" ).val();

   new Chart(document.getElementById("line-chart-user"), {

      type : 'line',
      
      data : {
        
        labels : [@foreach ($today_date1 as $res1)

           "{{$res1}}",

            @endforeach],
     
        datasets : [
          
            {
              data : [ @foreach ($today_user as $res11)

           {{($res11['user_id'])}},

            @endforeach

             ],
              label : "New Customer",
              borderColor : "#545454",
              fill : false
            },

             {
              data : [ 

                @foreach ($today_merchant as $res)
              
               {{($res['vendor_id'])}},
             
             @endforeach
           
             ],
              label : "New Merchant",
              borderColor : "#6495ED",
              fill : false
            },
            ]
      },

      options : {
        title : {
          display : true,
          text : ''
        }
      }
    });
   
  // });
  }else{

     new Chart(document.getElementById("line-chart-user"), {

      type : 'line',
      
      data : {
        
        labels : [ 

            @foreach ($today_date1 as $res)

            "{{$res}}", 
           
            @endforeach
         ],
     
        datasets : [
            {
            
            data : [  @foreach ($today_user as $res11)

           {{($res11['user_id'])}},

            @endforeach

            ],
           
             label : "New Customer",
              borderColor : "#545454",
              fill : false
            },

           {
              data : [ 

                @foreach ($today_merchant as $res)
              
               {{($res['vendor_id'])}},
             
             @endforeach
           
             ],
              label : "New Merchant",
              borderColor : "#6495ED",
              fill : false
            },
           
            ]
      },

      options : {
        title : {
          display : true,
          text : ''
        }
      }
    });

    
  }
    //$(".filter1").show();
   $(".chart1").hide();
   //$(".filter1").show();
 
</script>