@extends("layouts.grambunny")
@section("content")
<section id="ordr-grab-sec">
  <div class="container">
    <div class="row  wow fadeInUp" data-wow-delay="0.1s">
      <div class="col-lg-12">
        <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
          <!-- <span>Grab it features //</span> -->
          <h2>{{@$page_data->page_title}}</h2>
          <p>{{@$page_data->sub_title}}</p>
        </div>
      </div>
    </div>

  </div>
</section>

<section id="faq" class="">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="web-heading wow bounceIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: bounceIn;">
                  <?php echo $page_data->page_content; ?>
 
            </div>          
          </div>
        </div>

        <div class=" justify-content-center" id="faq-list">
                  <div class="col-lg-12 col-md-12">		  	  				  				  <?php $i=1; foreach($faq_data as $list){ ?>				  				                       <li class="wow fadeInUp" data-wow-delay="0.{{$i}}s">                        <a data-toggle="collapse" class="collapsed" href="#faq{{$i}}" aria-expanded="true"><span>{{$i}}</span>{{$list->title}} <i class="fa fa-angle-down"></i></a>                        <div id="faq{{$i}}" class="collapse show" data-parent="#faq-list">                          <p>                            {{$list->content}}                          </p>                        </div>                      </li>					  				  				  <?php $i++;} ?>

                  </div>
                </div>
      </div>
</section>

@endsection
