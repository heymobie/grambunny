@extends('layouts.grambunny')
@section('styles')
    <style type="text/css">
        span.catactive {
            color: #ed1c24;
            font-weight: bold;
        }

        .prduc_img {
            float: left;
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 172px !important;
            margin-top: 10px;
            overflow: hidden;
        }

        .bannerimg {
            width: 100%;
            height: 480px;
            overflow: hidden;
        }

        .bannerimg img {
            width: 100%;
            object-fit: cover;

        }

        @media only screen and (max-width: 767px) and (min-width: 320px) {
            .bannerimg {
                width: 100%;
                height: auto;
                overflow: hidden;
                object-fit: cover;
            }

        }

        @media screen and (orientation:portrait) {

            .bannerimg {
                width: 100%;
                height: auto;
                overflow: hidden;
                object-fit: cover;
            }

            .bannerimg {
                width: 100%;
                height: auto !important;
                overflow: hidden;
            }
        }

        @media screen and (orientation:landscape) {

            .bannerimg {
                width: 100%;
                height: auto !important;
                overflow: hidden;
            }

            .addtocart {
                width: 30px !important;
                float: right;
                position: relative;
                top: 5px;
            }
        }


        .product-bann .carousel-inner {
            position: relative;
            width: 100%;
            overflow: hidden;

        }

        .product-bann .carousel-item img {
            width: 100%;
            height: 480px;
            object-fit: fill;
        }

        @media only screen and (max-width: 1000px) and (min-width: 769px) {
            .product-bann .carousel-inner {
                position: relative;
                width: 100%;
                overflow: hidden;

            }

            .product-bann .carousel-inner img {
                height: 100% !important;
            }

            .product-bann .carousel-item {
                height: 300px;
            }

        }

        @media only screen and (max-width: 768px) and (min-width: 320px) {
            .product-bann .carousel-inner {
                position: relative;
                width: 100%;
                overflow: hidden;
                height: 175px !important;
            }

            .product-bann .carousel-inner img {
                height: 100% !important;
            }

            .product-bann .carousel-item {
                height: 175px;
            }

        }

        @media screen and (min-width:320px) and (max-width: 767px) and (orientation: landscape) {

            .product-bann .carousel-item img {
                width: 100%;
                height: 100%;
                object-fit: fill;
            }

            .product-bann .carousel-inner {
                position: relative;
                width: 100%;
                overflow: hidden;
                height: 300px;

            }
        }
    </style>
@endsection
@section('content')
    <link rel="stylesheet" type="text/css" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/home.css') }}?var=<?php echo rand(); ?>">
    <div class=" top_search_sec overlay">

        <?php $baseurl = url('/'); ?>

        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <div id="carouselExampleControls" class="carousel product-bann slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?php echo $banner_image; ?>">
                            </div>
                            @if (!empty($banner_image_multiple))
                                @foreach ($banner_image_multiple as $value1)
                                    <div class="carousel-item">
                                        <img class="d-block w-100"
                                            src="{{ url('/') }}/public/uploads/advertisement/{{ $value1->banner_image_multiple }}"
                                            height="500" alt="Second slide">
                                    </div>
                                @endforeach
                            @endif
                            <!-- <div class="carousel-item">
          <img class="d-block w-100" src="https://www.grambunny.com/public/uploads/advertisement/L8dNrF1697329338.png" alt="Third slide">
        </div> -->
                        </div>
                        <!--   <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a> -->
                    </div>
                    <!-- <div class="bannerimg">
          <img src="<?php echo $banner_image; ?>">
          </div> -->
                </div>
            </div>

            <div class="row">
                <div class="bg_color-serach">
                    <div class="col-md-8">
                        <form class="home_page_banner">
                            <div class="col-md-6">
                                <input type="text" class="form-control divider_rght"
                                    placeholder="Search by Merchant Name" name="searchpbr" id="searchpbr"
                                    onkeyup="searchKeypbr()">
                                <span class="serach_icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control mb_mg_top"
                                    placeholder="Search by City or Zip Code"
                                    style="border-left:1px solid #232f3e !important;" name="searchcityz" id="searchcityz"
                                    onkeyup="searchKeypbr()">
                                <span class="serach_icon mb_icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 pl-0 pr-0">
                        <ul class="rgt_map_list">

                            <li class="currentloc" onClick="clFunction()"><a id="currentloc" class=""
                                    style="display: block; cursor:pointer"><i class="fa fa-dot-circle-o"
                                        aria-hidden="true"></i> Current Location</a></li>

                            <li class="map-view" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;"><a
                                    id="mapview" class="" style="display: block; cursor:pointer">Map View</a></li>

                            <li class="list_view"><a id="listview" style="display: block; cursor:pointer">List View</a>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <category secttion start> -->

    <div id="ajaxdatacall">

        <!-- <category secttion end> -->
        <!-- <map secttion start> -->
        <div class="container">
            <div class="row map-full-bg">
                <div class="col-md-12">
                    <div class="map">

                        <div class="welcome-area " style="margin-top:0 !important;">

                            <div id="map"></div>


                            <div class="container">
                                <div class="list_view_bg_sect">

                                    <?php $storedata = [];
                                    $vdistance = ''; ?>

                                    <?php foreach ($verdorslist as $key => $value) {

            if($value->map_icon){

                 $mapicon = $value->map_icon;

                }else{

                  $mapicon = 'map.png';

                }

            ?>

                                    <?php $storedata[] = ['name' => "'" . $value->username . "'", 'location' => ['lat' => $value->lat, 'lng' => $value->lng], 'icon' => "'" . $mapicon . "'"]; ?>

                                    <?php foreach ($results as $key => $revalue) {
                                        if ($revalue->vendor_id == $value->vendor_id) {
                                            $vdistance = $revalue->distance;
                                        }
                                    } ?>

                                    <div class="col-md-2 listing_width_eq equal-col dltv pilist" id="listview"
                                        style="display:none;">
                                        <div class="sf-search-result-girds" id="proid-30">
                                            <div class="sf-featured-top">
                                                <a href="<?php echo $baseurl; ?>/{{ $value->username }}/store">
                                                    <div class="prduc_img">
                                                        <?php if(!empty($value->profile_img1)) { ?>
                                                        <img
                                                            src="<?php echo $baseurl; ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                                                        <?php }else{ ?>
                                                        <img src="https://www.grambunny.com/public/uploads/user.jpg">
                                                        <?php }?>
                                                    </div>
                                                    <!-- <div class="sf-featured-media" style="background-image:url(<?php echo $baseurl; ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }})"></div> -->
                                                    <!-- <div class="sf-overlay-box"></div> -->
                                                    <div class="descrption_set">
                                                        <!-- <div class="sf-featured-provider">{{ $value->name }} {{ $value->last_name }} </div> -->

                                                        <div class="sf-featured-provider">{{ $value->business_name }}
                                                        </div>

                                                        <div class="sf-featured-address">{{ $value->description }}</div>

                                                        <div class="sf-featured-provider"><i aria-hidden="true"
                                                                class="fa fa-check-circle"></i> {{ $value->username }}
                                                        </div>

                                                        <div class="sf-featured-address"><i
                                                                class="fa fa-map-marker"></i>Distance :
                                                            <?php echo round($vdistance, 1); ?> Miles</div>

                                                        <?php $ratings = $value->avg_rating; ?>
                                                        <div class="star-rating">

                                                            <div class="rating">

                                                                <?php

                                                                if ($ratings == 1) {
                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';
                                                                } elseif ($ratings == 2) {
                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';
                                                                } elseif ($ratings == 3) {
                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';
                                                                } elseif ($ratings == 4) {
                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';
                                                                } elseif ($ratings == 5) {
                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';

                                                                    echo '<i class="fa fa-star"></i>';
                                                                } elseif ($ratings == '0') {
                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';

                                                                    echo '<i class="fa fa-star-o"></i>';
                                                                }

                                                                ?>

                                                                <!--<span>5.0(2)</span>-->
                                                            </div>

                                                            <input type="hidden" name="whatever1" class="rating-value"
                                                                value="2.56">
                                                        </div>

                                                    </div>

                                                </a>
                                            </div>
                                            <!--             <div class="sf-featured-bot">
                   <div class="sf-featured-comapny">{{ $value->business_name }}</div>
                   <div class="sf-featured-text"></div>
                   <div class="btn-group sf-provider-tooltip">
                      <a href="<?php echo $baseurl; ?>/{{ $value->username }}/store" class="text-dark">View Store</a>
                   </div>
                </div> -->
                                        </div>
                                    </div>

                                    <?php } ?>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php $drivermap = json_encode($storedata);
        $drivermaps = str_replace('"', '', $drivermap); ?>

        <!-- <baaner ands & product secttion start> -->

        <div class="container">
            <div class="row">

                <div class="b_add">
                    <div class="col-md-6">
                        <div class="adds_banner">
                            @if ($advertisement1)
                                <img src="{{ asset('public/uploads/advertisement/' . $advertisement1->image) }}">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 ads_rgt">
                        <div class="adds_banner">
                            @if ($advertisement2)
                                <img src="{{ asset('public/uploads/advertisement/' . $advertisement2->image) }}">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="alert alert-success" id="addcartalt"
                    style="display: none; color: green;background: lightyellow;">
                    <button type="button" class="close" id="closeaddtoc">&times;</button>
                    <strong id="msgaddtocart">Product Added Successfully!</strong>
                </div>


                <div class="product-listing-section">
                    <div class="row row_listing">
                        <div class="col-md-12">

                            <div class="list_view_bg_sect" id="list_view_bg_sect">

                                <?php $storedata = [];
                                $vdistance = ''; ?>

                                <?php foreach ($verdorslist as $key => $value) {

           foreach ($results as $key => $revalue) {

            if($revalue->vendor_id==$value->vendor_id){

              $vdistance = $revalue->distance;

             }

            } ?>

                                <div class="col-md-2 listing_width_eq equal-col pilist" id="listviewscroll">
                                    <div class="sf-search-result-girds" id="proid-30">
                                        <div class="sf-featured-top">
                                            <a href="<?php echo $baseurl; ?>/{{ $value->username }}/store">
                                                <div class="prduc_img">
                                                    <?php if(!empty($value->profile_img1)) { ?>
                                                    <img
                                                        src="<?php echo $baseurl; ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}">

                                                    <?php }else{ ?>
                                                    <img src="https://www.grambunny.com/public/uploads/user.jpg">
                                                    <?php }?>
                                                </div>

                                                <div class="descrption_set">

                                                    <div class="sf-featured-provider">{{ $value->business_name }} </div>

                                                    <div class="sf-featured-address">{{ $value->description }}</div>

                                                    <div class="sf-featured-provider"><i aria-hidden="true"
                                                            class="fa fa-check-circle"></i> {{ $value->username }} </div>

                                                    <div class="sf-featured-address"><i
                                                            class="fa fa-map-marker"></i>Distance : <?php echo round($vdistance, 1); ?>
                                                        Miles</div>

                                                    <?php $ratings = $value->avg_rating; ?>
                                                    <div class="star-rating">

                                                        <div class="rating">

                                                            <?php

                                                            if ($ratings == 1) {
                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';
                                                            } elseif ($ratings == 2) {
                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';
                                                            } elseif ($ratings == 3) {
                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';
                                                            } elseif ($ratings == 4) {
                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';
                                                            } elseif ($ratings == 5) {
                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';

                                                                echo '<i class="fa fa-star"></i>';
                                                            } elseif ($ratings == '0') {
                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';

                                                                echo '<i class="fa fa-star-o"></i>';
                                                            }

                                                            ?>

                                                            <!--<span>5.0(2)</span>-->
                                                        </div>

                                                        <input type="hidden" name="whatever1" class="rating-value"
                                                            value="2.56">
                                                    </div>

                                                </div>

                                            </a>
                                        </div>

                                    </div>
                                </div>

                                <?php } ?>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <input type="hidden" name="pagenum" id="pagenum" value="1">
    <input type="hidden" name="totalvhp" id="totalvhp" value="{{ $total }}">

    <div class="col-md-12 load-btn"><button id="loadshow" class="round load-mor-btn" style="display: none;">Load
            More</button></div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var totalvrshp = $('#totalvhp').val();

            if (totalvrshp == 0) {

                $('#loadshow').hide();

            } else {

                $('#loadshow').show();

            }

            $('#loadshow').on('click', function() {

                $("#loadshow").html('<i class="fa fa-circle-o-notch fa-spin"> </i>Loading');

                var pagenum = $('#pagenum').val();

                var newpage = parseInt(pagenum) + 1;

                // code of searchajax function

                var x = document.getElementById("searchpbr");

                var xsearch = x.value;

                var citzi = document.getElementById("searchcityz");

                var xcitzi = citzi.value;

                var xcategory = '';

                if ($("input[name='category']:checked").val()) {

                    xcategory = $("input[name='category']:checked").val();

                }

                if (xsearch == '' && xcitzi == '' && xcategory == '') {

                    $.ajax({

                        type: "post",

                        headers: {
                            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                        },

                        data: {

                            "_token": "{{ csrf_token() }}",

                            "data": {
                                searchpbr: xsearch,
                                cityzip: xcitzi,
                                category: xcategory,
                                pages: newpage
                            },

                        },

                        url: "{{ url('/searchproduct') }}",

                        success: function(msg) {

                            $('#pagenum').val(newpage);

                            $('#list_view_bg_sect').append(msg);

                            $("#loadshow").html('Load More');

                            var totalvendor = $('#totalvendor').val();

                            if (totalvendor == 0) {

                                $('#loadshow').hide();

                            } else {

                                $('#loadshow').show();

                            }


                        }

                    });


                } else {

                    $.ajax({

                        type: "post",

                        headers: {
                            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                        },

                        data: {

                            "_token": "{{ csrf_token() }}",

                            "data": {
                                searchpbr: xsearch,
                                cityzip: xcitzi,
                                category: xcategory,
                                pages: newpage
                            },

                        },

                        url: "{{ url('/searchproduct') }}",

                        success: function(msg) {

                            $('#pagenum').val(newpage);

                            $('#list_view_bg_sect').append(msg);

                            $("#loadshow").html('Load More');

                            var totalvendor = $('#totalvendor').val();

                            if (totalvendor == 0) {

                                $('#loadshow').hide();

                            } else {

                                $('#loadshow').show();

                            }

                        }

                    });

                }

            });

        });

        $(document).ready(function() {
            $('#closeaddtoc').click(function(e) {
                $("#addcartalt").hide();
            });
        });

        function apply_qty(productid) {

            var qty = 1;

            var product_id = productid;

            var vendor_id = $('#vendor_id').val();

            var purl = $('#purl').val();

            $.ajax({

                type: "post",

                headers: {

                    'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

                },

                data: {

                    "_token": "{{ csrf_token() }}",

                    "qty": qty,

                    "product_id": product_id,

                    "vendor_id": vendor_id

                },

                url: "{{ url('/addtocart') }}",

                success: function(msg) {

                    $('#session_qty').text("(" + msg.cartcount + ")");

                    $(".dcart a").prop("href", "https://www.grambunny.com/cart");

                    $("#addcartalt").show();

                    $("#msgaddtocart").text(msg.msg);

                    //window.location.replace(purl);

                }

            });

        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#catfilter input').on('change', function() {

                var cateId = $("input[name='category']:checked").val();

                $('.astro').removeClass("catactive");

                $("input[name='category']:checked").addClass("catactive");

                searchKeypbr();

            });

            $('#catfiltermo input').on('change', function() {

                var cateId = $("input[name='categorymo']:checked").val();

                $('.astromo').removeClass("catactive");

                $("input[name='categorymo']:checked").addClass("catactive");

                searchKeypbrmo();

            });


        });
    </script>

    <?php // if(empty(Session::get('latitudegeo'))){
    ?>

    <script>
        function clFunction() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showPosition);

            }

            function showPosition(position) {

                var lat = position.coords.latitude;

                var long = position.coords.longitude;


                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            latitude: lat,
                            longitude: long
                        },

                    },

                    url: "{{ url('/searchproductonload') }}",

                    success: function(msg) {

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }

                    }

                });

            }

        }


        $(window).on('load', function() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(showPosition);

            }

            function showPosition(position) {

                var lat = position.coords.latitude;

                var long = position.coords.longitude;


                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            latitude: lat,
                            longitude: long
                        },

                    },

                    url: "{{ url('/searchproductonload') }}",

                    success: function(msg) {

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }

                    }

                });

            }

        })
    </script>

    <?php // }
    ?>

    <script>
        setInterval(searchKeypbr, 300000);

        setInterval(searchKeypbrmo, 300000);

        function searchKeypbr() {

            var page = 1;

            var x = document.getElementById("searchpbr");

            var xsearch = x.value;

            var citzi = document.getElementById("searchcityz");

            var xcitzi = citzi.value;

            var xcategory = '';

            if ($("input[name='category']:checked").val()) {

                xcategory = $("input[name='category']:checked").val();

            }

            /*if($("input[name='categorymo']:checked").val()){

            xcategory = $("input[name='categorymo']:checked").val();

            }*/

            if (xsearch == '' && xcitzi == '' && xcategory == '') {

                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            searchpbr: xsearch,
                            cityzip: xcitzi,
                            category: xcategory,
                            pages: page
                        },

                    },

                    url: "{{ url('/searchproduct') }}",

                    success: function(msg) {

                        $('#pagenum').val(page);

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }

                    }

                });


            } else {

                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            searchpbr: xsearch,
                            cityzip: xcitzi,
                            category: xcategory,
                            pages: page
                        },

                    },

                    url: "{{ url('/searchproduct') }}",

                    success: function(msg) {

                        $('#pagenum').val(page);

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }


                    }

                });

            }

        }

        function searchKeypbrmo() {

            var page = 1;
            $('#pagenum').val(page);

            var x = document.getElementById("searchpbr");

            var xsearch = x.value;

            var citzi = document.getElementById("searchcityz");

            var xcitzi = citzi.value;

            var xcategory = '';

            if ($("input[name='categorymo']:checked").val()) {

                xcategory = $("input[name='categorymo']:checked").val();

            }

            if (xsearch == '' && xcitzi == '' && xcategory == '') {

                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            searchpbr: xsearch,
                            cityzip: xcitzi,
                            category: xcategory,
                            pages: page
                        },

                    },

                    url: "{{ url('/searchproduct') }}",

                    success: function(msg) {

                        $('#pagenum').val(page);

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }

                    }

                });

            } else {

                $.ajax({

                    type: "post",

                    headers: {
                        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
                    },

                    data: {

                        "_token": "{{ csrf_token() }}",

                        "data": {
                            searchpbr: xsearch,
                            cityzip: xcitzi,
                            category: xcategory,
                            pages: page
                        },

                    },

                    url: "{{ url('/searchproduct') }}",

                    success: function(msg) {

                        $('#pagenum').val(page);

                        $('#ajaxdatacall').html(msg);

                        var totalvendor = $('#totalvendor').val();

                        if (totalvendor == 0) {

                            $('#loadshow').hide();

                        } else {

                            $('#loadshow').show();

                        }

                    }

                });

            }

        }
    </script>


    <!-- < baaner ands & product secttion end> -->
@endsection
@section('scripts')
    <script>
        function initMap() {
            var myMapCenter = {
                lat: <?php echo $latitude; ?>,
                lng: <?php echo $longitude; ?>
            };

            // Create a map object and specify the DOM element for display.
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myMapCenter,
                zoom: 11
            });


            function markStore(storeInfo) {

                var appUrl = "<?php echo url('/'); ?>";

                // Create a marker and set its position.
                var marker = new google.maps.Marker({
                    map: map,
                    position: storeInfo.location,
                    title: storeInfo.name,
                    icon: appUrl + "/public/uploads/" + storeInfo.icon
                });

                // show store info when marker is clicked
                marker.addListener('click', function() {
                    showStoreInfo(storeInfo);
                });
            }

            function markStoreuser() {

                var marker = new google.maps.Marker({
                    position: {
                        lat: <?php echo $latitude; ?>,
                        lng: <?php echo $longitude; ?>
                    },
                    map: map,
                    draggable: true,

                });

                // show store info when marker is clicked
                marker.addListener('click', function() {
                    showStoreInfo(storeInfo);
                });
            }

            // show store info in text box
            function showStoreInfo(storeInfo) {
                var info_div = document.getElementById('info_div');


                window.location.href = "<?php echo $baseurl; ?>/" + storeInfo.name + "/store";

                //info_div.innerHTML = 'Store name: '+ storeInfo.name;

            }

            var stores = <?php echo $drivermaps; ?>

            stores.forEach(function(store) {
                markStore(store);
            });

            markStoreuser();

        }
    </script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
        defer></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $("#mapview").click(function() {

                //$("#listview").show();
                $("#mapview").css("color", "#FFA41C");
                $("#listview").css("color", "#ffffff");
                //$(".dltv").hide();
                //$("#map").show();

                $('html, body').animate({
                    scrollTop: parseInt($("#ajaxdatacall").offset().top)
                }, 1000);

            });

            $("#listview").click(function() {

                //$("#mapview").show();
                $("#listview").css("color", "#FFA41C");
                $("#mapview").css("color", "#ffffff");
                //$(".dltv").show();
                //$("#map").hide();
                $('html, body').animate({
                    scrollTop: parseInt($("#listviewscroll").offset().top)
                }, 1000);

            });


        });
    </script>
@endsection
