@extends('layouts.grambunny')
@section('styles')
    <style type="text/css">
  .parallax-window#short {
    height: 230px;
    min-height: inherit;
    background: 0 0;
    position: relative;
    margin-top: 0px;
}
section.parallax-window {
    overflow: hidden;
    position: relative;
    width: 100%;
    background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-position: top center;
    background-size: cover;
}
#sub_content {
    display: table-cell;
    padding: 50px 0 0;
    font-size: 16px;
}
#sub_content h1 {
    margin: 0 0 10px;
    font-size: 28px;
    font-weight: 300;
    color: #F5F0E3;
    text-transform: capitalize;
}
#short #subheader {
    height: 230px;
    color: #F5F0E3;
    text-align: center;
    display: table;
    width: 100%;
}
div#subheader {
    color: #F5F0E3;
    text-align: center;
    display: table;
    width: 100%;
    height: 380px;
}
ul.brad-home {
    padding: 0;
    margin: 0;
    text-align: center;
}
.brad-home li {
    display: inline-block;
    list-style: none;
    padding: 5px 10px;
    font-size: 12px;
}
#subheader a {
    color: #fff;
}
.owl-item:hover{
	border:unset !important;
}
</style>
@endsection
@section('content')
    
<section id="ordr-grab-sec">
  <div class="container">
    <div class="row  wow fadeInUp" data-wow-delay="0.1s">
      <div class="col-lg-12">
        <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
          <!-- <span>Grab it features //</span> -->
          <h2>{{@$page_data->page_title}}</h2>
          <p>{{@$page_data->sub_title}}</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <section class="about-video-area section-gap" style="background:#f0f4f6">	<div class="container">		<div class="row align-items-center">
    </div>
  </div>   </section> -->
<section class="about-video-area section-gap">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-12 about-video-left">
				<?php echo $page_data->page_content; ?>
				
			</div>
			<!-- <div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
				<div class="abutBg">
					<img src="https://www.grambunny.com/public/assets/images/bg7.jpg">
				</div>
				<a class="play-btn" href="#"><img class="img-fluid mx-auto" src="img/play-btn.png" alt=""></a>
			</div> -->
		</div>
	</div>	
</section>


        <div class="site-section bg-white testimonialSection"  data-aos="fade-up" data-aos-delay="200">
            <div class="container">
                <div class="row justify-content-center mb-5">
                    <div class="col-md-7 text-center border-primary">
                        <h2 class="font-weight-light text-primary">Testimonials</h2>
                    </div>
                </div>
                <div class="slide-one-item home-slider owl-carousel">
                        @foreach($testimonials as $testimonial)
                    <div>
                        <div class="testimonial">
                            <figure class="mb-4">
                                <img src="{{asset("public/uploads/testimonial/".$testimonial->image)}}" alt="Image" class="img-fluid mb-3">
                                <p>{{ $testimonial->name }}</p>
                            </figure>
                            <blockquote>
                                <p>{{ $testimonial->content }}</p>
                            </blockquote>
                        </div>
                    </div>
                        @endforeach
                   
                </div>
            </div>
        </div>


@endsection
@section('scripts')
    
@endsection