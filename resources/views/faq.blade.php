@extends("layouts.app")

@section("content")

<section id="ordr-grab-sec">
  <div class="container">
    <div class="row  wow fadeInUp" data-wow-delay="0.1s">
      <div class="col-lg-12">
        <div class="web-heading wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
          <!-- <span>Grab it features //</span> -->
          <h2>How can we help?</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua.</p>
        </div>
      </div>
    </div>

  </div>
</section>

<section id="faq" class="">

      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="web-heading wow bounceIn" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: bounceIn;">
              <!-- <span>Grab it FAQ //</span> -->
              <h2>Frequently Asked Questions</h2>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when its layout.</p>
            </div>          
          </div>
        </div>

        <div class="row justify-content-center" id="faq-list">
          <div class="col-lg-6 col-md-6">

                <li class="wow fadeInUp" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" class="collapsed" href="#faq1"><span>1</span>What is Grab it? <i class="fa fa-angle-down"></i></a>
                  <div id="faq1" class="collapse" data-parent="#faq-list">
                    <p>
                      Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                    </p>
                  </div>
                </li>
      
                <li class="wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" href="#faq2" class="collapsed"><span>2</span>It is a long established fact that? <i class="fa fa-angle-down"></i></a>
                  <div id="faq2" class="collapse" data-parent="#faq-list">
                    <p>
                      Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                    </p>
                  </div>
                </li>
      
                <li class="wow fadeInUp" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" href="#faq3" class="collapsed"><span>3</span>Lorem Ipsum is simply dummy text?<i class="fa fa-angle-down"></i></a>
                  <div id="faq3" class="collapse" data-parent="#faq-list">
                    <p>
                      Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                    </p>
                  </div>
                </li>
      
              
          </div>
          <div class="col-lg-6 col-md-6">
              
      
                <li class="wow fadeInUp" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" href="#faq4" class="collapsed"><span>4</span>Lorem Ipsum is simply dummy text? <i class="fa fa-angle-down"></i></a>
                  <div id="faq4" class="collapse" data-parent="#faq-list">
                    <p>
                      Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                    </p>
                  </div>
                </li>
      
                <li class="wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" href="#faq5" class="collapsed"><span>5</span>It is a long established fact that?<i class="fa fa-angle-down"></i></a>
                  <div id="faq5" class="collapse" data-parent="#faq-list">
                    <p>
                      Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in
                    </p>
                  </div>
                </li>
      
                <li class="wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
                  <a data-toggle="collapse" href="#faq6" class="collapsed"><span>6</span>Lorem Ipsum is simply dummy text?<i class="fa fa-angle-down"></i></a>
                  <div id="faq6" class="collapse" data-parent="#faq-list">
                    <p>
                      Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
                    </p>
                  </div>
                </li>
              
          </div>
        
        </div>

      </div>
    </section>

<section class="support-text">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-lg-12 col-xl-12">
                        <div class="d-flex dc-py-15 flex-column flex-xl-row">
                            <div class="sl-privacydetails">
                                <div class="sl-privacydetails__title">
                                    <h3>Sed ut perspiciatis unde omnis?</h3>
                                </div>
                                <div class="sl-privacydetails__description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempoer incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis auete irure dolor in reprehenderit in voluptate velit.</p>
                                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sitame voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicoe quia consequuntur magni dolores eos qui</p>
                                </div>
                            </div>
                           
                        </div>
                         <div class="sl-privacydetails">
                            <div class="sl-privacydetails__title">
                                <h3>Ut enim ad minim veniam</h3>
                            </div>
                            <div class="sl-privacydetails__description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</p>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>
                            </div>
                        </div>
                         <div class="sl-privacydetails">
                            <div class="sl-privacydetails__title">
                                <h3>Sed ut perspiciatis</h3>
                            </div>
                            <div class="sl-privacydetails__description">
                                <p>Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem.</p>
                                <p>Weuia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eostateums qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sitam amet, consectetur, adipisci velit, sed quia non numquam eius modi</p>
                            </div>
                        </div>
                    </div>
      </div>
    </div>
  </div>
</section>

@endsection