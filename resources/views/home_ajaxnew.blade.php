<?php if($pages==1){ ?>
<!-- <category secttion end> -->
<!-- <map secttion start> -->
<div class="container-fuild">
    <div class="row">
        <div class="col-md-12">
            <div class="map">

                <div class="welcome-area wow fadeInUp" style="margin-top:0 !important;" data-wow-delay="200ms"
                    style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">

                    <div id="map"></div>


                    <section id="" class="merchnt">
                        <div class="" data-aos="fade-up">

                            <div class="row d-flex flex-wrap">
                                @php
                                    $storedata = [];
                                    $vdistance = '';
                                @endphp
                                @foreach ($verdorslist as $value)

                               <?php if($value->map_icon){
                
                                    $mapicon = $value->map_icon;
                   
                                   }else{
                   
                                     $mapicon = 'map.png';
                   
                                   } ?>
                   
                   <?php $storedata[] = array('name' => "'".$value->username."'",'location' => array('lat' =>$value->lat,'lng' =>$value->lng),'icon' => "'".$mapicon."'");?>
                   
                                    @foreach ($results as $revalue)
                                        @if ($revalue->vendor_id == $value->vendor_id)
                                            <div class="col-md-3 d-md-flex align-items-md-stretch" id="listview"
                                                style="display:none!important;">
                                                <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">

                                                    <div class="count-box">
                                                        <div class="count-img">
                                                            @if (!empty($value->profile_img1))
                                                                <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
                                                                    alt="Profile Image">
                                                            @else
                                                                <img src="https://www.grambunny.com/public/uploads/user.jpg"
                                                                    alt="Profile Image">
                                                            @endif
                                                        </div>
                                                        <p>{{ $value->business_name }}</p>
                                                        <small>{{ $value->description }}</small>
                                                        <div class="conpmy-name">
                                                            <h6><i class='bx bx-check-circle'></i>
                                                                {{ $value->username }}</h6>
                                                            <h6><i class="bx bx-map"></i> Distance :
                                                                {{ round($revalue->distance, 1) }} Miles
                                                            </h6>
                                                        </div>
                                                        <div class="rating-box">
                                                            @php
                                                                $ratings = $value->avg_rating;
                                                            @endphp
                                                            @for ($i = 0; $i < 5; $i++)
                                                                @if ($ratings > $i)
                                                                    <i class='bx bxs-star'></i>
                                                                @else
                                                                    <i class='bx bx-star'></i>
                                                                @endif
                                                            @endfor
                                                        </div>
                                                    </div>
                                                </a>

                                            </div>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </section>


                </div>

            </div>
        </div>
    </div>
</div>

<?php $drivermap = json_encode($storedata);
$drivermaps = str_replace('"', '', $drivermap); ?>

<!-- <baaner ands & product secttion start> -->

<div class="container-fuild">
    <div class="row">

        {{-- <div class="b_add">
              <div class="col-md-6">
                  <div class="adds_banner">
                      @if ($advertisement1)
                          <img src="{{ asset('public/uploads/advertisement/' . $advertisement1->image) }}">
                      @endif
                  </div>
              </div>
              <div class="col-md-6 ads_rgt">
                  <div class="adds_banner">
                      @if ($advertisement2)
                          <img src="{{ asset('public/uploads/advertisement/' . $advertisement2->image) }}">
                      @endif
                  </div>
              </div>
          </div> --}}

        <div class="alert alert-success" id="addcartalt" style="display: none; color: green;background: lightyellow;">
            <button type="button" class="close" id="closeaddtoc">&times;</button>
            <strong id="msgaddtocart">Product Added Successfully!</strong>
        </div>


        <div class="product-listing-section">
            <div class="row row_listing">
                <div class="col-md-12">

                    <div class="list_view_bg_sect" id="list_view_bg_sect">
                        <section id="" class="merchnt">

                            <div class="" data-aos="fade-up">

                                <div class="row d-flex flex-wrap">
                                    @php
                                        $storedata = [];
                                        $vdistance = '';
                                    @endphp
                                    <div class="section-title">
                                        <p>Merchants</p>
                                    </div>
                                    @foreach ($verdorslist as $value)
                                        @foreach ($results as $revalue)
                                            @if ($revalue->vendor_id == $value->vendor_id)
                                                <div class="col-md-3 d-md-flex align-items-md-stretch"
                                                    id="listviewscroll">
                                                    <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">

                                                        <div class="count-box">
                                                            <div class="count-img">
                                                                @if (!empty($value->profile_img1))
                                                                    <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
                                                                        alt="Profile Image">
                                                                @else
                                                                    <img src="https://www.grambunny.com/public/uploads/user.jpg"
                                                                        alt="Profile Image">
                                                                @endif
                                                            </div>
                                                            <p>{{ $value->business_name }}</p>
                                                            <small>{{ $value->description }}</small>
                                                            <div class="conpmy-name">
                                                                <h6><i class='bx bx-check-circle'></i>
                                                                    {{ $value->username }}</h6>
                                                                <h6><i class="bx bx-map"></i> Distance :
                                                                    {{ round($revalue->distance, 1) }} Miles
                                                                </h6>
                                                            </div>
                                                            <div class="rating-box">
                                                                @php
                                                                    $ratings = $value->avg_rating;
                                                                @endphp
                                                                @for ($i = 0; $i < 5; $i++)
                                                                    @if ($ratings > $i)
                                                                        <i class='bx bxs-star'></i>
                                                                    @else
                                                                        <i class='bx bx-star'></i>
                                                                    @endif
                                                                @endfor
                                                            </div>
                                                        </div>
                                                    </a>

                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<input type="hidden" name="totalvendor" id="totalvendor" value="{{ $total }}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#closeaddtoc').click(function(e) {
            $("#addcartalt").hide();
        });
    });

    function apply_qty(productid) {

        var qty = 1;

        var product_id = productid;

        var vendor_id = $('#vendor_id').val();

        var purl = $('#purl').val();

        $.ajax({

            type: "post",

            headers: {

                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

            },

            data: {

                "_token": "{{ csrf_token() }}",

                "qty": qty,

                "product_id": product_id,

                "vendor_id": vendor_id

            },

            url: "{{ url('/addtocart') }}",

            success: function(msg) {

                $('#session_qty').text("(" + msg.cartcount + ")");

                $(".dcart a").prop("href", "https://www.grambunny.com/cart");

                $("#addcartalt").show();

                $("#msgaddtocart").text(msg.msg);

                //window.location.replace(purl);

            }

        });

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#catfilter input').on('change', function() {

            var cateId = $("input[name='category']:checked").val();

            $('.astro').removeClass("catactive");

            $("input[name='category']:checked").addClass("catactive");

            searchKeypbr();

        });

        $('#catfiltermo input').on('change', function() {

            var cateId = $("input[name='categorymo']:checked").val();

            $('.astromo').removeClass("catactive");

            $("input[name='categorymo']:checked").addClass("catactive");

            searchKeypbrmo();

        });


    });
</script>

<script>
    function searchKeypbr() {

        var page = 1;

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='category']:checked").val()) {

            xcategory = $("input[name='category']:checked").val();

        }

        /* if($("input[name='categorymo']:checked").val()){
  
           xcategory = $("input[name='categorymo']:checked").val();
  
           } */


        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory,
                    pages: page
                },

            },

            url: "{{ url('/searchproduct_new') }}",

            success: function(msg) {

                $('#pagenum').val(page);

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }


            }

        });

    }

    function searchKeypbrmo() {

        var page = 1;

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='categorymo']:checked").val()) {

            xcategory = $("input[name='categorymo']:checked").val();

        }

        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory,
                    pages: page
                },

            },

            url: "{{ url('/searchproduct_new') }}",

            success: function(msg) {

                $('#pagenum').val(page);

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }

            }

        });

    }
</script>


<script>
    function initMap() {
        var myMapCenter = {
            lat: <?php echo $latitude; ?>,
            lng: <?php echo $longitude; ?>
        };

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myMapCenter,
            zoom: 11
        });


        function markStore(storeInfo) {

            var appUrl = "<?php echo url('/'); ?>";

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
                map: map,
                position: storeInfo.location,
                title: storeInfo.name,
                icon: appUrl + "/public/uploads/" + storeInfo.icon
            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        function markStoreuser() {

            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $latitude; ?>,
                    lng: <?php echo $longitude; ?>
                },
                map: map,
                draggable: true,

            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        // show store info in text box
        function showStoreInfo(storeInfo) {
            var info_div = document.getElementById('info_div');


            window.location.href = "<?php echo url('/'); ?>/" + storeInfo.name + "/store";

            //info_div.innerHTML = 'Store name: '+ storeInfo.name;

        }

        var stores = <?php echo $drivermaps; ?>

        stores.forEach(function(store) {
            markStore(store);
        });

        markStoreuser();

    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
    defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<?php }else{ ?>

<?php $storedata = [];
$vdistance = ''; ?>

<?php foreach ($verdorslist as $key => $value) { 
  
              if($value->map_icon){
                  
                   $mapicon = $value->map_icon;
  
                  }else{
  
                    $mapicon = 'map.png';
  
                  } 
  
              ?>

<?php $storedata[] = ['name' => "'" . $value->username . "'", 'location' => ['lat' => $value->lat, 'lng' => $value->lng], 'icon' => "'" . $mapicon . "'"]; ?>

<?php foreach ($results as $key => $revalue) {
    if ($revalue->vendor_id == $value->vendor_id) {
        $vdistance = $revalue->distance;
    }
} ?>



<div class="col-md-3 d-md-flex align-items-md-stretch" id="listview" style="display:none!important;">
    <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">

        <div class="count-box">
            <div class="count-img">
                @if (!empty($value->profile_img1))
                    <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
                        alt="Profile Image">
                @else
                    <img src="https://www.grambunny.com/public/uploads/user.jpg" alt="Profile Image">
                @endif
            </div>
            <p>{{ $value->business_name }}</p>
            <small>{{ $value->description }}</small>
            <div class="conpmy-name">
                <h6><i class='bx bx-check-circle'></i>
                    {{ $value->username }}</h6>
                <h6><i class="bx bx-map"></i> Distance :
                    {{ round($revalue->distance, 1) }} Miles
                </h6>
            </div>
            <div class="rating-box">
                @php
                    $ratings = $value->avg_rating;
                @endphp
                @for ($i = 0; $i < 5; $i++)
                    @if ($ratings > $i)
                        <i class='bx bxs-star'></i>
                    @else
                        <i class='bx bx-star'></i>
                    @endif
                @endfor
            </div>
        </div>
    </a>

</div>

<?php } ?>


<?php $drivermap = json_encode($storedata);
$drivermaps = str_replace('"', '', $drivermap); ?>


<?php $storedata = [];
$vdistance = ''; ?>

<?php foreach ($verdorslist as $key => $value) { 
  
              foreach ($results as $key => $revalue) {
  
              if($revalue->vendor_id==$value->vendor_id){
  
                $vdistance = $revalue->distance;
  
               }
  
              } ?>

<div class="col-md-3 d-md-flex align-items-md-stretch" id="listviewscroll">
    <a href="<?php echo url('/'); ?>/{{ $value->username }}/store">

        <div class="count-box">
            <div class="count-img">
                @if (!empty($value->profile_img1))
                    <img src="<?php echo url('/'); ?>/public/uploads/vendor/profile/{{ $value->profile_img1 }}"
                        alt="Profile Image">
                @else
                    <img src="https://www.grambunny.com/public/uploads/user.jpg" alt="Profile Image">
                @endif
            </div>
            <p>{{ $value->business_name }}</p>
            <small>{{ $value->description }}</small>
            <div class="conpmy-name">
                <h6><i class='bx bx-check-circle'></i>
                    {{ $value->username }}</h6>
                <h6><i class="bx bx-map"></i> Distance :
                    {{ round($revalue->distance, 1) }} Miles
                </h6>
            </div>
            <div class="rating-box">
                @php
                    $ratings = $value->avg_rating;
                @endphp
                @for ($i = 0; $i < 5; $i++)
                    @if ($ratings > $i)
                        <i class='bx bxs-star'></i>
                    @else
                        <i class='bx bx-star'></i>
                    @endif
                @endfor
            </div>
        </div>
    </a>
</div>
<?php } ?>

<input type="hidden" name="totalvendor" id="totalvendor" value="{{ $total }}">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#closeaddtoc').click(function(e) {
            $("#addcartalt").hide();
        });
    });

    function apply_qty(productid) {

        var qty = 1;

        var product_id = productid;

        var vendor_id = $('#vendor_id').val();

        var purl = $('#purl').val();

        $.ajax({

            type: "post",

            headers: {

                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')

            },

            data: {

                "_token": "{{ csrf_token() }}",

                "qty": qty,

                "product_id": product_id,

                "vendor_id": vendor_id

            },

            url: "{{ url('/addtocart') }}",

            success: function(msg) {

                $('#session_qty').text("(" + msg.cartcount + ")");

                $(".dcart a").prop("href", "https://www.grambunny.com/cart");

                $("#addcartalt").show();

                $("#msgaddtocart").text(msg.msg);

                //window.location.replace(purl);

            }

        });

    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#catfilter input').on('change', function() {

            var cateId = $("input[name='category']:checked").val();

            $('.astro').removeClass("catactive");

            $("input[name='category']:checked").addClass("catactive");

            searchKeypbr();

        });

        $('#catfiltermo input').on('change', function() {

            var cateId = $("input[name='categorymo']:checked").val();

            $('.astromo').removeClass("catactive");

            $("input[name='categorymo']:checked").addClass("catactive");

            searchKeypbrmo();

        });


    });
</script>

<script>
    function searchKeypbr() {

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='category']:checked").val()) {

            xcategory = $("input[name='category']:checked").val();

        }

        /* if($("input[name='categorymo']:checked").val()){
  
           xcategory = $("input[name='categorymo']:checked").val();
  
           } */


        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory
                },

            },

            url: "{{ url('/searchproduct_new') }}",

            success: function(msg) {

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }

            }

        });

    }

    function searchKeypbrmo() {

        var x = document.getElementById("searchpbr");

        var xsearch = x.value;

        var citzi = document.getElementById("searchcityz");

        var xcitzi = citzi.value;

        var xcategory = '';

        if ($("input[name='categorymo']:checked").val()) {

            xcategory = $("input[name='categorymo']:checked").val();

        }

        if (xsearch == '' && xcitzi == '' && xcategory == '') {
            return false;
        }

        $.ajax({

            type: "post",

            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },

            data: {

                "_token": "{{ csrf_token() }}",

                "data": {
                    searchpbr: xsearch,
                    cityzip: xcitzi,
                    category: xcategory
                },

            },

            url: "{{ url('/searchproduct_new') }}",

            success: function(msg) {

                $('#ajaxdatacall').html(msg);

                var totalvendor = $('#totalvendor').val();

                if (totalvendor == 0) {

                    $('#loadshow').hide();

                } else {

                    $('#loadshow').show();

                }


            }

        });

    }
</script>


<script>
    function initMap() {
        var myMapCenter = {
            lat: <?php echo $latitude; ?>,
            lng: <?php echo $longitude; ?>
        };

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myMapCenter,
            zoom: 11
        });


        function markStore(storeInfo) {

            var appUrl = "<?php echo url('/'); ?>";

            // Create a marker and set its position.
            var marker = new google.maps.Marker({
                map: map,
                position: storeInfo.location,
                title: storeInfo.name,
                icon: appUrl + "/public/uploads/" + storeInfo.icon
            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        function markStoreuser() {

            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $latitude; ?>,
                    lng: <?php echo $longitude; ?>
                },
                map: map,
                draggable: true,

            });

            // show store info when marker is clicked
            marker.addListener('click', function() {
                showStoreInfo(storeInfo);
            });
        }

        // show store info in text box
        function showStoreInfo(storeInfo) {
            var info_div = document.getElementById('info_div');


            window.location.href = "<?php echo url('/'); ?>/" + storeInfo.name + "/store";

            //info_div.innerHTML = 'Store name: '+ storeInfo.name;

        }

        var stores = <?php echo $drivermaps; ?>

        stores.forEach(function(store) {
            markStore(store);
        });

        markStoreuser();

    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap" async
    defer></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php } ?>
