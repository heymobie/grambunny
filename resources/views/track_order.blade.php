@extends("layouts.grambunny")

@section("styles")

<style type="text/css">

.parallax-window#short {

    height: 230px;

    min-height: inherit;

    background: 0 0;

    position: relative;

    margin-top: 0px;

}



section.parallax-window {

    overflow: hidden;

    position: relative;

    width: 100%;

    background-image: url(https://www.grambunny.com/public/design/front/img/sub_header_2.jpg) !important;

    background-attachment: fixed;

    background-repeat: no-repeat;

    background-position: top center;

    background-size: cover;

}

#short #subheader {

    height: 230px;

    color: #F5F0E3;

    text-align: center;

    display: table;

    width: 100%;

}

#sub_content {

    display: table-cell;

    padding: 50px 0 0;

    font-size: 16px;

}

#sub_content h1 {

    margin: 0 0 10px;

    font-size: 28px;

    font-weight: 300;

    color: #F5F0E3;

    text-transform: capitalize;

}

.ordrSumry tr {

    font-size: 13px;

}



    #map {height: 100%;}

    html, body {

      height: 100%;

      margin: 0;

      padding: 0;

    }

    #floating-panel {

      position: absolute;

      top: 10px;

      right: 1%;

      z-index: 5;

      background-color: #fff;

      border: 1px solid #999;

      text-align: center;

    }



   input#start1 {

    display: none;

}



input#end1 {

    display: none;

} 



</style>



@endsection



@section("content")



<section id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335" class="parallax-window Serv_Prod_Banner">

  <div id="subheader">

    <div id="sub_content" class="animated zoomIn">

      <h1><span class="restaunt_countrt">Track Order</span></h1> 

    </div>

  </div>

</section>



<section class="section-full myOrderThnk">

  <div class="container">

    <div class="row">

      <div class="col-md-12  ">

        <div class="MnThnxBox">


    <div id="floating-panel">


    <input tye="text" id="start1" value="<?php echo $market_area;?>"><!-- Starting Location -->

    <input type="text" id="end1" value="<?php echo $user_address;?>"><!-- Ending Location -->

    </div>



    <div id="map" style="overflow: visible;height: 600px;"></div>



  </div>

  </div>

  </div>

  </div>

  </section>  

  @endsection


  @section("scripts")  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  

  <script>

  function calcDistance(p1, p2) {//p1 and p2 in the form of google.maps.LatLng object

    return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(3);//distance in KiloMeters

  }

  
  function getLocation() {

    if (navigator.geolocation) {navigator.geolocation.getCurrentPosition(showPosition, showError);} 

    else {alert("Geolocation is not supported by this browser.");}

  }

  function showPosition(position) {

    alert("Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude); 

  }

  function showError(error) {

    switch(error.code) {

      case error.PERMISSION_DENIED:

        alert("User denied the request for Geolocation.");

        break;

      case error.POSITION_UNAVAILABLE:

        alert("Location information is unavailable.");

        break;

      case error.TIMEOUT:

        alert("The request to get user location timed out.");

        break;

      case error.UNKNOWN_ERROR:

        alert("An unknown error occurred.");

        break;

    }

  }

  

  function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {

      zoom: 10,

      center: { lat: 33.84073390, lng: -118.21602090 }

    });

    
    var waypts = [];//origin to destination via waypoints

    //waypts.push({location: 'indore', stopover: true});

    
    function continuouslyUpdatePosition(location){//Take current location from location.php and set marker to that location

      var xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {

          var pos = JSON.parse(this.responseText);

          location.setPosition(new google.maps.LatLng(pos.lat,pos.lng));

          setTimeout(function(){

            continuouslyUpdatePosition(location);

          },5000);

        }

      };

      xhttp.open("GET", "location", true);

      xhttp.send();

    }

    

    /* Distance between p1 & p2

    var p1 = new google.maps.LatLng(45.463688, 9.18814);

    var p2 = new google.maps.LatLng(46.0438317, 9.75936230000002);

    alert(calcDistance(p1,p2)+" Kilimeters");

    */

    

    //Make marker at any position in form of {lat,lng}

    function makeMarker(position /*, icon*/) {

      var marker = new google.maps.Marker({

        position: position,

        map: map,

        /*animation: google.maps.Animation.DROP,*/

        /*icon: icon,*/

      });

      return marker;

    }

    
    var icons = {

      end: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'),

      start: new google.maps.MarkerImage('http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Chartreuse-icon.png')

    };

    
    //Show suggestions for places, requires libraries=places in the google maps api script link

    var autocomplete1 = new google.maps.places.Autocomplete(document.getElementById('start1'));

    var autocomplete2 = new google.maps.places.Autocomplete(document.getElementById('end1'));

    
    var directionsService1 = new google.maps.DirectionsService;

    var directionsDisplay1 = new google.maps.DirectionsRenderer({

      polylineOptions: {strokeColor: "red"}, //path color

      //draggable: true,// change start, waypoints and destination by dragging

      /* Start and end marker with same image

      markerOptions : {icon: 'http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Push-Pin-1-Left-Pink-icon.png'},

      */

      //suppressMarkers: true

    });

    directionsDisplay1.setMap(map);

    var onChangeHandler1 = function() {calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'),$('#end1'));};

    $('#start1,#end1').change(onChangeHandler1);    

    
    function calculateAndDisplayRoute(directionsService, directionsDisplay, start, end) { 

      directionsService.route({

        origin: start.val(),

        destination: end.val(),

        waypoints: waypts,

        travelMode: 'DRIVING'

      }, function(response, status) {

        if (status === 'OK') {

          directionsDisplay.setDirections(response);

          var leg = response.routes[ 0 ].legs[ 0 ];

          /*// Move marker along path from A to B

          var markers = [];

          for(var i=0; i<leg.steps.length; i++){

            var marker = makeMarker(leg.steps[i].start_location);

            markers.push(marker);

            marker.setMap(null);

          }

          tracePath(markers, 0);

          */

          var location = makeMarker(leg.steps[0].start_location);

          continuouslyUpdatePosition(location);       

        } else {window.alert('Directions request failed due to ' + status);}

      });

    }


    function tracePath(markers, index){// move marker along path from A to B

      if(index==markers.length) return;

      markers[index].setMap(map);

      setTimeout(function(){

        markers[index].setMap(null);

        tracePath(markers, index+1);

      },500);

    }

    calculateAndDisplayRoute(directionsService1, directionsDisplay1, $('#start1'),$('#end1'));

  }

  </script>


    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-ANulZRLHUhROP47UlRNTBXrvVl102fU&callback=initMap">
    </script>

@endsection