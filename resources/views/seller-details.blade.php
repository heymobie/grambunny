@extends("layouts.grambunny")
@section("styles")
{{-- styles --}}
<style type="text/css">
   .parallax-window#short {
   height: 230px;
   min-height: inherit;
   background: 0 0;
   position: relative;
   margin-top: 0px;
   }
   section.parallax-window {
   overflow: hidden;
   position: relative;
   width: 100%;
   background-image: url(https://herbariumdelivery.com/public/design/front/img/sub_header_2.jpg) !important;
   background-attachment: fixed;
   background-repeat: no-repeat;
   background-position: top center;
   background-size: cover;
   }
   #sub_content {
   display: table-cell;
   padding: 50px 0 0;
   font-size: 16px;
   }
   #sub_content h1 {
   margin: 0 0 10px;
   font-size: 28px;
   font-weight: 300;
   color: #F5F0E3;
   text-transform: capitalize;
   }
   #short #subheader {
   height: 230px;
   color: #F5F0E3;
   text-align: center;
   display: table;
   width: 100%;
   }
   div#subheader {
   color: #F5F0E3;
   text-align: center;
   display: table;
   width: 100%;
   height: 380px;
   }
   ul.brad-home {
   padding: 0;
   margin: 0;
   text-align: center;
   }
   .brad-home li {
   display: inline-block;
   list-style: none;
   padding: 5px 10px;
   font-size: 12px;
   }
   #subheader a {
   color: #fff;
   }
   .myOrderDtelBtn a{
   border: 1px solid #231f20;
   background-color: #fff;
   color: #231f20;
   font-weight: 700;
   border-radius: 25px;
   padding: 8px 25px;
   min-width: 100px;
   cursor: pointer;
   text-decoration: none;
   }
   .myOrderDtelBtn a:hover {
   background-color: #231f20;
   color: #fff;
   }
   .myOrderDtelBtn {
   float: none;
   text-align: center;
   }
   @media only screen and (max-width: 480px) {
    .sf-thum-bx img, .post-thum-bx img, .post-thum img {
    width: 100%;
    height: auto;
    }
    .sf-thum-bx {
    margin-bottom: 0px;
    background-color: unset;
}
.about-info .sf-provider-des {
    padding: 0px !important;
}
}

.addtocart{
  width: 25px;
  float: right;
}

.drepoimg img {
    width: 200px !important;
    margin: 16% 3% -13% -8%;
}
.pict img {
    width: 54px;

    cursor: pointer;
}
#videospopupplay {
    left: -25px;
    position: relative;
}
#more {display: none;}
.remimage {
     height: inherit;
    /* overflow: hidden; */
    width: inherit;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
}
.parallax-window.Serv_Prod_Banner {
    height: 103px !important;
    padding: 30px;
}
</style>
@endsection
@section("content")
{{-- content goes here --}}

<section class="parallax-window Serv_Prod_Banner" id="short" data-parallax="scroll" data-image-src="" data-natural-width="1350" data-natural-height="335">

    <div id="subheader"></div>

</section>


<section class="bg-gray seller_ProSer-info">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="sl-appointment bnmts">
<div class="row">
         <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="sf-provider-des">
               <div class="sf-thum-bx overlay-black-light">
                  <img id="merchantimg" src="{{asset("public/uploads/vendor/profile/".$merchant->profile_img1)}}" alt="">
               </div>

               <div class="remimage">
                  <?php  if(!empty($merchant->profile_img1)){ ?>
                 <div class="repo-img">
                  <img id="mprofileimg1" src="{{asset("public/uploads/vendor/profile/".$merchant->profile_img1)}}">
                </div>
                 <?php }else{ ?>
                <div class="drepoimg">
                <img id="mprofileimg1" src="https://www.grambunny.com/public/uploads/user.jpg">
                </div>
                  <?php } ?>
                  <?php  if(!empty($merchant->profile_img2)){?>
                      <div class="repo-img">
                  <img id="mprofileimg2" src="{{asset("public/uploads/vendor/profile/".$merchant->profile_img2)}}"> </div>
                  <?php } ?>
                  <?php if(!empty($merchant->profile_img3)){?>
                      <div class="repo-img">
                  <img id="mprofileimg3" src="{{asset("public/uploads/vendor/profile/".$merchant->profile_img3)}}"></div>
                  <?php } ?>
                  <?php if(!empty($merchant->profile_img4)){?>
                      <div class="repo-img">
                  <img id="mprofileimg4" src="{{asset("public/uploads/vendor/profile/".$merchant->profile_img4)}}">  </div>
                  <?php }  ?>
 <div class="repo-img">
     <?php  if(!empty($merchant->video)){ ?>

<div class="picture-sec">
  <div class="pict">
  <!-- <a onclick="lightbox_open('<?php echo $merchant->vendor_id;?>');"> -->
    <img src="{{url('public/download.png')}}" onclick="popupvideoplay('<?php echo $merchant->vendor_id;?>');"  class="img-fluid" alt="" id="img_id">
<!-- </a> -->
</div>
</div>

<?php }?>
</div>
 <div id="videospopupplay" class="mt-2"></div>
               </div>

               <!--<span class="sf-provider-name">{{ $merchant->fullname }}</span>-->
               <ul class="social-bx list-inline">
                  <?php if(!empty($social_links->facebook_link)){ ?>
                  <li><a href="<?php echo $social_links->facebook_link; ?>" target="_blank" rel="nofollow" class="fa fa-facebook"></a></li>
                  <?php } ?>
                  <?php if(!empty($social_links->twitter_link)){ ?>
                  <li><a href="<?php echo $social_links->twitter_link;?>" target="_blank" rel="nofollow" class="fa fa-twitter"></a></li>
                  <?php } ?>
                  <?php if(!empty($social_links->pinterest_link)){ ?>
                  <li><a href="<?php echo $social_links->pinterest_link; ?>" target="_blank" rel="nofollow" class="fa fa-pinterest-p"></a></li>
                  <?php } ?>
                  <?php if(!empty($social_links->instagram_link)){ ?>
                  <li><a href="<?php echo $social_links->instagram_link; ?>" target="_blank" rel="nofollow" class="fa fa-instagram"></a></li>
                  <?php } ?>
                  <?php if(!empty($social_links->google_plus_link)){ ?>
                  <li><a href="<?php echo $social_links->google_plus_link;?>" target="_blank" rel="nofollow" class="fa fa-google-plus"></a></li>
                  <?php } ?>
               </ul>
               <!-- <span class="sf-featured-approve bg-success"><span>Verified</span></span>                       -->
            </div>
         </div>

         <div class="col-md-6 col-sm-12 col-xs-12 pl-0 pr-0">
            <div class="sf-about-box">
               <div class="row">
               <div class="col-md-7">
               <h2 class="sf-title">{{ $merchant->fullname }}</h2>
               <span class="tagline">{{ $merchant->business_name }}</span>
               <div class="sf-provider-cat">
                  <strong>Permit/License Number : </strong>
                   {{ $merchant->permit_number }}
               </div>
                <div class="sl-appointment__feature">

                        <div class="sl-featureRating">

                        <div class="text-warning">


                 <?php

                    if($merchant->avg_rating == 1){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 2){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 3){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 4){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 5){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                  }else if($merchant->avg_rating == '0'){

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }

                        ?>


                                </div>

                                <em>({{ $merchant->rating_count }} )</em>

                            </div>

                            {{--

                            <div class="sl-appointment__location">

                                <em

                                    >Location:

                                    <a href="javascript:void(0);"

                                        >Get Direction</a

                                    ></em

                                >

                            </div>

                            --}}

                        </div>
               <div class="sf-provider-cat">
               <strong>Contact Phone Number : </strong>
                   {{ $merchant->contact_no }}

               </div>
              <!--  <div class="shared-bx"></div> -->

              <?php  if(!empty($merchant->description)){ ?>

               <span id="more-{{ $merchant->vendor_id }}" > {!! nl2br(substr($merchant->description, 0, 100)) !!}</span>
               <span id="more1-{{ $merchant->vendor_id }}" style="display: none;"> {{ $merchant->description }} </span>

                 <?php  if(!empty(strlen($merchant->description) > 100)){ ?>

               <button type="button" class="btn btn-danger " onclick="loadMore({{ $merchant->vendor_id }})" id="myBtn">..Read More</button>
               <button type="button" class="btn btn-danger " onclick="loadLess({{ $merchant->vendor_id }})" id="myBtn1" style="display:none">Read Less</button>
           <?php }}?>

               <p class="comorder"> Completed orders : {{$corder}} </p>

               <button class="btn btn-sm btn-primary sf-review-btn" href="#reviews-anchor" id="open-review-box">Write A Review</button>

                </div>


                 <div class="col-md-5 clend-blog">
                 <h4 class="widget-title">Hours Of Operation</h4>
               <?php  if(!empty($hoperation)){ ?>

                 <?php

                 $monday = explode("-",$hoperation->monday);
                 $tuesday = explode("-",$hoperation->tuesday);
                 $wednesday = explode("-",$hoperation->wednesday);
                 $thursday = explode("-",$hoperation->thursday);
                 $friday = explode("-",$hoperation->friday);
                 $saturday = explode("-",$hoperation->saturday);
                 $sunday = explode("-",$hoperation->sunday);

                  ?>

                  <div class="wek-day">
                     <div class="wek-main"><span><b>Monday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_mon==1){ ?>{{$monday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_mon==1){ ?>{{$monday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                     <!-- <div class="open-close">
                     <?php if($hoperation->open_close_mon==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                  </div>

                   <div class="wek-day">
                     <div class="wek-main"><span><b>Tuesday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_tue==1){ ?>{{$tuesday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_tue==1){ ?>{{$tuesday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                 <!--  <div class="open-close">
                  <?php if($hoperation->open_close_tue==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>

                   <div class="wek-day">
                     <div class="wek-main"><span><b>Wednesday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_wed==1){ ?>{{$wednesday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_wed==1){ ?>{{$wednesday[1]}}<?php }else{?>0:00<?php } ?></span></div>
<!--                     <div class="open-close">
                     <?php if($hoperation->open_close_wed==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>

                   <div class="wek-day">
                     <div class="wek-main"><span><b>Thursday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_thu==1){ ?>{{$thursday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_thu==1){ ?>{{$thursday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                     <!-- <div class="open-close">
                        <?php if($hoperation->open_close_thu==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>
                   <div class="wek-day">
                     <div class="wek-main"><span><b>Friday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_fri==1){ ?>{{$friday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_fri==1){ ?>{{$friday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                    <!--  <div class="open-close">
                        <?php if($hoperation->open_close_fri==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>

                   <div class="wek-day">
                     <div class="wek-main"><span><b>Saturday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_sat==1){ ?>{{$saturday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_sat==1){ ?>{{$saturday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                     <!-- <div class="open-close">
                        <?php if($hoperation->open_close_sat==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>

                  <div class="wek-day">
                     <div class="wek-main"><span><b>Sunday</b></span></div> <div class="time-am"><span class="cotime"><?php if($hoperation->open_close_sun==1){ ?>{{$sunday[0]}}<?php }else{?>0:00<?php } ?></span>-<span class="cotime2"><?php if($hoperation->open_close_sun==1){ ?>{{$sunday[1]}}<?php }else{?>0:00<?php } ?></span></div>
                   <!--  <div class="open-close">
                     <?php if($hoperation->open_close_sun==1){ echo '<span class=opentime>open</span>';}else{ echo '<span class=closetime>close</span>';} ?>
                  </div> -->
                </div>


                <!--  Monday :   Open -->


                <?php } ?>

                <div class="social-icn">
<ul>
<li><a href="@if(!empty($merchant->facebook)){{ $merchant->facebook }}@endif" target="_blank"><img src="https://www.grambunny.com/public/assets/images/facebook.png"> </a></li>
<li><a href="@if(!empty($merchant->insta)){{ $merchant->insta }}@endif" target="_blank"><img src="https://www.grambunny.com/public/assets/images/insta1.png"> </a></li>
<li><a href="@if(!empty($merchant->tiktok)){{ $merchant->tiktok }}@endif" target="_blank"><img src="https://www.grambunny.com/public/assets/images/tik-tok.png"> </a></li>
<li><a href="@if(!empty($merchant->pinterest)){{ $merchant->pinterest }}@endif" target="_blank"><img src="https://www.grambunny.com/public/assets/images/pinterest.png"> </a></li>
</ul>
</div>

                </div>

                 </div>

            </div>

         </div>
         <div class="col-md-3 sidebar-wrapper">
            <div class="widget widget_text">
               <h4 class="widget-title">Merchant Info</h4>
               <div class="contact-info-widget">
                  <div class="single-info fix">
                     <h5>Merchant ID</h5>
                     <p>{{ $merchant->vendor_id }}</p>
                  </div>
                  <div class="single-info fix">
                     <h5>User Name</h5>
                     <p>{{ $merchant->username }}</p>
                  </div>
                  <div class="single-info fix">
                     <h5>Name</h5>
                     <p>{{ $merchant->fullname }}</p>
                  </div>
                  <div class="single-info fix">
                     <h5>City</h5>
                     <p>{{ $merchant->city }}</p>
                  </div>
                  <div class="single-info fix">
                     <h5>State</h5>
                     <p>{{ $merchant->state }}</p>
                  </div>
                  <div class="single-info fix">
                     <h5>Zip Code</h5>
                     <p>{{ $merchant->zipcode}}</p>
                  </div>
               </div>
            </div>
         </div>
      </div>



              <!--   <div class="sl-appointment__img">

                <img src="{{ $merchant->profileURL }}" alt="Image Description"/>

                    </div> -->

                    <!-- <div class="sl-appointment__content">

                        <div class="sl-slider__tags ">


              <?php if($merchant->vendor_status==1){ ?>

              <a href="javascript:void(0);" class="sl-bg-green">Verified</a>

              <?php } ?>

                        </div>

                    <div class="infoh">

                        <h3>{{ $merchant->fullname }}</h3>

                        <h5>{{ $merchant->business_name }}</h5>

                        <h5>{{ $merchant->mailing_address }}, <?php if(!empty($merchant->address)){ ?>{{ $merchant->address }}, <?php } ?>{{ $merchant->city }}, {{ $merchant->state }} {{ $merchant->zipcode }}</h5>
                     </div>

                        <div class="starview">

                        <div class="sl-appointment__feature">

                        <div class="sl-featureRating">

                        <div class="text-warning">


                 <?php

                    if($merchant->avg_rating == 1){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 2){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 3){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 4){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }else if($merchant->avg_rating == 5){

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                    echo '<i class="fa fa-star"></i>';

                  }else if($merchant->avg_rating == '0'){

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                    echo '<i class="fa fa-star-o"></i>';

                  }

                        ?>


                                </div>

                                <em>({{ $merchant->rating_count }} )</em>

                            </div>

                            {{--

                            <div class="sl-appointment__location">

                                <em

                                    >Location:

                                    <a href="javascript:void(0);"

                                        >Get Direction</a

                                    ></em

                                >

                            </div>

                            --}}

                        </div>

                        <div class="sl-detail">

                            <div class="sl-detail__date">

                                <em

                                    ><i class="fa fa-calendar"></i>Since:

                                    {{ $merchant->created_at->format("M d, Y") }}</em

                                >

                            </div>

                            <div class="sl-detail__view">

                                <em><i class="fa fa-eye"></i>{{ $merchant->views }} Viewed</em>

                            </div>

                        </div>

                    </div>

                    </div> -->

                   <!--  <div class="shared-bx_social">

                    </div> -->

                </div>

            </div>

        </div>

    </div>

</section>


<section class="section-full bg-gray about-info bnmts" id="sf-provider-info">
   <div class="container">

      <!-- merchant product start-->

                    <div class="row">

                    <div class="col-md-12 col-sm-12">

                    <div class="resto-near">



    <div class="alert alert-success" id="addcartalt" style="display: none; color: green;background: lightyellow;">
        <button type="button" class="close" id="closeaddtoc">&times;</button>
    <strong id="msgaddtocart">Product Added Successfully!</strong>
    </div>

    <div class="category_sect desktop-view_homepage_cat_sect roll-flowers">
         <ul id="catfilter">
   <li>
          <a id="astro12" class="active"><input type ="hidden" name="category" class="astro12" value="all"><span class="" onclick="myFunction('all')">All Categories</span></a>
        </li>

        <?php foreach ($categories as $key => $value): ?>
         <li>
   <a class="deep{{$value->id}}" ><input type ="hidden" name="category" class="astro" id="statics" value="<?php echo $value->id;?>"><span class="" onclick="myFunction('<?php echo $value->id;?>')"><?php echo $value->category;?></span></a></li>

         <?php endforeach ?>

         </ul>
      </div>
        <h4 class="mt-4">PRODUCTS</h4>

<script>
function myFunction(id) {

 if(id=='all'){

 $(".deepcls").show();

 $("#astro12").addClass("active");

 var oldclassget= localStorage.getItem("oldclass");

  $("."+oldclassget).removeClass("active");

}else{

   var category_id = 'deep'+id;

  $(".deepcls").hide();

  $("."+category_id).show();

  var oldclassget= localStorage.getItem("oldclass");

  $("."+oldclassget).removeClass("active");

  $("."+category_id).addClass("active");

   $("#astro12").removeClass("active");

  localStorage.setItem("oldclass", category_id);

}
}
</script>

                    <input type="hidden" name="vendor_id"  id="vendor_id"  value="{{ $merchant->vendor_id }}">

                    <input type="hidden" name="purl" id="purl" value="{{ url('/').'/cart' }}">

                    <input type="hidden" name="product_id" id="product_id" value="">
                    </div>

                    </div>

          <div class="flaxwrap">

          <?php ///echo $merchant->profileURL; ?>
           @foreach($items as $item)

            <?php

            //$imageurls =  DB::table('ps_images')->where("ps_id",$item->id)->where("thumb",1)->get();

            $imageurls =  DB::table('ps_images')->where("ps_id",$item->id)->get();

            ?>

            <?php

               /* if (!empty($imageurls[0]->name)) {
                    $imgpath = asset("/public/uploads/product")."/".$imageurls[0]->name;
                }else{
                    //$imgpath = asset("/public/uploads/vendor/profile/gU8Hi0Bo8PImrWjk.jpg");
                    $imgpath = $merchant->profileURL;
                } */

             $imgpath = asset("/public/uploads/product")."/".$item->image;

            ?>

            <?php $countpro = ''; ?>

        <?php if($item->vendor_id==0){

        $mydetails =  DB::table('admin_merchant_product')->where("product_id",$item->id)->where("vendor_id",$merchant->vendor_id)->get();

        //print_r(count($mydetails));

        if(count($mydetails)>0){ ?>


                        <div class="col-md-2 col-sm-6 equal-col deepcls deep{{$item->category_id}}" >

                        <div class="sl-featuredProducts--post">

                        <figure>

                         <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}"> <img src="{{ $imgpath }}" alt="Image Description"/></a>

                        <figcaption class="fg_mb_view">

                        <div class="sl-slider__tags mb_viwe_slag">

                        <a class="sl-bg-red-orange">{{ $item->subcategoryname }}</a>

                        </div>

                                </figcaption>


                                </figure>

                             <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}">

                                <div class="sl-featuredProducts--post__content">

                                    <div class="sl-featuredProducts--post__title">

                                        <h6>{{ $item->name }}</h6>

                                    </div>

                                    <div

                                        class="sl-featuredProducts--post__price"

                                    >

                                      <h5> @if($item['type'] == 3) ${{ $item->price }} / {{ $item->unit }}
@elseif($item['type'] == 2) ${{ $item->rate_mile }} / {{ "Mile"}}
@else ${{ $item->price }} / {{ $item->unit }} @endif</h5>



                                    </div>

                                    <div class="sl-featureRating  ">

                                       <div class="star-rating">

                                       <?php

                                          if($item->avg_rating == 1){

                                          echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 2){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 3){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 4){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 5){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                }else{

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }

                                            ?>



                                            <input

                                                type="hidden"

                                                name="whatever1"

                                                class="rating-value"

                                                value="2.56"

                                            />

                                        </div>

                                        <em>({{ $item->rating_count }})</em>

                                    </div>


                                </div>

                                 </a>

                            </div>

                        </div>


                    <?php }

                }else{ ?>


                        <div class="col-md-2 col-sm-6 equal-col deepcls deep{{$item->category_id}}">

                        <div class="sl-featuredProducts--post">

                        <figure>

                         <a onclick="return apply_qty('{{$item->id}}');" style="cursor: pointer;"><img src="{{url('/public/uploads/addtocart.png')}}" class="addtocart"></a>

                         <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}"> <img src="{{ $imgpath }}" alt="Image Description"/></a>
                        <div class="fig-bar">
                        <figcaption class="fg_mb_view">
                        <?php if(!empty($item->subcategoryname)){ ?>
                        <div class="sl-slider__tags mb_viwe_slag">

                        <a class="sl-bg-red-orange">{{ $item->subcategoryname }}</a>

                        </div>
                      <?php } ?>

                                </figcaption>
                              </div>

                                </figure>

                             <a href="{{ route('productDetail',['username' => $merchant->vendor_id,'slug' => $item->slug]) }}">

                                <div class="sl-featuredProducts--post__content">

                                    <div

                                        class="sl-featuredProducts--post__title"

                                    >

                                        <h6>{{ $item->name }}</h6>

                                    </div>

                                    <div

                                        class="sl-featuredProducts--post__price"

                                    >

                                       <h5> @if($item['type'] == 3) ${{ $item->price }} / {{ $item->unit }}
@elseif($item['type'] == 2) ${{ $item->rate_mile }} / {{ "Mile"}}
@else ${{ $item->price }} / {{ $item->unit }} @endif</h5>



                                    </div>

                                    <div class="sl-featureRating  ">

                                        <!-- <div class="star-rating">

                                            <?php

                                                if($item->avg_rating == 1){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 2){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 3){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 4){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }elseif($item->avg_rating == 5){

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                    echo '<span class="fa fa-star"></span>';

                                                }else{

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                    echo '<span class="fa fa-star-o"></span>';

                                                }

                                            ?>

                                            <input

                                                type="hidden"

                                                name="whatever1"

                                                class="rating-value"

                                                value="2.56"

                                            />

                                        </div>

                                        <em>({{ $item->rating_count }})</em> -->

                                        <?php if($item->type!=3){ ?>
                                        <h6 class="product-title-btype" style="font-size:12px;"> TYPE: {{ str_limit($item->types, 10) }} <br>@if(!empty($item->potency_thc)) THC: {{ $item->potency_thc }}% @endif <br> @if(!empty($item->potency_cbd)) CBD: {{ $item->potency_cbd }}% @endif</h6>
                                      <?php } ?>
                                    </div>
<div class="other-service-product">
<?php if($item->type==0){ ?>
<span class="yelo">Products</span>
<?php } ?>

<?php if($item->type==1){ ?>
<span class="gray">Services</span>
<?php } ?>

<?php if($item->type==3){ ?>
<span class="red">Event tickets</span>
<?php } ?>
</div>

                                </div>

                                 </a>

                            </div>

                        </div>

                   <?php }  ?>

                        @endforeach

            </div>
          </div>



      <!-- end merchant products --->


      <div class="row">
         <div class="col-md-12">
          <?php if(count($itemsbs)>0){ ?>
            <div id="demo">
               <h2>Top Selling Products</h2>
               <div class="">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="block-13">
                           <div class="owl-carousel similer_prod">
                              @foreach($itemsbs as $item)
                              <div class="d-block d-md-flex listing vertical">
                                 <div class="item">
                                    <div class="ctimg_show">
                                       <img src="{{ $item->imageURL }}" alt="">
                                    </div>
                                    <div class="smilrProdct">
                                       <a href="{{ route("productDetails",["slug" => $item->slug]) }}"> {{ $item->name }}</a>
                                       <div class=" text-warning">
                                          <?php
                                             if($item->avg_rating == 1){

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             }else if($item->avg_rating == 2){

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             }else if($item->avg_rating == 3){

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             }else if($item->avg_rating == 4){

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             }else if($item->avg_rating == 5){

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             echo '<i class="fa fa-star"></i>';

                                             }else if($item->avg_rating == '0'){

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             echo '<i class="fa fa-star-o"></i>';

                                             }

                                                 ?>
                                       </div>
                                       <p>{{ $item->price }}</p>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

          <?php } ?>
            <div class="row for-Ratingsec">
               <div class="col-sm-12">
                  <div class="rating-block deeprating">
                     <h4>Average User Rating</h4>
                     <h2 class="bold padding-bottom-7">{{$merchant->avg_rating}} <small>/ 5</small></h2>
                     <?php
                        if($merchant->avg_rating == 1){

                          echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                        }else if($merchant->avg_rating == 2){

                          echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                        }else if($merchant->avg_rating == 3){

                          echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';



                        }else if($merchant->avg_rating == 4){

                          echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                        }else if($merchant->avg_rating == 5){

                          echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';

                        }else if($merchant->avg_rating == '0'){

                          echo '<i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                        }

                        ?>
                     {{-- <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                     <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                     </button>
                     <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                     <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                     </button>
                     <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                     <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                     </button>
                     <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                     <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                     </button>
                     <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                     <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                     </button> --}}
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">

                <?php if(count($ratingReviewData)>0){ ?>

                  <div class="review-block">
                     <?php $i = 1; ?>
                     @foreach ($ratingReviewData as $data)
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="bcx_lft">
                              @if(!empty($data->profile_image))
                              <img src="{{url('public/uploads/user/'.$data->profile_image)}}" class="img-rounded" height="60" width="60">
                              @else
                              <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded">
                              @endif
                           </div>
                           <div class="bcx_rg">
                              <div class="review-block-name">{{$data->name.' '.$data->lname}}</div>
                              <div class="review-block-date">{{date('l d, Y', strtotime ( $data->created_at ))}} <br> {{Helper::calculate_time_span($data->created_at)}}</div>
                           </div>
                        </div>
                        <div class="col-sm-6 review-blockOne">
                           <div class="review-block-rate deeprevblock">
                              <?php
                                 if($data->rating == 1){

                                   echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                                 }else if($data->rating== 2){

                                   echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                                 }else if($data->rating == 3){

                                   echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';



                                 }else if($data->rating == 4){

                                   echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i>';

                                 }else if($data->rating == 5){

                                   echo '<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>';

                                 }

                                 ?>
                           </div>
                           <div class="review-block-title">Customer Review</div>
                           <div class="review-block-description">{{$data->review}}</div>
                        </div>
                     </div>
                     <?php $i++; ?>
                     @if($i != count($ratingReviewData))
                     <hr/>
                     @endif
                     @endforeach
                  </div>

                <?php } ?>
                  <div class="post-review-box" id="post-review-box" style="">
                     <div class="col-md-12">
                        <div class="msg-box">
                           <div id="message-box" style="display:none;">
                              <i class="fa fa-check"></i>
                              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                              <span id="message"></span>
                           </div>
                        </div>
                        <h2>Write Your Review</h2>
                        <form accept-charset="UTF-8" id="merchant-rating-form" action="javascript:void(0);" method="post">
                           <input id="ratings-hidden" name="rating" type="hidden">
                           <input id="merchant-id" name="merchant_id" type="hidden" value="{{$merchant->vendor_id}}">
                           <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
                           <div class="text-right">
                              <div class="stars starrr" data-rating="0"></div>
                              <!--  <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="">Cancel</a> -->
                              <button class="btn btn-success btn-lg review-sub" type="submit" id="submit-merchant-review">Submit Review</button>
                              <a href="javascript:history.back()" class="back-to-store">Go Back to Store</a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
@endsection
@section("scripts")
{{-- styles --}}
<script type="text/javascript">

  $(document).ready(function() {
    $('#closeaddtoc').click(function(e) {
     $("#addcartalt").hide();
    });
});


    function apply_qty(productid){

      var qty = 1;

      $('#product_id').val(productid);

      var product_id = productid;

      var vendor_id  = $('#vendor_id').val();

      var purl = $('#purl').val();

      $.ajax({

         type: "post",

         headers: {

            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

         },

         data: {

            "_token": "{{ csrf_token() }}",

            "qty": qty,

            "product_id": product_id,

            "vendor_id": vendor_id

         },

         url: "{{url('/check_same_merchant')}}",

         success: function(msg) {

         if(msg.mstatus==1){

          apply_qty_add(product_id);

         }else{

        var ans = "Your cart contains product from "+msg.store1+". Do you want to discard the selection and add product from "+msg.store2+"?";

        $('#msgmodal').html(ans);

        $('#myModal').modal('show');

         }

        }

      });

   }

   $(document).ready(function() {
    $('#modalyes').click(function(e) {
     var productid  = $('#product_id').val();
     $('#myModal').modal('hide');

     if(productid!=''){

      apply_qty_add(productid);

    }

    });
});


   function apply_qty_add(productid){

      var qty = 1;

      var product_id = productid;

      var vendor_id  = $('#vendor_id').val();

      var purl = $('#purl').val();

      $.ajax({

         type: "post",

         headers: {

            'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')

         },

         data: {

            "_token": "{{ csrf_token() }}",

            "qty": qty,

            "product_id": product_id,

            "vendor_id": vendor_id

         },

         url: "{{url('/addtocart')}}",

         success: function(msg) {

          $('#session_qty').text(msg.cartcount);

          $(".dcart a").prop("href", "https://www.grambunny.com/cart");

          $("#addcartalt").show();

          $("#msgaddtocart").text(msg.msg);

         setTimeout(explode, 2000);

        //window.location.replace(purl);

        }

      });

   }

  function explode(){
  $("#addcartalt").hide();
  }

   $('#mprofileimg1').on('click', function() {

   var imgpath = $('#mprofileimg1').attr('src');

   $("#merchantimg").attr("src",imgpath);

   });

    $('#mprofileimg2').on('click', function() {

   var imgpath = $('#mprofileimg2').attr('src');

   $("#merchantimg").attr("src",imgpath);

   });

    $('#mprofileimg3').on('click', function() {

   var imgpath = $('#mprofileimg3').attr('src');

   $("#merchantimg").attr("src",imgpath);

   });

    $('#mprofileimg4').on('click', function() {

   var imgpath = $('#mprofileimg4').attr('src');

   $("#merchantimg").attr("src",imgpath);

   });


       $('#submit-merchant-review').on('click', function() {

        $.ajax({

          type: "post",

          headers: { 'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content') },

          data: $("#merchant-rating-form").serialize(),

          url: "{{url('/save-merchant-review')}}",

          success: function(data) {

            $("#message-box").show();

            $("#message-box").removeClass();

            if(data.ok){

              $("#message-box").addClass("alert alert-success alert-dismissable");

              $("#message").text(data.message);

            }else{



              var html = '<div id="message-box" class="alert alert-danger alert-dismissable"><i class="fa fa-check"></i><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><span id="message">'+data.message+'</span></div>';

              $('.msg-box').html(html);



              $("#message-box").addClass("alert alert-danger alert-dismissable");

              $("#message").text(data.message);

            }

              }

          });

      });



</script>

<!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModal">

    <div class="modal-dialog">
      <div class="modal-content bord-cann m-auto w-75 p-3">

        <!-- Modal Header -->
        <div class="modal-header border-0 pb-0 modal-box-cann">
          <h4 class="modal-title">Replace cart item?</h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body cann-modal pt-1">
       <p id="msgmodal">Your cart contains product from other store. Do you want to discard the selection and add product from New store?</p>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer border-0 cann-btn">
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-danger" id="modalyes">Yes</button>

        </div>

      </div>
    </div>
  </div>

  <script>
    function loadMore(id) {
        $("#more1-"+id).show();
        $("#more-"+id).hide();
        $("#myBtn1").show();
        $("#myBtn").hide();
}

 function loadLess(id) {
        $("#more1-"+id).hide();
        $("#more-"+id).show();
        $("#myBtn1").hide();
        $("#myBtn").show();
}
  </script>

   <script type="text/javascript">

  function popupvideoplay(id) {

    // $("#img_id").hide();

   $.ajax({

      type: "GET",

      dataType: "json",

      url: "<?php echo url('/user/videos_popup_play'); ?>",

      data: {'video_id': id},

       success: function(data){

     $("#videospopupplay").html(data);
  },

    });
  }

</script>
<!-- popup model end -->

@endsection