@extends("layouts.grambunny")

@section("styles")

{{-- styles goes here --}}

@endsection

@section("content")

{{-- content goes here --}}



<!--<div class="welcome-area wow fadeInUp contact-map" data-wow-delay="200ms" style="visibility: visible; animation-delay: 200ms; animation-name: fadeInUp;">

	<div class="google-maps">

<iframe width="" height="" src="https://maps.google.com/maps?width=&amp;height=&amp;hl=en&amp;q=5701%20Park%20Dr.%2C%20%237210%2C%20Chino%20Hills%2C%20CA%2091709+(grambunny)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="http://www.googlemapsgenerator.com/en/">gmapgen en</a> & <a href="https://youtubeembedcode.com/en/buy-youtube-views/">quality youtube backlinks</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style>

	</div>

</div>-->

<section id="ordr-grab-sec"><div class="container"><div data-wow-delay="0.1s" class="row  wow fadeInUp"><div class="col-lg-12"><div data-wow-delay="0.2s" class="web-heading wow fadeInUp" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"><h2>Contact us</h2> <p></p></div></div></div></div></section>

<!-- <section class="contactusHead" > 

	<div class="container">

		<div class="row">

			<div class="col-md-12">

				<div class="sl-contactform__details">

                    <h5>{{@$page_data->page_title}}</h5>

                    <p>{{@$page_data->sub_title}}</p>

                </div>

			</div>

		</div>

	</div>

</section> -->


<div class="site-section bg-light contactForm " style="padding-top:100px;">

	<div class="container">

	@if(Session::has('message'))		 

	<div class="alert alert-success alert-dismissable" style="width: 57%;">
    <i class="fa fa-check"></i>
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
    <!--{{Session::get('message')}}-->
    <b>Thank you!</b>
    <p>Your message has been successfully sent. We will contact you very soon!</p>
    </div>

	@endif	

	    <div class="row">

	      <div class="col-md-7 mb-5"  data-aos="fade">

	     <form role="form" action="{{ url('/contact-us') }}" class="p-5 bg-white for-Whit-Bck" method="POST">

	        	{!! csrf_field() !!}

	          <div class="row form-group">

	            <div class="col-md-6 mb-3 mb-md-0">

	              <label class="text-black" for="fname">First Name</label>

	              <input type="text" id="fname" name="fname" class="form-control" required="required">

	            </div>

	            <div class="col-md-6">

	              <label class="text-black" for="lname">Last Name</label>

	              <input type="text" id="lname" name="lname" class="form-control" required="required">

	            </div>

	          </div>


	          <div class="row form-group">
     
	            <div class="col-md-12">

	              <label class="text-black" for="email">Email</label> 

	              <input type="email" id="email" name="email" class="form-control" required="required">

	            </div>

	          </div>

	          <div class="row form-group">
 
	            <div class="col-md-12">

	              <label class="text-black" for="subject">Subject</label> 

	              <input type="subject" id="subject" name="subject" class="form-control" required="required">

	            </div>

	          </div>



	          <div class="row form-group">

	            <div class="col-md-12">

	              <label class="text-black" for="message">Message</label> 

	              <textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Write your notes or questions here..." required="required"></textarea>

	            </div>

	          </div>


	          <div class="row form-group">

	            <div class="col-md-12">

	              <input type="submit" value="Send Message" class="btn btn-primary py-2 px-4 text-white">

	            </div>

	          </div>


	        </form>

	      </div>

	      <div class="col-md-5"  data-aos="fade" data-aos-delay="100">

	        
	        <div class="p-4 mb-3 bg-white for-Whit-Bck fpnt-family">

	        <?php echo $page_data->page_content; ?>	

	        </div>

	        <div class="map_ifram">
	        	
	        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d26449.67497395459!2d-117.594302!3d34.0385!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c33520db391c1d%3A0xb2072b6dde0210c2!2s1920%20S%20Archibald%20Ave%2C%20Ontario%2C%20CA%2091761!5e0!3m2!1sen!2sus!4v1640773100971!5m2!1sen!2sus" width="100%" height="370" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	
	        </div>
	        
	      </div>

	    </div>

	</div>

</div>



@endsection

@section("scripts")

{{-- scripts goes here --}}

@endsection