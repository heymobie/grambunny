/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import swal from 'sweetalert';
//  import {Loader, LoaderOptions} from 'google-maps';
// const loader = new Loader('AIzaSyB-pjqPgNOISQDRg5Vo7DxkdFwV6D_EKHY', {});
// import GoogleMapsLoader from 'google-maps'

window.Vue = require('vue');
import SearchField from "./components/Home/SearchField.vue";
import NearByMap from "./components/Home/Map.vue";
import Products from "./components/Product/Products.vue";
import ProductsFilter from "./components/Product/Filter.vue";
import OrderSummary from "./components/Product/ApplyPromoCode.vue";
import States from "./components/Others/States.vue";
import Axios from 'axios';
 
  
  
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
export const eventBus = new Vue();
const app = new Vue({
    data(){
        return{
            login:{
                email:"",
                password:""
            },
            productType:"",
            categories:[],
            selectedCategory:"",
            subCatgories:[],
            selectedSubCategory:""
        }
    },
    methods:{
         showSwal(title,text,icon){
            swal({
                title: title,
                text: text,
                icon: icon,
            });
         },
         getCategory(){
            // console.log();
            // this.categories=[];
             this.selectedCategory="";
             Axios.get("get-categories.json",{
                 params:{
                    type:this.productType
                }
             }).then(response=> {
                 //console.log(response);
                 this.categories=response.data.categories;
             });
         },
         getSubCategories(){
            axios.get("/get-sub-categories.json",{
                params:{
                    category:this.selectedCategory
                }
            }).then(response => {
                this.subCatgories=response.data.subCategories;
            })
        }

    },
    el: '#app',
    components:{
        "app-search-field" : SearchField, 
        "app-products":Products,
        "app-products-filter":ProductsFilter,
        "app-nearby-map":NearByMap,
        'app-order-summary':OrderSummary,
        "app-select-states":States
    } 
});


